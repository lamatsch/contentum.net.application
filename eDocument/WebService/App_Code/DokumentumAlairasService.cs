﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Services;
using System.Xml;

/// <summary>
/// Summary description for DokumentumAlairasService
/// </summary>
[WebService(Namespace = "Contentum.eDocument.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class DokumentumAlairasService : System.Web.Services.WebService
{
    private KRT_DokumentumAlairasokStoredProcedure sp = null;
    private DataContext dataContext;

    public DokumentumAlairasService()
    {
        dataContext = new DataContext(this.Application);
        sp = new KRT_DokumentumAlairasokStoredProcedure(dataContext);
    }

    public DokumentumAlairasService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new KRT_DokumentumAlairasokStoredProcedure(dataContext);
    }

    [WebMethod]
    public Result IratCsatolmanyokAlairas(ExecParam execParam, string[] csatolmanyIdsArray, string megjegyzes)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        #region Paraméterek ellenőrzése

        if (execParam == null
            || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || csatolmanyIdsArray == null
            || csatolmanyIdsArray.Length == 0)
        {
            // hiba: Nem megfelelő paraméterek
            // (Hiba az iratcsatolmányok aláírása során!)
            return ResultError.CreateNewResultWithErrorCode(52750);
        }

        #endregion Paraméterek ellenőrzése

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        Result result_IratAlairokGetAll = new Result();
        EREC_IratAlairokService service_IratAlairok = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IratAlairokService();

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            /// - Csatolmányok ellenőrzése, IratId meghatározása
            /// - Előjegyzett aláírás lekérése, ellenőrzés
            /// - Vizsgálat: Szerveroldali? Aláírásmód? (M_ADM esetén nem kell szerveroldali aláírás)
            /// - Filetartalmak lekérése Sharepointból
            /// - Szerveroldali aláírás
            /// - Aláírt file feltöltése
            /// - Aláírásadatok adminisztrálása
            ///

            #region Csatolmányok ellenőrzése, IratId meghatározása

            /// eRecordWebservice hívás
            EREC_CsatolmanyokService service_csatolmanyok = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

            StringBuilder sb_csatIds = new StringBuilder();
            for (int i = 0; i < csatolmanyIdsArray.Length; i++)
            {
                if (i == 0)
                {
                    sb_csatIds.Append("'" + csatolmanyIdsArray[i] + "'");
                }
                else
                {
                    sb_csatIds.Append(",'" + csatolmanyIdsArray[i] + "'");
                }
            }

            search_csatolmanyok.Id.Value = sb_csatIds.ToString();
            search_csatolmanyok.Id.Operator = Query.Operators.inner;

            Result result_csatGetAll = service_csatolmanyok.GetAll(execParam.Clone(), search_csatolmanyok);

            if (result_csatGetAll.IsError)
            {
                // hiba:
                throw new ResultException(result_csatGetAll);
            }

            // Találatok számának ellenőrzése:
            if (result_csatGetAll.Ds.Tables[0].Rows.Count != csatolmanyIdsArray.Length)
            {
                // Hiba a csatolmányok lekérdezése során!
                Logger.Error("Hiba a csatolmányok lekérdezése során! Találatok száma nem egyezik...", execParam);
                throw new ResultException(52752);
            }

            // Ellenőrzés, minden csatolmánynak ugyanaz-e az iratId-ja:
            string iratId = result_csatGetAll.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();

            foreach (DataRow row_csat in result_csatGetAll.Ds.Tables[0].Rows)
            {
                string row_IraIrat_Id = row_csat["IraIrat_Id"].ToString();
                if (row_IraIrat_Id != iratId)
                {
                    // hiba:
                    Logger.Error("Hiba a csatolmányok lekérdezése során! Nem egyeznek az iratId-k. row_IraIrat_Id==" + row_IraIrat_Id + " iratId==" + iratId, execParam);
                    throw new ResultException(52752);
                }
            }

            #endregion Csatolmányok ellenőrzése, IratId meghatározása

            #region Előjegyzett aláírás lekérése, ellenőrzés

            AlairasokService service_Alairasok = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetAlairasokService();

            Result result_ElojegyzettAlairas = service_Alairasok.GetAll_PKI_ElojegyzettAlairas(execParam.Clone(), String.Empty, "Irat", iratId);

            if (result_ElojegyzettAlairas.IsError)
            {
                // hiba:
                throw new ResultException(result_ElojegyzettAlairas);
            }

            if (result_ElojegyzettAlairas.Ds.Tables[0].Rows.Count == 0
                || String.IsNullOrEmpty(result_ElojegyzettAlairas.Ds.Tables[0].Rows[0]["AlairasSzabaly_Id"].ToString()))
            {
                // Ön nincs felvéve az előjegyzett aláírók közé, vagy Önhöz nem található aláírószabály.
                throw new ResultException(52751);
            }

            DataRow row_ElojegyzettAlairas = result_ElojegyzettAlairas.Ds.Tables[0].Rows[0];

            string alairasMod = row_ElojegyzettAlairas["AlairasMod"].ToString();
            string alairasSzint = row_ElojegyzettAlairas["AlairasSzint"].ToString();
            string alairasSzabalyId = row_ElojegyzettAlairas["AlairasSzabaly_Id"].ToString();
            string kivarasiIdo = row_ElojegyzettAlairas["KivarasiIdo"].ToString();
            string konfigTetel = row_ElojegyzettAlairas["KonfigTetel"].ToString();
            string tanusitvanyId = row_ElojegyzettAlairas["Tanusitvany_Id"].ToString();
            string alairoId = row_ElojegyzettAlairas["Alairo_Id"].ToString();
            string alairasTulajdonos = row_ElojegyzettAlairas["AlairasTulajdonos"].ToString();
            string kivitelezes = row_ElojegyzettAlairas["Kivitelezes"].ToString();

            // Szerveroldali aláírás kell?
            bool kellSzerveroldaliAlairas = false;
            bool alairasAdatokatNyiltDokumentumhozKapcsol = false;

            if (kivitelezes == "Kliens")
            {
                // hiba: Nem szerveroldali az aláírás típusa!
                Logger.Error("Nem szerveroldali az aláírástípus! (Kivitelezes: " + kivitelezes + ")", execParam);
                throw new ResultException(52753);
            }
            else if (kivitelezes == "Szerver")
            {
                kellSzerveroldaliAlairas = true;
            }

            if (alairasMod == "M_ADM" || Rendszerparameterek.Get(execParam.Clone(), "PKI_VENDOR") != "SDXM")
            {
                kellSzerveroldaliAlairas = false;
                alairasAdatokatNyiltDokumentumhozKapcsol = true;
            }

            #endregion Előjegyzett aláírás lekérése, ellenőrzés

            #region EREC_IratAlairok rekord megkeresése

            // Majd a végén update-eljük, de most lekérjük, hogy ne húzzuk majd vele a tranzakciót
            EREC_IratAlairokSearch search_iratAlairok = new EREC_IratAlairokSearch();

            // Keresés: az adott irathoz a felhasznalo az aláíró, és állapot aláírandó

            search_iratAlairok.Obj_Id.Value = iratId;
            search_iratAlairok.Obj_Id.Operator = Query.Operators.equals;

            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Value = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;
            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Group = "alairo";
            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.GroupOperator = Query.Operators.or;

            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.Value = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.Operator = Query.Operators.equals;
            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.Group = "alairo";
            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.GroupOperator = Query.Operators.or;

            search_iratAlairok.Allapot.Value = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
            search_iratAlairok.Allapot.Operator = Query.Operators.equals;

            result_IratAlairokGetAll = service_IratAlairok.GetAll(execParam.Clone(), search_iratAlairok);
            if (result_IratAlairokGetAll.IsError)
            {
                // hiba:
                throw new ResultException(result_IratAlairokGetAll);
            }

            Logger.Info("EREC_IratAlairok lekérése: találatok száma: " + result_IratAlairokGetAll.Ds.Tables[0].Rows.Count);

            #endregion EREC_IratAlairok rekord megkeresése

            #region Feltöltéshez szükséges adatok lekérése (Főszám, Iktatóhely, stb...) (Tárolási helyhez)

            #region Ügyirat, Iktatókönyv lekérése

            EREC_IraIratokService service_iratok = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();

            ExecParam execParam_iratHier = execParam.Clone();
            execParam_iratHier.Record_Id = iratId;

            Result result_iratHier = service_iratok.GetIratHierarchia(execParam_iratHier);
            if (result_iratHier.IsError)
            {
                // hiba:
                throw new ResultException(result_iratHier);
            }

            IratHierarchia iratHier = (IratHierarchia)result_iratHier.Record;

            // Iktatókönyv lekérése Iktatóhelyhez:
            EREC_IraIktatoKonyvekService service_iktKonyv = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();

            ExecParam execParam_iktKonyvGet = execParam.Clone();
            execParam_iktKonyvGet.Record_Id = iratHier.UgyiratObj.IraIktatokonyv_Id;

            Result result_iktKonyvGet = service_iktKonyv.Get(execParam_iktKonyvGet);
            if (result_iktKonyvGet.IsError)
            {
                // hiba:
                throw new ResultException(result_iktKonyvGet);
            }

            EREC_IraIktatoKonyvek iktatoKonyv = (EREC_IraIktatoKonyvek)result_iktKonyvGet.Record;

            #endregion Ügyirat, Iktatókönyv lekérése

            #endregion Feltöltéshez szükséges adatok lekérése (Főszám, Iktatóhely, stb...) (Tárolási helyhez)

            #region Aláírandó fájl(ok) tartalmának lekérése

            #region Linkek, fájlnevek lekérése a KRT_Dokumentumok táblából

            KRT_DokumentumokService krt_DokumentumokService = new KRT_DokumentumokService(this.dataContext);

            // dokumentumId-k összeállítása:
            StringBuilder sb_dokIds = new StringBuilder();
            foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
            {
                string row_Dokumentum_Id = row["Dokumentum_Id"].ToString();

                if (sb_dokIds.Length == 0)
                {
                    sb_dokIds.Append("'" + row_Dokumentum_Id + "'");
                }
                else
                {
                    sb_dokIds.Append(",'" + row_Dokumentum_Id + "'");
                }
            }

            KRT_DokumentumokSearch search_krtDok = new KRT_DokumentumokSearch();

            search_krtDok.Id.Value = sb_dokIds.ToString();
            search_krtDok.Id.Operator = Query.Operators.inner;

            Result result_krtDokGetAll = krt_DokumentumokService.GetAll(execParam.Clone(), search_krtDok);
            if (result_krtDokGetAll.IsError)
            {
                // hiba:
                throw new ResultException(result_krtDokGetAll);
            }

            // Fájladatok eltárolásához:
            Dictionary<string, AlairandoDok> alairandoDokDict = new Dictionary<string, AlairandoDok>();

            foreach (DataRow row in result_krtDokGetAll.Ds.Tables[0].Rows)
            {
                string row_Id = row["Id"].ToString();
                string row_External_Link = row["External_Link"].ToString();
                string row_External_Info = row["External_Info"].ToString();
                string row_FajlNev = row["FajlNev"].ToString();

                if (alairandoDokDict.ContainsKey(row_Id) == false)
                {
                    AlairandoDok alairandoDok = new AlairandoDok();
                    alairandoDok.fileUrl_eredeti = row_External_Link;
                    alairandoDok.fileNev_eredeti = row_FajlNev;
                    alairandoDok.external_Info_eredeti = row_External_Info;

                    alairandoDokDict.Add(row_Id, alairandoDok);
                }
            }

            #endregion Linkek, fájlnevek lekérése a KRT_Dokumentumok táblából

            #region Fájltartalmak lekérése

            // Csak ha kell szerveroldali aláírás:
            if (kellSzerveroldaliAlairas)
            {
                foreach (KeyValuePair<string, AlairandoDok> kvp in alairandoDokDict)
                {
                    AlairandoDok alairandoDok = kvp.Value;

                    string dokUrl = alairandoDok.fileUrl_eredeti;

                    // SharePoint-ból fájltartalom lekérése:
                    alairandoDok.fileTartalom_eredeti = GetFileContent(dokUrl);

                    if (alairandoDok.fileTartalom_eredeti == null || alairandoDok.fileTartalom_eredeti.Length == 0)
                    {
                        // Hiba az aláírás során: dokumentum lekérése sikertelen!
                        throw new ResultException(52754);
                    }
                }
            }

            #endregion Fájltartalmak lekérése



            #endregion Aláírandó fájl(ok) tartalmának lekérése

            #region Szerveroldali aláírás (ha kell)

            if (kellSzerveroldaliAlairas)
            {
                foreach (KeyValuePair<string, AlairandoDok> kvp in alairandoDokDict)
                {
                    AlairandoDok alairandoDok = kvp.Value;

                    SzerverOldaliAlairas(alairandoDok.fileTartalom_eredeti, alairandoDok.fileNev_eredeti, konfigTetel
                        , out alairandoDok.fileTartalom_alairt, out alairandoDok.fileNev_alairt);
                }
            }

            #endregion Szerveroldali aláírás (ha kell)

            #region Aláírt file(ok) feltöltése (ha volt szerveroldali aláírás)

            DocumentService documentService = new DocumentService();
            Contentum.eDocument.SharePoint.DocumentService spsDs = new Contentum.eDocument.SharePoint.DocumentService();

            string documentStoreType = Contentum.eUtility.Constants.DocumentStoreType.SharePoint;

            // csak ha volt szerveroldali aláírás:
            if (kellSzerveroldaliAlairas)
            {
                foreach (KeyValuePair<string, AlairandoDok> kvp in alairandoDokDict)
                {
                    AlairandoDok alairandoDok = kvp.Value;

                    string rootSiteCollectionUrl;
                    string doktarSitePath;
                    string doktarDocLibPath;
                    string doktarFolderPath;

                    // Tárolási struktúra:
                    DocumentService.Utility.GetSpsTarolasiHelyStrukturaFromExternalInfo(alairandoDok.external_Info_eredeti
                        , out rootSiteCollectionUrl, out doktarSitePath, out doktarDocLibPath, out doktarFolderPath);

                    // Aláírt fájl feltöltése:
                    Result result_alirtFileUpload = AlairtFileFeltoltes(spsDs, alairandoDok.fileTartalom_alairt, alairandoDok.fileNev_alairt
                        , documentStoreType, rootSiteCollectionUrl, doktarSitePath, doktarDocLibPath, doktarFolderPath
                        , out alairandoDok.external_Id_alairt, out alairandoDok.external_Link_alairt, out alairandoDok.external_Source_alairt, out alairandoDok.external_Info_alairt);

                    if (result_alirtFileUpload.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_alirtFileUpload);
                    }
                }
            }

            #endregion Aláírt file(ok) feltöltése (ha volt szerveroldali aláírás)

            #region Aláírt dokumentumok regisztrálása

            foreach (KeyValuePair<string, AlairandoDok> kvp in alairandoDokDict)
            {
                string dokumentumId = kvp.Key;
                AlairandoDok alairandoDok = kvp.Value;

                #region BLG 7148 kr kiküldés beállítása

                try
                {
                    if (alairandoDok != null && !string.IsNullOrEmpty(alairandoDok.fileNev_eredeti))
                    {
                        string ext = System.IO.Path.GetExtension(alairandoDok.fileNev_eredeti).Replace(".", "").Trim();
                        bool isKrFile = ext.Equals("KR", StringComparison.CurrentCultureIgnoreCase) || ext.Equals("KRX", StringComparison.CurrentCultureIgnoreCase);
                        if (!string.IsNullOrEmpty(ext) && isKrFile)
                        {
                            alairasSzint = "5";
                        }
                    }
                }
                catch (Exception)
                {
                }

                #endregion BLG 7148 kr kiküldés beállítása

                if (kellSzerveroldaliAlairas)
                {
                    // Aláírás adminisztrálása:
                    AlairtDokumentumRegisztralasa(execParam.Clone(), dokumentumId, alairandoDok.fileTartalom_alairt, alairandoDok.fileNev_alairt
                        , alairasSzabalyId, alairasMod, alairasSzint, alairasTulajdonos, tanusitvanyId, kivarasiIdo
                        , alairandoDok.external_Link_alairt, alairandoDok.external_Id_alairt, alairandoDok.external_Source_alairt, alairandoDok.external_Info_alairt);
                }
                else
                {
                    if (alairasAdatokatNyiltDokumentumhozKapcsol)
                    {
                        // KRT_DokumentumAlairasok adatokat a fő KRT_Dokumentumok-hoz kapcsoljuk:
                        Insert_KRT_DokumentumAlairasok(execParam, dokumentumId, alairasMod, alairasSzabalyId, alairasSzint
                            , alairasTulajdonos, tanusitvanyId, kivarasiIdo);
                    }

                    // Csak a fődokumentumban kell beadminisztrálni az aláírást:
                    UpdateFoDokumentumElektronikusAlairas(execParam, krt_DokumentumokService, dokumentumId, alairasSzint);
                }
            }

            #endregion Aláírt dokumentumok regisztrálása

            #region TODO: Eseménynaplózás

            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid,).Record;

            //    Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            //}

            #endregion TODO: Eseménynaplózás

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);

            // TODO: esetlegesen feltöltött fájlok kiszedése Sharepointból valahogy...
            //Result tmpRes = DeleteExternalFile(documentStoreType, documentSite, documentStore, documentFolder, filename);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        #region EREC_IratAlairok-ban az aláírás beregisztrálása

        if (!result.IsError)
        {
            // már korábban lekértük az iratAlairok rekordot

            if (result_IratAlairokGetAll.Ds.Tables[0].Rows.Count > 0)
            {
                Logger.Info("EREC_IratAlairok-ban az aláírás beregisztrálása");

                DataRow row_IratAlairok = result_IratAlairokGetAll.Ds.Tables[0].Rows[0];

                string iratAlairok_Id = row_IratAlairok["Id"].ToString();
                string iratAlairok_Ver = row_IratAlairok["Ver"].ToString();

                EREC_IratAlairok iratAlairok = new EREC_IratAlairok();
                iratAlairok.Updated.SetValueAll(false);
                iratAlairok.Base.Updated.SetValueAll(false);

                // Állapot állítása aláírt-ra:
                iratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                iratAlairok.Updated.Allapot = true;

                //iratAlairok.FelhasznaloCsoport_Id_Alairo = execParam.Felhasznalo_Id;
                //iratAlairok.Updated.FelhasznaloCsoport_Id_Alairo = true;

                iratAlairok.FelhaszCsoport_Id_Helyettesito = execParam.LoginUser_Id;
                iratAlairok.Updated.FelhaszCsoport_Id_Helyettesito = true;

                iratAlairok.AlairasDatuma = DateTime.Now.ToString();
                iratAlairok.Updated.AlairasDatuma = true;

                // Megjegyzés
                iratAlairok.Leiras = megjegyzes;
                iratAlairok.Updated.Leiras = true;

                // Verzió megadása:
                iratAlairok.Base.Ver = iratAlairok_Ver;
                iratAlairok.Base.Updated.Ver = true;

                ExecParam execParam_iratAlairokUpdate = execParam.Clone();
                execParam_iratAlairokUpdate.Record_Id = iratAlairok_Id;

                Result result_iratAlairokUpdate = service_IratAlairok.Update(execParam_iratAlairokUpdate, iratAlairok);
                if (result_iratAlairokUpdate.IsError)
                {
                    // hiba:
                    Logger.Error("EREC_IratAlairok.Update hiba", execParam_iratAlairokUpdate, result_iratAlairokUpdate);
                    result = result_iratAlairokUpdate;
                }
            }
        }

        #endregion EREC_IratAlairok-ban az aláírás beregisztrálása

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// GetAll(ExecParam ExecParam, KRT_DokumentumAlairasokSearch _KRT_DokumentumAlairasokSearch)
    /// A(z) KRT_DokumentumAlairasok táblára vonatkozó keresés eredményhalmazának elkérése. A táblára vonatkozó szűrési feltételeket paraméter tartalmazza (*Search).
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam.Record_Id nem használt, további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).</param>
    /// <param name="_KRT_DokumentumAlairasokSearch">Bemenő paraméter, a keresési feltételeket tartalmazza. </param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott táblára vonatkozó keresés eredményhalmazának elkérése. A táblára vonatkozó szűrési feltételeket paraméter tartalmazza (*Search). Az ExecParam.Record_Id nem használt, a további adatok a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_DokumentumAlairasokSearch))]
    public Result GetAll(ExecParam ExecParam, KRT_DokumentumAlairasokSearch _KRT_DokumentumAlairasokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _KRT_DokumentumAlairasokSearch);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private class AlairandoDok
    {
        public string fileNev_eredeti;
        public string fileNev_alairt;

        public string fileUrl_eredeti;

        public byte[] fileTartalom_eredeti;
        public byte[] fileTartalom_alairt;

        public string external_Id_alairt;
        public string external_Link_alairt;
        public string external_Source_alairt;

        public string external_Info_eredeti;
        public string external_Info_alairt;
    }

    #region Segédfüggvények

    public void SzerverOldaliAlairas(byte[] feltoltottFileByteArray, string filename, string easzKod, out byte[] alairtTartalom, out string alairtFileNev)
    {
        #region Szerver oldali aláírás:

        string eASZBusinessServiceUrl = UI.GetAppSetting("eASZBusinessServiceUrl");
        Logger.Info("eASZBusinessServiceUrl: " + eASZBusinessServiceUrl);

        if (string.IsNullOrEmpty(eASZBusinessServiceUrl))
        {
            throw new ResultException("Nincs megadva az aláírószerver URL (eASZBusinessServiceUrl a web.config-ban)");
        }

        // == EASZKod, a szerver oldali aláíráshoz kell
        int ConfigId = 0;

        try
        {
            ConfigId = Int32.Parse(easzKod);
        }
        catch
        {
            // hiba:
            Logger.Error(String.Format("easzKod nem megfelelő: '{0}'", easzKod));
            throw new ResultException("EASZKod értéke nem megfelelő");
        }

        eSign.SDXM.Url = eASZBusinessServiceUrl;

        // Aláírás:
        string feltoltottFileTartalom_Base64String = System.Convert.ToBase64String(feltoltottFileByteArray);
        eSign.common.SignResult sr = eSign.SDXM.SignDirectly(ConfigId, filename, feltoltottFileTartalom_Base64String);

        if (sr == null || string.IsNullOrEmpty(sr.Result))
        {
            // hiba:
            Logger.Error("Szerver oldali aláírás sikertelen");
            throw new ResultException("Szerver oldali aláírás sikertelen");
        }

        alairtTartalom = System.Convert.FromBase64String(sr.Result);
        alairtFileNev = filename + ".sdxm";

        Logger.Info("File aláírása ok. File név: " + alairtFileNev + " Aláírt file mérete: " + alairtTartalom.Length + "byte");

        #endregion Szerver oldali aláírás:
    }

    public Result AlairtFileFeltoltes(Contentum.eDocument.SharePoint.DocumentService spsDs, byte[] alairtTartalom, string alairtFileNev
        , string documentStoreType, string rootSiteCollectionUrl, string doktarSitePath, string doktarDocLibPath, string doktarFolderPath
        , out string external_Id, out string external_Link, out string external_Source, out string external_Info)
    {
        Result _ret = new Result();

        external_Id = "";
        external_Link = "";
        external_Source = "";
        external_Info = "";

        #region ellenorzes, h van-e mar olyan file a cel helyen -> ha igen checkout kell

        Logger.Info("Szerver oldali alairas: ellenorzes, h van-e mar olyan file a cel helyen -> ha igen checkout kell");

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            #region van-e ilyen a cel helye spsben ellenorzes

            Logger.Info("ellenorzes, h van-e mar olyan file a cel helyen -> ha igen checkout kell... resz kezdodik");

            bool vanUgyanolyanFileACelHelyen = false;

            Contentum.eDocument.SharePoint.ListUtilService ds = new Contentum.eDocument.SharePoint.ListUtilService();
            Result retSpsbenVaneIlyenFile = ds.GetListItemsByItemNameAppendWithPath(RemoveSlash(doktarSitePath)
                , RemoveSlash(doktarDocLibPath)
                , RemoveSlash(doktarFolderPath)
                , alairtFileNev);

            if (!String.IsNullOrEmpty(retSpsbenVaneIlyenFile.ErrorCode))
            {
                Logger.Error(String.Format("Szerver oldali alairas: Hiba az item nev alapjan leztezes vizsgalatkor!\nHibakod: {0}\nHibauzenet: {1}", retSpsbenVaneIlyenFile.ErrorCode, retSpsbenVaneIlyenFile.ErrorMessage));
                return retSpsbenVaneIlyenFile;
            }

            XmlDocument xmlDoc = new XmlDocument();

            try
            {
                xmlDoc.LoadXml(Convert.ToString(retSpsbenVaneIlyenFile.Record));
            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("Szerver oldali alairas: Hiba az item nev alapjan leztezes vizsgalatkor!\nHibakod: {0}\nHibauzenet: {1}", retSpsbenVaneIlyenFile.ErrorCode, retSpsbenVaneIlyenFile.ErrorMessage));
                Result retXmlError = new Result();
                retXmlError.ErrorCode = "ALAIRXMLP001";
                retXmlError.ErrorMessage = String.Format("{0}\n {1}", exc.Message, exc.StackTrace);
                return retXmlError;
            }

            Logger.Info(String.Format("Szerver oldali alairas: GetListItemsByItemName adott xml eredmeny: {0}", xmlDoc.OuterXml.ToString()));

            int dbTalalt = ds.GetItemCount(xmlDoc.FirstChild);

            Logger.Info(String.Format("spsben nev alapjan talalt db: {0}", dbTalalt));

            if (dbTalalt > 0)
            {
                vanUgyanolyanFileACelHelyen = true;
            }

            #endregion van-e ilyen a cel helye spsben ellenorzes

            #region checkout

            if (vanUgyanolyanFileACelHelyen)
            {
                Logger.Info("Szerver oldali alairas: van ilyen a cel helyen checkout kell.");

                try
                {
                    using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                    {
                        string itemSitePath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                            + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                            + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);

                        String res = upld3.ItemCheckout(itemSitePath
                            , RemoveSlash(doktarDocLibPath)
                            , RemoveSlash(doktarFolderPath)
                            , alairtFileNev);

                        XmlDocument resultDoc = new XmlDocument();
                        try
                        {
                            resultDoc.LoadXml(res);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(String.Format("Szerver oldali alairas: Hiba az ItemCheckout altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                            _ret.ErrorCode = "ItemCheckout";
                            _ret.ErrorMessage = "Szerver oldali alairas: Hiba az ItemCheckout függvény által visszaadott string xml konvertálásakor!";
                        }

                        if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                        {
                            _ret.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                            _ret.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                            Logger.Error(String.Format("Szerver oldali alairas: Hiba az spsen levo Upload.ItemCheckout fuggvenyben! Ezt adta vissza: {0}\n{1}", _ret.ErrorCode, _ret.ErrorMessage));
                            return _ret;
                        }
                    }
                }
                catch (Exception exc)
                {
                    Logger.Error(String.Format("Szerver oldali alairas: Hiba a file checkoutkor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                    Logger.Error(String.Format("Szerver oldali alairas: A hiba oka az AxisFileUpload WSben van az SPSen!"));
                    Result ret = new Result();
                    ret.ErrorCode = "ALAItemCheckout";
                    ret.ErrorMessage = String.Format("Szerver oldali alairas: Hiba a file checkout-elésekor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                    return ret;
                }
            }

            #endregion checkout
        }

        #endregion ellenorzes, h van-e mar olyan file a cel helyen -> ha igen checkout kell

        #region Aláírt file feltoltese...

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.SharePoint)
        {
            Result alairtFeltoltottFileRet = spsDs.Upload(RemoveSlash(doktarSitePath)
                , RemoveSlash(doktarDocLibPath)
                , RemoveSlash(doktarFolderPath)
                , alairtFileNev
                , alairtTartalom);

            if (alairtFeltoltottFileRet.IsError)
            {
                Logger.Error("Aláírt file Sharepoint-ba feltoltese hiba: " + alairtFeltoltottFileRet.ErrorCode + "; " + alairtFeltoltottFileRet.ErrorMessage);
                return alairtFeltoltottFileRet;
            }

            Logger.Info(String.Format("alairtFeltoltottFileRet.Uid: {0}", alairtFeltoltottFileRet.Uid));

            // sharepoint-os document id
            external_Id = alairtFeltoltottFileRet.Uid;

            // url:
            external_Link = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath)
                + ((!doktarDocLibPath.EndsWith("/") && !String.IsNullOrEmpty(doktarDocLibPath)) ? doktarDocLibPath + "/" : doktarDocLibPath)
                + ((!doktarFolderPath.EndsWith("/") && !String.IsNullOrEmpty(doktarFolderPath)) ? doktarFolderPath + "/" : doktarFolderPath)
                + alairtFileNev;

            external_Source = "SharePoint";

            external_Info =
                Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                + ";" + rootSiteCollectionUrl
                + ";" + RemoveSlash(doktarSitePath.Trim())
                + ";" + RemoveSlash(doktarDocLibPath.Trim())
                + ";" + RemoveSlash(doktarFolderPath.Trim());

            //Logger.Info(String.Format("ujKrtDokBejegyzesKell: {0} || munkaanyag: {1} -> checkInMode: {2} (0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn)", ujKrtDokBejegyzesKell, munkaanyag, checkInMode));

            //Contentum.eDocument.SharePoint.FolderService fos = new Contentum.eDocument.SharePoint.FolderService();

            //Result checkinRet = spsDs.CheckIn(RemoveSlash(doktarSitePath), RemoveSlash(doktarDocLibPath), RemoveSlash(doktarFolderPath), alairtFileNev, String.Empty, "1");
            //if (!String.IsNullOrEmpty(checkinRet.ErrorCode))
            //{
            //    /// -2130245361 = nincs beállítva a folderen a verzio kezeles beallitva
            //    if (!checkinRet.ErrorCode.Equals("-2130245361"))
            //    {
            //        Logger.Error("Sharepoint-ba feltoltott SDXM file checkin hiba: " + checkinRet.ErrorCode + "; " + checkinRet.ErrorMessage);
            //        return checkinRet;
            //    }
            //    else
            //    {
            //        Logger.Info("Sharepoint-ba feltoltott SDXM file checkin hiba: Nincs beallitva a verziokezeles a folderen!");
            //    }
            //}

            try
            {
                using (AxisUploadToSps.Upload upld3 = GetAxisUploadToSps())
                {
                    string itemSitePath = Contentum.eUtility.UI.GetAppSetting("SharePointUrl")
                        + ((!rootSiteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(rootSiteCollectionUrl)) ? rootSiteCollectionUrl + "/" : rootSiteCollectionUrl)
                        + ((!doktarSitePath.EndsWith("/") && !String.IsNullOrEmpty(doktarSitePath)) ? doktarSitePath + "/" : doktarSitePath);

                    String res = upld3.ItemCheckin(itemSitePath
                        , RemoveSlash(doktarDocLibPath)
                        , doktarFolderPath
                        , alairtFileNev
                        , "1"
                        , String.Empty);

                    XmlDocument resultDoc = new XmlDocument();
                    try
                    {
                        resultDoc.LoadXml(res);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(String.Format("Hiba az ItemCheckin altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                        _ret.ErrorCode = "ItemCheckin";
                        _ret.ErrorMessage = "Hiba az ItemCheckin függvény által visszaadott string xml konvertálásakor!";
                    }

                    if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                    {
                        _ret.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                        _ret.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                        Logger.Error(String.Format("Hiba az spsen levo Upload.ItemCheckin fuggvenyben! Ezt adta vissza: {0}\n{1}", _ret.ErrorCode, _ret.ErrorMessage));
                        return _ret;
                    }
                }
            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("Hiba a file checkinkor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                Logger.Error(String.Format("A hiba ok az AxisFileUpload WSben van az SPSen!"));
                Result ret = new Result();
                ret.ErrorCode = "ItemCheckin";
                ret.ErrorMessage = String.Format("Hiba a file checkin-elésekor! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace);
                return ret;
            }
        }

        #endregion Aláírt file feltoltese...

        return _ret;
    }

    public void AlairtDokumentumRegisztralasa(ExecParam execparam, string dokumentumId
        , byte[] alairtTartalom, string alairtFileNev
        , string alairasSzabalyId, string alairasMod, string alairasSzint, string alairasTulajdonos, string tanusitvanyId, string kivarasiIdo
        , string external_Link, string external_Id, string external_Source, string external_Info)
    {
        #region KRT_Dokumentumok INSERT (aláírt fájlra)

        Logger.Info("KRT_Dokumentumok INSERT Start...");

        //KRT_AlairtDokumentumok krt_alairtDokumentum = new KRT_AlairtDokumentumok();
        KRT_Dokumentumok krt_Dokumentumok_alairt = new KRT_Dokumentumok();

        krt_Dokumentumok_alairt.FajlNev = alairtFileNev;
        krt_Dokumentumok_alairt.Updated.FajlNev = true;

        // External_Link:
        krt_Dokumentumok_alairt.External_Link = external_Link;
        krt_Dokumentumok_alairt.Updated.External_Link = true;

        krt_Dokumentumok_alairt.External_Id = external_Id;
        krt_Dokumentumok_alairt.Updated.External_Id = true;

        krt_Dokumentumok_alairt.External_Source = external_Source;
        krt_Dokumentumok_alairt.Updated.External_Source = true;

        krt_Dokumentumok_alairt.External_Info = external_Info;
        krt_Dokumentumok_alairt.Updated.External_Info = true;

        krt_Dokumentumok_alairt.Csoport_Id_Tulaj = Csoportok.GetFelhasznaloSajatCsoportId(execparam);
        krt_Dokumentumok_alairt.Updated.Csoport_Id_Tulaj = true;

        krt_Dokumentumok_alairt.Meret = alairtTartalom.Length.ToString();
        krt_Dokumentumok_alairt.Updated.Meret = true;

        krt_Dokumentumok_alairt.Allapot = KodTarak.DOKUMENTUM_ALLAPOT.Iktatorendszerben_nyilvantartott;
        krt_Dokumentumok_alairt.Updated.Allapot = true;

        krt_Dokumentumok_alairt.Formatum = "sdxm";
        krt_Dokumentumok_alairt.Updated.Formatum = true;

        // TODO:
        krt_Dokumentumok_alairt.Tipus = KodTarak.DOKUMENTUMTIPUS.Egyeb; //"Egyéb";
        krt_Dokumentumok_alairt.Updated.Tipus = true;

        krt_Dokumentumok_alairt.Alkalmazas_Id = execparam.Alkalmazas_Id;
        krt_Dokumentumok_alairt.Updated.Alkalmazas_Id = true;

        // krt_Dokumentumok_alairt.Dokumentum_Id ??

        krt_Dokumentumok_alairt.ElektronikusAlairas = alairasSzint;
        krt_Dokumentumok_alairt.Updated.ElektronikusAlairas = true;

        // Insert:
        KRT_DokumentumokService service_krt_dok = new KRT_DokumentumokService(this.dataContext);
        ExecParam execParam_alairtDokInsert = execparam.Clone();

        Result result_alairtDokInsert = service_krt_dok.Insert(execParam_alairtDokInsert, krt_Dokumentumok_alairt);
        if (result_alairtDokInsert.IsError)
        {
            // hiba:
            Logger.Error(String.Format("KRT_Dokumentumok INSERT HIBA: errorCode: {0}; errorMessage: {1}", result_alairtDokInsert.ErrorCode, result_alairtDokInsert.ErrorMessage));
            throw new ResultException(result_alairtDokInsert);
        }

        string ujAlairtDokumentum_Id = result_alairtDokInsert.Uid;

        Logger.Info("KRT_AlairtDokumentumok INSERT OK");

        #endregion KRT_Dokumentumok INSERT (aláírt fájlra)

        #region KRT_DokumentumAlairasok INSERT

        Insert_KRT_DokumentumAlairasok(execparam, ujAlairtDokumentum_Id, alairasMod, alairasSzabalyId, alairasSzint, alairasTulajdonos
            , tanusitvanyId, kivarasiIdo);

        #endregion KRT_DokumentumAlairasok INSERT

        #region KRT_DokumentumKapcsolatok INSERT  (kapcsolótáblába regisztrálás)

        Logger.Info("KRT_DokumentumKapcsolatok INSERT Start...");

        KRT_DokumentumKapcsolatok krt_DokumentumKapcsolat = new KRT_DokumentumKapcsolatok();

        krt_DokumentumKapcsolat.Dokumentum_Id_Fo = ujAlairtDokumentum_Id;
        krt_DokumentumKapcsolat.Updated.Dokumentum_Id_Fo = true;

        krt_DokumentumKapcsolat.Dokumentum_Id_Al = dokumentumId;
        krt_DokumentumKapcsolat.Updated.Dokumentum_Id_Al = true;

        krt_DokumentumKapcsolat.Tipus = KodTarak.DOKUKAPCSOLAT_TIPUS.Elektronikus_alairas;
        krt_DokumentumKapcsolat.Updated.Tipus = true;

        KRT_DokumentumKapcsolatokService service_dokuKapcsolatok = new KRT_DokumentumKapcsolatokService(this.dataContext);

        Result result_dokuKapcsInsert = service_dokuKapcsolatok.Insert(execparam.Clone(), krt_DokumentumKapcsolat);
        if (result_dokuKapcsInsert.IsError)
        {
            // hiba:
            Logger.Error("KRT_DokumentumKapcsolatok INSERT HIBA", execparam, result_dokuKapcsInsert);
            throw new ResultException(result_dokuKapcsInsert);
        }

        Logger.Info("KRT_DokumentumKapcsolatok INSERT OK");

        #endregion KRT_DokumentumKapcsolatok INSERT  (kapcsolótáblába regisztrálás)

        #region KRT_Dokumentumok.ElektronikusAlairas mező UPDATE (aláírásSzint alapján)

        UpdateFoDokumentumElektronikusAlairas(execparam, service_krt_dok, dokumentumId, alairasSzint);

        #endregion KRT_Dokumentumok.ElektronikusAlairas mező UPDATE (aláírásSzint alapján)
    }

    private void Insert_KRT_DokumentumAlairasok(ExecParam execparam, string ujAlairtDokumentum_Id
        , string alairasMod, string alairasSzabalyId, string alairasSzint, string alairasTulajdonos,
        string tanusitvanyId, string kivarasiIdo)
    {
        #region KRT_DokumentumAlairasok INSERT

        KRT_DokumentumAlairasok krt_DokumentumAlairas = new KRT_DokumentumAlairasok();

        krt_DokumentumAlairas.Dokumentum_Id_Alairt = ujAlairtDokumentum_Id;
        krt_DokumentumAlairas.Updated.Dokumentum_Id_Alairt = true;

        krt_DokumentumAlairas.AlairasMod = alairasMod;
        krt_DokumentumAlairas.Updated.AlairasMod = true;

        krt_DokumentumAlairas.AlairasSzabaly_Id = alairasSzabalyId;
        krt_DokumentumAlairas.Updated.AlairasSzabaly_Id = true;

        // AlairasRendben beállítva 1-re; AlairasVeglegRendben-t majd a kivárási idő letelte után kell beállítani (egy script állítja majd)
        krt_DokumentumAlairas.AlairasRendben = "1";
        krt_DokumentumAlairas.Updated.AlairasRendben = true;

        // AlairasVeglegRendben: akkor lehet 1-re állítani, ha az aláírásszint 'Hivatali belső aláírás'
        if (alairasSzint == KodTarak.DOKUMENTUM_ALAIRAS_PKI.Hivatali_Belso_Alairas)
        {
            krt_DokumentumAlairas.AlairasVeglegRendben = "1";
        }
        else
        {
            krt_DokumentumAlairas.AlairasVeglegRendben = "0";
        }
        krt_DokumentumAlairas.Updated.AlairasVeglegRendben = true;

        krt_DokumentumAlairas.Csoport_Id_Alairo = Csoportok.GetFelhasznaloSajatCsoportId(execparam);
        krt_DokumentumAlairas.Updated.Csoport_Id_Alairo = true;

        // AlairasTulajdonos
        // (csak mert kötelező mező, ne haljon már el, ha nincs megadva)
        if (!string.IsNullOrEmpty(alairasTulajdonos))
        {
            krt_DokumentumAlairas.AlairasTulajdonos = alairasTulajdonos;
        }
        else
        {
            krt_DokumentumAlairas.AlairasTulajdonos = krt_DokumentumAlairas.Csoport_Id_Alairo;
        }
        krt_DokumentumAlairas.Updated.AlairasTulajdonos = true;

        // Tanusítvány Id:
        krt_DokumentumAlairas.Tanusitvany_Id = tanusitvanyId;
        krt_DokumentumAlairas.Updated.Tanusitvany_Id = true;

        // Kivárási idő vége
        // Aktuális idő + paraméterben kapott kivárási idő percben
        int kivarasiIdo_Int = 0;
        try
        {
            kivarasiIdo_Int = Int32.Parse(kivarasiIdo);
        }
        catch
        {
            kivarasiIdo_Int = 0;
        }
        krt_DokumentumAlairas.KivarasiIdoVege = DateTime.Now.AddMinutes(kivarasiIdo_Int).ToString();
        krt_DokumentumAlairas.Updated.KivarasiIdoVege = true;

        // TODO: Állapot
        //krt_DokumentumAlairas.Allapot = ??
        //krt_DokumentumAlairas.Updated.Allapot = true;

        KRT_DokumentumAlairasokService service_dokuAlairasok = new KRT_DokumentumAlairasokService(this.dataContext);

        Result result_dokuAlairasokInsert = service_dokuAlairasok.Insert(execparam.Clone(), krt_DokumentumAlairas);
        if (result_dokuAlairasokInsert.IsError)
        {
            // hiba:
            Logger.Error("Hiba a KRT_DokumentumAlairasok bejegyzés létrehozásánál", execparam, result_dokuAlairasokInsert);
            throw new ResultException(result_dokuAlairasokInsert);
        }

        #endregion KRT_DokumentumAlairasok INSERT
    }

    private void UpdateFoDokumentumElektronikusAlairas(ExecParam execparam, KRT_DokumentumokService service_krt_dok, string dokumentumId, string alairasSzint)
    {
        if (!string.IsNullOrEmpty(alairasSzint) && alairasSzint != KodTarak.DOKUMENTUM_ALAIRAS_PKI.Nincs_Vagy_Nem_Hiteles)
        {
            // KRT_Dokumentumok GET:
            ExecParam execParam_KRT_Dok_Get = execparam.Clone();
            execParam_KRT_Dok_Get.Record_Id = dokumentumId;

            Result result_krt_Dok_Get = service_krt_dok.Get(execParam_KRT_Dok_Get);
            if (result_krt_Dok_Get.IsError)
            {
                // hiba:
                throw new ResultException(result_krt_Dok_Get);
            }

            KRT_Dokumentumok foDokumentum = (KRT_Dokumentumok)result_krt_Dok_Get.Record;

            int foDokumentumAlairasSzint = 0;
            if (!string.IsNullOrEmpty(foDokumentum.ElektronikusAlairas))
            {
                try
                {
                    foDokumentumAlairasSzint = Int32.Parse(foDokumentum.ElektronikusAlairas);
                }
                catch
                {
                    foDokumentumAlairasSzint = 0;
                }
            }

            int alairasSzint_int = 0;
            if (!string.IsNullOrEmpty(alairasSzint))
            {
                try
                {
                    alairasSzint_int = Int32.Parse(alairasSzint);
                }
                catch
                {
                    alairasSzint_int = 0;
                }
            }

            // ElektronikusAlairas mezőt akkor kell update-elni, ha az alairaszint kisebb, mint a már meglévő, de nem 0
            if (alairasSzint_int < foDokumentumAlairasSzint && alairasSzint_int > 0
                || foDokumentumAlairasSzint == 0 && alairasSzint_int > 0)
            {
                #region KRT_Dokumentumok UPDATE

                foDokumentum.Updated.SetValueAll(false);
                foDokumentum.Base.Updated.SetValueAll(false);
                foDokumentum.Base.Updated.Ver = true;

                foDokumentum.ElektronikusAlairas = alairasSzint;
                foDokumentum.Updated.ElektronikusAlairas = true;

                // AlairasFelulvizsgalat mezőt is update-eljük:
                foDokumentum.AlairasFelulvizsgalat = DateTime.Now.ToString();
                foDokumentum.Updated.AlairasFelulvizsgalat = true;

                ExecParam execParam_krt_Dok_Update = execparam.Clone();
                execParam_krt_Dok_Update.Record_Id = dokumentumId;

                Result result_krt_Dok_Update = service_krt_dok.Update(execParam_krt_Dok_Update, foDokumentum);
                if (result_krt_Dok_Update.IsError)
                {
                    // hiba:
                    throw new ResultException(result_krt_Dok_Update);
                }

                #endregion KRT_Dokumentumok UPDATE
            }
        }
    }

    private string RemoveSlash(String s)
    {
        return DocumentService.Utility.RemoveSlash(s);
    }

    private AxisUploadToSps.Upload GetAxisUploadToSps()
    {
        return DocumentService.Utility.GetAxisUploadToSps();
    }

    private byte[] GetFileContent(string externalLink)
    {
        System.Net.WebRequest webRequest = System.Net.WebRequest.Create(externalLink);
        webRequest.Credentials = DocumentService.Utility.GetAxisSpsCredential();

        System.IO.Stream ioStream = webRequest.GetResponse().GetResponseStream();

        byte[] file_buffer = null;

        System.IO.StreamReader sr = new System.IO.StreamReader(ioStream, Encoding.UTF8);
        string content = sr.ReadToEnd();

        file_buffer = Encoding.UTF8.GetBytes(content);

        sr.Close();
        ioStream.Close();

        //ioStream.Read(file_buffer, 0, (Int32)ioStream.Length);
        //ioStream.Close();

        return file_buffer;
    }

    #endregion Segédfüggvények
}