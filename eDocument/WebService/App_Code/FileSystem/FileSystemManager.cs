﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileSystem
{
    /// <summary>
    /// Summary description for FileSystemManager
    /// </summary>
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;
    using Contentum.eRecord.Service;
    using Contentum.eUtility;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Xml;

    /// <summary>
    /// Summary description for UMCDocumentumManager
    /// </summary>
    public class FileSystemManager
    {
        public FileSystemManager()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public Result UploadFromeRecordWithCTT(
          ExecParam execparam
        , string documentStoreType
        , string filename
        , byte[] cont
        , KRT_Dokumentumok kapottKrtDokObj
        , XmlDocument xmlParams
        )
        {

            #region Doc

            /* **********************************************************************************
             * Lepesek, nagyvonalakban:
             * 
             * feltoltesi hely-utvonal meghatarozasa
             *    ha iktatás van, akkor iktatas ala, ha nem csatolas
             * adatbazisban es spsben letrehozas, ha nincs meg az mappa/utvonal
             *     (ha nem uj irat az valasztottIratId alapjan vissza kell szedni a dokumentum_id-jet!)
             * csekout
             *     ha mar van ott olyan nevu file SPSben
             * feltoltes
             * 	
             * regisztralas
             *    ha új irat
             *     ha még nem volt csatolva hozzá
             * 
             * 
             * verziok lekerdezese
             * sps metaadatok beallitasa
             * chekin
             *     irat munkaanyag ellenorzes
             *    foverzio, ha iktatas/vagy iktatott anyag ujra csatolasa
             *     alverzio, ha munakanyag/munkaanyag ujra csatolasa
             * egyeb adatok (external_link) modositasa a bejegyzeseben
             *     ha van elozo verzio
             * mappatartalom regisztralasa
             * 
             *************************************************************************** */

            #endregion

            Logger.Info(String.Format("DocumentService._UploadFromeRecordWithCTT indul."));

            string originalFileName = filename;
            filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

            Result _ret = new Result();

            #region A kiindulo, alap feltoltesi struktura meghatarozasa.

            Result resultTHStrukt = TarolasiHelyStrukturaNevEsTartalomAlapjan(execparam, documentStoreType, filename, cont, xmlParams.OuterXml.ToString());

            if (!String.IsNullOrEmpty(resultTHStrukt.ErrorCode))
            {
                return resultTHStrukt;
            }

            try
            {
                xmlParams.LoadXml((string)resultTHStrukt.Record);
            }
            catch (Exception exc1)
            {
                Logger.Info(String.Format("Hiba a struktura meghatarozo altal visszaadott xml string konvertalasakor.\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace));
                _ret.ErrorCode = "XMLC0001";
                _ret.ErrorMessage = String.Format("Hiba a struktura meghatarozo altal visszaadott xml string konvertalasakor.\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace);
                return _ret;
            }


            DocumentService.Utility.TarolasiHelyStruktura tarolasiHely = new DocumentService.Utility.TarolasiHelyStruktura();
            //  A site collection - a kiindulási url.
            string rootSiteCollectionUrl = GetParam("rootSiteCollectionUrl", xmlParams);
            tarolasiHely.RootSiteCollectionUrl = rootSiteCollectionUrl;
            //  Ez a dokumentumtár url neve és az év.
            string doktarSitePath = DocumentService.Utility.RemoveSlash(GetParam("doktarSitePath", xmlParams));
            tarolasiHely.DoktarSitePath = doktarSitePath;
            string doktarDocLibPath = DocumentService.Utility.RemoveSlash(GetParam("doktarDocLibPath", xmlParams));
            tarolasiHely.DoktarDocLibPath = doktarDocLibPath;
            string doktarFolderPath = GetParam("doktarFolderPath", xmlParams);
            tarolasiHely.DoktarFolderPath = doktarFolderPath;
            tarolasiHely.FileName = filename;

            //  Ezzel vizsgalhatjuk a kesobbiek folyaman, hogy tipusos dokumentumrol van-e szo. (Ha nem nyilvan empty :))
            string spsCttId = GetParam("spsCttId", xmlParams);

            //  Ez lesz az uj Krt_dok bejegyzes GUIDja.
            //  Vagy ha letezo, akkor ebbe allitjuk be az erteket.
            Guid ujKrtDokGuid = new Guid(GetParam("ujKrtDokGuid", xmlParams));
            bool letezoKrtDok = false;

            //  ha kaptunk dokumentum guid-ot, akkor az lesz az ujKrtDokGuid
            //  de ellenorizzuk, hogy letezik-e
            if (!String.IsNullOrEmpty(GetParam("docmetaDokumentumId", xmlParams)))
            {
                Logger.Info(String.Format("Van dokumentum_id az xml parameterek kozott. Ellenorzunk. ({0})", GetParam("docmetaDokumentumId", xmlParams)));

                ExecParam gede = new ExecParam();
                gede.Alkalmazas_Id = execparam.Alkalmazas_Id;
                gede.Felhasznalo_Id = execparam.Felhasznalo_Id;
                gede.FelhasznaloSzervezet_Id = execparam.FelhasznaloSzervezet_Id;
                gede.Record_Id = GetParam("docmetaDokumentumId", xmlParams);

                Result resultIsDokExists = eDocumentUtility.GetDoc(gede);

                if (!String.IsNullOrEmpty(resultIsDokExists.ErrorCode))
                {
                    if (resultIsDokExists.ErrorCode.Equals("-2146232060") && resultIsDokExists.ErrorMessage.Equals("[50101]"))
                    {
                        resultIsDokExists = new Result();
                        resultIsDokExists.ErrorCode = "NDIDA00001";
                        resultIsDokExists.ErrorMessage = "A dokumentum tartalmaz egy dokumentum id-t. Ez nem található a rendszerben!";
                    }
                    Logger.Error(String.Format("Hiba a dokumentum letenek ellenorzesekor!ErrCode: {0}\nErrMsg: {1}", resultIsDokExists.ErrorCode, resultIsDokExists.ErrorMessage));
                    return resultIsDokExists;
                }

                if (resultIsDokExists.Record == null)
                {
                    Logger.Error(String.Format("Hiba a dokumentum letenek ellenorzesekor! Null a kapott result record mezoje! Valoszinuleg nem letezik a megadott guid-u krt_dok bejegyzes."));
                    _ret.ErrorCode = "NEX0001";
                    _ret.ErrorMessage = "Upload: Hiba a dokumentum letenek ellenorzesekor! Null a kapott result record mezoje! Valoszinuleg nem letezik a megadott guid-u krt_dok bejegyzes.";
                    return _ret;
                }

                ujKrtDokGuid = new Guid(GetParam("docmetaDokumentumId", xmlParams));
                letezoKrtDok = true;
            }

            //  2. Ha docx, akkor megnezzuk van-e tartalomtipusa
            string kiterjesztes = (filename.LastIndexOf('.') == -1) ? " " : filename.Trim().Substring(filename.LastIndexOf('.') + 1);
            string sablonAzonosito = GetParam("sablonAzonosito", xmlParams);

            Logger.Info(String.Format("filename: {0} - kiterjesztes: {1}", filename, kiterjesztes));
            Logger.Info(String.Format("Kapott cuccok: doktarSitePath: {0}\ndoktarDocLibPath: {1}\ndoktarFolderPath: {2}\nsablonAzonosito: {3}", doktarSitePath, doktarDocLibPath, doktarFolderPath, sablonAzonosito));

            #endregion

            #region ha nem uj irat a kivalasztottIratId alapjan vissza kell szedni a dokumentum_id-jet

            string kivalasztottIratIdAlapjanDokumentumId = String.Empty;
            string ujKrtDokBejegyzesKell = this.GetParam("ujKrtDokBejegyzesKell", xmlParams);
            String csatolmanyId = String.Empty;
            String csatolmanyVerzio = String.Empty;

            if (GetParam("ujirat", xmlParams).Equals("NEM"))
            {
                bool vanIlyenNevuNeki = true;
                Logger.Info(String.Format("ha nem uj irat a kivalasztottIratId alapjan vissza kell szedni a dokumentum_id-jet - indul. kivalasztottIratId: {0} - kivalasztottKuldemenyId: {1}", GetParam("kivalasztottIratId", xmlParams), GetParam("kivalasztottKuldemenyId", xmlParams)));

                Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
                Contentum.eRecord.Service.EREC_CsatolmanyokService csatolmanyokService = sf.GetEREC_CsatolmanyokService();
                Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch csatolmanyokSearch = new Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch();

                if (!String.IsNullOrEmpty(GetParam("kivalasztottKuldemenyId", xmlParams)))
                {
                    csatolmanyokSearch.KuldKuldemeny_Id.Value = GetParam("kivalasztottKuldemenyId", xmlParams);
                    csatolmanyokSearch.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                }
                else
                {
                    csatolmanyokSearch.IraIrat_Id.Value = GetParam("kivalasztottIratId", xmlParams);
                    csatolmanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                }

                csatolmanyokSearch.Manual_Dokumentum_Nev.Value = filename;
                csatolmanyokSearch.Manual_Dokumentum_Nev.Operator = Contentum.eQuery.Query.Operators.equals;

                Result iratDokRes = csatolmanyokService.GetAllWithExtension(execparam, csatolmanyokSearch);

                if (!String.IsNullOrEmpty(iratDokRes.ErrorCode))
                {
                    Logger.Error(String.Format("Hibával tért vissza az irat dokumentum kapcsolótábla lekérdezés!\nErrorCode: {0}\nErrorMessage: {1}", iratDokRes.ErrorCode, iratDokRes.ErrorMessage));
                    return iratDokRes;
                }

                if (iratDokRes.Ds.Tables[0].Rows.Count == 0)
                {
                    //Logger.Error(String.Format("Nem adott vissza eredmenyt az irat dokumentum kapcsolotabla select!"));
                    //_ret = new Result();
                    //_ret.ErrorCode = "IRATK00001";
                    //_ret.ErrorMessage = String.Format("Nem adott vissza eredmenyt az irat dokumentum kapcsolotabla select!");
                    //return _ret;
                    ujKrtDokBejegyzesKell = "IGEN";
                    vanIlyenNevuNeki = false;
                    Logger.Info(String.Format("Nincs ilyen nevu neki: {0}", filename));
                }

                if (iratDokRes.Ds.Tables[0].Rows.Count > 1)
                {
                    Logger.Error(String.Format("Tul sok eredmenyt adott az irat dokumentum kapcsolotabla select!"));
                    _ret = new Result();
                    _ret.ErrorCode = "IRATK00002";
                    _ret.ErrorMessage = String.Format("Tul sok eredmenyt adott az irat dokumentum kapcsolotabla select!");
                    return _ret;
                }

                Logger.Info(String.Format("iratDokRes.Ds.Tables[0].Rows.Count: {0}", iratDokRes.Ds.Tables[0].Rows.Count));
                if (iratDokRes.Ds.Tables[0].Rows.Count == 1)
                {
                    csatolmanyId = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["Id"]);
                    csatolmanyVerzio = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["erecCsat_Ver"]);
                    Logger.Info(String.Format("kapott csatolmanyId: {0} - csatolmanyVerzio: {1}", csatolmanyId, csatolmanyVerzio));

                    #region zarolas figyelese

                    Logger.Info("Dokumentum zarolas figyelese.");

                    if (!String.IsNullOrEmpty(Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"])))
                    {
                        //  TODO: itt az lenne szep, h a zarolo id mellett a nevet is visszadnank!

                        Logger.Error(String.Format("A dokumentum zarolva van! Zarolo_Id: {0} ZarolasIdo: {1}", iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"], iratDokRes.Ds.Tables[0].Rows[0]["krtDok_ZarolasIdo"]));

                        Result iratDokRes2 = new Result();
                        iratDokRes2.ErrorCode = "ZAROLT001";
                        iratDokRes2.ErrorMessage = String.Format("A feltöltendő dokumentum zárolva van (ki: {0}, mikor: {1})!", iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"], iratDokRes.Ds.Tables[0].Rows[0]["krtDok_ZarolasIdo"]);
                        iratDokRes2.Record = String.Format("<result><zarolo_id>{0}</zarolo_id><zarolasido>{1}</zarolasido></result>", iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"], iratDokRes.Ds.Tables[0].Rows[0]["krtDok_ZarolasIdo"]);
                        return iratDokRes2;
                    }

                    #endregion
                }

                if (vanIlyenNevuNeki)
                {
                    Logger.Info(String.Format("Eredmeny ok"));

                    kivalasztottIratIdAlapjanDokumentumId = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["Dokumentum_Id"]);

                    Logger.Info(String.Format("Kapott dokumentum id: {0}", kivalasztottIratIdAlapjanDokumentumId));
                }
            }

            #endregion

            #region A felepitett struktura ellenorzese es letrehozasa a DB-ben

            string storePath = String.Empty;
            Result resTarhely = null;

            #endregion

            #region A felepitett struktura ellenorzese es letrehozasa a TARHELY-en

            Logger.Info("SharePoint: tarhely felepitese indul.");

            resTarhely = TarhelyekEllenorzeseMappakTarhely(execparam, documentStoreType, doktarSitePath, doktarDocLibPath, doktarFolderPath, spsCttId);

            if (!String.IsNullOrEmpty(resTarhely.ErrorCode))
            {
                return resTarhely;
            }

            storePath = resTarhely.Record as string;

            #endregion

            #region  Megnezzuk van-e ugyanolyan nevu file a cel helyen.

            #endregion

            #region Feltoltes elott: ha letezett es iktatas, akkor check-out kell


            #endregion

            #region Feltoltes: a file feltoltese

            string filePath = Path.Combine(storePath, filename);
            FileSystemUtility.UploadFile(filePath, cont);

            #endregion

            #region kell-e uj krtDok bejegyzes (vizsgalat, h nem uj vagy ujKrtDokBejegyzesKell nem letezik)

            if (GetParam("ujirat", xmlParams).Equals("NEM") && !eDocumentUtility.IsParamExists("ujKrtDokBejegyzesKell", xmlParams) && !"NEM".Equals(ujKrtDokBejegyzesKell))
            {
                Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
                Contentum.eRecord.Service.EREC_CsatolmanyokService csatolmanyokService = sf.GetEREC_CsatolmanyokService();

                ExecParam execp2 = execparam.Clone();

                EREC_CsatolmanyokSearch csatSearch = new EREC_CsatolmanyokSearch();

                if (!String.IsNullOrEmpty(GetParam("kivalasztottKuldemenyId", xmlParams)))
                {
                    csatSearch.KuldKuldemeny_Id.Value = GetParam("kivalasztottKuldemenyId", xmlParams);
                    csatSearch.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                }
                else
                {
                    csatSearch.IraIrat_Id.Value = GetParam("kivalasztottIratId", xmlParams);
                    csatSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                }

                csatSearch.Manual_Dokumentum_Nev.Value = filename;
                csatSearch.Manual_Dokumentum_Nev.Operator = Contentum.eQuery.Query.Operators.equals;

                Result resultCsatt = csatolmanyokService.GetAllWithExtension(execp2, csatSearch);

                if (!String.IsNullOrEmpty(resultCsatt.ErrorCode))
                {
                    Logger.Error(String.Format("Hibával tért vissza a kiválasztott irat id csatolmány neve alapján lekérdezés! ErrCode: {0}\nErrM: {1}", resultCsatt.ErrorCode, resultCsatt.ErrorMessage));
                    return resultCsatt;
                }

                if (resultCsatt.Ds.Tables[0].Rows.Count == 0)
                {
                    ujKrtDokBejegyzesKell = "IGEN";
                    Logger.Info(String.Format("Nulla a talált csatolmányok száma a megadott irat idhez ({0}), ezért lesz insert.", GetParam("kivalasztottIratId", xmlParams)));
                }

            }

            #endregion

            #region Krt_dokumentum bejegyzes megejtese.

            string externalLink = FileSystemUtility.GetExternalLink(filePath); // tarolasiHely.ToExternalLink();

            String tortentUjKrtDokBejegyzes = String.Empty;

            if (GetParam("ujirat", xmlParams).Equals("IGEN") || ujKrtDokBejegyzesKell.Equals("IGEN"))
            {

                if (kapottKrtDokObj == null)
                {
                    kapottKrtDokObj = new KRT_Dokumentumok();
                    kapottKrtDokObj.Updated.SetValueAll(false);
                    kapottKrtDokObj.Base.Updated.SetValueAll(false);
                }

                kapottKrtDokObj.Csoport_Id_Tulaj = execparam.Felhasznalo_Id;
                kapottKrtDokObj.Updated.Csoport_Id_Tulaj = true;

                kapottKrtDokObj.FajlNev = filename.Trim();
                kapottKrtDokObj.Updated.FajlNev = true;

                kapottKrtDokObj.Meret = cont.Length.ToString();
                kapottKrtDokObj.Updated.Meret = true;

                kapottKrtDokObj.Allapot = "1";
                kapottKrtDokObj.Updated.Allapot = true;

                //
                //  CR#2191 innen

                /*

                if (String.IsNullOrEmpty(kapottKrtDokObj.Tipus))
                {
                    kapottKrtDokObj.Tipus = "Egyéb";
                    kapottKrtDokObj.Updated.Tipus = true;
                }

                //(filename.LastIndexOf('.') == -1) ? "" : filename.Trim().Substring(filename.LastIndexOf('.'));
                if (String.IsNullOrEmpty(kapottKrtDokObj.Formatum))
                {
                    kapottKrtDokObj.Formatum = GetFormatum(execparam, filename); // (fileTipus == null) ? "Egyéb" : fileTipus;
                    kapottKrtDokObj.Updated.Formatum = true;
                }

                */

                if (String.IsNullOrEmpty(kapottKrtDokObj.Tipus))
                {
                    kapottKrtDokObj.Tipus = eDocumentUtility.GetFormatum(execparam, filename);
                    kapottKrtDokObj.Updated.Tipus = true;
                }

                if (String.IsNullOrEmpty(kapottKrtDokObj.Formatum))
                {
                    kapottKrtDokObj.Formatum = Path.GetExtension(filename).Trim(eDocumentUtility.kiterjesztesVegeirol);
                    kapottKrtDokObj.Updated.Formatum = true;
                }

                //
                //  CR#2191 idaig

                /// -------

                kapottKrtDokObj.Alkalmazas_Id = execparam.Alkalmazas_Id;
                kapottKrtDokObj.Updated.Alkalmazas_Id = true;

                kapottKrtDokObj.Dokumentum_Id = kapottKrtDokObj.Id;
                kapottKrtDokObj.Updated.Dokumentum_Id = true;

                //kapottKrtDokObj.VerzioJel = aktVersion;
                //kapottKrtDokObj.Updated.VerzioJel = true;

                //kapottKrtDokObj.External_Id = felotoltottFileSpsId;
                //kapottKrtDokObj.Updated.External_Id = true;

                kapottKrtDokObj.External_Link = externalLink;
                kapottKrtDokObj.Updated.External_Link = true;

                // masolaskor ebbol vesszuk ki a masolando file site, doc es folder cuccait
                // vissza is lehetne selectalni a mappakbol is, de igy egyszerubb es gyorsabb :)
                kapottKrtDokObj.External_Info = tarolasiHely.ToExternalInfo();
                kapottKrtDokObj.Updated.External_Info = true;

                kapottKrtDokObj.External_Source = documentStoreType;
                kapottKrtDokObj.Updated.External_Source = true;

                //kapottKrtDokObj.Leiras = externalTargetMappa["PATH"] + "/" + _ret.Uid.ToString() + "/" + filename;
                kapottKrtDokObj.Leiras = originalFileName;
                kapottKrtDokObj.Updated.Leiras = true;

                kapottKrtDokObj.TartalomHash = "<null>";
                kapottKrtDokObj.Updated.TartalomHash = true;

                kapottKrtDokObj.KivonatHash = "<null>";
                kapottKrtDokObj.Updated.KivonatHash = true;

                if (!String.IsNullOrEmpty(sablonAzonosito))
                {
                    kapottKrtDokObj.SablonAzonosito = sablonAzonosito;
                    kapottKrtDokObj.Updated.SablonAzonosito = true;
                }

                kapottKrtDokObj.BarCode = GetParam("vonalkod", xmlParams);
                kapottKrtDokObj.Updated.BarCode = true;

                // a fajl feltoltesekor az alairas felulvizsgalat datumot
                // automatikusan generaljuk a feltoltessel egyidejuleg
                // de csak ha van ElektronikusAlairas informacionk
                if (!String.IsNullOrEmpty(kapottKrtDokObj.ElektronikusAlairas)
                    && kapottKrtDokObj.ElektronikusAlairas != "<null>")
                {
                    kapottKrtDokObj.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
                    kapottKrtDokObj.Updated.AlairasFelulvizsgalat = true;
                }

                Logger.Info("File regisztralasa eDocba.");
                KRT_DokumentumokService dokumentumService2 = new KRT_DokumentumokService();
                _ret = dokumentumService2.Insert(execparam, kapottKrtDokObj);

                if (!String.IsNullOrEmpty(_ret.ErrorCode))
                {
                    Logger.Error("File regisztralasa eDocba hiba: " + _ret.ErrorCode + "; " + _ret.ErrorMessage);
                    return _ret;
                }

                tortentUjKrtDokBejegyzes = _ret.Uid;
            }
            else
            {
                //_ret.Uid = GetParam("docmetaDokumentumId", xmlParams);
                _ret.Uid = kivalasztottIratIdAlapjanDokumentumId;
            }

            _ret.Record = externalLink;

            #endregion

            #region A rekord mezobe visszaadando, a kovetkezo lepeshez hasznalatos parameterek

            string uploadXmlStrParams_alairasResz = "";
            string alairaskell = GetParam("alairaskell", xmlParams);
            if (alairaskell == "IGEN")
            {
                uploadXmlStrParams_alairasResz = String.Format("<alairaskell>{0}</alairaskell>"
                                                             + "<alairasSzabalyId>{1}</alairasSzabalyId>"
                                                             + "<alairasSzint>{2}</alairasSzint>"
                                                             + "<easzKod>{3}</easzKod>"
                                                             + "<tanusitvanyId>{4}</tanusitvanyId>"
                                                             + "<kivarasiIdo>{5}</kivarasiIdo>"
                                                             + "<alairasMod>{6}</alairasMod>"
                                                             + "<alairasTulajdonos>{7}</alairasTulajdonos>"
                                                             , GetParam("alairaskell", xmlParams)
                                                             , GetParam("alairasSzabalyId", xmlParams)
                                                             , GetParam("alairasSzint", xmlParams)
                                                             , GetParam("easzKod", xmlParams)
                                                             , GetParam("tanusitvanyId", xmlParams)
                                                             , GetParam("kivarasiIdo", xmlParams)
                                                             , GetParam("alairasMod", xmlParams)
                                                             , GetParam("alairasTulajdonos", xmlParams));
            }

            string visszaadandoXmlString = String.Format("<uploadparameterek>" +
                "<documentStoreType>{0}</documentStoreType>" +
                "<rootSiteCollectionUrl>{1}</rootSiteCollectionUrl>" +
                "<doktarSitePath>{2}</doktarSitePath>" +
                "<doktarDocLibPath>{3}</doktarDocLibPath>" +
                "<doktarFolderPath>{4}</doktarFolderPath>" +
                "<dokumentum_id>{5}</dokumentum_id>" +
                "<spsCttId>{6}</spsCttId>" +
                "<eDocMappaGuid>{7}</eDocMappaGuid>" +
                "<filename>{8}</filename>" +
                "<iktatokonyv>{9}</iktatokonyv>" +
                "<sourceSharePath>{10}</sourceSharePath>" +
                "<foszam>{11}</foszam>" +
                "<iktatoszam>{12}</iktatoszam>" +
                "<ujKrtDokGuid>{13}</ujKrtDokGuid>" +
                "<megjegyzes>{14}</megjegyzes>" +
                "<munkaanyag>{15}</munkaanyag>" +
                "<docmetaDokumentumId>{16}</docmetaDokumentumId>" +
                "<dokumentumFullUrl>{17}</dokumentumFullUrl>" +
                "<ujirat>{18}</ujirat>" +
                "<cttNeve>{19}</cttNeve>" +
                "<docmetaIratId>{20}</docmetaIratId>" +
                "<kivalasztottIratId>{21}</kivalasztottIratId>" +
                "<ujKrtDokBejegyzesKell>{22}</ujKrtDokBejegyzesKell>" +
                "<tartalomSHA1>{23}</tartalomSHA1>" +
                "<iratMetaDefId>{24}</iratMetaDefId>" +
                "<iratMetaDefFoDokumentum>{25}</iratMetaDefFoDokumentum>" +
                "<sablonAzonosito>{26}</sablonAzonosito>" +
                uploadXmlStrParams_alairasResz +
                "<kivalasztottKuldemenyId>{27}</kivalasztottKuldemenyId>" +
                "<tortentUjKrtDokBejegyzes>{28}</tortentUjKrtDokBejegyzes>" +
                "<csatolmanyId>{29}</csatolmanyId>" +
                "<csatolmanyVerzio>{30}</csatolmanyVerzio>" +
                "<originalFileName>{31}</originalFileName>" +
                "</uploadparameterek>"
                , documentStoreType
                , tarolasiHely.RootSiteCollectionUrl
                , tarolasiHely.DoktarSitePath
                , tarolasiHely.DoktarDocLibPath
                , doktarFolderPath
                , _ret.Uid
                , spsCttId
                , String.Empty         //eDocMappaGuid.Uid.ToString()
                , Contentum.eUtility.XmlFunction.ConvertToCData(filename)
                , GetParam("iktatokonyv", xmlParams)
                , GetParam("sourceSharePath", xmlParams)
                , GetParam("foszam", xmlParams)
                , GetParam("iktatoszam", xmlParams)
                , GetParam("docmetaDokumentumId", xmlParams)
                , GetParam("megjegyzes", xmlParams)
                , GetParam("munkaanyag", xmlParams)
                , _ret.Uid.ToString()
                , _ret.Record.ToString()
                , GetParam("ujirat", xmlParams)
                , GetParam("cttNeve", xmlParams)
                , GetParam("docmetaIratId", xmlParams)
                , GetParam("kivalasztottIratId", xmlParams)
                , ujKrtDokBejegyzesKell
                , GetParam("tartalomSHA1", xmlParams)
                , GetParam("iratMetaDefId", xmlParams)
                , GetParam("iratMetaDefFoDokumentum", xmlParams)
                , GetParam("sablonAzonosito", xmlParams)
                , GetParam("kivalasztottKuldemenyId", xmlParams)
                , tortentUjKrtDokBejegyzes
                , csatolmanyId
                , csatolmanyVerzio
                , Contentum.eUtility.XmlFunction.ConvertToCData(originalFileName)
                );

            Logger.Info(String.Format("Tovabbadott xml paramterek: {0}", visszaadandoXmlString));

            _ret.Record = visszaadandoXmlString;

            #endregion

            Logger.Info("DocumentService._UploadFromeRecordWithCTT vege.");

            return _ret;
        } // _UploadFromeRecordWithCTT



        public Result TarolasiHelyStrukturaNevEsTartalomAlapjan(
          ExecParam execparam
        , string documentStoreType
        , string filename
        , byte[] cont
        , string paramsXmlStr
     )
        {
            Result _ret = new Result();

            Logger.Info("--- DocumentService.TarolasiHelyStrukturaNevEsTartalomAlapjan indul ---");

            #region Xml parameterek xmldocca es parameterek ellenorzese

            XmlDocument xmlParams = new XmlDocument();

            if (paramsXmlStr != null)
            {
                try
                {
                    Logger.Info("Xml parameter string feldolgozasa.");
                    xmlParams.LoadXml(paramsXmlStr);
                }
                catch (Exception ex)
                {
                    Logger.Error(String.Format("Hiba az XML paraméter string átalakításakor!\nMessage: {0}\nStackTrace: {1}\nXmlParams: {2}", ex.Message, ex.StackTrace, paramsXmlStr));
                    _ret.ErrorCode = "XMLPAR0001";
                    _ret.ErrorMessage = String.Format("Hiba az XML paraméter string átalakításakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                    return _ret;
                }
            }
            else
            {
                Logger.Error(String.Format("Hiba az XML paraméter stringgel. Ures string!"));
                _ret.ErrorCode = "XMLPAR0002";
                _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Hiba az XML paraméter stringgel! Üres string!");
                return _ret;
            }

            //if (cont == null)
            //{
            //    Logger.Error(String.Format("Ures file tartalom parameter!"));
            //    _ret.ErrorCode = "XMLPAR0003";
            //    _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: null a file tartalma paraméter!");
            //    return _ret;
            //}

            if (String.IsNullOrEmpty(filename))
            {
                Logger.Error(String.Format("Ures file neve parameter!"));
                _ret.ErrorCode = "XMLPAR0004";
                _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: nincs megadva a file neve paraméter!");
                return _ret;
            }

            if (xmlParams.SelectNodes("//iktatokonyv").Count == 0)
            {
                Logger.Error(String.Format("Nincs megadva az xmlben megadott ertekek kozott az iktatokonyv!"));
                _ret.ErrorCode = "XMLPAR0005";
                _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Nincs megadva az xmlben megadott ertekek kozott az iktatokonyv!");
                return _ret;
            }

            if (xmlParams.SelectNodes("//foszam").Count == 0)
            {
                Logger.Error(String.Format("Nincs megadva az xmlben megadott ertekek kozott a foszam!"));
                _ret.ErrorCode = "XMLPAR0006";
                _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Nincs megadva az xmlben megadott ertekek kozott a foszam!");
                return _ret;
            }

            //  Ezzel vizsgalhatjuk a kesobbiek folyaman, hogy tipusos dokumentumrol van-e szo. (Ha nem nyilvan empty :))
            string spsCttId = String.Empty;
            string sablonAzonosito = String.Empty;

            //  ha a file tartalom ures, akkor lennie kell sablon azonositonak es content type id nek
            int i_sablonAzonosito = xmlParams.SelectNodes("//sablonAzonosito").Count;
            int i_spsCttId = xmlParams.SelectNodes("//spsCttId").Count;

            if ((i_sablonAzonosito != 1 || i_spsCttId != 1) && cont == null)
            {
                Logger.Error(String.Format("Parameterezesi hiba! Ha a tartalom nincs megadva meg kell adni a CTTid-t es a sablonazonositot az xmlbe!"));
                Logger.Error(String.Format("sablonAzonosito node db: {0} ; spsCttId node db: {1}", i_sablonAzonosito, i_spsCttId));
                _ret.ErrorCode = "XMLPAR0007";
                _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Parameterezesi hiba! Ha a tartalom nincs megadva meg kell adni a CTTid-t es a sablonazonositot az xmlbe!");
                return _ret;
            }
            else
            {
                spsCttId = GetParam("spsCttId", xmlParams);
                sablonAzonosito = GetParam("sablonAzonosito", xmlParams);
                Logger.Info(String.Format("XML parameterben kapott sablonAzonosito: {0} es spsCttId: {1}", sablonAzonosito, spsCttId));
            }

            #endregion

            #region A kiindulo, alap feltoltesi struktura meghatarozasa.

            //  A site collection - a kiindulási url.
            string rootSiteCollectionUrl = UI.GetAppSetting("SiteCollectionUrl");

            //  Ez a dokumentumtár url neve és az év.
            string doktarSitePath = String.Format("{0}/{1}/", DocumentService.Utility.RemoveSlash(UI.GetAppSetting("DokumentumtarSite")), DateTime.Today.ToString("yyyy"));

            string doktarDocLibPath = "Egyeb";

            string doktarFolderPath = String.Empty;

            string foszam = GetParam("foszam", xmlParams);

            string alszam = GetParam("alszam", xmlParams);

            Guid ujKrtDokGuid = Guid.NewGuid();

            //  Sitepath valtozik annak fuggvenyeben, h iktatas vagy nem.
            if (String.IsNullOrEmpty(GetParam("iktatokonyv", xmlParams)))
            {
                //doktarSitePath += "CSATOLMANYOK/";
                doktarSitePath += String.Format("CSATOLMANYOK/{0}/", DateTime.Today.ToString("yyyyMM"));

                doktarFolderPath = ujKrtDokGuid.ToString();
            }
            else
            {
                doktarSitePath += String.Format("IKTATOTTANYAGOK/{0}/", DocumentService.Utility.RemoveSlash(GetParam("iktatokonyv", xmlParams)));

                if (!String.IsNullOrEmpty(foszam))
                {
                    Logger.Info("a könyvtárba bekerül a főszám mellett az alszám is");

                    if (String.IsNullOrEmpty(alszam))
                    {
                        string kivalasztottIratId = GetParam("kivalasztottIratId", xmlParams);
                        if (!String.IsNullOrEmpty(kivalasztottIratId))
                        {
                            Logger.Info(String.Format("A kiválasztott irat {0} lekérése.", kivalasztottIratId));
                            ExecParam xpm = execparam.Clone();
                            xpm.Record_Id = kivalasztottIratId;
                            EREC_IraIratokService svc = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                            Result res = svc.Get(xpm);
                            if (!res.IsError)
                            {
                                EREC_IraIratok kivalasztottIrat = res.Record as EREC_IraIratok;
                                alszam = Contentum.eRecord.BaseUtility.Iratok.GetAlszamString(kivalasztottIrat);

                                Logger.Info(String.Format("A kiválasztott irat alszáma: {0}", alszam));
                            }
                            else
                            {
                                Logger.Error(String.Format("Hiba a kiválasztott irat lekérése {0} közben.", kivalasztottIratId), xpm, res);
                            }
                        }
                    }
                    else
                    {
                        Logger.Info("A paraméterben kapott alszám: " + alszam);
                    }

                }
                else
                {
                    Logger.Info("nincs főszám átadva");
                }

                doktarFolderPath = DocumentService.Utility.GetIktatottAnyagFolderPath(foszam, alszam);
                Logger.Info(String.Format("doktarFolderPath={0}", doktarFolderPath));

                //doktarFolderPath = foszam;


            }

            string cttNeve = String.Empty;

            #endregion

            #region Visszaadott parameterek xmlbe torteno betetele majd a resultba illesztese

            Logger.Info("vissza param xmlhez nodok legyartasa");

            XmlNode nd1 = xmlParams.CreateNode(XmlNodeType.Element, "rootSiteCollectionUrl", "");
            nd1.InnerText = rootSiteCollectionUrl;

            XmlNode nd2 = xmlParams.CreateNode(XmlNodeType.Element, "doktarSitePath", "");
            nd2.InnerText = doktarSitePath;

            XmlNode nd3 = xmlParams.CreateNode(XmlNodeType.Element, "doktarDocLibPath", "");
            nd3.InnerText = Contentum.eDocument.SharePoint.Utility.ConvertToUrl(doktarDocLibPath);

            XmlNode nd4 = xmlParams.CreateNode(XmlNodeType.Element, "doktarFolderPath", "");
            nd4.InnerText = doktarFolderPath;

            XmlNode nd5 = xmlParams.CreateNode(XmlNodeType.Element, "spsCttId", "");
            nd5.InnerText = spsCttId;

            XmlNode nd6 = xmlParams.CreateNode(XmlNodeType.Element, "sablonAzonosito", "");
            nd6.InnerText = sablonAzonosito;

            XmlNode nd7 = xmlParams.CreateNode(XmlNodeType.Element, "ujKrtDokGuid", "");
            nd7.InnerText = ujKrtDokGuid.ToString();

            XmlNode nd8 = xmlParams.CreateNode(XmlNodeType.Element, "cttNeve", "");
            nd8.InnerText = cttNeve;

            XmlNode nd9 = xmlParams.CreateNode(XmlNodeType.Element, "iratMetaDefId", "");
            nd9.InnerText = String.Empty;

            XmlNode nd10 = xmlParams.CreateNode(XmlNodeType.Element, "iratMetaDefFoDokumentum", "");
            nd10.InnerText = String.Empty;

            Logger.Info("vissza param xmlhez nodok hozzafuzese");

            xmlParams.ChildNodes[0].AppendChild(nd1);
            xmlParams.ChildNodes[0].AppendChild(nd2);
            xmlParams.ChildNodes[0].AppendChild(nd3);
            xmlParams.ChildNodes[0].AppendChild(nd4);
            xmlParams.ChildNodes[0].AppendChild(nd5);
            xmlParams.ChildNodes[0].AppendChild(nd6);
            xmlParams.ChildNodes[0].AppendChild(nd7);
            xmlParams.ChildNodes[0].AppendChild(nd8);
            xmlParams.ChildNodes[0].AppendChild(nd9);
            xmlParams.ChildNodes[0].AppendChild(nd10);

            Logger.Info("vissza param xml bele a record mezobe");

            _ret.Record = xmlParams.OuterXml.ToString();

            Logger.Info(String.Format("Visszaadott xml: {0}", xmlParams.OuterXml.ToString()));

            #endregion

            Logger.Info("--- DocumentService.TarolasiHelyStrukturaNevEsTartalomAlapjan rendben vege. ---");

            return _ret;
        }

        public Result TarhelyekEllenorzeseMappakTarhely(ExecParam ex
            , string documentStoreType
            , string documentSite
            , string documentStore
            , string documentFolder
            , string cttId)
        {
            Result ret = new Result();
            Logger.Info("TarhelyekEllenorzeseMappakTarhely start.");

            if (ex.Felhasznalo_Id == null ||
                String.IsNullOrEmpty(documentStoreType) ||
                String.IsNullOrEmpty(documentSite) ||
                String.IsNullOrEmpty(documentStore)
                )
            {
                // TODO: alap parameter hibak:
                Logger.Error("TarhelyekEllenorzeseMappakTarhely parameter hianyzik hiba.");
                return ResultError.CreateNewResultWithErrorCode(200000);
            }

            Logger.Info(String.Format("Kapott cttid: {0}", cttId));


            Logger.Info("Filesystem create path.");

            try
            {

                string path = FileSystemUtility.GetSorePath(documentStoreType, documentSite, documentStore, documentFolder);

                Directory.CreateDirectory(path);

                ret.Record = path;

                if (!String.IsNullOrEmpty(ret.ErrorCode))
                {
                    Logger.Error("Filesystem create path hiba: " + ret.ErrorMessage.ToString());
                    return ret;
                }
            }
            catch (Exception exc)
            {
                ret.ErrorCode = "100000";
                ret.ErrorMessage = "Hiba az fs directory létrehozása során! " + exc.ToString();
            }



            Logger.Info("TarhelyekEllenorzeseMappakTarhely vege.");
            return ret;
        }

        public Result IsFilesUploadableToMOSS(ExecParam ex, String xmlStrFilenames)
        {
            Logger.Info(String.Format("IsFilesUploadableToMOSS indul - xmlStrFilenames: {0}", xmlStrFilenames));

            Result result = new Result();

            try
            {

                #region bejovo parameterek ellenorzese

                Logger.Info("IsFilesUploadableToMOSS: bejovo parameterek ellenorzese");

                XmlDocument xmlParams = new XmlDocument();
                xmlParams.LoadXml(xmlStrFilenames);

                if (xmlParams.SelectNodes("//filename").Count == 0)
                {
                    Logger.Error("Nincs megadva az xml stringben egy filename node sem!");
                    result.ErrorCode = "PARAM0001";
                    result.ErrorMessage = "Nincs megadva az xml paraméter stringben egy filename node sem!";
                    return result;
                }


                #endregion

                #region bejovo parameterek attributumokkal valo kiegeszitese

                Logger.Info("IsFilesUploadableToMOSS: bejovo parameterek attributumokkal valo kiegeszitese");

                foreach (XmlNode nd in xmlParams.SelectNodes("//filename"))
                {
                    bool vanAttrUp = false;
                    bool vanAttrRe = false;

                    for (int i = 0; i < nd.Attributes.Count; i++)
                    {
                        if (nd.Attributes[i].Name.Equals("uploadable"))
                            vanAttrUp = true;
                        if (nd.Attributes[i].Name.Equals("reason"))
                            vanAttrRe = true;
                    }

                    if (!vanAttrUp)
                    {
                        XmlAttribute a = xmlParams.CreateAttribute("uploadable");
                        a.Value = "YES";
                        nd.Attributes.Append(a);
                    }
                    else
                    {
                        nd.Attributes["uploadable"].Value = "YES";
                    }

                    if (!vanAttrRe)
                    {
                        XmlAttribute a = xmlParams.CreateAttribute("reason");
                        a.Value = "";
                        nd.Attributes.Append(a);
                    }
                    else
                    {
                        nd.Attributes["reason"].Value = "";
                    }

                }

                #endregion

                result.Record = xmlParams.OuterXml.ToString();

            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("futási hiba a IsFilesUploadableToMOSS eljarasban. Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                result.ErrorCode = "FUTHIBA00001";
                result.ErrorMessage = String.Format("Hiba az eDocumentService.IsFilesUploadableToMOSS eljárás futása közben! Hibaüzenet: {0}", exc.Message);
            }

            return result;
        }

        public Result FileExists(String doktarSitePath, String doktarDocLibPath, String doktarFolderPath, String filename)
        {
            Logger.Info(String.Format("FileSystemManager.FileExists indul - doktarSitePath: {0} - doktarDocLibPath: {1} - doktarFolderPath: {2}  - filename: {3}", doktarSitePath, doktarDocLibPath, doktarFolderPath, filename));

            Result result = new Result();

            try
            {
                filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

                string path = FileSystemUtility.GetSorePath(String.Empty,doktarSitePath, doktarDocLibPath, doktarFolderPath);
                string filePath = Path.Combine(path, filename);

                bool vanUgyanolyanFileACelHelyen = File.Exists(filePath);


                result.Record = vanUgyanolyanFileACelHelyen;

            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("futási hiba a FileExists eljarasban. Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                result.ErrorCode = "FUTHIBA00001";
                result.ErrorMessage = String.Format("Hiba az FileSystemManager.FileExists eljárás futása közben! Hibaüzenet: {0}", exc.Message);
            }

            return result;
        }

        public Result DirectUploadAndDocumentInsert(ExecParam execparam
        , String documentStoreType
        , String doktarSitePath
        , String doktarDocLibPath
        , String doktarFolderPath
        , String filename
        , byte[] cont
        , String checkInMode
        , KRT_Dokumentumok krt_Dokumentumok
        )
        {
            Logger.Info("FileSystemManager.DirectUploadAndDocumentInsert ");

            string originalFileName = filename;
            filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

            Result result = new Result();

            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
            {
                Logger.Info("DirectUploadWithCheckin hivas");

                Result resultUpload = this.DirectUploadWithCheckin(execparam
                    , documentStoreType
                    , doktarSitePath
                    , doktarDocLibPath
                    , doktarFolderPath
                    , filename
                    , cont
                    , checkInMode);

                if (!String.IsNullOrEmpty(resultUpload.ErrorCode))
                {
                    Logger.Error(String.Format("Hibat adott vissza a DirectUploadWithCheckin method."));
                    return resultUpload;
                }
                else
                {
                    #region verzio lekerdezese

                    Logger.Info("verzio lekerdezese");
                    String aktVersion = "1";

                    string itemFullUrl = resultUpload.Record as string;

                    #endregion

                    #region krt_dok bejegyzes

                    Logger.Info("krt_dok bejegyzes");

                    KRT_Dokumentumok kapottKrtDokObj = krt_Dokumentumok;
                    if (kapottKrtDokObj == null)
                    {
                        kapottKrtDokObj = new KRT_Dokumentumok();

                        kapottKrtDokObj.Updated.SetValueAll(false);
                        kapottKrtDokObj.Base.Updated.SetValueAll(false);
                    }

                    kapottKrtDokObj.Csoport_Id_Tulaj = execparam.Felhasznalo_Id;
                    kapottKrtDokObj.Updated.Csoport_Id_Tulaj = true;

                    kapottKrtDokObj.FajlNev = filename.Trim();
                    if (String.IsNullOrEmpty(kapottKrtDokObj.FajlNev))
                    {
                        kapottKrtDokObj.FajlNev = "-";
                    }
                    kapottKrtDokObj.Updated.FajlNev = true;

                    kapottKrtDokObj.Meret = cont.Length.ToString();
                    kapottKrtDokObj.Updated.Meret = true;

                    kapottKrtDokObj.Allapot = checkInMode;
                    kapottKrtDokObj.Updated.Allapot = true;

                    if (String.IsNullOrEmpty(kapottKrtDokObj.Tipus) || kapottKrtDokObj.Updated.Tipus == false)
                    {
                        kapottKrtDokObj.Tipus = eDocumentUtility.GetFormatum(execparam, filename);
                        kapottKrtDokObj.Updated.Tipus = true;
                    }

                    if (String.IsNullOrEmpty(kapottKrtDokObj.Formatum) || kapottKrtDokObj.Updated.Formatum == false)
                    {
                        kapottKrtDokObj.Formatum = Path.GetExtension(filename).Trim(eDocumentUtility.kiterjesztesVegeirol);
                        kapottKrtDokObj.Updated.Formatum = true;
                    }

                    kapottKrtDokObj.Alkalmazas_Id = execparam.Alkalmazas_Id;
                    kapottKrtDokObj.Updated.Alkalmazas_Id = true;

                    kapottKrtDokObj.Dokumentum_Id = kapottKrtDokObj.Id;
                    kapottKrtDokObj.Updated.Dokumentum_Id = true;

                    kapottKrtDokObj.VerzioJel = aktVersion;
                    kapottKrtDokObj.Updated.VerzioJel = true;

                    //kapottKrtDokObj.External_Id = resultUpload.Uid;
                    //kapottKrtDokObj.Updated.External_Id = true;

                    kapottKrtDokObj.External_Link = itemFullUrl;
                    kapottKrtDokObj.Updated.External_Link = true;

                    // masolaskor ebbol vesszuk ki a masolando file site, doc es folder cuccait
                    // vissza is lehetne selectalni a mappakbol is, de igy egyszerubb es gyorsabb :)
                    //kapottKrtDokObj.External_Info = Contentum.eUtility.UI.GetAppSetting("SharePointUrl") + ";"
                    //    +  eDocumentUtility.RemoveSlash(UI.GetAppSetting("SiteCollectionUrl")) + ";"
                    //    + RemoveSlash(doktarSitePath.Trim()) + ";"
                    //    + RemoveSlash(doktarDocLibPath.Trim()) + ";"
                    //    + RemoveSlash(doktarFolderPath.Trim());
                    //kapottKrtDokObj.Updated.External_Info = true;

                    kapottKrtDokObj.External_Source = documentStoreType;
                    kapottKrtDokObj.Updated.External_Source = true;

                    kapottKrtDokObj.Leiras = originalFileName;
                    kapottKrtDokObj.Updated.Leiras = true;

                    kapottKrtDokObj.TartalomHash = Contentum.eDocument.SharePoint.Utility.CalculateSHA1(cont);
                    kapottKrtDokObj.Updated.TartalomHash = true;

                    kapottKrtDokObj.KivonatHash = Contentum.eDocument.SharePoint.Utility.CalculateSHA1(cont);
                    kapottKrtDokObj.Updated.KivonatHash = true;

                    //kapottKrtDokObj.BarCode = GetParam("vonalkod", xmlParams);
                    //kapottKrtDokObj.Updated.BarCode = true;

                    Logger.Info("File regisztralasa eDocba.");

                    if (checkInMode == "1")
                    {
                        KRT_DokumentumokService dokumentumService2 = new KRT_DokumentumokService();
                        Result _ret = dokumentumService2.Insert(execparam, kapottKrtDokObj);

                        if (!String.IsNullOrEmpty(_ret.ErrorCode))
                        {
                            Logger.Error("File regisztralasa eDocba hiba: " + _ret.ErrorCode + "; " + _ret.ErrorMessage);
                            return _ret;
                        }

                        kapottKrtDokObj.Id = _ret.Uid;

                        result = _ret;
                    }

                    result.Record = kapottKrtDokObj;

                    #endregion
                }
            }
            else
            {
                result = new Result();
                result.ErrorCode = "DSTYP00001";
                result.ErrorMessage = "Nem értékelhető a documentStoreType paraméter!";
            }
            return result;
        }

        public Result DirectUploadWithCheckin(ExecParam execparam
        , String documentStoreType
        , String doktarSitePath
        , String doktarDocLibPath
        , String doktarFolderPath
        , String filename
        , byte[] cont
        , String checkInMode
        )
        {
            Logger.Info("FileSystemManager.DirectUploadWithCheckin");

            string originalFileName = filename;
            filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

            Result result = new Result();

            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.FileSystem)
            {

                #region A felepitett struktura ellenorzese es letrehozasa a TARHELY-en

                Logger.Info("FileSystem: tarhely felepitese indul.");

                string storePath = String.Empty;

                Result resTarhely = TarhelyekEllenorzeseMappakTarhely(execparam, documentStoreType, doktarSitePath, doktarDocLibPath, doktarFolderPath, String.Empty);

                if (!String.IsNullOrEmpty(resTarhely.ErrorCode))
                {
                    Logger.Error(String.Format("Hiba a tarhely letenek ellenorzesekor! resTarhely.Error: {0} - {1}", resTarhely.ErrorCode, resTarhely.ErrorMessage));
                    return resTarhely;
                }

                storePath = resTarhely.Record as string;

                #endregion

                #region feloltes
                //  TODO:
                //NetworkCredential uploadCred = new NetworkCredential(GetSharePointUserName(), GetSharePointPassword(), GetSharePointUserDomain());
                //NetworkCredential uploadCred = new NetworkCredential(

                string filePath = Path.Combine(storePath, filename);

                if (!String.IsNullOrEmpty(filename))
                {
                    FileSystemUtility.UploadFile(filePath, cont);
                }
                string externalLink = FileSystemUtility.GetExternalLink(filePath);

                result.Record = externalLink;
                #endregion
            }
            else
            {
                result = new Result();
                result.ErrorCode = "DSTYP00001";
                result.ErrorMessage = "Nem értékelhető a documentStoreType paraméter!";
            }
            return result;
        }

        private string GetParam(String pname, XmlDocument xdoc)
        {
            return DocumentService.Utility.GetParam(pname, xdoc);
        }


    }
}