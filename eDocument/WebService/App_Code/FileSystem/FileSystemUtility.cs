﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FileSystem
{
    /// <summary>
    /// Summary description for UCMUtility
    /// </summary>
    public class FileSystemUtility
    {
        static readonly string UCMStoreBasePath = "~/DocStore";

        public static string GetUCMStoreBasePath()
        {
            return HttpContext.Current.Server.MapPath(UCMStoreBasePath);
        }

        public static string GetExternalLink(string filePath)
        {
            HttpServerUtility server = HttpContext.Current.Server;
            HttpRequest request = HttpContext.Current.Request;

            string rootPath = server.MapPath("~/");
            string relativePath = filePath.Replace(rootPath, "~\\");
            string relativeLink = relativePath.Replace("\\", "/");
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + request.ApplicationPath.TrimEnd('/');
            string externaLink = relativeLink.Replace("~", baseUrl);
            return externaLink;
        }

        public static string GetSorePath(string documentStoreType, string documentSite, string documentStore, string documentFolder)
        {
            string path = DocumentService.Utility.RemoveSlash(GetUCMStoreBasePath())
                + "/" + DocumentService.Utility.RemoveSlash(documentSite)
                + "/" + DocumentService.Utility.RemoveSlash(documentStore)
                + "/" + DocumentService.Utility.RemoveSlash(documentFolder);

            path = path.Replace('/', '\\');

            return path;
        }

        public static void UploadFile(string filePath, byte[] cont)
        {
            File.WriteAllBytes(filePath, cont);

        }
    }
}