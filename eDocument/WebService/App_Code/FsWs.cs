using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using System.IO;
using System.Collections;

/// <summary>
/// Most ez reprenzentalja a filesystem webservice dolgot.
/// Tipikusan ezeket a methodokat kell majd betenni webservice-nek.
/// </summary>
public class FsWs
{

    public const string Header = "$Header: FsWs.cs, 8, 2010.02.17 13:52:29, Varsanyi P?ter$";
    public const string Version = "$Revision: 8$";

    const string INFOFILENAME = "dok.info";

	public FsWs()
	{
        Logger.Info(String.Format("DocumentService.FsWs start.- {0} {1}", Header, Version));
    }

    /// <summary>
    /// Ez tulajdonkeppen egy folder delete. A guid vegu file-t tartalmazo folder torlese.
    /// </summary>
    /// <param name="path">Folder utvonal.</param>
    /// <returns>Result - ha torolte, akkor uresen jon  vissza, ha hiba van, akkor ErrCode.</returns>
    public Result Delete(string path)
    {
        Result _ret = new Result();
        try
        {
            Directory.Delete(GetBaseLocation()+path, true);
        }
        catch (Exception ex)
        {
            _ret.ErrorCode = "100000";
            _ret.ErrorMessage = "Hiba a t�rl�s sor�n! "+ex.ToString();
        }

        return _ret;
    }

    /// <summary>
    /// Adott utvonalon levo file torlese.
    /// </summary>
    /// <param name="path">Folder utvonal.</param>
    /// <param name="filename">File neve.</param>
    /// <returns>Result - ha torolte, akkor uresen jon  vissza, ha hiba van, akkor ErrCode.</returns>
    public Result Delete(string path, string filename)
    {
        Result _ret = new Result();
        try
        {
            File.Delete(GetBaseLocation()+CheckPath(path) + filename);
        }
        catch (Exception ex)
        {
            _ret.ErrorCode = "100000";
            _ret.ErrorMessage = "Hiba a t�rl�s sor�n! "+ex.ToString();
        }

        return _ret;
    }

    /// <summary>
    /// Konyvtar letrehozasa. 
    /// </summary>
    /// <param name="path">Utvonal.</param>
    /// <returns>Result - ures, ha ok, egyebkent hibakod.</returns>
    public Result CreateFolder(string path) 
    {
        Result _ret = new Result();
        try
        {
            //if (Directory.Exists(CheckPath(path)+name.Trim())) return _ret;
            Directory.CreateDirectory(GetBaseLocation()+CheckPath(path.Trim()));
        }
        catch (Exception ex)
        {
            _ret.ErrorCode = "100000";
            _ret.ErrorMessage = "Hiba az fs directory l�trehoz�sa sor�n! " + ex.ToString();
        }

        return _ret;
    }

    /// <summary>
    /// Mindenkeppen munkafileba menti (vagyis verziojel nelkul teszi le a file-t),
    /// s ha nincs csekautolva, akkor beverziozza.
    /// Ha chekoutolva van, akkor a nem csinal semmit a munkafileba mentes utan. Majd
    /// a csekin beverziozza.
    /// </summary>
    /// <param name="ppath">Utvonal.</param>
    /// <param name="pfilename">Filenev.</param>
    /// <param name="cont">File tartalma.</param>
    /// <returns>Result - ures, ha ok; ErrorCode ha hiba volt.</returns>
    public Result Upload(string ppath, string pfilename, byte[] cont)
    {
        Result _ret = new Result();
        FileStream fout;
        string filename = GetBaseLocation() + CheckPath(ppath) + pfilename.Trim();

        Result fileInfoResult = GetInfo(ppath);

        // ide kell majd meg ellenorzes

        Hashtable fileInfo = (Hashtable)fileInfoResult.Record;

        try
        {
            fout = new FileStream(filename, FileMode.Create);
        }
        catch (IOException exc)
        {
            _ret.ErrorCode = "100002";
            _ret.ErrorMessage = "Upload hib�s file nyit�s! " + exc.ToString();
            return _ret;
        }

        try
        {
            foreach (byte c in cont)
                fout.WriteByte((byte)c);
        }
        catch (IOException exc)
        {
            _ret.ErrorCode = "100003";
            _ret.ErrorMessage = "Upload file felt�lt�s! " + exc.ToString();
            return _ret;
        }

        fout.Close();

        //if (fileInfo.ContainsKey("CHECKOUT"))
        //    if ("false".Equals(fileInfo["CHECKOUT"].ToString().Trim().ToLower())) 
        //    {
            _ret = CheckIn(ppath, pfilename);
        //    }

        return _ret;
    }

    /// <summary>
    /// A megadott helyen levo munkafilebol uj verziot csinal.
    /// </summary>
    /// <param name="ppath">Utvonal.</param>
    /// <param name="pfilename">File neve.</param>
    /// <returns>Result - ures vagy Erroros.</returns>
    public Result CheckIn(string ppath, string pfilename)
    {
        Result _ret = new Result();

        string filename = GetBaseLocation() + CheckPath(ppath) + pfilename.Trim();
        Result fileInfoResult = GetInfo(ppath);

        // ide kell majd meg ellenorzes

        Hashtable fileInfo = (Hashtable)fileInfoResult.Record;

        int maxverzio = (fileInfo.ContainsKey(Constants.VERZIO)) ? Convert.ToInt32(fileInfo[Constants.VERZIO]) : 0;
        maxverzio++;

        string ujfilename = GetBaseLocation() + CheckPath(ppath) + "_" + maxverzio + "_" + pfilename.Trim();

        try
        {
            File.Move(filename, ujfilename);
        }
        catch (Exception ex)
        {
            _ret.ErrorCode = "1000066";
            _ret.ErrorMessage = "CheckIn: Hiba csekinkor! " + ex.ToString();
            return _ret;
        }

        fileInfo[Constants.VERZIO] = Convert.ToString(maxverzio);
        fileInfo[Constants.CHECKOUT] = "false";
        _ret = SetInfo(ppath, fileInfo);

        return _ret;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ppath"></param>
    /// <param name="pfilename"></param>
    /// <returns></returns>
    public Result CheckOut(string ppath, string pfilename)
    {
        Result _ret = new Result();

        string filename = GetBaseLocation() + CheckPath(ppath) + pfilename.Trim();
        Result fileInfoResult = GetInfo(ppath);

        // ide kell majd meg ellenorzes

        Hashtable fileInfo = (Hashtable)fileInfoResult.Record;

        string csekkelendoFilenev = GetBaseLocation() + CheckPath(ppath) + "_" + fileInfo[Constants.VERZIO].ToString() + "_" + pfilename.Trim();

        // filetartalom a _ret-be?!

        fileInfo[Constants.CHECKOUT] = "true";
        _ret = SetInfo(ppath, fileInfo);

        return _ret;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ppath"></param>
    /// <param name="pfilename"></param>
    /// <returns></returns>
    public Result UndoCheckOut(string ppath, string pfilename)
    {
        Result _ret = new Result();

        string filename = GetBaseLocation() + CheckPath(ppath) + pfilename.Trim();
        Result fileInfoResult = GetInfo(ppath);
        
        // ide kell majd meg ellenorzes

        Hashtable fileInfo = (Hashtable)fileInfoResult.Record;

        fileInfo[Constants.CHECKOUT] = "false";
        _ret = SetInfo(ppath, fileInfo);

        return _ret;
    }

    /// <summary>
    /// Ki van-e csekkelve.
    /// </summary>
    /// <param name="ppath">Utvonal.</param>
    /// <param name="pfilename">File neve.</param>
    /// <returns>bool - true-e vagy false-e? he?</returns>
    public bool IsCheckedOut(string ppath, string pfilename)
    {
        Result fileInfoResult = GetInfo(ppath);

        // ide kell majd meg ellenorzes

        Hashtable fileInfo = (Hashtable)fileInfoResult.Record;
        return "false".Equals(fileInfo[Constants.CHECKOUT].ToString().ToLower()) ? false : true;
    }

    /// <summary>
    /// Info file visszaadasa.
    /// 
    /// kulcsok:
    /// VERZIO - legnagyobb letezo verzio szama; ha nincs 1
    /// CHECKOUT - (true/false) - csekoutpolva vagyon-e; ha nincs false
    /// </summary>
    /// <param name="path">Mely utvonalon levore vagyunk kivancsiak.</param>
    /// <returns>Result - vagy a kulcsokkal vagy ErrorCode-dal ter vissza</returns>
    private Result GetInfo(string path)
    {
        Result _ret = new Result();
        Hashtable htabla = new Hashtable();

        try
        {
            string filename = GetBaseLocation() + CheckPath(path) + INFOFILENAME;

            if (!File.Exists(filename)) 
            {
                _ret.ErrorCode = "100055";
                _ret.ErrorMessage = "Nem l�tezik az info file!";
                return _ret;
            }
            using (StreamReader sr = File.OpenText(filename))
            {
                string input;
                while ((input=sr.ReadLine())!=null) 
                {
                    if (!String.IsNullOrEmpty(input))
                        htabla.Add(GetInfoKey(input), GetInfoValue(input));
                }
                sr.Close();
            }    
            _ret.Record = htabla;
        }
        catch (Exception ex)
        {
            _ret.ErrorCode = "100001";
            _ret.ErrorMessage =  "GetInfo hiba! " + ex.ToString();
        }

        return _ret;
    }

    /// <summary>
    /// Info file kiirasa filerendserbe.
    /// A megadott utvonalon levo helyre teszi. Ha mar van felulirja.
    /// Csak azokat a kulcsokat modositja meg, amik a param Hastable-be bemennek. Ami nincs a paramban,
    /// de a filerendszerben levoben benne van, azt nem bantja.
    /// </summary>
    /// <param name="path">Utvonal.</param>
    /// <param name="info">Kiirando kulcs-ertek parok Hastableben.</param>
    /// <returns>Result - ures, ha minden ok; ha nem hibakoddal.</returns>
    private Result SetInfo(string path, Hashtable paramInfo)
    {
        Result _ret = new Result();

        Result aktInfoResult = GetInfo(path);

        // ide kell majd meg ellenorzes

        Hashtable aktInfo = (Hashtable)aktInfoResult.Record;
        
        //string[] aktInfoKeys = paramInfo.Keys;
        //string[] paramInfoKeys = paramInfo.Keys;

        foreach (string keyname in aktInfo.Keys)
        {
            if (!aktInfo.ContainsKey(keyname))
            {
                paramInfo.Add(keyname, aktInfo[keyname]);
            }
        }

        string filename = GetBaseLocation() + CheckPath(path) + INFOFILENAME;

        try
        {
            /// felulirjuk!
            /// 
            TextWriter tw = new StreamWriter(filename, false);

            foreach (string keyname in paramInfo.Keys)
            {
                string output = keyname + "=" + (string)paramInfo[keyname];
                tw.WriteLine(output);
            }
            
            tw.Close();
        } 
        catch (Exception ex) 
        {
            _ret.ErrorCode = "100088";
            _ret.ErrorMessage = "SetInfo: Info file write exception! " + ex.ToString();
        }

        return _ret;
    }

    /// <summary>
    /// A megadott utvonalat ellenorzi, h van-e a vegen backslash. Ha nincs rak oda egyet.
    /// </summary>
    /// <param name="path">Utvonal.</param>
    /// <returns>String - utvonal backslash-el a vegen.</returns>
    private string CheckPath(string path)
    {
        string _ret = path.Trim();
        if (!(_ret.Substring((int)_ret.Length-1).Equals("\\"))) _ret = _ret + "\\";
        return _ret;
    }

    /// <summary>
    /// Beadott info sorbol visszaadja a kulcsot.
    /// </summary>
    /// <param name="sor">Info file egy sora.</param>
    /// <returns>String - a kulcs neve.</returns>
    /// <see>SetInfo; GetInfo</see>
    private string GetInfoKey(string sor)
    {
        string[] parts = sor.Trim().Split('=');
        return (parts.Length >= 1) ? parts[0] : "";
    }

    /// <summary>
    /// Beadott info sorbol visszaadja az erteket.
    /// </summary>
    /// <param name="sor">Info file egy sora.</param>
    /// <returns>String - ertek.</returns>
    /// <see>SetInfo; GetInfo</see>
    private string GetInfoValue(string sor)
    {
        string[] parts = sor.Trim().Split('=');
        return (parts.Length >= 2) ? parts[1] : "";
    }

    /// <summary>
    /// Alap fs doktar direktori-t visszaadja a webkonfigbol. Egyenlore.
    /// </summary>
    /// <returns>string</returns>
    private string GetBaseLocation()
    {
        String ret = GetAppSetting(Constants.FSBASELOCATION);
        if (!ret.EndsWith("\\")) { ret += "\\"; }
        return ret;                
    }

    public string GetAppSetting(string Name)
    {
        string _ret;
        _ret = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(Name);
        if (_ret == null)
            _ret = "";
        return _ret;
    }
}