﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eRecord.Service;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Data;

[WebService(Namespace = "Contentum.eDocument.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_DokumentumKapcsolatokService : System.Web.Services.WebService
{

    #region Generáltból átemelt

    /// <summary>
    /// Insert(ExecParam ExecParam, KRT_DokumentumKapcsolatok Record)
    /// Egy rekord felvétele a KRT_DokumentumKapcsolatok táblába. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_DokumentumKapcsolatok))]
    public Result Insert(ExecParam ExecParam, KRT_DokumentumKapcsolatok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Összetett dokumentum kapcsolat felvitele előtt ellenőrizni kell, hogy ugyanabban az iratban/küldeményben vannak-e
            if (Record.DokumentumKapcsolatJelleg == KodTarak.DOKUKAPCSOLAT_TIPUS.Dokumentum_hierarchia
                && !String.IsNullOrEmpty(Record.Dokumentum_Id_Fo) && !String.IsNullOrEmpty(Record.Dokumentum_Id_Al))
            {
                #region Fő dokumentum iratának/küldeményének lekérdezése

                // (eRecord-os webservice hívás)
                EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

                EREC_CsatolmanyokSearch search_csatolmanyok_fo = new EREC_CsatolmanyokSearch();

                search_csatolmanyok_fo.Dokumentum_Id.Value = Record.Dokumentum_Id_Fo;
                search_csatolmanyok_fo.Dokumentum_Id.Operator = Query.Operators.equals;

                Result result_csat_fo = service_csatolmanyok.GetAll(ExecParam.Clone(), search_csatolmanyok_fo);
                if (result_csat_fo.IsError)
                {
                    // hiba:
                    throw new ResultException(result_csat_fo);
                }

                #endregion

                #region Aldokumentum iratának/küldeményének lekérdezése

                EREC_CsatolmanyokSearch search_csat_al = new EREC_CsatolmanyokSearch();

                search_csat_al.Dokumentum_Id.Value = Record.Dokumentum_Id_Al;
                search_csat_al.Dokumentum_Id.Operator = Query.Operators.equals;

                Result result_csat_al = service_csatolmanyok.GetAll(ExecParam.Clone(), search_csat_al);
                if (result_csat_al.IsError)
                {
                    // hiba:
                    throw new ResultException(result_csat_al);
                }

                #endregion

                // Van-e közös iratuk/küldeményük:

                List<string> iratok = new List<string>();
                List<string> kuldemenyek = new List<string>();

                foreach (DataRow row in result_csat_fo.Ds.Tables[0].Rows)
                {
                    string iratId = row["IraIrat_Id"].ToString();
                    string kuldemenyId = row["KuldKuldemeny_Id"].ToString();

                    if (!String.IsNullOrEmpty(iratId))
                    {
                        iratok.Add(iratId);
                    }
                    if (!String.IsNullOrEmpty(kuldemenyId))
                    {
                        kuldemenyek.Add(kuldemenyId);
                    }
                }

                bool vanEgyezes = false;

                foreach (DataRow row in result_csat_al.Ds.Tables[0].Rows)
                {
                    string iratId = row["IraIrat_Id"].ToString();
                    string kuldemenyId = row["KuldKuldemeny_Id"].ToString();

                    if ((!String.IsNullOrEmpty(iratId) && iratok.Contains(iratId))
                        || (!String.IsNullOrEmpty(kuldemenyId) && kuldemenyek.Contains(kuldemenyId)))
                    {
                        vanEgyezes = true;
                        break;
                    }
                }

                // ha nincs közös irat/küldemény, hiba:
                if (vanEgyezes == false)
                {
                    // hiba:
                    throw new ResultException(52791);
                }

            }

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //#region Eseménynaplózás
            //if (isTransactionBeginHere)
            //{
            //   KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //   KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_DokumentumKapcsolatok", "New").Record;

            //   Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            //}
            //#endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion


}
