using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Services;

public partial class KRT_DokumentumokService
{
    /// <summary>
    /// Optikai sz�vegfelismer�s t�mogat�sa 
    /// </summary>
    /// <param name="felhasznaloId"></param>
    /// <param name="felhasznaloSzervezetId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result ScheduledOCR(string felhasznaloId, string felhasznaloSzervezetId)
    {
        if (string.IsNullOrEmpty(felhasznaloId) || string.IsNullOrEmpty(felhasznaloSzervezetId))
        {
            Logger.Debug("KRT_DokumentumokService.ScheduledOCR Hi�nyz� param�terek !");
            return null;
        }
        ExecParam execParamContentum = new ExecParam();
        execParamContentum.Felhasznalo_Id = felhasznaloId;
        execParamContentum.FelhasznaloSzervezet_Id = felhasznaloSzervezetId;

        return ScheduledOCR_WEParam(execParamContentum);
    }
    /// <summary>
    /// Optikai sz�vegfelismer�s t�mogat�sa 
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result ScheduledOCR_WEParam(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            Logger.Info("OCR UPLOAD START");
            MainOCRUploadProcedure(execParam);
            Logger.Info("OCR UPLOAD STOP");
        }
        catch (Exception e)
        {
            Logger.Error("KRT_DokumentumokService.OCR_DOWNLOAD.ERROR: " + e.Message + (e.InnerException != null ? e.InnerException.Message : string.Empty));
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            Logger.Info("OCR DOWNLOAD START");
            MainOCRDownloadProcedure(execParam);
            Logger.Info("OCR DOWNLOAD STOP");
        }
        catch (Exception e)
        {
            Logger.Error("KRT_DokumentumokService.OCR_DOWNLOAD.ERROR: " + e.Message + (e.InnerException != null ? e.InnerException.Message : string.Empty));
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            Logger.Info("OCR ERROR CHECK START");
            MainOCRErrorCheckProcedure(execParam);
            Logger.Info("OCR ERROR CHECK STOP");
        }
        catch (Exception e)
        {
            Logger.Error("KRT_DokumentumokService.OCR_CHECK.ERROR: " + e.Message + (e.InnerException != null ? e.InnerException.Message : string.Empty));
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #region MAIN
    private void MainOCRUploadProcedure(ExecParam execParam)
    {
        //string SCAN_OCR_EXPORT_PATH = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_EXPORT_PATH);
        string SCAN_OCR_IMPORT_PATH = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_IMPORT_PATH);
        string SCAN_OCR_IMPORT_DELAY = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_IMPORT_DELAY);
        //string SCAN_OCR_IMPORT_TIMEOUT = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_IMPORT_TIMEOUT);

        KRT_DokumentumokService service = new KRT_DokumentumokService(this.dataContext);
        KRT_DokumentumokSearch src = new KRT_DokumentumokSearch();
        src.OCRAllapot.Value = KodTarak.OCR_Allapot.OCR_Ezes_Alatt_Levo_Dokumentum;
        src.OCRAllapot.Operator = Query.Operators.equals;

        Result resultFind = service.GetAll(execParam, src);
        if (!resultFind.IsError && resultFind.Ds.Tables[0].Rows.Count > 0)
        {
            string ocrFullFilePath;
            string ocrFileName;
            foreach (DataRow row in resultFind.Ds.Tables[0].Rows)
            {
                ocrFullFilePath = null;
                ocrFileName = null;
                try
                {
                    ocrFileName = GetDokumentumOCRImportFileName(row);
                    if (CheckOCRFileExistence(SCAN_OCR_IMPORT_PATH, ocrFileName, out ocrFullFilePath))
                    {
                        if (CheckOCRFileCreationDate(ocrFullFilePath, SCAN_OCR_IMPORT_DELAY))
                        {
                            UploadOCRFileToDokumentumok(execParam.Clone(), ocrFullFilePath, row["Id"].ToString());
                            Logger.Debug("KRT_DokumentumokService.ScheduledOCR: OCR File uploaded: " + ocrFullFilePath);
                        }
                        else
                        {
                            Logger.Debug("KRT_DokumentumokService.ScheduledOCR: OCR File keszites datum hiba: " + ocrFullFilePath);
                        }
                    }
                    else
                    {
                        Logger.Debug("KRT_DokumentumokService.ScheduledOCR: OCR File nem talalhato: " + ocrFullFilePath);
                    }
                }
                catch (Exception exc)
                {
                    Logger.Error("ScheduledOCR.KRT_DokumentumokService.MainOCRUploadProcedure.ERROR: " + exc.Message + (exc.InnerException != null ? exc.InnerException.Message : string.Empty));
                    Logger.Error("ScheduledOCR.KRT_DokumentumokService.MainOCRUploadProcedure.ERROR", exc);
                }

            }
        }
        else
        {
            Logger.Info("ScheduledOCR.KRT_DokumentumokService.MainOCRUploadProcedure: Nincs feldogozand� file");
        }
    }
    private void MainOCRDownloadProcedure(ExecParam execParam)
    {
        string SCAN_OCR_EXPORT_PATH = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_EXPORT_PATH);
        //string SCAN_OCR_IMPORT_PATH = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_IMPORT_PATH);
        //string SCAN_OCR_IMPORT_DELAY = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_IMPORT_DELAY);
        //string SCAN_OCR_IMPORT_TIMEOUT = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_IMPORT_TIMEOUT);

        KRT_DokumentumokService service = new KRT_DokumentumokService(this.dataContext);
        KRT_DokumentumokSearch src = new KRT_DokumentumokSearch();
        src.OCRAllapot.Value = KodTarak.OCR_Allapot.OCR_Ezendo_Dokumentum;
        src.OCRAllapot.Operator = Query.Operators.equals;

        Result resultFind = service.GetAll(execParam, src);
        if (!resultFind.IsError && resultFind.Ds.Tables[0].Rows.Count > 0)
        {
            string ocrFullFilePath = null;
            string ocrFileName = null;

            if (!Directory.Exists(SCAN_OCR_EXPORT_PATH))
                Directory.CreateDirectory(SCAN_OCR_EXPORT_PATH);

            foreach (DataRow row in resultFind.Ds.Tables[0].Rows)
            {
                ocrFileName = GetDokumentumOCRExportFileName(row);
                ocrFullFilePath = null;

                if (!CheckOCRFileExistence(SCAN_OCR_EXPORT_PATH, ocrFileName, out ocrFullFilePath))
                {
                    try
                    {
                        byte[] content = GetDocumentContent(execParam, row["Id"].ToString());
                        SaveDocumentToFile(content, ocrFullFilePath);
                        UpdateSourceDokumentumOCRAllapot(execParam.Clone(), row["Id"].ToString(), KodTarak.OCR_Allapot.OCR_Ezes_Alatt_Levo_Dokumentum);
                    }
                    catch (Exception exc)
                    {
                        Logger.Error("KRT_DokumentumokService.MainOCRDownloadProcedure.Error: " + exc.Message + exc.InnerException != null ? exc.InnerException.Message : string.Empty);
                        Logger.Error("KRT_DokumentumokService.MainOCRDownloadProcedure.Error: ", exc);
                    }
                }
                else
                {
                    Logger.Debug("KRT_DokumentumokService.MainOCRDownloadProcedure.Error: OCR File nem talalhato: " + ocrFullFilePath);
                }
            }
        }
        else
        {
            Logger.Info("ScheduledOCR.KRT_DokumentumokService.MainOCRDownloadProcedure: Nincs feldogozand� file");
        }
    }
    private void MainOCRErrorCheckProcedure(ExecParam execParam)
    {
        string SCAN_OCR_EXPORT_PATH = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_EXPORT_PATH);
        //string SCAN_OCR_IMPORT_PATH = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_IMPORT_PATH);
        //string SCAN_OCR_IMPORT_DELAY = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_IMPORT_DELAY);
        string SCAN_OCR_IMPORT_TIMEOUT = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.SCAN_OCR_IMPORT_TIMEOUT);

        int timeOutValue;
        if (!int.TryParse(SCAN_OCR_IMPORT_TIMEOUT, out timeOutValue))
        {
            Logger.Error("Rendszerparameterek.SCAN_OCR_IMPORT_TIMEOUT hiba");
            return;
        }
        KRT_DokumentumokService service = new KRT_DokumentumokService(this.dataContext);
        KRT_DokumentumokSearch src = new KRT_DokumentumokSearch();
        src.OCRAllapot.Value = KodTarak.OCR_Allapot.OCR_Ezes_Alatt_Levo_Dokumentum;
        src.OCRAllapot.Operator = Query.Operators.equals;

        DateTime nowTimeOutBefore = DateTime.Now.AddMinutes(-timeOutValue);

        src.WhereByManual = " AND KRT_Dokumentumok.ModositasIdo < '" + nowTimeOutBefore.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' ";
        src.OCRAllapot.Operator = Query.Operators.equals;

        Result resultFind = service.GetAll(execParam, src);
        if (!resultFind.IsError && resultFind.Ds.Tables[0].Rows.Count > 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Environment.NewLine);
            foreach (DataRow row in resultFind.Ds.Tables[0].Rows)
            {
                try
                {
                    UpdateSourceDokumentumOCRAllapot(execParam.Clone(), row["Id"].ToString(), KodTarak.OCR_Allapot.Nem_OCR_Ezendo_Dokumentum);

                    sb.Append(row["FajlNev"].ToString());
                    sb.Append(Environment.NewLine);
                }
                catch (Exception exc)
                {
                    Logger.Error("KRT_DokumentumokService.MainOCRErrorCheckProcedure.Error: " + exc.Message + exc.InnerException != null ? exc.InnerException.Message : string.Empty);
                    Logger.Error("KRT_DokumentumokService.MainOCRErrorCheckProcedure.Error: ", exc);
                }
            }

            //string errorMessage = string.Format(Resources.Error.ErrorDokumentumOCRFeldolgozasTimeOutHiba, timeOutValue, sb.ToString());
            string errorMessage = string.Format(ErrorDokumentumOCRFeldolgozasTimeOutHiba, timeOutValue, sb.ToString());
            SendMail_OCRTimeOutError(execParam, errorMessage);
        }
        else
        {
            Logger.Info("ScheduledOCR.KRT_DokumentumokService.MainOCRErrorCheckProcedure: Nincs feldogozand� file");
        }
    }
    #endregion MAIN

    #region HELPER

    #region CONST
    private const string CONST_OCR_IMPORT_FILE_EXTENSION = "pdf";
    private const string ErrorDokumentumOCRFeldolgozasTimeOutHiba = "Az al�bbi t�telek t�bb mint {0} perc alatt nem ker�ltek OCR-ez�sre, a f�jlok �jra OCR-ezhet�k: {1}";
    #endregion CONST

    #region UPLOAD
    /// <summary>
    /// GetDokumentumOCRFileName
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private string GetDokumentumOCRImportFileName(DataRow row)
    {
        if (row == null || string.IsNullOrEmpty(row["FajlNev"].ToString()))
            return null;

        string onlyFileName = System.IO.Path.GetFileNameWithoutExtension(row["FajlNev"].ToString());
        return string.Format("{0}{1}.{2}", row["Id"].ToString(), onlyFileName, CONST_OCR_IMPORT_FILE_EXTENSION);
    }

    /// <summary>
    /// GetDokumentumOCRExportFileName
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private string GetDokumentumOCRExportFileName(DataRow row)
    {
        if (row == null || string.IsNullOrEmpty(row["FajlNev"].ToString()))
            return null;

        return string.Format("{0}{1}", row["Id"].ToString(), row["FajlNev"].ToString());
    }

    /// <summary>
    /// CheckOCRFileExistence
    /// </summary>
    /// <param name="folder"></param>
    /// <param name="fileName"></param>
    /// <param name="fullFilePath"></param>
    /// <returns></returns>
    private bool CheckOCRFileExistence(string folder, string fileName, out string fullFilePath)
    {
        fullFilePath = string.Empty;

        if (string.IsNullOrEmpty(folder) || string.IsNullOrEmpty(fileName))
            return false;

        if (!System.IO.Directory.Exists(folder))
            return false;

        fullFilePath = System.IO.Path.Combine(folder, fileName);
        if (!System.IO.File.Exists(fullFilePath))
            return false;

        return true;
    }

    /// <summary>
    /// CheckOCRFileCreationDate
    /// </summary>
    /// <param name="fullFilePath"></param>
    /// <param name="delay"></param>
    /// <returns></returns>
    private bool CheckOCRFileCreationDate(string fullFilePath, string delay)
    {
        int delayInteger;
        if (!int.TryParse(delay, out delayInteger))
            return false;

        DateTime dateDelay = DateTime.Now.AddSeconds(-delayInteger);
        System.IO.FileInfo fileInfo = new System.IO.FileInfo(fullFilePath);

        if (fileInfo.CreationTime < dateDelay)
            return true;
        return false;
    }

    /// <summary>
    /// UploadOCRFileToDokumentumok
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="filePath"></param>
    /// <param name="sourceDokumentumId"></param>
    private void UploadOCRFileToDokumentumok(ExecParam execParam, string filePath, string sourceDokumentumId)
    {
        Logger.Info("UploadOCRFileToDokumentumok.Start");
        Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
        Contentum.eRecord.Service.EREC_CsatolmanyokService service = sf.GetEREC_CsatolmanyokService();
        Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch src = new Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch();

        src.Dokumentum_Id.Value = sourceDokumentumId;
        src.Dokumentum_Id.Operator = Query.Operators.equals;

        Result resultFind = service.GetAll(execParam, src);
        if (!resultFind.IsError && resultFind.Ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow row in resultFind.Ds.Tables[0].Rows)
            {
                AddNewOCRDocument(execParam.Clone(), filePath, sourceDokumentumId, row);
            }
        }
        else
        {
            Logger.Error("UploadOCRFileToDokumentumok", execParam, resultFind);
        }

        UpdateSourceDokumentumOCRAllapot(execParam.Clone(), sourceDokumentumId, KodTarak.OCR_Allapot.OCR_Ezett_Dokumentum);
        Logger.Info("UploadOCRFileToDokumentumok.Stop");
    }

    /// <summary>
    /// UpdateSourceDokumentumOCRAllapot
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="sourceDokumentumId"></param>
    private void UpdateSourceDokumentumOCRAllapot(ExecParam execParam, string sourceDokumentumId, string ocrAllapot)
    {
        Logger.Info("UpdateSourceDokumentumOCRAllapot.Start");
        KRT_DokumentumokService service = new KRT_DokumentumokService(this.dataContext);
        execParam.Record_Id = sourceDokumentumId;

        Result resultFind = service.Get(execParam);
        if (resultFind.IsError)
        {
            throw new ResultException(resultFind);
        }

        KRT_Dokumentumok dok = (KRT_Dokumentumok)resultFind.Record;
        dok.OCRAllapot = ocrAllapot;
        dok.Updated.OCRAllapot = true;

        Result resultUpdate = service.Update(execParam, dok);
        if (resultUpdate.IsError)
        {
            throw new ResultException(resultUpdate);
        }
        Logger.Info("UpdateSourceDokumentumOCRAllapot.Stop");
    }

    /// <summary>
    /// AddNewOCRDocument
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="filePath"></param>
    /// <param name="originalDocument"></param>
    private void AddNewOCRDocument(ExecParam execParam, string filePath, string originalDocumentId, DataRow csatolmanyRow)
    {
        Logger.Info("AddNewOCRDocument.Start");
        System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);
        byte[] fileContent = LoadFileFromPath(filePath);
        if (fileContent == null)
            throw new MissingFieldException();

        if (!string.IsNullOrEmpty(csatolmanyRow["IraIrat_Id"].ToString()))
        {
            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_IraIratokService serviceIrat = sf.GetEREC_IraIratokService();
            execParam.Record_Id = csatolmanyRow["IraIrat_Id"].ToString();

            Csatolmany csatolmany = Generate_OCR_CsatolmanyObj(filePath, fileInfo);

            EREC_Csatolmanyok erec_Csatolmanyok = Generate_OCR_EREC_CsatolmanyObj(execParam.Clone(), csatolmanyRow["KuldKuldemeny_Id"].ToString(), csatolmanyRow["IraIrat_Id"].ToString());
            Result result = serviceIrat.CsatolmanyUpload(execParam, erec_Csatolmanyok, csatolmany);

            if (result.IsError)
            {
                Logger.Debug(result.ErrorCode + result.ErrorMessage);
                throw new ResultException(result);
            }
        }
        else if (!string.IsNullOrEmpty(csatolmanyRow["KuldKuldemeny_Id"].ToString()))
        {
            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_KuldKuldemenyekService serviceKuld = sf.GetEREC_KuldKuldemenyekService();
            execParam.Record_Id = csatolmanyRow["KuldKuldemeny_Id"].ToString();

            Csatolmany csatolmany = Generate_OCR_CsatolmanyObj(filePath, fileInfo);
            EREC_Csatolmanyok erec_Csatolmanyok = Generate_OCR_EREC_CsatolmanyObj(execParam.Clone(), csatolmanyRow["KuldKuldemeny_Id"].ToString(), csatolmanyRow["IraIrat_Id"].ToString());

            Result result = serviceKuld.CsatolmanyUpload(execParam, erec_Csatolmanyok, csatolmany);
            if (result.IsError)
            {
                Logger.Debug(result.ErrorCode + result.ErrorMessage);
                throw new ResultException(result);
            }
        }
        Logger.Info("AddNewOCRDocument.Stop");
    }

    /// <summary>
    /// GenerateOCRCsatolmanyObj
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="linkedKuldId"></param>
    /// <param name="linkedIratId"></param>
    /// <param name="dokumentId"></param>
    /// <returns></returns>
    private static EREC_Csatolmanyok Generate_OCR_EREC_CsatolmanyObj(ExecParam execParam, string linkedKuldId, string linkedIratId)
    {
        EREC_Csatolmanyok csatolmany = new EREC_Csatolmanyok();

        if (!string.IsNullOrEmpty(linkedIratId))
        {
            csatolmany.IraIrat_Id = linkedIratId;
            csatolmany.Updated.IraIrat_Id = true;
        }
        if (!string.IsNullOrEmpty(linkedKuldId))
        {
            csatolmany.KuldKuldemeny_Id = linkedKuldId;
            csatolmany.Updated.KuldKuldemeny_Id = true;
        }
        return csatolmany;
    }

    /// <summary>
    /// Generate_OCR_CsatolmanyObj
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="fileInfo"></param>
    /// <returns></returns>
    private Csatolmany Generate_OCR_CsatolmanyObj(string filePath, System.IO.FileInfo fileInfo)
    {
        Csatolmany csatolmany = new Csatolmany();
        csatolmany.Tartalom = GetFile(filePath);
        csatolmany.Nev = fileInfo.Name;
        csatolmany.TartalomHash = Contentum.eUtility.FileFunctions.CalculateSHA1(csatolmany.Tartalom);

        return csatolmany;
    }

    /// <summary>
    /// LoadFileFromPath
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    private byte[] LoadFileFromPath(string filePath)
    {
        if (string.IsNullOrEmpty(filePath) || !System.IO.File.Exists(filePath))
            return null;

        byte[] fileContent = System.IO.File.ReadAllBytes(filePath);
        return fileContent;
    }

    /// <summary>
    /// GetFile
    /// </summary>
    /// <param name="FilePath"></param>
    /// <returns></returns>
    public byte[] GetFile(String FilePath)
    {
        System.IO.FileStream fs = null;
        byte[] file_buffer = null;
        try
        {
            fs = new System.IO.FileStream(FilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
        }
        catch (System.IO.FileNotFoundException e)
        {
            throw e;
        }

        try
        {
            file_buffer = new byte[fs.Length];

            fs.Read(file_buffer, 0, (int)fs.Length);

            fs.Flush();
            fs.Close();
        }
        catch (System.IO.FileLoadException e)
        {
            throw e;
        }

        return file_buffer;
    }
    #endregion UPLOAD

    #region DOWNLOAD
    /// <summary>
    /// GetDocumentContent
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="documentId"></param>
    /// <returns></returns>
    public byte[] GetDocumentContent(ExecParam execParam, string documentId)
    {
        Logger.Debug(String.Format("GetDocContent: {0}", documentId));

        KRT_DokumentumokService service = new KRT_DokumentumokService(this.dataContext);
        ExecParam getExecParam = execParam.Clone();
        getExecParam.Record_Id = documentId;

        Result res = service.Get(getExecParam);

        if (res.IsError)
            throw new ResultException(res);

        KRT_Dokumentumok dok = res.Record as KRT_Dokumentumok;

        string externalLink = dok.External_Link;

        using (WebClient wc = new WebClient())
        {
            //wc.Credentials = new NetworkCredential(UI.GetAppSetting("eSharePointBusinessServiceUserName"), UI.GetAppSetting("eSharePointBusinessServicePassword"), UI.GetAppSetting("eSharePointBusinessServiceUserDomain"));
            wc.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
            return wc.DownloadData(externalLink);
        }
    }
    /// <summary>
    /// SaveDocumentToFile
    /// </summary>
    /// <param name="content"></param>
    /// <param name="filePath"></param>
    private void SaveDocumentToFile(byte[] content, string filePath)
    {
        File.WriteAllBytes(filePath, content);
    }
    #endregion DOWNLOAD

    #region TIMEOUT ERROR
    /// <summary>
    /// SendMail_OCRTimeOutError
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="msg"></param>
    private void SendMail_OCRTimeOutError(ExecParam execParam, string msg)
    {
        if (string.IsNullOrEmpty(msg))
            return;

        string to = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.HIBABEJELENTES_EMAIL_CIM);
        string from = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.HIBABEJELENTES_EMAIL_CIM);
        using (Contentum.eAdmin.Service.EmailService mailService = eAdminService.ServiceFactory.GetEmailService())
        {
            var res = mailService.SendEmail(execParam, from, new string[] { to }, "OCR", null, null, false, msg);
            if (!res)
                Logger.Error(String.Format("Az e-mail k�ld�s sikertelen. From: {0}, To: {1}, Message: {2}", from, to[0], msg));
        }
        return;
    }
    #endregion TIMEOUT ERROR

    #endregion HELPER
}