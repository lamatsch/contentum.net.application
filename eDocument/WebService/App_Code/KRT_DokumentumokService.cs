using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eDocument.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_DokumentumokService : System.Web.Services.WebService
{

    /// <summary>
    /// Keres�s dokumentumokra jogosults�gkezel�ssel
    /// A sz�r�felt�teleket a keres�si objektumban kell megadni
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="krt_DokumentumokSearch"></param>
    /// <param name="jogosultak"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_DokumentumokSearch))]
    public Result GetAllWithExtensionAndJogosultak(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch, bool jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtension(execParam, krt_DokumentumokSearch, jogosultak);
            
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);        
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }



    [WebMethod()]
    public Result GetWithRightCheck(ExecParam execParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);
            
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(execParam, result);
        return result;
    }




    [WebMethod()]
    // BUG_10252
    public Result DokumentumHierarchiaGetAll(ExecParam execParam, string dokumentumId, char standalone)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            // BUG_10252
            result = sp.DokumentumHierarchiaGetAll(execParam, dokumentumId, standalone);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Keres�s dokumentumokra jogosults�gkezel�ssel
    /// A sz�r�felt�teleket a keres�si objektumban kell megadni
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="krt_DokumentumokSearch"></param>
    /// <param name="jogosultak"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_DokumentumokSearch))]
    public Result GetAllWithExtensionAndMetaAndJogosultak(ExecParam execParam, KRT_DokumentumokSearch krt_DokumentumokSearch
        , string Obj_Id, String ObjMetaDefinicio_Id
        , String ColumnName, String[] ColumnValues, bool bColumnValuesExclude
        , String DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus
        , bool bStandaloneMode
        , bool jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            // metaadatok lek�r�se
            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_ObjektumTargyszavaiService service_ot = sf.GetEREC_ObjektumTargyszavaiService();
            ExecParam execParam_ot = execParam.Clone();

            EREC_ObjektumTargyszavaiSearch search_ot = new EREC_ObjektumTargyszavaiSearch();

            Result result_ot = service_ot.GetAllMetaByObjMetaDefinicio(execParam_ot
                , search_ot, Obj_Id, ObjMetaDefinicio_Id
                , Contentum.eUtility.Constants.TableNames.KRT_Dokumentumok
                , ColumnName, ColumnValues, bColumnValuesExclude
                , DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus);

            if (result_ot.IsError)
            {
                throw new ResultException(result_ot);
            }

            System.Collections.Generic.List<string> lstColumNames = new System.Collections.Generic.List<string>();
            foreach (System.Data.DataRow row in result_ot.Ds.Tables[0].Rows)
            {
                string Targyszo = row["Targyszo"].ToString();

                if (!String.IsNullOrEmpty(Targyszo) && !lstColumNames.Contains(String.Format("[{0}]", Targyszo)))
                {
                    lstColumNames.Add(String.Format("[{0}]", Targyszo));
                }
            }

            result = sp.GetAllWithExtensionAndMeta(execParam, krt_DokumentumokSearch
                , String.Join(",", lstColumNames.ToArray()), bStandaloneMode, jogosultak);

            #region t�rgysz�/metaadat oszlopok le�r� adatainak hozz�ad�sa
            if (!result.IsError)
            {
                System.Data.DataTable table = result_ot.Ds.Tables[0].Copy();
                table.TableName = Contentum.eUtility.Constants.TableNames.EREC_ObjektumTargyszavai;

                result.Ds.Tables.Add(table);
            }
            #endregion t�rgysz�/metaadat oszlopok le�r� adatainak hozz�ad�sa



            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

}