﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for KRT_FelhasznalokService
/// </summary>
[WebService(Namespace = "Contentum.eDocument.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService()]
public partial class KRT_FelhasznalokService : System.Web.Services.WebService
{
    private KRT_FelhasznalokStoredProcedure sp = null;

    private DataContext dataContext;

    public KRT_FelhasznalokService()
    {
        dataContext = new DataContext(this.Application);

        //sp = new KRT_FelhasznalokStoredProcedure(this.Application);
        sp = new KRT_FelhasznalokStoredProcedure(dataContext);
    }

    public KRT_FelhasznalokService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new KRT_FelhasznalokStoredProcedure(dataContext);
    }

    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott táblára vonatkozó keresés eredményhalmazának elkérése. A táblára vonatkozó szűrési feltételeket paraméter tartalmazza (*Search). Az ExecParam.Record_Id nem használt, a további adatok a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_FelhasznalokSearch))]
    public Result GetAll(ExecParam ExecParam, KRT_FelhasznalokSearch _KRT_FelhasznalokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _KRT_FelhasznalokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}