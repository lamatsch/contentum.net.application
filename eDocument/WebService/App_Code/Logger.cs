using log4net;
using log4net.Config;

//namespace Contentum.eDocument.WebService
//{
    /// <summary>
    /// Loggolo osztaly (by GrafL)
    /// </summary>
	public sealed class Logger
	{
        public const string Header = "$Header: Logger.cs, 4, 2008.11.03. 14:48:23, Kov?cs Andr?s$";
        public const string Version = "$Revision: 4$";
        
        private static Logger instance = new Logger();
		private static readonly ILog log = LogManager.GetLogger(typeof(Logger));
		
		public static Logger Instance {
			get { return instance; }
		}
		
		private Logger() { 
			log4net.Config.XmlConfigurator.Configure();
            //log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(@"Logger.config"));
			//DOMConfigurator.Configure();
		}
		
		public static ILog GetLogger
		{
			get { return log; }
		}


        private static string Append(string p_msg,
                                     System.Exception p_ex,
                                     Contentum.eBusinessDocuments.ExecParam p_execParam,
                                     Contentum.eBusinessDocuments.Result p_result)
        {
            string felh_Id = (p_execParam == null) ? "NULL" : p_execParam.Felhasznalo_Id;
            string errorCode = (p_result == null) ? "NULL" : p_result.ErrorCode;
            string errorMsg = (p_result == null) ? "NULL" : p_result.ErrorMessage;
            string exMsg = (p_ex == null) ? "NULL" : p_ex.Message;

            return "[" + felh_Id + "] " + p_msg + "(ErrorCode:'" + errorCode + "'; ErrorMessage:'" + errorMsg + "') " + exMsg;
        }

		
		public static void Debug(string p_msg)
		{
			Logger.log.Debug(p_msg, null);
		}

		public static void Debug(string p_msg, System.Exception p_ex)
		{
			if(Logger.log.IsDebugEnabled)
			{
				if(p_ex == null)
				{
					Logger.log.Debug(p_msg);
				}
				else
				{
					Logger.log.Debug(p_msg, p_ex);
				}
			}
		}

		public static void Info(string p_msg)
		{
			Logger.log.Info(p_msg, null);
		}

		public static void Info(string p_msg, System.Exception p_ex)
		{
			if(Logger.log.IsInfoEnabled)
			{
				if(p_ex == null)
				{
					Logger.log.Info(p_msg);
				}
				else
				{
					Logger.log.Info(p_msg, p_ex);
				}
			}
		}

		public static void Warn(string p_msg)
		{
			Logger.log.Warn(p_msg, null);
		}

		public static void Warn(string p_msg, System.Exception p_ex)
		{
			if(Logger.log.IsWarnEnabled)
			{
				if(p_ex == null)
				{
					Logger.log.Warn(p_msg);
				}
				else
				{
					Logger.log.Warn(p_msg, p_ex);
				}
			}
        }

        #region Error

        public static bool IsErrorEnabled()
        {
            return Logger.log.IsErrorEnabled;
        }

        public static void Error(string p_msg)
		{
			Logger.log.Error(p_msg, null);
		}

		public static void Error(string p_msg, System.Exception p_ex)
		{
			if(Logger.log.IsErrorEnabled)
			{
				if(p_ex == null)
				{
					Logger.log.Error(p_msg);
				}
				else
				{
					Logger.log.Error(p_msg, p_ex);
				}
			}
		}

        public static void Error(string p_msg,
                                  Contentum.eBusinessDocuments.ExecParam p_execParam)
        {
            Logger.Error(p_msg, null, p_execParam, null);
        }

        public static void Error(string p_msg,
                                  Contentum.eBusinessDocuments.ExecParam p_execParam,
                                  Contentum.eBusinessDocuments.Result p_result)
        {
            Logger.Error(p_msg, null, p_execParam, p_result);
        }

        public static void Error(string p_msg,
                                 System.Exception p_ex,
                                 Contentum.eBusinessDocuments.ExecParam p_execParam,
                                 Contentum.eBusinessDocuments.Result p_result)
        {
            if (Logger.IsErrorEnabled())
            {
                Logger.log.Error(Append(p_msg, p_ex, p_execParam, p_result));
            }
        }

        #endregion

        public static void Fatal(string p_msg)
		{
			Logger.log.Fatal(p_msg, null);
		}

		public static void Fatal(string p_msg, System.Exception p_ex)
		{
			if(Logger.log.IsFatalEnabled)
			{
				if(p_ex == null)
				{
					Logger.log.Fatal(p_msg);
				}
				else
				{
					Logger.log.Fatal(p_msg, p_ex);
				}
			}
		}
	}
//}
