﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for RightsService
/// </summary>
[WebService(Namespace = "Contentum.eDocument.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class RightsService : System.Web.Services.WebService
{
    private RightsServiceStoredProcedure sp = null;

    private DataContext dataContext;

    public RightsService()
    {
        dataContext = new DataContext(this.Application);

        sp = new RightsServiceStoredProcedure(dataContext);
    }

    public RightsService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new RightsServiceStoredProcedure(dataContext);
    }

    [WebMethod]
    public Result AddCsoportToJogtargy(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Kezi)
    {

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddCsoportToJogtargy(execParam, JogtargyId, CsoportId, Kezi, 'I', '0');

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    [WebMethod]
    public Result AddCsoportToJogtargyWithJogszint(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Kezi,
        char Jogszint,
        char Tipus)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddCsoportToJogtargy(execParam, JogtargyId, CsoportId, Kezi, Jogszint, Tipus);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }
}