﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using Contentum.eBusinessDocuments;

/// <summary>
/// 
/// </summary>

namespace Contentum.eDocument.SharePoint
{

    public class ContentTypeService
    {
        public const string Header = "$Header: ContentTypeService.cs, 9, 2015. 12. 15. 12:53:31, Molnár Péter$";
        public const string Version = "$Revision: 9$";

        private const string NSPC = "http://schemas.microsoft.com/sharepoint/soap/";

        private string SPUrl = null;
        private SPWebs.Webs webService = null;

        //  Ezeket a GetSiteContentTypeById, GetContentTypeByWordCTT tolti
        private string latestContentTypeId = String.Empty;
        private string latestContentTypeName = String.Empty;

        public ContentTypeService()
        {
            Logger.Info(String.Format("DocumentService.SharePoint.ContentTypeService start.- {0} {1}", Header, Version));

            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SPUrl = Utility.GetSharePointUrl();

            this.webService = new SPWebs.Webs();
            this.webService.Url = this.SPUrl + Utility.SharePointUrlPostfixRootSiteUrl() + DateTime.Today.ToString("yyyy") + "/" + "_vti_bin/webs.asmx";
            this.webService.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();

            Logger.Info(String.Format("ContentTypeService.InitializeComponent"));
            Logger.Info(String.Format("webService.Url={0}", this.webService.Url));
        }

        //  properties

        public string LatestContentTypeId
        {
            get { return this.latestContentTypeId; }
        }

        public string LatestContentTypeName
        {
            get { return this.latestContentTypeName; }
        }


        /// <summary>
        /// A root site-on definialva van-e a megadott id-ju CTT.
        /// </summary>
        /// <param name="cttid">A keresett CTT id-je.</param>
        /// <returns></returns>
        public bool IsExistsContentTypeById(string cttid)
        {
            Logger.Info(String.Format("ContentTypeService.IsExistsContentTypeById"));

            bool retVal = false;

            try
            {
                XmlNode ctts = this.webService.GetContentTypes();

                string search = String.Format("/a:ContentType[@ID='{0}']", cttid);

                XmlNamespaceManager xmlNspMgr = new XmlNamespaceManager(new XmlDocument().NameTable);
                xmlNspMgr.AddNamespace("a", NSPC);

                if (ctts.SelectNodes(search, xmlNspMgr).Count == 1)
                    retVal = true;

            }
            catch (SoapException soaexc)
            {
                Logger.Error("EXCEPTION (SOAP) itt: ContentTypeService.IsExistsContentTypeById");
                Logger.Error(String.Format("SOAP exc! Message: {0}\nStackTrace: {1}\nInner: {2}", soaexc.Message, soaexc.StackTrace, soaexc.InnerException));
                Logger.Error(String.Format("Kapott parameter: {0}", cttid));
            }
            catch (Exception exc)
            {
                Logger.Error("EXCEPTION itt: ContentTypeService.IsExistsContentTypeById");
                Logger.Error(String.Format("ExceptION! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                Logger.Error(String.Format("Kapott parameter: {0}", cttid));
            }

            return retVal;
        }

        /// <summary>
        /// A root siteon definialva van-e a megadott nevu CTT.
        /// </summary>
        /// <param name="cttname">A CTT neve.</param>
        /// <returns></returns>
        public bool IsExistsContentTypeByName(string cttname)
        {
            Logger.Info(String.Format("ContentTypeService.IsExistsContentTypeByName"));

            bool retVal = false;

            try
            {
                XmlNode ctts = this.webService.GetContentTypes();

                string search = String.Format("/a:ContentType[@Name='{0}']", cttname);

                XmlNamespaceManager xmlNspMgr = new XmlNamespaceManager(new XmlDocument().NameTable);
                xmlNspMgr.AddNamespace("a", NSPC);

                if (ctts.SelectNodes(search, xmlNspMgr).Count == 1)
                    retVal = true;

            }
            catch (SoapException soaexc)
            {
                Logger.Error("EXCEPTION (SOAP) itt: ContentTypeService.IsExistsContentTypeByName");
                Logger.Error(String.Format("SOAP exc! Message: {0}\nStackTrace: {1}\nInner: {2}", soaexc.Message, soaexc.StackTrace, soaexc.InnerException));
                Logger.Error(String.Format("Kapott parameter: {0}", cttname));
            }
            catch (Exception exc)
            {
                Logger.Error("EXCEPTION itt: ContentTypeService.IsExistsContentTypeByName");
                Logger.Error(String.Format("ExceptION! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                Logger.Error(String.Format("Kapott parameter: {0}", cttname));
            }

            return retVal;
        }

        /// <summary>
        /// Huhuhuu.
        /// Ez ugy mukodik, hogy vegigmegy mindenkeppen az osszes ctt-n. Mert.
        /// A wordtol kapott cttid sokkal hosszabb, mint az spsben hasznalt cttid, mert a wordben hozzacsap (vagy sps doclib?)
        /// egy plussz guid-ot. Az orokles miatt a guidban van leirva, ezert vegig kell menni az osszes sps-es ctt-n,
        /// s amelyikkel a "leghosszabban" megegyezik, az lesz az.
        /// </summary>
        /// <param name="wordCTTid"></param>
        /// <returns></returns>
        public string GetContentTypeByWordCTT(string wordCTTid)
        {
            Logger.Info(String.Format("ContentTypeService.GetContentTypeByWordCTT indul."));
            Logger.Info(String.Format("Kapott parameter (wordCTTid): {0}", wordCTTid));

            string retVal = "";

            try
            {
                Logger.Info(String.Format("GetContentTypes elott."));
                
                XmlNode ctts = this.webService.GetContentTypes();

                Logger.Info(String.Format("XmlNamespaceManager beallitasa."));

                XmlNamespaceManager xmlNspMgr = new XmlNamespaceManager(new XmlDocument().NameTable);
                xmlNspMgr.AddNamespace("a", NSPC);

                int y = 0;
                int spsCttIdLength = 0;

                Logger.Info(String.Format("Keresett nodeok elott."));

                XmlNodeList fphCtts = ctts.SelectNodes("/a:ContentType", xmlNspMgr);

                Logger.Info(String.Format("wordCTTid: {0}", wordCTTid));

                while (y < fphCtts.Count)
                {
                    XmlNode ctt = fphCtts[y];

                    string spsCttId = Convert.ToString(ctt.Attributes["ID"].Value);

                    Logger.Info(String.Format("{0}  -   : {1}",y, spsCttId));

                    if (wordCTTid.Trim().Length >= spsCttId.Length)
                    {
                        Logger.Info(String.Format("    ehhez hasonlitom: {0}", wordCTTid.Trim().Substring(0, spsCttId.Length)));
                        Logger.Info(String.Format("                 ezt: {0}", spsCttId.Trim()));

                        if ((wordCTTid.Trim().Substring(0, spsCttId.Length).Equals(spsCttId.Trim())) && (spsCttId.Length > spsCttIdLength))
                        {

                            Logger.Info(String.Format("Megvan!!!!!"));

                            spsCttIdLength = spsCttId.Length;
                            retVal = spsCttId;              //ctt.OuterXml.ToString();

                            this.latestContentTypeId = Convert.ToString(ctt.Attributes["ID"].Value);
                            this.latestContentTypeName = Convert.ToString(ctt.Attributes["Name"].Value);
                        }
                    }
                    else
                    {
                        Logger.Info("Hosszabb a vizsgalt CTTid, ezert tuti nem az.");
                    }
                    y++;
                }

            }
            catch (SoapException soaexc)
            {
                Logger.Error("EXCEPTION (SOAP) itt: ContentTypeService.GetContentTypeByWordCTT");
                Logger.Error(String.Format("SOAP exc! Message: {0}\nStackTrace: {1}\nInner: {2}", soaexc.Message, soaexc.StackTrace, soaexc.InnerException));
                Logger.Error(String.Format("Kapott parameter: {0}", wordCTTid));
            }
            catch (Exception exc)
            {
                Logger.Error("EXCEPTION itt: ContentTypeService.GetContentTypeByWordCTT");
                Logger.Error(String.Format("ExceptION! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                Logger.Error(String.Format("Kapott parameter: {0}", wordCTTid));
            }

            Logger.Info(String.Format("a GetContentTypeByWordCTT altal visszaadando ertek: {0}", retVal));

            return retVal;
        }
    
        /// <summary>
        /// A meadott site doclibjen van-e a megadott idju CTT.
        /// </summary>
        /// <param name="siteFullUrl">Site path, teljesen, gep nevvel midennel egyutt.</param>
        /// <param name="docLibName">A docLib neve.</param>
        /// <param name="cttid">A keresett CTT id-je.</param>
        /// <returns></returns>
        public bool IsExistsContentTypeOnDocLibById(string siteFullUrl, string docLibName, string cttid)
        {
            Logger.Info(String.Format("ContentTypeService.IsExistsContentTypeOnDocLibById"));

            bool retVal = false;

            SPLists.Lists list = new SPLists.Lists();
            list.Url = string.Format("{0}_vti_bin/lists.asmx", ((siteFullUrl.EndsWith("/")) ? siteFullUrl : siteFullUrl+"/"));
            list.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();

            Logger.Info(String.Format("Params -> siteFullUrl: {0}\ndocLibName: {1}\ncttid: {2}", siteFullUrl, docLibName, cttid));

            try
            {

                Logger.Info(String.Format("list.GetListContentTypes elott."));

                XmlNode ctts = list.GetListContentTypes(docLibName, "");

                Logger.Info(String.Format("XmlNamespaceManager elott."));

                XmlNamespaceManager xmlNspMgr = new XmlNamespaceManager(new XmlDocument().NameTable);
                xmlNspMgr.AddNamespace("a", NSPC);

                Logger.Info(String.Format("SelectNodes elott."));

                XmlNodeList ndls = ctts.SelectNodes("/a:ContentType", xmlNspMgr);

                int y = 0;
                while (retVal == false && y < ndls.Count)
                {
                    if (Convert.ToString(ndls[y].Attributes["ID"].Value).Equals(cttid))
                        retVal = true;
                    y++;
                }

                Logger.Info(String.Format("Eredmeny: {0}", retVal));

            }
            catch (SoapException soaexc)
            {
                Logger.Error("EXCEPTION  (SOAP) itt: ContentTypeService.IsExistsContentTypeOnDocLibById");
                Logger.Error(String.Format("SOAP exc! Message: {0}\nStackTrace: {1}\nInner: {2}", soaexc.Message, soaexc.StackTrace, soaexc.InnerException));
                Logger.Error(String.Format("Kapott parameter: siteFullUrl: {1}, docLibName: {2}, cttid: {0}", cttid, siteFullUrl, docLibName));
            }
            catch (Exception exc)
            {
                Logger.Error("EXCEPTION itt: ContentTypeService.IsExistsContentTypeOnDocLibById");
                Logger.Error(String.Format("ExceptION! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                Logger.Error(String.Format("Kapott parameter: siteFullUrl: {1}, docLibName: {2}, cttid: {0}", cttid, siteFullUrl, docLibName));
            }

            return retVal;
        }

        ///
        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <param name="docLibName"></param>
        /// <param name="cttId"></param>
        /// <returns></returns>
        /// 
        public Result AddContentType2DocumentLibrary(
              string siteUrl
            , string docLibName
            , string cttId
        )
        {
            Logger.Info("ListUtilService.AddContentType2DocumentLibrary indul.");

            Result ret = new Result();


            //  parameterek ellenorzese

            if (String.IsNullOrEmpty(siteUrl))
            {
                ret.ErrorCode = "PARAMHIBA1";
                ret.ErrorMessage = "ListUtilService.AddContentType2DocumentLibrary: üres a site-ra vonatkozó paraméter!";

                Logger.Error(String.Format("Hiba! Üres a megadott siteUrl paraméter!"));

                return ret;
            }

            if (String.IsNullOrEmpty(docLibName))
            {
                ret.ErrorCode = "PARAMHIBA2";
                ret.ErrorMessage = "ListUtilService.AddContentType2DocumentLibrary: üres a doclib-re vonatkozó paraméter!";

                Logger.Error(String.Format("Hiba! Üres a megadott docLibName paraméter!"));

                return ret;
            }

            if (String.IsNullOrEmpty(cttId))
            {
                ret.ErrorCode = "PARAMHIBA3";
                ret.ErrorMessage = "ListUtilService.AddContentType2DocumentLibrary: üres a tartalomtípusra vonatkozó paraméter!";

                Logger.Error(String.Format("Hiba! Üres a megadott cttId paraméter!"));

                return ret;
            }


            //  webservice:

            siteUrl += (siteUrl.EndsWith("/")) ? "" : "/";

            SPLists.Lists list = new SPLists.Lists();
            list.Url = String.Format("{0}_vti_bin/lists.asmx", siteUrl);

            list.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();

            //  szerepel-e a kivasztott ctt a listan
            //  mert ha igen, akkor nem kell semmit csinalnunk

            Logger.Info(String.Format("A lista ctt-k megszerzese:"));

            try
            {
                XmlNode listaCtts = list.GetListContentTypes(docLibName, "");

                XmlNamespaceManager xmlNspMgr = new XmlNamespaceManager(new XmlDocument().NameTable);
                xmlNspMgr.AddNamespace("a", NSPC);

                Logger.Info(String.Format("SelectNodes elott."));

                XmlNodeList ndls = listaCtts.SelectNodes("/a:ContentType", xmlNspMgr);

                bool megvan = false;
                int y = 0;
                while (megvan == false && y < ndls.Count)
                {
                    if (Convert.ToString(ndls[y].Attributes["ID"].Value).Length >= cttId.Trim().Length)
                        if (Convert.ToString(ndls[y].Attributes["ID"].Value).Substring(0,cttId.Trim().Length).Equals(cttId.Trim()))
                            megvan = true;
                    y++;
                }

                if (megvan)
                {
                    Logger.Info("A CTT fel van mar veve a docLibhez.");
                    ret = new Result();
                    ret.Uid = cttId;
                    return ret;
                }

            }
            catch (SoapException spe)
            {
                Logger.Error("SoapException: " +
                String.Format("Message:{0}\nDetail:{1}\nStackTrace:{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: "
                   + " siteUrl = " + siteUrl + "\n"
                   + " cttId = " + cttId);
                ret.ErrorCode = "GETLCTTS001";
                ret.ErrorMessage = String.Format("AddContentType2DocumentLibrary method GetListContentTypes lekérdezés hiba!\nMessage:{0}\nDetail:{1}\nStackTrace:{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace);
                return ret;
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Exception : {0}\n{1}",ex.Message,ex.StackTrace));
                Logger.Error("Kapott parameterek: "
                   + " siteUrl = " + siteUrl + "\n"
                   + " cttId = " + cttId);
                ret.ErrorCode = "GETLCTTS002";
                ret.ErrorMessage = String.Format("AddContentType2DocumentLibrary method GetListContentTypes lekérdezés hiba!\nMessage:{0}\nStackTrace:{1}\n", ex.Message, ex.StackTrace);
                return ret;
            }


            //  a ctt display nevenek megszerzese

            Logger.Info(String.Format("A ctt Display nevenek megszerzese."));

            string cttDisplayName = "Nevenincs CTT";


            Result cttDispNameResult = this.GetSiteContentTypeDispalyNameById(siteUrl, cttId);

            if (!String.IsNullOrEmpty(cttDispNameResult.ErrorCode))
            {
                return cttDispNameResult;
            }

            cttDisplayName = Convert.ToString(cttDispNameResult.Record);

            Logger.Info(String.Format("A ctt Display neve: {0}", cttDisplayName));

            //  CreateContentType method hívása

            Logger.Info(String.Format("A CreateContentType hivasa."));

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlNode xmlFields = xmlDoc.CreateNode(XmlNodeType.Element, "FieldRefs", "");
                XmlNode xmlProps = xmlDoc.CreateNode(XmlNodeType.Element, "ContentType", "");

                Logger.Info(String.Format("list.CreateContentType elott."));

                string eredmeny = list.CreateContentType(docLibName, cttDisplayName, cttId, xmlFields, xmlProps, null);

                Logger.Info(String.Format("list.CreateContentType eredmenye: {0}", eredmeny));
            }
            catch (SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException: " +
                String.Format("Message:{0}\nDetail:{1}\nStackTrace:{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: "
                   + " siteUrl = " + siteUrl + "\n"
                   + " docLibName = " + docLibName + "\n"
                   + " cttId = " + cttId);
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "3422222444";
                ret.ErrorMessage = ex.StackTrace.ToString();
                Logger.Error("Exception : " + ex.ToString());
                Logger.Error("Kapott parameterek: "
                   + " siteUrl = " + siteUrl + "\n"
                   + " docLibName = " + docLibName + "\n"
                   + " cttId = " + cttId);
            }

            return ret;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <param name="cttId"></param>
        /// <returns></returns>
        public XmlNode GetSiteContentTypeById(
              string siteUrl
            , string cttId)
        {
            XmlNode retval = null;

            Logger.Info(String.Format("ContentTypeService.GetContentTypeById indul."));
            Logger.Info(String.Format("Params -> siteUrl: {0}, cttId: {1}",siteUrl,cttId));

            try
            {
                SPWebs.Webs webs = new SPWebs.Webs();
                webs.Url = String.Format("{0}_vti_bin/webs.asmx", (siteUrl.EndsWith("/")) ? siteUrl : siteUrl+"/");
                webs.Credentials = Utility.GetCredential();

                Logger.Info(String.Format("web.url: {0}", webs.Url));

                XmlNode siteCtts = webs.GetContentTypes();

                XmlNamespaceManager xmlNsMan = new XmlNamespaceManager(new XmlDocument().NameTable);
                xmlNsMan.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/soap/");

                XmlNodeList xmlNlis = siteCtts.SelectNodes(String.Format("/a:ContentType[@ID='{0}']", cttId), xmlNsMan);

                if (xmlNlis.Count == 0)
                {
                    Logger.Error(String.Format("HIBA: Nem talált megfelelo CTTid-t!!!"));
                    return null;
                }

                if (xmlNlis.Count > 1)
                {
                    Logger.Error(String.Format("HIBA: Túl sok sort talált a megadott CTTid alapján!!!"));
                    return null;
                }

                Logger.Error(String.Format("Megvan az id."));
                retval = xmlNlis[0];

                Logger.Error(String.Format("(Letesszuk valtozoba nevet es az id-t.)"));
                this.latestContentTypeId = Convert.ToString(xmlNlis[0].Attributes["ID"].Value);
                this.latestContentTypeName = Convert.ToString(xmlNlis[0].Attributes["Name"].Value);

            }
            catch (SoapException spe)
            {
                Logger.Error("SoapException: " +
                String.Format("Message:{0}\nDetail:{1}\nStackTrace:{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: "
                   + " siteUrl = " + siteUrl + "\n"
                   + " cttId = " + cttId);
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("Exception : " + ex.ToString());
                Logger.Error("Kapott parameterek: "
                   + " siteUrl = " + siteUrl + "\n"
                   + " cttId = " + cttId);
                return null;
            }

            return retval;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sitePath"></param>
        /// <param name="cttid"></param>
        /// <returns></returns>
        public Result GetSiteContentTypeDispalyNameById(string sitePath, string cttid)
        {
            Logger.Info(String.Format("ContentTypeService.GetSiteContentTypeDispalyNameById indul."));
            Logger.Info(String.Format("Params -> siteUrl: {0}, cttId: {1}", sitePath, cttid));

            Result ret = new Result();

            XmlNode cttXml = this.GetSiteContentTypeById(sitePath, cttid);

            if (cttXml == null)
            {
                ret.ErrorCode = "GSCTDNBI00001";
                ret.ErrorMessage = "Hiba a GetSiteContentTypeDispalyNameById methodban: null node-dal tért vissza a GetSiteContentTypeById.";
                return ret;
            }


            try
            {
                ret.Record = Convert.ToString(cttXml.Attributes["Name"].Value);
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba a visszakapott xml-bol attributum kivetelekor (szep magyarul :))."));
                Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error(String.Format("Visszakapott XML: {0}", cttXml.OuterXml.ToString()));

                ret.ErrorCode = "GCTT0002";
                ret.ErrorMessage = String.Format("Ctt id alapján keresés hiba a ctt docLibhez kötésekor: Hiba az attributum kivételekor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                return ret;
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteFullUrl"></param>
        /// <param name="cttid"></param>
        /// <returns></returns>
        public string GetCTTNameById(string sitePath, string cttid)
        {
            Logger.Info(String.Format("ContentTypeService.GetCTTNameById indul."));
            Logger.Info(String.Format("Params -> siteUrl: {0}, cttId: {1}", sitePath, cttid));

            string retVal = String.Empty;

            Logger.Info(String.Format("this.GetSiteContentTypeById hivasa."));
            XmlNode cttXml = this.GetSiteContentTypeById(sitePath, cttid);

            if (cttXml == null)
            {
                Logger.Info(String.Format("Null-al tert vissza a this.GetSiteContentTypeById. Hiba!"));
                return retVal;
            }

            retVal = Convert.ToString(cttXml.Attributes["Name"].Value);
            Logger.Info(String.Format("Kapott nev: {0}", retVal));

            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteFullUrl"></param>
        /// <param name="cttid"></param>
        /// <returns></returns>
        public bool GetTTAzonositoByCttId(string siteFullUrl, string cttid)
        {
            Logger.Info(String.Format("ContentTypeService.GetTTAzonositoByCttId"));

            bool retVal = false;

            //SPWebs.Webs web = new SPWebs.Webs();
            //list.Url = string.Format("{0}_vti_bin/webs.asmx", ((siteFullUrl.EndsWith("/")) ? siteFullUrl : siteFullUrl + "/"));
            //list.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();

            //Logger.Info(String.Format("Params -> siteFullUrl: {0}\ncttid: {1}", siteFullUrl, cttid));

            //try
            //{

            //    Logger.Info(String.Format("web.GetListContentTypes elott."));

            //    XmlNode ctts = web.GetContentType(cttid);

            //    Logger.Info(String.Format("XmlNamespaceManager elott."));

            //    XmlNamespaceManager xmlNspMgr = new XmlNamespaceManager(new XmlDocument().NameTable);
            //    xmlNspMgr.AddNamespace("a", NSPC);

            //    Logger.Info(String.Format("SelectNodes elott."));

            //    XmlNodeList ndls = ctts.SelectNodes("/a:ContentType", xmlNspMgr);

            //    int y = 0;
            //    while (retVal == false && y < ndls.Count)
            //    {
            //        if (Convert.ToString(ndls[y].Attributes["ID"].Value).Equals(cttid))
            //            retVal = true;
            //        y++;
            //    }

            //    Logger.Info(String.Format("Eredmeny: {0}", retVal));

            //}
            //catch (SoapException soaexc)
            //{
            //    Logger.Error("EXCEPTION  (SOAP) itt: ContentTypeService.IsExistsContentTypeOnDocLibById");
            //    Logger.Error(String.Format("SOAP exc! Message: {0}\nStackTrace: {1}\nInner: {2}", soaexc.Message, soaexc.StackTrace, soaexc.InnerException));
            //    Logger.Error(String.Format("Kapott parameter: siteFullUrl: {1}, docLibName: {2}, cttid: {0}", cttid, siteFullUrl, docLibName));
            //}
            //catch (Exception exc)
            //{
            //    Logger.Error("EXCEPTION itt: ContentTypeService.IsExistsContentTypeOnDocLibById");
            //    Logger.Error(String.Format("ExceptION! Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
            //    Logger.Error(String.Format("Kapott parameter: siteFullUrl: {1}, docLibName: {2}, cttid: {0}", cttid, siteFullUrl, docLibName));
            //}

            return retVal;
        }

        /// <summary>
        /// Id alapjan visszaadja egy xmlben a nem hidden filed-eket! Xml stringet ad vissza a Record mezoben.
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <param name="cttId"></param>
        /// <returns>Xml stringet ad vissza a Record mezoben.</returns>
        public Result GetCTTFieldsById(string siteUrl, string cttId)
        {
            Logger.Info(String.Format("ContentTypeService.GetCTTFieldsById indul. Parameterek: siteUrl: {0} ; cttId: {1}", siteUrl,cttId));

            Result _ret = new Result();

            #region Paramok ellenorzese

            if (String.IsNullOrEmpty(siteUrl))
            {
                Logger.Error("Parameterezesi hiba! Ures a siteUrl parameter!");
                _ret.ErrorCode = "PARAM0001";
                _ret.ErrorMessage = "Parameterezesi hiba! Ures a siteUrl parameter!";
                return _ret;
            }

            if (String.IsNullOrEmpty(cttId))
            {
                Logger.Error("Parameterezesi hiba! Ures a cttId parameter!");
                _ret.ErrorCode = "PARAM0002";
                _ret.ErrorMessage = "Parameterezesi hiba! Ures a cttId parameter!";
                return _ret;
            }

            #endregion

            #region web-s fele ctt lekerdezese

            XmlNode ctt = null;

            try
            {
                SPWebs.Webs webs = new SPWebs.Webs();
                webs.Url = String.Format("{0}_vti_bin/webs.asmx", (siteUrl.EndsWith("/")) ? siteUrl : siteUrl+"/");
                webs.Credentials = Utility.GetCredential();

                Logger.Info(String.Format("web.url: {0}", webs.Url));

                ctt = webs.GetContentType(cttId);

            }
            catch (SoapException spe)
            {
                Logger.Error("SoapException: " +
                String.Format("Message:{0}\nDetail:{1}\nStackTrace:{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: "
                   + " siteUrl = " + siteUrl + "\n"
                   + " cttId = " + cttId);
                _ret.ErrorCode = "SPEXC00X1";
                _ret.ErrorMessage = String.Format("Message:{0}\nDetail:{1}\nStackTrace:{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace);
                return _ret;
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1} ", ex.Message, ex.StackTrace));
                Logger.Error("Kapott parameterek: "
                   + " siteUrl = " + siteUrl + "\n"
                   + " cttId = " + cttId);
                _ret.ErrorCode = "SPEXC00X2";
                _ret.ErrorMessage = String.Format("Exception! Message: {0}\nStackTrace: {1} ", ex.Message, ex.StackTrace);
                return _ret;
            }

            #endregion

            #region nem hidden csoportuak kiszedese

            try 
        	{
                Logger.Info(String.Format("Kapott eredmenyhalmaz: {0}", ctt.OuterXml.ToString()));

                XmlDocument eredmenyXml = new XmlDocument();

                //Group = "_Hidden";
		        XmlNamespaceManager xmlNsMan = new XmlNamespaceManager(new XmlDocument().NameTable);
                xmlNsMan.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/soap/");

                XmlNodeList xmlNlis = ctt.SelectNodes(String.Format("//a:Field[@Group!='_Hidden']"), xmlNsMan);

                Logger.Info(String.Format("Talalt darabszam: {0}", xmlNlis.Count));

                if (xmlNlis.Count == 0)
                {
                    Logger.Error(String.Format("Nem talalhato nem hidden field!"));
                    _ret.ErrorCode = "EREDMELOALL001";
                    _ret.ErrorMessage = String.Format("Nem található nem hidden field!");
                    return _ret;
                }

                XmlNode ndRoot = eredmenyXml.CreateNode(XmlNodeType.Element, "Fields", "");

                int i = 0;
                while (i < xmlNlis.Count)
                {
                    XmlNode nd = xmlNlis[i];

                    XmlNode ndUj = eredmenyXml.CreateNode(XmlNodeType.Element, "Field", "");

                    for (int t = 0; t < nd.Attributes.Count; t++)
                    {
                        String attrName = nd.Attributes[t].Name;
                        XmlAttribute a = eredmenyXml.CreateAttribute(attrName);
                        a.Value = Convert.ToString(nd.Attributes[attrName].Value);
                        ndUj.Attributes.Append(a);
                    }

                    ndRoot.AppendChild(ndUj);

                    i++;
                }

                eredmenyXml.AppendChild(ndRoot);

                _ret.Record = eredmenyXml.OuterXml.ToString();

	        }
	        catch (Exception ex)
	        {
                Logger.Error(String.Format("Exception SPEXC00X4! Message: {0}\nStackTrace: {1} ", ex.Message, ex.StackTrace));
                Logger.Error("Kapott parameterek: "
                   + " siteUrl = " + siteUrl + "\n"
                   + " cttId = " + cttId);
                _ret.ErrorCode = "SPEXC00X4";
                _ret.ErrorMessage = String.Format("Exception! Message: {0}\nStackTrace: {1} ", ex.Message, ex.StackTrace);
                return _ret;
	        }
                        

            #endregion

            return _ret;
        }

    }
}
