using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eBusinessDocuments;
using System.IO;
using System.Net;
using System.Xml;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Globalization;

/*
 *  VT: - Valami kegyetlen�l fogja ezt a g�pet!
 *  KP: - Az asztal.
 *  
 *  Valami ilyesmi szitu volt:
 *  BB Norbinak: - Futtattam ezt a programot, de nem volt j�. Most meg eb�delni voltam.
 *  KP: - De az j� volt.
 *  
 */

namespace Contentum.eDocument.SharePoint
{
    public class DocumentService
    {
        public const string Header = "$Header: DocumentService.cs, 18, 2010.10.12 17:09:23, Lamatsch Andr?s$";
        public const string Version = "$Revision: 18$";

        public DocumentService()
        {
            Logger.Info(String.Format("DocumentService.SharePoint.DocumentService start.- {0} {1}", Header, Version));
        }

        /// <summary>
        /// Ez jo.
        /// </summary>
        /// <param name="siteName"></param>
        /// <param name="listName"></param>
        /// <param name="folderNames"></param>
        /// <param name="filename"></param>
        /// <param name="cont"></param>
        /// <returns></returns>
        /// 
        public Result Upload(string siteName, string listName, string folderNames, string filename, byte[] cont)
        {
            return Upload(siteName, listName, folderNames, filename, cont, Utility.GetCredential());
        }
        
        public Result Upload(string siteName, string listName, string folderNames, string filename, byte[] cont, NetworkCredential upldCred)
        {

            Logger.Info("DocumentService.Upload start");
            Result _ret = new Result();

            if (String.IsNullOrEmpty(filename))
            {
                // TODO:
                Logger.Info("Ures a filenev param.");
                _ret.ErrorCode = "300002";
                return _ret;
            }

            #region WebClient modon valo feltoltes
            try
            {
                String SPUrl = Utility.GetSharePointUrl();
                //if (!path.EndsWith("/")) { path += "/"; }

                System.Net.WebClient client = new System.Net.WebClient();
                client.Credentials = upldCred; // Utility.GetCredential();

                // barmilyen valaszt visszaadhat :(
                // ha ures akkor is sikeres az upload
                string pathToFileUpload = SPUrl + Utility.SharePointUrlPostfix()
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                    + ((!folderNames.EndsWith("/") && !String.IsNullOrEmpty(folderNames)) ? folderNames + "/" : folderNames)
                    + filename;
                Logger.Info(String.Format("client.UploadData elott: {0}", pathToFileUpload));
                //byte[] valasz = client.UploadData(pathToFileUpload, "PUT", cont);

                #region upload - upload API hasznalata

                Logger.Info("GetAxisUploadToSps indul.");

                string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
                string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
                string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");

                Logger.Info(String.Format("Upload Credentials: {0}\\{1}", __doamin, __usernev));

                AxisUploadToSps.Upload upld = new AxisUploadToSps.Upload();
                upld.Url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AxisUploadToSps.upload");

                Logger.Info(String.Format("Upload url: {0}", upld.Url));

                upld.Credentials = new System.Net.NetworkCredential(__usernev, __password, __doamin);

                String res = upld.UploadFile(pathToFileUpload, listName, cont);

                XmlDocument resultDoc = new XmlDocument();
                try
                {
                    resultDoc.LoadXml(res);
                }
                catch (Exception ex)
                {
                    Logger.Error(String.Format("Hiba az upload altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res,ex.Message, ex.StackTrace));
                    _ret.ErrorCode = "UPLDXML0001";
                    _ret.ErrorMessage = "Hiba az upload f�ggv�ny �ltal visszaadott string xml konvert�l�sakor!";
                    return _ret;
                }

                if (!String.IsNullOrEmpty(resultDoc.SelectSingleNode("//errorcode").InnerText))
                {
                    _ret.ErrorCode = resultDoc.SelectSingleNode("//errorcode").InnerText;
                    _ret.ErrorMessage = resultDoc.SelectSingleNode("//errormessage").InnerText;
                    Logger.Error(String.Format("Hiba az spsen levo UploadFile fuggvenyben! Ezt adta vissza: {0}\n{1}",_ret.ErrorCode, _ret.ErrorMessage));
                    return _ret;
                }

                #endregion

                // id visszaselect
                //ListUtilService lus = new ListUtilService();
                //Logger.Info("GetListItemIdByName elott");
                //Result lusRet = lus.GetListItemIdByName(siteName, listName, folderNames, filename);

                //if (lusRet.Uid != null)
                //{
                //    _ret.Uid = lusRet.Uid;
                //    Logger.Info("kapott id: " + lusRet.Uid.ToString());
                //}
                //else
                //{
                //    Logger.Info("Ures a visszakapott id!!!!");
                //}

                _ret.Uid = resultDoc.SelectSingleNode("//uniqueid").InnerText;

            }
            catch (Exception ex)
            {
                _ret.ErrorCode = "11111111111";
                _ret.ErrorMessage = ex.ToString();
                Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; listName=" + listName + " ; folderNames=" + folderNames + " ; filename=" + filename);
            }
            #endregion

            #region sajat ws - sps objektumokkkal valo feltoltes

            //Logger.Info("GetAxisUploadToSps indul.");

            //using (AxisUploadToSps.Upload upld = new AxisUploadToSps.Upload())
            //{
            //    upld.Url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AxisUploadToSps.upload");

            //    Logger.Info(String.Format("Upload url: {0}", upld.Url));
            //    Logger.Info(String.Format("Upload cred: {0}", upldCred.ToString()));

            //    upld.Credentials = upldCred;

            //    string strGuid = upld.UploadData(siteName, listName, folderNames, filename, cont);

            //    try
            //    {
            //        Guid g = new Guid(strGuid);
            //        Logger.Info(String.Format("Feltoltes soran kapott guid: {0}", strGuid));
            //    }
            //    catch (Exception ex)
            //    {
            //        Logger.Error(String.Format("Hiba az upload soaran! Reszletesebb hibauzenet az SPSen levo AxisFileUpload logban! Kapott hibauzenet: {0}", strGuid));
            //        _ret.ErrorCode = "UPLD0001";
            //        _ret.ErrorMessage = strGuid;
            //        return _ret;
            //    }

            //    _ret.Uid = strGuid;
            //}

            #endregion

            return _ret;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteName"></param>
        /// <param name="listName"></param>
        /// <param name="folderNames"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        /// 
        
        public Result Get(string siteName, string listName, string folderNames, string filename)
        {
            Result _ret = new Result();
            Logger.Info("DocumentService.Get start.");

            if (String.IsNullOrEmpty(filename))
            {
                // TODO:
                Logger.Info("Ures a filenev param.");
                _ret.ErrorCode = "300002";
                return _ret;
            }

            try
            {
                String SPUrl = Utility.GetSharePointUrl();

                System.Net.WebClient client = new System.Net.WebClient();
                client.Credentials = Utility.GetCredential();

                // barmilyen valaszt visszaadhat :(
                // ha ures akkor is sikeres az upload
                //    ide nem kell + Utility.SharePointUrlPostfix()!!!
                string pathFromFile = SPUrl
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                    + ((!folderNames.EndsWith("/") && !String.IsNullOrEmpty(folderNames)) ? folderNames + "/" : folderNames)
                    + filename;

                Logger.Info("client.DownloadData elott");
                byte[] valasz = client.DownloadData(pathFromFile);

                Logger.Info("client.DownloadData utan - valasz a Record valtozoba :)");
                _ret.Record = valasz;
            }
            catch (SoapException spe)
            {
                _ret.ErrorCode = "1111111SOAP";
                _ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException:" +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; listName=" + listName + " ; folderNames=" + folderNames + " ; filename=" + filename);
            }
            catch (Exception ex)
            {
                _ret.ErrorCode = "11111111111";
                _ret.ErrorMessage = ex.ToString();
                Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; listName=" + listName + " ; folderNames=" + folderNames + " ; filename=" + filename);
            }

            return _ret;
        }

        /// <summary>
        /// Ez jo!
        /// </summary>
        /// <param name="path"></param>
        /// <param name="filename"></param>
        /// <param name="komment"></param>
        /// <param name="checkInMode">0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn
        /// Contentum.eUtility.Constants.SharePointCheckIn.MajorCheckIn
        /// Contentum.eUtility.Constants.SharePointCheckIn.MinorCheckIn
        /// Contentum.eUtility.Constants.SharePointCheckIn.OverwriteCheckIn
        /// </param>
        /// <returns></returns>
        /// 
        
        public Result CheckIn(string siteName
            , string listName
            , string folderNames
            , string filename
            , string komment
            , string checkInMode)
        {
            Logger.Info("DocumentService.CheckIn start.");
            Result _ret = new Result();

            if (String.IsNullOrEmpty(filename))
            {
                // TODO:
                Logger.Info("Ures a filenev param.");
                _ret.ErrorCode = "300002";
                return _ret;
            }

            String SPUrl = Utility.GetSharePointUrl();
            SPLists.Lists list = new SPLists.Lists();
            string fileUrl = "";
            try
            {
                // + ((!folderNames.EndsWith("/") && !String.IsNullOrEmpty(folderNames)) ? folderNames + "/" : folderNames)
                list.Url = SPUrl + Utility.SharePointUrlPostfix()
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                    + "_vti_bin/lists.asmx";
                //list.Url = SPUrl + path + "_vti_bin/lists.asmx";
                list.Credentials = Utility.GetCredential();

                fileUrl = SPUrl + Utility.SharePointUrlPostfix()
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                    + ((!folderNames.EndsWith("/") && !String.IsNullOrEmpty(folderNames)) ? folderNames + "/" : folderNames)
                    + filename;

                Logger.Info("list.CheckInFile elott.");

                //0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn
                _ret.Record = list.CheckInFile(fileUrl, komment, checkInMode);

            }
            catch (SoapException spe)
            {
                _ret.ErrorCode = "1111111SOAP";
                _ret.ErrorMessage = String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace);
                Logger.Error("SoapException:" +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; listName=" + listName + " ; folderNames=" + folderNames + " ; filename=" + filename);
                Logger.Error(String.Format("ListURL: {0}", list.Url));
                Logger.Error(String.Format("fileUrl: {0}", fileUrl));
            }
            catch (Exception ex)
            {
                _ret.ErrorCode = "11111111112";
                _ret.ErrorMessage = ex.ToString();
                Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; listName=" + listName + " ; folderNames=" + folderNames + " ; filename=" + filename);
            }

            return _ret;

        }

        /// <summary>
        /// Ez jo.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        /// 
        
        public Result CheckOut(string siteName, string listName, string folderNames, string filename)
        {
            Result _ret = new Result();

            Logger.Info("DocumentService.CheckOut start.");
            if (String.IsNullOrEmpty(filename))
            {
                // TODO:
                Logger.Info("Ures a filenev param.");
                _ret.ErrorCode = "300002";
                return _ret;
            }

            String SPUrl = Utility.GetSharePointUrl();

            try
            {
                ListUtilService lisu = new ListUtilService();
                Result itemResult = lisu.GetListItemsByItemNameAppendWithPath(siteName, listName, folderNames, filename);

                if (!String.IsNullOrEmpty(itemResult.ErrorCode))
                {
                    Logger.Error(String.Format("Hiba a GetListItemsByItemNameAppendWithPath methodban: {0} - {1}", itemResult.ErrorCode, itemResult.ErrorMessage));
                    return itemResult;
                }

                XmlDocument xmld = new XmlDocument();
                String lastModDate = String.Empty;

                try
                {
                    Logger.Info(String.Format("GetListItemsByItemNameAppendWithPath adott xml eredmeny: {0}", itemResult.Record));
                    xmld.LoadXml(Convert.ToString(itemResult.Record));

                    XmlNodeList nodeList = xmld.GetElementsByTagName("z:row");
                    if (nodeList.Count == 1)
                    {
                        XmlNode node = nodeList.Item(0);
                        lastModDate = node.Attributes["ows_Last_x0020_Modified"].InnerXml.ToString();

                        lastModDate = lastModDate.Substring(lastModDate.LastIndexOf(";#") + 2);
                        Logger.Info(String.Format("lastModDate ertek: {0}", lastModDate));

                        try
                        {
                            DateTime tmpD;
                            if (!DateTime.TryParse(lastModDate, out tmpD))
                            {
                                Logger.Error(String.Format("Hiba a kapott lastmodified datum parsolasakor (0)!"));

                                _ret = new Result();
                                _ret.ErrorCode = "XML0004";
                                _ret.ErrorMessage = String.Format("Hiba a kapott lastmodified datum parsolasakor (0)!");
                                return _ret;
                            }

                            lastModDate = tmpD.ToString("r");
                            Logger.Info(String.Format("lastModDate ertek RFC1123: {0}", lastModDate));
                        }
                        catch (Exception exx)
                        {
                            Logger.Error(String.Format("Hiba a kapott lastmodified datum parsolasakor (1)! Message: {0}\nStackTrace: {1}", exx.Message, exx.StackTrace));
                            
                            _ret = new Result();
                            _ret.ErrorCode = "XML0003";
                            _ret.ErrorMessage = String.Format("Hiba a kapott lastmodified datum parsolasakor (1)! Message: {0}\nStackTrace: {1}", exx.Message, exx.StackTrace);
                            return _ret;
                        }

                    }
                    else
                    {
                        Logger.Error(String.Format("A GetListItemsByItemNameAppendWithPath method xml eredmenyeben nem egy sor van!"));
                        _ret = new Result();
                        _ret.ErrorCode = "XML0002";
                        _ret.ErrorMessage = String.Format("A GetListItemsByItemNameAppendWithPath method xml eredmenyeben nem egy sor van!");
                        return _ret;
                    }

                }
                catch (Exception ez)
                {
                    Logger.Error(String.Format("Xml error: {0}\n{1}", ez.Message, ez.StackTrace));
                    _ret = new Result();
                    _ret.ErrorCode = "XML0001";
                    _ret.ErrorMessage = String.Format("Xml error: {0}\n{1}", ez.Message, ez.StackTrace);
                    return _ret;
                }


                //+ ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                //+ ((!folderNames.EndsWith("/") && !String.IsNullOrEmpty(folderNames)) ? folderNames + "/" : folderNames)
                SPLists.Lists list = new SPLists.Lists();
                list.Url = SPUrl + Utility.SharePointUrlPostfix()
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + "_vti_bin/lists.asmx";
                list.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();

                Logger.Info(String.Format("list.Url: {0}", list.Url));

                string fileUrl = SPUrl + Utility.SharePointUrlPostfix()
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                    + ((!folderNames.EndsWith("/") && !String.IsNullOrEmpty(folderNames)) ? folderNames + "/" : folderNames)
                    + filename;

                Logger.Info(String.Format("fileUrl: {0}", fileUrl));

                // RFC 1123 datum fomatum kell!
                Logger.Info("CheckOutFile elott.");
                _ret.Record = list.CheckOutFile(fileUrl, "true", ""); //lastModDate); //, DateTime.Now.ToString("r"));

            }
            catch (SoapException spe)
            {
                _ret.ErrorCode = "1111111SOAP";
                _ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException:" +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; listName=" + listName + " ; folderNames=" + folderNames + " ; filename=" + filename);
            }
            catch (Exception ex)
            {
                _ret.ErrorCode = "11111111112";
                _ret.ErrorMessage = ex.ToString();
                Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; listName=" + listName + " ; folderNames=" + folderNames + " ; filename=" + filename);
            }

            return _ret;

        }

        /// <summary>
        /// Ez jo.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        /// 
        
        public Result UndoCheckOut(string siteName, string listName, string folderNames, string filename)
        {
            Logger.Info("DocumentService.UndoCheckOut start.");
            Result _ret = new Result();

            if (String.IsNullOrEmpty(filename))
            {
                // TODO:
                Logger.Info("Ures a filenev param.");
                _ret.ErrorCode = "300002";
                return _ret;
            }

            String SPUrl = Utility.GetSharePointUrl();

            string fileUrl = SPUrl + Utility.SharePointUrlPostfix()
                + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                + ((!folderNames.EndsWith("/") && !String.IsNullOrEmpty(folderNames)) ? folderNames + "/" : folderNames)
                + filename;

            try
            {
                SPLists.Lists list = new SPLists.Lists();
                list.Url = SPUrl + Utility.SharePointUrlPostfix()
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + "_vti_bin/lists.asmx";
                list.Credentials = Utility.GetCredential();

                Logger.Info("UndoCheckOut elott.");

                // RFC 1123 datum fomatum kell!
                _ret.Record = list.UndoCheckOut(fileUrl);

            }
            catch (SoapException spe)
            {
                _ret.ErrorCode = "1111111SOAP";
                _ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException:" +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; listName=" + listName + " ; folderNames=" + folderNames + " ; filename=" + filename);
            }
            catch (Exception ex)
            {
                _ret.ErrorCode = "11111111112";
                _ret.ErrorMessage = ex.ToString();
                Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; listName=" + listName + " ; folderNames=" + folderNames + " ; filename=" + filename);
            }

            return _ret;

        }

        public Result IsDocumentExists(string sitePath, string docLibName, string folderPath,  string fileName)
        {
            Logger.Info("DocumentService.IsDocumentExists start.");
            Result ret = new Result();

            if (String.IsNullOrEmpty(docLibName) ||
                String.IsNullOrEmpty(sitePath) ||
                String.IsNullOrEmpty(fileName))
            {
                // TODO:
                Logger.Info("Ures a filenev param.");
                ret.ErrorCode = "300002";
                return ret;
            }

            String SPUrl = Utility.GetSharePointUrl();
            if (!sitePath.EndsWith("/")) { sitePath += "/"; }

            try
            {

                SPLists.Lists list = new SPLists.Lists();
                list.Url = SPUrl + Utility.SharePointUrlPostfix() + sitePath + "_vti_bin/lists.asmx";
                
                Logger.Info(String.Format("List WS url: {0}", list.Url));
                
                list.Credentials = Utility.GetCredential();

                XmlDocument xmlDoc = new System.Xml.XmlDocument();

                XmlNode ndQuery = xmlDoc.CreateNode(XmlNodeType.Element, "Query", "");
                XmlNode ndViewFields = xmlDoc.CreateNode(XmlNodeType.Element, "ViewFields", "");
                XmlNode ndQueryOptions = xmlDoc.CreateNode(XmlNodeType.Element, "QueryOptions", "");

                ndQueryOptions.InnerXml = "<IncludeMandatoryColumns>FALSE</IncludeMandatoryColumns>" +
                    "<DateInUtc>TRUE</DateInUtc>";
                ndViewFields.InnerXml = "<FieldRef Name='Id' /><FieldRef Name='Title'/>";
                ndQuery.InnerXml = "<Where><And><Gt><FieldRef Name='Field1'/>" +
                    "<Value Type='Number'>5000</Value></Gt><Gt><FieldRef Name='Field2'/>" +
                    "<Value Type='DateTime'>2003-07-03T00:00:00</Value></Gt></And></Where>";

                Logger.Info("GetListItems elott.");
                XmlNode eredmeny = list.GetListItems(docLibName, null, null, ndViewFields, null, ndQueryOptions, null);

                ret.Record = eredmeny.OuterXml.ToString();

            }
            catch (SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException:" +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: path=" + sitePath + " ; folderName=" + docLibName);
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "10001010";
                ret.ErrorMessage = ex.StackTrace.ToString();
                Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error("Kapott parameterek: path=" + sitePath + " ; folderName=" + docLibName);
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromSiteName"></param>
        /// <param name="fromDoclibName"></param>
        /// <param name="fromFoldersName"></param>
        /// <param name="filename"></param>
        /// <param name="toSiteName"></param>
        /// <param name="toDoclibName"></param>
        /// <param name="toFoldersName"></param>
        /// <returns></returns>
        /// 
        
        public Result Copy(string fromSiteName, string fromDoclibName, string fromFoldersName, string filename,
                        string toSiteName, string toDoclibName, string toFoldersName)
        {
            Logger.Info("DocumentService.Copy start.");
            Result ret = new Result();

            string frompath = ((!fromSiteName.EndsWith("/") && !String.IsNullOrEmpty(fromSiteName)) ? fromSiteName + "/" : fromSiteName) +
                            ((!fromDoclibName.EndsWith("/") && !String.IsNullOrEmpty(fromDoclibName)) ? fromDoclibName + "/" : fromDoclibName) +
                            fromFoldersName;

            string topath = ((!toSiteName.EndsWith("/") && !String.IsNullOrEmpty(toSiteName)) ? toSiteName + "/" : toSiteName) +
                            ((!toDoclibName.EndsWith("/") && !String.IsNullOrEmpty(toDoclibName)) ? toDoclibName + "/" : toDoclibName) +
                            toFoldersName;

            ret = Get(fromSiteName, fromDoclibName, fromFoldersName, filename);

            if (!String.IsNullOrEmpty(ret.ErrorCode))
            {
                return ret;
            }

            if (ret.Record == null)
            {
                ret = new Result();
                ret.ErrorCode = "1111111111";
                ret.ErrorMessage = "Copy hiba: a Get fuggveny null tartalommal adta vissza a file-t!";
                Logger.Info("Copy hiba: a Get fuggveny null tartalommal adta vissza a file-t.");
                return ret;
            }

            byte[] cont = (byte[])ret.Record;

            ret = Upload(toSiteName, toDoclibName, toFoldersName, filename, cont);

            return ret;
        }

        public Result CopyInsideSps()
        {
            //
            // ide majd az kell, h egy belso copy legyen
            // meg meg kell irni
            // copy ws
            // az elozo copy-t lehet, h at lehet irni erre vagy egy ujat is lehetne
            //

            Result ret = new Result();
            //SPCopy.Copy cpy = new SPCopy.Copy();

            //cpy.Url = @"http://axis-notev.axis.hu/_vti_bin/copy.asmx";
            //cpy.Credentials = new NetworkCredential("contentumspuser", "123456", "axis");

            //string copySource = @"http://axis-notev.axis.hu/Documents/off1.docx";
            //SPCopy.FieldInformation myFieldInfo = new SPCopy.FieldInformation();
            //SPCopy.FieldInformation[] myFieldInfoArray = { myFieldInfo };
            //byte[] myByteArray;

            //try
            //{
            //    uint myGetUint = cpy.GetItem(copySource, out myFieldInfoArray, out myByteArray);

            //    //foreach (SPCopy.FieldInformation fld in myFieldInfoArray)
            //    //{
            //    //    Console.WriteLine("{0} ({2}) = {1}", fld.DisplayName, fld.Value, fld.InternalName);
            //    //}

            //    int i = 0;
            //    while (i < myFieldInfoArray.Length)
            //    {
            //        SPCopy.FieldInformation fld = (SPCopy.FieldInformation)myFieldInfoArray[i];
            //        if (fld.InternalName.Equals("MetaInfo"))
            //        {
            //            //Console.WriteLine("{0} ({2}) = {1}", fld.DisplayName, fld.Value, fld.InternalName);
            //            //Console.WriteLine("{0}", fld.Value.GetType());

            //            char[] elvalaszto = { '\n' };
            //            string[] parts = fld.Value.ToString().Split(elvalaszto);
            //            Console.WriteLine("Db: {0}", parts.Length);

            //            for (int x = 0; x < parts.Length; x++)
            //                Console.WriteLine("{0} -> {1}", x, parts[x]);
            //        }
            //        i++;
            //    }

            //    Console.WriteLine("OK");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("HIba: {0}", ex.StackTrace.ToString());
            //}


            //Console.ReadLine();
            //return;

            return ret;
        }

        /// <summary>
        /// Egy megadott file atnevezese.
        /// </summary>
        /// <param name="serverUrl">A szerver url-je. http://serverneve/ </param>
        /// <param name="sitesUrlPath">A siteok urlje, ami a szerver neve utan jon. pl: sites/edok/...
        /// A <i>serverUrl</i> parameterrel egyutt ehhez teszi hozza a vti_bin/Lists.asmx-et, az SPS WS-t.</param>
        /// <param name="docLibName">A file-t tartalmazo doclib neve.</param>
        /// <param name="folderPath">A file eleresi utja a docliben belul. Lehet ures is.</param>
        /// <param name="fileName">A file jelenlegi neve.</param>
        /// <param name="fileNewName">Ide jon az uj nev.</param>
        /// <returns>Result ojektum. Ha minden ok, akkor a Record mezobe kerul az uj folder teljes urlje.</returns>
        public Result Rename(String serverUrl, String sitesUrlPath, String docLibName, String folderPath, String fileName, String fileNewName)
        {
            Logger.Info(String.Format("eDocument.SharePoint.DocumentService.Rename indul. serverUrl: {0} - sitesUrlPath: {1} - docLibName: {2} - folderPath: {3} - fileName: {4} - fileNewName: {5}", serverUrl, sitesUrlPath, docLibName, folderPath, fileName, fileNewName));

            Result result = new Result();

            docLibName = Utility.RemoveSlash(docLibName.Trim()).Trim();
            fileName = Utility.RemoveSlash(fileName.Trim()).Trim();
            fileNewName = Utility.RemoveSlash(fileNewName.Trim()).Trim();

            //  namespace manager
            NameTable nt = new NameTable();
            XmlNamespaceManager nsManager = new XmlNamespaceManager(nt);

            try
            {

                using (SPLists.Lists list = new SPLists.Lists())
                {
                    list.Url = ((serverUrl.Trim().EndsWith("/")) ? String.Format("{0}", serverUrl.Trim()) : String.Format("{0}/", serverUrl.Trim()))
                        + ((!sitesUrlPath.EndsWith("/") && !String.IsNullOrEmpty(sitesUrlPath)) ? String.Format("{0}/", sitesUrlPath) : sitesUrlPath)
                        + "_vti_bin/lists.asmx";

                    Logger.Info(String.Format("List url: {0}", list.Url));
                    list.Credentials = Utility.GetCredential();

                    #region a lista adatainak megszerzese

                    String listVersion = String.Empty;
                    XmlNode ndReturn1 = null;

                    try
                    {
                        Logger.Info("a lista adatainak megszerzese indul.");

                        ndReturn1 = list.GetList(docLibName);

                        Logger.Info(String.Format("a lista adatainak megszerzese megvan.: ", ndReturn1.OuterXml.ToString()));
                    }
                    catch (Exception ex1)
                    {
                        Logger.Error(String.Format("Hiba a document library adatainak lekerdezesekor! Message: {0}\nStackTrace: {1}", ex1.Message, ex1.StackTrace));
                        result.ErrorCode = "FRENB0001";
                        result.ErrorMessage = String.Format("Hiba a document library adatainak lekerdezesekor! Message: {0}\nStackTrace: {1}", ex1.Message, ex1.StackTrace);
                        return result;
                    }

                    try
                    {
                        Logger.Info("a lista adatainak feldolgozasa indul.");
                        listVersion = Convert.ToString(ndReturn1.Attributes["Version"].Value);
                        Logger.Info(String.Format("a lista adatainak feldolgozasa ok, kapott verzio: {0}", listVersion));
                    }
                    catch (Exception ex1)
                    {
                        Logger.Error(String.Format("Hiba a document library adatainak feldolgozasakor! Message: {0}\nStackTrace: {1}", ex1.Message, ex1.StackTrace));
                        result.ErrorCode = "FRENB0002";
                        result.ErrorMessage = String.Format("Hiba a document library adatainak feldolgozasakor! Message: {0}\nStackTrace: {1}", ex1.Message, ex1.StackTrace);
                        return result;
                    }

                    #endregion

                    #region az item adatainak megszerzese

                    String itemOwsId = String.Empty;
                    String itemOwsHiddenVersion = String.Empty;
                    XmlNode ndListItems = null;

                    try
                    {
                        Logger.Info("a item adatainak megszerzese indul.");

                        XmlDocument xmlDoc1 = new System.Xml.XmlDocument();

                        XmlNode ndQuery = xmlDoc1.CreateNode(XmlNodeType.Element, "Query", "");
                        XmlNode ndViewFields = xmlDoc1.CreateNode(XmlNodeType.Element, "ViewFields", "");
                        XmlNode ndQueryOptions = xmlDoc1.CreateNode(XmlNodeType.Element, "QueryOptions", "");

                        ndQueryOptions.InnerXml =
                            "<IncludeMandatoryColumns>TRUE</IncludeMandatoryColumns>" +
                            "<Folder />"+
                            "<DateInUtc>TRUE</DateInUtc>";
                        
                        //ndViewFields.InnerXml = "<FieldRef Name='Field1'/><FieldRef Name='Field2'/>";

                        String ows_FileRef = ((!sitesUrlPath.EndsWith("/") && !String.IsNullOrEmpty(sitesUrlPath)) ? String.Format("{0}/", sitesUrlPath) : sitesUrlPath)
                            + ((!docLibName.EndsWith("/") && !String.IsNullOrEmpty(docLibName)) ? String.Format("{0}/", docLibName) : docLibName)
                            + ((!folderPath.EndsWith("/") && !String.IsNullOrEmpty(folderPath)) ? String.Format("{0}/", folderPath) : folderPath)
                            + fileName;

                        ows_FileRef = String.Format("/{0}", Utility.RemoveSlash(ows_FileRef.Trim()).Trim());

                        //ndQuery.InnerXml = String.Format("<Where><Contains><FieldRef Name='FileRef'/>" +
                        //    "<Value Type='String'>{0}</Value></Contains></Where>", ows_FileRef);

                        //  ezt valamiert b@szik figyelembe venni.... (hatalmas sohaj)....   :(((((((((((((
                        ndQuery.InnerXml = String.Format("<Where><Neq><FieldRef Name='ContentType'/>" +
                            "<Value Type='String'>Mappa</Value></Neq></Where>");

                        Logger.Info(String.Format("ndQuery.InnerXml: {0}", ndQuery.InnerXml));

                        //XmlNode ndChkout = list.CheckOutFile(ows_FileRef, true,

                        ndListItems = list.GetListItems(docLibName, null, ndQuery, ndViewFields, null, ndQueryOptions, null);

                        Logger.Info(String.Format("GetListItems eredmeny: {0}", ndListItems.OuterXml.ToString()));

                    }
                    catch (Exception ex2)
                    {
                        Logger.Error(String.Format("Hiba az item adatainak lekerdezesekor! Message: {0}\n StackTrace: {1}", ex2.Message, ex2.StackTrace));
                        result.ErrorCode = "FRENB0002";
                        result.ErrorMessage = String.Format("Hiba az item adatainak lekerdezesekor! Message: {0}\n StackTrace: {1}", ex2.Message, ex2.StackTrace);
                        return result;
                    }


                    try
                    {
                        nsManager.AddNamespace("s", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
                        nsManager.AddNamespace("dt", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882");
                        nsManager.AddNamespace("rs", "urn:schemas-microsoft-com:rowset");
                        nsManager.AddNamespace("z", "#RowsetSchema");
                        nsManager.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/soap/");

                        Logger.Info(String.Format("data ItemCount: {0}", Convert.ToString(ndListItems.SelectSingleNode("//rs:data", nsManager).Attributes["ItemCount"].Value)));

                        if ("1".Equals(Convert.ToString(ndListItems.SelectSingleNode("//rs:data", nsManager).Attributes["ItemCount"].Value)))
                        {
                            XmlNode ff = ndListItems.SelectSingleNode("//z:row", nsManager);

                            itemOwsId = Convert.ToString(ff.Attributes["ows_ID"].Value);
                            itemOwsHiddenVersion = Convert.ToString(ff.Attributes["ows_owshiddenversion"].Value);

                            Logger.Info(String.Format("itemOwsHiddenVersion: {0}; itemOwsId: {1}", itemOwsHiddenVersion, itemOwsId));
                        }
                        else
                        {
                            //Logger.Error(String.Format("Hiba az item adatainak feldolgozasakor: az adatsor ItemCount erteke nem 1!"));
                            //result.ErrorCode = "FRENB0004";
                            //result.ErrorMessage = String.Format("Hiba az item adatainak feldolgozasakor: az adatsor ItemCount erteke nem 1!");
                            //return result;

                            String ows_FileRef = ((!sitesUrlPath.EndsWith("/") && !String.IsNullOrEmpty(sitesUrlPath)) ? String.Format("{0}/", sitesUrlPath) : sitesUrlPath)
                                + ((!docLibName.EndsWith("/") && !String.IsNullOrEmpty(docLibName)) ? String.Format("{0}/", docLibName) : docLibName)
                                + ((!folderPath.EndsWith("/") && !String.IsNullOrEmpty(folderPath)) ? String.Format("{0}/", folderPath) : folderPath)
                                + fileName;

                            //String keresettFolderUrl = String.Format("//z:row[@ows_FileRef='{0}']", ows_ServerUrl);
                            String keresettFolderUrl = String.Format("//z:row[contains(@ows_FileRef,'{0}')]", ows_FileRef);

                            Logger.Info(String.Format("keresettFolderUrl: {0}", keresettFolderUrl));

                            if (ndListItems.SelectNodes(keresettFolderUrl, nsManager).Count == 1)
                            {
                                XmlNode ff = ndListItems.SelectSingleNode(keresettFolderUrl, nsManager);

                                itemOwsId = Convert.ToString(ff.Attributes["ows_ID"].Value);
                                itemOwsHiddenVersion = Convert.ToString(ff.Attributes["ows_owshiddenversion"].Value);

                                Logger.Info(String.Format("itemOwsHiddenVersion: {0}; itemOwsId: {1}", itemOwsHiddenVersion, itemOwsId));
                            }
                            else
                            {
                                Logger.Error(String.Format("Hiba az item adatainak feldolgozasakor: az adatsorok kozott nem talalhato a keresett item!"));
                                Logger.Error(String.Format("(Lehet, hogy ebben az esetben kellene caml queryvel szukiteni a lekerdezest!!!!)"));
                                result.ErrorCode = "FRENB00014";
                                result.ErrorMessage = String.Format("Hiba az item adatainak feldolgozasakor: az adatsorok kozott nem talalhato a keresett item!");
                                return result;
                            }
                        }
                    }
                    catch (Exception ex2)
                    {
                        Logger.Error(String.Format("Hiba az item adatainak feldolgozasakor! Message: {0}\n StackTrace: {1}", ex2.Message, ex2.StackTrace));
                        result.ErrorCode = "FRENB0003";
                        result.ErrorMessage = String.Format("Hiba az item adatainak feldolgozasakor! Message: {0}\n StackTrace: {1}", ex2.Message, ex2.StackTrace);
                        return result;
                    }


                    #endregion

                    #region item rename

                    String fullItemUrl = ((!serverUrl.EndsWith("/") && !String.IsNullOrEmpty(serverUrl)) ? String.Format("{0}/", serverUrl) : serverUrl)
                        + ((!sitesUrlPath.EndsWith("/") && !String.IsNullOrEmpty(sitesUrlPath)) ? String.Format("{0}/", sitesUrlPath) : sitesUrlPath)
                        + ((!docLibName.EndsWith("/") && !String.IsNullOrEmpty(docLibName)) ? String.Format("{0}/", docLibName) : docLibName)
                        + ((!folderPath.EndsWith("/") && !String.IsNullOrEmpty(folderPath)) ? String.Format("{0}/", folderPath) : folderPath)
                        + fileName;

                    Logger.Info(String.Format("osszeallitott fullItemUrl: {0}", fullItemUrl));

                    String strBatch = String.Format("<Method ID='1' Cmd='Update'>" +
                      "<Field Name='ID'>{0}</Field>" +
                      "<Field Name='owshiddenversion'>{1}</Field>" +
                      "<Field Name='FileRef'>{2}</Field>" +
                      "<Field Name='FSObjType'>1</Field>" +
                      "<Field Name='BaseName'>{3}</Field>" +
                    "</Method>", itemOwsId, itemOwsHiddenVersion, fullItemUrl, fileNewName);

                    XmlDocument xmlDoc = new System.Xml.XmlDocument();

                    System.Xml.XmlElement elBatch = xmlDoc.CreateElement("Batch");

                    elBatch.SetAttribute("OnError", "Continue");
                    elBatch.SetAttribute("ListVersion", listVersion);

                    elBatch.InnerXml = strBatch;

                    XmlNode ndReturn = list.UpdateListItems(docLibName, elBatch);

                    Logger.Info(String.Format("UpdateListItems eredmeny: {0}", ndReturn.OuterXml.ToString()));

                    //  hibakereses
                    //.... TODO

                    String errorCode = ndReturn.SelectSingleNode("//a:ErrorCode", nsManager).InnerText;
                    String errorMessage = String.Empty;

                    if (!"0x00000000".Equals(errorCode))
                    {
                        Logger.Error(String.Format("Kapott error: {0} - {1}", errorCode, errorMessage));
                        errorMessage = ndReturn.SelectSingleNode("//a:ErrorText", nsManager).InnerText;

                        result.ErrorCode = "FREN0009";
                        result.ErrorMessage = String.Format("Hibas eredmenyt adott vissza az SPS updateListItem! Code: {0}\nSzoveg: {1}", errorCode, errorMessage);
                        return result;
                    }

                    result.Record = ((serverUrl.Trim().EndsWith("/")) ? String.Format("{0}", serverUrl.Trim()) : serverUrl.Trim())
                        + ((!sitesUrlPath.EndsWith("/") && !String.IsNullOrEmpty(sitesUrlPath)) ? String.Format("{0}/", sitesUrlPath) : sitesUrlPath)
                        + ((!docLibName.EndsWith("/") && !String.IsNullOrEmpty(docLibName)) ? String.Format("{0}/", docLibName) : docLibName)
                        + ((!folderPath.EndsWith("/") && !String.IsNullOrEmpty(folderPath)) ? String.Format("{0}/", folderPath) : folderPath)
                        + (fileNewName);

                    #endregion

                }

            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba az item rename futasa soran! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                result.ErrorCode = "FREN0001";
                result.ErrorMessage = String.Format("Hiba az item rename futasa soran! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            }


            return result;
        } //  Rename


    } //class
}
