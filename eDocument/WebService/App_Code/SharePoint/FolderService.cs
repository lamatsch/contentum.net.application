using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eBusinessDocuments;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Net;
using System.Xml;

namespace Contentum.eDocument.SharePoint
{

    class FolderService
    {

        public const string Header = "$Header: FolderService.cs, 7, 2010.02.17 13:52:29, Varsanyi P?ter$";
        public const string Version = "$Revision: 7$";
        private static readonly object _sync = new object();

        public FolderService()
        {
            Logger.Info(String.Format("DocumentService.SharePoint.FolderService start.- {0} {1}", Header, Version));
        }

        /// <summary>
        /// A megadott lista jellemzoinek letoltese.
        /// </summary>
        /// <param name="siteName">site.</param>
        /// <param name="listName">list.</param>
        /// <returns>Result - hiba vagy a recordban XmlNode</returns>
        /// 
        
        public Result GetList(string siteName, string listName)
        {
            Logger.Info("FolderService.GetList start");
            Result ret = new Result();

            if (String.IsNullOrEmpty(listName))
            {
                // TODO:
                Logger.Error("parameterezes hiba!");
                ret.ErrorCode = "300002";
                return ret;
            }

            String SPUrl = Utility.GetSharePointUrl();

            try
            {

                SPLists.Lists list = new SPLists.Lists();
                list.Url = SPUrl + Utility.SharePointUrlPostfix()
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + "_vti_bin/lists.asmx";
                list.Credentials = Utility.GetCredential(); //new NetworkCredential("varsanyi.peter", "", "axis");

                Logger.Info("list.GetList elott, url: " + list.Url);
                XmlNode eredmeny = list.GetList(listName);

                ret.Record = eredmeny;

            }
            catch (SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException:");
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace);
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ;listName=" + listName);
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "10001010";
                ret.ErrorMessage = ex.StackTrace.ToString();
                Logger.Error("Exception! Parameterek: siteName=" + siteName + " ;listName=" + listName);
                Logger.Error(ex.ToString());
            }

            return ret;
        }

        /// <summary>
        /// Verziokezeles be van-e kacspolva a megadott folderen.
        /// </summary>
        /// <param name="siteName">site.</param>
        /// <param name="listName">list.</param>
        /// <returns>Result - hiba, vagy a rekordban true/false ertek.</returns>
        /// 
        
        public Result IsVersioning(string siteName, string listName)
        {
            Logger.Info("FolderService.IsVersioning start");
            Result ret = new Result();

            Logger.Info("GetList elott");
            ret = GetList(siteName, listName);
            bool eredmeny = false;

            if (String.IsNullOrEmpty(ret.ErrorCode))
            {
                if (ret.Record != null)
                {
                    XmlNode node = (XmlNode)ret.Record;
                    foreach (XmlAttribute a in node.Attributes)
                    {
                        if (a.Name.ToString().Equals("EnableVersioning"))
                        {
                            if (a.Value.ToString().Equals("True"))
                                eredmeny = true;
                        }

                    }
                    ret.Record = eredmeny;
                }
                else
                {
                    ret = new Result();
                    ret.ErrorCode = "10000001";
                    ret.ErrorMessage = "Ures XmlNode-ot adott vissza a GetList!";
                    Logger.Error("Ures XmlNode-ot adott vissza a GetList!");
                }

            }

            return ret;
        }

        /// <summary>
        /// Ez jo.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="folderName"></param>
        /// <param name="tf"></param>
        /// <returns></returns>
        /// 
        ///  MajorWithMinorVersionsLimit="0"
        ///  RequireCheckout="False"

        
        public Result SetVersioning(string siteName, string listName, bool tf)
        {
            Logger.Info("FolderService.SetVersioning start");
            Result ret = new Result();

            Logger.Info("GetList elott");
            ret = GetList(siteName, listName);

            if (String.IsNullOrEmpty(ret.ErrorCode))
            {
                if (ret.Record != null)
                {
                    XmlNode node = (XmlNode)ret.Record;

                    string ndVersion = node.Attributes["Version"].Value;

                    XmlDocument xmlDoc = new System.Xml.XmlDocument();

                    XmlNode ndProperties = xmlDoc.CreateNode(XmlNodeType.Element, "List", "");

                    XmlAttribute att = xmlDoc.CreateAttribute("EnableVersioning");
                    att.Value = tf ? "True" : "False";
                    XmlAttribute att2 = xmlDoc.CreateAttribute("EnableMinorVersion");
                    att2.Value = tf ? "True" : "False";
                    XmlAttribute att3 = xmlDoc.CreateAttribute("RequireCheckout");
                    att3.Value = tf ? "True" : "False";

                    ndProperties.Attributes.Append(att);
                    ndProperties.Attributes.Append(att2);
                    ndProperties.Attributes.Append(att3);

                    Logger.Info("valtoztato node osszeallitva: " + ndProperties.OuterXml.ToString());
                    Logger.Info("verzioja: " + ndVersion);

                    try
                    {
                        String SPUrl = Utility.GetSharePointUrl();

                        if (!SPUrl.EndsWith("/")) { SPUrl += "/"; }

                        SPLists.Lists list = new SPLists.Lists();
                        list.Url = SPUrl + Utility.SharePointUrlPostfix()
                            + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                            + "_vti_bin/lists.asmx";
                        list.Credentials = Utility.GetCredential();

                        Logger.Info("list.UpdateList hivasa");

                        XmlNode ndReturn = list.UpdateList(listName, ndProperties, null, null, null, ndVersion);

                        ret.Record = ndReturn.OuterXml.ToString();
                        //MessageBox.Show(ndReturn.OuterXml);
                    }
                    catch (SoapException spe)
                    {
                        ret.ErrorCode = "1111111SOAP";
                        ret.ErrorMessage = spe.ToString();
                        Logger.Error("SoapException:");
                        String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace);
                        Logger.Error("Kapott parameterek: siteName=" + siteName + " ;listName=" + listName);
                    }
                    catch (Exception ex)
                    {
                        ret = new Result();
                        ret.ErrorCode = "10000003";
                        ret.ErrorMessage = ex.StackTrace.ToString();
                        Logger.Error("Exception! Parameterek: siteName=" + siteName + " ;listName=" + listName);
                        Logger.Error(ex.ToString());
                    }


                }
                else
                {
                    ret = new Result();
                    ret.ErrorCode = "10000001";
                    ret.ErrorMessage = "Ures XmlNode-ot adott vissza a GetList!";
                    Logger.Error("Exception! Parameterek: siteName=" + siteName + " ;listName=" + listName);
                    Logger.Error("Ures XmlNode-ot adott vissza a GetList!");
                }

            }

            return ret;
        }


        /// <summary>
        /// Ez jo
        /// </summary>
        /// <param name="path">Site neve</param>
        /// <param name="listName">Doclib, List... neve</param>
        /// <param name="folderName">konyvtar vagy konyvtarak neve. Mindig az utolsot hozza letre.</param>
        /// <returns>Result. </returns>
        /// 
        
        public Result CreateFolder(string siteName, string listName, string folderNames)
        {
            Logger.Info("FolderService.CreateFolder start");
            Logger.Info("Kapott paramok: siteName=" + siteName + "; listName=" + listName + "; folderNames=" + folderNames);
            Result ret = new Result();

            if (String.IsNullOrEmpty(folderNames) ||
                String.IsNullOrEmpty(listName))
            {
                // TODO:
                Logger.Error("parameterezes hiba!");
                ret.ErrorCode = "300002";
                return ret;
            }

            String SPUrl = Utility.GetSharePointUrl();

            if (IsExistsFolder(siteName, listName, folderNames))
            {
                ret.Record = SPUrl + Utility.SharePointUrlPostfix() + Utility.SharePointUrlPostfixDokumentumtarSiteUrl() 
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                    + folderNames;
                Logger.Error("Mar letezik: " + ret.Record);
                return ret;
            }

            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Xml.XmlTextWriter xw = new System.Xml.XmlTextWriter(sw);
            xw.WriteStartDocument();

            xw.WriteStartElement("Batch");
            xw.WriteAttributeString("OnError", "Return");
            xw.WriteAttributeString("RootFolder", "");

            xw.WriteStartElement("Method");
            xw.WriteAttributeString("ID", Guid.NewGuid().ToString("n"));
            xw.WriteAttributeString("Cmd", "New");

            xw.WriteStartElement("Field");
            xw.WriteAttributeString("Name", "ID");
            xw.WriteString("New");
            xw.WriteEndElement(); // Field end

            xw.WriteStartElement("Field");
            xw.WriteAttributeString("Name", "FSObjType");
            xw.WriteString("1");  // 1= folder
            xw.WriteEndElement(); // Field end

            xw.WriteStartElement("Field");
            xw.WriteAttributeString("Name", "BaseName");
            xw.WriteString(folderNames);
            xw.WriteEndElement(); // Field end

            xw.WriteEndElement(); // Method end
            xw.WriteEndElement(); // Batch end

            xw.WriteEndDocument();

            System.Xml.XmlDocument batchElement = new System.Xml.XmlDocument();
            batchElement.LoadXml(sw.GetStringBuilder().ToString());

            try
            {

                SPLists.Lists listService = new SPLists.Lists();
                listService.Url = SPUrl + Utility.SharePointUrlPostfix() 
                        + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                        + @"_vti_bin/lists.asmx";
                Logger.Info(String.Format("listService.Url: {0}", listService.Url));

                listService.Credentials = Utility.GetCredential(); // new System.Net.NetworkCredential("someuser", "somepassword", "somedomain");

                Logger.Info("listService.UpdateListItems elott");

                System.Xml.XmlNode result = listService.UpdateListItems(listName, batchElement);

                if (result.FirstChild.ChildNodes[0].InnerText.ToString().Equals(""))
                {
                    ret.ErrorCode = result.FirstChild.ChildNodes[0].InnerText.ToString();
                    ret.ErrorMessage = result.FirstChild.ChildNodes[1].InnerText.ToString();
                    Logger.Error("Error!");
                    Logger.Error(result.FirstChild.ChildNodes[0].InnerText.ToString());
                    Logger.Error(result.FirstChild.ChildNodes[1].InnerText.ToString());

                }
                else
                {
                    ret.Record = SPUrl
                        + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                        + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                        + folderNames;
                }

            }
            catch (SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException:"+
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error(String.Format("Kapott parameterek:  siteName={0}, listName={1},  folderNames={2}", siteName, listName, folderNames));
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "823777774";
                ret.ErrorMessage = ex.StackTrace.ToString();
                Logger.Error("Excetion!!!!");
                Logger.Error(ex.StackTrace.ToString() + "\n" + ex.Message.ToString());
                Logger.Error(String.Format("Kapott parameterek:  siteName={0}, listName={1},  folderNames={2}", siteName, listName, folderNames));
            }

            return ret;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteName"></param>
        /// <param name="listName"></param>
        /// <param name="folderNames"></param>
        /// <returns></returns>
        public Result CreateLinksListFolder(string siteName, string listName, string folderNames)
        {
            Logger.Info("FolderService.CreateLinksListFolder start");
            Logger.Info("Kapott paramok: siteName=" + siteName + "; listName=" + listName + "; folderNames=" + folderNames);
            Result ret = new Result();

            if (String.IsNullOrEmpty(folderNames) ||
                String.IsNullOrEmpty(listName))
            {
                // TODO:
                Logger.Error("parameterezes hiba!");
                ret.ErrorCode = "300002";
                return ret;
            }

            String SPUrl = Utility.GetSharePointUrl();

            #region Folder letezes vizsgalata

            SPLists.Lists l = new SPLists.Lists();
            l.Url = SPUrl + Utility.SharePointUrlPostfix() + Utility.SharePointUrlPostfixDokumentumtarSiteUrl() +
                    ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName) +
                    "_vti_bin/lists.asmx";
            l.Credentials = Utility.GetCredential();

            Logger.Info(String.Format("url: {0}", l.Url));

            XmlDocument xmlDoc = new System.Xml.XmlDocument();

            XmlNode ndQuery = xmlDoc.CreateNode(XmlNodeType.Element, "Query", "");
            XmlNode ndViewFields =
                xmlDoc.CreateNode(XmlNodeType.Element, "ViewFields", "");
            XmlNode ndQueryOptions =
                xmlDoc.CreateNode(XmlNodeType.Element, "QueryOptions", "");

            ndQueryOptions.InnerXml =
                "<IncludeMandatoryColumns>FALSE</IncludeMandatoryColumns>" +
                "<DateInUtc>TRUE</DateInUtc>" +
                "<Folder></Folder>";

            bool folderLetezik = false;

            try
            {
                XmlNamespaceManager xmlnsm = new XmlNamespaceManager(new NameTable());
                xmlnsm.AddNamespace("s", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
                xmlnsm.AddNamespace("dt", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882");
                xmlnsm.AddNamespace("rs", "urn:schemas-microsoft-com:rowset");
                xmlnsm.AddNamespace("z", "#RowsetSchema");
                xmlnsm.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/soap/");

                Logger.Info("FolderService.CreateLinksListFolder folder letezes ellenorzese GetLitItems elott.");
                XmlNode x = l.GetListItems(listName, null, ndQuery, ndViewFields, null, ndQueryOptions, null);

                int i = 0;

                XmlNodeList ndls = x.SelectNodes("//z:row[@ows_ContentType='Mappa']", xmlnsm);

                Logger.Info(String.Format("FolderService.CreateLinksListFolder folder letezes ellenorzese: talalt folderek szama: {0}", ndls.Count));
                while (i < ndls.Count && folderLetezik == false)
                {

                    XmlNode nd = ndls[i];
                    string ows_Id = Convert.ToString(nd.Attributes["ows_ID"].Value);

                    string ows_FileRef = String.Format("{0};#{3}{4}Lists/{1}/{2}", ows_Id, listName
                        , ((folderNames.EndsWith("/")) ? folderNames.Substring(0,folderNames.Length-1) : folderNames)
                        , Utility.SharePointUrlPostfix()
                        , ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                        );

                    if (Convert.ToString(nd.Attributes["ows_FileRef"].Value).Equals(ows_FileRef))
                    {
                        folderLetezik = true;
                        Logger.Info(String.Format("Megvan, letezik.: {0}", ows_FileRef));
                    }

                    i++;
                }

            }
            catch (SoapException spe)
            {
                ret.ErrorCode = "1111133SOAP";
                ret.ErrorMessage = "SoapException (a list folder letezesenek vizsgalata soran):"+
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace);
                Logger.Error("SoapException (a folder letezesenek vizsgalata soran):"+
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error(String.Format("Kapott parameterek:  siteName={0}, listName={1},  folderNames={2}", siteName, listName, folderNames));
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "823777745";
                ret.ErrorMessage = "Excetion a list folder letezesenek vizsgalata soran\n"+ex.StackTrace.ToString();
                Logger.Error("Excetion a folder letezesenek vizsgalata soran!!!!");
                Logger.Error(ex.StackTrace.ToString() + "\n" + ex.Message.ToString());
            }

            #endregion

            #region Ha letezik a folder, akkor vissza

            if (folderLetezik)
            {
                ret.Record = SPUrl + Utility.SharePointUrlPostfix()
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                    + folderNames;
                Logger.Error("Mar letezik: " + ret.Record);
                return ret;
            }

            #endregion

            //if (IsExistsFolder(siteName, listName, folderNames))
            //{
            //    ret.Record = SPUrl + Utility.SharePointUrlPostfix()
            //        + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
            //        + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
            //        + folderNames;
            //    Logger.Error("Mar letezik: " + ret.Record);
            //    return ret;
            //}

            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Xml.XmlTextWriter xw = new System.Xml.XmlTextWriter(sw);
            xw.WriteStartDocument();

            xw.WriteStartElement("Batch");
            xw.WriteAttributeString("OnError", "Return");
            xw.WriteAttributeString("RootFolder", "");

            xw.WriteStartElement("Method");
            xw.WriteAttributeString("ID", Guid.NewGuid().ToString("n"));
            xw.WriteAttributeString("Cmd", "New");

            xw.WriteStartElement("Field");
            xw.WriteAttributeString("Name", "ID");
            xw.WriteString("New");
            xw.WriteEndElement(); // Field end

            xw.WriteStartElement("Field");
            xw.WriteAttributeString("Name", "FSObjType");
            xw.WriteString("1");  // 1= folder
            xw.WriteEndElement(); // Field end

            xw.WriteStartElement("Field");
            xw.WriteAttributeString("Name", "BaseName");
            xw.WriteString(folderNames);
            xw.WriteEndElement(); // Field end

            xw.WriteEndElement(); // Method end
            xw.WriteEndElement(); // Batch end

            xw.WriteEndDocument();

            System.Xml.XmlDocument batchElement = new System.Xml.XmlDocument();
            batchElement.LoadXml(sw.GetStringBuilder().ToString());

            try
            {

                SPLists.Lists listService = new SPLists.Lists();
                listService.Url = SPUrl + Utility.SharePointUrlPostfix() + Utility.SharePointUrlPostfixDokumentumtarSiteUrl() 
                        + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                        + @"_vti_bin/lists.asmx";
                listService.Credentials = Utility.GetCredential(); // new System.Net.NetworkCredential("someuser", "somepassword", "somedomain");

                Logger.Info("listService.UpdateListItems elott");

                System.Xml.XmlNode result = listService.UpdateListItems(listName, batchElement);

                if (result.FirstChild.ChildNodes[0].InnerText.ToString().Equals(""))
                {
                    ret.ErrorCode = result.FirstChild.ChildNodes[0].InnerText.ToString();
                    ret.ErrorMessage = result.FirstChild.ChildNodes[1].InnerText.ToString();
                    Logger.Error("Error!");
                    Logger.Error(result.FirstChild.ChildNodes[0].InnerText.ToString());
                    Logger.Error(result.FirstChild.ChildNodes[1].InnerText.ToString());

                }
                else
                {
                    ret.Record = SPUrl
                        + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                        + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                        + folderNames;
                }

            }
            catch (SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException:"+
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error(String.Format("Kapott parameterek:  siteName={0}, listName={1},  folderNames={2}", siteName, listName, folderNames));
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "823777774";
                ret.ErrorMessage = ex.StackTrace.ToString();
                Logger.Error("Excetion!!!!");
                Logger.Error(ex.StackTrace.ToString() + "\n" + ex.Message.ToString());
            }

            return ret;

        }


        /// <summary>
        /// Lista letrehozasa. Teljes utat kell megadni!
        /// Ez jo.
        /// </summary>
        /// <param name="path">csak a site-ot kell megadni! lehet ures is.</param>
        /// <param name="listName">doclib neve</param>
        /// <param name="description">doclib leirasa</param>
        /// <returns>Result - ssss</returns>

        public Result CreateLinksList(
             string siteName
            ,string listName
            ,string description)
        {
            Logger.Info("FolderService.CreateLinksList start");
            Result _ret = new Result();

            if (String.IsNullOrEmpty(listName))
            {
                Logger.Info("parameterezes hiba!");
                _ret.ErrorCode = "320002";
                return _ret;
            }

            string SPUrl = Utility.GetSharePointUrl();

            #region Letezik-e a lista

            Logger.Info("Lista letezik-e ellenorzes.");

            SPLists.Lists list1 = new SPLists.Lists();
            list1.Url = SPUrl + Utility.SharePointUrlPostfix() + Utility.SharePointUrlPostfixDokumentumtarSiteUrl() 
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + @"_vti_bin/lists.asmx";
            list1.PreAuthenticate = true;
            list1.Credentials = Utility.GetCredential();

            Logger.Info(String.Format("Lista beallitasa. Url: {0}", list1.Url));

            try
            {
                XmlNode xn = list1.GetListCollection();

                XmlNamespaceManager nscpm = new XmlNamespaceManager(new NameTable());
                nscpm.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/soap/");

                XmlNodeList linkesListak = xn.SelectNodes("/a:List[@ServerTemplate='103']", nscpm);

                bool megvan = false;
                int y = 0;
                while (y < linkesListak.Count && !megvan)
                {
                    XmlNode item = linkesListak[y];

                    if (Convert.ToString(item.Attributes["Title"]).Equals(listName))
                    {
                        megvan = true;
                    }

                    y++;
                }

                if (megvan)
                {
                    _ret.Record = SPUrl + Utility.SharePointUrlPostfix() + Utility.SharePointUrlPostfixDokumentumtarSiteUrl() 
                        + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                        + "Lists/" + listName;
                    Logger.Info("Mar letezik: " + _ret.Record);
                    return _ret;
                }

            }
            catch (SoapException soae)
            {
                _ret.ErrorCode = "1111112SOAP";
                _ret.ErrorMessage = soae.ToString();
                Logger.Error("SoapException:");
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", soae.Message, soae.Detail.InnerText, soae.StackTrace);
                Logger.Error(String.Format("Kapott parameterek:  siteName={0}, documentLibraryName={1},  description={2}", siteName, listName, description));
                return _ret;
            }
            catch (Exception ex)
            {
                _ret.ErrorCode = "10001002";
                _ret.ErrorMessage = ex.Message;
                Logger.Error("Exception!");
                Logger.Error(ex.Message.ToString() + "\n" + ex.StackTrace.ToString());
                return _ret;
            }

            #endregion

            try
            {
                SPLists.Lists list = new SPLists.Lists();
                list.Url = SPUrl + Utility.SharePointUrlPostfix() + Utility.SharePointUrlPostfixDokumentumtarSiteUrl() 
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + @"_vti_bin/lists.asmx";
                list.PreAuthenticate = true;
                list.Credentials = Utility.GetCredential();

                Logger.Info("list.AddList elott");

                // l�trehoz�s

                XmlNode ndList = list.AddList(listName, description, 103);

                _ret.Record = SPUrl + Utility.SharePointUrlPostfix() + Utility.SharePointUrlPostfixDokumentumtarSiteUrl() 
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + "Lists/" + listName; // ndList.OuterXml.ToString();

                return _ret;
            }
            catch (SoapException spe)
            {
                _ret.ErrorCode = "1111111SOAP";
                _ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException:");
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace);
                Logger.Error(String.Format("Kapott parameterek:  siteName={0}, listName={1},  description={2}", siteName, listName, description));
                return _ret;
            }
            catch (Exception spe)
            {
                _ret.ErrorCode = "10001001";
                _ret.ErrorMessage = spe.Message;
                Logger.Error("Exception!");
                Logger.Error(spe.Message.ToString() + "\n" + spe.StackTrace.ToString());
                return _ret;
            }

        }

        /// <summary>
        /// Doklib letrehozasa. Teljes utat kell megadni!
        /// Ez jo.
        /// </summary>
        /// <param name="path">csak a site-ot kell megadni! lehet ures is.</param>
        /// <param name="documentLibraryName">doclib neve</param>
        /// <param name="description">doclib leirasa</param>
        /// <returns>Result - ssss</returns>
        
        public Result CreateDocumentLibrary(
            string siteName, string documentLibraryName,
            string description)
        {
            Logger.Info("FolderService.CreateDocumentLibrary start");
            Result _ret = new Result();

            if (String.IsNullOrEmpty(documentLibraryName))
            {
                Logger.Info("parameterezes hiba!");
                _ret.ErrorCode = "320002";
                return _ret;
            }

            string SPUrl = Utility.GetSharePointUrl();

            if (IsExistsDocumentLibrary(siteName, documentLibraryName))
            {
                _ret.Record = SPUrl + Utility.SharePointUrlPostfix() + Utility.SharePointUrlPostfixDokumentumtarSiteUrl() 
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + documentLibraryName;
                Logger.Info("Mar letezik: " + _ret.Record);
                return _ret;
            }

            try
            {
                SPLists.Lists list = new SPLists.Lists();
                list.Url = SPUrl + Utility.SharePointUrlPostfix()
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + @"_vti_bin/lists.asmx";
                list.PreAuthenticate = true;
                list.Credentials = Utility.GetCredential();

                Logger.Info(String.Format("list.AddList elott. documentLibraryName: {0} - description: {1}", documentLibraryName, description));

                // l�trehoz�s
                // lock: ne j�jj�n t�bbsz�r l�tre
                lock (_sync)
                {
                    if (!IsExistsDocumentLibrary(siteName, documentLibraryName))
                    {
                        XmlNode ndList = list.AddList(documentLibraryName, description, 101);
                    }
                }

                _ret.Record = SPUrl + Utility.SharePointUrlPostfix() + Utility.SharePointUrlPostfixDokumentumtarSiteUrl() 
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + documentLibraryName; // ndList.OuterXml.ToString();
                
                //// rendes n�v ad�sa

                //if (!Utility.ConvertToUrl(documentLibraryName).Equals(documentLibraryName))
                //{
                //    Logger.Info("Rendes nev megadasa - updateListhez lekerdezes.");

                //    ndList = list.GetList(Utility.ConvertToUrl(documentLibraryName));

                //    XmlNode ndVersion = ndList.Attributes["Version"];

                //    XmlDocument xmlDoc = new System.Xml.XmlDocument();

                //    XmlNode ndDeleteFields = xmlDoc.CreateNode(XmlNodeType.Element,
                //        "Fields", "");
                //    XmlNode ndProperties = xmlDoc.CreateNode(XmlNodeType.Element, "List",
                //        "");
                //    XmlAttribute ndTitleAttrib =
                //        (XmlAttribute)xmlDoc.CreateNode(XmlNodeType.Attribute,
                //        "Title", "");

                //    XmlNode ndNewFields = xmlDoc.CreateNode(XmlNodeType.Element,
                //        "Fields", "");
                //    XmlNode ndUpdateFields = xmlDoc.CreateNode(XmlNodeType.Element,
                //        "Fields", "");

                //    ndTitleAttrib.Value = Utility.GetEkezetesBetukAtalakitasa(documentLibraryName);

                //    ndProperties.Attributes.Append(ndTitleAttrib);

                //    Logger.Info("Rendes nev megadasa - updateList futtatasa.");

                //    // ndNewFields, ndUpdateFields, ndDeleteFields,
                //    XmlNode ndReturn =
                //          list.UpdateList(documentLibraryName,
                //          ndProperties, null, null, null,
                //          ndVersion.Value);

                //}
                return _ret;
            }
            catch (SoapException spe)
            {
                _ret.ErrorCode = "1111111SOAP";
                _ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException:");
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace);
                Logger.Error(String.Format("Kapott parameterek:  siteName={0}, documentLibraryName={1},  description={2}", siteName, documentLibraryName, description));
                return _ret;
            }
            catch (Exception spe)
            {
                _ret.ErrorCode = "10001001";
                _ret.ErrorMessage = spe.Message;
                Logger.Error("Exception!");
                Logger.Error(spe.Message.ToString() + "\n" + spe.StackTrace.ToString());
                return _ret;
            }

        }

        
        public bool IsExistsDocumentLibrary(string siteName, string doclibName)
        {
            Logger.Info(String.Format("IsExistsDocumentLibrary indul. (siteName: {0}, doclibName: {1}", siteName, doclibName));
            bool ret = false;

            String SPUrl = Utility.GetSharePointUrl();

            SPSiteDatas.SiteData sd = new SPSiteDatas.SiteData();
            sd.Url = SPUrl + Utility.SharePointUrlPostfix()  
                + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                + "_vti_bin/sitedata.asmx";
            sd.Credentials = Utility.GetCredential();

            Logger.Info(String.Format("Url: {0}", sd.Url));

            SPSiteDatas._sListMetadata lstMetaData;
            SPSiteDatas._sProperty[] lstFields;

            try
            {
                Logger.Info("sd.GetList elott.");
                sd.GetList(doclibName, out lstMetaData, out lstFields);
                Logger.Info("sd.GetList utan: tehat van.");
                ret = true;
            }
            catch (Exception ex)
            {
                Logger.Info("sd.GetList exception: tehat nincs.");
                ret = false;
            }

            //Console.WriteLine(lstMetaData.Title + " :: " + lstMetaData.DefaultViewUrl);

            //foreach (SPSiteDatas._sProperty field in lstFields)
            //{
            //    Console.WriteLine(field.Title + " :: " + field.Name + " :: " + field.Type);
            //}

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteName"></param>
        /// <param name="listName"></param>
        /// <param name="folderNames"></param>
        /// <returns></returns>
        public bool IsExistsFolder(string siteName, string listName, string folderNames)
        {
            Logger.Info(String.Format("IsExistsFolder({0},{1},{2}) indul.",siteName, listName, folderNames));

            bool ret = false;

            if (String.IsNullOrEmpty(folderNames)) return ret;

            ListUtilService lisu = new ListUtilService();

            Logger.Info(String.Format("lisu.GetListItems elott."));
            
            Result ret2 = lisu.GetListItems(siteName, listName, folderNames, null, null, null, null, null, null);

            if (!String.IsNullOrEmpty(ret2.ErrorCode))
            {
                Logger.Error(String.Format("GetListItems hibat adott vissza!!!!"));
                Logger.Error(String.Format("ErrorCode: {0}\nErrorMessage: {1}",ret2.ErrorCode, ret2.ErrorMessage));
                ret = false;
            }
            else
            {
                Logger.Info(String.Format("Xmlnodda konvertalas elott."));

                XmlDocument xxx = new XmlDocument();

                try
                {
                    xxx.LoadXml(Convert.ToString(ret2.Record));
                }
                catch (Exception exc)
                {
                    Logger.Error(String.Format("GetListItems altal adott eredmeny xml-e konvertalas hiba!!!!"));
                    Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                    ret = false;
                }

                if (lisu.GetItemCount(xxx) != 0)
                {

                    System.Xml.XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(xxx.OuterXml.ToString());

                    XmlNodeList nodeList = xmldoc.GetElementsByTagName("z:row");

                    string keresett = ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                        + ((!listName.EndsWith("/") && !String.IsNullOrEmpty(listName)) ? listName + "/" : listName)
                        + folderNames;
                    int i = 0;
                    while (i < nodeList.Count && ret == false)
                    {
                        XmlNode xe = nodeList.Item(i);
                        string s = xe.Attributes["ows_FileRef"].InnerText.ToString();
                        string owdId = xe.Attributes["ows_ID"].InnerText.ToString();
                        if (s.Equals(owdId + ";#" + keresett)) ret = true;
                        i++;
                    }
                }
            }

            return ret;

        }

        /// <summary>
        /// Folder atnevezese.
        /// </summary>
        /// <param name="serverUrl">A szerver url-je. http://serverneve/ </param>
        /// <param name="sitesUrlPath">A siteok urlje, ami a szerver neve utan jon. pl: sites/edok/...
        /// A <i>serverUrl</i> parameterrel egyutt ehhez teszi hozza a vti_bin/Lists.asmx-et, az SPS WS-t.</param>
        /// <param name="docLibName">A folder tartalmazo doclib neve.</param>
        /// <param name="folderPath">A folder eleresi utja a docliben belul. Lehet ures is.</param>
        /// <param name="folderName">A folder jelenlegi neve.</param>
        /// <param name="folderNewName">Ide jon az uj nev.</param>
        /// <returns>Result ojektum. Ha minden ok, akkor a Record mezobe kerul az uj folder teljes urlje.</returns>
        public Result Rename(String serverUrl, String sitesUrlPath, String docLibName, String folderPath, String folderName, String folderNewName)
        {
            Logger.Info(String.Format("eDocument.SharePoint.FolderService.Rename indul. serverUrl: {0} - sitesUrlPath: {1} - docLibName: {2} - folderPath: {3} - folderName: {4} - folderNewName: {5}", serverUrl, sitesUrlPath, docLibName, folderPath, folderName, folderNewName));

            Result result = new Result();

            docLibName = Utility.RemoveSlash(docLibName.Trim()).Trim();
            folderName = Utility.RemoveSlash(folderName.Trim()).Trim();
            folderNewName = Utility.RemoveSlash(folderNewName.Trim()).Trim();

            //  namespace manager
            NameTable nt = new NameTable();
            XmlNamespaceManager nsManager = new XmlNamespaceManager(nt);

            try
            {

                using (SPLists.Lists list = new SPLists.Lists())
                {
                    list.Url = ((serverUrl.Trim().EndsWith("/")) ? String.Format("{0}", serverUrl.Trim()) : String.Format("{0}/", serverUrl.Trim()))
                        + ((!sitesUrlPath.EndsWith("/") && !String.IsNullOrEmpty(sitesUrlPath)) ? String.Format("{0}/", sitesUrlPath) : sitesUrlPath)
                        + "_vti_bin/lists.asmx";

                    Logger.Info(String.Format("List url: {0}", list.Url));
                    list.Credentials = Utility.GetCredential();

                    #region a lista adatainak megszerzese

                    String listVersion = String.Empty;
                    XmlNode ndReturn1 = null;

                    try
                    {
                        Logger.Info("a lista adatainak megszerzese indul.");

                        ndReturn1 = list.GetList(docLibName);

                        Logger.Info(String.Format("a lista adatainak megszerzese megvan.: ", ndReturn1.OuterXml.ToString()));
                    }
                    catch (Exception ex1)
                    {
                        Logger.Error(String.Format("Hiba a document library adatainak lekerdezesekor! Message: {0}\nStackTrace: {1}", ex1.Message, ex1.StackTrace));
                        result.ErrorCode = "FRENB0001";
                        result.ErrorMessage = String.Format("Hiba a document library adatainak lekerdezesekor! Message: {0}\nStackTrace: {1}", ex1.Message, ex1.StackTrace);
                        return result;
                    }

                    try 
                    {
                        Logger.Info("a lista adatainak feldolgozasa indul.");
                        listVersion = Convert.ToString(ndReturn1.Attributes["Version"].Value);
                        Logger.Info(String.Format("a lista adatainak feldolgozasa ok, kapott verzio: {0}", listVersion));
                    }
                    catch (Exception ex1)
                    {
                        Logger.Error(String.Format("Hiba a document library adatainak feldolgozasakor! Message: {0}\nStackTrace: {1}", ex1.Message, ex1.StackTrace));
                        result.ErrorCode = "FRENB0002";
                        result.ErrorMessage = String.Format("Hiba a document library adatainak feldolgozasakor! Message: {0}\nStackTrace: {1}", ex1.Message, ex1.StackTrace);
                        return result;
                    }

                    #endregion

                    #region a folder adatainak megszerzese

                    String folderOwsId = String.Empty;
                    String folderOwsHiddenVersion = String.Empty;
                    XmlNode ndListItems = null;

                    try
                    {
                        Logger.Info("a folder adatainak megszerzese indul.");

                        XmlDocument xmlDoc1 = new System.Xml.XmlDocument();

                        XmlNode ndQuery = xmlDoc1.CreateNode(XmlNodeType.Element,"Query","");
                        XmlNode ndViewFields = xmlDoc1.CreateNode(XmlNodeType.Element,"ViewFields","");
                        XmlNode ndQueryOptions = xmlDoc1.CreateNode(XmlNodeType.Element,"QueryOptions","");

                        ndQueryOptions.InnerXml = 
                            "<IncludeMandatoryColumns>TRUE</IncludeMandatoryColumns>" + 
                            "<DateInUtc>TRUE</DateInUtc>" +
                            String.Format("<Folder>{0}</Folder>", folderName);
                        //ndViewFields.InnerXml = "<FieldRef Name='Field1'/><FieldRef Name='Field2'/>";

                        /**
                         *  TODO: ez itt nagyon hasznos lenne, de valami�rt mindig SoapException-t dob!!!! :(((((((((((((((((((

                        String ows_ServerUrl = ((!sitesUrlPath.EndsWith("/") && !String.IsNullOrEmpty(sitesUrlPath)) ? String.Format("{0}/", sitesUrlPath) : sitesUrlPath)
                            + ((!docLibName.EndsWith("/") && !String.IsNullOrEmpty(docLibName)) ? String.Format("{0}/", docLibName) : docLibName)
                            + ((!folderPath.EndsWith("/") && !String.IsNullOrEmpty(folderPath)) ? String.Format("{0}/", folderPath) : folderPath)
                            + folderName;

                        ows_ServerUrl = String.Format("/{0}",Utility.RemoveSlash(ows_ServerUrl.Trim()).Trim());

                        ndQuery.InnerXml = String.Format("<Where><Eq><FieldRef Name='ows_ServerUrl'/>" + 
                            "<Value Type='String'>{0}</Value></Eq></Where>", ows_ServerUrl);
                          
                        */

                        Logger.Info(String.Format("ndQuery.InnerXml: {0}", ndQuery.InnerXml));

                        ndListItems = list.GetListItems(docLibName, null, ndQuery, ndViewFields, null, ndQueryOptions, null);

                        Logger.Info(String.Format("GetListItems eredmeny: {0}", ndListItems.OuterXml.ToString()));

                    }
                    catch (Exception ex2)
                    {
                        Logger.Error(String.Format("Hiba a folder adatainak lekerdezesekor! Message: {0}\n StackTrace: {1}", ex2.Message, ex2.StackTrace));
                        result.ErrorCode = "FRENB0002";
                        result.ErrorMessage = String.Format("Hiba a folder adatainak lekerdezesekor! Message: {0}\n StackTrace: {1}", ex2.Message, ex2.StackTrace);
                        return result;
                    }


                    try 
                    {
                        nsManager.AddNamespace("s","uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
                        nsManager.AddNamespace("dt","uuid:C2F41010-65B3-11d1-A29F-00AA00C14882");
                        nsManager.AddNamespace("rs","urn:schemas-microsoft-com:rowset");
                        nsManager.AddNamespace("z","#RowsetSchema");
                        nsManager.AddNamespace("a","http://schemas.microsoft.com/sharepoint/soap/");

                        Logger.Info(String.Format("data ItemCount: {0}",Convert.ToString(ndListItems.SelectSingleNode("//rs:data", nsManager).Attributes["ItemCount"].Value)));

                        if ("1".Equals(Convert.ToString(ndListItems.SelectSingleNode("//rs:data", nsManager).Attributes["ItemCount"].Value)))
                        {
                            XmlNode ff = ndListItems.SelectSingleNode("//z:row", nsManager);

                            folderOwsId = Convert.ToString(ff.Attributes["ows_ID"].Value);
                            folderOwsHiddenVersion = Convert.ToString(ff.Attributes["ows_owshiddenversion"].Value);

                            Logger.Info(String.Format("folderOwsHiddenVersion: {0}; folderOwsId: {1}", folderOwsHiddenVersion, folderOwsId));
                        }
                        else
                        {
                            //Logger.Error(String.Format("Hiba a folder adatainak feldolgozasakor: az adatsor ItemCount erteke nem 1!"));
                            //result.ErrorCode = "FRENB0004";
                            //result.ErrorMessage = String.Format("Hiba a folder adatainak feldolgozasakor: az adatsor ItemCount erteke nem 1!");
                            //return result;

                            String ows_ServerUrl = ((!sitesUrlPath.EndsWith("/") && !String.IsNullOrEmpty(sitesUrlPath)) ? String.Format("{0}/", sitesUrlPath) : sitesUrlPath)
                                + ((!docLibName.EndsWith("/") && !String.IsNullOrEmpty(docLibName)) ? String.Format("{0}/", docLibName) : docLibName)
                                + ((!folderPath.EndsWith("/") && !String.IsNullOrEmpty(folderPath)) ? String.Format("{0}/", folderPath) : folderPath)
                                + folderName;
                            
                            ows_ServerUrl = String.Format("/{0}", Utility.RemoveSlash(ows_ServerUrl.Trim()).Trim());

                            String keresettFolderUrl = String.Format("//z:row[@ows_ServerUrl='{0}']", ows_ServerUrl);

                            if (ndListItems.SelectNodes(keresettFolderUrl, nsManager).Count == 1)
                            {
                                XmlNode ff = ndListItems.SelectSingleNode(keresettFolderUrl, nsManager);

                                folderOwsId = Convert.ToString(ff.Attributes["ows_ID"].Value);
                                folderOwsHiddenVersion = Convert.ToString(ff.Attributes["ows_owshiddenversion"].Value);

                                Logger.Info(String.Format("folderOwsHiddenVersion: {0}; folderOwsId: {1}", folderOwsHiddenVersion, folderOwsId));
                            }
                            else
                            {
                                Logger.Error(String.Format("Hiba a folder adatainak feldolgozasakor: az adatsorok kozott nem talalhato a keresett folder!"));
                                Logger.Error(String.Format("(Lehet, hogy ebben az esetben kellene caml queryvel szukiteni a lekerdezest!!!!)"));
                                result.ErrorCode = "FRENB00014";
                                result.ErrorMessage = String.Format("Hiba a folder adatainak feldolgozasakor: az adatsorok kozott nem talalhato a keresett folder!");
                                return result;
                            }
                        }
                    }
                    catch (Exception ex2)
                    {
                        Logger.Error(String.Format("Hiba a folder adatainak feldolgozasakor! Message: {0}\n StackTrace: {1}", ex2.Message, ex2.StackTrace));
                        result.ErrorCode = "FRENB0003";
                        result.ErrorMessage = String.Format("Hiba a folder adatainak feldolgozasakor! Message: {0}\n StackTrace: {1}", ex2.Message, ex2.StackTrace);
                        return result;
                    }


                    #endregion

                    #region folder rename

                    String fullFolderUrl = ((!serverUrl.EndsWith("/") && !String.IsNullOrEmpty(serverUrl)) ? String.Format("{0}/", serverUrl) : serverUrl)
                        + ((!sitesUrlPath.EndsWith("/") && !String.IsNullOrEmpty(sitesUrlPath)) ? String.Format("{0}/", sitesUrlPath) : sitesUrlPath)
                        + ((!docLibName.EndsWith("/") && !String.IsNullOrEmpty(docLibName)) ? String.Format("{0}/", docLibName) : docLibName)
                        + ((!folderPath.EndsWith("/") && !String.IsNullOrEmpty(folderPath)) ? String.Format("{0}/", folderPath) : folderPath)
                        + folderName;
                    
                    Logger.Info(String.Format("osszeallitott fullFolderUrl: {0}", fullFolderUrl));

                    String strBatch = String.Format("<Method ID='1' Cmd='Update'>" +
                      "<Field Name='ID'>{0}</Field>" +
                      "<Field Name='owshiddenversion'>{1}</Field>" +
                      "<Field Name='FileRef'>{2}</Field>" +
                      "<Field Name='FSObjType'>1</Field>" +
                      "<Field Name='BaseName'>{3}</Field>" +
                    "</Method>", folderOwsId, folderOwsHiddenVersion, fullFolderUrl, folderNewName);

                    XmlDocument xmlDoc = new System.Xml.XmlDocument();

                    System.Xml.XmlElement elBatch = xmlDoc.CreateElement("Batch");

                    elBatch.SetAttribute("OnError", "Continue");
                    elBatch.SetAttribute("ListVersion", listVersion);

                    elBatch.InnerXml = strBatch;

                    XmlNode ndReturn = list.UpdateListItems(docLibName, elBatch);

                    Logger.Info(String.Format("UpdateListItems eredmeny: {0}",ndReturn.OuterXml.ToString()));

                    //  hibakereses
                    //.... TODO

                    String errorCode = ndReturn.SelectSingleNode("//a:ErrorCode", nsManager).InnerText;
                    String errorMessage = String.Empty;

                    if (!"0x00000000".Equals(errorCode))
                    {
                        Logger.Error(String.Format("Kapott error: {0} - {1}", errorCode, errorMessage));
                        errorMessage = ndReturn.SelectSingleNode("//a:ErrorText", nsManager).InnerText;

                        result.ErrorCode = "FREN0009";
                        result.ErrorMessage = String.Format("Hibas eredmenyt adott vissza az SPS updateListItem! Code: {0}\nSzoveg: {1}", errorCode, errorMessage);
                        return result;
                    }

                    result.Record = ((serverUrl.Trim().EndsWith("/")) ? String.Format("{0}", serverUrl.Trim()) : serverUrl.Trim())
                        + ((!sitesUrlPath.EndsWith("/") && !String.IsNullOrEmpty(sitesUrlPath)) ? String.Format("{0}/", sitesUrlPath) : sitesUrlPath)
                        + ((!docLibName.EndsWith("/") && !String.IsNullOrEmpty(docLibName)) ? String.Format("{0}/", docLibName) : docLibName)
                        + ((!folderPath.EndsWith("/") && !String.IsNullOrEmpty(folderPath)) ? String.Format("{0}/", folderPath) : folderPath)
                        + (folderNewName);

                    #endregion

                }

            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba a folder rename futasa soran! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                result.ErrorCode = "FREN0001";
                result.ErrorMessage = String.Format("Hiba a folder rename futasa soran! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            }

            return result;
        } // rename

    } // class

}