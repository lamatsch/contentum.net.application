using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eBusinessDocuments;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;

namespace Contentum.eDocument.SharePoint
{

    public class ListUtilService
    {
        public const string Header = "$Header: ListUtilService.cs, 15, 2016.01.06. 13:24:14, Bern�th L�szl�$";
        public const string Version = "$Revision: 15$";

        /// HOW TO: Use XmlDocument Elements When Passed to or Returned from WebMethods by Using Visual C# .NET
        /// http://support.microsoft.com/kb/330600

        public ListUtilService()
        {
            Logger.Info(String.Format("DocumentService.SharePoint.ListUtilService start.- {0} {1}", Header, Version));
        }

        /// <summary>
        /// Ez jo.
        /// A '(*)' jellel jelolt paramok kotelezoek.
        /// </summary>
        /// <param name="str_siteName">A site neve.</param>
        /// <param name="str_listName">(*) A lista/doclib/... neve.</param>
        /// <param name="str_folderNames">Folder vagy folderok nevei. Lehet ures.</param>
        /// <param name="str_viewName">View neve. Ha a default, akkor null.</param>
        /// <param name="xml_query">Query xml stringben. Ha nem kell lehet null.</param>
        /// <param name="xml_viewFields">A megnezendo oszlopok nevei. Xml stringbe. !!!Ha ures, default erteket kap!!!</param>
        /// <param name="str_rowLimit">Sor limit. Lehet null.</param>
        /// <param name="xml_queryOptions">Opciok. String Xmlben. !!!Ha ures, default erteket kap!!!</param>
        /// <param name="str_webId">??? errol nem talaltam leirast. Null. :(</param>
        /// <returns>Record-ben az eredmenyulkapott NodeXml.</returns>
        /// 

        public Result GetListItems(string str_siteName
                , string str_listName
                , string str_folderNames
                , string str_viewName
                , string xml_query
                , string xml_viewFields
                , string str_rowLimit
                , string xml_queryOptions
                , string str_webId)
        {
            Logger.Info("--- ListUtilService.GetListItems start");
            Result ret = new Result();

            if (String.IsNullOrEmpty(str_listName)
                )
            {
                // TODO:
                Logger.Info("parameterezes hiba!");
                ret.ErrorCode = "300002";
                return ret;
            }

            Logger.Info(String.Format("Kapott parameterek: str_siteName: {0}; str_listName: {1}; str_folderNames: {2}; xml_query: {3}", str_siteName, str_listName, str_folderNames, xml_query));

            String SPUrl = Utility.GetSharePointUrl();

            SPLists.Lists list = new SPLists.Lists();
            list.Url = SPUrl + Utility.SharePointUrlPostfix()
                + ((!str_siteName.EndsWith("/") && !String.IsNullOrEmpty(str_siteName)) ? str_siteName + "/" : str_siteName) + "_vti_bin/lists.asmx";

            Logger.Info(String.Format("List Url: {0}", list.Url));

            list.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();

            XmlDocument xmlDoc = new System.Xml.XmlDocument();

            XmlNode ndViewFields = xmlDoc.CreateNode(XmlNodeType.Element, "ViewFields", "");
            XmlNode ndQueryOptions = xmlDoc.CreateNode(XmlNodeType.Element, "QueryOptions", ""); ;
            XmlNode ndQuery = null;

            if (String.IsNullOrEmpty(xml_queryOptions))
            {
                ndQueryOptions.InnerXml = "<IncludeMandatoryColumns>FALSE</IncludeMandatoryColumns>" +
                    "<DateInUtc>FALSE</DateInUtc><ViewAttributes Scope='RecursiveAll'/>";
            }
            else
            {
                ndQueryOptions.InnerXml = xml_queryOptions;
            }

            if (!String.IsNullOrEmpty(str_folderNames))
                ndQueryOptions.InnerXml += "<Folder>" + str_folderNames + "</Folder>";

            if (String.IsNullOrEmpty(xml_viewFields))
                ndViewFields.InnerXml = "<FieldRef Name='Id' /><FieldRef Name='Title' /><FieldRef Name='Name' /><FieldRef Name='Owner' /><FieldRef Name='BaseName' />";
            else
                ndViewFields.InnerXml = xml_viewFields;


            if (!String.IsNullOrEmpty(xml_query))
            {
                ndQuery = xmlDoc.CreateNode(XmlNodeType.Element, "Query", "");
                ndQuery.InnerXml = xml_query;
            }

            try
            {
                str_listName = (str_listName.EndsWith("/")) ? str_listName.Substring(0, str_listName.Length - 1) : str_listName;
                str_listName = (str_listName.StartsWith("/")) ? str_listName.Substring(1) : str_listName;

                Logger.Info("list.GetListItems elott");
                XmlNode e = list.GetListItems(Utility.GetEkezetesBetukAtalakitasa(str_listName), str_viewName, ndQuery,
                        ndViewFields, str_rowLimit, ndQueryOptions, str_webId);

                ret.Record = e.OuterXml.ToString();

            }
            catch (SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException: " +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: "
                    + "str_siteName=" + str_siteName + "\n"
                    + "str_listName=" + str_listName + "\n"
                    + "str_folderNames=" + str_folderNames + "\n"
                    + "str_viewName=" + str_viewName + "\n"
                    + "xml_query=" + xml_query + "\n"
                    + "xml_viewFields=" + xml_viewFields + "\n"
                    + "str_rowLimit=" + str_rowLimit + "\n"
                    + "xml_queryOptions=" + xml_queryOptions + "\n"
                    + "str_webId=" + str_webId + "\n");
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "123499999";
                ret.ErrorMessage = ex.StackTrace.ToString();
                Logger.Error("Exception! : " + ex.ToString());
                Logger.Error("Kapott parameterek: "
                    + "str_siteName=" + str_siteName + "\n"
                    + "str_listName=" + str_listName + "\n"
                    + "str_folderNames=" + str_folderNames + "\n"
                    + "str_viewName=" + str_viewName + "\n"
                    + "xml_query=" + xml_query + "\n"
                    + "xml_viewFields=" + xml_viewFields + "\n"
                    + "str_rowLimit=" + str_rowLimit + "\n"
                    + "xml_queryOptions=" + xml_queryOptions + "\n"
                    + "str_webId=" + str_webId + "\n");

            }

            return ret;
        }

        /// <summary>
        /// Nev alapjan keres.
        /// </summary>
        /// <param name="str_siteName">A site neve.</param>
        /// <param name="str_listName">A lista/doclib/... neve.</param>
        /// <param name="str_folderNames">Folder vagy folderok nevei. Lehet ures.</param>
        /// <param name="str_fileName">A keresett file neve.</param>
        /// <returns></returns>
        /// 

        public Result GetListItemsByItemName(string str_siteName
                , string str_listName
                , string str_folderNames
                , string str_fileName)
        {
            Logger.Info("--- ListUtilService.GetListItemsByItemName start");

            Logger.Info("ListUtilService.GetListItemsByItemName start");
            string where = "<Where><Eq><FieldRef Name='FileLeafRef'/>" +
                    "<Value Type='Text'>" + str_fileName + "</Value></Eq></Where>";

            return GetListItems(str_siteName, str_listName, str_folderNames, null, where, null, null, null, null);
        }

        /// <summary>
        /// Nem t'om ilyen kell-e majd....
        /// </summary>
        /// <param name="str_siteName">A site neve.</param>
        /// <param name="str_listName">A lista/doclib/... neve.</param>
        /// <param name="str_folderNames">Folder vagy folderok nevei. Lehet ures.</param>
        /// <param name="str_fileNameWidthPath">A keresett file neve teljes eleresi uttal. :)</param>
        /// <returns></returns>
        /// 

        public Result GetListItemsByItemNameAppendWithPath(string str_siteName
                , string str_listName
                , string str_folderNames
                , string str_fileName)
        {
            Logger.Info("--- ListUtilService.GetListItemsByItemNamePath start");
            string s = String.Format("{0}{1}{2}{3}{4}"
                , Utility.SharePointUrlPostfix()
                , ((!String.IsNullOrEmpty(str_siteName) && !str_siteName.EndsWith("/")) ? str_siteName + "/" : str_siteName)
                , ((!String.IsNullOrEmpty(str_listName) && !str_listName.EndsWith("/")) ? str_listName + "/" : str_listName)
                , ((!String.IsNullOrEmpty(str_folderNames) && !str_folderNames.EndsWith("/")) ? str_folderNames + "/" : str_folderNames)
                , str_fileName
                );
            string where = "<Where><Eq><FieldRef Name='FileRef'/>" +
                    "<Value Type='Text'>" + s + "</Value></Eq></Where>";
            return GetListItems(str_siteName, str_listName, str_folderNames, null, where, null, null, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str_siteName">A site neve.</param>
        /// <param name="str_listName">A lista/doclib/... neve.</param>
        /// <param name="str_folderNames">Folder vagy folderok nevei. Lehet ures.</param>
        /// <param name="str_ItemId">Id. :)</param>
        /// <returns></returns>
        /// 

        public Result GetListItemById(string str_siteName
                , string str_listName
                , string str_folderNames
                , string str_ItemId)
        {
            Logger.Info("--- ListUtilService.GetListItemById start");
            string where = "<Where><Eq><FieldRef Name='UniqueId'/>" +
                    "<Value Type='Text'>" + str_ItemId + "</Value></Eq></Where>";
            return GetListItems(str_siteName, str_listName, str_folderNames, null, where, null, null, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str_siteName">A site neve.</param>
        /// <param name="str_listName"></param>
        /// <param name="str_folderNames"></param>
        /// <param name="str_fileName"></param>
        /// <returns></returns>
        /// 

        public Result GetListItemIdByName(string str_siteName
                , string str_listName
                , string str_folderNames
                , string str_itemLeafRef)
        {
            Logger.Info("--- ListUtilService.GetListItemIdByName start");
            Logger.Info("Kapott parameterek: str_siteName=" + str_siteName + "; str_listName=" + str_listName + "; str_folderNames=" + str_folderNames + "; str_itemLeafRef=" + str_itemLeafRef);

            Result ret = new Result();

            String SPUrl = Utility.GetSharePointUrl();

            SPLists.Lists list = new SPLists.Lists();
            //            + ((!str_listName.EndsWith("/") && !String.IsNullOrEmpty(str_listName)) ? str_listName + "/" : str_listName)
            list.Url = SPUrl + Utility.SharePointUrlPostfix()
                + ((!str_siteName.EndsWith("/") && !String.IsNullOrEmpty(str_siteName)) ? str_siteName + "/" : str_siteName)
                + "_vti_bin/lists.asmx";

            list.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();

            Logger.Info(String.Format("Ezt futtatjuk: {0}", list.Url));

            string str_viewName = null;
            string xml_viewFields = null;
            string str_rowLimit = null;
            string xml_queryOptions = null;
            string str_webId = null;

            XmlDocument xmlDoc = new System.Xml.XmlDocument();

            XmlNode ndViewFields = xmlDoc.CreateNode(XmlNodeType.Element, "ViewFields", "");
            XmlNode ndQueryOptions = xmlDoc.CreateNode(XmlNodeType.Element, "QueryOptions", ""); ;
            XmlNode ndQuery = null;

            if (String.IsNullOrEmpty(xml_queryOptions))
            {
                ndQueryOptions.InnerXml = "<IncludeMandatoryColumns>FALSE</IncludeMandatoryColumns>" +
                    "<DateInUtc>FALSE</DateInUtc>";
            }
            else
            {
                ndQueryOptions.InnerXml = xml_queryOptions;
            }

            if (!String.IsNullOrEmpty(str_folderNames))
                ndQueryOptions.InnerXml += "<Folder>" + str_folderNames + "</Folder>";

            if (String.IsNullOrEmpty(xml_viewFields))
                ndViewFields.InnerXml = "<FieldRef Name='Id' /><FieldRef Name='Title' /><FieldRef Name='Name' /><FieldRef Name='Owner' /><FieldRef Name='BaseName' />";
            else
                ndViewFields.InnerXml = xml_viewFields;


            ndQuery = xmlDoc.CreateNode(XmlNodeType.Element, "Query", "");

            string str_itemLeafRefwPath = Utility.SharePointUrlPostfix()
                        + ((!str_siteName.EndsWith("/") && !String.IsNullOrEmpty(str_siteName)) ? str_siteName + "/" : str_siteName)
                        + ((!str_listName.EndsWith("/") && !String.IsNullOrEmpty(str_listName)) ? str_listName + "/" : str_listName)
                        + ((!str_folderNames.EndsWith("/") && !String.IsNullOrEmpty(str_folderNames)) ? str_folderNames + "/" : str_folderNames)
                        + str_itemLeafRef;
            Logger.Info("str_itemLeafRefwPath = " + str_itemLeafRefwPath);

            ndQuery.InnerXml = "<Where><Eq><FieldRef Name='FileRef'/>" +
                    "<Value Type='Text'>" + str_itemLeafRefwPath + "</Value></Eq></Where>";

            XmlNode node = null;

            try
            {
                Logger.Info("list.GetListItems meghivasa elott");
                node = list.GetListItems(str_listName, str_viewName, ndQuery,
                        ndViewFields, str_rowLimit, ndQueryOptions, str_webId);


                Logger.Info("eredmeny node: " + node.OuterXml.ToString());

                int countItem = GetItemCount(node);

                if (countItem == 0)
                {
                    ret.ErrorCode = "190000002"; // Ez maradjon, mert ezt hasznalja az eDocument:DocumentService.LetezikMarIlyenNevuWithCTT !!!!
                    ret.ErrorMessage = "GetListItemIdByName: Nincs a megadott feltetelnek megfelelo objektum (0 elemmel tert vissza a GetListItems fuggveny)!";
                    Logger.Error(ret.ErrorMessage);
                    return ret;
                }

                if (countItem > 1)
                {
                    ret.ErrorCode = "190000003";
                    ret.ErrorMessage = "GetListItemIdByName: Tul sok a megadott feltetelnek megfelelo objektum (>1 elemmel tert vissza a GetListItems fuggveny)!";
                    Logger.Error(ret.ErrorMessage);
                    return ret;
                }

                // itt visszaadjuk a guid-ot, {} nelkul
                string ows_ID = node.ChildNodes[1].ChildNodes[1].Attributes["ows_ID"].InnerXml.ToString();
                string elejerolLevagando = ows_ID + ";#{";
                string tmp_id = node.ChildNodes[1].ChildNodes[1].Attributes["ows_UniqueId"].InnerXml.ToString().Substring(elejerolLevagando.Length);

                Logger.Info("Guid visszaadasa ok");

                ret.Uid = tmp_id.Substring(0, tmp_id.Length - 1);

            }
            catch (SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException:" +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: "
                    + "str_siteName=" + str_siteName + "\n"
                    + "str_listName=" + str_listName + "\n"
                    + "str_folderNames=" + str_folderNames);
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "123499999";
                ret.ErrorMessage = ex.StackTrace.ToString();
                Logger.Error("Exception! : " + ex.ToString());
                Logger.Error("Kapott parameterek: "
                    + "str_siteName=" + str_siteName + "\n"
                    + "str_listName=" + str_listName + "\n"
                    + "str_folderNames=" + str_folderNames);

            }

            return ret;
        }

        /// <summary>
        /// Egy GetListItem altal visszaadott rowset-es xmlnode eredmenybol megmondja mennyi az eredmenysor.
        /// </summary>
        /// <param name="pnode"></param>
        /// <returns></returns>
        public int GetItemCount(XmlNode pnode)
        {
            Logger.Info("--- ListUtilService.GetItemCount start");
            int ret = 0;

            try
            {
                //ret = Convert.ToInt16(node.ChildNodes[1].Attributes["ItemCount"].InnerXml.ToString());
                System.Xml.XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(pnode.OuterXml.ToString());

                XmlNodeList nodeList = xmldoc.GetElementsByTagName("rs:data");
                XmlNode node = nodeList.Item(0);
                ret = Convert.ToInt16(node.Attributes["ItemCount"].InnerXml.ToString());
            }
            catch (Exception ex)
            {
                Logger.Error("Exception volt! : " + ex.ToString());
                Logger.Error("Eredmenysornak nullat adtunk vissza.");
                Logger.Error("A kapott XmlNode: " + (pnode != null ? pnode.OuterXml.ToString() : "null"));
            }

            return ret;
        }

        /// <summary>
        /// Egy GetListItem altal visszaadott rowset-es xmlnode eredmenybol megmondja az elso item ows_ID-jet.
        /// </summary>
        /// <param name="pnode"></param>
        /// <returns></returns>
        public string GetItemOwsId(XmlNode pnode)
        {
            Logger.Info("--- ListUtilService.GetItemOwsId start");
            string ret = "";

            try
            {
                //ret = node.ChildNodes[1].ChildNodes[1].Attributes["ows_ID"].InnerXml.ToString();
                System.Xml.XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(pnode.OuterXml.ToString());

                XmlNodeList nodeList = xmldoc.GetElementsByTagName("z:row");
                XmlNode node = nodeList.Item(0);
                ret = node.Attributes["ows_ID"].InnerXml.ToString();
                Logger.Info(String.Format("visszaadott ertek: {0}", ret));
            }
            catch (Exception ex)
            {
                Logger.Error("Exception volt! : " + ex.ToString());
                Logger.Error("Ures stringet adtunk vissza.");
                Logger.Error("A kapott XmlNode: " + (pnode != null ? pnode.OuterXml.ToString() : "null"));
            }

            return ret;
        }

        public string GetItemOwsUniqueId(XmlNode pnode)
        {
            Logger.Info("--- ListUtilService.GetItemOwsId start");
            string ret = "";

            try
            {
                //ret = node.ChildNodes[1].ChildNodes[1].Attributes["ows_ID"].InnerXml.ToString();
                System.Xml.XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(pnode.OuterXml.ToString());

                XmlNodeList nodeList = xmldoc.GetElementsByTagName("z:row");
                XmlNode node = nodeList.Item(0);

                string ows_ID = node.Attributes["ows_ID"].InnerXml.ToString();
                string elejerolLevagando = ows_ID + ";#{";
                string tmp_id = node.Attributes["ows_UniqueId"].InnerXml.ToString().Substring(elejerolLevagando.Length);
                ret = tmp_id.Substring(0, tmp_id.Length - 1);

                //ret = node.Attributes["ows_ID"].InnerXml.ToString();
                Logger.Info(String.Format("visszaadott ertek: {0}", ret));
            }
            catch (Exception ex)
            {
                Logger.Error("Exception volt! : " + ex.ToString());
                Logger.Error("Ures stringet adtunk vissza.");
                Logger.Error("A kapott XmlNode: " + (pnode != null ? pnode.OuterXml.ToString() : "null"));
            }

            return ret;
        }

        public string GetListItemIdFromXml(XmlNode node, int nodeIdx)
        {
            string ret = "";

            try
            {
                string ows_ID = node.ChildNodes[1].ChildNodes[1].Attributes["ows_ID"].InnerXml.ToString();
                string elejerolLevagando = ows_ID + ";#{";
                string tmp_id = node.ChildNodes[1].ChildNodes[nodeIdx].Attributes["ows_UniqueId"].InnerXml.ToString().Substring(elejerolLevagando.Length);

                ret = tmp_id.Substring(0, tmp_id.Length - 1);
            }
            catch (Exception ex)
            {
                //TODO: log kell ide!!
            }

            return ret;
        }

        public string GetListItemIdFromXml(XmlNode node)
        {
            return GetListItemIdFromXml(node, 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str_site"></param>
        /// <param name="str_listName"></param>
        /// <param name="str_folderNames"></param>
        /// <param name="old_filename">Regi (teljes) neve, kiterjesztessel egyutt!!!</param>
        /// <param name="new_basename">BaseName: kiterjesztes nelkuli nev!!! Ez kell ide.</param>
        /// <returns></returns>
        /// 
        [WebMethod(Description = "<b>old_filename:</b> Regi (teljes) neve, kiterjesztessel egyutt!!!" +
          "<br><b>new_basename:</b> kiterjesztes nelkuli nev!!!")]
        public Result Rename(string str_site, string str_listName, string str_folderNames, string old_filename, string new_basename)
        {
            Logger.Info("--- ListUtilService.Rename start");
            Result ret = new Result();
            XmlNode itemNode = null;

            ret = GetListItemsByItemName(str_site, str_listName, str_folderNames, old_filename);
            if (String.IsNullOrEmpty(ret.ErrorCode))
            {
                XmlDocument xmlD = new XmlDocument();
                xmlD.LoadXml((string)ret.Record);
                itemNode = xmlD.FirstChild;
                Logger.Info(String.Format("itemNode: {0}", itemNode.OuterXml.ToString()));
            }
            else
            {
                return ret;
            }

            if (itemNode == null)
            {
                ret = new Result();
                ret.ErrorCode = "192222222222";
                ret.ErrorMessage = "Rename hiba: ures XmlNode-dal tert vissza a GetListItemsByItemName!";
                Logger.Error(ret.ErrorMessage);
                return ret;
            }

            if (GetItemCount(itemNode) == 0)
            {
                ret = new Result();
                ret.ErrorCode = "192222222223";
                ret.ErrorMessage = "Rename hiba: nincs meg a megadott elem!";
                Logger.Error(ret.ErrorMessage);
                return ret;
            }

            String SPUrl = Utility.GetSharePointUrl();

            string fileRef = SPUrl + Utility.SharePointUrlPostfix()
                + ((!str_site.EndsWith("/") && !String.IsNullOrEmpty(str_site)) ? str_site + "/" : str_site)
                + ((!str_listName.EndsWith("/") && !String.IsNullOrEmpty(str_listName)) ? str_listName + "/" : str_listName)
                + ((!str_folderNames.EndsWith("/") && !String.IsNullOrEmpty(str_folderNames)) ? str_folderNames + "/" : str_folderNames)
                + old_filename;

            Logger.Info(String.Format("fileRef erteke: {0}", fileRef));

            SPLists.Lists list = new SPLists.Lists();
            list.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();
            list.Url = SPUrl + Utility.SharePointUrlPostfix() + ((!str_site.EndsWith("/") && !String.IsNullOrEmpty(str_site)) ? str_site + "/" : str_site) + "_vti_bin/lists.asmx";

            Logger.Info(String.Format("list.Url: {0}", list.Url));

            #region List id meghatarozasa

            //string listId = String.Empty;

            //try
            //{
            //    XmlNode ndList = list.GetList(Contentum.eDocument.SharePoint.Utility.RemoveSlash(str_listName));

            //    Logger.Info("list id megszerzese: ws hivas ok.");

            //    listId = Convert.ToString(ndList.Attributes["ID"].Value);

            //    Logger.Info(String.Format("list id megszerzese: list id: {0}", listId));
            //} 
            //catch (SoapException spe) 
            //{
            //    ret.ErrorCode = "2222222222SOAP";
            //    ret.ErrorMessage = String.Format("SOAP exception! Message: {0}\nDetail: {1}\nStackTrace:{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace);
            //    Logger.Error("SoapException: " +
            //    String.Format("Message: {0}\nDetail: {1}\nStackTrace: {2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
            //    Logger.Error("Kapott parameterek: "
            //       + " str_site = " + str_site + "\n"
            //       + " str_listName = " + str_listName + "\n"
            //       + " str_folderNames = " + str_folderNames + "\n"
            //       + " old_filename = " + old_filename + "\n"
            //       + " new_basename = " + new_basename);
            //    return ret;
            //}
            //catch (Exception ex)
            //{
            //    ret.ErrorCode = "1122222678";
            //    ret.ErrorMessage = ex.StackTrace.ToString();
            //    Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1}",ex.Message, ex.StackTrace));
            //    Logger.Error("Kapott parameterek: "
            //       + " str_site = " + str_site + "\n"
            //       + " str_listName = " + str_listName + "\n"
            //       + " str_folderNames = " + str_folderNames + "\n"
            //       + " old_filename = " + old_filename + "\n"
            //       + " new_basename = " + new_basename);
            //    return ret;
            //}


            #endregion

            // BaseName: alapnev, kiterjesztes nelkul :) aaaaaaa
            string strBatch = "<Method ID='1' Cmd='Update'>" +
                "<Field Name='ID'>" + GetItemOwsId(itemNode) + "</Field>" +
                "</Method>";  //<Field Name='BaseName'>" + old_filename + "</Field>

            //"<Field Name='FileRef'>" + fileRef + "</Field>" +

            XmlDocument xmlDoc = new System.Xml.XmlDocument();

            System.Xml.XmlNode elBatch = xmlDoc.CreateNode(XmlNodeType.Element, "Batch", "");

            //elBatch.SetAttribute("OnError", "Continue");
            //elBatch.SetAttribute("ListVersion", "1");
            //elBatch.SetAttribute("ViewName", "");

            elBatch.InnerXml = strBatch;

            try
            {
                Logger.Info("Rename: list.UpdateListItems hivas elott");
                XmlNode xxx = list.UpdateListItems(Contentum.eDocument.SharePoint.Utility.RemoveSlash(str_listName), elBatch);
                ret.Record = xxx.OuterXml.ToString();
            }
            catch (SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = String.Format("SOAP exception! Message: {0}\nDetail: {1}\nStackTrace:{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace);
                Logger.Error("SoapException: " +
                String.Format("Message: {0}\nDetail: {1}\nStackTrace: {2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: "
                   + " str_site = " + str_site + "\n"
                   + " str_listName = " + str_listName + "\n"
                   + " str_folderNames = " + str_folderNames + "\n"
                   + " old_filename = " + old_filename + "\n"
                   + " new_basename = " + new_basename);
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "3422222444";
                ret.ErrorMessage = ex.StackTrace.ToString();
                Logger.Error(String.Format("Exception! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error("Kapott parameterek: "
                   + " str_site = " + str_site + "\n"
                   + " str_listName = " + str_listName + "\n"
                   + " str_folderNames = " + str_folderNames + "\n"
                   + " old_filename = " + old_filename + "\n"
                   + " new_basename = " + new_basename);
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str_site"></param>
        /// <param name="str_listName"></param>
        /// <param name="str_folderNames"></param>
        /// <param name="str_filename"></param>
        /// <returns></returns>
        /// 

        public Result Delete(string rootsite, string str_site, string str_listName, string str_folderNames, string str_filename)
        {
            Logger.Info("ListUtilService.Delete start");
            Result ret = new Result();
            XmlNode itemNode = null;
            string deleteGUID = String.Empty;

            ret = GetListItemsByItemNameAppendWithPath(RemoveSlash(str_site), RemoveSlash(str_listName), RemoveSlash(str_folderNames), str_filename);
            if (String.IsNullOrEmpty(ret.ErrorCode))
            {
                /// itemNode = (XmlNode)ret.Record; - mod by MoPe - use the same as rename
                XmlDocument xmlD = new XmlDocument();
                xmlD.LoadXml((string)ret.Record);
                itemNode = xmlD.FirstChild;

                Logger.Info(String.Format("Delete - itemNode: {0}", itemNode.OuterXml.ToString()));
            }
            else
            {
                return ret;
            }

            if (itemNode == null)
            {
                ret = new Result();
                ret.ErrorCode = "199222222222";
                ret.ErrorMessage = "Delete hiba: ures XmlNode-dal tert vissza a GetListItemsByItemName!";
                Logger.Error(ret.ErrorMessage);
                return ret;
            }

            // BUG_13766
            //if (GetItemCount(itemNode) == 0)
            //{
            //    ret = new Result();
            //    ret.ErrorCode = "199222222223";
            //    ret.ErrorMessage = "Delete hiba: nincs meg a megadott elem!";
            //    Logger.Error(ret.ErrorMessage);
            //    return ret;
            //}
            if (GetItemCount(itemNode) == 0)
            {
                ret = new Result();
                //ret.ErrorCode = "199222222223";
                //ret.ErrorMessage = "Delete hiba: nincs meg a megadott elem!";
                Logger.Warn("Delete hiba: nincs meg a megadott elem!");
                return ret;
            }
            String SPUrl = Utility.GetSharePointUrl();

            string fileRef = SPUrl + ((!rootsite.EndsWith("/") && !String.IsNullOrEmpty(rootsite)) ? rootsite + "/" : rootsite)
                + ((!str_site.EndsWith("/") && !String.IsNullOrEmpty(str_site)) ? str_site + "/" : str_site)
                + ((!str_listName.EndsWith("/") && !String.IsNullOrEmpty(str_listName)) ? str_listName + "/" : str_listName)
                + ((!str_folderNames.EndsWith("/") && !String.IsNullOrEmpty(str_folderNames)) ? str_folderNames + "/" : str_folderNames)
                + str_filename;

            SPLists.Lists list = new SPLists.Lists();
            list.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();
            Logger.Info(String.Format("SPUrl : {0}", SPUrl));
            list.Url = SPUrl + "_vti_bin/lists.asmx";

            string strBatch = "<Method ID='1' Cmd='Delete'>" +
                "<Field Name='ID'>" + GetItemOwsId(itemNode) + "</Field>" +
                "<Field Name='FileRef'>" + fileRef + "</Field></Method>";

            XmlDocument xmlDoc = new System.Xml.XmlDocument();

            System.Xml.XmlNode elBatch = xmlDoc.CreateNode(XmlNodeType.Element, "Batch", "");

            //elBatch.SetAttribute("OnError", "Continue");
            //elBatch.SetAttribute("ListVersion", "1");
            //elBatch.SetAttribute("ViewName", "All Documents");

            elBatch.InnerXml = strBatch;

            try
            {
                Logger.Info("list.UpdateListItems elott");
                Logger.Info(String.Format("elBatch.OuterXml: {0}", elBatch.OuterXml.ToString()));
                //XmlNode xxx = list.UpdateListItems(RemoveSlash(str_listName), elBatch);
                
                string pathUrl = Utility.GetSharePointUrl() + ((!rootsite.EndsWith("/") && !String.IsNullOrEmpty(rootsite)) ? rootsite + "/" : rootsite)
                + ((!str_site.EndsWith("/") && !String.IsNullOrEmpty(str_site)) ? str_site + "/" : str_site);
                list.Url = pathUrl + "_vti_bin/lists.asmx";
                XmlNode xxx = list.UpdateListItems(ListName2GUID(pathUrl, str_listName), elBatch);
                ret.Record = xxx;
            }
            catch (SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Error("SoapException: " +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Error("Kapott parameterek: "
                   + " str_site = " + str_site + "\n"
                   + " str_listName = " + str_listName + "\n"
                   + " str_folderNames = " + str_folderNames + "\n"
                   + " str_filename = " + str_filename);
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "3422222444";
                ret.ErrorMessage = ex.StackTrace.ToString();
                Logger.Error("Exception : " + ex.ToString());
                Logger.Error("Kapott parameterek: "
                   + " str_site = " + str_site + "\n"
                   + " str_listName = " + str_listName + "\n"
                   + " str_folderNames = " + str_folderNames + "\n"
                   + " str_filename = " + str_filename);
            }

            return ret;
        }

        public static string RemoveSlash(String s)
        {
            if (String.IsNullOrEmpty(s)) return String.Empty;
            s = s.Replace('\\', '/');
            string x = s.Trim().StartsWith("/") ? s.Trim().Substring(1) : s.Trim();
            return x.EndsWith("/") ? x.Substring(0, x.Length - 1) : x;
        }

        public static string ListName2GUID(string URL, string dLibName)
        {
            string _result = "";
            try
            {
                SPLists.Lists getGuidList = new SPLists.Lists();
				getGuidList.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();
				getGuidList.Url = URL + "_vti_bin/lists.asmx";
                XmlNode List = getGuidList.GetListCollection();
                foreach (XmlNode list in List)
                {
                    if (list.Attributes["Title"].Value == dLibName)
                    { _result = list.Attributes["ID"].Value; }
                }
                Logger.Info(String.Format("List guid: {0}", _result));
                return _result;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

    } //class
}
