using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eBusinessDocuments;
using System.Web.Services;

namespace Contentum.eDocument.SharePoint
{

    public class PermissionService 
    {
        public const string Header = "$Header: PermissionService.cs, 4, 2010.02.17 13:52:30, Varsanyi P?ter$";
        public const string Version = "$Revision: 4$";

        public static string List = "List";
        public static string Web = "Web";
        public static string User = "user";
        public static string Group = "group";
        public static string Role = "role";

        public PermissionService()
        {
            Logger.Info(String.Format("DocumentService.SharePoint.PermissionService start.- {0} {1}", Header, Version));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteName">Site neve.</param>
        /// <param name="objName">Lista vagy web neve.</param>
        /// <param name="objType">"List" || "Web"</param>
        /// <param name="userOrGroup">User neve (domain\\nev), group neve</param>
        /// <param name="permType">"user" || "group" || "role"</param>
        /// <param name="permMask">mask: | jelekkel 0x00000002 | 0x00000400</param>
        /// <returns></returns>
        /// 
        
        public Result AddPermission(string siteName, string objName, string objType, string userOrGroup,
                                    string permType, Int32 permMask)
        {
            Result ret = new Result();

            String SPUrl = Utility.GetSharePointUrl();

            try
            {

                SPPermissions.Permissions perm = new SPPermissions.Permissions();
                perm.Url = SPUrl
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + "_vti_bin/permissions.asmx";
                perm.Credentials = Utility.GetCredential(); 

                perm.AddPermission(objName, objType, userOrGroup, permType, permMask);

                ret.Record = "OK";

            }
            catch (System.Web.Services.Protocols.SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Info("SoapException: " +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                // Logger.Info("Kapott parameterek: path=" + path + " ; folderName=" + folderName);
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "10001010";
                ret.ErrorMessage = ex.StackTrace.ToString();
            }


            return ret;
        }

        /// <summary>
        /// List permission.
        /// </summary>
        /// <param name="siteName">Site neve. Lehet ures is.</param>
        /// <param name="objName">Lista neve</param>
        /// <param name="permId">A tipustol fuggo. Pl domain\\user, group neve, role neve</param>
        /// <param name="permType">"user" || "group" || "role"</param>
        /// <returns></returns>
        
        public Result RemovePermission(string siteName, string listName,
                                    string permId, string permType)
        {
            Result ret = new Result();

            String SPUrl = Utility.GetSharePointUrl();

            try
            {

                SPPermissions.Permissions perm = new SPPermissions.Permissions();
                perm.Url = SPUrl
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + "_vti_bin/permissions.asmx";
                perm.Credentials = Utility.GetCredential(); //new NetworkCredential("varsanyi.peter", "", "axis");

                perm.RemovePermission(listName, PermissionService.List, permId, permType);

                ret.Record = "OK";

            }
            catch (System.Web.Services.Protocols.SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Info("SoapException: " +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                //Logger.Info("Kapott parameterek: path=" + path + " ; folderName=" + folderName);
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "10001012";
                ret.ErrorMessage = ex.StackTrace.ToString();
            }


            return ret;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteName">Site neve.</param>
        /// <param name="objName">Lista vagy web neve.</param>
        /// <param name="objType">"List" || "Web"</param>
        /// <param name="userOrGroup">User neve (domain\\nev), group neve</param>
        /// <param name="permType">"user" || "group" || "role"</param>
        /// <param name="permMask">mask: | jelekkel 0x00000002 | 0x00000400</param>
        /// <returns></returns>
        /// 
        
        public Result UpdatePermission(string siteName, string objName, string objType, string userOrGroup,
                                    string permType, Int32 permMask)
        {
            Result ret = new Result();

            String SPUrl = Utility.GetSharePointUrl();

            try
            {

                SPPermissions.Permissions perm = new SPPermissions.Permissions();
                perm.Url = SPUrl
                    + ((!siteName.EndsWith("/") && !String.IsNullOrEmpty(siteName)) ? siteName + "/" : siteName)
                    + "_vti_bin/permissions.asmx";
                perm.Credentials = Utility.GetCredential(); //new NetworkCredential("varsanyi.peter", "", "axis");

                perm.UpdatePermission(objName, objType, userOrGroup, permType, permMask);

                ret.Record = "OK";

            }
            catch (System.Web.Services.Protocols.SoapException spe)
            {
                ret.ErrorCode = "1111111SOAP";
                ret.ErrorMessage = spe.ToString();
                Logger.Info("SoapException: " +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                //Logger.Info("Kapott parameterek: path=" + path + " ; folderName=" + folderName);
            }
            catch (Exception ex)
            {
                ret.ErrorCode = "10001013";
                ret.ErrorMessage = ex.StackTrace.ToString();
            }


            return ret;
        }


    }
}