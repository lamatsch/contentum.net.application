using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using Contentum.eBusinessDocuments;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace Contentum.eDocument.SharePoint
{

    public class SiteService
    {

        public const string Header = "$Header: SiteService.cs, 4, 2010.02.17 13:52:30, Varsanyi P?ter$";
        public const string Version = "$Revision: 4$";

        public SiteService()
        {
            Logger.Info(String.Format("DocumentService.SharePoint.SiteService start.- {0} {1}", Header, Version));
        }

        /// <summary>
        /// Ez jo.
        /// </summary>
        /// <param name="siteName"></param>
        /// <param name="siteTitle"></param>
        /// <param name="siteTemplateName"></param>
        /// <returns></returns>
        /// 
        
        public Result Create(string path, string siteName,
            string siteTitle, string siteTemplateName)
        {
            Logger.Info("SiteService.Create start.");
            Result _ret = new Result();

            if (String.IsNullOrEmpty(siteName) ||
                String.IsNullOrEmpty(siteTitle))
            {
                // TODO:
                Logger.Error("parameteres hiba!");
                _ret.ErrorCode = "320001";
                return _ret;
            }

            String SPUrl = Utility.GetSharePointUrl();
            string siteCollectionUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SiteCollectionUrl");
            string rootDoktarSite = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("DokumentumtarSite");

            if (String.IsNullOrEmpty(siteTemplateName))
            {
                siteTemplateName = "STS#2";
                Logger.Info("siteTemplateName ures volt, default erteket kapott: " + siteTemplateName);
            }

            Result isSE = IsExistsSite(path, siteTitle);
            if (String.IsNullOrEmpty(isSE.ErrorCode))
            {
                if (Convert.ToBoolean(isSE.Record))
                {
                    _ret.Record = String.Format("{0}{1}{2}{3}", SPUrl, siteCollectionUrl, rootDoktarSite, siteName);
                    return _ret;
                }
            }

            path = ((!path.EndsWith("/")) && (!String.IsNullOrEmpty(path))) ? path + "/" : path;

            try
            {
                SPAdmins.Admin admin = new SPAdmins.Admin();
                admin.Url = Utility.GetSharePointAdminUrl() + @"_vti_adm/Admin.asmx";
                admin.PreAuthenticate = true;
                admin.Credentials = Utility.GetCredential();

                Int32 langId = Convert.ToInt32(GetLanguages());
                string userAuth = Utility.GetSharePointUserDomain() + "\\" + Utility.GetSharePointUserName();
                string userName = Utility.GetSharePointUserName();
                string userEmail = userName + @"@axis.hu";


                Logger.Info("admin.CreateSite elott.");
                //
                //String.Format("{0}{1}{2}{3}", SPUrl, siteCollectionUrl, rootDoktarSite, siteName)
                //
                _ret.Record = admin.CreateSite(String.Format("{2}{0}{1}", path, siteName, Utility.GetSharePointUrl())
                    , siteName
                    , siteTitle
                    , langId
                    , siteTemplateName
                    , userAuth
                    , userName
                    , userEmail
                    , ""
                    , ""
                );

                //AxisUploadToSps.Upload cws = new AxisUploadToSps.Upload();
                //cws.PreAuthenticate = true;
                //cws.Credentials = Utility.GetCredential();
                //string siteCollectionUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SiteCollectionUrl");
                //string rootDoktarSite = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("DokumentumtarSite");

                //path = String.Format("{0}{1}{2}",siteCollectionUrl,rootDoktarSite,path);

                //string eredmeny = cws.CreateSite(path, siteName, siteTitle, siteTemplateName);

                //if (eredmeny.Equals("OK"))
                //{
                //    eredmeny = Utility.GetSharePointAdminUrl() + String.Format("{0}{1}{2}{3}", siteCollectionUrl, rootDoktarSite, path, siteName);
                //    _ret.Record = eredmeny;
                //} 
                //else 
                //{
                //    _ret.ErrorCode = "12333CREE";
                //    _ret.ErrorMessage = eredmeny;
                //}
            }
            catch (SoapException spe1)
            {
                _ret.ErrorCode = "1111111SOAP";
                _ret.ErrorMessage = String.Format("SoapException! MESSAGE:\n{0}\nDETAIL:\n{1}\nSTACKTRACE:\n{2}\nINNEREXCEPTION: {3}\nTARGETSITE:{4}\n", spe1.Message, spe1.Detail.InnerText, spe1.StackTrace, spe1.InnerException, spe1.TargetSite);
                Logger.Error(String.Format("SoapException MESSAGE:\n{0}\nDETAIL:\n{1}\nSTACKTRACE:\n{2}\nINNEREXCEPTION: {3}\nTARGETSITE:{4}\n", spe1.Message, spe1.Detail.InnerText, spe1.StackTrace, spe1.InnerException, spe1.TargetSite));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; siteTitle=" + siteTitle + " ; siteTemplateName=" + siteTemplateName);
            }
            catch (Exception spe)
            {
                _ret.ErrorCode = "333333333 : " + Utility.GetSharePointUrl() + siteName;  // spe.ErrorCode.ToString();
                _ret.ErrorMessage = String.Format("Exception! INNEREXCEPTION: {0}\nMESSAGE: {1}\nSTACKTRACE: {2}\nTARGETSITE: {3}\n", spe.InnerException, spe.Message, spe.StackTrace, spe.TargetSite);
                Logger.Error(String.Format("Exception! INNEREXCEPTION: {0}\nMESSAGE: {1}\nSTACKTRACE: {2}\nTARGETSITE: {3}\n", spe.InnerException, spe.Message, spe.StackTrace, spe.TargetSite));
                Logger.Error("Kapott parameterek: siteName=" + siteName + " ; siteTitle=" + siteTitle + " ; siteTemplateName=" + siteTemplateName);
                return _ret;
            }

            //_ret.Record = SPUrl + siteName;
            return _ret;
        }

        /// <summary>
        /// Ez csak tesztre hasznalatos
        /// </summary>
        /// <param name="path"></param>
        /// <param name="siteName"></param>
        /// <param name="siteTitle"></param>
        /// <param name="siteTemplateName"></param>
        /// <returns></returns>
        private Result CreateWithAPI(string path, string siteName,
            string siteTitle, string siteTemplateName)
        {
            Logger.Info("SiteService.Create start.");
            Result _ret = new Result();

            if (String.IsNullOrEmpty(siteName) ||
                String.IsNullOrEmpty(siteTitle))
            {
                // TODO:
                Logger.Info("parameteres hiba!");
                _ret.ErrorCode = "320001";
                return _ret;
            }

            String SPUrl = Utility.GetSharePointUrl();
            string siteCollectionUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SiteCollectionUrl");
            string rootDoktarSite = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("DokumentumtarSite");

            if (String.IsNullOrEmpty(siteTemplateName))
            {
                siteTemplateName = "STS#2";
                Logger.Info("siteTemplateName ures volt, default erteket kapott: " + siteTemplateName);
            }

            Result isSE = IsExistsSite(path, siteTitle);
            if (String.IsNullOrEmpty(isSE.ErrorCode))
            {
                if (Convert.ToBoolean(isSE.Record))
                {
                    _ret.Record = String.Format("{0}{1}{2}{3}", SPUrl, siteCollectionUrl, rootDoktarSite, siteName);
                    return _ret;
                }
            }

            try
            {
                AxisUploadToSps.Upload cws = new AxisUploadToSps.Upload();
                cws.PreAuthenticate = true;
                cws.Credentials = Utility.GetCredential();

                path = String.Format("{0}{1}{2}", siteCollectionUrl, rootDoktarSite, path);

                string eredmeny = cws.CreateSite(path, siteName, siteTitle, siteTemplateName);

                if (eredmeny.Equals("OK"))
                {
                    eredmeny = Utility.GetSharePointAdminUrl() + String.Format("{0}{1}{2}{3}", siteCollectionUrl, rootDoktarSite, path, siteName);
                    _ret.Record = eredmeny;
                }
                else
                {
                    _ret.ErrorCode = "12333CREE";
                    _ret.ErrorMessage = eredmeny;
                }
            }
            catch (SoapException spe1)
            {
                _ret.ErrorCode = "1111111SOAP";
                _ret.ErrorMessage = spe1.ToString();
                Logger.Info(String.Format("SoapException message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe1.Message, spe1.Detail.InnerText, spe1.StackTrace));
                Logger.Info("Kapott parameterek: siteName=" + siteName + " ; siteTitle=" + siteTitle + " ; siteTemplateName=" + siteTemplateName);
            }
            catch (Exception spe)
            {
                _ret.ErrorCode = "333333333 : " + Utility.GetSharePointUrl() + siteName;  // spe.ErrorCode.ToString();
                _ret.ErrorMessage = spe.StackTrace.ToString();
                Logger.Info("Exception: " + spe.ToString());
                Logger.Info("Kapott parameterek: siteName=" + siteName + " ; siteTitle=" + siteTitle + " ; siteTemplateName=" + siteTemplateName);
                return _ret;
            }

            //_ret.Record = SPUrl + siteName;
            return _ret;
        }



        /// <summary>
        /// Ez jo.
        /// </summary>
        /// <returns></returns>
        
        public Result IsExistsSite(string path, string siteTitle)
        {
            Logger.Info("SiteService.IsExistsSite start.");
            Result _ret = new Result();

            if (String.IsNullOrEmpty(siteTitle))
            {
                Logger.Info("parameterezes hiba!");
                _ret.ErrorCode = "320002";
                return _ret;
            }

            string SPUrl = Utility.GetSharePointUrl();
            string siteCollectionUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SiteCollectionUrl");
            string rootDoktarSite = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("DokumentumtarSite");

            if (path.StartsWith("/")) path = path.Substring(1).Trim();
            if (!path.EndsWith("/")) path += "/";

            path = String.Format("{0}{1}{2}", siteCollectionUrl, rootDoktarSite, path);

            SPWebs.Webs websService = new SPWebs.Webs();
            websService.Url = SPUrl + path + @"_vti_bin/webs.asmx";
            websService.PreAuthenticate = true;
            websService.Credentials = Utility.GetCredential();


            XmlNode sites = null;

            try
            {
                Logger.Info("websService.GetWebCollection elott");
                sites = websService.GetWebCollection();
                bool letezik = false;

                //Console.WriteLine("E: {0}", sites.OuterXml.ToString());

                Logger.Info("node-ok feldolgozasa elott");
                foreach (XmlNode node in sites.SelectNodes("*"))
                {
                    if (siteTitle.Equals(node.Attributes["Title"].Value.ToString()))
                    {
                        letezik = true;
                        break;

                    }
                }

                _ret.Record = letezik;
            }
            catch (SoapException spe)
            {
                _ret.ErrorCode = "1111111SOAP";
                _ret.ErrorMessage = spe.ToString();
                Logger.Info(String.Format("SoapException message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
                Logger.Info("Kapott parameterek: siteTitle=" + siteTitle);
            }
            catch (Exception ex)
            {
                _ret.ErrorCode = "30001000";
                _ret.ErrorMessage = ex.StackTrace.ToString();
                Logger.Info("Exception: " + ex.ToString());
                Logger.Info("Kapott parameterek: siteTitle=" + siteTitle);
            }

            return _ret;
        }

        /// <summary>
        /// Ez jo.
        /// </summary>
        /// <returns></returns>
        private string GetLanguages()
        {
            Logger.Info("SiteService.GetLanguages start.");
            SPAdmins.Admin admin = new SPAdmins.Admin();

            admin.PreAuthenticate = true;
            admin.Credentials = Utility.GetCredential();
            admin.Url = Utility.GetSharePointAdminUrl() + @"_vti_adm/Admin.asmx";

            string ret = "1033"; //default ertek  :)

            try
            {
                XmlNode xxx = admin.GetLanguages();
                ret = xxx.FirstChild.InnerXml.ToString();
            }
            catch (SoapException spe)
            {
                Logger.Info("SoapException:" +
                String.Format("Message:\n{0}\nDetail:\n{1}\nStackTrace:\n{2}\n", spe.Message, spe.Detail.InnerText, spe.StackTrace));
            }
            catch (Exception ex)
            {
                Logger.Info("Exception!!! Default erteket adott vissza: " + ret);
                Logger.Info(ex.ToString());
            }

            return ret;
        }
    }

}