﻿using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Net;
using System.Web.Configuration;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Contentum.eDocument.SharePoint
{

    public class Utility
    {

        public const string Header = "$Header: Utility.cs, 19, 2010.09.24 11:03:18, Boda Eszter$";
        public const string Version = "$Revision: 19$";

        private static string _keyWordSharePointAdminUrl = "SharePointAdminUrl";
        private static string _keyWordSharePointUrl = "SharePointUrl";
        private static string _keyWordSharePointUserDomain = "SharePointUserDomain";
        private static string _keyWordSharePointUserName = "SharePointUserName";
        private static string _keyWordSharePointPassword = "SharePointPassword";

        public Utility()
        {
        }

        private static string GetAppSetting(string Name)
        {
            string _ret;
            _ret = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(Name);
            //_ret = ConfigurationSettings.AppSettings.Get(Name);
            if (_ret == null)
                _ret = "";
            return _ret;
        }

        public static string SharePointUrlPostfixRootSiteUrl()
        {
            string siteCollectionUrl = GetAppSetting("SiteCollectionUrl");
            if (!siteCollectionUrl.EndsWith("/") && !String.IsNullOrEmpty(siteCollectionUrl)) { siteCollectionUrl += "/"; }

            return siteCollectionUrl;
        }

        public static string SharePointUrlPostfixDokumentumtarSiteUrl()
        {
            string rootDoktarSite = GetAppSetting("DokumentumtarSite");
            if (!rootDoktarSite.EndsWith("/") && !String.IsNullOrEmpty(rootDoktarSite)) { rootDoktarSite += "/"; }

            return rootDoktarSite;
        }

        public static string SharePointUrlPostfix()
        {
            //return String.Format("{0}{1}", SharePointUrlPostfixRootSiteUrl(), SharePointUrlPostfixDokumentumtarSiteUrl());
            return String.Format("{0}", SharePointUrlPostfixRootSiteUrl());
        }



        public static string GetSharePointAdminUrl()
        {
            string ret = GetAppSetting(_keyWordSharePointAdminUrl);
            if (!ret.EndsWith("/")) { ret += "/"; }
            return ret;
        }

        public static string GetSharePointUrl()
        {
            string ret = GetAppSetting(_keyWordSharePointUrl);
            if (!ret.EndsWith("/")) { ret += "/"; }
            return ret;
        }

        public static string GetSharePointUserName()
        {
            return GetAppSetting(_keyWordSharePointUserName);
        }

        public static string GetSharePointUserDomain()
        {
            return GetAppSetting(_keyWordSharePointUserDomain);
        }

        public static string GetSharePointPassword()
        {
            return GetAppSetting(_keyWordSharePointPassword);
        }

        public static NetworkCredential GetCredential()
        {
            return new NetworkCredential(GetSharePointUserName(), GetSharePointPassword(), GetSharePointUserDomain());
        }

        /// <summary>
        /// A field neveket a SharePoint érdekesen kezeli. Ékezetes betűkön a következő konverziót kell 
        /// végrehajtanunk, mielőtt hozzálátnánk a kérés összeállításához: 
        /// Karakter >> _x + Unicode kód lowercase-ben + _
        /// Pl.) é >> _x00e9_
        /// Eme fuggveny ezt csinalja.
        /// </summary>
        /// <param name="forras"></param>
        /// <returns></returns>
        public static string GetEkezetesBetukAtalakitasa(string forras)
        {
            string retVal = "";

            foreach (char c in forras)
            {
                retVal += ((int)c >= 128) ? String.Format("_x{0:x4}_", (int)c).ToLower() : String.Format("{0}", c);
            }

            return retVal;
        }

        /// <summary>
        /// Egy megadott ekezetes stringet konvertal at ekezettelenne.
        /// A space helyere _ (sutty) jelet rak.
        /// 
        /// Ezt fokent a docLib letrehozasanal hasznaljuk.
        /// 
        /// Van egy ugyanilyen az AxisFileUploadban (az SPSen)! Ha ez modosul, ott is kell molyolni!
        /// </summary>
        /// <param name="forras">Az atkonveralando string.</param>
        /// <returns>String visszateresi ertek.</returns>
        /// ConvertToIdentifier - 
        public static string ConvertToUrl(string forras)
        {
            char[] mit = { '-', ' ', 'á', 'é', 'ű', 'û', 'ő', 'ô', 'ú', 'ö', 'ü', 'ó', 'í',
                                      'Á', 'É', 'Ű', 'Û', 'Ő', 'Ô', 'Ú', 'Ö', 'Ü', 'Ó', 'Í' };
            char[] mire = { '_', '_', 'a', 'e', 'u', 'u', 'o', 'o', 'u', 'o', 'u', 'o', 'i',
                                       'A', 'E', 'U', 'U', 'O', 'O', 'U', 'O', 'U', 'O', 'I' };

            string eredmeny = "";

            foreach (char item in forras.ToCharArray())
            {

                int idx = -1;
                int i = 0;
                while (idx == -1 && i < mit.Length)
                {
                    if (mit[i].Equals(item))
                        idx = i;

                    i++;
                }

                char b = (idx == -1) ? item : mire[idx];

                eredmeny += Convert.ToString(b);
            }

            foreach (char item in GetamiNemKell())
            {
                eredmeny = eredmeny.Replace(Convert.ToString(item), "");
            }

            return eredmeny;
        }

        public static char[] GetamiNemKell()
        {
            //  par dolog, ami tuti nem kell
            char[] amiNemKell = { '$', '%', '?', '!', '+', '/', '\\', '[', ']', '(', ')', ':', '#', '*', '^', '=', '@', '&', '>', '<', '|', ',', ';', '"', '`', '~', '-' };
            return amiNemKell;
        }

        public static char[] GetamiTiltott()
        {
            //  ami tilott, ez egyezik a nemKell-el amugy, illetve ebben nincs benne a - jel
            char[] amiTiltott = { '$', '%', '?', '!', '+', '/', '\\', '[', ']', '(', ')', ':', '#', '*', '^', '=', '@', '&', '>', '<', '|', ',', ';', '"', '`', '~'  };
            return amiTiltott;
        }

        /// <summary>
        /// byte tombon SHA1 hash kiszamolasa.
        /// </summary>
        /// <param name="cont">byte tomb - ide amirol kerunk hash-t</param>
        /// <param name="enc">karakter kodolas - </param>
        /// <returns>hash string</returns>
        public static string CalculateSHA1(byte[] cont)
        {
            Logger.Info("Contentum.eDocument.SharePoint.Utility.CalculateSHA1 (byte  tombos) indul.");
            SHA1CryptoServiceProvider cryptoTransformSHA1 = new SHA1CryptoServiceProvider();
            return BitConverter.ToString(cryptoTransformSHA1.ComputeHash(cont)).Replace("-", "");
        }

        /// <summary>
        /// stringen SHA1 hash kiszamolasa
        /// </summary>
        /// <param name="cont">string szoveg, amire hash-t kerunk</param>
        /// <param name="enc">karakterkodolas</param>
        /// <returns>hash string</returns>
        public static string CalculateSHA1(string scont, Encoding enc)
        {

            //  enc-re példa:
            //  System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

            Logger.Info("Contentum.eDocument.SharePoint.Utility.CalculateSHA1 (ences stringes) indul.");
            byte[] buffer = enc.GetBytes(scont);
            //byte[] buffer = Convert.FromBase64String(scont);
            return CalculateSHA1(buffer);
        }

        /// <summary>
        /// stringen SHA1 hash kiszamolasa, a beadott stringet UTF8-al konvertalja
        /// </summary>
        /// <param name="cont">string szoveg, amire hash-t kerunk</param>
        /// <returns>hash string</returns>
        public static string CalculateSHA1WithUTF8(string scont)
        {
            Logger.Info("Contentum.eDocument.SharePoint.Utility.CalculateSHA1WithUTF8 (sima stringes) indul.");
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            return CalculateSHA1(scont, encoder);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string RemoveSlash(string s)
        {
            string x = s.Trim().StartsWith("/") ? s.Trim().Substring(1) : s.Trim();
            return x.EndsWith("/") ? x.Substring(0, x.Length - 1) : x;
        }

        /// <summary>
        /// Ervenyes-e a megadott string, minf filenev.
        /// Ha hiba van, a hibaszoveggel ter vissza. Ha miden rendben, akkor a beadott doknevet adja vissza.
        /// </summary>
        /// <param name="dokNev"></param>
        /// <returns></returns>
        public static string validIktatandoDokumentumNeve(string dokNev)
        {
            string retVal = dokNev.Trim();

            if (!String.IsNullOrEmpty(dokNev))
            {
                // ^[^\\\./:\*\?\"<>\|]{1}[^\\/:\*\?\"<>\|]{0,254}$
                Regex regexPattern = new Regex(@"^[^\\\./:\*\?\" + "\"" + @"<>\|]{1}[^\\/:\*\?\" + "\"" + @"<>\|]{0,254}$");
                if (!regexPattern.IsMatch(retVal))
                {
                    retVal = "A dokumentum neve nem megengedett karaktereket tartalmaz!";
                }
                if (retVal.StartsWith(".") || retVal.IndexOf("..") != -1)
                {
                    retVal = "A dokumentum neve nem kezdőhet ponttal és/vagy nem tartalmazhat két pontot közvetlenül egymás mellett!";
                }
            }
            else
            {
                retVal = "A dokumentum nevét meg kell adni!";
            }

            return retVal;
        }

    } //class
}