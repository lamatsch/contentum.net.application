﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using System.Xml;

// Kriszta csodálkozó felkiáltása: Azt a meredt egeret!!!!!

/// <summary>
/// Summary description for VersionService
/// </summary>
namespace Contentum.eDocument.SharePoint
{


    public class VersionService
    {
        public const string Header = "$Header: VersionService.cs, 9, 2010.02.17 13:52:30, Varsanyi P?ter$";
        public const string Version = "$Revision: 9$";

        private const string xmlResultNs = "http://schemas.microsoft.com/sharepoint/soap/";

        //  ezt a GetCurrentVersion allitja. A visszaadott verziok szamat tartalmazza.
        private int latestGetCVResultRowCount = 0;
        //  ezt a GetCurrentVersion allitja. Az aktualis elotti verziot adja vissza, ha van.
        private string latestGetCVResultPrevVersion = String.Empty;
        private string latestGetCVResultPrevVersionUrl = String.Empty;

        private string SPUrl = null;
        private SPVersions.Versions webService = null;

        public VersionService()
        {
            Logger.Info(String.Format("DocumentService.SharePoint.VersionService start.- {0} {1}", Header, Version));
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SPUrl = Utility.GetSharePointUrl();

            this.webService = new SPVersions.Versions();
            this.webService.Url = this.SPUrl + Utility.SharePointUrlPostfixRootSiteUrl() + "_vti_bin/versions.asmx";
            this.webService.Credentials = Utility.GetCredential(); //new NetworkCredential("contentumspuser", "123456", "axis"); //System.Net.CredentialCache.DefaultCredentials; //Utility.GetCredential();

            Logger.Info(String.Format("VersionService.InitializeComponent"));
            Logger.Info(String.Format("webService.Url={0}", this.webService.Url));
        }

        public int LatestGetCVResultRowCount
        {
            get { return this.latestGetCVResultRowCount; }
        }

        public string LatestGetCVResultPrevVersion
        {
            get { return this.latestGetCVResultPrevVersion; }
        }

        public string LatestGetCVResultPrevVersionUrl
        {
            get { return this.latestGetCVResultPrevVersionUrl; }
        }

        /// <summary>
        /// Az aktualis verzio szamat adja vissza (stringkent a Result.Uid-ban).
        /// </summary>
        /// <param name="itemSitePath">Az item site eleresi utja (gepnevvel, mindennel).</param>
        /// <param name="itemFullUrl">A file maradek (doclib/folders/itemNeve.kiterjesztese) eleresi utja, a file nevevel.</param>
        /// <returns>verziszam stringkent a Result.Uid-ban</returns>
        
        public Result GetCurrentVersion(string itemSitePath, string itemFullUrl)
        {
            Logger.Info(String.Format("VersionService.GetCurrentVersion start"));

            Result ret = new Result();

            //  megvan-e minden parameter
            if (String.IsNullOrEmpty(itemFullUrl)
                )
            {
                Logger.Info("parameterezes hiba!");
                ret.ErrorCode = "300002";
                ret.ErrorMessage = "Hiányzó paraméter!";
                return ret;
            }

            //  a verziok az eredmenybe kerulnek
            XmlNode eredmeny = null;

            // verziok lekerdezese
            try
            {
                string newWSUrl = String.Format("{0}_vti_bin/versions.asmx", (!itemSitePath.EndsWith("/") && !String.IsNullOrEmpty(itemSitePath)) ? itemSitePath + "/" : itemSitePath);
                this.webService.Url = newWSUrl;

                Logger.Info(String.Format("Version ws NEW url: {0}", newWSUrl));

                eredmeny = this.webService.GetVersions(itemFullUrl);

                Logger.Info(String.Format("versions eredmeny xml: {0}", eredmeny.OuterXml));
            }
            catch (SoapException soaex)
            {
                Logger.Error(String.Format("webService.GetVersions soapexception!!!"));
                Logger.Error(String.Format("itemFullUrl parameter: {1}, {0}", itemFullUrl, itemSitePath));
                Logger.Error(String.Format("Detail:{0}\nInnerException: {1}\nMessage: {2}\nStackTrace: {3}", soaex.Detail, soaex.InnerException.Message, soaex.Message, soaex.StackTrace));
                ret.ErrorCode = "SOAPEXC00001";
                ret.ErrorMessage = soaex.Message;
                return ret;
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("this.webService.GetVersions exception!!!"));
                Logger.Error(String.Format("itemFullUrl parameter: {1}, {0}", itemFullUrl, itemSitePath));
                Logger.Error(String.Format("InnerException: {0}\nMessage: {1}\nStackTrace: {2}", ex.InnerException.Message, ex.Message, ex.StackTrace));
                ret.ErrorCode = "EXC00001";
                ret.ErrorMessage = ex.Message;
                return ret;
            }

            XmlNamespaceManager xmlNsMan = new XmlNamespaceManager(new XmlDocument().NameTable);
            xmlNsMan.AddNamespace("a", xmlResultNs);

            int darab = eredmeny.SelectNodes("/a:result[starts-with(@version,'@')]", xmlNsMan).Count;

            if (darab == 1)
            {
                XmlNode aktResultNode = eredmeny.SelectSingleNode("/a:result[starts-with(@version,'@')]", xmlNsMan);
                ret.Uid = aktResultNode.Attributes["version"].Value.ToString().Substring(1);
            }
            else
            {
                Logger.Error(String.Format("Nem egy (hanem {0}) darabot talált a NodeSelect-t!", darab));
                ret.ErrorCode = "SELNODE0001";
                ret.ErrorMessage = String.Format("Nem egy (hanem {0}) darabot talált a NodeSelect-t!", darab);
            }

            // verzio darabszamok lekerdezese
            this.latestGetCVResultPrevVersion = String.Empty;
            this.latestGetCVResultPrevVersionUrl = String.Empty;
            this.latestGetCVResultRowCount = eredmeny.SelectNodes("/a:result", xmlNsMan).Count;
            
            if (this.latestGetCVResultRowCount > 2) 
            {
                //XmlNode lastItem = null;
                //foreach (XmlNode item in eredmeny.SelectNodes("/a:result", xmlNsMan))
                //{
                //     lastItem = item;
                //}
                //if (lastItem != null) 
                //{
                //    this.latestGetCVResultPrevVersion = Convert.ToString(lastItem.Attributes["version"].Value);
                //    this.latestGetCVResultPrevVersionUrl = Convert.ToString(lastItem.Attributes["url"].Value);
                //}
                XmlNodeList ndResultList = eredmeny.SelectNodes("/a:result", xmlNsMan);
                this.latestGetCVResultPrevVersion = Convert.ToString(ndResultList[2].Attributes["version"].Value);
                this.latestGetCVResultPrevVersionUrl = Convert.ToString(ndResultList[2].Attributes["url"].Value);
            }
 
            return ret;
        }

        public Result GetCurrentVersion(String sitePath, String docLibName, String folderPath, String fileName)
        {

            Logger.Info(String.Format("VersionService.GetCurrentVersion (2) start"));
            Logger.Info(String.Format("Parameters: sitePath: {0} - docLibName: {1} - folderPath: {2} - fileName: {3}", sitePath, docLibName, folderPath, fileName));

            Result ret = new Result();

            //  a verziok az eredmenybe kerulnek
            XmlDocument eredmeny = new XmlDocument();

            // verziok lekerdezese
            try
            {
                Logger.Info("GetAxisUploadToSps indul.");

                string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
                string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
                string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");

                Logger.Info(String.Format("Upload Credentials: {0}\\{1}", __doamin, __usernev));

                AxisUploadToSps.Upload upld = new AxisUploadToSps.Upload();
                upld.Url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AxisUploadToSps.upload");

                Logger.Info(String.Format("Upload url: {0}", upld.Url));

                upld.Credentials = new System.Net.NetworkCredential(__usernev, __password, __doamin);

                String res = upld.GetItemVersions(sitePath, docLibName, folderPath, fileName);

                try
                {
                    eredmeny.LoadXml(res);
                }
                catch (Exception ex)
                {
                    Logger.Error(String.Format("Hiba az GetItemVersions altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                    ret.ErrorCode = "VERSXML0001";
                    ret.ErrorMessage = "Hiba az upload függvény által visszaadott string xml konvertálásakor!";
                }

                if (!String.IsNullOrEmpty(eredmeny.SelectSingleNode("//errorcode").InnerText))
                {
                    ret.ErrorCode = eredmeny.SelectSingleNode("//errorcode").InnerText;
                    ret.ErrorMessage = eredmeny.SelectSingleNode("//errormessage").InnerText;
                    Logger.Error(String.Format("Hiba az spsen levo GetItemVersions fuggvenyben! Ezt adta vissza: {0}\n{1}", ret.ErrorCode, ret.ErrorMessage));
                    return ret;
                }

                Logger.Info(String.Format("versions eredmeny xml: {0}", eredmeny.OuterXml));

            }
            catch (SoapException soaex)
            {
                Logger.Error(String.Format("webService.GetVersions soapexception!!!"));
                Logger.Error(String.Format("Detail:{0}\nInnerException: {1}\nMessage: {2}\nStackTrace: {3}", soaex.Detail, soaex.InnerException.Message, soaex.Message, soaex.StackTrace));
                ret.ErrorCode = "SOAPEXC00001";
                ret.ErrorMessage = soaex.Message;
                return ret;
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("this.webService.GetVersions exception!!!"));
                Logger.Error(String.Format("InnerException: {0}\nMessage: {1}\nStackTrace: {2}", ex.InnerException.Message, ex.Message, ex.StackTrace));
                ret.ErrorCode = "EXC00001";
                ret.ErrorMessage = ex.Message;
                return ret;
            }

            XmlNamespaceManager xmlNsMan = new XmlNamespaceManager(new XmlDocument().NameTable);
            xmlNsMan.AddNamespace("a", xmlResultNs);

            int darab = eredmeny.SelectNodes("//result[starts-with(@version,'@')]").Count;

            if (darab == 1)
            {
                XmlNode aktResultNode = eredmeny.SelectSingleNode("//result[starts-with(@version,'@')]");
                ret.Uid = aktResultNode.Attributes["version"].Value.ToString().Substring(1);
            }
            else
            {
                Logger.Error(String.Format("Nem egy (hanem {0}) darabot talált a NodeSelect-t!", darab));
                ret.ErrorCode = "SELNODE0001";
                ret.ErrorMessage = String.Format("Nem egy (hanem {0}) darabot talált a NodeSelect-t!", darab);
            }

            // verzio darabszamok lekerdezese
            this.latestGetCVResultPrevVersion = String.Empty;
            this.latestGetCVResultPrevVersionUrl = String.Empty;
            this.latestGetCVResultRowCount = eredmeny.SelectNodes("//result", xmlNsMan).Count;

            if (this.latestGetCVResultRowCount > 2)
            {
                XmlNodeList ndResultList = eredmeny.SelectNodes("//result", xmlNsMan);
                this.latestGetCVResultPrevVersion = Convert.ToString(ndResultList[2].Attributes["version"].Value);
                this.latestGetCVResultPrevVersionUrl = Convert.ToString(ndResultList[2].Attributes["url"].Value);
            }

            return ret;
        }

        /// <summary>
        /// Egy megadott URL es verzio alapjan visszaadja a tenyleges eleresi utat.
        /// </summary>
        /// <param name="actualItemFullUrl">Az item aktualis teljese eleresi utja</param>
        /// <param name="versionNumber">A verzio szama.</param>
        /// <returns></returns>
        
        public Result GetVersionUrl(string actualItemFullUrl, string versionNumber)
        {
            Logger.Info(String.Format("VersionService.GetVersionUrl start"));

            Result ret = new Result();

            //  megvan-e minden parameter
            if (String.IsNullOrEmpty(actualItemFullUrl) ||
                String.IsNullOrEmpty(versionNumber)
                )
            {
                Logger.Info("parameterezes hiba!");
                ret.ErrorCode = "300002";
                ret.ErrorMessage = "Hiányzó paraméter!";
                return ret;
            }

            //  a verziok az eredmenybe kerulnek
            XmlNode eredmeny = null;

            // verziok lekerdezese
            try
            {
                eredmeny = this.webService.GetVersions(actualItemFullUrl);
            }
            catch (SoapException soaex)
            {
                Logger.Error(String.Format("this.webService.GetVersionUrl soapexception!!!"));
                Logger.Error(String.Format("actualItemFullUrl parameter: {0}", actualItemFullUrl));
                Logger.Error(String.Format("versionNumber parameter: {0}", versionNumber));
                Logger.Error(String.Format("Detail:{0}\nInnerException: {1}\nMessage: {2}\nStackTrace: {3}", soaex.Detail, soaex.InnerException, soaex.Message, soaex.StackTrace));
                ret.ErrorCode = "SOAPEXC00001";
                ret.ErrorMessage = soaex.Message;
                return ret;
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("this.webService.GetVersionUrl exception!!!"));
                Logger.Error(String.Format("actualItemFullUrl parameter: {0}", actualItemFullUrl));
                Logger.Error(String.Format("versionNumber parameter: {0}", versionNumber));
                Logger.Error(String.Format("InnerException: {0}\nMessage: {1}\nStackTrace: {2}", ex.InnerException, ex.Message, ex.StackTrace));
                ret.ErrorCode = "EXC00001";
                ret.ErrorMessage = ex.Message;
                return ret;
            }

            XmlNamespaceManager xmlNsMan = new XmlNamespaceManager(new XmlDocument().NameTable);
            xmlNsMan.AddNamespace("a", xmlResultNs);

            int darab = eredmeny.SelectNodes(String.Format("//result[contains(@version,'{0}')]", versionNumber)).Count;

            if (darab == 1)
            {
                XmlNode aktResultNode = eredmeny.SelectSingleNode(String.Format("/a:result[contains(@version,'{0}')]", versionNumber), xmlNsMan);
                ret.Uid = aktResultNode.Attributes["url"].Value.ToString();
            }
            else
            {
                Logger.Error(String.Format("Nem egy (hanem {0}) darabot talált a NodeSelect-t!", darab));
                ret.ErrorCode = "SELNODE0001";
                ret.ErrorMessage = String.Format("Nem egy (hanem {0}) darabot talált a NodeSelect-t!", darab);
            }

            return ret;
        }

        public Result GetVersionUrl(String sitePath, String docLibName, String folderPath, String fileName, String versionNumber)
        {
            Logger.Info(String.Format("VersionService.GetVersionUrl (2) start"));
            Logger.Info(String.Format("Parameters: sitePath: {0} - docLibName: {1} - folderPath: {2} - fileName: {3} - versionNumber: {4}", sitePath, docLibName, folderPath, fileName, versionNumber));

            Result ret = new Result();

            //  a verziok az eredmenybe kerulnek
            XmlDocument eredmeny = new XmlDocument();

            // verziok lekerdezese
            try
            {
                Logger.Info("GetAxisUploadToSps indul.");

                string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
                string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
                string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");

                Logger.Info(String.Format("Upload Credentials: {0}\\{1}", __doamin, __usernev));

                AxisUploadToSps.Upload upld = new AxisUploadToSps.Upload();
                upld.Url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AxisUploadToSps.upload");

                Logger.Info(String.Format("Upload url: {0}", upld.Url));

                upld.Credentials = new System.Net.NetworkCredential(__usernev, __password, __doamin);

                String res = upld.GetItemVersions(sitePath, docLibName, folderPath, fileName);

                try
                {
                    eredmeny.LoadXml(res);
                }
                catch (Exception ex)
                {
                    Logger.Error(String.Format("Hiba az GetItemVersions altal visszaadott xml string konvertalaskor!\nAmit visszaadott: {0}\nHiba: {1}\n{2}", res, ex.Message, ex.StackTrace));
                    ret.ErrorCode = "VERSXML0001";
                    ret.ErrorMessage = "Hiba az upload függvény által visszaadott string xml konvertálásakor!";
                }

                if (!String.IsNullOrEmpty(eredmeny.SelectSingleNode("//errorcode").InnerText))
                {
                    ret.ErrorCode = eredmeny.SelectSingleNode("//errorcode").InnerText;
                    ret.ErrorMessage = eredmeny.SelectSingleNode("//errormessage").InnerText;
                    Logger.Error(String.Format("Hiba az spsen levo GetItemVersions fuggvenyben! Ezt adta vissza: {0}\n{1}", ret.ErrorCode, ret.ErrorMessage));
                    return ret;
                }
            }
            catch (SoapException soaex)
            {
                Logger.Error(String.Format("this.webService.GetVersionUrl soapexception!!!"));
                Logger.Error(String.Format("Detail:{0}\nInnerException: {1}\nMessage: {2}\nStackTrace: {3}", soaex.Detail, soaex.InnerException, soaex.Message, soaex.StackTrace));
                ret.ErrorCode = "SOAPEXC00001";
                ret.ErrorMessage = soaex.Message;
                return ret;
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("this.webService.GetVersionUrl exception!!!"));
                Logger.Error(String.Format("InnerException: {0}\nMessage: {1}\nStackTrace: {2}", ex.InnerException, ex.Message, ex.StackTrace));
                ret.ErrorCode = "EXC00001";
                ret.ErrorMessage = ex.Message;
                return ret;
            }

            XmlNamespaceManager xmlNsMan = new XmlNamespaceManager(new XmlDocument().NameTable);
            xmlNsMan.AddNamespace("a", xmlResultNs);

            int darab = eredmeny.SelectNodes(String.Format("//result[contains(@version,'{0}')]", versionNumber)).Count;

            if (darab == 1)
            {
                XmlNode aktResultNode = eredmeny.SelectSingleNode(String.Format("//result[contains(@version,'{0}')]", versionNumber));
                ret.Uid = aktResultNode.Attributes["url"].Value.ToString();
            }
            else
            {
                Logger.Error(String.Format("Nem egy (hanem {0}) darabot talált a NodeSelect-t!", darab));
                ret.ErrorCode = "SELNODE0001";
                ret.ErrorMessage = String.Format("Nem egy (hanem {0}) darabot talált a NodeSelect-t!", darab);
            }

            return ret;
        }

    }

}