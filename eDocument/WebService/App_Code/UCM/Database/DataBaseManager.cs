﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace NMHH.UCM.Database
{
    /// <summary>
    /// Summary description for DataBaseManager
    /// </summary>
    public class DataBaseManager
    {
        public static String GetConnectionString()
        {
            ConnectionStringSettings conn = new ConnectionStringSettings();
            conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["UCMConnectionString"];

            return conn.ConnectionString;
        }

        public DataBaseManager()
        {
        }

        public StoredProcedureResult Felh_Hozaad_Iktszam(string jog_felh_nid, string ugyiratszam, string alszam, string jog, string mod_felh_nid, string rendszerkod)
        {
            Logger.Info("DataBaseManager.Felh_Hozaad_Iktszam kezdete");
            Logger.Debug(String.Format("jog_felh_nid={0}", jog_felh_nid));

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                string _cmd = "idc.dms_jogosultsag_pkg.felh_hozzaad_iktszam";

                OracleCommand cmd = new OracleCommand(_cmd, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("p_jog_felh_nid", jog_felh_nid));
                cmd.Parameters.Add(new OracleParameter("p_ugyiratszam", ugyiratszam));
                cmd.Parameters.Add(new OracleParameter("p_alszam", String.IsNullOrEmpty(alszam) ? "0" : alszam));
                cmd.Parameters.Add(new OracleParameter("p_jog", jog));
                cmd.Parameters.Add(new OracleParameter("p_mod_felh_nid", mod_felh_nid));
                cmd.Parameters.Add(new OracleParameter("p_rendszerkod", rendszerkod));
                cmd.Parameters.Add(new OracleParameter("p_result", OracleDbType.Varchar2, 4000));
                cmd.Parameters["p_result"].Direction = ParameterDirection.Output;


                cmd.ExecuteNonQuery();

                string result = cmd.Parameters["p_result"].Value.ToString();

                Logger.Info("DataBaseManager.Felh_Hozaad_Iktszam vege");

                return new StoredProcedureResult(result);

            }
        }

        public StoredProcedureResult Felh_List_Alk_Iktszam(string jog_felh_nids, string ugyiratszam, string alszam, string jog, string mod_felh_nid, string rendszerkod)
        {
            Logger.Info("DataBaseManager.Felh_List_Alk_Iktszam kezdete");

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                string _cmd = "idc.dms_jogosultsag_pkg.felh_list_alk_iktszam";

                OracleCommand cmd = new OracleCommand(_cmd, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("p_jog_felh_nids", jog_felh_nids));
                cmd.Parameters.Add(new OracleParameter("p_ugyiratszam", ugyiratszam));
                cmd.Parameters.Add(new OracleParameter("p_alszam", alszam));
                cmd.Parameters.Add(new OracleParameter("p_jog", jog));
                cmd.Parameters.Add(new OracleParameter("p_mod_felh_nid", mod_felh_nid));
                cmd.Parameters.Add(new OracleParameter("p_rendszerkod", rendszerkod));
                cmd.Parameters.Add(new OracleParameter("p_result", OracleDbType.Varchar2, 4000));
                cmd.Parameters["p_result"].Direction = ParameterDirection.Output;


                cmd.ExecuteNonQuery();

                string result = cmd.Parameters["p_result"].Value.ToString();

                Logger.Info("DataBaseManager.Felh_List_Alk_Iktszam vege");

                return new StoredProcedureResult(result);

            }
        }

        public StoredProcedureResult Felh_List_Alk_Ugyszam(string jog_felh_nids, string ugyiratszam, string jog, string mod_felh_nid, string rendszerkod)
        {
            Logger.Info("DataBaseManager.Felh_List_Alk_Ugyszam kezdete");

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                string _cmd = "idc.dms_jogosultsag_pkg.felh_list_alk_ugyszam";

                OracleCommand cmd = new OracleCommand(_cmd, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("p_jog_felh_nids", jog_felh_nids));
                cmd.Parameters.Add(new OracleParameter("p_ugyiratszam", ugyiratszam));
                cmd.Parameters.Add(new OracleParameter("p_jog", jog));
                cmd.Parameters.Add(new OracleParameter("p_mod_felh_nid", mod_felh_nid));
                cmd.Parameters.Add(new OracleParameter("p_rendszerkod", rendszerkod));
                cmd.Parameters.Add(new OracleParameter("p_result", OracleDbType.Varchar2, 4000));
                cmd.Parameters["p_result"].Direction = ParameterDirection.Output;


                cmd.ExecuteNonQuery();

                string result = cmd.Parameters["p_result"].Value.ToString();

                Logger.Info("DataBaseManager.Felh_List_Alk_Ugyszam vege");

                return new StoredProcedureResult(result);

            }
        }

        public StoredProcedureResult Felh_Torol_Iktszam(string jog_felh_nid, string ugyiratszam, string alszam, string mod_felh_nid, string rendszerkod)
        {
            Logger.Info("DataBaseManager.Felh_Torol_Iktszam kezdete");

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                string _cmd = "idc.dms_jogosultsag_pkg.felh_torol_iktszam";

                OracleCommand cmd = new OracleCommand(_cmd, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("p_jog_felh_nid", jog_felh_nid));
                cmd.Parameters.Add(new OracleParameter("p_ugyiratszam", ugyiratszam));
                cmd.Parameters.Add(new OracleParameter("p_alszam", alszam));
                cmd.Parameters.Add(new OracleParameter("p_mod_felh_nid", mod_felh_nid));
                cmd.Parameters.Add(new OracleParameter("p_rendszerkod", rendszerkod));
                cmd.Parameters.Add(new OracleParameter("p_result", OracleDbType.Varchar2, 4000));
                cmd.Parameters["p_result"].Direction = ParameterDirection.Output;


                cmd.ExecuteNonQuery();

                string result = cmd.Parameters["p_result"].Value.ToString();

                Logger.Info("DataBaseManager.Felh_Torol_Iktszam vege");

                return new StoredProcedureResult(result);

            }
        }

        public StoredProcedureResult Felh_Torol_Ugyszam(string jog_felh_nid, string ugyiratszam, string mod_felh_nid, string rendszerkod)
        {
            Logger.Info("DataBaseManager.Felh_Torol_Ugyszam kezdete");

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                string _cmd = "idc.dms_jogosultsag_pkg.felh_torol_ugyszam";

                OracleCommand cmd = new OracleCommand(_cmd, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("p_jog_felh_nid", jog_felh_nid));
                cmd.Parameters.Add(new OracleParameter("p_ugyiratszam", ugyiratszam));
                cmd.Parameters.Add(new OracleParameter("p_mod_felh_nid", mod_felh_nid));
                cmd.Parameters.Add(new OracleParameter("p_rendszerkod", rendszerkod));
                cmd.Parameters.Add(new OracleParameter("p_result", OracleDbType.Varchar2, 4000));
                cmd.Parameters["p_result"].Direction = ParameterDirection.Output;


                cmd.ExecuteNonQuery();

                string result = cmd.Parameters["p_result"].Value.ToString();

                Logger.Info("DataBaseManager.Felh_Torol_Ugyszam vege");

                return new StoredProcedureResult(result);

            }
        }

        public StoredProcedureResult Felh_Torol(string jog_felh_nid, string mod_felh_nid, string rendszerkod)
        {
            Logger.Info("DataBaseManager.Felh_Torol kezdete");

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                string _cmd = "idc.dms_jogosultsag_pkg.felh_torol";

                OracleCommand cmd = new OracleCommand(_cmd, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("p_jog_felh_nid", jog_felh_nid));
                cmd.Parameters.Add(new OracleParameter("p_mod_felh_nid", mod_felh_nid));
                cmd.Parameters.Add(new OracleParameter("p_rendszerkod", rendszerkod));
                cmd.Parameters.Add(new OracleParameter("p_result", OracleDbType.Varchar2, 4000));
                cmd.Parameters["p_result"].Direction = ParameterDirection.Output;


                cmd.ExecuteNonQuery();

                string result = cmd.Parameters["p_result"].Value.ToString();

                Logger.Info("DataBaseManager.Felh_Torol vege");

                return new StoredProcedureResult(result);

            }
        }

        const string _cmdGetJogosultak = @"select id, ugyiratszam, alszam, login_nev, nid, jog, crd from idc.dms_jogosultsag t
                                where t.ugyiratszam = :ugyiratszam
                                and t.alszam = :alszam";
        public List<Jogosultsag> GetJogosultakList(string ugyiratszam, string alszam)
        {
            Logger.Info("DataBaseManager.GetJogosultakList kezdete");
            Logger.Debug(String.Format("ugyiratszam={0}", ugyiratszam));
            Logger.Debug(String.Format("alszam={0}", alszam));

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(_cmdGetJogosultak, conn);
                cmd.CommandType = CommandType.Text;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("ugyiratszam", ugyiratszam));
                cmd.Parameters.Add(new OracleParameter("alszam", alszam));

                DataSet ds = new DataSet();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                List<Jogosultsag> result = new List<Jogosultsag>();

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    result.Add(new Jogosultsag(row));
                }

                Logger.Info("DataBaseManager.GetJogosultakList vege");

                return result;
            }
        }
    }
}