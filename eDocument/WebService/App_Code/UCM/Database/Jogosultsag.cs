﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NMHH.UCM.Database
{
    /// <summary>
    /// Summary description for Jogosultsag
    /// </summary>
    public class Jogosultsag
    {
        public decimal Id { get; set; }
        public string Ugyiratszam { get; set; }
        public int Alszam { get; set; }
        public string Login_nev { get; set; }
        public string NId { get; set; }
        public string Jog { get; set; }
        public DateTime CRD { get; set; }

        public Jogosultsag(DataRow row)
        {
            this.Id = (decimal)row["id"];
            this.Ugyiratszam = row["ugyiratszam"].ToString();
            this.Alszam = (int)row["alszam"];
            this.Login_nev = row["login_nev"].ToString();
            this.NId = row["nid"].ToString();
            this.Jog = row["jog"].ToString();
            this.CRD = (DateTime)row["crd"];
        }
    }
}
