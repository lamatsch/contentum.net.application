﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMHH.UCM.Database
{
    /// <summary>
    /// Summary description for SoredProcedureResult
    /// </summary>
    public class StoredProcedureResult
    {
        private bool _IsError;

        public bool IsError
        {
            get { return _IsError; }
            set { _IsError = value; }
        }

        private string _ErrorMessage;

        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }

        private string _Value;

        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        public StoredProcedureResult(string result)
        {
            this.Value = result;
            if ("OK".Equals(result, StringComparison.InvariantCultureIgnoreCase))
            {
                this.IsError = false;
            }
            else
            {
                this.IsError = true;
                this.ErrorMessage = result;
            }
        }
    }
}