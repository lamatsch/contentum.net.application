﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMHH.UCM.Servlet
{
    /// <summary>
    /// Summary description for Dokumentum
    /// </summary>
    public class Dokumentum
    {
        private string _Path;

        public string Path
        {
            get { return _Path; }
            set { _Path = value; }
        }

        private string _FajlNev;

        public string FajlNev
        {
            get { return _FajlNev; }
            set { _FajlNev = value; }
        }

        private string _Ugyiratszam;

        public string Ugyiratszam
        {
            get { return _Ugyiratszam; }
            set { _Ugyiratszam = value; }
        }

        private string _Alszam;

        public string Alszam
        {
            get { return _Alszam; }
            set { _Alszam = value; }
        }

        private string _Irattervezetszam;

        public string Irattervezetszam
        {
            get { return _Irattervezetszam; }
            set { _Irattervezetszam = value; }
        }

        private string _BiztonsagiCsoport;

        public string BiztonsagiCsoport
        {
            get { return _BiztonsagiCsoport; }
            set { _BiztonsagiCsoport = value; }
        }

        private string _Meret;

        public string Meret
        {
            get { return _Meret; }
            set { _Meret = value; }
        }

        private string _Tulajdonos;

        public string Tulajdonos
        {
            get { return _Tulajdonos; }
            set { _Tulajdonos = value; }
        }

        private string _UtolsoModositasIdeje;

        public string UtolsoModositasIdeje
        {
            get { return _UtolsoModositasIdeje; }
            set { _UtolsoModositasIdeje = value; }
        }

        private string _UtolosModosito;

        public string UtolosModosito
        {
            get { return _UtolosModosito; }
            set { _UtolosModosito = value; }
        }

        private string _Verziokezelt;

        public string Verziokezelt
        {
            get { return _Verziokezelt; }
            set { _Verziokezelt = value; }
        }

        private string _Zarolo;

        public string Zarolo
        {
            get { return _Zarolo; }
            set { _Zarolo = value; }
        }

        public Dokumentum()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static Dokumentum Create(string line)
        {
            Dokumentum dok = new Dokumentum();

            string[] columns = line.Split(';');
            dok.Path = GetColumns(columns, 0);
            dok.FajlNev = GetColumns(columns, 1);
            dok.Ugyiratszam = GetColumns(columns, 2);
            dok.Alszam = GetColumns(columns, 3);
            dok.Irattervezetszam = GetColumns(columns, 4);
            dok.BiztonsagiCsoport = GetColumns(columns, 5);
            dok.Meret = GetColumns(columns, 6);
            dok.Tulajdonos = GetColumns(columns, 7);
            dok.UtolsoModositasIdeje = GetColumns(columns, 8);
            dok.UtolosModosito = GetColumns(columns, 9);
            dok.Verziokezelt = GetColumns(columns, 10);
            dok.Zarolo = GetColumns(columns, 11);

            return dok;
        }

        static string GetColumns(string[] columns, int index)
        {
            if (columns.Length > index)
                return columns[index];

            return null;
        }
    }
}