﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

/// <summary>
/// Summary description for ExtendedWebClient
/// </summary>
public class ExtendedWebClient : System.Net.WebClient
{
    public int Timeout { get; set; }

    protected override WebRequest GetWebRequest(Uri uri)
    {
        WebRequest lWebRequest = base.GetWebRequest(uri);
        lWebRequest.Timeout = Timeout;
        ((HttpWebRequest)lWebRequest).ReadWriteTimeout = Timeout;
        return lWebRequest;
    }
}