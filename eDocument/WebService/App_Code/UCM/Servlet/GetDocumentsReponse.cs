﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace NMHH.UCM.Servlet
{
    /// <summary>
    /// Summary description for GetDocumentsResult
    /// </summary>
    public class GetDocumentsReponse : ServletResponse
    {
        private string _Count;

        public string Count
        {
            get { return _Count; }
            set { _Count = value; }
        }

        private List<Dokumentum> _Dokumentumok = new List<Dokumentum>();

        public List<Dokumentum> Dokumentumok
        {
            get { return _Dokumentumok; }
            set { _Dokumentumok = value; }
        }

        public GetDocumentsReponse(ServletResponse response) : base(response.ResponseData, response.ResponseHeaders)
        {
            if (!this.IsError)
            {
                string[] lines = Regex.Split(this.ResponseString, "\r\n|\n");
                int i = 0;
                foreach (string line in lines)
                {
                    i++;
                    //első sor a találatok száma
                    if (i == 1)
                    {
                        this.Count = line;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(line))
                        {
                            this.Dokumentumok.Add(Dokumentum.Create(line));
                        }
                    }


                }
            }
        }
    }
}