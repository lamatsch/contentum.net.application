﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace NMHH.UCM.Servlet
{
    /// <summary>
    /// Summary description for GetVersionsResponse
    /// </summary>
    public class GetVersionsResponse: ServletResponse
    {
        private string _Count;

        public string Count
        {
            get { return _Count; }
            set { _Count = value; }
        }

        private List<Version> _Versions = new List<Version>();

        public List<Version> Versions
        {
            get { return _Versions; }
            set { _Versions = value; }
        }

        public GetVersionsResponse(ServletResponse response) : base(response.ResponseData, response.ResponseHeaders)
        {
            if (!this.IsError)
            {
                string[] lines = Regex.Split(this.ResponseString, "\r\n|\n");
                int i = 0;
                foreach (string line in lines)
                {
                    i++;
                    //első sor a találatok száma
                    if (i == 1)
                    {
                        this.Count = line;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(line))
                        {
                            this.Versions.Add(Version.Create(line));
                        }
                    }


                }
            }
        }
    }
}