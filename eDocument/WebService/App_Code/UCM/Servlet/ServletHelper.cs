﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace NMHH.UCM.Servlet
{
    /// <summary>
    /// Summary description for ServletHelper
    /// </summary>
    public class ServletHelper
    {
        public static string GetUgyiratSzam(string iktatokonyv, string foszam, string ev)
        {
            return String.Format("{0}-{1}-{2}", iktatokonyv, foszam, ev);
        }

        public static string GetExternalLink(string path)
        {
            HttpRequest request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + request.ApplicationPath.TrimEnd('/') + "/";
            string pageUrl = "GetDocumentContent.aspx";
            string fileName = Path.GetFileName(path);
            string qs = String.Format("path={0}", HttpUtility.UrlEncode(path));
            return baseUrl + pageUrl + "/" + EncodeFileName(fileName) + '?' + qs;
        }

        private static string EncodeFileName(string fileName)
        {
            if (String.IsNullOrEmpty(fileName))
                return String.Empty;

            fileName = HttpUtility.UrlEncode(fileName);
            fileName = fileName.Replace("+", "%20");

            return fileName;
        }

        public static DateTime? ConvertTimestampToDateTime(string value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                long milliseconds;

                if (long.TryParse(value, out milliseconds))
                {
                    return DateTimeOffsetHelper.FromUnixTimeMilliseconds(milliseconds).DateTime.ToLocalTime();
                }
            }

            return null;
        }

        public static string GetUgyiratSzam(string path, out string alszam, out string fileName)
        {
            string ugyiratszam = String.Empty;
            alszam = String.Empty;
            fileName = String.Empty;

            string[] parts = path.Split('/');

            if (parts.Length > 7)
            {
                string ikatokonyv = parts[2];
                string ev = parts[4];
                string foszam = parts[6];
                fileName = parts[7];

                ugyiratszam = GetUgyiratSzam(ikatokonyv, foszam, ev);

                Match m = Regex.Match(fileName, "^(\\d{5})_(.*)");

                if (m.Success)
                {
                    string value = m.Groups[1].Value;
                    int i;

                    if (Int32.TryParse(value, out i))
                    {
                        alszam = i.ToString();
                    }

                    fileName = m.Groups[2].Value;
                }
            }

            return ugyiratszam;
        }
    }
}