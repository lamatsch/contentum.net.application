﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Text;
using System.Net;

namespace NMHH.UCM.Servlet
{
    /// <summary>
    /// Summary description for ServletResult
    /// </summary>
    public class ServletResponse
    {

        private bool _IsError;

        public bool IsError
        {
            get { return _IsError; }
            set { _IsError = value; }
        }

        private string _ErrorCode;

        public string ErrorCode
        {
            get { return _ErrorCode; }
            set { _ErrorCode = value; }
        }
        private string _ErrorMessage;

        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }

        private byte[] _ResponseData;

        public byte[] ResponseData
        {
            get { return _ResponseData; }
            set { _ResponseData = value; }
        }

        private string _ResponseString;

        public string ResponseString
        {
            get { return _ResponseString; }
            set { _ResponseString = value; }
        }

        private WebHeaderCollection _ResponseHeaders;

        public WebHeaderCollection ResponseHeaders
        {
            get { return _ResponseHeaders; }
            set { _ResponseHeaders = value; }
        }

        public ServletResponse(byte[] responseData, WebHeaderCollection responseHeaders)
        {
            this.ResponseData = responseData;
            this.ResponseHeaders = responseHeaders;
            this.ResponseString = Encoding.UTF8.GetString(responseData);
            CheckError(this.ResponseString);

        }

        void CheckError(string response)
        {
            if (!String.IsNullOrEmpty(response))
            {
                if (response.StartsWith("ERROR"))
                {
                    this.IsError = true;

                    string[] lines = Regex.Split(response, "\n");

                    this.ErrorCode = lines[0];
                    if (lines.Length > 0)
                    {
                        this.ErrorMessage = lines[1];
                    }
                }
            }
        }
    }
}