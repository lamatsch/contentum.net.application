﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net;
using System.Text;
using System.Collections.Specialized;

namespace NMHH.UCM.Servlet
{
    /// <summary>
    /// Summary description for UCMServletManager
    /// </summary>
    public class UCMServletManager
    {
        static string servletUrl;
        static string userName;
        static string password;
        static string nid;

        public static string Nid
        {
            get
            {
                return nid;
            }
        }

        static UCMServletManager()
        {
            servletUrl = ConfigurationManager.AppSettings["UCMServlet_URL"];
            userName = ConfigurationManager.AppSettings["UCMServlet_UserName"];
            password = ConfigurationManager.AppSettings["UCMServlet_Password"];
            nid = ConfigurationManager.AppSettings["UCM_nid"];
        }

        WebClient GetWebClient()
        {
            ExtendedWebClient wc = new ExtendedWebClient();
            wc.Timeout = (int)TimeSpan.FromMinutes(20).TotalMilliseconds;
            wc.Credentials = new NetworkCredential(userName, password);
            return wc;
        }

        public string GetDocumentContentUrl(string path)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("nid", nid);
            parameters.Add("path", path);
            string url = GetUrl("getDocumentContent", parameters);
            return url;
        }

        public ServletResponse GetDocumentContent(string path)
        {
            Logger.Info("UCMServletManager.GetDocumentContent");

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("nid", nid);
            parameters.Add("path", path);
            ServletResponse response = CallMethod("getDocumentContent", parameters);
            return response;
        }

        public GetDocumentsReponse GetDocuments(string ugyiratszam, string alszam, string irattervezetszam, List<string> ugyiratszamok, string rendszerkod, string irattipuskod, string maxItems, string startIndex)
        {
            Logger.Info("UCMServletManager.GetDocuments");

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("nid", nid);
            parameters.Add("ugyiratszam", ugyiratszam);
            parameters.Add("alszam", alszam);
            parameters.Add("irattervezetszam", irattervezetszam);
            if (ugyiratszamok != null)
                parameters.Add("ugyiratszamok", String.Join(";", ugyiratszamok.ToArray()));
            parameters.Add("rendszerkod", rendszerkod);
            parameters.Add("irattipuskod", irattipuskod);

            parameters.Add("maxItems", maxItems);
            parameters.Add("startIndex", startIndex);

            ServletResponse response = CallMethod("getDocuments", parameters);
            return new GetDocumentsReponse(response);
        }

        public UploadResponse Upload(byte[] content, string name, string ugyiratszam, string description, string alszam, string irattervezetszam, string irany, string acl, string generalt, bool extended)
        {
            Logger.Info("UCMServletManager.Upload");

            Dictionary <string, string> parameters = new Dictionary<string, string>();
            parameters.Add("name", name);
            parameters.Add("nid", nid);
            parameters.Add("ugyiratszam", ugyiratszam);
            parameters.Add("description", description);
            parameters.Add("alszam", alszam);
            parameters.Add("irattervezetszam", irattervezetszam);
            parameters.Add("irany", irany);
            parameters.Add("acl", acl);
            parameters.Add("generalt", generalt);
            parameters.Add("extended", extended ? "true" : "false");

            ServletResponse response = PostMethod("upload", parameters, content);
            return new UploadResponse(response);
        }

        public GetVersionsResponse GetVersions(string path)
        {
            Logger.Info("UCMServletManager.GetDocuments");

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("nid", nid);
            parameters.Add("path", path);

            ServletResponse response = CallMethod("getVersions", parameters);
            return new GetVersionsResponse(response);
        }

        public ServletResponse Version(string path, VersionFunctions function)
        {
            Logger.Info("UCMServletManager.Version");

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("nid", nid);
            parameters.Add("path", path);
            parameters.Add("function", function.ToString());
            ServletResponse response = CallMethod("version", parameters);
            return response;
        }

        public ServletResponse DeleteDocument(string path)
        {
            Logger.Info("UCMServletManager.DeleteDocument");

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("nid", nid);
            parameters.Add("path", path);
            ServletResponse response = CallMethod("deleteDocument", parameters);
            return response;
        }

        public GetDocumentsReponse FullTextSearch(string szoveg, int? sortIndex, bool? sortOrder, int? maxItems, int? startIndex)
        {
            Logger.Info("UCMServletManager.FullTextSearch");

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("nid", nid);
            parameters.Add("szoveg", szoveg);

            if(sortIndex != null)
                parameters.Add("sortIndex", sortIndex.ToString());

            if (sortOrder != null)
                parameters.Add("sortOrder", sortOrder.ToString());

            if(maxItems != null)
                parameters.Add("maxItems", maxItems.ToString());

            if(startIndex != null)
                parameters.Add("startIndex", startIndex.ToString());

            ServletResponse response = CallMethod("fullTextSearch", parameters);
            return new GetDocumentsReponse(response);
        }

        public ServletResponse CallMethod(string method, Dictionary<string, string> parameters)
        {
            Logger.Info("UCMServletManager.CallMethod");
            string url = GetUrl(method, parameters);
            Logger.Info(String.Format("url={0}", url));
            using (WebClient wc = GetWebClient())
            {
                byte[] response = wc.DownloadData(url);
                return new ServletResponse(response, wc.ResponseHeaders);
            }
        }

        public ServletResponse PostMethod(string method, Dictionary<string, string> parameters, byte[] data)
        {
            Logger.Info("UCMServletManager.PostMethod");
            string url = GetUrl(method, parameters);
            Logger.Info(String.Format("url={0}", url));
            using (WebClient wc = GetWebClient())
            {
                byte[] response = wc.UploadData(url, data);
                return new ServletResponse(response, wc.ResponseHeaders);
            }
        }

        private string GetUrl(string method, Dictionary<string, string> parameters)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("method=");
            sb.Append(HttpUtility.UrlEncode(method));
            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                sb.Append("&");
                sb.Append(parameter.Key);
                sb.Append("=");
                sb.Append(HttpUtility.UrlEncode(parameter.Value));
            }
            string url = servletUrl + "?" + sb.ToString();
            return url;
        }
    }
}