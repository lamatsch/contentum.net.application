﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMHH.UCM.Servlet
{
    /// <summary>
    /// Summary description for UploadResponse
    /// </summary>
    public class UploadResponse : ServletResponse
    {
        private string _Path;

        public string Path
        {
            get
            {
                return _Path;
            }

            set
            {
                _Path = value;
            }
        }


        public UploadResponse(ServletResponse response) : base(response.ResponseData, response.ResponseHeaders)
        {
            if (!this.IsError)
            {
                string[] parts = this.ResponseString.Split('=');
                if (parts.Length > 1)
                    this.Path = parts[1].Trim();
            }
        }
    }
}