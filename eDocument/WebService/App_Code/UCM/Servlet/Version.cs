﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMHH.UCM.Servlet
{
    /// <summary>
    /// Summary description for Version
    /// </summary>
    public class Version
    {
        private string _DokumentumAzonosito;

        public string DokumentumAzonosito
        {
            get { return _DokumentumAzonosito; }
            set { _DokumentumAzonosito = value; }
        }

        private string _Verzio;

        public string Verzio
        {
            get { return _Verzio; }
            set { _Verzio = value; }
        }

        private string _Ugyiratszam;

        public string Ugyiratszam
        {
            get { return _Ugyiratszam; }
            set { _Ugyiratszam = value; }
        }

         private string _Alszam;

        public string Alszam
        {
            get { return _Alszam; }
            set { _Alszam = value; }
        }

        private string _Irattervezetszam;

        public string Irattervezetszam
        {
            get { return _Irattervezetszam; }
            set { _Irattervezetszam = value; }
        }

        private string _UtolosModosito;

        public string UtolosModosito
        {
            get { return _UtolosModosito; }
            set { _UtolosModosito = value; }
        }

        private string _UtolsoModositasIdeje;

        public string UtolsoModositasIdeje
        {
            get { return _UtolsoModositasIdeje; }
            set { _UtolsoModositasIdeje = value; }
        }

        public DateTime? UtolsoModositasDateTime
        {
            get
            {
                return ServletHelper.ConvertTimestampToDateTime(this.UtolsoModositasIdeje);
            }
        }

        public Version()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static Version Create(string line)
        {
            Version ver = new Version();

            string[] columns = line.Split(';');
            ver.DokumentumAzonosito = GetColumns(columns, 0);
            ver.Verzio = GetColumns(columns, 1);
            ver.Ugyiratszam = GetColumns(columns, 2);
            ver.Alszam = GetColumns(columns, 3);
            ver.Irattervezetszam = GetColumns(columns, 4);
            ver.UtolosModosito = GetColumns(columns, 5);
            ver.UtolsoModositasIdeje = GetColumns(columns, 6);

            return ver;
        }

        static string GetColumns(string[] columns, int index)
        {
            if (columns.Length > index)
                return columns[index];

            return null;
        }
    }
}