﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using NMHH.UCM.Database;
using NMHH.UCM.Servlet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using UCM;

/// <summary>
/// Summary description for UMCDocumentumManager
/// </summary>
public class UCMDocumentumManager
{
    public UCMDocumentumManager()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public Result UploadFromeRecordWithCTT(
      ExecParam execparam
    , string documentStoreType
    , string filename
    , byte[] cont
    , KRT_Dokumentumok kapottKrtDokObj
    , XmlDocument xmlParams
    )
    {

        #region Doc

        /* **********************************************************************************
         * Lepesek, nagyvonalakban:
         * 
         * feltoltesi hely-utvonal meghatarozasa
	     *    ha iktatás van, akkor iktatas ala, ha nem csatolas
         * adatbazisban es spsben letrehozas, ha nincs meg az mappa/utvonal
	     *     (ha nem uj irat az valasztottIratId alapjan vissza kell szedni a dokumentum_id-jet!)
         * csekout
	     *     ha mar van ott olyan nevu file SPSben
         * feltoltes
         * 	
         * regisztralas
	     *    ha új irat
	     *     ha még nem volt csatolva hozzá
         * 
         * 
         * verziok lekerdezese
         * sps metaadatok beallitasa
         * chekin
	     *     irat munkaanyag ellenorzes
	     *    foverzio, ha iktatas/vagy iktatott anyag ujra csatolasa
	     *     alverzio, ha munakanyag/munkaanyag ujra csatolasa
         * egyeb adatok (external_link) modositasa a bejegyzeseben
	     *     ha van elozo verzio
         * mappatartalom regisztralasa
         * 
         *************************************************************************** */

        #endregion

        Logger.Info(String.Format("UCMDocumentumManager.UploadFromeRecordWithCTT indul."));

        string originalFileName = filename;
        filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

        Result _ret = new Result();

        #region A kiindulo, alap feltoltesi struktura meghatarozasa.

        Result resultTHStrukt = TarolasiHelyStrukturaNevEsTartalomAlapjan(execparam, documentStoreType, filename, cont, xmlParams.OuterXml.ToString());

        if (!String.IsNullOrEmpty(resultTHStrukt.ErrorCode))
        {
            return resultTHStrukt;
        }

        try
        {
            xmlParams.LoadXml((string)resultTHStrukt.Record);
        }
        catch (Exception exc1)
        {
            Logger.Info(String.Format("Hiba a struktura meghatarozo altal visszaadott xml string konvertalasakor.\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace));
            _ret.ErrorCode = "XMLC0001";
            _ret.ErrorMessage = String.Format("Hiba a struktura meghatarozo altal visszaadott xml string konvertalasakor.\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace);
            return _ret;
        }

        string rootSiteCollectionUrl = GetParam("rootSiteCollectionUrl", xmlParams);
        string ugyiratszam = GetParam("ugyiratszam", xmlParams);
        string alszam = GetParam("alszam", xmlParams);

        //  Ez lesz az uj Krt_dok bejegyzes GUIDja.
        //  Vagy ha letezo, akkor ebbe allitjuk be az erteket.
        Guid ujKrtDokGuid = new Guid(GetParam("ujKrtDokGuid", xmlParams));
        bool letezoKrtDok = false;

        //  ha kaptunk dokumentum guid-ot, akkor az lesz az ujKrtDokGuid
        //  de ellenorizzuk, hogy letezik-e
        if (!String.IsNullOrEmpty(GetParam("docmetaDokumentumId", xmlParams)))
        {
            Logger.Info(String.Format("Van dokumentum_id az xml parameterek kozott. Ellenorzunk. ({0})", GetParam("docmetaDokumentumId", xmlParams)));

            ExecParam gede = new ExecParam();
            gede.Alkalmazas_Id = execparam.Alkalmazas_Id;
            gede.Felhasznalo_Id = execparam.Felhasznalo_Id;
            gede.FelhasznaloSzervezet_Id = execparam.FelhasznaloSzervezet_Id;
            gede.Record_Id = GetParam("docmetaDokumentumId", xmlParams);

            Result resultIsDokExists = eDocumentUtility.GetDoc(gede);

            if (!String.IsNullOrEmpty(resultIsDokExists.ErrorCode))
            {
                if (resultIsDokExists.ErrorCode.Equals("-2146232060") && resultIsDokExists.ErrorMessage.Equals("[50101]"))
                {
                    resultIsDokExists = new Result();
                    resultIsDokExists.ErrorCode = "NDIDA00001";
                    resultIsDokExists.ErrorMessage = "A dokumentum tartalmaz egy dokumentum id-t. Ez nem található a rendszerben!";
                }
                Logger.Error(String.Format("Hiba a dokumentum letenek ellenorzesekor!ErrCode: {0}\nErrMsg: {1}", resultIsDokExists.ErrorCode, resultIsDokExists.ErrorMessage));
                return resultIsDokExists;
            }

            if (resultIsDokExists.Record == null)
            {
                Logger.Error(String.Format("Hiba a dokumentum letenek ellenorzesekor! Null a kapott result record mezoje! Valoszinuleg nem letezik a megadott guid-u krt_dok bejegyzes."));
                _ret.ErrorCode = "NEX0001";
                _ret.ErrorMessage = "Upload: Hiba a dokumentum letenek ellenorzesekor! Null a kapott result record mezoje! Valoszinuleg nem letezik a megadott guid-u krt_dok bejegyzes.";
                return _ret;
            }

            ujKrtDokGuid = new Guid(GetParam("docmetaDokumentumId", xmlParams));
            letezoKrtDok = true;
        }

        //  2. Ha docx, akkor megnezzuk van-e tartalomtipusa
        string kiterjesztes = (filename.LastIndexOf('.') == -1) ? " " : filename.Trim().Substring(filename.LastIndexOf('.') + 1);

        Logger.Info(String.Format("filename: {0} - kiterjesztes: {1}", filename, kiterjesztes));
        Logger.Info(String.Format("Kapott cuccok: ugyiratszam: {0}\nalszam: {1}", ugyiratszam, alszam));

        #endregion

        #region ha nem uj irat a kivalasztottIratId alapjan vissza kell szedni a dokumentum_id-jet

        string kivalasztottIratIdAlapjanDokumentumId = String.Empty;
        string ujKrtDokBejegyzesKell = this.GetParam("ujKrtDokBejegyzesKell", xmlParams);
        String csatolmanyId = String.Empty;
        String csatolmanyVerzio = String.Empty;

        if (GetParam("ujirat", xmlParams).Equals("NEM"))
        {
            bool vanIlyenNevuNeki = true;
            Logger.Info(String.Format("ha nem uj irat a kivalasztottIratId alapjan vissza kell szedni a dokumentum_id-jet - indul. kivalasztottIratId: {0} - kivalasztottKuldemenyId: {1}", GetParam("kivalasztottIratId", xmlParams), GetParam("kivalasztottKuldemenyId", xmlParams)));

            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_CsatolmanyokService csatolmanyokService = sf.GetEREC_CsatolmanyokService();
            Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch csatolmanyokSearch = new Contentum.eQuery.BusinessDocuments.EREC_CsatolmanyokSearch();

            if (!String.IsNullOrEmpty(GetParam("kivalasztottKuldemenyId", xmlParams)))
            {
                csatolmanyokSearch.KuldKuldemeny_Id.Value = GetParam("kivalasztottKuldemenyId", xmlParams);
                csatolmanyokSearch.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            else
            {
                csatolmanyokSearch.IraIrat_Id.Value = GetParam("kivalasztottIratId", xmlParams);
                csatolmanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            csatolmanyokSearch.Manual_Dokumentum_Nev.Value = filename;
            csatolmanyokSearch.Manual_Dokumentum_Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result iratDokRes = csatolmanyokService.GetAllWithExtension(execparam, csatolmanyokSearch);

            if (!String.IsNullOrEmpty(iratDokRes.ErrorCode))
            {
                Logger.Error(String.Format("Hibával tért vissza az irat dokumentum kapcsolótábla lekérdezés!\nErrorCode: {0}\nErrorMessage: {1}", iratDokRes.ErrorCode, iratDokRes.ErrorMessage));
                return iratDokRes;
            }

            if (iratDokRes.Ds.Tables[0].Rows.Count == 0)
            {
                //Logger.Error(String.Format("Nem adott vissza eredmenyt az irat dokumentum kapcsolotabla select!"));
                //_ret = new Result();
                //_ret.ErrorCode = "IRATK00001";
                //_ret.ErrorMessage = String.Format("Nem adott vissza eredmenyt az irat dokumentum kapcsolotabla select!");
                //return _ret;
                ujKrtDokBejegyzesKell = "IGEN";
                vanIlyenNevuNeki = false;
                Logger.Info(String.Format("Nincs ilyen nevu neki: {0}", filename));
            }

            if (iratDokRes.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error(String.Format("Tul sok eredmenyt adott az irat dokumentum kapcsolotabla select!"));
                _ret = new Result();
                _ret.ErrorCode = "IRATK00002";
                _ret.ErrorMessage = String.Format("Tul sok eredmenyt adott az irat dokumentum kapcsolotabla select!");
                return _ret;
            }

            Logger.Info(String.Format("iratDokRes.Ds.Tables[0].Rows.Count: {0}", iratDokRes.Ds.Tables[0].Rows.Count));
            if (iratDokRes.Ds.Tables[0].Rows.Count == 1)
            {
                csatolmanyId = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["Id"]);
                csatolmanyVerzio = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["erecCsat_Ver"]);
                Logger.Info(String.Format("kapott csatolmanyId: {0} - csatolmanyVerzio: {1}", csatolmanyId, csatolmanyVerzio));

                #region zarolas figyelese

                Logger.Info("Dokumentum zarolas figyelese.");

                if (!String.IsNullOrEmpty(Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"])))
                {
                    //  TODO: itt az lenne szep, h a zarolo id mellett a nevet is visszadnank!

                    Logger.Error(String.Format("A dokumentum zarolva van! Zarolo_Id: {0} ZarolasIdo: {1}", iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"], iratDokRes.Ds.Tables[0].Rows[0]["krtDok_ZarolasIdo"]));

                    Result iratDokRes2 = new Result();
                    iratDokRes2.ErrorCode = "ZAROLT001";
                    iratDokRes2.ErrorMessage = String.Format("A feltöltendő dokumentum zárolva van (ki: {0}, mikor: {1})!", iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"], iratDokRes.Ds.Tables[0].Rows[0]["krtDok_ZarolasIdo"]);
                    iratDokRes2.Record = String.Format("<result><zarolo_id>{0}</zarolo_id><zarolasido>{1}</zarolasido></result>", iratDokRes.Ds.Tables[0].Rows[0]["krtDok_Zarolo_id"], iratDokRes.Ds.Tables[0].Rows[0]["krtDok_ZarolasIdo"]);
                    return iratDokRes2;
                }

                #endregion
            }

            if (vanIlyenNevuNeki)
            {
                Logger.Info(String.Format("Eredmeny ok"));

                kivalasztottIratIdAlapjanDokumentumId = Convert.ToString(iratDokRes.Ds.Tables[0].Rows[0]["Dokumentum_Id"]);

                Logger.Info(String.Format("Kapott dokumentum id: {0}", kivalasztottIratIdAlapjanDokumentumId));
            }
        }

        #endregion

        #region Feltoltes: a file feltoltese

        UCMServletManager manager = new UCMServletManager();

        string irattervezetszam = null;
        ////munkaanyag
        //if (!String.IsNullOrEmpty(alszam) && alszam.StartsWith("MI"))
        //{
        //    irattervezetszam = alszam.Substring(2);
        //    alszam = null;
        //}
        Result uploadResult = this.Upload(cont, filename, ugyiratszam, null, alszam, irattervezetszam, null, null, null, false);
        if (uploadResult.IsError)
        {
            Logger.Error(String.Format("Hibával tért vissza a dokumantunm feltöltés!\nErrorCode: {0}\nErrorMessage: {1}", uploadResult.ErrorCode, uploadResult.ErrorMessage));
            return uploadResult;
        }

        string path = uploadResult.Uid;
        string externalLink = ServletHelper.GetExternalLink(path);

        #endregion

        #region kell-e uj krtDok bejegyzes (vizsgalat, h nem uj vagy ujKrtDokBejegyzesKell nem letezik)

        if (GetParam("ujirat", xmlParams).Equals("NEM") && !eDocumentUtility.IsParamExists("ujKrtDokBejegyzesKell", xmlParams) && !"NEM".Equals(ujKrtDokBejegyzesKell))
        {
            Contentum.eRecord.Service.ServiceFactory sf = Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(eDocumentUtility.GeteRecordServiceFactory());
            Contentum.eRecord.Service.EREC_CsatolmanyokService csatolmanyokService = sf.GetEREC_CsatolmanyokService();

            ExecParam execp2 = execparam.Clone();

            EREC_CsatolmanyokSearch csatSearch = new EREC_CsatolmanyokSearch();

            if (!String.IsNullOrEmpty(GetParam("kivalasztottKuldemenyId", xmlParams)))
            {
                csatSearch.KuldKuldemeny_Id.Value = GetParam("kivalasztottKuldemenyId", xmlParams);
                csatSearch.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            else
            {
                csatSearch.IraIrat_Id.Value = GetParam("kivalasztottIratId", xmlParams);
                csatSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            csatSearch.Manual_Dokumentum_Nev.Value = filename;
            csatSearch.Manual_Dokumentum_Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result resultCsatt = csatolmanyokService.GetAllWithExtension(execp2, csatSearch);

            if (!String.IsNullOrEmpty(resultCsatt.ErrorCode))
            {
                Logger.Error(String.Format("Hibával tért vissza a kiválasztott irat id csatolmány neve alapján lekérdezés! ErrCode: {0}\nErrM: {1}", resultCsatt.ErrorCode, resultCsatt.ErrorMessage));
                return resultCsatt;
            }

            if (resultCsatt.Ds.Tables[0].Rows.Count == 0)
            {
                ujKrtDokBejegyzesKell = "IGEN";
                Logger.Info(String.Format("Nulla a talált csatolmányok száma a megadott irat idhez ({0}), ezért lesz insert.", GetParam("kivalasztottIratId", xmlParams)));
            }

        }

        #endregion

        #region Krt_dokumentum bejegyzes megejtese.

        String tortentUjKrtDokBejegyzes = String.Empty;

        if (GetParam("ujirat", xmlParams).Equals("IGEN") || ujKrtDokBejegyzesKell.Equals("IGEN"))
        {

            if (kapottKrtDokObj == null)
            {
                kapottKrtDokObj = new KRT_Dokumentumok();
                kapottKrtDokObj.Updated.SetValueAll(false);
                kapottKrtDokObj.Base.Updated.SetValueAll(false);
            }

            kapottKrtDokObj.Csoport_Id_Tulaj = execparam.Felhasznalo_Id;
            kapottKrtDokObj.Updated.Csoport_Id_Tulaj = true;

            kapottKrtDokObj.FajlNev = filename.Trim();
            kapottKrtDokObj.Updated.FajlNev = true;

            kapottKrtDokObj.Meret = cont.Length.ToString();
            kapottKrtDokObj.Updated.Meret = true;

            kapottKrtDokObj.Allapot = "1";
            kapottKrtDokObj.Updated.Allapot = true;

            if (String.IsNullOrEmpty(kapottKrtDokObj.Tipus))
            {
                kapottKrtDokObj.Tipus = eDocumentUtility.GetFormatum(execparam, filename);
                kapottKrtDokObj.Updated.Tipus = true;
            }

            if (String.IsNullOrEmpty(kapottKrtDokObj.Formatum))
            {
                kapottKrtDokObj.Formatum = Path.GetExtension(filename).Trim(eDocumentUtility.kiterjesztesVegeirol);
                kapottKrtDokObj.Updated.Formatum = true;
            }

            kapottKrtDokObj.Alkalmazas_Id = execparam.Alkalmazas_Id;
            kapottKrtDokObj.Updated.Alkalmazas_Id = true;

            kapottKrtDokObj.Dokumentum_Id = kapottKrtDokObj.Id;
            kapottKrtDokObj.Updated.Dokumentum_Id = true;

            kapottKrtDokObj.External_Link = externalLink;
            kapottKrtDokObj.Updated.External_Link = true;

            // masolaskor ebbol vesszuk ki a masolando file site, doc es folder cuccait
            // vissza is lehetne selectalni a mappakbol is, de igy egyszerubb es gyorsabb :)
            kapottKrtDokObj.External_Info = path;
            kapottKrtDokObj.Updated.External_Info = true;

            kapottKrtDokObj.External_Source = documentStoreType;
            kapottKrtDokObj.Updated.External_Source = true;

            //kapottKrtDokObj.Leiras = externalTargetMappa["PATH"] + "/" + _ret.Uid.ToString() + "/" + filename;
            kapottKrtDokObj.Leiras = originalFileName;
            kapottKrtDokObj.Updated.Leiras = true;

            kapottKrtDokObj.TartalomHash = "<null>";
            kapottKrtDokObj.Updated.TartalomHash = true;

            kapottKrtDokObj.KivonatHash = "<null>";
            kapottKrtDokObj.Updated.KivonatHash = true;

            kapottKrtDokObj.BarCode = GetParam("vonalkod", xmlParams);
            kapottKrtDokObj.Updated.BarCode = true;

            // a fajl feltoltesekor az alairas felulvizsgalat datumot
            // automatikusan generaljuk a feltoltessel egyidejuleg
            // de csak ha van ElektronikusAlairas informacionk
            if (!String.IsNullOrEmpty(kapottKrtDokObj.ElektronikusAlairas)
                && kapottKrtDokObj.ElektronikusAlairas != "<null>")
            {
                kapottKrtDokObj.AlairasFelulvizsgalat = System.DateTime.Now.ToString();
                kapottKrtDokObj.Updated.AlairasFelulvizsgalat = true;
            }

            Logger.Info("File regisztralasa eDocba.");
            KRT_DokumentumokService dokumentumService2 = new KRT_DokumentumokService();
            _ret = dokumentumService2.Insert(execparam, kapottKrtDokObj);

            if (!String.IsNullOrEmpty(_ret.ErrorCode))
            {
                Logger.Error("File regisztralasa eDocba hiba: " + _ret.ErrorCode + "; " + _ret.ErrorMessage);
                return _ret;
            }

            tortentUjKrtDokBejegyzes = _ret.Uid;
        }
        else
        {
            //_ret.Uid = GetParam("docmetaDokumentumId", xmlParams);
            _ret.Uid = kivalasztottIratIdAlapjanDokumentumId;
        }

        _ret.Record = externalLink;

        #endregion

        #region A rekord mezobe visszaadando, a kovetkezo lepeshez hasznalatos parameterek

        string uploadXmlStrParams_alairasResz = "";
        string alairaskell = GetParam("alairaskell", xmlParams);
        if (alairaskell == "IGEN")
        {
            uploadXmlStrParams_alairasResz = String.Format("<alairaskell>{0}</alairaskell>"
                                                         + "<alairasSzabalyId>{1}</alairasSzabalyId>"
                                                         + "<alairasSzint>{2}</alairasSzint>"
                                                         + "<easzKod>{3}</easzKod>"
                                                         + "<tanusitvanyId>{4}</tanusitvanyId>"
                                                         + "<kivarasiIdo>{5}</kivarasiIdo>"
                                                         + "<alairasMod>{6}</alairasMod>"
                                                         + "<alairasTulajdonos>{7}</alairasTulajdonos>"
                                                         , GetParam("alairaskell", xmlParams)
                                                         , GetParam("alairasSzabalyId", xmlParams)
                                                         , GetParam("alairasSzint", xmlParams)
                                                         , GetParam("easzKod", xmlParams)
                                                         , GetParam("tanusitvanyId", xmlParams)
                                                         , GetParam("kivarasiIdo", xmlParams)
                                                         , GetParam("alairasMod", xmlParams)
                                                         , GetParam("alairasTulajdonos", xmlParams));
        }

        string visszaadandoXmlString = String.Format("<uploadparameterek>" +
            "<documentStoreType>{0}</documentStoreType>" +
            "<rootSiteCollectionUrl>{1}</rootSiteCollectionUrl>" +
            "<doktarSitePath>{2}</doktarSitePath>" +
            "<doktarDocLibPath>{3}</doktarDocLibPath>" +
            "<doktarFolderPath>{4}</doktarFolderPath>" +
            "<dokumentum_id>{5}</dokumentum_id>" +
            "<spsCttId>{6}</spsCttId>" +
            "<eDocMappaGuid>{7}</eDocMappaGuid>" +
            "<filename>{8}</filename>" +
            "<iktatokonyv>{9}</iktatokonyv>" +
            "<sourceSharePath>{10}</sourceSharePath>" +
            "<foszam>{11}</foszam>" +
            "<iktatoszam>{12}</iktatoszam>" +
            "<ujKrtDokGuid>{13}</ujKrtDokGuid>" +
            "<megjegyzes>{14}</megjegyzes>" +
            "<munkaanyag>{15}</munkaanyag>" +
            "<docmetaDokumentumId>{16}</docmetaDokumentumId>" +
            "<dokumentumFullUrl>{17}</dokumentumFullUrl>" +
            "<ujirat>{18}</ujirat>" +
            "<cttNeve>{19}</cttNeve>" +
            "<docmetaIratId>{20}</docmetaIratId>" +
            "<kivalasztottIratId>{21}</kivalasztottIratId>" +
            "<ujKrtDokBejegyzesKell>{22}</ujKrtDokBejegyzesKell>" +
            "<tartalomSHA1>{23}</tartalomSHA1>" +
            "<iratMetaDefId>{24}</iratMetaDefId>" +
            "<iratMetaDefFoDokumentum>{25}</iratMetaDefFoDokumentum>" +
            "<sablonAzonosito>{26}</sablonAzonosito>" +
            uploadXmlStrParams_alairasResz +
            "<kivalasztottKuldemenyId>{27}</kivalasztottKuldemenyId>" +
            "<tortentUjKrtDokBejegyzes>{28}</tortentUjKrtDokBejegyzes>" +
            "<csatolmanyId>{29}</csatolmanyId>" +
            "<csatolmanyVerzio>{30}</csatolmanyVerzio>" +
            "<originalFileName>{31}</originalFileName>" +
            "</uploadparameterek>"
            , documentStoreType
            , rootSiteCollectionUrl
            , String.Empty
            , String.Empty
            , String.Empty
            , _ret.Uid
            , String.Empty
            , String.Empty         //eDocMappaGuid.Uid.ToString()
            , Contentum.eUtility.XmlFunction.ConvertToCData(filename)
            , GetParam("iktatokonyv", xmlParams)
            , GetParam("sourceSharePath", xmlParams)
            , GetParam("foszam", xmlParams)
            , GetParam("iktatoszam", xmlParams)
            , GetParam("docmetaDokumentumId", xmlParams)
            , GetParam("megjegyzes", xmlParams)
            , GetParam("munkaanyag", xmlParams)
            , _ret.Uid.ToString()
            , Contentum.eUtility.XmlFunction.ConvertToCData(_ret.Record.ToString())
            , GetParam("ujirat", xmlParams)
            , GetParam("cttNeve", xmlParams)
            , GetParam("docmetaIratId", xmlParams)
            , GetParam("kivalasztottIratId", xmlParams)
            , ujKrtDokBejegyzesKell
            , GetParam("tartalomSHA1", xmlParams)
            , GetParam("iratMetaDefId", xmlParams)
            , GetParam("iratMetaDefFoDokumentum", xmlParams)
            , GetParam("sablonAzonosito", xmlParams)
            , GetParam("kivalasztottKuldemenyId", xmlParams)
            , tortentUjKrtDokBejegyzes
            , csatolmanyId
            , csatolmanyVerzio
            , Contentum.eUtility.XmlFunction.ConvertToCData(originalFileName)
            );

        Logger.Info(String.Format("Tovabbadott xml paramterek: {0}", visszaadandoXmlString));

        _ret.Record = visszaadandoXmlString;

        #endregion

        Logger.Info("DocumentService._UploadFromeRecordWithCTT vege.");

        return _ret;
    } // _UploadFromeRecordWithCTT



    private Result TarolasiHelyStrukturaNevEsTartalomAlapjan(
      ExecParam execparam
    , string documentStoreType
    , string filename
    , byte[] cont
    , string paramsXmlStr
 )
    {
        Result _ret = new Result();

        Logger.Info("--- UCMDocumentumManager.TarolasiHelyStrukturaNevEsTartalomAlapjan indul ---");

        #region Xml parameterek xmldocca es parameterek ellenorzese

        XmlDocument xmlParams = new XmlDocument();

        if (paramsXmlStr != null)
        {
            try
            {
                Logger.Info("Xml parameter string feldolgozasa.");
                xmlParams.LoadXml(paramsXmlStr);
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba az XML paraméter string átalakításakor!\nMessage: {0}\nStackTrace: {1}\nXmlParams: {2}", ex.Message, ex.StackTrace, paramsXmlStr));
                _ret.ErrorCode = "XMLPAR0001";
                _ret.ErrorMessage = String.Format("Hiba az XML paraméter string átalakításakor!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                return _ret;
            }
        }
        else
        {
            Logger.Error(String.Format("Hiba az XML paraméter stringgel. Ures string!"));
            _ret.ErrorCode = "XMLPAR0002";
            _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Hiba az XML paraméter stringgel! Üres string!");
            return _ret;
        }

        if (String.IsNullOrEmpty(filename))
        {
            Logger.Error(String.Format("Ures file neve parameter!"));
            _ret.ErrorCode = "XMLPAR0004";
            _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: nincs megadva a file neve paraméter!");
            return _ret;
        }

        if (xmlParams.SelectNodes("//iktatokonyv").Count == 0)
        {
            Logger.Error(String.Format("Nincs megadva az xmlben megadott ertekek kozott az iktatokonyv!"));
            _ret.ErrorCode = "XMLPAR0005";
            _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Nincs megadva az xmlben megadott ertekek kozott az iktatokonyv!");
            return _ret;
        }

        if (xmlParams.SelectNodes("//foszam").Count == 0)
        {
            Logger.Error(String.Format("Nincs megadva az xmlben megadott ertekek kozott a foszam!"));
            _ret.ErrorCode = "XMLPAR0006";
            _ret.ErrorMessage = String.Format("DocumentService.GetTarolasiHelyStruktura: Nincs megadva az xmlben megadott ertekek kozott a foszam!");
            return _ret;
        }

        #endregion

        #region A kiindulo, alap feltoltesi struktura meghatarozasa.

        //  A site collection - a kiindulási url.
        string rootSiteCollectionUrl = UI.GetAppSetting("UCMServlet_URL");

        string ugyiratszam = String.Empty;

        string ev = GetParam("ev", xmlParams);

        string iktatokonyv = DocumentService.Utility.RemoveSlash(GetParam("iktatokonyv", xmlParams));

        string foszam = GetParam("foszam", xmlParams);

        string alszam = GetParam("alszam", xmlParams);

        Guid ujKrtDokGuid = Guid.NewGuid();

        if (String.IsNullOrEmpty(iktatokonyv))
        {
            iktatokonyv = GetParam("ucmiktatokonyv", xmlParams);
            if (String.IsNullOrEmpty(iktatokonyv))
            {
                iktatokonyv = "N";
            }

            string kuldemenyId = GetParam("kivalasztottKuldemenyId", xmlParams);
            if (!String.IsNullOrEmpty(kuldemenyId))
            {
                ExecParam kuldExecParam = execparam.Clone();
                kuldExecParam.Record_Id = kuldemenyId;
                EREC_KuldKuldemenyekService svcKuld = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                Result resKuld = svcKuld.Get(kuldExecParam);

                if (!resKuld.IsError)
                {
                    EREC_KuldKuldemenyek kuld = resKuld.Record as EREC_KuldKuldemenyek;
                    if (kuld.Erkezteto_Szam != "0")
                        foszam = kuld.Erkezteto_Szam;
                    if (kuld.Erkeztetes_Ev != "0")
                        ev = kuld.Erkeztetes_Ev;
                }

            }
            else
            {
            }
        }
        else
        {
            if (!String.IsNullOrEmpty(foszam))
            {
                Logger.Info("a könyvtárba bekerül a főszám mellett az alszám is");

                if (String.IsNullOrEmpty(alszam))
                {
                    string kivalasztottIratId = GetParam("kivalasztottIratId", xmlParams);
                    if (!String.IsNullOrEmpty(kivalasztottIratId))
                    {
                        Logger.Info(String.Format("A kiválasztott irat {0} lekérése.", kivalasztottIratId));
                        ExecParam xpm = execparam.Clone();
                        xpm.Record_Id = kivalasztottIratId;
                        EREC_IraIratokService svc = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        Result res = svc.Get(xpm);
                        if (!res.IsError)
                        {
                            EREC_IraIratok kivalasztottIrat = res.Record as EREC_IraIratok;
                            alszam = Contentum.eRecord.BaseUtility.Iratok.GetAlszamString(kivalasztottIrat);

                            Logger.Info(String.Format("A kiválasztott irat alszáma: {0}", alszam));

                            eDocumentUtility.SetParam("alszam", xmlParams, alszam);
                        }
                        else
                        {
                            Logger.Error(String.Format("Hiba a kiválasztott irat lekérése {0} közben.", kivalasztottIratId), xpm, res);
                        }
                    }
                }
                else
                {
                    Logger.Info("A paraméterben kapott alszám: " + alszam);
                }

            }
            else
            {
                Logger.Info("nincs főszám átadva");
            }


        }

        bool isMU = false;
        bool isMI = false;

        if (String.IsNullOrEmpty(ev))
        {
            ev = DateTime.Now.Year.ToString();
        }

        //munkaugyirat
        if (!String.IsNullOrEmpty(foszam) && foszam.StartsWith("MU"))
        {
            isMU = true;
            foszam = foszam.Substring(2);
        }

        int i;

        //guid-ból int
        if (!Int32.TryParse(foszam, out i))
        {
            foszam = Math.Abs(foszam.GetHashCode()).ToString();
        }

        //munkairat
        if (!String.IsNullOrEmpty(alszam) && alszam.StartsWith("MI"))
        {
            isMI = true;
            alszam = alszam.Substring(2);
            eDocumentUtility.SetParam("alszam", xmlParams, alszam);
        }

        if (isMU)
        {
            iktatokonyv += "MU";
        }

        if (isMI)
        {
            iktatokonyv += "MI";
        }

        ugyiratszam = ServletHelper.GetUgyiratSzam(iktatokonyv, foszam, ev);

        #endregion

        #region Visszaadott parameterek xmlbe torteno betetele majd a resultba illesztese

        Logger.Info("vissza param xmlhez nodok legyartasa");

        XmlNode nd1 = xmlParams.CreateNode(XmlNodeType.Element, "rootSiteCollectionUrl", "");
        nd1.InnerText = rootSiteCollectionUrl;

        XmlNode nd2 = xmlParams.CreateNode(XmlNodeType.Element, "ugyiratszam", "");
        nd2.InnerText = ugyiratszam;

        XmlNode nd3 = xmlParams.CreateNode(XmlNodeType.Element, "ujKrtDokGuid", "");
        nd3.InnerText = ujKrtDokGuid.ToString();

        XmlNode nd4 = xmlParams.CreateNode(XmlNodeType.Element, "iratMetaDefId", "");
        nd4.InnerText = String.Empty;

        XmlNode nd5 = xmlParams.CreateNode(XmlNodeType.Element, "iratMetaDefFoDokumentum", "");
        nd5.InnerText = String.Empty;

        Logger.Info("vissza param xmlhez nodok hozzafuzese");

        xmlParams.ChildNodes[0].AppendChild(nd1);
        xmlParams.ChildNodes[0].AppendChild(nd2);
        xmlParams.ChildNodes[0].AppendChild(nd3);
        xmlParams.ChildNodes[0].AppendChild(nd4);
        xmlParams.ChildNodes[0].AppendChild(nd5);

        Logger.Info("vissza param xml bele a record mezobe");

        _ret.Record = xmlParams.OuterXml.ToString();

        Logger.Info(String.Format("Visszaadott xml: {0}", xmlParams.OuterXml.ToString()));

        #endregion

        Logger.Info("--- DocumentService.TarolasiHelyStrukturaNevEsTartalomAlapjan rendben vege. ---");

        return _ret;
    }

    public Result IsFilesUploadableToMOSS(ExecParam ex, String xmlStrFilenames)
    {
        Logger.Info(String.Format("UCMDocumentumManager.IsFilesUploadableToMOSS indul - xmlStrFilenames: {0}", xmlStrFilenames));

        Result result = new Result();

        try
        {

            #region bejovo parameterek ellenorzese

            Logger.Info("IsFilesUploadableToMOSS: bejovo parameterek ellenorzese");

            XmlDocument xmlParams = new XmlDocument();
            xmlParams.LoadXml(xmlStrFilenames);

            if (xmlParams.SelectNodes("//filename").Count == 0)
            {
                Logger.Error("Nincs megadva az xml stringben egy filename node sem!");
                result.ErrorCode = "PARAM0001";
                result.ErrorMessage = "Nincs megadva az xml paraméter stringben egy filename node sem!";
                return result;
            }


            #endregion

            #region bejovo parameterek attributumokkal valo kiegeszitese

            Logger.Info("IsFilesUploadableToMOSS: bejovo parameterek attributumokkal valo kiegeszitese");

            foreach (XmlNode nd in xmlParams.SelectNodes("//filename"))
            {
                bool vanAttrUp = false;
                bool vanAttrRe = false;

                for (int i = 0; i < nd.Attributes.Count; i++)
                {
                    if (nd.Attributes[i].Name.Equals("uploadable"))
                        vanAttrUp = true;
                    if (nd.Attributes[i].Name.Equals("reason"))
                        vanAttrRe = true;
                }

                if (!vanAttrUp)
                {
                    XmlAttribute a = xmlParams.CreateAttribute("uploadable");
                    a.Value = "YES";
                    nd.Attributes.Append(a);
                }
                else
                {
                    nd.Attributes["uploadable"].Value = "YES";
                }

                if (!vanAttrRe)
                {
                    XmlAttribute a = xmlParams.CreateAttribute("reason");
                    a.Value = "";
                    nd.Attributes.Append(a);
                }
                else
                {
                    nd.Attributes["reason"].Value = "";
                }

            }

            #endregion

            result.Record = xmlParams.OuterXml.ToString();

        }
        catch (Exception exc)
        {
            Logger.Error(String.Format("futási hiba a IsFilesUploadableToMOSS eljarasban. Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
            result.ErrorCode = "FUTHIBA00001";
            result.ErrorMessage = String.Format("Hiba az eDocumentService.IsFilesUploadableToMOSS eljárás futása közben! Hibaüzenet: {0}", exc.Message);
        }

        return result;
    }

    public Result CheckInUploadedFileWithCTT(ExecParam execparam
        , XmlDocument xmlParams)
    {

        Logger.Info(String.Format("UCMDocumentumManager.CheckInUploadedFileWithCTT indul."));

        string filename = GetParam("filename", xmlParams);
        string originalFileName = filename;
        filename = Contentum.eUtility.FileFunctions.GetValidSharePointFileName(filename);

        String dokumentumId = GetParam("dokumentum_id", xmlParams);
        Result _ret = new Result();

        #region Feltoltes: verzio lekerdezese. - Krt_Dok bejegyzes update

        Logger.Info("--- Feltoltes: verzio lekerdezese. - Krt_Dok bejegyzes update");

        string aktVersion = String.Empty;

        Logger.Info("A verziojel bejegyzese.");

        ExecParam execp = new ExecParam();
        execp.Alkalmazas_Id = execparam.Alkalmazas_Id;
        execp.Felhasznalo_Id = execparam.Felhasznalo_Id;
        execp.FelhasznaloSzervezet_Id = execparam.FelhasznaloSzervezet_Id;
        execp.Record_Id = dokumentumId;

        Result resultDokSel = GetDoc(execp);

        if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
        {
            Logger.Error(String.Format("Hiba a dokumentum lekerdezesekor.\nErrorCode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
            return resultDokSel;
        }

        if (resultDokSel.Record == null)
        {
            Logger.Error(String.Format("Hiba a dokumentum lekerdezesekor. Null a result record mezoje!"));
            resultDokSel = new Result();
            resultDokSel.ErrorCode = "VESD00001";
            resultDokSel.ErrorMessage = "Hiba a krt_dokumentumok bejegyzés lekérdezésekor! Null a visszaadott objektum.";
            return resultDokSel;
        }

        Logger.Info("Lekerdezes ok. Update elokeszitese.");

        KRT_Dokumentumok updateDok = (KRT_Dokumentumok)resultDokSel.Record;

        UCMServletManager manager = new UCMServletManager();

        
        //GetVersionsResponse versionsResponse = manager.GetVersions(updateDok.External_Info);

        //if (versionsResponse.IsError)
        //{
        //    _ret.ErrorCode = versionsResponse.ErrorCode;
        //    _ret.ErrorMessage = versionsResponse.ErrorMessage;
        //    Logger.Error(String.Format("Hibával tért vissza a verziók lekérése!\nErrorCode: {0}\nErrorMessage: {1}", _ret.ErrorCode, _ret.ErrorMessage));
        //    return _ret;
        //}

        //aktVersion = versionsResponse.Versions[0].Verzio;

        byte[] fileContent;

        ServletResponse contentResponse = manager.GetDocumentContent(updateDok.External_Info);

        if (contentResponse.IsError)
        {
            _ret.ErrorCode = contentResponse.ErrorCode;
            _ret.ErrorMessage = contentResponse.ErrorMessage;
            Logger.Error(String.Format("Hibával tért vissza a dokumentumtartalom lekérése!\nErrorCode: {0}\nErrorMessage: {1}", _ret.ErrorCode, _ret.ErrorMessage));
            return _ret;
        }

        fileContent = contentResponse.ResponseData;

        #region Dokumentum tulajdonságok beállítása

        string kiterjesztes = Path.GetExtension(filename);
        if (kiterjesztes.Equals(".docx", StringComparison.InvariantCultureIgnoreCase))
        {
            try
            {
                Dictionary<string, string> properties = new Dictionary<string, string>();

                string edok_w_verzio = updateDok.Base.Ver;
                properties.Add("edok_w_verzio", edok_w_verzio);
                string edok_w_dokumentum_id = dokumentumId;
                properties.Add("edok_w_dokumentum_id", edok_w_dokumentum_id);

                if (!String.IsNullOrEmpty(GetParam("iktatokonyv", xmlParams)))
                {
                    string edok_w_ujirat = "0";
                    string edok_w_url_gep = Contentum.eUtility.UI.GetAppSetting("UCMServlet_URL");
                    string edok_w_url_site = updateDok.External_Info;
                    string edok_w_url_doknev = filename;
                    string edok_w_irat_id = GetParam("irat_id", xmlParams);

                    properties.Add("edok_w_ujirat", edok_w_ujirat);
                    properties.Add("edok_w_url_gep", edok_w_url_gep);
                    properties.Add("edok_w_url_site", edok_w_url_site);
                    properties.Add("edok_w_url_doknev", edok_w_url_doknev);
                    properties.Add("edok_w_irat_id", edok_w_irat_id);

                }

                bool modified = WordProcessing.SetDocumentProperties(ref fileContent, properties);

                if (modified)
                {
                    string alszam;
                    string ugyiratszam = ServletHelper.GetUgyiratSzam(updateDok.External_Info, out alszam, out filename);

                    Result uploadResult = this.Upload(fileContent, filename, ugyiratszam, null, alszam, null, null, null, null, false);
                    if (uploadResult.IsError)
                    {
                        Logger.Error(String.Format("Hibával tért vissza a dokumantunm feltöltés!\nErrorCode: {0}\nErrorMessage: {1}", uploadResult.ErrorCode, uploadResult.ErrorMessage));
                        return uploadResult;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("A dokumentum tulajdonságok beállítása közben történt exception! Message: {0}", ex.Message));
                _ret.ErrorCode = "TUL0000";
                _ret.ErrorMessage = String.Format("A dokumentum tulajdonságok beállítása közben történt exception! Message: {0}", ex.Message);
                return _ret;
            }
        }

        #endregion

        #region SHA1 checksumma számolása a file-ra

        string sha1chk = Contentum.eDocument.SharePoint.Utility.CalculateSHA1(fileContent);

        Logger.Info(String.Format("Szamolt SHA1: {0}", sha1chk));

        #endregion

        updateDok.Updated.SetValueAll(false);
        updateDok.Base.Updated.SetValueAll(false);

        updateDok.VerzioJel = aktVersion;
        updateDok.Updated.VerzioJel = true;

        //Logger.Info(String.Format("VerzioJel: {0}, ver: {1}", aktVersion, updateDok.Base.Ver));

        updateDok.Base.Updated.Ver = true;

        updateDok.KivonatHash = sha1chk;
        updateDok.Updated.KivonatHash = true;

        //  tartalom: a belso tartalomra hash (word addin szamolja es adja at parameterkent)
        updateDok.TartalomHash = GetParam("tartalomSHA1", xmlParams);
        updateDok.Updated.TartalomHash = true;

        Logger.Info("Update elott.");

        KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();
        resultDokSel = dokumentumService.Update(execp, updateDok);

        if (!String.IsNullOrEmpty(resultDokSel.ErrorCode))
        {
            Logger.Error(String.Format("Aktualis verzio update hiba!\nErrocode: {0}\nErrorMessage: {1}", resultDokSel.ErrorCode, resultDokSel.ErrorMessage));
            return resultDokSel;
        }

        #endregion

        Logger.Info("dokumentumId a resultba");
        _ret.Uid = dokumentumId;

        Logger.Info("result Xml string gyartasa.");

        String visszaXml = "<result>"
            + "<dokumentumFullUrl>" + Contentum.eUtility.XmlFunction.ConvertToCData(GetParam("dokumentumFullUrl", xmlParams)) + "</dokumentumFullUrl>"
            + "<tortentUjKrtDokBejegyzes>" + GetParam("tortentUjKrtDokBejegyzes", xmlParams) + "</tortentUjKrtDokBejegyzes>"
            + "<csatolmanyId>" + GetParam("csatolmanyId", xmlParams) + "</csatolmanyId>"
            + "<csatolmanyVerzio>" + GetParam("csatolmanyVerzio", xmlParams) + "</csatolmanyVerzio>"
            + "</result>";

        _ret.Record = visszaXml;

        Logger.Info(String.Format("result Xml string: {0}", visszaXml));

        Logger.Info("DocumentService._CheckInUploadedFileWithCTT vege.");

        return _ret;
    }

    private string GetParam(String pname, XmlDocument xdoc)
    {
        return DocumentService.Utility.GetParam(pname, xdoc);
    }

    public Result GetDocumentVersionsFromMOSS(ExecParam ex)
    {
        Logger.Info("UCMDocumentumManager.GetDocumentVersionsFromMOSS indul.");

        Result retResult = new Result();


        if (String.IsNullOrEmpty(ex.Record_Id))
        {
            retResult.ErrorCode = "PARAM0001";
            retResult.ErrorMessage = "Hiányzik az execparam Record_Id-jéből a krt_dokumentum id!";
            return retResult;
        }

        try
        {
            #region krt_dok bejegyzes lekerdezese

            Result resDoc = GetDoc(ex.Clone());

            if (!String.IsNullOrEmpty(resDoc.ErrorCode))
            {
                Logger.Error(String.Format("GetDoc hiba: {0}", resDoc.ErrorMessage));
                return resDoc;
            }

            KRT_Dokumentumok dokumentum = (KRT_Dokumentumok)resDoc.Record;

            #endregion

            #region verziok lekerdezese 

            UCMServletManager manager = new UCMServletManager();
            GetVersionsResponse response = manager.GetVersions(dokumentum.External_Info);

            if (response.IsError)
            {
                retResult.ErrorCode = response.ErrorCode;
                retResult.ErrorMessage = response.ErrorMessage;
                Logger.Error(String.Format("Hibával tért vissza a veruiók lekérése!\nErrorCode: {0}\nErrorMessage: {1}", retResult.ErrorCode, retResult.ErrorMessage));
                return retResult;
            }

            #endregion

            #region result dataset osszeallaitasa

            retResult.Ds = new DataSet();
            DataTable dt = retResult.Ds.Tables.Add();

            dt.Columns.Add("Id", typeof(Guid));
            dt.Columns.Add("name", typeof(String));
            dt.Columns.Add("Formatum", typeof(String));
            dt.Columns.Add("version", typeof(String));
            dt.Columns.Add("url", typeof(String));
            dt.Columns.Add("External_Link", typeof(String));
            dt.Columns.Add("size", typeof(Int32));
            dt.Columns.Add("created", typeof(DateTime));
            dt.Columns.Add("comments", typeof(String));

            foreach (NMHH.UCM.Servlet.Version version in response.Versions)
            {
                dt.Rows.Add(
                    dokumentum.Id
                    , dokumentum.FajlNev.Trim()
                    , dokumentum.Formatum
                    , version.Verzio
                    , dokumentum.External_Link
                    , dokumentum.External_Link
                    , dokumentum.Meret
                    , version.UtolsoModositasDateTime
                    , null
                    );
            }

            #endregion

        }
        catch (Exception ex1)
        {
            Logger.Error(String.Format("GetDocumentVersionsFromMOSS futasi hiba: {0}\nSource: {1}\nStackTrace: {2}", ex1.Message, ex1.Source, ex1.StackTrace));
            retResult.ErrorCode = "RTM0001";
            retResult.ErrorMessage = String.Format("GetDocumentVersionsFromMOSS futási hiba: {0}", ex1.Message);
        }

        return retResult;
    }

    private Result GetDoc(ExecParam execparam)
    {
        Logger.Info("UCMDocumentumManager.GetDoc indul.");

        if (String.IsNullOrEmpty(execparam.Record_Id))
        {
            Logger.Error("Ures az execparam Record_Id mezoje!");
            Result reske = new Result();
            reske.ErrorCode = "PARAM0001";
            reske.ErrorMessage = "eDocumentWebService.DocumentService.GetDoc (private) method paraméterezési hiba! Üres az execparam Record_Id mezoje!";
            return reske;
        }

        KRT_DokumentumokService dokService = new KRT_DokumentumokService();
        Result dokumentumResult = dokService.Get(execparam);

        if (!String.IsNullOrEmpty(dokumentumResult.ErrorCode))
            Logger.Error(String.Format("Hibaval tert vissza a dokService.Get: {0}\nErrMsg: {1}"
                , dokumentumResult.ErrorCode
                , dokumentumResult.ErrorMessage));

        return dokumentumResult;
    }

    public Result SetDocumentPropertiesAndRevalidate(ExecParam ex, XmlDocument xmlParams, KRT_Dokumentumok dok)
    {
        Logger.Info("UCMDocumentumManager.SetDocumentPropertiesAndRevalidate indul");

        Result result = new Result();

        try
        {
            if ("docx".Equals(Convert.ToString(dok.Formatum).ToLower().Trim()))
            {
                string aktVersion = String.Empty;
                UCMServletManager manager = new UCMServletManager();
                //GetVersionsResponse versionsResponse = manager.GetVersions(dok.External_Info);

                //if (versionsResponse.IsError)
                //{
                //    result.ErrorCode = versionsResponse.ErrorCode;
                //    result.ErrorMessage = versionsResponse.ErrorMessage;
                //    Logger.Error(String.Format("Hibával tért vissza a verziók lekérése!\nErrorCode: {0}\nErrorMessage: {1}", result.ErrorCode, result.ErrorMessage));
                //    return result;
                //}

                //aktVersion = versionsResponse.Versions[0].Verzio;

                byte[] fileContent;

                ServletResponse contentResponse = manager.GetDocumentContent(dok.External_Info);

                if (contentResponse.IsError)
                {
                    result.ErrorCode = contentResponse.ErrorCode;
                    result.ErrorMessage = contentResponse.ErrorMessage;
                    Logger.Error(String.Format("Hibával tért vissza a dokumentumtartalom lekérése!\nErrorCode: {0}\nErrorMessage: {1}", result.ErrorCode, result.ErrorMessage));
                    return result;
                }

                fileContent = contentResponse.ResponseData;

                #region adatok beállítása

                Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: adatok beállítása"));

                try
                {
                    Logger.Info(String.Format("xmlParams.SelectNodes(\"metaadat\").Count: {0}", xmlParams.SelectNodes("//metaadat").Count));

                    //
                    //  property-k beallitasa

                    Dictionary<string, string> properties = new Dictionary<string, string>();
                    foreach (XmlNode ndProp in xmlParams.SelectNodes("//metaadat"))
                    {
                        Logger.Info(String.Format("set doc meta prop: {0} = {1}", ndProp.Attributes["internalName"].Value, ndProp.Attributes["value"].Value));

                        properties.Add(ndProp.Attributes["internalName"].Value, ndProp.Attributes["value"].Value);
                    }

                    properties.Add("edok_w_verzio", dok.Base.Ver);

                    bool modified = WordProcessing.SetDocumentProperties(ref fileContent, properties);

                    if (modified)
                    {
                        string alszam;
                        string filename;
                        string ugyiratszam = ServletHelper.GetUgyiratSzam(dok.External_Info, out alszam, out filename);

                        Result uploadResult = this.Upload(fileContent, filename, ugyiratszam, null, alszam, null, null, null, null, false);
                        if (uploadResult.IsError)
                        {
                            Logger.Error(String.Format("Hibával tért vissza a dokumantunm feltöltés!\nErrorCode: {0}\nErrorMessage: {1}", uploadResult.ErrorCode, uploadResult.ErrorMessage));
                            return uploadResult;
                        }
                    }

                }

                catch (Exception exP)
                {
                    Logger.Error(String.Format("!!!!Nem allitjuk meg a folyamatot e hiba miatt!!!!\nFutasi hiba a property-k beallitasa kozben: {0}\nSource: {1}\nStackTrace: {2}"
                        , exP.Message
                        , exP.Source
                        , exP.StackTrace));
                }

                #endregion

                #region hash szamolasa

                Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: állomány megszerzése a hash szamolasahoz es hash szamolasa"));

                string sha1chk = sha1chk = Contentum.eDocument.SharePoint.Utility.CalculateSHA1(fileContent);

                #endregion

                #region a kapott adatok update a krt_dokumentumokba

                Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: a kapott adatok update a krt_dokumentumokba"));

                KRT_DokumentumokService dokumentumService = new KRT_DokumentumokService();

                ExecParam exUpdDok = ex.Clone();
                exUpdDok.Record_Id = xmlParams.SelectSingleNode("//krtdocument_id").InnerText;

                dok.Updated.SetValueAll(false);
                dok.Base.Updated.SetValueAll(false);

                dok.Base.Updated.Ver = true;

                dok.VerzioJel = aktVersion;
                dok.Updated.VerzioJel = true;

                //  kivonat: a sps-be feltoltott allapot, teljes file hash-e.
                dok.KivonatHash = sha1chk;
                dok.Updated.KivonatHash = true;

                result = dokumentumService.Update(exUpdDok, dok);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    Logger.Error(String.Format("Krt_dok update hiba!\nErrocode: {0}\nErrorMessage: {1}", result.ErrorCode, result.ErrorMessage));
                    return result;
                }

                #endregion


            }
            else
            {
                Logger.Info("Nem docx a formatum kiterjesztese, ezert nem csinalunk semmit.");
            }

            #region a visszatero xml visszallitasa

            Logger.Info(String.Format("SetDocumentPropertiesAndRevalidate: a visszatero xml osszeallitasa"));

            ///     <krtdocument_ver>A krt_dokumentumok rekord verziószáma.</krtdocument_ver>
            ///     <item_ver>A feltöltött item aktuális verizószáma.</item_ver>
            ///     <item_fullurl>Az elem teljes elérési útja.</item_fullurl>

            XmlNode ndUj1 = xmlParams.CreateNode(XmlNodeType.Element, "krtdocument_ver", "");
            ndUj1.InnerText = dok.Base.Ver;

            XmlNode ndUj2 = xmlParams.CreateNode(XmlNodeType.Element, "item_ver", "");
            ndUj2.InnerText = dok.VerzioJel;

            XmlNode ndUj3 = xmlParams.CreateNode(XmlNodeType.Element, "item_fullurl", "");
            ndUj3.InnerText = dok.External_Link;

            xmlParams.FirstChild.AppendChild(ndUj1);
            xmlParams.FirstChild.AppendChild(ndUj2);
            xmlParams.FirstChild.AppendChild(ndUj3);

            result.Record = xmlParams.OuterXml.ToString();

            #endregion

        }
        catch (Exception exc)
        {
            Logger.Error(String.Format("futási hiba a SetDocumentPropertiesAndRevalidate eljarasban. Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
            result.ErrorCode = "FUTHIBA00001";
            result.ErrorMessage = String.Format("Hiba az eDocumentService.SetDocumentPropertiesAndRevalidate eljárás futása közben! Hibaüzenet: {0}", exc.Message);
        }

        return result;
    }

    public Result MoveDokument(ExecParam execParam, EREC_UgyUgyiratok ugyirat, EREC_IraIratok irat, DataRow dokumentumRow)
    {
        Logger.Info("UCMDocumentumManager.MoveDokument kezdete");

        Result res = new Result();

        try
        {

            string ugyszam;
            string alszam;

            GetUgyszam(execParam, ugyirat, irat, out ugyszam, out alszam);

            string id = dokumentumRow["Id"].ToString();
            string ver = dokumentumRow["Ver"].ToString();
            string fileName = dokumentumRow["FajlNev"].ToString();
            string path = dokumentumRow["External_Info"].ToString();

            this.MoveDokument(execParam, path, fileName, ugyszam, alszam, id, ver);


        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
            Logger.Error("UCMDocumentumManager.MoveDokument hiba", execParam, res);
        }

        Logger.Info("UCMDocumentumManager.MoveDokument vege");
        return res;
    }

    void MoveDokument(ExecParam execParam, string path, string fileName, string ugyiratszam, string alszam, string dokumentumId, string dokumentumVer)
    {
        Result result = new Result();

        UCMServletManager manager = new UCMServletManager();

        //dokumentum lekérése
        ServletResponse getDocumentResponse = manager.GetDocumentContent(path);

        if (getDocumentResponse.IsError)
        {
            result.ErrorCode = getDocumentResponse.ErrorCode;
            result.ErrorMessage = getDocumentResponse.ErrorMessage;
            throw new ResultException(result);
        }

        byte[] cont = getDocumentResponse.ResponseData;

        //feltöltés új helyre
        Result uploadResult = this.Upload(cont, fileName, ugyiratszam, null, alszam, null, null, null, null, false);

        if (uploadResult.IsError)
        {
            throw new ResultException(uploadResult);
        }

        string newPath = uploadResult.Uid;
        string newLink = ServletHelper.GetExternalLink(newPath);

        //törlés régi helyről
        ServletResponse deleteResponse = manager.DeleteDocument(path);

        if (deleteResponse.IsError)
        {
            result.ErrorCode = deleteResponse.ErrorCode;
            result.ErrorMessage = deleteResponse.ErrorMessage;
            throw new ResultException(result);
        }

        KRT_Dokumentumok dokumentum = new KRT_Dokumentumok();
        dokumentum.Updated.SetValueAll(false);
        dokumentum.Base.Updated.SetValueAll(false);

        dokumentum.Base.Ver = dokumentumVer;
        dokumentum.Base.Updated.Ver = true;

        dokumentum.External_Info = newPath;
        dokumentum.Updated.External_Info = true;

        dokumentum.External_Link = newLink;
        dokumentum.Updated.External_Link = true;

        dokumentum.VerzioJel = "1";
        dokumentum.Updated.VerzioJel = true;

        Logger.Debug(String.Format("Dokumentumok.Update: {0}", dokumentumId));
        KRT_DokumentumokService dokumentumokService = new KRT_DokumentumokService();
        ExecParam xpmDokumentumUpdate = execParam.Clone();
        xpmDokumentumUpdate.Record_Id = dokumentumId;

        Result resDokumentumUpdate = dokumentumokService.Update(xpmDokumentumUpdate, dokumentum);

        if (resDokumentumUpdate.IsError)
        {
            Logger.Error("Dokumentumok.Update hiba", xpmDokumentumUpdate, resDokumentumUpdate);
            throw new ResultException(resDokumentumUpdate);
        }

    }

    void GetUgyszam(ExecParam execParam, EREC_UgyUgyiratok ugyirat, EREC_IraIratok irat, out string ugyszam, out string alszam)
    {
        EREC_IraIktatoKonyvekService iktatokonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        ExecParam iktatokonyvekExecParam = execParam.Clone();
        iktatokonyvekExecParam.Record_Id = ugyirat.IraIktatokonyv_Id;
        Result iktatokonyvekResult = iktatokonyvekService.Get(iktatokonyvekExecParam);

        if (iktatokonyvekResult.IsError)
            throw new ResultException(iktatokonyvekResult);

        EREC_IraIktatoKonyvek iktatokonyv = iktatokonyvekResult.Record as EREC_IraIktatoKonyvek;

        string iktatohely = iktatokonyv.Iktatohely;
        string foszam = ugyirat.Foszam;
        string ev = iktatokonyv.Ev;

        ugyszam = ServletHelper.GetUgyiratSzam(iktatohely, foszam, ev);
        alszam = irat.Alszam;
    }

    public Result Upload(byte[] content, string name, string ugyiratszam, string description, string alszam, string irattervezetszam, string irany, string acl, string generalt, bool extended)
    {
        Logger.Info("UCMDocumentumManager.Upload kezdete");

        Result result = new Result();

        try
        {
            UCMServletManager manager = new UCMServletManager();
            UploadResponse response = manager.Upload(content, name, ugyiratszam, description, alszam, irattervezetszam, irany, acl, generalt, extended);

            if (response.IsError)
            {
                Result error = new Result();
                error.ErrorCode = response.ErrorCode;
                error.ErrorMessage = response.ErrorMessage;
                Logger.Error(String.Format("Hibával tért vissza a dokumantum feltöltés!\nErrorCode: {0}\nErrorMessage: {1}", error.ErrorCode, error.ErrorMessage));
                return error;
            }

            result.Uid = response.Path;

            //jogosultság beállítása
            AddJogosultak(String.Empty, ugyiratszam, alszam);

        }
        catch (Exception ex)
        {
            Logger.Error("UCMDocumentumManager.Upload hiba!", ex);
            result = ResultException.GetResultFromException(ex);
        }

        Logger.Info("UCMDocumentumManager.Upload vege");
        return result;
    }

    public void AddJogosultak(string jog_felh_nids, string ugyszam, string alszam)
    {
        Logger.Info("UCMDocumentumManager.AddDefaultJogosultak kezdete");
        Logger.Debug(String.Format("jog_felh_nids={0}", jog_felh_nids));
        Logger.Debug(String.Format("ugyszam={0}", ugyszam));
        Logger.Debug(String.Format("alszam={0}", alszam));

        Result result = new Result();

        DataBaseManager dbManager = new DataBaseManager();
        List<Jogosultsag> currentJogosultak = dbManager.GetJogosultakList(ugyszam, alszam);

        if (!String.IsNullOrEmpty(jog_felh_nids))
        {
            List<string> jogosultak = new List<string>(jog_felh_nids.Split(','));

            List<string> newJogosultak = new List<string>();

            foreach (string jogosult in jogosultak)
            {
                if (!currentJogosultak.Exists(j => j.NId.Equals(jogosult, StringComparison.CurrentCultureIgnoreCase)))
                {
                    newJogosultak.Add(jogosult);
                }
            }

            string nid = UCMServletManager.Nid;

            foreach (string jogosult in newJogosultak)
            {
                StoredProcedureResult res = dbManager.Felh_Hozaad_Iktszam(jogosult, ugyszam, alszam, "R", nid, "Contentum");

                if (res.IsError)
                {
                    result.ErrorCode = res.ErrorMessage;
                    result.ErrorMessage = res.ErrorMessage;
                    throw new ResultException(result);
                }
            }
        }

        AddDefaultJogosultak(currentJogosultak, ugyszam, alszam);

        Logger.Info("UCMDocumentumManager.AddDefaultJogosultak vege");

    }

    private void AddDefaultJogosultak(List<Jogosultsag> currentJogosultak, string ugyszam, string alszam)
    {
        Logger.Info("UCMDocumentumManager.AddDefaultJogosultak kezdete");

        string defaultJogosultNid = UCMServletManager.Nid;

        if (!currentJogosultak.Exists(j => j.NId.Equals(defaultJogosultNid, StringComparison.CurrentCultureIgnoreCase)))
        {
            DataBaseManager dbManager = new DataBaseManager();
            StoredProcedureResult spRes = dbManager.Felh_Hozaad_Iktszam(defaultJogosultNid, ugyszam, alszam, "RW", defaultJogosultNid, "Contentum");

            if (spRes.IsError)
            {
                Result result = new Result();
                result.ErrorCode = spRes.ErrorMessage;
                result.ErrorMessage = spRes.ErrorMessage;
                throw new ResultException(result);
            }
        }

        Logger.Info("UCMDocumentumManager.AddDefaultJogosultak vege");
    }
}