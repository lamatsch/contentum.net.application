﻿using Microsoft.Office.DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace UCM
{
    /// <summary>
    /// Summary description for WordProcessing
    /// </summary>
    public class WordProcessing
    {
        static readonly XNamespace p = "http://schemas.microsoft.com/office/2006/metadata/properties";
        static readonly XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";

        public static bool SetDocumentProperties(ref byte[] fileContent, Dictionary<string, string> properties)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(fileContent, 0, fileContent.Length);
                using (var document = WordprocessingDocument.Open(stream, true))
                {
                    if (SetDocumentProperties(document, properties))
                    {
                        fileContent = stream.ToArray();
                        return true;
                    }
                }
            }

            return false;
        }

        static bool SetDocumentProperties(WordprocessingDocument document, Dictionary<string, string> properties)
        {
            var parts = document.MainDocumentPart.GetPartsOfType<CustomXmlPart>();

            foreach (var part in parts)
            {
                XmlReader reader = XmlReader.Create(part.GetStream());
                XDocument doc = XDocument.Load(reader);
                var propertiesElement = doc.Element(p + "properties");

                if (propertiesElement != null)
                {
                    bool set = false;
                    foreach (KeyValuePair<string, string> prop in properties)
                    {
                        set = SetDocumentProperty(propertiesElement, prop.Key, prop.Value) || set;
                    }

                    if (set)
                    {
                        MemoryStream ms = new MemoryStream();
                        using (XmlWriter writer = XmlWriter.Create(ms))
                        {
                            doc.Save(writer);
                        }
                        ms.Position = 0;
                        part.FeedData(ms);

                        return true;
                    }
                }
            }

            return false;
        }

        static bool SetDocumentProperty(XElement propertiesElement, string propertyName, string propertyValue)
        {
            Logger.Debug(String.Format("SetDocumentProperty: propertyName={0}, propertyValue={1}", propertyName, propertyValue));

            var prop = propertiesElement.Descendants().Where(e => e.Name.LocalName == propertyName).FirstOrDefault();

            if (prop != null)
            {
                prop.Value = propertyValue;
                var nil = prop.Attribute(xsi + "nil");

                if (!String.IsNullOrEmpty(prop.Value))
                {
                    if (nil != null)
                    {
                        nil.Remove();
                    }
                }
                else
                {
                    if (nil == null)
                    {
                        nil = new XAttribute(xsi + "nil", true);
                        prop.Add(nil);
                    }
                }

                return true;
            }

            return false;
        }
    }
}