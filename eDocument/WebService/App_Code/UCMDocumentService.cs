﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using NMHH.UCM.Database;
using NMHH.UCM.Servlet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for UCMDocumentService
/// </summary>
[WebService(Namespace = "Contentum.eDocument.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class UCMDocumentService : System.Web.Services.WebService
{
    private DataContext dataContext;

    public UCMDocumentService()
    {
        dataContext = new DataContext(this.Application);
    }

    public UCMDocumentService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }

    [WebMethod]
    public Result RefreshDocuments(ExecParam execParam, string iratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();

        try
        {

            string ugyszam;
            string alszam;

            GetUgyszamAndAlszam(execParam, iratId, out ugyszam, out alszam);

            //Lekérjük az iratokhoz tartozó EREC_Csatolmanyok-at
            KRT_DokumentumokService dokumentumokService1 = new KRT_DokumentumokService();
            KRT_DokumentumokSearch dokumentumokSearch = new KRT_DokumentumokSearch();
            dokumentumokSearch.Id.Value = String.Format("select Dokumentum_Id from EREC_Csatolmanyok where IraIrat_Id = '{0}'", iratId);
            dokumentumokSearch.Id.Operator = Query.Operators.inner;
            dokumentumokSearch.ErvKezd.Clear();
            dokumentumokSearch.ErvVege.Clear();
            Result dokumentumokResult = dokumentumokService1.GetAll(execParam, dokumentumokSearch);

            if (dokumentumokResult.IsError)
                return dokumentumokResult;

            //megkeressük, hogy van-e már fődokumentum csatolva az irathoz
            EREC_CsatolmanyokService csatolmanyokService = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            EREC_CsatolmanyokSearch csatolmanySearch = new EREC_CsatolmanyokSearch();
            csatolmanySearch.IraIrat_Id.Value = iratId;
            csatolmanySearch.IraIrat_Id.Operator = Query.Operators.equals;
            csatolmanySearch.DokumentumSzerep.Value = Contentum.eUtility.KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
            csatolmanySearch.DokumentumSzerep.Operator = Query.Operators.equals;

            Result dokumentumCsatolmanyokForIratResult = csatolmanyokService.GetAll(execParam, csatolmanySearch);

            if (dokumentumCsatolmanyokForIratResult.IsError)
                return dokumentumokResult;

            bool hasFodokumentum = dokumentumCsatolmanyokForIratResult.GetData<EREC_Csatolmanyok>().Count > 0;

            DataTable dokumentumokTable = dokumentumokResult.Ds.Tables[0];

            List<string> externalInfoList = new List<string>();

            foreach (DataRow row in dokumentumokTable.Rows)
            {
                string externalInfo = row["External_Info"] as string;
                externalInfoList.Add(externalInfo);
            }

            //lekérjük az UCM ből az ugyszamhoz és az alszamhoz tartozo dokumentumokat
            UCMServletManager manager = new UCMServletManager();
            GetDocumentsReponse response = manager.GetDocuments(ugyszam, alszam, null, null, null, null, "1000", null);

            if (response.IsError)
            {
                result.ErrorCode = response.ErrorCode;
                result.ErrorMessage = response.ErrorMessage;
                return result;
            }

            //Azokat a dokumentumokat amik nincsenek jelenleg az irathoz csatolva kigyüjtjük mint uj dokumentumok
            List<Dokumentum> ujDokumentumok = new List<Dokumentum>();

            foreach (Dokumentum dokumentum in response.Dokumentumok)
            {
                if (!externalInfoList.Contains(dokumentum.Path))
                {
                    ujDokumentumok.Add(dokumentum);
                }
            }

            if (ujDokumentumok.Count == 0)
            {
                return result;
            }

            bool isConnectionOpenHere = false;
            bool isTransactionBeginHere = false;
            List<string> ujDokumentumIdList = new List<string>();
            List<string> ujPDFDokumentumokIdList = new List<string>();

            string lastUjFodokumentumPDFId = string.Empty;
            DateTime lastUjFodokumentumPDFTimeStamp = DateTime.MinValue;
            //feltöltjük az új dokumentumokat 
            try
            {
                isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
                isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

                KRT_DokumentumokService dokumentumokService2 = new KRT_DokumentumokService(this.dataContext);

                foreach (Dokumentum dokumentum in ujDokumentumok)
                {
                    string fajlNev = dokumentum.FajlNev;

                    //alszám levágása
                    if (fajlNev.Length > 6)
                    {
                        fajlNev = fajlNev.Substring(6);
                    }

                    KRT_Dokumentumok krt_dokumentum = new KRT_Dokumentumok();
                    krt_dokumentum.FajlNev = fajlNev;
                    krt_dokumentum.Formatum = eDocumentUtility.GetFormatum(execParam, fajlNev);
                    krt_dokumentum.Csoport_Id_Tulaj = execParam.Felhasznalo_Id;
                    krt_dokumentum.FajlNev = fajlNev.Trim();
                    krt_dokumentum.Meret = dokumentum.Meret;
                    krt_dokumentum.Allapot = "1";
                    krt_dokumentum.Tipus = eDocumentUtility.GetFormatum(execParam, fajlNev);
                    krt_dokumentum.Formatum = Path.GetExtension(fajlNev).Trim(eDocumentUtility.kiterjesztesVegeirol);
                    krt_dokumentum.Alkalmazas_Id = execParam.Alkalmazas_Id;
                    krt_dokumentum.External_Link = ServletHelper.GetExternalLink(dokumentum.Path);
                    krt_dokumentum.External_Info = dokumentum.Path;
                    krt_dokumentum.External_Source = Contentum.eUtility.Constants.DocumentStoreType.UCM;
                    krt_dokumentum.Leiras = dokumentum.FajlNev;
                    krt_dokumentum.TartalomHash = "<null>";
                    krt_dokumentum.KivonatHash = "<null>";


                    Result dokumentumInsertResult = dokumentumokService2.Insert(execParam, krt_dokumentum);

                    if (dokumentumInsertResult.IsError)
                    {
                        throw new ResultException(dokumentumInsertResult);
                    }

                    string dokumentumId = dokumentumInsertResult.Uid;
                    ujDokumentumIdList.Add(dokumentumId);

                    Logger.Debug("UCMDocumentService.RefreshDocuemnts. Fajlnev: " + krt_dokumentum.FajlNev + " UtolsoModositas: " + dokumentum.UtolsoModositasIdeje);
                    //ha nincs fodokumentum, akkor az uj dokumentumok kozul az utolso PDF formatumu lesz a fodokumentum
                    //ezert az uj dokumentumok kozul megkeressuk azt amelyik ezt a feltetelt teljesiti
                    bool isPDF = krt_dokumentum.FajlNev.ToLower().EndsWith(".pdf");
                    bool isOlderThenPrevious = ServletHelper.ConvertTimestampToDateTime(dokumentum.UtolsoModositasIdeje) > lastUjFodokumentumPDFTimeStamp;
                    if (isPDF && isOlderThenPrevious)
                    {
                        Logger.Debug("Found new Fodokumentum");
                        lastUjFodokumentumPDFId = dokumentumId;
                        lastUjFodokumentumPDFTimeStamp = (DateTime)ServletHelper.ConvertTimestampToDateTime(dokumentum.UtolsoModositasIdeje);
                    }
                }

                // COMMIT
                dataContext.CommitIfRequired(isTransactionBeginHere);
            }
            catch (Exception e)
            {
                dataContext.RollbackIfRequired(isTransactionBeginHere);
                result = ResultException.GetResultFromException(e);
                Logger.Error("UCMDocumentService.RefreshDocuments hiba:", e);
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }

            if (result.IsError)
            {
                return result;
            }

            //hozzácsatoljuk az uj dokumentumokat az irathoz
            foreach (string dokumentumId in ujDokumentumIdList)
            {
                EREC_Csatolmanyok csatolmany = new EREC_Csatolmanyok();
                csatolmany.IraIrat_Id = iratId;
                csatolmany.Dokumentum_Id = dokumentumId;
                csatolmany.DokumentumSzerep = Contentum.eUtility.KodTarak.DOKUMENTUM_SZEREP.DokumentumMelleklet;

                //ha nincs fődokumentum és van új fődokumentum jelöltünk akkor ez lesz az
                if (!hasFodokumentum && dokumentumId == lastUjFodokumentumPDFId)
                {
                    csatolmany.DokumentumSzerep = Contentum.eUtility.KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
                }

                Result csatolmanyokResult = csatolmanyokService.Insert(execParam, csatolmany);

                if (csatolmanyokResult.IsError)
                {
                    return csatolmanyokResult;
                }
            }
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error("UCMDocumentService.RefreshDocuments hiba:", ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    public Result CheckOut(ExecParam execParam, List<string> dokumentumIdList)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        if (dokumentumIdList.Count > 0)
        {

            try
            {
                KRT_DokumentumokService dokumentumokService = new KRT_DokumentumokService();
                KRT_DokumentumokSearch dokumentumokSearch = new KRT_DokumentumokSearch();
                dokumentumokSearch.Id.Value = Search.GetSqlInnerString(dokumentumIdList.ToArray());
                dokumentumokSearch.Id.Operator = Query.Operators.inner;

                Result dokumentumokResult = dokumentumokService.GetAll(execParam, dokumentumokSearch);

                if (dokumentumokResult.IsError)
                {
                    throw new ResultException(dokumentumokResult);
                }

                UCMServletManager manager = new UCMServletManager();
                foreach (DataRow row in dokumentumokResult.Ds.Tables[0].Rows)
                {
                    string path = row["External_Info"].ToString();

                    ServletResponse response = manager.Version(path, VersionFunctions.checkOut);

                    if (response.IsError)
                    {
                        result.ErrorCode = response.ErrorCode;
                        result.ErrorMessage = response.ErrorMessage;
                        throw new ResultException(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result = ResultException.GetResultFromException(ex);
                Logger.Error("UCMDocumentService.CheckOut hiba:", ex);
            }
        }

        log.WsEnd(execParam, result);

        return result;
    }

    [WebMethod]
    public Result CancelCheckOut(ExecParam execParam, List<string> dokumentumIdList)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        if (dokumentumIdList.Count > 0)
        {

            try
            {
                KRT_DokumentumokService dokumentumokService = new KRT_DokumentumokService();
                KRT_DokumentumokSearch dokumentumokSearch = new KRT_DokumentumokSearch();
                dokumentumokSearch.Id.Value = Search.GetSqlInnerString(dokumentumIdList.ToArray());
                dokumentumokSearch.Id.Operator = Query.Operators.inner;

                Result dokumentumokResult = dokumentumokService.GetAll(execParam, dokumentumokSearch);

                if (dokumentumokResult.IsError)
                {
                    throw new ResultException(dokumentumokResult);
                }

                UCMServletManager manager = new UCMServletManager();
                foreach (DataRow row in dokumentumokResult.Ds.Tables[0].Rows)
                {
                    string path = row["External_Info"].ToString();

                    ServletResponse response = manager.Version(path, VersionFunctions.cancelCheckOut);

                    if (response.IsError)
                    {
                        result.ErrorCode = response.ErrorCode;
                        result.ErrorMessage = response.ErrorMessage;
                        throw new ResultException(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result = ResultException.GetResultFromException(ex);
                Logger.Error("UCMDocumentService.CancelCheckOut hiba:", ex);
            }
        }

        log.WsEnd(execParam, result);

        return result;
    }

    string MoveDocument(ExecParam execParam, string path, string fileName, string ugyiratszam, string alszam)
    {
        Result result = new Result();

        string newPath = null;

        UCMServletManager manager = new UCMServletManager();

        //dokumentum lekérése
        ServletResponse getDocumentResponse = manager.GetDocumentContent(path);

        if (getDocumentResponse.IsError)
        {
            result.ErrorCode = getDocumentResponse.ErrorCode;
            result.ErrorMessage = getDocumentResponse.ErrorMessage;
            throw new ResultException(result);
        }

        byte[] cont = getDocumentResponse.ResponseData;

        //feltöltés új helyre
        UCMDocumentumManager ucmManager = new UCMDocumentumManager();
        Result uploadResult = ucmManager.Upload(cont, fileName, ugyiratszam, null, alszam, null, null, null, null, false);

        if (uploadResult.IsError)
        {
            throw new ResultException(uploadResult);
        }

        newPath = uploadResult.Uid;

        //törlés régi helyről
        ServletResponse deleteResponse = manager.DeleteDocument(path);

        if (deleteResponse.IsError)
        {
            result.ErrorCode = deleteResponse.ErrorCode;
            result.ErrorMessage = deleteResponse.ErrorMessage;
            throw new ResultException(result);
        }

        return newPath;

    }

    [WebMethod]
    public Result MoveKuldemenyDocuments(ExecParam execParam, string dokumentumId, string iktatohely, string foszam, string ev, string alszam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string ugyszam = ServletHelper.GetUgyiratSzam(iktatohely, foszam, ev);

            KRT_DokumentumokService dokumentumokService = new KRT_DokumentumokService(this.dataContext);
            ExecParam getExecParam = execParam.Clone();
            getExecParam.Record_Id = dokumentumId;
            Result dokumentumokResult = dokumentumokService.Get(getExecParam);

            if (dokumentumokResult.IsError)
                throw new ResultException(dokumentumokResult);

            KRT_Dokumentumok dokumentum = dokumentumokResult.Record as KRT_Dokumentumok;

            //áthelyezés ucm-ben
            string newPath = MoveDocument(execParam, dokumentum.External_Info, dokumentum.FajlNev, ugyszam, alszam);
            string newLink = ServletHelper.GetExternalLink(newPath);

            //dokumentum update
            dokumentum.Updated.SetValueAll(false);
            dokumentum.Base.Updated.SetValueAll(false);
            dokumentum.Base.Updated.Ver = true;

            dokumentum.External_Info = newPath;
            dokumentum.Updated.External_Info = true;

            dokumentum.External_Link = newLink;
            dokumentum.Updated.External_Link = true;

            ExecParam updateExecParam = execParam.Clone();
            updateExecParam.Record_Id = dokumentumId;

            Result updateResult = dokumentumokService.Update(updateExecParam, dokumentum);

            if (updateResult.IsError)
                throw new ResultException(updateResult);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception ex)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(ex);
            Logger.Error("UCMDocumentService.MoveKuldemenyDocuments hiba:", ex);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    void GetUgyszamAndAlszam(ExecParam execParam, string iratId, out string ugyszam, out string alszam)
    {
        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam iratokExecParam = execParam.Clone();
        iratokExecParam.Record_Id = iratId;
        Result iratokResult = iratokService.Get(iratokExecParam);

        if (iratokResult.IsError)
            throw new ResultException(iratokResult);

        EREC_IraIratok irat = iratokResult.Record as EREC_IraIratok;

        EREC_UgyUgyiratokService ugyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam ugyiratokExecParam = execParam.Clone();
        ugyiratokExecParam.Record_Id = irat.Ugyirat_Id;
        Result ugyiratokResult = ugyiratokService.Get(ugyiratokExecParam);

        if (ugyiratokResult.IsError)
            throw new ResultException(ugyiratokResult);

        EREC_UgyUgyiratok ugyirat = ugyiratokResult.Record as EREC_UgyUgyiratok;

        EREC_IraIktatoKonyvekService iktatokonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        ExecParam iktatokonyvekExecParam = execParam.Clone();
        iktatokonyvekExecParam.Record_Id = ugyirat.IraIktatokonyv_Id;
        Result iktatokonyvekResult = iktatokonyvekService.Get(iktatokonyvekExecParam);

        if (iktatokonyvekResult.IsError)
            throw new ResultException(iktatokonyvekResult);

        EREC_IraIktatoKonyvek iktatokonyv = iktatokonyvekResult.Record as EREC_IraIktatoKonyvek;

        string iktatohely = iktatokonyv.Iktatohely;
        string foszam = ugyirat.Foszam;
        string ev = iktatokonyv.Ev;

        ugyszam = ServletHelper.GetUgyiratSzam(iktatohely, foszam, ev);
        alszam = irat.Alszam;
    }

    [WebMethod]
    public Result FullTextSearch(ExecParam execParam, string szoveg)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            UCMServletManager manager = new UCMServletManager();
            GetDocumentsReponse response = manager.FullTextSearch(szoveg, null, null, null, null);

            if (response.IsError)
            {
                result.ErrorCode = response.ErrorCode;
                result.ErrorMessage = response.ErrorMessage;
                throw new ResultException(result);
            }

            DataSet ds = new DataSet();
            DataTable table = new DataTable();
            ds.Tables.Add(table);

            table.Columns.Add("Title", typeof(string));
            table.Columns.Add("Path", typeof(string));

            foreach (Dokumentum dokumentum in response.Dokumentumok)
            {
                DataRow row = table.NewRow();
                row["Title"] = dokumentum.FajlNev;
                row["Path"] = dokumentum.Path;
                table.Rows.Add(row);
            }

            result.Ds = ds;


        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error("UCMDocumentService.FullTextSearch hiba:", ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    public Result SetIratJogosultak(ExecParam execParam, string jog_felh_nids, string iktatohely, string foszam, string ev, string alszam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            string ugyszam = ServletHelper.GetUgyiratSzam(iktatohely, foszam, ev);
            UCMDocumentumManager ucmManager = new UCMDocumentumManager();
            ucmManager.AddJogosultak(jog_felh_nids, ugyszam, alszam);

        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error("UCMDocumentService.SetIratJogosultak hiba:", ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    public Result SetJogosultak(ExecParam execParam, string jogtargyId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Debug(String.Format("jogtargyId={0}", jogtargyId));

        Result result = new Result();

        try
        {
            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
            iratokSearch.Id.Value = jogtargyId;
            iratokSearch.Id.Operator = Query.Operators.equals;
            Result iratokResult = iratokService.GetAll(execParam, iratokSearch);

            if (iratokResult.IsError)
                throw new ResultException(iratokResult);

            //irat
            if (iratokResult.Ds.Tables[0].Rows.Count == 1)
            {
                string iratId = iratokResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                string ugyszam;
                string alszam;
                GetUgyszamAndAlszam(execParam, iratId, out ugyszam, out alszam);

                Result jogosultakResult = iratokService.GetCsatolmanyJogosultak(execParam, iratId);

                if (jogosultakResult.IsError)
                {
                    throw new ResultException(jogosultakResult);
                }

                List<string> userNevek = new List<string>();

                foreach (DataRow row in jogosultakResult.Ds.Tables[0].Rows)
                {
                    userNevek.Add(row["UserNev"].ToString());
                }

                string jog_felh_nids = String.Join(",", userNevek.ToArray());

                UCMDocumentumManager ucmManager = new UCMDocumentumManager();
                ucmManager.AddJogosultak(jog_felh_nids, ugyszam, alszam);
            }
            else
            {
                Logger.Debug("Nem irat!");
            }


        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error("UCMDocumentService.SetJogosultak hiba:", ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    public Result RefreshJogosultakFromUcm(ExecParam execParam, string iratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            string ugyszam;
            string alszam;
            GetUgyszamAndAlszam(execParam, iratId, out ugyszam, out alszam);

            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result jogosultakResult = iratokService.GetCsatolmanyJogosultak(execParam, iratId);

            if (jogosultakResult.IsError)
            {
                throw new ResultException(jogosultakResult);
            }

            List<string> oldJogosultak = new List<string>();

            foreach (DataRow row in jogosultakResult.Ds.Tables[0].Rows)
            {
                oldJogosultak.Add(row["UserNev"].ToString());
            }

            DataBaseManager manager = new DataBaseManager();

            List<Jogosultsag> newJogosultak = manager.GetJogosultakList(ugyszam, alszam);

            List<Jogosultsag> contentumJogosultak = new List<Jogosultsag>();

            foreach (Jogosultsag jogosult in newJogosultak)
            {
                if (!oldJogosultak.Exists(j => j.Equals(jogosult.NId, StringComparison.CurrentCultureIgnoreCase)))
                {
                    contentumJogosultak.Add(jogosult);
                }
            }


            if (contentumJogosultak.Count > 0)
            {

                Dictionary<string, string> userIds = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

                KRT_FelhasznalokService felhasznalokService = new KRT_FelhasznalokService();
                KRT_FelhasznalokSearch felhasznalokSearch = new KRT_FelhasznalokSearch();
                felhasznalokSearch.UserNev.Value = Search.GetSqlInnerString(contentumJogosultak.Select(j => j.NId).ToArray());
                felhasznalokSearch.UserNev.Operator = Query.Operators.inner;

                Result felhasznalokResult = felhasznalokService.GetAll(execParam, felhasznalokSearch);

                if (felhasznalokResult.IsError)
                {
                    throw new ResultException(felhasznalokResult);
                }

                foreach (DataRow row in felhasznalokResult.Ds.Tables[0].Rows)
                {
                    string id = row["id"].ToString();
                    string userNev = row["UserNev"].ToString();

                    if (!userIds.ContainsKey(userNev))
                    {
                        userIds.Add(userNev, id);
                    }
                }

                RightsService rightService = new RightsService();


                foreach (Jogosultsag jogosult in contentumJogosultak)
                {
                    if (userIds.ContainsKey(jogosult.NId))
                    {
                        string felhasznaloId = userIds[jogosult.NId];

                        char jogszint = 'O';

                        if (jogosult.Jog == "RW")
                        {
                            jogszint = 'I';
                        }

                        Result rightResult = rightService.AddCsoportToJogtargyWithJogszint(execParam, iratId, felhasznaloId, 'I', jogszint, '0');

                        if (rightResult.IsError)
                        {
                            Logger.Error(String.Format("AddCsoportToJogtargyWithJogszint hiba: {0}", rightResult.ErrorMessage));
                        }
                    }
                    else
                    {
                        Logger.Error(String.Format("UserNev nem található: {0}", jogosult.NId));
                    }
                }
            }

        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error("UCMDocumentService.RefreshJogosultakFromUcm hiba:", ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    public Result RemoveJogosultak(ExecParam execParam, string jogtargyId, string csoportId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
            iratokSearch.Id.Value = jogtargyId;
            iratokSearch.Id.Operator = Query.Operators.equals;
            Result iratokResult = iratokService.GetAll(execParam, iratokSearch);

            if (iratokResult.IsError)
                throw new ResultException(iratokResult);

            //irat
            if (iratokResult.Ds.Tables[0].Rows.Count == 1)
            {
                string iratId = iratokResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                string ugyszam;
                string alszam;
                GetUgyszamAndAlszam(execParam, iratId, out ugyszam, out alszam);

                DataBaseManager dbManager = new DataBaseManager();

                List<Jogosultsag> oldJogosultak = dbManager.GetJogosultakList(ugyszam, alszam);

                KRT_FelhasznalokService felhasznalokService = new KRT_FelhasznalokService();
                KRT_FelhasznalokSearch felhasznalokSearch = new KRT_FelhasznalokSearch();
                felhasznalokSearch.Id.Value = csoportId;
                felhasznalokSearch.Id.Operator = Query.Operators.equals;

                Result felhasznalokResult = felhasznalokService.GetAll(execParam, felhasznalokSearch);

                if (felhasznalokResult.IsError)
                {
                    throw new ResultException(felhasznalokResult);
                }

                if (felhasznalokResult.Ds.Tables[0].Rows.Count > 0)
                {
                    string userNev = felhasznalokResult.Ds.Tables[0].Rows[0]["UserNev"].ToString();

                    if (oldJogosultak.Exists(j => j.NId.Equals(userNev, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        string nid = ConfigurationManager.AppSettings["UCM_nid"];
                        StoredProcedureResult res = dbManager.Felh_Torol_Iktszam(userNev, ugyszam, alszam, nid, "Contentum");

                        if (res.IsError)
                        {
                            result.ErrorCode = res.ErrorMessage;
                            result.ErrorMessage = res.ErrorMessage;
                            throw new ResultException(result);
                        }
                    }
                    else
                    {
                        Logger.Info(String.Format("A felhasználó nincs benne az ucm jogosultság táblában: {0}", userNev));
                    }
                }
                else
                {
                    Logger.Error(String.Format("A felhasználó id nem található: {0}", csoportId));
                }
            }
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error("UCMDocumentService.RemoveJogosultak hiba:", ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    public Result SetJogosultakTomeges(ExecParam execParam, string[] jogtargyIds, string obj_type)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Debug(String.Format("jogtargyIds={0}", jogtargyIds));
        Logger.Debug(String.Format("obj_type={0}", obj_type));

        Result result = new Result();

        try
        {
            List<string> iratIds = new List<string>();
            if (obj_type == Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok)
            {
                EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
                iratokSearch.Ugyirat_Id.Value = Search.GetSqlInnerString(jogtargyIds);
                iratokSearch.Ugyirat_Id.Operator = Query.Operators.inner;

                Result iratokResult = iratokService.GetAll(execParam, iratokSearch);

                if (iratokResult.IsError)
                    throw new ResultException(iratokResult);

                foreach (DataRow row in iratokResult.Ds.Tables[0].Rows)
                {
                    iratIds.Add(row["Id"].ToString());
                }
            }
            else if (obj_type == Contentum.eUtility.Constants.TableNames.EREC_IraIratok)
            {
                iratIds.AddRange(jogtargyIds);
            }
            else
            {
                Logger.Debug("Nem ügyirat vagy irat!");
            }

            if (iratIds.Count > 0)
            {
                foreach (string iratId in iratIds)
                {
                    SetJogosultak(execParam, iratId);
                }
            }
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error("UCMDocumentService.SetJogosultakTomeges hiba:", ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_DokumentumokList))]
    public Result GetDocuments(ExecParam execParam, string ugyszam, string alszam, bool addJogosultsag)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();

        try
        {
            UCMServletManager servletManager = new UCMServletManager();
            GetDocumentsReponse response = servletManager.GetDocuments(ugyszam, alszam, null, null, null, null, "1000", null);

            if (response.IsError)
            {
                result.ErrorCode = response.ErrorCode;
                result.ErrorMessage = response.ErrorMessage;
                return result;
            }

            List<KRT_Dokumentumok> dokumentumok = new List<KRT_Dokumentumok>();

            foreach (Dokumentum ucmDokumentum in response.Dokumentumok)
            {
                //alszám levágása
                string fajlNev = Regex.Replace(ucmDokumentum.FajlNev, "^\\d{5}_", "");

                KRT_Dokumentumok krt_dokumentum = new KRT_Dokumentumok();
                krt_dokumentum.FajlNev = fajlNev;
                krt_dokumentum.Formatum = eDocumentUtility.GetFormatum(execParam, fajlNev);

                krt_dokumentum.Csoport_Id_Tulaj = execParam.Felhasznalo_Id;
                krt_dokumentum.FajlNev = fajlNev.Trim();
                krt_dokumentum.Meret = ucmDokumentum.Meret;
                krt_dokumentum.Allapot = "1";
                krt_dokumentum.Tipus = eDocumentUtility.GetFormatum(execParam, fajlNev);
                krt_dokumentum.Formatum = Path.GetExtension(fajlNev).Trim(eDocumentUtility.kiterjesztesVegeirol);
                krt_dokumentum.Alkalmazas_Id = execParam.Alkalmazas_Id;
                krt_dokumentum.External_Link = ServletHelper.GetExternalLink(ucmDokumentum.Path);
                krt_dokumentum.External_Info = ucmDokumentum.Path;
                krt_dokumentum.External_Source = Contentum.eUtility.Constants.DocumentStoreType.UCM;
                krt_dokumentum.Leiras = ucmDokumentum.FajlNev;
                krt_dokumentum.TartalomHash = "<null>";
                krt_dokumentum.KivonatHash = "<null>";

                dokumentumok.Add(krt_dokumentum);
            }

            result.Record = new KRT_DokumentumokList(dokumentumok);

            if (addJogosultsag)
            {
                UCMDocumentumManager ucmManager = new UCMDocumentumManager();
                ucmManager.AddJogosultak(String.Empty, ugyszam, alszam);
            }
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error("UCMDocumentService.GetDocuments hiba:", ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

}
