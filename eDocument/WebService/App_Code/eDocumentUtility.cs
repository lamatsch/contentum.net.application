﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Xml;
using System.IO;

/// <summary>
/// Summary description for Utility
/// </summary>
public class eDocumentUtility
{
    public eDocumentUtility()
    {
    }

    public static Contentum.eUtility.BusinessService GeteRecordServiceFactory()
    {
        Contentum.eUtility.BusinessService bs = new Contentum.eUtility.BusinessService();
        bs.Type = ConfigurationManager.AppSettings.Get("eRecordBusinessServiceType");
        bs.Url = ConfigurationManager.AppSettings.Get("eRecordBusinessServiceUrl");
        bs.Authentication = ConfigurationManager.AppSettings.Get("eRecordBusinessServiceAuthentication");
        //bs.UserDomain = "";
        //bs.UserName = Utility.GetUserName();
        //bs.Password = "";
        return bs;
        //return Contentum.eRecord.BaseUtility.eRecordService.GetServiceFactory(bs);
    }

    public static Result GetKodTar(ExecParam ex1, string p_kodcsoport)
    {

        Logger.Info("GetKodTar indul");
        Result mresult = new Result();

        KRT_KodCsoportokService kcsServ = new KRT_KodCsoportokService();
        KRT_KodCsoportokSearch kcss = new KRT_KodCsoportokSearch();
        kcss.Kod.Value = p_kodcsoport; // "DOKUMENTUMTIPUS";
        kcss.Kod.Operator = Contentum.eQuery.Query.Operators.equals;

        Result kodcsoportResult = kcsServ.GetAll(ex1, kcss);

        // akarmi hiba
        if (!String.IsNullOrEmpty(kodcsoportResult.ErrorCode))
        {
            return kodcsoportResult;
        }

        Logger.Info(String.Format("GetKodTar: kodtarcsoportok talalt rekordok szama: {0}", kodcsoportResult.Ds.Tables[0].Rows.Count));

        // nincs meg a keresett kodcsoport hiba
        if (kodcsoportResult.Ds.Tables[0].Rows.Count == 0)
        {
            mresult.ErrorCode = "ROWC1";
            mresult.ErrorMessage = "Nincs meg a keresett kodcsoport.";
            return mresult;
        }

        // tul sok kodcsoport vissza hiba
        if (kodcsoportResult.Ds.Tables[0].Rows.Count > 1)
        {
            mresult.ErrorCode = "ROWC2";
            mresult.ErrorMessage = "A kodcsoport kereses soran kapott sorok szama nem 1.";
            return mresult;
        }

        KRT_KodTarakService ktSerc = new KRT_KodTarakService();
        KRT_KodTarakSearch kts = new KRT_KodTarakSearch();

        kts.KodCsoport_Id.Value = kodcsoportResult.Ds.Tables[0].Rows[0]["Id"].ToString();
        kts.KodCsoport_Id.Operator = Contentum.eQuery.Query.Operators.equals;

        Result kodtarResult = ktSerc.GetAll(ex1, kts);

        Logger.Info(String.Format("GetKodTar: kodtar ertekek talalt rekordok szama: {0}", kodtarResult.Ds.Tables[0].Rows.Count));

        return kodtarResult;
    }

    public static Result GetDoc(ExecParam execparam)
    {
        Logger.Info("GetDoc indul.");

        if (String.IsNullOrEmpty(execparam.Record_Id))
        {
            Logger.Error("Ures az execparam Record_Id mezoje!");
            Result reske = new Result();
            reske.ErrorCode = "PARAM0001";
            reske.ErrorMessage = "eDocumentWebService.DocumentService.GetDoc (private) method paraméterezési hiba! Üres az execparam Record_Id mezoje!";
            return reske;
        }

        KRT_DokumentumokService dokService = new KRT_DokumentumokService();
        Result dokumentumResult = dokService.Get(execparam);

        if (!String.IsNullOrEmpty(dokumentumResult.ErrorCode))
            Logger.Error(String.Format("Hibaval tert vissza a dokService.Get: {0}\nErrMsg: {1}"
                , dokumentumResult.ErrorCode
                , dokumentumResult.ErrorMessage));

        //KRT_Dokumentumok dokumentum = (KRT_Dokumentumok)dokumentumResult.Record;

        return dokumentumResult;
    }

    public static char[] kiterjesztesVegeirol = { '.', ' ' };

    public static string GetFormatum(ExecParam ex1, string filename)
    {
        Logger.Info(String.Format("DocumentService.GetFormatum indul. Filename: {0}", filename));

        Result kodok = GetKodTar(ex1, "DOKUMENTUM_FORMATUM");

        if (!String.IsNullOrEmpty(kodok.ErrorCode))
        {
            Logger.Info(String.Format("DOKUMENTUM_FORMATUM kodlekerdezesek hiba: {0} {1}", kodok.ErrorCode, kodok.ErrorMessage));
            return "Egyeb";
        }

        if (kodok.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Info(String.Format("DOKUMENTUM_FORMATUM kodlekerdezesek ures, nincs eredmenysor."));
            return "Egyeb";
        }

        Logger.Info(String.Format("kapott kodok darabszama: {0}", kodok.Ds.Tables[0].Rows.Count));

        string kiterjesztes = Path.GetExtension(filename).Trim(kiterjesztesVegeirol);
        Logger.Info(String.Format("Megallapitott kiterjesztes: {0}", kiterjesztes));

        int idx = 0;
        string retVal = "";
        while ((idx < kodok.Ds.Tables[0].Rows.Count) && (String.IsNullOrEmpty(retVal)))
        {
            //string egyeb = (String.IsNullOrEmpty(String.Format("{0}", kodok.Ds.Tables[0].Rows[idx]["Kod"]))) ? "." : kodok.Ds.Tables[0].Rows[idx]["Kod"].ToString();
            string egyeb = Convert.ToString(kodok.Ds.Tables[0].Rows[idx]["Kod"]);
            if (egyeb.Trim().ToLower().Equals(kiterjesztes.Trim().ToLower()))
            {
                //retVal = String.Format("{0}", kodok.Ds.Tables[0].Rows[idx]["Kod"]);
                retVal = String.Format("{0}", kodok.Ds.Tables[0].Rows[idx]["Nev"]);
            }
            idx++;
        }
        Logger.Info(String.Format("Megallapitott tipus: {0}", retVal));

        return String.IsNullOrEmpty(retVal) ? KodTarak.DOKUMENTUMTIPUS.Egyeb : retVal; //"Egyeb" : retVal;
    }

    public static bool IsParamExists(String pname, XmlDocument xdoc)
    {
        if (xdoc == null) return false;
        return (xdoc.SelectNodes(String.Format("//{0}", pname)).Count == 1) ? true : false;
    }

    public static void SetParam(string pname, XmlDocument xdoc, string pvalue)
    {
        if (xdoc == null) return;

        string xpath = String.Format("//{0}", pname);

        if (xdoc.SelectNodes(xpath).Count == 1)
        {
            try
            {
                XmlNode xn = xdoc.SelectSingleNode(xpath);
                if (xn != null)
                {
                    xn.InnerText = pvalue;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("DocumentService.SetParam hiba!");
                Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error(String.Format("panem: {0}\nxmlDoc: {1}", pname, xdoc.OuterXml.ToString()));
            }
        }
    }

    /// <summary>
    /// Convert Illegal Characters In Filename
    /// </summary>
    /// <param name="forras"></param>
    /// <returns></returns>
    public static string ConvertIllegalCharactersInFilename(string forras)
    {
        char[] mit = {     'õ',
                           'Õ',
                           'Â','À','Ã','Ä','Å',
                           'Ê','Ë',
                           'Ì','Î','Ï',
                           'Ô','Õ',
                           'â','ã','ä','å',
                           'ê','ë',
                           'î','ï',
                           'ô','õ',
                           'û'
                     };
        char[] mire = {    'o',
                           'O',
                           'A','A','A','A','A',
                           'E','E',
                           'I','I','I',
                           'O','O',
                           'a','a','a','a',
                           'e','e',
                           'i','i',
                           'o','o',
                           'u'
                      };

        string eredmeny = "";

        foreach (char item in forras.ToCharArray())
        {

            int idx = -1;
            int i = 0;
            while (idx == -1 && i < mit.Length)
            {
                if (mit[i].Equals(item))
                    idx = i;

                i++;
            }

            char b = (idx == -1) ? item : mire[idx];

            eredmeny += Convert.ToString(b);
        }
        return eredmeny;
    }
}
