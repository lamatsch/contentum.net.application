﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using NMHH.UCM.Servlet;
using System.IO;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using System.Net;
using System.Data;
using Contentum.eQuery.BusinessDocuments;

partial class GetDocumentContent : System.Web.UI.Page
{
    string externalLink = string.Empty;
    string filename = string.Empty;
    string guid = string.Empty;
    ContentDownloader contentDownloader = new ContentDownloader();
    protected void Page_PreRender(object sender, EventArgs e)
    {

        string currentVersion = String.Empty;
        string version = Request.QueryString.Get("version");
        string mode = Request.QueryString.Get("mode") ?? String.Empty;
        string filesource = string.Empty;
       
        try
        {

            #region GUID alapján externalLink lekérése

            KRT_DokumentumokService dokService = new KRT_DokumentumokService();
            ExecParam ep = UI.SetExecParamDefault(Page);

            if (!string.IsNullOrEmpty(Request.QueryString.Get("id")))
            {
                ep.Record_Id = Request.QueryString.Get("id");
                //char jogszint = 'O';

                Result dokGetResult = dokService.Get(ep);
                if (dokGetResult.IsError)
                {
                    throw new ResultException(dokGetResult);
                }

                externalLink = (dokGetResult.Record as KRT_Dokumentumok).External_Link;
                filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
                guid = ep.Record_Id;
                currentVersion = (dokGetResult.Record as KRT_Dokumentumok).VerzioJel;
                filesource = (dokGetResult.Record as KRT_Dokumentumok).External_Source;
            }
            

            #endregion GUID alapján externalLink lekérése

            // a word nem tudja átirányítással (akkor nem lesznek bent a tulajdonságok), a sharepointnak meg kell hogy legyen /akármi.docx
            //if (string.IsNullOrEmpty(Request.QueryString.Get("redirected")))
            //{
            //        //Response.Redirect(Request.Url.AbsolutePath + "/" + EncodeFileName(filename) + "?id=" + guid +
            //        //(String.IsNullOrEmpty(version) ? String.Empty : ("&version=" + version)) +
            //        //"&redirected=true", false);
            //}
            //else
            //{

                //verziók kezelése
                //ha van megadva verzió és nem az aktuális, akkor le kell kérni a verzió url-jét
                if (!String.IsNullOrEmpty(version) && version != currentVersion)
                {
                    if (!filesource.Contains("UCM"))
                    {
                        ExecParam xpmVersion = UI.SetExecParamDefault(Page);
                        xpmVersion.Record_Id = guid;
                        Contentum.eDocument.Service.DocumentService serviceVersion = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
                        Result resVersion = serviceVersion.GetDocumentVersionsFromMOSS(xpmVersion);

                        if (resVersion.IsError)
                        {
                            Logger.Error(String.Format("Verzio ({0}) lekerese hiba!", version), xpmVersion, resVersion);
                            throw new ResultException(resVersion);
                        }

                        bool isVersion = false;
                        foreach (DataRow rowVersion in resVersion.Ds.Tables[0].Rows)
                        {
                            if (version == rowVersion["version"].ToString())
                            {
                                externalLink = rowVersion["url"].ToString();
                                isVersion = true;
                                break;
                            }
                        }

                        if (!isVersion)
                        {
                            Logger.Error(String.Format("A verzio ({0}) nem talalhato!", version));
                            throw new ResultException(String.Format("A kért verzió ({0}) nem található!", version));
                        }
                    }
                }

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

            if (filesource.Contains("SharePoint"))
                GetFileBySharePoint(externalLink, filename, mode);
            else if (filesource.Contains("FileShare"))
                GetFileByFileShare(externalLink, filename, mode);
            else if (filesource.Contains("UCM"))
                GetFileByUCM(externalLink, filename, mode);
            else if (filesource.Contains("FileSystem"))
                GetFileBySharePoint(externalLink, filename, mode);
            else if (filesource.Contains(HAIRCsatolmanyFeltoltesParameters.Constants.HAIR_FILE_SOURCE))
                contentDownloader.GetFileByHAIR(Page, externalLink, filename, string.Empty);
            else
                throw new NotImplementedException(string.Format("Hibás external_source mező:{0}", filesource));

           // }
        }
        catch (ResultException resEx)
        {
            try
            {
                Response.ClearHeaders();
                Response.Write(ResultError.GetErrorMessageFromResultObject(resEx.GetResult()));
            }
            catch { }
        }
        catch (Exception exp)
        {
            try
            {
                Response.ClearHeaders();
                Response.Write("A kért dokumentum nem érhető el! Kérem, próbálja meg később."); // A kért dokumentum nem érhető el! Kérem, próbálja meg később.
                Logger.Warn("GetDocumentumContent hiba: " + exp.Message);
            }
            catch { }
        }
        finally
        {
            Response.End();
        }
    }

    /// <summary>
    /// Filetartalom letőltése és továbbítása a kliensnek SharePoint esetén
    /// </summary>
    /// <param name="externalLink">a file ürlje</param>
    /// <param name="filename">a fájl neve és kiterjesztése kliens oldalon</param>
    private void GetFileBySharePoint(string externalLink, string filename, string mode)
    {
        Logger.Debug("GetFileBySharePoint kezdete");
        Logger.Debug(String.Format("externalLink={0}, filename={1}", externalLink, filename));

        #region Header bejegyzések beállítása

        WebRequest request = WebRequest.Create(externalLink);
        request.Timeout = (int)TimeSpan.FromMinutes(20).TotalMilliseconds;

        request.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
        WebResponse response = request.GetResponse();

        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        string responseFileName = filename;
        if (mode == "decrypt")
        {
            if (filename.EndsWith(".enc"))
            {
                responseFileName = filename.Substring(0, filename.LastIndexOf('.'));
            }
        }
        // BUG_12301
        // int csatolmanyLetoltesConfig = Rendszerparameterek.GetInt(Page, Rendszerparameterek.CSATOLMANY_LETOLTES_MOD);

        ExecParam execParam = new ExecParam();
        if (!string.IsNullOrEmpty(Request.QueryString.Get("fid")))
        {
            execParam.Felhasznalo_Id = Request.QueryString.Get("fid");
        }
        if (String.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            execParam = UI.SetExecParamDefault(Page, new ExecParam());
        }
        int csatolmanyLetoltesConfig = Rendszerparameterek.GetInt(execParam, Rendszerparameterek.CSATOLMANY_LETOLTES_MOD);

        string letoltesMod = csatolmanyLetoltesConfig == 1 ? "attachment" : "inline";

        // Content-Type és fájl nevének beállítása
        Response.ContentType = response.ContentType;
        Response.CacheControl = "public";
        Response.HeaderEncoding = System.Text.Encoding.UTF8;
        Response.Charset = "utf-8";
        Response.AddHeader("Content-Disposition", string.Format("{0};filename=\"", letoltesMod) + responseFileName + "\";");

        #endregion Header bejegyzések beállítása

        #region Fájltartalom letöltése és továbbítása a kliensnek

        Stream s = response.GetResponseStream();

        //if (mode == "decrypt")
        //{
        //    Decrypt(s);
        //}
        //else
        //{

            byte[] buf = new byte[20480];

            while (true)
            {
                int readBytes = s.Read(buf, 0, buf.Length);

                if (readBytes == 0)
                    break;

                Response.OutputStream.Write(buf, 0, readBytes);
            }
   //     }

        s.Close();
        #endregion Fájltartalom letöltése és továbbítása a kliensnek

        Logger.Debug("GetFileBySharePoint vege");

    }

    private System.IO.Stream GetFileBySharePointForZip(string externalLink, string filename)
    {
        WebRequest request = WebRequest.Create(externalLink);

        request.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
        WebResponse response = request.GetResponse();

        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        return response.GetResponseStream();
    }

    /// <summary>
    /// Filetartalom letőltése és továbbítása a kliensnek FileShare esetén
    /// </summary>
    /// <param name="externalLink">a file ürlje</param>
    /// <param name="filename">a fájl neve és kiterjesztése kliens oldalon</param>
    private void GetFileByFileShare(string externalLink, string filename, string mode)
    {
        #region Header bejegyzések beállítása        

        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        string responseFileName = filename;
        if (mode == "decrypt")
        {
            if (filename.EndsWith(".enc"))
            {
                responseFileName = filename.Substring(0, filename.LastIndexOf('.'));
            }
        }

        // Content-Type és fájl nevének beállítása
        Response.ContentType = "application/octet-stream";
        Response.CacheControl = "public";
        Response.HeaderEncoding = System.Text.Encoding.UTF8;
        Response.Charset = "utf-8";
        Response.AddHeader("Content-Disposition", "attachment;filename=\"" + responseFileName + "\";");

        #endregion Header bejegyzések beállítása
        using (var file = File.OpenRead(externalLink))
        {
            //if (mode == "decrypt")
            //{
            //    Decrypt(file);
            //}
            //else
            //{
                byte[] buf = new byte[20480];

                while (true)
                {
                    int readBytes = file.Read(buf, 0, buf.Length);

                    if (readBytes == 0)
                        break;

                    Response.OutputStream.Write(buf, 0, readBytes);
                }
           // }
        }
    }

    private byte[] ReadAll(Stream stream)
    {
        byte[] buf = new byte[20480];

        MemoryStream ms = new MemoryStream();

        while (true)
        {
            int readBytes = stream.Read(buf, 0, buf.Length);

            if (readBytes == 0)
                break;

            ms.Write(buf, 0, readBytes);
        }

        ms.Position = 0;

        byte[] newBuf = new byte[ms.Length];

        ms.Read(newBuf, 0, (int)ms.Length);
        return newBuf;
    }

    private System.IO.Stream GetFileByFileShareForZip(string externalLink, string filename)
    {
        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        return File.OpenRead(externalLink);
    }


    /// <summary>
    /// Filetartalom letőltése és továbbítása a kliensnek SharePoint esetén
    /// </summary>
    /// <param name="externalLink">a file ürlje</param>
    /// <param name="filename">a fájl neve és kiterjesztése kliens oldalon</param>
    private void GetFileByUCM(string externalLink, string filename, string mode)
    {
        if (!String.IsNullOrEmpty(externalLink))
        {
            GetFileBySharePoint(externalLink, filename, mode);
        }
    }

    private System.IO.Stream GetFileByUCMForZip(string externalLink, string filename)
    {
        if (!String.IsNullOrEmpty(externalLink))
        {
            return GetFileBySharePointForZip(externalLink, filename);
        }

        return null;
    }

    
    private string EncodeFileName(string fileName)
    {
        if (String.IsNullOrEmpty(fileName))
            return String.Empty;

        fileName = HttpUtility.UrlEncode(fileName);
        fileName = fileName.Replace("+", "%20");

        return fileName;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string path = Request.QueryString.Get("path");
        // BUG_6073
        string dokId = Request.QueryString.Get("id");

        if (String.IsNullOrEmpty(dokId))
        {
            // UCM (path-nak kell lennie)
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();

            UCMServletManager manager = new UCMServletManager();
            ServletResponse response = manager.GetDocumentContent(path);
            Response.OutputStream.Write(response.ResponseData, 0, response.ResponseData.Length);

            foreach (string header in response.ResponseHeaders)
            {
                string headerValue = response.ResponseHeaders[header];
                if (header == "Content-Disposition")
                {
                    headerValue = EncodeContentDisposition(headerValue);
                }
                Response.AddHeader(header, headerValue);
            }

            Response.HeaderEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "utf-8";


            Response.OutputStream.Flush();
            Response.OutputStream.Close();

            HttpContext.Current.ApplicationInstance.CompleteRequest();

            Response.Flush();
            Response.Close();
            Response.End();
        }
    }

    private string EncodeContentDisposition(string headerValue)
    {
        Match match = Regex.Match(headerValue, "filename=\"([^\"]*)\"");

        if (match.Success)
        {
            string fileName = match.Groups[1].Value;
            fileName = GetFileNameFromUCM(fileName);
            fileName = EncodeFileName(fileName);

            headerValue = Regex.Replace(headerValue, "filename=\"([^\"]*)\"", "fileName=\"" + fileName + "\"");
        }

        return headerValue;
    }

    string GetFileNameFromUCM(string fileName)
    {
        if (!String.IsNullOrEmpty(fileName))
        {
            return Regex.Replace(fileName, "^\\d{5}_", "");
        }

        return fileName;
    }
}