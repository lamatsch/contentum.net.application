﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UCMTest.aspx.cs" Inherits="UCMTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p>Iktatószám:</p><asp:TextBox ID="ugyszam" runat="server" Text="B-4-2018" />
    </div>
    <div>
        <p>Alszám:</p><asp:TextBox ID="alszam" runat="server" Text="1" />
    </div>
    <div>
       <p>MaxItems:</p> <asp:TextBox ID="maxItems" runat="server" Text="10" />
    </div>
    <div>
        <p>StartIndex:</p><asp:TextBox ID="startIndex" runat="server" Text="0" />
    </div>
    <div>
        <asp:FileUpload ID="file" runat="server" />
    </div>
    <div>
        <asp:Button ID="btnTest" runat="server" Text="Lekérdezés" OnClick="btnTest_Click"/>
        <asp:Button ID="btnUpload" runat="server" Text="Feltöltés" OnClick="btnUpload_Click"/>
    </div>
    <div>
        <asp:Label ID="result" runat="server" />
    </div>
    <div>
        <asp:GridView ID="grid" runat="server" OnRowDeleting="grid_RowDeleting" OnRowCommand="grid_RowCommand">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDelete" runat="server" Text="Törlés" CommandName="Delete" CommandArgument='<%#Eval("Path") as string %>'
                            OnClientClick="if(!confirm('Biztosan törli a dokumentumot?')) return false;"/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Link">
                    <ItemTemplate>
                        <asp:HyperLink ID="linkPath" runat="server" Text='<%#Eval("Path")  %>' NavigateUrl='<%#GetPathLink(Eval("Path") as string) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div>
        <asp:GridView ID="gridDev" runat="server" />
     </div>
        <div>
            <asp:Label ID="commandOutput" runat="server" />
        </div>
    </form>
</body>
</html>
