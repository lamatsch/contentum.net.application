﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text.RegularExpressions;
using System.Configuration;
using NMHH.UCM.Servlet;
using Contentum.eBusinessDocuments;

public partial class UCMTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnTest_Click(object sender, EventArgs e)
    {
        result.Text = String.Empty;

        try
        {
            UCMServletManager manager = new UCMServletManager();
            var res = manager.GetDocuments(ugyszam.Text, alszam.Text, null, null, null, null, maxItems.Text, startIndex.Text);

            if (res.IsError)
            {
                result.Text = String.Format("ErrorCode: {0}; ", res.ErrorCode);
                result.Text += String.Format("ErrorMessage: {0}", res.ErrorMessage);
            }
            else
            {

                grid.DataSource = res.Dokumentumok;
                grid.DataBind();
            }
        }
        catch (Exception ex)
        {
            result.Text = ex.ToString();
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        result.Text = String.Empty;

        try
        {
            UCMServletManager manager = new UCMServletManager();
            var res = manager.Upload(file.FileBytes, file.FileName, ugyszam.Text, null, alszam.Text, null, null, null, null, false);

            if (res.IsError)
            {
                result.Text = String.Format("ErrorCode: {0}; ", res.ErrorCode);
                result.Text += String.Format("ErrorMessage: {0}", res.ErrorMessage);
            }
            else
            {

                result.Text = res.Path;
            }
        }
        catch (Exception ex)
        {
            result.Text = ex.ToString();
        }
    }


    public string GetPathLink(string path)
    {
        string servletUrl = "GetDocumentContent.aspx";
        string qs = String.Format("path={0}", HttpUtility.UrlEncode(path));
        return servletUrl + '?' + qs;
    }

    protected void btnDevTest_Click(object sender, EventArgs e)
    {
        result.Text = String.Empty;

        try
        {

            //UCMDocumentService docService = new UCMDocumentService();
            //var res = docService.SetIratJogosultak(new ExecParam(), "B4D730EF-D9F6-E711-80C9-00155D020DD3");

            //result.Text = res.ErrorMessage;

        }
        catch (Exception ex)
        {
            result.Text = ex.ToString();
        }
    }

    protected void grid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
    }

    protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            string path = e.CommandArgument.ToString();
            Delete(path);
        }
    }

    private void Delete(string path)
    {
        UCMServletManager ucmManager = new UCMServletManager();

        try
        {

            var result = ucmManager.DeleteDocument(path);
            commandOutput.Text = result.ResponseString;

        }
        catch (Exception ex)
        {
            commandOutput.Text = ex.ToString();
        }
    }
}