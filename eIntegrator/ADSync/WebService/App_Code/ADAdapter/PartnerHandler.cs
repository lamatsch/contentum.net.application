﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using log4net;
using System;
using System.Data;
using System.Reflection;

namespace Contentum.eIntegrator.ADAdapter
{
    /// <summary>
    /// Summary description for PartnerHandler
    /// </summary>
    internal static class PartnerHandler
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Logger));
        private static KRT_PartnerekService service_partnerek = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        private static KRT_PartnerKapcsolatokService service_partnerKapcsolatok = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
        //  private static INT_ExternalIDsService service_ExtIds = INT_ExternalIDsService.ServiceFactory.GetINT_ExternalIDsService();

        //public PartnerHandler()
        //{
        //    //
        //    // TODO: Add constructor logic here
        //    //
        //}

        /// <summary>
        /// Új szervezeti egység rögzítése.
        /// </summary>
        /// <param name="wrappedGroup">A szervezeti egység adatai.</param>
        /// <returns>A kapott egyedi azonosító.</returns>
        /// <exception >
        /// Belső hiba.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        /// A ParentGroupIDInAdapterSystem property nincs beállítva.
        /// </exception>
        internal static string InsertPartner(WrappedADGroup wrappedGroup)
        {

            //if (String.IsNullOrEmpty(wrappedGroup.OuterSystem.ParentGroupID))
            //    throw new ArgumentNullException("OuterSystem.ParentGroupID");

            if (IsNameInUse(wrappedGroup.Name))
            {
                log.Error(String.Format("{0} Ilyen nevű partner már létezik a szervezeti struktúrában! Partner név: {1}, (AD azonosító:{2})",
                    MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.ADProp.ObjectID));
                return null;
            }



            KRT_Partnerek edokPartner = new KRT_Partnerek();

            MapToKrtPartnerek(ref edokPartner, wrappedGroup, ADAdapterTypes.ConvertToPartnerTipus(wrappedGroup.ADProp.GroupType), INT_ADSyncService.Org_id, false);
            //partner.Org = org_Id;
            //partner.Nev = wrappedGroup.Name;
            //partner.Tipus = ADAdapterTypes.ConvertToPartnerTipus(wrappedGroup.ADProp.GroupType).ToString();
            //partner.Forras = ADAdapterTypes.AD_FORRAS;
            //partner.Belso = "1";
            //  partner.Id = wrappedGroup.ADProp.ObjectID;

            //   KRT_PartnerekService service_partnerek = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            Result result = service_partnerek.Insert(ADAdapterTypes.GetExecParam(), edokPartner);
            if (result.ErrorCode == null)
            {

                InsertNewPartnerKapcsolatok(result.Uid, wrappedGroup.EDOKProp.ParentGroupID,
                    ADAdapterTypes.PartnerkapcsolatTipus.Szervezet_FelettesSzervezete);
                log.DebugFormat("[0] Partner rögzítés: {1} ({2})", MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, result.Uid);
                return result.Uid;
            }
            else
            {
                log.Error(String.Format("[{0}] Hiba a Partner rögzítésnél Partner: {0} (AD azonosító: {1}), Error: {2}, {3} ",
                    MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.ADProp.ObjectID, result.ErrorCode, result.ErrorMessage));

                return null;
            }
        }

        /// <summary>
        /// Szervezeti egység adatainak aktualizálása.
        /// </summary>
        /// <param name="wrappedGroup">Az aktuális adatok.</param>
        /// A szülő szervezeti egység Edok-azonosítója, 
        /// vagy null, ha ez egy gyökér szinű szervezeti egység.
        /// </param>
        /// <returns>Igaz, ha az update sikeres volt.</returns>
        internal static bool UpdatePartner(WrappedADGroup wrappedGroup)
        {

            //if (String.IsNullOrEmpty(wrappedGroup.OuterSystem.ParentGroupID))
            //    throw new ArgumentNullException("OuterSystem.ParentGroupID");

            if (String.IsNullOrEmpty(wrappedGroup.EDOKProp.ID))
            {
                log.ErrorFormat("[{0}] Nincs megadva Partner Id! Név: {1}", MethodBase.GetCurrentMethod().Name, wrappedGroup.Name);
                return false;
            }
            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = wrappedGroup.EDOKProp.ID;

            Result result = service_partnerek.Get(execParam);
            KRT_Partnerek edokPartner = (KRT_Partnerek)result.Record;
            if (edokPartner == null)
            {
                log.ErrorFormat("[{0}] Partner nem található! Partner: {1} ({2})", MethodBase.GetCurrentMethod().Name, wrappedGroup.EDOKProp.ID, wrappedGroup.Name);
                return false;
            }

            string version = edokPartner.Base.Ver;

            ADAdapterTypes.PartnerTipus edokPartnerTipus = ADAdapterTypes.ConvertToPartnerTipus(wrappedGroup.ADProp.GroupType);
            MapToKrtPartnerek(ref edokPartner, wrappedGroup, edokPartnerTipus, INT_ADSyncService.Org_id, true); // updateMode

            edokPartner.Base.Ver = version;
            edokPartner.Base.Updated.Ver = true;

            result = service_partnerek.Update(execParam, edokPartner);

            if (result.ErrorCode == null)
            {
                SetPartnerKapcsolatokOfGroup(wrappedGroup.EDOKProp.ParentGroupID, edokPartner);
                log.DebugFormat("[{0}] Partner módosítva! Partner: {1} ({2})", MethodBase.GetCurrentMethod().Name, edokPartner.Id, edokPartner.Nev);
                return true;
            }
            else
            {
                log.ErrorFormat("[{0}] Módosítás hiba! Partner: {1} ({2}), Error: {3}, {4}", MethodBase.GetCurrentMethod().Name, edokPartner.Nev, edokPartner.Id, result.ErrorCode, result.ErrorMessage);
                //  Adapter.ThrowCustomException(result, Util.GetStringForErrorLog(wrappedGroup));
                return false;
            }
        }


        /// <summary>
        /// Szervezeti egység adatainak invalidálása.
        /// </summary>
        /// <param name="wrappedGroup">Az aktuális adatok.</param>
        /// <param name="parentGroupEdokID">
        /// A szülő szervezeti egység Edok-azonosítója, 
        /// vagy null, ha ez egy gyökér szinű szervezeti egység.
        /// </param>
        /// <returns>Igaz, ha az update sikeres volt.</returns>
        internal static bool InvalidatePartner(String edokID)
        {

            //if (String.IsNullOrEmpty(wrappedGroup.OuterSystem.ParentGroupID))
            //    throw new ArgumentNullException("OuterSystem.ParentGroupID");

            if (String.IsNullOrEmpty(edokID))
            {
                log.ErrorFormat("[{0}] Nincs megadva Partner Id!", MethodBase.GetCurrentMethod().Name);
                return false;
            }
            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = edokID;

            Result result = service_partnerek.Get(execParam);
            KRT_Partnerek edokPartner = (KRT_Partnerek)result.Record;
            if (edokPartner == null)
            {
                log.ErrorFormat("[{0}] Partner nem található! PartnerId: {1}", MethodBase.GetCurrentMethod().Name, edokID);
                return false;
            }

            result = service_partnerek.Invalidate(execParam);

            if (result.ErrorCode == null)
            {
                // Partnerkapcsolatokat nem kell invalidálni? TODO
                // SetPartnerKapcsolatokOfGroup(wrappedGroup.EDOKProp.ParentGroupID, edokPartner);
                return true;
            }
            else
            {
                log.ErrorFormat("[{0}] Invalidálási hiba! Partner: {1} , Error: {2}, {3}", MethodBase.GetCurrentMethod().Name, edokID, result.ErrorCode, result.ErrorMessage);
                //  Adapter.ThrowCustomException(result, Util.GetStringForErrorLog(wrappedGroup));
                return false;
            }
        }


        internal static bool IsNameInUse(string partnerName)
        {

            ExecParam execParam = ADAdapterTypes.GetExecParam();

            KRT_PartnerekSearch criteria = new KRT_PartnerekSearch();
            criteria.Nev.Value = partnerName;
            criteria.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = service_partnerek.GetAll(execParam, criteria);
            if (result.ErrorCode == null)
            {
                System.Data.DataSet partnersDS = result.Ds;
                if (result.Ds == null
                    || result.Ds.Tables.Count == 0
                    || result.Ds.Tables[0].Rows.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a partner ellenőrzésnél. Partnernév: {0}", partnerName);
                return false;

            }
        }


        internal static void InsertNewPartnerKapcsolatok(string partnerEdokID,
            string partnerKapcsoltID, ADAdapterTypes.PartnerkapcsolatTipus partnerkapcsolatTipus)
        {
            if (partnerEdokID == null || partnerKapcsoltID == null)
                return;

            //KRT_PartnerKapcsolatokService service_partnerKapcsolatok = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
            // új partnerkapcsolat létrehozása
            KRT_PartnerKapcsolatok newPartnerKapcsolatok = new KRT_PartnerKapcsolatok();
            newPartnerKapcsolatok.Partner_id = partnerEdokID;
            newPartnerKapcsolatok.Partner_id_kapcsolt = partnerKapcsoltID;
            newPartnerKapcsolatok.Tipus = ADAdapterTypes.PartnerkapcsolatKod[partnerkapcsolatTipus];

            Result result = service_partnerKapcsolatok.Insert(ADAdapterTypes.GetExecParam(), newPartnerKapcsolatok);

            if (result.ErrorCode == null)
            {
                log.Debug(String.Format("[{0}] Új Partnerkapcsolat rögzítése. Partner ID: {1} Partner kapcsolt id: {2} Partnerkapcsolat típus: {3}",
                    MethodBase.GetCurrentMethod().Name, partnerEdokID, partnerKapcsoltID, Enum.GetName(typeof(ADAdapterTypes.PartnerkapcsolatTipus), partnerkapcsolatTipus)));
            }
            else if (result.ErrorMessage == "[50304]")
            {
                log.Error(String.Format("[{0}] A szervezetnek már van vezetője! Partner ID: {1} Partner kapcsolt id: {2} Partnerkapcsolat típus: {3}",
                    MethodBase.GetCurrentMethod().Name, partnerEdokID, partnerKapcsoltID, Enum.GetName(typeof(ADAdapterTypes.PartnerkapcsolatTipus), partnerkapcsolatTipus)));
            }
            else
            {
                log.Error(String.Format("[{0}] Hiba a Partnerkapcsolat rögzítésénél! Partner ID: {1} Partner kapcsolt id: {2} partnerkapcsolat típus: {3}, Error: {4},{5}",
                    MethodBase.GetCurrentMethod().Name, partnerEdokID, partnerKapcsoltID, Enum.GetName(typeof(ADAdapterTypes.PartnerkapcsolatTipus), partnerkapcsolatTipus), result.ErrorCode, result.ErrorMessage));
            }
        }

        /// <summary>
        /// Szervezeti egység lekérdezése.
        /// </summary>
        /// <param name="edokGroupId">EDOK-azonosító.</param>
        /// <returns>A megtalált és becsomagolt objektum.</returns>
        /// <exception >
        /// Belső hiba.
        /// </exception>
        internal static WrappedADGroup GetPartner(string edokGroupId)
        {

            WrappedADGroup wrappedGroup = new WrappedADGroup();
            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = edokGroupId;

            Result result = service_partnerek.Get(execParam);
            //TODO: kijavtani valós hibakódra!
            if (String.IsNullOrEmpty(result.ErrorCode))   // == null || result.ErrorCode == "-2146232060")   // "Rekord lekérése az adatbázisból sikertelen", vagyis: az ID alapján kért rekord nem létezik
            {
                KRT_Partnerek edokGroup = (KRT_Partnerek)result.Record;
                if (edokGroup == null)
                {
                    //TraceCallReturnEvent.Raise(false);
                    //throw new Fph.AdajkSync.BusinessFacade.Utility.Exceptions
                    //    .InvalidRecordReferenceException("EdokAdapter", edokGroupId);
                }
                else
                    MapToWrappedGroup(ref wrappedGroup, edokGroup);

            }
            else
            {
                log.ErrorFormat("[{0}] Partnert nem sikerült lekérdezni! Partner ID: {0}, Error: {1}, {2}",
                    MethodBase.GetCurrentMethod().Name, edokGroupId, result.ErrorCode, result.ErrorMessage);

                //                Adapter.ThrowCustomException(result, String.Format("EDOK KRT_Partnerek ID: {0}", edokGroupId));
            }

            return wrappedGroup;
        }



        private static void MapToKrtPartnerek(
                ref KRT_Partnerek edokPartner, WrappedADGroup wrappedGroup, ADAdapterTypes.PartnerTipus partnerTipus,
                String org_Id, bool updateMode)
        {

            edokPartner.Org = org_Id;
            edokPartner.Nev = wrappedGroup.Name;
            edokPartner.Tipus = ADAdapterTypes.PartnerTipusKod[partnerTipus];
            edokPartner.Forras = ADAdapterTypes.AD_FORRAS;
            edokPartner.Belso = "1";

            //edokPartner.Org = org_Id;
            //edokPartner.Nev = wrappedGroup.Name;
            //edokPartner.Tipus = partnerTipus.ToString();
            //edokPartner.Forras = ADAdapterTypes.AD_FORRAS;

            //edokPartner.LetrehozoSzervezet = 
            //edokPartner.Orszag_Id =
            //edokPartner.Kiszolgalo =
            //edokPartner.KulsoAzonositok =
            //edokPartner.PublikusKulcs =

            if (updateMode)
            {
                edokPartner.Updated.SetValueAll(true);
                edokPartner.Base.Updated.SetValueAll(true);
            }
        }

        private static void MapToWrappedGroup(ref WrappedADGroup wrappedGroup,
          KRT_Partnerek edokGroup)
        {
            if (wrappedGroup == null)
                wrappedGroup = new WrappedADGroup();

            wrappedGroup.EDOKProp.ID = edokGroup.Id;
            wrappedGroup.Name = edokGroup.Nev;
            wrappedGroup.Deleted = DateTime.Parse(edokGroup.ErvVege) < DateTime.Now;
            wrappedGroup.EDOKProp.ParentGroupID = GetParentOfKrtPartnerek(edokGroup.Id);
            wrappedGroup.ADProp.GroupType = ADAdapterTypes.ConvertToADGroupType(edokGroup.Tipus);

            //wrappedGroup.Adajk.ObjectID = ezt az assoc táblából lehet kiolvasni...
            //wrappedGroup.Manager = külön beállítandó !!!
            //wrappedGroup.GroupMembers

            wrappedGroup.EDOKProp.OriginalBusinessDocument = edokGroup;
        }

        private static string GetParentOfKrtPartnerek(string childGroupID)
        {

            KRT_PartnerKapcsolatokSearch criteria = new KRT_PartnerKapcsolatokSearch();
            criteria.Partner_id.Value = childGroupID;
            criteria.Partner_id.Operator = Contentum.eQuery.Query.Operators.equals;
            criteria.Tipus.Value = ADAdapterTypes.PartnerkapcsolatKod[ADAdapterTypes.PartnerkapcsolatTipus.Szervezet_FelettesSzervezete];
            criteria.Tipus.Operator = Contentum.eQuery.Query.Operators.equals;

            //KRT_PartnerKapcsolatokService service_PartnerKapcs = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
            Result result = service_partnerKapcsolatok.GetAll(ADAdapterTypes.GetExecParam(), criteria);
            if (result.ErrorCode == null)
            {

                if (result.Ds.Tables[0].Rows.Count == 0)
                {
                    log.DebugFormat("[{0}] Felettes szervezet nem található! Partner Id: {1} ", MethodBase.GetCurrentMethod().Name, childGroupID);
                    return null;
                }
                else if (result.Ds.Tables[0].Rows.Count > 1)
                {
                    log.ErrorFormat("[{0}] Több ({1} db) felettes szervezet! Partner id: {2})",
                            MethodBase.GetCurrentMethod().Name, result.Ds.Tables[0].Rows.Count, childGroupID);
                    return null;
                }
                else
                {
                    log.DebugFormat("[{0}] Talált felettes szervezet: PartnerId: {1}, Felettes szervezet: {2}",
                        MethodBase.GetCurrentMethod().Name, childGroupID, result.Ds.Tables[0].Rows[0]["Partner_id"].ToString());
                    return result.Ds.Tables[0].Rows[0]["Partner_id_kapcsolt"].ToString();
                }
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a felettes szervezet lekérdezésnél!, Partner Id: {1}, Error: {2}, {3}",
                    MethodBase.GetCurrentMethod().Name, childGroupID, result.ErrorCode, result.ErrorMessage);
                return null;
            }
        }


        private static void SetPartnerKapcsolatokOfGroup(string parentGroupEdokID, KRT_Partnerek edokPartner)
        {

            KRT_PartnerKapcsolatokSearch criteria = new KRT_PartnerKapcsolatokSearch();
            // pl. partner_id = Informatikai Üo., partner_id_kapcsolt = a négy alosztály
            criteria.Partner_id.Value = edokPartner.Id;  //ha valakinek ő a felettese
            criteria.Partner_id.Operator = Contentum.eQuery.Query.Operators.equals;
            criteria.Tipus.Value = ADAdapterTypes.PartnerkapcsolatKod[ADAdapterTypes.PartnerkapcsolatTipus.Szervezet_FelettesSzervezete];
            criteria.Tipus.Operator = Contentum.eQuery.Query.Operators.equals;


            Result result = service_partnerKapcsolatok.GetAll(ADAdapterTypes.GetExecParam(), criteria);
            if (result.ErrorCode == null)
            {
                //System.Data.DataSet parentsDs = result.Ds;
                if (result.Ds.Tables[0].Rows.Count == 0)
                {
                    if (parentGroupEdokID != null)
                    {
                        InsertNewPartnerKapcsolatok(edokPartner.Id, parentGroupEdokID,
                            ADAdapterTypes.PartnerkapcsolatTipus.Szervezet_FelettesSzervezete);
                        log.DebugFormat("[{0}] Felettes szervezet rögzítése: {1} feletes szervezete: {2}",
                            MethodBase.GetCurrentMethod().Name, edokPartner.Id, parentGroupEdokID);
                    }
                }
                else
                {
                    bool isRequiredParentAlreadySet = false;
                    foreach (System.Data.DataRow partnerKapcsolatokRow in result.Ds.Tables[0].Rows)
                    {
                        if (partnerKapcsolatokRow["Partner_id_kapcsolt"].ToString().ToUpperInvariant() == parentGroupEdokID.ToUpperInvariant())
                            isRequiredParentAlreadySet = true;
                        else
                        {
                            RemovePartnerKapcsolatok(partnerKapcsolatokRow["Id"].ToString());
                            log.DebugFormat("[{0}] Felettes szervezet törlése: {1} feletes szervezete: {2}",
                            MethodBase.GetCurrentMethod().Name, edokPartner.Id, parentGroupEdokID);
                        }
                    }
                    if (!isRequiredParentAlreadySet)
                    {
                        InsertNewPartnerKapcsolatok(edokPartner.Id, parentGroupEdokID, ADAdapterTypes.PartnerkapcsolatTipus.Szervezet_FelettesSzervezete);
                        log.DebugFormat("[{0}] Felettes szervezet rögzítése: {1} feletes szervezete: {2}",
                            MethodBase.GetCurrentMethod().Name, edokPartner.Id, parentGroupEdokID);
                    }
                }
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a Felettes szervezet keresésnél! Partner: {1} ({2}), Error: {3}, {4}",
                    MethodBase.GetCurrentMethod().Name, edokPartner.Nev, edokPartner.Id, result.ErrorCode, result.ErrorMessage);

                //Adapter.ThrowCustomException(result, String.Format("EDOK parent group ID: {0} user: {1}",
                //    parentGroupEdokID, edokPartner.Nev));
            }

        }



        private static void RemovePartnerKapcsolatok(string partnerKapcsolatokID)
        {

            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = partnerKapcsolatokID;

            Result result = service_partnerKapcsolatok.Invalidate(execParam);

            if (result.ErrorCode != null)
            {
                log.ErrorFormat("[{0}] Partnerkapcsolat érvénytelenítés hiba, PartnerkapcsolatId: {1}", MethodBase.GetCurrentMethod().Name, partnerKapcsolatokID);
            }
        }



        internal static DataSet GetGroupMembershipOfUser(string edokUserId, string partner_id)
        {
            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Felhasznalo_Id = edokUserId;

            DataSet partnerek = null;

            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = partner_id;

            KRT_PartnerKapcsolatokSearch criteria = new KRT_PartnerKapcsolatokSearch();
            criteria.Tipus.Value = ADAdapterTypes.PartnerkapcsolatKod[ADAdapterTypes.PartnerkapcsolatTipus.FelhasznaloSzervezete];  //Util.PartnerkapcsolatKod[PartnerkapcsolatTipus.FelhasznaloSzervezete];
            criteria.Tipus.Operator = Contentum.eQuery.Query.Operators.equals;

            // A partnerhez hozzárendelt kapcsolt partnerek lekérdezése
            KRT_PartnerekService service_partnerek = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            Result result = service_partnerek.GetAllByPartner(execParam, partner, criteria);
            if (result.ErrorCode == null)
            {
                partnerek = result.Ds;
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a felhasználó szervezeteinek lekérdezésekor! Felhasználó Id: {1}", MethodBase.GetCurrentMethod().Name, edokUserId);

            }
            return partnerek;
        }

        // <summary>
        /// A felhasználó szervezeti egységeinek lekérdezése EDOK-ból.
        /// </summary>
        /// <param name="edokUserId">Felhasználó EDOK-azonosítója.</param>
        /// <returns>Szervezeti egység lista, amely lehet 0 elemű is.</returns>
        /// <exception cref="Fph.AdajkSync.BusinessFacade.Utility.Exceptions.InvalidRecordReferenceException">
        /// A hivatkozott rekord nem létezik.
        /// </exception>
        internal static WrappedADGroupCollection GetWrappedADGroupMembershipOfEdokUser(KRT_Felhasznalok edokUser)
        {
            WrappedADGroupCollection wrappedGroups = new WrappedADGroupCollection();

            System.Data.DataSet partnerekOfUser = GetGroupMembershipOfUser(edokUser.Id, edokUser.Partner_id);
            if (partnerekOfUser == null)
            {
                log.DebugFormat("[{0}] Nem talált szervezetet a felhasználóhoz! Felhaználó Id: {1}"
                    , MethodBase.GetCurrentMethod().Name, edokUser.Id);
            }
            else
            {
                foreach (System.Data.DataRow partnerRow in partnerekOfUser.Tables[0].Rows)
                {
                    WrappedADGroup wrappedGroup = GetPartner(partnerRow["Partner_id_kapcsolt"].ToString());
                    wrappedGroups.Add(wrappedGroup);
                }
            }

            return wrappedGroups;
        }



        internal static void RemoveAdditionallyStoredGroupmembersOfUser(WrappedADGroupCollection requiredGroupMembers, DataSet partnerekOfUser)
        {
            if (partnerekOfUser.Tables[0].Rows.Count > requiredGroupMembers.Count)
            {
                foreach (System.Data.DataRow partnerRow in partnerekOfUser.Tables[0].Rows)
                {
                    string groupEdokID = partnerRow["Partner_id_kapcsolt"].ToString();

                    bool isThisGroupRequired = false;

                    for (int i = 0; i < requiredGroupMembers.Count; i++)
                    {
                        if (requiredGroupMembers[i].EDOKProp.ID.ToUpperInvariant() == groupEdokID.ToUpperInvariant())
                        {
                            isThisGroupRequired = true;
                            break;
                        }
                    }
                    if (!isThisGroupRequired)
                    {

                        RemovePartnerKapcsolatok(partnerRow["Id"].ToString());
                        log.DebugFormat("[{0}] Felhasználó szervezeteinek törlése! Partnerkapcsolat id: {1}",
                            MethodBase.GetCurrentMethod().Name, partnerRow["Id"].ToString());
                        //string msg = "Az EDOK csoport érvénytelenítendő kapcsolata: {0}";
                        //Util.WriteToTraceFile(String.Format(msg, partnerRow["Id"].ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// A felhasználó által vezetett szervezeti egység lekérdezése.
        /// </summary>
        /// <param name="edokUserId">Felhasználó Edok-azonosítója.</param>
        /// <returns>
        /// Null, ha nem vezet szervezeti egységet, vagy a vezetett szervezeti egység.
        /// </returns>

        public static KRT_Partnerek GetManagedGroupOfEdokUser(KRT_Felhasznalok edokUser)
        {

            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = edokUser.Id;

            KRT_Partnerek parent = new KRT_Partnerek();
            parent.Id = edokUser.Partner_id;

            KRT_PartnerKapcsolatokSearch criteria = new KRT_PartnerKapcsolatokSearch();
            criteria.Tipus.Value = ADAdapterTypes.PartnerkapcsolatKod[ADAdapterTypes.PartnerkapcsolatTipus.SzervezetiEgysegVezetoje]; // Util.PartnerkapcsolatKod[PartnerkapcsolatTipus.SzervezetiEgysegVezetoje];
            criteria.Tipus.Operator = Query.Operators.equals;

            Result result = service_partnerek.GetAllByPartner(execParam, parent, criteria);

            KRT_Partnerek edokGroup = null;
            if (result.ErrorCode == null)
            {
                if (result.Ds.Tables[0].Rows.Count > 1)
                {
                    log.ErrorFormat("[{0}] Több ({1} db) menedzselt szervezet található. Felhasználó: {2} ({3}) "
                            , MethodBase.GetCurrentMethod().Name, result.Ds.Tables[0].Rows.Count, edokUser.Nev, edokUser.Id);
                }
                else if (result.Ds.Tables[0].Rows.Count == 1)
                {
                    System.Data.DataRow partnerRow = result.Ds.Tables[0].Rows[0];
                    execParam = ADAdapterTypes.GetExecParam();
                    execParam.Record_Id = partnerRow["Partner_id_kapcsolt"].ToString();
                    result = service_partnerek.Get(execParam);
                    if (result.ErrorCode == null)
                    {
                        edokGroup = (KRT_Partnerek)result.Record;
                    }
                    else
                    {
                        log.ErrorFormat("[{0}] Hiba a menedzselt szervezet lekérésénél!, UserId: {1}, Kapcsolt PartnerId: {2}, Errorkód: {3}, Error: {4}"
                            , MethodBase.GetCurrentMethod().Name, edokUser.Id, partnerRow["Partner_id_kapcsolt"].ToString(), result.ErrorCode, result.ErrorMessage);
                    }
                }
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a menedzselt szervezet lekérésénél!, UserId: {1}, Errorkód: {2}, Error: {3}"
                    , MethodBase.GetCurrentMethod().Name, edokUser.Id, result.ErrorCode, result.ErrorMessage);

            }

            return edokGroup;
        }



        internal static void SetManagedGroupOfEdokUser(string edokUsersPartnerID, string managedGroupEdokID)
        {

            KRT_PartnerKapcsolatokSearch criteria = new KRT_PartnerKapcsolatokSearch();
            criteria.Partner_id.Value = edokUsersPartnerID;
            criteria.Partner_id.Operator = Contentum.eQuery.Query.Operators.equals;
            criteria.Tipus.Value = ADAdapterTypes.PartnerkapcsolatTipus.SzervezetiEgysegVezetoje.ToString();   // Util.PartnerkapcsolatKod[PartnerkapcsolatTipus.SzervezetiEgysegVezetoje];
            criteria.Tipus.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = service_partnerKapcsolatok.GetAll(ADAdapterTypes.GetExecParam(), criteria);
            if (result.ErrorCode == null)
            {
                System.Data.DataSet kapcsolatokDS = result.Ds;
                if (!
                    (result.Ds == null || result.Ds.Tables.Count == 0
                    || result.Ds.Tables[0].Rows.Count == 0))
                {
                    if (kapcsolatokDS.Tables[0].Rows[0]["Partner_id_kapcsolt"].ToString().ToUpperInvariant() != managedGroupEdokID.ToUpperInvariant())
                    {
                        RemovePartnerKapcsolatok(kapcsolatokDS.Tables[0].Rows[0]["Id"].ToString()); // csak egy csoportot vezethet!
                    }
                }
                SetUserAsGroupManager(managedGroupEdokID, edokUsersPartnerID);
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a felhasználó menedzselt szervezeteinek lekérdezésekor! PartnerId: {1}, ManagedGroupId: {2}, Errorkód: {3}, Error: {4}", MethodBase.GetCurrentMethod().Name, edokUsersPartnerID, managedGroupEdokID, result.ErrorCode, result.ErrorMessage);
            }

        }



        private static void SetUserAsGroupManager(string managedGroupEdokID, string edokUsersPartnerID)
        {
            if (managedGroupEdokID == null || edokUsersPartnerID == null)
                return;


            string partnerKapcsolatokRecordId = GetPartnerKapcsolatokRecordId(
                managedGroupEdokID, ADAdapterTypes.PartnerkapcsolatTipus.SzervezetiEgysegVezetoje);

            if (partnerKapcsolatokRecordId == null)
            {
                InsertNewPartnerKapcsolatok(edokUsersPartnerID, managedGroupEdokID,
                    ADAdapterTypes.PartnerkapcsolatTipus.SzervezetiEgysegVezetoje);
                log.DebugFormat("[{0}] Menedzselt szervezet rögzítés. PartnerId: {1}, ManagedGroupId: {2}", MethodBase.GetCurrentMethod().Name, edokUsersPartnerID, managedGroupEdokID);

            }
            else
            {
                UpdatePartnerKapcsolatok(partnerKapcsolatokRecordId, edokUsersPartnerID, managedGroupEdokID,
                    ADAdapterTypes.PartnerkapcsolatTipus.SzervezetiEgysegVezetoje);
                log.DebugFormat("[{0}] Menedzselt szervezet módosítás. PartnerId: {1}, ManagedGroupId: {2}", MethodBase.GetCurrentMethod().Name, edokUsersPartnerID, managedGroupEdokID);

            }

        }


        private static string GetPartnerKapcsolatokRecordId(string managedGroupEdokID, ADAdapterTypes.PartnerkapcsolatTipus partnerkapcsolatTipus)
        {

            KRT_PartnerKapcsolatokSearch criteria = new KRT_PartnerKapcsolatokSearch();

            criteria.Partner_id_kapcsolt.Value = managedGroupEdokID;
            criteria.Partner_id_kapcsolt.Operator = Contentum.eQuery.Query.Operators.equals;

            criteria.Tipus.Value = ADAdapterTypes.PartnerkapcsolatKod[partnerkapcsolatTipus];  //PartnerkapcsolatKod[partnerkapcsolatTipus];
            criteria.Tipus.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = service_partnerKapcsolatok.GetAll(ADAdapterTypes.GetExecParam(), criteria);
            if (result.ErrorCode == null)
            {
                if (result.Ds.Tables[0].Rows.Count == 0)
                {
                    log.DebugFormat("[{0}] Nem talált partnerkapcsolatot! Menedszelt szervezet id: {1}, Partnerkapcsolat id: {2}", MethodBase.GetCurrentMethod().Name,
                        managedGroupEdokID, partnerkapcsolatTipus);
                    return null;
                }
                else
                {
                    return result.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a felhasználó menedzselt szervezeteinek lekérdezésekor! ManagedGroupId: {2}, Errorkód: {3}, Error: {4}", MethodBase.GetCurrentMethod().Name, managedGroupEdokID, result.ErrorCode, result.ErrorMessage);
                return null;
            }
        }


        private static void UpdatePartnerKapcsolatok(string partnerKapcsolatokRecordId, string partnerEdokID,
           string partnerKapcsoltID, ADAdapterTypes.PartnerkapcsolatTipus partnerkapcsolatTipus)
        {

            if (partnerEdokID == null || partnerKapcsoltID == null || partnerKapcsolatokRecordId == null)
                return;


            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = partnerKapcsolatokRecordId;

            Result result = service_partnerKapcsolatok.Get(execParam);

            if (result.ErrorCode == null)
            {
                KRT_PartnerKapcsolatok partnerKapcsolatok = (KRT_PartnerKapcsolatok)result.Record;

                partnerKapcsolatok.Partner_id = partnerEdokID;
                partnerKapcsolatok.Partner_id_kapcsolt = partnerKapcsoltID;
                partnerKapcsolatok.Tipus = ADAdapterTypes.PartnerkapcsolatKod[partnerkapcsolatTipus]; //Util.PartnerkapcsolatKod[partnerkapcsolatTipus];

                execParam = ADAdapterTypes.GetExecParam();
                execParam.Record_Id = partnerKapcsolatokRecordId;
                result = service_partnerKapcsolatok.Update(execParam, partnerKapcsolatok);

                if (result.ErrorCode != null)
                {

                    log.ErrorFormat("[{0}] Hiba a felhasználó szervezeteinek módosításakor! EDOK partner ID: {1} partner_kapcsolt_id: {2}, Errorkód: {3}, Error: {4}",
                            MethodBase.GetCurrentMethod().Name, partnerEdokID, partnerKapcsoltID, result.ErrorCode, result.ErrorMessage);
                }
            }
            else
            {
                log.ErrorFormat("[{0}] A partnerkapcsolat rekord nem kérdezhető le! EDOK partner ID: {1} partner_kapcsolt_id: {2} partnerkapcsolat típus: {3} partnerKapcsolatokRecordId: {4}, Errorkód: {5}, Error: {6}",
                        MethodBase.GetCurrentMethod().Name, partnerEdokID, partnerKapcsoltID, Enum.GetName(typeof(ADAdapterTypes.PartnerkapcsolatTipus), partnerkapcsolatTipus), partnerKapcsolatokRecordId, result.ErrorCode, result.ErrorMessage);
            }
        }

    }
}
