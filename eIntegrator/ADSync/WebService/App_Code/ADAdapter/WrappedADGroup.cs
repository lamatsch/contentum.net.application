﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Xml;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.ADAdapter
{
    /// <summary>
    /// Az AD-s Group dokumentum helyi megfelelője, csak a 
    /// lényeges dolgok vannak benne.
    /// </summary>
    [Serializable]
    [XmlRootAttribute("ADGroup", IsNullable = false)] //, Namespace=Constants.C_NameSpaceURI
    public sealed class WrappedADGroup : IComparer<WrappedADGroup>
    {

        public sealed class ADProperties
        {
            internal ADProperties()
            {
            }
            /// <summary>ObjectID (AD-s azonosító).</summary>
            public Guid ObjectID;
            /// <summary>
            /// AD GroupTypeRef mező értéke.
            /// </summary>
            public string GroupType;
            /// <summary>
            /// A szülő szervezeti egység ObjectID-ja.
            /// </summary>
            public Guid? ParentGroupID;


        }

        public sealed class EDOKProperties
        {
            internal EDOKProperties()
            {
            }
            /// <summary>
            /// A kapcsolódó rendszer eredeti üzleti dokumentum objektuma,
            /// amelynek csomagolásával ez az objektum létrejött.
            /// </summary>
            public Object OriginalBusinessDocument;
            /// <summary>
            /// A csoport azonosítója a külső rendszerben.
            /// </summary>
            public string ID;
            /// <summary>
            /// A szülő szervezeti egység ObjectID-ja.
            /// </summary>
            public string ParentGroupID;
        }

        #region Fields

        private string _Name = null;
        private bool _Deleted;
        private DateTime? _Created = null;

        private ADProperties _ADProperties = new ADProperties();
        private EDOKProperties _EDOKProperties = new EDOKProperties();


        /// <summary>
        /// Szervezeti egység alszerveinek hierarchikus listája.
        /// </summary>
        /// <remarks>
        /// Automatikusan feltölti a Fph.Adajk.BusinessDocuments.Group
        /// paraméterű konstruktor, egyéb esetben kézzel kell tölteni.
        /// </remarks>
        public WrappedADGroupCollection GroupMembers;

        #endregion

        #region Constructors

        /// <summary>
        /// Public konstruktor. Kell a szerializáláshoz.
        /// </summary>
        public WrappedADGroup() { }

        public WrappedADGroup(GroupPrincipal ADGroup)
        {
            if (ADGroup != null)
            {
                _ADProperties.ObjectID = ADGroup.Guid.Value;
                _Name = ADGroup.Name;
                //_AdajkProperties.Manager = null; //kívülről kell beállítani, ha szükséges
                //_ADProperties.GroupType = ADGroup.GroupTypeRef; //kívülről kell beállítani, ha szükséges
                DirectoryEntry de = ADGroup.GetUnderlyingObject() as DirectoryEntry;
                if (de.Properties["whenCreated"].Value == null)
                    _Created = null;
                else
                    _Created = Convert.ToDateTime(de.Properties["whenCreated"].Value);
                _Deleted = false; // ADGroup.Deleted;
            }
            GroupMembers = new WrappedADGroupCollection(ADGroup.Members);

        }

        /// <summary>
        /// XML-es konstruktor, amely beállítja a memberváltozókat is.
        /// </summary>
        /// <param name="node"></param>
        public WrappedADGroup(XmlNode node)
        {
            if (node != null)
            {
                _ADProperties.ObjectID = new Guid(node.Attributes["ObjectID"].Value);
                _Name = node.SelectSingleNode("GroupName").InnerText;
            }
        }

        //    /// <summary>
        //    /// Konstruktor, amely beállítja a memberváltozókat is.
        //    /// </summary>
        //    /// <param name="group"></param>
        //    public WrappedAdajkGroup(adajk.Group group) 
        //    {
        //      if (group != null)
        //      {
        //        m_ObjectID = group.ObjectId;
        //        m_GroupName = group.Name;
        //      }
        //    }

        #endregion

        #region Public members

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (this.GetType() != obj.GetType()) return false;
            if (Object.ReferenceEquals(this, obj)) return true;

            WrappedADGroup other = (WrappedADGroup)obj;
            //   _ChangeIndicator = new List<ChangedGroupIndicator>(5);
            bool isChanged = false;
            #region comparsions

            //if (other.Adajk.Manager == null & this._AdajkProperties.Manager != null)
            //    _ChangeIndicator.Add(ChangedGroupIndicator.Manager);
            //else if (other.Adajk.Manager != null & this._AdajkProperties.Manager == null)
            //    _ChangeIndicator.Add(ChangedGroupIndicator.Manager);
            //else if (other.Adajk.Manager != null & this._AdajkProperties.Manager != null 
            //    && (other.Adajk.ObjectID != this._AdajkProperties.ObjectID))
            //    _ChangeIndicator.Add(ChangedGroupIndicator.Manager);

            if (other.Name != this._Name)
                isChanged = true;
            //     _ChangeIndicator.Add(ChangedGroupIndicator.Name);

            if ((other.EDOKProp.ParentGroupID == null && this._EDOKProperties.ParentGroupID != null)
                || (other.EDOKProp.ParentGroupID != null && this._EDOKProperties.ParentGroupID == null))
                //_ChangeIndicator.Add(ChangedGroupIndicator.Position);
                isChanged = true;
            else if (other.EDOKProp.ParentGroupID != null)
            {
                if (!other.EDOKProp.ParentGroupID.Equals(this._EDOKProperties.ParentGroupID, StringComparison.InvariantCultureIgnoreCase))
                    // _ChangeIndicator.Add(ChangedGroupIndicator.Position);
                    isChanged = true;
            }

            if (this._Deleted != other.Deleted)
                //  _ChangeIndicator.Add(ChangedGroupIndicator.Deletion);
                isChanged = true;

            if (this._ADProperties.GroupType != other.ADProp.GroupType)
                //   _ChangeIndicator.Add(ChangedGroupIndicator.GroupType);
                isChanged = true;

            // ez szándékosan nem része az egyezőség vizsgálatának
            //if (!WrappedAdajkGroup.Equals(this.GroupMembers, other.GroupMembers))
            //    _ChangeIndicator.Add(ChangedGroupIndicator.Groupmembers);

            #endregion comparsions

            //return _ChangeIndicator.Count == 0;
            return !isChanged;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion Public members

        #region Properties

        /// <summary>
        /// AD-specifikus értékek.
        /// </summary>
        public ADProperties ADProp
        {
            get
            {
                return _ADProperties;
            }
            set
            {
                _ADProperties = value;
            }
        }

        /// <summary>
        /// Az adaptált rendszerben specifikus értékek.
        /// </summary>
        public EDOKProperties EDOKProp
        {
            get
            {
                return _EDOKProperties;
            }
            set
            {
                _EDOKProperties = value;
            }
        }

        //private List<ChangedGroupIndicator> _ChangeIndicator;
        ///// <summary>
        ///// Jelzi, hogy a szervezeti egység adataiban
        ///// milyen jellegű változások következtek be.
        ///// </summary>
        ///// <remarks>
        ///// Az Equal metódus meghívásával kap értéket.
        ///// </remarks>
        //public List<ChangedGroupIndicator> ChangeIndicator
        //{
        //    get
        //    {
        //        return _ChangeIndicator;
        //    }
        //    set
        //    {
        //        _ChangeIndicator = value;
        //    }
        //}

        /// <summary>
        /// XML string representation of this instance.
        /// </summary>
        public string AsXmlString
        {
            get
            {
                // TODO: implementálni
                System.IO.StringWriter sw = new System.IO.StringWriter();
                //XmlTextWriter xtw = new XmlTextWriter(sw);
                //xtw.WriteStartElement("AdajkGroup");
                //xtw.WriteAttributeString("ObjectID", m_ObjectID);
                //xtw.WriteStartElement("GroupName");
                //xtw.WriteCData(m_GroupName);
                //xtw.WriteEndElement();
                //xtw.WriteEndElement();
                return sw.ToString();
            }
        }

        /// <summary>
        /// Szervezeti egység neve.
        /// </summary>
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        /// <summary>
        /// AD-beli létrehozás időpontja.
        /// </summary>
        [XmlAttribute]
        public DateTime? Created
        {
            get
            {
                return _Created;
            }
            set
            {
                _Created = value;
            }
        }

        /// <summary>
        /// Az objektum az AD-ből törölve lett.
        /// </summary>
        [XmlAttribute]
        public bool Deleted
        {
            get
            {
                return _Deleted;
            }
            set
            {
                _Deleted = value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} ({1})", this._Name, this._ADProperties.ObjectID);
        }

        #endregion

        #region IComparer<ADGroup> Members

        public int Compare(WrappedADGroup x, WrappedADGroup y)
        {
            if (x == null || (y == null))
                return 0;
            return x._Name.CompareTo(y._Name);
        }

        #endregion
    }
}
