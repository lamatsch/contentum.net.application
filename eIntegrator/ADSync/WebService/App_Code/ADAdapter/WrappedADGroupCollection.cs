﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.ADAdapter
{
    // =====================================================================================
    /// <summary>
    /// ADGroup szerializálható kollekciója. 
    /// </summary>
    // =====================================================================================
    [Serializable]
[XmlRootAttribute("ADGroupCollection", IsNullable = false)] //, Namespace=Constants.C_NameSpaceURI
public sealed class WrappedADGroupCollection : IEnumerable<WrappedADGroup>
{
    #region Fields

    /// <summary>UA felhasználó szervezeti egység-tagsága.</summary>
    private List<WrappedADGroup> m_Groups = new List<WrappedADGroup>();

    #endregion

    #region Constructors

    /// <summary>
    /// Public konstruktor. Kell a szerializáláshoz.
    /// </summary>
    public WrappedADGroupCollection()
    {
        m_Groups = new List<WrappedADGroup>();
    }

    /// <summary>
    /// Létező listát felhasználó konstruktor.
    /// </summary>
    /// <param name="groups">Csoportlista.</param>
    public WrappedADGroupCollection(List<WrappedADGroup> groups)
    {
        if (groups == null)
            throw new ArgumentNullException("groups", "Érvényes listát kell megadni.");
        m_Groups = groups;
    }

        /// <summary>
        /// Létező listát felhasználó konstruktor.
        /// </summary>
        /// <param name="ADGroups">AD csoportok kollekciója.</param>
        public WrappedADGroupCollection(PrincipalCollection ADGroups)
        {
            if (ADGroups == null)
                throw new ArgumentNullException("ADGroups", "Érvényes listát kell megadni.");

         //   m_Groups = new List<WrappedADGroup>(ADGroups.Count);

            foreach (Principal pr in ADGroups)
            {

                GroupPrincipal gpr = pr as GroupPrincipal;
                if (gpr != null)
                {
                    if (gpr.IsSecurityGroup.Value)   //.Technical a technikai csoportokat kihagyjuk
                    {
                        m_Groups.Add(new WrappedADGroup(gpr));
                    }
                }
            }
        }

        #endregion

        #region Properties

        [XmlAttribute]
    public int Count
    {
        get
        {
            return m_Groups.Count;
        }
    }

    #endregion

    #region Public members

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        if (!(obj is WrappedADGroupCollection))
            return false;

        WrappedADGroupCollection other = (WrappedADGroupCollection)obj;

        if (other.Count != m_Groups.Count)
            return false;

        #region comparsions

        bool equal = true;
        for (int i = 0; i < m_Groups.Count; i++)
        {
            equal = equal && m_Groups.Contains(other[i]);
            if (!equal)
                break;
        }

        #endregion comparsions

        return equal;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public void Add(WrappedADGroup item)
    {
        m_Groups.Add(item);
    }

    public void AddRange(WrappedADGroupCollection collection)
    {
        if (collection != null)
            m_Groups.AddRange(collection);
    }

    public void Remove(WrappedADGroup item)
    {
        m_Groups.Remove(item);
    }

    public void Clear()
    {
        m_Groups.Clear();
    }

    public bool Contains(WrappedADGroup item)
    {
        return m_Groups.Contains(item);
    }

    /// <summary>
    /// Tartalmazás megítélése.
    /// </summary>
    /// <param name="item">Group neve vagy ObjectID-ja.</param>
    /// <returns>Igaz, ha része a kollekciónak.</returns>
    public bool Contains(string item)
    {
        if (String.IsNullOrEmpty(item))
            return false;

        foreach (WrappedADGroup group in m_Groups)
        {
            if (group.Name.Equals(item) || group.ADProp.ObjectID.Equals(item))
                return true;
        }

        return false;
    }

    public int IndexOf(WrappedADGroup item)
    {
        return m_Groups.IndexOf(item);
    }

    /// <summary>
    /// Elem pozíciójának lekérdezése.
    /// </summary>
    /// <param name="item">Group neve vagy ID-ja.</param>
    /// <returns>A megtalált elem indexe, vagy -1.</returns>
    public int IndexOf(string item)
    {
        WrappedADGroup wrapped = this[item];
        return wrapped == null ? -1 : IndexOf(wrapped);
    }

    /// <summary>
    /// Elem lekérdezése.
    /// </summary>
    /// <param name="item">Group neve vagy ID-ja.</param>
    /// <returns>A megtalált elem, vagy null.</returns>
    public WrappedADGroup this[string item]
    {
        get
        {
            if (String.IsNullOrEmpty(item))
                return null;

            foreach (WrappedADGroup group in m_Groups)
            {
                if (group.Name.Equals(item) || group.ADProp.ObjectID.Equals(item))
                    return group;
            }

            return null;
        }
    }

    /// <summary>
    /// Gets the element at the specified index.
    /// </summary>
    public WrappedADGroup this[int index]
    {
        get
        {
            return m_Groups[index];
        }
        set
        {
            m_Groups[index] = value;
        }
    }

    public List<WrappedADGroup>.Enumerator GetEnumerator()
    {
        return m_Groups.GetEnumerator();
    }

    public override string ToString()
    {
        return String.Format("({0} groups inside)", m_Groups.Count);
    }

    /// <summary>
    /// A nem kívánt tagságot (pl. Vezetés nevű fiktív csoport)
    /// eltávolítja a kollekcióból.
    /// </summary>
    public void RemoveLogicalGroups()
    {
        if (m_Groups == null)
            return;

        for (int i = 0; i < m_Groups.Count; i++)
        {
            if (m_Groups[i].ADProp.ObjectID.ToString() == "61636564-8401-4003-971c-4ba2bd2c5774")   // A Vezetés csoport azonosítója
            {
                m_Groups.RemoveAt(i);
                i = 0;
            }
        }
    }

    #endregion Public members


    #region IEnumerable<WrappedAdajkGroup> Members

    IEnumerator<WrappedADGroup> IEnumerable<WrappedADGroup>.GetEnumerator()
    {
        return GetEnumerator();
    }

    #endregion

    #region IEnumerable Members

    IEnumerator IEnumerable.GetEnumerator()
    {
        for (int i = 0; i < m_Groups.Count; i++)
            yield return m_Groups[i];
    }

        #endregion
    }
}
