﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.ADAdapter
{

    public sealed class EdokProperties
    {
        internal EdokProperties()
        {
        }
        /// <summary>
        /// A szerepkör azonosítója a külső rendszerben.
        /// </summary>
        public string ID;
    }


    /// <summary>
    /// WrappedADRole - szerepkör objektum.
    /// </summary>
    [Serializable]
    [XmlRootAttribute("ADRole", IsNullable=false)] //, Namespace=Constants.C_NameSpaceURI
    public sealed class WrappedADRole
    {
        #region Fields

        /// <summary>UA felhasználó szervezeti egység-tagsága.</summary>
        private string _Name;
        private EdokProperties _EdokProperties = new EdokProperties();

        #endregion

        #region Constructors

        /// <summary>
        /// Public konstruktor. Kell a szerializáláshoz.
        /// </summary>
        public WrappedADRole()
        {
            _Name = null;
        }

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="name">A szerepkör megnevezése.</param>
        public WrappedADRole(GroupPrincipal ADRole)
        {
            if (ADRole != null)
               _Name = ADRole.Name;
        }


        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="name">A szerepkör megnevezése.</param>
        public WrappedADRole(string name)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentNullException();
            _Name = name;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Szerepkör megnevezése.
        /// </summary>
        [XmlAttribute]
        public string Name
        {
            get
            {
                return _Name;
            }
        }

        /// <summary>
        /// Az adaptált rendszerben specifikus értékek.
        /// </summary>
        public EdokProperties EdokProp
        {
            get
            {
                return _EdokProperties;
            }
        }

        #endregion Properties

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (this.GetType() != obj.GetType()) return false;
            if (Object.ReferenceEquals(this, obj)) return true;

            WrappedADRole other = (WrappedADRole)obj;

            return other.Name == this._Name;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format(_Name == null ? "<empty>" : _Name);
        }

    }
}