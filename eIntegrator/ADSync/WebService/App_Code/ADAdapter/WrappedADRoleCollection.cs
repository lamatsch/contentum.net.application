﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.ADAdapter
{

    // =====================================================================================
    /// <summary>
    /// WrappedAdajkRole szerializálható kollekciója. 
    /// </summary>
    // =====================================================================================
    [Serializable]
    [XmlRootAttribute("ADRoleCollection", IsNullable=false)] //, Namespace=Constants.C_NameSpaceURI
    public sealed class WrappedADRoleCollection
    {
        #region Fields

		private List<WrappedADRole> m_Roles = new List<WrappedADRole>();

        #endregion

        #region Constructors

        /// <summary>
        /// Public konstruktor. Kell a szerializáláshoz.
        /// </summary>
        public WrappedADRoleCollection()
        {
            m_Roles = new List<WrappedADRole>();
        }

        public WrappedADRoleCollection(PrincipalSearchResult<Principal> ADRoles)
        {
            if (ADRoles != null)
            {
                foreach (GroupPrincipal sr in ADRoles)
                {
                    //DirectoryEntry de = sr.GetUnderlyingObject() as DirectoryEntry;

                    WrappedADRole wrapped = new WrappedADRole(sr);
                    m_Roles.Add(wrapped);
                   
                }
              
            }
        }

        public WrappedADRoleCollection(List<WrappedADRole> roles)
        {
            if (roles == null)
                throw new ArgumentNullException("roles", "Érvényes listát kell megadni.");
            m_Roles = roles;
        }

        #endregion

        #region Properties

        [XmlAttribute]
        public int Count
        {
            get
            {
                return m_Roles.Count;
            }
        }

        #endregion

        #region Public members

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (!(obj is WrappedADRoleCollection))
                return false;

            WrappedADRoleCollection other = (WrappedADRoleCollection)obj;

            if (other.Count != m_Roles.Count)
                return false;

            #region comparsions

            bool equal = true;
            for (int i = 0; i < m_Roles.Count; i++)
                equal = equal && m_Roles.Contains(other[i]);

            #endregion comparsions

            return equal;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public void Add(WrappedADRole item)
        {
            m_Roles.Add(item);
        }

        public void Remove(WrappedADRole item)
        {
            m_Roles.Remove(item);
        }

        public void Clear()
        {
            m_Roles.Clear();
        }

        public bool Contains(WrappedADRole item)
        {
            return m_Roles.Contains(item);
        }

        /// <summary>
        /// Tartalmazás megállapítása.
        /// </summary>
        /// <param name="item">Szerepkör neve.</param>
        /// <returns>Igaz, ha része a kollekciónak.</returns>
        public bool Contains(string item)
        {
            if (String.IsNullOrEmpty(item))
                return false;

            foreach (WrappedADRole role in m_Roles)
            {
                if (role.Name.Equals(item))
                    return true;
            }
            
            return false;
        }

        public int IndexOf(WrappedADRole item)
        {
            return m_Roles.IndexOf(item);
        }

        /// <summary>
        /// Elem pozíciójának lekérdezése.
        /// </summary>
        /// <param name="item">Szerepkör neve.</param>
        /// <returns>A megtalált elem indexe, vagy -1.</returns>
        public int IndexOf(string item)
        {
            WrappedADRole role = this[item];
            return role == null ? -1 : IndexOf(role);
        }

        /// <summary>
        /// Elem lekérdezése.
        /// </summary>
        /// <param name="item">Role neve.</param>
        /// <returns>A megtalált elem, vagy null.</returns>
        public WrappedADRole this[string item]
        {
            get
            {
                if (String.IsNullOrEmpty(item))
                    return null;

                foreach (WrappedADRole role in m_Roles)
                {
                    if (role.Name.Equals(item))
                        return role;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the element at the specified index.
        /// </summary>
        public WrappedADRole this[int index]
        {
            get
            {
                return m_Roles[index];
            }
            set
            {
                m_Roles[index] = value;
            }
        }

        public List<WrappedADRole>.Enumerator GetEnumerator()
        {
            return m_Roles.GetEnumerator();
        }

        public override string ToString()
        {
            return String.Format("({0} roles inside)", m_Roles.Count);
        }

        #endregion Public members

    }
}
