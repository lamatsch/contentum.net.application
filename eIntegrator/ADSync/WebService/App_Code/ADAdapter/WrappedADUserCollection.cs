﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.ADAdapter
{
    // =====================================================================================
    /// <summary>
    /// ADUser szerializálható kollekciója. 
    /// </summary>
    // =====================================================================================
    [Serializable]
    [XmlRootAttribute("ADUserCollection", IsNullable=false)] //, Namespace=Constants.C_NameSpaceURI
    public sealed class WrappedADUserCollection : IEnumerable<WrappedADUser>
    {

        #region Fields

        /// <summary>UA felhasználó szervezeti egység-tagsága.</summary>
        private List<WrappedADUser> m_Users = new List<WrappedADUser>();

        #endregion

        #region Constructors

        /// <summary>
        /// Public konstruktor. Kell a szerializáláshoz.
        /// </summary>
        public WrappedADUserCollection()
        {
            m_Users = new List<WrappedADUser>();
        }

        /// <summary>
        /// Létező listát használó konstruktor.
        /// </summary>
        /// <param name="adUsers">ADAJK felhasználók kollekciója.</param>
        //public WrappedADUserCollection(Fph.Adajk.BusinessDocuments.UserExtendedContainer adajkUsers)
        //{
        //    if (adajkUsers == null)
        //        throw new ArgumentNullException("adajkUsers", "Érvényes listát kell megadni.");

        //    m_Users = new List<WrappedADUser>(adajkUsers.AllCount);
        //    for (int i = 0; i < adajkUsers.AllCount; i++)
        //    {
        //        Fph.Adajk.BusinessDocuments.UserExtended adajkUser = 
        //            (Fph.Adajk.BusinessDocuments.UserExtended)adajkUsers[i];

        //        if (adajkUser.Dn.IndexOf("_Unpersonate") == -1)
        //            if (!adajkUser.Deleted)
        //                if (!adajkUser.LoginName.StartsWith("$"))   // még nem aktivált felhasználók
        //                    m_Users.Add(new WrappedADUser(adajkUser));
        //    }
        //}

        /// <summary>
        /// Létező listát felhasználó konstruktor.
        /// </summary>
        /// <param name="users">Felhasználók kollekciója.</param>
        public WrappedADUserCollection(List<WrappedADUser> users)
        {
            if (users == null)
                throw new ArgumentNullException("users", "Érvényes listát kell megadni.");
            m_Users = users;
        }

        #endregion

        #region Properties

        [XmlAttribute]
        public int Count
        {
            get
            {
                return m_Users.Count;
            }
        }

        #endregion

        #region Public members

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (!(obj is WrappedADUserCollection))
                return false;

            WrappedADUserCollection other = (WrappedADUserCollection)obj;

            if (other.Count != m_Users.Count)
                return false;

            #region comparsions

            bool equal = true;
            for (int i = 0; i < m_Users.Count; i++)
            {
                equal = equal && m_Users.Contains(other[i]);
                if (!equal)
                    break;
            }

            #endregion comparsions

            return equal;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public void Add(WrappedADUser item)
        {
            m_Users.Add(item);
        }

        public void AddRange(WrappedADUserCollection collection)
        {
            if (collection != null)
                m_Users.AddRange(collection);
        }

        public void Remove(WrappedADUser item)
        {
            m_Users.Remove(item);
        }

        public void Clear()
        {
            m_Users.Clear();
        }

        public bool Contains(WrappedADUser item)
        {
            return m_Users.Contains(item);
        }

        /// <summary>
        /// Tartalmazás megállapítása.
        /// </summary>
        /// <param name="item">User loginneve vagy ObjectID-ja.</param>
        /// <returns>Igaz, ha része a kollekciónak.</returns>
        public bool Contains(string item)
        {
            if (String.IsNullOrEmpty(item))
                return false;

            foreach (WrappedADUser user in m_Users)
            {
                if (user.LoginName.Equals(item) || user.ADProp.ObjectID.Equals(item))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Elem pozíciójának lekérdezése.
        /// </summary>
        /// <param name="item">A kérdéses elem.</param>
        /// <returns>A megtalált elem indexe, vagy -1.</returns>
        public int IndexOf(WrappedADUser item)
        {
            return m_Users.IndexOf(item);
        }

        /// <summary>
        /// Elem pozíciójának lekérdezése.
        /// </summary>
        /// <param name="item">User loginneve vagy ObjectID-ja.</param>
        /// <returns>A megtalált elem indexe, vagy -1.</returns>
        public int IndexOf(string item)
        {
            WrappedADUser user = this[item];
            return user == null ? -1 : IndexOf(user);
        }

        /// <summary>
        /// Elem lekérdezése.
        /// </summary>
        /// <param name="item">User loginneve vagy ObjectID-ja.</param>
        /// <returns>A megtalált elem, vagy null.</returns>
        public WrappedADUser this[string item]
        {
            get
            {
                if (String.IsNullOrEmpty(item))
                    return null;

                item = item.ToLower();

                foreach (WrappedADUser user in m_Users)
                {
                    if (user.LoginName.Equals(item, StringComparison.InvariantCultureIgnoreCase) || user.ADProp.ObjectID.ToString().Equals(item, StringComparison.InvariantCultureIgnoreCase))
                    //if (user.LoginName.ToLower().Equals(item) || user.Adajk.ObjectID.Equals(item))
                        return user;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the element at the specified index.
        /// </summary>
        public WrappedADUser this[int index]
        {
            get
            {
                return m_Users[index];
            }
            set
            {
                m_Users[index] = value;
            }
        }

        public List<WrappedADUser>.Enumerator GetEnumerator()
        {
            return m_Users.GetEnumerator();
        }

        public override string ToString()
        {
            return String.Format("({0} users inside)", m_Users.Count);
        }

        #endregion Public members

        #region IEnumerable<WrappedADGroup> Members

        IEnumerator<WrappedADUser> IEnumerable<WrappedADUser>.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < m_Users.Count; i++)
                yield return m_Users[i];
        }

        #endregion

    }
}