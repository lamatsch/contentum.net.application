﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.8009
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.3038.
// 
namespace Contentum.eIntegrator.Service {
    using System.Xml.Serialization;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Diagnostics;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="IAPIInterfacebinding", Namespace="127.0.0.1")]
    public partial class IAPIInterfaceService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback getSessionOperationCompleted;
        
        private System.Threading.SendOrPostCallback checkSessionOperationCompleted;
        
        private System.Threading.SendOrPostCallback DMSOneGetPartnerOperationCompleted;
        
        private System.Threading.SendOrPostCallback DMSOneDataCommOperationCompleted;
        
        private System.Threading.SendOrPostCallback DMSOneRendeleshezKepOperationCompleted;
        
        private System.Threading.SendOrPostCallback DMSOneAddSzerzodesKepOperationCompleted;
        
        private System.Threading.SendOrPostCallback DMSOneSzamlahozKepOperationCompleted;

        /// <remarks/>
        public IAPIInterfaceService(string ServiceUrl)
        {
            this.Url = ServiceUrl;
        }


        /// <remarks/>
        public event getSessionCompletedEventHandler getSessionCompleted;
        
        /// <remarks/>
        public event checkSessionCompletedEventHandler checkSessionCompleted;
        
        /// <remarks/>
        public event DMSOneGetPartnerCompletedEventHandler DMSOneGetPartnerCompleted;
        
        /// <remarks/>
        public event DMSOneDataCommCompletedEventHandler DMSOneDataCommCompleted;
        
        /// <remarks/>
        public event DMSOneRendeleshezKepCompletedEventHandler DMSOneRendeleshezKepCompleted;
        
        /// <remarks/>
        public event DMSOneAddSzerzodesKepCompletedEventHandler DMSOneAddSzerzodesKepCompleted;
        
        /// <remarks/>
        public event DMSOneSzamlahozKepCompletedEventHandler DMSOneSzamlahozKepCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("urn:USoapServices-IAPIInterface#getSession", RequestNamespace="urn:USoapServices-IAPIInterface", ResponseNamespace="urn:USoapServices-IAPIInterface")]
        [return: System.Xml.Serialization.SoapElementAttribute("return")]
        public bool getSession(string UID, string password, ref string SID, string isVallalkozas) {
            object[] results = this.Invoke("getSession", new object[] {
                        UID,
                        password,
                        SID,
                        isVallalkozas});
            SID = ((string)(results[1]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BegingetSession(string UID, string password, string SID, string isVallalkozas, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("getSession", new object[] {
                        UID,
                        password,
                        SID,
                        isVallalkozas}, callback, asyncState);
        }
        
        /// <remarks/>
        public bool EndgetSession(System.IAsyncResult asyncResult, out string SID) {
            object[] results = this.EndInvoke(asyncResult);
            SID = ((string)(results[1]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void getSessionAsync(string UID, string password, string SID, string isVallalkozas) {
            this.getSessionAsync(UID, password, SID, isVallalkozas, null);
        }
        
        /// <remarks/>
        public void getSessionAsync(string UID, string password, string SID, string isVallalkozas, object userState) {
            if ((this.getSessionOperationCompleted == null)) {
                this.getSessionOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetSessionOperationCompleted);
            }
            this.InvokeAsync("getSession", new object[] {
                        UID,
                        password,
                        SID,
                        isVallalkozas}, this.getSessionOperationCompleted, userState);
        }
        
        private void OngetSessionOperationCompleted(object arg) {
            if ((this.getSessionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getSessionCompleted(this, new getSessionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("urn:USoapServices-IAPIInterface#checkSession", RequestNamespace="urn:USoapServices-IAPIInterface", ResponseNamespace="urn:USoapServices-IAPIInterface")]
        [return: System.Xml.Serialization.SoapElementAttribute("return")]
        public bool checkSession(string SID) {
            object[] results = this.Invoke("checkSession", new object[] {
                        SID});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BegincheckSession(string SID, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("checkSession", new object[] {
                        SID}, callback, asyncState);
        }
        
        /// <remarks/>
        public bool EndcheckSession(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void checkSessionAsync(string SID) {
            this.checkSessionAsync(SID, null);
        }
        
        /// <remarks/>
        public void checkSessionAsync(string SID, object userState) {
            if ((this.checkSessionOperationCompleted == null)) {
                this.checkSessionOperationCompleted = new System.Threading.SendOrPostCallback(this.OncheckSessionOperationCompleted);
            }
            this.InvokeAsync("checkSession", new object[] {
                        SID}, this.checkSessionOperationCompleted, userState);
        }
        
        private void OncheckSessionOperationCompleted(object arg) {
            if ((this.checkSessionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.checkSessionCompleted(this, new checkSessionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("urn:USoapServices-IAPIInterface#DMSOneGetPartner", RequestNamespace="urn:USoapServices-IAPIInterface", ResponseNamespace="urn:USoapServices-IAPIInterface")]
        [return: System.Xml.Serialization.SoapElementAttribute("return")]
        public string DMSOneGetPartner(string SID, string adoszam) {
            object[] results = this.Invoke("DMSOneGetPartner", new object[] {
                        SID,
                        adoszam});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginDMSOneGetPartner(string SID, string adoszam, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("DMSOneGetPartner", new object[] {
                        SID,
                        adoszam}, callback, asyncState);
        }
        
        /// <remarks/>
        public string EndDMSOneGetPartner(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void DMSOneGetPartnerAsync(string SID, string adoszam) {
            this.DMSOneGetPartnerAsync(SID, adoszam, null);
        }
        
        /// <remarks/>
        public void DMSOneGetPartnerAsync(string SID, string adoszam, object userState) {
            if ((this.DMSOneGetPartnerOperationCompleted == null)) {
                this.DMSOneGetPartnerOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDMSOneGetPartnerOperationCompleted);
            }
            this.InvokeAsync("DMSOneGetPartner", new object[] {
                        SID,
                        adoszam}, this.DMSOneGetPartnerOperationCompleted, userState);
        }
        
        private void OnDMSOneGetPartnerOperationCompleted(object arg) {
            if ((this.DMSOneGetPartnerCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DMSOneGetPartnerCompleted(this, new DMSOneGetPartnerCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("urn:USoapServices-IAPIInterface#DMSOneDataComm", RequestNamespace="urn:USoapServices-IAPIInterface", ResponseNamespace="urn:USoapServices-IAPIInterface")]
        [return: System.Xml.Serialization.SoapElementAttribute("return")]
        public bool DMSOneDataComm(string SID, string XMLDataPacket, string[] ImageData, string ImageExtension, ref int ErrorID, ref string ErrorMessage) {
            object[] results = this.Invoke("DMSOneDataComm", new object[] {
                        SID,
                        XMLDataPacket,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage});
            ErrorID = ((int)(results[1]));
            ErrorMessage = ((string)(results[2]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginDMSOneDataComm(string SID, string XMLDataPacket, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("DMSOneDataComm", new object[] {
                        SID,
                        XMLDataPacket,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage}, callback, asyncState);
        }
        
        /// <remarks/>
        public bool EndDMSOneDataComm(System.IAsyncResult asyncResult, out int ErrorID, out string ErrorMessage) {
            object[] results = this.EndInvoke(asyncResult);
            ErrorID = ((int)(results[1]));
            ErrorMessage = ((string)(results[2]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void DMSOneDataCommAsync(string SID, string XMLDataPacket, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage) {
            this.DMSOneDataCommAsync(SID, XMLDataPacket, ImageData, ImageExtension, ErrorID, ErrorMessage, null);
        }
        
        /// <remarks/>
        public void DMSOneDataCommAsync(string SID, string XMLDataPacket, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage, object userState) {
            if ((this.DMSOneDataCommOperationCompleted == null)) {
                this.DMSOneDataCommOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDMSOneDataCommOperationCompleted);
            }
            this.InvokeAsync("DMSOneDataComm", new object[] {
                        SID,
                        XMLDataPacket,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage}, this.DMSOneDataCommOperationCompleted, userState);
        }
        
        private void OnDMSOneDataCommOperationCompleted(object arg) {
            if ((this.DMSOneDataCommCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DMSOneDataCommCompleted(this, new DMSOneDataCommCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("urn:USoapServices-IAPIInterface#DMSOneRendeleshezKep", RequestNamespace="urn:USoapServices-IAPIInterface", ResponseNamespace="urn:USoapServices-IAPIInterface")]
        [return: System.Xml.Serialization.SoapElementAttribute("return")]
        public bool DMSOneRendeleshezKep(string SID, string rendelesszam, string[] ImageData, string ImageExtension, ref int ErrorID, ref string ErrorMessage) {
            object[] results = this.Invoke("DMSOneRendeleshezKep", new object[] {
                        SID,
                        rendelesszam,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage});
            ErrorID = ((int)(results[1]));
            ErrorMessage = ((string)(results[2]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginDMSOneRendeleshezKep(string SID, string rendelesszam, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("DMSOneRendeleshezKep", new object[] {
                        SID,
                        rendelesszam,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage}, callback, asyncState);
        }
        
        /// <remarks/>
        public bool EndDMSOneRendeleshezKep(System.IAsyncResult asyncResult, out int ErrorID, out string ErrorMessage) {
            object[] results = this.EndInvoke(asyncResult);
            ErrorID = ((int)(results[1]));
            ErrorMessage = ((string)(results[2]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void DMSOneRendeleshezKepAsync(string SID, string rendelesszam, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage) {
            this.DMSOneRendeleshezKepAsync(SID, rendelesszam, ImageData, ImageExtension, ErrorID, ErrorMessage, null);
        }
        
        /// <remarks/>
        public void DMSOneRendeleshezKepAsync(string SID, string rendelesszam, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage, object userState) {
            if ((this.DMSOneRendeleshezKepOperationCompleted == null)) {
                this.DMSOneRendeleshezKepOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDMSOneRendeleshezKepOperationCompleted);
            }
            this.InvokeAsync("DMSOneRendeleshezKep", new object[] {
                        SID,
                        rendelesszam,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage}, this.DMSOneRendeleshezKepOperationCompleted, userState);
        }
        
        private void OnDMSOneRendeleshezKepOperationCompleted(object arg) {
            if ((this.DMSOneRendeleshezKepCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DMSOneRendeleshezKepCompleted(this, new DMSOneRendeleshezKepCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("urn:USoapServices-IAPIInterface#DMSOneAddSzerzodesKep", RequestNamespace="urn:USoapServices-IAPIInterface", ResponseNamespace="urn:USoapServices-IAPIInterface")]
        [return: System.Xml.Serialization.SoapElementAttribute("return")]
        public bool DMSOneAddSzerzodesKep(string SID, string iktatoszam, string[] ImageData, string ImageExtension, ref int ErrorID, ref string ErrorMessage) {
            object[] results = this.Invoke("DMSOneAddSzerzodesKep", new object[] {
                        SID,
                        iktatoszam,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage});
            ErrorID = ((int)(results[1]));
            ErrorMessage = ((string)(results[2]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginDMSOneAddSzerzodesKep(string SID, string iktatoszam, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("DMSOneAddSzerzodesKep", new object[] {
                        SID,
                        iktatoszam,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage}, callback, asyncState);
        }
        
        /// <remarks/>
        public bool EndDMSOneAddSzerzodesKep(System.IAsyncResult asyncResult, out int ErrorID, out string ErrorMessage) {
            object[] results = this.EndInvoke(asyncResult);
            ErrorID = ((int)(results[1]));
            ErrorMessage = ((string)(results[2]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void DMSOneAddSzerzodesKepAsync(string SID, string iktatoszam, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage) {
            this.DMSOneAddSzerzodesKepAsync(SID, iktatoszam, ImageData, ImageExtension, ErrorID, ErrorMessage, null);
        }
        
        /// <remarks/>
        public void DMSOneAddSzerzodesKepAsync(string SID, string iktatoszam, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage, object userState) {
            if ((this.DMSOneAddSzerzodesKepOperationCompleted == null)) {
                this.DMSOneAddSzerzodesKepOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDMSOneAddSzerzodesKepOperationCompleted);
            }
            this.InvokeAsync("DMSOneAddSzerzodesKep", new object[] {
                        SID,
                        iktatoszam,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage}, this.DMSOneAddSzerzodesKepOperationCompleted, userState);
        }
        
        private void OnDMSOneAddSzerzodesKepOperationCompleted(object arg) {
            if ((this.DMSOneAddSzerzodesKepCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DMSOneAddSzerzodesKepCompleted(this, new DMSOneAddSzerzodesKepCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("urn:USoapServices-IAPIInterface#DMSOneSzamlahozKep", RequestNamespace="urn:USoapServices-IAPIInterface", ResponseNamespace="urn:USoapServices-IAPIInterface")]
        [return: System.Xml.Serialization.SoapElementAttribute("return")]
        public bool DMSOneSzamlahozKep(string SID, string szamlakod, string[] ImageData, string ImageExtension, ref int ErrorID, ref string ErrorMessage) {
            object[] results = this.Invoke("DMSOneSzamlahozKep", new object[] {
                        SID,
                        szamlakod,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage});
            ErrorID = ((int)(results[1]));
            ErrorMessage = ((string)(results[2]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginDMSOneSzamlahozKep(string SID, string szamlakod, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("DMSOneSzamlahozKep", new object[] {
                        SID,
                        szamlakod,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage}, callback, asyncState);
        }
        
        /// <remarks/>
        public bool EndDMSOneSzamlahozKep(System.IAsyncResult asyncResult, out int ErrorID, out string ErrorMessage) {
            object[] results = this.EndInvoke(asyncResult);
            ErrorID = ((int)(results[1]));
            ErrorMessage = ((string)(results[2]));
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void DMSOneSzamlahozKepAsync(string SID, string szamlakod, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage) {
            this.DMSOneSzamlahozKepAsync(SID, szamlakod, ImageData, ImageExtension, ErrorID, ErrorMessage, null);
        }
        
        /// <remarks/>
        public void DMSOneSzamlahozKepAsync(string SID, string szamlakod, string[] ImageData, string ImageExtension, int ErrorID, string ErrorMessage, object userState) {
            if ((this.DMSOneSzamlahozKepOperationCompleted == null)) {
                this.DMSOneSzamlahozKepOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDMSOneSzamlahozKepOperationCompleted);
            }
            this.InvokeAsync("DMSOneSzamlahozKep", new object[] {
                        SID,
                        szamlakod,
                        ImageData,
                        ImageExtension,
                        ErrorID,
                        ErrorMessage}, this.DMSOneSzamlahozKepOperationCompleted, userState);
        }
        
        private void OnDMSOneSzamlahozKepOperationCompleted(object arg) {
            if ((this.DMSOneSzamlahozKepCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DMSOneSzamlahozKepCompleted(this, new DMSOneSzamlahozKepCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void getSessionCompletedEventHandler(object sender, getSessionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class getSessionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal getSessionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public string SID {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void checkSessionCompletedEventHandler(object sender, checkSessionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class checkSessionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal checkSessionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void DMSOneGetPartnerCompletedEventHandler(object sender, DMSOneGetPartnerCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DMSOneGetPartnerCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal DMSOneGetPartnerCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void DMSOneDataCommCompletedEventHandler(object sender, DMSOneDataCommCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DMSOneDataCommCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal DMSOneDataCommCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public int ErrorID {
            get {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[1]));
            }
        }
        
        /// <remarks/>
        public string ErrorMessage {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[2]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void DMSOneRendeleshezKepCompletedEventHandler(object sender, DMSOneRendeleshezKepCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DMSOneRendeleshezKepCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal DMSOneRendeleshezKepCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public int ErrorID {
            get {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[1]));
            }
        }
        
        /// <remarks/>
        public string ErrorMessage {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[2]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void DMSOneAddSzerzodesKepCompletedEventHandler(object sender, DMSOneAddSzerzodesKepCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DMSOneAddSzerzodesKepCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal DMSOneAddSzerzodesKepCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public int ErrorID {
            get {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[1]));
            }
        }
        
        /// <remarks/>
        public string ErrorMessage {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[2]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void DMSOneSzamlahozKepCompletedEventHandler(object sender, DMSOneSzamlahozKepCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DMSOneSzamlahozKepCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal DMSOneSzamlahozKepCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public int ErrorID {
            get {
                this.RaiseExceptionIfNecessary();
                return ((int)(this.results[1]));
            }
        }
        
        /// <remarks/>
        public string ErrorMessage {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[2]));
            }
        }
    }
}
