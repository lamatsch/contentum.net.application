using Contentum.eBusinessDocuments;
using System;

namespace Contentum.eIntegrator.Service
{

    public partial class ServiceFactory
    {

        //public INT_ParameterekService GetINT_ParameterekService()
        //{
        //    if (_BusinessServiceType == "SOAP")
        //    {
        //        INT_ParameterekService _Service = new INT_ParameterekService(_BusinessServiceUrl + "INT_ParameterekService.asmx");
        //        _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
        //        return _Service;
        //    }

        //    return null;
        //}

        public INT_ModulokService GetINT_ModulokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_ModulokService _Service = new INT_ModulokService(_BusinessServiceUrl + "INT_ModulokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public INT_LogService GetINT_LogService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_LogService _Service = new INT_LogService(_BusinessServiceUrl + "INT_LogService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public INT_ExternalIDsService GetINT_ExternalIDsService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_ExternalIDsService _Service = new INT_ExternalIDsService(_BusinessServiceUrl + "INT_ExternalIDsService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public INT_ManagedEmailAddressService GetINT_ManagedEmailAddressService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_ManagedEmailAddressService _Service = new INT_ManagedEmailAddressService(_BusinessServiceUrl + "INT_ManagedEmailAddressService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public INT_PIRService getINT_PIRService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_PIRService _Service = new INT_PIRService(_BusinessServiceUrl + "INT_PIRService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);

                return _Service;
            }

            return null;
        }

        public INT_PIRService getINT_PIRService(String PIR_Rendszer)
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_PIRService _Service = new INT_PIRService(_BusinessServiceUrl + "INT_PIRService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                _Service.SetPIR_Rendszer(PIR_Rendszer);
                return _Service;
            }

            return null;
        }


        //    public IAPIInterfaceService getIAPIInterfaceService()
        //    {
        //        if (_BusinessServiceType == "SOAP")
        //        {
        //            IAPIInterfaceService _Service = new IAPIInterfaceService(_BusinessServiceUrl);
        //            _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
        //            return _Service;
        //        }

        //        return null;
        //    }

        public eUzenetService geteUzenetService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                eUzenetService _Service = new eUzenetService(_BusinessServiceUrl + "eUzenetService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);

                return _Service;
            }

            return null;
        }

        public INT_HAIRAdatokService GetINT_HAIRAdatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_HAIRAdatokService _Service = new INT_HAIRAdatokService(_BusinessServiceUrl + "INT_HAIRAdatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }
    }
}