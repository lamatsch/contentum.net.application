﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.Utility;
using System;
using System.Configuration;

namespace Contentum.eIntegrator.Service
{

    public partial class ServiceFactory
    {
        #region CR3235 -WINPA
        public INT_WINPAService GetINT_WINPAServiceService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_WINPAService _Service = new INT_WINPAService(_BusinessServiceUrl + "INT_WINPAService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }
        #endregion

        private static void SetDefaults(System.Web.Services.Protocols.SoapHttpClientProtocol service)
        {
            service.PreAuthenticate = true;
            // BUG_11753
            service.AllowAutoRedirect = true;

        }

        public CommonEIntegratorService GetCommonEIntegratorService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                CommonEIntegratorService _Service = new CommonEIntegratorService(_BusinessServiceUrl + "CommonEIntegratorService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public AdatkapuService GetAdatkapuService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                AdatkapuService _Service = new AdatkapuService(_BusinessServiceUrl + "AdatkapuService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public INT_ParameterekService GetINT_ParameterekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_ParameterekService _Service = new INT_ParameterekService(_BusinessServiceUrl + "INT_ParameterekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public INT_ONYPService GetINT_ONYPService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_ONYPService _Service = new INT_ONYPService(_BusinessServiceUrl + "INT_ONYPService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }          

        public INT_ASPADO_DocumentService GetASPADO_DocumentService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_ASPADO_DocumentService _Service = new INT_ASPADO_DocumentService(_BusinessServiceUrl + "INT_ASPADO_DocumentService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public INT_ASPDWH_FileService GetINT_ASPDWH_FileService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_ASPDWH_FileService _Service = new INT_ASPDWH_FileService(_BusinessServiceUrl + "INT_ASPDWH_FileService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }



        public DWHFileService GetDWHFileService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                DWHFileService _Service = null;
                string ASP_DWH_IFACE_Mode_VALUE = ConfigurationManager.AppSettings.Get(ASP_DWH_KKSZB_Helper.Enum_ASP_DWH_APPSETTINGS.ASP_DWH_IFACE_Mode.ToString());

                if (!string.IsNullOrEmpty(ASP_DWH_IFACE_Mode_VALUE) &&
                     ASP_DWH_IFACE_Mode_VALUE.Equals(ASP_DWH_KKSZB_Helper.Enum_ASP_DWH_IFACE_Mode.KKSZB.ToString(), System.StringComparison.CurrentCultureIgnoreCase))
                {
                    Logger.Debug("ASP_DWH_IFACE_Mode_VALUE = 'KKSZB'");

                    #region URL
                    string ASP_DWH_KKSZB_URL_VALUE = ConfigurationManager.AppSettings.Get(ASP_DWH_KKSZB_Helper.Enum_ASP_DWH_APPSETTINGS.ASP_DWH_KKSZB_URL.ToString());
                    if (string.IsNullOrEmpty(ASP_DWH_KKSZB_URL_VALUE))
                    {
                        Logger.Debug("Missing 'ASP_DWH_KKSZB_URL_VALUE' app settings value !");
                        throw new Exception("Missing 'ASP_DWH_KKSZB_URL_VALUE' app settings value !");
                    }
                    _Service = new DWHFileService(ASP_DWH_KKSZB_URL_VALUE);
                    #endregion
                }
                else
                {
                    _Service = new DWHFileService(ConfigurationManager.AppSettings.Get("ASP_DWH_WS_URL"));
                }

                Logger.Debug(ConfigurationManager.AppSettings.Get("ASP_DWH_WS_URLBusinessServiceUserName") + "__________" +
                      ConfigurationManager.AppSettings.Get("ASP_DWH_WS_URLBusinessServicePassword") + "________" +
                      ConfigurationManager.AppSettings.Get("ASP_DWH_WS_URLBusinessServiceUserDomain"));
                _Service.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("ASP_DWH_WS_URLBusinessServiceUserName"),
                    ConfigurationManager.AppSettings.Get("ASP_DWH_WS_URLBusinessServicePassword"),
                    ConfigurationManager.AppSettings.Get("ASP_DWH_WS_URLBusinessServiceUserDomain"));
                SetDefaults(_Service);

                return _Service;
            }

            return null;
        }

        public INT_NAPService GetINT_NAPService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                INT_NAPService _Service = new INT_NAPService(_BusinessServiceUrl + "INT_NAPService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

    }
}