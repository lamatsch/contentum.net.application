﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using log4net;
using System;
using System.Data;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Xml;

// CR3234: PIR interface
// EcoStat PIR interface-t megvalósító osztály
namespace Contentum.eIntegrator.Service
{
    public class EcoStat : INT_PIRInterface
    {
        #region Varible declaration

        private static readonly ILog logger = LogManager.GetLogger(typeof(Logger));

        //public static string Modul_id;
        //public static string Org_id;

        private static Contentum.eIntegrator.Service.INT_LogService service_Log = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.GetINT_LogService();

        #endregion

        private class PIRExternalService
        {

            private string _pIRExternalServiceURL;
            public string PIRExternalServiceURL
            {
                get { return _pIRExternalServiceURL; }
                set { _pIRExternalServiceURL = value; }
            }
            private string _pIRExternalServiceUserName;
            public string PIRExternalServiceUserName
            {
                get { return _pIRExternalServiceUserName; }
                set { _pIRExternalServiceUserName = value; }
            }

            private string _pIRExternalServicePassword;
            public string PIRExternalServicePassword
            {
                get { return _pIRExternalServicePassword; }
                set { _pIRExternalServicePassword = value; }
            }

            private bool _isServiceActive;
            public bool IsServiceActive
            {
                get { return _isServiceActive; }
                set { _isServiceActive = value; }
            }

            //private string _SID;
            //public string SID
            //{
            //    get { return _SID; }
            //    set { _SID = value; }
            //}

            public PIRExternalService()
            {
                //this.PIRExternalServiceURL = WebConfigurationManager.AppSettings["PIRServiceBusinessServiceUrl"];
                //this.PIRExternalServiceUserName = WebConfigurationManager.AppSettings["PIRServiceBusinessServiceUserName"];
                //this.PIRExternalServicePassword = WebConfigurationManager.AppSettings["PIRServiceBusinessServicePassword"];
                //    this.IsServiceActive = bool.Parse(WebConfigurationManager.AppSettings["isServiceActive"]);

            }

            public PIRExternalService(String Config)
            {

                this.PIRExternalServiceURL = WebConfigurationManager.AppSettings["PIRServiceBusinessServiceUrl_"+Config];
                this.PIRExternalServiceUserName = WebConfigurationManager.AppSettings["PIRServiceBusinessServiceUserName_" + Config];
                this.PIRExternalServicePassword = WebConfigurationManager.AppSettings["PIRServiceBusinessServicePassword_" + Config];
                //    this.IsServiceActive = bool.Parse(WebConfigurationManager.AppSettings["isServiceActive"]);
                if (String.IsNullOrEmpty(this.PIRExternalServiceURL))
                    throw new ResultException("PIR1001","Nincs a megadott Vevőbesoroláshoz tartozó configbeállítás. Besorolás: " +Config);

            }
        }

   
        private PIRExternalService pirExternalService;

        public EcoStat()
        {
            pirExternalService = new PIRExternalService();
        }

   
        private string _SID;
        public string SID
        {
            get { return _SID; }
            set { _SID = value; }
        }

        private bool CheckSession(IAPIInterfaceService pirService)
        {
            bool isSession = false;
            if (!String.IsNullOrEmpty(SID))
            {
                isSession = pirService.checkSession(SID);
            }
            if (!isSession)
            {
                string sid = "";
                pirService.getSession(pirExternalService.PIRExternalServiceUserName, pirExternalService.PIRExternalServicePassword, ref sid, "");
                if (!String.IsNullOrEmpty(sid))
                {
                    SID = sid;
                    isSession = true;
                }

            }
            return isSession;

        }

        private Result UpdateSzamlaPIRStatus(ExecParam execParam, string newPIRStatus)
        {
            logger.DebugFormat("UpdateSzamlaPirStatusz");
            EREC_SzamlakService service_szamla = eRecordService.ServiceFactory.GetEREC_SzamlakService();
            logger.DebugFormat("getserviceszamla");
            ExecParam execParam_szamlaUpdate = execParam.Clone();
            execParam_szamlaUpdate.Record_Id = execParam.Record_Id;
            logger.DebugFormat("Felh_Id: [{0}], [{1}] ", execParam.Felhasznalo_Id, execParam_szamlaUpdate.Felhasznalo_Id);
            logger.DebugFormat("LoginUsr_Id: [{0}], [{1}] ", execParam.LoginUser_Id, execParam_szamlaUpdate.LoginUser_Id);
            logger.DebugFormat("Record_Id: [{0}], [{1}] ", execParam_szamlaUpdate.Record_Id, execParam_szamlaUpdate.Record_Id);
            Result szamlaResult = service_szamla.Get(execParam_szamlaUpdate);
            logger.DebugFormat("getutan");
            EREC_Szamlak _EREC_Szamlak = (EREC_Szamlak)szamlaResult.Record;
            if (_EREC_Szamlak != null)
            {
                logger.DebugFormat("szamla: [{0}] ", _EREC_Szamlak.Id);
                logger.DebugFormat("newPirstatusz: [{0}] ", newPIRStatus);
                _EREC_Szamlak.PIRAllapot = newPIRStatus;
                _EREC_Szamlak.Updated.PIRAllapot = true;
                logger.DebugFormat("update elott");
                Result szamlaUpdateResult = service_szamla.Update(execParam, _EREC_Szamlak);
                logger.DebugFormat("update utan");
                logger.DebugFormat("Result: [{0}] ", szamlaUpdateResult.ErrorCode);
                return szamlaUpdateResult;
            } else
            {
                logger.DebugFormat("szamla null");

                return new Result();
            }

        }
       
        private IAPIInterfaceService GetPirInterfaceService(String Config)
        {
            if (!String.IsNullOrEmpty(Config))
            {
                pirExternalService = new PIRExternalService(Config);
            }
            logger.DebugFormat("pirExternalService: url: [{0}], username: [{1}]", pirExternalService.PIRExternalServiceURL, pirExternalService.PIRExternalServiceUserName);

            IAPIInterfaceService pirService = new IAPIInterfaceService(pirExternalService.PIRExternalServiceURL);
            //pirService.Credentials = Utility.GetCacheSetting(pirService.Url, "Windows", "", pirExternalService.PIRExternalServiceUserName, pirExternalService.PIRExternalServicePassword);
            return pirService;
            //return PIRInterfaceService.ServiceFactory.getIAPIInterfaceService();
        }


    public Result PirSzamlaFejFelvitel(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla, HttpContext Context, DataContext dataContext)
        {
          
            Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

            bool isConnectionOpenHere = false;
            bool isTransactionBeginHere = false;

          //  DataSet dataSet = new DataSet();
            Result result = new Result();
            logger.DebugFormat("[{0}] START", MethodBase.GetCurrentMethod().Name);
            logger.DebugFormat("[{0}] PirEdokSzamla.InvoiceID: [{1}]", MethodBase.GetCurrentMethod().Name, pirEdokSzamla.Szamlaszam);
            try
            {
                isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
                isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
                //Contentum.eIntegrator.Service.ServiceFactory sf =
                //      Contentum.eIntegrator.Service.PIRInterfaceService.ServiceFactory;

                logger.DebugFormat("Connect");

                // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
                // Webservice-választás Vevőbesorolás alapján
                IAPIInterfaceService pirInterfaceService = GetPirInterfaceService(pirEdokSzamla.VevoBesorolas);

                logger.DebugFormat("pirInterfaceService");

                if (CheckSession(pirInterfaceService))
                {
                    logger.DebugFormat("Checked");
                    string partnerData;
                    string partnerId = "";
                    string edokAdoszam = "";
                    if (!String.IsNullOrEmpty(pirEdokSzamla.BelfoldiAdoszam))
                    {
                        edokAdoszam = pirEdokSzamla.BelfoldiAdoszam;
                    } else
                    {
                        edokAdoszam = pirEdokSzamla.KozossegiAdoszam;
                    }
                    edokAdoszam = edokAdoszam.Replace("-","");
                    logger.DebugFormat("edokAdoszam: [{0}] ",edokAdoszam);
                    if (!String.IsNullOrEmpty(edokAdoszam))
                    {

                        partnerData = pirInterfaceService.DMSOneGetPartner(SID, edokAdoszam);

                        if (!String.IsNullOrEmpty(partnerData))
                        {
                            logger.DebugFormat("partnerdata: [{0}]", partnerData);
                            try
                            {
                                XmlDocument partnerDataxDoc = new XmlDocument();
                                partnerDataxDoc.LoadXml(partnerData);

                                XmlNode row = partnerDataxDoc.GetElementsByTagName("ROW").Item(0);
                                if (row != null)
                                {
                                    partnerId = row.Attributes["PARTNERKOD"].Value;
                                }
                            }
                            catch (XmlException exc)
                            {
                                logger.DebugFormat("partnerdata load error: [{0}]  [{1}]", exc.Message, exc.InnerException);
                            }

                        }
                    }
                    if (String.IsNullOrEmpty(partnerId))
                    {
                        partnerId = "n/a";
                    }
                    logger.DebugFormat("partnerId: [{0}] ", partnerId);
                    string datapacketForSzamlafej = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                                                    "<DATAPACKET Version=\"2.0\">\n" +
                                                    "    <METADATA>\n" +
                                                    "        <FIELDS>\n" +
                                                    "            <FIELD attrname=\"InvoiceID\" fieldtype=\"string\" width=\"100\"/>\n" +
                                                    "            <FIELD attrname=\"partnerID\" fieldtype=\"string\" width=\"100\"/>\n" +
                                                    "            <FIELD attrname=\"kulsoID\" fieldtype=\"string\" width=\"100\"/>\n" +
                                                    "            <FIELD attrname=\"partnerNev\" fieldtype=\"string\" width=\"100\"/>\n" +
                                                    "            <FIELD attrname=\"irsz\" fieldtype=\"string\" width=\"10\"/>\n" +
                                                    "            <FIELD attrname=\"telepules\" fieldtype=\"string\" width=\"100\"/>\n" +
                                                    "            <FIELD attrname=\"cim\" fieldtype=\"string\" width=\"100\"/>\n" +
                                                    "            <FIELD attrname=\"tipus\" fieldtype=\"string\" width=\"2\"/>\n" +
                                                    "            <FIELD attrname=\"teljdat\" fieldtype=\"date\" WIDTH=\"20\"/>\n" +
                                                    "            <FIELD attrname=\"fizhatido\" fieldtype=\"date\" WIDTH=\"20\"/>\n" +
                                                    "            <FIELD attrname=\"iktatdat\" fieldtype=\"date\" WIDTH=\"20\"/>\n" +
                                                    "            <FIELD attrname=\"ertek\" fieldtype=\"float\" WIDTH=\"20\"/>\n" +
                                                    "            <FIELD attrname=\"szamladat\" fieldtype=\"date\" WIDTH=\"20\"/>\n" +
                                                    "        </FIELDS>\n" +
                                                    "    </METADATA>\n" +
                                                    "    <ROWDATA>\n" +
                                                    "       <ROW RowState=\"4\" InvoiceID=\"" + pirEdokSzamla.Szamlaszam + "\"" +
                                                                " partnerID=\"" + partnerId + "\" kulsoID =\"\" partnerNev=\"" + pirEdokSzamla.EDOKPartnerNev + "\" irsz=\"" + pirEdokSzamla.EDOKPartnerIranyitoszam + "\" telepules=\"" + pirEdokSzamla.EDOKPartnerHelyseg + "\" cim=\"" + pirEdokSzamla.EDOKPartnerCim + "\"" +
                                                                " tipus=\"SZ\" teljdat=\"" + pirEdokSzamla.TeljesitesDatuma + "\" fizhatido=\"" + pirEdokSzamla.FizetesiHatarido + "\"" +
                                                                " iktatdat=\"" + pirEdokSzamla.ErkeztetesDatuma + "\" ertek=\"" + pirEdokSzamla.EllenorzoOsszeg + "\" szamladat=\"" + pirEdokSzamla.BizonylatDatuma + "\"/>\n" +
                                                    "   </ROWDATA>\n" +
                                                    "</DATAPACKET>\n";
                    
                    logger.DebugFormat("DataPacke: {0}",datapacketForSzamlafej);
                    string[] imagedata = null;
                    string ext = "";
                    int errorcode = 0;
                    string errormsg = "";
                    if (pirInterfaceService.DMSOneDataComm(SID, datapacketForSzamlafej, imagedata, ext, ref errorcode, ref errormsg))
                    {
                        logger.DebugFormat("feladas");
                        Result szamlaUpdateResult = UpdateSzamlaPIRStatus(ExecParam, KodTarak.PIR_ALLAPOT.Veglegesitett);
                        logger.DebugFormat("szamlaupdate");
                        if (szamlaUpdateResult.IsError)
                        {
                            logger.DebugFormat("szamlaUpdateResult: [{0}] ", szamlaUpdateResult.ErrorCode);
                            throw new ResultException(szamlaUpdateResult);
                        }
                        else
                        {
                            logger.DebugFormat("szamlaupdate siker");
                            result.ErrorCode = String.Empty;
                            result.ErrorMessage = String.Empty;
                        }
                    }
                    else
                    {
                        logger.DebugFormat("[{0}] ErrorId: [{1}]", MethodBase.GetCurrentMethod().Name, errorcode.ToString());
                        result.ErrorCode = "PIR"+errorcode.ToString();
                        result.ErrorMessage = errormsg;
                        logger.DebugFormat("hiba");
                        Result szamlaUpdateResult = UpdateSzamlaPIRStatus(ExecParam, KodTarak.PIR_ALLAPOT.Fogadott);
                        logger.DebugFormat("szamlaupdate fogadott");
                    }

                }
                else
                {
                    logger.DebugFormat("Bejelentkezési hiba");
                    result.ErrorCode = "PIR1000";
                    result.ErrorMessage = "Ecostat bejelentkezési hiba";
                    Result szamlaUpdateResult = UpdateSzamlaPIRStatus(ExecParam, KodTarak.PIR_ALLAPOT.Fogadott);
                    logger.DebugFormat("szamlaupdate fogadott");
                }
                dataContext.CommitIfRequired(isTransactionBeginHere);

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("exception: [{0}],[{1}]  ", ex.Message, ex.InnerException==null?"": ex.InnerException.Message);
                dataContext.RollbackIfRequired(isTransactionBeginHere);
                result = ResultException.GetResultFromException(ex);
                result.ErrorCode = "PIR" + result.ErrorCode;
                logger.ErrorFormat("[{0}] ErrorId: [{1}], Message: [{2}]", MethodBase.GetCurrentMethod().Name, result.ErrorCode, result.ErrorMessage);
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }
            logger.DebugFormat("end");
            logger.DebugFormat("[{0}] Result: [{1}]", MethodBase.GetCurrentMethod().Name, (!result.IsError).ToString());
            //log.WsEnd(eParam, result);
            return result;
        }


        public Result PirSzamlaFejModositas(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla, HttpContext Context, DataContext dataContext)
        {
            // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
            // Módosításnál megpróbáljuk újraküldeni az adatokat
            return PirSzamlaFejFelvitel(ExecParam, pirEdokSzamla, Context, dataContext);
        }

        public Result PirSzamlaFejSztorno(ExecParam ExecParam, String Szamlaszam, HttpContext Context, DataContext dataContext)
        {
            // CR3412 Számla sztornózás javítás
            logger.DebugFormat("NOT IMPLEMENTED (PirSzamlaFejSztorno)");
            return new Result();
            //throw new Exception();
        }

        public Result PirSzamlaFejAllapot(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla, HttpContext Context, DataContext dataContext)
        {
            Result result_allapot = new Result();
            result_allapot.Record = pirEdokSzamla.PIRAllapot;
            return result_allapot;
        }

        //    public Result PirSzamlaKepFeltoltes(PIRFileContent image, HttpContext Context)
        //    {

        //        // TODO: connection kell???
        //        //ExecParam eParam = ADAdapterTypes.GetExecParam();
        //        //Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(eParam, Context, GetType());

        //        //     bool isConnectionOpenHere = false;

        //        Result result = new Result();
        //        logger.DebugFormat("[{0}] START", MethodBase.GetCurrentMethod().Name);
        //        logger.DebugFormat("[{0}] PirEdokSzamla.InvoiceID: [{1}]", MethodBase.GetCurrentMethod().Name, image.Id);
        //        try
        //        {
        //            if (CheckSession())
        //            {
        //                SoapCaller caller = new SoapCaller(this.pirExternalService.PIRExternalServiceURL);
        //                caller.setRequestEnvelope(getMessage_getSzamlaKepFeltoltes(image));
        //                caller.call();
        //                ReturnStatus responseStatus = new ReturnStatus(caller.SoapResult.Envelope);
        //                if (responseStatus.ReturnValue)
        //                {
        //                    result.ErrorCode = String.Empty;
        //                    result.ErrorMessage = String.Empty;
        //                }
        //                else
        //                {
        //                    logger.DebugFormat("[{0}] ErrorId: [{1}]", MethodBase.GetCurrentMethod().Name, responseStatus.ErrorID.ToString());
        //                    result.ErrorCode = responseStatus.ErrorID.ToString();
        //                    result.ErrorMessage = responseStatus.ErrorMessage;
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            result = ResultException.GetResultFromException(ex);
        //            logger.DebugFormat("[{0}] ErrorId: [{1}]", MethodBase.GetCurrentMethod().Name, result.ErrorCode);

        //        }
        //        logger.DebugFormat("[{0}] Result: [{1}]", MethodBase.GetCurrentMethod().Name, result.IsError.ToString());
        //        //log.WsEnd(eParam, result);
        //        return result;
        //    }
        //}

        /// <summary>
        /// Get the extension from the given filename
        /// </summary>
        /// <param name="fileName">the given filename ie:abc.123.txt</param>
        /// <returns>the extension ie:txt</returns>
        public string GetFileExtension(string fileName)
        {
            
            string ext = string.Empty;
            int fileExtPos = fileName.LastIndexOf(".", StringComparison.Ordinal);
            if (fileExtPos >= 0)
                ext = fileName.Substring(fileExtPos+1, fileName.Length - fileExtPos-1);

            return ext;
        }

        public Result PirSzamlaKepFeltoltes(ExecParam ExecParam, PIRFileContent image, HttpContext Context, DataContext dataContext)
        {

           
            //ExecParam eParam = ADAdapterTypes.GetExecParam();
            Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

            //     bool isConnectionOpenHere = false;

            Result result = new Result();
            logger.DebugFormat("[{0}] START", MethodBase.GetCurrentMethod().Name);
            logger.DebugFormat("[{0}] PirEdokSzamla.InvoiceID: [{1}]", MethodBase.GetCurrentMethod().Name, image.Id);
            try
            {
                // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
                // Webservice-választás Vevőbesorolás alapján
                IAPIInterfaceService pirInterfaceService = GetPirInterfaceService(image.VevoBesorolas);
                if (CheckSession(pirInterfaceService))
                {

                    // BLG_3876
                    string partnerData;
                    string partnerId = "";
                    string edokAdoszam = "";
                    edokAdoszam = image.Adoszam.Replace("-", "");
                    logger.DebugFormat("edokAdoszam: [{0}] ", edokAdoszam);
                    if (!String.IsNullOrEmpty(edokAdoszam))
                    {

                        partnerData = pirInterfaceService.DMSOneGetPartner(SID, edokAdoszam);

                        if (!String.IsNullOrEmpty(partnerData))
                        {
                            logger.DebugFormat("partnerdata: [{0}]", partnerData);
                            try
                            {
                                XmlDocument partnerDataxDoc = new XmlDocument();
                                partnerDataxDoc.LoadXml(partnerData);

                                XmlNode row = partnerDataxDoc.GetElementsByTagName("ROW").Item(0);
                                if (row != null)
                                {
                                    partnerId = row.Attributes["PARTNERKOD"].Value;
                                }
                            }
                            catch (XmlException exc)
                            {
                                logger.DebugFormat("partnerdata load error: [{0}]  [{1}]", exc.Message, exc.InnerException);
                            }

                        }
                    }
                    if (String.IsNullOrEmpty(partnerId))
                    {
                        partnerId = "n/a";
                    }
                    logger.DebugFormat("partnerId: [{0}] ", partnerId);

                    int errorcode = 0;
                    string errormsg = "";
                    string[] images = new string[1];
                    images[0] = System.Convert.ToBase64String(image.Content);
                    logger.DebugFormat(" Content name : [{0}], length: [{1}]", image.FileName,images[0].Length.ToString());

                    // Sikeres végrehajtáskor is false a visszatérési értéke
                    // BLG_3876
                    //pirInterfaceService.DMSOneSzamlahozKep(SID, image.Id, images, GetFileExtension(image.FileName), ref errorcode, ref errormsg);
                    pirInterfaceService.DMSOneSzamlahozKep(SID, image.Id, partnerId, images, GetFileExtension(image.FileName), ref errorcode, ref errormsg);

                    //    SoapCaller caller = new SoapCaller(this.pirExternalService.PIRExternalServiceURL);
                    //    caller.setRequestEnvelope(getMessage_getSzamlaKepFeltoltes(image));
                    //    caller.call();
                    //    ReturnStatus responseStatus = new ReturnStatus(caller.SoapResult.Envelope);
                    //if (responseStatus.ReturnValue)
                    logger.DebugFormat("DMSOneSzamlahozKep Result:  ErrorId: [{1}] ErrorMessage: [{2}]", MethodBase.GetCurrentMethod().Name, errorcode.ToString(), errormsg);
                    if (errorcode != 0)
                    {
                        result.ErrorCode = "PIR"+errorcode.ToString();
                        result.ErrorMessage = errorcode.ToString();
                    }
                    logger.DebugFormat("[{0}] ErrorId: [{1}]", MethodBase.GetCurrentMethod().Name, errorcode.ToString());
                }
                else
                {
                    logger.DebugFormat("Bejelentkezési hiba");
                    result.ErrorCode = "PIR1000";
                    result.ErrorMessage = "Ecostat bejelentkezési hiba";
                }
            }
            catch (Exception ex)
            {
                result = ResultException.GetResultFromException(ex);
                logger.DebugFormat("[{0}] ErrorId: [{1}]", MethodBase.GetCurrentMethod().Name, result.ErrorCode);

            }
            logger.DebugFormat("[{0}] Result: [{1}]", MethodBase.GetCurrentMethod().Name, (!result.IsError).ToString());
            log.WsEnd(ExecParam, result);
            return result;
        }

        public Result PirSzamlaFejAllapotValtozas(ExecParam ExecParam, DateTime datum, HttpContext context, DataContext dataContext)
        {
            Result result_allapot = new Result();

            System.Collections.Generic.Dictionary<string, string> dictPIRAllapot = new System.Collections.Generic.Dictionary<string, string>();
            foreach (DataRow row in result_allapot.Ds.Tables[0].Rows)
            {
                string id = row[0].ToString();
                string pirallapot = row[1].ToString();
                if (!String.IsNullOrEmpty(id) && !dictPIRAllapot.ContainsKey(id))
                {
                    dictPIRAllapot.Add(id, pirallapot);
                }
            }

            #region PIR állapot visszavezetése (EDOK)

            string[] ids = new string[dictPIRAllapot.Count];
            string[] vers = new string[dictPIRAllapot.Count];
            string[] pirallapotok = new string[dictPIRAllapot.Count];

            //string szamlaIds = String.Empty;

            if (dictPIRAllapot.Count > 0)
            {
                string[] externalIds = new string[dictPIRAllapot.Count];
                dictPIRAllapot.Keys.CopyTo(externalIds, 0);

                ExecParam execParam_byExternId = ExecParam.Clone();

                EREC_SzamlakSearch search_byExternId = new EREC_SzamlakSearch(true);
                search_byExternId.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_SzamlakSearch.SzamlaSorszam.Value = Search.GetSqlInnerString(externalIds);
                search_byExternId.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_SzamlakSearch.SzamlaSorszam.Operator = Query.Operators.inner;

                EREC_SzamlakService service_szamla = eRecordService.ServiceFactory.GetEREC_SzamlakService();
                Result result_byExternId = service_szamla.GetAllWithExtension(execParam_byExternId, search_byExternId);

                if (result_byExternId.IsError)
                {
                    throw new ResultException(result_byExternId);
                }

                int j = 0;
                for (int i = 0; i < result_byExternId.Ds.Tables[0].Rows.Count; i++)
                {
                    DataRow row = result_byExternId.Ds.Tables[0].Rows[i];
                    string id = row["Id"].ToString();
                    string externId = row["Id"].ToString();
                    string ver = row["Ver"].ToString();
                    string curPIRAllapot = row["PIRAllapot"].ToString();

                    if (!String.IsNullOrEmpty(id) && !String.IsNullOrEmpty(ver)
                        && !String.IsNullOrEmpty(externId)
                        && dictPIRAllapot.ContainsKey(externId))
                    {
                        if (dictPIRAllapot[externId] != curPIRAllapot)
                        {

                            ids[j] = id;
                            pirallapotok[j] = dictPIRAllapot[id];
                            vers[j] = ver;
                            j++;
                        }
                        else
                        {
                            Logger.Debug(String.Format("PIRAllapot UPDATE allapot azonos: id: {0};external id: {1}; ver: {2}; pirallapot: {3}", id, externId, ver, curPIRAllapot));
                        }
                    }
                    else
                    {
                        Logger.Warn(String.Format("PIRAllapot UPDATE hiba: id: {0};external id: {1}; ver: {2}; pirallapot: {3}", id, externId, ver, curPIRAllapot));
                    }
                }

            }
            #endregion

            string[][] changedArray = new string[3][];
            changedArray[0] = ids;
            changedArray[1] = vers;
            changedArray[2] = pirallapotok;

            result_allapot.Record = changedArray;
            return result_allapot;
        }

        public Result PirSzamlaExportToCsv(ExecParam ExecParam, HttpContext context, DataContext dataContext)
        {
            throw new NotImplementedException();
        }

    }
}