﻿using Contentum.eUtility;
using System.Configuration;
using System.Net;

namespace Contentum.eIntegrator.Service
{
    public static class ASP_DWH_KKSZB_Helper
    {
        #region PROPERTIES
        public static readonly string CONST_KKSZB_AUTH_HEADER = "x-kk-authentication";
        public static readonly string CONST_KKSZB_REQUEST_ID_HEADER = "x-request-id";
        public static readonly string CONST_KKSZB_TENANT_HEADER = "Tenant";

        public enum Enum_ASP_DWH_IFACE_Mode
        {
            KKSZB,
            WS
        }

        public enum Enum_ASP_DWH_APPSETTINGS
        {
            ASP_DWH_IFACE_Mode,
            ASP_DWH_KKSZB_URL,
            ASP_DWH_KKSZB_Token,
            ASP_DWH_KKSZB_Tenant                
        }
        #endregion
        /// <summary>
        /// x‑request‑id - az adott küldés INT_Log tábla bejegyzés ID mezőjének értékét küldjük
        /// </summary>
        /// <param name="metadata"></param>
        public static string AddIntLogToHTTPHeader(UploadDocumentMetaData metadata)
        {
            if (metadata == null)
            {
                Logger.Debug("Missing 'UploadDocumentMetaData' value !");
                return null;
            }
            string ASP_DWH_IFACE_Mode_VALUE = ConfigurationManager.AppSettings.Get(ASP_DWH_KKSZB_Helper.Enum_ASP_DWH_APPSETTINGS.ASP_DWH_IFACE_Mode.ToString());
            if (!string.IsNullOrEmpty(ASP_DWH_IFACE_Mode_VALUE) &&
                ASP_DWH_IFACE_Mode_VALUE.Equals(ASP_DWH_KKSZB_Helper.Enum_ASP_DWH_IFACE_Mode.KKSZB.ToString(), System.StringComparison.CurrentCultureIgnoreCase))
            {
                System.Web.HttpContext.Current.Request.Headers.Add(ASP_DWH_KKSZB_Helper.CONST_KKSZB_REQUEST_ID_HEADER, metadata.feladasAzonosito);
                return metadata.feladasAzonosito;
            }
            return null;
        }
    }
}
