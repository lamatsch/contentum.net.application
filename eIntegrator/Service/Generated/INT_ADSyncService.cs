﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.8009
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.3038.
// 
namespace Contentum.eIntegrator.Service
{
    using Contentum.eBusinessDocuments;

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="INT_ADSyncServiceSoap", Namespace="Contentum.eIntegrator.WebService")]
    public partial class INT_ADSyncService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback ADSyncStartOperationCompleted;
        
        /// <remarks/>
        //public INT_ADSyncService() {
        //    this.Url = "http://localhost:101/eIntegratorWebservice/INT_ADSyncService.asmx";
        //}
        
        /// <remarks/>
        public event ADSyncStartCompletedEventHandler ADSyncStartCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eIntegrator.WebService/ADSyncStart", RequestNamespace="Contentum.eIntegrator.WebService", ResponseNamespace="Contentum.eIntegrator.WebService", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result ADSyncStart(string modulId, string orgId) {
            object[] results = this.Invoke("ADSyncStart", new object[] {
                        modulId,
                        orgId});
            return ((Result)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginADSyncStart(string modulId, string orgId, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("ADSyncStart", new object[] {
                        modulId,
                        orgId}, callback, asyncState);
        }
        
        /// <remarks/>
        public Result EndADSyncStart(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }
        
        /// <remarks/>
        public void ADSyncStartAsync(string modulId, string orgId) {
            this.ADSyncStartAsync(modulId, orgId, null);
        }
        
        /// <remarks/>
        public void ADSyncStartAsync(string modulId, string orgId, object userState) {
            if ((this.ADSyncStartOperationCompleted == null)) {
                this.ADSyncStartOperationCompleted = new System.Threading.SendOrPostCallback(this.OnADSyncStartOperationCompleted);
            }
            this.InvokeAsync("ADSyncStart", new object[] {
                        modulId,
                        orgId}, this.ADSyncStartOperationCompleted, userState);
        }
        
        private void OnADSyncStartOperationCompleted(object arg) {
            if ((this.ADSyncStartCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ADSyncStartCompleted(this, new ADSyncStartCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
    }
    
    /// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    //[System.SerializableAttribute()]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(Namespace="Contentum.eIntegrator.WebService")]
    //public partial class Result {
        
    //    private string errorCodeField;
        
    //    private string errorMessageField;
        
    //    private string errorTypeField;
        
    //    private ErrorDetails errorDetailField;
        
    //    private string uidField;
        
    //    private System.Data.DataSet dsField;
        
    //    private object recordField;
        
    //    private bool isLogExistField;
        
    //    private bool isUpdatedField;
        
    //    private SerializableSqlCommand sqlCommandField;
        
    //    /// <remarks/>
    //    public string ErrorCode {
    //        get {
    //            return this.errorCodeField;
    //        }
    //        set {
    //            this.errorCodeField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public string ErrorMessage {
    //        get {
    //            return this.errorMessageField;
    //        }
    //        set {
    //            this.errorMessageField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public string ErrorType {
    //        get {
    //            return this.errorTypeField;
    //        }
    //        set {
    //            this.errorTypeField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public ErrorDetails ErrorDetail {
    //        get {
    //            return this.errorDetailField;
    //        }
    //        set {
    //            this.errorDetailField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public string Uid {
    //        get {
    //            return this.uidField;
    //        }
    //        set {
    //            this.uidField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public System.Data.DataSet Ds {
    //        get {
    //            return this.dsField;
    //        }
    //        set {
    //            this.dsField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public object Record {
    //        get {
    //            return this.recordField;
    //        }
    //        set {
    //            this.recordField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public bool IsLogExist {
    //        get {
    //            return this.isLogExistField;
    //        }
    //        set {
    //            this.isLogExistField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public bool IsUpdated {
    //        get {
    //            return this.isUpdatedField;
    //        }
    //        set {
    //            this.isUpdatedField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public SerializableSqlCommand SqlCommand {
    //        get {
    //            return this.sqlCommandField;
    //        }
    //        set {
    //            this.sqlCommandField = value;
    //        }
    //    }
    //}
    
    ///// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    //[System.SerializableAttribute()]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(Namespace="Contentum.eIntegrator.WebService")]
    //public partial class ErrorDetails {
        
    //    private int codeField;
        
    //    private string messageField;
        
    //    private string objectTypeField;
        
    //    private string objectIdField;
        
    //    private string columnTypeField;
        
    //    private string columnValueField;
        
    //    /// <remarks/>
    //    public int Code {
    //        get {
    //            return this.codeField;
    //        }
    //        set {
    //            this.codeField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public string Message {
    //        get {
    //            return this.messageField;
    //        }
    //        set {
    //            this.messageField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public string ObjectType {
    //        get {
    //            return this.objectTypeField;
    //        }
    //        set {
    //            this.objectTypeField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public string ObjectId {
    //        get {
    //            return this.objectIdField;
    //        }
    //        set {
    //            this.objectIdField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public string ColumnType {
    //        get {
    //            return this.columnTypeField;
    //        }
    //        set {
    //            this.columnTypeField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public string ColumnValue {
    //        get {
    //            return this.columnValueField;
    //        }
    //        set {
    //            this.columnValueField = value;
    //        }
    //    }
    //}
    
    ///// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    //[System.SerializableAttribute()]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(Namespace="Contentum.eIntegrator.WebService")]
    //public partial class SerializableSqlParams {
        
    //    private string nameField;
        
    //    private string valueField;
        
    //    /// <remarks/>
    //    public string Name {
    //        get {
    //            return this.nameField;
    //        }
    //        set {
    //            this.nameField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public string Value {
    //        get {
    //            return this.valueField;
    //        }
    //        set {
    //            this.valueField = value;
    //        }
    //    }
    //}
    
    ///// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    //[System.SerializableAttribute()]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(Namespace="Contentum.eIntegrator.WebService")]
    //public partial class SerializableSqlCommand {
        
    //    private string commandTextField;
        
    //    private SerializableSqlParams[] parametersField;
        
    //    /// <remarks/>
    //    public string CommandText {
    //        get {
    //            return this.commandTextField;
    //        }
    //        set {
    //            this.commandTextField = value;
    //        }
    //    }
        
    //    /// <remarks/>
    //    public SerializableSqlParams[] Parameters {
    //        get {
    //            return this.parametersField;
    //        }
    //        set {
    //            this.parametersField = value;
    //        }
    //    }
    //}
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void ADSyncStartCompletedEventHandler(object sender, ADSyncStartCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ADSyncStartCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal ADSyncStartCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        ///// <remarks/>
        //public Result Result {
        //    get {
        //        this.RaiseExceptionIfNecessary();
        //        return ((Result)(this.results[0]));
        //    }
        //}
    }
}
