﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.BaseUtility;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Contentum.eIntegrator.Service.Helper
{
    public enum TOMEGES_IKTATAS_FOLYAMAT_PRIORITAS
    {
        Szinkron = 0,
        Hatterszalon = 1,
        HatterszalonVarakozassal = 2,
        Job = 3,
        JobVarakozassal = 4
    }

    public class TomegesIktatasFolyamat
    {
        public int Forras_Azonosito;
        public int Prioritas;
        public string Id;

        public const string Forras_Excel = "Excel";
        public const string Forras_HAIR = "HAIR";
        public const string kcs_TOMEGES_IKTATAS_FOLYAMAT_PRIORITAS = "TOMEGES_IKTATAS_FOLYAMAT_PRIORITAS";

        private readonly ExecParam _globalExecParam;

        public TomegesIktatasFolyamat(ExecParam globalExecParam)
        {
            _globalExecParam = globalExecParam;
        }

        public TomegesIktatasFolyamat(ExecParam globalExecParam, Guid id) : this(globalExecParam)
        {
            Id = id == null ? "" : id.ToString();
        }

        public List<EREC_TomegesIktatasTetelek> GetTetelek(ExecParam execParam, string allapot)
        {
            var tomegesIktatasTetelekService = eRecordService.ServiceFactory.GetEREC_TomegesIktatasTetelekService();
            var tetelekSearch = new EREC_TomegesIktatasTetelekSearch();
            tetelekSearch.Folyamat_Id.Filter(Id);
            tetelekSearch.Allapot.FilterIfNotEmpty(allapot);

            Result tetelekResult = tomegesIktatasTetelekService.GetAll(execParam, tetelekSearch);
            tetelekResult.CheckErr();

            var tetelek = new List<EREC_TomegesIktatasTetelek>();
            foreach (DataRow row in tetelekResult.Ds.Tables[0].Rows)
            {
                var tetel = new EREC_TomegesIktatasTetelek();
                eUtility.Utils.LoadBusinessDocumentFromDataRow(tetel, row);
                tetelek.Add(tetel);
            }

            return tetelek;
        }


        public Result Insert(ExecParam execParam, string forras, int? egyediPrioritas, Result uploadResult)
        {
            var tomegesIktatasService = eRecordService.ServiceFactory.GetEREC_TomegesIktatasFolyamatService();

            EREC_TomegesIktatasFolyamat folyamat = new EREC_TomegesIktatasFolyamat();
            folyamat.Updated.SetValueAll(false);
            folyamat.Base.Updated.SetValueAll(false);
            folyamat.Forras = forras;
            folyamat.Updated.Forras = true;

            Result existingFolyamatok = tomegesIktatasService.GetAll(execParam, new EREC_TomegesIktatasFolyamatSearch());

            if (existingFolyamatok.IsError)
            {
                return existingFolyamatok;
            }

            Forras_Azonosito = 0;

            if (existingFolyamatok.GetCount > 0)
            {
                const string forras_column = "Forras";
                const string forras_azonosito_column = "Forras_Azonosito";
                var folyamatokList = existingFolyamatok.Ds.Tables[0].AsEnumerable();
                if (folyamatokList.Any(x => (x[forras_azonosito_column] != DBNull.Value && !string.IsNullOrEmpty(x[forras_azonosito_column].ToString()))))
                {
                    var list = folyamatokList.Where(x => (x[forras_azonosito_column] != DBNull.Value && 
                    !string.IsNullOrEmpty(x[forras_azonosito_column].ToString()) && x[forras_column].ToString() == forras)).
                     Select(x => int.Parse(x[forras_azonosito_column].ToString()));
                    Forras_Azonosito = list.Any() ? list.Max() + 1 : 1;
                }
            }

            folyamat.Forras_Azonosito = Forras_Azonosito.ToString();
            folyamat.Updated.Forras_Azonosito = true;

            folyamat.Base.Ver = "1";
            folyamat.Base.Updated.Ver = true;

            if (uploadResult != null && !uploadResult.IsError)
            {
                folyamat.RQ_Dokumentum_Id = uploadResult.Uid;
                folyamat.Updated.RQ_Dokumentum_Id = true;
            }

            Prioritas = egyediPrioritas ?? Rendszerparameterek.GetInt(execParam, kcs_TOMEGES_IKTATAS_FOLYAMAT_PRIORITAS, (int)TOMEGES_IKTATAS_FOLYAMAT_PRIORITAS.HatterszalonVarakozassal);
            folyamat.Prioritas = Prioritas.ToString();
            folyamat.Updated.Prioritas = true;

            folyamat.Allapot = KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.InicializalasFolyamatban;
            folyamat.Updated.Allapot = true;

            var result = tomegesIktatasService.Insert(execParam, folyamat);

            Id = result.IsError ? "" : result.Uid;

            return result;
        }

        public Result SetAllapot(string allapot)
        {
            EREC_TomegesIktatasFolyamatService tomegesIktatasService = eRecordService.ServiceFactory.GetEREC_TomegesIktatasFolyamatService();
            var execParam = _globalExecParam.Clone();
            execParam.Record_Id = Id;

            Result folyamatGet = tomegesIktatasService.Get(execParam);
            folyamatGet.CheckErr();

            EREC_TomegesIktatasFolyamat folyamat = folyamatGet.Record as EREC_TomegesIktatasFolyamat;
            folyamat.Updated.SetValueAll(false);
            folyamat.Base.Updated.SetValueAll(false);
            folyamat.Base.Updated.Ver = true;

            folyamat.Allapot = allapot;
            folyamat.Updated.Allapot = true;

            Result folyamatUpdate = tomegesIktatasService.Update(execParam, folyamat);
            folyamatUpdate.CheckErr();
            folyamatUpdate.Record = folyamatGet.Record;

            return folyamatUpdate;
        }
    }
}
