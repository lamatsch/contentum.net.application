﻿using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.BaseUtility;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using Logger = Contentum.eUtility.Logger;
using AlairasokUtility = Contentum.eUtility.AlairasokUtility;

namespace Contentum.eIntegrator.Service.Helper
{
    // refactored from eIntegrator\WebService\App_Code\INT_ASPADO_DocumentService
    public class TomegesIktatasHelper
    {
        public List<IktatoCsoport> IktatoCsoportok;
        public string KuldemenyIktatokonyvId = null;
        public ManualResetEvent CsoportokReady = new ManualResetEvent(false);

        private readonly ExecParam _globalExecParam;
        private IEnumerable<EREC_TomegesIktatasTetelek> _tetelek;

        public TomegesIktatasFolyamat Folyamat { get; private set; }

        public TomegesIktatasHelper(ExecParam globalExecParam, IEnumerable<EREC_TomegesIktatasTetelek> tetelek)
        {
            _globalExecParam = globalExecParam;
            Folyamat = new TomegesIktatasFolyamat(globalExecParam);
            SetTetelek(tetelek);
        }

        public void SetTetelek(IEnumerable<EREC_TomegesIktatasTetelek> tetelek)
        {
            _tetelek = tetelek;
        }

        public KRT_Dokumentumok GetBusinessObjectFromComponents(string fileName, bool isSigned)
        {
            KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();
            // összes mező update-elhetőségét kezdetben letiltani:
            krt_Dokumentumok.Updated.SetValueAll(false);
            krt_Dokumentumok.Base.Updated.SetValueAll(false);

            krt_Dokumentumok.FajlNev = fileName;
            krt_Dokumentumok.Updated.FajlNev = true;

            krt_Dokumentumok.Megnyithato = "1";
            krt_Dokumentumok.Updated.Megnyithato = true;

            krt_Dokumentumok.Olvashato = "1";
            krt_Dokumentumok.Updated.Olvashato = true;

            krt_Dokumentumok.Titkositas = "0";
            krt_Dokumentumok.Updated.Titkositas = true;

            krt_Dokumentumok.ElektronikusAlairas = isSigned ? KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas : String.Empty;
            krt_Dokumentumok.Updated.ElektronikusAlairas = true;

            krt_Dokumentumok.Tipus = "Alairas (Xml)";
            krt_Dokumentumok.Updated.Tipus = true;

            krt_Dokumentumok.ErvKezd = DateTime.Today.ToString();
            krt_Dokumentumok.Updated.ErvKezd = true;

            krt_Dokumentumok.Base.Ver = "1";
            krt_Dokumentumok.Base.Updated.Ver = true;

            return krt_Dokumentumok;
        }

        public Result FileUpload(ExecParam execParam, Stream stream, string fileName, string existingFileId)
        {
            #region Feltöltött file beolvasása byte[]-ba

            stream.Position = 0;
            System.IO.StreamReader sr = new System.IO.StreamReader(stream);
            string s = sr.ReadToEnd();
            byte[] buf = System.Text.ASCIIEncoding.ASCII.GetBytes(s);
            //byte[] buf = fileUpload.FileBytes;

            #endregion Feltöltött file beolvasása byte[]-ba

            DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();

            #region FileUpload
            KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents(fileName, false);
            //string checkinmode = "1";
            //if (TextBoxFileName.Text == "-")
            //{
            //    checkinmode = "2";
            //    krt_Dokumentumok.Id = Request.QueryString.Get(QueryStringVars.Id);
            //}
            //Result result = documentService.DirectUploadAndDocumentInsert(execParam, Contentum.eUtility.Constants.DocumentStoreType.UCM
            //    , SITE_PATH, DOC_LIB_PATH, FOLDER_PATH, fileName, buf, checkinmode, krt_Dokumentumok);

            string ujBejegyzes = String.IsNullOrEmpty(existingFileId) ? "IGEN" : "NEM";


            //iktatoHely = iktatoHely.Substring(0, iktatoHely.IndexOf("/", 0)).Trim();

            String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                       "<iktatokonyv></iktatokonyv>" +
                                                       "<sourceSharePath></sourceSharePath>" +
                                                       "<foszam>{0}</foszam>" +
                                                       "<kivalasztottIratId></kivalasztottIratId>" +
                                                       "<docmetaDokumentumId>{1}</docmetaDokumentumId>" +
                                                       "<megjegyzes></megjegyzes>" +
                                                       "<munkaanyag>{2}</munkaanyag>" +
                                                       "<ujirat>{3}</ujirat>" +
                                                       "<vonalkod></vonalkod>" +
                                                       "<docmetaIratId></docmetaIratId>" +
                                                       "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                       "<ucmiktatokonyv>{5}</ucmiktatokonyv>" +
                                                   "</uploadparameterek>"
                                                   , String.Empty
                                                   , existingFileId
                                                   , "NEM"
                                                   , "IGEN"
                                                   , ujBejegyzes
                                                   , "HK"
                                                   , fileName
                                                   );

            Result result_upload = documentService.UploadFromeRecordWithCTTCheckin
                (
                   execParam
                   , Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                   , fileName
                   , buf
                   , krt_Dokumentumok
                   , uploadXmlStrParams
                );

            return result_upload;


            #endregion FileUpload
        }

        public static Result GetKuldemenyIktatokonyvId(ExecParam execParam)
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();

            EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();
            search.Ev.Filter(DateTime.Now.Year.ToString());
            search.IktatoErkezteto.Filter("E");
            search.ErvKezd.LessOrEqual("getdate()");
            search.ErvVege.GreaterOrEqual("getdate()");
            search.Statusz.Filter("1");

            return service.GetAllWithIktathat(execParam, search);
        }

        public void SetKuldemenyIktatoKonyv()
        {
            var execParam = _globalExecParam.Clone();
            var kuldemenyIktatokonyvIdResult = TomegesIktatasHelper.GetKuldemenyIktatokonyvId(execParam);
            kuldemenyIktatokonyvIdResult.CheckErr();

            if (kuldemenyIktatokonyvIdResult.GetCount < 1)
            {
                throw new Exception("Nem található érkeztetőkönyv");
            }
            else
            {
                KuldemenyIktatokonyvId = kuldemenyIktatokonyvIdResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                //"149FF030-5B0F-E911-80D1-00155D020B3C"; // NMHH_teszt // select * from EREC_IraIktatoKonyvek where IktatoErkezteto='E' and Statusz=1 AND Ev= AND Erv...!
            }
        }

        public void CreateGroups(IEnumerable<IktatoObjektumBase> iktatoDocuments)
        {
            IktatoCsoportok = new List<IktatoCsoport>();

            var tempGroupedIktatoCsoportok = from iktatasokGroup in iktatoDocuments
                                             group iktatasokGroup by new
                                             {
                                                 iktatasokGroup.IktatoKonyvId,
                                                 Foszam = iktatasokGroup.GetFoszam(),
                                                 Irany = iktatasokGroup.GetIratIrany(),
                                                 //ArrivalExists = (iktatasokGroup.Document.Packages != null && iktatasokGroup.Document.Packages.Length > 0 && iktatasokGroup.Document.Packages[0].ArrivalInformation != null && !(string.IsNullOrEmpty(iktatasokGroup.Document.Packages[0].ArrivalInformation.ArrivalNumber)))
                                                 ArrivalExists = (false)
                                             } into iktatasokGroup
                                             select iktatasokGroup;

            var list = tempGroupedIktatoCsoportok.ToList();
            for (int i = 0; i < tempGroupedIktatoCsoportok.Count(); i++)
            {
                var group = list[i];
                var k = group.Key;
                int darabszam = group.Count();
                int iratIrany = k.Irany;

                bool kuldemenyAzonositott = false;

                Guid? erkeztetoszam = null;

                //!!!!!!!!!!!!!!!!!!!!!FAKE!!!!!!!!!!!!!!!!
                if (k.ArrivalExists)
                {
                    try
                    {
                        //erkeztetoszam = new Guid(group.First().Document.Packages[0].ArrivalInformation.ArrivalNumber);
                        erkeztetoszam = new Guid("52976749-8139-4555-9b94-14ad62345766");

                        if (iratIrany == 1) // EnumsDirection.In
                        {
                            kuldemenyAzonositott = true;
                        }
                    }
                    catch { }
                }
                else
                {
                    erkeztetoszam = new Guid(KuldemenyIktatokonyvId);
                }

                Guid iktatoKonyvId = Guid.NewGuid();
                try
                {
                    iktatoKonyvId = new Guid(k.IktatoKonyvId);
                }
                catch { }

                IktatoCsoport csoport = new IktatoCsoport();
                csoport.iktatokonyv_Id = iktatoKonyvId;
                csoport.erkeztetokonyv_Id = erkeztetoszam;

                csoport.foszam = k.Foszam == 0 ? (int?)null : k.Foszam;

                if (k.ArrivalExists)
                {
                    iktatoDocuments.Where(x => x.GetIratIrany() == k.Irany
                                          && x.IktatoKonyvId == k.IktatoKonyvId
                                          && x.GetFoszam() == k.Foszam
                                          //&& new Guid(x.Document.Packages[0].ArrivalInformation.ArrivalNumber) == erkeztetoszam)
                                          && new Guid("52976749-8139-4555-9b94-14ad62345766") == erkeztetoszam)
                        .ToList().ForEach(x => x.CsoportId = i);
                }
                else
                {
                    iktatoDocuments.Where(x => x.GetIratIrany() == k.Irany
                                          && x.IktatoKonyvId == k.IktatoKonyvId
                                          && x.GetFoszam() == k.Foszam
                                          && !k.ArrivalExists)
                        .ToList().ForEach(x => x.CsoportId = i);
                }

                csoport.iratDarabszam = darabszam;
                csoport.iratIrany = iratIrany;
                csoport.kuldemenyAzonositott = kuldemenyAzonositott;
                csoport.Id = i;
                IktatoCsoportok.Add(csoport);
            }
        }

        public void CallTomegesIktatasInitForGroupsSync()
        {
            Logger.Debug("CallTomegesIktatasInitForGroupsSync  START");
            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            foreach (IktatoCsoport csoport in IktatoCsoportok)
            {
                var result = iratokService.TomegesIktatas_INIT(_globalExecParam, csoport.iktatokonyv_Id, csoport.erkeztetokonyv_Id, csoport.foszam, csoport.iratDarabszam, csoport.iratIrany, csoport.kuldemenyAzonositott);
                csoport.SetResult(result);
            }
            Logger.Debug("CallTomegesIktatasInitForGroupsSync  END");
        }

        public void CallTomegesIktatasInitForGroups()
        {
            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            iratokService.TomegesIktatas_INITCompleted += IratokService_TomegesIktatas_INITCompleted;
            foreach (IktatoCsoport csoport in IktatoCsoportok)
            {
                iratokService.TomegesIktatas_INITAsync(_globalExecParam, csoport.iktatokonyv_Id, csoport.erkeztetokonyv_Id, csoport.foszam, csoport.iratDarabszam, csoport.iratIrany, csoport.kuldemenyAzonositott, csoport);
            }
        }

        private void IratokService_TomegesIktatas_INITCompleted(object sender, EREC_IraIratokService.TomegesIktatas_INITCompletedEventArgs e)
        {
            var csoport = (IktatoCsoport)e.UserState;
            csoport.SetResult(e.Result);

            if (IktatoCsoportok.All(x => x.Ready))
            {
                CsoportokReady.Set();
            }
        }

        public void StartExec()
        {
            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            iratokService.TomegesIktatas_EXEC(_globalExecParam, new Guid(Folyamat.Id));
        }

        public void InsertItemsForProcess(IEnumerable<EREC_TomegesIktatasTetelek> tomegesIktatasTetelek)
        {
            var tomegesIktatasTetelekService = eRecordService.ServiceFactory.GetEREC_TomegesIktatasTetelekService();
            foreach (var tetel in tomegesIktatasTetelek)
            {
                if (tetel == null) { continue; }
                tetel.Allapot = KodTarak.TOMEGES_IKTATAS_TETEL_ALLAPOT.Inicializalt;
                tetel.Updated.Allapot = true;

                tetel.Folyamat_Id = Folyamat.Id;
                tetel.Updated.Folyamat_Id = true;

                var result = tomegesIktatasTetelekService.Insert(_globalExecParam, tetel);
                result.CheckErr();

                tetel.Id = result.Uid;
            }
        }

        public List<TomegesIktatasTetelHiba> PostOperations()
        {
            var errors = new List<TomegesIktatasTetelHiba>();
            errors.AddRange(IntezkedesreAtvetel());
            errors.AddRange(Lezaras());
            errors.AddRange(Alairas());
            errors.AddRange(Expedialas());
            return errors;
        }

        public List<TomegesIktatasTetelHiba> Expedialas()
        {
            var errorResults = new List<TomegesIktatasTetelHiba>();

            // TobbIratEgyKuldemenybe=0 tételek
            var kulonalloKuldemenyek = _tetelek.Where(t => t.Allapot == KodTarak.TOMEGES_IKTATAS_TETEL_ALLAPOT.Iktatott && t.Expedialas == "1" && t.TobbIratEgyKuldemenybe != "1").ToList();
            foreach (var tetel in kulonalloKuldemenyek)
            {
                var result = TetelekExpedialasa(new List<EREC_TomegesIktatasTetelek> { tetel });
                tetel.LogResult(result, errorResults);
            }

            // TobbIratEgyKuldemenybe=1 tételek
            var tobbIratEgyKuldemenybeBusinessObjList = _tetelek.Where(t => t.Expedialas == "1" && t.TobbIratEgyKuldemenybe == "1").Select(tetel => TomegesIktatasTetelekBusinessObjects.Deserialize(tetel));

            // csoportosítás azonos partner és cím szerint
            var tobbIratEgyKuldemenybeGroups = tobbIratEgyKuldemenybeBusinessObjList.GroupBy(bo =>
                new { bo.EREC_UgyUgyiratok.Partner_Id_Ugyindito, bo.EREC_UgyUgyiratok.Cim_Id_Ugyindito },
                (key, group) => new
                {
                    key.Partner_Id_Ugyindito,
                    key.Cim_Id_Ugyindito,
                    Items = group
                });

            foreach (var group in tobbIratEgyKuldemenybeGroups)
            {
                var kuldemenyTetelek = group.Items.Select(item => item.Tetel).ToList();
                var result = TetelekExpedialasa(kuldemenyTetelek);
                if (result.IsError)
                {
                    // csak egyszer logoljuk ezt a hibát, ne tételenként
                    result.LogError(null, errorResults);
                    // a csoportban levő összes tételre állítsuk be a hibát, logolás nélkül
                    kuldemenyTetelek.ForEach(tetel => tetel.SetError(result.ErrorMessage, null));
                }
            }

            errorResults.ForEach(e => e.TomegesIktatasMuvelet = TomegesIktatasMuvelet.Expedialas);
            return errorResults;
        }

        private Result TetelekExpedialasa(IEnumerable<EREC_TomegesIktatasTetelek> tetelek)
        {
            Result resultExpedialas = new Result();

            var execParam = _globalExecParam.Clone();

            using (var iratPeldanyService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService())
            {
                iratPeldanyService.Timeout = 1800000;
                EREC_PldIratPeldanyok iratPeldany = null;
                var iratPeldanyok = new List<string>();
                var kuldemenyAutomatikusAtadasaCsoportId = "";
                foreach (var tetel in tetelek)
                {
                    // az első megadott KuldemenyAtadas-t használjuk az azonos címmel rendelkező iratpéldányoknál
                    if (String.IsNullOrEmpty(kuldemenyAutomatikusAtadasaCsoportId) && !String.IsNullOrEmpty(tetel.KuldemenyAtadas))
                    {
                        kuldemenyAutomatikusAtadasaCsoportId = tetel.KuldemenyAtadas;
                    }

                    // iratpéldány template az expediáláshoz (KuldesMod, Partner_Id_Cimzett, NevSTR_Cimzett, Cim_id_Cimzett, CimSTR_Cimzett)
                    if (iratPeldany == null || String.IsNullOrEmpty(iratPeldany.Cim_id_Cimzett))
                    {
                        var bo = TomegesIktatasTetelekBusinessObjects.Deserialize(tetel);
                        if (bo != null)
                        {
                            iratPeldany = bo.EREC_PldIratPeldanyok;
                        }
                    }

                    // első iratpéldány
                    var res = iratPeldanyService.GetElsoIratPeldanyByIraIrat(execParam, tetel.Irat_Id);
                    if (!res.IsError && res.Record is EREC_PldIratPeldanyok)
                    {
                        var iratPeldanyId = ((EREC_PldIratPeldanyok)res.Record).Id;
                        iratPeldanyok.Add(iratPeldanyId);
                    }
                    else
                    {
                        tetel.LogResult(res, null);
                        continue;
                    }
                }

                if (iratPeldany != null)
                {
                    resultExpedialas = iratPeldanyService.Expedialas(execParam, iratPeldanyok.ToArray(), "", iratPeldany, null);
                    if (resultExpedialas.IsError)
                    {
                        return resultExpedialas;
                    }

                    // BUG_6058: küldemény automatikus átadása
                    var resultAtadas = KuldemenyAtadasa(resultExpedialas.Uid, kuldemenyAutomatikusAtadasaCsoportId);
                    if (resultAtadas.IsError)
                    {
                        return resultAtadas;
                    }
                }
            }
            return resultExpedialas;
        }

        private Result KuldemenyAtadasa(string kuldemenyId, string kuldemenyAutomatikusAtadasaCsoportId)
        {
            Logger.Debug(String.Format("kuldemenyId={0} kuldemenyAutomatikusAtadasaCsoportId={1}", kuldemenyId, kuldemenyAutomatikusAtadasaCsoportId));

            if (!String.IsNullOrEmpty(kuldemenyAutomatikusAtadasaCsoportId) && !String.IsNullOrEmpty(kuldemenyId))
            {
                using (var kuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService())
                {
                    var execParam = _globalExecParam.Clone();
                    return kuldemenyekService.Atadas_Tomeges(execParam, new string[] { kuldemenyId }, kuldemenyAutomatikusAtadasaCsoportId, null);
                }
            }
            return new Result();
        }

        public Result Elintezes(EREC_IraIratok irat)
        {
            var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam elintezesExecParam = _globalExecParam.Clone();
            elintezesExecParam.Record_Id = irat.Id;
            irat.Updated.SetValueAll(false);
            irat.Base.Updated.SetValueAll(false);
            irat.Base.Updated.Ver = true;

            return iratokService.Elintezes(elintezesExecParam, irat, null);
        }

        public string GetAlairoSzabalyId(string alairoId, string iratId, string alirasMod)
        {
            ExecParam execParam_alairoSzabalyok = _globalExecParam.Clone();
            AlairasokService alairoSzabalyok = eRecordService.ServiceFactory.GetAlairasokService();

            Result alairoSzabalyokResult = alairoSzabalyok.GetAll_PKI_IratAlairoSzabalyok(execParam_alairoSzabalyok, alairoId, null, "Irat", iratId, "IratJovahagyas", null, KodTarak.ALAIRO_SZEREP.Kiadmanyozo);
            alairoSzabalyokResult.CheckErr("GetAll_PKI_IratAlairoSzabalyok", execParam_alairoSzabalyok);

            foreach (DataRow row in alairoSzabalyokResult.Ds.Tables[0].Rows)
            {
                var mod = row["AlairasMod"];
                var nev = row["AlairasNev"];
                if (((mod != DBNull.Value && mod.ToString() == alirasMod) // HAIR
                    || (nev != DBNull.Value && nev.ToString() == alirasMod) // Excel
                    )
                    && row["AlairasSzabaly_Id"] != DBNull.Value)
                {
                    return row["AlairasSzabaly_Id"].ToString();
                }
            }

            return null;
        }

        private EREC_IraIratok GetIrat(string id)
        {
            EREC_IraIratokService iktatasIratService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam eParam = _globalExecParam.Clone();
            eParam.Record_Id = id;
            Result iratResult = iktatasIratService.Get(eParam);
            iratResult.CheckErr("GetIrat", eParam);

            return iratResult.Record as EREC_IraIratok;
        }

        private List<TomegesIktatasTetelHiba> IntezkedesreAtvetel()
        {
            Logger.Debug("TomegesIktatas_Post_Operations- Intézkedésre átvétel START");
            var errorResults = new List<TomegesIktatasTetelHiba>();

            if (Iratok.IsIratIntezkedesreAtvetelElerheto(_globalExecParam))
            {
                var intezkedesreAtvehetoTetelek = _tetelek.ToList();

                var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                var xp = _globalExecParam.Clone();

                for (int i = 0; i < intezkedesreAtvehetoTetelek.Count(); i++)
                {
                    var tetel = intezkedesreAtvehetoTetelek[i];
                    try
                    {
                        if (!String.IsNullOrEmpty(tetel.Irat_Id))
                        {
                            var result = iratokService.AtvetelIntezkedesre(xp, new string[] { tetel.Irat_Id }, "0", "");
                            tetel.LogResult(result, errorResults);
                        }
                    }
                    catch (eUtility.ResultException rx)
                    {
                        tetel.LogResult(rx.GetResult(), errorResults);
                    }
                    catch (Exception ex)
                    {
                        tetel.SetError("  Elintézés hiba:", ex);
                    }
                }
            }
            Logger.Debug("TomegesIktatas_Post_Operations- Intézkedésre átvétel END");

            errorResults.ForEach(e => e.TomegesIktatasMuvelet = TomegesIktatasMuvelet.IntezkedesreAtvetel);
            return errorResults;
        }

        private List<TomegesIktatasTetelHiba> Lezaras()
        {
            Logger.Debug("TomegesIktatas_Post_Operations- Lezárás START");
            var errorResults = new List<TomegesIktatasTetelHiba>();

            var lezarhatoTetelek = _tetelek.Where(x => x.Lezarhato == "1").ToList();

            for (int i = 0; i < lezarhatoTetelek.Count(); i++)
            {
                var tetel = lezarhatoTetelek[i];
                try
                {
                    var irat = GetIrat(tetel.Irat_Id);
                    if (irat != null)
                    {
                        var result = Elintezes(irat);
                        tetel.LogResult(result, errorResults);
                    }
                }
                catch (eUtility.ResultException rx)
                {
                    tetel.LogResult(rx.GetResult(), errorResults);
                }
                catch (Exception ex)
                {
                    tetel.SetError("  Elintézés hiba:", ex);
                }
            }
            Logger.Debug("TomegesIktatas_Post_Operations- Lezárás END");

            errorResults.ForEach(e => e.TomegesIktatasMuvelet = TomegesIktatasMuvelet.Lezaras);
            return errorResults;
        }

        private List<TomegesIktatasTetelHiba> Alairas()
        {
            Logger.Debug("TomegesIktatas_Post_Operations- Aláírás START");
            var errorResults = new List<TomegesIktatasTetelHiba>();

            var alairhatoTetelek = _tetelek.Where(x => !string.IsNullOrEmpty(x.EREC_IratAlairok)).ToList();
            
            for (int i = 0; i < alairhatoTetelek.Count(); i++)
            {
                var tetel = alairhatoTetelek[i];
                try
                {
                    EREC_IratAlairok alairo = JsonConvert.DeserializeObject<EREC_IratAlairok>(tetel.EREC_IratAlairok);
                    if (alairo != null)
                    {
                        alairo.Obj_Id = tetel.Irat_Id;
                        alairo.Updated.Obj_Id = true;

                        Alairas(tetel.Irat_Id, alairo);
                    }
                }
                catch (eUtility.ResultException rx)
                {
                    tetel.LogResult(rx.GetResult(), errorResults);
                }
                catch (Exception ex)
                {
                    var res = new Result() { ErrorCode = "ERR", ErrorMessage = "  Aláírás hiba:" + ex.Message };
                    tetel.LogResult(res, errorResults);
                }
            }
            Logger.Debug("TomegesIktatas_Post_Operations- Aláírás END");

            errorResults.ForEach(e => e.TomegesIktatasMuvelet = TomegesIktatasMuvelet.Alairas);
            return errorResults;
        }

        public void Alairas(string iratID, EREC_IratAlairok iratAlairo)
        {
            string adminUtolagosIratAlairoSzabalyId = GetAlairoSzabalyId(iratAlairo.FelhasznaloCsoport_Id_Alairo, iratID, iratAlairo.AlairasMod);

            if (!string.IsNullOrEmpty(adminUtolagosIratAlairoSzabalyId))
            {
                iratAlairo.AlairasSzabaly_Id = adminUtolagosIratAlairoSzabalyId;
                iratAlairo.Updated.AlairasSzabaly_Id = true;
            }
            else
            {
                throw new eUtility.ResultException("-6", "Nem található aláírószabály.");
            }

            InsertAlairas(iratID, iratAlairo);
        }

        public void InsertAlairas(string iratID, EREC_IratAlairok iratAlairok)
        {
            ExecParam execParam_iratAlairas = _globalExecParam.Clone();

            string err;
            var alairo = new Alairo(execParam_iratAlairas, iratID);
            if (alairo.Felveheto(iratAlairok.AlairasMod, out err))
            {
                EREC_IratAlairokService service_iratAlairok = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                AlairasokUtility.SetNoteTomeges(iratAlairok);
                Result result_iratAlairas = service_iratAlairok.Insert(execParam_iratAlairas, iratAlairok);
                AlairasokUtility.UnSetNoteTomeges(iratAlairok);
                result_iratAlairas.CheckErr("InsertAlairas", execParam_iratAlairas);
            }
            else
            {
                throw new eUtility.ResultException("-6", err);
            }
        }
    }

    public class IktatoCsoport
    {
        public int Id;
        public bool Ready;
        public Guid iktatokonyv_Id;
        public Guid? erkeztetokonyv_Id;
        public int? foszam;
        public int iratDarabszam;
        public int iratIrany;
        public bool kuldemenyAzonositott;
        public object Tag;

        public string ErrorMessage;
        public Guid[] kapottIratId;
        public Guid?[] kapottUgyiratId;
        public Guid?[] kapottKuldemenyId;
        public string[] kapottAlszam;
        public int[] kapottFoszam;
        public string[] kapottAzonosito;

        public void SetResult(Result result)
        {
            Ready = true;

            if (result.IsError)
            {
                ErrorMessage = result.ErrorMessage;
            }
            else
            {
                kapottIratId = new Guid[iratDarabszam];
                kapottAlszam = new string[iratDarabszam];
                kapottFoszam = new int[iratDarabszam];
                kapottAzonosito = new string[iratDarabszam];
                kapottUgyiratId = new Guid?[iratDarabszam];
                kapottKuldemenyId = new Guid?[iratDarabszam];

                for (int i = 0; i < result.GetCount; i++)
                {
                    var row = result.Ds.Tables[0].Rows[i];
                    kapottIratId[i] = (Guid)row["Id"];
                    kapottAlszam[i] = row["Alszam"].ToString();
                    kapottFoszam[i] = int.Parse(row["Foszam"].ToString());
                    kapottAzonosito[i] = row["Azonosito"].ToString();
                    kapottUgyiratId[i] = row["Ugyirat_Id"] == DBNull.Value ? null : (Guid?)row["Ugyirat_Id"];
                    kapottKuldemenyId[i] = row["Kuldemenyek_Id"] == DBNull.Value ? null : (Guid?)row["Kuldemenyek_Id"];
                }
            }
        }
    }

    public abstract class IktatoObjektumBase
    {
        public int CsoportId;
        public string IktatoKonyvId;

        public abstract int GetFoszam();

        public abstract int GetIratIrany();
    }

    public static class ResultExtension
    {
        public static void CheckErr(this Result result)
        {
            CheckErr(result, "", null);
        }

        public static void CheckErr(this Result result, string msg, ExecParam execParam)
        {
            LogError(result, msg, execParam, null, null);
        }

        public static bool LogError(this Result result, EREC_TomegesIktatasTetelek tetel, List<TomegesIktatasTetelHiba> results)
        {
            return LogError(result, "", null, tetel, results);
        }

        /// <summary>
        /// Hiba logolása, és gyűjtése az errorResults-ban megadott hibalistába (ha null, akkor ehelyett ResultException-t dob)
        /// </summary>
        /// <param name="result"></param>
        /// <param name="msg"></param>
        /// <param name="execParam"></param>
        /// <param name="errorResults">hibalista, ha null, akkor a result alapján ResultException-t dob</param>
        /// <returns></returns>
        public static bool LogError(this Result result, string msg, ExecParam execParam, EREC_TomegesIktatasTetelek tetel, List<TomegesIktatasTetelHiba> errorResults)
        {
            if (result != null && result.IsError)
            {
                if (!String.IsNullOrEmpty(msg))
                {
                    Logger.Error(msg, execParam, result);
                }

                if (errorResults != null)
                {
                    errorResults.Add(new TomegesIktatasTetelHiba(tetel, result));
                    return true;
                }
                else
                {
                    throw new eUtility.ResultException(result);
                }
            }
            return false;
        }
    }
}
