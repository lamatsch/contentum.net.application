﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;

namespace Contentum.eIntegrator.Service.Helper
{
    public static class TomegesIktatasTetelExtension
    {
        public static void SetError(this EREC_TomegesIktatasTetelek tetel, string message, Exception exception)
        {
            if (message.StartsWith("[") && message.EndsWith("]"))
            {
                message += " " + Contentum.eUtility.ResultError.ErrorMessageCodeToDetailedText(message);
            }

            // NOTE: ez csak az iktatásnál állítódjon be
            //tetel.Allapot = KodTarak.TOMEGES_IKTATAS_TETEL_ALLAPOT.Hibas;
            //tetel.Updated.Allapot = true;

            if (!String.IsNullOrEmpty(tetel.Hiba))
            {
                tetel.Hiba += " ";
            }
            tetel.Hiba += message + (exception == null ? "" : exception.Message);
            tetel.Updated.Hiba = true;
        }

        public static void LogResult(this EREC_TomegesIktatasTetelek tetel, Result result, List<TomegesIktatasTetelHiba> errors)
        {
            if (result.LogError(tetel, errors))
            {
                tetel.SetError(result.ErrorMessage, null);
            }
        }
    }
}
