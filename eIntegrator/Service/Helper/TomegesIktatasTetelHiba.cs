﻿using Contentum.eBusinessDocuments;

namespace Contentum.eIntegrator.Service.Helper
{
    public class TomegesIktatasTetelHiba
    {
        public EREC_TomegesIktatasTetelek Tetel { get; private set; }
        public Result Result { get; private set; }

        public TomegesIktatasMuvelet TomegesIktatasMuvelet { get; set; }

        public TomegesIktatasTetelHiba(EREC_TomegesIktatasTetelek tetel, Result result)
        {
            Tetel = tetel;
            Result = result;
        }
    }

    public enum TomegesIktatasMuvelet
    {
        IntezkedesreAtvetel,
        Lezaras,
        Alairas,
        Expedialas
    }
}
