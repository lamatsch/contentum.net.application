﻿using Contentum.eBusinessDocuments;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eIntegrator.Service.Helper
{
    public class TomegesIktatasTetelekBusinessObjects
    {
        public EREC_TomegesIktatasTetelek Tetel { get; set; }
        public ExecParam ExecParam { get; set; }
        public EREC_UgyUgyiratok EREC_UgyUgyiratok { get; set; }
        public EREC_UgyUgyiratdarabok EREC_UgyUgyiratdarabok { get; set; }
        public EREC_IraIratok EREC_IraIratok { get; set; }
        public EREC_PldIratPeldanyok EREC_PldIratPeldanyok { get; set; }
        public EREC_HataridosFeladatok EREC_HataridosFeladatok { get; set; }
        public EREC_KuldKuldemenyek EREC_KuldKuldemenyek { get; set; }
        public IktatasiParameterek IktatasiParameterek { get; set; }
        public ErkeztetesParameterek ErkeztetesParameterek { get; set; }

        public TomegesIktatasTetelekBusinessObjects(EREC_TomegesIktatasTetelek tetel)
        {
            this.Tetel = tetel;
        }

        public void DeserializeBusinessObects()
        {
            if (!String.IsNullOrEmpty(this.Tetel.ExecParam)) { this.ExecParam = JsonConvert.DeserializeObject<ExecParam>(this.Tetel.ExecParam.Replace("\"CallingChain\":[],", "")); }
            if (!String.IsNullOrEmpty(this.Tetel.EREC_UgyUgyiratok)) { this.EREC_UgyUgyiratok = JsonConvert.DeserializeObject<EREC_UgyUgyiratok>(this.Tetel.EREC_UgyUgyiratok); }
            if (!String.IsNullOrEmpty(this.Tetel.EREC_UgyUgyiratdarabok)) { this.EREC_UgyUgyiratdarabok = JsonConvert.DeserializeObject<EREC_UgyUgyiratdarabok>(this.Tetel.EREC_UgyUgyiratdarabok); }
            if (!String.IsNullOrEmpty(this.Tetel.EREC_IraIratok)) { this.EREC_IraIratok = JsonConvert.DeserializeObject<EREC_IraIratok>(this.Tetel.EREC_IraIratok); }
            if (!String.IsNullOrEmpty(this.Tetel.EREC_PldIratPeldanyok)) { this.EREC_PldIratPeldanyok = JsonConvert.DeserializeObject<EREC_PldIratPeldanyok>(this.Tetel.EREC_PldIratPeldanyok); }
            if (!String.IsNullOrEmpty(this.Tetel.EREC_HataridosFeladatok)) { this.EREC_HataridosFeladatok = JsonConvert.DeserializeObject<EREC_HataridosFeladatok>(this.Tetel.EREC_HataridosFeladatok); }
            if (!String.IsNullOrEmpty(this.Tetel.EREC_KuldKuldemenyek)) { this.EREC_KuldKuldemenyek = JsonConvert.DeserializeObject<EREC_KuldKuldemenyek>(this.Tetel.EREC_KuldKuldemenyek); }
            if (!String.IsNullOrEmpty(this.Tetel.IktatasiParameterek)) { this.IktatasiParameterek = JsonConvert.DeserializeObject<IktatasiParameterek>(this.Tetel.IktatasiParameterek); }
            if (!String.IsNullOrEmpty(this.Tetel.ErkeztetesParameterek)) { this.ErkeztetesParameterek = JsonConvert.DeserializeObject<ErkeztetesParameterek>(this.Tetel.ErkeztetesParameterek); }
        }

        public static TomegesIktatasTetelekBusinessObjects Deserialize(EREC_TomegesIktatasTetelek tetel)
        {
            var res = new TomegesIktatasTetelekBusinessObjects(tetel);
            res.DeserializeBusinessObects();
            return res;
        }
    }
}
