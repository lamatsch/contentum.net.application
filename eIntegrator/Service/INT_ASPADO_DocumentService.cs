﻿namespace Contentum.eIntegrator.Service
{
    using Contentum.eBusinessDocuments;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "INT_ASPADO_DocumentServiceSoap", Namespace = "Contentum.eIntegrator.WebService")]

    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class INT_ASPADO_DocumentService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        public INT_ASPADO_DocumentService(string ServiceUrl)
        {
            this.Url = ServiceUrl;
        }
    }

}