﻿namespace Contentum.eIntegrator.Service
{
    using Contentum.eBusinessDocuments;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "INT_HAIRServiceSoap", Namespace = "Contentum.eIntegrator.WebService")]

    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class INT_HAIRService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        public INT_HAIRService(string ServiceUrl)
        {
            this.Url = ServiceUrl;
        }
    }

}