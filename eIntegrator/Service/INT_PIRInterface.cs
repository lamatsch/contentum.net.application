﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using Contentum.eIntegrator;
using System;
using System.Web;
using System.Web.Services;

// CR3234: PIR interface
//PIR Számlázás során megvalósítandó interface-k
//[WebService(Namespace = "Contentum.eIntegrator.WebService")]
//[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
namespace Contentum.eIntegrator.Service
{
    public interface INT_PIRInterface
    {
        Result PirSzamlaFejFelvitel(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla, HttpContext context, DataContext dataContext);
        Result PirSzamlaFejModositas(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla, HttpContext context, DataContext dataContext);
        Result PirSzamlaFejSztorno(ExecParam ExecParam, String Szamlaszam, HttpContext context, DataContext dataContext);
        // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
        // Állapot lekérdezésnél is megy a teljes osztály
        Result PirSzamlaFejAllapot(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla, HttpContext context, DataContext dataContext);
        Result PirSzamlaKepFeltoltes(ExecParam ExecParam, PIRFileContent image, HttpContext context, DataContext dataContext);

        Result PirSzamlaFejAllapotValtozas(ExecParam ExecParam, DateTime datum, HttpContext context, DataContext dataContext);
        /// <summary>
        /// Számlák exportálása csv formátumban,
        /// </summary>
        /// <param name="ExecParam">ExecParam objektum</param>
        /// <param name="context">HttpContext</param>
        /// <param name="dataContext">DataContext</param>
        /// <returns></returns>
        Result PirSzamlaExportToCsv(ExecParam ExecParam, HttpContext context, DataContext dataContext);
    }
}