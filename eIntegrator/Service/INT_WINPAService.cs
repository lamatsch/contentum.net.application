﻿namespace Contentum.eIntegrator.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "INT_WINPAServiceSoap", Namespace = "Contentum.eIntegrator.WebService")]

    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class INT_WINPAService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        public INT_WINPAService(string ServiceUrl)
        {
            this.Url = ServiceUrl;
        }
        
        #region KuldemenyekFromWinpa
        private System.Threading.SendOrPostCallback KuldemenyekFromWinpaOperationCompleted;
        /// <remarks/>
        public event KuldemenyekFromWinpaCompletedEventHandler KuldemenyekFromWinpaCompleted;
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eIntegrator.WebService/KuldemenyekFromWinpa", RequestNamespace = "Contentum.eIntegrator.WebService", ResponseNamespace = "Contentum.eIntegrator.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result KuldemenyekFromWinpa(/*ExecParam ExecParam*/)
        {
            object[] results = this.Invoke("KuldemenyekFromWinpa", new object[] { 
                        });
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginKuldemenyekFromWinpa(/*ExecParam ExecParam,*/ System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("KuldemenyekFromWinpa", new object[] {
                        }, callback, asyncState);
        }

        /// <remarks/>
        public Result EndKuldemenyekFromWinpa(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void KuldemenyekFromWinpaAsync(/*ExecParam ExecParam*/)
        {
            this.KuldemenyekFromWinpaAsync(/*ExecParam,*/ null);
        }

        /// <remarks/>
        public void KuldemenyekFromWinpaAsync(/*ExecParam ExecParam,*/ object userState)
        {
            if ((this.KuldemenyekFromWinpaOperationCompleted == null))
            {
                this.KuldemenyekFromWinpaOperationCompleted = new System.Threading.SendOrPostCallback(this.OnKuldemenyekFromWinpaOperationCompleted);
            }
            this.InvokeAsync("KuldemenyekFromWinpa", new object[] {
                        }, this.KuldemenyekFromWinpaOperationCompleted, userState);
        }

        private void OnKuldemenyekFromWinpaOperationCompleted(object arg)
        {
            if ((this.KuldemenyekFromWinpaCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.KuldemenyekFromWinpaCompleted(this, new KuldemenyekFromWinpaCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void KuldemenyekFromWinpaCompletedEventHandler(object sender, KuldemenyekFromWinpaCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class KuldemenyekFromWinpaCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal KuldemenyekFromWinpaCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion KuldemenyekFromwinpa

        #region KuldemenyekTowinpa
        private System.Threading.SendOrPostCallback KuldemenyekToWinpaOperationCompleted;
        /// <remarks/>
        public event KuldemenyekToWinpaCompletedEventHandler KuldemenyekToWinpaCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eIntegrator.WebService/KuldemenyekToWinpa", RequestNamespace = "Contentum.eIntegrator.WebService", ResponseNamespace = "Contentum.eIntegrator.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result KuldemenyekToWinpa(/*ExecParam ExecParam*/)
        {
            object[] results = this.Invoke("KuldemenyekToWinpa", new object[] {
                        });
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginKuldemenyekToWinpa(/*ExecParam ExecParam*/ System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("KuldemenyekToWinpa", new object[] {
                        }, callback, asyncState);
        }

        /// <remarks/>
        public Result EndKuldemenyekToWinpa(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void KuldemenyekToWinpaAsync(/*ExecParam ExecParam*/)
        {
            this.KuldemenyekToWinpaAsync(null);
        }

        /// <remarks/>
        public void KuldemenyekToWinpaAsync(/*ExecParam ExecParam,*/ object userState)
        {
            if ((this.KuldemenyekToWinpaOperationCompleted == null))
            {
                this.KuldemenyekToWinpaOperationCompleted = new System.Threading.SendOrPostCallback(this.OnKuldemenyekToWinpaOperationCompleted);
            }
            this.InvokeAsync("KuldemenyekToWinpa", new object[] {
                        }, this.KuldemenyekToWinpaOperationCompleted, userState);
        }

        private void OnKuldemenyekToWinpaOperationCompleted(object arg)
        {
            if ((this.KuldemenyekToWinpaCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.KuldemenyekToWinpaCompleted(this, new KuldemenyekToWinpaCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void KuldemenyekToWinpaCompletedEventHandler(object sender, KuldemenyekToWinpaCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class KuldemenyekToWinpaCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal KuldemenyekToWinpaCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion KuldemenyekTowinpa
        
    }

}