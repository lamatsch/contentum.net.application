﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Data;
using Contentum.eUtility;
using System.Configuration;
using System.Web.Configuration;

namespace Contentum.eIntegrator.Service
{
    public class PIRInterfaceService
    {
      

        public static Contentum.eIntegrator.Service.ServiceFactory GetServiceFactory(BusinessService BS)
        {
            return GetIAPIInterfaceServiceFactory(BS);
        }

        public static Contentum.eIntegrator.Service.ServiceFactory ServiceFactory
        {
            get { return GetIAPIInterfaceServiceFactory(); }
        }

        private static Contentum.eIntegrator.Service.ServiceFactory GetIAPIInterfaceServiceFactory()
        {
            BusinessService BS = new BusinessService();
            BS.Type = ConfigurationManager.AppSettings.Get("PIRServiceBusinessServiceType");
            BS.Url = ConfigurationManager.AppSettings.Get("PIRServiceBusinessServiceUrl");
            BS.Authentication = ConfigurationManager.AppSettings.Get("PIRServiceBusinessServiceAuthentication");
            BS.UserDomain = ConfigurationManager.AppSettings.Get("PIRServiceBusinessServiceUserDomain");
            BS.UserName = ConfigurationManager.AppSettings.Get("PIRServiceBusinessServiceUserName");
            BS.Password = ConfigurationManager.AppSettings.Get("PIRServiceBusinessServicePassword");
            return GetIAPIInterfaceServiceFactory(BS);
        }

        private static Contentum.eIntegrator.Service.ServiceFactory GetIAPIInterfaceServiceFactory(BusinessService BS)
        {
            Contentum.eIntegrator.Service.ServiceFactory sc = new Contentum.eIntegrator.Service.ServiceFactory();
            sc.BusinessServiceType = BS.Type;
            sc.BusinessServiceUrl = BS.Url;
            sc.BusinessServiceAuthentication = BS.Authentication;
            sc.BusinessServiceUserDomain = BS.UserDomain;
            sc.BusinessServiceUserName = BS.UserName;
            sc.BusinessServicePassword = BS.Password;
            return sc;
        }

        public static string GetFelhasznaloNevById(string Felhasznalo_Id, Page page)
        {
            return FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(Felhasznalo_Id, page);
        }
    }
}
