﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System.Web;
using System.Reflection;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;

namespace Contentum.eIntegrator.Service
{
    // TODO:Áthelyezni
    public class ProcusysExportObj
    {
        public string SzamlaId { get; set; }
        public string ErkeztetetesiAzonosito { get; set; }
        public string SzallitoSzamlaSorszam { get; set; }        
        public DateTime BeerkezesDatuma { get; set; }

        public string SzallitoMegnevezes { get; set; }
        public string SzallitoCime { get; set; }
        
        public string Szamla { get { return "számla"; } private set { } }

        public string ToCsvRow()
        {
            return this.ToCsvRow("\"{0}\"", ",", "yyyyMMdd",true);
        }

        public string ToCsvRow(string escapeCharacter, string dividerCharacter, string datetimeString,bool newLine)
        {        
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat(escapeCharacter, this.ErkeztetetesiAzonosito).Append(dividerCharacter).AppendFormat(escapeCharacter, this.SzallitoSzamlaSorszam)
               .Append(dividerCharacter).AppendFormat(escapeCharacter, this.BeerkezesDatuma.ToString(datetimeString))
               .Append(dividerCharacter).AppendFormat(escapeCharacter, this.SzallitoMegnevezes).Append(dividerCharacter).AppendFormat(escapeCharacter, this.SzallitoCime)
               .Append(dividerCharacter).AppendFormat(escapeCharacter, Szamla);

            if (newLine)
            sb.Append(Environment.NewLine);                    

            return sb.ToString();
        }

    }
    public class Procusys : INT_PIRInterface
    {
        #region Varible declaration

        private static readonly ILog logger = LogManager.GetLogger(typeof(Logger));

        //public static string Modul_id;
        //public static string Org_id;

        private static Contentum.eIntegrator.Service.INT_LogService service_Log = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.GetINT_LogService();
        #endregion

        public Result PirSzamlaExportToCsv(ExecParam ExecParam, HttpContext context, DataContext dataContext)
        {
            Result Result = new Result();
            Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, context, GetType());

            bool isConnectionOpenHere = false;
            bool isTransactionBeginHere = false;

            try
            {
                isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
                isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

                #region SzamlakGetAllWithExtension
                EREC_SzamlakService svcSzamlak = eUtility.eRecordService.ServiceFactory.GetEREC_SzamlakService();
                EREC_SzamlakSearch schSzamlak = new EREC_SzamlakSearch(true);

                schSzamlak.PIRAllapot.Value = KodTarak.PIR_ALLAPOT.Fogadott;
                schSzamlak.PIRAllapot.Operator = eQuery.Query.Operators.equals;
                schSzamlak.Extended_EREC_KuldKuldemenyekSearch.SztornirozasDat.Value = string.Empty;
                schSzamlak.Extended_EREC_KuldKuldemenyekSearch.SztornirozasDat.Operator = eQuery.Query.Operators.isnull;
                schSzamlak.Extended_EREC_KuldKuldemenyekSearch.BeerkezesIdeje.Value = string.Empty;
                schSzamlak.Extended_EREC_KuldKuldemenyekSearch.BeerkezesIdeje.Operator = eQuery.Query.Operators.notnull;
                schSzamlak.Extended_EREC_KuldKuldemenyekSearch.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Bejovo;
                
                Result = svcSzamlak.GetAllWithExtension(ExecParam.Clone(),schSzamlak);
                if (Result.IsError)
                    throw new ResultException(Result);
                if (Result.Ds == null)
                { 
                    Logger.Error("DataSet is null");
                    // BLG_1500
                    Result.ErrorCode = "PIR";
                    Result.ErrorMessage = "Számlák lekérdezése sikertelen (dataSet is null)!";
                    return Result;
                }

                if (Result.Ds.Tables[0].Rows.Count == 0)
                {
                    Logger.Info("Row is empty");
                    // BLG_1500
                    Result.ErrorCode = "PIR" ;
                    Result.ErrorMessage = "Nincs átadandó számla!";
                    Result.ErrorType = Constants.ErrorType.Warning;
                    return Result;
                }
                #endregion
                //BUG_4731
                Logger.Info("BUG_4731 configbeállítások beolvasása elött");
                string baseDir = System.Configuration.ConfigurationManager.AppSettings["PIRSzlaExportPath"];
                if (baseDir[baseDir.Length - 1] != '\\') baseDir = baseDir + '\\';
                string prefix = System.Configuration.ConfigurationManager.AppSettings["PIRSzlaExportPrefix"];
                string escapeCharacter = System.Configuration.ConfigurationManager.AppSettings["PIRSzlaExportEscapeCharacter"];
                string dividerCharacter = System.Configuration.ConfigurationManager.AppSettings["PIRSzlaExportDividerCharacter"];
                string datetimeString = System.Configuration.ConfigurationManager.AppSettings["PIRSzlaExportDateTimeFormat"];
                bool BreakLineAfterRow;
                bool defaultValues = !bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["PIRSzlaExportBreakLineAfterRow"],out BreakLineAfterRow) || string.IsNullOrEmpty(datetimeString);

                if (string.IsNullOrEmpty(baseDir) || string.IsNullOrEmpty(prefix))
                {
                    Logger.Error("PIRSzlaExportPath vagy PIRSzlaExportPrefix értéke üres!");
                }

                //BUG_4731
                Logger.Info("BUG_4731 configbeállítások beolvasása után");
                Logger.Info("BUG_4731 baseDir: "+ baseDir);
                Logger.Info("BUG_4731 prefix: " + prefix);
                Logger.Info("BUG_4731 filenév: " + string.Format("{0}{1}_{2}.txt", baseDir, prefix, DateTime.Now.ToString("yyMMdd_HHmmss")));
                Logger.Info("BUG_4731 defaultValues: " + defaultValues.ToString());
                Logger.Info("BUG_4731 escapeCharacter, dividerCharacter, datetimeString, BreakLineAfterRow : " + escapeCharacter + ";" + dividerCharacter + ";" + datetimeString + ";" + BreakLineAfterRow.ToString());

                ExecParam ExecParam_Update = ExecParam.Clone();
                using (StreamWriter writer = File.CreateText(string.Format("{0}{1}_{2}.txt", baseDir, prefix, DateTime.Now.ToString("yyMMdd_HHmmss"))))
                {
                    //BUG_4731
                    Logger.Info("BUG_4731 foreach elött");
                    Logger.Info("BUG_4731 RowCount: " + Result.Ds.Tables[0].Rows.Count.ToString());
                    foreach (DataRow Row in Result.Ds.Tables[0].Rows)
                {
                    ProcusysExportObj Obj = FillData(ExecParam.Clone(), Row);
                    if (Obj == null)
                    {
                        Logger.Info("Obj is null");
                        // BLG_1500
                        Result.ErrorCode = "PIR";
                        Result.ErrorMessage = "Procusys export hiba (Obj is null)!";
                            
                        return Result;
                    }
                        // BUG_4731
                        Logger.Info("BUG_4731 record id:" + Row["Id"].ToString());
                        ExecParam_Update.Record_Id = Row["Id"].ToString();

                        EREC_Szamlak erec_szamlak = new EREC_Szamlak();
                        
                        erec_szamlak.Base.Updated.SetValueAll(false);
                        erec_szamlak.Updated.SetValueAll(false);
                        erec_szamlak.Base.Updated.Ver = true;
                        erec_szamlak.Base.Ver = Row["Ver"].ToString();
                                                
                        erec_szamlak.PIRAllapot = (Obj.BeerkezesDatuma == null || string.IsNullOrEmpty(Obj.ErkeztetetesiAzonosito) || string.IsNullOrEmpty(Obj.SzallitoCime) || string.IsNullOrEmpty(Obj.SzallitoMegnevezes) || string.IsNullOrEmpty(Obj.SzallitoSzamlaSorszam) ? erec_szamlak.PIRAllapot = KodTarak.PIR_ALLAPOT.Elutasitott : erec_szamlak.PIRAllapot = KodTarak.PIR_ALLAPOT.Veglegesitett);
                        erec_szamlak.Updated.PIRAllapot = true;

                        // BUG_4731
                        Logger.Info("BUG_4731 pir állapot:" + erec_szamlak.PIRAllapot.ToString());
                        Result resUpdate = svcSzamlak.Update(ExecParam_Update, erec_szamlak);
                        if (resUpdate.IsError)
                        {
                            throw new ResultException(resUpdate);
                        }

                        if (erec_szamlak.PIRAllapot == KodTarak.PIR_ALLAPOT.Veglegesitett)
                        {
                            // BUG_4731
                            String csvRow = String.Empty;
                            if (defaultValues)
                                csvRow = Obj.ToCsvRow();
                            else
                                csvRow = Obj.ToCsvRow(escapeCharacter, dividerCharacter, datetimeString, BreakLineAfterRow);
                            // BUG_4731
                            Logger.Info("BUG_4731 csvRow:" + csvRow);
                            writer.Write(csvRow);
                            // BUG_4731
                            Logger.Info("BUG_4731 CSV-be kiírva");
                            
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("exception: [{0}],[{1}]  ", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                dataContext.RollbackIfRequired(isTransactionBeginHere);
                Result = ResultException.GetResultFromException(ex);
                Result.ErrorCode = "PIR" + Result.ErrorCode;
                logger.ErrorFormat("[{0}] ErrorId: [{1}], Message: [{2}]", MethodBase.GetCurrentMethod().Name, Result.ErrorCode, Result.ErrorMessage);
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }
            logger.DebugFormat("end");
            logger.DebugFormat("[{0}] Result: [{1}]", MethodBase.GetCurrentMethod().Name, (!Result.IsError).ToString());

            return Result; 
        }

        private ProcusysExportObj FillData(ExecParam ExecParam, DataRow Row)
        {

            ProcusysExportObj Obj = new ProcusysExportObj();
            Obj.SzamlaId = Row["Id"].ToString();
            Obj.SzallitoSzamlaSorszam = Row["SzamlaSorszam"].ToString();

            #region CimQuery 
            // BLG_4731
            //Contentum.eAdmin.Service.KRT_PartnerekService service_partner = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
            //ExecParam execParam_partnerGetAll = ExecParam.Clone();

            //KRT_PartnerekSearch search_partner = new KRT_PartnerekSearch();

            //search_partner.Id.Value = Row["Partner_Id_Szallito"].ToString();
            //search_partner.Id.Operator = eQuery.Query.Operators.equals;

            //search_partner.TopRow = 1;

            //Result result_partnerGetAll = service_partner.GetAllForSzamla(execParam_partnerGetAll, search_partner, Row["Cim_Id_Szallito"].ToString(), string.Empty);
            //if (result_partnerGetAll.IsError)
            //{
            //    throw new ResultException(result_partnerGetAll);
            //}

            //if (result_partnerGetAll.Ds.Tables[0].Rows.Count == 0)
            //{
            //    throw new ResultException(53100); // A partner nem található.
            //}

            //DataRow dr = result_partnerGetAll.Ds.Tables[0].Rows[0];


            //if (dr != null)
            //{
            //    Obj.SzallitoMegnevezes = dr["Nev"].ToString();
            //    Obj.SzallitoCime =dr["IRSZ"]  +" "+ dr["CimNev"].ToString();
            //}

            Obj.SzallitoMegnevezes = Row["NevSTR_Szallito"].ToString();
            Obj.SzallitoCime = Row["CimSTR_Szallito"].ToString();

            #endregion

            using (EREC_KuldKuldemenyekService svcKuldemenyek = eUtility.eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService())
            {
                ExecParam ep_Kuldemenyek = ExecParam.Clone();
                ep_Kuldemenyek.Record_Id = Row["KuldKuldemeny_Id"].ToString();
                Result res = svcKuldemenyek.Get(ep_Kuldemenyek);
                if (res.IsError || res.Record == null)
                {
                    throw new ResultException(res);
                }
                EREC_KuldKuldemenyek record = (EREC_KuldKuldemenyek)res.Record;
                Obj.BeerkezesDatuma = DateTime.Parse(record.BeerkezesIdeje);
                Obj.ErkeztetetesiAzonosito = record.Azonosito;            

            }

            return Obj;
        }        

        public Result PirSzamlaFejAllapot(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla, HttpContext context, DataContext dataContext)
        {
            logger.DebugFormat("NOT IMPLEMENTED (PirSzamlaFejAllapot)");
            return new Result();
        }

        public Result PirSzamlaFejAllapotValtozas(ExecParam ExecParam, DateTime datum, HttpContext context, DataContext dataContext)
        {
            logger.DebugFormat("NOT IMPLEMENTED (PirSzamlaFejAllapot)");
            return new Result();
        }

        public Result PirSzamlaFejFelvitel(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla, HttpContext context, DataContext dataContext)
        {
            logger.DebugFormat("NOT IMPLEMENTED (PirSzamlaFejAllapot)");
            return new Result();
        }

        public Result PirSzamlaFejModositas(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla, HttpContext context, DataContext dataContext)
        {
            logger.DebugFormat("NOT IMPLEMENTED (PirSzamlaFejModositas)");
            return new Result();
        }

        public Result PirSzamlaFejSztorno(ExecParam ExecParam, string Szamlaszam, HttpContext context, DataContext dataContext)
        {
            logger.DebugFormat("NOT IMPLEMENTED (PirSzamlaFejSztorno)");
            return new Result();
        }

        public Result PirSzamlaKepFeltoltes(ExecParam ExecParam, PIRFileContent image, HttpContext context, DataContext dataContext)
        {
            logger.DebugFormat("NOT IMPLEMENTED (PirSzamlaKepFeltoltes)");
            return new Result();
        }

        
    }
}
