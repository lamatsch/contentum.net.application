﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eIntegrator.Service.NAP.BusinessObjects
{
    public class AzonositoAdat
    {
        public Szemely4TAdatok SzemelyAdatok { get; set; }
        public string TitkosKapcsolatiKod { get; set; }
    }
}
