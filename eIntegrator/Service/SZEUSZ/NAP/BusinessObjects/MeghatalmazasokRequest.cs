﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eIntegrator.Service.NAP.BusinessObjects
{
    public class MeghatalmazasokRequest
    {
        public AzonositoAdat AzonositoAdat { get; set; }
        public SzereploTipusok Szereplo { get; set; }

        public MeghatalmazasTipusok Tipus { get; set; }
    }
}
