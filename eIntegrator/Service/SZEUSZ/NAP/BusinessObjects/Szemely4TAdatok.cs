﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eIntegrator.Service.NAP.BusinessObjects
{
    public class Szemely4TAdatok
    {
        public string Nev { get; set; }
        public string SzuletesiNev { get; set; }
        public BontottNev AnyjaNeve { get; set; }
        public DateTime SzuletesiIdo { get; set; }
        public string SzuletesiHely { get; set; }

        public Szemely4TAdatok()
        {
            AnyjaNeve = new BontottNev();
        }
    }
}
