﻿using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eIntegrator.Service.NAP
{
    public class MeghatalmazasTipusokHelper
    {
        public static List<MeghatalmazasTipusok> GetOsszesMeghatalmazasTipus()
        {
            return Enum.GetValues(typeof(MeghatalmazasTipusok)).Cast<MeghatalmazasTipusok>().ToList();
        }
    }
}
