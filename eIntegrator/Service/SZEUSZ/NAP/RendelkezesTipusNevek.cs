﻿using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eIntegrator.Service.NAP
{
    public class RendelkezesTipusNevek
    {
        public const string AltalanosMeghatalmazas = "Meghatalmazás";
        public const string SzabadSzovegesMeghatalmazas = "Szabad szöveges meghatalmazás";
        public const string AltalanosRendelkezes = "Általános meghatalmazás";
        public const string Osszes = "Összes";

        public static string GetNev(MeghatalmazasTipusok meghatalmazasTipus)
        {
            switch (meghatalmazasTipus)
            {
                case MeghatalmazasTipusok.AltalanosMeghatalmazas:
                    return AltalanosMeghatalmazas;
                case MeghatalmazasTipusok.SzabadSzovegesMeghatalmazas:
                    return SzabadSzovegesMeghatalmazas;
                case MeghatalmazasTipusok.AltalanosRendelkezes:
                    return AltalanosRendelkezes;
                case MeghatalmazasTipusok.Osszes:
                    return Osszes;
            }

            return String.Empty;
        }
    }
}
