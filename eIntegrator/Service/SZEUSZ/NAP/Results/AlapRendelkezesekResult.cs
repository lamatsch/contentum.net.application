﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eIntegrator.Service.NAP.Results
{
    public class AlapRendelkezesekResult: Result
    {
        public List<KRT_Cimek> ElerhetosegekList { get; set; }
    }
}
