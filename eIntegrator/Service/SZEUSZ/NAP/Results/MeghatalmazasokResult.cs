﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eIntegrator.Service.NAP.Results
{
    public class MeghatalmazasokResult: Result
    {
        public List<Meghatalmazas> MeghatalmazasokList { get; set; }
    }
}
