﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eIntegrator.Service.ONYP.BusinessObjects
{
    public class Meghatalmazas
    {
        public long Id { get; set; }
        public long Azonosito { get; set; }
        public string Tipus { get; set; }
        public string Ki { get; set; }
        public string Kit { get; set; }
        public string Kire { get; set; }
        public string RendelkezesXML { get; set; }
        public DateTime? HatalyKezdete { get; set; }
        public DateTime? HatalyVege { get; set; }
    }
}
