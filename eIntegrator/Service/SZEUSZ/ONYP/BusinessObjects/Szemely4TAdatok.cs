﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eIntegrator.Service.ONYP.BusinessObjects
{
    public class Szemely4TAdatok
    {
        public string Nev { get; set; }
        public string SzuletesiNev { get; set; }
        public string AnyjaNeve { get; set; }
        public DateTime SzuletesiIdo { get; set; }
        public string SzuletesiHely { get; set; }
    }
}
