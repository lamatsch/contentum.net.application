using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Data;
using Contentum.eUtility;
using System.Configuration;
using System.Web.Configuration;

namespace Contentum.eIntegrator.Service
{
    public partial class eIntegratorService
    {
        //public class BusinessService
        //{
        //    private string _Type = "";

        //    public string Type
        //    {
        //        get { return _Type; }
        //        set { _Type = value; }
        //    }
        //    private string _Url = "";

        //    public string Url
        //    {
        //        get { return _Url; }
        //        set { _Url = value; }
        //    }
        //    private string _Authentication = "";

        //    public string Authentication
        //    {
        //        get { return _Authentication; }
        //        set { _Authentication = value; }
        //    }
        //    private string _UserDomain = "";

        //    public string UserDomain
        //    {
        //        get { return _UserDomain; }
        //        set { _UserDomain = value; }
        //    }
        //    private string _UserName = "";

        //    public string UserName
        //    {
        //        get { return _UserName; }
        //        set { _UserName = value; }
        //    }
        //    private string _Password = "";

        //    public string Password
        //    {
        //        get { return _Password; }
        //        set { _Password = value; }
        //    }

        //}

        public static Contentum.eIntegrator.Service.ServiceFactory GetServiceFactory(BusinessService BS)
        {
            return GeteIntegratorServiceFactory(BS);
        }

        public static Contentum.eIntegrator.Service.ServiceFactory ServiceFactory
        {
            get { return GeteIntegratorServiceFactory(); }
        }

        private static Contentum.eIntegrator.Service.ServiceFactory GeteIntegratorServiceFactory()
        {
            BusinessService BS = new BusinessService();
            BS.Type = ConfigurationManager.AppSettings.Get("eIntegratorBusinessServiceType");
            BS.Url = ConfigurationManager.AppSettings.Get("eIntegratorBusinessServiceUrl");
            BS.Authentication = ConfigurationManager.AppSettings.Get("eIntegratorBusinessServiceAuthentication");
            BS.UserDomain = ConfigurationManager.AppSettings.Get("eIntegratorBusinessServiceUserDomain");
            BS.UserName = ConfigurationManager.AppSettings.Get("eIntegratorBusinessServiceUserName");
            BS.Password = ConfigurationManager.AppSettings.Get("eIntegratorBusinessServicePassword");
            return GeteIntegratorServiceFactory(BS);
        }

        private static Contentum.eIntegrator.Service.ServiceFactory GeteIntegratorServiceFactory(BusinessService BS)
        {
            Contentum.eIntegrator.Service.ServiceFactory sc = new Contentum.eIntegrator.Service.ServiceFactory();
            sc.BusinessServiceType = BS.Type;
            sc.BusinessServiceUrl = BS.Url;
            sc.BusinessServiceAuthentication = BS.Authentication;
            sc.BusinessServiceUserDomain = BS.UserDomain;
            sc.BusinessServiceUserName = BS.UserName;
            sc.BusinessServicePassword = BS.Password;
            return sc;
        }

        public static string GetFelhasznaloNevById(string Felhasznalo_Id, Page page)
        {
            return FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(Felhasznalo_Id, page);
        }
    }
}
