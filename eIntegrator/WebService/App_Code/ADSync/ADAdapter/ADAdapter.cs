﻿using log4net;
using Contentum.eUtility;
using System.Reflection;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using Contentum.eIntegrator.Service;
using System.Data;

namespace Contentum.eIntegrator.ADAdapter
{

    /// <summary>
    /// Summary description for ADAdapter
    /// AD kezelő függvények
    /// </summary>
    public class ADAdapter
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(Logger));



        public ADAdapter()
        {
        }

        /// <summary>
        /// Lekéri az ADból a szervezeti egység hierarchia típus
        /// szerinti részhalmazát.
        /// </summary>
        /// <param name="groupTypeEnum">Csoporttípus meghatározása.</param>
        /// <returns>A felépített hierarchia.</returns>
        internal static WrappedADGroupCollection GetPartialADGroupHierarchy()
        {
            //internal static WrappedADGroupCollection GetPartialADGroupHierarchy(ADAdapterTypes.GroupTypeEnum groupTypeEnum)
            //{
            log.DebugFormat("[{0}] START", MethodBase.GetCurrentMethod().Name);

            string rootGroupName = GetRootGroup(INT_ADSyncService.Modul_id);
            GroupPrincipal srcGroup = GroupPrincipal.FindByIdentity(INT_ADSyncService.ADctx, rootGroupName);

            WrappedADGroupCollection wrappedGroups = new WrappedADGroupCollection();
            //BuildGroupTree(ref wrappedGroups, null, srcGroup.Members, groupTypeEnum);
            BuildGroupTree(ref wrappedGroups, null, srcGroup.Members);

            // Add Deleted Group
            WrappedADGroupCollection deletedOUs = null;
            try
            {
                // deletedOUs = GetInactiveADGroups(InvolvedGroupTypes[i].ToString());
                deletedOUs = GetInactiveADGroups(ADAdapterTypes.GroupTypeEnum.Org.ToString());
                if (wrappedGroups != null)
                {
                    wrappedGroups.AddRange(deletedOUs);
                }
            }
            catch (Exception e)
            {

                log.ErrorFormat(String.Format("[{0}] Hiba a törölt szervezetek feldolgozása közben.", MethodBase.GetCurrentMethod().Name), e);
            }
            log.DebugFormat("[{0}] END Szervezetek száma:{1}",
                MethodBase.GetCurrentMethod().Name, wrappedGroups.Count);
            return wrappedGroups;
        }

        /// <summary>
        /// Inaktivált szervezeti egységek listájának lekérdezése.
        /// </summary>
        /// <param name="groupTypeID">AD Group érték.</param>
        /// <returns>Az adott típusú, inaktivált group objektumok.</returns>
        /// <exception cref="DatabaseException">
        /// Adatkapcsolati hiba esetén veti 
        /// az IDataAccessAdapter típusú osztály.
        /// </exception>
        public static WrappedADGroupCollection GetInactiveADGroups(string groupTypeName)
        {
            
            ExecParam execParam = ADAdapterTypes.GetExecParam();
            // INT_ExternalIDsService service_ExtIds = new INT_ExternalIDsService(dataContext);
            Contentum.eIntegrator.Service.INT_ExternalIDsService service_ExtIds = eIntegratorService.ServiceFactory.GetINT_ExternalIDsService();

            INT_ExternalIDsSearch search = new INT_ExternalIDsSearch();

            search.Modul_Id.Value = INT_ADSyncService.Modul_id.ToString();
            search.Modul_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.External_Group.Value = groupTypeName;
            search.External_Group.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = new Result();
            WrappedADGroupCollection deletedGroups = new WrappedADGroupCollection();

            result = service_ExtIds.GetAll(execParam, search);

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                log.ErrorFormat(String.Format("[{0}] Hiba a törölt szervezetek feldolgozása közben. Error: {1}, {2}", 
                    MethodBase.GetCurrentMethod().Name), result.ErrorCode, result.ErrorMessage);
                throw new ResultException(result);
            }

            if (result.Ds != null)
            {
                WrappedADGroup group;

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    GroupPrincipal srcGroup = GroupPrincipal.FindByIdentity(INT_ADSyncService.ADctx, row["External_Id"].ToString());
                    if (srcGroup == null)
                    {
                        group = new WrappedADGroup();
                        group.ADProp.ObjectID = new Guid(row["External_Id"].ToString());
                        group.Deleted = true; // (row["Deleted"].ToString() == "1");
                        group.ADProp.GroupType = row["External_Group"].ToString();
                        // group.Name = groups[i].cName.Value;
                        // group.Created = groups[i].dCreated.Value;
                        deletedGroups.Add(group);
                    }
                }
            }
            log.DebugFormat("[{0}] END Törölt szervezetek száma:{1}", MethodBase.GetCurrentMethod().Name, deletedGroups.Count);
            return deletedGroups;

        }

        /// <summary>
        /// Szervezeti hierarchia felépítése.
        /// </summary>
        /// <param name="wrappedGroups">A felépítendő kollekció.</param>
        /// <param name="rootGroupObjectID">A szülő Group ObjectID-je.</param>
        /// <param name="groupHierarchy">Az AD-tól lekérdezett hierarchia.</param>
        /// <remarks>Rekurzív metódus.</remarks>
        //private static void BuildGroupTree(ref WrappedADGroupCollection wrappedGroups, Guid? rootGroupObjectID, PrincipalCollection groupHierarchy, ADAdapterTypes.GroupTypeEnum groupTypeEnum)
        private static void BuildGroupTree(ref WrappedADGroupCollection wrappedGroups, Guid? rootGroupObjectID, PrincipalCollection groupHierarchy)
        {
            if (groupHierarchy == null)   //|| groupHierarchy.AllCount == 0
                return;

            // wrappedGroups = new WrappedADGroupCollection();

            foreach (Principal gh in groupHierarchy)
            {
                //DirectoryEntry de = sr.GetUnderlyingObject() as DirectoryEntry;
                GroupPrincipal grp = gh as GroupPrincipal;
                if (grp != null)
                {
                    if (grp.IsSecurityGroup.Value)   //.Technical a technikai csoportokat kihagyjuk
                    {
                        WrappedADGroup wrapped = new WrappedADGroup(grp);

                        wrapped.ADProp.ParentGroupID = rootGroupObjectID == null ? rootGroupObjectID : rootGroupObjectID.Value;
                        wrapped.ADProp.GroupType = ADAdapterTypes.GroupTypeEnum.Org.ToString(); // groupTypeEnum.ToString();

                        wrappedGroups.Add(wrapped);
                        //BuildGroupTree(ref wrappedGroups, grp.Guid.Value, grp.Members, groupTypeEnum);
                        BuildGroupTree(ref wrappedGroups, grp.Guid.Value, grp.Members);
                    }
                }
            }
            //for (int i = 0; i < groupHierarchy.AllCount; i++)
            //{
            //    adajk.GroupExtended2 g = (adajk.GroupExtended2)groupHierarchy[i];
            //    if (!g.Technical)   // a technikai csoportokat kihagyjuk
            //    {
            //        WrappedADGroup wrapped = new WrappedADGroup(g);
            //        wrapped.ADProp.ParentGroupID = rootGroupObjectID;
            //        wrappedGroups.Add(wrapped);
            //        BuildGroupTree(ref wrapped.GroupMembers, g.ObjectId, g.Children);
            //    }
            //}
        }

        /// <summary>
        /// Inaktivált felhasználók listájának lekérdezése.
        /// </summary>
        /// <param name="userTypeID">AD UserTypeRef érték.</param>
        /// <returns>Az adott típusú, inaktivált felhasználó-objektumok.</returns>
        //public WrappedADUserCollection GetInactiveADUsers(string userTypeName)
        internal static WrappedADUserCollection GetInactiveADUsers()
        {
 
            ExecParam execParam = ADAdapterTypes.GetExecParam();
            Contentum.eIntegrator.Service.INT_ExternalIDsService service_ExtIds = eIntegratorService.ServiceFactory.GetINT_ExternalIDsService();

            INT_ExternalIDsSearch search = new INT_ExternalIDsSearch();

            search.Modul_Id.Value = INT_ADSyncService.Modul_id.ToString();
            search.Modul_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.External_Group.Value = ADAdapterTypes.ADType.User.ToString();
            search.External_Group.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = new Result();
            WrappedADUserCollection deletedUsers = new WrappedADUserCollection();

            result = service_ExtIds.GetAll(execParam, search);

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                log.ErrorFormat("[{0}] Hiba az Inaktív (törölt) felhasználók lekérdezésénél! Error: {1},{2}", 
                    MethodBase.GetCurrentMethod().Name, result.ErrorCode, result.ErrorMessage);
                throw new ResultException(result);
            }


            if (result.Ds != null)
            {
                WrappedADUser user;

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    UserPrincipal srcUser = UserPrincipal.FindByIdentity(INT_ADSyncService.ADctx, row["External_Id"].ToString());
                    if (srcUser == null)
                    {

                        user = new WrappedADUser(srcUser);
                        user.Deleted = true;
                        //user.ADProp.ObjectID = new Guid(row["External_Id"].ToString());
                        //user.Deleted = (row["Deleted"].ToString() == "1");
                        ////TODO
                        ////user.Enabled = users[i].bEnabled.Value;
                        ////user.Locked = users[i].bLocked.Value;
                        ////user.Expired = row["ExpiredDate"].ToString();
                        //user.ADProp.UserType = row["External_Group"].ToString();
                        ////user.FullName = users[i].cFullName.IsNull ? null : users[i].cFullName.Value;
                        deletedUsers.Add(user);
                    }
                }
            }
            log.DebugFormat("[{0}] END Törölt felhasználók száma:{1}", MethodBase.GetCurrentMethod().Name, deletedUsers.Count);
            return deletedUsers;
        }


        /// <summary>
        /// A felhasználók egy részhalmazának lekérdezése.
        /// </summary>
        /// <param name="userTypeEnum">A felhasználótípus, mint szűrőfeltétel.</param>
        /// <param name="loginFirstLetter">A login nevek első betűje, mint szűrőfeltétel.</param>
        /// <returns>Minden, a megadott típusba sorolt felhasználó.</returns>
        /// <remarks>
        /// Figyelem! A felhasználó szerepkörtagsága alkalmazásfüggő, ezért
        /// ez a metódus nem is tölti fel!
        /// </remarks>
        //internal static WrappedADUserCollection GetADUserSubSet(
        //    ADAdapterTypes.UserTypeEnum userTypeEnum)
        internal static WrappedADUserCollection GetADUserSubSet()
        //adajk.UserTypeEnum userTypeEnum, char loginFirstLetter)
        {
            log.DebugFormat("[{0}] START", MethodBase.GetCurrentMethod().Name);

            WrappedADUserCollection wrappedUsers = new WrappedADUserCollection();

            string rootUserName = GetRootUserName(INT_ADSyncService.Modul_id);
            GroupPrincipal srcGroup = GroupPrincipal.FindByIdentity(INT_ADSyncService.ADctx, rootUserName);

            foreach (Principal sr in srcGroup.Members)
            {
                UserPrincipal up = sr as UserPrincipal;
                if (up != null)
                {
                    //DirectoryEntry de = sr.GetUnderlyingObject() as DirectoryEntry;
                    WrappedADUser wrapped = new WrappedADUser(up);
                    //  wrapped.ADProp.UserType = userTypeEnum.ToString();
                    wrappedUsers.Add(wrapped);
                }
            }

            log.DebugFormat("[{0}] END Felhasználók száma:{1}", 
                MethodBase.GetCurrentMethod().Name, wrappedUsers.Count);
            return wrappedUsers;
        }

        /// <summary>
        /// Felhasználóhoz rendelt szerepkörök lekérdezése.
        /// Jelenleg nem használjuk
        /// </summary>
        /// <param name="ADUserObjectID">A felhasználó AD-azonosítója.</param>
        /// <returns>Szerepkörlista.</returns>
        public static void SetRoleMembershipProperties(ref WrappedADUser wrappedUser) //adajkUserObjectID = wrappedUser.ADProp.ObjectID
        {
            log.DebugFormat("[{0}] Start", MethodBase.GetCurrentMethod().Name);

            try
            {
                UserPrincipal ADuser = UserPrincipal.FindByIdentity(INT_ADSyncService.ADctx, IdentityType.Guid, wrappedUser.ADProp.ObjectID.ToString());

                if (ADuser == null)
                {
                    log.ErrorFormat("[{0}] {1} azonosítójú \"{2}\" felhasználó nem található az AD-ban!"
                        , MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID, wrappedUser.FullName);


                }
                else
                {
                    // find the roles....
                    wrappedUser.RoleMembership = new WrappedADRoleCollection(ADuser.GetAuthorizationGroups());

                }
            }
            catch (Exception ex)
            {
                log.Error(String.Format("[{0}] Hiba a szerepkörök lekérdezésénél! User: {1} ({2})!"
                    , MethodBase.GetCurrentMethod().Name, wrappedUser.FullName, wrappedUser.ADProp.ObjectID), ex);

            }

            // Nincs megvalósítva

            //adajk.UserExtended adajkUser = _UserService.UserSelectById(
            //    true,
            //    true);

            //adajk.RoleContainer roles = null;
            //try
            //{
            //roles = _RoleService.RoleAccessList(applicationName, ADUser); //applicationName = GetADRootName(INT_ADSyncService.Modul_id);
            //}
            //catch (Fph.Adajk.Utility.Exceptions.AdajkAzInvalidUserContextException aaiuce)
            //{
            //    ExternalErrorEvent.Raise(
            //        "ADAJK",
            //        String.Format("{0} azonosítójú \"{1}\" felhasználó nem található az AD-ban.",
            //            adajkUser.ObjectId,
            //            adajkUser.FullName));
            //    TraceCallReturnEvent.Raise(false);
            //    ThrowAdajkException(aaiuce);
            //}
            //catch (Exception ex)
            //{
            //    TraceCallReturnEvent.Raise(false);
            //    ThrowAdajkException(ex);
            //}

            //if (roles == null)
            //{
            //    TraceCallReturnEvent.Raise(false);
            //    //throw new AdajkUserNotFoundException(loginName);
            //}
            //else
            //{
            //    TraceCallReturnEvent.Raise(true);
            //}
            //return roles;
        }

        /// <summary>
        /// Root Usernév lekérdezése
        /// </summary>
        /// <param name="modul_id">Modul azonosítója.</param>
        /// <returns>Szerepkörlista.</returns>
        public static string GetRootUserName(string modul_id)
        {

            Contentum.eIntegrator.Service.INT_ParameterekService service_Params = Service.eIntegratorService.ServiceFactory.GetINT_ParameterekService();

            ExecParam execParam = ADAdapterTypes.GetExecParam();

            INT_ParameterekSearch search = new INT_ParameterekSearch();

            search.Modul_id.Value = modul_id;
            search.Modul_id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.Nev.Value = ADAdapterTypes.EDOK_ROOT_USER;
            search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = service_Params.GetAll(execParam, search);

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                log.ErrorFormat("[{0}] Hiba a RootUser lekérdezésénél! Root User név: {1}, Error: {2},{3}", 
                    MethodBase.GetCurrentMethod().Name, ADAdapterTypes.EDOK_ROOT_USER, result.ErrorCode, result.ErrorMessage);
                throw new ResultException(result);

            }
            if (result.Ds.Tables[0].Rows.Count == 1)
            {
                return result.Ds.Tables[0].Rows[0]["Ertek"].ToString();
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a RootUser lekérdezésénél! Root User név: {1}", MethodBase.GetCurrentMethod().Name, ADAdapterTypes.EDOK_ROOT_USER);
                throw new System.Exception(string.Format("Hiba a RootUser lekérdezésénél! Root User név: {0}", ADAdapterTypes.EDOK_ROOT_USER));
            }

        }

        /// <summary>
        /// Root Group lekérdezése
        /// </summary>
        /// <param name="Modul_id"></param>
        /// <returns></returns>
        private static string GetRootGroup(String Modul_id)
        {
            Service.INT_ParameterekService service_Params = Service.eIntegratorService.ServiceFactory.GetINT_ParameterekService();
            ExecParam execParam = ADAdapterTypes.GetExecParam();

            INT_ParameterekSearch search = new INT_ParameterekSearch();

            search.Modul_id.Value = Modul_id;
            search.Modul_id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.Nev.Value = ADAdapterTypes.EDOK_ROOT_GROUP;
            search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;


            Result result = service_Params.GetAll(execParam, search);

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                log.ErrorFormat("[{0}] Hiba a RootGroup lekérdezésénél! Root Group név: {1}, Error:{2},{3}", 
                    MethodBase.GetCurrentMethod().Name, ADAdapterTypes.EDOK_ROOT_GROUP, result.ErrorCode, result.ErrorMessage);
                throw new ResultException(result);
            }

            if (result.Ds.Tables[0].Rows.Count == 1)
            {
                return result.Ds.Tables[0].Rows[0]["Ertek"].ToString();

            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a RootGroup lekérdezésénél! Root Group név: {1}", MethodBase.GetCurrentMethod().Name, ADAdapterTypes.EDOK_ROOT_GROUP);
                throw new Exception(string.Format("Hiba a RootGroup lekérdezésénél! Root Group név: {0}", ADAdapterTypes.EDOK_ROOT_GROUP));
            }
        }

        /// <summary>
        /// Beállítja a csoport tagságokat, ill. a menedzselt csoportot.
        /// </summary>
        /// <param name="wrappedUser">A beállítandó user.</param>
        public static void SetGroupMembershipProperties(ref WrappedADUser wrappedUser)
        {
            wrappedUser.GroupMembership = GetGroupMembership(Convert.ToString(wrappedUser.ADProp.ObjectID));
            //SetGroupMembership(ref wrappedUser);
            SetManagedGroup(ref wrappedUser);

        }

        //private static void SetGroupMembership(ref WrappedADUser wrappedUser)
        //{
        //    log.DebugFormat("[{0}] Start", MethodBase.GetCurrentMethod().Name);

        //    wrappedUser.GroupMembership = GetGroupMembership(Convert.ToString(wrappedUser.ADProp.ObjectID));

        //    //for (int i = 0; i < wrappedUser.GroupMembership.Count; i++)
        //    //{
        //    //    WrappedADGroup wrappedGroup = wrappedUser.GroupMembership[i];

        //    //    GroupPrincipal parentGroup = GetGroupParent(wrappedGroup.ADProp.ObjectID);

        //    //    if (parentGroup != null)
        //    //        wrappedGroup.ADProp.ParentGroupID = parentGroup.Guid.Value;
        //    //}

        //  //  wrappedUser.GroupMembership.RemoveLogicalGroups();

        //    log.DebugFormat("[{0}] End", MethodBase.GetCurrentMethod().Name);
        //}

        /// <summary>
        ///  Beállítja a user által vezetett csoportot
        /// </summary>
        /// <param name="wrappedUser"></param>
        private static void SetManagedGroup(ref WrappedADUser wrappedUser)
        {
            wrappedUser.ADProp.ManagedGroupCount = 0;
            for (int i = 0; i < wrappedUser.GroupMembership.Count; i++)
            {
                GroupPrincipal group = GroupPrincipal.FindByIdentity(INT_ADSyncService.ADctx, IdentityType.Guid, wrappedUser.GroupMembership[i].ADProp.ObjectID.ToString());

                DirectoryEntry de = group.GetUnderlyingObject() as DirectoryEntry;

                if (de.Properties["managedBy"].Value != null)
                {
                    UserPrincipal manager = UserPrincipal.FindByIdentity(INT_ADSyncService.ADctx, IdentityType.DistinguishedName, de.Properties["managedBy"].Value.ToString());
                    if (manager != null)
                    {
                        if (manager.Guid == wrappedUser.ADProp.ObjectID)
                        {
                            wrappedUser.ADProp.ManagedGroupCount++;
                            wrappedUser.ADProp.ManagedGroupID = group.Guid.Value;
                            log.DebugFormat("[{0}] User (AD): {1} menedzselt szervezete (AD): {2} ",
                                MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID, group.Guid.Value );

                        }
                    }
                }
            }
        }


        /// <summary>
        /// Felhasználó szervezeti egység-tagságának lekérdezése.
        /// </summary>
        /// <param name="ADUserObjectID">A felhasználó Adajk-azonosítója.</param>
        /// <returns>
        /// Azon szervezeti egységek listája, amelyeknek tagja a felhasználó,
        /// vagy null, ha egyiknek sem tagja.
        /// </returns>
        private static WrappedADGroupCollection GetGroupMembership(string ADUserObjectID)
        {
            UserPrincipal ADuser = UserPrincipal.FindByIdentity(INT_ADSyncService.ADctx, IdentityType.Guid, ADUserObjectID);
            WrappedADGroupCollection wrappedGroups = new WrappedADGroupCollection();
            try
            {

                foreach (GroupPrincipal grp in ADuser.GetGroups())
                {

                    if (grp.IsSecurityGroup.Value)   //.Technical a technikai csoportokat kihagyjuk
                    {

                        WrappedADGroup wrapped = new WrappedADGroup(grp);




                        // EDOK szervezet-e amit megtalált

                        //if (INT_ADSyncService.edokGroups.IndexOf(wrapped) >= 0)
                        //{
                        //    wrappedGroups.Add(wrapped);
                        //}

                        bool found = false;
                        for (int i = 0; i < INT_ADSyncService.edokGroups.Count; i++)
                        {
                            WrappedADGroup wg = INT_ADSyncService.edokGroups[i];
                            if (wg.ADProp.ObjectID == wrapped.ADProp.ObjectID)
                            {
                                found = true;
                                i = INT_ADSyncService.edokGroups.Count;

                            }
                        }
                        if (found)
                        {
                            // wrapped.ADProp.ParentGroupID = rootGroupObjectID == null ? rootGroupObjectID : rootGroupObjectID.Value;
                            // TODO: Jelenleg csak Org-ot kezelünk, ha nem az ki kell találni, honnan szedjük a groupType-ot
                            wrapped.ADProp.GroupType = ADAdapterTypes.GroupTypeEnum.Org.ToString();

                            GroupPrincipal parentGroup = GetGroupParent(wrapped.ADProp.ObjectID);

                            if (parentGroup != null)
                                wrapped.ADProp.ParentGroupID = parentGroup.Guid.Value;

                            wrappedGroups.Add(wrapped);
                        }
                    }

                }
                //adajk.GroupExtended3Container tempGroups;
                //tempGroups = _UserMemberService.GroupMembershipEx3(adajkUser, false);
                //// Vezetés nevű fiktív csoporttagság levágása
                //for (int i = 0; i < tempGroups.AllCount; i++)
                //{
                //    if (((adajk.GroupExtended3)tempGroups[i]).ObjectId != "61636564-8401-4003-971c-4ba2bd2c5774")   // A Vezetés csoport azonosítója
                //        adajkGroups.Add((adajk.GroupExtended3)tempGroups[i]);
                //}
            }
            catch (Exception ex)
            {
                log.Error(String.Format("[{0}] grouptagság lekérdezés hiba! AD User: {1}", MethodBase.GetCurrentMethod().Name, ADUserObjectID), ex);
            }

            if (wrappedGroups == null)
            {
                log.InfoFormat("[{0}] AD User: {1} nincs grouptagsága", MethodBase.GetCurrentMethod().Name, ADUserObjectID);
            }

            return wrappedGroups;
        }


        /// <summary>
        /// Szervezeti egység szülőjének lekérdezése.
        /// </summary>
        /// <param name="objectID">Azon szervezeti egység azonosítója, amelynek a szülőjét keressük.</param>
        /// <returns>
        /// Null, ha a szervezeti egység a hierarchia csúcsán áll és nincs szülője,
        /// egyébként a szülő objektum.
        /// </returns>
        public static GroupPrincipal GetGroupParent(Guid objectID)
        {

            GroupPrincipal parentGroup = null;
            try
            {
                GroupPrincipal childGroup = GroupPrincipal.FindByIdentity(INT_ADSyncService.ADctx, IdentityType.Guid, objectID.ToString());

                int count = 0;
              //  GroupPrincipal parent = null;
                using (PrincipalSearcher srch = new PrincipalSearcher(new GroupPrincipal(INT_ADSyncService.ADctx)))
                {

                    foreach (GroupPrincipal grp in srch.FindAll())
                    {
                        if (childGroup.IsMemberOf(grp))
                        {
                            parentGroup = grp;
                            count++;
                        }

                    }
                }

                if (count > 1)
                    log.ErrorFormat("[{0}] {1} ({2}) szervezeti egységnek egynél több szülője van az ADban.",
                            MethodBase.GetCurrentMethod().Name, childGroup.Name, objectID);
                if (count == 1)
                {
                    //log.DebugFormat("[{0}] END Beosztott szervezet: {1}", MethodBase.GetCurrentMethod().Name, objectID);
                }
            }
            catch (Exception ex)
            {
                log.Error(String.Format("[{0}] Lekérdezési hiba! AD beosztott szervezet id: {1}", MethodBase.GetCurrentMethod().Name, objectID), ex);
            }
            return parentGroup;

        }



    }
}
