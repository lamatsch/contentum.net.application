﻿using System;
using System.Collections.Generic;

namespace Contentum.eIntegrator.ADAdapter
{

    /// <summary>
    /// Summary description for ADAdapterTypes
    /// </summary>
    internal sealed class ADAdapterTypes
    {
        ///// <summary>
        ///// Az Admin user Edok-azonosítója.
        ///// </summary>
        //private const string ADMIN_PARTNEREK_ID = "54E861A5-36ED-44CA-BAA7-C287D125B309";
        /// <summary>
        /// Az EDOK-ban az AD azonosítója; az ő nevében fut a szinkronizálás.
        /// </summary>
        private const string AD_PARTNEREK_ID = "54E861A5-36ED-44CA-BAA7-C287D125B309";

        /// <summary>
        /// AD Forras jel KRT_Partnerek táblába.
        /// </summary>
        internal const string AD_FORRAS = "ADSync";

        /// <summary>
        /// Felhasználók tartományneve.
        /// </summary>
        private const string USER_DOMAIN_NAME = "EDOK_TEST";

        /// <summary>
        /// EDOK_ROOT_USER név.
        /// </summary>
        internal const string EDOK_ROOT_USER = "EDOK_ROOT_USER";

        /// <summary>
        /// EDOK_ROOT_GROUP név.
        /// </summary>
        internal const string EDOK_ROOT_GROUP = "EDOK_ROOT_GROUP";

        internal const string DEFAULT_SZEREPKOR = "FPH_LEKERDEZO";

        internal static String MaxDate = "4700-12-31 00:00:00.000";

        /// <summary>
        /// A partnerkapcsolatkódok enumeráltjai.
        /// </summary>
        internal static readonly Dictionary<PartnerkapcsolatTipus, string> PartnerkapcsolatKod;
        /// <summary>
        /// A partnertípuskódok enumeráltjai.
        /// </summary>
        internal static readonly Dictionary<PartnerTipus, string> PartnerTipusKod;

        public enum ADType
        {
            Group,
            User
        }

        #region State enum

        /// <summary>
        /// A kapcsolt rekord utolsó szinkronizálási
        /// kísérletének állapota. Adatbázis mező kulcsa.
        /// </summary>
        public enum State
        {
            OK,
            Failed //,
            //Ignored
        }

        #endregion State enum

        /// <summary>
        /// Azon szervezetiegység típusoknak a felsorolása,
        /// amelyeket szinkronizálni kell.
        /// </summary>
        /// 
        internal enum GroupTypeEnum
        {
            Builtin = 0,
            UsersCn = 1,
            FileShare = 2,
            App = 3,
            Printer = 4,
            OtherRes = 5,
            Org = 6,
            Comittee = 7,
            Location = 8,
            Project = 9,
            Activity = 10,
            Custom1 = 11,
            Custom2 = 12,
            Custom3 = 13
        }

        public enum UserTypeEnum
        {
            Builtin = 0,
            UsersCn = 1,
            Outer = 2,
            Internal = 3,
            Dismissed = 4,
            Deputy = 5
        }


        /// <summary>
        /// EDOK Partnertípus felsorolás.
        /// </summary>
        internal enum PartnerTipus
        {
            Szervezet = 10,
            Szemely = 20,
            Alkalmazas = 30,
            Project = 14,
            Comittee = 15,
            Activity = 16
        }


        /// <summary>
        /// EDOK Partnerkapcsolat típus felsorolás.
        /// </summary>
        internal enum PartnerkapcsolatTipus
        {
            Szervezet_FelettesSzervezete = '1',
            FelhasznaloSzervezete = '2',
            SzervezetiEgysegVezetoje = 'V'
        }

        /// <summary>
        /// Konstruktor, kódlista inicializálással.
        /// </summary>
        static ADAdapterTypes()
        {
            PartnerkapcsolatKod = new Dictionary<PartnerkapcsolatTipus, string>();
            PartnerkapcsolatKod.Add(PartnerkapcsolatTipus.FelhasznaloSzervezete, "2");
            PartnerkapcsolatKod.Add(PartnerkapcsolatTipus.Szervezet_FelettesSzervezete, "1");
            PartnerkapcsolatKod.Add(PartnerkapcsolatTipus.SzervezetiEgysegVezetoje, "V");

            PartnerTipusKod = new Dictionary<PartnerTipus, string>();
            PartnerTipusKod.Add(PartnerTipus.Szervezet, "10");
            PartnerTipusKod.Add(PartnerTipus.Szemely, "20");
            PartnerTipusKod.Add(PartnerTipus.Alkalmazas, "30");
            PartnerTipusKod.Add(PartnerTipus.Project, "14");
            PartnerTipusKod.Add(PartnerTipus.Comittee, "15");
            PartnerTipusKod.Add(PartnerTipus.Activity, "16");

  
        }


        /// <summary>
        /// AD group típusokat EDOK partnertípusokká konvertál.
        /// </summary>
        /// <param name="ADGroupTypeName">ADAJK típus.</param>
        /// <returns>EDOK Partnertípus.</returns>
        /// <exception cref="System.NotSupportedException">
        /// A megadott AD típust a metódus nem tudja EDOK típusra konvertálni.
        /// </exception>
        internal static PartnerTipus ConvertToPartnerTipus(string ADGroupTypeName)
        {
            switch (ADGroupTypeName)
            {
                case "Org":
                    return PartnerTipus.Szervezet;
                case "Activity":
                    return PartnerTipus.Activity;
                case "Comittee":
                    return PartnerTipus.Comittee;
                case "Project":
                    return PartnerTipus.Project;

                default:
                    throw new NotSupportedException(
                        String.Format("\"{0}\" nevű AD Group Type-ot nem ismer az algoritmus.",
                        ADGroupTypeName));
            }
        }

        /// <summary>
        /// EDOK partnertípusokat AD group típusokká konvertál.
        /// </summary>
        /// <param name="edokPartnerTípusKod">EDOK partnertípus.</param>
        /// <returns>AD group type.</returns>
        /// <exception cref="System.NotSupportedException">
        /// A megadott EDOK partnertípust a metódus nem tudja AD group type-pá konvertálni.
        /// </exception>
        internal static string ConvertToADGroupType(string edokPartnerTipusKod)
        {
            switch (edokPartnerTipusKod)
            {
                case "10":
                    return "Org";
                case "14":
                    return "Project";
                case "15":
                    return "Comittee";
                case "16":
                    return "Activity";
                default:
                    throw new NotSupportedException(
                        String.Format("\"{0}\" nevű EDOK partertípuskódot nem ismer az algoritmus.",
                        edokPartnerTipusKod));
            }
        }
        /// <summary>
        /// ExecParam beállítása: az edok által hívott webszolgáltatások kinek a nevében fussanak.
        /// </summary>
        /// <returns>A felparaméterezett objektum.</returns>
        public static Contentum.eBusinessDocuments.ExecParam GetExecParam()
        {
            Contentum.eBusinessDocuments.ExecParam execParam = new Contentum.eBusinessDocuments.ExecParam();
            execParam.Felhasznalo_Id = AD_PARTNEREK_ID;
            execParam.LoginUser_Id = execParam.Felhasznalo_Id;
            execParam.Org_Id = INT_ADSyncService.Org_id;
            return execParam;
        }


        /// <summary>
        /// Tartományi login név előállítása.
        /// </summary>
        /// <param name="loginName">Login név.</param>
        /// <returns>Domain névvel kiegészített login név.</returns>
        internal static string GetFormattedUserNev(string loginName)
        {
            //string userNev;
            //userNev = String.Format("{0}\\{1}", Environment.UserDomainName, loginName);
            //userNev = String.Format("{0}\\{1}", "INFORMATIKA", loginName);
            return String.Format(System.Globalization.CultureInfo.InvariantCulture,
                "{0}\\{1}", USER_DOMAIN_NAME, loginName);
        }
    }
}
