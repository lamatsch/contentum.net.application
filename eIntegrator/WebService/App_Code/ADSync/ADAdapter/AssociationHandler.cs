﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using log4net;
using Contentum.eQuery.BusinessDocuments;
using System.Reflection;
using Contentum.eAdmin.Service;
using Contentum.eIntegrator.Service;

namespace Contentum.eIntegrator.ADAdapter
{
    /// <summary>
    /// Összerendelő record
    /// </summary>
    internal class AssociationHandler
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Logger));
        //private static Contentum.eIntegrator.Service.INT_ExternalIDsService service_ExtIds = eIntegratorService.ServiceFactory.GetINT_ExternalIDsService();
        private static Contentum.eIntegrator.Service.INT_ExternalIDsService service_ExtIds = eIntegratorService.ServiceFactory.GetINT_ExternalIDsService();
       
        private static KRT_PartnerekService service_partnerek = eAdminService.ServiceFactory.GetKRT_PartnerekService();

       

        internal static INT_ExternalIDs GetAssociationByADObjectID(Guid? objectID)
        {
            
            INT_ExternalIDs assocRecord = null;
            Result result = new Result();
            try
            {


                ExecParam execParam = ADAdapterTypes.GetExecParam();


                INT_ExternalIDsSearch search = new INT_ExternalIDsSearch();

                search.Modul_Id.Value = INT_ADSyncService.Modul_id;
                search.Modul_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                search.External_Id.Value = objectID.ToString();
                search.External_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                result =  service_ExtIds.GetAll(execParam, search);

           
                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }

                if (result.Ds != null)
                {
                    if (result.Ds.Tables[0].Rows.Count > 1)
                    {
                        string debugMessage = String.Format("[{0}] Inkonzisztens adatbázis: {1} objectID elemre 1 helyett {2} találat van.",
                            MethodBase.GetCurrentMethod().Name, objectID, result.Ds.Tables[0].Rows.Count);

                        log.Error(debugMessage);
                        throw new ResultException(debugMessage);

                    }
                    else if (result.Ds.Tables[0].Rows.Count == 1)
                    {
                        assocRecord = new INT_ExternalIDs();
                        Utility.LoadBusinessDocumentFromDataRow(assocRecord, result.Ds.Tables[0].Rows[0]);

                    }
                }
            }
            catch (Exception e)
            {
                log.Error(String.Format("[{0}] Hiba az Association record lekérdezésnél! object Id: {1} ", MethodBase.GetCurrentMethod().Name, objectID), e);
                throw new ResultException(result);
            }

            return assocRecord;
        }



        internal static INT_ExternalIDs CreateAssocRecord(
               WrappedADGroup wrappedGroup, string EdokID, ADAdapterTypes.State state)
        {
            log.DebugFormat("[{0}] START AD Szervezet Id: {1}, EdokId: {2}, Státusz: {3}", MethodBase.GetCurrentMethod().Name, wrappedGroup.ADProp.ObjectID, EdokID, state);

            INT_ExternalIDs assocRecord = new INT_ExternalIDs();
            //assocRecord.bEnabled = true;
            assocRecord.External_Id = wrappedGroup.ADProp.ObjectID.ToString();
            assocRecord.External_Group = wrappedGroup.ADProp.GroupType;//ADAdapterTypes.ADType.Group.ToString();
                                                                       //assocRecord.cSubType = wrappedGroup.Adajk.GroupType;
            assocRecord.Modul_Id = INT_ADSyncService.Modul_id;
            //  assocRecord.cStateID = Enum.GetName(typeof(Utility.Enums.State), Utility.Enums.State.OK);
            assocRecord.Edok_ExpiredDate = ADAdapterTypes.MaxDate;
            assocRecord.LastSync = DateTime.Now.ToString();
            assocRecord.Edok_Id = EdokID;
            assocRecord.Edok_Type = ADAdapterTypes.PartnerTipusKod[ADAdapterTypes.ConvertToPartnerTipus(wrappedGroup.ADProp.GroupType)];
            assocRecord.State = state.ToString();
            assocRecord.ErvKezd = DateTime.Now.ToString();
            assocRecord.ErvVege = ADAdapterTypes.MaxDate;

            return assocRecord;
        }

        internal static INT_ExternalIDs CreateAssocRecord(
             WrappedADUser wrappedUser, string EdokID, ADAdapterTypes.State state)
        {
            log.DebugFormat("[{0}] START AD UserId: {1}, EdokId: {2}, Státusz: {3}", MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID, EdokID, state);

            INT_ExternalIDs assocRecord = new INT_ExternalIDs();
            //assocRecord.bEnabled = true;
            assocRecord.External_Id = wrappedUser.ADProp.ObjectID.ToString();
            //assocRecord.External_Group = wrappedUser.ADProp.UserType;//ADAdapterTypes.ADType.Group.ToString();
                                                                     //assocRecord.cSubType = wrappedGroup.Adajk.GroupType;
            assocRecord.External_Group =  ADAdapterTypes.ADType.User.ToString();

            assocRecord.Modul_Id = INT_ADSyncService.Modul_id;
            //  assocRecord.cStateID = Enum.GetName(typeof(Utility.Enums.State), Utility.Enums.State.OK);
            assocRecord.Edok_ExpiredDate = ADAdapterTypes.MaxDate;
            assocRecord.LastSync = DateTime.Now.ToString();
            assocRecord.Edok_Id = EdokID;
            assocRecord.Edok_Type = "Windows";  //Nem függ az AD-s type-tól 
            assocRecord.State = state.ToString();
            assocRecord.ErvKezd = DateTime.Now.ToString();
            assocRecord.ErvVege = ADAdapterTypes.MaxDate;

            return assocRecord;
        }

        /// <summary>
        /// Új kapcsolat létrehozása.
        /// </summary>
        /// <param name="record">Az ASSOC tábla egy leendő rekordja.</param>
        /// <returns>Igaz, ha a művelet sikeres volt.</returns>
        /// <exception cref="DatabaseException">
        /// Adatkapcsolati hiba esetén veti 
        /// az IDataAccessAdapter típusú osztály.
        /// </exception>
        internal static bool InsertAssociation(INT_ExternalIDs assocRecord)
        {
            if (assocRecord == null)
                return false;

 
            bool succeeded = false;

            try
            {
                //                INT_ExternalIDsService service_ExternalIDs = new INT_ExternalIDsService(dataContext);

                // INT_ExternalIDs newAssocRecord = new INT_ExternalIDs();

                Result result = service_ExtIds.Insert(ADAdapterTypes.GetExecParam(), assocRecord);

                succeeded = String.IsNullOrEmpty(result.ErrorCode);

                log.DebugFormat("[{0}] AssocRec rögzítve! AD UserId: {1}, EdokId: {2}, AssocRec: {3}",
                    MethodBase.GetCurrentMethod().Name, assocRecord.External_Id, assocRecord.Edok_Id, result.Uid);
            }
            catch (Exception e)
            {
                log.Error(string.Format("[{0}] Hiba az AssocRec rögzítésekor! AD UserId: {1}, EdokId: {2}", 
                    MethodBase.GetCurrentMethod().Name,assocRecord.External_Id, assocRecord.Edok_Id), e);
                //throw new Exception(string.Format("[{0}] Hiba az AssocRec rögzítésekor! AD UserId: {1}, EdokId: {2}",
                //    MethodBase.GetCurrentMethod().Name, assocRecord.External_Id, assocRecord.Edok_Id));
            }

    
            return succeeded;
        }


        /// <summary>
        /// Kapcsolórekord végleges törlése.
        /// </summary>
        /// <param name="id">Az ASSOC tábla egy rekordazonostója.</param>
        /// <returns>Igaz, ha a törlés sikeres volt.</returns>
        /// <exception cref="DatabaseException">
        /// Adatkapcsolati hiba esetén veti 
        /// az IDataAccessAdapter típusú osztály.
        /// </exception>
        internal static bool DeleteAssociation(String id)
        {
            log.Debug("[DeleteAssociation] Start");

            bool succeeded = false;

            try
            {
                ExecParam execParam = ADAdapterTypes.GetExecParam();
                execParam.Record_Id = id;

                Result result = service_ExtIds.Delete(execParam);

                succeeded = String.IsNullOrEmpty(result.ErrorCode);

            }
            catch (Exception e)
            {
                log.Error(string.Format("[{0}] Hiba az AssocRec invalidálásnál! AssocRec: {1}",
                    MethodBase.GetCurrentMethod().Name,  id), e);
            }
            log.DebugFormat("[{0}] AssocRec törölve! AssocRec: {1}", MethodBase.GetCurrentMethod().Name, id);
            return succeeded;
        }


        /// <summary>
        /// Kapcsolat módosítása.
        /// </summary>
        /// <param name="record">Az ASSOC tábla egy leendő rekordja.</param>
        /// <returns>Igaz, ha a művelet sikeres volt.</returns>
        /// <exception cref="DatabaseException">
        /// Adatkapcsolati hiba esetén veti 
        /// az IDataAccessAdapter típusú osztály.
        /// </exception>
        internal static bool UpdateAssociation(INT_ExternalIDs assocRecord)
        {
            if (assocRecord == null)
                return false;

            //log.DebugFormat("[0] Start Id: {1}", MethodBase.GetCurrentMethod().Name, assocRecord.Id);

            bool succeeded = false;

            try
            {

              
                ExecParam exp = ADAdapterTypes.GetExecParam();
                exp.Record_Id = assocRecord.Id;
                Result result = service_ExtIds.Update(exp, assocRecord);

                succeeded = String.IsNullOrEmpty(result.ErrorCode);

            }
            catch (Exception e)
            {
                log.Error(String.Format("[{0}] Módosítás hiba", MethodBase.GetCurrentMethod().Name), e);
                //throw new Exception(String.Format("[{0}] Módosítás hiba", MethodBase.GetCurrentMethod().Name));
            }

            log.DebugFormat("[{0}] Módosított Id: {1}", MethodBase.GetCurrentMethod().Name, assocRecord.Id);
            return succeeded;
        }

    }

}
