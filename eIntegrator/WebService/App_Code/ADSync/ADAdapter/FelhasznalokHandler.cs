﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;


namespace Contentum.eIntegrator.ADAdapter
{
    /// <summary>
    /// Summary description for Class1
    /// </summary>
    internal static class FelhasznalokHandler
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Logger));
        private static KRT_FelhasznalokService service_felhasznalok = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        private static KRT_SzerepkorokService service_szerepkorok = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
        private static KRT_Felhasznalo_SzerepkorService service_felh_szerepkor = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();

        //  private static INT_ExternalIDsService service_ExtIds = INT_ExternalIDsService.ServiceFactory.GetINT_ExternalIDsService();

        /// <summary>
        /// Új felhasználó rögzítése.
        /// </summary>
        /// <param name="wrappedUser">A felhasználó adatai.</param>
        /// <returns>A kapott egyedi azonosító.</returns>
        public static string InsertFelhasznalo(WrappedADUser wrappedUser)
        {
            //log.DebugFormat("[0] Start AD UserId: {1}", MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID);

            ExecParam execParam = ADAdapterTypes.GetExecParam();

            KRT_Felhasznalok edokFelhasznalo = new KRT_Felhasznalok();

            MapToKrtFelhasznalok(ref edokFelhasznalo, wrappedUser);

            Result result = service_felhasznalok.InsertAndCreateNewPartner(execParam, edokFelhasznalo);

            if (result.ErrorCode == null)
            {
                wrappedUser.EdokProp.ID = result.Uid;
                // Jelenleg nem használt
                //SetRolesInEdok(wrappedUser);

                SetPartnerKapcsolatokOfUser(wrappedUser);

                log.DebugFormat("[{0}] Felhasználó rögzítése {1}. AD UserId: {2}, Edok UserId: {3} ",
                    MethodBase.GetCurrentMethod().Name, wrappedUser.FullName, wrappedUser.ADProp.ObjectID, wrappedUser.EdokProp.ID);
                return result.Uid;
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a felhasználó rögzítésénél! AD UserId: {1}, Error kód: {2}, Error: {3}", MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID, result.ErrorCode, result.ErrorMessage);

                return null;
            }
        }


        /// <summary>
        /// KRT_Felhasznalok - User object mező összerendelés
        /// </summary>
        /// <param name="edokFelhasznalo"></param>
        /// <param name="wrappedUser"></param>
        private static void MapToKrtFelhasznalok(ref KRT_Felhasznalok edokFelhasznalo, WrappedADUser wrappedUser)
        {

            edokFelhasznalo.Nev = wrappedUser.FullName;
            edokFelhasznalo.EMail = wrappedUser.Mail == null ? String.Empty : wrappedUser.Mail;
            edokFelhasznalo.Beosztas = wrappedUser.Assignment == null ? String.Empty : wrappedUser.Assignment;
            edokFelhasznalo.Telefonszam = wrappedUser.WorkPhone == null ? String.Empty : wrappedUser.WorkPhone;
            if (wrappedUser.Expired == null)
                edokFelhasznalo.ErvVege = ADAdapterTypes.MaxDate;
            else
                edokFelhasznalo.ErvVege = ((DateTime)wrappedUser.Expired).ToString();
            edokFelhasznalo.Engedelyezett = wrappedUser.IsActive ? "1" : "0";
            if (wrappedUser.LoginName.IndexOf('\\') > 0)
                edokFelhasznalo.UserNev = wrappedUser.LoginName;
            else
                edokFelhasznalo.UserNev = ADAdapterTypes.GetFormattedUserNev(wrappedUser.LoginName);
            edokFelhasznalo.Tipus = "Windows";
            edokFelhasznalo.Org = INT_ADSyncService.Org_id;
            //edokFelhasznalo.Forras = ADAdapterTypes.AD_FORRAS;  //KRT_Felhasznalo-ban nincs Forras, így nem adható meg

            edokFelhasznalo.Updated.SetValueAll(true);
            edokFelhasznalo.Base.Updated.SetValueAll(true);
        }

        /// <summary>
        /// User object - KRT_Felhasznalok mező összerendelés
        /// </summary>
        /// <param name="wrappedUser"></param>
        /// <param name="edokUser"></param>
        internal static void MapToWrappedUser(ref WrappedADUser wrappedUser, KRT_Felhasznalok edokUser)
        {
            if (edokUser == null) return;

            if (wrappedUser == null)
                wrappedUser = new WrappedADUser();

            wrappedUser.FullName = edokUser.Nev;
            wrappedUser.LoginName = edokUser.UserNev;
            wrappedUser.Mail = edokUser.EMail;
            wrappedUser.EdokProp.OriginalBusinessDocument = edokUser;
            wrappedUser.EdokProp.ID = edokUser.Id;
            wrappedUser.LoginName = edokUser.UserNev;
            wrappedUser.WorkPhone = edokUser.Telefonszam;
            wrappedUser.Assignment = edokUser.Beosztas;

            SetGroupRelationsOfWrappedUser(wrappedUser, edokUser); // groupmembership és managed group beállítása
            SetValidityFlagsOfWrappedUser(ref wrappedUser, edokUser);  // enabled, deleted, locked, expired beállítása

            // Jelenleg nem használjuk
            //wrappedUser.RoleMembership = GetWrappedADRoles(edokUser.Id);
            //WrappedADRole defaultRole = new WrappedADRole(GetDefaultRoleName());
            //wrappedUser.RoleMembership.Remove(defaultRole);
        }

        /// <summary>
        /// Default Role név, amit minden userhez hozzárendelünk
        /// Jelenleg nem használt
        /// </summary>
        /// <returns></returns>
        private static string GetDefaultRoleName()
        {
            //if (defaultSzerepkorNev == null)
            //{
            //try
            //{
            //    KRT_ParameterekService service = new KRT_ParameterekService(eAdminWebserviceURL + "KRT_ParameterekService.asmx");
            //    service.Credentials = System.Net.CredentialCache.DefaultCredentials;
            //    ExecParam xpm = Util.GetExecParam();
            //    KRT_ParameterekSearch search = new KRT_ParameterekSearch();
            //    search.Nev.Value = "DEFAULT_SZEREPKOR";
            //    search.Nev.Operator = Query.Operators.equals;
            //    Result res = service.GetAll(xpm, search);
            //    if (!res.IsError && res.Ds.Tables[0].Rows.Count == 1)
            //    {
            //        string defaultSzerepkorNev = res.Ds.Tables[0].Rows[0]["Ertek"].ToString();
            //    }
            //    else
            //    {
            //        //hiba vagy nem talalhato
            //    }
            //}
            //catch (Exception)
            //{
            //defaultSzerepkorNev = "FPH_LEKERDEZO";
            //}

            //ExecParam execParam = Util.GetExecParam();
            //defaultSzerepkorNev = Contentum.eUtility.Rendszerparameterek.Get(execParam, "DEFAULT_SZEREPKOR");
            //}
            return ADAdapterTypes.DEFAULT_SZEREPKOR;
        }


        /// <summary>
        /// Szervezeti tagság és Menedzselt szervezet beállítása
        /// </summary>
        /// <param name="wrappedUser"></param>
        /// <param name="edokUser"></param>
        private static void SetGroupRelationsOfWrappedUser(WrappedADUser wrappedUser, KRT_Felhasznalok edokUser)
        {

            wrappedUser.GroupMembership = PartnerHandler.GetWrappedADGroupMembershipOfEdokUser(edokUser);

            // TODO: ide elég lenne egy olyan metódus, ami a managedGroup ID-t adja
            // vissza, ezzel felhasználónként egy webservice hívást spórolhatnánk a 
            // GetManagedGroupOfEdokUser metódusban.
            KRT_Partnerek managedGroup = PartnerHandler.GetManagedGroupOfEdokUser(edokUser);
            wrappedUser.EdokProp.ManagedGroupID = managedGroup == null ? null : managedGroup.Id;

        }

        /// <summary>
        /// A felhasználó objektumban beállított szerepkörök tárolása az EDOK-ban.
        /// Jelenleg nem használt
        /// </summary>
        /// <param name="wrappedUser">
        /// A szerepkörlistával és EDOK felhasználóazonosítóval 
        /// felparaméterezett felhasznló-objektum.
        /// </param>
        public static void SetRolesInEdok(WrappedADUser wrappedUser)
        {

            WrappedADRoleCollection wrappedEdokRoles = GetWrappedADRoles(wrappedUser.EdokProp.ID);
            AddNewRolesOfUser(wrappedUser.RoleMembership, wrappedEdokRoles, wrappedUser.EdokProp.ID);
            // TODO: Pethő Pali kérésére ideiglenesen eltávolítva TAG 20071204
            RemoveAdditionallyStoredRolesOfUser(wrappedUser.RoleMembership, wrappedEdokRoles, wrappedUser.EdokProp.ID);

            log.DebugFormat("[0] END UserId: {1}", MethodBase.GetCurrentMethod().Name, wrappedUser.EdokProp.ID);

        }

        /// <summary>
        /// A felhasználó EDOK-ban érvényes szerepköreinek lekérdezése.
        /// Jelenleg nem használt
        /// </summary>
        /// <param name="edokUserId">A kérdéses felhasználó.</param>
        /// <returns>Az eredmény WrappedAdajkRole kollekcióban, vagy üres kollekció.</returns>
        public static WrappedADRoleCollection GetWrappedADRoles(string edokUserID)
        {

            WrappedADRoleCollection wrappedRoles = new WrappedADRoleCollection();

            KRT_Felhasznalok felhasznalo = GetFelhasznalo(edokUserID);

            KRT_Felhasznalo_SzerepkorSearch criteria = new KRT_Felhasznalo_SzerepkorSearch();
            criteria.Felhasznalo_Id.Value = edokUserID;
            criteria.Felhasznalo_Id.Operator = Contentum.eQuery.Query.Operators.equals;


            System.Data.DataSet szerepkorok = null;
            Result result = service_szerepkorok.GetAllByFelhasznalo(ADAdapterTypes.GetExecParam(), felhasznalo, criteria);

            if (result.ErrorCode == null)
            {
                szerepkorok = result.Ds;

                if (result.Ds != null
                    && result.Ds.Tables.Count > 0
                    && result.Ds.Tables[0].Rows.Count > 0)
                {
                    foreach (System.Data.DataRow szerepkor in result.Ds.Tables[0].Rows)
                    {
                        WrappedADRole wrappedRole = new WrappedADRole(szerepkor["Szerepkor_Nev"].ToString());
                        wrappedRole.EdokProp.ID = szerepkor["Szerepkor_Id"].ToString();
                        wrappedRoles.Add(wrappedRole);
                    }
                }

            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a felhasználó szerepköreinek lekérésénél! UserId: {1}, Error kód: {2}, Error: {3}", MethodBase.GetCurrentMethod().Name, edokUserID, result.ErrorCode, result.ErrorMessage);
            }

            return wrappedRoles;
        }


        /// <summary>
        /// Felhasználó lekérdezése EDOK-ból.
        /// </summary>
        /// <param name="edokUserId">EDOK-azonosító.</param>
        /// <returns>A kért felhasználó adatai.</returns>
        /// A hivatkozott rekord nem létezik.
        /// </exception>
        internal static KRT_Felhasznalok GetFelhasznalo(string edokUserID)
        {

            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = edokUserID;

            KRT_Felhasznalok felhasznalo = null;

            Result result = service_felhasznalok.Get(execParam);
            // TODO: hibakódot javítani ErrorMessage-ra!
            if (result.ErrorCode == null || result.ErrorCode == "-2146232060")   // "Rekord lekérése az adatbázisból sikertelen", vagyis: az ID alapján kért rekord nem létezik
            {
                felhasznalo = (KRT_Felhasznalok)result.Record;
                if (felhasznalo == null)
                {
                    if (felhasznalo == null)
                    {
                        log.ErrorFormat("[{0}] Felhasználó nem található! Felhasználó Id: {1}", MethodBase.GetCurrentMethod().Name, edokUserID);
                    }
                }
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a felhasználó lekérésnél! UserId: {1}, Error kód: {2}, Error: {3}", MethodBase.GetCurrentMethod().Name, edokUserID, result.ErrorCode, result.ErrorMessage);
            }

            return felhasznalo;
        }


        /// <summary>
        /// Már létező delhasználó aktuális adatainak tárolása, ill. inaktív felhasználó reaktiválása.
        /// </summary>
        /// <param name="wrappedUser">Felhasználó adatai.</param>
        /// <returns>Igaz, ha a művelet sikeres volt.</returns>
        public static bool UpdateFelhasznalo(WrappedADUser wrappedUser)
        {
            if (String.IsNullOrEmpty(wrappedUser.EdokProp.ID))
            {
                log.ErrorFormat("[{0}] Nincs megadva Felhasználó Id ! AD Felhasználó Id: {1}", MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID);

            }
            KRT_Felhasznalok edokUser = GetFelhasznalo(wrappedUser.EdokProp.ID);

            if (FelhasznaloRevalidationRequired(wrappedUser, edokUser))
            {
                RevalidateFelhasznalo(edokUser);
                // a Base.Ver a revalidálás után nem egyezne, ezért újra lekérdezzük
                edokUser = GetFelhasznalo(wrappedUser.EdokProp.ID);
            }

            //verziót be kell állítani az update előtt:
            //csak olyan verziójú rekord updatelhető, amelynek a verziója megegyezik a beállított verzióval
            string version = edokUser.Base.Ver;
            MapToKrtFelhasznalok(ref edokUser, wrappedUser);
            edokUser.Base.Ver = version;
            edokUser.Base.Updated.Ver = true;

            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = wrappedUser.EdokProp.ID;

            Result result = service_felhasznalok.Update(execParam, edokUser);
            if (result.ErrorCode == null)
            {
                SetPartnerKapcsolatokOfUser(wrappedUser);
                log.DebugFormat("[{0}] Felhasználó és szervezeteinek módosítása {1} Felhasználó Id: {2}", MethodBase.GetCurrentMethod().Name, wrappedUser.FullName, wrappedUser.EdokProp.ID);
                return true;
            }
            {
                log.ErrorFormat("[{0}] Hiba a felhasználó módosításnál! Felhasználó Id: {1}, Errorkód: {2}, Error: {3}"
                    , MethodBase.GetCurrentMethod().Name, wrappedUser.EdokProp.ID, result.ErrorCode, result.ErrorMessage);
                return false;
            }

            //if (Util.SzerepkorokTableUpdateRequired(wrappedUser.ChangeIndicator))
            //    using (SzerepkorokHandler szerepkorokHandler = _ServiceHandlerFactory.CreateSzerepkorokHandler())
            //        szerepkorokHandler.SetRolesInEdok(wrappedUser);
            // Jelenleg nem használt
            //SetRolesInEdok(wrappedUser);

            //    using (PartnerekHandler handler = _ServiceHandlerFactory.CreatePartnerekHandler())
            //        handler.SetPartnerKapcsolatokOfUser(wrappedUser);



        }

        /// <summary>
        /// Felhasználó adatainak érvénytelenítése.
        /// </summary>
        /// <param name="edokUserId">Felhasználó-azonosító.</param>
        /// <returns></returns>
        public static bool InvalidateFelhasznalo(string edokUserId)
        {

            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = edokUserId;
            Result result = service_felhasznalok.Invalidate(execParam);
            if (result.ErrorCode != null)
            {
                log.ErrorFormat("[{0}] Hiba a felhasználó érvénytelenítésnél! UserId: {1}, Errorkód: {2}, Error: {3}"
      , MethodBase.GetCurrentMethod().Name, edokUserId, result.ErrorCode, result.ErrorMessage);
                return false;

            }
            else
            {
                log.DebugFormat("[{0}] Felhasználó érvénytelenítése. UserId: {1}", MethodBase.GetCurrentMethod().Name, edokUserId);
                return true;
            }

        }


        /// <summary>
        /// Elemzi, hogy az AD-ból érkező adatok alapján újra érvényesített felhasználóról
        /// van-e szó, s ha igen, szükség van-e az EDOK-beli revalidálására.
        /// </summary>
        /// <param name="wrappedUser">Címtárbeli állapotot hordozó user objektum.</param>
        /// <param name="edokUser">EDOK-beli állapotot hordozó user objektum.</param>
        /// <returns>Igaz, ha revalidálás szükséges az EDOK-ban.</returns>
        internal static bool FelhasznaloRevalidationRequired(WrappedADUser wrappedUser,
        Contentum.eBusinessDocuments.KRT_Felhasznalok edokUser)
        {
            if (wrappedUser == null || edokUser == null)
                return false;

            bool result = false;

            if (wrappedUser.IsActive)
            {
                DateTime ervVege;
                DateTime.TryParse(edokUser.ErvVege, out ervVege);
                result = ervVege < DateTime.Now;
            }

            return result;
        }

        /// <summary>
        /// Felhasználó újraérvényesítése
        /// </summary>
        /// <param name="edokUser"></param>
        private static void RevalidateFelhasznalo(KRT_Felhasznalok edokUser)
        {

            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = edokUser.Id;
            Result result = service_felhasznalok.Revalidate(execParam);
            if (result.ErrorCode != null)
            {
                log.ErrorFormat("[{0}] Hiba a felhasználó újraérvényesítésénél! UserId: {1}, Errorkód: {2}, Error: {3}"
                    , MethodBase.GetCurrentMethod().Name, edokUser.Id, result.ErrorCode, result.ErrorMessage);
            }
            else
            {
                log.DebugFormat("[{0}] Felhasználó újraérvényesítése UserId: {1}", MethodBase.GetCurrentMethod().Name, edokUser.Id);

            }
        }

        /// <summary>
        /// Új Szerepkör hozzárendelés
        /// Jelenleg nem használt
        /// </summary>
        /// <param name="requiredRoleMembership"></param>
        /// <param name="currentRoleMembership"></param>
        /// <param name="edokUserID"></param>
        private static void AddNewRolesOfUser(WrappedADRoleCollection requiredRoleMembership, WrappedADRoleCollection currentRoleMembership, string edokUserID)
        {
            for (int i = 0; i < requiredRoleMembership.Count; i++)
            {
                bool hasThisRoleSet = false;

                foreach (WrappedADRole wrappedEdokRole in currentRoleMembership)
                {
                    if (wrappedEdokRole.Name == requiredRoleMembership[i].Name)
                    {
                        hasThisRoleSet = true;
                        break;
                    }
                }

                if (!hasThisRoleSet)
                {
                    SetEDOKID(requiredRoleMembership[i]);
                    GrantNewRole(edokUserID, requiredRoleMembership[i]);
                }
            }
        }

        /// <summary>
        /// Érvénytelen szerepkörhozzárendelések törlése
        /// Jelenleg nem használt
        /// </summary>
        /// <param name="requiredRoleMembership"></param>
        /// <param name="currentRoleMembership"></param>
        /// <param name="edokUserID"></param>
        private static void RemoveAdditionallyStoredRolesOfUser(WrappedADRoleCollection requiredRoleMembership,
            WrappedADRoleCollection currentRoleMembership, string edokUserID)
        {
            for (int i = 0; i < currentRoleMembership.Count; i++)
            {
                bool isThisRoleRequired = false;

                foreach (WrappedADRole wrappedRequiredRole in requiredRoleMembership)
                {
                    if (wrappedRequiredRole.Name == currentRoleMembership[i].Name)
                    {
                        isThisRoleRequired = true;
                        break;
                    }
                }

                if (!isThisRoleRequired)
                {
                    SetEDOKID(currentRoleMembership[i]);
                    DenyRole(edokUserID, currentRoleMembership[i].EdokProp.ID);
                }
            }
        }

        /// <summary>
        /// Szerepkörök Edok Id-jának beállítása
        /// Jelenleg nem használt
        /// </summary>
        /// <param name="wrappedRole"></param>
        private static void SetEDOKID(WrappedADRole wrappedRole)
        {

            KRT_SzerepkorokSearch criteria = new KRT_SzerepkorokSearch();
            criteria.Nev.Value = wrappedRole.Name;
            criteria.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = service_szerepkorok.GetAll(ADAdapterTypes.GetExecParam(), criteria);

            if (result.ErrorCode == null)
            {
                if (result.Ds != null)
                    if (result.Ds.Tables[0].Rows.Count == 1)
                        wrappedRole.EdokProp.ID = result.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            else
            {
                log.ErrorFormat("[{0}] Hiba a szerepkörök beállításánál! Role: {1}, Error kód: {2}, Error: {3}", MethodBase.GetCurrentMethod().Name, wrappedRole.Name, result.ErrorCode, result.ErrorMessage);
            }

        }

        /// <summary>
        /// Szerepkör felhasználóhoz rendelése
        /// Jelenleg nem használt
        /// </summary>
        /// <param name="edokUserID"></param>
        /// <param name="wrappedRole"></param>
        private static void GrantNewRole(string edokUserID, WrappedADRole wrappedRole)
        {

            KRT_Felhasznalo_Szerepkor roleAssoc = new KRT_Felhasznalo_Szerepkor();
            roleAssoc.Felhasznalo_Id = edokUserID;
            roleAssoc.Szerepkor_Id = wrappedRole.EdokProp.ID;

            Result result = service_felh_szerepkor.Insert(ADAdapterTypes.GetExecParam(), roleAssoc);
            if (result.ErrorCode != null)
            {
                log.ErrorFormat("[{0}] Hiba a szerepkörök felhasználókhoz rebdelésekor! UserId: {1},  Role: {2}, Error kód: {3}, Error: {4}", MethodBase.GetCurrentMethod().Name, edokUserID, wrappedRole.Name, result.ErrorCode, result.ErrorMessage);
            }

            log.DebugFormat("[{0}] Szerepkör hozzárendelve Edok UserId: {1}, Role: {2}", MethodBase.GetCurrentMethod().Name, edokUserID, wrappedRole.Name);
        }

        /// <summary>
        /// Szerepkör megvonása
        /// Jelenleg nem használt
        /// </summary>
        /// <param name="edokUserID"></param>
        /// <param name="idOfRoleToDeny"></param>
        private static void DenyRole(string edokUserID, string idOfRoleToDeny)
        {

            KRT_Felhasznalo_SzerepkorSearch criteria = new KRT_Felhasznalo_SzerepkorSearch();
            criteria.Szerepkor_Id.Value = idOfRoleToDeny;
            criteria.Szerepkor_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            criteria.Felhasznalo_Id.Value = edokUserID;
            criteria.Felhasznalo_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = service_felh_szerepkor.GetAll(ADAdapterTypes.GetExecParam(), criteria);
            if (result.ErrorCode == null)
            {
                if (result.Ds != null
                    && result.Ds.Tables.Count > 0
                    && result.Ds.Tables[0].Rows.Count > 0)
                {
                    bool isErrorFree = true;
                    ExecParam execParam = ADAdapterTypes.GetExecParam();
                    foreach (System.Data.DataRow szerepkor in result.Ds.Tables[0].Rows)
                    {
                        execParam.Record_Id = szerepkor["Id"].ToString();
                        result = service_felh_szerepkor.Invalidate(execParam);
                        isErrorFree = isErrorFree && (result.ErrorCode == null);
                    }
                    if (isErrorFree)
                    {
                        log.DebugFormat("[{0}] Szerepkör hozzárendelés megszüntetve. Edok UserId: {1}, Role: {2}", MethodBase.GetCurrentMethod().Name, edokUserID, idOfRoleToDeny);
                        return;
                    }
                }
            }
            // hibaág
            log.ErrorFormat("[{0}] Hiba a szerepkörök törlésekor! UserId: {1},  Role: {2}, Error kód: {3}, Error: {4}", MethodBase.GetCurrentMethod().Name, edokUserID, idOfRoleToDeny, result.ErrorCode, result.ErrorMessage);

        }

        /// <summary>
        /// A paraméterben kapott értékekkel szinkronizálja a felhasználó
        /// EDOK-beli reprezentációjának szervezeti egység kapcsolatait,
        /// valamint a menedzselt szervezeti egységgel való viszonyát.
        /// </summary>
        /// <param name="wrappedUser">Felhasználó adatai, az aktuális ADAJK-állapot szerint.</param>
        internal static void SetPartnerKapcsolatokOfUser(WrappedADUser wrappedUser)
        {

            string usersPartnerID = GetFelhasznalo(wrappedUser.EdokProp.ID).Partner_id;

            System.Data.DataSet partnerekOfUser = PartnerHandler.GetGroupMembershipOfUser(wrappedUser.EdokProp.ID, usersPartnerID);

            // TODO: 2008.04.02-én kikommentezve, mert jelenleg az álláspont:
            // Az org mellett nem-org tagságú user-ek láthatják mindekét szervezet
            // iktatókönyvét, az Org-osban ui. úgy sincs semmi ezen esetekben.
            // Pl. a Bizottsági Alosztályra úgysem iktat a Hivatal.
            //HandleMultipleGroupMembershipInterference(ref wrappedUser);

            AddNewGroupmembersOfUser(wrappedUser.GroupMembership, usersPartnerID, partnerekOfUser);

            //Util.WriteToTraceFile(wrappedUser.LoginName);
            PartnerHandler.RemoveAdditionallyStoredGroupmembersOfUser(wrappedUser.GroupMembership, partnerekOfUser);

            if (wrappedUser.ADProp.ManagedGroupCount > 0)
                PartnerHandler.SetManagedGroupOfEdokUser(usersPartnerID, wrappedUser.EdokProp.ManagedGroupID);

        }



        /// <summary>
        /// Uj Partnerkapcsolat felvitele
        /// </summary>
        /// <param name="groupMembership"></param>
        /// <param name="edokUserPartnerID"></param>
        /// <param name="partnerekOfUser"></param>
        private static void AddNewGroupmembersOfUser(WrappedADGroupCollection groupMembership, string edokUserPartnerID, System.Data.DataSet partnerekOfUser)
        {
            for (int i = 0; i < groupMembership.Count; i++)
            {
                bool hasThisGroupSet = false;
                foreach (System.Data.DataRow partnerRow in partnerekOfUser.Tables[0].Rows)
                {
                    string groupEdokID = partnerRow["Partner_id_kapcsolt"].ToString();
                    if (groupMembership[i].EDOKProp.ID.ToUpperInvariant() == groupEdokID.ToUpperInvariant())
                    {
                        hasThisGroupSet = true;
                        break;
                    }
                }
                if (!hasThisGroupSet)
                {
                    PartnerHandler.InsertNewPartnerKapcsolatok(edokUserPartnerID, groupMembership[i].EDOKProp.ID,
                        ADAdapterTypes.PartnerkapcsolatTipus.FelhasznaloSzervezete);
                }
            }
        }

        /// <summary>
        ///  enabled, deleted, locked, expired beállítása
        /// </summary>
        /// <param name="wrappedUser"></param>
        /// <param name="edokUser"></param>
        private static void SetValidityFlagsOfWrappedUser(ref WrappedADUser wrappedUser, KRT_Felhasznalok edokUser)
        {
            DateTime expiration = DateTime.Parse(edokUser.ErvVege);

            if (expiration >= DateTime.Now)
            {
                wrappedUser.Enabled = true;
                wrappedUser.Deleted = false;
                wrappedUser.Locked = false;
            }
            else
            {
                wrappedUser.Enabled = false;
                wrappedUser.Deleted = false;
                wrappedUser.Locked = true;
            }

            if (expiration > new DateTime(4000, 1, 1))
                wrappedUser.Expired = null;
            else
                wrappedUser.Expired = expiration;

            wrappedUser.Enabled = wrappedUser.Enabled && edokUser.Engedelyezett == "1";
        }

    }
}
