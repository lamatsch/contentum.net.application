﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.ADAdapter
{

    //#region ChangedUserIndicator enum

    ///// <summary>
    ///// Használatával jelölhető, hogy a személy adataiban
    ///// milyen jellegű változás következett be.
    ///// </summary>
    //public enum ChangedUserIndicator
    //{
    //    /// <summary>
    //    /// A ChangedUserIndicator jelző még nincs beállítva.
    //    /// </summary>
    //    Unevaulated = 1,
    //    /// <summary>
    //    /// A kiértékelés alapján nincs változás
    //    /// a személy adataiban.
    //    /// </summary>
    //    Unchanged,
    //    /// <summary>
    //    /// Az egyéb enumerálttal nem jelzett
    //    /// tulajdonságokban történt változás.
    //    /// </summary>
    //    Properties,
    //    /// <summary>
    //    /// Változás történt a személy szerepköreiben.
    //    /// </summary>
    //    RoleMembership,
    //    /// <summary>
    //    /// Változás történt a személy szervezeti
    //    /// egységekhez rendelésében.
    //    /// </summary>
    //    GroupMembership,
    //    /// <summary>
    //    /// Az account-ot törölték.
    //    /// </summary>
    //    Deleted,
    //    /// <summary>
    //    /// Az account-ot zároltságát megváltoztatták.
    //    /// </summary>
    //    Locked,
    //    /// <summary>
    //    /// Az account engedélyezettsége megváltozott.
    //    /// </summary>
    //    Enabled,
    //    /// <summary>
    //    /// Az érvénytelenítési idő megváltozott.
    //    /// </summary>
    //    Expiration,
    //    /// <summary>
    //    /// A személy által vezetett szervezeti egység megváltozott.
    //    /// </summary>
    //    ManagedGroup,
    //    /// <summary>
    //    /// A személy neve módosult.
    //    /// </summary>
    //    Name,
    //    /// <summary>
    //    /// A személy munkaköri beosztása módosult.
    //    /// </summary>
    //    WorkAssignment,
    //    /// <summary>
    //    /// A személy munkahelyi telefonszáma módosult.
    //    /// </summary>
    //    WorkPhone,
    //    /// <summary>
    //    /// A személy email címe módosult.
    //    /// </summary>
    //    MailAddress,
    //    /// <summary>
    //    /// A személy loginneve módosult.
    //    /// </summary>
    //    LoginName
    //}

    //#endregion ChangedUserIndicator


    /// <summary>
    /// Az AD User dokumentum helyi megfelelője (szerializálható, és csak a 
    /// lényeges dolgok vannak benne)
    /// </summary>
    [Serializable]
	[XmlRootAttribute("ADUser", IsNullable = false)] //, Namespace=Constants.C_NameSpaceURI
	public sealed class WrappedADUser
    {

        public sealed class ADProperties
        {
            internal ADProperties() {}
            /// <summary>ObjectID (AD-beli azonosító).</summary>
            public Guid? ObjectID;
            /// <summary>
            /// Amennyiben a felhasználó vezető, akkor az általa
            /// vezetett szervezeti egység egyedi azonosítója.
            /// </summary>
            public Guid? ManagedGroupID;
            /// <summary>
            /// Amennyiben a felhasználó vezető, és több egységet
            /// is vezet, akkor értéke 1-nél nagyobb.
            /// </summary>
            public int ManagedGroupCount;
            /// <summary>
            /// Felhasználótípus neve, pl. Internal.
            /// </summary>
            public string UserType;
        }

        public sealed class EDOKProperties
        {
            internal EDOKProperties()
            {
            }
            /// <summary>
            /// A személy azonosítója a külső rendszerben.
            /// </summary>
            public string ID;
            /// <summary>
            /// Amennyiben a felhasználó vezető, akkor az általa
            /// vezetett szervezeti egység egyedi azonosítója
            /// a szinkronizált rendszerben.
            /// </summary>
            public string ManagedGroupID;
            /// <summary>
            /// A kapcsolódó rendszer eredeti üzleti dokumentum objektuma,
            /// amelynek csomagolásával ez az objektum létrejött.
            /// </summary>
            public Object OriginalBusinessDocument;
        }

        #region Fields

        private string _FullName = null;
        private string _FirstName = null;
        private string _FamilyName = null;
        private string _MiddleName = null;
        private string _Titulus = null;
        private string _LoginName = null;
		private string _Mail = null;
        private string _Assignment = null; // beosztás
        private string _WorkPhone = null; // munkahelyi telefonszám
        private bool _Deleted = false;
        private bool _Locked = false;
        private bool _Enabled = true;
        private DateTime? _Expired = null;
        /// <summary>Engedélyezett műveletek köre.</summary>
		private int[] _AllowedOperations = null;
		/// <summary>Mikor kérték le utoljára az AD-ból.</summary>
		private DateTime _LastADCheckTime = DateTime.MinValue;
		/// <summary>A felhasználóhoz rendelt szerepkörök az alkalmazásban.</summary>
        private WrappedADRoleCollection _Roles = new WrappedADRoleCollection();
		/// <summary>Felhasználó szervezeti egység-tagsága.</summary>
        private WrappedADGroupCollection _Groups = new WrappedADGroupCollection();

        private ADProperties _ADProperties = new ADProperties();
        private EDOKProperties _EDOKProperties = new EDOKProperties();

		#endregion

		#region Constructors

		/// <summary>
		/// Public konstruktor. Kell a vissza-szerializáláshoz.
		/// </summary>
		public WrappedADUser()
        {
            _ADProperties.ObjectID = null;
        }

        /// <summary>
        /// UserExtended-es másolós konstruktor.
        /// </summary>
        /// <param name="user"></param>
        public WrappedADUser(UserPrincipal ADUser)
        {
  
            if (ADUser != null)
            {
                _ADProperties.ObjectID = ADUser.Guid.Value;
                _FullName = ADUser.DisplayName;
                _FirstName = ADUser.GivenName;
                // _ADProperties.UserType = ADUser.UserTypeRef; //kivülről kerül beállításra
               
                _FamilyName = ADUser.Surname;
                _MiddleName = ADUser.MiddleName;
                //  _Titulus = ADUser.Initials;
                _LoginName = ADUser.UserPrincipalName;
                _Deleted = false;
                _Enabled = ADUser.Enabled.Value;
                _Locked = ADUser.IsAccountLockedOut();
                _Mail = ADUser.EmailAddress;
                //  _Assignment = ADUser.o;
                _WorkPhone = ADUser.VoiceTelephoneNumber;

                _LastADCheckTime = ADUser.LastLogon == null ? DateTime.MinValue : (DateTime)ADUser.LastLogon.Value;

                if (ADUser.AccountExpirationDate == null)
                    _Expired = null;
                else
                    _Expired = Convert.ToDateTime(ADUser.AccountExpirationDate.Value);
            }
        }
        /// <summary>
        /// XML-es konstruktor, amely beállítja a memberváltozókat is.
        /// </summary>
        /// <param name="node">
        /// A fake konfigurációból származó XML definíció.
        /// </param>
        public WrappedADUser(XmlNode node) 
		{
            SetBasicProperties(node);
            SetAllowedOperations(node);
            SetRoleMembership(node);
            SetGroupMembership(node);
		}

		/// <summary>
		/// WrappedADUser-es másolós konstruktor.
		/// </summary>
		/// <param name="user"></param>
		public WrappedADUser(WrappedADUser user) 
		{
			if (user != null)
			{
				_ADProperties.ObjectID = new System.Guid(user.ADProp.ObjectID.ToString());
                //_ChangeIndicator = user.ChangeIndicator;
                _Deleted = user.Deleted;
                _Enabled = user.Enabled;
                _Expired = user.Expired;
                _FamilyName = user.FamilyName;
                _FirstName = user.FirstName;
                _FullName = user.FullName;
                _Groups = user.GroupMembership;
                _LastADCheckTime = user.LastADCheck;
                _Locked = user.Locked;
                _LoginName = user.LoginName;
                _Mail = user.Mail;
                _ADProperties.ManagedGroupID = user.ADProp.ManagedGroupID;
                _EDOKProperties.ManagedGroupID = user.EdokProp.ManagedGroupID;
                _MiddleName = user.MiddleName;
                _Roles = user.RoleMembership;
                _Titulus = user.Titulus;
                _EDOKProperties.OriginalBusinessDocument = user.EdokProp.OriginalBusinessDocument;
			}
		}

		#endregion

        #region Public members

        public bool IsActive
        {
            get { return !_Deleted && _Enabled && !_Locked && (_Expired == null || _Expired > DateTime.Now); }
        }
	

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (this.GetType() != obj.GetType()) return false;
            if (Object.ReferenceEquals(this, obj)) return true;

            WrappedADUser other = (WrappedADUser)obj;
            bool isChanged = false;
            #region comparsions
            
            if (other.Deleted != this._Deleted)
                isChanged = true;
            if (other.Enabled != this._Enabled)
                isChanged = true;

            if (other.Expired != this._Expired)
                if ((this._Expired == null && other.Expired.Value.Year > 2100)
                   || (other.Expired == null && this._Expired.Value.Year > 2100))
                {
                    // do nothing
                }
                else
                    isChanged = true;  
            if (!other.GroupMembership.Equals(this._Groups))
                isChanged = true;

            if (other.Locked != this._Locked)
                isChanged = true;

            if ((other.EdokProp.ManagedGroupID == null && this._EDOKProperties.ManagedGroupID != null)
                || (other.EdokProp.ManagedGroupID != null && this._EDOKProperties.ManagedGroupID == null))
                isChanged = true;
            else if (other.EdokProp.ManagedGroupID != null)
            {
                if (!other.EdokProp.ManagedGroupID.Equals(this._EDOKProperties.ManagedGroupID, StringComparison.InvariantCultureIgnoreCase))
                    isChanged = true;
            }

            // Jelenleg nem használjuk
            //if (!other.RoleMembership.Equals(this._Roles))
            //    isChanged = true;

            if (other.FullName != this._FullName)
                isChanged = true;

            if (other.Assignment != this._Assignment && (String.IsNullOrEmpty(other.Assignment) && (!String.IsNullOrEmpty(this._Assignment))))
                isChanged = true; 

            if (other.WorkPhone != this._WorkPhone && (String.IsNullOrEmpty(other.WorkPhone) && (!String.IsNullOrEmpty(this._WorkPhone))))
                isChanged = true; 

            if (other.Mail != this._Mail && (String.IsNullOrEmpty(other.Mail) && (!String.IsNullOrEmpty(this._Mail))))
                isChanged = true; 

            // kikommentezve, mert a korrekt működéshez mindkét loginnevet domain-mentesíteni kellene
            // az EDOK denormalizált tárolásmódja miatt, ezt meg nem akarom
            //if (other.LoginName != this._LoginName)
            //    _ChangeIndicator.Add(ChangedUserIndicator.LoginName);

            #endregion comparsions

            return !isChanged;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion Public members

		#region Properties

        //private List<ChangedUserIndicator> _ChangeIndicator;
        ///// <summary>
        ///// Jelzi a személy adataiban
        ///// milyen jellegű változások következtek be.
        ///// </summary>
        ///// <remarks>
        ///// Az Equal metódus meghívásával kap értéket.
        ///// </remarks>
        //public List<ChangedUserIndicator> ChangeIndicator
        //{
        //    get
        //    {
        //        return _ChangeIndicator;
        //    }
        //    set
        //    {
        //        _ChangeIndicator = value;
        //    }
        //}
	
        /// <summary>
        /// Az utolsó AD lekérdezés időpontja.
        /// </summary>
        public DateTime LastADCheck
        {
            get
            {
                return _LastADCheckTime;
            }
            set
            {
                _LastADCheckTime = value;
            }
        }

		/// <summary>
		/// A felhasználó szervezeti egységei.
		/// </summary>
		[XmlArrayAttribute("Groups")]
        [XmlArrayItemAttribute("ADGroup", typeof(WrappedADGroupCollection))]
        public WrappedADGroupCollection GroupMembership
		{
			get
			{
				return _Groups;
			}
			set
			{
				_Groups = value;
            }
		}

        /// <summary>
        /// Az account inaktiválásának időpontja.
        /// </summary>
        [XmlAttribute]
        public DateTime? Expired
        {
            get
            {
                return _Expired;
            }
            set
            {
                _Expired = value;
            }
        }

        /// <summary>
        /// Az account törölve van-e?
        /// </summary>
        [XmlAttribute]
        public bool Deleted
        {
            get
            {
                return _Deleted;
            }
            set
            {
                _Deleted = value;
            }
        }

        /// <summary>
        /// Az account zárolva van-e?
        /// </summary>
        [XmlAttribute]
        public bool Locked
        {
            get
            {
                return _Locked;
            }
            set
            {
                _Locked = value;
            }
        }

        /// <summary>
        /// Az account használata engedélyezett-e?
        /// </summary>
        [XmlAttribute]
        public bool Enabled
        {
            get
            {
                return _Enabled;
            }
            set
            {
                _Enabled = value;
            }
        }

		/// <summary>
		/// A felhasználó alkalmazásspecifikus szerepköreinek listája.
		/// </summary>
        public WrappedADRoleCollection RoleMembership
		{
			get
			{
				return _Roles;
			}
			set
			{
                _Roles = value;
			}
		}

        /// <summary>
        /// AD-specifikus értékek.
        /// </summary>
        public ADProperties ADProp
        {
            get
            {
                return _ADProperties;
            }
        }

        /// <summary>
        /// Az adaptált rendszerben specifikus értékek.
        /// </summary>
        public EDOKProperties EdokProp
        {
            get
            {
                return _EDOKProperties;
            }
        }

		/// <summary>
		/// A felhasználó számára engedélyezett műveletek listája.
		/// </summary>
		public int[] AllowedOperations
		{
			get
			{
				if (_AllowedOperations == null)
                    return new int[0];
                else
                    return _AllowedOperations;
			}
			set
			{
				_AllowedOperations = value;
                if (value != null)
                    Array.Sort(_AllowedOperations);
            }
		}

        /// <summary>
        /// XML string representation of this instance.
        /// </summary>
        public string AsXmlString
        {
            get
            {
                System.IO.StringWriter sw = new System.IO.StringWriter();
                //XmlTextWriter xtw = new XmlTextWriter(sw);
                //xtw.WriteStartElement("AdajkUser");
                //xtw.WriteAttributeString("ObjectID", m_ObjectID);

                //xtw.WriteStartElement("FullName");
                //xtw.WriteCData(m_FullName);
                //xtw.WriteEndElement();

                //xtw.WriteStartElement("LoginName");
                //xtw.WriteCData(m_LoginName);
                //xtw.WriteEndElement();

                //xtw.WriteStartElement("Mail");
                //xtw.WriteCData(m_Mail);
                //xtw.WriteEndElement();

                //xtw.WriteStartElement("PhoneBusiness");
                //xtw.WriteCData(m_PhoneBusiness);
                //xtw.WriteEndElement();

                //xtw.WriteStartElement("PhoneFax");
                //xtw.WriteCData(m_PhoneFax);
                //xtw.WriteEndElement();

                //xtw.WriteStartElement("PhoneMobile");
                //xtw.WriteCData(m_PhoneMobile);
                //xtw.WriteEndElement();

                //xtw.WriteStartElement("Operations");
                //if (m_AllowedOperations != null)
                //{
                //    for (int i = 0; i < m_AllowedOperations.Length; i++)
                //        xtw.WriteElementString("OP", m_AllowedOperations[i].ToString());
                //}
                //xtw.WriteEndElement();

                //xtw.WriteStartElement("Roles");
                //if (m_Roles != null)
                //{
                //    for (int i = 0; i < m_Roles.Length; i++)
                //        xtw.WriteElementString("Role", m_Roles[i]);
                //}
                //xtw.WriteEndElement();

                //xtw.WriteStartElement("Groups");
                //if (m_GroupMemberships != null)
                //{
                //    foreach (AdajkGroup group in m_GroupMemberships)
                //    {
                //        xtw.WriteRaw(group.AsXmlString);
                //    }
                //}
                //xtw.WriteEndElement();


                //xtw.WriteStartElement("LastLogon");
                //xtw.WriteCData(m_LastLogon.ToString());
                //xtw.WriteEndElement();

                //xtw.WriteEndElement();
                return sw.ToString();
            }
        }

        /// <summary>
        /// A személy munkahelyi beosztásának megnevezése.
        /// </summary>
        public string Assignment
        {
            get
            {
                return _Assignment;
            }
            set
            {
                _Assignment = value;
            }
        }

        /// <summary>
        /// A személy munkahelyi telefonszáma.
        /// </summary>
        public string WorkPhone
        {
            get
            {
                return _WorkPhone;
            }
            set
            {
                _WorkPhone = value;
            }
        }

		/// <summary>
		/// A személy teljes neve.
		/// </summary>
		public string FullName
		{
			get
			{
				return _FullName;
			}
			set
			{
				_FullName = value;
			}
		}

        /// <summary>
        /// Keresztnév.
        /// </summary>
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        /// <summary>
        /// Vezetéknév.
        /// </summary>
        public string FamilyName
        {
            get
            {
                return _FamilyName;
            }
            set
            {
                _FamilyName = value;
            }
        }

        /// <summary>
        /// A személynév középső tagja.
        /// </summary>
        public string MiddleName
        {
            get
            {
                return _MiddleName;
            }
            set
            {
                _MiddleName = value;
            }
        }

        /// <summary>
        /// Titulus, pl. Dr., Ifj. stb.
        /// </summary>
        public string Titulus
        {
            get
            {
                return _Titulus;
            }
            set
            {
                _Titulus = value;
            }
        }

		/// <summary>
		/// Bejelentkezési account név.
		/// </summary>
		public string LoginName
		{
			get
			{
				return _LoginName;
			}
			set
			{
				_LoginName = value;
			}
		}

		/// <summary>
		/// E-mail cím.
		/// </summary>
		public string Mail
		{
			get
			{
				return _Mail;
			}
			set
			{
				_Mail = value;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// A megadott operation érvényes-e a felhasználónak.
		/// </summary>
		/// <param name="operationID">Elemi művelet azonosítója.</param>
		/// <returns>Igaz, ha a művelet a felhasználó számára engedélyezett.</returns>
		public bool IsValidOperation(int operationID)
		{
            if (_AllowedOperations == null)
                return false;

			for (int i = 0; i < _AllowedOperations.Length; i++)
				if (_AllowedOperations[i] == operationID)
                    return true;

            return false;
        }

		public override string ToString()
		{
			return String.Format("{0} ({1})", this._FullName, this._Mail);
		}

		#endregion

		#region Private members

        private void SetGroupMembership(XmlNode node)
        {
            XmlNodeList groups = node.SelectNodes("Groups/Group");
            if (groups != null)
            {
                foreach (XmlNode group in groups)
                    _Groups.Add(new WrappedADGroup(group));
            }
        }

        private void SetRoleMembership(XmlNode node)
        {
            XmlNodeList roles = node.SelectNodes("Roles/Role");
            if (roles != null)
            {
                _Roles.Clear();
                foreach (XmlNode role in roles)
                    _Roles.Add(new WrappedADRole(role.InnerText));
            }
        }

        private void SetAllowedOperations(XmlNode node)
        {
            XmlNodeList ops = node.SelectNodes("Operations/OP");
            if (ops != null)
            {
                _AllowedOperations = new int[ops.Count];
                int i = 0;

                foreach (XmlNode op in ops)
                {
                    int oper = Convert.ToInt32(op.InnerText);
                    _AllowedOperations[i++] = oper;
                }
            }
        }

        private void SetBasicProperties(XmlNode node)
        {
            _ADProperties.ObjectID = new System.Guid(node.Attributes["ObjectID"].Value.ToString());
            _FullName = node.SelectSingleNode("FullName").InnerText;
            _LoginName = node.SelectSingleNode("LoginName").InnerText.ToLower();
            _Mail = node.SelectSingleNode("Mail").InnerText;
            try
            {
                _LastADCheckTime = DateTime.Parse(node.SelectSingleNode("LastChecked").InnerText);
            }
            catch
            {
                _LastADCheckTime = DateTime.MinValue;
            }
        }

        #endregion Private members

    }
}