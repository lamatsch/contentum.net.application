﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;
namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        #region Fields

        public static int iktatoKonyvAzonositoHossz = 5;

        public static Dictionary<KRT_KodTarak, KRT_KodTarak> IratTipusok = new Dictionary<KRT_KodTarak, KRT_KodTarak>();
        public static Dictionary<KRT_KodTarak, KRT_KodTarak> TaxTypeKodTipusok = new Dictionary<KRT_KodTarak, KRT_KodTarak>();
        public static Dictionary<KRT_KodTarak, KRT_KodTarak> ExpedialasKodTipusok = new Dictionary<KRT_KodTarak, KRT_KodTarak>();
        public static Dictionary<KRT_KodTarak, KRT_KodTarak> TertivevenyKodTipusok = new Dictionary<KRT_KodTarak, KRT_KodTarak>();
        public static ASP_ADO_DOCUMENT.AddressDto addresseeAddressDTO = null;
        public static ASP_ADO_DOCUMENT.AddressDto senderAddressDTO = null;
        public static IktatasiParameterek defaultIktatasiParameter;
        #endregion

        static ASPADOHelper()
        {
            string err = "ASPADO inicializálás kritikus hiba: {0}";
            try
            {
                Constants.Parameters.ASPADOTenant = UI.GetAppSetting(Constants.AppSettings.ADOTenant);
            }
            catch
            {
                Logger.Error(string.Format(err, "Tenant nem található"));
            }

            ExecParam execparam = new ExecParam();
            execparam.Felhasznalo_Id = Constants.Parameters.FelhasznaloId;

            #region Parameters Init

            try
            {

                IratTipusok = GetDocumentTypeMapping(Constants.Parameters.DocumentTypeVezerloId, Constants.Parameters.IratTipusVezerloId, Constants.Parameters.AdminUser);
                ExpedialasKodTipusok = GetDocumentTypeMapping(Constants.Parameters.DefaultExpedialasCodeVezerloId, Constants.Parameters.KuldemenyKuldesModjaVezerloId, Constants.Parameters.AdminUser);
                TaxTypeKodTipusok = GetDocumentTypeMapping(Constants.Parameters.TaxTypeVezerloId, Constants.Parameters.UgyTipusVezerloId, Constants.Parameters.AdminUser);
                TertivevenyKodTipusok = GetDocumentTypeMapping(Constants.Parameters.ReceivingInformationVezerloId, Constants.Parameters.TertivevenyVezerloId, Constants.Parameters.AdminUser);
                iktatoKonyvAzonositoHossz = Rendszerparameterek.GetInt(execparam, Constants.Parameters.IktatoKonyvAzonositoHossz, 5);
            }
            catch { Logger.Error(string.Format(err, "Függő kódtár hiba.")); }

            #endregion

            #region AddressDTO Init from Config

            try
            {
                string addresseeAddress = UI.GetAppSetting(Constants.AppSettings.ADOAdresseAddress);
                addresseeAddressDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<ASP_ADO_DOCUMENT.AddressDto>(addresseeAddress);
            }
            catch { Logger.Error(string.Format(err, "ADOAdresseAddress nem található")); }

            try
            {
                string senderAddress = UI.GetAppSetting(Constants.AppSettings.ADOSenderSettings);
                senderAddressDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<ASP_ADO_DOCUMENT.AddressDto>(senderAddress);
            }
            catch { Logger.Error(string.Format(err, "ADOSenderSettings nem található")); }


            #endregion
        }

        #region Partner 
        /*a KRT_Partnerek táblába fel kell venni egy indexelt logikai mezőt: AspAdoTorolve, alapértelmezettként false. 
         * A DeletePartnerId hívás a megadott partner rekordban az AspAdoTorolve mezőt true-ra kell,
         * hogy állítsa. A GetPartners hívásban a visszaadott listából ki kell zárni azokat a rekordokat, ahol az AspAdoTorolve mező értéke true.*/
        public static ASP_ADO_PARTNER.BaseResponse DeletePartnerId(ASP_ADO_PARTNER.PartnerIdRequest request, ASP_ADO_PARTNER.BaseResponse response, string localUser)
        {
            var partnerekService = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            var execParam = GetExecParam(localUser);
            execParam.Record_Id = request.PartnerId;

            var getResult = partnerekService.Get(execParam);

            if (getResult.IsError)
            {
                response.SetError("Hiba a törlendő partner lekérdezése során: " + request.PartnerId);
                return response;
            }

            if (getResult.HasData())
            {
                response.SetError("Nem létezik a törlendő partner: " + request.PartnerId);
                return response;
            }

            var partner = getResult.GetData<KRT_Partnerek>().SingleOrDefault();
            partner.AspAdoTorolve = "1";
            partner.Updated.AspAdoTorolve = true;

            var updateResult = partnerekService.Update(GetExecParam(localUser), partner);
            if (updateResult.IsError)
            {
                response.SetError("Hiba a partner törlése során");
                return response;
            }
            return response.SetSuccess();
        }

        public static void MarkAddresses(this DataTable dataTable, IBaseResponse response, ExecParam execParam, ASP_ADO_PARTNER.AddressDto[] addresses, List<Tuple<string, ASP_ADO_PARTNER.AddressDto>> existingAddresses, List<Tuple<string, ASP_ADO_PARTNER.AddressDto>> newAddresses, List<Tuple<string, ASP_ADO_PARTNER.AddressDto>> notAssociatedAddresses, List<Tuple<string, ASP_ADO_PARTNER.AddressDto>> deleteAddresses)
        {
            KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();
            execParam.Clean();
            var partnerCimekEnumerable = dataTable.AsEnumerable();
            int count = partnerCimekEnumerable.Count();

            if (addresses != null)
            {
                foreach (var address in addresses)
                {
                    if (!address.IdSpecified || address.Id == 0)
                    {
                        newAddresses.Add(Tuple.Create("0", address));
                    }
                    else
                    {
                        Result addressGuidResult = cimekService.GetASPADOAddressId(execParam, address.Id);

                        if (addressGuidResult.IsError)
                        {
                            response.SetError(addressGuidResult.ErrorMessage);
                            return;
                        }

                        if (addressGuidResult.Record == null)
                        {
                            newAddresses.Add(Tuple.Create("0", address));
                        }
                        else
                        {
                            if (partnerCimekEnumerable.Any(x => x[Constants.TableColumns.Cim_Id].ToString() == addressGuidResult.Record.ToString()))
                            {
                                existingAddresses.Add(Tuple.Create(addressGuidResult.Record.ToString(), address));
                            }
                            else
                            {
                                //Does not belong to this partner
                                notAssociatedAddresses.Add(Tuple.Create("0", address));
                            }
                        }
                    }
                }
                foreach (var row in partnerCimekEnumerable)
                {
                    string cId = row[Constants.TableColumns.Cim_Id].ToString();
                    if (!notAssociatedAddresses.Any(x => x.Item1 == cId) && !existingAddresses.Any(x => x.Item1 == cId))
                    {
                        deleteAddresses.Add(Tuple.Create(cId, new ASP_ADO_PARTNER.AddressDto()));
                    }
                }
            }
            else if (count > 0)
            {
                partnerCimekEnumerable.ToList().ForEach(x => deleteAddresses.Add(Tuple.Create(x[Constants.TableColumns.Cim_Id].ToString(), new ASP_ADO_PARTNER.AddressDto())));
            }

        }
        public static void InsertAddresses(this ASP_ADO_PARTNER.AddressDto[] addresses, IBaseResponse response, string partnerId, string localUser)
        {
            if (addresses != null && addresses.Length > 0)
            {
                ExecParam execParam = GetExecParam(localUser);

                KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
                KRT_PartnerCimekService partnerCimekService = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
                KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();

                foreach (ASP_ADO_PARTNER.AddressDto address in addresses)
                {
                    KRT_Cimek krt_Cimek = address.GetCimekFromRequestForInsert(localUser);

                    Result cimekResult = cimekService.InsertWithFKResolution(execParam, krt_Cimek);

                    if (cimekResult.IsError)
                    {
                        response.SetError(cimekResult.ErrorMessage);
                        return;
                    }
                    else if (String.IsNullOrEmpty(cimekResult.Uid))
                    {
                        response.SetError(Constants.Messages.AddressCreateFailedError);
                        return;
                    }

                    address.Id = long.Parse(partnerService.GetASPADOId(execParam, cimekResult.Uid).Record.ToString());
                    address.IdSpecified = true;

                    KRT_PartnerCimek krt_partnercimek = address.GetPartnerCimekFromRequestForInsert(partnerId, cimekResult.Uid);

                    Result partnerCimekResult = partnerCimekService.Insert(execParam, krt_partnercimek);

                    if (partnerCimekResult.IsError)
                    {
                        response.SetError(partnerCimekResult.ErrorMessage);
                        return;
                    }
                    else if (String.IsNullOrEmpty(partnerCimekResult.Uid))
                    {
                        response.SetError(Constants.Messages.AddressAssociationFailedError);
                        return;
                    }
                }

                response.IsSucceeded = true;
            }
        }
        public static void DeleteAddresses(this string[] addresses, IBaseResponse response, string partnerId, string localUser)
        {
            if (addresses != null && addresses.Length > 0)
            {
                ExecParam execParam = GetExecParam(localUser);

                KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
                KRT_PartnerCimekService partnerCimekService = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
                KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();

                foreach (string address in addresses)
                {
                    //execParam.Record_Id = address;

                    //Result deleteResult = cimekService.Invalidate(execParam);

                    //if (!String.IsNullOrEmpty(deleteResult.ErrorMessage))
                    //{
                    //    response.SetError(deleteResult.ErrorMessage);
                    //    return;
                    //}

                    KRT_PartnerCimekSearch partnerCimekSearch = new KRT_PartnerCimekSearch();
                    partnerCimekSearch.Partner_id.Value = partnerId;
                    partnerCimekSearch.Partner_id.Operator = Contentum.eQuery.Query.Operators.equals;
                    partnerCimekSearch.Cim_Id.Value = address;
                    partnerCimekSearch.Cim_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                    partnerCimekSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                    partnerCimekSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
                    partnerCimekSearch.ErvKezd.Group = "100";
                    partnerCimekSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

                    partnerCimekSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                    partnerCimekSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
                    partnerCimekSearch.ErvVege.Group = "100";
                    partnerCimekSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

                    execParam.Clean();
                    Result partnerCimekGetResult = partnerCimekService.GetAll(execParam, partnerCimekSearch);

                    if (partnerCimekGetResult.IsError)
                    {
                        response.SetError(partnerCimekGetResult.ErrorMessage);
                        return;
                    }

                    foreach (DataRow row in partnerCimekGetResult.Ds.Tables[0].Rows)
                    {
                        execParam.Record_Id = row[Constants.TableColumns.Id].ToString();
                        Result partnerCimekDeleteResult = partnerCimekService.Invalidate(execParam);

                        if (partnerCimekDeleteResult.IsError)
                        {
                            response.SetError(partnerCimekDeleteResult.ErrorMessage);
                            return;
                        }
                    }
                }

                response.IsSucceeded = true;
            }
        }
        public static void AssociateAddresses(this ASP_ADO_PARTNER.AddressDto[] addresses, IBaseResponse response, string partnerId, string localUser)
        {
            if (addresses != null && addresses.Length > 0)
            {
                ExecParam execParam = GetExecParam(localUser);

                KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();

                KRT_PartnerCimekService partnerCimekService = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();

                foreach (ASP_ADO_PARTNER.AddressDto address in addresses)
                {
                    Result addressGuidResult = cimekService.GetASPADOAddressId(execParam, address.Id);

                    if (addressGuidResult.IsError || addressGuidResult.Record == null)
                    {
                        response.SetError(addressGuidResult.ErrorMessage);
                        return;
                    }

                    KRT_PartnerCimek partnerCim = address.GetPartnerCimekFromRequestForInsert(partnerId, addressGuidResult.Record.ToString());

                    Result associateResult = partnerCimekService.Insert(execParam, partnerCim);

                    if (!String.IsNullOrEmpty(associateResult.ErrorMessage))
                    {
                        response.SetError(associateResult.ErrorMessage);
                        return;
                    }
                }

                response.IsSucceeded = true;
            }
        }
        public static void InsertEmail(this string email, IBaseResponse response, string partnerId, string localUser)
        {
            if (!String.IsNullOrEmpty(email))
            {
                ExecParam execParam = GetExecParam(localUser);

                KRT_PartnerCimekService partnerCimekService = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
                KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();

                KRT_Cimek krt_EmailCimek = new KRT_Cimek();
                krt_EmailCimek.Updated.SetValueAll(false);
                krt_EmailCimek.Base.Updated.SetValueAll(false);

                krt_EmailCimek.Tipus = KodTarak.Cim_Tipus.Email;
                krt_EmailCimek.Updated.Tipus = true;

                krt_EmailCimek.CimTobbi = email;
                krt_EmailCimek.Updated.CimTobbi = true;

                krt_EmailCimek.ErvKezd = DateTime.Today.ToString();
                krt_EmailCimek.Updated.ErvKezd = true;
                krt_EmailCimek.ErvVege = new DateTime(4700, 12, 31).ToString();
                krt_EmailCimek.Updated.ErvVege = true;

                //Kategória beállítása konkrétra, jelenleg bedrótozva
                krt_EmailCimek.Kategoria = "K";
                krt_EmailCimek.Updated.Kategoria = true;

                //a megnyitási verzió megadása ellenõrzés miatt
                krt_EmailCimek.Base.Ver = "1";
                krt_EmailCimek.Base.Updated.Ver = true;

                Result cimekResult = cimekService.Insert(execParam, krt_EmailCimek);

                if (cimekResult.IsError)
                {
                    response.SetError(cimekResult.ErrorMessage);
                    return;
                }
                else if (String.IsNullOrEmpty(cimekResult.Uid))
                {
                    response.SetError(Constants.Messages.EmailCreateFailedError);
                    return;
                }

                KRT_PartnerCimek krt_partnercimek = GetPartnerCimek(partnerId, cimekResult.Uid);

                Result partnerCimekResult = partnerCimekService.Insert(execParam, krt_partnercimek);

                if (partnerCimekResult.IsError)
                {
                    response.SetError(partnerCimekResult.ErrorMessage);
                    return;
                }
                else if (String.IsNullOrEmpty(partnerCimekResult.Uid))
                {
                    response.SetError(Constants.Messages.EmailAssociationFailedError);
                    return;
                }

                response.IsSucceeded = true;
            }
        }
        public static void UpdateEmail(this string email, IBaseResponse response, string addressId, string localUser)
        {
            KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            execParam.Record_Id = addressId;

            Result cimResult = cimekService.Get(execParam);

            if (cimResult.IsError)
            {
                response.SetError(cimResult.ErrorMessage);
                return;
            }

            KRT_Cimek krt_Cimek = (KRT_Cimek)cimResult.Record;

            krt_Cimek.Updated.SetValueAll(false);
            krt_Cimek.Base.Updated.SetValueAll(false);

            krt_Cimek.Base.Updated.Ver = true;

            //krt_Cimek.Tipus = KodTarak.Cim_Tipus.Postai;
            //krt_Cimek.Updated.Tipus = true;

            krt_Cimek.CimTobbi = email;
            krt_Cimek.Updated.CimTobbi = true;

            Result cimUpdateResult = cimekService.UpdateWithFKResolution(execParam, krt_Cimek);

            if (cimUpdateResult.IsError)
            {
                response.SetError(cimUpdateResult.ErrorMessage);
            }
        }

        /* PartnersToMerge-ben megadott összes partnert konszolidálni kell az ActivePartner-ben megadott partnerrel (az ActivePartner marad).
         * A KRT_PARTNEREKService osztály Consolidate metódusát kell hívni, ahol a forrás partner összes címét és kapcsolat Id-ját ki kell tölteni.*/
        public static ASP_ADO_PARTNER.PartnerResponse MergePartner(ASP_ADO_PARTNER.MergePartnerRequest request, ASP_ADO_PARTNER.PartnerResponse response, string localUser)
        {
            var partnerekService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            var szemelyekService = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
            var vallalkozasokService = eAdminService.ServiceFactory.GetKRT_VallalkozasokService();
            var cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();


            var aspPartnerIdResult = partnerekService.GetASPADOPartnerId(ASPADOHelper.GetExecParam(localUser), (int)request.ActivePartner);
            Logger.Debug("MergePartner => ActivePartnerId: " + request.ActivePartner + " contentum id: " + (aspPartnerIdResult.Record == null ? "not found" : aspPartnerIdResult.Record));

            if (aspPartnerIdResult.IsError)
            {
                response.SetError("Hiba a konszolidálandó partner lekérdezése során: " + request.ActivePartner);
                return response;
            }
            if (aspPartnerIdResult.Record == null)
            {
                response.SetError("Hiba a konszolidálandó partner lekérdezése során: " + request.ActivePartner + " Nem tudjuk az idt lefordítani: " + (int)request.ActivePartner);
                return response;
            }
            var consolidatedPartnerExecParam = GetExecParam(localUser);
            consolidatedPartnerExecParam.Record_Id = aspPartnerIdResult.Record.ToString();
            Logger.Debug("MergePartner. Partner lekérése");

            var consolidatedPartnerResult = partnerekService.Get(consolidatedPartnerExecParam);

            if (consolidatedPartnerResult.IsError)
            {
                response.SetError("Hiba a konszolidálandó partner lekérdezése során: " + request.ActivePartner);
                return response;
            }

            if (consolidatedPartnerResult.Record == null)
            {
                response.SetError("Nem létezik a konszolidálandó partner: " + request.ActivePartner);
                return response;
            }
            Logger.Debug("MergePartner. Cimek lekérése a partnerhez");
            var activePartner = (KRT_Partnerek)consolidatedPartnerResult.Record;

            var activePartnerCimIds = new List<string>();
            var cimekForConsolidatedPartnerResult = cimekService.GetAllByPartner(
                GetExecParam(localUser),
                new KRT_Partnerek() { Id = activePartner.Id },
                null,
                new KRT_PartnerCimekSearch());

            if (cimekForConsolidatedPartnerResult.IsError)
            {
                response.SetError("Hiba a konszolidálandó partner lekérdezése során: " + cimekForConsolidatedPartnerResult.ErrorMessage);
                return response;
            }
            activePartnerCimIds = cimekForConsolidatedPartnerResult.GetData<KRT_Cimek>().Select(i => i.Id).ToList();

            Logger.Debug("MergePartner. Partner kapcsolatok lekérése a partnerhez");
            var activePartnerKapcsolatokIds = new List<string>();
            var searchForConsolidatedPartnerKapcsolatok = new KRT_PartnerKapcsolatokSearch();
            var list = KodTarak.PartnerKapcsolatTipus.KapcsolatMappings.FilterByPartnerTipus(activePartner.Tipus).Where(x => x.IsNormal == false && x.Belso == activePartner.Belso).Select(x => x.KapcsolatTipus).ToArray();
            searchForConsolidatedPartnerKapcsolatok.Tipus.In(list);
            Result resultOfConsolidatedPartnerKapcsolatok = partnerekService.GetAllByPartner(
                GetExecParam(localUser),
                new KRT_Partnerek() { Id = activePartner.Id },
                searchForConsolidatedPartnerKapcsolatok);

            if (resultOfConsolidatedPartnerKapcsolatok.IsError)
            {
                response.SetError("Hiba a konszolidálandó partner lekérdezése során: " + resultOfConsolidatedPartnerKapcsolatok.ErrorMessage);
                return response;
            }
            activePartnerKapcsolatokIds = resultOfConsolidatedPartnerKapcsolatok.GetData<KRT_Partnerek>().Select(i => i.Id).ToList();

            Logger.Debug("MergePartner. Konszolidálandó partnerek feldolozása");
            foreach (var partner in request.PartnersToMerge)
            {
                if (partner.PartnerId == request.ActivePartner.ToString())
                {
                    Logger.Debug("Az ASPADÓ az aktív partnerrel akarja konszolidálni ugyanazt a partner.PartnerId: " + partner.PartnerId + " ActivePartner: " + request.ActivePartner.ToString()); // Ez hibás működés az ASP részéről
                    continue;
                }
                int parsedPartnerID;
                bool canBeParsed = int.TryParse(partner.PartnerId, out parsedPartnerID);
                if (!canBeParsed)
                {
                    response.SetError("Érvénytelen partnerID: " + partner.PartnerId);
                    return response;
                }
                var currentAspPartnerResult = partnerekService.GetASPADOPartnerId(ASPADOHelper.GetExecParam(localUser), parsedPartnerID);
                Logger.Debug("MergePartner => PartnerID: " + partner.PartnerId + " ContentumPartnerID: " + currentAspPartnerResult.Record);

                if (currentAspPartnerResult.IsError)
                {
                    response.SetError("Hiba a konszolidálandó partner lekérdezése során: " + parsedPartnerID);
                    return response;
                }
                if (currentAspPartnerResult.Record == null)
                {
                    response.SetError("Hiba a konszolidálandó partner lekérdezése során: " + parsedPartnerID);
                    return response;
                }

                var krt_Partner = new KRT_Partnerek();
                krt_Partner.Id = currentAspPartnerResult.Record.ToString();

                KRT_Szemelyek krt_Szemely = null;
                if (partner.PartnerType == ASP_ADO_PARTNER.EnumsPartnerType.Individual)
                {
                    krt_Szemely = new KRT_Szemelyek();
                    Logger.Debug("MergePartner. Személy");
                    krt_Partner.Tipus = KodTarak.Partner_Tipus.Szemely;

                    KRT_SzemelyekSearch szemelyekSearch = new KRT_SzemelyekSearch();
                    szemelyekSearch.Partner_Id.Filter(krt_Partner.Id);

                    Result resultSzemelyek = szemelyekService.GetAll(GetExecParam(localUser), szemelyekSearch);
                    string szemelyId = "";
                    if (!resultSzemelyek.IsError && resultSzemelyek.HasData())
                    {
                        foreach (DataRow row in resultSzemelyek.Ds.Tables[0].Rows)
                        {
                            szemelyId = (row["Id"].ToString());
                        }
                    }
                    krt_Szemely.Id = szemelyId;
                    krt_Szemely.Partner_Id = krt_Partner.Id;
                }

                KRT_Vallalkozasok krt_Vallalkozas = null;
                if (partner.PartnerType == ASP_ADO_PARTNER.EnumsPartnerType.Company)
                {
                    krt_Vallalkozas = new KRT_Vallalkozasok();
                    Logger.Debug("MergePartner. Vállalkozás");
                    krt_Partner.Tipus = KodTarak.Partner_Tipus.Szervezet;

                    KRT_VallalkozasokSearch vallalkozasSearch = new KRT_VallalkozasokSearch();
                    vallalkozasSearch.Partner_Id.Filter(krt_Partner.Id);

                    Result resultVallalkozasok = vallalkozasokService.GetAll(GetExecParam(localUser), vallalkozasSearch);
                    string vallalkozasId = "";
                    if (!resultVallalkozasok.IsError && resultVallalkozasok.HasData())
                    {
                        foreach (DataRow row in resultVallalkozasok.Ds.Tables[0].Rows)
                        {
                            vallalkozasId = (row["Id"].ToString());
                        }
                    }

                    krt_Vallalkozas.Id = vallalkozasId;
                    krt_Vallalkozas.Partner_Id = krt_Partner.Id;
                }
                Logger.Debug("MergePartner. Végső konszolidálás. activePartner: " + activePartner.ToString()
                    + " activePartnerCimIds " + string.Join(",", activePartnerCimIds.ToArray())
                    + " activePartnerKapcsolatokIds " + string.Join(",", activePartnerKapcsolatokIds.ToArray())
                    + " krt_Szemely " + (krt_Szemely == null ? "null" : krt_Szemely.ToString())
                    + " krt_Vallalkozas " + (krt_Vallalkozas == null ? " null " : krt_Vallalkozas.ToString())
                    + " krt_partner " + krt_Partner.ToString());
                Result result = partnerekService.Consolidate(
                     GetExecParam(localUser),
                     activePartner,
                     activePartnerCimIds,
                     activePartnerKapcsolatokIds,
                     krt_Szemely,
                     krt_Vallalkozas,
                     krt_Partner);

                if (result.IsError)
                {
                    int resultCode;
                    bool canErrorBeParsed = int.TryParse(result.ErrorCode, out resultCode);
                    string errorMessage = canErrorBeParsed ? ResultError.GetErrorMessageByErrorCode(resultCode) : result.ErrorMessage;

                    response.SetErrorResponse("Nem sikerült a konszolidáció. Hiba: " + errorMessage);
                    return response;
                }
            }
            return response;
        }
        #endregion

        #region GetPartners, GetPartner    
        private static KRT_PartnerCimek GetPartnerCimek(string partnerId, string cimId)
        {
            KRT_PartnerCimek krt_partnercimek = new KRT_PartnerCimek();
            krt_partnercimek.Updated.SetValueAll(false);
            krt_partnercimek.Base.Updated.SetValueAll(false);

            krt_partnercimek.Partner_id = partnerId;
            krt_partnercimek.Updated.Partner_id = true;

            krt_partnercimek.Cim_Id = cimId;
            krt_partnercimek.Updated.Cim_Id = true;

            krt_partnercimek.ErvKezd = DateTime.Today.ToString();
            krt_partnercimek.Updated.ErvKezd = true;
            krt_partnercimek.ErvVege = new DateTime(4700, 12, 31).ToString();
            krt_partnercimek.Updated.ErvVege = true;

            krt_partnercimek.Base.Ver = "1";
            krt_partnercimek.Base.Updated.Ver = true;

            return krt_partnercimek;
        }
        private class PartnerComparer : IEqualityComparer<DataRow>
        {
            public bool Equals(DataRow x, DataRow y)
            {
                if (x == null && y == null)
                    return true;
                else if (x == null || y == null)
                    return false;
                else if (x[Constants.TableColumns.Id].ToString() == y[Constants.TableColumns.Id].ToString())
                    return true;
                else
                    return false;

            }

            public int GetHashCode(DataRow obj)
            {
                return obj[Constants.TableColumns.Id].ToString().GetHashCode();
            }
        }

        public static ASP_ADO_PARTNER.PartnerResponse GetPartner(ASP_ADO_PARTNER.PartnerIdRequest partnerIdRequest, ASP_ADO_PARTNER.PartnerResponse response, string localUser)
        {
            var partnerRequest = new ASP_ADO_PARTNER.PartnerRequest();
            partnerRequest.Partner = new ASP_ADO_PARTNER.PartnerDto();
            partnerRequest.Partner.PartnerId = partnerIdRequest.PartnerId;

            var resp = new ASP_ADO_PARTNER.PartnersResponse();
            resp = ASPADOHelper.GetPartners(partnerRequest, resp, 0, false, 0, false, localUser);

            if (!String.IsNullOrEmpty(resp.ErrorMessage))
            {
                return response.SetError(resp.ErrorMessage);
            }

            if (resp.Partners != null)
            {
                ASP_ADO_PARTNER.PartnerDto partner = resp.Partners.FirstOrDefault(x => x.PartnerId == partnerIdRequest.PartnerId);

                if (partner != null)
                {
                    response.Partner = partner;
                }
            }

            return response.SetSuccess();
        }

        public static ASP_ADO_PARTNER.PartnersResponse GetPartners(ASP_ADO_PARTNER.PartnerRequest partnerRequest, ASP_ADO_PARTNER.PartnersResponse response, int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified, string localUser)
        {
            KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            execParam.AddPaging(pageNumber, pageNumberSpecified, pageSize, pageSizeSpecified);

            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.OrderBy = null;

            KRT_PartnerCimekSearch partnerCimekSearch = new KRT_PartnerCimekSearch();
            partnerCimekSearch.OrderBy = null;

            KRT_CimekSearch cimekSearch = new KRT_CimekSearch();
            cimekSearch.OrderBy = null;

            KRT_VallalkozasokSearch vallalkozasokSearch = new KRT_VallalkozasokSearch();
            vallalkozasokSearch.OrderBy = null;

            KRT_SzemelyekSearch szemelyekSearch = new KRT_SzemelyekSearch();
            szemelyekSearch.OrderBy = null;

            if (partnerRequest.Partner != null)
            {
                if (!String.IsNullOrEmpty(partnerRequest.Partner.PartnerId))
                {
                    partnerekSearch.Id.Value = partnerRequest.Partner.PartnerId;
                }
                else
                {
                    partnerRequest.BuildSearchObjectsFromRequest(partnerekSearch, cimekSearch, szemelyekSearch, vallalkozasokSearch, partnerCimekSearch);
                }
            }

            partnerekSearch.AspAdoTorolve.Equals(false);

            Result partnerekResult = partnerService.GetAllWithCimAndSzemelyekandVallakozasok(execParam, partnerekSearch, cimekSearch, szemelyekSearch, vallalkozasokSearch, partnerCimekSearch);

            if (partnerekResult.IsError)
            {
                return response.SetError(partnerekResult.ErrorMessage);
            }
            else if (partnerekResult.Ds.Tables.Count == 0 || partnerekResult.Ds.Tables[0].Rows.Count == 0)
            {
                response.Partners = new ASP_ADO_PARTNER.PartnerDto[0];
                response.TotalItemCount = 0;
                return response.SetSuccess();
            }
            else
            {
                response.BuildResponseFromResult(partnerekResult.Ds.Tables[0]);
                return response.SetSuccess();
            }
        }
        #endregion
        /*A funkció lehetővé teszi az ADÓ rendszerből egy alszámhoz tartozó  csatolmányok lekérdezését az iratkezelő rendszerből. (Jelenleg nem implementált funkció)
        Jövőbeni fejlesztés, ahol egy iktatószámhoz (alszámhoz) tartozó, hitelesített dokumentumok és annak egyéb hitelesítési információi juttathatók az Irat szakrendszerből az Adó szakrendszerbe, pl. egy külső hitelesítést követően, további feldolgozás céljából. Elsősorban a külső PostaHibrid kapcsolathoz került kialakításra.
        */
        public static ASP_ADO_DOCUMENT.FileDto[] GetAttachments(ASP_ADO_DOCUMENT.RegistrationNumberDto registrationNumber, ASP_ADO_DOCUMENT.GetAttachmentsResponse response, string localUser)
        {
            ExecParam execParam = GetExecParam(localUser);

            return GetAttachmentsInternal(registrationNumber, response, execParam);
        }

        #region GetDocuments

        public static Result GetDocumentsData(ASP_ADO_DOCUMENT.DocumentListByRegistrationNumberRequest documentListByRegistrationNumberRequest, int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified, ExecParam execParam)
        {
            var erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);

            documentListByRegistrationNumberRequest.BuildSearchObjectsFromRequest(erec_IraIratokSearch);
            execParam.AddPaging(pageNumber, pageNumberSpecified, pageSize, pageSizeSpecified);
            erec_IraIratokSearch.OrderBy = "EREC_IraIratok.LetrehozasIdo ASC";

            Result erec_IraIratokResult = erec_IraIratokService.GetAllWithExtensionAndJogosultak(execParam, erec_IraIratokSearch, false);

            return erec_IraIratokResult;
        }
        public static Result GetDocumentsData(ASP_ADO_DOCUMENT.DocumentListByRegistrationNumberRequest documentListByRegistrationNumberRequest, int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified, string localUser)
        {
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            return GetDocumentsData(documentListByRegistrationNumberRequest, pageNumber, pageNumberSpecified, pageSize, pageSizeSpecified, execParam);
        }

        public static Result GetDocumentsDataForBejovo(ASP_ADO_DOCUMENT.DocumentListByRegistrationNumberRequest documentListByRegistrationNumberRequest, int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified, string localUser)
        {
            EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);
            Result erec_IraIratokResult = new Result();

            documentListByRegistrationNumberRequest.BuildSearchObjectsFromRequest(erec_IraIratokSearch);
            erec_IraIratokSearch.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Bejovo;
            erec_IraIratokSearch.PostazasIranya.Operator = Contentum.eQuery.Query.Operators.equals;
            string[] values = { "04", "05" };
            erec_IraIratokSearch.Allapot.Value = Search.GetSqlInnerString(values.ToArray());
            erec_IraIratokSearch.Allapot.Operator = Contentum.eQuery.Query.Operators.inner;

            execParam.AddPaging(pageNumber, pageNumberSpecified, pageSize, pageSizeSpecified);
            erec_IraIratokSearch.OrderBy = "EREC_IraIratok.LetrehozasIdo ASC";

            erec_IraIratokResult = erec_IraIratokService.GetAllWithExtensionAndJogosultak(execParam, erec_IraIratokSearch, false);

            return erec_IraIratokResult;
        }

        public static ASP_ADO_DOCUMENT.GetDocumentsResponse GetDocuments(ASP_ADO_DOCUMENT.DocumentListByRegistrationNumberRequest documentListByRegistrationNumberRequest, ASP_ADO_DOCUMENT.GetDocumentsResponse response, int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified, string localUser)
        {
            Result erec_IraIratokResult = new Result();
            erec_IraIratokResult = GetDocumentsDataForBejovo(documentListByRegistrationNumberRequest, pageNumber, pageNumberSpecified, pageSize, pageSizeSpecified, localUser);

            if (erec_IraIratokResult.IsError)
            {
                return response.SetErrorResponse(erec_IraIratokResult.ErrorMessage);
            }
            else if (erec_IraIratokResult.Ds.Tables.Count == 0 || erec_IraIratokResult.Ds.Tables[0].Rows.Count == 0)
            {
                response.TotalItemCount = 0;
                response.TotalItemCountSpecified = true;
                return response.SetSuccessResponse();
            }
            else
            {
                response.BuildResponseFromResult(erec_IraIratokResult.Ds.Tables[0], localUser);
                if (!String.IsNullOrEmpty(response.ErrorMessage))
                {
                    return response.SetErrorResponse(response.ErrorMessage);
                }
                else
                {
                    response.TotalItemCount = Convert.ToInt32(erec_IraIratokResult.Ds.Tables[1].Rows[0][0]);
                    response.TotalItemCountSpecified = true;
                    return response.SetSuccessResponse();
                }
            }
        }

        /* A GetDocuments metódushoz hasonlóan működik, de nem az iratokon,
         hanem a megadott keresési paraméterek alapján az EREC_KuldKuldemenyek táblából
         kell lekérdezni azokat a küldeményeket, amik nem iktatandók (IktatniKell = '0').*/
        public static ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponse GetNotToBeFiledDocuments(ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsRequest request, ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponse response, string localUser)
        {
            var kuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            var kuldemenySearch = new EREC_KuldKuldemenyekSearch(true);

            if (!string.IsNullOrEmpty(request.GetNotToBeFiledDocumentsRequestDto.Subject))
            {
                kuldemenySearch.Targy.Equals(request.GetNotToBeFiledDocumentsRequestDto.Subject);
            }

            if (!string.IsNullOrEmpty(request.GetNotToBeFiledDocumentsRequestDto.BarCode))
            {
                kuldemenySearch.BarCode.Equals(request.GetNotToBeFiledDocumentsRequestDto.BarCode);
            }

            if (!string.IsNullOrEmpty(request.GetNotToBeFiledDocumentsRequestDto.Comment))
            {
                kuldemenySearch.BontasiMegjegyzes.Equals(request.GetNotToBeFiledDocumentsRequestDto.Comment);
            }

            if (request.GetNotToBeFiledDocumentsRequestDto.ArrivalDateFromSpecified && request.GetNotToBeFiledDocumentsRequestDto.ArrivalDateToSpecified)
            {
                kuldemenySearch.BeerkezesIdeje.Between(request.GetNotToBeFiledDocumentsRequestDto.ArrivalDateFrom.ToString(), request.GetNotToBeFiledDocumentsRequestDto.ArrivalDateTo.ToString());
            }
            if (request.GetNotToBeFiledDocumentsRequestDto.ArrivalDateFromSpecified && !request.GetNotToBeFiledDocumentsRequestDto.ArrivalDateToSpecified)
            {
                kuldemenySearch.BeerkezesIdeje.Greater(request.GetNotToBeFiledDocumentsRequestDto.ArrivalDateFrom.ToString());
            }
            if (!request.GetNotToBeFiledDocumentsRequestDto.ArrivalDateFromSpecified && request.GetNotToBeFiledDocumentsRequestDto.ArrivalDateToSpecified)
            {
                kuldemenySearch.BeerkezesIdeje.Less(request.GetNotToBeFiledDocumentsRequestDto.ArrivalDateTo.ToString());
            }

            if (request.GetNotToBeFiledDocumentsRequestDto.SplitDateFromSpecified && request.GetNotToBeFiledDocumentsRequestDto.SplitDateToSpecified)
            {
                kuldemenySearch.FelbontasDatuma.Between(request.GetNotToBeFiledDocumentsRequestDto.SplitDateFrom.ToString(), request.GetNotToBeFiledDocumentsRequestDto.SplitDateTo.ToString());
            }
            if (request.GetNotToBeFiledDocumentsRequestDto.SplitDateFromSpecified && !request.GetNotToBeFiledDocumentsRequestDto.SplitDateToSpecified)
            {
                kuldemenySearch.FelbontasDatuma.Greater(request.GetNotToBeFiledDocumentsRequestDto.SplitDateFrom.ToString());
            }
            if (!request.GetNotToBeFiledDocumentsRequestDto.SplitDateFromSpecified && request.GetNotToBeFiledDocumentsRequestDto.SplitDateToSpecified)
            {
                kuldemenySearch.FelbontasDatuma.Less(request.GetNotToBeFiledDocumentsRequestDto.SplitDateTo.ToString());
            }

            if (request.GetNotToBeFiledDocumentsRequestDto.Partner != null && !string.IsNullOrEmpty(request.GetNotToBeFiledDocumentsRequestDto.Partner.PartnerId))
            {
                kuldemenySearch.Partner_Id_Bekuldo.Equals(request.GetNotToBeFiledDocumentsRequestDto.Partner.PartnerId);
            }

            if (request.GetNotToBeFiledDocumentsRequestDto.TaxTypeSpecified)
            {
                string kod = request.GetNotToBeFiledDocumentsRequestDto.TaxType.ToString();
                if (TaxTypeKodTipusok.Keys.Any(x => x.Kod.EndsWith(kod)))
                {

                    List<string> kodokNotSplitted = TaxTypeKodTipusok.Where(x => x.Key.Kod.EndsWith(kod)).Select(x => x.Value.Kod).ToList();

                    List<string[]> kodok = new List<string[]>();
                    kodokNotSplitted.ForEach(x =>
                    {
                        int pos = x.IndexOf('_'); string first = x.Substring(0, pos); string second = x.Substring(pos + 1); kodok.Add(new string[] { first, second });
                    });

                    string[] agazatiJelek = kodok.Select(x => x[0]).ToArray();
                    string[] ugytipusok = kodok.Select(x => x[1]).ToArray();
                    kuldemenySearch.WhereByManual += string.IsNullOrEmpty(kuldemenySearch.WhereByManual) ? string.Empty : " and ";
                    kuldemenySearch.WhereByManual += " EREC_UgyUgyiratok.UgyTipus in ('" + ugytipusok.Aggregate((x, y) => x + "','" + y) + "') ";
                    kuldemenySearch.WhereByManual += " and EREC_UgyUgyiratok.IraIrattariTetel_Id in (select itt.Id from EREC_IraIrattariTetelek as itt where itt.Id=EREC_UgyUgyiratok.IraIrattariTetel_Id and itt.IrattariTetelszam in ('" + agazatiJelek.Aggregate((x, y) => x + "','" + y) + "')) ";
                }
            }

            execParam.AddPaging(request.GetNotToBeFiledDocumentsRequestDto.Page, request.GetNotToBeFiledDocumentsRequestDto.PageSpecified, request.GetNotToBeFiledDocumentsRequestDto.PageSize, request.GetNotToBeFiledDocumentsRequestDto.PageSizeSpecified);
            kuldemenySearch.OrderBy = "EREC_KuldKuldemenyek.LetrehozasIdo ASC";


            var kuldemenyekResult = kuldemenyekService.GetAllWithExtensionAndJogosultak(execParam, kuldemenySearch, false);

            if (kuldemenyekResult.IsError)
            {
                return response.SetErrorResponse(kuldemenyekResult.ErrorMessage);
            }

            if (!kuldemenyekResult.HasData())
            {
                response.Count = 0;
                response.CountSpecified = true;
                return response.SetSuccessResponse();
            }

            response.BuildResponseFromResult(kuldemenyekResult.Ds.Tables[0], localUser);
            if (!String.IsNullOrEmpty(response.ErrorMessage))
            {
                return response.SetErrorResponse(response.ErrorMessage);
            }
            else
            {
                response.Count = kuldemenyekResult.Ds.Tables[0].Rows.Count;
                response.CountSpecified = true;
                return response.SetSuccessResponse();
            }
        }

        public static ASP_ADO_DOCUMENT.GetDocumentsResponse GetDocuments(string iratId, ASP_ADO_DOCUMENT.GetDocumentsResponse response, int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified, string localUser)
        {

            EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);
            Result erec_IraIratokResult = new Result();

            erec_IraIratokSearch.Id.Value = iratId;
            erec_IraIratokSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;
            execParam.AddPaging(pageNumber, pageNumberSpecified, pageSize, pageSizeSpecified);
            erec_IraIratokSearch.OrderBy = "EREC_IraIratok.LetrehozasIdo ASC";

            erec_IraIratokResult = erec_IraIratokService.GetAllWithExtensionAndJogosultak(execParam, erec_IraIratokSearch, false);

            if (erec_IraIratokResult.IsError)
            {
                return response.SetErrorResponse(erec_IraIratokResult.ErrorMessage);
            }
            else if (erec_IraIratokResult.Ds.Tables.Count == 0 || erec_IraIratokResult.Ds.Tables[0].Rows.Count == 0)
            {
                response.TotalItemCount = 0;
                response.TotalItemCountSpecified = true;
                return response.SetSuccessResponse();
            }
            else
            {
                response.BuildResponseFromResult(erec_IraIratokResult.Ds.Tables[0], localUser);
                if (!String.IsNullOrEmpty(response.ErrorMessage))
                {
                    return response.SetErrorResponse(response.ErrorMessage);
                }
                else
                {
                    response.TotalItemCount = erec_IraIratokResult.Ds.Tables[0].Rows.Count;
                    response.TotalItemCountSpecified = true;
                    return response.SetSuccessResponse();
                }
            }
        }

        public static ASP_ADO_DOCUMENT.GetDocumentResponse GetDocument(ASP_ADO_DOCUMENT.DocumentByRegistrationNumberRequest documentByRegistrationNumberRequest, ASP_ADO_DOCUMENT.GetDocumentResponse response, string localUser)
        {
            var request = new ASP_ADO_DOCUMENT.DocumentListByRegistrationNumberRequest();
            request.RegistrationNumber = documentByRegistrationNumberRequest.RegistrationNumber;

            Result erec_IraIratokResult = new Result();
            erec_IraIratokResult = GetDocumentsData(request, 0, false, 0, false, localUser);

            if (erec_IraIratokResult.IsError)
            {
                return response.SetErrorResponse(erec_IraIratokResult.ErrorMessage);
            }
            else if (erec_IraIratokResult.Ds.Tables.Count == 0 || erec_IraIratokResult.Ds.Tables[0].Rows.Count == 0)
            {
                return response.SetSuccessResponse();
            }
            else if (erec_IraIratokResult.Ds.Tables.Count > 0 && erec_IraIratokResult.Ds.Tables[0].Rows.Count == 1)
            {
                response.BuildResponseFromResult(erec_IraIratokResult.Ds.Tables[0], localUser);
                if (!String.IsNullOrEmpty(response.ErrorMessage))
                {
                    return response.SetErrorResponse(response.ErrorMessage);
                }
                else
                {
                    return response.SetSuccessResponse();
                }
            }
            else
            {
                return response.SetErrorResponse("A megadott adatok alapján nem határozható meg egyértelműen a keresett tétel");
            }
        }

        public static ASP_ADO_DOCUMENT.FilingDocumentResponse GetDocument(string iratId, ASP_ADO_DOCUMENT.FilingDocumentResponse response, string localUser)
        {
            var request = new ASP_ADO_DOCUMENT.DocumentListByRegistrationNumberRequest();
            var documentsResponse = new ASP_ADO_DOCUMENT.GetDocumentsResponse();
            documentsResponse = GetDocuments(iratId, documentsResponse, 0, false, 0, false, localUser);

            if (!documentsResponse.IsSucceeded)
            {
                return response.SetErrorResponse(documentsResponse.ErrorMessage);
            }
            else if (documentsResponse.TotalItemCount == 0)
            {
                return response.SetSuccessResponse();
            }
            else if (documentsResponse.TotalItemCount == 1)
            {
                response.Document = documentsResponse.Documents.FirstOrDefault().MapFiling();
                return response.SetSuccessResponse();
            }
            else
            {
                return response.SetErrorResponse("A megadott adatok alapján nem határozható meg egyértelműen a keresett tétel");
            }
        }

        public static ASP_ADO_DOCUMENT.DocumentResponse GetDocumentByAzonosito(string azonosito, ASP_ADO_DOCUMENT.DocumentResponse response, string localUser)
        {
            EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);
            Result erec_IraIratokResult = new Result();

            erec_IraIratokSearch.Id.Value = azonosito;
            erec_IraIratokSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;
            erec_IraIratokSearch.OrderBy = "EREC_IraIratok.LetrehozasIdo ASC";

            erec_IraIratokResult = erec_IraIratokService.GetAllWithExtensionAndJogosultak(execParam, erec_IraIratokSearch, false);

            if (erec_IraIratokResult.IsError)
            {
                return response.SetErrorResponse(erec_IraIratokResult.ErrorMessage);
            }
            else if (erec_IraIratokResult.Ds.Tables.Count == 0 || erec_IraIratokResult.Ds.Tables[0].Rows.Count == 0)
            {
                return response.SetSuccessResponse();
            }
            else if (erec_IraIratokResult.Ds.Tables.Count > 0 && erec_IraIratokResult.Ds.Tables[0].Rows.Count == 1)
            {
                response.BuildResponseFromResult(erec_IraIratokResult.Ds.Tables[0], localUser);
                if (!String.IsNullOrEmpty(response.ErrorMessage))
                {
                    return response.SetErrorResponse(response.ErrorMessage);
                }
                else
                {
                    return response.SetSuccessResponse();
                }
            }
            else
            {
                return response.SetErrorResponse("A megadott adatok alapján nem határozható meg egyértelműen a keresett tétel");
            }
        }

        public static Result Expedialas(string iratId, string expMode, string localUser)
        {
            Result resultExpedialas = new Result();
            EREC_PldIratPeldanyok iratPeldany = null;

            using (var iratPeldanyService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService())
            {
                var res = iratPeldanyService.GetElsoIratPeldanyByIraIrat(GetExecParam(localUser), iratId);
                if (!res.IsError)
                {
                    iratPeldany = (EREC_PldIratPeldanyok)res.Record;
                }
                else
                {
                    return res;
                }
                string[] iratPeldanyokIdArray = { iratPeldany.Id };
                if (iratPeldany.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott)
                {
                    iratPeldany.KuldesMod = expMode;
                    iratPeldany.Updated.KuldesMod = true;
                    resultExpedialas = iratPeldanyService.Expedialas(GetExecParam(localUser), iratPeldanyokIdArray, "", iratPeldany, null);
                }
            }
            return resultExpedialas;
        }

        private static Dictionary<KRT_KodTarak, KRT_KodTarak> GetDocumentTypeMapping(string vezerloKodcsoportKod, string fuggoKodcsoportKod, string localUser)
        {
            Dictionary<KRT_KodTarak, KRT_KodTarak> dic = new Dictionary<KRT_KodTarak, KRT_KodTarak>();
            Contentum.eAdmin.Service.KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();

            KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
            search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", vezerloKodcsoportKod);
            search.Vezerlo_KodCsoport_Id.Operator = Contentum.eQuery.Query.Operators.inner;
            search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", fuggoKodcsoportKod);
            search.Fuggo_KodCsoport_Id.Operator = Contentum.eQuery.Query.Operators.inner;

            Result res = service.GetAll(GetExecParam(localUser), search);

            if (!res.IsError)
            {
                {
                    if (res.Ds.Tables[0].Rows.Count > 0)
                    {
                        string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();
                        KodtarFuggosegDataClass fuggosegek = JSONFunctions.DeSerialize(adat);

                        foreach (KodtarFuggosegDataItemClass item in fuggosegek.Items)
                        {
                            var kod1 = GetKodtarElem(item.VezerloKodTarId, localUser);
                            var kod2 = GetKodtarElem(item.FuggoKodtarId, localUser);
                            if (kod1 != null && kod2 != null)
                            {
                                dic.Add(kod1, kod2);
                            }
                        }
                        return dic;
                    }
                }
            }
            return dic;
        }

        private static KRT_KodTarak GetKodtarElem(string kodtarId, string localUser)
        {
            Contentum.eAdmin.Service.KRT_KodTarakService ktService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KodTarakService();
            ExecParam ktExecParam = GetExecParam(localUser);
            ktExecParam.Record_Id = kodtarId;
            Result resultFind = ktService.Get(ktExecParam);
            if (resultFind.IsError)
            {
                return null;
            }
            return (KRT_KodTarak)resultFind.Record;
        }

        public static void InitPackagesForKimeno(ASP_ADO_DOCUMENT.GetDocumentsDto document, List<string> kuldemenyIds, string localUser)
        {
            Result kuldemenyekResult = GetKuldemenyekByIds(kuldemenyIds, localUser);

            if (kuldemenyekResult.IsError)
                return;

            if (kuldemenyekResult.Ds.Tables.Count > 0 && kuldemenyekResult.Ds.Tables[0].Rows.Count > 0)
            {
                document.Packages = new ASP_ADO_DOCUMENT.GetDocumentsPackageDto[kuldemenyekResult.Ds.Tables[0].Rows.Count];
                KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();
                ExecParam cimekExecParam = GetExecParam(localUser);

                for (int i = 0; i < kuldemenyekResult.Ds.Tables[0].Rows.Count; i++)
                {
                    //DataRow row = kuldemenyekResult.Ds.Tables[0].Rows[i];
                    //document.Packages[i]= new GetDocumentsPackageDto();
                    //document.Packages[i].ArrivalInformation = new GetDocumentsArrivalInformationDto();
                }
            }
        }

        public static void InitPackagesForKimeno(ASP_ADO_DOCUMENT.GetDocumentDto document, List<string> kuldemenyIds, string localUser)
        {
            Result kuldemenyekResult = GetKuldemenyekByIds(kuldemenyIds, localUser);

            if (kuldemenyekResult.IsError)
                return;

            if (kuldemenyekResult.Ds.Tables.Count > 0 && kuldemenyekResult.Ds.Tables[0].Rows.Count > 0)
            {
                document.Packages = new ASP_ADO_DOCUMENT.GetDocumentPackageDto[kuldemenyekResult.Ds.Tables[0].Rows.Count];

                EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                ExecParam execParam = GetExecParam(localUser);

                for (int i = 0; i < kuldemenyekResult.Ds.Tables[0].Rows.Count; i++)
                {
                    DataRow row = kuldemenyekResult.Ds.Tables[0].Rows[i];
                    document.Packages[i] = new ASP_ADO_DOCUMENT.GetDocumentPackageDto();
                    document.Packages[i].TrackingNumber = row.GetRowValueAsString(Constants.TableColumns.Ragszam);

                    document.Packages[i].ArrivalInformation = new ASP_ADO_DOCUMENT.GetDocumentArrivalInformationDto();

                    if (!IsDBNull(row["Partner_Id_Bekuldo"]))
                    {
                        document.Partner = GetDocumentPartner(row.GetRowValueAsString("Partner_Id_Bekuldo"), localUser);
                    }

                    EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
                    search.Kuldemeny_Id.Value = row[Constants.TableColumns.Id].ToString();
                    search.Kuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                    Result tertivevenyekResult = service.GetAll(execParam, search);

                    if (!tertivevenyekResult.IsError && tertivevenyekResult.Ds.Tables.Count > 0 && tertivevenyekResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow tertivevenyRow = tertivevenyekResult.Ds.Tables[0].Rows[0];
                        document.Packages[i].ReceivingInformation = new ASP_ADO_DOCUMENT.GetDocumentReceivingInformationDto();

                        document.Packages[i].ReceivingInformation.ReceiverName = tertivevenyRow.GetRowValueAsString(Constants.TableColumns.AtvevoSzemely);

                        string adoIdInt = GetASPADOID(tertivevenyRow[Constants.TableColumns.Id].ToString(), localUser);
                        if (!string.IsNullOrEmpty(adoIdInt))
                        {
                            int value;
                            if (int.TryParse(adoIdInt, out value))
                            {
                                document.Packages[i].ReceivingInformationId = value;
                                document.Packages[i].ReceivingInformationIdSpecified = true;
                            }
                        }

                        if (!IsDBNull(tertivevenyRow[Constants.TableColumns.TertivisszaKod]))
                        {
                            string kod = tertivevenyRow[Constants.TableColumns.TertivisszaKod].ToString();
                            string tertiKod = "";

                            if (TertivevenyKodTipusok.Values.Any(x => x.Kod == kod))
                            {
                                tertiKod = TertivevenyKodTipusok.First(x => x.Value.Kod == kod).Key.Kod;
                            }
                            else
                            {
                                tertiKod = kod.PadLeft(3, '0');
                            }
                            document.Packages[i].ReceivingInformation.Type = tertiKod;
                        }
                        else
                        {
                            document.Packages[i].ReceivingInformation.Type = "000";
                        }
                        Logger.Debug("InitPackagesForKimeno(ASP_ADO_DOCUMENT.GetDocumentDto document, List<string> kuldemenyIds, string localUser))>GetReceivingDate");
                        DateTime? receivingDate = GetReceivingDate(tertivevenyRow, execParam);
                        if (receivingDate != null)
                        {
                            document.Packages[i].ReceivingInformation.ReceivingDate = (DateTime)receivingDate;
                            document.Packages[i].ReceivingInformation.ReceivingDateSpecified = true;
                        }

                        //if (!IsDBNull(tertivevenyRow[Constants.TableColumns.TertivisszaKod]))
                        //{
                        //    string kod = tertivevenyRow[Constants.TableColumns.TertivisszaKod].ToString();
                        //    if ((kod == "1" || kod == "2"))
                        //    {
                        //        document.Packages[i].ReceivingInformation.Result = true;
                        //        document.Packages[i].ReceivingInformation.ResultSpecified = true;
                        //    }
                        //    else if ((kod == "9" || kod == "10"))
                        //    {
                        //        document.Packages[i].ReceivingInformation.Result = false;
                        //        document.Packages[i].ReceivingInformation.ResultSpecified = true;
                        //    }
                        //}
                        document.Packages[i].ReceivingInformation.Result = true; // Muszáj true ra beállítani mert különben üres marad a receivingdate
                        document.Packages[i].ReceivingInformation.ResultSpecified = true;
                    }
                }
            }
        }
        public static void InitPackagesForKimeno(ASP_ADO_DOCUMENT.DocumentDto document, List<string> kuldemenyIds, string localUser)
        {
            Result kuldemenyekResult = GetKuldemenyekByIds(kuldemenyIds, localUser);

            if (kuldemenyekResult.IsError)
                return;

            if (kuldemenyekResult.Ds.Tables.Count > 0 && kuldemenyekResult.Ds.Tables[0].Rows.Count > 0)
            {
                EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                ExecParam execParam = GetExecParam(localUser);
                document.Packages = new ASP_ADO_DOCUMENT.PackageDto[kuldemenyekResult.Ds.Tables[0].Rows.Count];
                KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();
                ExecParam cimekExecParam = GetExecParam(localUser);

                for (int i = 0; i < kuldemenyekResult.Ds.Tables[0].Rows.Count; i++)
                {
                    DataRow row = kuldemenyekResult.Ds.Tables[0].Rows[i];
                    document.Packages[i] = new ASP_ADO_DOCUMENT.PackageDto();

                    if (!IsDBNull(row[Constants.TableColumns.Cim_Id]))
                    {
                        cimekExecParam.Record_Id = row[Constants.TableColumns.Cim_Id].ToString();

                        Result cimekResult = cimekService.Get(cimekExecParam);

                        if (!cimekResult.IsError)
                        {
                            KRT_Cimek cim = (KRT_Cimek)cimekResult.Record;
                            document.Packages[i].AddresseeAddress = BuildAddressDTOFromCim(cim, document, localUser);
                        }
                    }

                    document.Packages[i].AddresseeName = row.GetRowValueAsString(Constants.TableColumns.NevSTR_Bekuldo);

                    string adoIdLong = GetASPADOID(row[Constants.TableColumns.Id].ToString(), localUser);
                    if (!string.IsNullOrEmpty(adoIdLong))
                    {
                        long value;
                        if (long.TryParse(adoIdLong, out value))
                        {
                            document.Packages[i].ConsignmentId = value;
                            document.Packages[i].ConsignmentIdSpecified = true;
                        }
                    }

                    document.Packages[i].ConsignmentStates = row.GetRowValueAsString(Constants.TableColumns.Allapot);
                    document.Packages[i].CreateAddressPage = false;
                    document.Packages[i].CreateAddressPageSpecified = true;
                    document.Packages[i].MailProvider = ASP_ADO_DOCUMENT.EnumsMailProvider.DocumentHandlerSystem;
                    document.Packages[i].MailProviderSpecified = true;

                    document.Packages[i].SenderAddress = senderAddressDTO;

                    if (!IsDBNull(row[Constants.TableColumns.Csoport_ID_Cimzett]))
                    {
                        document.Packages[i].SenderName = GetCsoportNev(row[Constants.TableColumns.Csoport_ID_Cimzett].ToString(), localUser);
                    }

                    document.Packages[i].TrackingNumber = row.GetRowValueAsString(Constants.TableColumns.Ragszam);

                    EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
                    search.Kuldemeny_Id.Value = row[Constants.TableColumns.Id].ToString();
                    Result tertivevenyekResult = service.GetAll(execParam, search);

                    if (!tertivevenyekResult.IsError && tertivevenyekResult.Ds.Tables.Count > 0 && tertivevenyekResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow tertivevenyRow = tertivevenyekResult.Ds.Tables[0].Rows[0];
                        document.Packages[i].ReceivingInformation = new ASP_ADO_DOCUMENT.ReceivingInformationDto();

                        document.Packages[i].ReceivingInformation.ReceiverName = tertivevenyRow.GetRowValueAsString(Constants.TableColumns.AtvevoSzemely);

                        Logger.Debug("InitPackagesForKimeno(ASP_ADO_DOCUMENT.DocumentDto document, List<string> kuldemenyIds, string localUser)>GetReceivingDate");
                        DateTime? receivingDate = GetReceivingDate(tertivevenyRow, execParam);
                        if (receivingDate != null)
                        {
                            document.Packages[i].ReceivingInformation.ReceivingDate = (DateTime)receivingDate;
                            document.Packages[i].ReceivingInformation.ReceivingDateSpecified = true;
                        }

                        if (!IsDBNull(tertivevenyRow[Constants.TableColumns.TertivisszaKod]))
                        {
                            string kod = tertivevenyRow[Constants.TableColumns.TertivisszaKod].ToString();
                            //if ((kod == "1" || kod == "2"))
                            //{
                            //    document.Packages[i].ReceivingInformation.Result = true;
                            //    document.Packages[i].ReceivingInformation.ResultSpecified = true;
                            //}
                            //else if ((kod == "9" || kod == "10"))
                            //{
                            //    document.Packages[i].ReceivingInformation.Result = false;
                            //    document.Packages[i].ReceivingInformation.ResultSpecified = true;

                            if (kod == "9")
                            {
                                document.Packages[i].ReceivingInformation.ReturnReason = "Nem vette át";
                            }
                            else
                            {
                                document.Packages[i].ReceivingInformation.ReturnReason = "Ismeretlen címzett";
                            }
                            //}
                        }
                        document.Packages[i].ReceivingInformation.Result = true; // Muszáj true ra beállítani mert különben üres marad a receivingdate
                        document.Packages[i].ReceivingInformation.ResultSpecified = true;
                    }
                }
            }
        }

        public static void InitPackagesForBejovo(ASP_ADO_DOCUMENT.GetDocumentsDto document, string kuldemenyId, string localUser)
        {
            if (!String.IsNullOrEmpty(kuldemenyId))
            {
                EREC_KuldKuldemenyek kuldemeny = GetKuldemeny(kuldemenyId, localUser);

                if (kuldemeny != null)
                {
                    document.Packages = new ASP_ADO_DOCUMENT.GetDocumentsPackageDto[1];
                    document.Packages[0] = new ASP_ADO_DOCUMENT.GetDocumentsPackageDto();
                    document.Packages[0].ArrivalInformation = new ASP_ADO_DOCUMENT.GetDocumentsArrivalInformationDto();

                    if (!string.IsNullOrEmpty(kuldemeny.BeerkezesIdeje))
                    {
                        DateTime value;
                        if (DateTime.TryParse(kuldemeny.BeerkezesIdeje, out value))
                        {
                            document.Packages[0].ArrivalInformation.ArrivalDate = value;
                            document.Packages[0].ArrivalInformation.ArrivalDateSpecified = true;
                        }
                    }

                    document.Packages[0].ArrivalInformation.ArrivalNumber = kuldemeny.Azonosito;

                    if (!String.IsNullOrEmpty(kuldemeny.KezbesitesModja))
                    {
                        switch (kuldemeny.KezbesitesModja)
                        {
                            case "03":
                                document.Packages[0].ArrivalInformation.ArrivalModeName = "személyesen";
                                document.Packages[0].ArrivalInformation.ArrivalModeCode = "001";
                                break;
                            case "02":
                                document.Packages[0].ArrivalInformation.ArrivalModeName = "futár";
                                document.Packages[0].ArrivalInformation.ArrivalModeCode = "002";
                                break;
                            case "01":
                                if (!string.IsNullOrEmpty(kuldemeny.RagSzam))
                                {
                                    document.Packages[0].ArrivalInformation.ArrivalModeName = "könyvelt postai küldemény";
                                    document.Packages[0].ArrivalInformation.ArrivalModeCode = "003";
                                }
                                else
                                {
                                    document.Packages[0].ArrivalInformation.ArrivalModeName = "levél";
                                    document.Packages[0].ArrivalInformation.ArrivalModeCode = "005";
                                }
                                break;
                            case "05":
                                document.Packages[0].ArrivalInformation.ArrivalModeName = "fax";
                                document.Packages[0].ArrivalInformation.ArrivalModeCode = "004";
                                break;
                            default:
                                document.Packages[0].ArrivalInformation.ArrivalModeName = "külső rendszerből";
                                document.Packages[0].ArrivalInformation.ArrivalModeCode = "006";
                                break;
                        }
                    }
                }
            }
        }

        public static void InitPackagesForBejovo(ASP_ADO_DOCUMENT.GetDocumentDto document, string kuldemenyId, string localUser)
        {
            if (!String.IsNullOrEmpty(kuldemenyId))
            {
                EREC_KuldKuldemenyek kuldemeny = GetKuldemeny(kuldemenyId, localUser);

                if (kuldemeny != null)
                {
                    document.Packages = new ASP_ADO_DOCUMENT.GetDocumentPackageDto[1];
                    document.Packages[0] = new ASP_ADO_DOCUMENT.GetDocumentPackageDto();
                    document.Packages[0].ArrivalInformation = new ASP_ADO_DOCUMENT.GetDocumentArrivalInformationDto();
                    document.Packages[0].ReceivingInformation = new ASP_ADO_DOCUMENT.GetDocumentReceivingInformationDto();

                    document.Packages[0].TrackingNumber = kuldemeny.RagSzam;


                    EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                    ExecParam execParam = GetExecParam(localUser);
                    EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
                    search.Kuldemeny_Id.Value = kuldemeny.Id;
                    Result tertivevenyekResult = service.GetAll(execParam, search);

                    if (!tertivevenyekResult.IsError && tertivevenyekResult.Ds.Tables.Count > 0 && tertivevenyekResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow tertivevenyRow = tertivevenyekResult.Ds.Tables[0].Rows[0];
                        document.Packages[0].ReceivingInformation = new ASP_ADO_DOCUMENT.GetDocumentReceivingInformationDto();

                        document.Packages[0].ReceivingInformation.ReceiverName = tertivevenyRow.GetRowValueAsString(Constants.TableColumns.AtvevoSzemely);

                        string adoIdInt = GetASPADOID(tertivevenyRow[Constants.TableColumns.Id].ToString(), localUser);
                        if (!string.IsNullOrEmpty(adoIdInt))
                        {
                            int value;
                            if (int.TryParse(adoIdInt, out value))
                            {
                                document.Packages[0].ReceivingInformationId = value;
                                document.Packages[0].ReceivingInformationIdSpecified = true;
                            }
                        }
                        Logger.Debug("InitPackagesForBejovo(ASP_ADO_DOCUMENT.GetDocumentDto document, string kuldemenyId, string localUser)>GetReceivingDate");
                        DateTime? receivingDate = GetReceivingDate(tertivevenyRow, execParam);
                        if (receivingDate != null)
                        {
                            document.Packages[0].ReceivingInformation.ReceivingDate = receivingDate;
                            document.Packages[0].ReceivingInformation.ReceivingDateSpecified = true;
                        }

                        //if (!IsDBNull(tertivevenyRow[Constants.TableColumns.TertivisszaKod]))
                        //{
                        //    string kod = tertivevenyRow[Constants.TableColumns.TertivisszaKod].ToString();
                        //    if ((kod == "1" || kod == "2"))
                        //    {
                        //        document.Packages[0].ReceivingInformation.Result = true;
                        //        document.Packages[0].ReceivingInformation.ResultSpecified = true;
                        //    }
                        //    else if ((kod == "9" || kod == "10"))
                        //    {
                        //        document.Packages[0].ReceivingInformation.Result = false;
                        //        document.Packages[0].ReceivingInformation.ResultSpecified = true;
                        //    }
                        //}
                        document.Packages[0].ReceivingInformation.Result = true; // Muszáj true ra beállítani mert különben üres marad a receivingdate
                        document.Packages[0].ReceivingInformation.ResultSpecified = true;
                        if (!IsDBNull(tertivevenyRow[Constants.TableColumns.TertivisszaKod]))
                        {
                            string kod = tertivevenyRow[Constants.TableColumns.TertivisszaKod].ToString();
                            if (kod == "9")
                            {
                                document.Packages[0].ReceivingInformation.Type = "010";
                            }
                            else if (kod == "10")
                            {
                                document.Packages[0].ReceivingInformation.Type = "009";
                            }
                            else if (kod == "1")
                            {
                                document.Packages[0].ReceivingInformation.Type = "001";
                            }
                            else if (kod == "2")
                            {
                                document.Packages[0].ReceivingInformation.Type = "004";
                            }
                        }
                        else
                        {
                            document.Packages[0].ReceivingInformation.Type = "000";
                        }
                    }

                }
            }
        }

        private static DateTime? GetReceivingDate(DataRow tertivevenyRow, ExecParam execParam)
        {
            DateTime? resultDate = null;
            var tertiVisszaDat = tertivevenyRow[Constants.TableColumns.TertivisszaDat];
            var tertiVisszaKod = tertivevenyRow[Constants.TableColumns.TertivisszaKod];
            var atvetelDat = tertivevenyRow[Constants.TableColumns.AtvetelDat];
            var ragSzam = tertivevenyRow[Constants.TableColumns.Ragszam];
            var kezbVelelemDatuma = tertivevenyRow[Constants.TableColumns.KezbVelelemDatuma];

            if (IsDBNull(ragSzam))
            {
                Logger.Error("ASPADOHelper.GetReceivingDate: Üres a ragszám " + tertivevenyRow[Constants.TableColumns.Id]);
                ragSzam = "-üres-";
            }

            if (IsDBNull(tertiVisszaKod))
            {
                Logger.Error("ASPADOHelper.GetReceivingDate: Üres a tertiVisszaKod Ragszam: " + ragSzam);
                return resultDate;
            }

            /*
           A KodTarak.TERTI_VISSZA_KOD_POSTA.Nem_kereste;
           értékhez függő kódtárral hozzárendelt TERTIVEVENY_VISSZA_KOD értékek.
            */
            var kod = KodTarak.TERTI_VISSZA_KOD_POSTA.Nem_kereste;
            var nemKeresteKod = string.Empty;
            bool nincsItem;
            var tvKodok = KodtarFuggoseg.GetFuggoKodtarakKodList(execParam, "TERTI_VISSZA_KOD_POSTA", "TERTIVEVENY_VISSZA_KOD", kod, out nincsItem);
            if (nincsItem || tvKodok == null || tvKodok.Count == 0)
            {
                Logger.Debug("ASPADOHelper. Nincs összerendelve a TERTI_VISSZA_KOD_POSTA a TERTIVEVENY_VISSZA_KOD-al");
                nemKeresteKod = KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_nem_kereste;
            }
            else
            {
                nemKeresteKod = tvKodok.First();
            }
            bool nemKereste = (string)tertiVisszaKod == nemKeresteKod;

            if (nemKereste)
            {
                resultDate = GetReceivingDateForNemKereste(execParam, tertiVisszaDat, ragSzam, atvetelDat, kezbVelelemDatuma);
            }
            else
            {
                resultDate = GetReceivingDateForOther(execParam, tertiVisszaDat, ragSzam, atvetelDat, kezbVelelemDatuma);
            }

            Logger.Debug("ASPADOHelper.GetReceivingDate: " + (resultDate == null ? "null" : resultDate.ToString()));
            return resultDate;
        }

        private static DateTime? GetReceivingDateForNemKereste(ExecParam execParam, object tertiVisszaDat, object ragSzam, object atvetelDat, object kezbVelelemDatuma)
        {
            DateTime? resultDate = null;

            //Ha van atvetelDat, akkor atvetelDat + 5 munkanapot adunk vissza
            if (!IsDBNull(atvetelDat))
            {
                DateTime temp;
                bool isParsable = DateTime.TryParse(atvetelDat.ToString(), out temp);

                if (!isParsable)
                {
                    Logger.Error("ASPADOHelper.GetReceivingDate: Nem lehet a atvetelDat ot parsolni. Ragszam: " + ragSzam);
                    return resultDate;
                }
                return AddWorkingDays(execParam, temp, 5, ragSzam.ToString());
            }
            //Ha van tertiVisszaDat, akkor tertiVisszaDat + 5 munkanapot adunk vissza
            else if (!IsDBNull(tertiVisszaDat))
            {
                DateTime temp;
                bool isParsable = DateTime.TryParse(tertiVisszaDat.ToString(), out temp);

                if (!isParsable)
                {
                    Logger.Error("ASPADOHelper.GetReceivingDateForNemKereste: Nem lehet a tertiVisszaDat ot parsolni. Ragszam: " + ragSzam);
                    return resultDate;
                }
                return AddWorkingDays(execParam, temp, 5, ragSzam.ToString());
            }// Megprobáljuk a KezbesitesiVelelemmel kitölteni
            else if (!IsDBNull(kezbVelelemDatuma))
            {
                DateTime temp;
                bool isParsable = DateTime.TryParse(kezbVelelemDatuma.ToString(), out temp);

                if (!isParsable)
                {
                    Logger.Error("ASPADOHelper.GetReceivingDateForNemKereste: Nem lehet a kezbVelelemDatuma -t parsolni. Ragszam: " + ragSzam);
                    return resultDate;
                }
                return temp;
            }
            return resultDate;
        }

        private static DateTime? GetReceivingDateForOther(ExecParam execParam, object tertiVisszaDat, object ragSzam, object atvetelDat, object kezbVelelemDatuma)
        {
            DateTime? resultDate = null;
            if (!IsDBNull(kezbVelelemDatuma))
            {
                DateTime temp;
                bool isParsable = DateTime.TryParse(atvetelDat.ToString(), out temp);
                if (!isParsable)
                {
                    Logger.Error("ASPADOHelper.GetReceivingDateForOther: Nem lehet a atvetelDat ot parsolni. Ragszam: " + ragSzam);
                    return resultDate;
                }
                return temp;
            }
            else if (!IsDBNull(atvetelDat))
            {
                DateTime temp;
                bool isParsable = DateTime.TryParse(atvetelDat.ToString(), out temp);
                if (!isParsable)
                {
                    Logger.Error("ASPADOHelper.GetReceivingDateForOther: Nem lehet a atvetelDat ot parsolni. Ragszam: " + ragSzam);
                    return resultDate;
                }
                return temp;
            }
            return resultDate;
        }

        private static DateTime? AddWorkingDays(ExecParam execParam, DateTime fromDate, int days, string ragSzam)
        {
            DateTime? resultDate = null;
            KRT_Extra_NapokService service_extra_napok = eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
            Result result = service_extra_napok.KovetkezoMunkanap(execParam, fromDate, days);
            if (result.IsError)
            {
                Logger.Error("ASPADOHelper.GetReceivingDateForNemKereste: Nem sikerült a " + days + ". munkanap meghatározása Ragszam: " + ragSzam + " Err: " + result.ErrorMessage);
                return resultDate;
            }
            resultDate = (DateTime)result.Record;
            return resultDate;
        }

        public static void InitPackagesForBejovo(ASP_ADO_DOCUMENT.DocumentDto document, string kuldemenyId, string localUser)
        {
            if (!String.IsNullOrEmpty(kuldemenyId))
            {
                EREC_KuldKuldemenyek kuldemeny = GetKuldemeny(kuldemenyId, localUser);

                if (kuldemeny != null)
                {
                    document.Packages = new ASP_ADO_DOCUMENT.PackageDto[1];
                    document.Packages[0] = new ASP_ADO_DOCUMENT.PackageDto();
                    document.Packages[0].ArrivalInformation = new ASP_ADO_DOCUMENT.ArrivalInformationDto();
                    document.Packages[0].ReceivingInformation = new ASP_ADO_DOCUMENT.ReceivingInformationDto();

                    if (!string.IsNullOrEmpty(kuldemeny.BeerkezesIdeje))
                    {
                        DateTime value;
                        if (DateTime.TryParse(kuldemeny.BeerkezesIdeje, out value))
                        {
                            document.Packages[0].ArrivalInformation.ArrivalDate = value;
                            document.Packages[0].ArrivalInformation.ArrivalDateSpecified = true;
                        }
                    }

                    document.Packages[0].AddresseeAddress = addresseeAddressDTO;

                    document.Packages[0].ArrivalInformation.ArrivalNumber = kuldemeny.Azonosito;
                    document.Packages[0].ConsignmentStates = kuldemeny.Allapot;
                    document.Packages[0].CreateAddressPage = false;
                    document.Packages[0].CreateAddressPageSpecified = true;

                    document.Packages[0].TrackingNumber = kuldemeny.RagSzam;


                    if (!string.IsNullOrEmpty(kuldemeny.Cim_Id))
                    {
                        KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();
                        ExecParam cimekExecParam = GetExecParam(localUser);
                        cimekExecParam.Record_Id = kuldemeny.Cim_Id;

                        Result cimekResult = cimekService.Get(cimekExecParam);

                        if (!cimekResult.IsError)
                        {
                            KRT_Cimek cim = (KRT_Cimek)cimekResult.Record;
                            document.Packages[0].SenderAddress = BuildAddressDTOFromCim(cim, document, localUser);
                        }
                    }


                    if (!String.IsNullOrEmpty(kuldemeny.Csoport_Id_Cimzett))
                    {
                        string addrNev = GetCsoportNev(kuldemeny.Csoport_Id_Cimzett, localUser);
                        if (!string.IsNullOrEmpty(addrNev))
                        {
                            document.Packages[0].AddresseeName = addrNev;
                        }
                    }

                    if (!String.IsNullOrEmpty(kuldemeny.KezbesitesModja))
                    {
                        switch (kuldemeny.KezbesitesModja)
                        {
                            case "03":
                                document.Packages[0].ArrivalInformation.ArrivalModeName = "személyesen";
                                document.Packages[0].ArrivalInformation.ArrivalModeCode = "001";
                                break;
                            case "02":
                                document.Packages[0].ArrivalInformation.ArrivalModeName = "futár";
                                document.Packages[0].ArrivalInformation.ArrivalModeCode = "002";
                                break;
                            case "01":
                                if (!string.IsNullOrEmpty(kuldemeny.RagSzam))
                                {
                                    document.Packages[0].ArrivalInformation.ArrivalModeName = "könyvelt postai küldemény";
                                    document.Packages[0].ArrivalInformation.ArrivalModeCode = "003";
                                }
                                else
                                {
                                    document.Packages[0].ArrivalInformation.ArrivalModeName = "levél";
                                    document.Packages[0].ArrivalInformation.ArrivalModeCode = "005";
                                }
                                break;
                            case "05":
                                document.Packages[0].ArrivalInformation.ArrivalModeName = "fax";
                                document.Packages[0].ArrivalInformation.ArrivalModeCode = "004";
                                break;
                            default:
                                document.Packages[0].ArrivalInformation.ArrivalModeName = "külső rendszerből";
                                document.Packages[0].ArrivalInformation.ArrivalModeCode = "006";
                                break;
                        }
                    }

                    string adoIdLong = GetASPADOID(kuldemeny.Id, localUser);
                    if (!string.IsNullOrEmpty(adoIdLong))
                    {
                        long value;
                        if (long.TryParse(adoIdLong, out value))
                        {
                            document.Packages[0].ConsignmentId = value;
                            document.Packages[0].ConsignmentIdSpecified = true;
                        }

                    }

                    EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                    ExecParam execParam = GetExecParam(localUser);
                    EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
                    search.Kuldemeny_Id.Value = kuldemeny.Id;
                    Result tertivevenyekResult = service.GetAll(execParam, search);

                    if (!tertivevenyekResult.IsError && tertivevenyekResult.Ds.Tables.Count > 0 && tertivevenyekResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow tertivevenyRow = tertivevenyekResult.Ds.Tables[0].Rows[0];
                        document.Packages[0].ReceivingInformation = new ASP_ADO_DOCUMENT.ReceivingInformationDto();

                        document.Packages[0].ReceivingInformation.ReceiverName = tertivevenyRow.GetRowValueAsString(Constants.TableColumns.AtvevoSzemely);

                     
                        Logger.Debug("InitPackagesForBejovo(ASP_ADO_DOCUMENT.DocumentDto document, string kuldemenyId, string localUser)>GetReceivingDate");
                        DateTime? receivingDate = GetReceivingDate(tertivevenyRow, execParam);
                        if (receivingDate != null)
                        {
                            document.Packages[0].ReceivingInformation.ReceivingDate = (DateTime)receivingDate;
                            document.Packages[0].ReceivingInformation.ReceivingDateSpecified = true;
                        }

                        if (!IsDBNull(tertivevenyRow[Constants.TableColumns.TertivisszaKod]))
                        {
                            string kod = tertivevenyRow[Constants.TableColumns.TertivisszaKod].ToString();
                            //if ((kod == "1" || kod == "2"))
                            //{
                            //    document.Packages[0].ReceivingInformation.Result = true;
                            //    document.Packages[0].ReceivingInformation.ResultSpecified = true;
                            //}
                            //else if ((kod == "9" || kod == "10"))
                            //{
                            //    document.Packages[0].ReceivingInformation.Result = false;
                            //    document.Packages[0].ReceivingInformation.ResultSpecified = true;

                            if (kod == "9")
                            {
                                document.Packages[0].ReceivingInformation.ReturnReason = "Nem vette át";
                            }
                            else
                            {
                                document.Packages[0].ReceivingInformation.ReturnReason = "Ismeretlen címzett";
                            }
                            //}
                        }
                        document.Packages[0].ReceivingInformation.Result = true; // Muszáj true ra beállítani mert különben üres marad a receivingdate
                        document.Packages[0].ReceivingInformation.ResultSpecified = true;
                    }

                }
            }
        }

        public static List<string> GetKuldemenyIDsByIratId(string iratId, string localUser)
        {
            List<string> kuldemenyIds = new List<string>();

            if (!string.IsNullOrEmpty(iratId))
            {
                EREC_Kuldemeny_IratPeldanyaiService kuldemenyIratPeldanyaiService = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService();
                ExecParam kuldemenyIratPeldanyaiExecParam = GetExecParam(localUser);
                EREC_Kuldemeny_IratPeldanyaiSearch kuldemenyIratPeldanyaiSearch = new EREC_Kuldemeny_IratPeldanyaiSearch();

                EREC_PldIratPeldanyokService iratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                ExecParam iratPeldanyokExecParam = GetExecParam(localUser);
                EREC_PldIratPeldanyokSearch iratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();
                iratPeldanyokSearch.IraIrat_Id.Value = iratId;
                iratPeldanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                Result iratPeldanyokResult = iratPeldanyokService.GetAll(iratPeldanyokExecParam, iratPeldanyokSearch);

                if (!iratPeldanyokResult.IsError && iratPeldanyokResult.Ds.Tables.Count > 0 && iratPeldanyokResult.Ds.Tables[0].Rows.Count > 0)
                {
                    List<string> values = new List<string>();
                    foreach (DataRow iratPeldanyokRow in iratPeldanyokResult.Ds.Tables[0].Rows)
                    {
                        if (!IsDBNull(iratPeldanyokRow[Constants.TableColumns.Id]))
                        {
                            values.Add(iratPeldanyokRow[Constants.TableColumns.Id].ToString());
                        }
                    }

                    kuldemenyIratPeldanyaiSearch.Peldany_Id.Value = Search.GetSqlInnerString(values.ToArray());
                    kuldemenyIratPeldanyaiSearch.Peldany_Id.Operator = Contentum.eQuery.Query.Operators.inner;

                    Result kuldemenyIratPeldanyaiResult = kuldemenyIratPeldanyaiService.GetAll(kuldemenyIratPeldanyaiExecParam, kuldemenyIratPeldanyaiSearch);

                    if (!kuldemenyIratPeldanyaiResult.IsError && kuldemenyIratPeldanyaiResult.Ds.Tables.Count > 0 && kuldemenyIratPeldanyaiResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        kuldemenyIds = kuldemenyIratPeldanyaiResult.Ds.Tables[0].AsEnumerable().Select(x => x["KuldKuldemeny_Id"].ToString()).Distinct().ToList();
                    }
                }
            }
            return kuldemenyIds;
        }

        public static Result GetKuldemenyekByIds(List<string> ids, string localUser)
        {
            EREC_KuldKuldemenyekService kuldemenyService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = GetExecParam(localUser);
            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            search.Id.Value = Search.GetSqlInnerString(ids.ToArray());
            search.Id.Operator = Contentum.eQuery.Query.Operators.inner;

            Result result = kuldemenyService.GetAll(execParam, search);

            return result;
        }
        #endregion

        public static ASP_ADO_DOCUMENT.GetOrganizationUnitsResponse GetOrganizationUnitsData(string localUser)
        {
            var response = new ASP_ADO_DOCUMENT.GetOrganizationUnitsResponse();

            ExecParam execParam = ASPADOHelper.GetExecParam(localUser, true);

            Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportTagok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

            KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();

            search_csoportok.Csoport_Id_Jogalany.Value = localUser;
            search_csoportok.Csoport_Id_Jogalany.Operator = Contentum.eQuery.Query.Operators.equals;
            search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

            Result result_csoportok = service_csoportTagok.GetAllWithExtension(execParam, search_csoportok);

            if (result_csoportok.IsError)
            {
                return response.SetErrorResponse(result_csoportok.ErrorMessage);
            }
            else if (result_csoportok.Ds.Tables.Count == 0 || result_csoportok.Ds.Tables[0].Rows.Count == 0)
            {
                return response.SetErrorResponse(Constants.Messages.OrganizationUnitsNotFoundError);
            }
            else
            {
                int i = 0;

                response.OrganizationUnits = new ASP_ADO_DOCUMENT.GetOrganizationUnitsDto[result_csoportok.Ds.Tables[0].Rows.Count];

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    response.OrganizationUnits[i] = new ASP_ADO_DOCUMENT.GetOrganizationUnitsDto();
                    // response.OrganizationUnits[i].CsoportTagId = row["Id"].ToString(); DTAP=>NEW WSDL 
                    response.OrganizationUnits[i].Name = row["Csoport_Nev"].ToString();
                    response.OrganizationUnits[i].OrganizationUnitId = row["Csoport_Id"].ToString();
                    response.OrganizationUnits[i].Roles = new ASP_ADO_DOCUMENT.GetOrganizationUnitsUserRolesDto[0];
                    i++;
                }
            }

            return response.SetSuccessResponse();
        }


        #region Filing

        public static string GetKuldemenyIktatokonyvId(ASP_ADO_DOCUMENT.BaseResponse response, string localUser)
        {
            ExecParam execParam = GetExecParam(localUser);
            var result = TomegesIktatasHelper.GetKuldemenyIktatokonyvId(execParam);

            string id;

            if (result.IsError)
            {
                response.SetErrorResponse(result.ErrorMessage);
                return null;
            }
            else if (result.GetCount < 1)
            {
                response.SetErrorResponse("Nem található érkeztetőkönyv");
                return null;
            }
            else
            {
                id = result.Ds.Tables[0].Rows[0][Constants.TableColumns.Id].ToString();
            }

            return id;
        }

        private static string AddString(this string s, object toAdd)
        {
            if (toAdd != null)
            {
                s += " " + toAdd.ToString();
            }
            return s;
        }

        private static string AddStringWitComma(this string s, string toAdd)
        {
            if (!string.IsNullOrEmpty(toAdd))
            {
                s += ", " + toAdd;
            }
            return s;
        }

        public static IktatasiParameterek GetIktatasiParameterkFromRequestForFiling()
        {
            if (defaultIktatasiParameter != null)
            {
                return defaultIktatasiParameter;
            }

            defaultIktatasiParameter = new IktatasiParameterek();

            defaultIktatasiParameter.IratpeldanyVonalkodGeneralasHaNincs = false;
            defaultIktatasiParameter.UgyiratUjranyitasaHaLezart = true;
            defaultIktatasiParameter.UgyiratPeldanySzukseges = false;
            defaultIktatasiParameter.KeszitoPeldanyaSzukseges = false;
            defaultIktatasiParameter.Atiktatas = false;
            defaultIktatasiParameter.EmptyUgyiratSztorno = false;
            defaultIktatasiParameter.MunkaPeldany = false;

            return defaultIktatasiParameter;
        }

        public static DateTime GetIntezesiHataridoByIdoegyseg(ExecParam ExecParam, string Idoegyseg, string IntezesiIdo, DateTime DatumTol, out string ErrorMessage)
        {
            Logger.Debug("GetIntezesiHataridoByIdoegyseg - START");
            Logger.Debug(String.Format("Bemenõ paraméterek: Idõegység: {0}; Intézési idõ: {1}; Dátum: {2}", Idoegyseg, IntezesiIdo, DatumTol));

            ErrorMessage = String.Empty;
            DateTime dtHatarido = DateTime.MinValue;

            int nIdoegysegParsed = 0;
            int nIntezesiIdoParsed = 0;

            if (String.IsNullOrEmpty(Idoegyseg) || Idoegyseg == "0"
                || String.IsNullOrEmpty(IntezesiIdo) || IntezesiIdo == "0")
            {
                // default érték használata
                int nDefaultIntezesiIdo = Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.DEFAULT_INTEZESI_IDO);
                int nDefaultIdoegyseg = Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.DEFAULT_INTEZESI_IDO_IDOEGYSEG);

                if (nDefaultIdoegyseg == 0)
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65101); //"Az alapértelmezett idõegység nem határozható meg!"
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                if (nDefaultIntezesiIdo == 0)
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65102); //"Az alapértelmezett intézési idõ nem határozható meg!"
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                nIdoegysegParsed = nDefaultIdoegyseg;
                nIntezesiIdoParsed = nDefaultIntezesiIdo;
            }
            else
            {
                int nIdoegyseg;
                if (!Int32.TryParse(Idoegyseg, out nIdoegyseg))
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65103); //"Az idõegység nem határozható meg."
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                int nIntezesiIdo;
                if (!Int32.TryParse(IntezesiIdo, out nIntezesiIdo))
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65104); //"Az intézési idõ nem határozható meg!"
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                nIdoegysegParsed = nIdoegyseg;
                nIntezesiIdoParsed = nIntezesiIdo;
            }

            Logger.Debug(String.Format("Elemzett idõegység: {0}; Elemzett intézési idõ: {1}", nIdoegysegParsed, nIntezesiIdoParsed));

            // intézési határidõ meghatározása
            if (nIdoegysegParsed < 0)
            {
                switch (nIdoegysegParsed.ToString())
                {
                    case KodTarak.IDOEGYSEG.Munkanap:   // -1440                    
                        Contentum.eAdmin.Service.KRT_Extra_NapokService service_extra_napok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_Extra_NapokService();

                        Result result_extra_napok = service_extra_napok.KovetkezoMunkanap(ExecParam, DatumTol, nIntezesiIdoParsed);
                        if (result_extra_napok.IsError)
                        {
                            ErrorMessage = ResultError.GetErrorMessageFromResultObject(result_extra_napok);
                        }
                        else
                        {
                            try
                            {
                                dtHatarido = (DateTime)result_extra_napok.Record;
                            }
                            catch
                            {
                                dtHatarido = DateTime.MinValue;
                                ErrorMessage = "Rossz dátum formátum.";
                            }
                        }

                        // óra, perc beállítás
                        dtHatarido = dtHatarido.AddHours(DatumTol.Hour);
                        dtHatarido = dtHatarido.AddMinutes(DatumTol.Minute);
                        break;
                    default:
                        ErrorMessage = ResultError.GetErrorMessageByErrorCode(65105); //"Ismeretlen idõegység. Az idõegységre vonatkozó határidõszámítás nincs implementálva!"
                        break;
                }
            }
            else
            {
                dtHatarido = DatumTol.AddMinutes(nIdoegysegParsed * nIntezesiIdoParsed);
            }

            Logger.Debug(String.Format("Határidõ: {0}", dtHatarido));
            Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
            return dtHatarido;
        }

        public static void RefreshGuidCacheByPartner(string localUser)
        {
            var partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            Result partnerIdGuidResult = partnerService.GetAllASPADOPartnerId(ASPADOHelper.GetExecParam(localUser));

            if (!partnerIdGuidResult.IsError && partnerIdGuidResult.GetCount > 0)
            {
                foreach (DataRow row in partnerIdGuidResult.Ds.Tables[0].Rows)
                {
                    if (!GuidCache.ContainsKey(row["Id"].ToString()))
                    {
                        GuidCache.Add(row["Id"].ToString(), row["ASPADOId"].ToString());
                    }
                }
            }
        }

        public static void RefreshGuidCacheByCim(string localUser)
        {
            var cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();
            Result cimekIdGuidResult = cimekService.GetAllASPADOAddressId(ASPADOHelper.GetExecParam(localUser));

            if (!cimekIdGuidResult.IsError && cimekIdGuidResult.GetCount > 0)
            {
                foreach (DataRow row in cimekIdGuidResult.Ds.Tables[0].Rows)
                {
                    if (!GuidCache.ContainsKey(row["Id"].ToString()))
                    {
                        GuidCache.Add(row["Id"].ToString(), row["ASPADOId"].ToString());
                    }
                }
            }
        }
        #endregion

        #region BecameFinal

        public static IBaseResponse SetObjektumTargyszavak(ExecParam execParam, string objId, string objTableName, Dictionary<string, string> targyszoErtekek, ASP_ADO_DOCUMENT.BaseResponse response)
        {
            #region Objektum tárgyszavak lekérése

            EREC_ObjektumTargyszavaiService serviceObjTargyszavai = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

            EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

            Result resultGetAll = serviceObjTargyszavai.GetAllMetaByObjMetaDefinicio(execParam, search, objId, null
                , objTableName, null, null, false
                , null, false, true);


            if (resultGetAll.IsError)
            {
                return response.SetErrorResponse(resultGetAll.ErrorMessage);
            }

            Dictionary<string, DataRow> objTargyszavakDict = new Dictionary<string, DataRow>();

            foreach (DataRow row in resultGetAll.Ds.Tables[0].Rows)
            {
                string belsoAzonosito = row["BelsoAzonosito"].ToString();

                // Paraméterben jött-e ilyen tárgyszó:
                var keresettTargyszo = targyszoErtekek.FirstOrDefault(e => e.Key == belsoAzonosito);

                objTargyszavakDict[belsoAzonosito] = row;
            }

            #endregion

            #region EREC_ObjektumTargyszavai objektumok feltöltése

            List<EREC_ObjektumTargyszavai> modositandoObjTargyszavak = new List<EREC_ObjektumTargyszavai>();

            foreach (KeyValuePair<string, string> modositandoTargyszo in targyszoErtekek)
            {
                // Ha a lekérdezés nem adott vissza ilyen tárgyszót, akkor hiba:
                if (!objTargyszavakDict.ContainsKey(modositandoTargyszo.Key))
                {
                    return response.SetErrorResponse("Hiányzó tárgyszó: " + modositandoTargyszo.Key);
                }

                DataRow rowObjTargyszo = objTargyszavakDict[modositandoTargyszo.Key];

                // Tárgyszavas objektum feltöltése:
                EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                erec_ObjektumTargyszavai.Updated.SetValueAll(false);
                erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

                erec_ObjektumTargyszavai.Id = rowObjTargyszo[Constants.TableColumns.Id].ToString();
                erec_ObjektumTargyszavai.Updated.Id = true;

                erec_ObjektumTargyszavai.Obj_Metaadatai_Id = rowObjTargyszo["Obj_Metaadatai_Id"].ToString();
                erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

                erec_ObjektumTargyszavai.Targyszo_Id = rowObjTargyszo["Targyszo_Id"].ToString();
                erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

                erec_ObjektumTargyszavai.Targyszo = rowObjTargyszo["Targyszo"].ToString();
                erec_ObjektumTargyszavai.Updated.Targyszo = true;

                erec_ObjektumTargyszavai.Ertek = modositandoTargyszo.Value;
                erec_ObjektumTargyszavai.Updated.Ertek = true;

                modositandoObjTargyszavak.Add(erec_ObjektumTargyszavai);
            }

            #endregion

            #region Objektum tárgyszó insert/update

            // Insert/update webservice hívása:
            var resultInsertUpdate = serviceObjTargyszavai.InsertOrUpdateValuesByObjMetaDefinicio(
                            execParam
                            , modositandoObjTargyszavak.ToArray()
                            , objId.ToString()
                            , null
                            , objTableName, "*", null, false, null, false, false);

            if (resultInsertUpdate.IsError)
            {
                return response.SetErrorResponse(resultInsertUpdate.ErrorMessage);
            }

            #endregion

            return response;
        }

        public static Dictionary<string, string> LoadTargySzavakFromTemplate(HatosagiStatisztikaTargyszavakTemplate targyszavakTemplate)
        {
            Dictionary<string, string> targyszok = new Dictionary<string, string>();

            if (targyszavakTemplate == null)
            {
                return null;
            }

            var props = targyszavakTemplate.GetProperties();
            foreach (var prop in props)
            {
                var attrs = prop.GetCustomAttributes(typeof(TargyszavakIdAttribute), false);
                if (attrs != null && attrs.Length == 1)
                {
                    var id = (attrs[0] as TargyszavakIdAttribute).Id;
                    if (!String.IsNullOrEmpty(id))
                    {
                        object val = prop.GetValue(targyszavakTemplate, null);
                        targyszok.Add(prop.Name, val == null ? null : val as string);
                    }
                }
            }

            return targyszok;
        }

        private static string GetTemplateName(string documentType, string defaultExpedialasCode)
        {
            string prefix = "ASPADO_";
            string template = template = prefix + "{0}";
            if (!string.IsNullOrEmpty(documentType))
            {
                template = prefix + documentType + "_{0}";
            }

            if (defaultExpedialasCode.Trim() == "001")
            {
                //ASPADO_postai
                return string.Format(template, "postai");
            }
            //ASPADO_nempostai
            return string.Format(template, "nempostai");
        }


        public static string GetTemplateForDocumentType(string documentType, string localUser, string defaultExpedialasCode)
        {
            Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

            KRT_UIMezoObjektumErtekekSearch search = new KRT_UIMezoObjektumErtekekSearch();
            var execParam = ASPADOHelper.GetExecParam(localUser);
            search.Nev.Value = GetTemplateName(documentType, defaultExpedialasCode);
            search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result_Search = service.GetAll(execParam, search);
            if (result_Search.IsError)
            {
                Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.GetAll", execParam, result_Search);
            }
            var resultList = result_Search.GetData<KRT_UIMezoObjektumErtekek>();

            if (resultList.Count == 0)
            {
                search = new KRT_UIMezoObjektumErtekekSearch();
                //Nincs template a dokumentum tipushoz ezért az előre definiált template idkat használjuk
                search.Nev.Value = GetTemplateName(string.Empty, defaultExpedialasCode);
                search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

                result_Search = service.GetAll(execParam, search);
            }
            if (result_Search.IsError)
            {
                Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.GetAll", execParam, result_Search);
            }
            resultList = result_Search.GetData<KRT_UIMezoObjektumErtekek>();
            if (resultList.Count == 0)
            {
                Logger.Error("Could not find default template with Name: " + search.Nev.Value);
                return null;
            }

            //LZS - BLG_10089
            //Obsolate ccode
            //HatosagiStatisztikaTargyszavakTemplate template = (HatosagiStatisztikaTargyszavakTemplate)XmlFunction.XmlToObject(resultList.First().TemplateXML, typeof(HatosagiStatisztikaTargyszavakTemplate));
            string templateId = resultList.First().Id;

            return templateId;
        }

        public static bool HasObjektumTargyszavai(EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch
         , string Obj_Id, string ObjMetaDefinicio_Id
         , string TableName, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude
         , string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, string localUser)
        {

            Result result = GetAllMetaByObjMetaDefinicio(Obj_Id, ObjMetaDefinicio_Id, TableName, ColumnName, ColumnValues, bColumnValuesExclude, DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus, localUser);

            if (result.IsError)
                return false;

            if (result.Ds.Tables[0].Rows.Count < 1)
                return false;

            return true;
        }

        public static Result GetAllMetaByObjMetaDefinicio(string Obj_Id, string ObjMetaDefinicio_Id, string TableName, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude, string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, string localUser)
        {
            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            ExecParam execParam = GetExecParam(localUser);

            EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

            Result result = service_ot.GetAllMetaByObjMetaDefinicio(execParam, search, Obj_Id, ObjMetaDefinicio_Id
                , TableName, ColumnName, ColumnValues, bColumnValuesExclude
                , DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus);
            return result;
        }

        public static DataSet GetObjektumTargyszavai(EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch
           , String Obj_Id, String ObjMetaDefinicio_Id
           , String TableName, String ColumnName, String[] ColumnValues, bool bColumnValuesExclude
           , String DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, string localUser)
        {
            Result result = GetAllMetaByObjMetaDefinicio(Obj_Id, ObjMetaDefinicio_Id, TableName, ColumnName, ColumnValues, bColumnValuesExclude, DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus, localUser);

            if (result.IsError)
            {
                return null;
            }

            return result.Ds;
        }

        #endregion

        #region Helpers

        public static Stream GetHttpRequestStream()
        {
            return HttpContext.Current.Request.InputStream;
        }

        public static Stream GetHttpOutputStream()
        {
            return HttpContext.Current.Response.OutputStream;

        }

        public static FelhasznaloProfil GetUserProfil(String felhasznaloId)
        {
            if (String.IsNullOrEmpty(felhasznaloId)) return null;
            FelhasznaloProfil _FelhasznaloProfil = new FelhasznaloProfil();
            _FelhasznaloProfil.Felhasznalo.Id = felhasznaloId;
            #region Felhasznalo GET

            Contentum.eAdmin.Service.KRT_FelhasznalokService _KRT_FelhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = new ExecParam();
            ExecParam.Felhasznalo_Id = Constants.Parameters.AdminUser;
            ExecParam.Record_Id = felhasznaloId;

            Contentum.eBusinessDocuments.Result _ret = _KRT_FelhasznalokService.Get(ExecParam);
            if (_ret.IsError || _ret.Record == null)
            {
                return _FelhasznaloProfil;
            }
            Contentum.eBusinessDocuments.KRT_Felhasznalok _KRT_Felhasznalok = (Contentum.eBusinessDocuments.KRT_Felhasznalok)_ret.Record;

            #endregion

            _FelhasznaloProfil.Felhasznalo.Id = _KRT_Felhasznalok.Id;
            _FelhasznaloProfil.Felhasznalo.Nev = _KRT_Felhasznalok.Nev;

            _FelhasznaloProfil.Felhasznalo.Org.Id = _KRT_Felhasznalok.Org;
            if (!String.IsNullOrEmpty(_FelhasznaloProfil.Felhasznalo.Org.Id))
            {
                #region Org GET (Kell?)

                Contentum.eAdmin.Service.KRT_OrgokService _KRT_OrgokService = eAdminService.ServiceFactory.GetKRT_OrgokService();
                //ExecParam.Felhasznalo_Id = LoginId;

                ExecParam.Record_Id = _FelhasznaloProfil.Felhasznalo.Org.Id;
                _ret = _KRT_OrgokService.Get(ExecParam);
                Contentum.eBusinessDocuments.KRT_Orgok _KRT_Orgok = (Contentum.eBusinessDocuments.KRT_Orgok)_ret.Record;
                _FelhasznaloProfil.Felhasznalo.Org.Nev = _KRT_Orgok.Nev;

                // CR#2013 (EB 2009.10.12): a felhasználói felület eltérõ elemeinek kezeléséhez
                _FelhasznaloProfil.Felhasznalo.Org.Kod = _KRT_Orgok.Kod;
                #endregion
            }

            return _FelhasznaloProfil;
        }
        public static bool IsDBNull(object obj)
        {
            return obj == System.DBNull.Value;
        }

        public static string GetRowValueAsString(this DataRow row, string columnName)
        {
            return !IsDBNull(row[columnName]) ? row[columnName].ToString() : null;
        }
        public static bool TryParseEnum<T>(string enumValue, out T parsedEnumValue)
        {
            parsedEnumValue = default(T);
            try
            {
                parsedEnumValue = (T)Enum.Parse(typeof(T), enumValue, true);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static U MapEnum<T, U>(this T type)
        {
            U newType = default(U);
            try
            {
                newType = (U)Enum.Parse(typeof(T), type.ToString(), true);
            }
            catch
            { }

            return newType;
        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static ASP_ADO_DOCUMENT.AddressDto BuildAddressDTOFromCim(KRT_Cimek cim, ASP_ADO_DOCUMENT.DocumentDto document, string localUser)
        {
            var address = new ASP_ADO_DOCUMENT.AddressDto();

            if (!string.IsNullOrEmpty(cim.Tipus) && cim.Tipus == KodTarak.Cim_Tipus.Email)
            {
                if (string.IsNullOrEmpty(document.Partner.Email))
                {
                    document.Partner.Email = cim.CimTobbi;
                }
            }
            else
            {
                address.Id = long.Parse(GetASPADOID(cim.Id, localUser));
                address.IdSpecified = true;
                address.City = cim.TelepulesNev;
                //c.CountryCode = string.IsNullOrEmpty(cim.orsz ? null : cim["OrszagKod"].ToString();
                address.Number = cim.Hazszam;
                address.Staircase = cim.Lepcsohaz;
                address.Door = cim.Ajto;
                address.Floor = cim.Szint;
                address.ParcelNumber = cim.HRSZ;
                address.Building = cim.HazszamBetujel;
                address.StreetName = cim.KozteruletNev;
                address.Zip = cim.IRSZ;

                if (!String.IsNullOrEmpty(cim.KozteruletTipusNev))
                {
                    address.StreetType = cim.KozteruletTipusNev;
                }
            }
            return address;
        }

        public static string GetCsoportNev(string id, string localUser)
        {
            string nev = null;

            if (!String.IsNullOrEmpty(id))
            {
                KRT_CsoportokService csoportService = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                ExecParam eParam = GetExecParam(localUser);
                eParam.Record_Id = id;

                Result csoportResult = csoportService.Get(eParam);
                if (!csoportResult.IsError)
                {
                    KRT_Csoportok csoport = (KRT_Csoportok)csoportResult.Record;
                    nev = csoport.Nev;
                }
            }
            return nev;
        }

        public static EREC_KuldKuldemenyek GetKuldemeny(string id, string localUser)
        {
            EREC_KuldKuldemenyek kuldemeny = null;

            EREC_KuldKuldemenyekService kuldemenyService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            execParam.Record_Id = id;

            Result result = kuldemenyService.Get(execParam);

            if (string.IsNullOrEmpty(result.ErrorCode))
            {
                kuldemeny = (EREC_KuldKuldemenyek)result.Record;
            }
            return kuldemeny;
        }

        public static ASP_ADO_DOCUMENT.PartnerDto GetPartner(string guid, string localUser)
        {
            string partnerIdLong = GetASPADOID(guid, localUser);
            if (string.IsNullOrEmpty(partnerIdLong))
            {
                return null;
            }
            ASP_ADO_PARTNER.PartnerIdRequest request = new ASP_ADO_PARTNER.PartnerIdRequest();
            request.PartnerId = partnerIdLong;
            ASP_ADO_PARTNER.PartnerResponse partnerResponse = new ASP_ADO_PARTNER.PartnerResponse();
            partnerResponse = ASPADOHelper.GetPartner(request, partnerResponse, localUser);

            if (string.IsNullOrEmpty(partnerResponse.ErrorMessage))
            {
                return partnerResponse.Partner.MapPartner();
            }

            return null;
        }

        public static ASP_ADO_DOCUMENT.GetDocumentsPartnerDto GetDocumentsPartner(string guid, string localUser)
        {
            string partnerIdLong = GetASPADOID(guid, localUser);
            if (string.IsNullOrEmpty(partnerIdLong))
            {
                return null;
            }
            ASP_ADO_PARTNER.PartnerIdRequest request = new ASP_ADO_PARTNER.PartnerIdRequest();
            request.PartnerId = partnerIdLong;
            var partnerResponse = new ASP_ADO_PARTNER.PartnerResponse();
            partnerResponse = ASPADOHelper.GetPartner(request, partnerResponse, localUser);

            if (string.IsNullOrEmpty(partnerResponse.ErrorMessage))
            {
                return partnerResponse.Partner.MapGetDocumentsPartner();
            }

            return null;
        }

        public static ASP_ADO_DOCUMENT.GetDocumentPartnerDto GetDocumentPartner(string guid, string localUser)
        {
            string partnerIdLong = GetASPADOID(guid, localUser);
            if (string.IsNullOrEmpty(partnerIdLong))
            {
                return null;
            }
            ASP_ADO_PARTNER.PartnerIdRequest request = new ASP_ADO_PARTNER.PartnerIdRequest();
            request.PartnerId = partnerIdLong;
            var partnerResponse = new ASP_ADO_PARTNER.PartnerResponse();
            partnerResponse = ASPADOHelper.GetPartner(request, partnerResponse, localUser);

            if (string.IsNullOrEmpty(partnerResponse.ErrorMessage))
            {
                return partnerResponse.Partner.MapGetDocumentPartner();
            }

            return null;
        }

        public static ASP_ADO_DOCUMENT.PartnerDto GetPartnerDTO(string guid, string localUser)
        {
            string partnerIdLong = GetASPADOID(guid, localUser);
            if (string.IsNullOrEmpty(partnerIdLong))
            {
                return null;
            }
            var request = new ASP_ADO_PARTNER.PartnerIdRequest();
            request.PartnerId = partnerIdLong;
            var partnerResponse = new ASP_ADO_PARTNER.PartnerResponse();
            partnerResponse = ASPADOHelper.GetPartner(request, partnerResponse, localUser);

            if (string.IsNullOrEmpty(partnerResponse.ErrorMessage))
            {
                return partnerResponse.Partner.MapPartner();
            }

            return null;
        }


        public static ASP_ADO_DOCUMENT.GetDocumentsInvolvedPartnerDto GetDocumentsInvolvedPartner(string guid, string localUser)
        {
            string partnerIdLong = GetASPADOID(guid, localUser);
            if (string.IsNullOrEmpty(partnerIdLong))
            {
                return null;
            }
            var request = new ASP_ADO_PARTNER.PartnerIdRequest();
            request.PartnerId = partnerIdLong;
            var partnerResponse = new ASP_ADO_PARTNER.PartnerResponse();
            partnerResponse = ASPADOHelper.GetPartner(request, partnerResponse, localUser);

            if (string.IsNullOrEmpty(partnerResponse.ErrorMessage))
            {
                return partnerResponse.Partner.MapGetDocumentsInvolvedPartner();
            }

            return null;
        }

        static Dictionary<string, string> guidCache = new Dictionary<string, string>();

        public static string GetASPADOID(string guidId, string localUser)
        {
            if (GuidCache.Any(x => x.Key == guidId))
            {
                return GuidCache[guidId];
            }

            KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            Result partnerIdResult = partnerService.GetASPADOId(execParam, guidId);

            if (partnerIdResult.IsError)
            {
                return null;
            }

            string partnerIdLong = partnerIdResult.Record.ToString();

            GuidCache.Add(guidId, partnerIdLong);

            return partnerIdLong;

        }

        public static string ValidateUnknownSoapHeaders(this SoapUnknownHeader[] headers)
        {
            string userID = string.Empty;
            if (headers != null)
                foreach (SoapUnknownHeader header in headers)
                {
                    if (header.Element.Name == "Action")
                    {
                        string mustUnderStand = header.Element.GetAttribute("mustUnderstand", "http://www.w3.org/2003/05/soap-envelope");
                        if (string.IsNullOrEmpty(mustUnderStand))
                        {
                            mustUnderStand = header.Element.GetAttribute("mustUnderstand", "http://schemas.xmlsoap.org/soap/envelope/");
                        }

                        if (!string.IsNullOrEmpty(mustUnderStand) && mustUnderStand == "1")
                        {
                            header.DidUnderstand = true;
                            //header.Element.SetAttribute("didUnderstand", "http://www.w3.org/2003/05/soap-envelope", "1");
                        }
                    }

                    if (header.Element.Name == "Tenant" && header.Element.InnerText != Constants.Parameters.ASPADOTenant)
                    {
                        throw new SoapException("Invalid Tenant", SoapException.ClientFaultCode);
                    }

                    if (header.Element.Name == "User")
                    {
                        userID = header.Element.InnerText;
                    }
                }

            return userID;
        }

        public static string GetMappedUserId(this string userId)
        {
            if (mappedUserIds.Keys.Contains(userId))
            {
                return mappedUserIds[userId].Felhasznalo.Id;
            }

            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = Constants.Parameters.AdminUser;
            execParam.LoginUser_Id = Constants.Parameters.AdminUser;

            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.OrderBy = null;

            KRT_CimekSearch cimekSearch = new KRT_CimekSearch();
            cimekSearch.CimTobbi.Value = userId;
            cimekSearch.CimTobbi.Operator = Contentum.eQuery.Query.Operators.equals;
            cimekSearch.Tipus.Value = "08";
            cimekSearch.Tipus.Operator = Contentum.eQuery.Query.Operators.equals;

            Result partnerekResult = service.GetAllWithCim(execParam, partnerekSearch, cimekSearch);

            if (partnerekResult.IsError)
            {
                return null;
            }
            try
            {
                string partnerId = partnerekResult.Ds.Tables[0].Rows[0][Constants.TableColumns.Id].ToString();

                Contentum.eAdmin.Service.KRT_FelhasznalokService felhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

                KRT_FelhasznalokSearch felhasznalokSearch = new KRT_FelhasznalokSearch();
                felhasznalokSearch.Partner_id.Value = partnerId;
                felhasznalokSearch.Partner_id.Operator = Contentum.eQuery.Query.Operators.equals;

                Result felhasznalokResult = felhasznalokService.GetAll(execParam, felhasznalokSearch);

                if (felhasznalokResult.IsError)
                {
                    return null;
                }

                mappedUserIds[userId] = GetUserProfil(felhasznalokResult.Ds.Tables[0].Rows[0][Constants.TableColumns.Id].ToString());

                return felhasznalokResult.Ds.Tables[0].Rows[0][Constants.TableColumns.Id].ToString();
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region ExecParam
        public static void Clean(this ExecParam execParam)
        {
            string executorId = execParam.Felhasznalo_Id;
            execParam = new ExecParam();
            execParam.Felhasznalo_Id = executorId;
            execParam.LoginUser_Id = executorId;
        }

        public static ExecParam GetExecParam(string localUser)
        {
            return GetExecParam(localUser, false);
        }

        public static ExecParam GetExecParam(string localUser, bool withoutSzervezet)
        {
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = localUser;
            execParam.LoginUser_Id = localUser;

            if (!withoutSzervezet)
            {
                if (!localUserCsoportok.Keys.Contains(localUser))
                {
                    var data = GetOrganizationUnitsData(localUser);
                    if (data != null && data.OrganizationUnits != null)
                    {
                        localUserCsoportok[localUser] = data.OrganizationUnits[0];
                    }
                }
                if (localUserCsoportok.ContainsKey(localUser) && localUserCsoportok[localUser] != null)
                {
                    execParam.FelhasznaloSzervezet_Id = localUserCsoportok[localUser].OrganizationUnitId;
                }
                // execParam.CsoportTag_Id = localUserCsoportok[localUser].CsoportTagId; DTAP=>NEW WSDL
            }

            return execParam;
        }

        private static Dictionary<string, ASP_ADO_DOCUMENT.GetOrganizationUnitsDto> localUserCsoportok = new Dictionary<string, ASP_ADO_DOCUMENT.GetOrganizationUnitsDto>();
        private static Dictionary<string, FelhasznaloProfil> mappedUserIds = new Dictionary<string, FelhasznaloProfil>();

        public static Dictionary<string, string> GuidCache
        {
            get
            {
                return guidCache;
            }
        }

        public static void AddPaging(this ExecParam execParam, int? pageNumber, bool pageNumberSpecified, int? pageSize, bool pageSizeSpecified)
        {
            if (pageNumberSpecified && pageNumber != null)
            {
                execParam.Paging.PageNumber = ((int)pageNumber) - 1;
            }
            if (pageSizeSpecified && pageSize != null)
            {
                execParam.Paging.PageSize = ((int)pageSize);
            }
        }

        static string GetTempPath()
        {
            return Path.GetTempPath();
        }

        static string GetTempDir(string mainForrasAzonosito)
        {
            return Path.Combine(GetTempPath(), "ASPADO_" + mainForrasAzonosito);
        }

        public static string GetInputDir(string mainForrasAzonosito, string subForrasAzonosito)
        {
            return Path.Combine(GetTempDir(mainForrasAzonosito), subForrasAzonosito);
        }

        public static string PadToIktatokonyvAzonosito(string id)
        {
            if (id.Length < ASPADOHelper.iktatoKonyvAzonositoHossz)
            {
                id = id.PadLeft(ASPADOHelper.iktatoKonyvAzonositoHossz, '0');
            }
            return id;
        }

        #endregion



        #region PartnerService Response Helpers
        public static T SetError<T>(this T response, string errorMsg) where T : IBaseResponse
        {
            response.IsSucceeded = false;
            response.ErrorMessage = errorMsg;
            Logger.Error("ASPADO ErrorResponse: " + response.GetType() + "Message:" + errorMsg + Environment.NewLine + "Stacktrace: " + Environment.StackTrace);
            return response;
        }


        public static T SetSuccess<T>(this T response) where T : IBaseResponse
        {
            response.IsSucceeded = true;
            response.IsSucceededSpecified = true;
            return response;
        }
        #endregion

        #region DocumentService Response Helpers
        public static T SetErrorResponse<T>(this T response, string errorMsg)
            where T : IBaseResponse
        {
            response.IsSucceeded = false;
            response.IsSucceededSpecified = true;
            response.ErrorMessage = errorMsg;
            Logger.Error("ASPADO ErrorResponse: " + response.GetType() + "Message:" + errorMsg + Environment.NewLine + "Stacktrace: " + Environment.StackTrace);
            return response;
        }

        public static T SetSuccessResponse<T>(this T response) where T : IBaseResponse
        {
            response.IsSucceeded = true;
            response.IsSucceededSpecified = true;
            return response;
        }
        #endregion

        public static string GetHivataliKapuFiok()
        {
            Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
            string[] fiokok = svc.GetHivataliKapuFiokok();

            if (fiokok != null && fiokok.Length > 0)
            {
                return fiokok[0];
            }
            return null;
        }

        public static string GetSzemelyKapcsolatiKodHivataliKapu(KRT_Szemelyek szemely)
        {
            try
            {

                Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
                AzonositasKerdes kerdes = new AzonositasKerdes();
                kerdes.TermeszetesSzemelyAzonosito = new hkpTermeszetesSzemelyAzonosito();

                #region viselt név
                kerdes.TermeszetesSzemelyAzonosito.ViseltNeve = new NevAdat();
                kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.CsaladiNev = szemely.UjCsaladiNev;
                kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.UtoNev1 = szemely.UjUtonev;
                kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.UtoNev2 = szemely.UjTovabbiUtonev;
                #endregion

                #region anyja neve
                kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve = new NevAdat();
                kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.CsaladiNev = szemely.AnyjaNeveCsaladiNev;
                kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.UtoNev1 = szemely.AnyjaNeveElsoUtonev;
                kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.UtoNev2 = szemely.AnyjaNeveTovabbiUtonev;
                #endregion

                kerdes.TermeszetesSzemelyAzonosito.SzuletesiHely = new hkpSzuletesiHely();
                kerdes.TermeszetesSzemelyAzonosito.SzuletesiHely.Telepules = szemely.SzuletesiHely;

                DateTime ido;
                if (DateTime.TryParse(szemely.SzuletesiIdo, out ido))
                {
                    kerdes.TermeszetesSzemelyAzonosito.SzuletesiIdo = ido;
                }

                Result result = svc.Azonositas(GetHivataliKapuFiok(), kerdes);

                if (result.IsError)
                {
                    return null;
                }

                AzonositasValasz valasz = result.Record as AzonositasValasz;

                if (valasz != null)
                {
                    if (valasz.HibaUzenet != null && !String.IsNullOrEmpty(valasz.HibaUzenet.Tartalom))
                    {
                        return null;
                    }

                    if (valasz.AzonositottList.Length == 1)
                    {
                        return valasz.AzonositottList[0].KapcsolatiKod;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetSzemelyKapcsolatiKodHivataliKapu error: " + ex.Message);
            }
            return null;
        }

        public static string GetPartnerKRIDByNevHivataliKapu(string nev, Contentum.eIntegrator.Service.HivatalTipus hivatalTipus)
        {
            try
            {
                Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
                HivatalSzuroParameterek szuroParameterk = new HivatalSzuroParameterek();
                szuroParameterk.Nev = nev;
                //szuroParameterk.RovidNev = tbRovidNev.Text;
                //szuroParameterk.NevOperator = OperatorTipus.And;
                szuroParameterk.Tipus = hivatalTipus;
                //szuroParameterk.TamogatottSzolgaltatasok = cbTamogatottSzolgaltatasok.Checked;
                szuroParameterk.TamogatottSzolgaltatasok = false;
                Result result = svc.HivatalokListajaFeldolgozas_Szurt(GetHivataliKapuFiok(), szuroParameterk);

                Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

                if (resultError.IsError)
                {
                    return null;
                }


                HivatalokListajaValasz valasz = result.Record as HivatalokListajaValasz;

                if (valasz != null)
                {
                    if (valasz.HivatalokLista.Length == 1)
                    {
                        return valasz.HivatalokLista[0].KRID;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("GetPartnerKRIDByNevHivataliKapu error: " + e.Message);
            }
            return null;
        }

        public static void LoadPartnerKRID(string felhasznaloId, EREC_PldIratPeldanyok peldany, string ugyiratId)
        {
            Logger.Debug(String.Format("LoadPartnerKRID: KuldesMod={0}, Partner_Id_Cimzett={1}", peldany.KuldesMod, peldany.Partner_Id_Cimzett));

            try
            {
                if (peldany.KuldesMod == Contentum.eUtility.KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu)
                {
                    #region Korábbi

                    if (!string.IsNullOrEmpty(ugyiratId) && !string.IsNullOrEmpty(peldany.Partner_Id_Cimzett))
                    {
                        Contentum.eRecord.Service.EREC_PldIratPeldanyokService pldService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

                        Contentum.eRecord.Service.EREC_IraIratokService iratService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        ExecParam execParamIrat = GetExecParam(felhasznaloId);
                        EREC_IraIratokSearch iratSearch = new EREC_IraIratokSearch();
                        iratSearch.AdathordozoTipusa.Value = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                        iratSearch.AdathordozoTipusa.Operator = Contentum.eQuery.Query.Operators.equals;
                        iratSearch.ExpedialasModja.Value = KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
                        iratSearch.ExpedialasModja.Operator = Contentum.eQuery.Query.Operators.equals;
                        iratSearch.Ugyirat_Id.Value = ugyiratId;
                        iratSearch.Ugyirat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                        iratSearch.WhereByManual = " EREC_KuldKuldemenyek.Partner_Id_Bekuldo='" + peldany.Partner_Id_Cimzett + "' ";
                        Result iratResult = iratService.GetAllWithExtension(execParamIrat, iratSearch);
                        if (!iratResult.IsError && iratResult.Ds.Tables[0].Rows.Count > 0)
                        {
                            string cimSTR_Bekuldo = iratResult.Ds.Tables[0].Rows[0]["CimSTR_Bekuldo"] == DBNull.Value ? null : iratResult.Ds.Tables[0].Rows[0]["CimSTR_Bekuldo"].ToString();
                            if (!string.IsNullOrEmpty(cimSTR_Bekuldo))
                            {
                                peldany.CimSTR_Cimzett = cimSTR_Bekuldo;
                                peldany.Updated.CimSTR_Cimzett = true;
                                peldany.Cim_id_Cimzett = String.Empty;
                                peldany.Updated.Cim_id_Cimzett = true;
                                return;
                            }
                        }

                    }
                    #endregion

                    string partnerId = peldany.Partner_Id_Cimzett;

                    if (!String.IsNullOrEmpty(partnerId))
                    {
                        KRT_PartnerekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

                        Result result = service.GetKRID(GetExecParam(felhasznaloId), partnerId);

                        if (result.IsError)
                        {
                            string errorMessage = ResultError.GetErrorMessageFromResultObject(result);
                            Logger.Error("LoadPartnerKRID hiba: " + errorMessage);
                        }
                        else
                        {
                            peldany.CimSTR_Cimzett = result.Uid;
                            peldany.Updated.CimSTR_Cimzett = true;
                            peldany.Cim_id_Cimzett = String.Empty;
                            peldany.Updated.Cim_id_Cimzett = true;
                        }

                        ExecParam execParamPartner = GetExecParam(felhasznaloId);
                        execParamPartner.Record_Id = partnerId;
                        Result partnerResult = service.Get(execParamPartner);

                        if (!partnerResult.IsError)
                        {
                            KRT_Partnerek partner = (KRT_Partnerek)partnerResult.Record;

                            if (partner.Tipus == KodTarak.Partner_Tipus.Szervezet)
                            {
                                string krid = GetPartnerKRIDByNevHivataliKapu(partner.Nev, Contentum.eIntegrator.Service.HivatalTipus.HivataliKapu);

                                if (!string.IsNullOrEmpty(krid))
                                {
                                    peldany.CimSTR_Cimzett = krid;
                                    peldany.Updated.CimSTR_Cimzett = true;
                                    peldany.Cim_id_Cimzett = String.Empty;
                                    peldany.Updated.Cim_id_Cimzett = true;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = ResultError.GetErrorMessageFromResultObject(partnerResult);
                            Logger.Error("LoadPartnerKRID hiba: " + errorMessage);
                        }

                        if (!partnerResult.IsError && partnerResult.Record != null)
                        {
                            KRT_Partnerek partner = (KRT_Partnerek)partnerResult.Record;

                            if (partner.Tipus == KodTarak.Partner_Tipus.Szemely)
                            {
                                KRT_Szemelyek psz = ((KRT_PartnerSzemelyek)partner).Szemelyek;

                                if (psz != null)
                                {
                                    string krid = GetSzemelyKapcsolatiKodHivataliKapu(psz);

                                    if (!string.IsNullOrEmpty(krid))
                                    {
                                        peldany.CimSTR_Cimzett = krid;
                                        peldany.Updated.CimSTR_Cimzett = true;
                                        peldany.Cim_id_Cimzett = String.Empty;
                                        peldany.Updated.Cim_id_Cimzett = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error in LoadPartnerKRID: " + ex.Message);
            }
        }

        public interface IBaseResponse
        {
            string ErrorMessage { get; set; }
            bool IsSucceeded { get; set; }
            bool IsSucceededSpecified { get; set; }
        }
    }
}
