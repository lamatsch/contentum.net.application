﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        /// <summary>
        /// A funkció lehetővé teszi az ADÓ rendszerből egy alszámhoz tartozó  csatolmányok lekérdezését
        /// az iratkezelő rendszerből. (Jelenleg nem implementált funkció)
        /// Jövőbeni fejlesztés, ahol egy iktatószámhoz(alszámhoz) tartozó,
        /// hitelesített dokumentumok és annak egyéb hitelesítési információi juttathatók az Irat szakrendszerből
        /// az Adó szakrendszerbe, pl. egy külső hitelesítést követően, további feldolgozás céljából.
        /// Elsősorban a külső PostaHibrid kapcsolathoz került kialakításra.
        /// </summary>
        /// <param name="registrationNumber"></param>
        /// <param name="response"></param>
        /// <param name="localUser"></param>
        /// <returns></returns>
        public static ASP_ADO_DOCUMENT.FileDto[] GetAttachmentsInternal(ASP_ADO_DOCUMENT.RegistrationNumberDto registrationNumber, ASP_ADO_DOCUMENT.GetAttachmentsResponse response, ExecParam execParam)
        {
            #region IRAT

            #region LOAD DATA

            ASP_ADO_DOCUMENT.DocumentListByRegistrationNumberRequest request = new ASP_ADO_DOCUMENT.DocumentListByRegistrationNumberRequest();
            request.RegistrationNumber = registrationNumber;

            Result erec_IraIratokResult = GetDocumentsData(request, 0, false, 0, false, execParam);
            if (erec_IraIratokResult.IsError)
            {
                response.IsSucceeded = false;
                response.IsSucceededSpecified = true;
                response.ErrorMessage = erec_IraIratokResult.ErrorMessage;
                Logger.Error("ASPADO ErrorResponse: " + response.GetType() + "Message:" + erec_IraIratokResult.ErrorMessage + Environment.NewLine + "Stacktrace: " + Environment.StackTrace);
                return null;
            }
            else if (erec_IraIratokResult.Ds.Tables.Count == 0 || erec_IraIratokResult.Ds.Tables[0].Rows.Count == 0)
            {
                response.IsSucceeded = true;
                response.IsSucceededSpecified = true;
            }

            #endregion LOAD DATA

            #region EXTRACT IRAT IDS

            List<string> IratIds = new List<string>();
            for (int i = 0; i < erec_IraIratokResult.Ds.Tables[0].Rows.Count; i++)
            {
                IratIds.Add(erec_IraIratokResult.Ds.Tables[0].Rows[i]["Id"].ToString());
            }

            #endregion EXTRACT IRAT IDS

            #endregion IRAT

            #region CSATOLMANYOK

            List<EREC_Csatolmanyok> listCsat = LoadCsatolmanyok(IratIds, response, execParam);
            if (!response.IsSucceeded && response.IsSucceededSpecified)
            {
                return null;
            }
            List<string> DokIds = listCsat.Select(s => s.Dokumentum_Id).ToList();

            #endregion CSATOLMANYOK

            #region DOKUMENTUMOK

            List<KRT_Dokumentumok> listDok = LoadDokuemtumok(DokIds, response, execParam);
            if (!response.IsSucceeded && response.IsSucceededSpecified)
            {
                return null;
            }

            #endregion DOKUMENTUMOK

            #region DOK ALAIRAS

            List<KRT_DokumentumAlairasok> listDokAlairas = LoadDokAlairas(DokIds, execParam);

            #endregion DOK ALAIRAS

            #region GENERATE RESULT

            List<ASP_ADO_DOCUMENT.FileDto> result = new List<ASP_ADO_DOCUMENT.FileDto>();
            ASP_ADO_DOCUMENT.FileDto tmpResult = new ASP_ADO_DOCUMENT.FileDto();
            KRT_DokumentumAlairasok alairt;
            foreach (KRT_Dokumentumok item in listDok)
            {
                if (listCsat != null)
                {
                    EREC_Csatolmanyok csat = listCsat.FirstOrDefault(f => f.Dokumentum_Id == item.Id);
                    if (csat != null)
                    {
                        tmpResult.CustomId = csat.CustomId;
                    }
                }

                #region DOWNLOAD FILE CONTENT

                if (!string.IsNullOrEmpty(item.External_Link))
                {
                    try
                    {
                        tmpResult.Data = CommonFunctions4Web.GetDocumentContent(item.External_Link);
                    }
                    catch (Exception exc)
                    {
                        response.IsSucceeded = false;
                        response.IsSucceededSpecified = true;
                        response.ErrorMessage = exc.Message;
                        Logger.Error("ASPADO ErrorResponse: " + response.GetType() + "Message:" + exc.Message + Environment.NewLine + "Stacktrace: " + Environment.StackTrace);
                        return null;
                    }
                }

                #endregion DOWNLOAD FILE CONTENT

                tmpResult.FileDocStoreID = null;
                tmpResult.Guid = item.Dokumentum_Id;
                tmpResult.Name = item.FajlNev;
                if (listDokAlairas.Any(a => a.Dokumentum_Id_Alairt == item.Dokumentum_Id))
                {
                    alairt = listDokAlairas.Where(g => g.Dokumentum_Id_Alairt == item.Dokumentum_Id).OrderByDescending(o => o.Base.LetrehozasIdo).FirstOrDefault();
                    if (alairt != null)
                    {
                        tmpResult.Signatory = alairt.AlairasTulajdonos;
                        tmpResult.SignDate = DateTime.Parse(alairt.Base.ModositasIdo);
                        tmpResult.SignDateSpecified = true;
                    }
                }
                tmpResult.Url = item.External_Link;
                result.Add(tmpResult);
            }

            #endregion GENERATE RESULT

            return result.ToArray();
        }

        /// <summary>
        /// LoadDokuemtumok
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="response"></param>
        /// <param name="execParam"></param>
        /// <returns></returns>
        private static List<KRT_Dokumentumok> LoadDokuemtumok(List<string> ids, ASP_ADO_DOCUMENT.GetAttachmentsResponse response, ExecParam execParam)
        {
            #region DOKUMENTUMOK

            #region LOAD DATA

            KRT_DokumentumokService serviceDok = eAdminService.ServiceFactory.GetKRT_DokumentumokService();
            KRT_DokumentumokSearch searchDok = new KRT_DokumentumokSearch();

            searchDok.Id.Value = Search.GetSqlInnerString(ids.ToArray());
            searchDok.Id.Operator = eQuery.Query.Operators.inner;

            Result resultDok = serviceDok.GetAll(execParam, searchDok);

            if (resultDok.IsError)
            {
                response.IsSucceeded = false;
                response.IsSucceededSpecified = true;
                response.ErrorMessage = resultDok.ErrorMessage;
                Logger.Error("ASPADO ErrorResponse: " + response.GetType() + "Message:" + resultDok.ErrorMessage + Environment.NewLine + "Stacktrace: " + Environment.StackTrace);
                return null;
            }
            else if (resultDok.Ds.Tables.Count == 0 || resultDok.Ds.Tables[0].Rows.Count == 0)
            {
                response.IsSucceeded = true;
                response.IsSucceededSpecified = true;
            }

            #endregion LOAD DATA

            #region CREATE BO

            List<KRT_Dokumentumok> listDok = new List<KRT_Dokumentumok>();
            if (!resultDok.IsError)
            {
                KRT_Dokumentumok tmpDok;
                for (int i = 0; i < resultDok.Ds.Tables[0].Rows.Count; i++)
                {
                    tmpDok = new KRT_Dokumentumok();
                    Utility.LoadBusinessDocumentFromDataRow(tmpDok, resultDok.Ds.Tables[0].Rows[i]);
                    listDok.Add(tmpDok);
                }
            }

            #endregion CREATE BO

            #endregion DOKUMENTUMOK

            return listDok;
        }

        /// <summary>
        /// LoadDokAlairas
        /// </summary>
        /// <param name="dokids"></param>
        /// <param name="execParam"></param>
        /// <returns></returns>
        private static List<KRT_DokumentumAlairasok> LoadDokAlairas(List<string> dokids, ExecParam execParam)
        {
            #region DOK ALAIRAS

            #region LOAD DATA

            eDocument.Service.KRT_DokumentumAlairasokService serviceDok = eDocumentService.ServiceFactory.GetKRT_DokumentumAlairasService();
            KRT_DokumentumAlairasokSearch searchDokA = new KRT_DokumentumAlairasokSearch();

            searchDokA.Dokumentum_Id_Alairt.Value = Search.GetSqlInnerString(dokids.ToArray());
            searchDokA.Dokumentum_Id_Alairt.Operator = eQuery.Query.Operators.inner;

            searchDokA.OrderBy = " Letrehozasido DESC";
            Result resultDokAlair = serviceDok.GetAll(execParam, searchDokA);

            #endregion LOAD DATA

            #region CREATE BO

            List<KRT_DokumentumAlairasok> listDokAlairas = new List<KRT_DokumentumAlairasok>();
            if (!resultDokAlair.IsError)
            {
                KRT_DokumentumAlairasok tmpDokAlair;
                for (int i = 0; i < resultDokAlair.Ds.Tables[0].Rows.Count; i++)
                {
                    tmpDokAlair = new KRT_DokumentumAlairasok();
                    Utility.LoadBusinessDocumentFromDataRow(tmpDokAlair, resultDokAlair.Ds.Tables[0].Rows[i]);
                    listDokAlairas.Add(tmpDokAlair);
                }
            }

            #endregion CREATE BO

            #endregion DOK ALAIRAS

            return listDokAlairas;
        }

        /// <summary>
        /// LoadCsatolmanyok
        /// </summary>
        /// <param name="iratIds"></param>
        /// <param name="response"></param>
        /// <param name="execParam"></param>
        /// <returns></returns>
        private static List<EREC_Csatolmanyok> LoadCsatolmanyok(List<string> iratIds, ASP_ADO_DOCUMENT.GetAttachmentsResponse response, ExecParam execParam)
        {
            #region CSATOLMANYOK

            #region LOAD DATA

            EREC_CsatolmanyokService serviceCsat = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            EREC_CsatolmanyokSearch searchCsat = new EREC_CsatolmanyokSearch();

            searchCsat.IraIrat_Id.Value = Search.GetSqlInnerString(iratIds.ToArray());
            searchCsat.IraIrat_Id.Operator = eQuery.Query.Operators.inner;

            Result resultCsat = serviceCsat.GetAllWithExtension(execParam, searchCsat);

            if (resultCsat.IsError)
            {
                response.IsSucceeded = false;
                response.IsSucceededSpecified = true;
                response.ErrorMessage = resultCsat.ErrorMessage;
                Logger.Error("ASPADO ErrorResponse: " + response.GetType() + "Message:" + resultCsat.ErrorMessage + Environment.NewLine + "Stacktrace: " + Environment.StackTrace);
            }
            else if (resultCsat.Ds.Tables.Count == 0 || resultCsat.Ds.Tables[0].Rows.Count == 0)
            {
                response.IsSucceeded = true;
                response.IsSucceededSpecified = true;
            }

            #endregion LOAD DATA

            #region EXTRACT IRAT IDS

            List<string> DokIds = new List<string>();
            for (int i = 0; i < resultCsat.Ds.Tables[0].Rows.Count; i++)
            {
                DokIds.Add(resultCsat.Ds.Tables[0].Rows[i]["Dokumentum_Id"].ToString());
            }

            #endregion EXTRACT IRAT IDS

            #region CREATE BO

            List<EREC_Csatolmanyok> listCsat = new List<EREC_Csatolmanyok>();
            if (!resultCsat.IsError)
            {
                EREC_Csatolmanyok tmpCsat;
                for (int i = 0; i < resultCsat.Ds.Tables[0].Rows.Count; i++)
                {
                    tmpCsat = new EREC_Csatolmanyok();
                    Utility.LoadBusinessDocumentFromDataRow(tmpCsat, resultCsat.Ds.Tables[0].Rows[i]);
                    listCsat.Add(tmpCsat);
                }
            }

            #endregion CREATE BO

            #endregion CSATOLMANYOK

            return listCsat;
        }
    }
}