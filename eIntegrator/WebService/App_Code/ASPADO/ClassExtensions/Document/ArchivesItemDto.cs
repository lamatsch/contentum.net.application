﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        private static void BuildArchiveResponse(this ASP_ADO_DOCUMENT.ArchivesItemDto arhivesItem, DataRow ugyiratRow)
        {
            arhivesItem.CaseGroup = !IsDBNull(ugyiratRow["IrattariTetelszam"]) ? ugyiratRow["IrattariTetelszam"].ToString() : null;
            arhivesItem.ScrappingCode = !IsDBNull(ugyiratRow["IrattariJel"]) ? ugyiratRow["IrattariJel"].ToString() : null;
            arhivesItem.Sector = !IsDBNull(ugyiratRow["AgazatiJel"]) ? ugyiratRow["AgazatiJel"].ToString() : null;

            if (!IsDBNull(ugyiratRow["Ev"]))
            {
                try
                {
                    arhivesItem.Year = int.Parse(ugyiratRow["Ev"].ToString());
                    arhivesItem.YearSpecified = true;
                }
                catch { }
            }
        }

    }
}