﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        public static EREC_KuldKuldemenyek GetKuldemenyForBatchFiling(this ASP_ADO_DOCUMENT.BatchFilingDocumentDto document, KRT_Partnerek partner, KRT_Cimek cim, string kuldId, ASP_ADO_DOCUMENT.BaseResponse response, string localUser)
        {
            EREC_KuldKuldemenyek kuld = new EREC_KuldKuldemenyek();
            string now = DateTime.Now.ToString();
            kuld.BeerkezesIdeje = now;
            kuld.FelbontasDatuma = now;
            kuld.KuldesMod = "02";
            kuld.Targy = "ASP.ADÓ küldemény";
            kuld.Surgosseg = KodTarak.SURGOSSEG.Normal;
            kuld.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            kuld.PeldanySzam = "1";
            kuld.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            kuld.Erkeztetes_Ev = DateTime.Now.Year.ToString();
            //38DE439E-9DB1-E611-92C1-0050569A6FBA = Adóigazgatási Osztály
            kuld.Csoport_Id_Cimzett = "38DE439E-9DB1-E611-92C1-0050569A6FBA";
            kuld.Csoport_Id_Felelos = localUser;

            //if (cim != null)
            //{
            //    string cimstr = "";
            //    cimstr = cimstr.AddStringWitComma(cim.IRSZ).
            //        AddStringWitComma(cim.TelepulesNev).AddStringWitComma(cim.KozteruletNev).AddStringWitComma(cim.Hazszam).
            //        AddStringWitComma(cim.HazszamBetujel).AddStringWitComma(cim.Lepcsohaz).AddStringWitComma(cim.Szint).AddStringWitComma(cim.Ajto);

            //    kuld.CimSTR_Bekuldo = cimstr;
            //}
            //else if (document.Partner != null && document.Partner.Addresses != null && document.Partner.Addresses.Length > 0)
            //{
            //    var address = document.Partner.Addresses[0];

            //    string cimstr = "";
            //    cimstr = cimstr.AddStringWitComma(address.Zip).AddStringWitComma(address.StreetName)
            //        .AddStringWitComma(address.Number).AddStringWitComma(address.Building).
            //        AddStringWitComma(address.Staircase).AddStringWitComma(address.Floor).AddStringWitComma(address.Door);

            //    kuld.CimSTR_Bekuldo = cimstr;
            //}
            //else if (document.InvolvedPartner != null && document.InvolvedPartner.Addresses != null && document.InvolvedPartner.Addresses.Length > 0)
            //{
            //    var address = document.InvolvedPartner.Addresses[0];

            //    string cimstr = "";
            //    cimstr = cimstr.AddStringWitComma(address.Zip).AddStringWitComma(address.StreetName)
            //        .AddStringWitComma(address.Number).AddStringWitComma(address.Building).
            //        AddStringWitComma(address.Staircase).AddStringWitComma(address.Floor).AddStringWitComma(address.Door);

            //    kuld.CimSTR_Bekuldo = cimstr;
            //}

            kuld.NevSTR_Bekuldo = "Belügyminisztérium";
            kuld.CimSTR_Bekuldo = "https://teljesitmenyado.kekkh.gov.hu/";

            //if (partner != null)
            //{
            //    kuld.NevSTR_Bekuldo = partner.Nev;
            //    kuld.Partner_Id_Bekuldo = partner.Id;
            //}
            //else if (document.Partner != null)
            //{
            //    kuld.NevSTR_Bekuldo = document.Partner.Name;
            //}
            //else if (document.InvolvedPartner != null)
            //{
            //    kuld.NevSTR_Bekuldo = document.InvolvedPartner.Name;
            //}

            kuld.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            kuld.FelhasznaloCsoport_Id_Bonto = localUser;
            kuld.BoritoTipus = KodTarak.BoritoTipus.Irat;
            kuld.MegorzesJelzo = "1";
            kuld.IktatastNemIgenyel = "1";
            kuld.KezbesitesModja = KodTarak.KULD_KEZB_MODJA.e_ugyintezes;
            kuld.Munkaallomas = "ASP.ADÓ";
            kuld.SerultKuldemeny = "0";
            kuld.TevesCimzes = "0";
            kuld.TevesErkeztetes = "0";
            kuld.CimzesTipusa = KodTarak.KULD_CIMZES_TIPUS.nevre_szolo;
            kuld.IraIktatokonyv_Id = kuldId;

            return kuld;
        }
        public static EREC_PldIratPeldanyok GetIratPeldanyFromRequestForFiling(this ASP_ADO_DOCUMENT.BatchFilingDocumentDto document, KRT_Partnerek partner, KRT_Cimek cim, string localUser)
        {
            EREC_PldIratPeldanyok iratPeldany = new EREC_PldIratPeldanyok();
            iratPeldany.Updated.SetValueAll(false);
            iratPeldany.Base.Updated.SetValueAll(false);

            iratPeldany.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Postai_sima;
            iratPeldany.Updated.KuldesMod = true;

            if (!string.IsNullOrEmpty(document.MediaCode))
            {
                if (document.MediaCode == "004" || document.MediaCode == "005")
                {
                    iratPeldany.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
                }
                else
                {
                    iratPeldany.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;

                }
                iratPeldany.Updated.UgyintezesModja = true;
            }

            iratPeldany.VisszaerkezesiHatarido = "";
            iratPeldany.Updated.VisszaerkezesiHatarido = true;

            iratPeldany.Visszavarolag = "";
            iratPeldany.Updated.Visszavarolag = true;

            if (document.InvolvedPartner != null && document.InvolvedPartner.Addresses != null && document.InvolvedPartner.Addresses.Length > 0)
            {
                var address = document.InvolvedPartner.Addresses[0];

                string cimstr = "";
                cimstr = cimstr.AddString(address.Zip).AddString(address.City).AddString(address.StreetName).AddString(address.StreetType.ToString()).AddString(address.Number).
                    AddString(address.Building).AddString(address.Staircase).AddString(address.Floor).AddString(address.Door).AddString(address.ParcelNumber);

                iratPeldany.CimSTR_Cimzett = cimstr;
                iratPeldany.Updated.CimSTR_Cimzett = true;
            }
            else if (cim != null)
            {
                iratPeldany.Cim_id_Cimzett = cim.Id;
                iratPeldany.Updated.Cim_id_Cimzett = true;

                string cimstr = "";
                cimstr = cimstr.AddString(cim.IRSZ).AddString(cim.TelepulesNev).AddString(cim.KozteruletNev).AddString(cim.KozteruletTipusNev).AddString(cim.Hazszam).
                    AddString(cim.HazszamBetujel).AddString(cim.Lepcsohaz).AddString(cim.Szint).AddString(cim.Ajto).AddString(cim.HRSZ);

                iratPeldany.CimSTR_Cimzett = cimstr;
                iratPeldany.Updated.CimSTR_Cimzett = true;

            }
            else if (document.Partner != null && document.Partner.Addresses != null && document.Partner.Addresses.Length > 0)
            {
                var address = document.Partner.Addresses[0];

                string cimstr = "";
                cimstr = cimstr.AddString(address.Zip).AddString(address.City).AddString(address.StreetName).AddString(address.StreetType).AddString(address.Number).
                    AddString(address.Building).AddString(address.Staircase).AddString(address.Floor).AddString(address.Door).AddString(address.ParcelNumber);

                iratPeldany.CimSTR_Cimzett = cimstr;
                iratPeldany.Updated.CimSTR_Cimzett = true;
            }

            if (document.InvolvedPartner != null)
            {
                if (!string.IsNullOrEmpty(document.InvolvedPartner.Name))
                {
                    iratPeldany.NevSTR_Cimzett = document.InvolvedPartner.Name;
                    iratPeldany.Updated.NevSTR_Cimzett = true;
                }
            }
            else if (partner != null)
            {
                iratPeldany.Partner_Id_Cimzett = partner.Id;
                iratPeldany.Updated.Partner_Id_Cimzett = true;

                if (!string.IsNullOrEmpty(partner.Nev))
                {
                    iratPeldany.NevSTR_Cimzett = partner.Nev;
                    iratPeldany.Updated.NevSTR_Cimzett = true;
                }

            }
            else if (document.Partner != null)
            {
                if (!string.IsNullOrEmpty(document.Partner.Name))
                {
                    iratPeldany.NevSTR_Cimzett = document.Partner.Name;
                    iratPeldany.Updated.NevSTR_Cimzett = true;
                }
            }

            iratPeldany.Csoport_Id_Felelos = localUser;
            iratPeldany.Updated.Csoport_Id_Felelos = true;

            return iratPeldany;
        }

        public static EREC_UgyUgyiratok GetUgyiratFromRequestForFiling(this ASP_ADO_DOCUMENT.BatchFilingDocumentDto document, KRT_Partnerek partner, KRT_Cimek cim, ASP_ADO_DOCUMENT.DocumentResponse response, string localUser)
        {
            EREC_UgyUgyiratok ugyirat = null;

            if (document != null)
            {
                ugyirat = new EREC_UgyUgyiratok();
                ugyirat.Updated.SetValueAll(false);
                ugyirat.Base.Updated.SetValueAll(false);

                if (!string.IsNullOrEmpty(document.Subject))
                {
                    ugyirat.Targy = document.Subject;
                    ugyirat.Updated.Targy = true;
                }

                ugyirat.Csoport_Id_Felelos = localUser;
                ugyirat.Updated.Csoport_Id_Felelos = true;

                ugyirat.FelhasznaloCsoport_Id_Ugyintez = localUser;
                ugyirat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

                if (document.InvolvedPartner != null && document.InvolvedPartner.Addresses != null && document.InvolvedPartner.Addresses.Length > 0)
                {
                    var address = document.InvolvedPartner.Addresses[0];

                    string cimstr = "";
                    cimstr = cimstr.AddString(address.Zip).AddString(address.City).AddString(address.StreetName).AddString(address.StreetType).AddString(address.Number).
                        AddString(address.Building).AddString(address.Staircase).AddString(address.Floor).AddString(address.Door).AddString(address.ParcelNumber);

                    ugyirat.CimSTR_Ugyindito = cimstr;
                    ugyirat.Updated.CimSTR_Ugyindito = true;
                }
                else if (cim != null)
                {
                    ugyirat.Cim_Id_Ugyindito = cim.Id;
                    ugyirat.Updated.Cim_Id_Ugyindito = true;

                    string cimstr = "";
                    cimstr = cimstr.AddString(cim.IRSZ).AddString(cim.TelepulesNev).AddString(cim.KozteruletNev).AddString(cim.KozteruletTipusNev).AddString(cim.Hazszam).
                        AddString(cim.HazszamBetujel).AddString(cim.Lepcsohaz).AddString(cim.Szint).AddString(cim.Ajto).AddString(cim.HRSZ);

                    ugyirat.CimSTR_Ugyindito = cimstr;
                    ugyirat.Updated.CimSTR_Ugyindito = true;

                }
                else if (document.Partner != null && document.Partner.Addresses != null && document.Partner.Addresses.Length > 0)
                {
                    var address = document.Partner.Addresses[0];

                    string cimstr = "";
                    cimstr = cimstr.AddString(address.Zip).AddString(address.City).AddString(address.StreetName).AddString(address.StreetType).AddString(address.Number).
                        AddString(address.Building).AddString(address.Staircase).AddString(address.Floor).AddString(address.Door).AddString(address.ParcelNumber);

                    ugyirat.CimSTR_Ugyindito = cimstr;
                    ugyirat.Updated.CimSTR_Ugyindito = true;
                }

                if (document.InvolvedPartner != null)
                {
                    if (!string.IsNullOrEmpty(document.InvolvedPartner.Name))
                    {
                        ugyirat.NevSTR_Ugyindito = document.InvolvedPartner.Name;
                        ugyirat.Updated.NevSTR_Ugyindito = true;
                    }
                }
                else if (partner != null)
                {
                    ugyirat.Partner_Id_Ugyindito = partner.Id;
                    ugyirat.Updated.Partner_Id_Ugyindito = true;

                    if (!string.IsNullOrEmpty(partner.Nev))
                    {
                        ugyirat.NevSTR_Ugyindito = partner.Nev;
                        ugyirat.Updated.NevSTR_Ugyindito = true;
                    }

                }
                else if (document.Partner != null)
                {
                    if (!string.IsNullOrEmpty(document.Partner.Name))
                    {
                        ugyirat.NevSTR_Ugyindito = document.Partner.Name;
                        ugyirat.Updated.NevSTR_Ugyindito = true;
                    }
                }

                if (document.ArchivesItem != null && !string.IsNullOrEmpty(document.ArchivesItem.CaseGroup))
                {
                    if (document.TaxTypeSpecified)
                    {
                        string kod = document.ArchivesItem.CaseGroup + "_" + document.TaxType.ToString();
                        if (TaxTypeKodTipusok.Keys.Any(x => x.Kod == kod))
                        {
                            string mappedkod = TaxTypeKodTipusok.First(x => x.Key.Kod == kod).Value.Kod;
                            mappedkod = mappedkod.Substring(mappedkod.IndexOf("_") + 1);
                            ugyirat.UgyTipus = mappedkod;
                            ugyirat.Updated.UgyTipus = true;
                        }
                    }


                    EREC_IratMetaDefinicioService metaDefinicioService = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                    ExecParam metaExecParam = GetExecParam(localUser);
                    EREC_IratMetaDefinicioSearch metaSearch = new EREC_IratMetaDefinicioSearch();

                    metaSearch.UgykorKod.Value = document.ArchivesItem.CaseGroup.ToString();
                    metaSearch.UgykorKod.Operator = Contentum.eQuery.Query.Operators.equals;

                    Result result = metaDefinicioService.GetAll(metaExecParam, metaSearch);
                    if (result.IsError)
                    {
                        response.SetErrorResponse(result.ErrorMessage);
                        return null;
                    }

                    if (result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
                    {
                        if (string.IsNullOrEmpty(ugyirat.UgyTipus))
                        {
                            ugyirat.UgyTipus = result.Ds.Tables[0].Rows[0]["Ugytipus"].ToString();
                            ugyirat.Updated.UgyTipus = true;
                        }
                        string error;

                        object ugyiratIntezesiIdo = result.Ds.Tables[0].Rows[0]["UgyiratIntezesiIdo"];
                        object idoegyseg = result.Ds.Tables[0].Rows[0]["Idoegyseg"];

                        if (ugyiratIntezesiIdo != DBNull.Value && idoegyseg != DBNull.Value)
                        {
                            ExecParam hataridoExecParam = GetExecParam(localUser);
                            DateTime dtHatarido = GetIntezesiHataridoByIdoegyseg(hataridoExecParam, idoegyseg.ToString(), ugyiratIntezesiIdo.ToString(), DateTime.Now, out error);
                            if (string.IsNullOrEmpty(error))
                            {
                                ugyirat.Hatarido = dtHatarido.ToString();
                                ugyirat.Updated.Hatarido = true;
                            }
                        }

                    }


                    EREC_IraIrattariTetelekService irattariTetelekService = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                    ExecParam irattariTetelekExecParam = GetExecParam(localUser);
                    EREC_IraIrattariTetelekSearch irattariTetelekSearch = new EREC_IraIrattariTetelekSearch();

                    irattariTetelekSearch.IrattariTetelszam.Value = document.ArchivesItem.CaseGroup.ToString();
                    irattariTetelekSearch.IrattariTetelszam.Operator = Contentum.eQuery.Query.Operators.equals;

                    Result irattariTetelResult = irattariTetelekService.GetAllWithIktathat(irattariTetelekExecParam, irattariTetelekSearch);

                    if (irattariTetelResult.IsError)
                    {
                        response.SetErrorResponse(irattariTetelResult.ErrorMessage);
                        return null;
                    }

                    if (irattariTetelResult.Ds.Tables.Count > 0 && irattariTetelResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        ugyirat.IraIrattariTetel_Id = irattariTetelResult.Ds.Tables[0].Rows[0][Constants.TableColumns.Id].ToString();
                        ugyirat.Updated.IraIrattariTetel_Id = true;
                    }
                }
            }
            return ugyirat;
        }

        public static EREC_IraIratok GetIratFromRequestForFiling(this ASP_ADO_DOCUMENT.BatchFilingDocumentDto document, string localUser)
        {
            EREC_IraIratok irat = null;
            if (document != null)
            {
                irat = new EREC_IraIratok();
                irat.Updated.SetValueAll(false);
                irat.Base.Updated.SetValueAll(false);

                switch (document.Direction)
                {
                    //1
                    case ASP_ADO_DOCUMENT.EnumsDirection.In:
                        irat.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
                        break;
                    //2
                    case ASP_ADO_DOCUMENT.EnumsDirection.Out:
                        irat.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Belso;
                        break;
                    //0
                    case ASP_ADO_DOCUMENT.EnumsDirection.Inner:
                        irat.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;
                        break;
                    default:
                        break;
                }
                irat.Updated.PostazasIranya = true;

                if (!string.IsNullOrEmpty(document.Subject))
                {
                    irat.Targy = document.Subject;
                    irat.Updated.Targy = true;
                }

                irat.Jelleg = "1";
                irat.Updated.Jelleg = true;

                if (!string.IsNullOrEmpty(document.MediaCode))
                {
                    if (document.MediaCode == "004" || document.MediaCode == "005")
                    {
                        //irat.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
                        irat.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
                    }
                    else
                    {
                        //irat.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                        irat.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                    }
                    irat.Updated.UgyintezesAlapja = true;
                    irat.Updated.AdathordozoTipusa = true;
                }

                irat.KiadmanyozniKell = "1";
                irat.Updated.KiadmanyozniKell = true;

                irat.FelhasznaloCsoport_Id_Ugyintez = localUser;
                irat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

                if (document.DocumentTypeSpecified)
                {
                    string iratTipusNev = document.DocumentType.ToString();
                    string iratTipusKod;
                    if (ASPADOHelper.IratTipusok.Keys.Any(x => x.Kod == iratTipusNev))
                    {
                        iratTipusKod = ASPADOHelper.IratTipusok.First(x => x.Key.Kod == iratTipusNev).Value.Kod;
                    }
                    else if (ASPADOHelper.IratTipusok.Values.Any(x => x.Nev == "határozat"))
                    {
                        iratTipusKod = ASPADOHelper.IratTipusok.First(x => x.Value.Nev == "határozat").Value.Kod;
                    }
                    else
                    {
                        iratTipusKod = "000";
                    }
                    irat.Irattipus = iratTipusKod;
                    irat.Updated.Irattipus = true;
                }

                irat.Munkaallomas = "ASP.ADO";
                irat.Updated.Munkaallomas = true;

                irat.UgyintezesModja = "5";
                irat.Updated.UgyintezesModja = true;
            }
            return irat;
        }

        public static Result GetUgyiratByIktatoKonyvFromRequestForBatchFiling(this ASP_ADO_DOCUMENT.BatchFilingDocumentDto document, string iktatoKonyvId, string localUser)
        {
            EREC_UgyUgyiratokService ugyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ugyiratokExecParam = ASPADOHelper.GetExecParam(localUser);
            EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
            ugyiratokSearch.IraIktatokonyv_Id.Value = iktatoKonyvId;
            ugyiratokSearch.IraIktatokonyv_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            ugyiratokSearch.Foszam.Value = document.RegistrationNumber.MainNumber.ToString();
            ugyiratokSearch.Foszam.Operator = Contentum.eQuery.Query.Operators.equals;
            ugyiratokSearch.IraIktatokonyv_Id.Value = iktatoKonyvId;
            ugyiratokSearch.IraIktatokonyv_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result ugyiratokResult = ugyiratokService.GetAll(ugyiratokExecParam, ugyiratokSearch);

            return ugyiratokResult;
        }
        public static Result GetIktatokonyvFromRequestForFiling(this ASP_ADO_DOCUMENT.BatchFilingDocumentDto documentDTO, ASP_ADO_DOCUMENT.DocumentResponse response, string localUser)
        {
            Result result = null;
            if (documentDTO.Partner != null)
            {
                EREC_IraIktatoKonyvekService iktatokonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam iktatoKonyvekExecParam = ASPADOHelper.GetExecParam(localUser);
                EREC_IraIktatoKonyvekSearch iktatokonyvekSearch = new EREC_IraIktatoKonyvekSearch();

                if (documentDTO.RegistrationNumber != null)
                {
                    if (documentDTO.RegistrationNumber.RegistrationBook != null)
                    {
                        if (documentDTO.RegistrationNumber.RegistrationBook.RegistrationBookIdSpecified && documentDTO.RegistrationNumber.RegistrationBook.RegistrationBookId != 0)
                        {
                            string id = documentDTO.RegistrationNumber.RegistrationBook.RegistrationBookId.ToString();

                            id = PadToIktatokonyvAzonosito(id);

                            iktatokonyvekSearch.Azonosito.Value = id;
                            iktatokonyvekSearch.Azonosito.Operator = Contentum.eQuery.Query.Operators.equals;
                        }
                        if (documentDTO.RegistrationNumber.RegistrationBook.YearSpecified && documentDTO.RegistrationNumber.RegistrationBook.Year != 0)
                        {
                            iktatokonyvekSearch.Ev.Value = documentDTO.RegistrationNumber.RegistrationBook.Year.ToString();
                            iktatokonyvekSearch.Ev.Operator = Contentum.eQuery.Query.Operators.equals;
                        }

                        if (!String.IsNullOrEmpty(documentDTO.RegistrationNumber.RegistrationBook.Prefix))
                        {
                            iktatokonyvekSearch.Iktatohely.Value = documentDTO.RegistrationNumber.RegistrationBook.Prefix;
                            iktatokonyvekSearch.Iktatohely.Operator = Contentum.eQuery.Query.Operators.equals;
                        }
                    }
                    Result iktatoKonyvekResult = iktatokonyvekService.GetAllWithIktathat(iktatoKonyvekExecParam, iktatokonyvekSearch);

                    if (iktatoKonyvekResult.IsError)
                    {
                        response.SetErrorResponse(iktatoKonyvekResult.ErrorMessage);
                        return null;
                    }
                    result = iktatoKonyvekResult;
                }
            }
            return result;
        }
        public static KRT_Cimek GetCimFromRequestForFiling(this ASP_ADO_DOCUMENT.BatchFilingDocumentDto document, ASP_ADO_DOCUMENT.DocumentResponse response, string localUser)
        {
            KRT_CimekService cimServirce = eAdminService.ServiceFactory.GetKRT_CimekService();
            KRT_Cimek cim = null;
            if (document.Partner != null)
            {
                if (document.Partner.Addresses != null && document.Partner.Addresses.Length > 0)
                {
                    var address = document.Partner.Addresses[0];
                    ExecParam execParam = ASPADOHelper.GetExecParam(localUser);

                    if (address.IdSpecified && address.Id != 0)
                    {
                        string addressIdGuid;

                        if (GuidCache.Any(x => x.Value == address.Id.ToString()))
                        {
                            addressIdGuid = GuidCache.FirstOrDefault(x => x.Value == address.Id.ToString()).Key;
                        }
                        else
                        {

                            Result addressIdGuidResult = cimServirce.GetASPADOAddressId(execParam, address.Id);

                            if (addressIdGuidResult.IsError)
                            {
                                response.SetErrorResponse(addressIdGuidResult.ErrorMessage);
                                return null;
                            }

                            addressIdGuid = addressIdGuidResult.Record.ToString();

                            if (string.IsNullOrEmpty(addressIdGuid))
                            {
                                return null;
                            }

                            GuidCache.Add(addressIdGuid, address.Id.ToString());
                        }

                        execParam.Record_Id = addressIdGuid;

                        Result krtCimResult = cimServirce.Get(execParam);

                        if (krtCimResult.IsError)
                        {
                            response.SetErrorResponse(krtCimResult.ErrorMessage);
                            return null;
                        }

                        cim = (KRT_Cimek)krtCimResult.Record;
                    }

                }
            }
            return cim;
        }
        public static KRT_Partnerek GetPartnerFromRequestForFiling(this ASP_ADO_DOCUMENT.BatchFilingDocumentDto document, ASP_ADO_DOCUMENT.DocumentResponse response, string localUser)
        {
            KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            KRT_Partnerek partner = null;

            if (document.Partner != null)
            {
                if (!string.IsNullOrEmpty(document.Partner.PartnerId) && document.Partner.PartnerId != "0")
                {
                    ExecParam execParam = ASPADOHelper.GetExecParam(localUser);

                    int id;
                    if (!int.TryParse(document.Partner.PartnerId, out id))
                    {
                        response.SetErrorResponse(Constants.Messages.PartnerIdInvalidError);
                        return null;
                    }

                    string partnerIdGuid;

                    if (GuidCache.Any(x => x.Value == id.ToString()))
                    {
                        partnerIdGuid = GuidCache.FirstOrDefault(x => x.Value == id.ToString()).Key;
                    }
                    else
                    {
                        Result partnerIdGuidResult = partnerService.GetASPADOPartnerId(execParam, id);

                        if (partnerIdGuidResult.IsError)
                        {
                            response.SetErrorResponse(partnerIdGuidResult.ErrorMessage);
                            return null;
                        }

                        partnerIdGuid = partnerIdGuidResult.Record.ToString();

                        if (string.IsNullOrEmpty(partnerIdGuid))
                        {
                            return null;
                        }

                        GuidCache.Add(partnerIdGuid, id.ToString());
                    }

                    execParam.Record_Id = partnerIdGuid;

                    Result krtPartnerResult = partnerService.Get(execParam);

                    if (krtPartnerResult.IsError)
                    {
                        response.SetErrorResponse(krtPartnerResult.ErrorMessage);
                        return null;
                    }

                    partner = (KRT_Partnerek)krtPartnerResult.Record;
                }
            }

            return partner;
        }


    }
}