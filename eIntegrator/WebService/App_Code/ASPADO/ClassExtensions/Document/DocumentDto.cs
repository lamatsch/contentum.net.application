﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        private static ASP_ADO_DOCUMENT.DocumentResponse BuildFiles(this ASP_ADO_DOCUMENT.DocumentDto document, string iratId, string localUser, ASP_ADO_DOCUMENT.DocumentResponse response)
        {
            if (!String.IsNullOrEmpty(iratId))
            {
                EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                ExecParam execParam = GetExecParam(localUser);
                EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();
                search.IraIrat_Id.Value = iratId;
                search.IraIrat_Id.Operator = eQuery.Query.Operators.equals;

                Result result = service.GetAll(execParam, search);

                if (!result.IsError && result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
                {
                    List<ASP_ADO_DOCUMENT.FileDto> files = new List<ASP_ADO_DOCUMENT.FileDto>();

                    for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Ds.Tables[0].Rows[i];
                        if (!IsDBNull(row["Dokumentum_Id"]))
                        {
                            string documentId = row["Dokumentum_Id"].ToString();

                            var context = HttpContext.Current;
                            string root = context.Request.Url.OriginalString;
                            root = root.Remove(root.LastIndexOf("/"));

                            string url = root + "/GetDocumentContent.aspx?id=" + documentId;

                            WebRequest request;
                            WebResponse webResponse;

                            try
                            {
                                request = WebRequest.Create(url);
                                webResponse = request.GetResponse();

                                string header = webResponse.Headers["filename"];
                                string success = webResponse.Headers["success"];

                                if (!string.IsNullOrEmpty(success))
                                {
                                    ASP_ADO_DOCUMENT.FileDto file = new ASP_ADO_DOCUMENT.FileDto();
                                    file.Name = header;
                                    file.CustomId = row.Table.Columns.Contains("CustomId") ? row["CustomId"].ToString() : string.Empty;
                                    Stream s = webResponse.GetResponseStream();
                                    file.Data = ReadFully(s);
                                    files.Add(file);
                                    s.Close();
                                }
                            }
                            catch (Exception exp)
                            {
                                Logger.Warn("ASPADO BuildFiles hiba: " + exp.Message + " url: " + url);
                            }
                        }
                    }

                    if (files.Count > 0)
                    {
                        document.Files = files.ToArray();
                    }

                    return response;
                }
            }
            return response;
        }
        private static void BuildDocumentFromUgyIratok(this ASP_ADO_DOCUMENT.DocumentDto document, DataRow ugyiratRow, DataRow iratRow, string localUser)
        {
            #region From Ugyiratok

            if (document.RegistrationNumber == null)
            {
                document.RegistrationNumber = new ASP_ADO_DOCUMENT.RegistrationNumberDto();
            }

            if (document.RegistrationNumber.RegistrationBook == null)
            {
                document.RegistrationNumber.RegistrationBook = new ASP_ADO_DOCUMENT.RegistrationBookDto();
            }

            document.ArchivesItem = new ASP_ADO_DOCUMENT.ArchivesItemDto();
            document.ArchivesItem.BuildArchiveResponse(ugyiratRow);

            if (!IsDBNull(ugyiratRow["IrattariTetelszam"]))
            {
                ASP_ADO_DOCUMENT.EnumsCaseType value;
                if (TryParseEnum<ASP_ADO_DOCUMENT.EnumsCaseType>(ugyiratRow["IrattariTetelszam"].ToString(), out value))
                {
                    document.CaseType = value;
                    document.CaseTypeSpecified = true;
                }
            }

            if (!IsDBNull(ugyiratRow["Surgosseg"]))
            {
                string surgosseg = ugyiratRow["Surgosseg"].ToString();
                if (surgosseg == "30")
                {
                    document.PriorityCode = "050";
                    document.PriorityName = "normál";
                }
                else
                {
                    document.PriorityCode = "010";
                    document.PriorityName = "sürgős";
                }
            }

            if (!IsDBNull(ugyiratRow["Foszam"]))
            {
                int value;
                if (int.TryParse(ugyiratRow["Foszam"].ToString(), out value))
                {
                    document.RegistrationNumber.MainNumber = value;
                    document.RegistrationNumber.MainNumberSpecified = true;
                }
            }

            document.RegistrationNumber.Postfix = ugyiratRow.GetRowValueAsString("MegkulJelzes");
            document.RegistrationNumber.Prefix = ugyiratRow.GetRowValueAsString("Iktatohely");

            if (!IsDBNull(ugyiratRow["IktatokonyvekAzonosito"]))
            {
                document.RegistrationNumber.RegistrationBook.RegistrationBookIdSpecified = true;

                string value = ugyiratRow["IktatokonyvekAzonosito"].ToString();

                value = value.TrimStart('0');

                int iValue = 0;
                if (int.TryParse(value, out iValue))
                {
                    document.RegistrationNumber.RegistrationBook.RegistrationBookId = iValue;
                }
            }
            document.RegistrationNumber.RegistrationBook.ClosedSpecified = false;

            if (!IsDBNull(ugyiratRow["IktatokonyvekStatusz"]))
            {
                document.RegistrationNumber.RegistrationBook.ClosedSpecified = true;

                string value = ugyiratRow["IktatokonyvekStatusz"].ToString();
                if (value == "0")
                {
                    document.RegistrationNumber.RegistrationBook.Closed = true;
                }
                else
                {
                    document.RegistrationNumber.RegistrationBook.Closed = false;
                }

            }
         
            if (!IsDBNull(ugyiratRow["Ev"]))
            {
                try
                {
                    int value;
                    if (int.TryParse(ugyiratRow["Ev"].ToString(), out value))
                    {
                        document.RegistrationNumber.RegistrationBook.Year = value;
                        document.RegistrationNumber.RegistrationBook.YearSpecified = true;
                        document.RegistrationNumber.Year = value;
                        document.RegistrationNumber.YearSpecified = true;
                    }
                }
                catch { }
            }

            document.RegistrationNumber.Prefix = ugyiratRow.GetRowValueAsString("Iktatohely");
            document.RegistrationNumber.RegistrationBook.Name = ugyiratRow.GetRowValueAsString("Iktatokonyv_Nev");

            if (!IsDBNull(ugyiratRow["Partner_Id_Ugyindito"]))
            {
                document.InvolvedPartner = GetPartner(ugyiratRow["Partner_Id_Ugyindito"].ToString(), localUser);
            }
            else if (iratRow[Constants.TableColumns.NevSTR_Bekuldo] != DBNull.Value)
            {
                if (document.InvolvedPartner == null)
                    document.InvolvedPartner = new ASP_ADO_DOCUMENT.PartnerDto();
                document.InvolvedPartner.Name = iratRow[Constants.TableColumns.NevSTR_Bekuldo].ToString();
            }

            #endregion
        }
        private static ASP_ADO_DOCUMENT.DocumentResponse BuildDocumentFromIratok(this ASP_ADO_DOCUMENT.DocumentDto document, DataRow iratRow, string localUser, ASP_ADO_DOCUMENT.DocumentResponse response)
        {
            #region From Iratok

            if (!IsDBNull(iratRow["PostazasIranya"]))
            {
                string irany = iratRow["PostazasIranya"].ToString();

                switch (irany)
                {
                    case "1":
                        document.Direction = ASP_ADO_DOCUMENT.EnumsDirection.In;
                        document.DirectionSpecified = true;
                        break;
                    case "0":
                        document.Direction = ASP_ADO_DOCUMENT.EnumsDirection.Out;
                        document.DirectionSpecified = true;
                        break;
                    case "2":
                        document.Direction = ASP_ADO_DOCUMENT.EnumsDirection.Inner;
                        document.DirectionSpecified = true;
                        break;
                }
            }

            document.DocumentId = iratRow.GetRowValueAsString("IktatoSzam_Merge");
            document.DocumentIdPartner = null;

            document.BarCode = iratRow.GetRowValueAsString("ElsoIratPeldanyBarCode");

            try
            {
                if (!IsDBNull(iratRow["Irattipus"]))
                {
                    string iratTipusKod = iratRow["Irattipus"].ToString();
                    if (IratTipusok.Values.Any(x => x.Kod == iratTipusKod))
                    {
                        string iratTipusNev = IratTipusok.First(x => x.Value.Kod == iratTipusKod).Key.Kod;
                        ASP_ADO_DOCUMENT.EnumsDocumentType value;
                        if (TryParseEnum<ASP_ADO_DOCUMENT.EnumsDocumentType>(iratTipusNev, out value))
                        {
                            document.DocumentType = value;
                            document.DocumentTypeSpecified = true;
                        }
                    }
                }
            }
            catch
            { }

            if (!IsDBNull(iratRow[Constants.TableColumns.Allapot]))
            {
                document.IsCancelled = iratRow[Constants.TableColumns.Allapot].ToString() == "90" ? true : false;
                document.IsCancelledSpecified = true;
            }

            if (!IsDBNull(iratRow["Elintezett"]))
            {
                document.IsProcessed = iratRow["Elintezett"].ToString() == "1" ? true : false;
                document.IsProcessedSpecified = true;
            }

            if (!IsDBNull(iratRow["Alszam"]))
            {
                int value;
                if (int.TryParse(iratRow["Alszam"].ToString(), out value))
                {
                    document.RegistrationNumber.SubNumber = value;
                    document.RegistrationNumber.SubNumberSpecified = true;
                }
            }

            if (!IsDBNull(iratRow["ModositasIdo"]))
            {
                DateTime value;
                if (DateTime.TryParse(iratRow["ModositasIdo"].ToString(), out value))
                {
                    document.LastChangeDate = value;
                    document.LastChangeDateSpecified = true;
                }
            }

            if (!IsDBNull(iratRow["UgyintezesAlapja"]))
            {
                string ugyintezesAlapja = iratRow["UgyintezesAlapja"].ToString();
                if (ugyintezesAlapja == "0" || ugyintezesAlapja == "V")
                {
                    document.MediaCode = "004";
                    document.MediaName = "papir";
                }
                else if (ugyintezesAlapja == "1")
                {
                    document.MediaCode = "006";
                    document.MediaName = "elektronikus form";
                }
            }

            document.Subject = iratRow.GetRowValueAsString("Targy1");

            document.TaxTypeSpecified = false;


            if (!IsDBNull(iratRow["Partner_Id_Bekuldo"]))
            {
                document.Partner = GetPartnerDTO(iratRow["Partner_Id_Bekuldo"].ToString(), localUser);
            }
            else if (iratRow[Constants.TableColumns.NevSTR_Bekuldo] != DBNull.Value)
            {
                if (document.Partner == null)
                    document.Partner = new ASP_ADO_DOCUMENT.PartnerDto();
                document.Partner.Name = iratRow[Constants.TableColumns.NevSTR_Bekuldo].ToString();
            }

            if (!IsDBNull(iratRow["PostazasIranya"]))
            {
                string postazasIranya = iratRow["PostazasIranya"].ToString();

                //bejövő
                if (postazasIranya == "1")
                {
                    InitPackagesForBejovo(document, iratRow["KuldKuldemenyek_Id"].ToString(), localUser);
                }
                else if (postazasIranya == "0" || postazasIranya == "2")
                {
                    List<string> kuldemenyIds = GetKuldemenyIDsByIratId(iratRow[Constants.TableColumns.Id].ToString(), localUser);
                    InitPackagesForKimeno(document, kuldemenyIds, localUser);
                }
            }

            return response;

            #endregion
        }



    }
}