﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        public static void BuildSearchObjectsFromRequest(this ASP_ADO_DOCUMENT.DocumentListByRegistrationNumberRequest documentListByRegistrationNumberRequest, EREC_IraIratokSearch erec_IraIratokSearch)
        {
            if (documentListByRegistrationNumberRequest.RegistrationNumber != null)
            {
                if (documentListByRegistrationNumberRequest.RegistrationNumber.MainNumberSpecified && documentListByRegistrationNumberRequest.RegistrationNumber.MainNumber != 0)
                {
                    erec_IraIratokSearch.WhereByManual += " EREC_UgyUgyiratok.Foszam= " + documentListByRegistrationNumberRequest.RegistrationNumber.MainNumber;
                }

                if (documentListByRegistrationNumberRequest.TaxTypeSpecified && documentListByRegistrationNumberRequest.TaxType.HasValue)
                {
                    string kod = documentListByRegistrationNumberRequest.TaxType.ToString();
                    if (TaxTypeKodTipusok.Keys.Any(x => x.Kod.EndsWith(kod)))
                    {

                        List<string> kodokNotSplitted = TaxTypeKodTipusok.Where(x => x.Key.Kod.EndsWith(kod)).Select(x => x.Value.Kod).ToList();

                        List<string[]> kodok = new List<string[]>();
                        kodokNotSplitted.ForEach(x =>
                        {
                            int pos = x.IndexOf('_'); string first = x.Substring(0, pos); string second = x.Substring(pos + 1); kodok.Add(new string[] { first, second });
                        });

                        string[] agazatiJelek = kodok.Select(x => x[0]).ToArray();
                        string[] ugytipusok = kodok.Select(x => x[1]).ToArray();
                        erec_IraIratokSearch.WhereByManual += string.IsNullOrEmpty(erec_IraIratokSearch.WhereByManual) ? string.Empty : " and ";
                        erec_IraIratokSearch.WhereByManual += " EREC_UgyUgyiratok.UgyTipus in ('" + ugytipusok.Aggregate((x, y) => x + "','" + y) + "') ";
                        erec_IraIratokSearch.WhereByManual += " and EREC_UgyUgyiratok.IraIrattariTetel_Id in (select itt.Id from EREC_IraIrattariTetelek as itt where itt.Id=EREC_UgyUgyiratok.IraIrattariTetel_Id and itt.IrattariTetelszam in ('" + agazatiJelek.Aggregate((x, y) => x + "','" + y) + "')) ";
                    }
                }

                if (documentListByRegistrationNumberRequest.RegistrationNumber.SubNumberSpecified && documentListByRegistrationNumberRequest.RegistrationNumber.SubNumber != 0)
                {
                    erec_IraIratokSearch.WhereByManual += string.IsNullOrEmpty(erec_IraIratokSearch.WhereByManual) ? string.Empty : " and ";
                    erec_IraIratokSearch.WhereByManual += " EREC_IraIratok.Alszam= " + documentListByRegistrationNumberRequest.RegistrationNumber.SubNumber;
                }

                if (documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook != null)
                {
                    if (documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.RegistrationBookIdSpecified && documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.RegistrationBookId != 0)
                    {
                        string id = documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.RegistrationBookId.ToString();

                        id = PadToIktatokonyvAzonosito(id);


                        erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Azonosito.Value = id;
                        erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Azonosito.Operator = Contentum.eQuery.Query.Operators.equals;
                    }
                    else
                    {
                        if (documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.YearSpecified && documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.Year != 0)
                        {
                            erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value = documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.Year.ToString();
                            erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator = Contentum.eQuery.Query.Operators.equals;
                        }

                        if (!String.IsNullOrEmpty(documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.Prefix))
                        {
                            erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Value = documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.Prefix;
                            erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Operator = Contentum.eQuery.Query.Operators.equals;
                        }

                        if (!String.IsNullOrEmpty(documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.Name))
                        {
                            erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Nev.Value = documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.Name;
                            erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Nev.Operator = Contentum.eQuery.Query.Operators.equals;
                        }
                    }
                    if (documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.ClosedSpecified)
                    {
                        erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Statusz.Value = documentListByRegistrationNumberRequest.RegistrationNumber.RegistrationBook.Closed ? "0" : "1";
                        erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Statusz.Operator = Contentum.eQuery.Query.Operators.equals;
                    }
                }
            }

            if (documentListByRegistrationNumberRequest.IsProcessedSpecified && documentListByRegistrationNumberRequest.IsProcessed.HasValue)
            {
                erec_IraIratokSearch.WhereByManual += string.IsNullOrEmpty(erec_IraIratokSearch.WhereByManual) ? string.Empty : " and ";
                var elintezett = documentListByRegistrationNumberRequest.IsProcessed.Value;
                string val = elintezett ? "=1)" : " =0 OR EREC_IraIratok.Elintezett is NULL) ";
                erec_IraIratokSearch.WhereByManual += " (EREC_IraIratok.Elintezett " + val;
            }
        }

    }
}