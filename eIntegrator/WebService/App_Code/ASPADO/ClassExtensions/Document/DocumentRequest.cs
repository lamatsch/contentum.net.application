﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System.IO;
using System.Text.RegularExpressions;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        public static ASP_ADO_DOCUMENT.BaseResponse UploadCsatolmanyokForRequest(this ASP_ADO_DOCUMENT.DocumentRequest documentRequest, ASP_ADO_DOCUMENT.BaseResponse response, string iratId, string localUser)
        {
            if (documentRequest.Document.Files != null)
            {
                EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParamCsatolmany = GetExecParam(localUser);
                execParamCsatolmany.Record_Id = iratId.ToString();

                foreach (ASP_ADO_DOCUMENT.FileDto file in documentRequest.Document.Files)
                {
                    EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
                    erec_Csatolmanyok.Updated.SetValueAll(false);
                    erec_Csatolmanyok.Base.Updated.SetValueAll(false);

                    erec_Csatolmanyok.IraIrat_Id = iratId;
                    erec_Csatolmanyok.Updated.IraIrat_Id = true;

                    erec_Csatolmanyok.CustomId = file.CustomId;
                    erec_Csatolmanyok.Updated.CustomId = true;

                    Csatolmany csatolmany = new Csatolmany();
                    csatolmany.Tartalom = file.Data;

                    string nev = "";
                    if (string.IsNullOrEmpty(file.Name))
                    {
                        nev = "File_" + iratId;
                    }
                    else
                    {
                        Regex illegalInFileName = new Regex(@"[\\/:*?""<>|]");
                        nev = illegalInFileName.Replace(file.Name, "_");
                    }

                    string extension = Path.GetExtension(nev);
                    if (!string.IsNullOrEmpty(extension) && extension == ".pdf")
                    {
                        csatolmany.Nev = nev;
                    }
                    else
                    {
                        csatolmany.Nev = nev + ".pdf";
                    }

                    Result result = erec_IraIratokService.CsatolmanyUpload(execParamCsatolmany, erec_Csatolmanyok, csatolmany);

                    if (result.IsError)
                    {
                        return response.SetErrorResponse(result.ErrorMessage);
                    }
                }
            }
            return response;
        }
    }
}