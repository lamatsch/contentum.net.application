﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {

        public static ASP_ADO_DOCUMENT.BaseResponse UploadCsatolmanyokForRequest(this ASP_ADO_DOCUMENT.FilingDocumentRequest documentRequest, ASP_ADO_DOCUMENT.BaseResponse response, string iratId, string localUser)
        {
            if (documentRequest.FilingDocument.Files != null)
            {
                EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParamCsatolmany = ASPADOHelper.GetExecParam(localUser);
                execParamCsatolmany.Record_Id = iratId.ToString();

                foreach (ASP_ADO_DOCUMENT.FilingRequestFileDto file in documentRequest.FilingDocument.Files)
                {
                    EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
                    erec_Csatolmanyok.Updated.SetValueAll(false);
                    erec_Csatolmanyok.Base.Updated.SetValueAll(false);

                    erec_Csatolmanyok.IraIrat_Id = iratId;
                    erec_Csatolmanyok.Updated.IraIrat_Id = true;

                    Csatolmany csatolmany = new Csatolmany();
                    csatolmany.Tartalom = file.Data;
                    string nev = "";
                    if (string.IsNullOrEmpty(file.Name))
                    {
                        nev = "File_" + iratId;
                    }
                    else
                    {
                        Regex illegalInFileName = new Regex(@"[\\/:*?""<>|]");
                        nev = illegalInFileName.Replace(file.Name, "_");
                    }

                    string extension = Path.GetExtension(nev);
                    if (!string.IsNullOrEmpty(extension) && extension == ".pdf")
                    {
                        csatolmany.Nev = nev;
                    }
                    else
                    {
                        csatolmany.Nev = nev + ".pdf";
                    }

                    Result result = erec_IraIratokService.CsatolmanyUpload(execParamCsatolmany, erec_Csatolmanyok, csatolmany);

                    if (result.IsError)
                    {
                        return response.SetErrorResponse(result.ErrorMessage);
                    }
                }
            }
            return response;
        }


        public static Result GetUgyiratByIktatoKonyvFromRequestForFiling(this ASP_ADO_DOCUMENT.FilingDocumentRequest documentRequest, string iktatoKonyvId, IBaseResponse response, string localUser)
        {
            EREC_UgyUgyiratokService ugyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ugyiratokExecParam = ASPADOHelper.GetExecParam(localUser);
            EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
            ugyiratokSearch.IraIktatokonyv_Id.Value = iktatoKonyvId;
            ugyiratokSearch.IraIktatokonyv_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            ugyiratokSearch.Foszam.Value = documentRequest.FilingDocument.RegistrationNumber.MainNumber.ToString();
            ugyiratokSearch.Foszam.Operator = Contentum.eQuery.Query.Operators.equals;
            ugyiratokSearch.IraIktatokonyv_Id.Value = iktatoKonyvId;
            ugyiratokSearch.IraIktatokonyv_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result ugyiratokResult = ugyiratokService.GetAll(ugyiratokExecParam, ugyiratokSearch);

            if (ugyiratokResult.IsError)
            {
                response.SetErrorResponse(ugyiratokResult.ErrorMessage);
                return null;
            }
            else if (ugyiratokResult.Ds.Tables.Count == 0 || ugyiratokResult.Ds.Tables[0].Rows.Count < 1)
            {
                response.SetErrorResponse("A megadott ügyirat nem létezik");
                return null;
            }

            return ugyiratokResult;
        }
    }
}