﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        private static ASP_ADO_DOCUMENT.GetDocumentResponse BuildFiles(this ASP_ADO_DOCUMENT.GetDocumentDto document, string iratId, string localUser, ASP_ADO_DOCUMENT.GetDocumentResponse response)
        {
            if (!String.IsNullOrEmpty(iratId))
            {
                EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                ExecParam execParam = GetExecParam(localUser);
                EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();
                search.IraIrat_Id.Value = iratId;
                search.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                Result result = service.GetAll(execParam, search);

                if (!result.IsError && result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
                {
                    List<ASP_ADO_DOCUMENT.GetDocumentFileDto> files = new List<ASP_ADO_DOCUMENT.GetDocumentFileDto>();

                    for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Ds.Tables[0].Rows[i];
                        if (!IsDBNull(row["Dokumentum_Id"]))
                        {
                            string documentId = row["Dokumentum_Id"].ToString();

                            var context = System.Web.HttpContext.Current;
                            string root = context.Request.Url.OriginalString;
                            root = root.Remove(root.LastIndexOf("/"));

                            string url = root + "/GetDocumentContent.aspx?id=" + documentId;

                            WebRequest request;
                            WebResponse webResponse;

                            try
                            {
                                request = WebRequest.Create(url);
                                webResponse = request.GetResponse();

                                string header = webResponse.Headers["filename"];
                                string success = webResponse.Headers["success"];

                                if (!string.IsNullOrEmpty(success))
                                {
                                    ASP_ADO_DOCUMENT.GetDocumentFileDto file = new ASP_ADO_DOCUMENT.GetDocumentFileDto();
                                    file.Name = header;

                                    System.IO.Stream s = webResponse.GetResponseStream();
                                    file.Data = ReadFully(s);
                                    files.Add(file);
                                    s.Close();
                                }
                            }
                            catch (Exception exp)
                            {
                                Logger.Warn("ASPADO BuildFiles hiba: " + exp.Message + " url: " + url);
                            }
                        }
                    }

                    if (files.Count > 0)
                    {
                        document.Files = files.ToArray();
                    }


                    return response;
                }
            }
            return response;
        }

        private static ASP_ADO_DOCUMENT.GetDocumentResponse BuildDocumentFromIratok(this ASP_ADO_DOCUMENT.GetDocumentDto document, DataRow iratRow, string localUser, ASP_ADO_DOCUMENT.GetDocumentResponse response)
        {
            #region From Iratok        

            document.Subject = iratRow.GetRowValueAsString("Targy1");

            if (!IsDBNull(iratRow["Partner_Id_Bekuldo"]))
            {
                document.Partner = GetDocumentPartner(iratRow["Partner_Id_Bekuldo"].ToString(), localUser);
            }
            else if (iratRow[Constants.TableColumns.NevSTR_Bekuldo] != DBNull.Value)
            {
                if (document.Partner == null)
                    document.Partner = new ASP_ADO_DOCUMENT.GetDocumentPartnerDto();
                document.Partner.Name = iratRow[Constants.TableColumns.NevSTR_Bekuldo].ToString();
            }

            if (!IsDBNull(iratRow["PostazasIranya"]))
            {
                string postazasIranya = iratRow["PostazasIranya"].ToString();

                //bejövő
                if (postazasIranya == "1")
                {
                    InitPackagesForBejovo(document, iratRow["KuldKuldemenyek_Id"].ToString(), localUser);
                }
                else if (postazasIranya == "0" || postazasIranya == "2")
                {
                    List<string> kuldemenyIds = GetKuldemenyIDsByIratId(iratRow[Constants.TableColumns.Id].ToString(), localUser);
                    InitPackagesForKimeno(document, kuldemenyIds, localUser);
                }
            }

            return response;

            #endregion
        }

        private static void BuildDocumentFromUgyIratok(this ASP_ADO_DOCUMENT.GetDocumentDto document, DataRow ugyiratRow, DataRow iratRow, string localUser)
        {
            #region From Ugyiratok   
            #endregion
        }
    }
}