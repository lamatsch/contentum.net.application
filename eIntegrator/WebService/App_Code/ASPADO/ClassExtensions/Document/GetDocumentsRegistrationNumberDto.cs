﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        public static ASP_ADO_DOCUMENT.FilingDocumentRegistrationNumberDto Map(this ASP_ADO_DOCUMENT.GetDocumentsRegistrationNumberDto document)
        {
            if (document == null)
            {
                return null;
            }
            var doc = new ASP_ADO_DOCUMENT.FilingDocumentRegistrationNumberDto();

            doc.MainNumber = document.MainNumber;
            doc.MainNumberSpecified = document.MainNumberSpecified;
            doc.Postfix = document.Postfix;
            doc.Prefix = document.Prefix;
            doc.SubNumber = document.SubNumber;
            doc.SubNumberSpecified = document.SubNumberSpecified;
            doc.Year = document.Year;
            doc.YearSpecified = document.YearSpecified;
            doc.FullRegistrationNumber = document.FullRegistrationNumber;

            return doc;
        }
    }
}