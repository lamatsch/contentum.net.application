﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        public static void BuildResponseFromResult(this ASP_ADO_DOCUMENT.GetDocumentsResponse response, DataTable iratok, string localUser)
        {
            int i = 0;

            response.Documents = new ASP_ADO_DOCUMENT.GetDocumentsDto[iratok.Rows.Count];

            foreach (DataRow row in iratok.Rows)
            {
                response.Documents[i] = new ASP_ADO_DOCUMENT.GetDocumentsDto();
                response.Documents[i].RegistrationNumber = new ASP_ADO_DOCUMENT.GetDocumentsRegistrationNumberDto();
                //response.Documents[i].RegistrationNumber.RegistrationBook = new RegistrationBookDto();

                string ugyiratId = row.GetRowValueAsString("Ugyirat_Id");

                if (!string.IsNullOrEmpty(ugyiratId))
                {
                    EREC_UgyUgyiratokService erec_UgyUgyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
                    EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
                    ugyiratokSearch.Id.Value = ugyiratId;
                    ugyiratokSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

                    Result erec_UgyUgyIratokResult = new Result();
                    erec_UgyUgyIratokResult = erec_UgyUgyIratokService.GetAllWithExtensionAndJogosultak(ASPADOHelper.GetExecParam(localUser), ugyiratokSearch, true);

                    if (erec_UgyUgyIratokResult.IsError)
                    {
                        response.Documents[i].ErrorMessage = erec_UgyUgyIratokResult.ErrorMessage;
                        //response.SetErrorResponse(erec_UgyUgyIratokResult.ErrorMessage);
                        //return;
                    }
                    else if (erec_UgyUgyIratokResult.Ds.Tables.Count > 0 && erec_UgyUgyIratokResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        var ugyiratRow = erec_UgyUgyIratokResult.Ds.Tables[0].Rows[0];
                        response.Documents[i].BuildDocumentFromUgyIratok(ugyiratRow, row, localUser);
                    }
                }

                response.Documents[i].BuildDocumentFromIratok(row, localUser, response);

                if (!String.IsNullOrEmpty(response.ErrorMessage))
                {
                    return;
                }

                response.Documents[i].BuildFiles(row[Constants.TableColumns.Id].ToString(), localUser, response);

                if (!String.IsNullOrEmpty(response.ErrorMessage))
                {
                    return;
                }

                i++;
            }
        }

    }
}