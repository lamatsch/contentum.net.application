﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {

        private static void BuildResponseFromResult(this ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponse response, DataTable kuldemenyek, string localUser)
        {
            int i = 0;

            response.GetNotToBeFiledDocumentsResponseDtos = new ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponseDto[kuldemenyek.Rows.Count];

            foreach (DataRow row in kuldemenyek.Rows)
            {
                response.GetNotToBeFiledDocumentsResponseDtos[i] = new ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponseDto();
                string ugyiratId = row.GetRowValueAsString("Ugyirat_Id");

                if (!string.IsNullOrEmpty(ugyiratId))
                {
                    EREC_UgyUgyiratokService erec_UgyUgyIratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
                    EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
                    ugyiratokSearch.Id.Value = ugyiratId;
                    ugyiratokSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

                    Result erec_UgyUgyIratokResult = new Result();
                    erec_UgyUgyIratokResult = erec_UgyUgyIratokService.GetAllWithExtensionAndJogosultak(ASPADOHelper.GetExecParam(localUser), ugyiratokSearch, true);

                    if (erec_UgyUgyIratokResult.IsError)
                    {
                        response.SetErrorResponse(erec_UgyUgyIratokResult.ErrorMessage);
                        return;
                    }
                    else if (erec_UgyUgyIratokResult.Ds.Tables.Count > 0 && erec_UgyUgyIratokResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        var ugyiratRow = erec_UgyUgyIratokResult.Ds.Tables[0].Rows[0];
                        response.GetNotToBeFiledDocumentsResponseDtos[i].BuildDocumentFromUgyIratok(ugyiratRow, row, localUser);
                    }
                }

               response.GetNotToBeFiledDocumentsResponseDtos[i].BuildDocumentFromIratok(row, localUser, response);

                if (!String.IsNullOrEmpty(response.ErrorMessage))
                {
                    return;
                }

                response.GetNotToBeFiledDocumentsResponseDtos[i].BuildFiles(row[Constants.TableColumns.Id].ToString(), localUser, response);

                if (!String.IsNullOrEmpty(response.ErrorMessage))
                {
                    return;
                }

                i++;
            }
        }

    }
}