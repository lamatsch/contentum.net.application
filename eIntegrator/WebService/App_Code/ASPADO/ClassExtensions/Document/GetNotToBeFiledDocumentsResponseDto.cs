﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        private static void BuildDocumentFromUgyIratok(this ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponseDto document, DataRow ugyiratRow, DataRow iratRow, string localUser)
        {         
            /*if (!IsDBNull(ugyiratRow["Ev"]))
            {
                try
                {
                    int value;
                    if (int.TryParse(ugyiratRow["Ev"].ToString(), out value))
                    {
                        document.RegistrationNumber.Year = value;
                        document.RegistrationNumber.YearSpecified = true;
                    }
                    else
                    {
                        document.ErrorMessage = "Hibás év formátum: " + ugyiratRow["Ev"].ToString();
                    }
                }
                catch { }
            }

            document.RegistrationNumber.Prefix = ugyiratRow.GetRowValueAsString("Iktatohely");

            if (!IsDBNull(ugyiratRow["Partner_Id_Ugyindito"]))
            {
                document.InvolvedPartner = GetDocumentsInvolvedPartner(ugyiratRow["Partner_Id_Ugyindito"].ToString(), localUser);
            }
            else if (iratRow[Constants.TableColumns.NevSTR_Bekuldo] != DBNull.Value)
            {
                if (document.InvolvedPartner == null)
                    document.InvolvedPartner = new ASP_ADO_DOCUMENT.GetDocumentsInvolvedPartnerDto();
                document.InvolvedPartner.Name = iratRow[Constants.TableColumns.NevSTR_Bekuldo].ToString();
            }*/

            
        }
        private static ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponse BuildDocumentFromIratok(this ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponseDto document, DataRow iratRow, string localUser, ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponse response)
        {
            
            /*document.DocumentId = iratRow.GetRowValueAsString("IktatoSzam_Merge");
            document.RegistrationNumber.FullRegistrationNumber = iratRow.GetRowValueAsString("IktatoSzam_Merge");


            if (!IsDBNull(iratRow["Alszam"]))
            {
                int value;
                if (int.TryParse(iratRow["Alszam"].ToString(), out value))
                {
                    document.RegistrationNumber.SubNumber = value;
                    document.RegistrationNumber.SubNumberSpecified = true;
                }
                else
                {
                    document.ErrorMessage = "Hibás alszám formátum: " + iratRow["Alszam"].ToString();
                }
            }


            if (!IsDBNull(iratRow["ModositasIdo"]))
            {
                DateTime value;
                if (DateTime.TryParse(iratRow["ModositasIdo"].ToString(), out value))
                {
                    document.LastChangeDate = value;
                    document.LastChangeDateSpecified = true;
                }
                else
                {
                    document.ErrorMessage = "Hibás ModositasIdo formátum: " + iratRow["ModositasIdo"].ToString();
                }
            }

            document.Subject = iratRow.GetRowValueAsString("Targy1");

            if (!IsDBNull(iratRow["Partner_Id_Bekuldo"]))
            {
                document.Partner = GetDocumentsPartner(iratRow["Partner_Id_Bekuldo"].ToString(), localUser);
            }
            else if (iratRow[Constants.TableColumns.NevSTR_Bekuldo] != DBNull.Value)
            {
                if (document.Partner == null)
                    document.Partner = new ASP_ADO_DOCUMENT.GetDocumentsPartnerDto();
                document.Partner.Name = iratRow[Constants.TableColumns.NevSTR_Bekuldo].ToString();
            }

            if (!IsDBNull(iratRow["PostazasIranya"]))
            {
                string postazasIranya = iratRow["PostazasIranya"].ToString();

                //bejövő
                if (postazasIranya == "1")
                {
                    InitPackagesForBejovo(document, iratRow["KuldKuldemenyek_Id"].ToString(), localUser);
                }
                else if (postazasIranya == "0" || postazasIranya == "2")
                {
                    List<string> kuldemenyIds = GetKuldemenyIDsByIratId(iratRow[Constants.TableColumns.Id].ToString(), localUser);
                    InitPackagesForKimeno(document, kuldemenyIds, localUser);
                }
            }*/

            return response;
        }

        private static ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponse BuildFiles(this ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponseDto document, string iratId, string localUser, ASP_ADO_DOCUMENT.GetNotToBeFiledDocumentsResponse response)
        {
            /*if (!String.IsNullOrEmpty(iratId))
            {
                EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                ExecParam execParam = GetExecParam(localUser);
                EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();
                search.IraIrat_Id.Value = iratId;
                search.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                Result result = service.GetAll(execParam, search);

                if (!result.IsError && result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
                {
                    List<ASP_ADO_DOCUMENT.GetDocumentsFileDto> files = new List<ASP_ADO_DOCUMENT.GetDocumentsFileDto>();

                    for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Ds.Tables[0].Rows[i];
                        if (!IsDBNull(row["Dokumentum_Id"]))
                        {
                            string documentId = row["Dokumentum_Id"].ToString();

                            try
                            {
                                Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

                                execParam.Record_Id = documentId;
                                char jogszint = 'O';

                                Result dokGetResult = dokService.GetWithRightCheck(execParam, jogszint);
                                if (dokGetResult.IsError)
                                {
                                    // document.ErrorMessage = "Hiba az állomány lekérdezése folyamán: " + dokGetResult.ErrorMessage;
                                    // continue;
                                    response.ErrorMessage = "Hiba az állomány lekérdezése folyamán: " + dokGetResult.ErrorMessage;
                                    return response;
                                   
                                }

                                string filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
                                string guid = execParam.Record_Id;

                                ASP_ADO_DOCUMENT.GetDocumentsFileDto file = new ASP_ADO_DOCUMENT.GetDocumentsFileDto();
                                file.Guid = guid;
                                file.Name = filename;
                                files.Add(file);

                            }
                            catch (Exception exp)
                            {
                                //document.ErrorMessage = "Hiba az állomány lekérdezése folyamán: " + exp.Message;
                                response.ErrorMessage = "Hiba az állomány lekérdezése folyamán: " + exp.Message;
                                Logger.Warn("ASPADO BuildFiles hiba: " + exp.Message + " documentId: " + documentId);
                                return response;                               
                            }
                        }
                    }

                    if (files.Count > 0)
                    {
                        document.Files = files.ToArray();
                    }

                    return response;
                }
            }*/
            return response;
        }
    }
}