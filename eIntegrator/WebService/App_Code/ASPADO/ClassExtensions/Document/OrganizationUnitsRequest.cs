﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        public static ASP_ADO_DOCUMENT.GetOrganizationUnitsResponse GetOrganizationUnits(ASP_ADO_DOCUMENT.OrganizationUnitsRequest request, ASP_ADO_DOCUMENT.GetOrganizationUnitsResponse response, string runningUser)
        {
            ExecParam execParam = GetExecParam(runningUser);

            string requestUser = request.User.LoginName.GetMappedUserId();

            if (string.IsNullOrEmpty(requestUser))
            {
                return response.SetErrorResponse(Constants.Messages.UserError);
            }

            Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

            KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();

            search_csoportok.Csoport_Id.Value = request.User.ActualOrganizationUnit.OrganizationUnitId;
            search_csoportok.Csoport_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            search_csoportok.Csoport_Id_Jogalany.Value = requestUser;
            search_csoportok.Csoport_Id_Jogalany.Operator = Contentum.eQuery.Query.Operators.equals;
            search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";
            search_csoportok.TopRow = 1;

            Result result_csoportok = service_csoportok.GetAllWithExtension(execParam, search_csoportok);

            if (result_csoportok.IsError)
            {
                return response.SetErrorResponse(result_csoportok.ErrorMessage);
            }
            else if (result_csoportok.Ds.Tables.Count == 0 || result_csoportok.Ds.Tables[0].Rows.Count == 0)
            {
                return response.SetErrorResponse(Constants.Messages.OrganizationUnitsNotFoundError);
            }
            else
            {
                int i = 0;

                KRT_FunkciokService funkcioService = eAdminService.ServiceFactory.GetKRT_FunkciokService();

                response.OrganizationUnits = new ASP_ADO_DOCUMENT.GetOrganizationUnitsDto[result_csoportok.Ds.Tables[0].Rows.Count];

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    response.OrganizationUnits[i] = new ASP_ADO_DOCUMENT.GetOrganizationUnitsDto();
                    // response.OrganizationUnits[i].CsoportTagId = row["Id"].ToString();DTAP=>NEW WSDL
                    response.OrganizationUnits[i].Name = row["Csoport_Nev"].ToString();
                    response.OrganizationUnits[i].OrganizationUnitId = row["Csoport_Id"].ToString();

                    execParam.Record_Id = row[Constants.TableColumns.Id].ToString();
                    Result funkcioResult = funkcioService.GetAllByCsoporttagSajatJogu(execParam);

                    if (funkcioResult.IsError)
                    {
                        return response.SetErrorResponse("Hiba a jogosultságok olvasásakor: " + funkcioResult.ErrorMessage);
                    }

                    if (funkcioResult.Ds.Tables[0].Rows.Count < 1)
                    {
                        response.OrganizationUnits[i].Roles = new ASP_ADO_DOCUMENT.GetOrganizationUnitsUserRolesDto[0];
                        continue;
                    }

                    var rows = funkcioResult.Ds.Tables[0].Rows;

                    List<ASP_ADO_DOCUMENT.GetOrganizationUnitsUserRolesDto> roles = new List<ASP_ADO_DOCUMENT.GetOrganizationUnitsUserRolesDto>();

                    for (int j = 0; j < rows.Count; j++)
                    {
                        string kod = rows[j]["Kod"].ToString();

                        if (kod == "Expedialas")
                        {
                            var dto = new ASP_ADO_DOCUMENT.GetOrganizationUnitsUserRolesDto();
                            roles.Add(dto);
                            dto.Id = 1;
                            dto.IdSpecified = true;
                            dto.Code = ASP_ADO_DOCUMENT.EnumsRoles.exp;
                            dto.CodeSpecified = true;
                            dto.Name = "Expediálás";
                        }
                        else if (kod == "IraIratCsatolmanyElektronikusAlairas")
                        {
                            var dto = new ASP_ADO_DOCUMENT.GetOrganizationUnitsUserRolesDto();
                            roles.Add(dto);
                            dto.Id = 2;
                            dto.IdSpecified = true;
                            dto.Code = ASP_ADO_DOCUMENT.EnumsRoles.kiad;
                            dto.CodeSpecified = true;
                            dto.Name = "Kiadmányozás";
                        }
                        else if (kod == "Postazas")
                        {
                            var dto = new ASP_ADO_DOCUMENT.GetOrganizationUnitsUserRolesDto();
                            roles.Add(dto);
                            dto.Id = 3;
                            dto.IdSpecified = true;
                            dto.Code = ASP_ADO_DOCUMENT.EnumsRoles.post;
                            dto.CodeSpecified = true;
                            dto.Name = "Postázás";
                        }
                    }

                    response.OrganizationUnits[i].Roles = roles.ToArray();

                    if (response.OrganizationUnits[i].Roles == null)
                        response.OrganizationUnits[i].Roles = new ASP_ADO_DOCUMENT.GetOrganizationUnitsUserRolesDto[0];

                    i++;
                }
            }

            return response.SetSuccessResponse();
        }

    }
}