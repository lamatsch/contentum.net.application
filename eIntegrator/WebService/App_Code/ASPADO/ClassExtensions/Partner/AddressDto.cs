﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        public static KRT_PartnerCimek GetPartnerCimekFromRequestForInsert(this ASP_ADO_PARTNER.AddressDto address, string partnerId, string cimId)
        {
            KRT_PartnerCimek krt_partnercimek = new KRT_PartnerCimek();
            krt_partnercimek.Updated.SetValueAll(false);
            krt_partnercimek.Base.Updated.SetValueAll(false);

            krt_partnercimek.Partner_id = partnerId;
            krt_partnercimek.Updated.Partner_id = true;

            krt_partnercimek.Cim_Id = cimId;
            krt_partnercimek.Updated.Cim_Id = true;

            if (address.AddressTypeSpecified)
            {
                string fajta = "04";
                if (address.AddressType == ASP_ADO_PARTNER.EnumsAddressType.Mailing)
                {
                    fajta = "05";
                }
                else if (address.AddressType == ASP_ADO_PARTNER.EnumsAddressType.Permanent)
                {
                    fajta = "03";
                }

                krt_partnercimek.Fajta = fajta;
                krt_partnercimek.Updated.Fajta = true;
            }

            krt_partnercimek.ErvKezd = DateTime.Today.ToString();
            krt_partnercimek.Updated.ErvKezd = true;
            krt_partnercimek.ErvVege = new DateTime(4700, 12, 31).ToString();
            krt_partnercimek.Updated.ErvVege = true;

            krt_partnercimek.Base.Ver = "1";
            krt_partnercimek.Base.Updated.Ver = true;

            return krt_partnercimek;
        }
        public static KRT_Cimek GetCimekFromRequestForInsert(this ASP_ADO_PARTNER.AddressDto address, string localUser)
        {
            KRT_Cimek krt_Cimek = new KRT_Cimek();
            krt_Cimek.Updated.SetValueAll(false);
            krt_Cimek.Base.Updated.SetValueAll(false);

            krt_Cimek.Tipus = KodTarak.Cim_Tipus.Postai;
            krt_Cimek.Updated.Tipus = true;

            if (!string.IsNullOrEmpty(address.CountryCode))
            {
                ExecParam execParam = GetExecParam(localUser);
                Result result = new Result();
                Contentum.eAdmin.Service.KRT_OrszagokService serviceOrszagok = eAdminService.ServiceFactory.GetKRT_OrszagokService();
                KRT_OrszagokSearch searchOrszagok = new KRT_OrszagokSearch();
                searchOrszagok.Kod.Value = address.CountryCode;
                searchOrszagok.Kod.Operator = Contentum.eQuery.Query.Operators.like;
                result = serviceOrszagok.GetAll(execParam, searchOrszagok);

                if (!result.IsError)
                {
                    string orszagNev = "";
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        orszagNev = result.Ds.Tables[0].Rows[0].GetRowValueAsString(Constants.TableColumns.Nev);

                    }
                    if (!String.IsNullOrEmpty(orszagNev))
                    {
                        krt_Cimek.OrszagNev = orszagNev;
                        krt_Cimek.Updated.OrszagNev = true;
                    }
                }
            }

            if (!string.IsNullOrEmpty(address.City))
            {
                krt_Cimek.TelepulesNev = address.City;
                krt_Cimek.Updated.TelepulesNev = true;
            }

            if (!string.IsNullOrEmpty(address.Zip))
            {
                krt_Cimek.IRSZ = address.Zip;
                krt_Cimek.Updated.IRSZ = true;
            }

            if (!string.IsNullOrEmpty(address.StreetName))
            {
                krt_Cimek.KozteruletNev = address.StreetName;
                krt_Cimek.Updated.KozteruletNev = true;
            }

            if (!string.IsNullOrEmpty(address.StreetType))
            {

                krt_Cimek.KozteruletTipusNev = address.StreetType.ToString();
                krt_Cimek.Updated.KozteruletTipusNev = true;
            }

            if (!string.IsNullOrEmpty(address.Number))
            {
                krt_Cimek.Hazszam = address.Number;
                krt_Cimek.Updated.Hazszam = true;
            }

            if (!string.IsNullOrEmpty(address.Building))
            {
                krt_Cimek.HazszamBetujel = address.Building;
                krt_Cimek.Updated.HazszamBetujel = true;
            }

            if (!string.IsNullOrEmpty(address.Staircase))
            {
                krt_Cimek.Lepcsohaz = address.Staircase;
                krt_Cimek.Updated.Lepcsohaz = true;
            }

            if (!string.IsNullOrEmpty(address.Floor))
            {
                krt_Cimek.Szint = address.Floor;
                krt_Cimek.Updated.Szint = true;
            }

            if (!string.IsNullOrEmpty(address.Door))
            {
                krt_Cimek.Ajto = address.Door;
                krt_Cimek.Updated.Ajto = true;
            }

            if (!string.IsNullOrEmpty(address.ParcelNumber))
            {
                krt_Cimek.HRSZ = address.ParcelNumber;
                krt_Cimek.Updated.HRSZ = true;
            }

            if (!string.IsNullOrEmpty(address.Building))
            {
                krt_Cimek.HazszamBetujel = address.Building;
                krt_Cimek.Updated.HazszamBetujel = true;
            }

            krt_Cimek.ErvKezd = DateTime.Today.ToString();
            krt_Cimek.Updated.ErvKezd = true;
            krt_Cimek.ErvVege = new DateTime(4700, 12, 31).ToString();
            krt_Cimek.Updated.ErvVege = true;

            //Kategória beállítása konkrétra, jelenleg bedrótozva
            krt_Cimek.Kategoria = "K";
            krt_Cimek.Updated.Kategoria = true;

            //a megnyitási verzió megadása ellenõrzés miatt
            krt_Cimek.Base.Ver = "1";
            krt_Cimek.Base.Updated.Ver = true;

            return krt_Cimek;
        }
        public static KRT_Cimek GetCimekFromRequestForUpdate(this ASP_ADO_PARTNER.AddressDto address, KRT_Cimek krt_Cimek, string localUser)
        {
            krt_Cimek.Updated.SetValueAll(false);
            krt_Cimek.Base.Updated.SetValueAll(false);

            krt_Cimek.Base.Updated.Ver = true;

            //krt_Cimek.Tipus = KodTarak.Cim_Tipus.Postai;
            //krt_Cimek.Updated.Tipus = true;

            if (!string.IsNullOrEmpty(address.CountryCode))
            {
                ExecParam execParam = GetExecParam(localUser);
                Result result = new Result();
                Contentum.eAdmin.Service.KRT_OrszagokService serviceOrszagok = eAdminService.ServiceFactory.GetKRT_OrszagokService();
                KRT_OrszagokSearch searchOrszagok = new KRT_OrszagokSearch();
                searchOrszagok.Kod.Value = address.CountryCode;
                searchOrszagok.Kod.Operator = Contentum.eQuery.Query.Operators.like;
                result = serviceOrszagok.GetAll(execParam, searchOrszagok);

                if (!result.IsError)
                {
                    string orszagNev = "";
                    if (result.Ds.Tables[0].Rows.Count > 0)
                    {
                        orszagNev = result.Ds.Tables[0].Rows[0].GetRowValueAsString(Constants.TableColumns.Nev);

                    }
                    if (!String.IsNullOrEmpty(orszagNev) && orszagNev != krt_Cimek.OrszagNev)
                    {
                        krt_Cimek.OrszagNev = orszagNev;
                        krt_Cimek.Updated.OrszagNev = true;
                    }
                }
            }

            if (!string.IsNullOrEmpty(address.City))
            {
                if (address.City != krt_Cimek.TelepulesNev)
                {
                    krt_Cimek.TelepulesNev = address.City;
                    krt_Cimek.Updated.TelepulesNev = true;
                }
            }

            if (!string.IsNullOrEmpty(address.Zip))
            {
                if (address.Zip != krt_Cimek.IRSZ)
                {
                    krt_Cimek.IRSZ = address.Zip;
                    krt_Cimek.Updated.IRSZ = true;
                }
            }

            if (!string.IsNullOrEmpty(address.StreetName))
            {
                if (address.StreetName != krt_Cimek.KozteruletNev)
                {
                    krt_Cimek.KozteruletNev = address.StreetName;
                    krt_Cimek.Updated.KozteruletNev = true;
                }
            }

            if (!string.IsNullOrEmpty(address.StreetType))
            {
                if (krt_Cimek.KozteruletTipusNev != address.StreetType)
                {
                    krt_Cimek.KozteruletTipusNev = address.StreetType;
                    krt_Cimek.Updated.KozteruletTipusNev = true;
                }
            }

            if (!string.IsNullOrEmpty(address.Number))
            {
                if (krt_Cimek.Hazszam != address.Number)
                {
                    krt_Cimek.Hazszam = address.Number;
                    krt_Cimek.Updated.Hazszam = true;
                }
            }

            if (!string.IsNullOrEmpty(address.Floor))
            {
                if (krt_Cimek.Szint != address.Floor)
                {
                    krt_Cimek.Szint = address.Floor;
                    krt_Cimek.Updated.Szint = true;
                }
            }

            if (!string.IsNullOrEmpty(address.Staircase))
            {
                if (krt_Cimek.Lepcsohaz != address.Staircase)
                {
                    krt_Cimek.Lepcsohaz = address.Staircase;
                    krt_Cimek.Updated.Lepcsohaz = true;
                }
            }

            if (!string.IsNullOrEmpty(address.Door))
            {
                if (krt_Cimek.Ajto != address.Door)
                {
                    krt_Cimek.Ajto = address.Door;
                    krt_Cimek.Updated.Ajto = true;
                }
            }

            if (!string.IsNullOrEmpty(address.Building))
            {
                if (krt_Cimek.HazszamBetujel != address.Building)
                {
                    krt_Cimek.HazszamBetujel = address.Building;
                    krt_Cimek.Updated.HazszamBetujel = true;
                }
            }

            return krt_Cimek;
        }
        public static ASP_ADO_DOCUMENT.AddressDto MapAddress(this ASP_ADO_PARTNER.AddressDto address)
        {
            ASP_ADO_DOCUMENT.AddressDto mapped = null;

            if (address != null)
            {
                mapped = new ASP_ADO_DOCUMENT.AddressDto();

                mapped.AddressType = address.AddressType.MapEnum<ASP_ADO_PARTNER.EnumsAddressType, ASP_ADO_DOCUMENT.EnumsAddressType>();
                mapped.AddressTypeSpecified = address.AddressTypeSpecified;
                mapped.City = address.City;
                mapped.CountryCode = address.CountryCode;
                mapped.County = address.County;
                mapped.Id = address.Id;
                mapped.IdSpecified = address.IdSpecified;
                mapped.Number = address.Number;
                //mapped.Partner = address.Partner;
                mapped.Staircase = address.Staircase;
                mapped.Building = address.Building;
                mapped.Floor = address.Floor;
                mapped.Door = address.Door;
                mapped.ParcelNumber = address.ParcelNumber;
                mapped.StreetName = address.StreetName;
                mapped.StreetType = address.StreetType;
                mapped.Zip = address.Zip;
            }

            return mapped;

        }
        public static void AddValuesWithInOperatorToCimekSearch(this ASP_ADO_PARTNER.AddressDto[] Addresses, KRT_CimekSearch cimekSearch, Func<ASP_ADO_PARTNER.AddressDto, string> columnSelector, Func<KRT_CimekSearch, Field> cimekColumnSelector)
        {
            List<string> values = new List<string>();

            foreach (ASP_ADO_PARTNER.AddressDto address in Addresses)
            {
                if (!String.IsNullOrEmpty(columnSelector(address)))
                {
                    values.Add(columnSelector(address));
                }
            }
            cimekColumnSelector(cimekSearch).Value = Search.GetSqlInnerString(values.ToArray());
            cimekColumnSelector(cimekSearch).Operator = Contentum.eQuery.Query.Operators.inner;
        }

    }
}