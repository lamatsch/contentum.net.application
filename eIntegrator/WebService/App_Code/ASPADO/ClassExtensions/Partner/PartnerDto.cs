﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        public static ASP_ADO_DOCUMENT.PartnerDto MapPartner(this ASP_ADO_PARTNER.PartnerDto partner)
        {
            ASP_ADO_DOCUMENT.PartnerDto mapped = null;

            if (partner != null)
            {
                mapped = new ASP_ADO_DOCUMENT.PartnerDto();

                if (partner.Addresses != null)
                {
                    mapped.Addresses = new ASP_ADO_DOCUMENT.AddressDto[partner.Addresses.Length];

                    for (int i = 0; i < partner.Addresses.Length; i++)
                    {
                        mapped.Addresses[i] = partner.Addresses[i].MapAddress();
                    }
                }

                mapped.AccountNumber = partner.AccountNumber;
                mapped.ADOPartnerId = partner.ADOPartnerId;
                mapped.BirthDate = partner.BirthDate;
                mapped.BirthDateSpecified = partner.BirthDateSpecified;
                mapped.BirthName = partner.BirthName;
                mapped.BirthPlace = partner.BirthPlace;
                mapped.CardNumber = partner.CardNumber;
                mapped.Comment = partner.Comment;
                mapped.CompanyRegistrationNumber = partner.CompanyRegistrationNumber;
                mapped.CreationDate = partner.CreationDate;
                mapped.CreationDateSpecified = partner.CreationDateSpecified;
                mapped.CustomerGateId = partner.CustomerGateId;
                mapped.ECustomerId = partner.ECustomerId;
                mapped.Email = partner.Email;
                mapped.FirstName = partner.FirstName;
                mapped.IBANPrefix = partner.IBANPrefix;
                mapped.IsCancelled = partner.IsCancelled;
                mapped.IsCancelledSpecified = partner.IsCancelledSpecified;
                mapped.LastName = partner.LastName;
                mapped.MiddleName = partner.MiddleName;
                mapped.ModificationDate = partner.ModificationDate;
                mapped.ModificationDateSpecified = partner.ModificationDateSpecified;
                mapped.MothersName = partner.MothersName;
                mapped.Name = partner.Name;
                mapped.PartnerId = partner.PartnerId;
                mapped.PartnerType = partner.PartnerType.MapEnum<ASP_ADO_PARTNER.EnumsPartnerType?, ASP_ADO_DOCUMENT.EnumsPartnerType?>();
                mapped.PartnerTypeSpecified = partner.PartnerTypeSpecified;
                mapped.Post = partner.Post;
                mapped.StatisticNumber = partner.StatisticNumber;
                mapped.TaxId = partner.TaxId;
                mapped.TaxIdentificationNumber = partner.TaxIdentificationNumber;
                mapped.TaxIdentificationNumberSpecified = partner.TaxIdentificationNumberSpecified;
                mapped.Title = partner.Title;
            }

            return mapped;
        }

        public static ASP_ADO_DOCUMENT.GetDocumentsPartnerDto MapGetDocumentsPartner(this ASP_ADO_PARTNER.PartnerDto partner)
        {
            ASP_ADO_DOCUMENT.GetDocumentsPartnerDto mapped = null;

            if (partner != null)
            {
                mapped = new ASP_ADO_DOCUMENT.GetDocumentsPartnerDto();
                mapped.Name = partner.Name;
            }

            return mapped;
        }

        public static ASP_ADO_DOCUMENT.GetDocumentPartnerDto MapGetDocumentPartner(this ASP_ADO_PARTNER.PartnerDto partner)
        {
            ASP_ADO_DOCUMENT.GetDocumentPartnerDto mapped = null;

            if (partner != null)
            {
                mapped = new ASP_ADO_DOCUMENT.GetDocumentPartnerDto();
                mapped.Name = partner.Name;
            }

            return mapped;
        }
        public static ASP_ADO_DOCUMENT.GetDocumentsInvolvedPartnerDto MapGetDocumentsInvolvedPartner(this ASP_ADO_PARTNER.PartnerDto partner)
        {
            ASP_ADO_DOCUMENT.GetDocumentsInvolvedPartnerDto mapped = null;

            if (partner != null)
            {
                mapped = new ASP_ADO_DOCUMENT.GetDocumentsInvolvedPartnerDto();
                mapped.Name = partner.Name;
            }

            return mapped;
        }

    }

}