﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        public static KRT_Partnerek GetPartnerFromRequestForUpdateWithInvalidate(this ASP_ADO_PARTNER.PartnerRequest partnerRequest, KRT_Partnerek krt_Partnerek, string localUser, IBaseResponse response)
        {
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);

            string tipus = null;
            if (partnerRequest.Partner.PartnerType.HasValue)
            {
                if (partnerRequest.Partner.PartnerType == ASP_ADO_PARTNER.EnumsPartnerType.Individual)
                {
                    tipus = KodTarak.Partner_Tipus.Szemely;
                    KRT_PartnerVallalkozasok p = (KRT_PartnerVallalkozasok)krt_Partnerek;
                    KRT_VallalkozasokService vallakozasokService = eAdminService.ServiceFactory.GetKRT_VallalkozasokService();
                    execParam.Record_Id = p.Vallalkozasok.Id;

                    p.Vallalkozasok = (KRT_Vallalkozasok)vallakozasokService.Get(execParam).Record;

                    Result invalidateResult = vallakozasokService.Invalidate(execParam);
                    if (invalidateResult.IsError)
                    {
                        response.SetError(invalidateResult.ErrorMessage);
                        return krt_Partnerek;
                    }
                    p.Vallalkozasok = null;
                }
                else
                {
                    tipus = KodTarak.Partner_Tipus.Szervezet;
                    KRT_PartnerSzemelyek p = (KRT_PartnerSzemelyek)krt_Partnerek;
                    KRT_SzemelyekService szemelyekService = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
                    execParam.Record_Id = p.Szemelyek.Id;

                    p.Szemelyek = (KRT_Szemelyek)szemelyekService.Get(execParam).Record;

                    Result invalidateResult = szemelyekService.Invalidate(execParam);
                    if (invalidateResult.IsError)
                    {
                        response.SetError(invalidateResult.ErrorMessage);
                        return krt_Partnerek;
                    }
                    p.Szemelyek = null;
                    // p.Szemelyek.Id = "";            

                }
            }

            krt_Partnerek.Updated.SetValueAll(false);
            krt_Partnerek.Base.Updated.SetValueAll(false);

            if (!String.IsNullOrEmpty(tipus) && krt_Partnerek.Tipus != tipus)
            {
                krt_Partnerek.Tipus = tipus;
                krt_Partnerek.Updated.Tipus = true;
            }

            if (krt_Partnerek.Nev != partnerRequest.Partner.Name)
            {
                krt_Partnerek.Nev = partnerRequest.Partner.Name;
                krt_Partnerek.Updated.Nev = true;
            }

            if (partnerRequest.Partner.CreationDateSpecified && partnerRequest.Partner.CreationDate.HasValue)
            {
                if (krt_Partnerek.ErvKezd != partnerRequest.Partner.CreationDate.Value.ToShortDateString())
                {
                    krt_Partnerek.ErvKezd = partnerRequest.Partner.CreationDate.Value.ToShortDateString();
                    krt_Partnerek.Updated.ErvKezd = true;
                }
            }

            krt_Partnerek.Base.Updated.Ver = true;

            switch (tipus)
            {
                case KodTarak.Partner_Tipus.Szemely:
                    {
                        KRT_Szemelyek krt_szemelyek = GetSzemelyekFromRequestForInsert(partnerRequest);
                        KRT_PartnerSzemelyek krt_partnerSzemelyek = new KRT_PartnerSzemelyek(krt_Partnerek, krt_szemelyek);
                        krt_partnerSzemelyek.Szemelyek = krt_szemelyek;
                        return (KRT_Partnerek)krt_partnerSzemelyek;

                    }
                case KodTarak.Partner_Tipus.Szervezet:
                    {
                        KRT_Vallalkozasok krt_vallalkozasok = GetVallalkozasokFromRequestForInsert(partnerRequest);
                        KRT_PartnerVallalkozasok krt_partnerVallalkozasok = new KRT_PartnerVallalkozasok(krt_Partnerek, krt_vallalkozasok);
                        krt_partnerVallalkozasok.Vallalkozasok = krt_vallalkozasok;
                        return (KRT_Partnerek)krt_partnerVallalkozasok;
                    }
                case KodTarak.Partner_Tipus.Alkalmazas:
                    break;
                default:
                    break;
            }
            return krt_Partnerek;
        }

        public static KRT_Partnerek GetPartnerFromRequestForUpdate(this ASP_ADO_PARTNER.PartnerRequest partnerRequest, KRT_Partnerek krt_Partnerek)
        {
            krt_Partnerek.Updated.SetValueAll(false);
            krt_Partnerek.Base.Updated.SetValueAll(false);

            if (krt_Partnerek.Nev != partnerRequest.Partner.Name)
            {
                krt_Partnerek.Nev = partnerRequest.Partner.Name;
                krt_Partnerek.Updated.Nev = true;
            }

            if (partnerRequest.Partner.PartnerTypeSpecified && partnerRequest.Partner.PartnerType.HasValue)
            {
                string tipus;
                if (partnerRequest.Partner.PartnerType == ASP_ADO_PARTNER.EnumsPartnerType.Company)
                {
                    tipus = KodTarak.Partner_Tipus.Szervezet;
                }
                else
                {
                    tipus = KodTarak.Partner_Tipus.Szemely;
                }

                if (!String.IsNullOrEmpty(tipus) && krt_Partnerek.Tipus != tipus)
                {
                    krt_Partnerek.Tipus = tipus;
                    krt_Partnerek.Updated.Tipus = true;
                }
            }

            if (partnerRequest.Partner.CreationDateSpecified && partnerRequest.Partner.CreationDate.HasValue)
            {
                if (krt_Partnerek.ErvKezd != partnerRequest.Partner.CreationDate.Value.ToShortDateString())
                {
                    krt_Partnerek.ErvKezd = partnerRequest.Partner.CreationDate.Value.ToShortDateString();
                    krt_Partnerek.Updated.ErvKezd = true;
                }
            }

            krt_Partnerek.Base.Updated.Ver = true;

            switch (krt_Partnerek.Tipus)
            {
                case KodTarak.Partner_Tipus.Szemely:
                    {
                        KRT_Szemelyek krt_szemelyek = GetSzemelyekFromRequestForUpdate(partnerRequest, (KRT_PartnerSzemelyek)krt_Partnerek);
                        break;
                    }
                case KodTarak.Partner_Tipus.Szervezet:
                    {
                        KRT_Vallalkozasok krt_vallalkozasok = GetVallalkozasokFromRequestForUpdate(partnerRequest, (KRT_PartnerVallalkozasok)krt_Partnerek);
                        break;
                    }
                case KodTarak.Partner_Tipus.Alkalmazas:
                    break;
                default:
                    break;
            }

            return krt_Partnerek;
        }
        public static KRT_Partnerek GetPartnerFromRequestForInsert(this ASP_ADO_PARTNER.PartnerRequest partnerRequest)
        {
            KRT_Partnerek krt_Partnerek = new KRT_Partnerek();

            krt_Partnerek.Updated.SetValueAll(false);
            krt_Partnerek.Base.Updated.SetValueAll(false);

            krt_Partnerek.Nev = partnerRequest.Partner.Name;
            krt_Partnerek.Updated.Nev = true;

            krt_Partnerek.Forras = "Z";
            krt_Partnerek.Updated.Forras = true;

            if (partnerRequest.Partner.PartnerTypeSpecified && partnerRequest.Partner.PartnerType.HasValue)
            {
                if (partnerRequest.Partner.PartnerType == ASP_ADO_PARTNER.EnumsPartnerType.Company)
                {
                    krt_Partnerek.Tipus = KodTarak.Partner_Tipus.Szervezet;
                }
                else
                {
                    krt_Partnerek.Tipus = KodTarak.Partner_Tipus.Szemely;
                }
                krt_Partnerek.Updated.Tipus = true;
            }

            if (partnerRequest.Partner.CreationDateSpecified && partnerRequest.Partner.CreationDate.HasValue)
            {
                krt_Partnerek.ErvKezd = partnerRequest.Partner.CreationDate.Value.ToShortDateString();
                krt_Partnerek.Updated.ErvKezd = true;
            }
            else
            {
                krt_Partnerek.ErvKezd = DateTime.Today.ToString();
                krt_Partnerek.Updated.ErvKezd = true;
            }

            krt_Partnerek.ErvVege = new DateTime(4700, 12, 31).ToString();
            krt_Partnerek.Updated.ErvVege = true;

            krt_Partnerek.Base.Ver = "1";
            krt_Partnerek.Base.Updated.Ver = true;

            switch (krt_Partnerek.Tipus)
            {
                case KodTarak.Partner_Tipus.Szemely:
                    {
                        KRT_Szemelyek krt_szemelyek = GetSzemelyekFromRequestForInsert(partnerRequest);
                        KRT_PartnerSzemelyek krt_partnerSzemelyek = new KRT_PartnerSzemelyek(krt_Partnerek, krt_szemelyek);
                        return (KRT_Partnerek)krt_partnerSzemelyek;
                    }
                case KodTarak.Partner_Tipus.Szervezet:
                    {
                        KRT_Vallalkozasok krt_vallalkozasok = GetVallalkozasokFromRequestForInsert(partnerRequest);
                        KRT_PartnerVallalkozasok krt_partnerVallalkozasok = new KRT_PartnerVallalkozasok(krt_Partnerek, krt_vallalkozasok);
                        return (KRT_Partnerek)krt_partnerVallalkozasok;
                    }
                case KodTarak.Partner_Tipus.Alkalmazas:
                    break;
                default:
                    break;
            }

            return krt_Partnerek;
        }
        public static KRT_Szemelyek GetSzemelyekFromRequestForInsert(this ASP_ADO_PARTNER.PartnerRequest partnerRequest)
        {
            var partner = partnerRequest.Partner;

            KRT_Szemelyek krt_Szemelyek = new KRT_Szemelyek();
            krt_Szemelyek.Updated.SetValueAll(false);

            if (!String.IsNullOrEmpty(partner.Title))
            {
                krt_Szemelyek.UjTitulis = partner.Title;
                krt_Szemelyek.Updated.UjTitulis = true;
            }

            if (!String.IsNullOrEmpty(partner.LastName))
            {
                krt_Szemelyek.UjCsaladiNev = partner.LastName;
                krt_Szemelyek.Updated.UjCsaladiNev = true;
            }

            if (!String.IsNullOrEmpty(partner.MiddleName))
            {
                krt_Szemelyek.UjUtonev = partner.MiddleName;
                krt_Szemelyek.Updated.UjUtonev = true;

                if (!String.IsNullOrEmpty(partner.FirstName))
                {
                    krt_Szemelyek.UjTovabbiUtonev = partner.FirstName;
                    krt_Szemelyek.Updated.UjTovabbiUtonev = true;
                }
            }
            else if (!String.IsNullOrEmpty(partner.FirstName))
            {
                krt_Szemelyek.UjUtonev = partner.FirstName;
                krt_Szemelyek.Updated.UjUtonev = true;
            }


            if (!String.IsNullOrEmpty(partner.MothersName))
            {
                krt_Szemelyek.AnyjaNeve = partner.MothersName;
                krt_Szemelyek.Updated.AnyjaNeve = true;

                string[] names = partner.MothersName.Split(' ');

                if (names.Length > 0)
                {
                    krt_Szemelyek.AnyjaNeveCsaladiNev = names[0];
                    krt_Szemelyek.Updated.AnyjaNeveCsaladiNev = true;

                    if (names.Length > 1)
                    {
                        krt_Szemelyek.AnyjaNeveElsoUtonev = names[1];
                        krt_Szemelyek.Updated.AnyjaNeveElsoUtonev = true;

                        if (names.Length > 2)
                        {
                            krt_Szemelyek.AnyjaNeveTovabbiUtonev = names[2];
                            krt_Szemelyek.Updated.AnyjaNeveTovabbiUtonev = true;

                            for (int i = 3; i < names.Length; i++)
                            {
                                krt_Szemelyek.AnyjaNeveTovabbiUtonev += " " + names[i];
                            }
                        }
                    }
                }
            }

            if (!String.IsNullOrEmpty(partner.BirthName))
            {
                krt_Szemelyek.SzuletesiNev = partner.BirthName;
                krt_Szemelyek.Updated.SzuletesiNev = true;

                string[] names = partner.BirthName.Split(' ');

                if (names.Length > 0)
                {
                    krt_Szemelyek.SzuletesiCsaladiNev = names[0];
                    krt_Szemelyek.Updated.SzuletesiCsaladiNev = true;

                    if (names.Length > 1)
                    {
                        krt_Szemelyek.SzuletesiElsoUtonev = names[1];
                        krt_Szemelyek.Updated.SzuletesiElsoUtonev = true;

                        if (names.Length > 2)
                        {
                            krt_Szemelyek.SzuletesiTovabbiUtonev = names[2];
                            krt_Szemelyek.Updated.SzuletesiTovabbiUtonev = true;

                            for (int i = 3; i < names.Length; i++)
                            {
                                krt_Szemelyek.SzuletesiTovabbiUtonev += " " + names[i];
                            }
                        }
                    }
                }
            }


            if (!String.IsNullOrEmpty(partner.BirthPlace))
            {
                krt_Szemelyek.SzuletesiHely = partner.BirthPlace;
                krt_Szemelyek.Updated.SzuletesiHely = true;
            }

            if (partner.BirthDate.HasValue)
            {
                krt_Szemelyek.SzuletesiIdo = partner.BirthDate.ToString();
                krt_Szemelyek.Updated.SzuletesiIdo = true;
            }

            if (!String.IsNullOrEmpty(partner.TaxId))
            {
                krt_Szemelyek.Adoszam = partner.TaxId;
                krt_Szemelyek.Updated.Adoszam = true;
            }

            if (partner.TaxIdentificationNumberSpecified && partner.TaxIdentificationNumber.HasValue)
            {
                krt_Szemelyek.Adoazonosito = partner.TaxIdentificationNumber.ToString();
                krt_Szemelyek.Updated.Adoazonosito = true;
            }

            return krt_Szemelyek;
        }
        public static KRT_Vallalkozasok GetVallalkozasokFromRequestForInsert(this ASP_ADO_PARTNER.PartnerRequest partnerRequest)
        {
            var partner = partnerRequest.Partner;

            KRT_Vallalkozasok krt_Vallalkozasok = new KRT_Vallalkozasok();
            krt_Vallalkozasok.Updated.SetValueAll(false);

            if (!String.IsNullOrEmpty(partner.CompanyRegistrationNumber))
            {
                krt_Vallalkozasok.Cegjegyzekszam = partner.CompanyRegistrationNumber;
                krt_Vallalkozasok.Updated.Cegjegyzekszam = true;
            }

            if (!String.IsNullOrEmpty(partner.TaxId))
            {
                krt_Vallalkozasok.Adoszam = partner.TaxId;
                krt_Vallalkozasok.Updated.Adoszam = true;
            }

            return krt_Vallalkozasok;
        }
        public static KRT_Szemelyek GetSzemelyekFromRequestForUpdate(this ASP_ADO_PARTNER.PartnerRequest partnerRequest, KRT_PartnerSzemelyek krt_Partnerek)
        {
            var partner = partnerRequest.Partner;

            KRT_Szemelyek krt_Szemelyek = krt_Partnerek.Szemelyek;
            krt_Szemelyek.Updated.SetValueAll(false);

            if (partner.Title != null)
            {
                if (krt_Szemelyek.UjTitulis != partner.Title)
                {
                    krt_Szemelyek.UjTitulis = partner.Title;
                    krt_Szemelyek.Updated.UjTitulis = true;
                }
            }

            if (partner.MiddleName != null)
            {
                if (krt_Szemelyek.UjUtonev != partner.MiddleName)
                {
                    krt_Szemelyek.UjUtonev = partner.MiddleName;
                    krt_Szemelyek.Updated.UjUtonev = true;
                }

                if (partner.FirstName != null && partner.FirstName != krt_Szemelyek.UjTovabbiUtonev)
                {
                    krt_Szemelyek.UjTovabbiUtonev = partner.FirstName;
                    krt_Szemelyek.Updated.UjTovabbiUtonev = true;
                }
            }
            else if (partner.FirstName != null && partner.FirstName != krt_Szemelyek.UjUtonev)
            {
                krt_Szemelyek.UjUtonev = partner.FirstName;
                krt_Szemelyek.Updated.UjUtonev = true;
            }

            if (partner.LastName != null && krt_Szemelyek.UjCsaladiNev != partner.LastName)
            {
                krt_Szemelyek.UjCsaladiNev = partner.LastName;
                krt_Szemelyek.Updated.UjCsaladiNev = true;
            }

            if (partner.MothersName != null && krt_Szemelyek.AnyjaNeve != partner.MothersName)
            {
                krt_Szemelyek.AnyjaNeve = partner.MothersName;
                krt_Szemelyek.Updated.AnyjaNeve = true;

                string[] names = partner.MothersName.Split(' ');

                if (names.Length > 0)
                {
                    krt_Szemelyek.AnyjaNeveCsaladiNev = names[0];
                    krt_Szemelyek.Updated.AnyjaNeveCsaladiNev = true;

                    if (names.Length > 1)
                    {
                        krt_Szemelyek.AnyjaNeveElsoUtonev = names[1];
                        krt_Szemelyek.Updated.AnyjaNeveElsoUtonev = true;

                        if (names.Length > 2)
                        {
                            krt_Szemelyek.AnyjaNeveTovabbiUtonev = names[2];
                            krt_Szemelyek.Updated.AnyjaNeveTovabbiUtonev = true;

                            for (int i = 3; i < names.Length; i++)
                            {
                                krt_Szemelyek.AnyjaNeveTovabbiUtonev += " " + names[i];
                            }
                        }
                    }
                }
            }

            if (partner.BirthName != null && krt_Szemelyek.SzuletesiNev != partner.BirthName)
            {
                krt_Szemelyek.SzuletesiNev = partner.BirthName;
                krt_Szemelyek.Updated.SzuletesiNev = true;

                string[] names = partner.BirthName.Split(' ');

                if (names.Length > 0)
                {
                    krt_Szemelyek.SzuletesiCsaladiNev = names[0];
                    krt_Szemelyek.Updated.SzuletesiCsaladiNev = true;

                    if (names.Length > 1)
                    {
                        krt_Szemelyek.SzuletesiElsoUtonev = names[1];
                        krt_Szemelyek.Updated.SzuletesiElsoUtonev = true;

                        if (names.Length > 2)
                        {
                            krt_Szemelyek.SzuletesiTovabbiUtonev = names[2];
                            krt_Szemelyek.Updated.SzuletesiTovabbiUtonev = true;

                            for (int i = 3; i < names.Length; i++)
                            {
                                krt_Szemelyek.SzuletesiTovabbiUtonev += " " + names[i];
                            }
                        }
                    }
                }
            }

            if (partner.BirthPlace != null)
            {
                if (krt_Szemelyek.SzuletesiHely != partner.BirthPlace)
                {
                    krt_Szemelyek.SzuletesiHely = partner.BirthPlace;
                    krt_Szemelyek.Updated.SzuletesiHely = true;
                }
            }

            if (partner.BirthDate.HasValue)
            {
                if (krt_Szemelyek.SzuletesiIdo != partner.BirthDate.Value.ToString())
                {
                    krt_Szemelyek.SzuletesiIdo = partner.BirthDate.ToString();
                    krt_Szemelyek.Updated.SzuletesiIdo = true;
                }
            }

            if (partner.TaxId != null)
            {
                if (krt_Szemelyek.Adoszam != partner.TaxId)
                {
                    krt_Szemelyek.Adoszam = partner.TaxId;
                    krt_Szemelyek.Updated.Adoszam = true;
                }
            }

            if (partner.TaxIdentificationNumberSpecified && partner.TaxIdentificationNumber.HasValue)
            {
                if (krt_Szemelyek.Adoazonosito != partner.TaxIdentificationNumber.ToString())
                {
                    krt_Szemelyek.Adoazonosito = partner.TaxIdentificationNumber.ToString();
                    krt_Szemelyek.Updated.Adoazonosito = true;
                }
            }

            return krt_Szemelyek;
        }
        public static KRT_Vallalkozasok GetVallalkozasokFromRequestForUpdate(this ASP_ADO_PARTNER.PartnerRequest partnerRequest, KRT_PartnerVallalkozasok krt_Partnerek)
        {
            var partner = partnerRequest.Partner;

            KRT_Vallalkozasok krt_Vallalkozasok = krt_Partnerek.Vallalkozasok;
            krt_Vallalkozasok.Updated.SetValueAll(false);

            if (partner.CompanyRegistrationNumber != null)
            {
                if (krt_Vallalkozasok.Cegjegyzekszam != partner.CompanyRegistrationNumber)
                {
                    krt_Vallalkozasok.Cegjegyzekszam = partner.CompanyRegistrationNumber;
                    krt_Vallalkozasok.Updated.Cegjegyzekszam = true;
                }
            }

            if (partner.TaxId != null)
            {
                if (krt_Vallalkozasok.Adoszam != partner.TaxId)
                {
                    krt_Vallalkozasok.Adoszam = partner.TaxId;
                    krt_Vallalkozasok.Updated.Adoszam = true;
                }
            }


            return krt_Vallalkozasok;
        }
        public static void BuildSearchObjectsFromRequest(this ASP_ADO_PARTNER.PartnerRequest partnerRequest, KRT_PartnerekSearch partnerekSearch, KRT_CimekSearch cimekSearch, KRT_SzemelyekSearch szemelyekSearch, KRT_VallalkozasokSearch vallalkozasokSearch, KRT_PartnerCimekSearch partnerCimekSearch)
        {
            if (partnerRequest.Partner != null && partnerRequest.Partner.Addresses != null && partnerRequest.Partner.Addresses.Length > 0)
            {
                partnerRequest.Partner.Addresses.AddValuesWithInOperatorToCimekSearch(cimekSearch, x => x.City, x => x.TelepulesNev);
                partnerRequest.Partner.Addresses.AddValuesWithInOperatorToCimekSearch(cimekSearch, x => x.County, x => x.OrszagNev);
                partnerRequest.Partner.Addresses.AddValuesWithInOperatorToCimekSearch(cimekSearch, x => x.Number, x => x.Hazszam);
                partnerRequest.Partner.Addresses.AddValuesWithInOperatorToCimekSearch(cimekSearch, x => x.Staircase, x => x.Lepcsohaz);
                partnerRequest.Partner.Addresses.AddValuesWithInOperatorToCimekSearch(cimekSearch, x => x.Door, x => x.Ajto);
                partnerRequest.Partner.Addresses.AddValuesWithInOperatorToCimekSearch(cimekSearch, x => x.Floor, x => x.Szint);
                partnerRequest.Partner.Addresses.AddValuesWithInOperatorToCimekSearch(cimekSearch, x => x.Building, x => x.HazszamBetujel);
                partnerRequest.Partner.Addresses.AddValuesWithInOperatorToCimekSearch(cimekSearch, x => x.StreetName, x => x.KozteruletNev);
                partnerRequest.Partner.Addresses.AddValuesWithInOperatorToCimekSearch(cimekSearch, x => x.Zip, x => x.IRSZ);
                partnerRequest.Partner.Addresses.AddValuesWithInOperatorToCimekSearch(cimekSearch, x => x.StreetType.ToString(), x => x.KozteruletTipusNev);

                List<string> addressTypes = new List<string>();

                foreach (ASP_ADO_PARTNER.AddressDto address in partnerRequest.Partner.Addresses)
                {
                    if (address.AddressType == ASP_ADO_PARTNER.EnumsAddressType.Mailing)
                    {
                        addressTypes.Add("05");
                    }
                    else if (address.AddressType == ASP_ADO_PARTNER.EnumsAddressType.Permanent)
                    {
                        addressTypes.Add("03");
                    }
                    else
                    {
                        addressTypes.Add("04");
                    }
                }

                partnerCimekSearch.Fajta.Value = Search.GetSqlInnerString(addressTypes.ToArray());
                partnerCimekSearch.Fajta.Operator = Contentum.eQuery.Query.Operators.inner;
            }

            if (!String.IsNullOrEmpty(partnerRequest.Partner.Email))
            {
                cimekSearch.CimTobbi.Value = partnerRequest.Partner.Email;
                cimekSearch.CimTobbi.Operator = Contentum.eQuery.Query.Operators.like;
            }

            if (partnerRequest.Partner.BirthDate.HasValue)
            {
                szemelyekSearch.SzuletesiIdo.Value = partnerRequest.Partner.BirthDate.ToString();
                szemelyekSearch.SzuletesiIdo.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            if (!String.IsNullOrEmpty(partnerRequest.Partner.BirthName))
            {
                szemelyekSearch.SzuletesiNev.Value = partnerRequest.Partner.BirthName.ToString();
                szemelyekSearch.SzuletesiNev.Operator = Contentum.eQuery.Query.Operators.like;
            }

            if (!String.IsNullOrEmpty(partnerRequest.Partner.BirthPlace))
            {
                szemelyekSearch.SzuletesiHely.Value = partnerRequest.Partner.BirthPlace.ToString();
                szemelyekSearch.SzuletesiHely.Operator = Contentum.eQuery.Query.Operators.like;
            }

            if (!String.IsNullOrEmpty(partnerRequest.Partner.CompanyRegistrationNumber))
            {
                vallalkozasokSearch.Cegjegyzekszam.Value = partnerRequest.Partner.CompanyRegistrationNumber.ToString();
                vallalkozasokSearch.Cegjegyzekszam.Operator = Contentum.eQuery.Query.Operators.like;
            }

            if (!String.IsNullOrEmpty(partnerRequest.Partner.LastName))
            {
                szemelyekSearch.UjCsaladiNev.Value = partnerRequest.Partner.LastName.ToString();
                szemelyekSearch.UjCsaladiNev.Operator = Contentum.eQuery.Query.Operators.like;
            }

            if (!String.IsNullOrEmpty(partnerRequest.Partner.MiddleName))
            {
                szemelyekSearch.UjUtonev.Value = partnerRequest.Partner.MiddleName.ToString();
                szemelyekSearch.UjUtonev.Operator = Contentum.eQuery.Query.Operators.like;

                if (!String.IsNullOrEmpty(partnerRequest.Partner.FirstName))
                {
                    szemelyekSearch.UjTovabbiUtonev.Value = partnerRequest.Partner.FirstName.ToString();
                    szemelyekSearch.UjTovabbiUtonev.Operator = Contentum.eQuery.Query.Operators.like;
                }
            }
            else if (!String.IsNullOrEmpty(partnerRequest.Partner.FirstName))
            {
                szemelyekSearch.UjUtonev.Value = partnerRequest.Partner.FirstName.ToString();
                szemelyekSearch.UjUtonev.Operator = Contentum.eQuery.Query.Operators.like;
            }

            if (!String.IsNullOrEmpty(partnerRequest.Partner.MothersName))
            {
                szemelyekSearch.AnyjaNeve.Value = partnerRequest.Partner.MothersName.ToString();
                szemelyekSearch.AnyjaNeve.Operator = Contentum.eQuery.Query.Operators.like;
            }

            if (!String.IsNullOrEmpty(partnerRequest.Partner.TaxId))
            {
                string TaxId = partnerRequest.Partner.TaxId.ToString();
                TaxId = TaxId.Substring(0, 8) + "%" + TaxId.Substring(8, 1) + "%" + TaxId.Substring(9, 2);

                if (partnerRequest.Partner.BirthDateSpecified && partnerRequest.Partner.BirthDate.HasValue)
                {
                    szemelyekSearch.Adoszam.Value = TaxId;
                    szemelyekSearch.Adoszam.Operator = Contentum.eQuery.Query.Operators.like;
                }
                else
                {
                    vallalkozasokSearch.Adoszam.Value = TaxId;
                    vallalkozasokSearch.Adoszam.Operator = Contentum.eQuery.Query.Operators.like;
                }

            }

            if (partnerRequest.Partner.TaxIdentificationNumberSpecified && partnerRequest.Partner.TaxIdentificationNumber.HasValue)
            {
                szemelyekSearch.Adoazonosito.Value = partnerRequest.Partner.TaxIdentificationNumber.ToString();
                szemelyekSearch.Adoazonosito.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            if (!String.IsNullOrEmpty(partnerRequest.Partner.Title))
            {
                szemelyekSearch.UjTitulis.Value = partnerRequest.Partner.Title.ToString();
                szemelyekSearch.UjTitulis.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            if (!String.IsNullOrEmpty(partnerRequest.Partner.Name))
            {
                partnerekSearch.Nev.Value = partnerRequest.Partner.Name.ToString();
                partnerekSearch.Nev.Operator = Contentum.eQuery.Query.Operators.like;
            }

            if (partnerRequest.Partner.PartnerTypeSpecified && partnerRequest.Partner.PartnerType.HasValue)
            {
                if (partnerRequest.Partner.PartnerType == ASP_ADO_PARTNER.EnumsPartnerType.Company)
                {
                    partnerekSearch.Tipus.Value = KodTarak.Partner_Tipus.Szervezet;
                    //partnerekSearch.Forras.Value = "'C','Z'";
                }
                else
                {
                    partnerekSearch.Tipus.Value = KodTarak.Partner_Tipus.Szemely;
                    //partnerekSearch.Forras.Value = "'O','Z'";
                }
                partnerekSearch.Tipus.Operator = Contentum.eQuery.Query.Operators.equals;
                //partnerekSearch.Forras.Operator = Contentum.eQuery.Query.Operators.inner;
            }
            //else
            //{
            //    partnerekSearch.Forras.Value = "'C','O','Z'";
            //    partnerekSearch.Forras.Operator = Contentum.eQuery.Query.Operators.inner;
            //}
        }

    }
}