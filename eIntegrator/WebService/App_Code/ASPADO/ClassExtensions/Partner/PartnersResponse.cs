﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {


        public static void BuildResponseFromResult(this ASP_ADO_PARTNER.PartnersResponse response, DataTable partnerek)
        {
            var partnerekEnumerable = partnerek.AsEnumerable();
            var distinctPartners = partnerekEnumerable.Distinct(new PartnerComparer()).ToList();
            response.Partners = new ASP_ADO_PARTNER.PartnerDto[distinctPartners.Count];
            response.TotalItemCount = distinctPartners.Count;

            var cimekByPartner = from cim in partnerekEnumerable
                                 group cim by new
                                 {
                                     PartnerId = cim[Constants.TableColumns.Id],
                                     CimId = cim[Constants.TableColumns.CimId]
                                 } into cimGroup
                                 orderby cimGroup.Key.PartnerId
                                 select cimGroup;

            for (int j = 0; j < distinctPartners.Count; j++)
            {
                response.Partners[j] = new ASP_ADO_PARTNER.PartnerDto();

                var partner = distinctPartners[j];

                #region Filling partner fields

                response.Partners[j].PartnerId = partner.GetRowValueAsString(Constants.TableColumns.Id);
                response.Partners[j].BirthName = partner.GetRowValueAsString(Constants.TableColumns.SzuletesiNev);
                response.Partners[j].BirthPlace = partner.GetRowValueAsString(Constants.TableColumns.SzuletesiHely);
                response.Partners[j].CompanyRegistrationNumber = partner.GetRowValueAsString(Constants.TableColumns.Cegjegyzekszam);
                response.Partners[j].IsCancelled = (partner[Constants.TableColumns.IsCancelled].ToString() == "1");
                response.Partners[j].IsCancelledSpecified = true;
                response.Partners[j].MothersName = partner.GetRowValueAsString(Constants.TableColumns.AnyjaNeve);
                response.Partners[j].Name = partner.GetRowValueAsString(Constants.TableColumns.Nev);
                response.Partners[j].TaxId = partner.GetRowValueAsString(Constants.TableColumns.Adoszam);
                response.Partners[j].Title = partner.GetRowValueAsString("UjTitulis");
                response.Partners[j].LastName = partner.GetRowValueAsString(Constants.TableColumns.UjCsaladiNev);

                string utonev = partner.GetRowValueAsString(Constants.TableColumns.UjUtonev);
                string utonev2 = partner.GetRowValueAsString(Constants.TableColumns.UjTovabbiUtonev);
                if (!string.IsNullOrEmpty(utonev2))
                {
                    response.Partners[j].MiddleName = utonev;
                    response.Partners[j].FirstName = utonev2;
                }
                else if (!string.IsNullOrEmpty(utonev))
                {
                    response.Partners[j].FirstName = utonev;
                }

                if (!IsDBNull(partner[Constants.TableColumns.SzuletesiIdo]))
                {
                    DateTime d;
                    if (DateTime.TryParse(partner.GetRowValueAsString(Constants.TableColumns.SzuletesiIdo), out d))
                    {
                        response.Partners[j].BirthDate = d;
                        response.Partners[j].BirthDateSpecified = true;
                    }
                    else
                    {
                        response.Partners[j].ErrorMessage = "Rossz formátumu születési dátum: " + partner.GetRowValueAsString(Constants.TableColumns.SzuletesiIdo);
                    }
                }

                if (!IsDBNull(partner[Constants.TableColumns.Adoazonosito]))
                {
                    long azonosito;
                    if (long.TryParse(partner.GetRowValueAsString(Constants.TableColumns.Adoazonosito), out azonosito))
                    {
                        response.Partners[j].TaxIdentificationNumber = azonosito;
                        response.Partners[j].TaxIdentificationNumberSpecified = true;
                    }
                    else
                    {
                        response.Partners[j].ErrorMessage = "Rossz formátumu adóazonosító: " + partner.GetRowValueAsString(Constants.TableColumns.Adoazonosito);
                    }
                }

                if (!IsDBNull(partner["Tipus"]))
                {
                    if (partner["Tipus"].ToString() == "10")
                    {
                        response.Partners[j].PartnerType = ASP_ADO_PARTNER.EnumsPartnerType.Company;
                        response.Partners[j].PartnerTypeSpecified = true;
                    }
                    else if (partner["Tipus"].ToString() == "20")
                    {
                        response.Partners[j].PartnerType = ASP_ADO_PARTNER.EnumsPartnerType.Individual;
                        response.Partners[j].PartnerTypeSpecified = true;
                    }
                }

                #endregion

                #region Filling partners addresses fields

                var partnerCimek = cimekByPartner.Where(x => x.Key.PartnerId.ToString() == partner[Constants.TableColumns.Id].ToString()).AsEnumerable();

                if (partnerCimek != null)
                {
                    List<ASP_ADO_PARTNER.AddressDto> addressList = new List<ASP_ADO_PARTNER.AddressDto>();

                    foreach (var groupCim in partnerCimek)
                    {
                        var cim = groupCim.FirstOrDefault();

                        ASP_ADO_PARTNER.AddressDto c = new ASP_ADO_PARTNER.AddressDto();

                        if (!IsDBNull(cim["CimTipus"]) && cim["CimTipus"].ToString() == KodTarak.Cim_Tipus.Email)
                        {
                            if (string.IsNullOrEmpty(response.Partners[j].Email))
                            {
                                response.Partners[j].Email = cim.GetRowValueAsString("CimTobbi");
                            }
                        }
                        else
                        {
                            addressList.Add(c);

                            c.Id = long.Parse(cim["CimIdASPAdo"].ToString());
                            c.IdSpecified = true;
                            c.City = cim.GetRowValueAsString("TelepulesNev");
                            c.CountryCode = cim.GetRowValueAsString("OrszagKod");
                            c.Number = cim.GetRowValueAsString("Hazszam");
                            c.Staircase = cim.GetRowValueAsString("Lepcsohaz");
                            c.Floor = cim.GetRowValueAsString("Szint");
                            c.Building = cim.GetRowValueAsString("HazszamBetujel");
                            c.ParcelNumber = cim.GetRowValueAsString("HRSZ");
                            c.Door = cim.GetRowValueAsString("Ajto");
                            c.StreetName = cim.GetRowValueAsString("KozteruletNev");
                            c.Zip = cim.GetRowValueAsString("IRSZ");

                            if (!IsDBNull(cim["KozteruletTipusNev"]))
                            {
                                c.StreetType = cim["KozteruletTipusNev"].ToString();
                            }
                        }
                    }
                    response.Partners[j].Addresses = addressList.ToArray();
                }

                #endregion

            }

            response.IsSucceeded = true;
        }
    }
}