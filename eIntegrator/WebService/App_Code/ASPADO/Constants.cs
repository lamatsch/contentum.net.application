﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using ASP_ADO_PARTNER = Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using ASP_ADO_DOCUMENT = Contentum.eIntegrator.Service.INT_ASPADO.Document;

namespace Contentum.eIntegrator.Service.INT_ASPADO
{
    public static partial class ASPADOHelper
    {
        #region Constants

        public static class Constants
        {
            public static class Messages
            {
                public const string SoapRequestError = "Could not parse SOAP request";
                public const string UserError = "User not found";
                public const string PartnerCreateFailedError = "Failed to create partner";
                public const string PartnerNotFoundError = "Partner not found";
                public const string PartnersNotFoundError = "No Partners found";
                public const string PartnerIdEmptyError = "PartnerId cannot be empty";
                public const string PartnerIdInvalidError = "PartnerId is not valid integer";

                public const string AddressCreateFailedError = "Failed to create address";
                public const string AddressAssociationFailedError = "Failed to associate address with partner";

                public const string EmailCreateFailedError = "Failed to insert email address";
                public const string EmailAssociationFailedError = "Failed to associate email address with partner";

                public const string DocumentsNotFoundError = "No Documents found";

                public const string OrganizationUnitsNotFoundError = "No OrganizationUnits found";
                public const string RegistrationBooksNotFoundError = "No RegistrationBooks found";
                public const string InsertAlairoError = "Nem sikerült az aláíró hozzáadása ";
            }

            public static class Parameters
            {
                public static string ASPADOTenant = "821ff66d-97ed-4e33-a3b6-8c96cfcc562d";
                public const string AdminUser = "54e861a5-36ed-44ca-baa7-c287d125b309";

                public const string DocumentTypeVezerloId = "ASPADO_DocumentType";
                public const string IratTipusVezerloId = "IRATTIPUS";

                public const string DefaultExpedialasCodeVezerloId = "ASPADO_DefaultExpedialasCode";
                public const string KuldemenyKuldesModjaVezerloId = "KULDEMENY_KULDES_MODJA";

                public const string TaxTypeVezerloId = "ASPADO_TaxType";
                public const string UgyTipusVezerloId = "ASPADO_UgyTipus";

                public const string ReceivingInformationVezerloId = "ASPADO_ReceivingInformation";
                public const string TertivevenyVezerloId = "TERTIVEVENY_VISSZA_KOD";

                public const string IktatoKonyvAzonositoHossz = "IKTATOKONYV_AZONOSITO_HOSSZ";

                public static string FelhasznaloId = "54e861a5-36ed-44ca-baa7-c287d125b309";
            }
            public static class AppSettings
            {
                public static string ADOTenant = "ADO_Tenant";
                public static string ADOAdresseAddress = "ADO_AddresseeAddress";
                public static string ADOSenderSettings = "ADO_SenderAddress";
            }

            public static class TableColumns
            {
                public static string TertivisszaDat = "TertivisszaDat";
                public static string TertivisszaKod = "TertivisszaKod";
                public static string Id = "Id";
                public static string CimId = "CimId";
                public static string SzuletesiNev = "SzuletesiNev";
                public static string SzuletesiHely = "SzuletesiHely";
                public static string Cegjegyzekszam = "Cegjegyzekszam";
                public static string UjUtonev = "UjUtonev";
                public static string IsCancelled = "IsCancelled";
                public static string UjCsaladiNev = "UjCsaladiNev";
                public static string UjTovabbiUtonev = "UjTovabbiUtonev";
                public static string AnyjaNeve = "AnyjaNeve";
                public static string Nev = "Nev";
                public static string Adoszam = "Adoszam";
                public static string SzuletesiIdo = "SzuletesiIdo";
                public static string Adoazonosito = "Adoazonosito";

                public static string AtvetelDat = "AtvetelDat";
                public static string KezbVelelemDatuma = "KezbVelelemDatuma";
                public static string Ragszam = "Ragszam";
                public static string Csoport_ID_Cimzett = "Csoport_ID_Cimzett";
                public static string AtvevoSzemely = "AtvevoSzemely";
                public static string Allapot = "Allapot";
                public static string NevSTR_Bekuldo = "NevSTR_Bekuldo";
                public static string Cim_Id = "Cim_Id";
            }
        }
        #endregion
    }
}