﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.9151
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.3038.
// 
namespace Contentum.eIntegrator.Service.INT_ASPADO.Partner {
    using System.Xml.Serialization;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Diagnostics;
       
     
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [WebService(Namespace = "http://tempuri.org/IPartnerWebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseDto))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BasePartnerDto))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseRequest))]
    public interface IINT_ASPADO_PartnerService
    {

        [LogExtension]
        [System.Web.Services.WebMethodAttribute()]
        [SoapHeader("headers", Direction = SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IPartnerWebService/GetPartners", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        PartnersResponse GetPartners([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] PartnerRequest partnerRequest, int pageNumber, [System.Xml.Serialization.XmlIgnoreAttribute()] bool pageNumberSpecified, int pageSize, [System.Xml.Serialization.XmlIgnoreAttribute()] bool pageSizeSpecified);

        [LogExtension]
        [System.Web.Services.WebMethodAttribute()]
        [SoapHeader("headers", Direction = SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IPartnerWebService/GetPartner", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        PartnerResponse GetPartner([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] PartnerIdRequest partnerIdRequest);

        [LogExtension]
        [System.Web.Services.WebMethodAttribute()]
        [SoapHeader("headers", Direction = SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IPartnerWebService/CreatePartner", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        PartnerResponse CreatePartner([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] PartnerRequest partnerRequest);

        [LogExtension]
        [System.Web.Services.WebMethodAttribute()]
        [SoapHeader("headers", Direction = SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IPartnerWebService/ModifyPartner", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        BaseResponse ModifyPartner([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] PartnerRequest partnerRequest);

        [LogExtension]
        [System.Web.Services.WebMethodAttribute()]
        [SoapHeader("headers", Direction = SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IPartnerWebService/DeletePartnerId", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        BaseResponse DeletePartnerId([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] PartnerIdRequest partnerIdRequest);

        [LogExtension]
        [System.Web.Services.WebMethodAttribute()]
        [SoapHeader("headers", Direction = SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IPartnerWebService/MergePartner", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        PartnerResponse MergePartner([System.Xml.Serialization.XmlElementAttribute(IsNullable=true)] MergePartnerRequest mergePartnerRequest);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/PartnerWebService")]
    public partial class PartnerRequest : BaseRequest {
        
        private PartnerDto partnerField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public PartnerDto Partner {
            get {
                return this.partnerField;
            }
            set {
                this.partnerField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/Mira.Common.DataTransferObjects")]
    public partial class PartnerDto : BasePartnerDto {
        
        private string aDOPartnerIdField;
        
        private string accountNumberField;
        
        private AddressDto[] addressesField;
        
        private string cardNumberField;
        
        private string commentField;
        
        private string customerGateIdField;
        
        private string eCustomerIdField;
        
        private string emailField;
        
        private string errorMessageField;
        
        private string iBANPrefixField;
        
        private bool isCancelledField;
        
        private bool isCancelledFieldSpecified;
        
        private string postField;
        
        private string statisticNumberField;
        
        private string taxIdField;
        
        private System.Nullable<long> taxIdentificationNumberField;
        
        private bool taxIdentificationNumberFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ADOPartnerId {
            get {
                return this.aDOPartnerIdField;
            }
            set {
                this.aDOPartnerIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string AccountNumber {
            get {
                return this.accountNumberField;
            }
            set {
                this.accountNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable=true)]
        public AddressDto[] Addresses {
            get {
                return this.addressesField;
            }
            set {
                this.addressesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CardNumber {
            get {
                return this.cardNumberField;
            }
            set {
                this.cardNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Comment {
            get {
                return this.commentField;
            }
            set {
                this.commentField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CustomerGateId {
            get {
                return this.customerGateIdField;
            }
            set {
                this.customerGateIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ECustomerId {
            get {
                return this.eCustomerIdField;
            }
            set {
                this.eCustomerIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Email {
            get {
                return this.emailField;
            }
            set {
                this.emailField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ErrorMessage {
            get {
                return this.errorMessageField;
            }
            set {
                this.errorMessageField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string IBANPrefix {
            get {
                return this.iBANPrefixField;
            }
            set {
                this.iBANPrefixField = value;
            }
        }
        
        /// <remarks/>
        public bool IsCancelled {
            get {
                return this.isCancelledField;
            }
            set {
                this.isCancelledField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsCancelledSpecified {
            get {
                return this.isCancelledFieldSpecified;
            }
            set {
                this.isCancelledFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Post {
            get {
                return this.postField;
            }
            set {
                this.postField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string StatisticNumber {
            get {
                return this.statisticNumberField;
            }
            set {
                this.statisticNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string TaxId {
            get {
                return this.taxIdField;
            }
            set {
                this.taxIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<long> TaxIdentificationNumber {
            get {
                return this.taxIdentificationNumberField;
            }
            set {
                this.taxIdentificationNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TaxIdentificationNumberSpecified {
            get {
                return this.taxIdentificationNumberFieldSpecified;
            }
            set {
                this.taxIdentificationNumberFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/Mira.Common.DataTransferObjects")]
    public partial class AddressDto : BaseDto {
        
        private EnumsAddressType addressTypeField;
        
        private bool addressTypeFieldSpecified;
        
        private string buildingField;
        
        private string cityField;
        
        private string countryCodeField;
        
        private string countyField;
        
        private string doorField;
        
        private string floorField;
        
        private string numberField;
        
        private string parcelNumberField;
        
        private PartnerDto partnerField;
        
        private string staircaseField;
        
        private string streetNameField;
        
        private string streetTypeField;
        
        private string zipField;
        
        /// <remarks/>
        public EnumsAddressType AddressType {
            get {
                return this.addressTypeField;
            }
            set {
                this.addressTypeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AddressTypeSpecified {
            get {
                return this.addressTypeFieldSpecified;
            }
            set {
                this.addressTypeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Building {
            get {
                return this.buildingField;
            }
            set {
                this.buildingField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string City {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CountryCode {
            get {
                return this.countryCodeField;
            }
            set {
                this.countryCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string County {
            get {
                return this.countyField;
            }
            set {
                this.countyField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Door {
            get {
                return this.doorField;
            }
            set {
                this.doorField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Floor {
            get {
                return this.floorField;
            }
            set {
                this.floorField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ParcelNumber {
            get {
                return this.parcelNumberField;
            }
            set {
                this.parcelNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public PartnerDto Partner {
            get {
                return this.partnerField;
            }
            set {
                this.partnerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Staircase {
            get {
                return this.staircaseField;
            }
            set {
                this.staircaseField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string StreetName {
            get {
                return this.streetNameField;
            }
            set {
                this.streetNameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string StreetType {
            get {
                return this.streetTypeField;
            }
            set {
                this.streetTypeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Zip {
            get {
                return this.zipField;
            }
            set {
                this.zipField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(TypeName="Enums.AddressType", Namespace="http://schemas.datacontract.org/2004/07/Mira.Common")]
    public enum EnumsAddressType {
        
        /// <remarks/>
        Permanent,
        
        /// <remarks/>
        Temporary,
        
        /// <remarks/>
        Mailing,
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AddressDto))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/Mira.Common.DataTransferObjects")]
    public partial class BaseDto {
        
        private long idField;
        
        private bool idFieldSpecified;
        
        /// <remarks/>
        public long Id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdSpecified {
            get {
                return this.idFieldSpecified;
            }
            set {
                this.idFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartnerResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartnersResponse))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/PartnerWebService")]
    public partial class BaseResponse : ASPADOHelper.IBaseResponse
    {
        
        private string errorMessageField;
        
        private bool isSucceededField;
        
        private bool isSucceededFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string ErrorMessage {
            get {
                return this.errorMessageField;
            }
            set {
                this.errorMessageField = value;
            }
        }
        
        /// <remarks/>
        public bool IsSucceeded {
            get {
                return this.isSucceededField;
            }
            set {
                this.isSucceededField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsSucceededSpecified {
            get {
                return this.isSucceededFieldSpecified;
            }
            set {
                this.isSucceededFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/PartnerWebService")]
    public partial class PartnerResponse : BaseResponse {
        
        private PartnerDto partnerField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public PartnerDto Partner {
            get {
                return this.partnerField;
            }
            set {
                this.partnerField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/PartnerWebService")]
    public partial class PartnersResponse : BaseResponse {
        
        private PartnerDto[] partnersField;
        
        private int totalItemCountField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable=true)]
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.datacontract.org/2004/07/Mira.Common.DataTransferObjects")]
        public PartnerDto[] Partners {
            get {
                return this.partnersField;
            }
            set {
                this.partnersField = value;
            }
        }
        
        /// <remarks/>
        public int TotalItemCount {
            get {
                return this.totalItemCountField;
            }
            set {
                this.totalItemCountField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartnerDto))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/Mira.Common.DataTransferObjects")]
    public partial class BasePartnerDto {
        
        private System.Nullable<System.DateTime> birthDateField;
        
        private bool birthDateFieldSpecified;
        
        private string birthNameField;
        
        private string birthPlaceField;
        
        private string companyRegistrationNumberField;
        
        private System.Nullable<System.DateTime> creationDateField;
        
        private bool creationDateFieldSpecified;
        
        private string firstNameField;
        
        private string lastNameField;
        
        private string middleNameField;
        
        private System.Nullable<System.DateTime> modificationDateField;
        
        private bool modificationDateFieldSpecified;
        
        private string mothersNameField;
        
        private string nameField;
        
        private string partnerIdField;
        
        private System.Nullable<EnumsPartnerType> partnerTypeField;
        
        private bool partnerTypeFieldSpecified;
        
        private string titleField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> BirthDate {
            get {
                return this.birthDateField;
            }
            set {
                this.birthDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BirthDateSpecified {
            get {
                return this.birthDateFieldSpecified;
            }
            set {
                this.birthDateFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string BirthName {
            get {
                return this.birthNameField;
            }
            set {
                this.birthNameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string BirthPlace {
            get {
                return this.birthPlaceField;
            }
            set {
                this.birthPlaceField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string CompanyRegistrationNumber {
            get {
                return this.companyRegistrationNumberField;
            }
            set {
                this.companyRegistrationNumberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> CreationDate {
            get {
                return this.creationDateField;
            }
            set {
                this.creationDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreationDateSpecified {
            get {
                return this.creationDateFieldSpecified;
            }
            set {
                this.creationDateFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string FirstName {
            get {
                return this.firstNameField;
            }
            set {
                this.firstNameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string LastName {
            get {
                return this.lastNameField;
            }
            set {
                this.lastNameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string MiddleName {
            get {
                return this.middleNameField;
            }
            set {
                this.middleNameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> ModificationDate {
            get {
                return this.modificationDateField;
            }
            set {
                this.modificationDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ModificationDateSpecified {
            get {
                return this.modificationDateFieldSpecified;
            }
            set {
                this.modificationDateFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string MothersName {
            get {
                return this.mothersNameField;
            }
            set {
                this.mothersNameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string PartnerId {
            get {
                return this.partnerIdField;
            }
            set {
                this.partnerIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<EnumsPartnerType> PartnerType {
            get {
                return this.partnerTypeField;
            }
            set {
                this.partnerTypeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PartnerTypeSpecified {
            get {
                return this.partnerTypeFieldSpecified;
            }
            set {
                this.partnerTypeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string Title {
            get {
                return this.titleField;
            }
            set {
                this.titleField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.FlagsAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(TypeName="Enums.PartnerType", Namespace="http://schemas.datacontract.org/2004/07/Mira.Common")]
    public enum EnumsPartnerType {
        
        /// <remarks/>
        Individual = 1,
        
        /// <remarks/>
        Company = 2,
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(MergePartnerRequest))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartnerIdRequest))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PartnerRequest))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/PartnerWebService")]
    public partial class BaseRequest {
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/PartnerWebService")]
    public partial class MergePartnerRequest : BaseRequest {
        
        private long activePartnerField;
        
        private bool activePartnerFieldSpecified;
        
        private PartnerDto[] partnersToMergeField;
        
        /// <remarks/>
        public long ActivePartner {
            get {
                return this.activePartnerField;
            }
            set {
                this.activePartnerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ActivePartnerSpecified {
            get {
                return this.activePartnerFieldSpecified;
            }
            set {
                this.activePartnerFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable=true)]
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.datacontract.org/2004/07/Mira.Common.DataTransferObjects")]
        public PartnerDto[] PartnersToMerge {
            get {
                return this.partnersToMergeField;
            }
            set {
                this.partnersToMergeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.datacontract.org/2004/07/PartnerWebService")]
    public partial class PartnerIdRequest : BaseRequest {
        
        private string partnerIdField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string PartnerId {
            get {
                return this.partnerIdField;
            }
            set {
                this.partnerIdField = value;
            }
        }
    }
}
