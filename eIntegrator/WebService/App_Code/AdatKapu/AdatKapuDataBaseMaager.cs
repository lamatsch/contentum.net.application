﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.Utility;
using System.Configuration;
namespace Contentum.eIntegrator.AdatKapu.PartnerSync
{
    /// <summary>
    /// Summary description for AdatKapuDataBaseMaager
    /// </summary>
    public partial class AdatKapuDataBaseManager
    {
        public ExecParam ExecParamContentum { get; set; }

        public AdatKapuDataBaseManager(ExecParam execParameter)
        {
            ExecParamContentum = execParameter;
            InitAdatKapuPartnerekSchemaFromParameterek();
        }
        public AdatKapuDataBaseManager(string felhasznaloId, string FelhasznaloSzervezetId)
        {
            ExecParamContentum = new ExecParam();
            ExecParamContentum.Felhasznalo_Id = felhasznaloId;
            ExecParamContentum.FelhasznaloSzervezet_Id = FelhasznaloSzervezetId;
            InitAdatKapuPartnerekSchemaFromParameterek();
        }
        public static string GetConnectionString()
        {
            ConnectionStringSettings conn = new ConnectionStringSettings();
            try
            {
                conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ADATKAPUDConnectionString"];
            }
            catch (System.Exception exc)
            {
                Logger.Error("AdatKapuDataBaseManager.GetConnectionString.ADATKAPUDConnectionString", exc);
                throw;
            }
          
            return conn.ConnectionString;
        }
    }
}