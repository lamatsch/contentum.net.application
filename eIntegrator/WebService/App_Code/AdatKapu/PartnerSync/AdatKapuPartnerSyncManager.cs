﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Contentum.eIntegrator.AdatKapu.PartnerSync
{
    /// <summary>
    /// Summary description for AdatKapuPartnerSyncManager
    /// </summary>
    public class AdatKapuPartnerSyncManager
    {
        private readonly string Version = "V2018.06.06 12:00";
        private List<AdatkapuPartnerModel> PartnerModels { get; set; }
        private AdatKapuDataBaseManager DBManagerContentum { get; set; }
        public AdatKapuPartnerSyncManager(ExecParam execParameter)
        {
            PartnerModels = new List<AdatkapuPartnerModel>();
            DBManagerContentum = new AdatKapuDataBaseManager(execParameter);
            LoadAdatKapuPartnerekFromOracle();
        }
        public AdatKapuPartnerSyncManager(string felhasznaloId, string FelhasznaloSzervezetId)
        {
            PartnerModels = new List<AdatkapuPartnerModel>();
            DBManagerContentum = new AdatKapuDataBaseManager(felhasznaloId, FelhasznaloSzervezetId);
            LoadAdatKapuPartnerekFromOracle();
        }
        private void LoadAdatKapuPartnerekFromOracle()
        {
            DataSet ds = DBManagerContentum.GetPartnerAdatok();

            if (ds == null || ds.Tables.Count < 1 || ds.Tables[0].Rows.Count < 1)
            {
                eUtility.Logger.Info("AK.LoadAdatKapuPartnerekFromOracle Nincsenek adatsorok!");
                return;
            }
            eUtility.Logger.Info("AK.LoadAdatKapuPartnerekFromOracle Adatsorok száma:" + ds.Tables[0].Rows.Count);
            AdatkapuPartnerModel tmp = null;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                tmp = new AdatkapuPartnerModel();
                tmp.company_name = item["company_name"].ToString().Trim();
                tmp.company_hirid = item["company_hirid"].ToString().Trim();
                tmp.company_fmsid = item["company_fmsid"].ToString().Trim();
                tmp.partner_name = item["partner_name"].ToString().Trim();
                tmp.partner_email = item["partner_email"].ToString().Trim();
                tmp.partner_contactcode = item["partner_contactcode"].ToString().Trim();
                PartnerModels.Add(tmp);
            }
        }
        /// <summary>
        /// StartPartnerSync
        /// </summary>
        /// <returns></returns>
        public bool StartPartnerSynchronization()
        {
            Logger.Info("AK.PARTNER.SYNC.START " + Version);
            if (PartnerModels == null || PartnerModels.Count < 1)
                return false;
            Dictionary<string, string> existingMainPartnerIds = new Dictionary<string, string>();
            Dictionary<string, string> existingSubPartnerIds = new Dictionary<string, string>();
            Dictionary<string, string> existingMainHIRCimIds = new Dictionary<string, string>();
            Dictionary<string, string> existingMainFMSCimIds = new Dictionary<string, string>();
            Dictionary<string, string> existingSubCimIds = new Dictionary<string, string>();
            Dictionary<string, List<string>> existingPartnerCimekIds = new Dictionary<string, List<string>>();

            for (int i = 0; i < PartnerModels.Count; i++)
            {
                #region MAIN PARTNER
                if (string.IsNullOrEmpty(PartnerModels[i].company_name))
                    continue;

                string mainPartnerId = null;

                try
                {
                    Result foundMatch = DBManagerContentum.FindPartnerSimple(PartnerModels[i].company_name);
                    if (foundMatch == null || foundMatch.Ds.Tables.Count < 1 || foundMatch.Ds.Tables[0].Rows.Count < 1)
                    {
                        #region ADD
                        mainPartnerId = System.Guid.NewGuid().ToString();
                        Result resultMainPartnerAdd = DBManagerContentum.AddMainPartnerSimple(PartnerModels[i], mainPartnerId);
                        PartnerModels[i].CompanyId = mainPartnerId;
                        Logger.Info("AK.MP.NEW   = " + PartnerModels[i].company_name);
                        #endregion
                    }
                    else
                    {
                        mainPartnerId = foundMatch.Ds.Tables[0].Rows[0]["Id"].ToString();
                        PartnerModels[i].CompanyId = mainPartnerId;
                        Logger.Info("AK.MP.FOUND= " + PartnerModels[i].company_name);
                    }
                    AddToCimekHistory(ref existingMainPartnerIds, mainPartnerId, PartnerModels[i].company_name);
                }
                catch (ResultException exc)
                {
                    Logger.Error(string.Format("AK.MP.ADDORGET.ERROR= {0} {1}", exc.ErrorCode, exc.ErrorMessage), exc);
                }
                catch (System.Exception exc)
                {
                    Logger.Error("AK.MP.ADDORGET.ERROR= " + exc.Message + exc.StackTrace, exc);
                }
                #endregion

                #region SUB PARTNER
                if (string.IsNullOrEmpty(PartnerModels[i].partner_name))
                    continue;

                string subPartnerId = null;
                if (string.IsNullOrEmpty(PartnerModels[i].partner_contactcode))
                {
                    try
                    {
                        Result foundSubPartnerMatch = DBManagerContentum.FindPartnerSimple(PartnerModels[i].partner_name);
                        if (foundSubPartnerMatch == null || foundSubPartnerMatch.Ds.Tables.Count < 1 || foundSubPartnerMatch.Ds.Tables[0].Rows.Count < 1)
                        {
                            #region ADD
                            subPartnerId = System.Guid.NewGuid().ToString();
                            DBManagerContentum.AddSubPartner(PartnerModels[i], mainPartnerId, subPartnerId);
                            PartnerModels[i].PartnerId = subPartnerId;
                            Logger.Info("AK.SP.WCC.NEW   = " + PartnerModels[i].partner_name);
                            #endregion
                        }
                        else
                        {
                            subPartnerId = foundSubPartnerMatch.Ds.Tables[0].Rows[0]["Id"].ToString();
                            PartnerModels[i].PartnerId = subPartnerId;
                            DBManagerContentum.AddPartnerkapcsolat(mainPartnerId, subPartnerId, PartnerModels[i].partner_name);

                            #region SZEMELY
                            KRT_Szemelyek szemely = DBManagerContentum.GenerateSzemely(PartnerModels[i], subPartnerId);
                            if (szemely != null)
                            {
                                Result resultFindSzemely = DBManagerContentum.FindSzemelyPartner(szemely);
                                if (resultFindSzemely == null || resultFindSzemely.Ds.Tables[0].Rows.Count < 1)
                                {
                                    Logger.Info("AK.SP.WCC.ADDSZEMELY= " + szemely.UjCsaladiNev + string.Empty + szemely.UjUtonev + string.Empty + szemely.UjTovabbiUtonev);
                                    DBManagerContentum.AddSzemely(szemely);
                                }
                            }
                            #endregion
                            Logger.Info("AK.SP.WCC.FOUND= " + PartnerModels[i].partner_name);
                        }

                        AddToCimekHistory(ref existingMainPartnerIds, subPartnerId, PartnerModels[i].partner_name);
                    }
                    catch (ResultException exc)
                    {
                        Logger.Error(string.Format("AK.SP.WCC.ADDORGET.ERROR= {0} {1}", exc.ErrorCode, exc.ErrorMessage), exc);
                    }
                    catch (System.Exception exc)
                    {
                        Logger.Error("AK.SP.WCC.ADDORGET.ERROR= " + exc.Message + exc.StackTrace, exc);
                    }
                }
                else
                {
                    try
                    {
                        Result foundSubPartnerMatch = DBManagerContentum.FindPartnerSimple(PartnerModels[i].partner_name);
                        if (foundSubPartnerMatch == null || foundSubPartnerMatch.Ds.Tables.Count < 1 || foundSubPartnerMatch.Ds.Tables[0].Rows.Count < 1)
                        {
                            #region ADD
                            subPartnerId = System.Guid.NewGuid().ToString();
                            DBManagerContentum.AddSubPartner(PartnerModels[i], mainPartnerId, subPartnerId);
                            PartnerModels[i].PartnerId = subPartnerId;
                            Logger.Info("AK.SP.NEW   = " + PartnerModels[i].partner_name);
                            #endregion
                        }
                        else
                        {
                            subPartnerId = foundSubPartnerMatch.Ds.Tables[0].Rows[0]["Id"].ToString();
                            PartnerModels[i].PartnerId = subPartnerId;
                            DBManagerContentum.AddPartnerkapcsolat(mainPartnerId, subPartnerId, PartnerModels[i].partner_name);

                            #region SZEMELY
                            KRT_Szemelyek szemely = DBManagerContentum.GenerateSzemely(PartnerModels[i], subPartnerId);
                            if (szemely != null)
                            {
                                Result resultFindSzemely = DBManagerContentum.FindSzemelyPartner(szemely);
                                if (resultFindSzemely == null || resultFindSzemely.Ds.Tables[0].Rows.Count < 1)
                                {
                                    Logger.Info("AK.SP.ADDSZEMELY= " + szemely.UjCsaladiNev + string.Empty + szemely.UjUtonev + string.Empty + szemely.UjTovabbiUtonev);
                                    DBManagerContentum.AddSzemely(szemely);
                                }
                            }
                            #endregion
                            Logger.Info("AK.SP.FOUND= " + PartnerModels[i].partner_name);
                        }

                        AddToCimekHistory(ref existingMainPartnerIds, subPartnerId, PartnerModels[i].partner_name);
                    }
                    catch (ResultException exc)
                    {
                        Logger.Error(string.Format("AK.SP.ADDORGET.ERROR= {0} {1}", exc.ErrorCode, exc.ErrorMessage), exc);
                    }
                    catch (System.Exception exc)
                    {
                        Logger.Error("AK.SP.ADDORGET.ERROR= " + exc.Message + exc.StackTrace, exc);
                    }
                }
                #endregion

                if (!string.IsNullOrEmpty(PartnerModels[i].CompanyId))
                {
                    #region MAIN PARTNER CIM - FMSID
                    if (!string.IsNullOrEmpty(PartnerModels[i].company_fmsid))
                    {
                        string outCimId = null;
                        try
                        {
                            AddOrSetCimAndPartnerCim(PartnerModels[i], mainPartnerId,
                            EnumCimForras.FMSID,
                            EnumMainSubPartner.MAIN,
                            ref existingMainPartnerIds,
                            ref existingSubPartnerIds,
                            ref existingMainHIRCimIds,
                            ref existingMainFMSCimIds,
                            ref existingSubCimIds,
                            ref existingPartnerCimekIds,
                            out outCimId);
                        }
                        catch (System.Exception exc)
                        {
                            Logger.Error("AddOrSetCimAndPartnerCim= " + exc.Message, exc);
                        }
                    }
                    #endregion

                    #region MAIN PARTNER CIM - HIRID
                    if (!string.IsNullOrEmpty(PartnerModels[i].company_hirid))
                    {
                        string outCimId = null;
                        try
                        {
                            AddOrSetCimAndPartnerCim(PartnerModels[i], mainPartnerId,
                            EnumCimForras.HIRID,
                            EnumMainSubPartner.MAIN,
                            ref existingMainPartnerIds,
                            ref existingSubPartnerIds,
                            ref existingMainHIRCimIds,
                            ref existingMainFMSCimIds,
                            ref existingSubCimIds,
                            ref existingPartnerCimekIds,
                            out outCimId);
                        }
                        catch (System.Exception exc)
                        {
                            Logger.Error("AddOrSetCimAndPartnerCim= " + exc.Message, exc);
                        }
                    }
                    #endregion
                }

                if (!string.IsNullOrEmpty(PartnerModels[i].PartnerId) && !string.IsNullOrEmpty(PartnerModels[i].partner_email))
                {
                    string outCimId = null;
                    try
                    {
                        AddOrSetCimAndPartnerCim(PartnerModels[i], subPartnerId,
                           EnumCimForras.CONTACTCODE,
                           EnumMainSubPartner.SUB,
                           ref existingMainPartnerIds,
                           ref existingSubPartnerIds,
                           ref existingMainHIRCimIds,
                           ref existingMainFMSCimIds,
                           ref existingSubCimIds,
                           ref existingPartnerCimekIds,
                           out outCimId);

                        if (PartnerModels[i] != null && !string.IsNullOrEmpty(PartnerModels[i].partner_contactcode))
                            AddOrSetCimAndPartnerCim(PartnerModels[i], subPartnerId,
                             EnumCimForras.SECRET_CONTACT_CODE,
                             EnumMainSubPartner.SUB,
                             ref existingMainPartnerIds,
                             ref existingSubPartnerIds,
                             ref existingMainHIRCimIds,
                             ref existingMainFMSCimIds,
                             ref existingSubCimIds,
                             ref existingPartnerCimekIds,
                             out outCimId);
                    }
                    catch (System.Exception exc)
                    {
                        Logger.Error("AddOrSetCimAndPartnerCim= " + exc.Message, exc);
                    }
                }
            }

            try
            {
                Invalidate(existingMainPartnerIds, existingSubPartnerIds, existingMainHIRCimIds, existingMainFMSCimIds, existingSubCimIds, existingPartnerCimekIds);
            }
            catch (ResultException exc)
            {
                Logger.Error(string.Format("AK.MP.INVALIDATE.ERROR= {0} {1}", exc.ErrorCode, exc.ErrorMessage), exc);
            }
            catch (System.Exception exc)
            {
                Logger.Error("AK.MP.INVALIDATE.ERROR= " + exc.Message, exc);
            }
            Logger.Info("AK.PARTNER.SYNC.STOP");
            return true;
        }

        /// <summary>
        /// AddOrSetCimAndPartnerCim
        /// </summary>
        /// <param name="model"></param>
        /// <param name="partnerId"></param>
        /// <param name="forras"></param>
        /// <param name="partnerType"></param>
        /// <param name="existingMainPartnerIds"></param>
        /// <param name="existingSubPartnerIds"></param>
        /// <param name="existingMainHIRCimIds"></param>
        /// <param name="existingMainFMSCimIds"></param>
        /// <param name="existingSubCimIds"></param>
        /// <param name="outCimId"></param>
        /// <returns></returns>
        private bool AddOrSetCimAndPartnerCim(AdatkapuPartnerModel model, string partnerId,
                    EnumCimForras forras,
                    EnumMainSubPartner partnerType,
                    ref Dictionary<string, string> existingMainPartnerIds,
                    ref Dictionary<string, string> existingSubPartnerIds,
                    ref Dictionary<string, string> existingMainHIRCimIds,
                    ref Dictionary<string, string> existingMainFMSCimIds,
                    ref Dictionary<string, string> existingSubCimIds,
                    ref Dictionary<string, List<string>> existingPartnerCimekIds,
                    out string outCimId)
        {
            outCimId = null;
            try
            {
                KRT_Cimek cim = DBManagerContentum.GenerateCim(model, forras);
                Result resultFindCim = DBManagerContentum.FindCim(cim);
                if (resultFindCim == null || resultFindCim.Ds.Tables[0].Rows.Count < 1)
                {
                    #region LOG
                    switch (forras)
                    {
                        case EnumCimForras.HIRID:
                            Logger.Info(string.Format("AK.CIM_HIRID.ADD  F{0} N{1}", forras, model.company_hirid));
                            break;
                        case EnumCimForras.FMSID:
                            Logger.Info(string.Format("AK.CIM_FMSID.ADD  F{0} N{1}", forras, model.company_fmsid));
                            break;
                        case EnumCimForras.CONTACTCODE:
                            Logger.Info(string.Format("AK.CIM_CC.ADD  F{0} N{1}", forras, model.partner_contactcode));
                            break;
                        case EnumCimForras.SECRET_CONTACT_CODE:
                            Logger.Info(string.Format("AK.CIM_SEC.ADD  F{0} N{1}", forras, model.partner_contactcode));
                            break;
                        default:
                            break;
                    }
                    #endregion

                    Result resultAddedCim = DBManagerContentum.AddCim(cim);
                    if (resultAddedCim != null && string.IsNullOrEmpty(resultAddedCim.ErrorCode))
                    {
                        outCimId = cim.Id;
                    }
                }
                else
                {
                    outCimId = cim.Id = resultFindCim.Ds.Tables[0].Rows[0]["Id"].ToString();

                    #region LOG
                    switch (forras)
                    {
                        case EnumCimForras.HIRID:
                            Logger.Info(string.Format("AK.CIM.FOUND  F{0} N{1}", forras, model.company_hirid));
                            break;
                        case EnumCimForras.FMSID:
                            Logger.Info(string.Format("AK.CIM.FOUND  F{0} N{1}", forras, model.company_fmsid));
                            break;
                        case EnumCimForras.CONTACTCODE:
                            Logger.Info(string.Format("AK.CIM.FOUND  F{0} N{1}", forras, model.partner_contactcode));
                            break;
                        default:
                            break;
                    }
                    #endregion
                }

                AddOrSetPartnerCim(cim.Id, partnerId);
                AddToPartnerCimekHistory(ref existingPartnerCimekIds, partnerId, cim.Id);

                switch (forras)
                {
                    case EnumCimForras.HIRID:
                        switch (partnerType)
                        {
                            case EnumMainSubPartner.MAIN:
                                AddToCimekHistory(ref existingMainHIRCimIds, cim.Id, model.company_hirid);
                                break;
                            case EnumMainSubPartner.SUB:
                                AddToCimekHistory(ref existingSubCimIds, cim.Id, model.company_hirid);
                                break;
                        }
                        break;
                    case EnumCimForras.FMSID:
                        switch (partnerType)
                        {
                            case EnumMainSubPartner.MAIN:
                                AddToCimekHistory(ref existingMainFMSCimIds, cim.Id, model.company_fmsid);
                                break;
                            case EnumMainSubPartner.SUB:
                                AddToCimekHistory(ref existingSubCimIds, cim.Id, model.company_fmsid);
                                break;
                        }
                        break;
                    case EnumCimForras.CONTACTCODE:

                        AddToCimekHistory(ref existingSubCimIds, cim.Id, model.partner_contactcode);
                        break;
                    case EnumCimForras.SECRET_CONTACT_CODE:
                        AddToCimekHistory(ref existingSubCimIds, cim.Id, model.partner_contactcode);
                        break;
                    default:
                        break;
                }
            }
            catch (ResultException exc)
            {
                Logger.Error(string.Format("AK.CIM.ERROR= E={0} M={1}", exc.ErrorCode, exc.ErrorMessage), exc);
            }
            catch (System.Exception exc)
            {
                Logger.Error("AK.CIM.ERROR= " + exc.Message + exc.StackTrace, exc);
            }
            return true;
        }
        /// <summary>
        /// /AddOrSetPartnerCim
        /// </summary>
        /// <param name="cimId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        private bool AddOrSetPartnerCim(string cimId, string partnerId)
        {
            KRT_PartnerCimek partnerCim = DBManagerContentum.GeneratePartnerCim(cimId, partnerId);
            Result resultFindPartnerCim = DBManagerContentum.FindPartnerCim(partnerCim);
            if (resultFindPartnerCim == null || !string.IsNullOrEmpty(resultFindPartnerCim.ErrorCode))
            {
                Logger.Error("AK.PARTNERCIM.ERROR=" + resultFindPartnerCim.ErrorCode + resultFindPartnerCim.ErrorMessage);
                return false;
            }
            if (resultFindPartnerCim.Ds.Tables[0].Rows.Count < 1)
            {
                Result resultAddPartnerCim = DBManagerContentum.AddPartnerCim(partnerCim);
                Logger.Error(string.Format("AK.PARTNERCIM.ADD= C={0} P={1}", cimId, partnerId));
            }
            return true;
        }

        private void Invalidate(
            Dictionary<string, string> existingMainPartnerIds,
            Dictionary<string, string> existingSubPartnerIds,
            Dictionary<string, string> existingMainHIRCimIds,
            Dictionary<string, string> existingMainFMSCimIds,
            Dictionary<string, string> existingSubCimIds,
            Dictionary<string, List<string>> existingPartnerCimekIds
            )
        {
            try
            {
                DBManagerContentum.InvalidatePartnerekNotInner(existingMainPartnerIds, existingMainHIRCimIds, existingMainFMSCimIds, true);
            }
            catch (ResultException exc)
            {
                Logger.Error(string.Format("AK.MP.INVALIDATE.ERROR= {0} {1}", exc.ErrorCode + exc.StackTrace, exc.ErrorMessage), exc);
            }
            catch (System.Exception exc)
            {
                Logger.Error("AK.MP.INVALIDATE.ERROR= " + exc.Message + exc.StackTrace, exc);
            }

            try
            {
                DBManagerContentum.InvalidatePartnerekNotInner(existingSubPartnerIds, existingMainHIRCimIds, existingMainFMSCimIds, true);
            }
            catch (ResultException exc)
            {
                Logger.Error(string.Format("AK.SP.INVALIDATE.ERROR= {0} {1}", exc.ErrorCode + exc.StackTrace, exc.ErrorMessage), exc);
            }
            catch (System.Exception exc)
            {
                Logger.Error("AK.SP.INVALIDATE.ERROR= " + exc.Message + exc.StackTrace, exc);
            }

            try
            {
                DBManagerContentum.InvalidateCimekNotInner(existingMainHIRCimIds, EnumCimForras.HIRID);
            }
            catch (ResultException exc)
            {
                Logger.Error(string.Format("AK.HIRID.INVALIDATE.ERROR= {0} {1}", exc.ErrorCode + exc.StackTrace, exc.ErrorMessage), exc);
            }
            catch (System.Exception exc)
            {
                Logger.Error("AK.HIRID.INVALIDATE.ERROR= " + exc.Message + exc.StackTrace, exc);
            }

            try
            {
                DBManagerContentum.InvalidateCimekNotInner(existingMainFMSCimIds, EnumCimForras.FMSID);
            }
            catch (ResultException exc)
            {
                Logger.Error(string.Format("AK.FMSID.INVALIDATE.ERROR= {0} {1}", exc.ErrorCode + exc.StackTrace, exc.ErrorMessage), exc);
            }
            catch (System.Exception exc)
            {
                Logger.Error("AK.FMSID.INVALIDATE.ERROR= " + exc.Message + exc.StackTrace, exc);
            }

            try
            {
                DBManagerContentum.InvalidateCimekNotInner(existingSubCimIds, EnumCimForras.CONTACTCODE);
            }
            catch (ResultException exc)
            {
                Logger.Error(string.Format("AK.CC.INVALIDATE.ERROR= {0} {1}", exc.ErrorCode + exc.StackTrace, exc.ErrorMessage), exc);
            }
            catch (System.Exception exc)
            {
                Logger.Error("AK.CC.INVALIDATE.ERROR= " + exc.Message + exc.StackTrace, exc);
            }

            try
            {
                DBManagerContentum.InvalidatePartnerCimekNotInner(existingPartnerCimekIds);
            }
            catch (ResultException exc)
            {
                Logger.Error(string.Format("AK.PARCIM.INVALIDATE.ERROR= {0} {1}", exc.ErrorCode + exc.StackTrace, exc.ErrorMessage), exc);
            }
            catch (System.Exception exc)
            {
                Logger.Error("AK.PARCIM.INVALIDATE.ERROR= " + exc.Message + exc.StackTrace, exc);
            }
        }
        /// <summary>
        /// GetKeyInnserSqlFormat
        /// </summary>
        /// <param name="Ids"></param>
        /// <returns></returns>
        private string GetKeyInnserSqlFormat(Dictionary<string, string> Ids)
        {
            StringBuilder sb = new StringBuilder();
            bool elso = true;
            foreach (string id in Ids.Keys)
            {
                if (elso)
                    sb.Append(string.Format("'{0}'", id));
                else
                    sb.Append(string.Format(",'{0}'", id));
            }
            return sb.ToString();
        }
        /// <summary>
        /// AddToCimekHistory
        /// </summary>
        /// <param name="cimekHistory"></param>
        /// <param name="partnerId"></param>
        /// <param name="partnerNev"></param>
        private void AddToCimekHistory(ref Dictionary<string, string> cimekHistory, string partnerId, string partnerNev)
        {
            if (cimekHistory.ContainsKey(partnerId))
            {
                cimekHistory[partnerId] = partnerNev;
            }
            else
            {
                cimekHistory.Add(partnerId, partnerNev);
            }
        }
        /// <summary>
        /// AddToPartnerCimekHistory
        /// </summary>
        /// <param name="partnerCimekHistory"></param>
        /// <param name="partnerId"></param>
        /// <param name="cimId"></param>
        private void AddToPartnerCimekHistory(ref Dictionary<string, List<string>> partnerCimekHistory, string partnerId, string cimId)
        {
            if (partnerCimekHistory.ContainsKey(partnerId))
            {
                partnerCimekHistory[partnerId].Add(cimId);
            }
            else
            {
                partnerCimekHistory.Add(partnerId, new List<string>() { cimId });
            }
        }
    }
}