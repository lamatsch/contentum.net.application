﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace Contentum.eIntegrator.AdatKapu.PartnerSync
{
    /// <summary>
    /// Summary description for AdatkapuDataBasePartnerManager
    /// </summary>
    public partial class AdatKapuDataBaseManager
    {
        #region PROPERTIES
        private readonly string PartnerSyncNoteValue = "AdatKapu.PartnerSync";
        private readonly string DefaultAdatKapuPartnerSelectQuerySchema = "neaddfel";
        private string AdatKapuPartnerSelectQuerySchema { get; set; }

        private readonly string AdatKapuPartnerSelectQueryFormat =
            "select company_name, company_hirid, company_fmsid, partner_name, partner_email, partner_contactcode from {0}.partner_view";
        private string AdatKapuPartnerQuery
        {
            get { return string.Format(AdatKapuPartnerSelectQueryFormat, AdatKapuPartnerSelectQuerySchema); }
        }
        #endregion

        public void InitAdatKapuPartnerekSchemaFromParameterek()
        {
            AdatKapuPartnerSelectQuerySchema = IntegratorParameterek.GetParameter(IntegratorParameterek.ADATKAPU_PARTNER_SYNC_VIEW_SCHEMA);
            if (string.IsNullOrEmpty(AdatKapuPartnerSelectQuerySchema))
            {
                AdatKapuPartnerSelectQuerySchema = DefaultAdatKapuPartnerSelectQuerySchema;
            }
        }
        /// <summary>
        /// Adatkapu.Partnerek
        /// </summary>
        /// <returns></returns>
        public DataSet GetPartnerAdatok()
        {
            eUtility.Logger.Info("DataBaseManager.GetPartnerAdatok kezdete");

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(AdatKapuPartnerQuery, conn);
                cmd.CommandType = CommandType.Text;
                cmd.BindByName = true;

                DataSet ds = new DataSet();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                eUtility.Logger.Info("DataBaseManager.GetPartnerAdatok vege");

                return ds;

            }
        }

        public Result FindPartnerSimple(string name)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            KRT_PartnerekSearch searchObj = new KRT_PartnerekSearch();

            searchObj.Nev.Value = name;
            searchObj.Nev.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, searchObj);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
            return result;
        }

        public Result AddMainPartnerSimple(AdatkapuPartnerModel item, string id)
        {
            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            KRT_Partnerek newPartner = GenerateMainPartnerSimple(item, id);

            Result result = service.Insert(execParam, newPartner);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
            return result;
        }
        /// <summary>
        /// AddSubPartner
        /// </summary>
        /// <param name="item"></param>
        /// <param name="parentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Result AddSubPartner(AdatkapuPartnerModel item, string parentId, string id)
        {
            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            KRT_PartnerKapcsolatokService serviceKapcsolat = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();

            KRT_Partnerek newPartner = GenerateSubPartner(item, id);
            id = newPartner.Id;

            Result result = service.Insert(execParam, newPartner);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);

            Logger.Info("AK.SP.ADDSUBPARTNER= " + newPartner.Nev);

            AddPartnerkapcsolat(parentId, id, newPartner.Nev);

            #region SZEMELY
            KRT_Szemelyek szemely = GenerateSzemely(item, newPartner.Id);
            if (szemely != null)
            {
                Result resultFindSzemely = FindSzemelyPartner(szemely);
                if (resultFindSzemely == null || resultFindSzemely.Ds.Tables[0].Rows.Count < 1)
                {
                    Logger.Info("AK.SP.ADDSZEMELY= " + szemely.UjCsaladiNev + string.Empty + szemely.UjUtonev + string.Empty + szemely.UjTovabbiUtonev);
                    AddSzemely(szemely);
                }
            }
            #endregion

            return result;
        }

        public void AddPartnerkapcsolat(string parentId, string id, string partnerNev)
        {
            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerKapcsolatokService serviceKapcsolat = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
            KRT_PartnerKapcsolatokSearch src = new KRT_PartnerKapcsolatokSearch();
            src.Partner_id.Value = id;
            src.Partner_id.Operator = Query.Operators.equals;

            src.Partner_id_kapcsolt.Value = parentId;
            src.Partner_id_kapcsolt.Operator = Query.Operators.equals;

            src.Tipus.Value = KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja;
            src.Tipus.Operator = Query.Operators.equals;

            Result resultFind = serviceKapcsolat.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(resultFind.ErrorCode))
                throw new ResultException(resultFind);

            if (resultFind == null || resultFind.Ds.Tables[0].Rows.Count < 1)
            {
                KRT_PartnerKapcsolatok kapcsolat = new KRT_PartnerKapcsolatok();
                kapcsolat.Id = Guid.NewGuid().ToString();
                kapcsolat.Partner_id = id;
                kapcsolat.Partner_id_kapcsolt = parentId;
                kapcsolat.Tipus = KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja;
                kapcsolat.Base.Note = PartnerSyncNoteValue;

                Result resultKapcsolat = serviceKapcsolat.Insert(execParam, kapcsolat);
                if (!string.IsNullOrEmpty(resultKapcsolat.ErrorCode))
                    throw new ResultException(resultKapcsolat);

                Logger.Info("AK.SP.PARTNERKAPCSOLAT= " + partnerNev);
            }
        }
        /*szervezet kapcsolattartója
       
             */
        /// <summary>
        /// GenerateMainPartner
        /// </summary>
        /// <param name="item"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public KRT_Partnerek GenerateMainPartner(AdatkapuPartnerModel item, string id)
        {
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = id;
            partner.Nev = item.company_name;
            partner.Tipus = KodTarak.Partner_Tipus.Szervezet;
            if (!string.IsNullOrEmpty(item.company_hirid) && !string.IsNullOrEmpty(item.company_fmsid))
            {
                partner.KulsoAzonositok = string.Join(",", new string[] { item.company_hirid, item.company_fmsid });
            }
            else if (!string.IsNullOrEmpty(item.company_hirid))
            {
                partner.KulsoAzonositok = string.Join(",", new string[] { item.company_hirid, "NULL" });
            }
            else if (!string.IsNullOrEmpty(item.company_fmsid))
            {
                partner.KulsoAzonositok = string.Join(",", new string[] { "NULL", item.company_fmsid });
            }
            partner.Base.Note = PartnerSyncNoteValue;
            partner.Allapot = KodTarak.Partner_Allapot.Jovahagyott;
            return partner;
        }
        public KRT_Partnerek GenerateMainPartnerSimple(AdatkapuPartnerModel item, string id)
        {
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = id;
            partner.Nev = item.company_name;
            partner.Forras = KodTarak.Partner_Forras.ADATKAPU;
            partner.Tipus = KodTarak.Partner_Tipus.Szervezet;
            if (!string.IsNullOrEmpty(item.company_hirid) && !string.IsNullOrEmpty(item.company_fmsid))
            {
                partner.KulsoAzonositok = string.Join(",", new string[] { item.company_hirid, item.company_fmsid });
            }
            else if (!string.IsNullOrEmpty(item.company_hirid))
            {
                partner.KulsoAzonositok = string.Join(",", new string[] { item.company_hirid, "NULL" });
            }
            else if (!string.IsNullOrEmpty(item.company_fmsid))
            {
                partner.KulsoAzonositok = string.Join(",", new string[] { "NULL", item.company_fmsid });
            }
            partner.Base.Note = PartnerSyncNoteValue;
            partner.Allapot = KodTarak.Partner_Allapot.Jovahagyott;
            partner.Belso = "0"; // BUG_10140
            return partner;
        }
        /// <summary>
        /// GenerateSubPartner
        /// </summary>
        /// <param name="item"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public KRT_Partnerek GenerateSubPartner(AdatkapuPartnerModel item, string id)
        {
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = id;
            partner.Nev = item.partner_name;
            partner.KulsoAzonositok = item.partner_contactcode;
            partner.Forras = KodTarak.Partner_Forras.ADATKAPU;
            partner.Tipus = KodTarak.Partner_Tipus.Szemely;
            partner.Base.Note = PartnerSyncNoteValue;
            partner.Allapot = KodTarak.Partner_Allapot.Jovahagyott;
            partner.Belso = "0"; // BUG_10140
            return partner;
        }

        #region KRT_CIMEK
        public KRT_Cimek GenerateCim(AdatkapuPartnerModel item, EnumCimForras forras)
        {
            KRT_Cimek cim = new KRT_Cimek();
            cim.Id = System.Guid.NewGuid().ToString();
            switch (forras)
            {
                case EnumCimForras.HIRID:
                    cim.CimTobbi = item.company_hirid;
                    cim.Forras = KodTarak.Cim_Forras.HIR;
                    cim.Tipus = KodTarak.Cim_Tipus.Hivatali_Kapu;
                    if (!string.IsNullOrEmpty(item.company_hirid))
                        cim.KulsoAzonositok = item.company_hirid;
                    break;
                case EnumCimForras.FMSID:
                    cim.CimTobbi = item.company_fmsid;
                    cim.Forras = KodTarak.Cim_Forras.FMS;
                    cim.Tipus = KodTarak.Cim_Tipus.Hivatali_Kapu;
                    if (!string.IsNullOrEmpty(item.company_fmsid))
                        cim.KulsoAzonositok = item.company_fmsid;
                    break;
                case EnumCimForras.CONTACTCODE:
                    cim.CimTobbi = item.partner_email;
                    cim.Forras = KodTarak.Cim_Forras.AK;
                    cim.Tipus = KodTarak.Cim_Tipus.Email;
                    if (!string.IsNullOrEmpty(item.partner_contactcode))
                        cim.KulsoAzonositok = item.partner_contactcode;
                    break;
                case EnumCimForras.SECRET_CONTACT_CODE:
                    cim.CimTobbi = item.partner_contactcode;
                    cim.Forras = KodTarak.Cim_Forras.AK;
                    cim.Tipus = KodTarak.Cim_Tipus.UGYFELKAPU;
                    if (!string.IsNullOrEmpty(item.partner_contactcode))
                        cim.KulsoAzonositok = item.partner_contactcode;
                    break;
                default:
                    break;
            }
            cim.Kategoria = KodTarak.Cim_Kategoria.K;
            cim.Base.Note = PartnerSyncNoteValue;
            return cim;
        }
        public Result FindCim(KRT_Cimek item)
        {
            if (string.IsNullOrEmpty(item.CimTobbi))
                return null;

            ExecParam execParam = this.ExecParamContentum;
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            KRT_CimekSearch searchObj = new KRT_CimekSearch();

            searchObj.CimTobbi.Value = item.CimTobbi;
            searchObj.CimTobbi.Operator = Query.Operators.equals;

            searchObj.Forras.Value = item.Forras;
            searchObj.Forras.Operator = Query.Operators.equals;

            searchObj.Tipus.Value = item.Tipus;
            searchObj.Tipus.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, searchObj);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
            return result;
        }
        public KRT_Cimek FindCimById(string cimId)
        {
            if (string.IsNullOrEmpty(cimId))
                return null;

            ExecParam execParam = this.ExecParamContentum;
            execParam.Record_Id = cimId;
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            KRT_CimekSearch searchObj = new KRT_CimekSearch();

            Result result = service.Get(execParam);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                return null;
            return (KRT_Cimek)result.Record;
        }
        public Result AddCim(KRT_Cimek item)
        {
            ExecParam execParam = this.ExecParamContentum;
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            Result result = service.Insert(execParam, item);

            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
            return result;
        }
        public KRT_PartnerCimek GeneratePartnerCim(KRT_Cimek cim, string partnerId)
        {
            if (string.IsNullOrEmpty(partnerId))
                return null;

            KRT_PartnerCimek partnerCim = new KRT_PartnerCimek();
            partnerCim.Id = System.Guid.NewGuid().ToString();
            partnerCim.Partner_id = partnerId;
            partnerCim.Cim_Id = cim.Id;
            partnerCim.Base.Note = PartnerSyncNoteValue;
            return partnerCim;
        }
        public KRT_PartnerCimek GeneratePartnerCim(string cimId, string partnerId)
        {
            if (string.IsNullOrEmpty(partnerId))
                return null;

            KRT_PartnerCimek partnerCim = new KRT_PartnerCimek();
            partnerCim.Id = System.Guid.NewGuid().ToString();
            partnerCim.Partner_id = partnerId;
            partnerCim.Cim_Id = cimId;
            partnerCim.Base.Note = PartnerSyncNoteValue;
            return partnerCim;
        }
        public Result AddPartnerCim(KRT_PartnerCimek item)
        {
            if (item == null)
                return null;

            KRT_Cimek cimFound = FindCimById(item.Cim_Id);
            if (cimFound == null)
            {
                Logger.Info("PartnerCim nem letrehzhato, mert cim nem talalhato:" + item.Cim_Id);
                return null;
            }

            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            Result result = service.Insert(execParam, item);

            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
            return result;
        }
        public Result FindPartnerCim(KRT_PartnerCimek item)
        {
            if (item == null || string.IsNullOrEmpty(item.Cim_Id) || string.IsNullOrEmpty(item.Partner_id))
                return null;

            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            KRT_PartnerCimekSearch searchObj = new KRT_PartnerCimekSearch();

            searchObj.Partner_id.Value = item.Partner_id;
            searchObj.Partner_id.Operator = Query.Operators.equals;

            searchObj.Cim_Id.Value = item.Cim_Id;
            searchObj.Cim_Id.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, searchObj);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
            return result;
        }
        public Result FindPartnerCimekByPartner(string partnerId)
        {
            if (string.IsNullOrEmpty(partnerId))
                return null;

            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            KRT_PartnerCimekSearch searchObj = new KRT_PartnerCimekSearch();

            searchObj.Partner_id.Value = partnerId;
            searchObj.Partner_id.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, searchObj);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
            return result;
        }
        #endregion

        #region INVALIDATE
        /// <summary>
        /// InvalidateKRTCimek
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Result InvalidateCimById(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;

            ExecParam execParam = this.ExecParamContentum;
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            execParam.Record_Id = id;
            Result result = service.Invalidate(execParam);
            //if (!string.IsNullOrEmpty(result.ErrorCode))
            //    throw new ResultException(result);
            return result;
        }
        /// <summary>
        /// InvalidateKRTPartnerCim
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Result InvalidatePartnerCimById(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;

            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            execParam.Record_Id = id;
            Result result = service.Invalidate(execParam);
            //if (!string.IsNullOrEmpty(result.ErrorCode))
            //    throw new ResultException(result);
            return result;
        }

        /// <summary>
        /// InvalidatePartner
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Result InvalidatePartnerById(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;

            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            execParam.Record_Id = id;
            Result result = service.Invalidate(execParam);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
            return result;
        }

        /// <summary>
        /// InvalidatePartnerCimekByCim
        /// </summary>
        /// <param name="cimId"></param>
        public void InvalidatePartnerCimekByCim(string cimId)
        {
            if (string.IsNullOrEmpty(cimId))
                return;

            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            KRT_PartnerCimekSearch searchObj = new KRT_PartnerCimekSearch();
            Result result = null;
            searchObj.Cim_Id.Value = cimId;
            searchObj.Cim_Id.Operator = Query.Operators.equals;

            result = service.GetAll(execParam, searchObj);
            if (!string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count < 1)
                return;
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                InvalidatePartnerCimById(row["Id"].ToString());
            }
        }
        /// <summary>
        /// InvalidatePartnerCimek
        /// </summary>
        /// <param name="partnerCimekHistory"></param>
        public void InvalidatePartnerCimekNotInner(Dictionary<string, List<string>> partnerCimekHistory)
        {
            if (partnerCimekHistory == null || partnerCimekHistory.Count < 1)
                return;

            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            KRT_PartnerCimekSearch searchObj = null;
            Result result = null;

            foreach (var item in partnerCimekHistory.Keys)
            {
                searchObj = new KRT_PartnerCimekSearch();
                result = null;

                searchObj.Partner_id.Value = item;
                searchObj.Partner_id.Operator = Query.Operators.equals;

                searchObj.Cim_Id.Value = ToSqlInnerSearchString(partnerCimekHistory[item]);
                searchObj.Cim_Id.Operator = Query.Operators.notinner;

                result = service.GetAll(execParam, searchObj);
                if (!string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count < 1)
                    continue;
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    InvalidatePartnerCimById(row["Id"].ToString());
                }
            }
        }

        public void InvalidatePartnerekNotInner(Dictionary<string, string> newPartnerItemIds,
                                                Dictionary<string, string> existingHIRCimIds,
                                                Dictionary<string, string> existingFMSCimIds,
                                                bool mainCimek)
        {
            if (newPartnerItemIds == null || newPartnerItemIds.Count < 1)
                return;

            ExecParam execParam = this.ExecParamContentum;
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            KRT_PartnerekSearch searchObj = new KRT_PartnerekSearch();
            Result result = null;

            searchObj.Forras.Value = KodTarak.Partner_Forras.ADATKAPU;
            searchObj.Forras.Operator = Query.Operators.equals;
            if (mainCimek)
            {
                searchObj.Tipus.Value = KodTarak.Partner_Tipus.Szervezet;
                searchObj.Tipus.Operator = Query.Operators.equals;
            }
            else
            {
                searchObj.Tipus.Value = KodTarak.Partner_Tipus.Szemely;
                searchObj.Tipus.Operator = Query.Operators.equals;
            }
            //searchObj.WhereByManual = " and KRT_Cimek.Note = " + PartnerSyncNoteValue;

            result = service.GetAll(execParam, searchObj);
            if (result == null || !string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count < 1)
                return;
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                if (!newPartnerItemIds.ContainsKey(row["Id"].ToString()))
                {
                    Logger.Info("AK.P.INVALIDATE= " + row["Id"].ToString());
                    InvalidatePartnerById(row["Id"].ToString());

                    #region PARTNER CIMEK
                    Result resultPartnerCimek = FindPartnerCimekByPartner(row["Id"].ToString());
                    if (resultPartnerCimek != null && resultPartnerCimek.Ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow rowPC in resultPartnerCimek.Ds.Tables[0].Rows)
                        {
                            if (!existingHIRCimIds.ContainsKey(rowPC["Cim_id"].ToString())
                             && !existingFMSCimIds.ContainsKey(rowPC["Cim_id"].ToString()))
                            {
                                InvalidatePartnerCimById(rowPC["Id"].ToString());
                                Logger.Info("AK.P.PC.INVALIDATE= " + rowPC["Id"].ToString());
                            }
                        }
                    }
                    #endregion
                }
            }
        }

        public void InvalidateCimekNotInner(Dictionary<string, string> newItemIds, EnumCimForras forras)
        {
            if (newItemIds == null || newItemIds.Count < 1)
                return;

            //Logger.Info("AK.InvalidatePartnerCimekNotInner= " + notInnerQueryString);
            ExecParam execParam = this.ExecParamContentum;
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            KRT_CimekSearch searchObj = new KRT_CimekSearch();
            Result result = null;

            switch (forras)
            {
                case EnumCimForras.HIRID:
                    searchObj.Forras.Value = KodTarak.Cim_Forras.HIR;
                    searchObj.Forras.Operator = Query.Operators.equals;

                    searchObj.Tipus.Value = KodTarak.Cim_Tipus.Hivatali_Kapu;
                    searchObj.Tipus.Operator = Query.Operators.equals;
                    break;
                case EnumCimForras.FMSID:
                    searchObj.Forras.Value = KodTarak.Cim_Forras.FMS;
                    searchObj.Forras.Operator = Query.Operators.equals;

                    searchObj.Tipus.Value = KodTarak.Cim_Tipus.Hivatali_Kapu;
                    searchObj.Tipus.Operator = Query.Operators.equals;
                    break;
                case EnumCimForras.CONTACTCODE:
                    searchObj.Forras.Value = KodTarak.Cim_Forras.AK;
                    searchObj.Forras.Operator = Query.Operators.equals;

                    searchObj.Tipus.Value = KodTarak.Cim_Tipus.UGYFELKAPU;
                    searchObj.Tipus.Operator = Query.Operators.equals;
                    break;
                default:
                    break;
            }
            //searchObj.WhereByManual = " and KRT_Cimek.Note = " + PartnerSyncNoteValue;

            result = service.GetAll(execParam, searchObj);
            if (result == null || !string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count < 1)
                return;
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                if (!newItemIds.ContainsKey(row["Id"].ToString()))
                {
                    Logger.Info("AK.C.INVALIDATE= " + row["Id"].ToString());
                    InvalidateCimById(row["Id"].ToString());
                    InvalidatePartnerCimekByCim(row["Id"].ToString());
                }
            }
        }
        #endregion

        #region KRT_SZEMELY
        /// <summary>
        /// GenerateSzemely
        /// </summary>
        /// <param name="item"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public KRT_Szemelyek GenerateSzemely(AdatkapuPartnerModel item, string partnerId)
        {
            if (string.IsNullOrEmpty(item.partner_name))
                return null;

            KRT_Szemelyek szemely = new KRT_Szemelyek();
            string[] splitted = item.partner_name.Split(new string[] { " " }, System.StringSplitOptions.RemoveEmptyEntries);
            if (splitted == null || splitted.Length < 1)
                return null;

            byte indexCsaladiNev = 0;
            byte indexUtoNev = 1;
            byte indexUtoNevTovabbi = 2;

            if (splitted[indexCsaladiNev].ToUpper() == KodTarak.TITULUSOK.DR
                || splitted[indexCsaladiNev].ToUpper() == KodTarak.TITULUSOK.IFJ
                || splitted[indexCsaladiNev].ToUpper() == KodTarak.TITULUSOK.OZV
                || splitted[indexCsaladiNev].ToUpper() == KodTarak.TITULUSOK.II)
            {
                szemely.UjTitulis = splitted[0];

                indexCsaladiNev += 1;
                indexUtoNev += 1;
                indexUtoNevTovabbi += 1;
            }
            szemely.Id = System.Guid.NewGuid().ToString();
            szemely.Partner_Id = partnerId;
            szemely.UjCsaladiNev = splitted[indexCsaladiNev];
            if (splitted.Length >= indexCsaladiNev + 1)
                szemely.UjUtonev = splitted[indexUtoNev];
            if (splitted.Length >= indexUtoNevTovabbi + 1)
                szemely.UjTovabbiUtonev = splitted[indexUtoNevTovabbi];

            szemely.Base.Note = PartnerSyncNoteValue;
            return szemely;
        }
        /// <summary>
        /// FindSzemelyPartner
        /// </summary>
        /// <param name="szemely"></param>
        /// <returns></returns>
        public Result FindSzemelyPartner(KRT_Szemelyek szemely)
        {
            ExecParam execParam = this.ExecParamContentum;
            KRT_SzemelyekService service = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
            KRT_SzemelyekSearch searchObj = new KRT_SzemelyekSearch();

            if (!string.IsNullOrEmpty(szemely.UjCsaladiNev))
            {
                searchObj.UjCsaladiNev.Value = szemely.UjCsaladiNev;
                searchObj.UjCsaladiNev.Operator = Query.Operators.equals;
            }

            if (!string.IsNullOrEmpty(szemely.UjUtonev))
            {
                searchObj.UjUtonev.Value = szemely.UjUtonev;
                searchObj.UjUtonev.Operator = Query.Operators.equals;
            }

            if (!string.IsNullOrEmpty(szemely.UjTovabbiUtonev))
            {
                searchObj.UjTovabbiUtonev.Value = szemely.UjTovabbiUtonev;
                searchObj.UjTovabbiUtonev.Operator = Query.Operators.equals;
            }

            searchObj.Partner_Id.Value = szemely.Partner_Id;
            searchObj.Partner_Id.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, searchObj);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error(result.ErrorCode + string.Empty + result.ErrorMessage);
                return null;
            }

            return result;
        }
        /// <summary>
        /// AddSzemely
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Result AddSzemely(KRT_Szemelyek item)
        {
            ExecParam execParam = this.ExecParamContentum;
            KRT_SzemelyekService service = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
            Result result = service.Insert(execParam, item);

            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
            return result;
        }
        #endregion

        /// <summary>
        /// ToSqlInnerSearchString
        /// </summary>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public string ToSqlInnerSearchString(List<string> itemList)
        {
            return "'" + String.Join("','", itemList.ToArray()) + "'";
        }
    }
}