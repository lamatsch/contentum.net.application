﻿namespace Contentum.eIntegrator.AdatKapu.PartnerSync
{
    /// <summary>
    /// AdatkapuPartnerModel
    /// select 
    ///     company_name, company_hirid, company_fmsid, partner_name, partner_email, partner_contactcode 
    /// from 
    ///     neaddfel.partner_view
    /// </summary>
    public class AdatkapuPartnerModel
    {
        public string company_name { get; set; }
        public string CompanyId { get; set; }

        public string company_hirid { get; set; }
        public string CompanyHirIdCim { get; set; }

        public string company_fmsid { get; set; }
        public string CompanyFmsIdCim { get; set; }

        public string partner_name { get; set; }
        public string PartnerIdentity { get; set; }
     
        public string partner_email { get; set; }
        public string PartnerId { get; set; }
        public string partner_contactcode { get; set; }
    }

    public enum EnumCimForras
    {
        HIRID,
        FMSID,
        CONTACTCODE,
        SECRET_CONTACT_CODE
    }

    public enum EnumMainSubPartner
    {
        MAIN,
        SUB,
    }
}