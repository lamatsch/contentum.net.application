﻿using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Web;

namespace Contentum.eIntegrator.AdatKapu.Servlet
{
    /// <summary>
    /// Summary description for AdatkapuServletManager
    /// </summary>
    public class AdatkapuServletManager
    {
        static string servletUrl;

        static AdatkapuServletManager()
        {
            servletUrl = ConfigurationManager.AppSettings["AdatkapuServlet_URL"];
        }

        WebClient GetWebClient()
        {
            WebClient wc = new WebClient();
            return wc;
        }

        ServletResponse CallMethod(string method, Dictionary<string, string> parameters)
        {
            Logger.Info("AdatkapuServletManager.CallMethod");
            string url = GetUrl(method, parameters);
            Logger.Info(String.Format("url={0}", url));
            using (WebClient wc = GetWebClient())
            {
                byte[] response = wc.DownloadData(url);
                return new ServletResponse(response, wc.ResponseHeaders);
            }
        }

        string GetUrl(string method, Dictionary<string, string> parameters)
        {
            if (String.IsNullOrEmpty(servletUrl))
            {
                string error = "AdatkapuServlet_URL config paraméter nincs megadva!";
                Logger.Error(error);
                throw new Exception(error);
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("method=");
            sb.Append(HttpUtility.UrlEncode(method));
            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                sb.Append("&");
                sb.Append(parameter.Key);
                sb.Append("=");
                sb.Append(HttpUtility.UrlEncode(parameter.Value));
            }
            string url = servletUrl + "?" + sb.ToString();
            return url;
        }

        public ServletResponse SetRegistrationIdentifier(string formid, string iktatoszam, string iktato)
        {
            Logger.Info("AdatkapuServletManager.SetRegistrationIdentifier");

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("formid", formid);
            parameters.Add("iktatoszam", iktatoszam);
            parameters.Add("iktato", iktato);
            ServletResponse response = CallMethod("SetRegistrationIdentifier", parameters);
            Logger.Debug(String.Format("Response: {0}", response.ResponseString));
            return response;
        }
    }
}