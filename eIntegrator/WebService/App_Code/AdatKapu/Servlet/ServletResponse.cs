﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Text;

namespace Contentum.eIntegrator.AdatKapu.Servlet
{
    /// <summary>
    /// Summary description for ServletResponse
    /// </summary>
    public class ServletResponse
    {
        private byte[] _ResponseData;

        public byte[] ResponseData
        {
            get { return _ResponseData; }
            set { _ResponseData = value; }
        }

        private string _ResponseString;

        public string ResponseString
        {
            get { return _ResponseString; }
            set { _ResponseString = value; }
        }

        private WebHeaderCollection _ResponseHeaders;

        public WebHeaderCollection ResponseHeaders
        {
            get { return _ResponseHeaders; }
            set { _ResponseHeaders = value; }
        }

        private bool _IsError;

        public bool IsError
        {
            get { return _IsError; }
            set { _IsError = value; }
        }

        private string _ErrorCode;

        public string ErrorCode
        {
            get { return _ErrorCode; }
            set { _ErrorCode = value; }
        }
        private string _ErrorMessage;

        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }

        public ServletResponse(byte[] responseData, WebHeaderCollection responseHeaders)
        {
            this.ResponseData = responseData;
            this.ResponseHeaders = responseHeaders;
            this.ResponseString = Encoding.UTF8.GetString(responseData);

        }
    }
}