﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.AdatKapu.Servlet;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for AdatkapuService
/// </summary>
[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class AdatkapuService : System.Web.Services.WebService
{

    public AdatkapuService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public Result SetRegistrationIdentifier(ExecParam execParam, string formid, string iktatoszam, string iktato)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();

        try
        {
            AdatkapuServletManager manager = new AdatkapuServletManager();
            ServletResponse response = manager.SetRegistrationIdentifier(formid, iktatoszam, iktato);

            if (response.IsError)
            {
                result.ErrorCode = response.ErrorCode;
                result.ErrorMessage = response.ErrorMessage;
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(e.Message);
        }

        log.WsEnd(execParam, result);
        return result;
    }


}
