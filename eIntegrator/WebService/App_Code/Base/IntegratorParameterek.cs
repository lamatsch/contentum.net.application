﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

/// <summary>
/// Summary description for IntegratorParameterek
/// </summary>
public class IntegratorParameterek
{
    public const string eUzenetForras = "eUzenetForras";
    public const string eUzenetSchema = "eUzenetSchema";
    public const string ADATKAPU_PARTNER_SYNC_VIEW_SCHEMA = "ADATKAPU_PARTNER_SYNC_VIEW_SCHEMA";

    private const String cache_key = "IntegratorParameterekDictionary";

    private static readonly object _sync = new object();

    const int refreshPeriod = 600;

    private static Cache GetCache()
    {
        if (HttpContext.Current == null)
        {
            return System.Web.HttpRuntime.Cache;
        }
        return HttpContext.Current.Cache;
    }

    public static string GetParameter(string nev)
    {
        Cache cache = GetCache();

        Dictionary<string, string> cacheDict = cache[cache_key] as Dictionary<string, string>;

        if (cacheDict == null)
        {
            lock (_sync)
            {
                cacheDict = cache[cache_key] as Dictionary<string, string>;

                if (cacheDict == null)
                {
                    cacheDict = new Dictionary<string, string>();
                    cache.Insert(cache_key, cacheDict, null, DateTime.Now.AddMinutes(refreshPeriod),
                        System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.NotRemovable, null);
                }
            }
        }

        if (!cacheDict.ContainsKey(nev))
        {
            string ertek = ReadParameter(nev);

            lock (_sync)
            {
                if (!cacheDict.ContainsKey(nev))
                {
                    cacheDict.Add(nev, ertek);
                }
            }
        }

        return cacheDict[nev];
    }

    //Ahol van service context onnan lehet ezt hívni
    static string ReadParameter(string nev)
    {
        try
        {
            ExecParam execParam = new ExecParam();
            INT_ParameterekService service = new INT_ParameterekService();

            Result res = service.GetByNev(execParam, nev);

            if (res.IsError)
            {
                Logger.Error(String.Format("GetParameter hiba: {0}", nev), execParam, res);
            }

            if (res.Ds.Tables[0].Rows.Count == 1)
            {
                return res.Ds.Tables[0].Rows[0]["Ertek"].ToString();
            }
        }
        catch (Exception e)
        { // ha nincs context akkor hiba történik
            return ReadParameterAlternative(nev);
        }
        return null;
    }
    //Ha nincs service context akkor így kérjük le a servicet
    private static string ReadParameterAlternative(string nev)
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";
        execParam.Org_Id = "450B510A-7CAA-46B0-83E3-18445C0C53A9";


        Contentum.eIntegrator.Service.INT_ParameterekService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.GetINT_ParameterekService();
        if (svc == null) { throw new ArgumentException("INT_ParameterekService létrehozása sikertelen !"); }

        var search = new INT_ParameterekSearch();
        search.Nev.Value = nev;
        search.Nev.Operator = Query.Operators.equals;

        Result res = svc.GetAll(execParam, search);


        if (res.IsError)
        {
            Logger.Error(String.Format("GetParameter hiba: {0}", nev), execParam, res);
            return string.Empty;
        }

        if (res.Ds.Tables[0].Rows.Count == 1)
        {
            return res.Ds.Tables[0].Rows[0]["Ertek"].ToString();
        }
        return null;

    }
}