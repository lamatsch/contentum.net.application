﻿using Contentum.eUtility;

/// <summary>
/// Summary description for CommonFunctions4Web
/// </summary>
public static class CommonFunctions4Web
{
    /// <summary>
    /// GetDocumentContent
    /// </summary>
    /// <param name="externalLink"></param>
    /// <returns></returns>
    public static byte[] GetDocumentContent(string externalLink)
    {
        Logger.Debug(string.Format("GetDocumentContent kezdete: {0}", externalLink));

        string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserName");
        string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServicePassword");
        string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserDomain");

        using (System.Net.WebClient wc = new System.Net.WebClient())
        {
            #region CREDENTIAL 1

            wc.Credentials = new System.Net.NetworkCredential(__usernev, __password, __doamin);

            #endregion CREDENTIAL 1

            #region CREDENTIAL 2

            //wc.UseDefaultCredentials = true;

            #endregion CREDENTIAL 2

            return wc.DownloadData(externalLink);
        }
    }
}