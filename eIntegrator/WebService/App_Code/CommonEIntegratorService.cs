﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.AdatKapu.PartnerSync;
using Contentum.eUtility;
using System;
using System.Web.Services;

/// <summary>
/// Summary description for eIntegratorService
/// </summary>
[WebService(Namespace = "Contentum.eIntegrator.WebService", Description = "Contentum.CommonEIntegratorService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class CommonEIntegratorService : System.Web.Services.WebService
{
    public CommonEIntegratorService()
    {
    }

    /// <summary>
    /// AdatKapu partner szinkronizáció
    /// </summary>
    /// <param name="felhasznaloId"></param>
    /// <param name="FelhasznaloSzervezetId"></param>
    /// <returns></returns>
    [WebMethod(Description = "AdatKapu partner szinkronizáció")]
    public bool AdatKapuPartnerSyncWU(string felhasznaloId, string FelhasznaloSzervezetId)
    {
        if (string.IsNullOrEmpty(felhasznaloId) || string.IsNullOrEmpty(FelhasznaloSzervezetId))
        {
            Logger.Debug("eIntegratorService.AdatKapuPartnerSyncWU Hiányzó paraméterek !");
            return false;
        }

        Result result = new Result();
        try
        {
            AdatKapuPartnerSyncManager partnerSyncManager = new AdatKapuPartnerSyncManager(felhasznaloId, FelhasznaloSzervezetId);
            return partnerSyncManager.StartPartnerSynchronization();
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(e.Message);
            return false;
        }
    }

    /// <summary>
    /// AdatKapu partner szinkronizáció
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod(Description = "AdatKapu partner szinkronizáció")]
    public bool AdatKapuPartnerSyncWE(ExecParam execParam)
    {
        if (execParam == null)
        {
            Logger.Debug("eIntegratorService.AdatKapuPartnerSyncWE Hiányzó paraméter !");
            return false;
        }

        Result result = new Result();
        try
        {
            AdatKapuPartnerSyncManager partnerSyncManager = new AdatKapuPartnerSyncManager(execParam);
            return partnerSyncManager.StartPartnerSynchronization();
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(e.Message);
            return false;
        }
    }
}