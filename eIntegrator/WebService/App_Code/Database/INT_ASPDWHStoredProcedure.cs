﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for INT_ParameterekStoredProcedure
/// </summary>
public partial class INT_ASPDWHStoredProcedure
{
    private DataContext dataContext;
    public INT_ASPDWHStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }

    public Result GetData(ExecParam ExecParam, string spName, DateTime start, DateTime end)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, spName);

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            Query query = new Query();

            SqlCommand SqlComm = new SqlCommand(spName);
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 0;

            SqlComm.Parameters.Add(new SqlParameter("@startDate", start));
            SqlComm.Parameters.Add(new SqlParameter("@endDate", end));

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    //public Result GetUgyiratIratTipus(ExecParam ExecParam, DateTime start, DateTime end)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_ASPDWH_ugyirat_irat");

    //    Result _ret = new Result();

    //    //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
    //    //{
    //    //sqlConnection.Open();

    //    try
    //    {
    //        Query query = new Query();            

    //        SqlCommand SqlComm = new SqlCommand("[sp_ASPDWH_ugyirat_irat]");
    //        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
    //        //SqlComm.Connection = Connection;
    //        //SqlComm.Connection = sqlConnection;
    //        SqlComm.Connection = dataContext.Connection;
    //        SqlComm.Transaction = dataContext.Transaction;

    //        SqlComm.Parameters.Add(new SqlParameter("@startDate", start));
    //        SqlComm.Parameters.Add(new SqlParameter("@endDate", end));

    //        DataSet ds = new DataSet();
    //        SqlDataAdapter adapter = new SqlDataAdapter();
    //        adapter.SelectCommand = SqlComm;
    //        adapter.Fill(ds);

    //        _ret.Ds = ds;

    //    }
    //    catch (SqlException e)
    //    {
    //        _ret.ErrorCode = e.ErrorCode.ToString();
    //        _ret.ErrorMessage = e.Message;
    //    }

    //    //sqlConnection.Close();
    //    //}

    //    log.SpEnd(ExecParam, _ret);
    //    return _ret;

    //}

 
}