﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for INT_ETDR_ObjektumokStoredProcedure
/// </summary>
public class INT_ETDR_ObjektumokStoredProcedure
{
    private DataContext dataContext;

    public INT_ETDR_ObjektumokStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }

    public Result Get_Felhasznalok(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_INT_ETDR_Objektumok_Get_Felhasznalok");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_INT_ETDR_Objektumok_Get_Felhasznalok]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    /// <summary>
    /// SP_GetIntMod
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    public Result SP_GetIntezesiModok(ExecParam ExecParam)
    {
        Log log = Log.SpStart(ExecParam, "sp_INT_ETDR_Objektumok_Get_IntMod");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_INT_ETDR_Objektumok_Get_IntMod]");
            SqlComm.CommandType = CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            if (ExecParam != null)
            {
                SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;
            }
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result Get_By_EtdrId(ExecParam execParam, int ETDR_Id, string obj_Tip_Kod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_INT_ETDR_Objektumok_Get_By_EtdrId");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_INT_ETDR_Objektumok_Get_By_EtdrId]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ETDR_Id", System.Data.SqlDbType.Int));
            SqlComm.Parameters["@ETDR_Id"].Value = ETDR_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Obj_Tip_Kod", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Obj_Tip_Kod"].Value = obj_Tip_Kod;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result Get_By_ObjId(ExecParam execParam, Guid obj_Id, string obj_Tip_Kod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_INT_ETDR_Objektumok_Get_By_ObjId");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_INT_ETDR_Objektumok_Get_By_ObjId]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Obj_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Obj_Id"].Value = obj_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Obj_Tip_Kod", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Obj_Tip_Kod"].Value = obj_Tip_Kod;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }
    /// <summary>
    /// SP_InsertETDRObj
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="obj_Id"></param>
    /// <param name="obj_Tip_Id"></param>
    /// <param name="obj_Tip_Nev"></param>
    /// <param name="data"></param>
    /// <param name="note"></param>
    /// <returns></returns>
    public Result SP_Insert_ETDR_Obj(ExecParam ExecParam, string obj_Id, string obj_Tip_Id, string obj_Tip_Nev, string data, string note)
    {
        Log log = Log.SpStart(ExecParam, "sp_INT_ETDR_Objektumok_Get_IntMod");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_INT_ETDR_Objektumok_Get_IntMod]");
            SqlComm.CommandType = CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            if (ExecParam != null)
            {
                SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;
            }
            if (!string.IsNullOrEmpty(obj_Tip_Id))
            {
                SqlComm.Parameters.Add(new SqlParameter("@Obj_Tip_Id", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Obj_Tip_Id"].Value = obj_Tip_Id;
            }
            if (!string.IsNullOrEmpty(obj_Tip_Nev))
            {
                SqlComm.Parameters.Add(new SqlParameter("@Obj_Tip_Nev", SqlDbType.NVarChar, int.MaxValue));
                SqlComm.Parameters["@Obj_Tip_Nev"].Value = obj_Tip_Nev;
            }
            if (!string.IsNullOrEmpty(obj_Id))
            {
                SqlComm.Parameters.Add(new SqlParameter("@Obj_Id", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Obj_Id"].Value = obj_Id;
            }
            if (!string.IsNullOrEmpty(data))
            {
                SqlComm.Parameters.Add(new SqlParameter("@ETDR_Data", SqlDbType.NVarChar, int.MaxValue));
                SqlComm.Parameters["@ETDR_Data"].Value = data;
            }
            if (!string.IsNullOrEmpty(note))
            {
                SqlComm.Parameters.Add(new SqlParameter("@ETDR_Note", SqlDbType.NVarChar, int.MaxValue));
                SqlComm.Parameters["@ETDR_Note"].Value = note;
            }

            Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

            try
            {
                SqlComm.ExecuteNonQuery();
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}