﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for INT_ParameterekStoredProcedure
/// </summary>
public partial class INT_ParameterekStoredProcedure
{
    //private DataContext dataContext;
    //public INT_ParameterekStoredProcedure(DataContext _dataContext)
    //{
    //    this.dataContext = _dataContext;
    //}

    const string _cmdGetByNev = @"select * from INT_Parameterek where Nev = @nev";
    public Result GetByNev(ExecParam ExecParam, string nev)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "GetByNev");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand(_cmdGetByNev);
            SqlComm.CommandType = System.Data.CommandType.Text;
            SqlComm.Connection = dataContext.Connection;

            SqlComm.Parameters.Add(new SqlParameter("@nev", nev));

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();

            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    const string _cmdGetByModulNev = @"select p.* from INT_Parameterek p join INT_Modulok m on p.Modul_id = m.Id where m.Nev = @modulNev";
    public Result GetByModulNev(ExecParam ExecParam, string modulNev)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "GetByModulNev");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand(_cmdGetByModulNev);
            SqlComm.CommandType = System.Data.CommandType.Text;
            SqlComm.Connection = dataContext.Connection;

            SqlComm.Parameters.Add(new SqlParameter("@modulNev", modulNev));

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();

            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}