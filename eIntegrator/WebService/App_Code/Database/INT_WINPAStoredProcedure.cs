﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using FirebirdSql.Data.FirebirdClient;

public partial class INT_WINPAStoredProcedure
{
    private DataContext dataContext;
    /*
        public static class OrszagViszonylatKod
        {
            public const string EgyebKulfoldi = "K2";
            public const string Europai = "K1";
        }

     
kódja   Megnevezés
BELFOLD belföld
EKULF   egyéb külföld
EU      Európa
     */
    private enum RendeltetesiHely 
    { 
        BELFOLD,
        EKULF,
        EU
    }
    /*
     Szolgáltatás kódja Megnevezés
Normál Nincs különszolgáltatás megadva.
E-ÉRT elektronikus értesítés
EXP expressz (csak külföldre)
AJ ajánlott
E-ELO elektronikus előrejelzés
TV tértivevény
SK saját kezébe
UV=<utánvét értéke> utánvét
NGK gépi feldolgozásra nem alkalmas
EAR egyedi áras
ÉRTÉKNY=<értéknyilvánítás> értéklevél, értéknyilvánítás
LEZÁR postai lezáró szolgáltatás
E-VISSZA elektronikus visszajelzés
HIV hivatalos irat*/

    private static class SzolgaltatasokClass
    {
        public static String Normal { get { return "Normál"; } }
        public static String ElektronikusERT { get { return "E-ÉRT"; } }
        public static String Expressz { get { return "EXP"; } }
        public static String Ajanlott { get { return "AJ"; } }
        public static String ElektronikusEloreJelzes { get { return "E-ELO"; } }
        public static String Tertitveveny { get { return "TV"; } }
        public static String SajatKezbe { get { return "SK"; } }
        //public static String Utanvet { get { return "UV"; } }
        public static String GepiFeldNemAlkalmas { get { return "NGK"; } }
        public static String EgyediAras { get { return "EAR"; } }
        //public static String Ertek_Nyilv { get { return "ÉRTÉKNY"; } }
        public static String PostaiLezaroSzolgalat { get { return "LEZÁR"; } }
        public static String ElektronikusVisszajelzes { get { return "E-VISSZA"; } }
        public static String HivatalosIrat { get { return "HIV"; } }
    }
    /*Küldeménytípus kódja Megnevezés
    SZL szabványlevél
    EL egyéblevél
    SZL_EL szabványlevél elsőbbséggel
    EL_EL egyéblevél elsőbbséggel
    NYOMT nyomtatvány
    DM címzett reklámküldemény
    KL képeslap
    CS csomag
    SZAMLALEV számlalevél*/
    
    private  class KuldemenyTipusokClass
    {
        public static Tuple<String, String, String>[] KuldemenyTipusokWithSuly =
            new Tuple<String, String, String>[]
            {
                Tuple.Create("K1_01", "20", "SZL"),
                Tuple.Create("K2_01", "20", "SZL"),
                Tuple.Create("01", "30", "SZL"),
                Tuple.Create("K2_10", "NULL", "KL"),
                Tuple.Create("K1_10", "NULL", "KL"),
                Tuple.Create("18", "NULL", "KL"),
                Tuple.Create("K2_05", "250", "EL"),
                Tuple.Create("K1_05", "250", "EL"),
                Tuple.Create("04", "250", "EL"),
                Tuple.Create("05", "500", "EL"),
                Tuple.Create("K2_06", "500", "EL"),
                Tuple.Create("K1_06", "500", "EL"),
                Tuple.Create("02", "50", "EL"),
                Tuple.Create("K1_03", "50", "EL"),
                Tuple.Create("K2_03", "50", "EL"),
                Tuple.Create("06", "750", "EL"),
                Tuple.Create("K2_07", "1000", "EL"),
                Tuple.Create("K1_07", "1000", "EL"),
                Tuple.Create("K2_08", "1500", "EL"),                
                Tuple.Create("K1_08", "1500", "EL"),
                Tuple.Create("07", "2000", "EL"),
                Tuple.Create("K1_09", "2000", "EL"),
                Tuple.Create("K2_09", "2000", "EL"),
                Tuple.Create("03", "100", "EL"),
                Tuple.Create("K1_04", "100", "EL"),
                Tuple.Create("K2_04", "100", "EL"),
                Tuple.Create("K1_02", "20", "EL"),
                Tuple.Create("K2_02", "20", "EL")
            };
        public static Tuple<String, String, String>[] ElsobbsegiKuldemenyTipusokWithSuly =
            new Tuple<String, String, String>[]
            {
                Tuple.Create("K1_01", "20", "SZL_EL"),
                Tuple.Create("K2_01", "20", "SZL_EL"),
                Tuple.Create("01", "30", "SZL_EL"),
                Tuple.Create("K2_05", "250", "EL_EL"),
                Tuple.Create("K1_05", "250", "EL_EL"),
                Tuple.Create("04", "250", "EL_EL"),
                Tuple.Create("05", "500", "EL_EL"),
                Tuple.Create("K2_06", "500", "EL_EL"),
                Tuple.Create("K1_06", "500", "EL_EL"),
                Tuple.Create("02", "50", "EL_EL"),
                Tuple.Create("K1_03", "50", "EL_EL"),
                Tuple.Create("K2_03", "50", "EL_EL"),
                Tuple.Create("06", "750", "EL_EL"),
                Tuple.Create("K2_07", "1000", "EL_EL"),
                Tuple.Create("K1_07", "1000", "EL_EL"),
                Tuple.Create("K2_08", "1500", "EL_EL"),                
                Tuple.Create("K1_08", "1500", "EL_EL"),
                Tuple.Create("07", "2000", "EL_EL"),
                Tuple.Create("K1_09", "2000", "EL_EL"),
                Tuple.Create("K2_09", "2000", "EL_EL"),
                Tuple.Create("03", "100", "EL_EL"),
                Tuple.Create("K1_04", "100", "EL_EL"),
                Tuple.Create("K2_04", "100", "EL_EL"),
                Tuple.Create("K1_02", "20", "EL_EL"),
                Tuple.Create("K2_02", "20", "EL_EL")
            };
        public static String Nyomtatvany { get { return "NYOMT"; } }
        public static String CimzettReklam { get { return "DM"; } }
        public static String Csomag { get { return "CS"; } }
        public static String SzamlaLevel { get { return "SZAMLALEV"; } }
    }


    public INT_WINPAStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;     
	}

    public void KuldemenyekToWinpa(ExecParam ExecParam, Result result, string dbPath)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "KuldemenyekToWinpa");

        Result _ret = new Result();
        
        try
        {
            if (result != null && result.Ds != null && result.Ds.Tables != null && result.Ds.Tables[0] != null)
            {
                string kuldemenyIds = "";
                foreach (DataRow dr in result.Ds.Tables[0].Rows)
                {
                    kuldemenyIds += dr["Id"].ToString()+"', '";
                }
                _ret = getWinpaAdatok(ExecParam,kuldemenyIds);
            }
            if (_ret != null && _ret.Ds != null && _ret.Ds.Tables != null && _ret.Ds.Tables[0] != null)
            {
                using (var fconnection = new FbConnection(@"User=SYSDBA;Password=masterkey;Database=" + dbPath))
                {
                    fconnection.Open();
                    using (var fCommand = new FbCommand())
                    {
                        fCommand.Connection = fconnection;
                        //lekérdezni az azonosító alapján, hogy be van-e töltve
                        foreach (DataRow dr in _ret.Ds.Tables[0].Rows)
                        {
                            fCommand.CommandText = "Select p.Id from IMPORT_LEVEL p where p.IKTATOSZAM='" + dr["IKTATOSZAM"].ToString() +"'";
                            var t = fCommand.ExecuteScalar();
                            if (t == null || String.IsNullOrEmpty(t.ToString()))
                            {
                                #region Rendeltetési hely
                                string rendeltetesi_hely = "'";
                                switch (dr["RENDELTETESI_HELY"].ToString())
                                { 
                                    case Contentum.eUtility.Constants.OrszagViszonylatKod.Europai:
                                        rendeltetesi_hely += RendeltetesiHely.EU.ToString()+"'";
                                        break;
                                    case Contentum.eUtility.Constants.OrszagViszonylatKod.EgyebKulfoldi:
                                        rendeltetesi_hely += RendeltetesiHely.EKULF.ToString()+"'";
                                        break;
                                    default:
                                        rendeltetesi_hely += RendeltetesiHely.BELFOLD.ToString()+"'";
                                        break;
                                }
                                
                                #endregion
                                #region Szolgáltatások
                                string Szolgaltatasok = "'";
                                if (!String.IsNullOrEmpty(dr["KimenoKuldemenyFajta"].ToString()))
                                {
                                    if (dr["KimenoKuldemenyFajta"].ToString() == KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat)
                                        Szolgaltatasok += SzolgaltatasokClass.HivatalosIrat + "," ;
                                }
                                //if (!String.IsNullOrEmpty(dr["Elsobbsegi"].ToString()))
                                //{
                                //    if (dr["Elsobbsegi"].ToString() == "1")
                                //        Szolgaltatasok += SzolgaltatasokClass.Expressz + "," ;
                                //}
                                if (dr["Ajanlott"]!=null && !String.IsNullOrEmpty(dr["Ajanlott"].ToString()))
                                {
                                    if (dr["Ajanlott"].ToString() == "1")
                                        Szolgaltatasok += SzolgaltatasokClass.Ajanlott  + "," ;
                                }
                                if (dr["Tertiveveny"] != null && !String.IsNullOrEmpty(dr["Tertiveveny"].ToString()))
                                {
                                    if (dr["Tertiveveny"].ToString() == "1")
                                        Szolgaltatasok += SzolgaltatasokClass.Tertitveveny + "," ;
                                }
                                if (dr["SajatKezbe"] != null && !String.IsNullOrEmpty(dr["SajatKezbe"].ToString()))
                                {
                                    if (dr["SajatKezbe"].ToString() == "1")
                                        Szolgaltatasok += SzolgaltatasokClass.SajatKezbe + "," ;
                                }
                                if (dr["E_ertesites"] != null && !String.IsNullOrEmpty(dr["E_ertesites"].ToString()))
                                {
                                    if (dr["E_ertesites"].ToString() == "1")
                                        Szolgaltatasok += SzolgaltatasokClass.ElektronikusERT + "," ;
                                }
                                if (dr["E_elorejelzes"] != null && !String.IsNullOrEmpty(dr["E_elorejelzes"].ToString()))
                                {
                                    if (dr["E_elorejelzes"].ToString() == "1")
                                        Szolgaltatasok += SzolgaltatasokClass.ElektronikusEloreJelzes + "," ;
                                }
                                if (dr["PostaiLezaroSzolgalat"] != null && !String.IsNullOrEmpty(dr["PostaiLezaroSzolgalat"].ToString()))
                                {
                                    if (dr["PostaiLezaroSzolgalat"].ToString() == "1")
                                        Szolgaltatasok += SzolgaltatasokClass.PostaiLezaroSzolgalat + "," ;
                                }
                                if (Szolgaltatasok.Length == 1)
                                    Szolgaltatasok = "NULL";
                                else
                                    Szolgaltatasok = Szolgaltatasok.Substring(0, Szolgaltatasok.Length - 1)+"'";
                                #endregion
                                #region Küldeménytípusok
                                string KuldemenyTipus = "";
                                string suly = "NULL";
                                if (dr["KimenoKuldemenyFajta"] != null && !String.IsNullOrEmpty(dr["KimenoKuldemenyFajta"].ToString()))
                                {
                                    //getküldeménytípus
                                    getKuldemenyTipusok(dr,ExecParam,out suly,out KuldemenyTipus);
                                }
                                else
                                   KuldemenyTipus = "NULL";
                                
                                #endregion
                                #region Paraméterek
                                string iktatoszam = "";
                                string obj_id = "";
                                string ragszam = "";
                                string nev = "";
                                string telepules = "";
                                string cim = "";
                                string irsz = "";
                                string orszagnev = "";
                                string orszagkod = "";
                                // BLG_984
                                string kozterulet_jelleg = "";
                                string hazszam = "";
                                string epulet = "";
                                string lepcsohaz = "";
                                string emelet = "";
                                string ajto = "";
                                string postafiok = "";
                                // BLG_1780
                                string ugyfelazonosito = "";

                                if (dr["IKTATOSZAM"] != null && !String.IsNullOrEmpty(dr["IKTATOSZAM"].ToString()))
                                {
                                    iktatoszam = "'" + dr["IKTATOSZAM"].ToString() + "'";
                                }
                                else
                                    continue;
                                if (dr["OBJ_ID"] != null && !String.IsNullOrEmpty(dr["OBJ_ID"].ToString()))
                                {
                                    obj_id = dr["OBJ_ID"].ToString();
                                }
                                else
                                { 
                                    obj_id = "NULL";
                                }
                                if (dr["RAGSZAM"] != null && !String.IsNullOrEmpty(dr["RAGSZAM"].ToString()))
                                {
                                    ragszam = "'" + dr["RAGSZAM"].ToString() + "'";
                                }
                                else
                                {
                                    ragszam = "NULL";
                                }
                                if (dr["NEV"] != null && !String.IsNullOrEmpty(dr["NEV"].ToString()))
                                {
                                    nev = "'" + EscapeSqlString(dr["NEV"].ToString()) + "'";
                                }
                                else
                                {
                                    nev = "NULL";
                                }
                                if (dr["TELEPULES"] != null && !String.IsNullOrEmpty(dr["TELEPULES"].ToString()))
                                {
                                    telepules = "'" + dr["TELEPULES"].ToString() + "'";
                                }
                                else
                                {
                                    telepules = "NULL";
                                }
                                if (dr["CIM"] != null && !String.IsNullOrEmpty(dr["CIM"].ToString()))
                                {
                                    cim = "'" + dr["CIM"].ToString() + "'";
                                }
                                else
                                {
                                    cim = "NULL";
                                }
                                if (dr["IRSZ"] != null && !String.IsNullOrEmpty(dr["IRSZ"].ToString()))
                                {
                                    irsz = "'" + dr["IRSZ"].ToString() + "'";
                                }
                                else
                                {
                                    irsz = "NULL";
                                }
                                if (dr["ORSZAG_NEV"] != null && !String.IsNullOrEmpty(dr["ORSZAG_NEV"].ToString()))
                                {
                                    orszagnev =  "'"+dr["ORSZAG_NEV"].ToString()+"'";
                                }
                                else
                                {
                                    orszagnev = "NULL";
                                }
                                if (dr["ORSZAG_KOD"] != null && !String.IsNullOrEmpty(dr["ORSZAG_KOD"].ToString()))
                                {
                                    orszagkod = dr["ORSZAG_KOD"].ToString();
                                    if ("HU".Equals(orszagkod, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        orszagkod = "HUN";
                                    }
                                    orszagkod =  "'"+ orszagkod +"'";
                                }
                                else
                                {
                                    orszagkod = "NULL";
                                }
                                // BLG_984
                                if (dr["KOZTERULET_JELLEG"] != null && !String.IsNullOrEmpty(dr["KOZTERULET_JELLEG"].ToString()))
                                {
                                    kozterulet_jelleg = "'" + dr["KOZTERULET_JELLEG"].ToString() + "'";
                                }
                                else
                                {
                                    kozterulet_jelleg = "NULL";
                                }
                                if (dr["HAZSZAM"] != null && !String.IsNullOrEmpty(dr["HAZSZAM"].ToString()))
                                {
                                    hazszam = "'" + dr["HAZSZAM"].ToString() + "'";
                                }
                                else
                                {
                                    hazszam = "NULL";
                                }
                                if (dr["EPULET"] != null && !String.IsNullOrEmpty(dr["EPULET"].ToString()))
                                {
                                    epulet = "'" + dr["EPULET"].ToString() + "'";
                                }
                                else
                                {
                                    epulet = "NULL";
                                }
                                if (dr["LEPCSOHAZ"] != null && !String.IsNullOrEmpty(dr["LEPCSOHAZ"].ToString()))
                                {
                                    lepcsohaz = "'" + dr["LEPCSOHAZ"].ToString() + "'";
                                }
                                else
                                {
                                    lepcsohaz = "NULL";
                                }
                                if (dr["EMELET"] != null && !String.IsNullOrEmpty(dr["EMELET"].ToString()))
                                {
                                    emelet = "'" + dr["EMELET"].ToString() + "'";
                                }
                                else
                                {
                                    emelet = "NULL";
                                }
                                if (dr["AJTO"] != null && !String.IsNullOrEmpty(dr["AJTO"].ToString()))
                                {
                                    ajto = "'" + dr["AJTO"].ToString() + "'";
                                }
                                else
                                {
                                    ajto = "NULL";
                                }
                                if (dr["POSTAFIOK"] != null && !String.IsNullOrEmpty(dr["POSTAFIOK"].ToString()))
                                {
                                    postafiok = "'" + dr["POSTAFIOK"].ToString() + "'";
                                }
                                else
                                {
                                    postafiok = "NULL";
                                }
                                //BLG_1780
                                if (dr["UGYFELAZONOSITO"] != null && !String.IsNullOrEmpty(dr["UGYFELAZONOSITO"].ToString()))
                                {
                                    ugyfelazonosito = "'" + dr["UGYFELAZONOSITO"].ToString() + "'";
                                }
                                else
                                {
                                    ugyfelazonosito = "NULL";
                                }
                                #endregion
                                //fCommand.CommandText = "Select max(p.Id) from IMPORT_LEVEL p ";//kivanni
                                //t = fCommand.ExecuteScalar();
                                //fCommand.CommandText = String.Format("INSERT INTO IMPORT_LEVEL (ID,IKTATOSZAM,OBJ_ID,RAGSZAM,NEV,TELEPULES,CIM,IRSZ,SZOLGALTATASOK,ALLAPOT,RENDELTETESI_HELY,KULDEMENY_TIPUS,SULY,ORSZAG_NEV,ORSZAG_KOD)" +
                                //    "VALUES (" + (String.IsNullOrEmpty(t.ToString()) ? "1" : (Convert.ToInt32(t) + 1).ToString()) + ",{0},{1},{2},{3},{4},{5},{6},{7},'1',{8},{9},{10},{11},{12})",
                                //                        iktatoszam, obj_id, ragszam, nev,
                                //                        telepules, cim, irsz, Szolgaltatasok,
                                //                        rendeltetesi_hely, KuldemenyTipus,
                                //                        suly,
                                //                        orszagnev,orszagkod);
                                // BLG_984
                                //fCommand.CommandText = String.Format("INSERT INTO IMPORT_LEVEL (IKTATOSZAM,OBJ_ID,RAGSZAM,NEV,TELEPULES,CIM,IRSZ,SZOLGALTATASOK,ALLAPOT,RENDELTETESI_HELY,KULDEMENY_TIPUS,SULY,ORSZAG_NEV,ORSZAG_KOD,ID_USER)" +
                                //    "VALUES ({0},{1},{2},{3},{4},{5},{6},{7},'1',{8},{9},{10},{11},{12},0)",
                                //                        iktatoszam, obj_id, ragszam, nev,
                                //                        telepules, cim, irsz, Szolgaltatasok,
                                //                        rendeltetesi_hely, KuldemenyTipus,
                                //                        suly,
                                //                        orszagnev, orszagkod);
                                //BLG_1780
                                //fCommand.CommandText = String.Format("INSERT INTO IMPORT_LEVEL (IKTATOSZAM,OBJ_ID,RAGSZAM,NEV,TELEPULES,CIM,IRSZ,SZOLGALTATASOK,ALLAPOT,RENDELTETESI_HELY,KULDEMENY_TIPUS,SULY,ORSZAG_NEV,ORSZAG_KOD,ID_USER,KOZTERULET_JELLEG,HAZSZAM,EPULET,LEPCSOHAZ,EMELET,AJTO,POSTAFIOK)" +
                                //    "VALUES ({0},{1},{2},{3},{4},{5},{6},{7},'1',{8},{9},{10},{11},{12},0,{13},{14},{15},{16},{17},{18},{19})",
                                //                        iktatoszam, obj_id, ragszam, nev,
                                //                        telepules, cim, irsz, Szolgaltatasok,
                                //                        rendeltetesi_hely, KuldemenyTipus,
                                //                        suly,
                                //                        orszagnev, orszagkod,
                                //                        kozterulet_jelleg, hazszam, epulet, lepcsohaz, emelet, ajto, postafiok);

                                fCommand.CommandText = String.Format("INSERT INTO IMPORT_LEVEL (IKTATOSZAM,OBJ_ID,RAGSZAM,NEV,TELEPULES,CIM,IRSZ,SZOLGALTATASOK,ALLAPOT,RENDELTETESI_HELY,KULDEMENY_TIPUS,SULY,ORSZAG_NEV,ORSZAG_KOD,ID_USER,KOZTERULET_JELLEG,HAZSZAM,EPULET,LEPCSOHAZ,EMELET,AJTO,POSTAFIOK,UGYFELAZONOSITO)" +
                                    "VALUES ({0},{1},{2},{3},{4},{5},{6},{7},'1',{8},{9},{10},{11},{12},0,{13},{14},{15},{16},{17},{18},{19},{20})",
                                                        iktatoszam, obj_id, ragszam, nev,
                                                        telepules, cim, irsz, Szolgaltatasok,
                                                        rendeltetesi_hely, KuldemenyTipus,
                                                        suly,
                                                        orszagnev, orszagkod,
                                                        kozterulet_jelleg, hazszam, epulet, lepcsohaz, emelet, ajto, postafiok,ugyfelazonosito);

                                fCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }            
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
            Logger.Error(e.Message);
        }
        log.SpEnd(ExecParam, _ret);
        return;
    }
    public Result KuldemenyekFromWinpa(ExecParam ExecParam, Result result, string dbPath)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "KuldemenyekFromWinpa");
        //eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService().Postazas_Tomeges
        Result _ret = new Result();
        try
        {
            Result res = new Result();
            if (result != null && result.Ds != null && result.Ds.Tables != null && result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
            {
                string kuldemenyIds = "";
                foreach (DataRow dr in result.Ds.Tables[0].Rows)
                {
                    kuldemenyIds += dr["Id"].ToString() + "', '";
                }
                res = getWinpaAdatok(ExecParam, kuldemenyIds);
            }
            if (res != null && res.Ds != null && res.Ds.Tables.Count > 0 && res.Ds.Tables[0].Rows.Count >0)
            {
                using (var connection = new FbConnection(@"User=SYSDBA;Password=masterkey;Database=" + dbPath))
                {
                    connection.Open();
                    using (var sqlCommand = new FbCommand())
                    {
                        sqlCommand.Connection = connection;
                        string iktatoszamok = "";
                        // BUG_7243
                        List<string> iktatoszamokList = new List<string>();
                        int i = 0;
                        foreach (DataRow dr in res.Ds.Tables[0].Rows)
                        {
                            iktatoszamok += dr["IKTATOSZAM"].ToString() + "', '";
                            i++;
                            if (i>=1000)
                            {
                                iktatoszamokList.Add(iktatoszamok);
                                i = 0;
                                iktatoszamok = String.Empty;
                            }
                            
                        }
                        if (i>0)
                        {
                            iktatoszamokList.Add(iktatoszamok);
                        }
                        DataSet dsFull = new DataSet();
                        DataTable dtFull = null;
                        foreach (string iktatoszamok1000 in iktatoszamokList)
                        {
                            //sqlCommand.CommandText = "SELECT * from IMPORT_LEVEL where ALLAPOT='2' and IKTATOSZAM in ('" + iktatoszamok.Substring(0, iktatoszamok.Length - 3) + ")";
                            sqlCommand.CommandText = "SELECT * from IMPORT_LEVEL where ALLAPOT='2' and IKTATOSZAM in ('" + iktatoszamok1000.Substring(0, iktatoszamok1000.Length - 3) + ")";

                            DataSet ds = new DataSet();
                            FbDataAdapter adapter = new FbDataAdapter();
                            adapter.SelectCommand = sqlCommand;
                            adapter.Fill(ds);

                            if (ds.Tables.Count > 0)
                            {
                                DataTable table = ds.Tables[0];
                                table.Columns.Add("KuldemenyId", typeof(Guid));

                                if (dtFull == null)
                                {
                                    dtFull = table.Clone();

                                }

                                foreach (DataRow drWinpa in table.Rows)
                                {
                                    string iktatoszamWinpa = drWinpa["IKTATOSZAM"].ToString();

                                    foreach (DataRow dr in res.Ds.Tables[0].Rows)
                                    {
                                        string iktatoszamContentum = dr["IKTATOSZAM"].ToString();
                                        Guid kuldemenyId = (Guid)dr["Id"];

                                        if (iktatoszamContentum.Trim() == iktatoszamWinpa.Trim())
                                        {
                                            drWinpa["KuldemenyId"] = kuldemenyId;
                                            break;
                                        }
                                    }
                                }
                                foreach (DataRow dr in table.Rows)
                                {
                                    dtFull.ImportRow(dr);
                                }
                            }

                        }
                        // _ret.Ds = ds;
                        if (dtFull != null)
                        {
                            dsFull.Tables.Add(dtFull);
                        }
                        _ret.Ds = dsFull;
                    }
                }
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
            Logger.Error(e.Message);
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public void WinpaRefreshFeldolgozott(ExecParam ExecParam, string id, string dbPath)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "WinpaRefreshFeldolgozott");
        Result res = new Result();
        try
        {
            
            if (!String.IsNullOrEmpty(id))
            {
                //string Ids = "";
                //foreach (DataRow dr in result.Ds.Tables[0].Rows)
                //{
                //    Ids += dr["Id"].ToString() + "', '";
                //}
                using (var connection = new FbConnection(@"User=SYSDBA;Password=masterkey;Database=" + dbPath))
                {
                    connection.Open();
                    using (var sqlCommand = new FbCommand())
                    {
                        sqlCommand.Connection = connection;
                        sqlCommand.CommandText = "UPDATE IMPORT_LEVEL set ALLAPOT='3' where ID = '" + id + "'";//in ('" + Ids.Substring(0, Ids.Length - 3) + ")";
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
        }
        catch (SqlException e)
        {
            res.ErrorCode = e.ErrorCode.ToString();
            res.ErrorMessage = e.Message;
            Logger.Error(e.Message);
        }
        log.SpEnd(ExecParam, res);
        return;
    }

    private Result getWinpaAdatok(ExecParam ExecParam, string ids)
    {
        Result _ret = new Result();
        string where = null;
        if (!String.IsNullOrEmpty(ids))
            where = "EREC_KuldKuldemenyek.Id in ('" + ids.Substring(0, ids.Length - 3) + ")";
        else
            return _ret;
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_GetWinpaAdatok");

        
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_GetWinpaAdatok]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where"].Value = where;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
            Logger.Error(e.Message);
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }



    private void getKuldemenyTipusok(DataRow dr, ExecParam ExecParam, out string suly, out string kuldemenyTipus)
    {
        
        if (!String.IsNullOrEmpty(dr["Elsobbsegi"].ToString()) && dr["Elsobbsegi"].ToString() == "1")
        {
            if (!String.IsNullOrEmpty(dr["KimenoKuldemenyFajta"].ToString()))
            { 
                foreach(Tuple<String, String, String> item in KuldemenyTipusokClass.ElsobbsegiKuldemenyTipusokWithSuly)
                {
                    if(item.Item1.Equals(dr["KimenoKuldemenyFajta"].ToString()))
                    {
                        suly = item.Item2;
                        kuldemenyTipus ="'"+ item.Item3+"'";
                        return;
                    }
                }
            }
        }
        else
        {
            foreach (Tuple<String, String, String> item in KuldemenyTipusokClass.ElsobbsegiKuldemenyTipusokWithSuly)
            {
                if (item.Item1.Equals(dr["KimenoKuldemenyFajta"].ToString()))
                {
                    suly = item.Item2;
                    kuldemenyTipus = "'" + item.Item3 + "'";
                    return;
                }
            }
        }
        suly = "NULL";
        kuldemenyTipus = "NULL";
    }

    private static string EscapeSqlString(string text)
    {
        if (!String.IsNullOrEmpty(text))
        {
            return text.Replace("'", "''");
        }

        return text;
    }
}
