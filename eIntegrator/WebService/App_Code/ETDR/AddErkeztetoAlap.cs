﻿using System.Data;
using System.Xml.Serialization;
using CommonConstants = Contentum.eIntegrator.WebService.ETDR.Constants;

namespace Contentum.eIntegrator.WebService.ETDR.AddErkeztetoAlap
{
    #region Parameters

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Add_Erkezteto_Alap")]
    public class Add_Erkezteto_Alap
    {
        // Az ÉTDR felhasználói neve
        [XmlElement("LoginNev")]
        public string LoginNev;

        // A bejelentkezésnél kapott azonosító (utoljára kapott azonosító)
        [XmlElement("LoginAzon")]
        public string LoginAzon;

        // Az ÉTDR alrendszer azonosítója
        [XmlElement("AlrendszAzon")]
        public int AlrendszAzon;

        // A típus egyedi szöveges azonosítója(az irattípusnál a rendszergazda által megadott szöveges azonosító)
        [XmlElement("Ikttip_Azon")]
        public string Ikttip_Azon;

        // Küldő hatóság vagy személy neve
        [XmlElement("Nev")]
        public string Nev;

        // Küldő hatóság vagy személy irányítószáma
        [XmlElement("Irsz")]
        public string Irsz;

        // Küldő hatóság vagy személy települése(amennyiben nincs, vagy nem ÉTDR-en belül küldték, akkor az Építési tevékenységgel érintett telek települése
        [XmlElement("Telepules")]
        public string Telepules;

        // Küldő hatóság vagy személy címe
        [XmlElement("Utca")]
        public string Utca;

        // Küldő hatóság vagy személy házszáma
        [XmlElement("Hazszam")]
        public string Hazszam;

        // Az iratkezelő rendszerbe érkezés dátuma, vagyis az érkeztetés dátuma, pl. 2013.09.06
        [XmlElement("BeerkezesDate")]
        public string BeerkezesDate;

        // A kézbesítés, vagy a beérkezés típusának egyedi, belső azonosítója (az ÉTDR minidig -1-et ad át benne)
        [XmlElement("KezbAzon")]
        public int KezbAzon;

        // A postakönyv egyedi, belső azonosítója(az ÉTDR minidig -1-et ad át benne)
        [XmlElement("Postakonyv_Azon")]
        public int Postakonyv_Azon;

        // ÉTDR-be bejelentkezett felhasználó iktatórendszeri azonosítója
        [XmlElement("Ugyint_Id")]
        public int Ugyint_Id;

        // Űrlapról érkezett az ügy? (az ÉTDR minidig false-t ad át benne)
        [XmlElement("Urlap")]
        public bool Urlap;

        // Az irat iránya
        [XmlElement("bejovo")]
        public bool bejovo;

        // A csatolt dokumentumok. A tömb első eleme az űrlap képe (az ÉTDR nem ad át dokumentumokat az iktatórendszernek, így ez mindig üres)
        [XmlElement("DOC")]
        public byte[][] DOC;

        // Az irathoz csatolt dokumentumok fájlneve(az ÉTDR nem ad át dokumentumokat az iktatórendszernek, így ez mindig üres)
        [XmlElement("fajlnev")]
        public string[] fajlnev;

        // A megjelenítéshez szükséges dokumentum típus(az ÉTDR nem ad át dokumentumokat az iktatórendszernek, így ez mindig üres)
        [XmlElement("tipus")]
        public string[] tipus;

        // Az űrlap adattartalma(az ÉTDR nem ad át űrlapokat, így ez mindig üres)
        [XmlElement("XML")]
        public byte[] XML;

        // Az űrlap digitális aláírás(az ÉTDR nem ad át digitális aláírást, így ez mindig üres)
        [XmlElement("ALAIRAS")]
        public byte[] ALAIRAS;
    }

    #endregion Parameters

    #region Response

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Add_Erkezteto_AlapResponse")]
    public class Add_Erkezteto_AlapResponse
    {
        [XmlElement(Order = 0)]
        public string ErrMess;

        [XmlElement(Order = 1)]
        public DataSet Add_Erkezteto_AlapResult;
    }

    #endregion Response
}