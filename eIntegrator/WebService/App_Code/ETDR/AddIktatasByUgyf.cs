﻿using System;
using System.Data;
using System.Xml.Serialization;
using CommonConstants = Contentum.eIntegrator.WebService.ETDR.Constants;

namespace Contentum.eIntegrator.WebService.ETDR.AddIktatasByUgyf
{

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Connection")]
    public class Connection
    {
        // Az ÉTDR alrendszer azonosítója
        [XmlElement("AlrendszAzon")]
        public int AlrendszAzon;

        // A bejelentkezésnél kapott azonosító (utoljára kapott azonosító)
        [XmlElement("LoginAzon")]
        public string LoginAzon;

        // Az ÉTDR felhasználói neve
        [XmlElement("UserName")]
        public string UserName;

        // A hibaüzenet
        [XmlElement("Err")]
        public string Err;
    }

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Iktatas")]
    public class Iktatas : Connection
    {
        // Az iktatás egyedi, belső azonosítója.Ha kivan töltve a rendszer egy alszámos iktatás kezdeményez.
        // Ha nincs kitöltve, akkor főszámos iktatás történik.
        // Iktatás után ez a mező tartalmazza az új iktatás egyedi azonosítóját
        [XmlElement("IktId")]
        public int IktId;

        // A típus egyedi szöveges azonosítója (az irattípusnál a rendszergazda által megadott szöveges azonosító)
        [XmlElement("Ikttip_Azon")]
        public string Ikttip_Azon;

        // Az irat iránya
        [XmlElement("Bejovo")]
        public bool Bejovo;

        // Az irat keletkezésének dátuma
        [XmlElement("BeerkDatum")]
        public string BeerkDatum;

        // ÉTDR-be bejelentkezett felhasználó iktatórendszeri azonosítója
        [XmlElement("UgyintId")]
        public int UgyintId;

        // A tárgy egyedi, belső azonosítója(az ÉTDR minidig -1-et ad át benne)
        [XmlElement("TargyId")]
        public int TargyId;

        // Az altárgy egyedi, belső azonosítója(az ÉTDR minidig -1-et ad át benne)
        [XmlElement("AlTargyId")]
        public int AlTargyId;

        // Az iktatás jellegének egyedi azonosítója(az ÉTDR minidig -1-et ad át benne)
        [XmlElement("IktJelleg")]
        public int IktJelleg;

        // Az irat tárgya
        [XmlElement("Megjegyzes")]
        public string Megjegyzes;

        // Hivatkozási szám (az ÉTDR minidig üresen hagyja)
        [XmlElement("Hivszam")]
        public string Hivszam;

        // Az iktatás főügyfele
        [XmlElement("Ugyfel")]
        public UgyfelStruct Ugyfel;

        // Az iktatás többes ügyfelei, ha vannak
        [XmlElement("TobbUgyfel")]
        public ArrayOfUgyfelStruct TobbUgyfel;

        // Az iktatáshoz rendelendő kiegészítő információk(az ÉTDR minidig üresen hagyja)
        [XmlElement("KiegInfo")]
        public ArrayOfKiegStruct KiegInfo;

        // Beérkezés azonosító(az ÉTDR minidig -1-et ad át benne)
        [XmlElement("BeerkAzon")]
        public int BeerkAzon;

        // A mellékletek száma
        [XmlElement("Melleklet")]
        public int Melleklet;

        // Az új iktatás iktatószáma, rögzítés után lesz értéke
        [XmlElement("Iktatoszam")]
        public string Iktatoszam;

        // A csatolt dokumentumok(az ÉTDR nem ad át dokumentumokat az iktatórendszernek, így ez mindig üres)
        [XmlElement("CsatoltDokuk")]
        public ArrayOfCsatoltFile CsatoltDokuk;

        // Az érkeztetés azonosítója, amellyel az iktatást össze kell kapcsolni 
        [XmlElement("Erkazon")]
        public string Erkazon;

        // Az intézkedés módjának azonosítója(az ÉTDR minidig -1-et ad át benne)
        [XmlElement("IntezMod")]
        public int IntezMod;

        // Érkeztetés feladásának ellenőrzése(az ÉTDR minidig true-t ad át benne)
        [XmlElement("ErkFeladNemEllenor")]
        public bool ErkFeladNemEllenor;
    }

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "UgyfelStruct")]
    public class UgyfelStruct
    {
        // Az ügyfél egyedi azonosítója. Ha az ügyfél már fel lett küldve az iktatórendszernek,
        // akkor az azonosítója megy benne vissza, és akkor a leíró adatok üresek, ha még nem volt felküldve, akkor -1 van benne
        [XmlElement("UgyfelId")]
        public int UgyfelId;

        // Az ügyfél megnevezése
        [XmlElement("UgyfNev")]
        public string UgyfNev;

        // Az ügyfél típuskódja (0 – természetes személy, 1 – jogi személy)
        [XmlElement("UgyfTip")]
        public int UgyfTip;

        // Az ügyfél halott, vagy megszűnt, vagy     sem (az ÉTDR minidig false-t ad át benne)
        [XmlElement("Halott")]
        public bool Halott;

        // Az ügyfél születéskori neve
        [XmlElement("SzulNev")]
        public string SzulNev;

        // Az ügyfél születési dátuma, pl. 1987.09.06
        [XmlElement("SzulDat")]
        public string SzulDat;

        // Az ügyfél születési helye
        [XmlElement("SzulHely")]
        public string SzulHely;

        // Az ügyfél anyja neve
        [XmlElement("AnyjaNeve")]
        public string AnyjaNeve;

        // Kapcsolattartó neve (az ÉTDR minidig üresen hagyja)
        [XmlElement("Kapcst")]
        public string Kapcst;

        // Az ügyfélhez fűzött tetszőleges megjegyzés (az ÉTDR minidig üresen hagyja)
        [XmlElement("UgyfMegj")]
        public string UgyfMegj;

        //  Az ügyfél címének egyedi azonosítója. Ha az ügyfél már fel lett küldve az iktatórendszernek, akkor az azonosítója megy benne vissza,
        // és akkor a leíró adatok üresek, ha még nem volt felküldve, akkor -1 van benne
        [XmlElement("UgyfCimId")]
        public int UgyfCimId;

        // Az ügyfél címének irányítószáma
        [XmlElement("Irsz")]
        public string Irsz;

        // Az ügyfél címének település megnevezése
        [XmlElement("TelepNev")]
        public string TelepNev;

        // Az ügyfél címének utcaneve
        [XmlElement("UtcaNev")]
        public string UtcaNev;

        // Az ügyfél címének házszáma
        [XmlElement("HazSzam")]
        public string HazSzam;

        // Az ügyfél címéhez tartozó megjegyzés (az ÉTDR minidig üresen hagyja)
        [XmlElement("CimMegj")]
        public string CimMegj;

        // Az egyedi azonosító belső típuskódja (az ÉTDR minidig -1-et ad át benne)
        [XmlElement("AzonszAzon")]
        public int AzonszAzon;

        // ÉTDR azonosító(negatív tartományban) – ezt az adatot az ÉTDR egy az egyben várja vissza az iktatórendszertől
        [XmlElement("KulsoAzon")]
        public int KulsoAzon;

        // Az egyedi azonosító belső típuskódjához tartozó érték(az ÉTDR minidig üresen hagyja)
        [XmlElement("AzonszErtek")]
        public int AzonszErtek;

        // Kiegészítő információk tömbje(az ÉTDR minidig üresen hagyja)

        [XmlElement("KiegInfo")]
        public KiegStruct[] KiegInfo;

        // Az ügyfél titulusának belső azonosítója (az ÉTDR minidig -1-et ad át benne)
        [XmlElement("UgyfTitKod")]
        public int UgyfTitKod;
    }

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "ArrayOfUgyfelStruct")]

    public class ArrayOfUgyfelStruct
    {
        [XmlElement("UgyfelStruct")]
        public UgyfelStruct[] UgyfelStruct;
    }

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "KiegStruct")]
    public class KiegStruct
    {
        // A kiegészítő információ kódja (az ÉTDR minidig -1-et ad át benne)
        [XmlElement("AzonszAzon")]
        public int AzonszAzon;

        // A kiegészítő információ értéke (az ÉTDR minidig üresen hagyja)
        [XmlElement("AzonszErtek")]
        public string AzonszErtek;
    }

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "ArrayOfKiegStruct")]
    public class ArrayOfKiegStruct
    {
        [XmlElement("KiegStruct")]
        public KiegStruct[] KiegStruct;
    }

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "CsatoltFile")]
    public class CsatoltFile
    {
        // Fájlnév (az ÉTDR nem ad át dokumentumokat az iktatórendszernek)
        [XmlElement("Filenev")]
        public string Filenev;

        // MimeType(az ÉTDR nem ad át dokumentumokat az iktatórendszernek)
        [XmlElement("MimeType")]
        public string MimeType;

        //  Bináris adat (az ÉTDR nem ad át dokumentumokat az iktatórendszernek)
        [XmlElement("Data")]
        // base64Binary
        public byte[] Data;
    }

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "ArrayOfCsatoltFile")]
    public class ArrayOfCsatoltFile
    {
        [XmlElement("CsatoltFile")]
        public CsatoltFile[] CsatoltFile;
    }

    #region Parameters

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Add_Iktatas_By_Ugyf")]
    public class Add_Iktatas_By_Ugyf
    {
        // Iktatas
        [XmlElement("par")]
        public Iktatas Iktatas;

    }

    #endregion

    #region Response

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Add_Iktatas_By_UgyfResponse")]
    public class Add_Iktatas_By_UgyfResponse
    {
        // Sikeres volt-e az iktatás
        [XmlElement("Add_Iktatas_By_UgyfResult")]
        public bool Add_Iktatas_By_UgyfResult;

        // Az iktatás adatai
        [XmlElement("par")]
        public Iktatas Iktatas;
    }

    #endregion
}