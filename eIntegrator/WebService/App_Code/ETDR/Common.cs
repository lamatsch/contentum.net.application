﻿using System;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.ETDR
{
    public static class Constants {
        public const string DEFAULT_NAMESPACE = "http://tempuri.org/";
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class ResponseBase
    {
    }
}
