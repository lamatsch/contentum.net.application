﻿using System;
using System.Data;
using System.Xml.Serialization;
using CommonConstants = Contentum.eIntegrator.WebService.ETDR.Constants;

namespace Contentum.eIntegrator.WebService.ETDR.GetIntMod
{
    #region Parameters

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Get_IntMod")]
    public class Get_IntMod
    {
        // Az ÉTDR felhasználói neve
        [XmlElement("LoginNev", Order = 0)]
        public string LoginNev;

        // A bejelentkezésnél kapott azonosító (utoljára kapott azonosító)
        [XmlElement("LoginAzon", Order = 1)]
        public string LoginAzon ;

        // Az ÉTDR alrendszer azonosítója
        [XmlElement("AlrendszAzon", Order = 2)]
        public int AlrendszAzon;
    }

    #endregion

    #region Response

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Get_IntModResponse")]
    public class Get_IntModResponse
    {
        [XmlElement(Order = 0)]
        // A hibaüzenet, üres, ha nincs hiba
        public string ErrMess;

        [XmlElement(ElementName = "Get_IntmodResult", Order = 1)]
        // Fields in data set:
        // VALASZ_TIP_ID, int: Az ügyintézési mód azonosítója
        // VALASZ_TIP_NEV, string: Az ügyintézési mód neve
        public DataSet Get_IntmodResult;
    }

    #endregion
}

