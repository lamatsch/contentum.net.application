﻿using System;
using System.Data;
using System.Xml.Serialization;
using CommonConstants = Contentum.eIntegrator.WebService.ETDR.Constants;

namespace Contentum.eIntegrator.WebService.ETDR.GetUgyintezok
{
    #region Parameters

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Get_Ugyintezok")]
    public class Get_Ugyintezok
    {
        // Az ÉTDR felhasználói neve
        [XmlElement("LoginNev", Order = 0)]
        public string LoginNev;

        // A bejelentkezésnél kapott azonosító (utoljára kapott azonosító)
        [XmlElement("LoginAzon", Order = 1)]
        public string LoginAzon;

        // Az ÉTDR alrendszer azonosítója
        [XmlElement("AlrendszAzon", Order = 2)]
        public int AlrendszAzon;

        // A típus egyedi szöveges azonosítója (az ÉTDR mindig üresen hagyja)
        [XmlElement("Ikttip_Azon ", Order = 3)]
        public string Ikttip_Azon;
    }

    #endregion

    #region Response

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Get_UgyintezokResponse")]
    public class Get_UgyintezokResponse
    {
        // A hibaüzenet, üres, ha nincs hiba
        [XmlElement(Order = 0)]
        public string ErrMess;

        [XmlElement(ElementName = "Get_UgyintezokResult", Order = 1)]
        // Fields in data set:
        // FELH_ID, int: Az ügyintéző egyedi, belső azonosítója (amit majd pl. iktatásnál átad az ÉTDR az iktatórendszernek)
        // FELH_NEV, string: Az ügyintéző megnevezése
        public DataSet Get_UgyintezokResult;
    }

    #endregion
}

