﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for BaseIntETDRService
    /// </summary>
    public partial class BaseIntETDRService
    {
        #region PROPERTIES

        public virtual string TypeFullName { get; set; }
        public virtual ENUM_ETDR_SERVICETYPE ServiceType { get; set; }

        public string ContentumTechnikaiUser { get; set; }
        public string ContentumTechnikaiUserSzervezet { get; set; }
        public ExecParam ExecParamForServices { get; set; }

        #endregion PROPERTIES

        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        public BaseIntETDRService()
        {
            TypeFullName = this.GetType().FullName;
            ServiceType = ENUM_ETDR_SERVICETYPE.ALAPERTELMEZETT;

            ContentumTechnikaiUser = GetParameter(ENUM_ETDR_INT_PARAMETER_NAMES.ETDR_CONTENTUM_TECHNIKAI_USER.ToString());
            ContentumTechnikaiUserSzervezet = GetParameter(ENUM_ETDR_INT_PARAMETER_NAMES.ETDR_CONTENTUM_TECHNIKAI_USER_SZERVEZET.ToString());

            ExecParamForServices = new ExecParam()
            {
                Felhasznalo_Id = ContentumTechnikaiUser,
                LoginUser_Id = ContentumTechnikaiUserSzervezet,
                FelhasznaloSzervezet_Id = ContentumTechnikaiUserSzervezet,
            };
        }

        #region LOG

        /// <summary>
        /// LOG
        /// </summary>
        /// <param name="exception"></param>
        public void Log(Exception exception)
        {
            Logger.Debug(string.Format("SOURCE:{0}", TypeFullName), exception);
        }

        /// <summary>
        /// LOG
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void Log(string message, Exception exception)
        {
            Logger.Debug(string.Format("SOURCE:{0} MSG:{1}", TypeFullName, message), exception);
        }

        /// <summary>
        /// LOG
        /// </summary>
        /// <param name="message"></param>
        /// <param name="execParam"></param>
        /// <param name="result"></param>
        public void Log(string message, ExecParam execParam, Result result)
        {
            Logger.Debug(string.Format("SOURCE:{0} MSG{1}", TypeFullName, message), execParam, result);
        }

        /// <summary>
        /// LOG
        /// </summary>
        /// <param name="message"></param>
        /// <param name="result"></param>
        public void Log(string message, Result result)
        {
            Logger.Debug(string.Format("SOURCE:{0} MSG{1}", TypeFullName, message), this.ExecParamForServices, result);
        }

        /// <summary>
        /// LOG
        /// </summary>
        /// <param name="result"></param>
        public void Log(Result result)
        {
            Logger.Debug(string.Format("SOURCE:{0}", TypeFullName), this.ExecParamForServices, result);
        }

        #endregion LOG

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}", ServiceType.ToString());
        }
    }
}