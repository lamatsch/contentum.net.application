﻿using System;
using System.Web;
using System.Web.Caching;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for BaseIntETDRService
    /// </summary>
    public partial class BaseIntETDRService
    {
        #region CACHE

        #region PROPERTIES

        private static Cache cacheETDRLogin = HttpContext.Current.Cache;
        private static readonly object cacheLock = new object();
        private const int CacheRefreshPeriodDefaultInSec = 43200;

        public static Cache GetCacheETDR()
        {
            return cacheETDRLogin;
        }

        private static void SetCacheETDR(Cache value)
        {
            cacheETDRLogin = value;
        }

        #endregion PROPERTIES

        #region PROCEDURES

        /// <summary>
        /// GetCacheValue
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetCacheValue(string key)
        {
            if (GetCacheETDR() == null)
            {
                return null;
            }

            if (GetCacheETDR()[key] == null)
            {
                return null;
            }

            return GetCacheETDR()[key].ToString();
        }

        /// <summary>
        /// SetCacheValue
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetCacheValue(string key, string value)
        {
            if (GetCacheETDR() == null)
            {
                SetCacheETDR(HttpContext.Current.Cache);
            }

            if (GetCacheETDR()[key] == null)
            {
                string expireTimeInSecParam = GetParameter(ENUM_ETDR_INT_PARAMETER_NAMES.ETDR_LOGINIDEXPIRYTIMEINSECONDS.ToString());
                int expireTimeInSec;
                if (!int.TryParse(expireTimeInSecParam, out expireTimeInSec))
                {
                    expireTimeInSec = CacheRefreshPeriodDefaultInSec;
                }
                lock (cacheLock)
                {
                    GetCacheETDR().Insert(
                        key,
                        value,
                        null,
                        DateTime.Now.AddSeconds(expireTimeInSec),
                        Cache.NoSlidingExpiration,
                        CacheItemPriority.NotRemovable,
                        null);
                }
            }
            else
            {
                lock (cacheLock)
                {
                    GetCacheETDR()[key] = value;
                }
            }
        }

        public void RemoveCacheKey(string key)
        {
            if (GetCacheETDR() == null)
            {
                SetCacheETDR(HttpContext.Current.Cache);
            }

            if (GetCacheETDR()[key] != null)
            {
                GetCacheETDR().Remove(key);
            }
        }

        #endregion PROCEDURES

        #endregion CACHE
    }
}