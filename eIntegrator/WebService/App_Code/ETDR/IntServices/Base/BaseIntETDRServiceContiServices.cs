﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.BaseUtility;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    public partial class BaseIntETDRService
    {
        #region CONTI SERVICES

        #region INT_PARAMETEREK
        public string GetParameter(string nev)
        {
            return IntegratorParameterek.GetParameter(nev);
        }
        #endregion INT_PARAMETEREK

        #endregion CONTI SERVICES
    }
}