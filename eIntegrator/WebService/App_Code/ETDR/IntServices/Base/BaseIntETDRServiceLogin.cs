﻿using Contentum.eBusinessDocuments;
using System;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for BaseETDRServiceLogin
    /// </summary>
    public partial class BaseIntETDRService
    {
        //token ellenőrzése és lejárat beállítása
        protected void CheckLogin(string loginNev, string loginAzon)
        {
            IntServiceLogin loginService = new IntServiceLogin();
            bool ok = loginService.CheckLoginToken(loginAzon);

            if (!ok)
                throw new Exception(ETDRErrorMessages.CHECKLOGIN_ERROR_INVALID_TOKEN);

            loginService.UpdateLoginIDExpiry();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="felhasznaloId"></param>
        /// <returns></returns>
       protected ExecParam GetExecParamFromByFelhasznalo(string felhasznaloId)
        {
            string felhasznaloSzervezetId = ETDRConentumWrapper.GetFelhasznaloSzervezet(this.ExecParamForServices, felhasznaloId);
            return new ExecParam()
            {
                Felhasznalo_Id = felhasznaloId,
                FelhasznaloSzervezet_Id = felhasznaloSzervezetId
            };
        }
    }
}