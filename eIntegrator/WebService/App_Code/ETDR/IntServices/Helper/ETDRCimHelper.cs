﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using AddIktatasByUgyfData = Contentum.eIntegrator.WebService.ETDR.AddIktatasByUgyf;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for ETDRCimHelper
    /// </summary>
    public class ETDRCimHelper
    {
        static KRT_Cimek GetCim(AddIktatasByUgyfData.UgyfelStruct ugyfel)
        {
            KRT_Cimek cim = new KRT_Cimek();

            cim.IRSZ = ETDRConverter.ConvertToBOString(ugyfel.Irsz);
            cim.TelepulesNev = ETDRConverter.ConvertToBOString(ugyfel.TelepNev);
            cim.KozteruletNev = ETDRConverter.ConvertToBOString(ugyfel.UtcaNev);
            cim.Hazszam = ETDRConverter.ConvertToBOString(ugyfel.HazSzam);
            cim.Forras = KodTarak.Cim_Forras.ETDR;
            cim.Tipus = KodTarak.Cim_Tipus.Postai;
            //Kötelező mező
            cim.Kategoria = KodTarak.Cim_Kategoria.K;

            return cim;
        }

        public static string InsertCim(ExecParam execParam, AddIktatasByUgyfData.UgyfelStruct ugyfel)
        {
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            KRT_Cimek cim = GetCim(ugyfel);
            Result result = service.InsertWithFKResolution(execParam, cim);
            result.CheckError();
            return result.Uid;
        }

        public static string GetCimSTR(ExecParam execParam, string cimId)
        {
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam execParamCimGet = execParam.Clone();
            execParamCimGet.Record_Id = cimId;
            Result result = service.Get(execParamCimGet);
            result.CheckError();
            KRT_Cimek cim = result.Record as KRT_Cimek;
            return ETDRCimHelper.GetAppendedCim(cim);
        }

        class Cim
        {
            private string text;

            public string Text
            {
                get { return text; }
                set { text = value; }
            }
            private string delimeter;

            public string Delimeter
            {
                get { return delimeter; }
                set { delimeter = value; }
            }
            public Cim()
            {
            }
            public Cim(string text)
            {
                Text = text;
                Delimeter = ", ";
            }
            public Cim(string text, string delimeter)
            {
                Text = text;
                Delimeter = delimeter;
            }
        }

        class CimCollection : IEnumerable
        {
            private List<Cim> items;
            public CimCollection()
            {
                items = new List<Cim>();
            }
            public void Add(string text, string delimeter)
            {
                items.Add(new Cim(text, delimeter));
            }
            public void Add(string text)
            {
                items.Add(new Cim(text));
            }

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return items.GetEnumerator();
            }

            #endregion
        }

        static string GetAppendedCim(KRT_Cimek krt_cimek)
        {
            CimCollection cim = new CimCollection();
            StringBuilder text = new StringBuilder("");
            string delimeter = ", ";
            string delimeterSpace = " ";


            try
            {
                switch (krt_cimek.Tipus)
                {
                    case KodTarak.Cim_Tipus.Postai:
                        cim.Add(krt_cimek.OrszagNev, delimeter);
                        cim.Add(krt_cimek.IRSZ, delimeter);
                        cim.Add(krt_cimek.TelepulesNev, delimeter);
                        // BLG_1347
                        //cim.Add(krt_cimek.KozteruletNev, delimeterSpace);
                        //cim.Add(krt_cimek.KozteruletTipusNev, delimeterSpace);
                        if (String.IsNullOrEmpty(krt_cimek.KozteruletNev) && !String.IsNullOrEmpty(krt_cimek.HRSZ))
                        {
                            cim.Add("HRSZ.", delimeterSpace);
                            cim.Add(krt_cimek.HRSZ, delimeterSpace);
                        }
                        else
                        {
                            cim.Add(krt_cimek.KozteruletNev, delimeterSpace);
                            cim.Add(krt_cimek.KozteruletTipusNev, delimeterSpace);
                            string hazszam = krt_cimek.Hazszam;
                            string hazszamIg = krt_cimek.Hazszamig;
                            string hazszamBetujel = krt_cimek.HazszamBetujel;
                            if (!String.IsNullOrEmpty(hazszamIg))
                                hazszam += "-" + hazszamIg;
                            if (!String.IsNullOrEmpty(hazszamBetujel))
                                hazszam += "/" + hazszamBetujel;
                            cim.Add(hazszam, delimeter);
                            string lepcsohaz = krt_cimek.Lepcsohaz;
                            if (!String.IsNullOrEmpty(lepcsohaz))
                                lepcsohaz += " lépcsőház";
                            cim.Add(lepcsohaz, delimeter);
                            string szint = krt_cimek.Szint;
                            if (!String.IsNullOrEmpty(szint))
                                szint += ". emelet";
                            cim.Add(szint, delimeter);
                            string ajto = krt_cimek.Ajto;
                            if (!String.IsNullOrEmpty(ajto))
                            {
                                string ajtoBetujel = krt_cimek.AjtoBetujel;
                                if (!String.IsNullOrEmpty(ajtoBetujel))
                                    ajto += "/" + ajtoBetujel;
                                ajto += " ajtó";
                            }
                            cim.Add(ajto, delimeter);
                        }
                        break;

                    case KodTarak.Cim_Tipus.Egyeb:
                        string Cim = krt_cimek.CimTobbi;
                        cim.Add(Cim, delimeter);
                        break;

                    default:
                        goto case KodTarak.Cim_Tipus.Egyeb;
                }

                string lastDelimeter = "";

                foreach (Cim item in cim)
                {
                    if (!String.IsNullOrEmpty(item.Text))
                    {
                        text.Append(item.Text);
                        text.Append(item.Delimeter);
                        lastDelimeter = item.Delimeter;
                    }
                }

                if (text.Length >= lastDelimeter.Length)
                    text = text.Remove(text.Length - lastDelimeter.Length, lastDelimeter.Length);
            }
            catch (Exception e)
            {
                return "";
            }

            return text.ToString();
        }
    }
}