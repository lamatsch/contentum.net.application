﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for ETDRConentumWrapper
    /// </summary>
    public class ETDRConentumWrapper
    {
        public static string GetFelhasznaloSzervezet(ExecParam execParam, string felhasznaloId)
        {
            try
            {
                KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch search = new KRT_CsoportTagokSearch();
                search.Csoport_Id_Jogalany.Value = felhasznaloId;
                search.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                Result result = service.GetAll(execParam, search);

                result.CheckError();

                if (result.Rows().Count == 0)
                    throw new Exception("A felhasználóhoz nem tartozik szervezet!");

                if (result.Rows().Count > 1)
                {
                    string munkahely_id = GetFelhasznaloMunkahely(execParam, felhasznaloId);

                    if (!String.IsNullOrEmpty(munkahely_id))
                    {
                        foreach (DataRow row in result.Rows())
                        {
                            if (munkahely_id.Equals(row["Csoport_Id"].ToString(), StringComparison.InvariantCultureIgnoreCase))
                                return munkahely_id;
                        }
                    }
                }

                return result.Rows()[0]["Csoport_Id"].ToString();
            }
            catch (Exception ex)
            {
                Logger.Error("GetFelhasznaloSzervezet hiba", ex);
                throw new Exception(ETDRErrorMessages.USER_ERROR_GROUP_NOT_FOUND);
            }
        }

        static string GetFelhasznaloMunkahely(ExecParam execParam, string felhasznaloId)
        {
            try
            {
                KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                ExecParam execParamGet = execParam.Clone();
                execParamGet.Record_Id = felhasznaloId;
                Result result = service.Get(execParamGet);
                result.CheckError();
                KRT_Felhasznalok felhasznalo = result.Record as KRT_Felhasznalok;
                return felhasznalo.Partner_Id_Munkahely;
            }
            catch (Exception ex)
            {
                Logger.Error("GetFelhasznaloMunkahely hiba", ex);
            }

            return String.Empty;
        }

        public static string GetIktatokonyvId(ExecParam execParam, string iktatohely)
        {
            try
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();

                search.Ev.Filter(DateTime.Now.Year.ToString());
                search.Iktatohely.Filter(iktatohely);
                search.IktatoErkezteto.Filter("I");

                Result result = service.GetAll(execParam, search);

                result.CheckError();

                if (result.Rows().Count == 0)
                    throw new Exception("Az iktatókönyv nem található!");

                return result.Rows()[0]["Id"].ToString();
            }
            catch (Exception ex)
            {
                Logger.Error("GetIktatokonyvId hiba", ex);
                throw new Exception(ETDRErrorMessages.IKTATOKONYV_ERROR);
            }
        }

        public static string GetIrattariTetelId(ExecParam execParam, string irattariTetelszam)
        {
            try
            {
                EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();

                search.IrattariTetelszam.Filter(irattariTetelszam);

                Result result = service.GetAll(execParam, search);

                result.CheckError();

                if (result.Rows().Count == 0)
                    throw new Exception("Az irattári tételszám nem található!");

                return result.Rows()[0]["Id"].ToString();
            }
            catch (Exception ex)
            {
                Logger.Error("GetIrattariTetelId hiba", ex);
                throw new Exception(ETDRErrorMessages.IKTATOKONYV_ERROR);
            }
        }

        public static string GetErkeztetoKonyvId(ExecParam execParam, string megkulJelzes)
        {
            try
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                EREC_IraIktatoKonyvekSearch src = new EREC_IraIktatoKonyvekSearch();
                src.Ev.Value = DateTime.Now.Year.ToString();
                src.Ev.Operator = Query.Operators.equals;

                src.MegkulJelzes.Value = megkulJelzes;
                src.MegkulJelzes.Operator = Query.Operators.equals;

                src.IktatoErkezteto.Value = "E";
                src.IktatoErkezteto.Operator = Query.Operators.equals;
                src.TopRow = 1;

                Result result = service.GetAll(execParam, src);

                result.CheckError();

                if (result.Rows().Count == 0)
                    throw new Exception(ETDRErrorMessages.ERROR_ERKEZTETOKONYV);

                return result.Rows()[0]["Id"].ToString();
            }
            catch (Exception ex)
            {
                Logger.Error("GetIktatokonyvId hiba", ex);
                throw new Exception(ETDRErrorMessages.ERROR_ERKEZTETOKONYV);
            }
        }

        public static string GetHatarido(ExecParam execParam, DateTime ugyintezesKezdete, string ugykorId, string ugytipus)
        {
            try
            {
                EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

                Result result = service.GetIratMetaDefinicioByUgykorUgytipus(execParam, ugykorId, ugytipus);
                result.CheckError();

                if (result.Record == null)
                {
                    return GetDefaultHatarido(execParam, ugyintezesKezdete);
                }

                EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;
                string ugyiratIntezesiIdo = erec_IratMetaDefinicio.UgyiratIntezesiIdo.ToString();
                string idoegyseg = erec_IratMetaDefinicio.Idoegyseg.ToString();

                int nUgyiratIntezesiIdo;
                bool isIntegerUgyiratIntezesiIdo = Int32.TryParse(ugyiratIntezesiIdo, out nUgyiratIntezesiIdo);

                if (!isIntegerUgyiratIntezesiIdo || nUgyiratIntezesiIdo <= 0)
                {
                    return GetDefaultHatarido(execParam, ugyintezesKezdete);
                }

                String errorMessage = String.Empty;
                DateTime hatarido = Contentum.eUtility.Sakkora.GetUgyUgyintezesHataridoGlobal(execParam, ugyintezesKezdete, ugyiratIntezesiIdo, idoegyseg, out errorMessage);

                if (!String.IsNullOrEmpty(errorMessage))
                    throw new Exception(errorMessage);

                return hatarido.ToShortDateString();
            }
            catch (Exception ex)
            {
                Logger.Error("GetHatarido hiba", ex);
                throw new Exception(ETDRErrorMessages.HATARIDO_ERROR);
            }
        }

        public static string GetDefaultHatarido(ExecParam execParam, DateTime ugyintezesKezdete)
        {
            String errorMessage = String.Empty;
            DateTime hatarido = Contentum.eUtility.Sakkora.GetUgyUgyintezesHataridoGlobal(execParam, ugyintezesKezdete, "0", "0", out errorMessage);

            if (!String.IsNullOrEmpty(errorMessage))
                throw new Exception(errorMessage);

            return hatarido.ToShortDateString();
        }

        /// <summary>
        /// FindPartner
        /// </summary>
        /// <param name="nev"></param>
        /// <param name="irsz"></param>
        /// <param name="telepules"></param>
        /// <param name="utca"></param>
        /// <param name="hazszam"></param>
        /// <returns></returns>
        public static Result FindPartner(ExecParam execParam, string nev, string irsz, string telepules, string utca, string hazszam)
        {
            if (string.IsNullOrEmpty(nev))
                return null;

            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();

            partnerekSearch.Nev.Value = nev;
            partnerekSearch.Nev.Operator = Query.Operators.equals;

            KRT_CimekSearch cimekSearch = new KRT_CimekSearch();
            if (!string.IsNullOrEmpty(irsz))
            {
                cimekSearch.IRSZ.Value = irsz;
                cimekSearch.IRSZ.Operator = Query.Operators.equals;
            }
            if (!string.IsNullOrEmpty(telepules))
            {
                cimekSearch.TelepulesNev.Value = telepules;
                cimekSearch.TelepulesNev.Operator = Query.Operators.equals;
            }
            if (!string.IsNullOrEmpty(utca))
            {
                cimekSearch.KozteruletNev.Value = utca;
                cimekSearch.KozteruletNev.Operator = Query.Operators.equals;
            }
            if (!string.IsNullOrEmpty(hazszam))
            {
                cimekSearch.Hazszam.Value = hazszam;
                cimekSearch.Hazszam.Operator = Query.Operators.equals;
            }
            Result resultPartner = eAdminService.ServiceFactory.GetKRT_PartnerekService()
                .FindByPartnerAndCim(execParam, partnerekSearch, cimekSearch);
            if (resultPartner.IsError)
            {
                throw new ResultException(resultPartner);
            }

            return resultPartner;
        }

        /// <summary>
        /// Add ETDR Cim
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="irsz"></param>
        /// <param name="telepules"></param>
        /// <param name="utca"></param>
        /// <param name="hazszam"></param>
        /// <returns></returns>
        public static Result AddCim(ExecParam execParam, string irsz, string telepules, string utca, string hazszam)
        {
            KRT_Cimek cim = new KRT_Cimek();
            if (!string.IsNullOrEmpty(irsz))
            {
                cim.IRSZ = irsz;
            }
            if (!string.IsNullOrEmpty(telepules))
            {
                cim.TelepulesNev = telepules;
            }
            if (!string.IsNullOrEmpty(utca))
            {
                cim.KozteruletNev = utca;
            }
            if (!string.IsNullOrEmpty(hazszam))
            {
                cim.Hazszam = hazszam;
            }
            cim.Forras = KodTarak.Cim_Forras.ETDR;
            cim.Tipus = KodTarak.Cim_Tipus.Postai;
            cim.Kategoria = "K";

            KRT_CimekService servcie = eAdminService.ServiceFactory.GetKRT_CimekService();
            Result result = servcie.InsertWithFKResolution(execParam, cim);
            return result;
        }
        /// <summary>
        /// GetIrat
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static EREC_IraIratok GetIrat(ExecParam execParam, string id)
        {
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam xpm = execParam.Clone();
            xpm.Record_Id = id;

            Result result = service.Get(xpm);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            EREC_IraIratok item = (EREC_IraIratok)result.Record;
            item.Updated.SetValueAll(false);
            item.Base.Updated.SetValueAll(false);
            item.Base.Updated.Ver = true;

            return item;
        }

        public static string GetKuldemenyId(ExecParam execParam, string azonosito)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            search.Azonosito.Filter(azonosito);
            Result result = service.GetAll(execParam, search);

            result.CheckError();

            if (result.Rows().Count == 0)
                throw new Exception(ETDRErrorMessages.KULDEMENY_NOT_EXISTS);

            return result.Rows()[0]["Id"].ToString();
        }

        public static void AddTovabbiBekuldok(ExecParam execParam, string kuldemenyId, List<ETDRPartnerCimObject> partnerCimek)
        {
            try
            {
                EREC_KuldBekuldokService service = eRecordService.ServiceFactory.GetEREC_KuldBekuldokService();
                foreach (ETDRPartnerCimObject partnerCim in partnerCimek)
                {
                    EREC_KuldBekuldok erec_KuldBekuldok = new EREC_KuldBekuldok();
                    erec_KuldBekuldok.KuldKuldemeny_Id = kuldemenyId;
                    erec_KuldBekuldok.Partner_Id_Bekuldo = partnerCim.PartnerId;
                    erec_KuldBekuldok.NevSTR = partnerCim.PartnerSTR;
                    erec_KuldBekuldok.PartnerCim_Id_Bekuldo = partnerCim.CimId;
                    erec_KuldBekuldok.CimSTR = partnerCim.CimSTR;

                    Result res = service.Insert(execParam, erec_KuldBekuldok);
                    res.CheckError();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("AddTovabbiBekuldok hiba" , ex);
                throw new Exception(ETDRErrorMessages.KULDEMENY_TOVABBI_BEKULDOK_ERROR);
            }
        }
    }
}