﻿namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    public enum ENUM_ETDR_SERVICETYPE
    {
        ALAPERTELMEZETT,
        LOGIN,
        GET_UGYINTEZOK,
        GETINTMOD,
        ADD_IKTATAS_BY_UGYF,
        ADD_ERKEZTETO_ALAP,
        UPD_IKT_TEVES,
    }

    public enum ENUM_ETDR_INT_PARAMETER_NAMES
    {
        ETDR_CONTENTUM_TECHNIKAI_USER,
        ETDR_CONTENTUM_TECHNIKAI_USER_SZERVEZET,

        ETDR_SERVICEURL,

        ETDR_LOGINNAME,
        ETDR_LOGINPASSWORD,
        ETDR_ID,
        ETDR_LOGINIDEXPIRYTIMEINSECONDS,

        ETDR_ErkeztetoKonyvMegKulJel,
        ETDR_IKTATOKONYVIKTATOHELY,
        ETDR_ITSZ,
        ETDR_UGYTIPUS,
    }

    public enum ENUM_ETDR_CACHE_KEYS
    {
        CACHE_KEY_ETDR_TOKEN,
        CACHE_KEY_ETDR_TOKEN_EXPIRATION,
    }

    public enum ENUM_DATASET_COLUMNS
    {
        FELH_ID,
        FELH_NEV,

        ERK_AZON,
        IKT_SZAM,
        UGYINTEZO,
        UGYINTHATIDO,
        DOC_ID,
        IKT_ID
    }

    public enum ENUM_FUGGO_KODTAR
    {
        IRATTIPUS,
        ETDR_IKTTIP,
        KULDEMENY_KULDES_MODJA,
        ETDR_KULDMOD,
    }
}