﻿namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for ETDRConstants
    /// </summary>
    public static class ETDRErrorMessages
    {
        public const string LOGIN_ERROR_EMPTY_LOGIN_OR_PASSWORD = "Login paraméterek megadása kötelező !";
        public const string LOGIN_ERROR_LOGIN_OR_PASSWORD_DOES_NOT_MATCH = "Login paraméterek hibásak !";
        public const string LOGIN_ERROR_LOGIN_EMPTY_TOKEN = "Érvénytelen ETDR_LoginID !";
        public const string CHECKLOGIN_ERROR_INVALID_TOKEN = "Érvénytelen bejelentkezési azonosító!";

        public const string GETINTMOD_ERROR_EMPTY_RESULT = "Nincsenek ügyintézési módok !";
        public const string GETINTMOD_ERROR_EMPTY_INPUT_PARAMETERS = "Paraméterek megadása kötelező !";

        public const string ETDROBJ_ERROR_ETDR_ID_NOT_FOUND = "A paraméterben megadott belső azonosító nem található. A paraméter neve: {0}!";
        public const string KODTARFUGGOSEG_ERROR_MAP_NOT_FOUND = "A paraméterben megadott kódból nem sikerült meghatározni a contentum kódot. A paraméter neve: {0}!";
        public const string USER_ERROR_GROUP_NOT_FOUND = "A felhasználó szervezetének meghatározása sikertelen!";
        public const string IKTATOKONYV_ERROR = "Iktatókönyv meghatározása sikertelen!";
        public const string IRATTARI_TETEL_ERROR = "Irattári tétel meghatározása sikertelen!";
        public const string HATARIDO_ERROR = "Hiba lépett fel a határidő kiszámítása során!";
        public const string UGYFEL_ERROR = "Hiba lépett fel az ügyfél feldolgozása során!";
        public const string UGYFELCIM_ERROR = "Hiba lépett fel az ügyfél címének feldolgozása során!";
        public const string ETDROBJ_ERROR_GET_ETDR_ID_ERROR = "Hiba az ETDR azonosító lekérése során. A paraméter neve: {0}!";
        public const string KULDEMENY_NOT_EXISTS = "A megadott érkeztetési azonosító nem található!";
        public const string KULDEMENY_TOVABBI_BEKULDOK_ERROR = "Hiba lépett fel a küldemény további beküldőinek felvételekor!";

        public const string ADD_ERKEZTETO_ALAP_ERROR_EMPTY_INPUT_PARAMETERS = "Paraméterek megadása kötelező !";

        public const string ERROR_INPUT_PARAMETER_EMPTY = "{0} paraméter megadása kötelező !";
        public const string ERROR_ERKEZTETOKONYV = "Érkeztetőkönyv meghatározása sikertelen!";

        public const string ERROR_EMPTY_ETDR_ERKEZTETOKONYVMEGKULJEL = "Nincs megadva az érkeztetőkönyv megkuljel értéke";
        public const string ERROR_INSERT_ETDR_OBJ = "ETDR objektum létrehozása sikertelen !";
        public const string ERROR_ERKEZTETES = "ETDR érkeztetés hiba !";

        public const string UPT_IKT_TEVES_ERROR_EMPTY_INPUT_PARAMETERS = "Paraméterek megadása kötelező !";
        public const string UPT_IKT_TEVES_ERROR_INCORRECT_ETDR_IRATID = "Nem megfelelő ETDR.Ikt_id paraméter !";
        public const string UPT_IKT_TEVES_ERROR_NOT_FOUND_ASSIGNED_CONTENTUM_IRATID = "Hibás Ikt_id paraméterek !";

    }
}