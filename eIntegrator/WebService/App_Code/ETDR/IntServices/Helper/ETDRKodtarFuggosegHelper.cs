﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for ETDRKodtarFuggosegHelper
    /// </summary>
    public static class ETDRKodtarFuggosegHelper
    {
        public static string GetContentumKod(ExecParam execParam, string etdrKod, string etdrKodcsoportKod, string contentumKodcsoportKod, string propName)
        {
            try
            {
                bool hasNincsItem;
                List<string> kodok = Contentum.eUtility.KodtarFuggoseg.GetFuggoKodtarakKodList(execParam, etdrKodcsoportKod, contentumKodcsoportKod, etdrKod, out hasNincsItem);

                if (kodok == null)
                    throw new Exception("Kódtárfüggőség lekérése hiba!");

                if (kodok.Count != 1)
                    throw new Exception("A kapcsolat nem létezik vagy nem egyértelémű!");

                return kodok[0];
            }
            catch (Exception ex)
            {
                Logger.Error("GetContentumKod hiba", ex);
                throw new Exception(String.Format(ETDRErrorMessages.KODTARFUGGOSEG_ERROR_MAP_NOT_FOUND, propName));
            }
        }
    }
}