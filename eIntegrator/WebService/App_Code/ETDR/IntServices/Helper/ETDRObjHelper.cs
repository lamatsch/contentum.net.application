﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for RTDRObjHelper
    /// </summary>
    public static class ETDRObjHelper
    {
        public static class ObjtTipKod
        {
            public const string EREC_UgyUgyiratok = "EREC_UgyUgyiratok";
            public const string EREC_IraIratok = "EREC_IraIratok";
            public const string KRT_Felhasznalok = "KRT_Felhasznalok";
            public const string KRT_Partnerek = "KRT_Partnerek";
            public const string KRT_Cimek = "KRT_Cimek";

        }

        public static string GetObjId(ExecParam execParam, int ETDR_Id, string tableName, string propName)
        {
            try
            {
                INT_ETDR_ObjektumokService eTDR_ObjektumokService = new INT_ETDR_ObjektumokService();
                Result result = eTDR_ObjektumokService.Get_By_EtdrId(execParam, ETDR_Id, tableName);

                result.CheckError();

                return result.Uid;
            }
            catch (Exception ex)
            {
                Logger.Error("GetObjId hiba", ex);
                throw new Exception(String.Format(ETDRErrorMessages.ETDROBJ_ERROR_ETDR_ID_NOT_FOUND, propName));
            }
        }

        public static int GetETDR_Id(ExecParam execParam, string objId, string tableName, string propName)
        {
            try
            {
                INT_ETDR_ObjektumokService eTDR_ObjektumokService = new INT_ETDR_ObjektumokService();
                Result result = eTDR_ObjektumokService.Get_By_ObjId(execParam, objId, tableName);

                result.CheckError();

                return Int32.Parse(result.Uid);
            }
            catch (Exception ex)
            {
                Logger.Error("GetETDR_Id hiba", ex);
                throw new Exception(String.Format(ETDRErrorMessages.ETDROBJ_ERROR_GET_ETDR_ID_ERROR, propName));
            }
        }

        /// <summary>
        /// Insert_ETDR_Obj
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="obj_Id"></param>
        /// <param name="obj_Tip_Id"></param>
        /// <param name="obj_Tip_Nev"></param>
        /// <param name="data"></param>
        /// <param name="not"></param>
        /// <returns></returns>
        public static int Insert_ETDR_Obj(ExecParam execParam, string obj_Id, string obj_Tip_Id, string obj_Tip_Nev, string data, string not)
        {
            try
            {
                INT_ETDR_ObjektumokService eTDR_ObjektumokService = new INT_ETDR_ObjektumokService();
                Result result = eTDR_ObjektumokService.Insert_ETDR_Obj(execParam, obj_Id, obj_Tip_Id, obj_Tip_Nev, data, not);

                result.CheckError();
                if (string.IsNullOrEmpty(result.Uid))
                {
                    Logger.Error("Insert_ETDR_Obj hiba");
                    throw new Exception(ETDRErrorMessages.ERROR_INSERT_ETDR_OBJ);
                }
                return int.Parse(result.Uid);
            }
            catch (Exception ex)
            {
                Logger.Error("Insert_ETDR_Obj hiba", ex);
                throw new Exception(ETDRErrorMessages.ERROR_INSERT_ETDR_OBJ);
            }
        }
    }
}