﻿namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for ETDRUgyfelCim
    /// </summary>
    public class ETDRPartnerCimObject
    {
        public string PartnerId { get; set; }
        public string PartnerSTR { get; set; }
        public string CimId { get; set; }
        public string CimSTR { get; set; }
    }
}