﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using AddIktatasByUgyfData = Contentum.eIntegrator.WebService.ETDR.AddIktatasByUgyf;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for ETDRPartnerCimHelper
    /// </summary>
    public static class ETDRPartnerHelper
    {
        static void SetNevFromString(KRT_Szemelyek szemely, string nev)
        {
            if (String.IsNullOrEmpty(nev))
                return;

            string[] nevek = nev.Split(' ');
            string csaladiNev = String.Empty;
            string utoNev = String.Empty;
            string titulus = String.Empty;
            if (nevek.Length == 2)
            {
                csaladiNev = nevek[0];
                utoNev = nevek[1];
            }
            if (nevek.Length > 2)
            {
                for (int i = 0; i < nevek.Length; i++)
                {
                    if (nevek[i].Contains("."))
                    {
                        titulus = nevek[i];
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(csaladiNev))
                        {
                            csaladiNev = nevek[i];
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(utoNev))
                            {
                                utoNev += " ";
                            }
                            utoNev += nevek[i];
                        }
                    }
                }
            }

            if (nevek.Length < 2)
            {
                csaladiNev = nev;
            }

            if (!String.IsNullOrEmpty(csaladiNev))
            {
                szemely.UjCsaladiNev = csaladiNev;
                szemely.Updated.UjCsaladiNev = true;
            }

            if (!String.IsNullOrEmpty(utoNev))
            {
                szemely.UjUtonev = utoNev;
                szemely.Updated.UjUtonev = true;
            }

            if (!String.IsNullOrEmpty(titulus))
            {
                szemely.UjTitulis = titulus;
                szemely.Updated.UjTitulis = true;
            }
        }

        static KRT_Szemelyek GetSzemely(AddIktatasByUgyfData.UgyfelStruct ugyfel)
        {
            KRT_Szemelyek szemely = new KRT_Szemelyek();
            SetNevFromString(szemely, ugyfel.UgyfNev);
            szemely.SzuletesiNev = ugyfel.SzulNev;
            szemely.SzuletesiIdo = ETDRConverter.ConvertToBOString(ugyfel.SzulDat); //1980.12.23
            szemely.SzuletesiHely = ETDRConverter.ConvertToBOString(ugyfel.SzulHely);
            szemely.AnyjaNeve = ETDRConverter.ConvertToBOString(ugyfel.AnyjaNeve);

            return szemely;
        }

        static KRT_Vallalkozasok GetVallalkozas(AddIktatasByUgyfData.UgyfelStruct ugyfel)
        {
            KRT_Vallalkozasok vallalkozas = new KRT_Vallalkozasok();

            return vallalkozas;

        }

        static KRT_Partnerek GetPartner(AddIktatasByUgyfData.UgyfelStruct ugyfel)
        {
            bool isSzemely = ugyfel.UgyfTip == 0;

            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Nev = ugyfel.UgyfNev;
            partner.Tipus = isSzemely ? KodTarak.Partner_Tipus.Szemely : KodTarak.Partner_Tipus.Szervezet;
            partner.KulsoAzonositok = ugyfel.KulsoAzon.ToString();
            partner.Forras = KodTarak.Partner_Forras.ETDR;
            partner.Belso = Contentum.eUtility.Constants.Database.No;
            partner.Allapot = KodTarak.Partner_Allapot.Jovahagyando;

            if (isSzemely)
            {
                KRT_Szemelyek szemely = GetSzemely(ugyfel);
                KRT_PartnerSzemelyek partnerSzemely = new KRT_PartnerSzemelyek(partner, szemely);
                return partnerSzemely;
            }
            else
            {
                KRT_Vallalkozasok vallalkozas = GetVallalkozas(ugyfel);

                KRT_PartnerVallalkozasok partnerVallalkozas = new KRT_PartnerVallalkozasok(partner, vallalkozas);
                return partnerVallalkozas;
            }
        }

        public static string InsertPartner(ExecParam execParam, AddIktatasByUgyfData.UgyfelStruct ugyfel)
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            KRT_Partnerek partner = GetPartner(ugyfel);
            Result result = service.Insert(execParam, partner);
            result.CheckError();
            return result.Uid;
        }

        public static void BindPartnerCim(ExecParam execParam, string partnerId, string cimId)
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParamBind = execParam.Clone();
            execParamBind.Record_Id = partnerId;
            Result result = service.BindCim(execParamBind, cimId);
            result.CheckError();
        }

        public static string GetPartnerSTR(ExecParam execParam, string partnerId)
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParamPartnerGet = execParam.Clone();
            execParamPartnerGet.Record_Id = partnerId;
            Result result = service.Get(execParamPartnerGet);
            result.CheckError();
            KRT_Partnerek partner = result.Record as KRT_Partnerek;
            return partner.Nev;
        }
    }
}