﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System.Data;
using System.Text;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for ResultExtension
    /// </summary>
    public static class ResultExtension
    {
        public static DataRowCollection Rows(this Result result)
        {
            return result.Ds.Tables[0].Rows;
        }
        /// <summary>
        /// GetExceptionMessage
        /// </summary>
        /// <param name="exc"></param>
        /// <returns></returns>
        public static string GetExceptionMessage(ResultException exc)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(exc.Message);
            if (exc.InnerException != null)
            {
                sb.Append(" " + exc.InnerException.Message);
            }
            return sb.ToString();
        }
    }
}