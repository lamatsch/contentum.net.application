﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.ETDR.AddErkeztetoAlap;
using Contentum.eUtility;
using System;
using System.Data;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for IntServiceLogin
    /// </summary>
    public class IntServiceAddErkeztetoAlap : BaseIntETDRService
    {
        public IntServiceAddErkeztetoAlap()
        {
            base.ServiceType = ENUM_ETDR_SERVICETYPE.ADD_ERKEZTETO_ALAP;
        }

        public Add_Erkezteto_AlapResponse Add_Erkezteto_Alap(Add_Erkezteto_Alap item)
        {
            if (item == null)
            {
                return new Add_Erkezteto_AlapResponse()
                {
                    ErrMess = ETDRErrorMessages.ADD_ERKEZTETO_ALAP_ERROR_EMPTY_INPUT_PARAMETERS,
                };
            }

            try
            {
                return AddErkeztetoAlapFlow(item);
            }
            catch (ResultException exc)
            {
                base.Log(exc);
                return new Add_Erkezteto_AlapResponse()
                {
                    ErrMess = ResultExtension.GetExceptionMessage(exc)
                };
            }
            catch (Exception exc)
            {
                base.Log(exc);
                return new Add_Erkezteto_AlapResponse()
                {
                    ErrMess = exc.Message
                };
            }
        }

        /// <summary>
        /// GetIntModFlow
        /// </summary>
        /// <param name="LoginNev"></param>
        /// <param name="LoginAzon"></param>
        /// <returns></returns>
        private Add_Erkezteto_AlapResponse AddErkeztetoAlapFlow(Add_Erkezteto_Alap item)
        {
            base.CheckLogin(item.LoginNev, item.LoginAzon);

            //string iktTip = ETDRKodtarFuggosegHelper.GetContentumKod(
            //         this.ExecParamForServices,
            //         item.Ikttip_Azon, ENUM_FUGGO_KODTAR.ETDR_IKTTIP.ToString(), ENUM_FUGGO_KODTAR.IRATTIPUS.ToString(), "Ikttip_Azon");

            ExecParam execParam4Erkeztet = SetExecParam(item);
            ExecParam execParam2Select = this.ExecParamForServices;

            EREC_KuldKuldemenyek kuldemeny = Convert(item);

            #region ERKEZTETOKONYV

            string erkeztetoMegKulJel = this.GetParameter(ENUM_ETDR_INT_PARAMETER_NAMES.ETDR_ErkeztetoKonyvMegKulJel.ToString());
            if (string.IsNullOrEmpty(erkeztetoMegKulJel))
            {
                return new Add_Erkezteto_AlapResponse()
                {
                    ErrMess = ETDRErrorMessages.ERROR_EMPTY_ETDR_ERKEZTETOKONYVMEGKULJEL,
                };
            }
            string erkeztetoKonyvId = ETDRConentumWrapper.GetErkeztetoKonyvId(execParam2Select, erkeztetoMegKulJel);

            kuldemeny.IraIktatokonyv_Id = erkeztetoKonyvId;
            kuldemeny.Updated.IraIktatokonyv_Id = true;

            #endregion ERKEZTETOKONYV

            #region KULDMOD

            SetKuldMod(execParam2Select, item, ref kuldemeny);

            #endregion KULDMOD

            #region PARTNER BEKULDO

            SetBekuldo(execParam2Select, item, ref kuldemeny);

            #endregion PARTNER BEKULDO

            #region CIM

            Result resultCim = ETDRConentumWrapper.AddCim(execParam2Select, item.Irsz, item.Telepules, item.Utca, item.Hazszam);
            resultCim.CheckError();
            if (string.IsNullOrEmpty(resultCim.Uid))
            {
                kuldemeny.Cim_Id = resultCim.Uid;

                #region ADD CIM 2 INT_ETDR_OBJEKTUMOK

                ETDRObjHelper.Insert_ETDR_Obj(execParam2Select, kuldemeny.Cim_Id, null, ETDRObjHelper.ObjtTipKod.KRT_Cimek, null, ETDRObjHelper.ObjtTipKod.KRT_Cimek);

                #endregion ADD CIM 2 INT_ETDR_OBJEKTUMOK
            }

            #endregion CIM

            #region ERKEZTET

            eRecord.Service.EREC_KuldKuldemenyekService kuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            EREC_HataridosFeladatok feladatok = new EREC_HataridosFeladatok();
            ErkeztetesParameterek erkParameterek = new ErkeztetesParameterek();
            Result resultErkeztetes = kuldemenyekService.Erkeztetes(execParam4Erkeztet, kuldemeny, feladatok, erkParameterek);
            resultErkeztetes.CheckError();

            #region VISSZATÉRÉSI INFORMÁCIÓK

            ErkeztetesIktatasResult erkeztetesResult = null;
            if (resultErkeztetes.Record != null && resultErkeztetes.Record is ErkeztetesIktatasResult)
            {
                erkeztetesResult = (ErkeztetesIktatasResult)resultErkeztetes.Record;
            }

            #endregion VISSZATÉRÉSI INFORMÁCIÓK

            #endregion ERKEZTET

            #region RESULT

            if (erkeztetesResult != null)
            {
                return new Add_Erkezteto_AlapResponse()
                {
                    Add_Erkezteto_AlapResult = GetResultDataSet(erkeztetesResult.KuldemenyAzonosito)
                };
            }
            else
            {
                return new Add_Erkezteto_AlapResponse()
                {
                    ErrMess = ETDRErrorMessages.ERROR_ERKEZTETES,
                };
            }

            #endregion RESULT
        }

        /// <summary>
        /// SetExecParam
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private ExecParam SetExecParam(Add_Erkezteto_Alap item)
        {
            string ugyintezo_id = ETDRObjHelper.GetObjId(
                this.ExecParamForServices,
                item.Ugyint_Id, ETDRObjHelper.ObjtTipKod.KRT_Felhasznalok, "UgyintId");

            return GetExecParamFromByFelhasznalo(ugyintezo_id);
        }

        /// <summary>
        /// SetKuldMod
        /// </summary>
        /// <param name="item"></param>
        /// <param name="kuldemeny"></param>
        private void SetKuldMod(ExecParam execParam, Add_Erkezteto_Alap item, ref EREC_KuldKuldemenyek kuldemeny)
        {
#warning ELLENŐRÍZNI HOGY KezbAzon PARAMETER KELL-E IDE
            //string kuldmod = ETDRKodtarFuggosegHelper.GetContentumKod(
            //        execParam,
            //        item.KezbAzon.ToString(), ENUM_FUGGO_KODTAR.KULDEMENY_KULDES_MODJA.ToString(), ENUM_FUGGO_KODTAR.ETDR_KULDMOD.ToString(), "KuldesMod");
            string kuldmod = KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
            kuldemeny.KuldesMod = kuldmod;
            kuldemeny.Updated.KuldesMod = true;
        }

        /// <summary>
        /// SetBekuldo
        /// </summary>
        /// <param name="item"></param>
        /// <param name="kuldemeny"></param>
        private void SetBekuldo(ExecParam execParam, Add_Erkezteto_Alap item, ref EREC_KuldKuldemenyek kuldemeny)
        {
            try
            {
                Result resultFindPartner = ETDRConentumWrapper.FindPartner(execParam, item.Nev, item.Irsz, item.Telepules, item.Utca, item.Hazszam);
                if (resultFindPartner != null && !resultFindPartner.IsError && resultFindPartner.Ds.Tables[0].Rows.Count > 0)
                {
                    kuldemeny.Partner_Id_Bekuldo = resultFindPartner.Ds.Tables[0].Rows[0]["Id"].ToString();
                    kuldemeny.Updated.Partner_Id_Bekuldo = true;
                }
            }
            catch (Exception exc)
            {
                Logger.Error("ETDR.AddErkeztetoAlap.FindPartner", exc);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private EREC_KuldKuldemenyek Convert(Add_Erkezteto_Alap item)
        {
            EREC_KuldKuldemenyek result = new EREC_KuldKuldemenyek();
            result.Allapot = KodTarak.KULDEMENY_ALLAPOT.Erkeztetve;
            result.Updated.Allapot = true;

            result.BeerkezesIdeje = item.BeerkezesDate ?? DateTime.Now.ToString();
            result.Updated.BeerkezesIdeje = true;

            result.Targy = item.Nev;
            result.Updated.Targy = true;

            #region BEKULDO

            if (!string.IsNullOrEmpty(item.Nev))
            {
                result.NevSTR_Bekuldo = item.Nev;
                result.Updated.NevSTR_Bekuldo = true;
            }

            #endregion BEKULDO

            #region POSTAZAS IRANYA

            result.PostazasIranya = item.bejovo ? KodTarak.POSTAZAS_IRANYA.Bejovo : KodTarak.POSTAZAS_IRANYA.Belso;
            result.Updated.PostazasIranya = true;

            #endregion POSTAZAS IRANYA

            result.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            result.Updated.AdathordozoTipusa = true;

            result.CimzesTipusa = KodTarak.KULD_CIMZES_TIPUS.nevre_szolo;
            result.Updated.CimzesTipusa = true;

            result.ElsodlegesAdathordozoTipusa = KodTarak.ELSODLEGES_ADATHORDOZO.Elektronikus_irat;
            result.Updated.ElsodlegesAdathordozoTipusa = true;

            result.Erkeztetes_Ev = DateTime.Now.Year.ToString();
            result.Updated.Erkeztetes_Ev = true;

            result.FelbontasDatuma = result.BeerkezesIdeje;
            result.Updated.FelbontasDatuma = true;

            result.Munkaallomas = ETDRConstants.Munkaallomas;
            result.Updated.Munkaallomas = true;

            result.PeldanySzam = "1";
            result.Updated.PeldanySzam = true;

            result.Surgosseg = KodTarak.SURGOSSEG.Normal;
            result.Updated.Surgosseg = true;

            result.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            result.Updated.IktatniKell = true;

            result.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            result.Updated.UgyintezesModja = true;

            return result;
        }

        /// <summary>
        /// A válasz
        /// Fields in data set:
        /// ERK_AZON, string: Az irathoz tartozó érkeztető azonosító(az ÉTDR-ben az érkezetőszám mezőbe íródik)
        /// IKT_SZAM, string: Az irathoz tartozó iktatószám, amennyiben engedélyezett az automatikus iktatás(az ÉTDR-ben nem kerül elmentésre)
        /// UGYINTEZO, string: Az irat ügyintézője(az ÉTDR-ben nem kerül elmentésre)
        /// UGYINTHATIDO, string: Az ügyintézés határideje napokban(az ÉTDR-ben nem kerül elmentésre)
        /// DOC_ID, int: A dokumentum belső, egyedi azonosítója(az ÉTDR-ben nem kerül elmentésre)
        /// IKT_ID, int:  Az irat egyedi, belső azonosítója(az ÉTDR-ben nem kerül elmentésre)
        /// </summary>
        /// <param name="etdrFelhasznalok"></param>
        /// <returns></returns>
        private DataSet GetResultDataSet(string kuldemenyAzonosito)
        {
            DataSet resultDataSet = new DataSet();
            DataTable table = new DataTable();
            table.Columns.Add(ENUM_DATASET_COLUMNS.ERK_AZON.ToString(), typeof(string));
            table.Columns.Add(ENUM_DATASET_COLUMNS.IKT_SZAM.ToString(), typeof(string));
            table.Columns.Add(ENUM_DATASET_COLUMNS.UGYINTEZO.ToString(), typeof(string));
            table.Columns.Add(ENUM_DATASET_COLUMNS.UGYINTHATIDO.ToString(), typeof(string));
            table.Columns.Add(ENUM_DATASET_COLUMNS.DOC_ID.ToString(), typeof(int));
            table.Columns.Add(ENUM_DATASET_COLUMNS.IKT_ID.ToString(), typeof(int));
            resultDataSet.Tables.Add(table);
            DataRow ugyintezoRow = table.NewRow();
            ugyintezoRow[ENUM_DATASET_COLUMNS.ERK_AZON.ToString()] = kuldemenyAzonosito;
            table.Rows.Add(ugyintezoRow);
            return resultDataSet;
        }
    }
}