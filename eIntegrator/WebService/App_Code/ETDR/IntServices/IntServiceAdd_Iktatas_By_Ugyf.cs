﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using AddIktatasByUgyfData = Contentum.eIntegrator.WebService.ETDR.AddIktatasByUgyf;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for IntServiceAdd_Iktatas_By_Ugyf
    /// </summary>
    public class IntServiceAdd_Iktatas_By_Ugyf : BaseIntETDRService
    {
        const string IRATTIPUS = "IRATTIPUS";
        const string ETDR_IKTTIP = "ETDR_IKTTIP";

        public IntServiceAdd_Iktatas_By_Ugyf()
        {
            base.ServiceType = ENUM_ETDR_SERVICETYPE.ADD_IKTATAS_BY_UGYF;
        }

        public AddIktatasByUgyfData.Add_Iktatas_By_UgyfResponse Add_Iktatas_By_Ugyf(AddIktatasByUgyfData.Add_Iktatas_By_Ugyf Add_Iktatas_By_Ugyf)
        {
            AddIktatasByUgyfData.Add_Iktatas_By_Ugyf request = Add_Iktatas_By_Ugyf;
            AddIktatasByUgyfData.Iktatas iktatas = request.Iktatas;

            try
            {
                base.CheckLogin(iktatas.UserName, iktatas.LoginAzon);

                bool bejovoIktatas = iktatas.Bejovo;
                bool alszamosIktatas = iktatas.IktId > -1;

   
                string iktatohely = this.GetParameter(ENUM_ETDR_INT_PARAMETER_NAMES.ETDR_IKTATOKONYVIKTATOHELY.ToString());
                string iktatokonyvId = ETDRConentumWrapper.GetIktatokonyvId(this.ExecParamForServices, iktatohely);

                string kuldemenyId = String.Empty;
                string ugyiratId = String.Empty;

                //bejövő
                if (bejovoIktatas)
                {
                    if (String.IsNullOrEmpty(iktatas.Erkazon))
                        throw new Exception(String.Format(ETDRErrorMessages.ERROR_INPUT_PARAMETER_EMPTY, iktatas.Erkazon));

                    kuldemenyId = ETDRConentumWrapper.GetKuldemenyId(this.ExecParamForServices, iktatas.Erkazon);
                }

                //alszámos
                if (alszamosIktatas)
                {
                    ugyiratId = ETDRObjHelper.GetObjId(this.ExecParamForServices, iktatas.IktId, ETDRObjHelper.ObjtTipKod.EREC_UgyUgyiratok, "IktId");
                }

                string irattipus = ETDRKodtarFuggosegHelper.GetContentumKod(this.ExecParamForServices,
                    iktatas.Ikttip_Azon, ETDR_IKTTIP, IRATTIPUS, "Ikttip_Azon");

                string ugyintezo_id = ETDRObjHelper.GetObjId(this.ExecParamForServices,
                    iktatas.UgyintId, ETDRObjHelper.ObjtTipKod.KRT_Felhasznalok, "UgyintId");

                ExecParam iktatasExecParam = GetIktatasExecParam(ugyintezo_id);

                List<ETDRPartnerCimObject> ugyfelek = GetPartnerCimek(this.ExecParamForServices, iktatas);
                ETDRPartnerCimObject foUgyfel = ugyfelek[0];
                List<ETDRPartnerCimObject> tovabbiUgyfelek = new List<ETDRPartnerCimObject>(ugyfelek);
                tovabbiUgyfelek.RemoveAt(0);
                bool vannakTovabbiUgyfelek = tovabbiUgyfelek.Count > 0;

                EREC_UgyUgyiratok ugyirat = null;
                if (!alszamosIktatas)
                {
                    string etdrItsz = this.GetParameter(ENUM_ETDR_INT_PARAMETER_NAMES.ETDR_ITSZ.ToString());
                    string etdrUgytipus = this.GetParameter(ENUM_ETDR_INT_PARAMETER_NAMES.ETDR_UGYTIPUS.ToString());

                    ugyirat = GetUgyiratFromInput(iktatas, etdrItsz, etdrUgytipus, ugyintezo_id, foUgyfel);
                }

                EREC_UgyUgyiratdarabok ugyiratDarab = GetUgyiratDarabFromInput();

                EREC_IraIratok irat = GetIratFromInput(iktatas, irattipus, bejovoIktatas);

                EREC_PldIratPeldanyok peldany = null;
                if (!bejovoIktatas)
                {
                    peldany = GetPeldanyFromInput(foUgyfel, 1);
                }

                IktatasiParameterek iktatasiParameterek = new IktatasiParameterek();

                EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                Result result_iktatas = new Result();

                if (bejovoIktatas)
                {
                    if (vannakTovabbiUgyfelek)
                    {
                        ETDRConentumWrapper.AddTovabbiBekuldok(iktatasExecParam, kuldemenyId, tovabbiUgyfelek);
                    }

                    iktatasiParameterek.KuldemenyId = kuldemenyId;

                    if (alszamosIktatas)
                    {
                        result_iktatas = iratokService.BejovoIratIktatasa_Alszamra(iktatasExecParam, iktatokonyvId,
                               ugyiratId, ugyiratDarab, irat, null, iktatasiParameterek);
                    }
                    else
                    {
                        result_iktatas = iratokService.BejovoIratIktatasa(iktatasExecParam, iktatokonyvId,
                                                       ugyirat, ugyiratDarab, irat, null, iktatasiParameterek);
                    }
                }
                else
                {
                    if (vannakTovabbiUgyfelek)
                    {
                        iktatasiParameterek.Peldanyok = new List<EREC_PldIratPeldanyok>();
                        iktatasiParameterek.Peldanyok.Add(peldany);
                        iktatasiParameterek.Peldanyok.AddRange(GetTovabbiPeldanyok(tovabbiUgyfelek));
                    }

                    if (alszamosIktatas)
                    {

                        result_iktatas = iratokService.BelsoIratIktatasa_Alszamra(iktatasExecParam, iktatokonyvId,
                                                       ugyiratId, ugyiratDarab, irat, null, peldany, iktatasiParameterek);
                    }
                    else
                    {
                        result_iktatas = iratokService.BelsoIratIktatasa(iktatasExecParam, iktatokonyvId,
                                                  ugyirat, ugyiratDarab, irat, null, peldany, iktatasiParameterek);
                    }
                }

                result_iktatas.CheckError();

                ErkeztetesIktatasResult erkeztetesIktatasResult = result_iktatas.Record as ErkeztetesIktatasResult;
                if (!alszamosIktatas)
                {
                    iktatas.IktId = ETDRObjHelper.GetETDR_Id(ExecParamForServices, erkeztetesIktatasResult.UgyiratId, ETDRObjHelper.ObjtTipKod.EREC_UgyUgyiratok, "IktId");
                }
                iktatas.Iktatoszam = erkeztetesIktatasResult.IratAzonosito;
                iktatas.BeerkDatum = DateTime.Now.ToShortDateString();

                AddIktatasByUgyfData.Add_Iktatas_By_UgyfResponse response = new AddIktatasByUgyfData.Add_Iktatas_By_UgyfResponse();
                response.Add_Iktatas_By_UgyfResult = true;
                response.Iktatas = iktatas;
                return response;

            }
            catch (System.Exception ex)
            {
                Result errorResult = ResultException.GetResultFromException(ex);
                iktatas.Err = ResultError.GetErrorMessageFromResultObject(errorResult);
                return new AddIktatasByUgyfData.Add_Iktatas_By_UgyfResponse()
                {
                    Add_Iktatas_By_UgyfResult = false,
                    Iktatas = iktatas
                };
            }
        }

        ExecParam GetIktatasExecParam(string felhasznaloId)
        {
            string felhasznaloSzervezetId = ETDRConentumWrapper.GetFelhasznaloSzervezet(this.ExecParamForServices, felhasznaloId);
            return new ExecParam()
            {
                Felhasznalo_Id = felhasznaloId,
                FelhasznaloSzervezet_Id = felhasznaloSzervezetId
            };
        }

        EREC_UgyUgyiratok GetUgyiratFromInput(AddIktatasByUgyfData.Iktatas iktatas, string etdrItsz, string etdrUgytipus, string ugyintezo_id, ETDRPartnerCimObject ugyfel)
        {
            EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
            ugyirat.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            ugyirat.Targy = iktatas.Megjegyzes;
            ugyirat.IraIrattariTetel_Id = ETDRConentumWrapper.GetIrattariTetelId(this.ExecParamForServices, etdrItsz);
            ugyirat.UgyTipus = etdrUgytipus;
            ugyirat.Hatarido = ETDRConentumWrapper.GetHatarido(this.ExecParamForServices, DateTime.Now, ugyirat.IraIrattariTetel_Id, ugyirat.UgyTipus);
            ugyirat.Csoport_Id_Felelos = ugyintezo_id;
            ugyirat.Partner_Id_Ugyindito = ugyfel.PartnerId;
            ugyirat.NevSTR_Ugyindito = ugyfel.PartnerSTR;
            ugyirat.Cim_Id_Ugyindito = ugyfel.CimId;
            ugyirat.CimSTR_Ugyindito = ugyfel.CimSTR;
            return ugyirat;
        }

        EREC_UgyUgyiratdarabok GetUgyiratDarabFromInput()
        {
            return new EREC_UgyUgyiratdarabok();
        }

        EREC_IraIratok GetIratFromInput(AddIktatasByUgyfData.Iktatas iktatas, string irattipus, bool bejovoIktatas)
        {
            EREC_IraIratok irat = new EREC_IraIratok();
            irat.Targy = iktatas.Megjegyzes;
            irat.Irattipus = irattipus;
            irat.KiadmanyozniKell = "0";
            irat.PostazasIranya = bejovoIktatas ? KodTarak.POSTAZAS_IRANYA.Bejovo : KodTarak.POSTAZAS_IRANYA.Belso;
            irat.Munkaallomas = ETDRConstants.Munkaallomas;
            irat.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            return irat;
        }

        EREC_PldIratPeldanyok GetPeldanyFromInput(ETDRPartnerCimObject ugyfel, int sorszam)
        {
            EREC_PldIratPeldanyok peldany = new EREC_PldIratPeldanyok();
            peldany.Sorszam = sorszam.ToString();
            peldany.Partner_Id_Cimzett = ugyfel.PartnerId;
            peldany.NevSTR_Cimzett = ugyfel.PartnerSTR;
            peldany.Cim_id_Cimzett = ugyfel.CimId;
            peldany.CimSTR_Cimzett = ugyfel.CimSTR;
            peldany.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            peldany.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
            return peldany;
        }

        List<EREC_PldIratPeldanyok> GetTovabbiPeldanyok(List<ETDRPartnerCimObject> tovabbiUgyfelek)
        {
            List<EREC_PldIratPeldanyok> peldanyok = new List<EREC_PldIratPeldanyok>();
            int sorszam = 1;
            foreach (ETDRPartnerCimObject ugyfel in tovabbiUgyfelek)
            {
                sorszam++;
                EREC_PldIratPeldanyok peldany = GetPeldanyFromInput(ugyfel, sorszam);
                peldanyok.Add(peldany);
            }
            return peldanyok;
        }

        private string GetPartnerId(ExecParam execParam, AddIktatasByUgyfData.UgyfelStruct ugyfel)
        {
            string ugyfelId = String.Empty;

            try
            {

                bool nemUjUgyfel = (ugyfel.UgyfelId > -1);

                if (nemUjUgyfel)
                {
                    ugyfelId = ETDRObjHelper.GetObjId(execParam, ugyfel.UgyfelId, ETDRObjHelper.ObjtTipKod.KRT_Partnerek, "UgyfelId");
                }
                else
                {
                    ugyfelId = ETDRPartnerHelper.InsertPartner(execParam, ugyfel);
                    ugyfel.UgyfelId = ETDRObjHelper.GetETDR_Id(execParam, ugyfelId, ETDRObjHelper.ObjtTipKod.KRT_Partnerek, "UgyfelId");
                }

                return ugyfelId;
            }
            catch (Exception ex)
            {
                Logger.Error("GetUgyfelId hiba", ex);
                throw new Exception(String.Format("{0} {1}", ETDRErrorMessages.UGYFEL_ERROR, ex.Message));
            }
        }

        private string GetCimId(ExecParam execParam, AddIktatasByUgyfData.UgyfelStruct ugyfel)
        {
            string cimId = String.Empty;

            try
            {

                bool nemUjCim = (ugyfel.UgyfCimId > -1);

                if (nemUjCim)
                {
                    cimId = ETDRObjHelper.GetObjId(execParam, ugyfel.UgyfCimId, ETDRObjHelper.ObjtTipKod.KRT_Cimek, "UgyfCimId");
                }
                else
                {
                    cimId = ETDRCimHelper.InsertCim(execParam, ugyfel);
                    ugyfel.UgyfCimId = ETDRObjHelper.GetETDR_Id(execParam, cimId, ETDRObjHelper.ObjtTipKod.KRT_Cimek, "UgyfCimId");
                }

                return cimId;
            }
            catch (Exception ex)
            {
                Logger.Error("GetCimId hiba", ex);
                throw new Exception(String.Format("{0} {1}", ETDRErrorMessages.UGYFELCIM_ERROR, ex.Message));
            }
        }

        ETDRPartnerCimObject GetPartnerCim(ExecParam execParam, AddIktatasByUgyfData.UgyfelStruct ugyfel)
        {
            ETDRPartnerCimObject partnerCim = new ETDRPartnerCimObject();
            partnerCim.PartnerId = GetPartnerId(execParam, ugyfel);
            partnerCim.PartnerSTR = ETDRPartnerHelper.GetPartnerSTR(execParam, partnerCim.PartnerId);
            partnerCim.CimId = GetCimId(execParam, ugyfel);
            partnerCim.CimSTR = ETDRCimHelper.GetCimSTR(execParam, partnerCim.CimId);
            ETDRPartnerHelper.BindPartnerCim(execParam, partnerCim.PartnerId, partnerCim.CimId);
            return partnerCim;
        }

        List<ETDRPartnerCimObject> GetPartnerCimek(ExecParam execParam, AddIktatasByUgyfData.Iktatas iktatas)
        {
            List<ETDRPartnerCimObject> partnerCimek = new List<ETDRPartnerCimObject>();
            partnerCimek.Add(GetPartnerCim(execParam, iktatas.Ugyfel));

            if (iktatas.TobbUgyfel != null && iktatas.TobbUgyfel.UgyfelStruct != null)
            {
                foreach(AddIktatasByUgyfData.UgyfelStruct ugyfel in iktatas.TobbUgyfel.UgyfelStruct)
                {
                    partnerCimek.Add(GetPartnerCim(execParam, ugyfel));
                }
            }

            return partnerCimek;
        }
    }
}