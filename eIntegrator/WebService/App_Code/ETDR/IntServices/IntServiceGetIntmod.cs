﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.ETDR.GetIntMod;
using Contentum.eUtility;
using System.Data;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for IntServiceLogin
    /// </summary>
    public class IntServiceGetIntmod : BaseIntETDRService
    {
        private const string ColumnNameValaszTipId = "VALASZ_TIP_ID";
        private const string ColumnNameValaszTipNev = "VALASZ_TIP_NEV";

        public IntServiceGetIntmod()
        {
            base.ServiceType = ENUM_ETDR_SERVICETYPE.GETINTMOD;
        }

        public Get_IntModResponse GetIntMod(Get_IntMod Get_IntMod)
        {
            if (Get_IntMod == null)
            {
                return new Get_IntModResponse()
                {
                    ErrMess = ETDRErrorMessages.GETINTMOD_ERROR_EMPTY_INPUT_PARAMETERS,
                };
            }
            try
            {
                return GetIntModFlow(Get_IntMod.LoginNev, Get_IntMod.LoginAzon);
            }
            catch (ResultException exc)
            {
                base.Log(exc);
                return new Get_IntModResponse()
                {
                    ErrMess = ResultExtension.GetExceptionMessage(exc),
                };
            }
            catch (System.Exception exc)
            {
                base.Log(exc);
                return new Get_IntModResponse()
                {
                    ErrMess = exc.Message
                };
            }
        }

        /// <summary>
        /// GetIntModFlow
        /// </summary>
        /// <param name="LoginNev"></param>
        /// <param name="LoginAzon"></param>
        /// <returns></returns>
        private Get_IntModResponse GetIntModFlow(string LoginNev, string LoginAzon)
        {
            base.CheckLogin(LoginNev, LoginAzon);
            return GetIntModFromDataBase();
        }

        /// <summary>
        /// GetIntModFromDataBase
        /// </summary>
        /// <returns></returns>
        private Get_IntModResponse GetIntModFromDataBase()
        {
            Get_IntModResponse result = new Get_IntModResponse();

            Result resultGet;
            using (INT_ETDR_ObjektumokService etdr_Service = new INT_ETDR_ObjektumokService())
            {
                resultGet = etdr_Service.Get_IntezesiModok(ExecParamForServices);
            }

            resultGet.CheckError();

            if (resultGet.Ds.Tables[0].Rows.Count > 0)
            {
                result.Get_IntmodResult = GetResultDataSet(resultGet.Ds.Tables[0]);
            }
            else
            {
                return new Get_IntModResponse()
                {
                    ErrMess = ETDRErrorMessages.GETINTMOD_ERROR_EMPTY_RESULT.ToString()
                };
            }
            return result;
        }

        /// <summary>
        /// GetResultDataSet
        /// </summary>
        /// <param name="dsItem"></param>
        /// <returns></returns>
        private DataSet GetResultDataSet(DataTable dsItem)
        {
            DataSet resultDataSet = new DataSet();
            DataTable resultTable = new DataTable();
            resultTable.Columns.Add(ColumnNameValaszTipId, typeof(string));
            resultTable.Columns.Add(ColumnNameValaszTipNev, typeof(string));
            resultDataSet.Tables.Add(resultTable);

            DataRow tmpRow;
            foreach (DataRow row in dsItem.Rows)
            {
                string id = row[ColumnNameValaszTipId].ToString();
                string nev = row[ColumnNameValaszTipNev].ToString();

                tmpRow = resultTable.NewRow();
                tmpRow[ColumnNameValaszTipId] = id;
                tmpRow[ColumnNameValaszTipNev] = nev;

                resultTable.Rows.Add(tmpRow);
            }

            return resultDataSet;
        }
    }
}