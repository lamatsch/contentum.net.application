﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.ETDR.GetUgyintezok;
using Contentum.eUtility;
using System.Data;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for IntServiceGet_Ugyintezok
    /// </summary>
    public class IntServiceGet_Ugyintezok : BaseIntETDRService
    {
        public IntServiceGet_Ugyintezok()
        {
            base.ServiceType = ENUM_ETDR_SERVICETYPE.GET_UGYINTEZOK;
        }

        public Get_UgyintezokResponse Get_Ugyintezok(Get_Ugyintezok Get_Ugyintezok)
        {
            try
            {
                Get_Ugyintezok request = Get_Ugyintezok;

                base.CheckLogin(request.LoginNev, request.LoginAzon);

                INT_ETDR_ObjektumokService etdr_objService = new INT_ETDR_ObjektumokService();
                Result etdrFelhazsnalokResult = etdr_objService.Get_Felhasznalok(this.ExecParamForServices);

                etdrFelhazsnalokResult.CheckError();

                Get_UgyintezokResponse response = new Get_UgyintezokResponse();
                response.Get_UgyintezokResult = GetUgyintezokResultDataSet(etdrFelhazsnalokResult.Ds.Tables[0]);

                return response;

            }
            catch (ResultException exc)
            {
                base.Log(exc);
                return new Get_UgyintezokResponse()
                {
                    ErrMess = exc.Message + (exc.InnerException != null ? " " + exc.InnerException.Message : string.Empty),
                };
            }
            catch (System.Exception exc)
            {
                return new Get_UgyintezokResponse()
                {
                    ErrMess = exc.Message
                };
            }
        }

        const string ugyintezokCol_FelhId = "FELH_ID";
        const string ugyintezokCol_FelhNev = "FELH_NEV";

        DataSet GetUgyintezokResultDataSet(DataTable etdrFelhasznalok)
        {
            DataSet ugyintezokResultDataSet = new DataSet();
            DataTable ugyintezokTable = new DataTable();
            ugyintezokTable.Columns.Add(ugyintezokCol_FelhNev, typeof(string));
            ugyintezokTable.Columns.Add(ugyintezokCol_FelhId, typeof(string));
            ugyintezokResultDataSet.Tables.Add(ugyintezokTable);

            foreach (DataRow row in etdrFelhasznalok.Rows)
            {
                string ETDR_Id = row["ETDR_Id"].ToString();
                string nev = row["Nev"].ToString();

                DataRow ugyintezoRow = ugyintezokTable.NewRow();
                ugyintezoRow[ugyintezokCol_FelhId] = ETDR_Id;
                ugyintezoRow[ugyintezokCol_FelhNev] = nev;

                ugyintezokTable.Rows.Add(ugyintezoRow);
            }

            return ugyintezokResultDataSet;
        }
    }
}