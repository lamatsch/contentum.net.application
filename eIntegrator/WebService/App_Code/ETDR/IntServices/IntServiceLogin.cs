﻿using Contentum.eIntegrator.WebService.ETDR.Login;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for IntServiceLogin
    /// </summary>
    public class IntServiceLogin : BaseIntETDRService
    {
        public IntServiceLogin()
        {
            base.ServiceType = ENUM_ETDR_SERVICETYPE.LOGIN;
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="Login"></param>
        /// <returns></returns>
        public LoginResponse Login(Login.Login Login)
        {
            try
            {
                return LoginFlow(Login.LoginNev, Login.Password, Login.LoginTimeout, Login.LoginAzon);
            }
            catch (System.Exception exc)
            {
                base.Log(exc);
                return new LoginResponse()
                {
                    ErrMess = exc.Message,
                    LoginResult = string.Empty,
                };
            }
        }

        /// <summary>
        /// LoginFlow
        /// </summary>
        /// <param name="LoginNev"></param>
        /// <param name="Password"></param>
        /// <param name="LoginTimeout"></param>
        /// <param name="LoginAzon"></param>
        /// <returns></returns>
        private LoginResponse LoginFlow(string LoginNev, string Password, int LoginTimeout, string LoginAzon)
        {
            if (string.IsNullOrEmpty(LoginNev) || string.IsNullOrEmpty(Password))
            {
                return new LoginResponse()
                {
                    ErrMess = ETDRErrorMessages.LOGIN_ERROR_EMPTY_LOGIN_OR_PASSWORD,
                    LoginResult = string.Empty,
                };
            }

            string intLoginName = GetParameter(ENUM_ETDR_INT_PARAMETER_NAMES.ETDR_LOGINNAME.ToString());
            string intLoginPassword = GetParameter(ENUM_ETDR_INT_PARAMETER_NAMES.ETDR_LOGINPASSWORD.ToString());

            if (!string.Equals(LoginNev, intLoginName) || !string.Equals(Password, intLoginPassword))
            {
                return new LoginResponse()
                {
                    ErrMess = ETDRErrorMessages.LOGIN_ERROR_LOGIN_OR_PASSWORD_DOES_NOT_MATCH,
                    LoginResult = string.Empty,
                };
            }

            string tokenValueInCache = GetCacheValue(ENUM_ETDR_CACHE_KEYS.CACHE_KEY_ETDR_TOKEN.ToString());
            if (string.IsNullOrEmpty(tokenValueInCache))
            {
                string tokenETDR = System.Guid.NewGuid().ToString();
                RemoveCacheKey(ENUM_ETDR_CACHE_KEYS.CACHE_KEY_ETDR_TOKEN.ToString());
                SetCacheValue(ENUM_ETDR_CACHE_KEYS.CACHE_KEY_ETDR_TOKEN.ToString(), tokenETDR);
                return new LoginResponse()
                {
                    LoginResult = tokenETDR,
                };
            }
            else
            {
                return new LoginResponse()
                {
                    LoginResult = tokenValueInCache,
                };
            }
        }

        /// <summary>
        /// CheckLoginToken
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool CheckLoginToken(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return false;
            }

            string tokenInCache = GetCacheValue(ENUM_ETDR_CACHE_KEYS.CACHE_KEY_ETDR_TOKEN.ToString());
            if (string.IsNullOrEmpty(tokenInCache) || !string.Equals(token, tokenInCache))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Restart the token expiration
        /// </summary>
        /// <returns></returns>
        public LoginResponse UpdateLoginIDExpiry()
        {
            string tokenInCache = GetCacheValue(ENUM_ETDR_CACHE_KEYS.CACHE_KEY_ETDR_TOKEN.ToString());
            if (string.IsNullOrEmpty(tokenInCache))
            {
                return new LoginResponse()
                {
                    ErrMess = ETDRErrorMessages.LOGIN_ERROR_LOGIN_EMPTY_TOKEN,
                };
            }
            SetCacheValue(ENUM_ETDR_CACHE_KEYS.CACHE_KEY_ETDR_TOKEN.ToString(), tokenInCache);

            return new LoginResponse();
        }
    }
}