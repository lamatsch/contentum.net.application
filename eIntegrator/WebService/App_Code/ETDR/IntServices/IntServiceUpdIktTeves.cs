﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.ETDR.UpdIktTeves;
using Contentum.eQuery;
using Contentum.eUtility;

namespace Contentum.eIntegrator.WebService.ETDR.IntServices
{
    /// <summary>
    /// Summary description for IntServiceLogin
    /// </summary>
    public class IntServiceUpdIktTeves : BaseIntETDRService
    {
        public IntServiceUpdIktTeves()
        {
            base.ServiceType = ENUM_ETDR_SERVICETYPE.UPD_IKT_TEVES;
        }

        public Upd_IktTevesResponse Upd_IktTeves(Upd_IktTeves item)
        {
            if (item == null)
            {
                return new Upd_IktTevesResponse()
                {
                    ErrMess = ETDRErrorMessages.UPT_IKT_TEVES_ERROR_EMPTY_INPUT_PARAMETERS,
                };
            }

            try
            {
                return UpdIktTevesFlow(item);
            }
            catch (ResultException exc)
            {
                base.Log(exc);
                return new Upd_IktTevesResponse()
                {
                    ErrMess = ResultExtension.GetExceptionMessage(exc)
                };
            }
            catch (System.Exception exc)
            {
                base.Log(exc);
                return new Upd_IktTevesResponse()
                {
                    ErrMess = exc.Message
                };
            }
        }

        /// <summary>
        /// GetIntModFlow
        /// </summary>
        /// <param name="LoginNev"></param>
        /// <param name="LoginAzon"></param>
        /// <returns></returns>
        private Upd_IktTevesResponse UpdIktTevesFlow(Upd_IktTeves item)
        {
            base.CheckLogin(item.LoginNev, item.LoginAzon);

            string iktTip = ETDRKodtarFuggosegHelper.GetContentumKod(
                     this.ExecParamForServices,
                     item.Ikttip_Azon, ENUM_FUGGO_KODTAR.ETDR_IKTTIP.ToString(), ENUM_FUGGO_KODTAR.IRATTIPUS.ToString(), "Ikttip_Azon");

            int iratETDRId;
            if (!int.TryParse(item.Ikt_id, out iratETDRId))
            {
                return new Upd_IktTevesResponse()
                {
                    ErrMess = ETDRErrorMessages.UPT_IKT_TEVES_ERROR_INCORRECT_ETDR_IRATID
                };
            }

            string iratContentumId = ETDRObjHelper.GetObjId(this.ExecParamForServices, iratETDRId, ETDRObjHelper.ObjtTipKod.EREC_IraIratok, "IratId");
            if (string.IsNullOrEmpty(iratContentumId))
            {
                return new Upd_IktTevesResponse()
                {
                    ErrMess = ETDRErrorMessages.UPT_IKT_TEVES_ERROR_NOT_FOUND_ASSIGNED_CONTENTUM_IRATID
                };
            }

            EREC_IraIratok irat = ETDRConentumWrapper.GetIrat(base.ExecParamForServices, iratContentumId);
            if (irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
            {
                SztornoAllIratPeldany(iratContentumId);
            }
            else
            {
                IratFelszabaditas(iratContentumId);
            }

            return new Upd_IktTevesResponse();
        }

        /// <summary>
        /// IratFelszabaditas
        /// </summary>
        /// <param name="iratContentumId"></param>
        private void IratFelszabaditas(string iratContentumId)
        {
            eRecord.Service.EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result resultFelszabadit = service.Felszabaditas(base.ExecParamForServices, iratContentumId, true);
            resultFelszabadit.CheckError();
        }

        /// <summary>
        /// StornoAllIratPeldany
        /// </summary>
        /// <param name="iratContentumId"></param>
        private void SztornoAllIratPeldany(string iratContentumId)
        {
            eRecord.Service.EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            eQuery.BusinessDocuments.EREC_PldIratPeldanyokSearch src = new eQuery.BusinessDocuments.EREC_PldIratPeldanyokSearch();
            src.IraIrat_Id.Value = iratContentumId;
            src.IraIrat_Id.Operator = Query.Operators.equals;
            src.OrderBy = " Sorszam DESC";
            Result result = service.GetAll(base.ExecParamForServices, src);
            result.CheckError();

            if (result.Ds.Tables[0].Rows.Count < 1) { return; }

            string tmpIratPeldanyId;
            Result resultSztorno;
            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                tmpIratPeldanyId = result.Ds.Tables[0].Rows[i]["Id"].ToString();
                resultSztorno = service.Sztornozas(base.ExecParamForServices, tmpIratPeldanyId, "ETDR.Upd_IktTeves", false, string.Empty);
                resultSztorno.CheckError();
            }
        }
    }
}