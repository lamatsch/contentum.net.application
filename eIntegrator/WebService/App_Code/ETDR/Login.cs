﻿using System;
using System.Xml.Serialization;
using CommonConstants = Contentum.eIntegrator.WebService.ETDR.Constants;

namespace Contentum.eIntegrator.WebService.ETDR.Login
{
    #region Parameters

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE)]
    public class Login
    {
        // Az ÉTDR felhasználói neve
        [XmlElement("LoginNev", Order = 0)]
        public string LoginNev;

        // Az ÉTDR felhasználó jelszava
        [XmlElement("Password", Order = 1)]
        public string Password;

        // A kapcsolat lejárati ideje
        [XmlElement("LoginTimeout", Order = 2)]
        public int LoginTimeout;

        // A bejelentkezésnél kapott azonosító (utoljára kapott azonosító))
        [XmlElement("LoginAzon", Order = 3)]
        public string LoginAzon;
    }

    #endregion

    #region Response

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE)]
    public class LoginResponse
    {
        [XmlElement(Order = 0)]
        // A hibaüzenet, üres, ha nincs hiba
        public string ErrMess;

        [XmlElement(ElementName = "LoginResult", Order = 1)]
        // Amennyiben sikerül a kapcsolódás, egy szöveges azonosítót ad vissza, amelyet a későbbi funkcióknál az azonosításra
        // használ a service. Ha a kapcsolat nem sikeres, akkor üres string.
        public string LoginResult ;
    }

    #endregion
}

