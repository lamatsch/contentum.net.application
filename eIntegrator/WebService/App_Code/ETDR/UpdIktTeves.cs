﻿using System.Xml.Serialization;
using CommonConstants = Contentum.eIntegrator.WebService.ETDR.Constants;

namespace Contentum.eIntegrator.WebService.ETDR.UpdIktTeves
{
    #region Parameters

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Upd_IktTeves")]
    public class Upd_IktTeves
    {
        // Az ÉTDR felhasználói neve
        [XmlElement("LoginNev")]
        public string LoginNev;

        // A bejelentkezésnél kapott azonosító (utoljára kapott azonosító)
        [XmlElement("LoginAzon")]
        public string LoginAzon ;

        // Az ÉTDR alrendszer azonosítója
        [XmlElement("AlrendszAzon")]
        public int AlrendszAzon;

        // A típus egyedi szöveges azonosítója(az irattípusnál a rendszergazda által megadott szöveges azonosító)
        [XmlElement("Ikttip_Azon")]
        public string Ikttip_Azon;

        // Az irat azonosítója
        [XmlElement("Ikt_id")]
        public string Ikt_id;
    }

    #endregion

    #region Response

    [XmlType(Namespace = CommonConstants.DEFAULT_NAMESPACE,
             TypeName = "Upd_IktTevesResponse")]
    public class Upd_IktTevesResponse
    {
        [XmlElement(Order = 0)]
        // A hibaüzenet, üres, ha nincs hiba
        public string ErrMess;
    }

    #endregion
}
