﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Elhisz.BusinessObjects
{
    /// <summary>
    /// Summary description for ElhiszAdatok
    /// </summary>
    public class ElhiszAdatok
    {
        public Guid Kuldemeny_Id { get; set; }
        public Guid Irat_Id { get; set; }
        public string Irat_Azonosito { get; set; }
        public string FajlNev { get; set; }
        public string External_Link { get; set; }
        public EREC_eBeadvanyok eBeadvany { get; set; }
        public ELhiszCimzett Cimzett {get; set;}
        public DateTime Kuldemeny_LetrehozasIdo { get; set; }
        public string Irat_Csoport_Id_Ugyfelelos { get; set; }
        public string Irat_FelhasznaloCsoport_Id_Ugyintez { get; set; }
        public ElhiszAdatok(DataRow row)
        {
            this.Kuldemeny_Id = (Guid)row["Kuldemeny_Id"];
            this.Irat_Id = (Guid)row["Irat_Id"];
            this.Irat_Azonosito = row["Irat_Azonosito"].ToString();
            this.FajlNev = row["Dok_FajlNev"].ToString();
            this.External_Link = row["Dok_External_Link"].ToString();
            this.eBeadvany = new EREC_eBeadvanyok();
            eBeadvany.Id = row["eBeadvany_Id"].ToString();
            eBeadvany.KR_HivatkozasiSzam = row["eBeadvany_KR_HivatkozasiSzam"].ToString();
            eBeadvany.KR_DokTipusHivatal = row["eBeadvany_KR_DokTipusHivatal"].ToString();
            eBeadvany.KR_DokTipusAzonosito = row["eBeadvany_KR_DokTipusAzonosito"].ToString();
            eBeadvany.KR_DokTipusLeiras = row["eBeadvany_KR_DokTipusLeiras"].ToString();
            eBeadvany.KR_Megjegyzes = row["eBeadvany_KR_Megjegyzes"].ToString();
            eBeadvany.KR_Valaszutvonal = row["eBeadvany_KR_Valaszutvonal"].ToString();
            eBeadvany.KR_FileNev = row["eBeadvany_KR_FileNev"].ToString();
            eBeadvany.KR_Valasztitkositas = row["eBeadvany_KR_Valasztitkositas"].ToString();
            eBeadvany.PR_Parameterek = row["eBeadvany_PR_Parameterek"].ToString();
            ELhiszCimzett cimzett = new ELhiszCimzett();
            cimzett.Sorszam = (int)row["Peldany_Sorszam"];
            cimzett.SetCimzett(row["Peldany_CimSTR_Cimzett"].ToString());
            this.Kuldemeny_LetrehozasIdo = (DateTime)row["Kuldemeny_LetrehozasIdo"];
            this.Cimzett = cimzett;
            this.Irat_Csoport_Id_Ugyfelelos = row["Irat_Csoport_Id_Ugyfelelos"].ToString();
            this.Irat_FelhasznaloCsoport_Id_Ugyintez = row["Irat_FelhasznaloCsoport_Id_Ugyintez"].ToString();
        }
    }

    public class ELhiszCimzett
    {
        public int Sorszam { get; set; }
        public string KRID { get; set; }
        public string KapcsolatiKod { get; set; }
        public void SetCimzett(string value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                //személy
                if (value.Length > 9)
                    this.KapcsolatiKod = value;
                else
                    this.KRID = value;
            }
        }
    }
}