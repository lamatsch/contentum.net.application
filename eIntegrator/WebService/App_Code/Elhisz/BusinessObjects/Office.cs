﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Elhisz.BusinessObjects
{
    /// <summary>
    /// Summary description for Office
    /// </summary>
    public class Office
    {
        public decimal Id { get; set; }
        public string OfficeShortName { get; set; }
        public string OfficeName { get; set; }
        public string MakkCode { get; set; }
        public string Krid { get; set; }
        public DateTime? LastModifiedDate { get; set; }

        public string Azonosito
        {
            get
            {
                return String.Format("{0},{1},{2}", this.OfficeShortName, this.MakkCode, this.Krid);
            }

        }

        public static Office Create(DataRow row)
        {
            Office office = new Office();
            office.Id = (decimal)row["id"];
            office.OfficeShortName = row["office_short_name"].ToString();
            office.OfficeName = row["office_name"].ToString();
            office.MakkCode = row["makk_code"].ToString();
            office.Krid = row["krid"].ToString();
            office.LastModifiedDate = row["last_modified_date"] as DateTime?;
            return office;
        }
    }
}