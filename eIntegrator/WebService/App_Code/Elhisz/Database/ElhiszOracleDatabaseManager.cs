﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using Oracle.DataAccess.Client;
using Contentum.eUtility;
using Elhisz.BusinessObjects;

namespace Elhisz.DataBase
{
    /// <summary>
    /// Summary description for ElhiszOracleDatabaseManager
    /// </summary>
    public class ElhiszOracleDatabaseManager
    {
        public static String GetConnectionString()
        {
            ConnectionStringSettings conn = new ConnectionStringSettings();
            conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ElhiszdConnectionString"];

            return conn.ConnectionString;
        }

        public ElhiszOracleDatabaseManager()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        const string _cmdGetRecipientData = @"select rd.resolutionnumber, rd.xml_contact_id, rd.errorcode, rd.kr_registrationnumber, rd.createdate
                              from elhiszdbssn.recipient_data rd
                              where rd.resolutionnumber in ({0}) 
                              order by rd.createdate desc";

        public DataSet GetRecipientData(List<string> resolutionNumberList)
        {
            Logger.Info("DataBaseManager.GetRecipientData kezdete");
            Logger.Debug(String.Format("resolutionNumberList={0}", String.Join(",", resolutionNumberList.ToArray())));

            string inner = String.Join("','", resolutionNumberList.ToArray());
            inner = "'" + inner + "'";
            string _cmd = String.Format(_cmdGetRecipientData, inner);

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(_cmd, conn);
                cmd.CommandType = CommandType.Text;
                cmd.BindByName = true;

                DataSet ds = new DataSet();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                Logger.Info("DataBaseManager.GetRecipientData vege");

                return ds;

            }
        }

        const string _cmdGetRecipientDataForTertiveveny = @"select rd.resolutionnumber, rd.xml_contact_id, rd.errorcode, rd.kr_registrationnumber,
                              rd.kr_receiptregistrationnumber, rd.kr_receiptfile, rd.kr_nondeliveryregnumber, rd.kr_nondeliveryfile, rd.receiptregistrationnumber
                              from elhiszdbssn.recipient_data rd
                              where rd.kr_registrationnumber in ({0})";

        public DataSet GetRecipientDataForTertiveveny(List<string> kr_registrationNumberList)
        {
            Logger.Info("DataBaseManager.GetRecipientDataForTertiveveny kezdete");
            Logger.Debug(String.Format("kr_registrationNumberList={0}", String.Join(",", kr_registrationNumberList.ToArray())));

            string inner = String.Join("','", kr_registrationNumberList.ToArray());
            inner = "'" + inner + "'";
            string _cmd = String.Format(_cmdGetRecipientDataForTertiveveny, inner);

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(_cmd, conn);
                cmd.CommandType = CommandType.Text;
                cmd.BindByName = true;

                DataSet ds = new DataSet();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                Logger.Info("DataBaseManager.GetRecipientDataForTertiveveny vege");

                return ds;

            }
        }

        const string _cmdGetOffices = "select id, office_short_name, office_name, makk_code, krid, last_modified_date from elhiszdbssn.office";

        public List<Office> GetOffices()
        {
            Logger.Info("ElhiszOracleDatabaseManager.GetOffices kezdete");

            DataSet ds = new DataSet();

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(_cmdGetOffices, conn);
                cmd.CommandType = CommandType.Text;
                cmd.BindByName = true;

                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);
            }

            List<Office> offices = new List<Office>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Office office = Office.Create(row);
                offices.Add(office);
            }

            Logger.Info("ElhiszOracleDatabaseManager.GetOffices vege");

            return offices;
        }
    }
}