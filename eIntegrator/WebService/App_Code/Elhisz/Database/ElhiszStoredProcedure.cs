﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Elhisz.DataBase
{
    /// <summary>
    /// Summary description for INT_ElhiszServiceStoredProcedure
    /// </summary>
    public class ElhiszStoredProcedure
    {
        private DataContext dataContext;

        public ElhiszStoredProcedure()
        {
            this.dataContext = new DataContext(null as HttpApplicationState);
        }
        public ElhiszStoredProcedure(DataContext _dataContext)
        {
            this.dataContext = _dataContext;
        }

        public Result GetElhiszAdatok(ExecParam ExecParam, string where)
        {
            Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_GetElhiszAdatok");

            Result _ret = new Result();
            bool isConnectionOpenHere = false;

            try
            {
                isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

                SqlCommand SqlComm = new SqlCommand("[sp_GetElhiszAdatok]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where"].Value = where;
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }

            log.SpEnd(ExecParam, _ret);
            return _ret;

        }
    }
}