﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Elhisz.BusinessObjects;
using Elhisz.DataBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Elhisz
{
    /// <summary>
    /// Summary description for HivatalokSyncManager
    /// </summary>
    public class HivatalokSyncManager
    {
        ExecParam execParam;

        List<KRT_Partnerek> partnerek;

        List<Office> offices;

        int ujPartnerekSzama = 0;
        int toroltPartnerekSzama = 0;

        public HivatalokSyncManager(ExecParam execParam)
        {
            this.execParam = execParam;
        }

        public void HivatalokFromElhisz()
        {
            Logger.Info("HivatalokSyncManager.HivatalokFromElhisz kezdete");

            LoadOffices();

            LoadHivatalok();

            foreach (Office office in offices)
            {
                ProcessOffice(office);
            }

            foreach (KRT_Partnerek partner in partnerek)
            {
                ProcessPartner(partner);
            }

            Logger.Info(String.Format("Új partnerek: {0}, Törölt partnerek: {1}", ujPartnerekSzama, toroltPartnerekSzama));

            Logger.Info("HivatalokSyncManager.HivatalokFromElhisz vege");
        }

        void LoadOffices()
        {
            Logger.Debug("HivatalokSyncManager.LoadOffices kezdete");

            ElhiszOracleDatabaseManager db = new ElhiszOracleDatabaseManager();
            offices = db.GetOffices();

            Logger.Info(String.Format("Hivatalok száma: {0}", offices.Count));
            Logger.Debug("HivatalokSyncManager.LoadOffices kezdete");
        }

        void LoadHivatalok()
        {
            Logger.Debug("HivatalokSyncManager.LoadHivatalok kezdete");

            partnerek = new List<KRT_Partnerek>();

            KRT_PartnerekService partnerServcie = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.Forras.Value = KodTarak.Partner_Forras.HKP;
            partnerekSearch.Forras.Operator = Query.Operators.equals;
            partnerekSearch.TopRow = 99999;

            Result result = partnerServcie.GetAll(execParam, partnerekSearch);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                KRT_Partnerek partner = new KRT_Partnerek();
                Utility.LoadBusinessDocumentFromDataRow(partner, row);
                partnerek.Add(partner);
            }

            Logger.Info(String.Format("Partnerek száma: {0}", partnerek.Count));

            Logger.Debug("HivatalokSyncManager.LoadHivatalok vege");

        }

        void ProcessOffice(Office office)
        {
            Logger.Debug(String.Format("Office: {0}", office.Azonosito));

            if (partnerek.Exists(p => p.KulsoAzonositok == office.Azonosito))
            {
                Logger.Debug("Hivatal létezik!");
            }
            else
            {
                try
                {
                    AddHivatal(office);
                    ujPartnerekSzama++;
                }
                catch (Exception ex)
                {
                    Logger.Error(String.Format("AddHivatal hiba: {0}", office.Azonosito), ex);
                }
            }
        }

        void AddHivatal(Office office)
        {
            Logger.Debug("HivatalokSyncManager.AddHivatal kezdete");

            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Nev = office.OfficeName;
            partner.Forras = KodTarak.Partner_Forras.HKP;
            partner.Tipus = KodTarak.Partner_Tipus.Szervezet;
            partner.KulsoAzonositok = office.Azonosito;
            partner.Allapot = KodTarak.Partner_Allapot.Jovahagyott;
            partner.Belso = "0";

            KRT_PartnerekService partnerServcie = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            Logger.Debug("KRT_PartnerekService.Insert");
            Result partnerResult = partnerServcie.Insert(execParam, partner);

            if (partnerResult.IsError)
            {
                throw new ResultException(partnerResult);
            }

            string partnerId = partnerResult.Uid;

            Logger.Debug(String.Format("partnerId={0}", partnerId));

            AddCim(partnerId, office);

            AddCimFromMakKod(partnerId, office);

            Logger.Debug("HivatalokSyncManager.AddHivatal vege");
        }

        void AddCim(string partnerId, Office office)
        {
            Logger.Debug("HivatalokSyncManager.AddCim kezdete");

            KRT_Cimek cim = new KRT_Cimek();
            cim.CimTobbi = office.Krid;
            cim.Forras = KodTarak.Cim_Forras.HKP;
            cim.Tipus = KodTarak.Cim_Tipus.TitkosKapcsolatiKod;
            cim.Kategoria = KodTarak.Cim_Kategoria.K;

            KRT_CimekService cimService = eAdminService.ServiceFactory.GetKRT_CimekService();

            Logger.Debug("KRT_CimekService.Insert");
            Result cimResult = cimService.Insert(execParam, cim);

            if (cimResult.IsError)
            {
                throw new ResultException(cimResult);
            }

            string cimId = cimResult.Uid;

            Logger.Debug(String.Format("cimId={0}", cimId));

            KRT_PartnerCimek partnerCim = new KRT_PartnerCimek();
            partnerCim.Partner_id = partnerId;
            partnerCim.Cim_Id = cimId;

            KRT_PartnerCimekService partnerCimService = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();

            Logger.Debug("KRT_PartnerCimekService.Insert");
            Result partnerCimResult = partnerCimService.Insert(execParam, partnerCim);

            if (partnerCimResult.IsError)
            {
                throw new ResultException(partnerCimResult);
            }

            Logger.Debug("HivatalokSyncManager.AddCim vege");
        }

        /// <summary>
        /// AddCimFromMakKod
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="office"></param>
        void AddCimFromMakKod(string partnerId, Office office)
        {
            Logger.Debug("HivatalokSyncManager.AddCimFromMakKod kezdete");

            if (office == null || string.IsNullOrEmpty(office.MakkCode))
            {
                Logger.Debug("HivatalokSyncManager.AddCimFromMakKod.MakkCode.Ures");
                return;
            }

            KRT_Cimek cim = new KRT_Cimek();
            cim.CimTobbi = office.MakkCode;
            cim.Forras = KodTarak.Cim_Forras.HKP;
            cim.Tipus = KodTarak.Cim_Tipus.MAK_KOD;
            cim.Kategoria = KodTarak.Cim_Kategoria.K;

            KRT_CimekService cimService = eAdminService.ServiceFactory.GetKRT_CimekService();

            Logger.Debug("KRT_CimekService.Insert");
            Result cimResult = cimService.Insert(execParam, cim);

            if (cimResult.IsError)
            {
                throw new ResultException(cimResult);
            }

            string cimId = cimResult.Uid;

            Logger.Debug(String.Format("cimId={0}", cimId));

            KRT_PartnerCimek partnerCim = new KRT_PartnerCimek();
            partnerCim.Partner_id = partnerId;
            partnerCim.Cim_Id = cimId;

            KRT_PartnerCimekService partnerCimService = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();

            Logger.Debug("KRT_PartnerCimekService.Insert");
            Result partnerCimResult = partnerCimService.Insert(execParam, partnerCim);

            if (partnerCimResult.IsError)
            {
                throw new ResultException(partnerCimResult);
            }

            Logger.Debug("HivatalokSyncManager.AddCimFromMakKod vege");
        }

        void ProcessPartner(KRT_Partnerek partner)
        {
            Logger.Debug(String.Format("partner: {0}", partner.KulsoAzonositok));

            if (!offices.Exists(o => o.Azonosito == partner.KulsoAzonositok))
            {
                try
                {
                    InvalidatePartner(partner);
                    toroltPartnerekSzama++;
                }
                catch (Exception ex)
                {
                    Logger.Error(String.Format("InvalidatePartner hiba: {0}", partner.KulsoAzonositok), ex);
                }
            }
            else
            {
                Logger.Debug("Partner létezik!");
            }
        }

        void InvalidatePartner(KRT_Partnerek partner)
        {
            Logger.Debug("HivatalokSyncManager.InvalidatePartner kezdete");

            Logger.Debug(String.Format("partnerId={0}", partner.Id));

            KRT_PartnerekService partnerServcie = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParamPartnerInvalidate = execParam.Clone();
            execParamPartnerInvalidate.Record_Id = partner.Id;

            Logger.Debug("KRT_PartnerekService.Invalidate");
            Result partnerResult = partnerServcie.Invalidate(execParamPartnerInvalidate);

            if (partnerResult.IsError)
            {
                throw new ResultException(partnerResult);
            }

            InvalidateCim(partner.Id);


            Logger.Debug("HivatalokSyncManager.InvalidatePartner vege");
        }

        void InvalidateCim(string partnerId)
        {
            Logger.Debug("HivatalokSyncManager.InvalidateCim kezdete");

            KRT_PartnerCimekService partnerCimService = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            KRT_PartnerCimekSearch partnerCimSearch = new KRT_PartnerCimekSearch();
            partnerCimSearch.Partner_id.Value = partnerId;
            partnerCimSearch.Partner_id.Operator = Query.Operators.equals;

            Logger.Debug("KRT_PartnerCimekService.GetAll");
            Result partnerCimResult = partnerCimService.GetAll(execParam, partnerCimSearch);

            if (partnerCimResult.IsError)
            {
                throw new ResultException(partnerCimResult);
            }

            KRT_CimekService cimService = eAdminService.ServiceFactory.GetKRT_CimekService();
            Result cimResult = new Result();

            foreach (DataRow row in partnerCimResult.Ds.Tables[0].Rows)
            {
                string id = row["Id"].ToString();
                string cimId = row["Cim_Id"].ToString();

                ExecParam execParamPartnerCimInvalidate = execParam.Clone();
                execParamPartnerCimInvalidate.Record_Id = id;
                Logger.Debug("KRT_PartnerCimekService.Invalidate");
                partnerCimResult = partnerCimService.Invalidate(execParamPartnerCimInvalidate);

                if (partnerCimResult.IsError)
                {
                    throw new ResultException(partnerCimResult);
                }

                ExecParam execParamCimInvalidate = execParam.Clone();
                execParamCimInvalidate.Record_Id = cimId;
                Logger.Debug("KRT_CimekService.Invalidate");
                cimResult = cimService.Invalidate(execParamCimInvalidate);

                if (cimResult.IsError)
                {
                    throw new ResultException(cimResult);
                }
            }

            Logger.Debug("HivatalokSyncManager.InvalidateCim vege");
        }
    }
}