﻿using Contentum.eBusinessDocuments;
using Elhisz.BusinessObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace Elhisz
{
    /// <summary>
    /// Summary description for XmlHelper
    /// </summary>
    public class RecipientsXmlHelper
    {
        XNamespace nsElhisz = "http://10.0.0.120:8081/elhisz";

        public string CreateRecipientsXML(ElhiszAdatok adatok, List<ELhiszCimzett> cimzettek)
        {

            XDocument doc = new XDocument
            (
                 new XElement(nsElhisz + "ELHISZRecipients",
                new XAttribute("Id", "ELHISZRecipients-1"),
                new XAttribute("Version", "1.0"),
                new XAttribute(XNamespace.Xmlns + "elhisz", nsElhisz))
            );

            XElement elhiszParameters = new XElement(nsElhisz + "ELHISZParameters");
            doc.Root.Add(elhiszParameters);

            AddMeta(elhiszParameters, "HivatkozasiSzam", adatok.eBeadvany.KR_HivatkozasiSzam);
            AddMeta(elhiszParameters, "DokTipusHivatal", adatok.eBeadvany.KR_DokTipusHivatal);
            AddMeta(elhiszParameters, "DokTipusAzonosito", adatok.eBeadvany.KR_DokTipusAzonosito);
            AddMeta(elhiszParameters, "DokTipusLeiras", adatok.eBeadvany.KR_DokTipusLeiras);
            AddMeta(elhiszParameters, "Megjegyzes", adatok.eBeadvany.KR_Megjegyzes);
            AddMeta(elhiszParameters, "ValaszUtvonal", adatok.eBeadvany.KR_Valaszutvonal);
            AddMeta(elhiszParameters, "FileNev", adatok.eBeadvany.KR_FileNev);
            AddMeta(elhiszParameters, "ValaszTitkositas", adatok.eBeadvany.KR_Valasztitkositas == "1" ? "true" : "false");

            PR_Parameterek pr_parameter = adatok.eBeadvany.Get_PR_Parameterek();

            elhiszParameters.Add(new XElement(nsElhisz + "InputFormat", new XAttribute("format", pr_parameter.InputFormat)));
            elhiszParameters.Add(new XElement(nsElhisz + "OutputFormat", new XAttribute("format", pr_parameter.OutputFormat)));

            foreach (ELhiszCimzett cimzett in cimzettek)
            {
                AddRecipient(doc.Root, cimzett.Sorszam, cimzett.KRID, cimzett.KapcsolatiKod);
            }

            var wr = new Utf8StringWriter();
            doc.Save(wr);
            return wr.ToString();
        }

        void AddMeta(XElement elhiszParameters, string name, string value)
        {
            elhiszParameters.Add(new XElement(nsElhisz + "HKPMeta", new XAttribute("name", name), value));
        }

        void AddRecipient(XElement root, int sorszam, string krid, string kapcsolatiKod)
        {
            string elementName = "KRID";

            if (!String.IsNullOrEmpty(krid))
            {
                elementName = "KRID";

                root.Add(new XElement(nsElhisz + "Recipient", new XAttribute("Id", sorszam.ToString())
                                , new XElement(nsElhisz + elementName, new XAttribute("Id", sorszam.ToString()), krid)
                                ));
            }

            if (!String.IsNullOrEmpty(kapcsolatiKod))
            {
                elementName = "KapcsKod";

                root.Add(new XElement(nsElhisz + "Recipient", new XAttribute("Id", sorszam.ToString())
                                , new XElement(nsElhisz + elementName, new XAttribute("Id", sorszam.ToString()), kapcsolatiKod)
                                ));
            }
        }

        private class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding { get { return Encoding.UTF8; } }
        }

    }

}