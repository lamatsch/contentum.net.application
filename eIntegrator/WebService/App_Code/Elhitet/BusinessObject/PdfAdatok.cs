﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace NMHH.Elhitet.BusinessObject
{
    /// <summary>
    /// Summary description for PdfAdatok
    /// </summary>
    public class PdfAdatok
    {
        public string ElhiszErkeztetoszam { get; set; }
        public string Ugyiratszam { get; set; }
        public string Erkeztetoszam { get; set; }
        public DateTime? ErkezettDatum { get; set; }
        public string FajlNev { get; set; }
        public string External_Link { get; set; }

        public string GetErkezettDatumString()
        {
            if (this.ErkezettDatum != null)
                return this.ErkezettDatum.ToString();

            return String.Empty;
        }

        public void LoadBusinessDocumentFromDataAdapter(System.Data.SqlClient.SqlCommand sqlcommand)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = sqlcommand;
            da.Fill(ds);

            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Reflection.PropertyInfo[] Properties = this.GetType().GetProperties();
                    foreach (System.Reflection.PropertyInfo P in Properties)
                    {
                        if (!(ds.Tables[0].Rows[0].IsNull(P.Name)))
                        {
                            P.SetValue(this, ds.Tables[0].Rows[0][P.Name], null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                da.Dispose();
            }
        }
    }
}