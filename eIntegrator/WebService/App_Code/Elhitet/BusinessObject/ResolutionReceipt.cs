﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMHH.Elhitet.BusinessObject
{
    /// <summary>
    /// Summary description for ResolutionReceipt
    /// </summary>
    public class ResolutionReceipt
    {
        public string RegistrationNumber { get; set; }
        public string ResolutionNumber { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string ReceiptData { get; set; }
        public byte[] BedszPDF { get; set;}
    }
}