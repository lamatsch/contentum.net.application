﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using Contentum.eUtility;
using Oracle.DataAccess.Client;

namespace NMHH.Elhitet.Database
{
    /// <summary>
    /// Summary description for ElhitetOracleDatabaseManager
    /// </summary>
    public class ElhitetOracleDatabaseManager
    {
        public static String GetConnectionString()
        {
            ConnectionStringSettings conn = new ConnectionStringSettings();
            conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ElhitetdConnectionString"];

            return conn.ConnectionString;
        }

        string schema;
        public ElhitetOracleDatabaseManager()
        {
            schema = IntegratorParameterek.GetParameter(IntegratorParameterek.eUzenetSchema);

            if (String.IsNullOrEmpty(schema))
            {
                schema = "elhiszdb";
            }
        }

        private string GetCommandText(string cmd)
        {
            return cmd.Replace("#schema#", schema);
        }

        const string _cmdGetResolutionData = @"select ri.filenev, rd.completecontent from 
                                #schema#.resolutioninformation ri,
                                #schema#.resolutiondata rd
                                where ri.registrationnumber = rd.registrationnumber
                                and ri.resolutionnumber = :resolutionnumber
                                and rd.completecontentsize > 0";

        public DataSet GetResolutionData(string resolutionNumber)
        {
            Logger.Info("ElhitetOracleDatabaseManager.GetResolutionData kezdete");
            Logger.Debug(String.Format("resolutionNumber={0}", resolutionNumber));

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(GetCommandText(_cmdGetResolutionData), conn);
                cmd.CommandType = CommandType.Text;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("resolutionnumber", resolutionNumber));

                DataSet ds = new DataSet();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                Logger.Info("ElhitetOracleDatabaseManager.GetResolutionData vege");

                return ds;

            }
        }

        const string _cmdGetResolutionReceipts = @"select rr.registrationnumber, rr.resolutionnumber, rr.receiveddate, rr.receiptdata 
                                                   from #schema#.resolutionreceipts rr
                                                   where rr.registrationnumber = :registrationnumber";

        public DataSet GetResolutionReceipts(string registrationNumber)
        {
            Logger.Info("ElhitetOracleDatabaseManager.GetResolutionReceipts kezdete");
            Logger.Debug(String.Format("registrationNumber={0}", registrationNumber));

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(GetCommandText(_cmdGetResolutionReceipts), conn);
                cmd.CommandType = CommandType.Text;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("registrationnumber", registrationNumber));

                DataSet ds = new DataSet();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                Logger.Info("ElhitetOracleDatabaseManager.GetResolutionReceipts vege");

                return ds;

            }
        }



        const string _cmdUpdateResolutionReceipts = @"update #schema#.resolutionreceipts
                                                         set ActualState = :actualState
                                                         where RegistrationNumber = :registrationNumber";

        const int actualState = 9;
        public int UpdateResolutionReceipts(string registrationNumber)
        {
            Logger.Info("ElhitetOracleDatabaseManager.UpdateResolutionReceipts kezdete");
            Logger.Debug(String.Format("registrationNumber={0}", registrationNumber));

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(GetCommandText(_cmdUpdateResolutionReceipts), conn);
                cmd.CommandType = CommandType.Text;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("registrationNumber", registrationNumber));
                cmd.Parameters.Add(new OracleParameter("actualState", actualState));

                int ret = cmd.ExecuteNonQuery();

                Logger.Info("ElhitetOracleDatabaseManager.UpdateResolutionReceipts vege");

                return ret;

            }
        }
    }
}