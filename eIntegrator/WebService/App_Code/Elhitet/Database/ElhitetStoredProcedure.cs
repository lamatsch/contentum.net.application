﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using NMHH.Elhitet.BusinessObject;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace NMHH.Elhitet.Database
{
    /// <summary>
    /// Summary description for ElhitetStoredProcedure
    /// </summary>
    public class ElhitetStoredProcedure
    {
        private DataContext dataContext;

        public ElhitetStoredProcedure()
        {
            this.dataContext = new DataContext(null as HttpApplicationState);
        }
        public ElhitetStoredProcedure(DataContext _dataContext)
        {
            this.dataContext = _dataContext;
        }

        public Result GetPDFAdatok(ExecParam ExecParam, string dokumentumId)
        {
            Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_GetPDFAdatok");

            Result _ret = new Result();
            bool isConnectionOpenHere = false;

            try
            {
                isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

                SqlCommand SqlComm = new SqlCommand("[sp_GetPDFAdatok]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@DokumentumId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@DokumentumId"].Value = new Guid(dokumentumId);
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

                PdfAdatok _PdfAdatok = new PdfAdatok();
                _PdfAdatok.LoadBusinessDocumentFromDataAdapter(SqlComm);
                _ret.Record = _PdfAdatok;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }

            log.SpEnd(ExecParam, _ret);
            return _ret;

        }
    }
}