﻿using Contentum.eUtility;
using NMHH.Elhitet.BusinessObject;
using NMHH.Elhitet.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace NMHH.Elhitet
{
    /// <summary>
    /// Summary description for ElhitetManager
    /// </summary>
    public class ElhitetManager
    {
        public ResolutionReceipt GetResolutionReceipts(string registrationNumber)
        {
            Logger.Info("ElhitetManager.GetResolutionReceipts kezdete");
            Logger.Debug(String.Format("registrationNumber={0}", registrationNumber));

            ElhitetOracleDatabaseManager db = new ElhitetOracleDatabaseManager();
            DataSet ds = db.GetResolutionReceipts(registrationNumber);

            if (ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error("ResolutionReceipts tétel nem található!");
                return null;
            }
            else
            {
                DataRow row = ds.Tables[0].Rows[0];
                ResolutionReceipt receipt = new ResolutionReceipt();
                try
                {
                    receipt.RegistrationNumber = row["registrationnumber"].ToString();
                    receipt.ResolutionNumber = row["resolutionnumber"].ToString();
                    receipt.ReceivedDate = (DateTime)row["receiveddate"];
                    receipt.ReceiptData = row["receiptdata"].ToString();
                    receipt.BedszPDF = GetBedszPDF(receipt.ReceiptData);
                }
                catch (Exception ex)
                {
                    Logger.Error("ResolutionReceipt objektum létrehozása hiba!", ex);
                    throw;
                }

                return receipt;
            }
        }

        byte[] GetBedszPDF(string receiptData)
        {
            Logger.Info("ElhitetManager.GetBedszPDF kezdete");

            if (String.IsNullOrEmpty(receiptData))
            {
                Logger.Error("ReceiptData üres!");
                return null;
            }
            else
            {
                XDocument doc = XDocument.Parse(receiptData);

                string value = (from element in doc.Root.Descendants()
                         where element.Name.LocalName == "bedszPDF"
                         select element.Value).Single();

                byte[] pdf = Convert.FromBase64String(value);

                return pdf;
            }
        }
    }
}