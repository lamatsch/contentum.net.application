﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace NMHH.Elhitet.Servlet
{
    /// <summary>
    /// Summary description for ELHISZServerResolutionResponse
    /// </summary>
    public class ELHISZServerResolutionResponse : ServletResponse
    {
        private string _ResponseString;

        public string ResponseString
        {
            get { return _ResponseString; }
            set { _ResponseString = value; }
        }

        private string _ErrorCode;

        public string ErrorCode
        {
            get { return _ErrorCode; }
            set { _ErrorCode = value; }
        }

        private string _ErrorMessage;

        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }

        private string _RegistrationNumber;

        public string RegistrationNumber
        {
            get { return _RegistrationNumber; }
            set { _RegistrationNumber = value; }
        }

        public bool IsError
        {
            get
            {
                return ErrorCode != "0";
            }
        }

        public ELHISZServerResolutionResponse(ServletResponse response) : base(response.ResponseData, response.ResponseHeaders)
        {
            this.ResponseString = Encoding.UTF8.GetString(response.ResponseData);
            this.ParseResponse();
        }

        void ParseResponse()
        {
            if (!String.IsNullOrEmpty(this.ResponseString))
            {
                XDocument doc = XDocument.Parse(this.ResponseString);

                string errorCode = (from element in doc.Root.Elements()
                                    where element.Name.LocalName == "ErrorCode"
                                    select element.Value).FirstOrDefault();

                string errorMessage = (from element in doc.Root.Elements()
                                       where element.Name.LocalName == "ErrorMessage"
                                       select element.Value).FirstOrDefault();

                string registrationNumber = (from element in doc.Root.Elements()
                                             where element.Name.LocalName == "RegistrationNumber"
                                             select element.Value).FirstOrDefault();

                if (!String.IsNullOrEmpty(errorCode))
                    errorCode = errorCode.Trim();

                if (!String.IsNullOrEmpty(errorMessage))
                    errorMessage = errorMessage.Trim();

                if (!String.IsNullOrEmpty(registrationNumber))
                    registrationNumber = registrationNumber.Trim();

                this.ErrorCode = errorCode;
                this.ErrorMessage = errorMessage;
                this.RegistrationNumber = registrationNumber;
            }
        }
    }
}