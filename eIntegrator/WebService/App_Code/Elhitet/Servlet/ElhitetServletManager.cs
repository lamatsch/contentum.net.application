﻿using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Web;

namespace NMHH.Elhitet.Servlet
{
    /// <summary>
    /// Summary description for ElhitetServletManager
    /// </summary>
    public class ElhitetServletManager
    {
        static string servletUrl;

        static ElhitetServletManager()
        {
            servletUrl = ConfigurationManager.AppSettings["ElhitetServlet_URL"];
            InitiateSSLTrust();
        }

        public static void InitiateSSLTrust()
        {
            try
            {
                //Change SSL checks so that all checks pass
                ServicePointManager.ServerCertificateValidationCallback =
                   new RemoteCertificateValidationCallback(
                        delegate
                        { return true; }
                    );
            }
            catch (Exception ex)
            {
                Logger.Error("InitiateSSLTrust", ex);
            }
        }

        WebClient GetWebClient()
        {
            WebClient wc = new WebClient();
            return wc;
        }

        ServletResponse CallMethod(string action, Dictionary<string, string> parameters, Dictionary<string, string> headers)
        {
            Logger.Info("ElhitetServletManager.CallMethod");
            string url = GetUrl(action, parameters);
            Logger.Info(String.Format("url={0}", url));
            using (WebClient wc = GetWebClient())
            {
                AddHeaders(wc, headers);
                byte[] response = wc.DownloadData(url);
                return new ServletResponse(response, wc.ResponseHeaders);
            }
        }

        ServletResponse PostMethod(string action, byte[] data, Dictionary<string, string> parameters, Dictionary<string, string> headers)
        {
            Logger.Info("ElhitetServletManager.PostMethod");
            string url = GetUrl(action, parameters);
            Logger.Info(String.Format("url={0}", url));
            using (WebClient wc = GetWebClient())
            {
                AddHeaders(wc, headers);
                byte[] response = wc.UploadData(url, data);
                return new ServletResponse(response, wc.ResponseHeaders);
            }
        }

        string GetUrl(string action, Dictionary<string, string> parameters)
        {
            if (String.IsNullOrEmpty(servletUrl))
            {
                string error = "ElhitetServlet_URL config paraméter nincs megadva!";
                Logger.Error(error);
                throw new Exception(error);
            }

            string actionUrl = GetActionUrl(action);

            string queryString = String.Empty;

            if (parameters != null)
            {
                StringBuilder sb = new StringBuilder();
                int index = 0;
                foreach (KeyValuePair<string, string> parameter in parameters)
                {
                    if (index == 0)
                    {
                        sb.Append("?");
                    }
                    else
                    {
                        sb.Append("&");
                    }
                    sb.Append(parameter.Key);
                    sb.Append("=");
                    sb.Append(HttpUtility.UrlEncode(parameter.Value));
                    index++;
                }
                queryString = sb.ToString();
            }
            string url = actionUrl + queryString;
            return url;
        }

        string GetActionUrl(string action)
        {
            return String.Format("{0}/{1}", servletUrl.Trim('/'), action);
        }

        void AddHeaders(WebClient wc, Dictionary<string, string> headers)
        {
            if (headers != null)
            {
                foreach (KeyValuePair<string, string> header in headers)
                {
                    wc.Headers.Add(header.Key, header.Value);
                }
            }
        }

        public ServletResponse CreatePDF(string elhiszErkeztetoszam, string ugyiratszam, string erkeztetoszam, string erkezettDatum)
        {
            Logger.Info("ElhitetServletManager.CreatePDF");
            Logger.Debug(String.Format("elhiszErkeztetoszam={0}", elhiszErkeztetoszam));
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("ElhiszErkeztetoszam", elhiszErkeztetoszam);
            parameters.Add("Ugyiratszam", ugyiratszam);
            parameters.Add("Erkeztetoszam", erkeztetoszam);
            parameters.Add("ErkezettDatum", erkezettDatum);
            ServletResponse response = CallMethod("recipientsconverter/PDFCreator", parameters, null);
            return response;
        }

        public ServletResponse CreatePDF(byte[] data)
        {
            Logger.Info("ElhitetServletManager.CreatePDF");
            ServletResponse response = PostMethod("recipientsconverter/PDFCreator", data, null, null);
            return response;
        }

        public ELHISZServerResolutionResponse UnsignedResolution(string fileName, string resolutionModuleID, string resolutionNumber, 
            string submissionNumber, string organizationUnit, string submissionFormID, string completionFormID, byte[] fileContent)
        {
            Logger.Info("ElhitetServletManager.UnsignedResolution");
            Logger.Debug(String.Format("resolutionNumber={0}", resolutionNumber));
            Dictionary<string, string> headers = new Dictionary<string, string>();

            //header kódolva van
            headers.Add("encoded", "1");

            headers.Add("FileName", EncodeHeaderValue(fileName));
            headers.Add("ResolutionModuleID", EncodeHeaderValue(resolutionModuleID));
            headers.Add("ResolutionNumber", EncodeHeaderValue(resolutionNumber));
            headers.Add("SubmissionNumber", EncodeHeaderValue(submissionNumber));
            headers.Add("OrganizationUnit", EncodeHeaderValue(organizationUnit));
            headers.Add("SubmissionFormID", EncodeHeaderValue(submissionFormID));
            headers.Add("CompletionFormID", EncodeHeaderValue(completionFormID));
            

            ServletResponse response = PostMethod("e-nhh/unsignedresolution.post.action", fileContent, null, headers);
            return new ELHISZServerResolutionResponse(response);
        }

        public ELHISZServerRecipientResponse Recipients(string resolutionModuleID, string resolutionNumber, string registrationNumber,
            string recipientsXML)
        {
            Logger.Info("ElhitetServletManager.Recipients");
            Logger.Debug(String.Format("resolutionNumber={0}", resolutionNumber));
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("ResolutionModuleID", resolutionModuleID);
            headers.Add("ResolutionNumber", resolutionNumber);
            headers.Add("RegistrationNumber", registrationNumber);

            ServletResponse response = PostMethod("e-nhh/recipients.post.action", UTF8Encoding.UTF8.GetBytes(recipientsXML), null, headers);
            return new ELHISZServerRecipientResponse(response);
        }

        string EncodeHeaderValue(string value)
        {
            string encodedValue = String.Empty;

            if (!String.IsNullOrEmpty(value))
            {
                encodedValue = QuotedPrintableConverter.Encode(value);
            }

            return String.Format("=?utf-8?Q?{0}?=", encodedValue);
        }
    }
}