﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace NMHH.Elhitet.Servlet
{
    /// <summary>
    /// Summary description for QuotedPrintableConverter
    public class QuotedPrintableConverter
    {
        public static string Encode(string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            string hex = BitConverter.ToString(bytes);
            hex = hex.Replace('-', '=');
            return "=" + hex;
        }
    }
}