﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace NMHH.Elhitet.Servlet
{
    /// <summary>
    /// Summary description for ServletResponse
    /// </summary>
    public class ServletResponse
    {
        private byte[] _ResponseData;

        public byte[] ResponseData
        {
            get { return _ResponseData; }
            set { _ResponseData = value; }
        }

        private WebHeaderCollection _ResponseHeaders;

        public WebHeaderCollection ResponseHeaders
        {
            get { return _ResponseHeaders; }
            set { _ResponseHeaders = value; }
        }

        public ServletResponse(byte[] responseData, WebHeaderCollection responseHeaders)
        {
            this.ResponseData = responseData;
            this.ResponseHeaders = responseHeaders;

        }
    }
}