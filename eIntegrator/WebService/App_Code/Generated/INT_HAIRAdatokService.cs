using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.HAIR;
using Contentum.eIntegrator.WebService.HAIR.PartnerSync;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
///    A(z) INT_HAIRAdatok t�bl�hoz tartoz� Web szolg�ltat�sok.
/// </summary>

//[Transaction(Isolation = TransactionIsolationLevel.ReadCommitted)]
public partial class INT_HAIRAdatokService : System.Web.Services.WebService
{
    private INT_HAIRAdatokStoredProcedure sp = null;

    private DataContext dataContext;

    public INT_HAIRAdatokService()
    {
        dataContext = new DataContext(this.Application);

        //sp = new INT_HAIRAdatokStoredProcedure(this.Application);
        sp = new INT_HAIRAdatokStoredProcedure(dataContext);
    }

    public INT_HAIRAdatokService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new INT_HAIRAdatokStoredProcedure(dataContext);
    }
    /// <summary>
    /// Get(ExecParam ExecParam)  
    /// A INT_HAIRAdatok rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemeno param�ter tov�bbi adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(INT_HAIRAdatok))]
    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Get(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// GetAll(ExecParam ExecParam, INT_HAIRAdatokSearch _INT_HAIRAdatokSearch)
    /// A(z) INT_HAIRAdatok t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� szur�si felt�teleket param�ter tartalmazza (*Search). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemeno param�ter. Az ExecParam.Record_Id nem haszn�lt, tov�bbi adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).</param>
    /// <param name="_INT_HAIRAdatokSearch">Bemeno param�ter, a keres�si felt�teleket tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� szur�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(INT_HAIRAdatokSearch))]
    public Result GetAll(ExecParam ExecParam, INT_HAIRAdatokSearch _INT_HAIRAdatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _INT_HAIRAdatokSearch);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Insert(ExecParam ExecParam, INT_HAIRAdatok Record)
    /// Egy rekord felv�tele a INT_HAIRAdatok t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(INT_HAIRAdatok))]
    public Result Insert(ExecParam ExecParam, INT_HAIRAdatok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Contentum.eUtility.Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            LogDBTranzactionEvent(ExecParam, "INT_HAIRAdatok", "New");

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Update(ExecParam ExecParam, INT_HAIRAdatok Record)
    /// Az INT_HAIRAdatok t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemeno param�ter. Az ExecParam adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(INT_HAIRAdatok))]
    public Result Update(ExecParam ExecParam, INT_HAIRAdatok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Contentum.eUtility.Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            LogDBTranzactionEvent(ExecParam, "INT_HAIRAdatok", "Modify");

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az INT_HAIRAdatok t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemeno param�ter. Az ExecParam adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(INT_HAIRAdatok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            LogDBTranzactionEvent(ExecParam, "INT_HAIRAdatok", "Invalidate");

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// MultiInvalidate(ExecParam ExecParams)
    /// A INT_HAIRAdatok t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlendo t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemeno param�ter. Az ExecParam adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlendo t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(INT_HAIRAdatok))]
    public Result MultiInvalidate(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_invalidate = Invalidate(ExecParams[i]);
                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                {
                    throw new ResultException(result_invalidate);
                }
            }
            ////Commit:
            //ContextUtil.SetComplete();

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                string Ids = "";

                for (int i = 0; i < ExecParams.Length; i++)
                {
                    Ids += "'" + ExecParams[i].Record_Id + "',";
                }

                Ids = Ids.TrimEnd(',');

                Result eventLogResult = eventLogService.InsertTomeges(ExecParams[0], Ids, "INT_HAIRAdatok", "Invalidate");
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }
    /// <summary>
    /// Delete(ExecParam ExecParam)
    /// A(z) INT_HAIRAdatok t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak fizikai t�rl�se.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemeno param�ter. Az ExecParam adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>     
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak fizikai t�rl�se. Az ExecParam adatai a muvelet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(INT_HAIRAdatok))]
    public Result Delete(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Delete(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            LogDBTranzactionEvent(ExecParam, "INT_HAIRAdatok", "Delete");

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private void LogDBTranzactionEvent(ExecParam param, string tableName, string action)
    {
        KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

        KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(param, dataContext.Tranz_Id, param.Record_Id, tableName, action).Record;

        Result eventLogResult = eventLogService.Insert(param, eventLogRecord);
    }
}