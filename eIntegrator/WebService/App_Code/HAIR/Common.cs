﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR
{
    public static class Constants
    {
        public const string DEFAULT_NAMESPACE = "Contentum.eIntegrator.WebService.HAIR";
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class Iktatoszam
    {
        public string Elotag; // 10

        public int Foszam;
        [XmlIgnore] public bool FoszamSpecified;

        public int Ev;
        [XmlIgnore] public bool EvSpecified;

        public virtual bool IsValid()
        {
            return !String.IsNullOrEmpty(Elotag) && FoszamSpecified && Foszam > 0
                && EvSpecified && Ev >= 1990 && Ev <= 2100;
        }
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class IktatoszamAlszam : Iktatoszam
    {
        public int Alszam;
        [XmlIgnore] public bool AlszamSpecified;

        public override bool IsValid()
        {
            return base.IsValid() && AlszamSpecified && Alszam > 0;
        }
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class IktatoszamSorszam : IktatoszamAlszam
    {
        public int Sorszam;
        [XmlIgnore] public bool SorszamSpecified;
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class Alapadatok
    {
        public string KulsoAzonosito; // 100
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class Partner
    {
        public string PartnerAzonosito; // 100
        public string PartnerCimAzonosito; // 100
        public string PartnerNev; // 200
        public string PartnerCim; // 400
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class IratpeldanyAdatok
    {
        public string KuldesModja; // 10
        public string UgyintezesModja; // 10
        public Partner Ugyfel;
        public string Felelos; // 100
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class IrattariTetel
    {
        public string AgazatiJel; // 10
        public string Ugykor; // 10
        public string SelejtezesKod; // 10
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class HatosagiStatisztikaAdatok
    {
        [XmlElement(Order = 0)]
        public StatisztikaElem[] StatisztikaElem;
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class StatisztikaElem
    {
        public string StatisztikaElemKod; // 20
        public string StatisztikaElemErtek; // 20
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class Vonalkodok
    {
        public string UgyiratVonalkod; // 13
        public string IratpeldanyVonalkod; // 13
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class KuldemenyVonalkodok : Vonalkodok
    {
        public string KuldemenyVonalkod; // 100
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class HAIRAdatok
    {
        public bool KezbesitesErtesitesSzukseges;
        [XmlIgnore] public bool KezbesitesErtesitesSzuksegesSpecified;
        public string IratKulsoAzonosito; // 100
    }

    //[XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    //public class Valaszok
    //{
    //    [XmlElement(Order = 0)]
    //    public Valasz[] Valasz;
    //}

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class Valasz
    {
        public Valasz()
        {
            ValaszAdatok = new ValaszAdatok();
        }

        [XmlElement(Order = 0)]
        public string KulsoAzonosito;

        [XmlElement(Order = 1)]
        public ValaszAdatok ValaszAdatok;
    }

    [XmlType(Namespace = Constants.DEFAULT_NAMESPACE)]
    public class ValaszAdatok
    {
        public int HibaKod;
        public string HibaUzenet;
    }

    public class ValaszFactory
    {
        public static T GetSuccessValasz<T>(string adoazon) where T : Valasz, new()
        {
            return new T()
            {
                KulsoAzonosito = adoazon,
                ValaszAdatok = new ValaszAdatok()
                {
                    HibaKod = ErrorCodes.NoError,
                    HibaUzenet = string.Empty
                }
            };
        }
        public static T GetErrorValasz<T>(string adoazon, Result errorResult) where T : Valasz, new()
        {
            return new T()
            {
                KulsoAzonosito = adoazon,
                ValaszAdatok = new ValaszAdatok()
                {
                    HibaKod = Convert.ToInt32(errorResult.ErrorCode),
                    HibaUzenet = errorResult.ErrorMessage
                }
            };
        }

        public static T GetErrorValasz<T>(string adoazon, List<Result> errorResults) where T : Valasz, new()
        {
          
            string error = string.Empty;
            foreach (var errorResult in errorResults)
            {
                error += string.Format("HibaKód:{0} Hibaüzenet{1}\n", errorResult.ErrorCode, errorResult.ErrorMessage);
            }
            return new T()
            {
                KulsoAzonosito = adoazon,
                ValaszAdatok = new ValaszAdatok()
                {
                    HibaKod = ErrorCodes.InternalError,
                    HibaUzenet = error
                }
            };
        }
    }
}
