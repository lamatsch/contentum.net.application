﻿using System;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR.CsatolmanyFeltoltes
{
    public static class Constants
    {
        public const string NAMESPACE = "Contentum.eIntegrator.WebService.HAIR.CsatolmanyFeltoltes" ;
    }

    #region Parameters

    // [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]    
    //[XmlType(Namespace = "Contentum.eIntegrator.WebService.HAIR.CsatolmanyFeltoltes")]
    //public class Csatolmanyok
    //{
    //    [XmlElement(Order = 0)]
    //    public Csatolmany[] Csatolmany ;
    //}

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Csatolmany
    {
        [XmlElement(Order = 0)]
        public Alapadatok Alapadatok;
        [XmlElement(Order = 1)]
        public IratAdatok IratAdatok;
        [XmlElement(Order = 2)]
        public FileAdatok FileAdatok;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class IratAdatok
    {
        [XmlElement(Order = 0)]
        public IktatoszamAlszam Iktatoszam;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class FileAdatok
    {
        public string FajlNev; // 400
        public string Url; // 400

        public bool Alairt;
        [XmlIgnore] public bool AlairtSpecified;
    }

    #endregion

    #region Result

    [XmlType(Namespace = Constants.NAMESPACE)]
    //public class Valaszok : HAIR.Valaszok
    //{
    //}

    public class Valaszok
    {
        [XmlElement(Order = 0)]
        public Valasz[] Valasz;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valasz : HAIR.Valasz
    {
    }

    #endregion
}