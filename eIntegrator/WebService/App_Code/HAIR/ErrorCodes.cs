﻿namespace Contentum.eIntegrator.WebService.HAIR
{
    /// <summary>
    /// Error codes
    /// </summary>
    public static partial class ErrorCodes
    {
        public const int CannotConnectToHAIR = -5000;
        public const int NoError = 0;
        public const int InternalError = -1000;
        public const int InternalErrorAtItem = -900;
        public const int IktatasAlapadatokMissing = -3;
        public const int UgyiratNotFound = -4;
        public const int UgyiratTooMany = -5;
        public const int AlairoszabalyNotFound = -6;
        public const int ElintezesOrzo = -7;
        public const int DocumentAlreadyUploaded = 0;
        public const int IktatasIratadatokMissing = -8;
        public const int IktatasIratAdatokIktatoszamMissing = -9;
        public const int IktatasIratAdatokIktatoszamEvMissing = -10;
        public const int IktatasIratAdatokIktatoszamElotagMissing = -11;
        public const int IktatasIratAdatokIktatoszamFoszamMissing = -12;
        public const int IktatasIratAdatokIktatoszamAlszamMissing = -13;
        public const int IktatasIratAdatokHAIRAdatokMissing = -14;
        public const int IktatasElotagNotEqualsIktatoKonyv = -15;
        public const int IktatasIktatoKonyvNotFound = -16;
        public const int IratKulsoAzonositoFound = -17;
        public const int IratIktatoszamFound = -18;
        public const int IktatasAlapAdatokMissing = -19;
        public const int IktatasAlapAdatokKulsoAzonositoMissing = -20;
        public const int IktatasIratAdatokAlszamadatokMissing = -21;
        public const int IktatasAlszam = -22;
        public const int ErkeztatoKonyvMegKulJelNotEqualsErkeztetoKonyv = -23;

        public const int IktatasAtiktatasAdatokIktatoszamMissing = -24;
        public const int IktatasAtiktatasAdatokIktatoszamElotagMissing = -25;
        public const int IktatasAtiktatasAdatokIktatoszamFoszamMissing = -26;
        public const int IktatasAtiktatasAdatokIktatoszamAlszamMissing = -27;
        public const int IktatasAtiktatasAdatokIktatoszamEvMissing = -28;

        public const int IrattariTetelNotFound = -50;
        public const int UgyiratNemElintezheto = -51;
        public const int IratNotFound = -52;
        public const int KulsoAzonosito = -53;
        public const int ErkeztetesNincsVisszakuldesKod = -54;
        public const int ErkeztetesNincsKuldemenyAzonosito = -55;
        public const int ErkeztetesNincsKezbesitesVisszakuldesKod = -56;
        public const int ErkeztetesNincsKezbesitesVisszakuldesAlkod = -57;
        public const int ErkeztetesTertivevenyNemTalalhato = -58;
        public const int ErkeztetesTobbTertivevenyTalalhato = -59;
        public const int ErvenytelenIktatoszam = -60;
        public const int LetezoAzonosito = -61;

        public const int ERROR_PARAM_NULL_UJIRATPELDANY = -70;
        public const int ERROR_PARAM_NULL_UJIRATPELDANY_IKTATOSZAMALSZAM = -71;
        public const int ERROR_NOT_FOUND_IRATPELDANYADATOK_FELELOS = -72;

        public const int ERROR_PARAM_NULL_POSTAZAS = -73;
        public const int ERROR_NOT_FOUND_POSTAZAS_FELELOS = -74;
        public const int ERROR_PARAM_NOTFOUND_POSTAZAS_IRATPELDANY = -75;
        public const int ERROR_PARAM_NULL_POSTAZAS_POSTAKONYV = -76;

        public static string GetErrorMessage(int errorCode)
        {
            switch (errorCode)
            {
                case DocumentAlreadyUploaded: return Helper.Messages.DocumentAlreadyUploaded;
                case InternalErrorAtItem: return Helper.Messages.InternalErrorAtItem;
                case InternalError: return Helper.Messages.InternalError;
                case IktatasAlapadatokMissing: return Helper.Messages.IktatasAlapadatokMissingError;
                case UgyiratNotFound: return Helper.Messages.UgyiratNotFoundError;
                case UgyiratTooMany: return Helper.Messages.UgyiratTooManyError;
                case AlairoszabalyNotFound: return Helper.Messages.AlairoszabalyNotFoundError;
                case ElintezesOrzo: return Helper.Messages.ElintezesOrzoError;
                case IktatasIratadatokMissing: return Helper.Messages.IktatasIratadatokMissingError;
                case IktatasIratAdatokIktatoszamMissing: return Helper.Messages.IktatasIratAdatokIktatoszamMissingError;
                case IktatasIratAdatokIktatoszamEvMissing: return Helper.Messages.IktatasIratAdatokIktatoszamEvMissingError;
                case IktatasIratAdatokIktatoszamElotagMissing: return Helper.Messages.IktatasIratAdatokIktatoszamElotagMissingError;
                case IktatasIratAdatokIktatoszamFoszamMissing: return Helper.Messages.IktatasIratAdatokIktatoszamFoszamMissingError;
                case IktatasIratAdatokIktatoszamAlszamMissing: return Helper.Messages.IktatasIratAdatokIktatoszamAlszamMissingError;
                case IktatasIratAdatokHAIRAdatokMissing: return Helper.Messages.IktatasIratAdatokHAIRAdatokMissingError;
                case IktatasElotagNotEqualsIktatoKonyv: return Helper.Messages.IktatasElotagNotEqualsIktatoKonyvError;
                case IktatasIktatoKonyvNotFound: return Helper.Messages.IktatasIktatoKonyvNotFoundError;
                case IratKulsoAzonositoFound: return Helper.Messages.IratKulsoAzonositoFoundError;
                case IratIktatoszamFound: return Helper.Messages.IratIktatoszamFoundError;
                case IktatasAlapAdatokMissing: return Helper.Messages.IktatasAlapAdatokMissingError;
                case IktatasAlapAdatokKulsoAzonositoMissing: return Helper.Messages.IktatasAlapAdatokKulsoAzonositoMissingError;
                case IktatasIratAdatokAlszamadatokMissing: return Helper.Messages.IktatasIratAdatokAlszamadatokMissingError;
                case IktatasAlszam: return Helper.Messages.IktatasAlszamError;
                case ErkeztatoKonyvMegKulJelNotEqualsErkeztetoKonyv: return Helper.Messages.ErkeztatoKonyvMegKulJelNotEqualsErkeztetoKonyvError;

                case IrattariTetelNotFound: return Helper.Messages.IrattariTetelNotFoundError;
                case UgyiratNemElintezheto: return Helper.Messages.UgyiratNemElintezhetoError;
                case IratNotFound: return Helper.Messages.IratNotFoundError;
                case KulsoAzonosito: return Helper.Messages.KulsoAzonositoError;
                case ErkeztetesNincsVisszakuldesKod: return Helper.Messages.ErkeztetesNincsVisszakuldesKodError;
                case ErkeztetesNincsKuldemenyAzonosito: return Helper.Messages.ErkeztetesNincsKuldemenyAzonositoError;
                case ErkeztetesNincsKezbesitesVisszakuldesKod: return Helper.Messages.ErkeztetesNincsKezbesitesVisszakuldesKodError;
                case ErkeztetesNincsKezbesitesVisszakuldesAlkod: return Helper.Messages.ErkeztetesNincsKezbesitesVisszakuldesAlkodError;
                case ErkeztetesTertivevenyNemTalalhato: return Helper.Messages.ErkeztetesTertivevenyNemTalalhatoError;
                case ErkeztetesTobbTertivevenyTalalhato: return Helper.Messages.ErkeztetesTobbTertivevenyTalalhatoError;
                case ErvenytelenIktatoszam: return Helper.Messages.ErvenytelenIktatoszamError;

                case IktatasAtiktatasAdatokIktatoszamMissing: return Helper.Messages.IktatasAtiktatasAdatokIktatoszamMissingError;
                case IktatasAtiktatasAdatokIktatoszamElotagMissing: return Helper.Messages.IktatasAtiktatasAdatokIktatoszamElotagMissingError;
                case IktatasAtiktatasAdatokIktatoszamFoszamMissing: return Helper.Messages.IktatasAtiktatasAdatokIktatoszamFoszamMissingError;
                case IktatasAtiktatasAdatokIktatoszamAlszamMissing: return Helper.Messages.IktatasAtiktatasAdatokIktatoszamAlszamMissingError;
                case IktatasAtiktatasAdatokIktatoszamEvMissing: return Helper.Messages.IktatasAtiktatasAdatokIktatoszamEvMissingError;
                case LetezoAzonosito: return Helper.Messages.IktatasLetezoAzonositoError;

                case ERROR_PARAM_NULL_UJIRATPELDANY: return Helper.Messages.ERROR_PARAM_NULL_UJIRATPELDANY;
                case ERROR_PARAM_NULL_UJIRATPELDANY_IKTATOSZAMALSZAM: return Helper.Messages.ERROR_PARAM_NULL_UJIRATPELDANY_IKTATOSZAMALSZAM;
                case ERROR_NOT_FOUND_IRATPELDANYADATOK_FELELOS: return Helper.Messages.ERROR_NOT_FOUND_IRATPELDANYADATOK_FELELOS;
                case ERROR_PARAM_NULL_POSTAZAS: return Helper.Messages.ERROR_PARAM_NULL_POSTAZAS;
                case ERROR_NOT_FOUND_POSTAZAS_FELELOS: return Helper.Messages.ERROR_NOT_FOUND_POSTAZAS_FELELOS;
                case ERROR_PARAM_NOTFOUND_POSTAZAS_IRATPELDANY: return Helper.Messages.ERROR_PARAM_NOTFOUND_POSTAZAS_IRATPELDANY;
                case ERROR_PARAM_NULL_POSTAZAS_POSTAKONYV: return Helper.Messages.ERROR_PARAM_NULL_POSTAZAS_POSTAKONYV;

            }

            return errorCode.ToString();
        }
    }
}