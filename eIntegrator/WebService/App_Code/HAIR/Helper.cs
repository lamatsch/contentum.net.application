﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR
{
    public static class Helper
    {
        public static class Messages
        {
            public const string DocumentAlreadyUploaded = "A dokumentum korábban már feltöltésre került az irathoz";
            public const string InternalErrorAtItem = "Belső hiba történt a következő elem feldolgozása során: ";
            public const string InternalError = "Belső hiba történt a folyamat során.";
            public const string SoapRequestError = "Could not parse SOAP request";
            public const string UgyiratNotFoundError = "A megadott ügyirat nem létezik";
            public const string IratNotFoundError = "A megadott irat nem létezik";
            public const string IratKulsoAzonositoFoundError = "Az adott külsőazonosítóval már történt iktatás. ";
            public const string IratIktatoszamFoundError = "Az adott iktatószámmal már történt iktatás";
            public const string UgyiratTooManyError = "Nem határozható meg egyértelműen az ügyirat";
            public const string AlairoszabalyNotFoundError = "Nem található aláírószabály.";
            public const string ElintezesOrzoError = "Hiba az elintézés alatt. A dokumentumnak nem az örzője.";
            public const string IratTooManyError = "Nem határozható meg egyértelműen az irat.";
            public const string IktatasKimenoFelelosMissingError = "Kimenő alszámos iktatás a Felelős megadása kötelező";
            public const string IktatasKimenoKuldModjaMissingError = "Kimenő alszámos iktatás a Küldés módja mező értékének megadása kötelező";
            public const string IktatasKimenoUgyintezesModjaMissingError = "Kimenő alszámos iktatás az Ügyintézés módja mező értékének megadása kötelező";
            public const string IktatasAlapadatokMissingError = "Az Alapadatok megadása kötelező";
            public const string IktatasIratadatokMissingError = "Az Iratadatok megadása kötelező";
            public const string IktatasIratAdatokIktatoszamMissingError = "Az IratAdatok.Iktatoszam megadása kötelező";
            public const string IktatasIratAdatokIktatoszamEvMissingError = "Az IratAdatok.Iktatoszam.Ev megadása kötelező";
            public const string IktatasIratAdatokIktatoszamElotagMissingError = "Az IratAdatok.Iktatoszam.Elotag megadása kötelező";
            public const string IktatasIratAdatokIktatoszamFoszamMissingError = "Az IratAdatok.Iktatoszam.Foszam megadása kötelező";
            public const string IktatasIratAdatokIktatoszamAlszamMissingError = "Az IratAdatok.Iktatoszam.Alszam megadása kötelező";
            public const string IktatasIratAdatokHAIRAdatokMissingError = "Az IratAdatok.HAIRAdatok megadása kötelező";
            public const string IktatasAlapAdatokMissingError = "Az IratAdatok.Alapadatok megadása kötelező";
            public const string IktatasAlapAdatokKulsoAzonositoMissingError = "Az IratAdatok.Alapadatok.KulsoAzonosito megadása kötelező";
            public const string IktatasIratAdatokAlszamadatokMissingError = "Az IratAdatok.Alszamadatok megadása kötelező";
            public const string IktatasAlszamError = "1-nél kisebb alszámot nem lehet megadni";
            public const string IktatasAtiktatasAdatokIktatoszamMissingError = "Az Iktatas.AtiktatasAdatok.Iktatoszam megadása kötelező";
            public const string IktatasAtiktatasAdatokIktatoszamElotagMissingError = "Az Iktatas.AtiktatasAdatok.Iktatoszam.Elotag megadása kötelező";
            public const string IktatasAtiktatasAdatokIktatoszamFoszamMissingError = "Az Iktatas.AtiktatasAdatok.Iktatoszam.Foszam megadása kötelező";
            public const string IktatasAtiktatasAdatokIktatoszamAlszamMissingError = "Az Iktatas.AtiktatasAdatok.Iktatoszam.Alszam megadása kötelező";
            public const string IktatasAtiktatasAdatokIktatoszamEvMissingError = "Az Iktatas.AtiktatasAdatok.Iktatoszam.Ev megadása kötelező";
            public const string IktatasLetezoAzonositoError = "Már létezik folyamatban lévő iktatás vagy iktatott irat ezzel az azonosítóval.";

            public const string IktatasElotagNotEqualsIktatoKonyvError = "Az IratAdatok.Iktatoszam.Elotag étéke nem egyezik a technikai iktatókönyv értékével";
            public const string ErkeztatoKonyvMegKulJelNotEqualsErkeztetoKonyvError = "Az KuldemenyAdatok.ErkeztetoSzam.ErkeztetoKonyvKod étéke nem egyezik a technikai érkeztetőkönyv értékével";
            public const string IktatasIktatoKonyvNotFoundError = "Nem található az iktatókönyv";

            public const string IrattariTetelNotFoundError = "Nem található irattári tétel a megadott Ügykör és Selejtezéskód alapján";
            public const string UgyiratNemElintezhetoError = "Az ügyirat nem elintézhető";
            public const string KulsoAzonositoError = "A megadott iktatószámhoz nem a megadott külső azonosító tartozik a rendszerben.";
            public const string ErkeztetesNincsVisszakuldesKodError = "A megadott KezbesitesVisszakuldesKod-hoz nem tartozik tértivevény visszaküldés kód.";
            public const string ErkeztetesNincsKuldemenyAzonositoError = "A KuldemenyAzonosito paraméter nem lehet üres!";
            public const string ErkeztetesNincsKezbesitesVisszakuldesKodError = "A KezbesitesVisszakuldesKod paraméter nem lehet üres!";
            public const string ErkeztetesNincsKezbesitesVisszakuldesAlkodError = "A KezbesitesVisszakuldesAlkod paraméter nem lehet üres!";
            public const string ErkeztetesTertivevenyNemTalalhatoError = "A megadott tértivevény nem található.";
            public const string ErkeztetesTobbTertivevenyTalalhatoError = "Több tértivevény található ilyen azonosítóval.";
            public const string ErvenytelenIktatoszamError = "Az iktatószám nincs megadva vagy érvénytelen.";

            public const string ERROR_PARAM_NULL_UJIRATPELDANY = "Az UjIratpeldanyok paraméter nem lehet üres !";
            public const string ERROR_PARAM_NULL_UJIRATPELDANY_IKTATOSZAMALSZAM = "Az UjIratpeldany.IktatoszamAlszam paraméter nem lehet üres !";
            public const string ERROR_NOT_FOUND_IRATPELDANYADATOK_FELELOS = "IratPeldanyLetrehozas.IratpeldanyAdatok.Felelos nem található !";
            public const string ERROR_PARAM_NULL_POSTAZAS = "Az Postazas paraméter nem lehet üres !";
            public const string ERROR_NOT_FOUND_POSTAZAS_FELELOS = "Postazas.Felado nem található !";

            public const string ERROR_PARAM_NOTFOUND_POSTAZAS_IRATPELDANY = "Az Iratpéldány nem található Iktatoszam paraméter alapján !";
            public const string ERROR_PARAM_NULL_POSTAZAS_POSTAKONYV = "Az postakönyv nem található !";
        }

        public static class Parameters
        {
            public const string AdminUser = "54e861a5-36ed-44ca-baa7-c287d125b309";

            public const string TECHNIKAI_IKTATOKONYV_IKTATOHELY_NAME = "TECHNIKAI_IKTATOKONYV_IKTATOHELY";
            public const string TECHNIKAI_ERKEZTETOKONYV_MEGKULJEL_NAME = "TECHNIKAI_ERKEZTETOKONYV_MEGKULJEL";
            public const string TECHNIKAI_USER_NAME = "TECHNIKAI_USER";
            public const string TECHNIKAI_USER_SZERVEZET_NAME = "TECHNIKAI_USER_SZERVEZET";
            public const string HIBA_EMAIL_NAME = "HIBA_EMAIL";

            public static string FelhasznaloId = "54e861a5-36ed-44ca-baa7-c287d125b309";
            public static string CSATOLMANY_MODUL_ID = "38E22792-2CAD-486F-8B0F-44C75ADF0AC4";
        }

        #region Fields

        public static bool EmailKuldesBekapcsolva = false;

        public static string TECHNIKAI_IKTATOKONYV_IKTATOHELY;
        public static string TECHNIKAI_ERKEZTETOKONYV_MEGKULJEL;
        public static string TECHNIKAI_USER;
        public static string TECHNIKAI_USER_SZERVEZET;
        public static string HIBA_EMAIL;

        public const string EmailSubject = "HAIR integráció";

        public static IktatasiParameterek defaultIktatasiParameter;

        static Dictionary<string, string> objTipusKodok = new Dictionary<string, string>();
        static Dictionary<string, DataRow> targyszavak = new Dictionary<string, DataRow>();
        private static Dictionary<string, Result> felhasznalok = new Dictionary<string, Result>();
        private static Dictionary<string, Result> csoportok = new Dictionary<string, Result>();
        private static Dictionary<string, KRT_Cimek> cimek = new Dictionary<string, KRT_Cimek>();
        private static Dictionary<string, KRT_Partnerek> partnerek = new Dictionary<string, KRT_Partnerek>();
        private static object objTipusKodokLock = new object();
        private static object targyszavakLock = new object();
        private static object felhasznalokLock = new object();
        private static object csoportokLock = new object();
        private static object cimekLock = new object();
        private static object partnerekLock = new object();

        public static void AddObjTipusKodokToCache(string id, string szo)
        {
            lock (objTipusKodokLock)
            {
                if (!objTipusKodok.ContainsKey(id))
                {
                    objTipusKodok.Add(id, szo);
                }
            }
        }

        public static void AddTargyszoToCache(string id, DataRow szo)
        {
            lock (targyszavakLock)
            {
                if (!targyszavak.ContainsKey(id))
                {
                    targyszavak.Add(id, szo);
                }
            }
        }

        public static void AddFelhasznalokToCache(string userNev, Result felhasznalo)
        {
            lock (felhasznalokLock)
            {
                if (!felhasznalok.ContainsKey(userNev))
                {
                    felhasznalok.Add(userNev, felhasznalo);
                }
            }
        }

        public static void AddCsoportokToCache(string userNev, Result csoport)
        {
            lock (csoportokLock)
            {
                if (!csoportok.ContainsKey(userNev))
                {
                    csoportok.Add(userNev, csoport);
                }
            }
        }

        public static void AddPartnerToCache(KRT_Partnerek partner)
        {
            lock (partnerekLock)
            {
                if (!partnerek.Any(x => x.Key == partner.Id))
                {
                    partnerek.Add(partner.Id, partner);
                }
            }
        }

        public static void AddCimekToCache(KRT_Cimek cim)
        {
            lock (cimekLock)
            {
                if (!cimek.Any(x => x.Key == cim.Id))
                {
                    cimek.Add(cim.Id, cim);
                }
            }
        }


        #endregion

        #region Properties

        private static Contentum.eIntegrator.Service.INT_ParameterekService _paramerekService;
        private static EmailService _emailService;

        private static Contentum.eIntegrator.Service.INT_ParameterekService INTParameterekService
        {
            get
            {
                if (_paramerekService == null)
                {
                    _paramerekService = eIntegratorService.ServiceFactory.GetINT_ParameterekService();
                }
                return _paramerekService;
            }
        }
        private static EmailService Email
        {
            get
            {
                if (_emailService == null)
                {
                    _emailService = eAdminService.ServiceFactory.GetEmailService();
                }
                return _emailService;
            }
        }


        private static ExecParam _execParam;

        public static ExecParam GlobalExecParam
        {
            get
            {
                if (_execParam == null)
                {
                    _execParam = new ExecParam();
                }
                return _execParam;
            }
        }

        #endregion

        static Helper()
        {

            #region Parameters Init
            try
            {
                TECHNIKAI_IKTATOKONYV_IKTATOHELY = GetParameter(Parameters.TECHNIKAI_IKTATOKONYV_IKTATOHELY_NAME, Parameters.AdminUser);
                TECHNIKAI_ERKEZTETOKONYV_MEGKULJEL = GetParameter(Parameters.TECHNIKAI_ERKEZTETOKONYV_MEGKULJEL_NAME, Parameters.AdminUser);
                TECHNIKAI_USER = GetParameter(Parameters.TECHNIKAI_USER_NAME, Parameters.AdminUser);
                TECHNIKAI_USER_SZERVEZET = GetParameter(Parameters.TECHNIKAI_USER_SZERVEZET_NAME, Parameters.AdminUser);
                HIBA_EMAIL = GetParameter(Parameters.HIBA_EMAIL_NAME, Parameters.AdminUser);

                string csoporttagid = GetCsoportTagId(TECHNIKAI_USER_SZERVEZET, TECHNIKAI_USER);

                if (string.IsNullOrEmpty(csoporttagid))
                    throw new Exception("Nem található csoportagság a TECHNIKAI_USER és a TECHNIKAI_USER_SZERVEZET között.");

                GlobalExecParam.CsoportTag_Id = csoporttagid;
                GlobalExecParam.Felhasznalo_Id = TECHNIKAI_USER;
                GlobalExecParam.LoginUser_Id = TECHNIKAI_USER;
                GlobalExecParam.FelhasznaloSzervezet_Id = TECHNIKAI_USER_SZERVEZET;
            }
            catch (Exception ex)
            {
                Logger.Error("Fatal exception in HAIR.", ex);
            }

            #endregion
        }

        public static string GetCsoportTagId(string csoport_Id, string csoport_Id_Jogalany)
        {
            var csoportTagokService = eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

            var searchObjCsoportTagok = new eQuery.BusinessDocuments.KRT_CsoportTagokSearch();
            searchObjCsoportTagok.Csoport_Id_Jogalany.Value = csoport_Id_Jogalany;
            searchObjCsoportTagok.Csoport_Id_Jogalany.Operator = Contentum.eQuery.Query.Operators.equals;
            searchObjCsoportTagok.Csoport_Id.Value = csoport_Id;
            searchObjCsoportTagok.Csoport_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            searchObjCsoportTagok.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            searchObjCsoportTagok.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            searchObjCsoportTagok.ErvKezd.Group = "100";
            searchObjCsoportTagok.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            searchObjCsoportTagok.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            searchObjCsoportTagok.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            searchObjCsoportTagok.ErvVege.Group = "100";
            searchObjCsoportTagok.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            ExecParam searchExecParam = GetExecParam(Parameters.AdminUser);

            Result resultCsoportTagok = csoportTagokService.GetAll(searchExecParam, searchObjCsoportTagok);
            if (resultCsoportTagok.IsError)
            {
                throw new ResultException(resultCsoportTagok);
            }

            if (resultCsoportTagok.GetCount < 1)
            {
                return null;
            }

            return resultCsoportTagok.Ds.Tables[0].Rows[0]["Id"].ToString();

        }
        public static Result GetUgyiratByIktatoKonyvFromRequestForFiling(Iktatoszam iktatoszam, ValaszAdatok valaszAdatok, string iktatoKonyvId)
        {
            EREC_UgyUgyiratokService ugyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ugyiratokExecParam = GlobalExecParam.Clone();
            EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
            ugyiratokSearch.IraIktatokonyv_Id.Filter(iktatoKonyvId);
            ugyiratokSearch.Foszam.Filter(iktatoszam.Foszam.ToString());

            Result ugyiratokResult = ugyiratokService.GetAll(ugyiratokExecParam, ugyiratokSearch);

            if (ugyiratokResult.IsError)
            {
                return SetError(valaszAdatok, ugyiratokResult);
            }

            return ugyiratokResult;
        }

        public static Result SearchKuldemenyForFiling(Iktatas.Iktatas iktatas, Iktatas.Valasz valasz)
        {
            string vonalkod = "";
            int? sorszam = null;
            if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.Vonalkod))
            {
                vonalkod = iktatas.KuldemenyAdatok.Vonalkod;
            }

            if (iktatas.KuldemenyAdatok.ErkeztetoSzam.SorszamSpecified)
            {
                sorszam = iktatas.KuldemenyAdatok.ErkeztetoSzam.Sorszam;
            }

            Result kuldemenyResult = SearchKuldemenyForFiling(vonalkod, sorszam);

            if (kuldemenyResult.IsError)
            {
                return valasz.SetError(kuldemenyResult);
            }

            return kuldemenyResult;
        }

        public static Result SearchKuldemenyForFiling(string vonalkod, int? sorszam)
        {
            EREC_KuldKuldemenyekService kuldemenyService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam kuldemenyExecParam = GlobalExecParam.Clone();
            EREC_KuldKuldemenyekSearch kuldemenySearch = new EREC_KuldKuldemenyekSearch();

            if (!string.IsNullOrEmpty(vonalkod))
            {
                kuldemenySearch.BarCode.Value = vonalkod;
                kuldemenySearch.BarCode.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            if (sorszam.HasValue)
            {
                kuldemenySearch.Erkezteto_Szam.Value = sorszam.Value.ToString();
                kuldemenySearch.Erkezteto_Szam.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            Result kuldemenyResult = kuldemenyService.GetAll(kuldemenyExecParam, kuldemenySearch);

            return kuldemenyResult;
        }

        public static Result SearchKuldemenyForFilingByIktatoKonyvId(string iktatokonyvid, int? sorszam)
        {
            EREC_KuldKuldemenyekService kuldemenyService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam kuldemenyExecParam = GlobalExecParam.Clone();
            EREC_KuldKuldemenyekSearch kuldemenySearch = new EREC_KuldKuldemenyekSearch();

            if (!string.IsNullOrEmpty(iktatokonyvid))
            {
                kuldemenySearch.IraIktatokonyv_Id.Value = iktatokonyvid;
                kuldemenySearch.IraIktatokonyv_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            if (sorszam.HasValue)
            {
                kuldemenySearch.Erkezteto_Szam.Value = sorszam.Value.ToString();
                kuldemenySearch.Erkezteto_Szam.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            Result kuldemenyResult = kuldemenyService.GetAll(kuldemenyExecParam, kuldemenySearch);

            return kuldemenyResult;
        }


        public static Result GetIktatokonyvFromRequestForFiling(Iktatoszam iktatoszam, ValaszAdatok valaszAdatok)
        {
            EREC_IraIktatoKonyvekService iktatokonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam iktatoKonyvekExecParam = GlobalExecParam.Clone();
            EREC_IraIktatoKonyvekSearch iktatokonyvekSearch = new EREC_IraIktatoKonyvekSearch();
            string elotag = null;
            int ev = 0;

            if (iktatoszam.EvSpecified && iktatoszam.Ev != 0)
            {
                ev = iktatoszam.Ev;
            }

            iktatokonyvekSearch.IktatoErkezteto.Filter(Contentum.eUtility.Constants.IktatoErkezteto.Iktato);

            if (!String.IsNullOrEmpty(iktatoszam.Elotag))
            {
                elotag = iktatoszam.Elotag;
            }

            Result iktatoKonyvekResult = SearchIktatokonyv(Contentum.eUtility.Constants.IktatoErkezteto.Iktato, elotag, ev);

            if (iktatoKonyvekResult.IsError)
            {
                return SetError(valaszAdatok, iktatoKonyvekResult);
            }

            return iktatoKonyvekResult;
        }

        public static Result SearchIktatokonyv(string iktatoErkezteto, string elotag, int ev)
        {
            EREC_IraIktatoKonyvekService iktatokonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam iktatoKonyvekExecParam = GlobalExecParam.Clone();
            EREC_IraIktatoKonyvekSearch iktatokonyvekSearch = new EREC_IraIktatoKonyvekSearch();

            if (ev != 0)
            {
                iktatokonyvekSearch.Ev.Value = ev.ToString();
                iktatokonyvekSearch.Ev.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            iktatokonyvekSearch.IktatoErkezteto.Value = iktatoErkezteto;
            iktatokonyvekSearch.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;

            if (!String.IsNullOrEmpty(elotag))
            {
                iktatokonyvekSearch.Iktatohely.Value = elotag;
                iktatokonyvekSearch.Iktatohely.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            Result iktatoKonyvekResult = iktatokonyvekService.GetAllWithIktathat(iktatoKonyvekExecParam, iktatokonyvekSearch);

            return iktatoKonyvekResult;
        }

        public static EREC_IraIktatoKonyvek SearchIktatokonyvForKuldemeny(string iktatoErkezteto, string megkuljelzes, int ev, Dictionary<string, EREC_IraIktatoKonyvek> iktatoKonyvek, Iktatas.Valasz valasz)
        {
            if (iktatoKonyvek.Values.Any(x => x.IktatoErkezteto == iktatoErkezteto && x.Ev == ev.ToString() && x.MegkulJelzes == megkuljelzes))
            {
                return iktatoKonyvek.Values.First(x => x.IktatoErkezteto == iktatoErkezteto && x.Ev == ev.ToString() && x.MegkulJelzes == megkuljelzes);
            }

            EREC_IraIktatoKonyvekService iktatokonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam iktatoKonyvekExecParam = GlobalExecParam.Clone();
            EREC_IraIktatoKonyvekSearch iktatokonyvekSearch = new EREC_IraIktatoKonyvekSearch();

            if (ev != 0)
            {
                iktatokonyvekSearch.Ev.Value = ev.ToString();
                iktatokonyvekSearch.Ev.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            iktatokonyvekSearch.IktatoErkezteto.Value = iktatoErkezteto;
            iktatokonyvekSearch.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;

            if (!String.IsNullOrEmpty(megkuljelzes))
            {
                iktatokonyvekSearch.MegkulJelzes.Value = megkuljelzes;
                iktatokonyvekSearch.MegkulJelzes.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            Result iktatoKonyvekResult = iktatokonyvekService.GetAllWithIktathat(iktatoKonyvekExecParam, iktatokonyvekSearch);
            if (iktatoKonyvekResult.IsError)
            {
                valasz.SetError(iktatoKonyvekResult);
                return null;
            }
            EREC_IraIktatoKonyvek konyv = new EREC_IraIktatoKonyvek();
            Utility.LoadBusinessDocumentFromDataRow(konyv, iktatoKonyvekResult.Ds.Tables[0].Rows[0]);
            if (konyv != null)
            {
                if (!iktatoKonyvek.ContainsKey(konyv.Id))
                {
                    iktatoKonyvek.Add(konyv.Id, konyv);
                }
            }
            return konyv;
        }

        public static bool CheckExistingTetelAzonosito(string azonosito, Iktatas.Valasz valasz)
        {
            EREC_TomegesIktatasTetelekService service = eRecordService.ServiceFactory.GetEREC_TomegesIktatasTetelekService();
            var execParam = GlobalExecParam.Clone();
            EREC_TomegesIktatasTetelekSearch search = new EREC_TomegesIktatasTetelekSearch();
            search.Azonosito.Value = azonosito;
            search.Azonosito.Operator = Contentum.eQuery.Query.Operators.equals;
            search.Allapot.Value = "9";
            search.Allapot.Operator = Contentum.eQuery.Query.Operators.notequals;

            Result res = service.GetAll(execParam, search);
            if (res.IsError)
            {
                valasz.SetError(res);
                return true;
            }
            if (res.Ds.Tables[0].Rows.Count > 0)
                return true;

            return false;
        }

        public static EREC_IraIratok GetIratFromRequestForFiling(Iktatas.Iktatas iktatas, Iktatas.Valasz valasz)
        {
            EREC_IraIratok irat = new EREC_IraIratok();
            irat.Updated.SetValueAll(false);
            irat.Base.Updated.SetValueAll(false);

            irat.Munkaallomas = "HAIR";
            irat.Updated.Munkaallomas = true;

            if (!string.IsNullOrEmpty(iktatas.IratAdatok.HAIRAdatok.IratKulsoAzonosito))
            {
                irat.HivatkozasiSzam = iktatas.IratAdatok.HAIRAdatok.IratKulsoAzonosito;
                irat.Updated.HivatkozasiSzam = true;
            }

            if (!string.IsNullOrEmpty(iktatas.IratAdatok.AlszamAdatok.IratIranya))
            {
                irat.PostazasIranya = iktatas.IratAdatok.AlszamAdatok.IratIranya;
                irat.Updated.PostazasIranya = true;
            }

            if (!string.IsNullOrEmpty(iktatas.IratAdatok.AlszamAdatok.Irattipus))
            {
                irat.Irattipus = iktatas.IratAdatok.AlszamAdatok.Irattipus;
                irat.Updated.Irattipus = true;
            }

            if (!string.IsNullOrEmpty(iktatas.IratAdatok.AlszamAdatok.Targy))
            {
                irat.Targy = iktatas.IratAdatok.AlszamAdatok.Targy;
                irat.Updated.Targy = true;
            }

            //irat.Jelleg = "1";
            //irat.Updated.Jelleg = true;               

            if (iktatas.IratAdatok.KiadmanyozasAdatok != null)
            {
                if (!string.IsNullOrEmpty(iktatas.IratAdatok.KiadmanyozasAdatok.Alairo) && iktatas.IratAdatok.KiadmanyozasAdatok.AlairasDatumSpecified)
                {
                    irat.KiadmanyozniKell = "1";
                    irat.Updated.KiadmanyozniKell = true;
                }
            }

            if (!string.IsNullOrEmpty(iktatas.IratAdatok.AlszamAdatok.Ugyintezo))
            {
                Result felhasznaloResult = GetFelhasznaloByUserNev(iktatas.IratAdatok.AlszamAdatok.Ugyintezo, valasz);

                if (felhasznaloResult.IsError)
                {
                    valasz.SetError(felhasznaloResult);
                    return null;
                }
                else if (felhasznaloResult.Ds.Tables[0].Rows.Count > 0)
                {
                    irat.FelhasznaloCsoport_Id_Ugyintez = felhasznaloResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                    irat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                }
                else
                {
                    Result csoportResult = GetCsoportByUserNev(iktatas.IratAdatok.AlszamAdatok.Ugyintezo, valasz);
                    if (csoportResult.IsError)
                    {
                        valasz.SetError(csoportResult);
                        return null;
                    }
                    else if (csoportResult.Ds.Tables[0].Rows.Count < 1)
                    {
                        valasz.SetError("IratAdatok.AlszamAdatok.Ugyintezo nem található", -2);
                        return null;
                    }
                    else
                    {
                        irat.FelhasznaloCsoport_Id_Ugyintez = csoportResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                        irat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                    }
                }
            }

            if (iktatas.Alapadatok.TechnikaiIktatasSpecified && iktatas.Alapadatok.TechnikaiIktatas)
            {
                irat.Alszam = iktatas.IratAdatok.Iktatoszam.Alszam.ToString();
                irat.Updated.Alszam = true;
                irat.Typed.Alszam = iktatas.IratAdatok.Iktatoszam.Alszam;
            }

            return irat;
        }

        public static Result GetIrattariTetelResult(IrattariTetel irattariTetel)
        {
            EREC_IraIrattariTetelekService irattariTetelekService = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            ExecParam irattariTetelekExecParam = GlobalExecParam.Clone();
            EREC_IraIrattariTetelekSearch irattariTetelekSearch = new EREC_IraIrattariTetelekSearch();

            irattariTetelekSearch.IrattariTetelszam.Filter(irattariTetel.Ugykor);
            irattariTetelekSearch.IrattariJel.Filter(irattariTetel.SelejtezesKod);

            return irattariTetelekService.GetAllWithIktathat(irattariTetelekExecParam, irattariTetelekSearch);
        }


        public static EREC_UgyUgyiratok GetUgyiratFromRequestForFiling(Iktatas.Iktatas iktatas, Iktatas.Valasz valasz)
        {
            EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
            ugyirat.Updated.SetValueAll(false);
            ugyirat.Base.Updated.SetValueAll(false);

            if (iktatas.IratAdatok.FoszamAdatok.HataridoSpecified)
            {
                ugyirat.Hatarido = iktatas.IratAdatok.FoszamAdatok.Hatarido.ToString();
                ugyirat.Updated.Hatarido = true;
            }

            if (!string.IsNullOrEmpty(iktatas.IratAdatok.FoszamAdatok.Targy))
            {
                ugyirat.Targy = iktatas.IratAdatok.FoszamAdatok.Targy;
                ugyirat.Updated.Targy = true;
            }

            if (iktatas.IratAdatok.FoszamAdatok.Ugyfel != null)
            {
                if (!string.IsNullOrEmpty(iktatas.IratAdatok.FoszamAdatok.Ugyfel.PartnerAzonosito))
                {
                    KRT_Partnerek partner = GetPartnerByKulsoAzonosito(iktatas.IratAdatok.FoszamAdatok.Ugyfel.PartnerAzonosito, valasz.ValaszAdatok);
                    if (!valasz.IsError() && partner != null)
                    {
                        ugyirat.Partner_Id_Ugyindito = partner.Id;
                        ugyirat.Updated.Partner_Id_Ugyindito = true;
                        ugyirat.NevSTR_Ugyindito = partner.Nev;
                        ugyirat.Updated.NevSTR_Ugyindito = true;
                    }
                    else
                    {
                        ugyirat.NevSTR_Ugyindito = iktatas.IratAdatok.FoszamAdatok.Ugyfel.PartnerAzonosito;
                        ugyirat.Updated.NevSTR_Ugyindito = true;
                    }
                }

                if (!string.IsNullOrEmpty(iktatas.IratAdatok.FoszamAdatok.Ugyfel.PartnerCimAzonosito))
                {
                    KRT_Cimek cim = GetCimByKulsoAzonosito(iktatas.IratAdatok.FoszamAdatok.Ugyfel.PartnerCimAzonosito, valasz.ValaszAdatok);

                    if (!valasz.IsError() && cim != null)
                    {
                        ugyirat.Cim_Id_Ugyindito = cim.Id;
                        ugyirat.Updated.Cim_Id_Ugyindito = true;
                        ugyirat.CimSTR_Ugyindito = cim.Nev;
                        ugyirat.Updated.CimSTR_Ugyindito = true;
                    }
                    else
                    {
                        ugyirat.CimSTR_Ugyindito = iktatas.IratAdatok.FoszamAdatok.Ugyfel.PartnerCimAzonosito;
                        ugyirat.Updated.CimSTR_Ugyindito = true;
                    }
                }
                if (!string.IsNullOrEmpty(iktatas.IratAdatok.FoszamAdatok.Ugyfel.PartnerNev))
                {
                    ugyirat.NevSTR_Ugyindito = iktatas.IratAdatok.FoszamAdatok.Ugyfel.PartnerNev;
                    ugyirat.Updated.NevSTR_Ugyindito = true;
                }

                if (!string.IsNullOrEmpty(iktatas.IratAdatok.FoszamAdatok.Ugyfel.PartnerCim))
                {
                    ugyirat.CimSTR_Ugyindito = iktatas.IratAdatok.FoszamAdatok.Ugyfel.PartnerCim;
                    ugyirat.Updated.CimSTR_Ugyindito = true;
                }
            }

            if (!string.IsNullOrEmpty(iktatas.IratAdatok.FoszamAdatok.Ugytipus))
            {
                ugyirat.UgyTipus = iktatas.IratAdatok.FoszamAdatok.Ugytipus;
                ugyirat.Updated.UgyTipus = true;
            }

            if (!string.IsNullOrEmpty(iktatas.IratAdatok.FoszamAdatok.Felelos))
            {
                Result felhasznaloResult = GetFelhasznaloByUserNev(iktatas.IratAdatok.FoszamAdatok.Felelos, valasz);

                if (felhasznaloResult.IsError)
                {
                    valasz.SetError(felhasznaloResult);
                    return null;
                }
                else if (felhasznaloResult.Ds.Tables[0].Rows.Count > 0)
                {
                    ugyirat.Csoport_Id_Felelos = felhasznaloResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                    ugyirat.Updated.Csoport_Id_Felelos = true;
                }
                else
                {
                    Result csoportResult = GetCsoportByUserNev(iktatas.IratAdatok.FoszamAdatok.Felelos, valasz);
                    if (csoportResult.IsError)
                    {
                        valasz.SetError(csoportResult);
                        return null;
                    }
                    else if (csoportResult.Ds.Tables[0].Rows.Count < 1)
                    {
                        valasz.SetError("IratAdatok.FoszamAdatok.Felelos nem található", -2);
                        return null;
                    }
                    else
                    {
                        ugyirat.Csoport_Id_Felelos = csoportResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                        ugyirat.Updated.Csoport_Id_Felelos = true;
                    }
                }
            }

            if (!string.IsNullOrEmpty(iktatas.IratAdatok.FoszamAdatok.Ugyintezo))
            {
                Result felhasznaloResult = GetFelhasznaloByUserNev(iktatas.IratAdatok.FoszamAdatok.Ugyintezo, valasz);

                if (felhasznaloResult.IsError)
                {
                    valasz.SetError(felhasznaloResult);
                    return null;
                }
                else if (felhasznaloResult.Ds.Tables[0].Rows.Count > 0)
                {
                    ugyirat.FelhasznaloCsoport_Id_Ugyintez = felhasznaloResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                    ugyirat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                }
                else
                {
                    Result csoportResult = GetCsoportByUserNev(iktatas.IratAdatok.FoszamAdatok.Ugyintezo, valasz);
                    if (csoportResult.IsError)
                    {
                        valasz.SetError(csoportResult);
                        return null;
                    }
                    else if (csoportResult.Ds.Tables[0].Rows.Count < 1)
                    {
                        valasz.SetError("IratAdatok.FoszamAdatok.Ugyintezo nem található", -2);
                        return null;
                    }
                    else
                    {
                        ugyirat.FelhasznaloCsoport_Id_Ugyintez = csoportResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                        ugyirat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                    }
                }
            }

            if (iktatas.IratAdatok.FoszamAdatok != null && iktatas.IratAdatok.FoszamAdatok.IrattariTetel != null)
            {
                Result irattariTetelResult = GetIrattariTetelResult(iktatas.IratAdatok.FoszamAdatok.IrattariTetel);

                if (irattariTetelResult.IsError)
                {
                    valasz.SetError(irattariTetelResult);
                    return null;
                }

                if (irattariTetelResult.Ds.Tables.Count > 0 && irattariTetelResult.Ds.Tables[0].Rows.Count > 0)
                {
                    ugyirat.IraIrattariTetel_Id = irattariTetelResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                    ugyirat.Updated.IraIrattariTetel_Id = true;
                }
                else
                {
                    valasz.SetError(ErrorCodes.IrattariTetelNotFound);
                    return null;
                }
            }

            if (iktatas.Alapadatok.TechnikaiIktatasSpecified && iktatas.Alapadatok.TechnikaiIktatas)
            {
                ugyirat.Foszam = iktatas.IratAdatok.Iktatoszam.Foszam.ToString();
                ugyirat.Updated.Foszam = true;
                ugyirat.Typed.Foszam = iktatas.IratAdatok.Iktatoszam.Foszam;
            }

            return ugyirat;
        }

        public static IktatasiParameterek GetIktatasiParameterkFromRequestForFiling()
        {
            if (defaultIktatasiParameter != null)
            {
                return defaultIktatasiParameter;
            }

            defaultIktatasiParameter = new IktatasiParameterek();

            defaultIktatasiParameter.IratpeldanyVonalkodGeneralasHaNincs = true;
            defaultIktatasiParameter.UgyiratUjranyitasaHaLezart = true;
            defaultIktatasiParameter.UgyiratPeldanySzukseges = false;
            defaultIktatasiParameter.KeszitoPeldanyaSzukseges = false;
            defaultIktatasiParameter.Atiktatas = false;
            defaultIktatasiParameter.EmptyUgyiratSztorno = false;
            defaultIktatasiParameter.MunkaPeldany = false;

            return defaultIktatasiParameter;
        }

        public static EREC_KuldKuldemenyek GetKuldemenyForFiling(Iktatas.Iktatas iktatas, Iktatas.Valasz valasz)
        {
            EREC_KuldKuldemenyek kuld = new EREC_KuldKuldemenyek();
            kuld.Updated.SetValueAll(false);
            kuld.Base.Updated.SetValueAll(false);
            kuld.PeldanySzam = "1";
            kuld.Updated.PeldanySzam = true;
            kuld.Munkaallomas = "HAIR";
            kuld.Updated.Munkaallomas = true;
            if (iktatas.KuldemenyAdatok != null)
            {
                if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.Vonalkod))
                {
                    kuld.BarCode = iktatas.KuldemenyAdatok.Vonalkod;
                    kuld.Updated.BarCode = true;
                }

                if (iktatas.KuldemenyAdatok.ErkeztetoSzam != null)
                {

                    if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.ErkeztetoSzam.ErkeztetoKonyvKod))
                    {
                        int ev = DateTime.Now.Year;
                        if (iktatas.KuldemenyAdatok.ErkeztetoSzam.EvSpecified)
                            ev = iktatas.KuldemenyAdatok.ErkeztetoSzam.Ev;

                        Result erkeztetoKonyv = GetErkeztetoByKod(iktatas.KuldemenyAdatok.ErkeztetoSzam.ErkeztetoKonyvKod, ev, valasz);
                        if (erkeztetoKonyv.IsError)
                        {
                            valasz.SetError(erkeztetoKonyv);
                            return null;
                        }
                        else if (erkeztetoKonyv.Ds.Tables[0].Rows.Count < 1)
                        {
                            valasz.SetError("Nem található érkeztetókönyv a KuldemenyAdatok.ErkeztetoSzam.ErkeztetoKonyvKod értékei alapján", -100);
                            return null;
                        }
                        else
                        {
                            kuld.IraIktatokonyv_Id = erkeztetoKonyv.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                            kuld.Updated.IraIktatokonyv_Id = true;
                        }
                    }


                    if (iktatas.KuldemenyAdatok.ErkeztetoSzam.EvSpecified)
                    {
                        kuld.Erkeztetes_Ev = iktatas.KuldemenyAdatok.ErkeztetoSzam.Ev.ToString();
                        kuld.Updated.Erkeztetes_Ev = true;
                    }

                    if (iktatas.KuldemenyAdatok.ErkeztetoSzam.SorszamSpecified && iktatas.Alapadatok.TechnikaiIktatasSpecified && iktatas.Alapadatok.TechnikaiIktatas)
                    {
                        kuld.Erkezteto_Szam = iktatas.KuldemenyAdatok.ErkeztetoSzam.Sorszam.ToString();
                        kuld.Updated.Erkezteto_Szam = true;
                    }
                }

                if (iktatas.KuldemenyAdatok.ErkeztetesAdatok != null)
                {
                    if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.ErkeztetesAdatok.KezbesitesModja))
                    {
                        kuld.KezbesitesModja = iktatas.KuldemenyAdatok.ErkeztetesAdatok.KezbesitesModja;
                        kuld.Updated.KezbesitesModja = true;
                    }

                    if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.ErkeztetesAdatok.BeerkezesModja))
                    {
                        kuld.KuldesMod = iktatas.KuldemenyAdatok.ErkeztetesAdatok.BeerkezesModja;
                        kuld.Updated.KuldesMod = true;
                    }

                    if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.ErkeztetesAdatok.Ragszam))
                    {
                        kuld.RagSzam = iktatas.KuldemenyAdatok.ErkeztetesAdatok.Ragszam;
                        kuld.Updated.RagSzam = true;
                    }

                    if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.ErkeztetesAdatok.Cimzett))
                    {
                        Result felhasznaloResult = GetFelhasznaloByUserNev(iktatas.KuldemenyAdatok.ErkeztetesAdatok.Cimzett, valasz);

                        if (felhasznaloResult.IsError)
                        {
                            valasz.SetError(felhasznaloResult);
                            return null;
                        }
                        else if (felhasznaloResult.Ds.Tables[0].Rows.Count > 0)
                        {
                            kuld.Csoport_Id_Cimzett = felhasznaloResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                            kuld.Updated.Csoport_Id_Cimzett = true;
                        }
                        else
                        {
                            Result csoportResult = GetCsoportByUserNev(iktatas.KuldemenyAdatok.ErkeztetesAdatok.Cimzett, valasz);
                            if (csoportResult.IsError)
                            {
                                valasz.SetError(csoportResult);
                                return null;
                            }
                            else if (csoportResult.Ds.Tables[0].Rows.Count < 1)
                            {
                                // Ideiglenenes megoldás, később szinkronizálás lesz
                                //valasz.SetError("KuldemenyAdatok.ErkeztetesAdatok.Cimzett nem található", -2);
                                //return null;
                            }
                            else
                            {
                                kuld.Csoport_Id_Cimzett = csoportResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                                kuld.Updated.Csoport_Id_Cimzett = true;
                            }
                        }
                    }

                    if (iktatas.KuldemenyAdatok.ErkeztetesAdatok.BeerkezesIdopontjaSpecified)
                    {
                        kuld.BeerkezesIdeje = iktatas.KuldemenyAdatok.ErkeztetesAdatok.BeerkezesIdopontja.ToString();
                        kuld.Updated.BeerkezesIdeje = true;
                    }

                    if (iktatas.KuldemenyAdatok.ErkeztetesAdatok.FeladasIdopontjaSpecified)
                    {
                        kuld.BelyegzoDatuma = iktatas.KuldemenyAdatok.ErkeztetesAdatok.FeladasIdopontja.ToString();
                        kuld.Updated.BelyegzoDatuma = true;
                    }

                    if (iktatas.KuldemenyAdatok.ErkeztetesAdatok.ErkeztetesDatumaSpecified)
                    {
                        kuld.Base.LetrehozasIdo = iktatas.KuldemenyAdatok.ErkeztetesAdatok.ErkeztetesDatuma.ToString();
                        kuld.Base.Updated.LetrehozasIdo = true;
                    }
                }
                if (iktatas.KuldemenyAdatok.BontasAdatok != null)
                {
                    if (iktatas.KuldemenyAdatok.BontasAdatok.BontasDatumaSpecified)
                    {
                        kuld.FelbontasDatuma = iktatas.KuldemenyAdatok.BontasAdatok.BontasDatuma.ToString();
                        kuld.Updated.FelbontasDatuma = true;
                    }

                    if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.BontasAdatok.KuldemenyTargya))
                    {
                        kuld.Targy = iktatas.KuldemenyAdatok.BontasAdatok.KuldemenyTargya;
                        kuld.Updated.Targy = true;
                    }

                    if (iktatas.KuldemenyAdatok.BontasAdatok.SurgossegSpecified)
                    {
                        kuld.Surgosseg = iktatas.KuldemenyAdatok.BontasAdatok.Surgosseg.ToString();
                    }
                    else
                    {
                        kuld.Surgosseg = "30";
                    }
                    kuld.Updated.Surgosseg = true;

                    if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.BontasAdatok.UgyintezesModja))
                    {
                        kuld.UgyintezesModja = iktatas.KuldemenyAdatok.BontasAdatok.UgyintezesModja;
                        kuld.Updated.UgyintezesModja = true;
                        kuld.AdathordozoTipusa = iktatas.KuldemenyAdatok.BontasAdatok.UgyintezesModja;
                        kuld.Updated.AdathordozoTipusa = true;
                    }

                    if (iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo != null)
                    {
                        if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerAzonosito))
                        {
                            KRT_Partnerek partner = GetPartnerByKulsoAzonosito(iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerAzonosito, valasz.ValaszAdatok);
                            if (valasz.IsError())
                            {
                                return null;
                            }
                            else if (partner != null)
                            {
                                kuld.Partner_Id_Bekuldo = partner.Id;
                                kuld.Updated.Partner_Id_Bekuldo = true;
                                kuld.NevSTR_Bekuldo = partner.Nev;
                                kuld.Updated.NevSTR_Bekuldo = true;
                            }
                            else
                            {
                                // Ideiglenenes megoldás, később szinkronizálás lesz
                                //valasz.SetError("KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerAzonosito nem található", -101);
                                //return null;
                                kuld.NevSTR_Bekuldo = iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerAzonosito;
                                kuld.Updated.NevSTR_Bekuldo = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerNev))
                        {
                            kuld.NevSTR_Bekuldo = iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerNev;
                            kuld.Updated.NevSTR_Bekuldo = true;
                        }

                        if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerCimAzonosito))
                        {
                            KRT_Cimek cim = GetCimByKulsoAzonosito(iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerCimAzonosito, valasz.ValaszAdatok);
                            if (valasz.IsError())
                            {
                                return null;
                            }
                            else if (cim != null)
                            {
                                kuld.Cim_Id = cim.Id;
                                kuld.Updated.Cim_Id = true;
                                kuld.CimSTR_Bekuldo = cim.Nev;
                                kuld.Updated.CimSTR_Bekuldo = true;
                            }
                            else
                            {
                                // Ideiglenenes megoldás, később szinkronizálás lesz
                                //valasz.SetError("KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerCimAzonosito nem található", -102);
                                //return null;
                                kuld.CimSTR_Bekuldo = iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerCimAzonosito;
                                kuld.Updated.CimSTR_Bekuldo = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerCim))
                        {
                            kuld.CimSTR_Bekuldo = iktatas.KuldemenyAdatok.BontasAdatok.Bekuldo.PartnerCim;
                            kuld.Updated.CimSTR_Bekuldo = true;
                        }
                    }

                    if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.BontasAdatok.BontoFelhasznalo))
                    {
                        Result felhasznaloResult = GetFelhasznaloByUserNev(iktatas.KuldemenyAdatok.BontasAdatok.BontoFelhasznalo, valasz);

                        if (felhasznaloResult.IsError)
                        {
                            valasz.SetError(felhasznaloResult);
                            return null;
                        }
                        else if (felhasznaloResult.Ds.Tables[0].Rows.Count > 0)
                        {
                            kuld.FelhasznaloCsoport_Id_Bonto = felhasznaloResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                            kuld.Updated.FelhasznaloCsoport_Id_Bonto = true;
                        }
                        else
                        {
                            Result csoportResult = GetCsoportByUserNev(iktatas.KuldemenyAdatok.BontasAdatok.BontoFelhasznalo, valasz);
                            if (csoportResult.IsError)
                            {
                                valasz.SetError(csoportResult);
                                return null;
                            }
                            else if (csoportResult.Ds.Tables[0].Rows.Count < 1)
                            {
                                // Ideiglenenes megoldás, később szinkronizálás lesz
                                //valasz.SetError("KuldemenyAdatok.BontasAdatok.BontoFelhasznalo nem található", -2);
                                //return null;

                            }
                            else
                            {
                                kuld.FelhasznaloCsoport_Id_Bonto = csoportResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                                kuld.Updated.FelhasznaloCsoport_Id_Bonto = true;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.BontasAdatok.HivatkozasiSzam))
                    {
                        kuld.HivatkozasiSzam = iktatas.KuldemenyAdatok.BontasAdatok.HivatkozasiSzam;
                        kuld.Updated.HivatkozasiSzam = true;
                    }
                }
            }

            kuld.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            kuld.Updated.IktatniKell = true;
            kuld.IktatastNemIgenyel = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            kuld.Updated.IktatastNemIgenyel = true;

            return kuld;
        }

        public static Result GetFelhasznaloByUserNev(String userNev, Iktatas.Valasz valasz)
        {
            if (felhasznalok.ContainsKey(userNev))
            {
                return felhasznalok[userNev];
            }

            Contentum.eAdmin.Service.KRT_FelhasznalokService _KRT_FelhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();
            KRT_FelhasznalokSearch search = new KRT_FelhasznalokSearch();
            search.UserNev.Value = userNev;
            search.UserNev.Operator = Contentum.eQuery.Query.Operators.equals;

            search.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            Result res = _KRT_FelhasznalokService.GetAll(ExecParam, search);
            if (res.IsError)
            {
                return valasz.SetError(res);
            }

            if (res.Ds.Tables[0].Rows.Count > 0)
            {
                AddFelhasznalokToCache(userNev, res);
            }

            return res;
        }

        public static Result GetCsoportByUserNev(String userNev, Iktatas.Valasz valasz)
        {
            if (csoportok.ContainsKey(userNev))
            {
                return csoportok[userNev];
            }

            Contentum.eAdmin.Service.KRT_CsoportokService _KRT_CsoportokService = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();
            KRT_CsoportokSearch search = new KRT_CsoportokSearch();
            search.Kod.Value = userNev;
            search.Kod.Operator = Contentum.eQuery.Query.Operators.equals;

            search.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            Result res = _KRT_CsoportokService.GetAll(ExecParam, search);
            if (res.IsError)
            {
                return valasz.SetError(res);
            }

            if (res.Ds.Tables[0].Rows.Count > 0)
            {
                AddCsoportokToCache(userNev, res);
            }

            return res;
        }

        public static EREC_IraIratok GetIrat(string id, ValaszAdatok valaszAdatok)
        {
            EREC_IraIratokService iktatasIratService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam eParam = GlobalExecParam.Clone();
            eParam.Record_Id = id;
            Result iratResult = iktatasIratService.Get(eParam);

            if (iratResult.IsError)
            {
                SetError(valaszAdatok, iratResult);
                return null;
            }

            EREC_IraIratok iktatottIrat = (EREC_IraIratok)iratResult.Record;

            return iktatottIrat;
        }

        public static EREC_PldIratPeldanyok GetIratPeldanyById(string id)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam eParam = GlobalExecParam.Clone();
            eParam.Record_Id = id;
            Result iratResult = service.Get(eParam);

            if (iratResult.IsError)
            {
                return null;
            }

            EREC_PldIratPeldanyok peldany = (EREC_PldIratPeldanyok)iratResult.Record;

            return peldany;
        }

        public static Result SearchIratByUgyiratAndAlszamWithExtension(string ugyiratId, int alszam, ValaszAdatok valaszAdatok)
        {
            EREC_IraIratokService iktatasIratService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam eParam = GlobalExecParam;

            EREC_IraIratokSearch search = new EREC_IraIratokSearch();
            if (!string.IsNullOrEmpty(ugyiratId))
            {
                search.Ugyirat_Id.Filter(ugyiratId);
            }

            search.Alszam.Filter(alszam.ToString());

            Result iratResult = iktatasIratService.GetAllWithExtension(eParam, search);

            if (iratResult.IsError)
            {
                SetError(valaszAdatok, iratResult);
                return null;
            }

            return iratResult;
        }

        public static Result SearchIratByUgyiratAndAlszamORKulsoAzonosito(string ugyiratId, int alszam, string kulsoAzonosito, ValaszAdatok valaszAdatok)
        {
            EREC_IraIratokService iktatasIratService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam eParam = GlobalExecParam;

            EREC_IraIratokSearch search = new EREC_IraIratokSearch();

            search.Alszam.Value = alszam.ToString();
            search.Alszam.Operator = Contentum.eQuery.Query.Operators.equals;
            search.Alszam.Group = "100";
            search.Alszam.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.Ugyirat_Id.Value = ugyiratId;
            search.Ugyirat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            search.Ugyirat_Id.Group = "100";
            search.Ugyirat_Id.GroupOperator = Contentum.eQuery.Query.Operators.and;

            if (!string.IsNullOrEmpty(kulsoAzonosito))
            {
                search.WhereByManual = " OR EREC_IraIratok.HivatkozasiSzam='" + kulsoAzonosito + "' ";
            }

            Result iratResult = iktatasIratService.GetAll(eParam, search);

            if (iratResult.IsError)
            {
                SetError(valaszAdatok, iratResult);
                return null;
            }

            return iratResult;
        }

        public static Result SearchIratByKulsoAzonosito(string kulsoAzonosito, Iktatas.Valasz valasz)
        {
            EREC_IraIratokService iktatasIratService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam eParam = GlobalExecParam;

            EREC_IraIratokSearch search = new EREC_IraIratokSearch();
            if (!string.IsNullOrEmpty(kulsoAzonosito))
            {
                search.HivatkozasiSzam.Value = kulsoAzonosito;
                search.HivatkozasiSzam.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            Result iratResult = iktatasIratService.GetAll(eParam, search);

            if (iratResult.IsError)
            {
                SetError(valasz, iratResult);
                return null;
            }

            return iratResult;
        }

        public static Result GetIratWithExtension(string id, Iktatas.Valasz valasz)
        {
            EREC_IraIratokService iktatasIratService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam eParam = GlobalExecParam;
            eParam.Record_Id = id;
            EREC_IraIratokSearch search = new EREC_IraIratokSearch();
            search.Id.Value = id;
            search.Id.Operator = Contentum.eQuery.Query.Operators.equals;
            Result iratResult = iktatasIratService.GetAllWithExtension(eParam, search);

            if (iratResult.IsError)
            {
                SetError(valasz, iratResult);
                return null;
            }

            return iratResult;
        }

        public static EREC_IraIktatoKonyvek GetIktatoKonyv(string id, Dictionary<string, EREC_IraIktatoKonyvek> iktatoKonyvek, Iktatas.Valasz valasz)
        {
            if (iktatoKonyvek.ContainsKey(id))
            {
                return iktatoKonyvek[id];
            }

            EREC_IraIktatoKonyvekService iktatasIratService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam eParam = GlobalExecParam;
            eParam.Record_Id = id;
            Result iratResult = iktatasIratService.Get(eParam);

            if (iratResult.IsError)
            {
                SetError(valasz, iratResult);
                return null;
            }

            EREC_IraIktatoKonyvek iktatoKonyv = (EREC_IraIktatoKonyvek)iratResult.Record;

            if (iktatoKonyv != null)
            {
                iktatoKonyvek.Add(id, iktatoKonyv);
            }

            return iktatoKonyv;
        }

        public static EREC_IraIktatoKonyvek GetIktatoKonyvWithoutCaching(string id, Dictionary<string, EREC_IraIktatoKonyvek> iktatoKonyvek, Iktatas.Valasz valasz)
        {
            EREC_IraIktatoKonyvekService iktatasIratService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam eParam = GlobalExecParam;
            eParam.Record_Id = id;
            Result iratResult = iktatasIratService.Get(eParam);

            if (iratResult.IsError)
            {
                SetError(valasz, iratResult);
                return null;
            }

            EREC_IraIktatoKonyvek iktatoKonyv = (EREC_IraIktatoKonyvek)iratResult.Record;

            return iktatoKonyv;
        }

        public static Result GetErkeztetoByKod(string kod, int ev, Iktatas.Valasz valasz)
        {
            EREC_IraIktatoKonyvekService iktatasIratService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam eParam = GlobalExecParam;
            EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();
            search.MegkulJelzes.Value = kod;
            search.MegkulJelzes.Operator = Contentum.eQuery.Query.Operators.equals;
            search.Statusz.Value = "1";
            search.Statusz.Operator = Contentum.eQuery.Query.Operators.equals;
            search.Ev.Value = ev.ToString();
            search.Ev.Operator = Contentum.eQuery.Query.Operators.equals;

            search.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            Result iratResult = iktatasIratService.GetAll(eParam, search);

            if (iratResult.IsError)
            {
                SetError(valasz, iratResult);
                return null;
            }

            return iratResult;
        }
        public static Result UpdateOrInsertHAIRAdatok(string objId, string objTipId, string tipus, string ertek)
        {
            INT_HAIRAdatok getResult = GetHAIRAdatok(objId, objTipId, tipus);
            Result result;
            if (getResult != null)
            {
                getResult.Ertek = ertek;
                result = UpdateHAIRAdatok(getResult);
            }
            else
            {
                result = InsertHAIRAdatok(objId, objTipId, tipus, ertek);
            }
            return result;
        }
        public static Result InsertHAIRAdatok(string objId, string objTipId, string tipus, string ertek)
        {
            Contentum.eIntegrator.Service.INT_HAIRAdatokService service = eIntegratorService.ServiceFactory.GetINT_HAIRAdatokService();
            ExecParam execParam = GlobalExecParam.Clone();
            INT_HAIRAdatok adat = new INT_HAIRAdatok();

            adat.Updated.SetValueAll(false);
            adat.Base.Updated.SetValueAll(false);

            adat.Obj_Id = objId;
            adat.Updated.Obj_Id = true;
            adat.ObjTip_Id = objTipId;
            adat.Updated.ObjTip_Id = true;
            adat.Tipus = tipus;
            adat.Updated.Tipus = true;
            adat.Ertek = ertek;
            adat.Updated.Ertek = true;
            Result res = service.Insert(execParam, adat);

            return res;
        }

        public static INT_HAIRAdatok GetHAIRAdatok(string objId, string objTipId, string tipus)
        {
            var service = eIntegratorService.ServiceFactory.GetINT_HAIRAdatokService();
            var execParam = GlobalExecParam.Clone();
            var search = new INT_HAIRAdatokSearch();
            search.Obj_Id.Filter(objId);
            search.ObjTip_Id.Filter(objTipId);
            search.Tipus.Filter(tipus);
            var result = service.GetAll(execParam, search);
            if (result.IsError || result.GetCount < 1)
            {
                return null;
            }

            var adatok = new INT_HAIRAdatok();
            Utility.LoadBusinessDocumentFromDataRow(adatok, result.Ds.Tables[0].Rows[0]);
            return adatok;
        }

        public static Result UpdateHAIRAdatok(INT_HAIRAdatok adat)
        {
            var service = eIntegratorService.ServiceFactory.GetINT_HAIRAdatokService();
            var xp = GlobalExecParam.Clone();
            xp.Record_Id = adat.Id;
            adat.Base.Updated.Ver = true;

            return service.Update(xp, adat);
        }

        public static EREC_UgyUgyiratok GetUgyiratById(string id, Iktatas.Valasz valasz)
        {
            EREC_UgyUgyiratokService iktatasUgyiratIratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam eParam = GlobalExecParam;
            eParam.Record_Id = id;
            Result ugyiratResult = iktatasUgyiratIratService.Get(eParam);

            if (ugyiratResult.IsError)
            {
                SetError(valasz, ugyiratResult);
                return null;
            }

            EREC_UgyUgyiratok iktatottIrat = (EREC_UgyUgyiratok)ugyiratResult.Record;

            return iktatottIrat;
        }

        public static List<EREC_UgyUgyiratok> GetUgyiratokById(List<string> idList)
        {
            if (idList.Count == 0)
            {
                return new List<EREC_UgyUgyiratok>();
            }
            Logger.Debug("GetUgyiratokById. List:" + String.Join(",", idList.ToArray()));

            EREC_UgyUgyiratokService iktatasUgyiratIratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam eParam = GlobalExecParam;
            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            search.Id.In(idList);

            var ugyiratResult = iktatasUgyiratIratService.GetAll(eParam, search);
            if (ugyiratResult.IsError)
            {
                Logger.Debug("GetUgyiratokById  Error: " + ugyiratResult.ErrorMessage);
                return new List<EREC_UgyUgyiratok>();
            }
            if (!ugyiratResult.HasData())
            {
                Logger.Debug("GetUgyiratokById Result.Count = 0");
                return new List<EREC_UgyUgyiratok>();
            }
            return ugyiratResult.GetData<EREC_UgyUgyiratok>();
        }

        public static EREC_UgyUgyiratok GetUgyiratByAzonosito(string azonosito, Iktatas.Valasz valasz)
        {
            EREC_UgyUgyiratokService iktatasUgyiratIratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam eParam = GlobalExecParam;
            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            search.Azonosito.Value = azonosito;
            search.Azonosito.Operator = Contentum.eQuery.Query.Operators.equals;

            Result ugyiratResult = iktatasUgyiratIratService.GetAll(eParam, search);

            if (ugyiratResult.IsError)
            {
                SetError(valasz, ugyiratResult);
                return null;
            }
            if (ugyiratResult.Ds.Tables[0].Rows.Count < 1)
                return null;

            EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
            Utility.LoadBusinessDocumentFromDataRow(ugyirat, ugyiratResult.Ds.Tables[0].Rows[0]);
            return ugyirat;
        }

        public static List<EREC_UgyUgyiratok> GetUgyiratokByAzonosito(List<string> azonositoList)
        {
            if (azonositoList.Count == 0)
            {
                return new List<EREC_UgyUgyiratok>();
            }
            Logger.Debug("GetUgyiratokByAzonosito. List:" + String.Join(",", azonositoList.ToArray()));
            //Megpróbáljuk lekérni a technikai iktatókönyvet hogy annak az idjára tudjunk szűrni
            var iktatoKonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            var iktatoKonyvSearch = new EREC_IraIktatoKonyvekSearch();
            iktatoKonyvSearch.Nev.Filter(TECHNIKAI_IKTATOKONYV_IKTATOHELY);
            iktatoKonyvSearch.Ev.Filter(DateTime.Now.Year.ToString());
            var iktatoKonyvResult = iktatoKonyvekService.GetAll(GlobalExecParam, iktatoKonyvSearch);
            if (iktatoKonyvResult.IsError)
            {
                Logger.Debug("GetUgyiratokByAzonosito nem sikerült az iktatokonyv lekerese. Nev:" + TECHNIKAI_IKTATOKONYV_IKTATOHELY + " Ev: " + DateTime.Now.Year.ToString()+ " Error: "+  iktatoKonyvResult.ErrorMessage);
            }
            EREC_IraIktatoKonyvek iktatoKonyv = null;
            if(!iktatoKonyvResult.IsError && iktatoKonyvResult.HasData())
            {
                iktatoKonyv = iktatoKonyvResult.GetData<EREC_IraIktatoKonyvek>().SingleOrDefault();
            }

            EREC_UgyUgyiratokService iktatasUgyiratIratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam eParam = GlobalExecParam;
            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            search.Azonosito.In(azonositoList);
            if(iktatoKonyv != null)
            {
                search.IraIktatokonyv_Id.Filter(iktatoKonyv.Id);
            }
            var ugyiratResult = iktatasUgyiratIratService.GetAll(eParam, search);
            if (ugyiratResult.IsError)
            {

                Logger.Debug("GetUgyiratokByAzonosito. Error: " + ugyiratResult.ErrorMessage);
                return new List<EREC_UgyUgyiratok>();
            }
            if (!ugyiratResult.HasData())
            {
                Logger.Debug("GetUgyiratokByAzonosito: Result.Count = 0");
                return new List<EREC_UgyUgyiratok>();
            }
            return ugyiratResult.GetData<EREC_UgyUgyiratok>();
        }

        public static EREC_KuldKuldemenyek GetKuldemeny(string id, Iktatas.Valasz valasz)
        {
            EREC_KuldKuldemenyekService iktatasUgyiratIratService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam eParam = GlobalExecParam;
            eParam.Record_Id = id;
            Result ugyiratResult = iktatasUgyiratIratService.Get(eParam);

            if (ugyiratResult.IsError)
            {
                SetError(valasz, ugyiratResult);
                return null;
            }

            EREC_KuldKuldemenyek iktatottIrat = (EREC_KuldKuldemenyek)ugyiratResult.Record;

            return iktatottIrat;
        }

        public static EREC_PldIratPeldanyok GetIratPeldanyFromRequestForFiling(Iktatas.Iktatas iktatas, Iktatas.Valasz valasz)
        {
            EREC_PldIratPeldanyok iratPeldany = new EREC_PldIratPeldanyok();
            iratPeldany.Updated.SetValueAll(false);
            iratPeldany.Base.Updated.SetValueAll(false);

            if (iktatas.IratAdatok.AlszamAdatok.IratIranya == KodTarak.POSTAZAS_IRANYA.Belso)
            {
                if (iktatas.IratAdatok.IratpeldanyAdatok != null)
                {
                    if (!string.IsNullOrEmpty(iktatas.IratAdatok.IratpeldanyAdatok.KuldesModja))
                    {
                        iratPeldany.KuldesMod = iktatas.IratAdatok.IratpeldanyAdatok.KuldesModja;
                        iratPeldany.Updated.KuldesMod = true;
                    }

                    if (!string.IsNullOrEmpty(iktatas.IratAdatok.IratpeldanyAdatok.UgyintezesModja))
                    {
                        iratPeldany.UgyintezesModja = iktatas.IratAdatok.IratpeldanyAdatok.UgyintezesModja;
                        iratPeldany.Updated.UgyintezesModja = true;
                    }

                    if (!string.IsNullOrEmpty(iktatas.IratAdatok.IratpeldanyAdatok.Felelos))
                    {
                        Result felhasznaloResult = GetFelhasznaloByUserNev(iktatas.IratAdatok.IratpeldanyAdatok.Felelos, valasz);

                        if (felhasznaloResult.IsError)
                        {
                            valasz.SetError(felhasznaloResult);
                            return null;
                        }
                        else if (felhasznaloResult.Ds.Tables[0].Rows.Count > 0)
                        {
                            iratPeldany.Csoport_Id_Felelos = felhasznaloResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                            iratPeldany.Updated.Csoport_Id_Felelos = true;
                        }
                        else
                        {
                            Result csoportResult = GetCsoportByUserNev(iktatas.IratAdatok.IratpeldanyAdatok.Felelos, valasz);
                            if (csoportResult.IsError)
                            {
                                valasz.SetError(csoportResult);
                                return null;
                            }
                            else if (csoportResult.Ds.Tables[0].Rows.Count < 1)
                            {
                                // Ideiglenenes megoldás, később szinkronizálás lesz
                                //valasz.SetError("IratAdatok.IratpeldanyAdatok.Felelos nem található", -2);
                                //return null;
                            }
                            else
                            {
                                iratPeldany.Csoport_Id_Felelos = csoportResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                                iratPeldany.Updated.Csoport_Id_Felelos = true;
                            }
                        }
                    }

                    if (iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel != null)
                    {
                        if (!string.IsNullOrEmpty(iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerAzonosito))
                        {
                            KRT_Partnerek partner = GetPartnerByKulsoAzonosito(iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerAzonosito, valasz.ValaszAdatok);
                            if (valasz.IsError())
                            {
                                return null;
                            }
                            else if (partner != null)
                            {
                                iratPeldany.Partner_Id_Cimzett = partner.Id;
                                iratPeldany.Updated.Partner_Id_Cimzett = true;
                                iratPeldany.NevSTR_Cimzett = partner.Nev;
                                iratPeldany.Updated.NevSTR_Cimzett = true;
                            }
                            else
                            {
                                // Ideiglenenes megoldás, később szinkronizálás lesz
                                //valasz.SetError("IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerAzonosito nem található", -103);
                                //return null;
                                iratPeldany.NevSTR_Cimzett = iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerAzonosito;
                                iratPeldany.Updated.NevSTR_Cimzett = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerNev))
                        {
                            iratPeldany.NevSTR_Cimzett = iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerNev;
                            iratPeldany.Updated.NevSTR_Cimzett = true;
                        }

                        if (!string.IsNullOrEmpty(iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerCimAzonosito))
                        {
                            KRT_Cimek cim = GetCimByKulsoAzonosito(iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerCimAzonosito, valasz.ValaszAdatok);
                            if (valasz.IsError())
                            {
                                return null;
                            }
                            else if (cim != null)
                            {
                                iratPeldany.Cim_id_Cimzett = cim.Id;
                                iratPeldany.Updated.Cim_id_Cimzett = true;
                                iratPeldany.CimSTR_Cimzett = cim.Nev;
                                iratPeldany.Updated.CimSTR_Cimzett = true;
                            }
                            else
                            {
                                // Ideiglenenes megoldás, később szinkronizálás lesz
                                //valasz.SetError("IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerCimAzonosito nem található", -104);
                                //return null;
                                iratPeldany.CimSTR_Cimzett = iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerCimAzonosito;
                                iratPeldany.Updated.CimSTR_Cimzett = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerCim))
                        {
                            iratPeldany.CimSTR_Cimzett = iktatas.IratAdatok.IratpeldanyAdatok.Ugyfel.PartnerCim;
                            iratPeldany.Updated.CimSTR_Cimzett = true;
                        }
                    }
                }
            }

            return iratPeldany;
        }

        public static void SetDefaultValasz(Iktatas.Iktatas iktatas, Iktatas.Valasz valasz)
        {
            if (iktatas.Alapadatok != null)
            {
                valasz.KulsoAzonosito = iktatas.Alapadatok.KulsoAzonosito;
            }
            else
            {
                valasz.SetError(Messages.IktatasAlapadatokMissingError, -3);
                return;
            }


            if (iktatas.KuldemenyAdatok != null && iktatas.KuldemenyAdatok.ErkeztetoSzam != null)
            {
                if (valasz.ErkeztetoSzam == null)
                {
                    valasz.ErkeztetoSzam = new Iktatas.ErkeztetoSzam();
                }

                if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.ErkeztetoSzam.ErkeztetoKonyvKod))
                {
                    valasz.ErkeztetoSzam.ErkeztetoKonyvKod = iktatas.KuldemenyAdatok.ErkeztetoSzam.ErkeztetoKonyvKod;
                }

                if (iktatas.KuldemenyAdatok.ErkeztetoSzam.EvSpecified)
                {
                    valasz.ErkeztetoSzam.Ev = iktatas.KuldemenyAdatok.ErkeztetoSzam.Ev;
                    valasz.ErkeztetoSzam.EvSpecified = true;
                }

                if (iktatas.KuldemenyAdatok.ErkeztetoSzam.SorszamSpecified)
                {
                    valasz.ErkeztetoSzam.Sorszam = iktatas.KuldemenyAdatok.ErkeztetoSzam.Sorszam;
                    valasz.ErkeztetoSzam.SorszamSpecified = true;
                }
            }

            if (iktatas.IratAdatok != null && iktatas.IratAdatok.Iktatoszam != null)
            {
                if (valasz.Iktatoszam == null)
                {
                    valasz.Iktatoszam = new IktatoszamSorszam();
                }

                if (!string.IsNullOrEmpty(iktatas.IratAdatok.Iktatoszam.Elotag))
                {
                    valasz.Iktatoszam.Elotag = iktatas.IratAdatok.Iktatoszam.Elotag;
                }

                if (iktatas.IratAdatok.Iktatoszam.FoszamSpecified)
                {
                    valasz.Iktatoszam.Foszam = iktatas.IratAdatok.Iktatoszam.Foszam;
                    valasz.Iktatoszam.FoszamSpecified = true;
                }

                if (iktatas.IratAdatok.Iktatoszam.AlszamSpecified)
                {
                    valasz.Iktatoszam.Alszam = iktatas.IratAdatok.Iktatoszam.Alszam;
                    valasz.Iktatoszam.AlszamSpecified = true;
                }

                if (iktatas.IratAdatok.Iktatoszam.EvSpecified)
                {
                    valasz.Iktatoszam.Ev = iktatas.IratAdatok.Iktatoszam.Ev;
                    valasz.Iktatoszam.EvSpecified = true;
                }
            }

            if (iktatas.KuldemenyAdatok != null)
            {
                if (valasz.Vonalkodok == null)
                {
                    valasz.Vonalkodok = new KuldemenyVonalkodok();
                }
                if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.Vonalkod))
                {
                    valasz.Vonalkodok.KuldemenyVonalkod = iktatas.KuldemenyAdatok.Vonalkod;
                }
            }
        }

        public static KRT_Cimek GetCimByKulsoAzonosito(string kulsoAzonosito, ValaszAdatok valaszAdatok)
        {
            if (cimek.Any(x => x.Value.KulsoAzonositok == kulsoAzonosito))
            {
                return cimek.First(x => x.Value.KulsoAzonositok == kulsoAzonosito).Value;
            }
            KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam execParam = GlobalExecParam.Clone();

            KRT_CimekSearch cimekSearch = new KRT_CimekSearch();
            cimekSearch.OrderBy = null;

            cimekSearch.KulsoAzonositok.Value = kulsoAzonosito;
            cimekSearch.KulsoAzonositok.Operator = Contentum.eQuery.Query.Operators.equals;

            cimekSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            cimekSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            cimekSearch.ErvKezd.Group = "100";
            cimekSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            cimekSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            cimekSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            cimekSearch.ErvVege.Group = "100";
            cimekSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            Result cimekResult = cimekService.GetAll(execParam, cimekSearch);

            if (cimekResult.IsError)
            {
                SetError(valaszAdatok, cimekResult);
                return null;
            }

            if (cimekResult.Ds.Tables[0].Rows.Count > 0)
            {
                KRT_Cimek cim = new KRT_Cimek();
                Utility.LoadBusinessDocumentFromDataRow(cim, cimekResult.Ds.Tables[0].Rows[0]);
                AddCimekToCache(cim);
                return cim;
            }

            return null;
        }

        public static bool IsError(this Iktatas.Valasz valasz)
        {
            return IsError(valasz.ValaszAdatok);
        }

        public static bool IsError(this ValaszAdatok valaszAdatok)
        {
            return !string.IsNullOrEmpty(valaszAdatok.HibaUzenet);
        }

        public static void SetError(this Iktatas.Valasz valasz, ResultException resultException)
        {
            if (resultException != null)
            {
                var result = resultException.GetResult();
                if (result != null)
                {
                    valasz.SetError(result);
                }
            }
        }

        public static Result SetError(this Iktatas.Valasz valasz, Result result)
        {
            SetError(valasz.ValaszAdatok, result.ErrorMessage, result.ErrorCode.GetErrorCode(-1));
            return result;
        }

        public static void SetError(this Iktatas.Valasz valasz, string errorMessage, int errorCode)
        {
            SetError(valasz.ValaszAdatok, errorMessage, errorCode);
        }

        public static void SetError(this Iktatas.Valasz valasz, string errorMessage)
        {
            valasz.SetError(errorMessage, -1);
        }

        public static void SetError(this Iktatas.Valasz valasz, int errorCode)
        {
            valasz.SetError(ErrorCodes.GetErrorMessage(errorCode), errorCode);
        }

        public static void SetError(this ValaszAdatok valaszAdatok, string errorMessage, int errorCode)
        {
            valaszAdatok.HibaUzenet = errorMessage;
            valaszAdatok.HibaKod = errorCode;
            Logger.Error("HAIR error. HibaUzenet: " + errorMessage + "HibaKod: " + errorCode + Environment.NewLine + "StackTrace: " + Environment.StackTrace);
        }

        public static Result SetError(this ValaszAdatok valaszAdatok, Result result)
        {
            if (result != null)
            {
                SetError(valaszAdatok, result.ErrorMessage, result.ErrorCode.GetErrorCode(-1));
            }
            return result;
        }

        public static void SetError(this ValaszAdatok valaszAdatok, int errorCode)
        {
            SetError(valaszAdatok, ErrorCodes.GetErrorMessage(errorCode), errorCode);
        }

        public static void SetError(this ValaszAdatok valaszAdatok, Exception exception)
        {
            if (exception != null)
            {
                SetError(valaszAdatok, exception.ToString(), ErrorCodes.InternalError);
            }
        }

        public static Result GetTargyszoByBelsoAzonosito(string belsoAzonosito)
        {
            EREC_TargySzavakService targySzavakServiceservice = eRecordService.ServiceFactory.GetEREC_TargySzavakService();
            ExecParam execParam = GlobalExecParam.Clone();
            EREC_TargySzavakSearch search = new EREC_TargySzavakSearch();
            search.BelsoAzonosito.Value = belsoAzonosito;
            search.BelsoAzonosito.Operator = Contentum.eQuery.Query.Operators.equals;

            search.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            Result targySzavakResult = targySzavakServiceservice.GetAll(execParam, search);

            return targySzavakResult;
        }

        public static DataRow GetTargyszoByBelsoAzonosito(string belsoAzonosito, ValaszAdatok valaszAdatok)
        {
            if (targyszavak.ContainsKey(belsoAzonosito))
                return targyszavak[belsoAzonosito];

            Result targySzavakResult = GetTargyszoByBelsoAzonosito(belsoAzonosito);

            if (targySzavakResult.IsError)
            {
                valaszAdatok.SetError(targySzavakResult);
                return null;
            }

            if (targySzavakResult.Ds.Tables[0].Rows.Count < 1)
            {
                return null;
            }
            DataRow val = targySzavakResult.Ds.Tables[0].Rows[0];
            AddTargyszoToCache(belsoAzonosito, targySzavakResult.Ds.Tables[0].Rows[0]);

            return val;
        }

        public static string GetObjTipusByKod(string kod, ValaszAdatok valaszAdatok)
        {
            if (objTipusKodok.ContainsKey(kod))
            {
                return objTipusKodok[kod];
            }

            Result result = GetObjTipusByKod(kod);

            if (result.IsError)
            {
                SetError(valaszAdatok, result);
                return null;
            }

            if (result.Ds.Tables[0].Rows.Count < 1)
            {
                return null;
            }
            string val = result.Ds.Tables[0].Rows[0]["Id"].ToString();
            AddObjTipusKodokToCache(kod, val);

            return val;
        }

        public static Result GetObjTipusByKod(string kod)
        {
            KRT_ObjTipusokService objTipusServiceservice = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
            ExecParam execParam = GlobalExecParam.Clone();
            KRT_ObjTipusokSearch search = new KRT_ObjTipusokSearch();
            search.Kod.Value = kod;
            search.Kod.Operator = Contentum.eQuery.Query.Operators.equals;

            search.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            Result result = objTipusServiceservice.GetAll(execParam, search);

            return result;
        }

        public static void InsertObjektumTargyszavak(string iratId, string ugyFajtaja, StatisztikaElem[] statisztikaElem, ValaszAdatok valaszAdatok)
        {
            if (statisztikaElem != null)
            {
                EREC_ObjektumTargyszavaiService objektumTargySzavaiService = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                EREC_Obj_MetaAdataiService metaAdataiService = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();

                for (int i = 0; i < statisztikaElem.Length; i++)
                {
                    if (statisztikaElem[i] != null)
                    {
                        string kod = statisztikaElem[i].StatisztikaElemKod;
                        string ertek = statisztikaElem[i].StatisztikaElemErtek;

                        DataRow targyszo = GetTargyszoByBelsoAzonosito(kod, valaszAdatok);
                        if (valaszAdatok.IsError())
                        {
                            return;
                        }
                        if (targyszo == null)
                            continue;

                        string objTipusGuid = GetObjTipusByKod("EREC_IraIratok", valaszAdatok);
                        if (valaszAdatok.IsError())
                        {
                            return;
                        }
                        if (objTipusGuid == null)
                            continue;


                        EREC_Obj_MetaAdataiSearch search = new EREC_Obj_MetaAdataiSearch();
                        search.WhereByManual = " AND EREC_Obj_MetaDefinicio.DefinicioTipus='HA' and EREC_Obj_MetaDefinicio.ColumnValue='" + ugyFajtaja + "'";
                        Result objmetaResult = metaAdataiService.GetAllWithExtension(GlobalExecParam.Clone(), search);

                        if (objmetaResult.IsError)
                        {
                            SetError(valaszAdatok, objmetaResult);
                            return;
                        }
                        if (objmetaResult.GetCount < 1)
                            continue;
                        EREC_ObjektumTargyszavai ot = new EREC_ObjektumTargyszavai();
                        ot.Updated.SetValueAll(false);
                        ot.Base.Updated.SetValueAll(false);
                        ot.Obj_Metaadatai_Id = objmetaResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                        ot.Updated.Obj_Metaadatai_Id = true;
                        ot.Targyszo_Id = targyszo["Id"].ToString();
                        ot.Updated.Targyszo_Id = true;
                        ot.Targyszo = targyszo["TargySzavak"].ToString();
                        ot.Updated.Targyszo = true;
                        ot.Obj_Id = iratId;
                        ot.Updated.Obj_Id = true;
                        ot.ObjTip_Id = objTipusGuid;
                        ot.Updated.ObjTip_Id = true;
                        ot.Ertek = ertek;
                        ot.Updated.Ertek = true;
                        ot.Forras = KodTarak.TARGYSZO_FORRAS.Automatikus;
                        ot.Updated.Forras = true;

                        Result insertRes = objektumTargySzavaiService.Insert(GlobalExecParam.Clone(), ot);
                        if (insertRes.IsError)
                        {
                            valaszAdatok.SetError(insertRes);
                            return;
                        }
                    }

                }
            }
        }

        public static EREC_IratAlairok GetIratAlairok(string iratId, string alairoId, DateTime datum, string alairasMod, string alairoSzerep, string alairasSorrend)
        {
            EREC_IratAlairok iratAlairok = new EREC_IratAlairok();
            iratAlairok.FelhasznaloCsoport_Id_Alairo = alairoId;
            iratAlairok.Updated.FelhasznaloCsoport_Id_Alairo = true;
            if (alairasMod == "E_EMB")
            {
                iratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
                iratAlairok.Updated.Allapot = true;
                iratAlairok.Leiras = "HAIR által iktatott irat automatikus aláírás előjegyzése";
                iratAlairok.Updated.Leiras = true;
            }
            else
            {
                iratAlairok.AlairasDatuma = datum.ToString();
                iratAlairok.Updated.AlairasDatuma = true;
                iratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                iratAlairok.Updated.Allapot = true;
                iratAlairok.Leiras = "HAIR által iktatott irat automatikus aláírása";
                iratAlairok.Updated.Leiras = true;
            }

            iratAlairok.AlairoSzerep = alairoSzerep;
            iratAlairok.Updated.AlairoSzerep = true;
            iratAlairok.AlairasMod = alairasMod;
            iratAlairok.Updated.AlairasMod = true;
            iratAlairok.AlairasSorrend = alairasSorrend;
            iratAlairok.Updated.AlairasSorrend = true;
            iratAlairok.Obj_Id = iratId;
            iratAlairok.Updated.Obj_Id = true;

            return iratAlairok;
        }

        public static string GetAlairoSzabalyId(string alairoId, string iratId, string alirasMod, Iktatas.Valasz valasz)
        {
            try
            {
                return GetTomegesIktatasHelper().GetAlairoSzabalyId(alairoId, iratId, alirasMod);
            }
            catch (ResultException rx)
            {
                valasz.SetError(rx);
            }
            return null;
        }

        public static void InsertAlairas(EREC_IratAlairok iratAlairok, Iktatas.Valasz valasz)
        {
            ExecParam execParam_iratAlairas = GlobalExecParam.Clone();

            EREC_IratAlairokService service_iratAlairok = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
            Result result_iratAlairas = service_iratAlairok.Insert(execParam_iratAlairas, iratAlairok);

            if (result_iratAlairas.IsError)
            {
                valasz.SetError(result_iratAlairas);
                return;
            }
        }

        public static void Alairas(string iratID, string alairoId, DateTime datum, Iktatas.Valasz valasz)
        {
            EREC_IratAlairok adminUtolagosIratAlairo = GetIratAlairok(iratID, alairoId, datum, "M_UTO", KodTarak.ALAIRO_SZEREP.Kiadmanyozo, "2");

            string adminUtolagosIratAlairoSzabalyId = GetAlairoSzabalyId(alairoId, iratID, "M_UTO", valasz);

            if (valasz.IsError())
            {
                return;
            }
            if (!string.IsNullOrEmpty(adminUtolagosIratAlairoSzabalyId))
            {
                adminUtolagosIratAlairo.AlairasSzabaly_Id = adminUtolagosIratAlairoSzabalyId;
                adminUtolagosIratAlairo.Updated.AlairasSzabaly_Id = true;
            }
            else
            {
                SetError(valasz, Messages.AlairoszabalyNotFoundError, -6);
                return;
            }

            InsertAlairas(adminUtolagosIratAlairo, valasz);
        }

        public static void Elintezes(EREC_IraIratok irat, Iktatas.Valasz valasz)
        {
            var elintezesResult = GetTomegesIktatasHelper().Elintezes(irat);

            if (elintezesResult.IsError)
            {
                if (elintezesResult.ErrorCode == "80012")
                {
                    SetError(valasz, Messages.ElintezesOrzoError, -7);
                    return;
                }
                else
                {
                    SetError(valasz, elintezesResult);
                    return;
                }
            }
        }

        public static Result GetSzemelyByPartnerID(string partnerID)
        {
            KRT_SzemelyekService service = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
            ExecParam execParam = GlobalExecParam.Clone();
            KRT_SzemelyekSearch search = new KRT_SzemelyekSearch();
            search.OrderBy = null;


            search.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.Partner_Id.Value = partnerID;
            search.Partner_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            Result result = service.GetAll(execParam, search);

            return result;
        }
        public static Result GetPartnerByKulsoAzonosito(string kulsoAzonosito)
        {
            KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = GlobalExecParam.Clone();
            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.OrderBy = null;

            partnerekSearch.KulsoAzonositok.Value = kulsoAzonosito;
            partnerekSearch.KulsoAzonositok.Operator = Contentum.eQuery.Query.Operators.equals;

            partnerekSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            partnerekSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            partnerekSearch.ErvKezd.Group = "100";
            partnerekSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            partnerekSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            partnerekSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            partnerekSearch.ErvVege.Group = "100";
            partnerekSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            Result partnerekResult = partnerService.GetAll(execParam, partnerekSearch);

            return partnerekResult;
        }

        public static KRT_Partnerek GetPartnerByKulsoAzonosito(string kulsoAzonosito, ValaszAdatok valaszAdatok)
        {
            if (partnerek.Any(x => x.Value.KulsoAzonositok == kulsoAzonosito))
            {
                return partnerek.First(x => x.Value.KulsoAzonositok == kulsoAzonosito).Value;
            }
            var partnerekResult = GetPartnerByKulsoAzonosito(kulsoAzonosito);

            if (partnerekResult.IsError)
            {
                SetError(valaszAdatok, partnerekResult);
                return null;
            }
            if (partnerekResult.Ds.Tables[0].Rows.Count > 0)
            {
                KRT_Partnerek partner = new KRT_Partnerek();
                Utility.LoadBusinessDocumentFromDataRow(partner, partnerekResult.Ds.Tables[0].Rows[0]);
                AddPartnerToCache(partner);
                return partner;
            }

            return null;
        }

        public static ExecParam GetExecParam(string localUser)
        {
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = localUser;
            execParam.LoginUser_Id = localUser;

            return execParam;
        }

        public static int GetErrorCode(this string errorCode, int defaultValue)
        {
            int res;

            if (int.TryParse(errorCode, out res))
            {
                return res;
            }
            return defaultValue;
        }

        public static string GetParameter(string nev, string user)
        {
            ExecParam azonExecParam = GetExecParam(user);
            INT_ParameterekSearch search = new INT_ParameterekSearch();
            search.Nev.Value = nev;
            search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;
            string ret = null;
            Result res = INTParameterekService.GetAll(azonExecParam, search);
            if (res.IsError)
            {
                Logger.Error("HAIR GetParameter error.", azonExecParam, res);
                return null;
            }
            if (res.Ds.Tables.Count > 0 && res.Ds.Tables[0].Rows.Count > 0)
            {
                ret = GetRowValueAsString(res.Ds.Tables[0].Rows[0], "Ertek");
            }
            return ret;
        }

        public static string GetRowValueAsString(this DataRow row, string columnName)
        {
            return !IsDBNull(row[columnName]) ? row[columnName].ToString() : null;
        }

        public static bool IsDBNull(object obj)
        {
            return obj == System.DBNull.Value;
        }

        public static T[] Remove<T>(List<T> list, int index)
        {
            list.RemoveAt(index);
            return list.ToArray();
        }

        public static void SendErrorMail(string error, params Result[] result)
        {
            String[] MessageToAddress = { HIBA_EMAIL };
            string body;

            if (result != null && result.Any(x => x.IsError))
            {
                body = error + result.Select(x => Environment.NewLine + "ErrorMessage: " + x.ErrorMessage + Environment.NewLine + "ErrorCode: " + x.ErrorCode).Aggregate((x, y) => x + y);
            }
            else
            {
                body = error;
            }

            Logger.Error(body);
            if (EmailKuldesBekapcsolva)
            {
                Email.SendEmail(GlobalExecParam.Clone(),
               Rendszerparameterek.Get(GlobalExecParam.Clone(), Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM),
               MessageToAddress,
               EmailSubject,
               null,
               null,
               true,
               body);
            }

        }

        public static void SendErrorMail(string error, Exception ex)
        {
            String[] MessageToAddress = { HIBA_EMAIL };
            string body;

            if (ex != null)
            {
                body = error + Environment.NewLine + "ErrorMessage: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace;
            }
            else
            {
                body = error;
            }

            Logger.Error(body);

            Email.SendEmail(GlobalExecParam.Clone(),
           Rendszerparameterek.Get(GlobalExecParam.Clone(), Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM),
           MessageToAddress,
           EmailSubject,
           null,
           null,
           true,
           body);
        }

        public static EREC_UgyUgyiratok GetUgyiratByIktatoszam(Iktatoszam iktatoszam, ValaszAdatok v)
        {
            if (iktatoszam == null || !iktatoszam.IsValid())
            {
                v.SetError(ErrorCodes.ErvenytelenIktatoszam);
                return null;
            }

            Logger.Debug("Ügyirat iktatószám alapján - Iktatókönyv lekérdezés - BEGIN");
            //EREC_IraIktatoKonyvek iratIktatoKonyv = new EREC_IraIktatoKonyvek();
            Result iktatoKonyvekResult = Helper.GetIktatokonyvFromRequestForFiling(iktatoszam, v);

            if (iktatoKonyvekResult.IsError)
            {
                Helper.SetError(v, iktatoKonyvekResult);
                return null;
            }
            if (iktatoKonyvekResult.GetCount < 1)
            {
                Helper.SetError(v, ErrorCodes.IktatasIktatoKonyvNotFound);
                return null;
            }

            //Utility.LoadBusinessDocumentFromDataRow(iratIktatoKonyv, iktatoKonyvekResult.Ds.Tables[0].Rows[0]);
            var iktatoKonyvId = iktatoKonyvekResult.Ds.Tables[0].Rows[0]["Id"].ToString();

            Logger.Debug("Ügyirat iktatószám alapján  - Iktatókönyv lekérdezés - END");

            Logger.Debug("Ügyirat iktatószám alapján  - Ügyirat lekérdezés - BEGIN");

            // ügyirat keresése
            var ugyiratResult = Helper.GetUgyiratByIktatoKonyvFromRequestForFiling(iktatoszam, v, iktatoKonyvId);
            if (ugyiratResult.IsError)
            {
                Helper.SetError(v, ugyiratResult);
                return null;
            }

            if (ugyiratResult.GetCount < 1)
            {
                Logger.Debug("Ügyirat iktatószám alapján  - Ügyirat lekérdezés - Nincs találat");
                Helper.SetError(v, ErrorCodes.UgyiratNotFound);
                return null;
            }

            if (ugyiratResult.GetCount > 1)
            {
                Logger.Debug("Ügyirat iktatószám alapján  - Ügyirat lekérdezés - Több találat");
                Helper.SetError(v, ErrorCodes.UgyiratTooMany);
                return null;
            }

            // ügyirat betöltése
            var ugyirat = new EREC_UgyUgyiratok();
            Utility.LoadBusinessDocumentFromDataRow(ugyirat, ugyiratResult.Ds.Tables[0].Rows[0]);

            Logger.Debug("Ügyirat iktatószám alapján  - Ügyirat lekérdezés - END");
            return ugyirat;
        }

        public static EREC_IraIratok GetIratByIktatoszam(IktatoszamAlszam iktatoszam, ValaszAdatok v)
        {
            if (iktatoszam == null || !iktatoszam.IsValid())
            {
                v.SetError(ErrorCodes.ErvenytelenIktatoszam);
                return null;
            }

            // ügyirat lekérdezés
            var ugyirat = Helper.GetUgyiratByIktatoszam(iktatoszam, v);
            if (ugyirat == null || v.IsError())
            {
                return null;
            }

            // irat lekérdezés
            Logger.Debug("Irat lekérdezés - BEGIN");

            Result iratResult = Helper.SearchIratByUgyiratAndAlszamWithExtension(ugyirat.Id, iktatoszam.Alszam, v);
            if (v.IsError() || iratResult == null || iratResult.GetCount < 1)
            {
                return null;
            }
            Logger.Debug("Irat lekérdezés - END");

            Logger.Debug("Irat betöltés - BEGIN");
            var iratId = iratResult.Ds.Tables[0].Rows[0]["Id"].ToString();
            var irat = Helper.GetIrat(iratId, v);
            Logger.Debug("Irat betöltés - END");

            if (v.IsError())
            {
                return null;
            }

            return irat;
        }

        public static T InitValasz<T>(Alapadatok alapadatok) where T : Valasz, new()
        {
            return new T()
            {
                KulsoAzonosito = alapadatok == null ? "" : alapadatok.KulsoAzonosito,
                ValaszAdatok = new ValaszAdatok()
            };
        }

        public static string PadLeft(string id, int length)
        {
            if (id.Length < length)
            {
                id = id.PadLeft(length, '0');
            }
            return id;
        }

        static TomegesIktatasHelper GetTomegesIktatasHelper()
        {
            return new TomegesIktatasHelper(GlobalExecParam.Clone(), null);
        }
    }
}

