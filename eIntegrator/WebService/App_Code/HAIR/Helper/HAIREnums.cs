﻿namespace Contentum.eIntegrator.WebService.HAIR.IntServices
{
    public enum EnumHAIRServiceType
    {
        ALAPERTELMEZETT,
        IRAT_PELDANY_LETREHOZAS,
        CSATOLMANY_FELTOLTES,
        IRAT_MODOSITAS,
        PARTNER_MODOSITAS,
        POSTAZAS,
        TERTIVEVENY_ERKEZTETES,
        UGY_MODOSITAS,
    }

    public static class IktatoErkezteto
    {
        public const string Iktato = "I";
        public const string Erkezteto = "E";
        public const string Postakonyv = "P";
        public const string IratkezelesiSegedlet = "S";
    }
}