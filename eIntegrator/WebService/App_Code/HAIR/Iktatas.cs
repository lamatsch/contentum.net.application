﻿using System;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR.Iktatas {
    public static class Constants
    {
        public const string NAMESPACE = "Contentum.eIntegrator.WebService.HAIR.Iktatas";
    }

    #region Parameters

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Iktatas
    {
        [XmlElement(Order = 0)]
        public Alapadatok Alapadatok;
        [XmlElement(Order = 1)]
        public KuldemenyAdatok KuldemenyAdatok;
        [XmlElement(Order = 2)]
        public IratAdatok IratAdatok;
        [XmlElement(Order = 3)]
        public AtiktatasAdatok AtiktatasAdatok;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Alapadatok : HAIR.Alapadatok
    {
        public bool TechnikaiIktatas;
        [XmlIgnore] public bool TechnikaiIktatasSpecified;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class KuldemenyAdatok
    {
        public string Vonalkod; // 100
        public ErkeztetoSzam ErkeztetoSzam;
        public ErkeztetesAdatok ErkeztetesAdatok;
        public BontasAdatok BontasAdatok;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class ErkeztetoSzam
    {
        public int Ev;
        [XmlIgnore] public bool EvSpecified;

        public int Sorszam;
        [XmlIgnore] public bool SorszamSpecified;

        public string ErkeztetoKonyvKod; // 10
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class ErkeztetesAdatok
    {
        public string KezbesitesModja; // 10
        public string BeerkezesModja; // 10
        public string Megjegyzes; // 400
        public string Ragszam; // 50
        public string Cimzett; // 100

        public DateTime BeerkezesIdopontja;
        [XmlIgnore] public bool BeerkezesIdopontjaSpecified;

        public DateTime FeladasIdopontja;
        [XmlIgnore] public bool FeladasIdopontjaSpecified;

        public DateTime ErkeztetesDatuma;
        [XmlIgnore] public bool ErkeztetesDatumaSpecified;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class BontasAdatok
    {
        public DateTime BontasDatuma;
        [XmlIgnore] public bool BontasDatumaSpecified;

        public string KuldemenyTargya; // 400

        public int Surgosseg;
        [XmlIgnore] public bool SurgossegSpecified;

        public string UgyintezesModja; // 10
        public Partner Bekuldo;
        public string BontoFelhasznalo; // 100
        public string HivatkozasiSzam; // 400
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class IratAdatok
    {
        [XmlElement(Order = 0)]
        public HAIRAdatok HAIRAdatok;
        [XmlElement(Order = 1)]
        public IktatoszamAlszam Iktatoszam;
        [XmlElement(Order = 2)]
        public FoszamAdatok FoszamAdatok;
        [XmlElement(Order = 3)]
        public AlszamAdatok AlszamAdatok;
        [XmlElement(Order = 4)]
        public HatosagiStatisztikaAdatok HatosagiStatisztikaAdatok;
        [XmlElement(Order = 5)]
        public IratpeldanyAdatok IratpeldanyAdatok;
        [XmlElement(Order = 6)]
        public KiadmanyozasAdatok KiadmanyozasAdatok;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class FoszamAdatok
    {
        public DateTime Hatarido;
        [XmlIgnore] public bool HataridoSpecified;

        public string Targy; // 400
        public Partner Ugyfel;
        public string Ugytipus; // 10
        public string Felelos; // 100
        public string Ugyintezo; // 100
        public IrattariTetel IrattariTetel;

        public bool Lezarhato;
        [XmlIgnore] public bool LezarhatoSpecified;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class AlszamAdatok
    {
        public string IratIranya; // 10
        public string Targy; // 400
        public string Ugyintezo; // 100
        public string Irattipus; // 10
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class KiadmanyozasAdatok
    {
        public string Alairo; // 100
        public DateTime AlairasDatum;
        [XmlIgnore] public bool AlairasDatumSpecified;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class AtiktatasAdatok
    {
        public IktatoszamAlszam Iktatoszam;
    }

    #endregion

    #region Result

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valaszok
    {
        [XmlElement(Order = 0)]
        public Valasz[] Valasz;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valasz
    {
        [XmlElement(Order = 0)]
        public string KulsoAzonosito;

        [XmlElement(Order = 1)]
        public ErkeztetoSzam ErkeztetoSzam;

        [XmlElement(Order = 2)]
        public IktatoszamSorszam Iktatoszam;

        [XmlElement(Order = 3)]
        public KuldemenyVonalkodok Vonalkodok;

        [XmlElement(Order = 4)]
        public ValaszAdatok ValaszAdatok;
    }

    #endregion
}

