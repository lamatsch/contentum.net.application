﻿using System;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR.InitialSync
{
    public static class Constants
    {
        public const string NAMESPACE = "Contentum.eIntegrator.WebService.HAIR.InitialSync";
    }

    #region Parameters

    #endregion

    #region Result

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valaszok
    {
        [XmlElement(Order = 0)]
        public Valasz[] Valasz;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valasz : HAIR.Valasz
    {
    }

    #endregion
}

