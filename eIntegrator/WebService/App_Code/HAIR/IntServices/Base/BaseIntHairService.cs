﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;

namespace Contentum.eIntegrator.WebService.HAIR.IntServices
{
    public partial class BaseIntHairService
    {
        #region PROPERTIES
        public virtual string TypeFullName { get; set; }
        public virtual EnumHAIRServiceType ServiceType { get; set; }

        public string TechnikaiUser { get; set; }
        public string TechnikaiUserSzervezet { get; set; }
        public ExecParam ExecParamForServices { get; set; }
        #endregion

        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        public BaseIntHairService()
        {
            TypeFullName = this.GetType().FullName;
            ServiceType = EnumHAIRServiceType.ALAPERTELMEZETT;

            TechnikaiUser = Helper.TECHNIKAI_USER;
            TechnikaiUserSzervezet = Helper.TECHNIKAI_USER_SZERVEZET;

            ExecParamForServices = new ExecParam()
            {
                Felhasznalo_Id = TechnikaiUser,
                LoginUser_Id = TechnikaiUserSzervezet,
                FelhasznaloSzervezet_Id = TechnikaiUserSzervezet,
            };
        }

        #region LOG
        /// <summary>
        /// LOG
        /// </summary>
        /// <param name="exception"></param>
        public void Log(Exception exception)
        {
            Logger.Debug(string.Format("SOURCE:{0}", TypeFullName), exception);
        }
        /// <summary>
        /// LOG
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void Log(string message, Exception exception)
        {
            Logger.Debug(string.Format("SOURCE:{0} MSG:{1}", TypeFullName, message), exception);
        }
        /// <summary>
        /// LOG
        /// </summary>
        /// <param name="message"></param>
        /// <param name="execParam"></param>
        /// <param name="result"></param>
        public void Log(string message, ExecParam execParam, Result result)
        {
            Logger.Debug(string.Format("SOURCE:{0} MSG{1}", TypeFullName, message), execParam, result);
        }
        /// <summary>
        /// LOG
        /// </summary>
        /// <param name="message"></param>
        /// <param name="result"></param>
        public void Log(string message, Result result)
        {
            Logger.Debug(string.Format("SOURCE:{0} MSG{1}", TypeFullName, message), this.ExecParamForServices, result);
        }
        /// <summary>
        /// LOG
        /// </summary>
        /// <param name="result"></param>
        public void Log(Result result)
        {
            Logger.Debug(string.Format("SOURCE:{0}", TypeFullName), this.ExecParamForServices, result);
        }
        #endregion
        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}", ServiceType.ToString());
        }
    }
}