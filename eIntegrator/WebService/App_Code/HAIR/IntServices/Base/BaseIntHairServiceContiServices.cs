﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eDocument.Service;
using Contentum.eUtility;
using System;

namespace Contentum.eIntegrator.WebService.HAIR.IntServices
{
    public partial class BaseIntHairService
    {
        #region CONTI SERVICES

        #region GET

        /// <summary>
        /// GetIrat
        /// </summary>
        /// <param name="iktatoSzam"></param>
        /// <returns></returns>
        public Result GetIrat(IktatoszamAlszam iktatoSzam)
        {
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam xpm = ExecParamForServices.Clone();
            EREC_IraIratokSearch search = new EREC_IraIratokSearch();
            search.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
            search.Extended_EREC_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();

            if (search.Extended_EREC_IraIktatoKonyvekSearch == null)
            {
                search.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
            }

            if (search.Extended_EREC_UgyUgyiratdarabokSearch == null)
            {
                search.Extended_EREC_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();
            }

            if (iktatoSzam.Ev != 0)
            {
                search.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value = iktatoSzam.Ev.ToString();
                search.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.equals;
            }

            if (!string.IsNullOrEmpty(iktatoSzam.Elotag))
            {
                search.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Value = iktatoSzam.Elotag.ToString();
                search.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Operator = Query.Operators.equals;
            }

            if (iktatoSzam.Foszam != 0)
            {
                search.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Value = iktatoSzam.Foszam.ToString();
                search.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Operator = Query.Operators.equals;
            }

            if (iktatoSzam.Alszam != 0)
            {
                search.Alszam.Value = iktatoSzam.Alszam.ToString();
                search.Alszam.Operator = Query.Operators.equals;
            }

            search.Extended_EREC_IraIktatoKonyvekSearch.IktatoErkezteto.Value = IktatoErkezteto.Iktato;
            search.Extended_EREC_IraIktatoKonyvekSearch.IktatoErkezteto.Operator = Query.Operators.equals;

            search.TopRow = 1;

            Result result = service.GetAllWithExtension(xpm, search);
            return result;
        }

        public Result GetHAIRDokumentum(string HAIRToken)
        {
            eDocument.Service.KRT_DokumentumokService service = Contentum.eUtility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

            KRT_DokumentumokSearch search = new KRT_DokumentumokSearch();

            search.External_Source.Value = "HAIR";
            search.External_Source.Operator = Query.Operators.equals;
            search.External_Link.Value = HAIRToken;
            search.External_Link.Operator = Query.Operators.equals;

            search.TopRow = 1;

            Result result = service.GetAll(Helper.GlobalExecParam, search);
            return result;
        }

        public Result GetCsatolmany(string IratId, string DokumentumId)
        {
            EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();

            search.IraIrat_Id.Value = IratId;
            search.IraIrat_Id.Operator = Query.Operators.equals;
            search.Dokumentum_Id.Value = DokumentumId;
            search.Dokumentum_Id.Operator = Query.Operators.equals;

            search.TopRow = 1;

            Result result = service.GetAll(Helper.GlobalExecParam, search);
            return result;
        }

        /// <summary>
        /// GetUgyIrat
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EREC_UgyUgyiratok GetUgyIrat(string id)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam xpm = ExecParamForServices.Clone();
            xpm.Record_Id = id;

            Result result = service.Get(xpm);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            EREC_UgyUgyiratok item = (EREC_UgyUgyiratok)result.Record;
            item.Updated.SetValueAll(false);
            item.Base.Updated.SetValueAll(false);
            item.Base.Updated.Ver = true;

            return item;
        }

        /// <summary>
        /// GetIktatokonyv
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EREC_IraIktatoKonyvek GetIktatoKonyv(string id)
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam xpm = ExecParamForServices.Clone();
            xpm.Record_Id = id;

            Result result = service.Get(xpm);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            EREC_IraIktatoKonyvek item = (EREC_IraIktatoKonyvek)result.Record;
            item.Updated.SetValueAll(false);
            item.Base.Updated.SetValueAll(false);
            item.Base.Updated.Ver = true;

            return item;
        }

        /// <summary>
        /// GetCsoportByUserNev
        /// </summary>
        /// <param name="userNev"></param>
        /// <returns></returns>
        public Result GetCsoportByUserNev(string userNev)
        {
            eAdmin.Service.KRT_CsoportokService _KRT_CsoportokService = eAdminService.ServiceFactory.GetKRT_CsoportokService();
            ExecParam ExecParam = ExecParamForServices.Clone();
            KRT_CsoportokSearch search = new KRT_CsoportokSearch();
            search.Kod.Value = userNev;
            search.Kod.Operator = Query.Operators.equals;

            search.ErvKezd.Value = Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Query.Operators.and;

            search.ErvVege.Value = Query.SQLFunction.getdate;
            search.ErvVege.Operator = Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Query.Operators.and;

            Result result = _KRT_CsoportokService.GetAll(ExecParam, search);
            return result;
        }

        /// <summary>
        /// GetPartnerByKulsoAzonosito
        /// </summary>
        /// <param name="kulsoAzonosito"></param>
        /// <returns></returns>
        public Result GetPartnerByKulsoAzonosito(string kulsoAzonosito)
        {
            KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = ExecParamForServices.Clone();
            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch
            {
                OrderBy = null
            };

            partnerekSearch.KulsoAzonositok.Value = kulsoAzonosito;
            partnerekSearch.KulsoAzonositok.Operator = Query.Operators.equals;

            partnerekSearch.ErvKezd.Value = Query.SQLFunction.getdate;
            partnerekSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            partnerekSearch.ErvKezd.Group = "100";
            partnerekSearch.ErvKezd.GroupOperator = Query.Operators.and;

            partnerekSearch.ErvVege.Value = Query.SQLFunction.getdate;
            partnerekSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            partnerekSearch.ErvVege.Group = "100";
            partnerekSearch.ErvVege.GroupOperator = Query.Operators.and;

            Result partnerekResult = partnerService.GetAll(execParam, partnerekSearch);

            return partnerekResult;
        }

        /// <summary>
        /// GetCimByKulsoAzonosito
        /// </summary>
        /// <param name="kulsoAzonosito"></param>
        /// <returns></returns>
        public Result GetCimByKulsoAzonosito(string kulsoAzonosito)
        {
            KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = ExecParamForServices.Clone();

            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch
            {
                OrderBy = null
            };

            partnerekSearch.KulsoAzonositok.Value = kulsoAzonosito;
            partnerekSearch.KulsoAzonositok.Operator = Query.Operators.equals;

            partnerekSearch.ErvKezd.Value = Query.SQLFunction.getdate;
            partnerekSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            partnerekSearch.ErvKezd.Group = "100";
            partnerekSearch.ErvKezd.GroupOperator = Query.Operators.and;

            partnerekSearch.ErvVege.Value = Query.SQLFunction.getdate;
            partnerekSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            partnerekSearch.ErvVege.Group = "100";
            partnerekSearch.ErvVege.GroupOperator = Query.Operators.and;

            Result partnerekResult = partnerService.GetAll(execParam, partnerekSearch);
            return partnerekResult;
        }

        /// <summary>
        /// GetFelhasznaloByUserNev
        /// </summary>
        /// <param name="userNev"></param>
        /// <returns></returns>
        public Result GetFelhasznaloByUserNev(string userNev)
        {
            KRT_FelhasznalokService _KRT_FelhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            ExecParam ExecParam = ExecParamForServices.Clone();
            KRT_FelhasznalokSearch search = new KRT_FelhasznalokSearch();
            search.UserNev.Value = userNev;
            search.UserNev.Operator = Query.Operators.equals;

            search.ErvKezd.Value = Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Query.Operators.and;

            search.ErvVege.Value = Query.SQLFunction.getdate;
            search.ErvVege.Operator = Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Query.Operators.and;

            Result res = _KRT_FelhasznalokService.GetAll(ExecParam, search);

            return res;
        }

        /// <summary>
        /// GetIratPeldany
        /// </summary>
        /// <param name="iktatoSzam"></param>
        /// <returns></returns>
        public Result GetIratPeldany(IktatoszamSorszam iktatoSzam)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam xpm = ExecParamForServices.Clone();
            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();

            if (search.Extended_EREC_IraIktatoKonyvekSearch == null)
            {
                search.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
            }

            if (search.Extended_EREC_UgyUgyiratdarabokSearch == null)
            {
                search.Extended_EREC_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();
            }

            if (search.Extended_EREC_IraIratokSearch == null)
            {
                search.Extended_EREC_IraIratokSearch = new EREC_IraIratokSearch();
            }

            if (iktatoSzam.Ev != 0)
            {
                search.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value = iktatoSzam.Ev.ToString();
                search.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.equals;
            }

            if (!string.IsNullOrEmpty(iktatoSzam.Elotag))
            {
                search.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Value = iktatoSzam.Elotag.ToString();
                search.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Operator = Query.Operators.equals;
            }

            if (iktatoSzam.Foszam != 0)
            {
                search.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Value = iktatoSzam.Foszam.ToString();
                search.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Operator = Query.Operators.equals;
            }

            if (iktatoSzam.Alszam != 0)
            {
                search.Extended_EREC_IraIratokSearch.Alszam.Value = iktatoSzam.Alszam.ToString();
                search.Extended_EREC_IraIratokSearch.Alszam.Operator = Query.Operators.equals;
            }

            search.Extended_EREC_IraIktatoKonyvekSearch.IktatoErkezteto.Value = IktatoErkezteto.Iktato;
            search.Extended_EREC_IraIktatoKonyvekSearch.IktatoErkezteto.Operator = Query.Operators.equals;

            if (iktatoSzam.Sorszam != 0)
            {
                search.Sorszam.Value = iktatoSzam.Sorszam.ToString();
                search.Sorszam.Operator = Query.Operators.equals;
            }

            search.TopRow = 1;

            Result result = service.GetAllWithExtension(xpm, search);
            return result;
        }

        /// <summary>
        /// GetKuldemeny
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EREC_KuldKuldemenyek GetKuldemeny(string id)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam xpm = ExecParamForServices.Clone();
            xpm.Record_Id = id;

            Result result = service.Get(xpm);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            EREC_KuldKuldemenyek item = (EREC_KuldKuldemenyek)result.Record;
            item.Updated.SetValueAll(false);
            item.Base.Updated.SetValueAll(false);
            item.Base.Updated.Ver = true;

            return item;
        }

        /// <summary>
        /// GetPostaKonyv
        /// </summary>
        /// <param name="megkulJelzes"></param>
        /// <returns></returns>
        public Result GetPostaKonyv(string megkulJelzes)
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam xpm = ExecParamForServices.Clone();
            EREC_IraIktatoKonyvekSearch src = new EREC_IraIktatoKonyvekSearch();
            src.Ev.Value = DateTime.Now.Year.ToString();
            src.Ev.Operator = Query.Operators.equals;

            src.MegkulJelzes.Value = megkulJelzes;
            src.MegkulJelzes.Operator = Query.Operators.equals;

            src.IktatoErkezteto.Value = "P";
            src.IktatoErkezteto.Operator = Query.Operators.equals;
            src.TopRow = 1;

            Result result = service.GetAll(xpm, src);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            if (result.Ds.Tables[0].Rows.Count < 1)
            {
                throw new Exception(string.Format("Nem található postakonyv {0}", megkulJelzes));
            }
            return result;
        }

        /// <summary>
        /// GetIratPeldany
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EREC_PldIratPeldanyok GetIratPeldany(string id)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam xpm = ExecParamForServices.Clone();
            xpm.Record_Id = id;

            Result result = service.Get(xpm);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            EREC_PldIratPeldanyok item = (EREC_PldIratPeldanyok)result.Record;
            item.Updated.SetValueAll(false);
            item.Base.Updated.SetValueAll(false);
            item.Base.Updated.Ver = true;

            return item;
        }

        #endregion GET

        #region INSERT

        /// <summary>
        /// InsertPldIratPeldanyok
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Result InsertPldIratPeldanyok(EREC_PldIratPeldanyok item)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam xpm = ExecParamForServices.Clone();

            Result result = service.Insert(xpm, item);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            return result;
        }

        /// <summary>
        /// UpdatePldIratPeldanyok
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Result UpdatePldIratPeldanyok(EREC_PldIratPeldanyok item)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam xpm = ExecParamForServices.Clone();
            xpm.Record_Id = item.Id;
            Result result = service.Update(xpm, item);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            return result;
        }
        /// <summary>
        /// AddNewPldIratPeldanyToIraIrat
        /// </summary>
        /// <param name="item"></param>
        /// <param name="iratId"></param>
        /// <returns></returns>
        public Result AddNewPldIratPeldanyToIraIrat(EREC_PldIratPeldanyok item, string iratId)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam xpm = ExecParamForServices.Clone();

            Result result = service.AddNewPldIratPeldanyToIraIrat(xpm, item, iratId);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            return result;
        }

        #endregion INSERT

        #region BARCODE

        /// <summary>
        /// GenerateBarCode
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        public Result GenerateBarCode(out string barcode)
        {
            string vonalkodkezeles = Rendszerparameterek.Get(ExecParamForServices.Clone(), Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim();
            barcode = string.Empty;
            //KRT_BarkodSavokService service = eRecordService.ServiceFactory.GetKRT_BarkodSavokService();
            eRecord.Service.KRT_BarkodokService service = eRecordService.ServiceFactory.GetKRT_BarkodokService();
            ExecParam execParam = ExecParamForServices.Clone();
            Result result = null;

            if (!vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
            {
                result = service.BarkodSav_Igenyles(execParam, "G", 1);

                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
                barcode = (string)result.Record;
            }
            return result;
        }

        /// <summary>
        /// BarkodBindToIratPeldany
        /// </summary>
        /// <param name="pldIratId"></param>
        /// <param name="iratPeldanyBarCode"></param>
        /// <param name="barcodeId"></param>
        public void BarkodBindToIratPeldany(string pldIratId, string iratPeldanyBarCode, string barcodeId)
        {
            string vonalkodkezeles = Rendszerparameterek.Get(ExecParamForServices.Clone(), Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim();
            eRecord.Service.KRT_BarkodokService service = eRecordService.ServiceFactory.GetKRT_BarkodokService();
            ExecParam execParam = ExecParamForServices.Clone();
            Result resultBindBarcode = null;
            if ((!string.IsNullOrEmpty(iratPeldanyBarCode)
                         && !string.IsNullOrEmpty(barcodeId))
                         && !vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
            {
                resultBindBarcode = service.BarCodeBindToObject(execParam, barcodeId, pldIratId, Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok);
            }
            else if ((!string.IsNullOrEmpty(iratPeldanyBarCode)
                && vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO")))
            {
                resultBindBarcode = service.BarCodeBindToObject(execParam, barcodeId, pldIratId, Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok);
                if (!string.IsNullOrEmpty(resultBindBarcode.ErrorCode))
                {
                    // hiba
                    Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", execParam);
                    throw new ResultException(resultBindBarcode);
                }
            }
        }

        #endregion BARCODE

        /// <summary>
        /// Expedial
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Result Expedial(EREC_PldIratPeldanyok item)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam xpm = ExecParamForServices.Clone();
            xpm.Record_Id = item.Id;
            Result result = service.Expedialas(xpm, new string[] { item.Id }, string.Empty, item, null);
            return result;
        }

        /// <summary>
        /// Postaz
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Result Postaz(EREC_PldIratPeldanyok item)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam xpm = ExecParamForServices.Clone();
            xpm.Record_Id = item.Id;
            Result result = service.Postazas(xpm, item.Id);
            return result;
        }

        /// <summary>
        /// Postaz
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Result Postaz(EREC_KuldKuldemenyek item)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam xpm = ExecParamForServices.Clone();
            xpm.Record_Id = item.Id;
            Result result = service.Postazas(xpm, item, string.Empty);
            return result;
        }

        #endregion CONTI SERVICES
    }
}