﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.HAIR.IratPeldanyLetrehozas;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services.Protocols;

namespace Contentum.eIntegrator.WebService.HAIR.IntServices
{
    public class IntServiceIratPeldanyLetrehozas : BaseIntHairService
    {
        public IntServiceIratPeldanyLetrehozas()
        {
            base.ServiceType = EnumHAIRServiceType.IRAT_PELDANY_LETREHOZAS;
        }

        public IratPeldanyLetrehozas.Valaszok IratpeldanytLetrehoz(IEnumerable<UjIratpeldany> UjIratpeldanyok)
        {
            if (UjIratpeldanyok == null || !UjIratpeldanyok.Any())
            {
                throw new SoapException(Helper.Messages.SoapRequestError, SoapException.ClientFaultCode);
            }
            IratPeldanyLetrehozas.Valaszok valasz = null;
            try
            {
                InitValaszok(UjIratpeldanyok, ref valasz);
                if (!Core(UjIratpeldanyok, ref valasz))
                {
                    return valasz;
                }

                return valasz;
            }
            catch (Exception exc)
            {
                base.Log(exc);
                return GetValaszError(null, exc);
            }
        }

        #region CORE

        /// <summary>
        /// CheckIktatoszam - Find Irat by Iktatoszam
        /// </summary>
        /// <param name="UjIratpeldanyok"></param>
        /// <param name="valaszok"></param>
        /// <returns></returns>
        private bool Core(IEnumerable<UjIratpeldany> UjIratpeldanyok, ref IratPeldanyLetrehozas.Valaszok valaszok)
        {
            if (UjIratpeldanyok == null || !UjIratpeldanyok.Any())
            {
                base.Log(new ArgumentNullException(ErrorCodes.GetErrorMessage(ErrorCodes.ERROR_PARAM_NULL_UJIRATPELDANY)));
                valaszok = GetValaszError(null, ErrorCodes.ERROR_PARAM_NULL_UJIRATPELDANY);
                return false;
            }

            if (UjIratpeldanyok.Any(a => a.Iktatoszam == null
                                      || a.Iktatoszam.Ev == 0
                                      || a.Iktatoszam.Foszam == 0
                                      || a.Iktatoszam.Alszam == 0
                                   ))
            {
                base.Log(new ArgumentNullException(ErrorCodes.GetErrorMessage(ErrorCodes.ERROR_PARAM_NULL_UJIRATPELDANY_IKTATOSZAMALSZAM)));
                valaszok = GetValaszError(null, ErrorCodes.ERROR_PARAM_NULL_UJIRATPELDANY_IKTATOSZAMALSZAM);
                return false;
            }

            #region ITEM VARIABLES

            Result resultFindIrat;
            Result resultGenerateBarCode;
            Result result_IratPeldanyInsert;
            EREC_IraIratok tmpItemIrat = new EREC_IraIratok();
            EREC_UgyUgyiratok tmpItemUgyIrat = null;
            EREC_IraIktatoKonyvek tmpItemIktatoKonyv = null;
            EREC_PldIratPeldanyok tmpItemPldIrat = null;
            ValaszAdatok tmpItemValaszAdat = null;

            #endregion ITEM VARIABLES

            for (int i = 0; i < UjIratpeldanyok.Count(); i++)
            {
                resultFindIrat = base.GetIrat(UjIratpeldanyok.ElementAt(i).Iktatoszam);
                if (resultFindIrat.IsError || resultFindIrat.Ds.Tables[0].Rows.Count < 1)
                {
                    base.Log(resultFindIrat);
                    valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(ErrorCodes.ERROR_PARAM_NULL_UJIRATPELDANY_IKTATOSZAMALSZAM);
                    continue;
                }

                Utils.LoadBusinessDocumentFromDataRow(tmpItemIrat, resultFindIrat.Ds.Tables[0].Rows[0]);

                tmpItemPldIrat = GeneratePldIratPeldanyFromHair(UjIratpeldanyok.ElementAt(i), ref tmpItemValaszAdat);
                if (tmpItemPldIrat == null)
                {
                    valaszok.Valasz[i].ValaszAdatok = tmpItemValaszAdat;
                    continue;
                }

                #region SET IRAT ID

                tmpItemPldIrat.IraIrat_Id = tmpItemIrat.Id;
                tmpItemPldIrat.Updated.IraIrat_Id = true;

                #endregion SET IRAT ID

                #region SET SORSZAM

                int utolsoPldIratSorszam;
                try
                {
                    utolsoPldIratSorszam = GetUtolsoIratPeldanySorszam(tmpItemIrat);
                }
                catch (Exception excS)
                {
                    base.Log(excS);
                    valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(excS);
                    continue;
                }

                ++utolsoPldIratSorszam;
                tmpItemPldIrat.Sorszam = utolsoPldIratSorszam.ToString();
                tmpItemPldIrat.Updated.Sorszam = true;

                valaszok.Valasz[i].Iktatoszam.Sorszam = utolsoPldIratSorszam;
                valaszok.Valasz[i].Iktatoszam.SorszamSpecified = true;

                #endregion SET SORSZAM

                #region ALLAPOT

                tmpItemPldIrat.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                tmpItemPldIrat.Updated.Allapot = true;

                #endregion ALLAPOT

                #region BARCODE GENERATE

                try
                {
                    string generaltVonalkod;
                    resultGenerateBarCode = base.GenerateBarCode(out generaltVonalkod);
                    if (!string.IsNullOrEmpty(generaltVonalkod))
                    {
                        tmpItemPldIrat.BarCode = generaltVonalkod;
                        tmpItemPldIrat.Updated.BarCode = true;
                    }
                }
                catch (Exception excV)
                {
                    base.Log(excV);
                    valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(excV);
                    continue;
                }

                #endregion BARCODE GENERATE

                #region AZONOSITO

                tmpItemUgyIrat = GetUgyIrat(tmpItemIrat.Ugyirat_Id);
                tmpItemIktatoKonyv = GetIktatoKonyv(tmpItemUgyIrat.IraIktatokonyv_Id);
                tmpItemPldIrat.Azonosito = eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(base.ExecParamForServices, tmpItemIktatoKonyv, tmpItemUgyIrat, tmpItemIrat, tmpItemPldIrat);
                tmpItemPldIrat.Updated.Azonosito = true;

                #endregion AZONOSITO

                #region INSERT PLDIRAT
                /*
                try
                {
                    result_IratPeldanyInsert = InsertPldIratPeldanyok(tmpItemPldIrat);
                    result_IratPeldanyInsert.CheckError();
                }
                catch (Exception excI)
                {
                    base.Log(excI);
                    valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(excI);
                    continue;
                }
                
                #endregion INSERT PLDIRAT
                
                #region ASSIGN PLDIRAT 2 IRAT
                */

                Result result_AssignIratPeldanyToIrat;
                try
                {
                    result_AssignIratPeldanyToIrat = base.AddNewPldIratPeldanyToIraIrat(tmpItemPldIrat, tmpItemIrat.Id);
                    result_AssignIratPeldanyToIrat.CheckError();
                }
                catch (Exception excA)
                {
                    base.Log(excA);
                    valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(excA);
                    continue;
                }

                #endregion INSERT PLDIRAT

                #region BIND VONALKOD 2 PLDIRAT

                try
                {
                    base.BarkodBindToIratPeldany(result_AssignIratPeldanyToIrat.Uid, tmpItemPldIrat.BarCode, resultGenerateBarCode.Uid);
                }
                catch (Exception excB)
                {
                    base.Log(excB);
                    valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(excB);
                    continue;
                }

                #endregion BIND VONALKOD 2 PLDIRAT
            }

            return true;
        }

        #endregion CORE

        #region VALASZ

        /// <summary>
        /// GetValaszError
        /// </summary>
        /// <param name="Iktatoszam"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public IratPeldanyLetrehozas.Valaszok GetValaszError(IktatoszamAlszam Iktatoszam, int errorCode)
        {
            IratPeldanyLetrehozas.Valaszok valaszok = new IratPeldanyLetrehozas.Valaszok()
            {
                Valasz = new IratPeldanyLetrehozas.Valasz[1]
            };
            if (valaszok.Valasz[0] == null)
            {
                valaszok.Valasz[0] = new IratPeldanyLetrehozas.Valasz();
            }
            valaszok.Valasz[0].ValaszAdatok = GetValaszAdatError(errorCode);
            valaszok.Valasz[0].Iktatoszam = Convert(Iktatoszam);
            return valaszok;
        }

        /// <summary>
        /// GetValaszError
        /// </summary>
        /// <param name="Iktatoszam"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public IratPeldanyLetrehozas.Valaszok GetValaszError(IktatoszamAlszam Iktatoszam, Result result)
        {
            IratPeldanyLetrehozas.Valaszok valaszok = new IratPeldanyLetrehozas.Valaszok()
            {
                Valasz = new IratPeldanyLetrehozas.Valasz[1]
            };
            if (valaszok.Valasz[0] == null)
            {
                valaszok.Valasz[0] = new IratPeldanyLetrehozas.Valasz();
            }
            valaszok.Valasz[0].ValaszAdatok = GetValaszAdatError(result);
            valaszok.Valasz[0].Iktatoszam = Convert(Iktatoszam);
            return valaszok;
        }

        /// <summary>
        /// GetValaszError
        /// </summary>
        /// <param name="Iktatoszam"></param>
        /// <param name="exc"></param>
        /// <returns></returns>
        public IratPeldanyLetrehozas.Valaszok GetValaszError(IktatoszamAlszam Iktatoszam, Exception exc)
        {
            IratPeldanyLetrehozas.Valaszok valaszok = new IratPeldanyLetrehozas.Valaszok()
            {
                Valasz = new IratPeldanyLetrehozas.Valasz[1]
            };
            if (valaszok.Valasz[0] == null)
            {
                valaszok.Valasz[0] = new IratPeldanyLetrehozas.Valasz();
            }
            valaszok.Valasz[0].ValaszAdatok = GetValaszAdatError(exc);
            valaszok.Valasz[0].Iktatoszam = Convert(Iktatoszam);
            return valaszok;
        }

        public ValaszAdatok GetValaszAdatError(ResultException exc)
        {
            int eCode = -1000;
            int.TryParse(exc.ErrorCode, out eCode);

            return new ValaszAdatok()
            {
                HibaKod = eCode,
                HibaUzenet = exc.Message + (exc.InnerException != null ? " " + exc.InnerException.Message : string.Empty),
            };
        }

        public ValaszAdatok GetValaszAdatError(ResultException exc, ErrorDetails details)
        {
            int eCode = -1000;
            int.TryParse(exc.ErrorCode, out eCode);

            return new ValaszAdatok()
            {
                HibaKod = eCode,
                HibaUzenet = exc.Message + (details != null ? " " + details.Message : string.Empty),
            };
        }

        public ValaszAdatok GetValaszAdatError(Exception exc)
        {
            return new ValaszAdatok()
            {
                HibaKod = -1000,
                HibaUzenet = exc.Message + (exc.InnerException != null ? " " + exc.InnerException.Message : string.Empty),
            };
        }

        public ValaszAdatok GetValaszAdatError(Result result)
        {
            int code = 0;
            int.TryParse(result.ErrorCode, out code);
            return new ValaszAdatok()
            {
                HibaKod = code,
                HibaUzenet = result.ErrorMessage
            };
        }

        public ValaszAdatok GetValaszAdatError(int errorCode)
        {
            return new ValaszAdatok()
            {
                HibaKod = errorCode,
                HibaUzenet = ErrorCodes.GetErrorMessage(errorCode)
            };
        }

        public IratPeldanyLetrehozas.Valasz GetValaszError(ValaszAdatok valaszAdatok)
        {
            return new IratPeldanyLetrehozas.Valasz()
            {
                ValaszAdatok = valaszAdatok
            };
        }

        #endregion VALASZ

        #region INIT

        /// <summary>
        /// InitValaszok
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private void InitValaszok(IEnumerable<UjIratpeldany> UjIratpeldanyok, ref IratPeldanyLetrehozas.Valaszok valaszok)
        {
            valaszok = new IratPeldanyLetrehozas.Valaszok();
            valaszok.Valasz = new IratPeldanyLetrehozas.Valasz[UjIratpeldanyok.Count()];

            for (int i = 0; i < UjIratpeldanyok.Count(); i++)
            {
                valaszok.Valasz[i] = new IratPeldanyLetrehozas.Valasz()
                {
                    Iktatoszam = Convert(UjIratpeldanyok.ElementAt(i).Iktatoszam),
                    ValaszAdatok = new ValaszAdatok(),
                };
            }
        }

        #endregion INIT

        #region HELPER

        /// <summary>
        /// Convert Iktatoszam -> IktatoszamSorszam
        /// </summary>
        /// <param name="Iktatoszam"></param>
        /// <returns></returns>
        public IktatoszamSorszam Convert(IktatoszamAlszam Iktatoszam)
        {
            if (Iktatoszam == null)
            {
                return null;
            }

            IktatoszamSorszam result = new IktatoszamSorszam();
            result.Alszam = Iktatoszam.Alszam;
            result.AlszamSpecified = true;
            result.Elotag = Iktatoszam.Elotag;
            result.Ev = Iktatoszam.Ev;
            result.EvSpecified = true;
            result.Foszam = Iktatoszam.Foszam;
            result.FoszamSpecified = true;
            return result;
        }

        /// <summary>
        /// GeneratePldIratPeldanyFromHair
        /// </summary>
        /// <param name="UjIratpeldany"></param>
        /// <param name="valaszAdat"></param>
        /// <returns></returns>
        public EREC_PldIratPeldanyok GeneratePldIratPeldanyFromHair(UjIratpeldany UjIratpeldany, ref ValaszAdatok valaszAdat)
        {
            if (valaszAdat == null)
            {
                valaszAdat = new ValaszAdatok();
            }
            EREC_PldIratPeldanyok iratPeldany = new EREC_PldIratPeldanyok();
            iratPeldany.Updated.SetValueAll(false);
            iratPeldany.Base.Updated.SetValueAll(false);

            if (!string.IsNullOrEmpty(UjIratpeldany.IratpeldanyAdatok.KuldesModja))
            {
                iratPeldany.KuldesMod = UjIratpeldany.IratpeldanyAdatok.KuldesModja;
                iratPeldany.Updated.KuldesMod = true;
            }

            #region CSOPORT_ID_FELELOS

            if (!string.IsNullOrEmpty(UjIratpeldany.IratpeldanyAdatok.Felelos))
            {
                Result felhasznaloResult = GetFelhasznaloByUserNev(UjIratpeldany.IratpeldanyAdatok.Felelos);

                if (felhasznaloResult.IsError)
                {
                    base.Log(felhasznaloResult);
                    valaszAdat = GetValaszAdatError(felhasznaloResult);
                    return null;
                }
                else if (felhasznaloResult.Ds.Tables[0].Rows.Count > 0)
                {
                    iratPeldany.Csoport_Id_Felelos = felhasznaloResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                    iratPeldany.Updated.Csoport_Id_Felelos = true;

                    iratPeldany.FelhasznaloCsoport_Id_Orzo = iratPeldany.Csoport_Id_Felelos;
                    iratPeldany.Updated.FelhasznaloCsoport_Id_Orzo = true;
                }
                else
                {
                    Result csoportResult = base.GetCsoportByUserNev(UjIratpeldany.IratpeldanyAdatok.Felelos);
                    if (csoportResult.IsError)
                    {
                        base.Log(csoportResult);
                        valaszAdat = GetValaszAdatError(csoportResult);
                        return null;
                    }
                    else if (csoportResult.Ds.Tables[0].Rows.Count < 1)
                    {
                        base.Log(csoportResult);
                        valaszAdat = GetValaszAdatError(ErrorCodes.ERROR_NOT_FOUND_IRATPELDANYADATOK_FELELOS);
                        return null;
                    }
                    else
                    {
                        iratPeldany.Csoport_Id_Felelos = csoportResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                        iratPeldany.Updated.Csoport_Id_Felelos = true;

                        iratPeldany.FelhasznaloCsoport_Id_Orzo = iratPeldany.Csoport_Id_Felelos;
                        iratPeldany.Updated.FelhasznaloCsoport_Id_Orzo = true;
                    }
                }
            }

            #endregion CSOPORT_ID_FELELOS

            if (!string.IsNullOrEmpty(UjIratpeldany.IratpeldanyAdatok.UgyintezesModja))
            {
                iratPeldany.UgyintezesModja = UjIratpeldany.IratpeldanyAdatok.UgyintezesModja;
                iratPeldany.Updated.UgyintezesModja = true;
            }

            if (UjIratpeldany.IratpeldanyAdatok.Ugyfel != null)
            {
                if (!string.IsNullOrEmpty(UjIratpeldany.IratpeldanyAdatok.Ugyfel.PartnerAzonosito))
                {
                    Result partner = base.GetPartnerByKulsoAzonosito(UjIratpeldany.IratpeldanyAdatok.Ugyfel.PartnerAzonosito);
                    if (partner.IsError)
                    {
                        base.Log(partner);
                        valaszAdat = GetValaszAdatError(partner);
                        return null;
                    }
                    else if (!partner.IsError && partner.Ds.Tables[0].Rows.Count > 0)
                    {
                        iratPeldany.Partner_Id_Cimzett = partner.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                        iratPeldany.Updated.Partner_Id_Cimzett = true;
                    }
                }

                if (!string.IsNullOrEmpty(UjIratpeldany.IratpeldanyAdatok.Ugyfel.PartnerCimAzonosito))
                {
                    Result cim = base.GetCimByKulsoAzonosito(UjIratpeldany.IratpeldanyAdatok.Ugyfel.PartnerCimAzonosito);
                    if (cim.IsError)
                    {
                        base.Log(cim);
                        valaszAdat = GetValaszAdatError(cim);
                        return null;
                    }
                    else if (!cim.IsError && cim.Ds.Tables[0].Rows.Count > 0)
                    {
                        iratPeldany.Cim_id_Cimzett = cim.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                        iratPeldany.Updated.Cim_id_Cimzett = true;
                    }
                }

                if (!string.IsNullOrEmpty(UjIratpeldany.IratpeldanyAdatok.Ugyfel.PartnerNev))
                {
                    iratPeldany.NevSTR_Cimzett = UjIratpeldany.IratpeldanyAdatok.Ugyfel.PartnerNev;
                    iratPeldany.Updated.NevSTR_Cimzett = true;
                }

                if (!string.IsNullOrEmpty(UjIratpeldany.IratpeldanyAdatok.Ugyfel.PartnerCim))
                {
                    iratPeldany.CimSTR_Cimzett = UjIratpeldany.IratpeldanyAdatok.Ugyfel.PartnerCim;
                    iratPeldany.Updated.CimSTR_Cimzett = true;
                }
                iratPeldany.Eredet = KodTarak.IRAT_FAJTA.Eredeti;
                iratPeldany.Updated.Eredet = true;
            }

            return iratPeldany;
        }

        /// <summary>
        /// GetUtolsoIratPeldanySorszam
        /// </summary>
        /// <param name="irat"></param>
        /// <returns></returns>
        private int GetUtolsoIratPeldanySorszam(EREC_IraIratok irat)
        {
            int utolsoSorszam;
            if (int.TryParse(irat.UtolsoSorszam, out utolsoSorszam))
            {
                return utolsoSorszam;
            }
            else
            {
                throw new ResultException(52112);
            }
        }

        #endregion HELPER
    }
}