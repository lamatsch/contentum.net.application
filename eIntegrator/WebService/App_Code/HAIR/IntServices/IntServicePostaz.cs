﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services.Protocols;

namespace Contentum.eIntegrator.WebService.HAIR.IntServices
{
    public class IntServicePostaz : BaseIntHairService
    {
        public IntServicePostaz()
        {
            base.ServiceType = EnumHAIRServiceType.POSTAZAS;
        }

        public Postazas.Valaszok Postaz(IEnumerable<Postazas.Postazas> Postazasok)
        {
            if (Postazasok == null || !Postazasok.Any())
            {
                throw new SoapException(Helper.Messages.SoapRequestError, SoapException.ClientFaultCode);
            }
            Postazas.Valaszok valasz = null;
            try
            {
                InitValaszok(Postazasok, ref valasz);
                if (!Core(Postazasok, ref valasz))
                {
                    return valasz;
                }
                return valasz;
            }
            catch (Exception exc)
            {
                base.Log(exc);
                return GetValaszError(exc);
            }
        }

        #region FLOW

        private bool Core(IEnumerable<Postazas.Postazas> Postazasok, ref Postazas.Valaszok valaszok)
        {
            if (Postazasok == null || !Postazasok.Any())
            {
                base.Log(new ArgumentNullException(ErrorCodes.GetErrorMessage(ErrorCodes.ERROR_PARAM_NULL_POSTAZAS)));
                valaszok = GetValaszError(ErrorCodes.ERROR_PARAM_NULL_POSTAZAS);
                return false;
            }

            Result resultFindIratPeldany;
            Result resultExpedial;
            Result resultPostazas;
            EREC_PldIratPeldanyok actualItemIratPeldany = new EREC_PldIratPeldanyok();
            EREC_KuldKuldemenyek kuldemenyExp = null;
            for (int i = 0; i < Postazasok.Count(); i++)
            {
                #region GET

                resultFindIratPeldany = base.GetIratPeldany(Postazasok.ElementAt(i).IratAdatok.Iktatoszam);
                if (resultFindIratPeldany == null || resultFindIratPeldany.IsError || resultFindIratPeldany.Ds.Tables[0].Rows.Count < 1)
                {
                    base.Log(resultFindIratPeldany);
                    valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(ErrorCodes.ERROR_PARAM_NOTFOUND_POSTAZAS_IRATPELDANY);
                    continue;
                }

                #endregion GET

                #region OBJECT FROM DATAROW

                Utils.LoadBusinessDocumentFromDataRow(actualItemIratPeldany, resultFindIratPeldany.Ds.Tables[0].Rows[0]);

                #endregion OBJECT FROM DATAROW

                #region KULDESMOD UGYINTEZESMODJA

                if (actualItemIratPeldany.KuldesMod != KodTarak.KULDEMENY_KULDES_MODJA.HAIR || actualItemIratPeldany.UgyintezesModja != KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
                {
                    var refIratPeldanyStatusz = eRecord.BaseUtility.IratPeldanyok.GetAllapotByBusinessDocument(actualItemIratPeldany);
                    ErrorDetails errorDetail = null;
                    if (!eRecord.BaseUtility.IratPeldanyok.Modosithato(refIratPeldanyStatusz, base.ExecParamForServices, out errorDetail))
                    {
                        // Nem lehet az irathoz új iratpéldányt létrehozni
                        ResultException exc = new ResultException(52113, errorDetail);
                        base.Log(exc);
                        valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(exc, errorDetail);
                        continue;
                    }
                    else
                    {
                        SetIratPeldanyKuldesUgyintezesMod(actualItemIratPeldany);

                        Result updatePldIrat = base.UpdatePldIratPeldanyok(actualItemIratPeldany);
                        if (updatePldIrat.IsError)
                        {
                            base.Log(updatePldIrat);
                            valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(updatePldIrat);
                            continue;
                        }

                        if (updatePldIrat.Record is EREC_PldIratPeldanyok)
                        {
                            actualItemIratPeldany = updatePldIrat.Record as EREC_PldIratPeldanyok;
                        }
                        else
                        {
                            actualItemIratPeldany = base.GetIratPeldany(actualItemIratPeldany.Id);
                        }
                    }
                }

                #endregion KULDESMOD UGYINTEZESMODJA

                #region EXPEDIAL

                resultExpedial = base.Expedial(actualItemIratPeldany);
                if (resultExpedial.IsError)
                {
                    base.Log(resultExpedial);
                    valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(resultExpedial);
                    continue;
                }
                kuldemenyExp = base.GetKuldemeny(resultExpedial.Uid);

                #endregion EXPEDIAL

                #region PREPARE 2 POSTAZ

                PrepareKuldemenyDataToPostaz(ref kuldemenyExp, Postazasok.ElementAt(i));

                #endregion PREPARE 2 POSTAZ

                #region POSTAZ

                resultPostazas = base.Postaz(kuldemenyExp);
                if (resultPostazas.IsError)
                {
                    base.Log(resultPostazas);
                    valaszok.Valasz[i].ValaszAdatok = GetValaszAdatError(resultPostazas);
                    continue;
                }

                #endregion POSTAZ
            }
            return true;
        }

        /// <summary>
        /// SetIratPeldanyKuldesUgyintezesMod
        /// </summary>
        /// <param name="tmpItemIratPeldany"></param>
        private void SetIratPeldanyKuldesUgyintezesMod(EREC_PldIratPeldanyok tmpItemIratPeldany)
        {
            tmpItemIratPeldany.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.HAIR;
            tmpItemIratPeldany.Updated.KuldesMod = true;
            tmpItemIratPeldany.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            tmpItemIratPeldany.Updated.UgyintezesModja = true;
        }

        /// <summary>
        /// SetKuldemenyToPostaz
        /// </summary>
        /// <param name="item"></param>
        private void PrepareKuldemenyDataToPostaz(ref EREC_KuldKuldemenyek item, Postazas.Postazas postaAdatok)
        {
            item.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.HAIR;
            item.Updated.KuldesMod = true;

            item.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            item.Updated.UgyintezesModja = true;

            item.Tertiveveny = "1";
            item.Updated.Tertiveveny = true;

            #region FELELOS

            if (!string.IsNullOrEmpty(postaAdatok.PostazasAdatok.Felado))
            {
                Result felhasznaloResult = GetFelhasznaloByUserNev(postaAdatok.PostazasAdatok.Felado);

                if (felhasznaloResult.IsError)
                {
                    base.Log(felhasznaloResult);
                    throw new ResultException(felhasznaloResult);
                }
                else if (felhasznaloResult.Ds.Tables[0].Rows.Count > 0)
                {
                    item.Csoport_Id_Felelos = felhasznaloResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                    item.Updated.Csoport_Id_Felelos = true;
                }
                else
                {
                    Result csoportResult = base.GetCsoportByUserNev(postaAdatok.PostazasAdatok.Felado);
                    if (csoportResult.IsError)
                    {
                        base.Log(csoportResult);
                        throw new ResultException(csoportResult);
                    }
                    else if (csoportResult.Ds.Tables[0].Rows.Count < 1)
                    {
                        base.Log(csoportResult);
                        throw new Exception(ErrorCodes.GetErrorMessage(ErrorCodes.ERROR_NOT_FOUND_POSTAZAS_FELELOS));
                    }
                    else
                    {
                        item.Csoport_Id_Felelos = csoportResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                        item.Updated.Csoport_Id_Felelos = true;
                    }
                }
            }

            #endregion FELELOS

            #region POSTAZAS DATUMA

            if (postaAdatok.PostazasAdatok.PostazasDatuma != DateTime.MinValue)
            {
                item.BelyegzoDatuma = postaAdatok.PostazasAdatok.PostazasDatuma.ToString();
                item.Updated.BelyegzoDatuma = true;
            }
            else
            {
                item.BelyegzoDatuma = DateTime.Now.ToString();
                item.Updated.BelyegzoDatuma = true;
            }

            #endregion POSTAZAS DATUMA

            #region RAGSZAM

            if (!string.IsNullOrEmpty(postaAdatok.PostazasAdatok.KuldemenyAzonosito))
            {
                item.RagSzam = string.Format("HAIR_{0}", postaAdatok.PostazasAdatok.KuldemenyAzonosito);
                item.Updated.RagSzam = true;
            }

            #endregion RAGSZAM

            #region POSTAKONYV

            if (!string.IsNullOrEmpty(postaAdatok.PostazasAdatok.Postakonyv))
            {
                Result resulPostaKonyv = GetPostaKonyv(postaAdatok.PostazasAdatok.Postakonyv);
                if (resulPostaKonyv.IsError)
                {
                    throw new ResultException(resulPostaKonyv);
                }

                if (resulPostaKonyv.Ds.Tables[0].Rows.Count < 0)
                {
                    throw new Exception(ErrorCodes.GetErrorMessage(ErrorCodes.ERROR_PARAM_NULL_POSTAZAS_POSTAKONYV));
                }
                item.IraIktatokonyv_Id = resulPostaKonyv.Ds.Tables[0].Rows[0]["Id"].ToString();
                item.Updated.IraIktatokonyv_Id = true;
            }

            #endregion POSTAKONYV
        }

        #endregion FLOW

        #region VALASZ

        /// <summary>
        /// GetValaszError
        /// </summary>
        /// <param name="IktatoszamAlszam"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public Postazas.Valaszok GetValaszError(int errorCode)
        {
            Postazas.Valaszok valaszok = new Postazas.Valaszok()
            {
                Valasz = new Postazas.Valasz[1]
            };
            if (valaszok.Valasz[0] == null)
            {
                valaszok.Valasz[0] = new Postazas.Valasz();
            }
            valaszok.Valasz[0].ValaszAdatok = GetValaszAdatError(errorCode);
            return valaszok;
        }

        /// <summary>
        /// GetValaszError
        /// </summary>
        /// <param name="IktatoszamAlszam"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public Postazas.Valaszok GetValaszError(Result result)
        {
            Postazas.Valaszok valaszok = new Postazas.Valaszok()
            {
                Valasz = new Postazas.Valasz[1]
            };
            if (valaszok.Valasz[0] == null)
            {
                valaszok.Valasz[0] = new Postazas.Valasz();
            }
            valaszok.Valasz[0].ValaszAdatok = GetValaszAdatError(result);
            return valaszok;
        }

        /// <summary>
        /// GetValaszError
        /// </summary>
        /// <param name="IktatoszamAlszam"></param>
        /// <param name="exc"></param>
        /// <returns></returns>
        public Postazas.Valaszok GetValaszError(Exception exc)
        {
            Postazas.Valaszok valaszok = new Postazas.Valaszok()
            {
                Valasz = new Postazas.Valasz[1]
            };
            if (valaszok.Valasz[0] == null)
            {
                valaszok.Valasz[0] = new Postazas.Valasz();
            }
            valaszok.Valasz[0].ValaszAdatok = GetValaszAdatError(exc);
            return valaszok;
        }

        public ValaszAdatok GetValaszAdatError(ResultException exc)
        {
            int eCode = -1000;
            int.TryParse(exc.ErrorCode, out eCode);

            return new ValaszAdatok()
            {
                HibaKod = eCode,
                HibaUzenet = exc.Message + (exc.InnerException != null ? " " + exc.InnerException.Message : string.Empty),
            };

        }
        public ValaszAdatok GetValaszAdatError(ResultException exc, ErrorDetails details)
        {
            int eCode = -1000;
            int.TryParse(exc.ErrorCode, out eCode);

            return new ValaszAdatok()
            {
                HibaKod = eCode,
                HibaUzenet = exc.Message + (details != null ? " " + details.Message : string.Empty),
            };
        }
        public ValaszAdatok GetValaszAdatError(Exception exc)
        {
            return new ValaszAdatok()
            {
                HibaKod = -1000,
                HibaUzenet = exc.Message + (exc.InnerException != null ? " " + exc.InnerException.Message : string.Empty),
            };
        }

        public ValaszAdatok GetValaszAdatError(Result result)
        {
            int code = 0;
            int.TryParse(result.ErrorCode, out code);
            return new ValaszAdatok()
            {
                HibaKod = code,
                HibaUzenet = result.ErrorMessage
            };
        }

        public ValaszAdatok GetValaszAdatError(int errorCode)
        {
            return new ValaszAdatok()
            {
                HibaKod = errorCode,
                HibaUzenet = ErrorCodes.GetErrorMessage(errorCode)
            };
        }

        public Postazas.Valasz GetValaszError(ValaszAdatok valaszAdatok)
        {
            return new Postazas.Valasz()
            {
                ValaszAdatok = valaszAdatok
            };
        }

        #endregion VALASZ

        #region INIT

        /// <summary>
        /// InitValaszok
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        private void InitValaszok(IEnumerable<Postazas.Postazas> Postazasok, ref Postazas.Valaszok valaszok)
        {
            valaszok = new Postazas.Valaszok();
            valaszok.Valasz = new Postazas.Valasz[Postazasok.Count()];

            for (int i = 0; i < Postazasok.Count(); i++)
            {
                valaszok.Valasz[i] = new Postazas.Valasz()
                {
                    ValaszAdatok = new ValaszAdatok(),
                };
            }
        }

        #endregion INIT
    }
}