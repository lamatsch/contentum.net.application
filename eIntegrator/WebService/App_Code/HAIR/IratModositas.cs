﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR.IratModositas
{
    public static class Constants
    {
        public const string NAMESPACE = "Contentum.eIntegrator.WebService.HAIR.IratModositas";
    }

    // [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]    
    //[XmlType(Namespace = Constants.NAMESPACE)]
    //public class Iratok
    //{
    //    [XmlElement(Order = 0)]
    //    public Irat[] Irat;
    //}

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Irat
    {
        #region Parameters

        [XmlElement(Order = 0)]
        public Alapadatok Alapadatok;
        [XmlElement(Order = 1)]
        public IktatoszamAlszam Iktatoszam;
        [XmlElement(Order = 2)]
        public HAIRAdatok HAIRAdatok;
        [XmlElement(Order = 3)]
        public HatosagiStatisztikaAdatok HatosagiStatisztikaAdatok;

        #endregion

        #region Irat módosítás

        public void Modositas(ExecParam execParam, Valasz valasz)
        {
            var v = valasz.ValaszAdatok;
            try
            {
                // irat keresése iktatószám alapján
                var irat = Helper.GetIratByIktatoszam(Iktatoszam, v);
                if (irat == null)
                {
                    if (v.HibaKod == 0) // ha nincs megadva más hiba
                    {
                        Helper.SetError(v, ErrorCodes.IratNotFound);
                    }
                    return;
                }

                var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                // HivatkozasiSzam ellenőrzés/módosítás
                if (HAIRAdatok != null && !String.IsNullOrEmpty(HAIRAdatok.IratKulsoAzonosito))
                {
                    if (String.IsNullOrEmpty(irat.HivatkozasiSzam))
                    {
                        irat.HivatkozasiSzam = HAIRAdatok.IratKulsoAzonosito;
                        irat.Updated.HivatkozasiSzam = true;

                        var xp = execParam.Clone();
                        xp.Record_Id = irat.Id;
                        irat.Base.Updated.Ver = true;

                        var result = iratokService.Update(xp, irat);
                        if (result.IsError)
                        {
                            Helper.SetError(v, result);
                            return;
                        }
                    }
                    else if (irat.HivatkozasiSzam != HAIRAdatok.IratKulsoAzonosito)
                    {
                        Helper.SetError(v, ErrorCodes.KulsoAzonosito);
                        return;
                    }
                }

                // KezbesitesErtesitesSzukseges
                if (HAIRAdatok != null && HAIRAdatok.KezbesitesErtesitesSzuksegesSpecified && HAIRAdatok.KezbesitesErtesitesSzukseges)
                {
                    // kézbesítési értesítés keresése
                    var objTipusGuid = Helper.GetObjTipusByKod("EREC_IraIratok", v);
                    if (v.IsError())
                    {
                        return;
                    }

                    var iratHAIRAdatok = Helper.GetHAIRAdatok(irat.Id, objTipusGuid, KodTarak.HAIR_ADAT_TIPUSOK.KezbesitErtesites);

                    // ha nincs még kézbesítési értesítés, felvétel
                    if (iratHAIRAdatok == null)
                    {
                        if (Helper.IsError(v))
                        {
                            return;
                        }
                        if (objTipusGuid != null)
                        {
                            var ertesitesResult = Helper.InsertHAIRAdatok(irat.Id, objTipusGuid,
                                KodTarak.HAIR_ADAT_TIPUSOK.KezbesitErtesites, HAIRAdatok.KezbesitesErtesitesSzukseges ? "1" : "0");

                            if (ertesitesResult.IsError)
                            {
                                Helper.SetError(v, ertesitesResult);
                                return;
                            }
                        }
                    }
                    // meglevő kézbesítési értesítés módosítása
                    else
                    {
                        if (iratHAIRAdatok.Ertek == "0")
                        {
                            iratHAIRAdatok.Ertek = "1";
                            iratHAIRAdatok.Updated.Ertek = true;

                            var updateResult = Helper.UpdateHAIRAdatok(iratHAIRAdatok);
                            if (updateResult.IsError)
                            {
                                Helper.SetError(v, updateResult);
                                return;
                            }
                        }
                    }
                }

                // statisztika
                if (HatosagiStatisztikaAdatok != null && HatosagiStatisztikaAdatok.StatisztikaElem != null)
                {
                    foreach (var statElem in HatosagiStatisztikaAdatok.StatisztikaElem)
                    {
                        if (!String.IsNullOrEmpty(statElem.StatisztikaElemKod))
                        {
                            // tárgyszó
                            var targyszoRow = Helper.GetTargyszoByBelsoAzonosito(statElem.StatisztikaElemKod, v);
                            if (v.IsError() || targyszoRow == null)
                            {
                                continue;
                            }

                            // objTipus
                            string objTipusGuid = Helper.GetObjTipusByKod(Contentum.eUtility.Constants.TableNames.EREC_IraIratok, v);
                            if (v.IsError())
                            {
                                return;
                            }
                            if (objTipusGuid == null)
                                continue;

                            // metadef
                            var metaAdataiService = eRecordService.ServiceFactory.GetEREC_Obj_MetaAdataiService();
                            var metaSearch = new EREC_Obj_MetaAdataiSearch();
                            metaSearch.WhereByManual = " AND EREC_Obj_MetaDefinicio.DefinicioTipus='HA' and EREC_Obj_MetaDefinicio.ColumnValue='" + irat.Ugy_Fajtaja + "'";
                            Result objmetaResult = metaAdataiService.GetAllWithExtension(execParam.Clone(), metaSearch);

                            if (objmetaResult.IsError)
                            {
                                Helper.SetError(v, objmetaResult);
                                return;
                            }
                            if (objmetaResult.GetCount < 1)
                                continue;

                            var objMetaId = objmetaResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                            // statisztika lekérdezése
                            var otSearch = new EREC_ObjektumTargyszavaiSearch();
                            var service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                            otSearch.Targyszo_Id.Filter(targyszoRow["Id"].ToString());
                            otSearch.Obj_Metaadatai_Id.Filter(objMetaId);
                            otSearch.Obj_Id.Filter(irat.Id);
                            otSearch.ObjTip_Id.Filter(objTipusGuid);

                            var getResult = service_ot.GetAll(execParam.Clone(), otSearch);
                            if (getResult.IsError)
                            {
                                Helper.SetError(v, getResult);
                                continue;
                            }

                            EREC_ObjektumTargyszavai stat = null;
                            // létező statisztika?
                            if (getResult.GetCount > 0)
                            {
                                stat = new EREC_ObjektumTargyszavai();
                                Utility.LoadBusinessDocumentFromDataRow(stat, getResult.Ds.Tables[0].Rows[0]);

                                stat.Updated.SetValueAll(false);
                                stat.Base.Updated.SetValueAll(false);
                            }

                            // statisztika módosítása
                            if (stat != null)
                            {
                                stat.Ertek = statElem.StatisztikaElemErtek;
                                stat.Updated.Ertek = true;

                                var xp = execParam.Clone();
                                xp.Record_Id = stat.Id;
                                stat.Base.Updated.Ver = true;

                                var updateResult = service_ot.Update(xp, stat);
                                if (updateResult.IsError)
                                {
                                    Helper.SetError(v, updateResult);
                                    continue;
                                }
                            }
                            // statisztika létrehozása
                            else
                            {
                                Helper.InsertObjektumTargyszavak(irat.Id, irat.Ugy_Fajtaja, HatosagiStatisztikaAdatok.StatisztikaElem, v);
                            }
                        }
                    }
                }
            }
            catch (Exception x)
            {
                v.SetError(x);
            }
        }

        #endregion
    }

    #region Result

    // [XmlType(Namespace = Constants.NAMESPACE)]
    //public class Valaszok : HAIR.Valaszok
    //{
    //}

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valaszok
    {
        [XmlElement(Order = 0)]
        public Valasz[] Valasz;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valasz : HAIR.Valasz
    {
    }

    #endregion
}

