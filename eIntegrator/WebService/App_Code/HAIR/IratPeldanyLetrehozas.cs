﻿using System;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR.IratPeldanyLetrehozas
{

    public static class Constants
    {
        public const string NAMESPACE = "Contentum.eIntegrator.WebService.HAIR.IratPeldanyLetrehozas";
    }

    #region Parameters

    // [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]
    //[XmlType(Namespace = Constants.NAMESPACE)]
    //public class UjIratpeldanyok
    //{
    //    [XmlElement(Order = 0)]
    //    public UjIratpeldany[] UjIratpeldany;
    //}

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class UjIratpeldany
    {
        [XmlElement(Order = 0)]
        public Alapadatok Alapadatok;
        [XmlElement(Order = 1)]
        public IktatoszamAlszam Iktatoszam;
        [XmlElement(Order = 2)]
        public IratpeldanyAdatok IratpeldanyAdatok;
    }

    #endregion

    #region Result

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valaszok
    {
        [XmlElement(Order = 0)]
        public Valasz[] Valasz;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valasz
    {
        public string KulsoAzonosito;
        public IktatoszamSorszam Iktatoszam;
        public Vonalkodok Vonalkodok;
        public ValaszAdatok ValaszAdatok;
    }

    #endregion
}