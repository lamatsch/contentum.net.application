﻿using System;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR.PartnerModositas
{
    public static class Constants
    {
        public const string NAMESPACE = "Contentum.eIntegrator.WebService.HAIR.PartnerModositas";
    }

    #region Parameters

    // [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]    
    //[XmlType(Namespace = Constants.NAMESPACE)]
    //public class Partnerek
    //{
    //    [XmlElement(Order = 0)]
    //    public Partner[] Partner;
    //}

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Partner
    {
        [XmlElement(Order = 0)]
        public string PartnerAzonosito; // 100
    }

    #endregion

    #region Result

    [XmlType(Namespace = Constants.NAMESPACE)]
    //public class Valaszok : HAIR.Valaszok
    //{
    //}

    public class Valaszok
    {
        [XmlElement(Order = 0)]
        public Valasz[] Valasz;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valasz : HAIR.Valasz
    {
    }

    #endregion
}

