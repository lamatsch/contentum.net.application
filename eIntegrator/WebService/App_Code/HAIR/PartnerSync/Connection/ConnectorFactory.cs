﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.HAIR.PartnerSync
{
    public class ConnectorFactory
    {
        public IConnector Connector
        {
            get
            {
                string isMock = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("HAIR_MOCKMODE");
                if (!string.IsNullOrEmpty(isMock))
                {
                    return new HAIRMockConnector();
                }
                return new HAIRConnector();
            }
        }
    }
}