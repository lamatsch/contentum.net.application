﻿using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR.PartnerSync
{
    public class HAIRConnector : IConnector
    {
        HAIRParameters hairParameters;
        public HAIRConnector()
        {
            hairParameters = new HAIRPartnerModositasParameters(Helper.GlobalExecParam);
        }

        public string ServerAddress { get { return hairParameters.URL; } }
        private string CreateRequest(string url)
        {
            HttpWebRequest req = HttpWebRequest.Create(url) as HttpWebRequest;

            string auth = "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(hairParameters.User + ":" + hairParameters.Password));
            req.PreAuthenticate = true;
            req.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
            req.Headers.Add("Authorization", auth);

            string response = null;
            try
            {
                WebResponse resp = req.GetResponse();
                using (var reader = new StreamReader(resp.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    response = reader.ReadToEnd();
                }
                resp.Close();
            }
            catch (System.Net.WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var httpResponse = ex.Response as HttpWebResponse;
                    if (httpResponse != null && httpResponse.StatusCode == HttpStatusCode.NotFound)
                    {
                        throw new HAIRUgyfelNotFoundException();
                    }
                }
                throw ex;
            }
            return response;
        }

        public string GetHairUgyfelJSON(string ugyfelAzonosito)
        {
            string path = hairParameters.URL + ugyfelAzonosito;

            string responseJSON = CreateRequest(path);

            return responseJSON;
        }

    }
    public class HAIRUgyfelNotFoundException : Exception
    {

    }

    public class HAIRCannotConnectException : Exception { }

    public class InvalidJSONException : Exception { }
}
