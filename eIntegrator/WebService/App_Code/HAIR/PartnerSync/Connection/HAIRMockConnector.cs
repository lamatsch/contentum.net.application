﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.HAIR.PartnerSync
{
    public class HAIRMockConnector : IConnector
    {
        public string ServerAddress
        {
            get
            {
                return "MOCK_SERVER";
            }
        }
        public static string SZEMELY_AZONOSITO = "114000326";
        public static string VALLALKOZAS_AZONOSITO = "110022183";
        public string GetHairUgyfelJSON(string ugyfelAzonosito)
        {
            if (ugyfelAzonosito == SZEMELY_AZONOSITO) { return SZEMELY_JSON; }
            if (ugyfelAzonosito == VALLALKOZAS_AZONOSITO) { return VALLALKOZAS_JSON; }
            return "";
        }

        private string VALLALKOZAS_JSON = @"{
	""hairugyfel"": {
		""adoaz"": ""110022183"",
		""tipus"": ""S"",
		""teljes_nev"": ""Ba - Log Logisztikai, Tanácsadó És Szolgáltató BT"",
		""gfo_kod"": ""117"",
		""szervezet"": {
			""nev"": ""Ba - Log Logisztikai, Tanácsadó És Szolgáltató"",
			""utotag"": ""BT"",
			""cegbejegyzes_szama"": null
		},
		""adoszamok"": {
			""adoszam"": [{
					""adoszam"": ""20744647"",
					""ervke"": ""2000-06-26"",
					""ervig"": null
				}
			]
		},
		""kulfoldi_adoszamok"": null,
		""cimek"": {
			""cim"": [{
					""cimaz"": ""515390"",
					""teljes_cim"": ""1195 Budapest, Vas Gereben utca 5"",
					""orszag"": ""HU"",
					""iranyitoszam"": ""1195"",
					""helyseg"": ""Budapest"",
					""kozterulet"": ""Vas Gereben"",
					""kozterulet_jelleg"": ""utca"",
					""hazszam"": ""5"",
					""epulet"": null,
					""lepcsohaz"": null,
					""szint"": null,
					""ajto"": null,
					""helyrajzi_szam"": null,
					""postafiok"": null,
					""ervto"": ""2000-06-26"",
					""ervig"": ""2018-03-31"",
					""cim_tipusok"": {
						""cim_tipus"": [{
								""kod"": ""0""
							}, {
								""kod"": ""2""
							}
						]
					}
				}, {
					""cimaz"": ""1928059"",
					""teljes_cim"": ""1195 Budapest, Vas Gereben utca 5"",
					""orszag"": ""HU"",
					""iranyitoszam"": ""1195"",
					""helyseg"": ""Budapest"",
					""kozterulet"": ""Vas Gereben"",
					""kozterulet_jelleg"": ""utca"",
					""hazszam"": ""5"",
					""epulet"": null,
					""lepcsohaz"": null,
					""szint"": null,
					""ajto"": null,
					""helyrajzi_szam"": null,
					""postafiok"": null,
					""ervto"": ""2018-04-01"",
					""ervig"": null,
					""cim_tipusok"": {
						""cim_tipus"": [{
								""kod"": ""0""
							}
						]
					}
				}
			]
		}
	}
}
";

        public string SZEMELY_JSON = @"{
	""hairugyfel"": {
		""adoaz"": ""114000326"",
		""tipus"": ""M"",
		""teljes_nev"": ""Marcus Florin"",
		""gfo_kod"": null,
		""szemely"": {
			""elotag"": null,
			""csaladi_nev"": ""Marcus"",
			""utonev"": ""Florin"",
			""tovabbi_utonev"": null,
			""szuletesi_csaladi_nev"": null,
			""szuletesi_utonev"": null,
			""szuletesi_tovabbi_utonev"": null,
			""szuletesi_datum"": null,
			""szuletesi_hely"": null,
			""anyja_csaladi_neve"": null,
			""anyja_utoneve"": null,
			""anyja_tovabbi_utoneve"": null,
			""adoigazgatasi_jel"": null
		},
		""adoszamok"": null,
		""kulfoldi_adoszamok"": null,
		""cimek"": {
			""cim"": [{
					""cimaz"": ""1669741"",
					""teljes_cim"": ""445200 Negresti-Oas, Satu Mare Str.Livezilor 147."",
					""orszag"": ""RO"",
					""iranyitoszam"": ""445200"",
					""helyseg"": ""Negresti-Oas"",
					""kozterulet"": ""Satu Mare Str.Livezilor 147."",
					""kozterulet_jelleg"": null,
					""hazszam"": null,
					""epulet"": null,
					""lepcsohaz"": null,
					""szint"": null,
					""ajto"": null,
					""helyrajzi_szam"": null,
					""postafiok"": null,
					""ervto"": ""2015-11-04"",
					""ervig"": null,
					""cim_tipusok"": {
						""cim_tipus"": [{
								""kod"": ""1""
							}
						]
					}
				}
			]
		}
	}
}
";

     
    }
}