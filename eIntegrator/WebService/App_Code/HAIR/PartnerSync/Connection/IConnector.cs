﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.HAIR.PartnerSync
{
    public interface IConnector
    {
        string GetHairUgyfelJSON(string ugyfelAzonosito);
        string ServerAddress { get; }
    }
}