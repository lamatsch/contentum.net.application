﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.HAIR.PartnerSync
{
    public class HAIRPartnerSyncer
    {
        private ExecParam ExecParam { get; set; }
        private JsonSerializerSettings jsonSettings;

        public HAIRPartnerSyncer()
        {
            jsonSettings = new JsonSerializerSettings();
            jsonSettings.Formatting = Formatting.Indented;
            jsonSettings.NullValueHandling = NullValueHandling.Ignore;
        }

        public List<HAIRUgyfelMapped> StartSync(ExecParam ExecParam, IConnector connector, string[] adoazon)
        {
            this.ExecParam = ExecParam;
            List<HAIRUgyfelMapped> result = new List<HAIRUgyfelMapped>();

            //letoltjuk az adoazon alapján a HAIR bol az ugyfeleket
            List<HAIRUgyfel> ugyfelek = new List<HAIRUgyfel>();
            foreach (var adoazonItem in adoazon)
            {
                string ugyfelJSON;
                HAIRUgyfelWrapper wrapper;
                try
                {
                    ugyfelJSON = connector.GetHairUgyfelJSON(adoazonItem);
                    Logger.Debug(string.Format("Response from HAIR for Adoazon: {0}, JSON: {1}", adoazonItem, ugyfelJSON));
                    if (ugyfelJSON == null) { throw new HAIRCannotConnectException(); }
                    wrapper = (HAIRUgyfelWrapper)JsonConvert.DeserializeObject(ugyfelJSON, typeof(HAIRUgyfelWrapper), jsonSettings);
                    if (wrapper == null || wrapper.hairugyfel == null)
                    {
                        throw new HAIRCannotConnectException();
                    }
                    ugyfelek.Add(wrapper.hairugyfel);
                }
                catch (HAIRUgyfelNotFoundException ex)
                {
                    //A HAIR service http 404 el jelzi ha lekért adoazonhoz nem tartozik ugyfel. Ebben az esetben érvénytelenitani kell nálunk a partnert.                    
                    HAIRUgyfelMapped mapped = new HAIRUgyfelMapped();
                    InvalidatePartnerWithAdoazon(adoazonItem, mapped);
                    result.Add(mapped);
                }
                catch (Exception ex)
                {
                    Logger.Error("Nem sikerült lekérdezni a következő partnert: " + adoazonItem, ex);
                }
            }

            foreach (HAIRUgyfel ugyfel in ugyfelek)
            {
                var mappedUgyfel = SyncHAIRUgyfel(ugyfel);
                result.Add(mappedUgyfel);
            }
            return result;
        }

        public List<HAIRUgyfelMapped> StartSyncFromJSON(ExecParam ExecParam, string json)
        {
            this.ExecParam = ExecParam;
            List<HAIRUgyfelWrapper> wrappedUgyfelek  = (List<HAIRUgyfelWrapper>)JsonConvert.DeserializeObject(json, typeof(List<HAIRUgyfelWrapper>));
            
            List<HAIRUgyfelMapped> result = new List<HAIRUgyfelMapped>();
            foreach (HAIRUgyfelWrapper wrapper in wrappedUgyfelek)
            {
                var mappedUgyfel = SyncHAIRUgyfel(wrapper.hairugyfel);
                result.Add(mappedUgyfel);
            }
            return result;
        }
        #region Syncers are in charge of deciding when to insert and when to update 
        private HAIRUgyfelMapped SyncHAIRUgyfel(HAIRUgyfel ugyfel)
        {
            Logger.Debug("SyncHAIRUgyfel: " + ugyfel.adoaz);
            HAIRUgyfelMapped mappedUgyfel = new HAIRUgyfelMapped();
            mappedUgyfel.hairUgyfel = ugyfel;

            //ellenőrizzük, hogy az adott adoaz-hoz van e partner rendelve
            Result partnerResult = PartnerSyncHelper.GetPartnerByKulsoAzonosito(ugyfel.adoaz);
            KRT_Partnerek partner = new KRT_Partnerek();

            //ha nincs létrehozzuk a partnert
            if (!Utility.HasData(partnerResult))
            {                
                mappedUgyfel.isNewPartnerCreated = true;
                MapPartner(partner, ugyfel);
                Result saveResult = PartnerSyncHelper.SavePartner(partner);

                AddAsErrorIfNeeded("SyncHAIRUgyfel.InsertNewPartner", saveResult, mappedUgyfel);

                if (!saveResult.IsError)
                {
                    partner.Id = saveResult.Uid; //az eredmény HAIRUgyfelMapped.Partner be beleírjuk az ID-t mert kint szükség lesz rá
                }
            }

            // ha van akkor updateljük az ugyfel alapjan a partnert
            if (Utility.HasData(partnerResult))
            {               
                Utility.LoadBusinessDocumentFromDataRow(partner, partnerResult.Ds.Tables[0].Rows[0]);
                MapPartner(partner, ugyfel);
                Result updateResult = PartnerSyncHelper.UpdatePartner(partner);

                AddAsErrorIfNeeded("SyncHAIRUgyfel.UpdatePartner", updateResult, mappedUgyfel);
            }
            if (ugyfel.cimek != null)
            {
                foreach (var cim in ugyfel.cimek.cim)
                {
                    //megnézük hogy a megadott külső azonosítóval van-e már rögzítve cím KRT_Cimek táblába
                    mappedUgyfel.KRT_Cimek.Add(SyncCim(cim, partner, mappedUgyfel));
                }
            }

            if (partner.Tipus == "20")//szemely
            {
                mappedUgyfel.KRT_Szemely = SyncSzemelyData(partner, mappedUgyfel);
            }
            if (partner.Tipus == "10")//vállalkozás
            {
                mappedUgyfel.KRT_Vallalkozas = SyncVallalkozasData(partner, mappedUgyfel);
            }

            mappedUgyfel.KRT_Partner = partner;
            return mappedUgyfel;
        }

        private KRT_Cimek SyncCim(Cim cim, KRT_Partnerek krtPartner, HAIRUgyfelMapped mappedUgyfel)
        {         
            Result cimResult = PartnerSyncHelper.GetCimByKulsoAzonosito(cim.cimaz, cim.ervto, cim.ervig);
            AddAsErrorIfNeeded("SyncCim.GetCimByKulsoAzonosito", cimResult, mappedUgyfel);
            KRT_Cimek krtCim = new KRT_Cimek();

            //ha nincs ilyen cim akkor létrehozzuk
            if (!Utility.HasData(cimResult))
            {                
                MapCim(krtCim, cim);
                Result result = PartnerSyncHelper.SaveCim(krtCim);
                AddAsErrorIfNeeded("SyncCim.SaveCim", result, mappedUgyfel);
                if (!result.IsError)
                {
                    krtCim.Id = result.Uid;
                }
            }

            // ha van akkor updateljük az ugyfel.cimek.cim alapjan
            if (Utility.HasData(cimResult))
            {                
                Utility.LoadBusinessDocumentFromDataRow(krtCim, cimResult.Ds.Tables[0].Rows[0]);
                //korábban a CONTENTUM-ban érvénytelenített tételt nem lehet update-elni a WS-en keresztül               
                if (AreDatesValid(krtCim.ErvKezd, krtCim.ErvVege))
                {
                    MapCim(krtCim, cim);
                    Result result = PartnerSyncHelper.UpdateCim(krtCim);
                    AddAsErrorIfNeeded("SyncCim.UpdateCim", result, mappedUgyfel);
                }
            }

            //ellenorizzuk, hogy a cim hozza van-e kotve a partnerhez
            Result partnerCimResult = PartnerSyncHelper.GetPartnerCim(krtPartner.Id, krtCim.Id);
            KRT_PartnerCimek krtPartnerCim = new KRT_PartnerCimek(); ;
            AddAsErrorIfNeeded("SyncCim.GetPartnerCim", partnerCimResult, mappedUgyfel);

            //ha nincs ilyen hozzarendeles akkor létrehozzuk
            if (!Utility.HasData(partnerCimResult))
            {
                MapPartnerCim(cim, krtCim, krtPartner, krtPartnerCim);
                Result saveResult = PartnerSyncHelper.SavePartnerCim(krtPartnerCim);
                AddAsErrorIfNeeded("SyncCim.SavePartnerCim", saveResult, mappedUgyfel);
            }

            return krtCim;
        }
        private KRT_Szemelyek SyncSzemelyData(KRT_Partnerek partner, HAIRUgyfelMapped mappedUgyfel)
        {
            Result szemelyResult = PartnerSyncHelper.GetSzemelyByPartnerID(partner.Id);
            KRT_Szemelyek szemely = new KRT_Szemelyek();
            AddAsErrorIfNeeded("SyncSzemelyData.GetSzemelyByPartnerID", szemelyResult, mappedUgyfel);
            //ha nincs ilyen szemely akkor létrehozzuk
            if (!Utility.HasData(szemelyResult))
            {
                MapSzemely(partner, szemely, mappedUgyfel.hairUgyfel);
                Result result = PartnerSyncHelper.SaveSzemely(szemely);
                AddAsErrorIfNeeded("SyncSzemelyData.SaveSzemely", result, mappedUgyfel);
                if (!result.IsError)
                {
                    szemely.Id = result.Uid;
                }
            }

            // ha van akkor updateljük az ugyfel alapjan
            if (Utility.HasData(szemelyResult))
            {
                Utility.LoadBusinessDocumentFromDataRow(szemely, szemelyResult.Ds.Tables[0].Rows[0]);
                MapSzemely(partner, szemely, mappedUgyfel.hairUgyfel);
                PartnerSyncHelper.UpdateSzemely(szemely);
            }
            return szemely;
        }
        private KRT_Vallalkozasok SyncVallalkozasData(KRT_Partnerek partner, HAIRUgyfelMapped mappedUgyfel)
        {
            Result vallalkozasResult = PartnerSyncHelper.GetVallalkozasByPartnerID(partner.Id);
            AddAsErrorIfNeeded("SyncVallalkozasData.GetVallalkozasByPartnerData", vallalkozasResult, mappedUgyfel);
            KRT_Vallalkozasok vallalkozas = new KRT_Vallalkozasok();
            //ha nincs ilyen  vallalkozas akkor létrehozzuk
            if (!Utility.HasData(vallalkozasResult))
            {
                MapVallalkozas(partner, vallalkozas, mappedUgyfel.hairUgyfel);
                Result result = PartnerSyncHelper.SaveVallalkozas(vallalkozas);
                AddAsErrorIfNeeded("SyncVallalkozasData.SaveVallalkozas", result, mappedUgyfel);
                vallalkozas.Id = result.Uid;
            }

            // ha van akkor updateljük az ugyfel alapjan
            if (Utility.HasData(vallalkozasResult))
            {
                Utility.LoadBusinessDocumentFromDataRow(vallalkozas, vallalkozasResult.Ds.Tables[0].Rows[0]);
                MapVallalkozas(partner, vallalkozas, mappedUgyfel.hairUgyfel);
                Result result = PartnerSyncHelper.UpdateVallalkozas(vallalkozas);
                AddAsErrorIfNeeded("SyncVallalkozas.UpdateVallalkozas", result, mappedUgyfel);
            }
            return vallalkozas;
        }
        #endregion

        #region Mappers which are helping to connnect the Contentum Business objects to the incoming JSON object
        private void MapPartnerCim(Cim cim, KRT_Cimek krtCim, KRT_Partnerek partner, KRT_PartnerCimek partnerCim)
        {
            partnerCim.Partner_id = partner.Id;
            partnerCim.Cim_Id = krtCim.Id;
            partnerCim.Fajta = GetPartnerCimFajta(cim.cim_tipusok);
        }

        private void MapPartner(KRT_Partnerek partner, HAIRUgyfel ugyfel)
        {
            partner.Tipus = ugyfel.tipus == "M" ? "20" : "10";
            partner.Nev = ugyfel.teljes_nev;
            partner.KulsoAzonositok = ugyfel.adoaz;
            partner.Forras = PartnerSyncHelper.HAIR_FORRAS;
            partner.Org = "450B510A-7CAA-46B0-83E3-18445C0C53A9"; // TODO Ez jó?
        }

        private void MapSzemely(KRT_Partnerek partner, KRT_Szemelyek szemely, HAIRUgyfel ugyfel)
        {
            szemely.UjTitulis = ugyfel.szemely.elotag;
            szemely.UjCsaladiNev = ugyfel.szemely.csaladi_nev;
            szemely.UjUtonev = ugyfel.szemely.utonev;
            szemely.UjTovabbiUtonev = ugyfel.szemely.tovabbi_utonev;
            szemely.SzuletesiCsaladiNev = ugyfel.szemely.szuletesi_csaladi_nev;
            szemely.SzuletesiElsoUtonev = ugyfel.szemely.szuletesi_utonev;
            szemely.SzuletesiTovabbiUtonev = ugyfel.szemely.szuletesi_tovabbi_utonev;
            szemely.SzuletesiIdo = ugyfel.szemely.szuletesi_datum;
            szemely.SzuletesiHely = ugyfel.szemely.szuletesi_hely;
            szemely.AnyjaNeve = ugyfel.szemely.anyja_csaladi_neve;
            szemely.AnyjaNeveCsaladiNev = ugyfel.szemely.anyja_csaladi_neve;
            szemely.AnyjaNeveElsoUtonev = ugyfel.szemely.anyja_utoneve;
            szemely.AnyjaNeveTovabbiUtonev = ugyfel.szemely.anyja_tovabbi_utoneve;
            szemely.Adoazonosito = ugyfel.szemely.adoigazgatasi_jel;
            szemely.Partner_Id = partner.Id;

            if (ugyfel.adoszamok != null)
            {
                var adoszam = GetFirstValidAdoszam(ugyfel.adoszamok.adoszam);
                szemely.Adoszam = adoszam == null ? "" : adoszam.adoszam;
            }
            //ha nincs adoszam csak kulfoldi akkor azt kell használni
            if (ugyfel.adoszamok == null && ugyfel.kulfoldi_adoszamok != null)
            {
                var adoszam = GetFirstValidAdoszam(ugyfel.kulfoldi_adoszamok.kulfoldi_adoszam);
                szemely.Adoszam = adoszam == null ? "" : adoszam.adoszam;
                szemely.KulfoldiAdoszamJelolo = adoszam == null ? "0" : "1";
            }
        }

        private void MapVallalkozas(KRT_Partnerek partner, KRT_Vallalkozasok vallalkozas, HAIRUgyfel ugyfel)
        {
            bool nincsItem;
            List<string> result = new List<string>();
            try
            {
                result = KodtarFuggoseg.GetFuggoKodtarakKodList(this.ExecParam, "GFO_KOD","CEGTIPUS",  ugyfel.gfo_kod, out nincsItem);
            }catch(Exception e)
            {
                Logger.Error("Hiba a kodtárfüggőség lekérdezése közben: " + e.Message);
            }
            if (result == null || result.Count == 0)
            {
                result = new List<string>() { ugyfel.gfo_kod };
                Logger.Error(string.Format("HAIR PartnerSync nem tudta a vállalkozás típusát megállapítani a gfo kód ({0}) alapján mert az nem volt megtalálható a függő kódtárban.", ugyfel.gfo_kod));
            }
            vallalkozas.Tipus = result.First();
            vallalkozas.Cegjegyzekszam = ugyfel.szervezet.cegbejegyzes_szama;
            vallalkozas.Partner_Id = partner.Id;

            if (ugyfel.adoszamok != null)
            {
                var adoszam = GetFirstValidAdoszam(ugyfel.adoszamok.adoszam);
                vallalkozas.Adoszam = adoszam == null ? "" : adoszam.adoszam;
            }

            if (ugyfel.adoszamok == null && ugyfel.kulfoldi_adoszamok != null)
            {
                var adoszam = GetFirstValidAdoszam(ugyfel.kulfoldi_adoszamok.kulfoldi_adoszam);
                vallalkozas.Adoszam = adoszam == null ? "" : adoszam.adoszam;
                vallalkozas.KulfoldiAdoszamJelolo = adoszam == null ? "0" : "1"; // ha van adoszám akkor a jelőlőt 1 re kell álltani
            }
        }

        private void MapCim(KRT_Cimek krtCim, Cim cim)
        {
            krtCim.Forras = PartnerSyncHelper.HAIR_FORRAS;
            krtCim.KulsoAzonositok = ToStringIfNull(cim.cimaz);
            krtCim.Nev = cim.teljes_cim;
            krtCim.Orszag_Id = GetOrszagIdFromOrszagKod(cim.orszag);
            krtCim.OrszagNev = GetOrszagNevFromOrszagKod(cim.orszag);
            krtCim.IRSZ = cim.iranyitoszam;
            krtCim.Telepules_Id = GetTelepulesIdFromTelepulesNev(cim.helyseg);
            krtCim.TelepulesNev = cim.helyseg;
            krtCim.KozteruletNev = cim.kozterulet;
            krtCim.KozteruletTipusNev = cim.kozterulet_jelleg;
            krtCim.KozteruletTipus_Id = GetKozteruletTipusIdFromKozteruletNev(cim.kozterulet_jelleg);
            krtCim.Hazszam = cim.hazszam;
            krtCim.HazszamBetujel = cim.epulet;
            krtCim.Lepcsohaz = cim.lepcsohaz;
            krtCim.Szint = cim.szint;
            krtCim.Ajto = cim.ajto;
            krtCim.HRSZ = cim.helyrajzi_szam;
            krtCim.CimTobbi = cim.postafiok;
            krtCim.ErvKezd = cim.ervto;
            krtCim.ErvVege = cim.ervig;
            krtCim.Org = "450B510A-7CAA-46B0-83E3-18445C0C53A9";
            krtCim.Kategoria = "K"; //TODO OK??
            krtCim.Tipus = "01";
        }
        #endregion

        private void InvalidatePartnerWithAdoazon(string adoazon, HAIRUgyfelMapped mappedUgyfel)
        {
            Logger.Debug("Starting partner invalidation because the local partner is not found in HAIR");
            mappedUgyfel.IsInvalidated = true;
            KRT_Partnerek partner = new KRT_Partnerek();
            Result partnerGetResult = PartnerSyncHelper.GetPartnerByKulsoAzonosito(adoazon);
            AddAsErrorIfNeeded("InvalidatePartnerWithAdoazon.GetPartnerByKulsoAzonosito", partnerGetResult, mappedUgyfel);

            if (!Utility.HasData(partnerGetResult))
            {
                mappedUgyfel.ErrorResults.Add(new Result() { ErrorMessage = "Nem létezik partner a következő adoazonosítóval: " + adoazon });
                Logger.Error("Partner invalidation failed. Partner is not found with KulsoAzonosito: " + adoazon);
                return;
            }
            Logger.Debug("Invalidating partner: " + partner.Id);
            Utility.LoadBusinessDocumentFromDataRow(partner, partnerGetResult.Ds.Tables[0].Rows[0]);
            partner.ErvVege = DateTime.Now.ToString();

            mappedUgyfel.KRT_Partner = partner;

            Result saveResult = PartnerSyncHelper.UpdatePartner(partner);
            AddAsErrorIfNeeded("InvalidatePartnerWithAdoazon.UpdatePartner", saveResult, mappedUgyfel);
        }

        #region Internal Helpers
        private Adoszam GetFirstValidAdoszam(Adoszam[] adoszamok)
        {
            Adoszam firstValidAdoszam = null;
            foreach (var adoszam in adoszamok)
            {
                //az első olyan adoszám kell nekünk aminek az érvényessége jó
                if (AreDatesValid(adoszam.ervke, adoszam.ervig)) ;
                {
                    firstValidAdoszam = adoszam;
                    break;
                }
            }
            return firstValidAdoszam;
        }

        private string GetPartnerCimFajta(CimTipusok cimTipusok)
        {
            if (cimTipusok == null) { return string.Empty; }
            if (cimTipusok.cim_tipus == null) { return string.Empty; }
            var cimTipus = cimTipusok.cim_tipus.First();
            if (cimTipus == null) { return string.Empty; }
            switch (cimTipus.kod)
            {
                case "0":
                    return "01";//Szekhely
                case "1":
                    return "03";//Lakhely
                case "2":
                    return "05";//Levelezesi
                default: return string.Empty;
            }
        }

        private string GetOrszagIdFromOrszagKod(string orszag)
        {
            Result orszagResult = PartnerSyncHelper.GetOrszagByKod(orszag);

            if (Utility.HasData(orszagResult))
            {
                KRT_Orszagok krtOrszag = new KRT_Orszagok();
                Utility.LoadBusinessDocumentFromDataRow(krtOrszag, orszagResult.Ds.Tables[0].Rows[0]);
                return krtOrszag.Id;
            }
            return string.Empty;
        }

        private string GetOrszagNevFromOrszagKod(string orszag)
        {
            Result orszagResult = PartnerSyncHelper.GetOrszagByKod(orszag);

            if (Utility.HasData(orszagResult))
            {
                KRT_Orszagok krtOrszag = new KRT_Orszagok();
                Utility.LoadBusinessDocumentFromDataRow(krtOrszag, orszagResult.Ds.Tables[0].Rows[0]);
                return krtOrszag.Nev;
            }
            return string.Empty;
        }

        private string GetTelepulesIdFromTelepulesNev(string telepulesNev)
        {
            Result orszagResult = PartnerSyncHelper.GetTelepulesFromNev(telepulesNev);
            if (Utility.HasData(orszagResult))
            {
                KRT_Telepulesek krtTelepules = new KRT_Telepulesek();
                Utility.LoadBusinessDocumentFromDataRow(krtTelepules, orszagResult.Ds.Tables[0].Rows[0]);
                return krtTelepules.Id;
            }
            return string.Empty;
        }

        public string GetKozteruletTipusIdFromKozteruletNev(string kozteruletJelleg)
        {
            Result kozteruletResult = PartnerSyncHelper.GetKozteruletTipusFromNev(kozteruletJelleg);
            if (Utility.HasData(kozteruletResult))
            {
                KRT_KozteruletTipusok krtKozteruletTipus = new KRT_KozteruletTipusok();
                Utility.LoadBusinessDocumentFromDataRow(krtKozteruletTipus, kozteruletResult.Ds.Tables[0].Rows[0]);
                return krtKozteruletTipus.Id;
            }
            return string.Empty;
        }
        private string ToStringIfNull(string value) { if (value == null) { return string.Empty; } return value; }

        private void AddAsErrorIfNeeded(string actionTag, Result result, HAIRUgyfelMapped mapped)
        {
            string adoazon = mapped.hairUgyfel == null ? "- no adoaz - " : mapped.hairUgyfel.adoaz;

            if (result.IsError)
            {
                Logger.Debug(adoazon + " - Failed action: " + actionTag + " Error: " + result.ErrorMessage);
                result.ErrorMessage = actionTag + result.ErrorMessage;
                mapped.ErrorResults.Add(result);
            }
            else
            {
                Logger.Debug(adoazon + " - Successfull action: " + actionTag);
            }
        }
        private bool AreDatesValid(string ervKezdString, string ervVegeString)
        {
            DateTime? ervKezd = TryParseHAIRDate(ervKezdString);
            DateTime? ervVege = TryParseHAIRDate(ervVegeString);

            bool isErvKezdValid = ervKezd.HasValue && ervKezd < DateTime.Today;
            bool isErvVegeValid = !ervVege.HasValue || (ervVege.HasValue && ervVege > DateTime.Today);

            if (isErvKezdValid && isErvVegeValid)
            {
                return true;
            }
            return false;
        }
        private Nullable<DateTime> TryParseHAIRDate(string date)
        {
            if (date == null) { return null; }
            try
            {
                return DateTime.ParseExact(date, "yyyy-mm-dd", null);
            }
            catch (Exception e) { return null; }
        }
        #endregion
    }
}