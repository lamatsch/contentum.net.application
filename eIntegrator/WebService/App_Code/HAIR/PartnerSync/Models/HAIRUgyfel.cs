﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Contentum.eIntegrator.WebService.HAIR.PartnerSync
{
    public class HAIRUgyfelWrapper
    {
        public HAIRUgyfel hairugyfel { get; set; }
    }
    public class HAIRUgyfel
    {
        public string adoaz { get; set; }
        public string tipus { get; set; }
        public string teljes_nev { get; set; }
        public string gfo_kod { get; set; }
        public Szervezet szervezet { get; set; }
        public Adoszamok adoszamok { get; set; }
        public KulfoldiAdoszamok kulfoldi_adoszamok { get; set; }
        public Cimek cimek { get; set; }
        public Szemely szemely { get; set; }
    }
    public class Szemely
    {
        public string elotag { get; set; }
        public string csaladi_nev { get; set; }
        public string utonev { get; set; }
        public string tovabbi_utonev { get; set; }
        public string szuletesi_csaladi_nev { get; set; }
        public string szuletesi_utonev { get; set; }
        public string szuletesi_tovabbi_utonev { get; set; }
        public string szuletesi_datum { get; set; }
        public string szuletesi_hely { get; set; }
        public string anyja_csaladi_neve { get; set; }
        public string anyja_utoneve { get; set; }
        public string anyja_tovabbi_utoneve { get; set; }
        public string adoigazgatasi_jel { get; set; }
    }

    public class Szervezet
    {
        public string nev { get; set; }
        public string utotag { get; set; }
        public string cegbejegyzes_szama { get; set; }
    }

    public class Adoszam
    {
        public string adoszam { get; set; }
        public string ervke { get; set; }
        public string ervig { get; set; }
    }

    public class Adoszamok
    {
        public Adoszam[] adoszam { get; set; }
    }

    public class KulfoldiAdoszam : Adoszam
    {

    }

    public class KulfoldiAdoszamok
    {
        public KulfoldiAdoszam[] kulfoldi_adoszam { get; set; }
    }

    public class CimTipus
    {
        public string kod { get; set; }
    }

    public class CimTipusok
    {
        public CimTipus[] cim_tipus { get; set; }
    }

    public class Cim
    {
        public string cimaz { get; set; }
        public string teljes_cim { get; set; }
        public string orszag { get; set; }
        public string iranyitoszam { get; set; }
        public string helyseg { get; set; }
        public string kozterulet { get; set; }
        public string kozterulet_jelleg { get; set; }
        public string hazszam { get; set; }
        public string epulet { get; set; }
        public string lepcsohaz { get; set; }
        public string szint { get; set; }
        public string ajto { get; set; }
        public string helyrajzi_szam { get; set; }
        public string postafiok { get; set; }
        public string ervto { get; set; }
        public string ervig { get; set; }
        public CimTipusok cim_tipusok { get; set; }
    }

    public class Cimek
    {
        public Cim[] cim { get; set; }
    }



}