﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.HAIR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HAIRUgyfelMapped
/// </summary>
namespace Contentum.eIntegrator.WebService.HAIR.PartnerSync
{
    public class HAIRUgyfelMapped
    {
        public HAIRUgyfelMapped()
        {
            KRT_Cimek = new List<KRT_Cimek>();
            ErrorResults = new List<Result>();
        }
        public KRT_Partnerek KRT_Partner { get; set; }
        public bool isNewPartnerCreated { get; set; }
        public KRT_Szemelyek KRT_Szemely { get; set; }
        public KRT_Vallalkozasok KRT_Vallalkozas { get; set; }
        public List<KRT_Cimek> KRT_Cimek { get; set; }

        public HAIRUgyfel hairUgyfel;
        public List<Result> ErrorResults { get; set; }
        public bool IsInvalidated { get; set; }
    }
}