﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Serialization;


namespace Contentum.eIntegrator.WebService.HAIR.PartnerSync
{
    public class PartnerSyncHelper
    {
        private static ExecParam _execParam;
        public const string HAIR_FORRAS = "H";

        public static ExecParam GlobalExecParam
        {
            get
            {
               return  Helper.GlobalExecParam;                
            }
        }
        public static Result SavePartnerCim(KRT_PartnerCimek partnerCim)
        {
            Contentum.eAdmin.Service.KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();

            Result res = service.Insert(ExecParam, partnerCim);
            return res;
        }
        public static Result UpdatePartnerCim(KRT_PartnerCimek partnerCim)
        {
            Contentum.eAdmin.Service.KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();
            ExecParam.Record_Id = partnerCim.Id;
            Result res = service.Update(ExecParam, partnerCim);
            return res;
        }
        public static Result SaveCim(KRT_Cimek cim)
        {
            Contentum.eAdmin.Service.KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();

            Result res = service.Insert(ExecParam, cim);
            return res;
        }

        public static Result UpdateCim(KRT_Cimek cim)
        {
            Contentum.eAdmin.Service.KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();
            ExecParam.Record_Id = cim.Id;
            Result res = service.Update(ExecParam, cim);
            return res;
        }

        public static Result SaveVallalkozas(KRT_Vallalkozasok vallalkozas)
        {
            Contentum.eAdmin.Service.KRT_VallalkozasokService service = eAdminService.ServiceFactory.GetKRT_VallalkozasokService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();

            Result res = service.Insert(ExecParam, vallalkozas);
            return res;
        }

        public static Result UpdateVallalkozas(KRT_Vallalkozasok vallalkozas)
        {
            Contentum.eAdmin.Service.KRT_VallalkozasokService service = eAdminService.ServiceFactory.GetKRT_VallalkozasokService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();
            ExecParam.Record_Id = vallalkozas.Id;
            Result res = service.Update(ExecParam, vallalkozas);
            return res;
        }
        public static Result SaveSzemely(KRT_Szemelyek szemely)
        {
            Contentum.eAdmin.Service.KRT_SzemelyekService service = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();

            Result res = service.Insert(ExecParam, szemely);
            return res;
        }

        public static Result UpdateSzemely(KRT_Szemelyek szemely)
        {
            Contentum.eAdmin.Service.KRT_SzemelyekService service = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();
            ExecParam.Record_Id = szemely.Id;
            Result res = service.Update(ExecParam, szemely);
            return res;
        }

        public static Result SavePartner(KRT_Partnerek partner)
        {
            Contentum.eAdmin.Service.KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();
            Result res = service.Insert(ExecParam, partner);
            return res;
        }

        public static Result UpdatePartner(KRT_Partnerek partner)
        {
            Contentum.eAdmin.Service.KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();
            ExecParam.Record_Id = partner.Id;
            Result res = service.Update(ExecParam, partner);
            return res;
        }
        public static Result GetCimByKulsoAzonosito(string kulsoAzonosito,string ervKezd,string ervVege)
        {
            KRT_CimekService cimService = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam execParam = GlobalExecParam.Clone();

            KRT_CimekSearch cimekSearch = new KRT_CimekSearch();
            cimekSearch.OrderBy = null;

            cimekSearch.KulsoAzonositok.Value = kulsoAzonosito;
            cimekSearch.KulsoAzonositok.Operator = Contentum.eQuery.Query.Operators.equals;

            cimekSearch.ErvKezd.Value = ervKezd;
            cimekSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            cimekSearch.ErvKezd.Group = "100";
            cimekSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            cimekSearch.ErvVege.Value = ervVege;
            cimekSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            cimekSearch.ErvVege.Group = "100";
            cimekSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;


            Result cimekResult = cimService.GetAll(execParam, cimekSearch);

            return cimekResult;
        }

        public static Result GetSzemelyByPartnerID(string partnerID)
        {
            KRT_SzemelyekService service = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
            ExecParam execParam = GlobalExecParam.Clone();
            KRT_SzemelyekSearch search = new KRT_SzemelyekSearch();
            search.OrderBy = null;


            search.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.Partner_Id.Value = partnerID;
            search.Partner_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            Result result = service.GetAll(execParam, search);

            return result;
        }
        public static Result GetVallalkozasByPartnerID(string partnerID)
        {
            KRT_VallalkozasokService service = eAdminService.ServiceFactory.GetKRT_VallalkozasokService();
            ExecParam execParam = GlobalExecParam.Clone();
            KRT_VallalkozasokSearch search = new KRT_VallalkozasokSearch();
            search.OrderBy = null;


            search.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.Partner_Id.Value = partnerID;
            search.Partner_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            Result result = service.GetAll(execParam, search);

            return result;
        }
        public static Result GetOrszagByKod(string orszagKod)
        {
            KRT_OrszagokService partnerService = eAdminService.ServiceFactory.GetKRT_OrszagokService();
            ExecParam execParam = GlobalExecParam.Clone();
            KRT_OrszagokSearch search = new KRT_OrszagokSearch();
            search.OrderBy = null;

            search.Kod.Value = orszagKod;
            search.Kod.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = partnerService.GetAll(execParam, search);

            return result;
        }
        public static Result GetTelepulesFromNev(string telepulesNev)
        {
            KRT_TelepulesekService service = eAdminService.ServiceFactory.GetKRT_TelepulesekService();
            ExecParam execParam = GlobalExecParam.Clone();
            KRT_TelepulesekSearch search = new KRT_TelepulesekSearch();
            search.OrderBy = null;

            search.Nev.Value = telepulesNev;
            search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = service.GetAll(execParam, search);

            return result;
        }
        public static Result GetKozteruletTipusFromNev(string kozteruletTipusNev)
        {
            KRT_KozteruletTipusokService service = eAdminService.ServiceFactory.GetKRT_KozteruletTipusokService();
            ExecParam execParam = GlobalExecParam.Clone();
            KRT_KozteruletTipusokSearch search = new KRT_KozteruletTipusokSearch();
            search.OrderBy = null;

            search.Nev.Value = kozteruletTipusNev;
            search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = service.GetAll(execParam, search);

            return result;
        }
        public static Result GetPartnerCim(string partnerID, string cimID)
        {
            Contentum.eAdmin.Service.KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            Contentum.eBusinessDocuments.ExecParam ExecParam = GlobalExecParam.Clone();
            KRT_PartnerCimekSearch search = new KRT_PartnerCimekSearch();
            search.Partner_id.Value = partnerID;
            search.Partner_id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.Cim_Id.Value = cimID;
            search.Cim_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            search.ErvKezd.Group = "100";
            search.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            search.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            search.ErvVege.Group = "100";
            search.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            Result res = service.GetAll(ExecParam, search);

            return res;
        }

        public static Result GetPartnerByKulsoAzonosito(string kulsoAzonosito)
        {
            KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = GlobalExecParam.Clone();
            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.OrderBy = null;

            partnerekSearch.KulsoAzonositok.Value = kulsoAzonosito;
            partnerekSearch.KulsoAzonositok.Operator = Contentum.eQuery.Query.Operators.equals;

            partnerekSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            partnerekSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            partnerekSearch.ErvKezd.Group = "100";
            partnerekSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            partnerekSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            partnerekSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            partnerekSearch.ErvVege.Group = "100";
            partnerekSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            partnerekSearch.Forras.Value = HAIR_FORRAS;
            partnerekSearch.Forras.Operator = Contentum.eQuery.Query.Operators.equals;

            Result partnerekResult = partnerService.GetAll(execParam, partnerekSearch);

            return partnerekResult;
        }

        public static Result GetObjTipusFromObjectKod(string objKod)
        {
            KRT_ObjTipusokService service = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
            ExecParam execParam = GlobalExecParam.Clone();
            KRT_ObjTipusokSearch search = new KRT_ObjTipusokSearch();
            search.OrderBy = null;

            search.Kod.Value = objKod;
            search.Kod.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result = service.GetAll(execParam, search);

            return result;
        }   
    }
}