﻿using System;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR.Postazas
{
    public static class Constants
    {
        public const string NAMESPACE = "Contentum.eIntegrator.WebService.HAIR.Postazas";
    }

    #region Parameters

    // [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]    
    //[XmlType(Namespace = Constants.NAMESPACE)]
    //public class Postazasok
    //{
    //    [XmlElement(Order = 0)]
    //    public Postazas[] Postazas;
    //}

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Postazas
    {
        [XmlElement(Order = 0)]
        public Alapadatok Alapadatok;
        [XmlElement(Order = 1)]
        public IratAdatok IratAdatok;
        [XmlElement(Order = 2)]
        public PostazasAdatok PostazasAdatok;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class IratAdatok
    {
        [XmlElement(Order = 0)]
        public IktatoszamSorszam Iktatoszam;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class PostazasAdatok
    {
        public string Postakonyv; // 10

        public DateTime PostazasDatuma;
        [XmlIgnore] public bool PostazasDatumaSpecified;

        public string KuldemenyAzonosito; // 100
        public string Felado; // 100
    }

    #endregion

    #region Result

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valaszok
    {
        [XmlElement(Order = 0)]
        public Valasz[] Valasz;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valasz
    {
        [XmlElement(Order = 0)]
        public string KulsoAzonosito;

        [XmlElement(Order = 1)]
        public KuldemenyAzonosito KuldemenyAzonosito;

        [XmlElement(Order = 2)]
        public IktatoszamSorszam Iktatoszam;

        [XmlElement(Order = 3)]
        public KuldemenyVonalkodok Vonalkodok;

        [XmlElement(Order = 4)]
        public ValaszAdatok ValaszAdatok;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class KuldemenyAzonosito
    {
        public int Ev;
        [XmlIgnore] public bool EvSpecified;

        public int Sorszam;
        [XmlIgnore] public bool SorszamSpecified;

        public string PostakonyvKod;
    }

    #endregion
}

