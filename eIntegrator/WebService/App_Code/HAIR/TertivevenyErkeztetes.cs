﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Linq;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR.TertivevenyErkeztetes
{
    public static class Constants
    {
        public const string NAMESPACE = "Contentum.eIntegrator.WebService.HAIR.TertivevenyErkeztetes";
    }

    // [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]    
    //[XmlType(Namespace = Constants.NAMESPACE)]
    //public class Tertivevenyek
    //{
    //    [XmlElement(Order = 0)]
    //    public Tertiveveny[] Tertiveveny;
    //}

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Tertiveveny
    {
        #region Parameters

        [XmlElement(Order = 0)]
        public Alapadatok Alapadatok;
        [XmlElement(Order = 1)]
        public TertivevenyAdatok TertivevenyAdatok;

        #endregion

        #region Tértivevény érkeztetés

        // TERTI_VISSZA_KOD_POSTA => TERTIVEVENY_VISSZA_KOD
        private string GetTertivisszaKod(ExecParam execParam)
        {
            var kod = TertivevenyAdatok.KezbesitesVisszakuldesKod + TertivevenyAdatok.KezbesitesVisszakuldesAlkod;

            bool nincsItem;
            var tvKodok = KodtarFuggoseg.GetFuggoKodtarakKodList(execParam, "TERTI_VISSZA_KOD_POSTA", "TERTIVEVENY_VISSZA_KOD", kod, out nincsItem);
            if (nincsItem || tvKodok == null || tvKodok.Count == 0)
            {
                return null;
            }
            return tvKodok.First();
        }

        private EREC_KuldTertivevenyek FindTertiveveny(ExecParam execParam, ValaszAdatok valaszAdatok)
        {
            try
            {
                var hivatkozasiSzam = string.Format("HAIR_{0}", TertivevenyAdatok.KuldemenyAzonosito);
                Logger.Info(String.Format("GetTertiveveny kezdete: {0}", hivatkozasiSzam));

                var search = new EREC_KuldTertivevenyekSearch();
                search.Ragszam.Filter(hivatkozasiSzam);

                var service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                Result res = service.GetAll(execParam.Clone(), search);

                if (res.IsError)
                {
                    valaszAdatok.SetError(res);
                    return null;
                }

                if (res.GetCount == 0)
                {
                    Logger.Warn(String.Format("Nem található a tértivevény: {0}", hivatkozasiSzam));
                    valaszAdatok.SetError(ErrorCodes.ErkeztetesTertivevenyNemTalalhato);
                    return null;
                }

                if (res.GetCount > 1)
                {
                    Logger.Warn(String.Format("Több tértivevény található: {0}", hivatkozasiSzam));
                    valaszAdatok.SetError(ErrorCodes.ErkeztetesTobbTertivevenyTalalhato);
                    return null;
                }

                var tertiveveny = new EREC_KuldTertivevenyek();
                Utility.LoadBusinessDocumentFromDataRow(tertiveveny, res.Ds.Tables[0].Rows[0]);

                Logger.Info(String.Format("GetTertiveveny vege: {0}", hivatkozasiSzam));

                return tertiveveny;
            }
            catch (Exception x)
            {
                valaszAdatok.SetError(x);
                return null;
            }
        }

        private EREC_KuldTertivevenyek GetTertiveveny(ExecParam execParam, ValaszAdatok valaszAdatok)
        {
           try
           {
                // tértivissza kód
                var tertiVisszaKod = GetTertivisszaKod(execParam.Clone());
                if (String.IsNullOrEmpty(tertiVisszaKod))
                {
                    Helper.SetError(valaszAdatok, ErrorCodes.ErkeztetesNincsVisszakuldesKod);
                    return null;
                }

                // tértivevény keresése
                var tertivevenyObj = FindTertiveveny(execParam, valaszAdatok);
                if (tertivevenyObj == null)
                {
                    return null;
                }

                // tértivevény módosítása
                tertivevenyObj.Updated.SetValueAll(false);
                tertivevenyObj.Base.Updated.SetValueAll(false);

                tertivevenyObj.TertivisszaKod = tertiVisszaKod;
                tertivevenyObj.Updated.TertivisszaKod = true;

                //tertivevenyObj.Ragszam = TertivevenyAdatok.KuldemenyAzonosito;
                //tertivevenyObj.Updated.Ragszam = true;

                if (TertivevenyAdatok.TertiVisszaDatumSpecified)
                {
                    tertivevenyObj.TertivisszaDat = TertivevenyAdatok.TertiVisszaDatum.ToString();
                    tertivevenyObj.Updated.TertivisszaDat = true;
                }

                if (!String.IsNullOrEmpty(TertivevenyAdatok.AtvevoSzemely))
                {
                    tertivevenyObj.AtvevoSzemely = TertivevenyAdatok.AtvevoSzemely;
                    tertivevenyObj.Updated.AtvevoSzemely = true;
                }

                if (TertivevenyAdatok.AtvetelDatumSpecified)
                {
                    tertivevenyObj.AtvetelDat = TertivevenyAdatok.AtvetelDatum.ToString();
                    tertivevenyObj.Updated.AtvetelDat = true;
                }

                if (TertivevenyAdatok.KezbesitesiVelelemBeallt)
                {
                    tertivevenyObj.KezbVelelemBeallta = TertivevenyAdatok.KezbesitesiVelelemBeallt ? "1" : "0";
                    tertivevenyObj.Updated.KezbVelelemBeallta = true;
                }

                if (TertivevenyAdatok.KezbesitesiVelelemBealltanakDatumaSpecified)
                {
                    tertivevenyObj.KezbVelelemDatuma = TertivevenyAdatok.KezbesitesiVelelemBealltanakDatuma.ToString();
                    tertivevenyObj.Updated.KezbVelelemDatuma = true;
                }

                // átvétel jogcíme
                //tertivevenyObj.Base.Note = ;
                //tertivevenyObj.Base.Updated.Note = true;

                return tertivevenyObj;
            }
            catch (Exception x)
            {
                valaszAdatok.SetError(x);
                return null;
            }
        }

        private bool Validate(ValaszAdatok valaszAdatok)
        {
            if (String.IsNullOrEmpty(TertivevenyAdatok.KuldemenyAzonosito))
            {
                valaszAdatok.SetError(ErrorCodes.ErkeztetesNincsKuldemenyAzonosito);
                return false;
            }

            if (String.IsNullOrEmpty(TertivevenyAdatok.KezbesitesVisszakuldesKod))
            {
                valaszAdatok.SetError(ErrorCodes.ErkeztetesNincsKezbesitesVisszakuldesKod);
                return false;
            }

            if (String.IsNullOrEmpty(TertivevenyAdatok.KezbesitesVisszakuldesAlkod))
            {
                valaszAdatok.SetError(ErrorCodes.ErkeztetesNincsKezbesitesVisszakuldesAlkod);
                return false;
            }

            return true;
        }

        public void Erkeztetes(ExecParam execParam, Valasz valasz)
        {
            var v = valasz.ValaszAdatok;
            try
            {
                // paraméterek ellenőrzése
                if (!Validate(v))
                {
                    // hibakód már be van állítva
                    return;
                }

                // tértivevény objektum
                var tertiveveny = GetTertiveveny(execParam, v);
                if (tertiveveny == null)
                {
                    // hibakód már be van állítva
                    return;
                }

                // érkeztetés
                var service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                var result = service.TertivevenyErkeztetes(execParam.Clone(), tertiveveny.Id, tertiveveny);
                if (result.IsError)
                {
                    v.SetError(result);
                    return;
                }
            }
            catch (Exception x)
            {
                v.SetError(x);
            }
        }
        #endregion
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class TertivevenyAdatok
    {
        public string KuldemenyAzonosito; // 100
        public string KezbesitesVisszakuldesKod; // 1
        public string KezbesitesVisszakuldesAlkod; // 1

        public DateTime TertiVisszaDatum;
        [XmlIgnore] public bool TertiVisszaDatumSpecified;

        public string AtvevoSzemely; // 100

        public DateTime AtvetelDatum;
        [XmlIgnore] public bool AtvetelDatumSpecified;

        public bool KezbesitesiVelelemBeallt;

        public DateTime KezbesitesiVelelemBealltanakDatuma;
        [XmlIgnore] public bool KezbesitesiVelelemBealltanakDatumaSpecified;
    }

    #region Result

    [XmlType(Namespace = Constants.NAMESPACE)]
    // public class Valaszok : HAIR.Valaszok
    //{
    //}

    public class Valaszok
    {
        [XmlElement(Order = 0)]
        public Valasz[] Valasz;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valasz : HAIR.Valasz
    {
    }

    #endregion
}

