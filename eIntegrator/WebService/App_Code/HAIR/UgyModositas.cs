﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.HAIR.UgyModositas
{
    public static class Constants
    {
        public const string NAMESPACE = "Contentum.eIntegrator.WebService.HAIR.UgyModositas";
    }

    // [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]    
    //[XmlType(Namespace = Constants.NAMESPACE)]
    //public class Ugyek
    //{
    //    [XmlElement(Order = 0)]
    //    public Ugy[] Ugy;
    //}

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Ugy
    {
        #region Parameters

        public Alapadatok Alapadatok;
        public Iktatoszam Iktatoszam;

        public bool Elintezett;
        [XmlIgnore] public bool ElintezettSpecified;

        public string Targy; // 400
        public string Ugytipus; // 10

        public IrattariTetel IrattariTetel;
        public Partner Ugyfel;

        #endregion

        #region Ügyirat módosítás

        public void Modositas(ExecParam execParam, Valasz valasz)
        {
            var v = valasz.ValaszAdatok;
            try
            {
                // ügyirat keresése iktatószám alapján
                var ugyirat = Helper.GetUgyiratByIktatoszam(Iktatoszam, v);
                if (ugyirat == null)
                {
                    if (v.HibaKod == 0) // ha nincs megadva más hiba
                    {
                        Helper.SetError(v, ErrorCodes.UgyiratNotFound);
                    }
                    return;
                }

                var ugyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                // elintézett
                if (ElintezettSpecified && !Elintezett)
                {
                    if (ugyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart)
                    {
                        var result = ugyiratokService.LezarasVisszavonas(execParam.Clone(), ugyirat.Id);
                        if (result.IsError)
                        {
                            Helper.SetError(v, result);
                            return;
                        }
                    }
                    else
                    {
                        var result = ugyiratokService.AtvetelUgyintezesre(execParam.Clone(), ugyirat.Id);
                        if (result.IsError)
                        {
                            Helper.SetError(v, result);
                            return;
                        }                        
                    }

                    //Le kell kérnünk újra az ügyiratot, mert a LezarasVisszavonas vagy az AtvetelUgyintezesre módosította a verziószámát
                    Iktatas.Valasz iktValasz = new Iktatas.Valasz();
                    iktValasz.ValaszAdatok = new ValaszAdatok();

                    ugyirat = Helper.GetUgyiratById(ugyirat.Id, iktValasz);
                    if (Helper.IsError(iktValasz))
                    {
                        valasz.ValaszAdatok.HibaKod = iktValasz.ValaszAdatok.HibaKod;
                        valasz.ValaszAdatok.HibaUzenet = iktValasz.ValaszAdatok.HibaUzenet;
                        return;
                    }
                }

                // módosítható
                ErrorDetails errorDetails;
                var statusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(ugyirat);
                if (!Contentum.eRecord.BaseUtility.Ugyiratok.Modosithato(statusz, execParam.Clone(), out errorDetails))
                {
                    Helper.SetError(v, errorDetails.Message, errorDetails.Code);
                    return;
                }

                ugyirat.Updated.SetValueAll(false);
                ugyirat.Base.Updated.SetValueAll(false);

                // ügyirat tárgy módosítás
                if (!String.IsNullOrEmpty(Targy))
                {
                    ugyirat.Targy = Targy;
                    ugyirat.Updated.Targy = true;
                }

                // ügyirat irattári tétel módosítás
                if (IrattariTetel != null)
                {
                    Result irattariTetelResult = Helper.GetIrattariTetelResult(IrattariTetel);

                    if (irattariTetelResult.IsError)
                    {
                        Helper.SetError(v, irattariTetelResult);
                        return;
                    }

                    if (irattariTetelResult.GetCount > 0)
                    {
                        ugyirat.IraIrattariTetel_Id = irattariTetelResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                        ugyirat.Updated.IraIrattariTetel_Id = true;
                    }
                    else
                    {
                        Helper.SetError(v, ErrorCodes.IrattariTetelNotFound);
                        return;
                    }
                }

                // ügyirat ügytípus módosítás
                if (!String.IsNullOrEmpty(Ugytipus))
                {
                    ugyirat.UgyTipus = Ugytipus;
                    ugyirat.Updated.UgyTipus = true;
                }

                // ügyirat partner módosítás
                if (Ugyfel != null)
                {
                    // ügyindító
                    if (!string.IsNullOrEmpty(Ugyfel.PartnerAzonosito))
                    {
                        KRT_Partnerek partner = Helper.GetPartnerByKulsoAzonosito(Ugyfel.PartnerAzonosito, v);
                        if (!v.IsError() && partner != null)
                        {
                            ugyirat.Partner_Id_Ugyindito = partner.Id;
                            ugyirat.Updated.Partner_Id_Ugyindito = true;
                        }
                    }

                    // ügyindító cím
                    if (!string.IsNullOrEmpty(Ugyfel.PartnerCimAzonosito))
                    {
                        KRT_Cimek cim = Helper.GetCimByKulsoAzonosito(Ugyfel.PartnerCimAzonosito, v);

                        if (!v.IsError() && cim != null)
                        {
                            ugyirat.Cim_Id_Ugyindito = cim.Id;
                            ugyirat.Updated.Cim_Id_Ugyindito = true;
                        }
                    }

                    // partner név
                    if (!string.IsNullOrEmpty(Ugyfel.PartnerNev))
                    {
                        ugyirat.NevSTR_Ugyindito = Ugyfel.PartnerNev;
                        ugyirat.Updated.NevSTR_Ugyindito = true;
                    }

                    // partner cím
                    if (!string.IsNullOrEmpty(Ugyfel.PartnerCim))
                    {
                        ugyirat.CimSTR_Ugyindito = Ugyfel.PartnerCim;
                        ugyirat.Updated.CimSTR_Ugyindito = true;
                    }
                }

                // ügyirat módosítása
                var xp = execParam.Clone();
                xp.Record_Id = ugyirat.Id;
                ugyirat.Base.Updated.Ver = true;
                var resultUpdate = ugyiratokService.Update(xp, ugyirat);
                if (resultUpdate.IsError)
                {
                    Helper.SetError(v, resultUpdate);
                    return;
                }

                // elintézetté nyilvánítás
                if (ElintezettSpecified && Elintezett)
                {
                    var elintezesMod = "90"; // Egyéb
                    var result = ugyiratokService.ElintezetteNyilvanitas(execParam.Clone(), DateTime.Now.ToString(), elintezesMod, null);
                    if (result.IsError)
                    {
                        Helper.SetError(v, ErrorCodes.UgyiratNemElintezheto);
                        return;
                    }
                }
            }
            catch (Exception x)
            {
                v.SetError(x);
            }
        }

        #endregion
    }

    #region Result

    [XmlType(Namespace = Constants.NAMESPACE)]
    //public class Valaszok : HAIR.Valaszok
    //{
    //}
    public class Valaszok
    {
        [XmlElement(Order = 0)]
        public Valasz[] Valasz;
    }

    [XmlType(Namespace = Constants.NAMESPACE)]
    public class Valasz : HAIR.Valasz
    {
    }

    #endregion
}

