﻿using System;
using System.Web.Services;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using log4net;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using System.Reflection;
using System.DirectoryServices.AccountManagement;
using Contentum.eIntegrator.ADAdapter;
using Contentum.eIntegrator.Service;
using System.Web;
using System.Web.Script.Services;

[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public partial class INT_ADSyncService : System.Web.Services.WebService
{
    #region Varible declaration

    private DataContext dataContext;

    private static readonly ILog log = LogManager.GetLogger(typeof(Logger));

    public static string Modul_id;
    public static string Org_id; 

    public static PrincipalContext ADctx;

    public static WrappedADGroupCollection edokGroups;

    private static Contentum.eIntegrator.Service.INT_LogService service_Log = eIntegratorService.ServiceFactory.GetINT_LogService();

    //  private readonly ADAdapterTypes.GroupTypeEnum[] InvolvedGroupTypes = new ADAdapterTypes.GroupTypeEnum[]
    //{
    //              ADAdapterTypes.GroupTypeEnum.Org
    //    // Jelenleg csak szervezeteket kezelünk, később bővíthető az AD-s megvalósításnak megfelelően
    //    //ADAdapterTypes.GroupTypeEnum.Activity,
    //    //ADAdapterTypes.GroupTypeEnum.Comittee,
    //    //ADAdapterTypes.GroupTypeEnum.Project
    //};

    private readonly ADAdapterTypes.UserTypeEnum[] InvolvedUserTypes = new ADAdapterTypes.UserTypeEnum[]

         {
                ADAdapterTypes.UserTypeEnum.Internal,
                ADAdapterTypes.UserTypeEnum.Deputy    // Képviselő (pl. frakció tagok, de itt vannak a bizottsági titkárok is)
         };



    #endregion

    public INT_ADSyncService()
    {

        dataContext = new DataContext(this.Application);
        //   log4net.Config.XmlConfigurator.Configure();

        //    // A lista bővítésekor az Fph.AdajkSync.Adapter.Edok.Util osztály
        //    // ConvertToPartnerTipus és ConvertToAdajkGroupType metódusait is
        //    // módosítani kell!
        //    _InvolvedGroupTypes = new ADAdapter.GroupTypeEnum[]
        //    {
        //            GroupTypeEnum.Org,
        //            GroupTypeEnum.Activity,
        //            GroupTypeEnum.Comittee,
        //            GroupTypeEnum.Project
        //    };

        //Modul_id = "AC0EC5BB-E18A-E611-80BA-00155D020B4B";
        //Org_id = "450B510A-7CAA-46B0-83E3-18445C0C53A9";
    }

    //public INT_ADSyncService(DataContext _dataContext)
    //{
    //    this.dataContext = _dataContext;

    //    log4net.Config.XmlConfigurator.Configure();

    //    // A lista bővítésekor az Fph.AdajkSync.Adapter.Edok.Util osztály
    //    // ConvertToPartnerTipus és ConvertToAdajkGroupType metódusait is
    //    // módosítani kell!
    //    _InvolvedGroupTypes = new ADAdapter.GroupTypeEnum[]
    //    {
    //            GroupTypeEnum.Org,
    //            GroupTypeEnum.Activity,
    //            GroupTypeEnum.Comittee,
    //            GroupTypeEnum.Project
    //    };

    //    //  Modul_id = "AC0EC5BB - E18A - E611 - 80BA - 00155D020B4B";
    //}

    [WebMethod()]
    [ScriptMethod(UseHttpGet = true)]
    public Result ADSyncStart(String modulId, String orgId)
    {
        if (String.IsNullOrEmpty(modulId) || String.IsNullOrEmpty(orgId))
            throw new ResultException("Modul azonosító vagy Org nem került megadásra!");

        Modul_id = modulId;
        Org_id = orgId;


        ExecParam eParam = ADAdapterTypes.GetExecParam();
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(eParam, Context, GetType());

        bool isConnectionOpenHere = false;
        Result result = new Result();
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            if (IsOnGoingSync(Modul_id))
            {
                throw new ResultException("Van már elindított szinkronizáció, új nem indítható!");

            }
            else
            {

                ADctx = GetDirectoryEntry(Modul_id);
                StartFullSynchronization();
                //string ADServer_Name = @"LDAP://edoctest.local";
                //string AD_User = @"EDOCTEST\Administrator";
                //string AD_Psw = "Password123";
                //  DirectoryEntry d = new DirectoryEntry(ADServer_Name, AD_User, AD_Psw);

            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(eParam, result);
        return result;
    }


    //[WebMethod()]
    //public Result GetInactiveADGroups(GroupTypeEnum groupTypeEnum )
    //{

    //    ExecParam eParam = new ExecParam();
    //    Contentum.eUtility.Log log = Log.WsStart(eParam, Context, GetType());



    //    bool isConnectionOpenHere = false;
    //    Result result = new Result();
    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

    //        externalIDs_sp = new INT_ExternalIDsStoredProcedure(dataContext);
    //        result = extrenalIDs_sp.GetInactivADGroup(execParam, groupTypeEnum);

    //        return result;

    //    }
    //    catch (Exception e)
    //    {
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    log.WsEnd(eParam, result);
    //    return result;
    //}


    /// <summary>
    /// Van-e folyamatban lévő szinkronizáció adott modulhoz
    /// </summary>
    /// <param name="Modul_id"></param>
    /// <returns></returns>
    private Boolean IsOnGoingSync(String Modul_id)
    {
        ExecParam execParam = ADAdapterTypes.GetExecParam();
        INT_LogService service_Log = new INT_LogService(dataContext);

        INT_LogSearch search = new INT_LogSearch();

        search.Modul_id.Value = Modul_id;
        search.Modul_id.Operator = Contentum.eQuery.Query.Operators.equals;

    //    search.Sync_EndDate.Value = null;
        search.Sync_EndDate.Operator = Contentum.eQuery.Query.Operators.isnull;

        Result result = service_Log.GetAll(execParam, search);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            throw new ResultException(result);
        }


        return (result.Ds.Tables[0].Rows.Count > 0);

    }

    /// <summary>
    /// AD Connection 
    /// </summary>
    /// <param name="Modul_id"></param>
    /// <returns></returns>
    private PrincipalContext GetDirectoryEntry(String Modul_id)
    {
        INT_ParameterekService int_ParameterekService = new INT_ParameterekService(this.dataContext);
        ExecParam execParam = ADAdapterTypes.GetExecParam();

        INT_ParameterekSearch search = new INT_ParameterekSearch();

        search.Modul_id.Value = Modul_id;
        search.Modul_id.Operator = Contentum.eQuery.Query.Operators.equals;

        Result result = int_ParameterekService.GetAll(execParam, search);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            throw new ResultException(result);
        }

        string ADServer_Name = "";
        string AD_User = "";
        string AD_Psw = "";

        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            if (row["Nev"].ToString() == "AD_SERVER_ADDRESS")
                ADServer_Name = row["Ertek"].ToString();
            else if (row["Nev"].ToString() == "AD_USER")
                AD_User = row["Ertek"].ToString();
            else if (row["Nev"].ToString() == "AD_PSW")
                AD_Psw = row["Ertek"].ToString();
        }

        return new PrincipalContext(ContextType.Domain, ADServer_Name, AD_User, AD_Psw);
        // return new DirectoryEntry(ADServer_Name, AD_User, AD_Psw);
    }

    /// <summary>
    /// Szinkronizáció
    /// </summary>
    /// <returns></returns>
    public bool StartFullSynchronization()
    {
        log.DebugFormat("[{0}] START SYNCHRONIZATION", MethodBase.GetCurrentMethod().Name);

        String SyncId = StartLog();

        bool successful = false;
        // Jelenleg csak egy típust kezelünk
        // Előre felépítjük a szervezeti hierarchiát mert a userekhez is kell
        //edokGroups = ADAdapter.GetPartialADGroupHierarchy(ADAdapterTypes.GroupTypeEnum.Org);
        edokGroups = ADAdapter.GetPartialADGroupHierarchy();

        //try
        //{
        //TODO visszarakni
        // BLG_316
        //successful = SynchroniseAllGroup();
        //successful = successful & SynchroniseAllUser();
        //}
        //catch (Exception ex)
        //{
        //    log.Error(String.Format("[{0}]", MethodBase.GetCurrentMethod().Name), ex);
        //    successful = false;
        //}
        EndLog(SyncId, successful);
        log.DebugFormat("[{0}] END", MethodBase.GetCurrentMethod().Name);

        return successful;
    }

    /// <summary>
    /// Szinkronizációs logbejegyzés írása
    /// </summary>
    /// <returns></returns>
    private String StartLog ()
    {
     
        try
        {
            //                INT_ExternalIDsService service_ExternalIDs = new INT_ExternalIDsService(dataContext);

            // INT_ExternalIDs newAssocRecord = new INT_ExternalIDs();

            

                INT_Log record = new INT_Log();

            // [Id] ,[Org],[Modul_id],[Sync_StartDate],[Parancs],[Machine],[Sync_EndDate],[HibaKod],[HibaUzenet],[ErvKezd],[ErvVege]


            record.Org = Org_id;
            record.Modul_id = Modul_id;
            record.Sync_StartDate = DateTime.Now.ToString();
            record.Parancs = HttpContext.Current.Request.Url.ToString();
            record.Machine = System.Net.Dns.GetHostEntry(HttpContext.Current.Request.ServerVariables["remote_addr"]).HostName;
            record.ErvKezd = DateTime.Now.ToString();


            Result result = service_Log.Insert(ADAdapterTypes.GetExecParam(), record);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                return result.Uid;
            } else
            {
                log.ErrorFormat("[{0}]  Error: {1}, {2}", MethodBase.GetCurrentMethod().Name,result.ErrorCode, result.ErrorMessage);
                return null;
            }
        }
        catch (Exception e)
        {
            log.Error(String.Format("[{0}]  Error", MethodBase.GetCurrentMethod().Name), e);
            throw new Exception();
        }

    }

    /// <summary>
    /// Szinkronizációs logbejegyzés lezárása
    /// </summary>
    /// <param name="SyncId"></param>
    /// <param name="successful"></param>
    private void EndLog(String SyncId, bool successful)
    {

        try
        {
            //                INT_ExternalIDsService service_ExternalIDs = new INT_ExternalIDsService(dataContext);

            // INT_ExternalIDs newAssocRecord = new INT_ExternalIDs();

            ExecParam execParam = ADAdapterTypes.GetExecParam();
            execParam.Record_Id = SyncId;
            Result result = service_Log.Get(execParam);
            if (result != null)
            {
                INT_Log record = result.Record as INT_Log;
                // [Id] ,[Org],[Modul_id],[Sync_StartDate],[Parancs],[Machine],[Sync_EndDate],[HibaKod],[HibaUzenet],[ErvKezd],[ErvVege]
                record.Sync_EndDate = DateTime.Now.ToString();
                record.Updated.Sync_EndDate = true;
                record.HibaKod = successful ? ADAdapterTypes.State.OK.ToString() : ADAdapterTypes.State.Failed.ToString();
                record.Updated.HibaKod = true;

                ExecParam exp = ADAdapterTypes.GetExecParam();
                exp.Record_Id = SyncId;
                result = service_Log.Update(exp, record);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    log.ErrorFormat("[{0}]  Error: {1}, {2}", MethodBase.GetCurrentMethod().Name, result.ErrorCode, result.ErrorMessage);
                    
                }
            }
        }
        catch (Exception e)
        {
            log.Error(String.Format("[{0}]  Error", MethodBase.GetCurrentMethod().Name), e);
            
        }

    }


    /// <summary>
    /// Szervezetek szinkronizációja
    /// </summary>
    /// <returns></returns>
    private bool SynchroniseAllGroup()
    {
        log.DebugFormat("[{0}] START", MethodBase.GetCurrentMethod().Name);

        bool isSuccessful = true;
        //string groupTypeName;

        //for (int i = 0; i < InvolvedGroupTypes.Length; i++)
        //{
        //    groupTypeName = Enum.GetName(typeof(ADAdapterTypes.GroupTypeEnum), InvolvedGroupTypes[i]);


        //    log.Debug(String.Format("{0} csoportok feldolgozása megkezdve.", groupTypeName));

        for (int j = 0; j < edokGroups.Count; j++)
        {
            isSuccessful = isSuccessful & SynchronizeGroup(edokGroups[j]);
        }
        //   }

        //   log.Debug(String.Format("{0} csoportok feldolgozása befejeződött.", groupTypeName));
        //        }

        log.DebugFormat("[{0}] END", MethodBase.GetCurrentMethod().Name);
        return isSuccessful;
    }

    /// <summary>
    /// Felhasználók szinkronizációja
    /// </summary>
    /// <returns></returns>
    private bool SynchroniseAllUser()
    {
        log.DebugFormat("[{0}]", MethodBase.GetCurrentMethod().Name);

        bool isSuccessful = true;

        // Jelenleg csak egy típust kezelünk (AD-ban mi különböztetné meg??) 
        //for (int i = 0; i < InvolvedUserTypes.Length; i++)
        //{

        //    string userTypeName = Enum.GetName(typeof(ADAdapterTypes.UserTypeEnum), InvolvedUserTypes[i]);
        //    //    log.DebugFormat("[{0}] {1} felhasználói kör feldolgozása megkezdve.", MethodBase.GetCurrentMethod().Name, userTypeName);

        //    string allLetters = "abcdefghijklmnopqrstuvwxyz";   // egyszerre csak kisebb darabokat tárolunk a memóriában
        //for (int abc = 0; abc < allLetters.Length; abc++)
        //{
        WrappedADUserCollection ADUserList = ADAdapter.GetADUserSubSet();
        //AdajkAdapter.GetAdajkUserSubSet(_InvolvedUserTypes[i], allLetters[abc]);
        //ADAdapter.GetADUserSubSet(InvolvedUserTypes[i]);

        //   WrappedADGroupCollection edokGroups = ADAdapter.GetPartialADGroupHierarchy(ADAdapterTypes.GroupTypeEnum.Org);

        WrappedADUserCollection deletedUsers = null;
        try
        {
            //TODO: amint az adajk képes visszaadni a törölt elemeket, ezt az adajkadapter kell végezze!
            //deletedOUs = AdajkAdapter.GetInactiveAdajkGroups(_InvolvedGroupTypes[i]);
            //deletedUsers = GetInactiveADUsers(InvolvedUserTypes[i].ToString());
            deletedUsers = ADAdapter.GetInactiveADUsers();
        }
        catch (Exception e)
        {

            log.Error(String.Format("[{0}] Hiba a törölt felhasználók feldolgozása közben!", MethodBase.GetCurrentMethod().Name), e);
            throw new Exception("Hiba a törölt felhasználók feldolgozásánál!");


        }
        if (ADUserList != null)
        {
            ADUserList.AddRange(deletedUsers);   // hozzáadjuk az inaktív felhasználókat
            for (int j = 0; j < ADUserList.Count; j++)
            {
                isSuccessful = isSuccessful & SynchronizeUser(ADUserList[j]);
            }
        }
        //}
        //    log.DebugFormat("[{0}] {1} felhasználói kör feldolgozása befejezve.", MethodBase.GetCurrentMethod().Name, userTypeName);
        //}

        log.DebugFormat("[{0}] END Eredmény: {1} ", MethodBase.GetCurrentMethod().Name, isSuccessful);
        return isSuccessful;
    }

    ///// <summary>
    ///// Inaktivált szervezeti egységek listájának lekérdezése.
    ///// </summary>
    ///// <param name="groupTypeID">ADAJK GroupTypeRef érték.</param>
    ///// <returns>Az adott típusú, inaktivált group objektumok.</returns>
    ///// <exception cref="DatabaseException">
    ///// Adatkapcsolati hiba esetén veti 
    ///// az IDataAccessAdapter típusú osztály.
    ///// </exception>
    //public WrappedADGroupCollection GetInactiveADGroups(string groupTypeName)
    //{
    //    log.Debug("GetInactiveADGroups Start ");

    //    ExecParam execParam = ADAdapterTypes.GetExecParam();
    //    // INT_ExternalIDsService service_ExtIds = new INT_ExternalIDsService(dataContext);
    //    Contentum.eIntegrator.Service.INT_ExternalIDsService service_ExtIds = eIntegratorService.ServiceFactory.GetINT_ExternalIDsService();

    //    INT_ExternalIDsSearch search = new INT_ExternalIDsSearch();

    //    search.Modul_Id.Value = Modul_id.ToString();
    //    search.Modul_Id.Operator = Contentum.eQuery.Query.Operators.equals;

    //    search.External_Group.Value = groupTypeName;
    //    search.External_Group.Operator = Contentum.eQuery.Query.Operators.equals;

    //    Result result = new Result();
    //    WrappedADGroupCollection deletedGroups = new WrappedADGroupCollection();

    //    result = service_ExtIds.GetAll(execParam, search);

    //    if (!string.IsNullOrEmpty(result.ErrorCode))
    //    {
    //        throw new ResultException(result);
    //    }

    //    if (result.Ds != null)
    //    {
    //        WrappedADGroup group;

    //        foreach (DataRow row in result.Ds.Tables[0].Rows)
    //        {
    //            GroupPrincipal srcGroup = GroupPrincipal.FindByIdentity(INT_ADSyncService.ADctx, row["External_Id"].ToString());
    //            if (srcGroup == null)
    //            {
    //                group = new WrappedADGroup();
    //                group.ADProp.ObjectID = new Guid(row["External_Id"].ToString());
    //                group.Deleted = true; // (row["Deleted"].ToString() == "1");
    //                group.ADProp.GroupType = row["External_Group"].ToString();
    //                // group.Name = groups[i].cName.Value;
    //                // group.Created = groups[i].dCreated.Value;
    //                deletedGroups.Add(group);
    //            }
    //        }
    //    }

    //    return deletedGroups;

    //}


    /// <summary>
    /// Szervezeti egység adatainak szinkronizálása EDOKban.
    /// </summary>
    /// <param name="wrappedGroup">Szervezeti egység adatai.</param>
    /// <returns>Igaz, ha a művelet sikeresen lezajlott.</returns>
    /// <remarks>Rekurzív metódus.</remarks>
    public bool SynchronizeGroup(WrappedADGroup wrappedGroup)
    {
        if (wrappedGroup == null)
            return false;

        // TODO Ez mi????
        //if (wrappedGroup.Name == "PR Csoport")
        //    Console.WriteLine();
        //log.DebugFormat("[{0}] Szervezet (AD): {0} ({1})", MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.ADProp.ObjectID);

        bool succeeded = false;

        try
        {
            succeeded = DoCrudOperation(wrappedGroup);
            if (succeeded)
            {
                //if (wrappedGroup.GroupMembers != null)
                //    for (int subGroupsIndex = 0; subGroupsIndex < wrappedGroup.GroupMembers.Count; subGroupsIndex++)
                //        SynchronizeGroup(wrappedGroup.GroupMembers[subGroupsIndex]);
            }
            else
            {
                INT_ExternalIDs association = AssociationHandler.GetAssociationByADObjectID(wrappedGroup.ADProp.ObjectID);
                association.State = ADAdapterTypes.State.Failed.ToString();
                association.LastSync = DateTime.Now.ToString();
                AssociationHandler.UpdateAssociation(association);
            }
        }
        catch (Exception e)
        {
            succeeded = false;
            log.Error(String.Format("[{0}] Hiba a Szervezet feldolgozásnál! AD groupId: {1}", MethodBase.GetCurrentMethod().Name, wrappedGroup.ADProp.ObjectID), e);

        }

        //log.DebugFormat("[{0}] END AD GroupId: {1}, Status: {2}", MethodBase.GetCurrentMethod().Name, wrappedGroup.ADProp.ObjectID, succeeded);
        return succeeded;
    }


    /// <summary>
    /// Szervezet CRUD
    /// </summary>
    /// <param name="wrappedGroup"></param>
    /// <returns></returns>
    private bool DoCrudOperation(WrappedADGroup wrappedGroup)
    {
        //log.Debug(String.Format("[{0}] {1}({2}) AD Group feldolgozás Start ",
        //    MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.ADProp.ObjectID));

        bool succeeded = true;

        INT_ExternalIDs association = AssociationHandler.GetAssociationByADObjectID(wrappedGroup.ADProp.ObjectID);

        if (!wrappedGroup.Deleted)
        {
            SetEdokParentIDOfGroup(ref wrappedGroup);

            if (association == null)
            {
                // INSERT
                succeeded = succeeded && InsertRequired(wrappedGroup);
            }
            else
            {
                if (!String.IsNullOrEmpty(association.Edok_Id))
                {
                    wrappedGroup.EDOKProp.ID = association.Edok_Id;

                    // UPDATE

                    WrappedADGroup synchedGroup = PartnerHandler.GetPartner(association.Edok_Id);
                    if (synchedGroup == null) //&& EnvironmentFactory.Configuration.IsSimulation == false)
                    {
                        log.ErrorFormat("A kapcsolt rendszer már nem tartalmazza a korábban szinkronizált rekordot: {0} ({1}), volt külső azonosító: {2}. A rekord újra létrehozásra kerül.",
                            wrappedGroup.Name, wrappedGroup.ADProp.ObjectID, association.Edok_Id);

                        succeeded = succeeded && AssociationHandler.DeleteAssociation(association.Id);
                        succeeded = succeeded && InsertRequired(wrappedGroup);
                    }
                    else
                    {
                        synchedGroup.ADProp = wrappedGroup.ADProp;

                        if (!wrappedGroup.Equals(synchedGroup))
                        {
                            SetEdokParentIDOfGroup(ref synchedGroup);
                            succeeded = succeeded && PartnerHandler.UpdatePartner(synchedGroup);
                            // association.Edok_ExpiredDate = AssociationHandler.MaxDate;
                            association.State = ADAdapterTypes.State.OK.ToString();
                            association.LastSync = DateTime.Now.ToString();
                            succeeded = succeeded && AssociationHandler.UpdateAssociation(association);
                            //}

                            if (succeeded)
                            {
                                log.DebugFormat("[{0}] Szervezeti egység módosult: Partner: {1} ({2}, AD: {3})", MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.EDOKProp.ID, wrappedGroup.ADProp.ObjectID);

                            }
                            else
                            {
                                log.ErrorFormat("[{0}] A rekord módosítás sikertelen! Partner: {1} ({2}, AD: {3})", MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.EDOKProp.ID, wrappedGroup.ADProp.ObjectID);
                            }

                        }
                    }
                }
            }

        }
        else
        {
            // INVALIDATE
            if (association != null)
            {
                WrappedADGroup synchedGroup = PartnerHandler.GetPartner(association.Edok_Id);
                if (!synchedGroup.Deleted)
                {
                    //  Delete ideiglenesen kiszedve
                    // BLG_316
                    //succeeded = PartnerHandler.InvalidatePartner(association.Edok_Id);
                    //association.Edok_ExpiredDate = DateTime.Now.ToString();
                    //association.State = succeeded ? ADAdapterTypes.State.OK.ToString() : ADAdapterTypes.State.Failed.ToString();
                    //association.Deleted = "1";
                    //succeeded = succeeded && AssociationHandler.UpdateAssociation(association);

                    //if (succeeded)
                    //{

                    //    log.DebugFormat("[{0}] A rekord invalidálva lett: Partner: {1} ({2}, AD: {3})", MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.EDOKProp.ID, wrappedGroup.ADProp.ObjectID);
                    //}
                    //else
                    //{
                    //    log.ErrorFormat("[{0}] A rekord invalidálás sikertelen! Partner: {1} ({2}, AD: {3})", MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.EDOKProp.ID, wrappedGroup.ADProp.ObjectID);
                    //}
                }
            }
        }
        //log.Debug(String.Format("[{0}] {1}({2}) AD Group feldolgozás END ",
        //        MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.ADProp.ObjectID));
        return succeeded;
    }

    /// <summary>
    /// EDOK Szervezet fölérendelt (szülő) szervezet beállítás
    /// </summary>
    /// <param name="wrappedGroup"></param>
    private void SetEdokParentIDOfGroup(ref WrappedADGroup wrappedGroup)
    {
        if (wrappedGroup.ADProp.ParentGroupID != null)
        {
            INT_ExternalIDs association = AssociationHandler.GetAssociationByADObjectID(wrappedGroup.ADProp.ParentGroupID);
            if (association != null)
                wrappedGroup.EDOKProp.ParentGroupID = association.Edok_Id;
        }
    }

    /// <summary>
    /// Szervezet beszúrás
    /// </summary>
    /// <param name="wrappedGroup"></param>
    /// <returns></returns>
    private bool InsertRequired(WrappedADGroup wrappedGroup)
    {
        //try
        //{
        INT_ExternalIDs assocRecord;
        bool isSuccessful;
        string newEdokID = PartnerHandler.InsertPartner(wrappedGroup);
        if (String.IsNullOrEmpty(newEdokID))
        {
            assocRecord = AssociationHandler.CreateAssocRecord(wrappedGroup, null, ADAdapterTypes.State.Failed);
            isSuccessful = AssociationHandler.InsertAssociation(assocRecord);

            log.ErrorFormat("[{0}] Hiba a szervezet létrehozásnál! Szervezet (AD) : {1} ({2}), AssocRecord id: {3}", 
                MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.ADProp.ObjectID, assocRecord.Id);
            //throw new ApplicationException("A külső alkalmazás érvénytelen rekordazonosítót adott vissza!");
        }
        else
        {

            assocRecord = AssociationHandler.CreateAssocRecord(wrappedGroup, newEdokID, ADAdapterTypes.State.OK);
            wrappedGroup.EDOKProp.ID = newEdokID;

            isSuccessful = AssociationHandler.InsertAssociation(assocRecord);

            log.InfoFormat("[{0}] Új szervezeti egység jött létre: {1} ({2}), AssocRecord Id: {3}", 
                MethodBase.GetCurrentMethod().Name, wrappedGroup.Name, wrappedGroup.EDOKProp.ID, assocRecord.Id);
        }
        return isSuccessful;


    }

    /// <summary>
    /// User beszúrás
    /// </summary>
    /// <param name="wrappedUser"></param>
    /// <returns></returns>
    private bool InsertRequired(WrappedADUser wrappedUser)
    {
        //try
        //{
        INT_ExternalIDs assocRecord;
        bool isSuccessful;
        string newEdokID = FelhasznalokHandler.InsertFelhasznalo(wrappedUser);
        if (String.IsNullOrEmpty(newEdokID))
        {
            assocRecord= AssociationHandler.CreateAssocRecord(wrappedUser, "unknown", ADAdapterTypes.State.Failed);
            log.ErrorFormat("[{0}] Hiba a felhasználó létrehozásánál! Felhasználó (AD): {1} ({2}) AssocRec Id: {3}",
                MethodBase.GetCurrentMethod().Name, wrappedUser.FullName, wrappedUser.ADProp.ObjectID, assocRecord.Id);
            //throw new ApplicationException();
        }

         assocRecord = AssociationHandler.CreateAssocRecord(wrappedUser, newEdokID, ADAdapterTypes.State.OK);
        wrappedUser.EdokProp.ID = newEdokID;

        isSuccessful = AssociationHandler.InsertAssociation(assocRecord);

        log.InfoFormat("[{0}] Új felhasználó jött létre: {1} ({2}), AssocRec id: {3}", 
            MethodBase.GetCurrentMethod().Name, wrappedUser.FullName, wrappedUser.EdokProp.ID, assocRecord.Id);

        return isSuccessful;
    }

  


    /// <summary>
    /// Személy adatainak szinkronizálása a EDOKban.
    /// </summary>
    /// <param name="wrappedUser">Személy adatai.</param>
    /// <returns>Igaz, ha a művelet sikeresen lezajlott.</returns>
    public bool SynchronizeUser(WrappedADUser wrappedUser)
    {
        if (wrappedUser == null)
            return false;
        //log.DebugFormat("[{0}] Start Felhasználó: {1}", MethodBase.GetCurrentMethod().Name, wrappedUser.FullName);

        bool succeeded = false;

        if (wrappedUser.IsActive)
        {
            // TODO: Jelenleg Role-t nem kezelünk (tesztelve sincs, a megvalósítási is tisztázatlan
            //   ADAdapter.SetRoleMembershipProperties(ref wrappedUser);

            ADAdapter.SetGroupMembershipProperties(ref wrappedUser);

            if (wrappedUser.ADProp.ManagedGroupCount > 1)
            {

                log.ErrorFormat("[{0}] A felhasználó ({1}) több szervezeti egységnek is vezetője!", MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID);

            }

            SetGroupEdokIDsOfUser(wrappedUser);
            SetManagedGroupEdokID(wrappedUser);
        }


        try
        {
            succeeded = DoCrudOperation(wrappedUser);
            if (!succeeded)
            {
                INT_ExternalIDs association = AssociationHandler.GetAssociationByADObjectID(wrappedUser.ADProp.ObjectID);
                if (association != null)
                {
                    association.State = ADAdapterTypes.State.Failed.ToString();
                    association.LastSync = DateTime.Now.ToString();
                    AssociationHandler.UpdateAssociation(association);
                }
            }
        }
        catch (Exception ex)
        {
            succeeded = false;
            log.Error(String.Format("[{0}] Hiba a Felhasználó feldolgozásnál! AD UserId: {1}", MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID), ex);

        }

        //log.DebugFormat("[{0}] End AD GroupId: {1}, Status: {2}", MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID, succeeded);
        return succeeded;

    }

    /// <summary>
    /// Felhasználóhoz rendelt szervezetek
    /// </summary>
    /// <param name="wrappedUser"></param>
    private void SetGroupEdokIDsOfUser(WrappedADUser wrappedUser)
    {
        for (int i = 0; i < wrappedUser.GroupMembership.Count; i++)
        {
            WrappedADGroup wrappedGroup = wrappedUser.GroupMembership[i];

            INT_ExternalIDs association = AssociationHandler.GetAssociationByADObjectID(wrappedGroup.ADProp.ObjectID);
            if (association != null)
                wrappedUser.GroupMembership[i].EDOKProp.ID = association.Edok_Id;

            SetEdokParentIDOfGroup(ref wrappedGroup);
        }
    }

    /// <summary>
    /// Felhasználó által vezetett szervezet
    /// </summary>
    /// <param name="wrappedUser"></param>
    private void SetManagedGroupEdokID(WrappedADUser wrappedUser)
    {
        if (wrappedUser.ADProp.ManagedGroupID != null)
        {
            INT_ExternalIDs association = AssociationHandler.GetAssociationByADObjectID(wrappedUser.ADProp.ManagedGroupID);
            if (association != null)
                wrappedUser.EdokProp.ManagedGroupID = association.Edok_Id;
        }
    }


    /// <summary>
    /// User CRUD
    /// </summary>
    /// <param name="wrappedUser"></param>
    /// <returns></returns>
    private bool DoCrudOperation(WrappedADUser wrappedUser)
    {
        //log.DebugFormat("[{0}] Start AD UserId: {1}", MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID);

        bool succeeded = true;

        INT_ExternalIDs association = AssociationHandler.GetAssociationByADObjectID(wrappedUser.ADProp.ObjectID);

        //if (IsIgnoranceRequired(association, wrappedUser.FullName,
        //    Convert.ToString(wrappedUser.Adajk.ObjectID), adapter.ApplicationName))
        //{
        //    TraceCallReturnEvent.Raise(true);
        //    return true;
        //}

        if (wrappedUser.IsActive)
        {
            if (association == null)
            {
                // INSERT
                //     if (!EnvironmentFactory.Configuration.IsSimulation)
                succeeded = succeeded && InsertRequired(wrappedUser);
            }
            else
            {
                // UPDATE

                wrappedUser.EdokProp.ID = association.Edok_Id;
                WrappedADUser synchedUser = null;
                if (!String.IsNullOrEmpty(association.Edok_Id))
                {
   
                    KRT_Felhasznalok felh = FelhasznalokHandler.GetFelhasznalo(association.Edok_Id);
                    FelhasznalokHandler.MapToWrappedUser(ref synchedUser, felh);
                    if (synchedUser == null)
                    {
                        log.ErrorFormat("[{0}] Az Edok rendszer már nem tartalmazza a korábban szinkronizált rekordot: {1}, AD azonosító: {2}, AssocRec Id: {3}. A rekord újra létrehozásra kerül.",
                       wrappedUser.FullName, wrappedUser.ADProp.ObjectID, association.Edok_Id);

                        succeeded = succeeded && AssociationHandler.DeleteAssociation(association.Id);
                        succeeded = succeeded && InsertRequired(wrappedUser);
                    }
                    else
                    {
                        if (!wrappedUser.Equals(synchedUser))
                        {
                            succeeded = succeeded && FelhasznalokHandler.UpdateFelhasznalo(wrappedUser);
                            association.Edok_ExpiredDate = DateTime.MaxValue.ToString();
                            association.State = ADAdapterTypes.State.OK.ToString();
                            succeeded = succeeded && AssociationHandler.UpdateAssociation(association);

                            if (succeeded)
                            {
                                log.DebugFormat("[{0}] Felhasználó módosult: Felhasználó: {1} ({2}, AssocRec Id: {3})"
                                , MethodBase.GetCurrentMethod().Name, wrappedUser.FullName, wrappedUser.EdokProp.ID, association.Id);
                            }
                            else
                            {
                                log.ErrorFormat("[{0}] A felhasználó módosítás sikertelen! Felhasználó: {1} ({2}, AssocRec Id: {3})"
                                    , MethodBase.GetCurrentMethod().Name, wrappedUser.FullName, wrappedUser.EdokProp.ID, association.Id);
                            }
                        }
                    }
                }
            }
        }
        else
        {
            // INVALIDATE
            if (association != null)
            {
                string assocEdokId = association.Edok_Id;
                WrappedADUser synchedUser = null;
                FelhasznalokHandler.MapToWrappedUser(ref synchedUser, FelhasznalokHandler.GetFelhasznalo(assocEdokId));
                if (synchedUser != null)
                {
                    if (synchedUser.Enabled)
                    {
                       
                        succeeded = succeeded && FelhasznalokHandler.InvalidateFelhasznalo(assocEdokId);
                        association.Edok_ExpiredDate = DateTime.MaxValue.ToString();
                        association.State = ADAdapterTypes.State.OK.ToString();
                        succeeded = succeeded && AssociationHandler.UpdateAssociation(association);
                        //}
                        if (succeeded)
                        {
                            log.DebugFormat("[{0}] Felhasználó érvénytelenítve: Felhasználó: {1} ({2}, AssocRec Id: {3})"
                            , MethodBase.GetCurrentMethod().Name, wrappedUser.FullName, wrappedUser.EdokProp.ID, association.Id);
                        }
                        else
                        {
                            log.ErrorFormat("[{0}] A felhasználó érvénytelenítés sikertelen! Felhasználó: {1} ({2}, AssocRec Id: {3})"
                                , MethodBase.GetCurrentMethod().Name, wrappedUser.FullName, wrappedUser.EdokProp.ID, association.Id);
                        }


                    }
                }
            }
        }


        //log.DebugFormat("[{0}] END AD UserId: {1}", MethodBase.GetCurrentMethod().Name, wrappedUser.ADProp.ObjectID);

        return succeeded;
    }
}
