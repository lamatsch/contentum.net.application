﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using System.Linq;

using Contentum.eUtility;
using System.Web;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using Contentum.eRecord.Service;
using Contentum.eIntegrator.Service;
using Contentum.eAdmin.Service;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using Contentum.eUIControls;
using eUzenet.HKP_KRService.Core;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eIntegrator.Service.INT_ASPADO.Document;
using Contentum.eIntegrator.Service.INT_ASPADO;
public partial class INT_ASPADO_DocumentService : System.Web.Services.WebService, IASPASP_DocumentService
{
    public SoapUnknownHeader[] headers;

    #region BatchFiling

    public ExecParam globalExecParam;


    List<IktatoObjektum> iktatoDocuments;
    List<IktatoObjektum> badIktatoDocuments;
    Dictionary<string, Result> iktatoKonyvekCache = new Dictionary<string, Result>();
    Dictionary<string, Result> ugyiratokCache = new Dictionary<string, Result>();
    Dictionary<string, KRT_Partnerek> partnerekCache = new Dictionary<string, KRT_Partnerek>();
    Dictionary<string, KRT_Cimek> cimekCache = new Dictionary<string, KRT_Cimek>();
    string localUser;
    int prioritas = 2;

    TomegesIktatasHelper _helper;

    #region Helper classes

    private class IktatoObjektum : IktatoObjektumBase
    {
        public override int GetFoszam()
        {
            return Document.RegistrationNumber.MainNumber;
        }

        public override int GetIratIrany()
        {

            if (Document.Direction == EnumsDirection.Inner)
            {
                return 2;
            }
            else if (Document.Direction == EnumsDirection.In)
            {
                return 1;
            }
            return 0;
        }

        public IktatoObjektum()
        {
            IktatoKonyvId = "-1";
            CsoportId = -1;
        }

        public IktatoObjektum(BatchFilingDocumentDto document, string folyamatId, string mainForrasAzonosito, string errorMessage)
        {
            this.Document = document;

            this.Tetel = new EREC_TomegesIktatasTetelek();
            this.Tetel.Updated.SetValueAll(false);
            this.Tetel.Base.Updated.SetValueAll(false);

            this.Tetel.Folyamat_Id = folyamatId;
            this.Tetel.Updated.Folyamat_Id = true;

            this.Tetel.Forras_Azonosito = mainForrasAzonosito + document.Id.ToString();
            this.Tetel.Updated.Forras_Azonosito = true;

            this.Tetel.Base.Note = document.Id.ToString();
            this.Tetel.Base.Updated.Note = true;

            this.Tetel.Allapot = "9";
            this.Tetel.Updated.Allapot = true;

            this.Tetel.Hiba = errorMessage;
            this.Tetel.Updated.Hiba = true;

            this.ErrorMessage = errorMessage;

        }
        public IktatoObjektum(string iktatoKonyvId, BatchFilingDocumentDto document, string folyamatId, string mainForrasAzonosito)
        {
            this.IktatoKonyvId = iktatoKonyvId;
            this.Document = document;

            this.Tetel = new EREC_TomegesIktatasTetelek();
            this.Tetel.Updated.SetValueAll(false);
            this.Tetel.Base.Updated.SetValueAll(false);

            this.Tetel.Folyamat_Id = folyamatId;
            this.Tetel.Updated.Folyamat_Id = true;

            this.Tetel.Forras_Azonosito = mainForrasAzonosito + document.Id.ToString();
            this.Tetel.Updated.Forras_Azonosito = true;

            this.Tetel.Base.Note = document.Id.ToString();
            this.Tetel.Base.Updated.Note = true;

            this.Tetel.Allapot = "1";
            this.Tetel.Updated.Allapot = true;

        }
        public BatchFilingDocumentDto Document;
        public EREC_TomegesIktatasTetelek Tetel;
        private string errorMessage;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }

            set
            {
                errorMessage = value;
                if (this.Tetel != null)
                {
                    this.Tetel.Hiba = value;
                    this.Tetel.Updated.Hiba = true;

                    this.Tetel.Allapot = "9";
                    this.Tetel.Updated.Allapot = true;
                }
            }
        }
    }

    #endregion


    public BatchFilingDocumentResponse BatchFiling(BatchFilingDocumentRequest documentListRequest)
    {
        Logger.Debug("DocumentService.BatchFiling.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        BatchFilingDocumentResponse response = new BatchFilingDocumentResponse();

        try
        {

            if (documentListRequest == null)
            {
                return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
            }

            localUser = userId.GetMappedUserId();

            if (string.IsNullOrEmpty(localUser))
            {
                return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
            }

            EREC_TomegesIktatasTetelekService tomegesIktatasTetelekService = eRecordService.ServiceFactory.GetEREC_TomegesIktatasTetelekService();
            ExecParam tExecParam = ASPADOHelper.GetExecParam(localUser);
            EREC_TomegesIktatasTetelekSearch tSearch = new EREC_TomegesIktatasTetelekSearch();
            tSearch.Forras_Azonosito.In(documentListRequest.BatchFilingDocuments.Select(x => x.Id.ToString()));

            /*
            Result existingTetelekResult = tomegesIktatasTetelekService.GetAll(tExecParam, tSearch);

            if (existingTetelekResult.IsError)
            {
                return response.SetErrorResponse(existingTetelekResult.ErrorMessage);
            }
            List<string> existingTetelek = new List<string>();
            if (existingTetelekResult.Ds.Tables[0].Rows.Count > 0)
            {
                existingTetelek.AddRange(existingTetelekResult.Ds.Tables[0].AsEnumerable().Select(x => x["Forras_Azonosito"].ToString()));
            }
            */

            iktatoDocuments = new List<IktatoObjektum>();
            badIktatoDocuments = new List<IktatoObjektum>();
            Result iktatoKonyvekResult;
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            globalExecParam = ASPADOHelper.GetExecParam(localUser);

            #region Folyamat Insert

            _helper = new TomegesIktatasHelper(globalExecParam, null);
            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Stream stream = ASPADOHelper.GetHttpRequestStream();
            Result uploadResult = _helper.FileUpload(execParam, stream, "RQ_Dokumentum", "");
            ExecParam execParam_Folyamat = ASPADOHelper.GetExecParam(localUser);

            Result folyamatInsert = _helper.Folyamat.Insert(execParam_Folyamat, "ASPADO", prioritas, uploadResult);

            if (folyamatInsert.IsError)
            {
                return response.SetErrorResponse(folyamatInsert.ErrorMessage);
            }

            var folyamatId = _helper.Folyamat.Id;

            #endregion

            #region Get IktatoKonyvIds

            for (int i = 0; i < documentListRequest.BatchFilingDocuments.Length; i++)
            {
                DocumentResponse tempResponse = new DocumentResponse();

                BatchFilingDocumentDto tempDoc = documentListRequest.BatchFilingDocuments[i];

                //    if (!existingTetelek.Contains(tempDoc.Id.ToString()))
                //    {
                if (tempDoc.RegistrationNumber != null && tempDoc.RegistrationNumber.RegistrationBook != null && tempDoc.RegistrationNumber.RegistrationBook.RegistrationBookId != 0)
                {
                    var paddedId = ASPADOHelper.PadToIktatokonyvAzonosito(tempDoc.RegistrationNumber.RegistrationBook.RegistrationBookId.ToString());

                    if (iktatoKonyvekCache.Any(x => x.Key == paddedId))
                    {
                        iktatoKonyvekResult = iktatoKonyvekCache[paddedId];
                    }
                    else
                    {
                        iktatoKonyvekResult = tempDoc.GetIktatokonyvFromRequestForFiling(tempResponse, localUser);
                    }
                }
                else
                {
                    iktatoKonyvekResult = tempDoc.GetIktatokonyvFromRequestForFiling(tempResponse, localUser);
                }

                var forrasAzonosito = _helper.Folyamat.Forras_Azonosito.ToString();
                if (!string.IsNullOrEmpty(tempResponse.ErrorMessage))
                {
                    badIktatoDocuments.Add(new IktatoObjektum(tempDoc, folyamatId, forrasAzonosito, tempResponse.ErrorMessage));
                }
                else if (iktatoKonyvekResult.GetCount == 0)
                {
                    badIktatoDocuments.Add(new IktatoObjektum(tempDoc, folyamatId, forrasAzonosito, "Nem található iktatókönyv"));
                }
                else
                {
                    iktatoDocuments.Add(new IktatoObjektum(iktatoKonyvekResult.Ds.Tables[0].Rows[0]["Id"].ToString(), tempDoc, folyamatId, forrasAzonosito));
                    var paddedId = ASPADOHelper.PadToIktatokonyvAzonosito(iktatoKonyvekResult.Ds.Tables[0].Rows[0]["Azonosito"].ToString());

                    if (!iktatoKonyvekCache.Any(x => x.Key == paddedId))
                    {
                        iktatoKonyvekCache.Add(paddedId, iktatoKonyvekResult);
                    }

                }
                //    }
            }

            #endregion

            BaseResponse resp = new BaseResponse();
            _helper.KuldemenyIktatokonyvId = ASPADOHelper.GetKuldemenyIktatokonyvId(resp, localUser);

            if (!string.IsNullOrEmpty(resp.ErrorMessage))
            {
                _helper.Folyamat.SetAllapot(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.InicializalasiHiba);
                return response.SetErrorResponse(resp.ErrorMessage);
            }

            _helper.CreateGroups(iktatoDocuments.Cast<IktatoObjektumBase>());

            #region Parallel Create Tetelek objects, Files, Call Init webservice

            if (_helper.IktatoCsoportok.Count > 0)
            {
                Thread startInitCsoportok = new Thread(_helper.CallTomegesIktatasInitForGroups);
                Thread startCreateTetelek = new Thread(StartInitTetelek);
                Thread startCreateFiles = new Thread(StartCreateFiles);

                startCreateFiles.Start();
                startInitCsoportok.Start();
                startCreateTetelek.Start();
                startCreateFiles.Join();
                startInitCsoportok.Join();
                startCreateTetelek.Join();

                _helper.CsoportokReady.WaitOne();
            }

            #endregion

            #region Insert Tetelek into database        

            List<BatchFilingResponseDocumentDto> batchFilings = new List<BatchFilingResponseDocumentDto>();

            //batch.Documents = documentListRequest.BatchFilingDocuments;
            /*int eCount = existingTetelek.Count;
            if (existingTetelek.Count > 0)
            {
                GetBatchFilingStatusResponse existingResponse = GetBatchFilingStatus(existingTetelek.Select(x => Convert.ToInt32(x)).ToArray());
                batchFilings.AddRange(existingResponse.BatchFilings);

                foreach (var elem in batchFilings)
                {
                    BatchFilingDocumentDto doc = documentListRequest.BatchFilingDocuments.FirstOrDefault(x => x.Id == elem.LineId);
                    if (!elem.IsComplete)
                    {
                        CopyValues(elem, doc);
                    }
                }

            }
            */

            for (int i = 0; i < badIktatoDocuments.Count; i++)
            {
                BatchFilingResponseDocumentDto respDoc = new BatchFilingResponseDocumentDto();
                batchFilings.Add(respDoc);

                respDoc.RegistrationNumber = new FilingRequestRegistartionNumberDto();
                respDoc.RegistrationNumber.RegistrationBook = new FilingRequestRegistrationBookDto();
                EREC_TomegesIktatasTetelek tetel = badIktatoDocuments[i].Tetel;
                BatchFilingDocumentDto doc = badIktatoDocuments[i].Document;
                respDoc.ErrorMessage = badIktatoDocuments[i].ErrorMessage;

                CopyValues(respDoc, doc);

                //Result insertResult = tomegesIktatasTetelekService.Insert(execParam, tetel);
            }

            for (int i = 0; i < iktatoDocuments.Count(); i++)
            {
                BatchFilingResponseDocumentDto respDoc = new BatchFilingResponseDocumentDto();
                batchFilings.Add(respDoc);
                respDoc.RegistrationNumber = new FilingRequestRegistartionNumberDto();
                respDoc.RegistrationNumber.RegistrationBook = new FilingRequestRegistrationBookDto();

                BatchFilingDocumentDto doc = iktatoDocuments[i].Document;
                IktatoCsoport csoport = _helper.IktatoCsoportok.FirstOrDefault(x => x.Id == iktatoDocuments[i].CsoportId);
                EREC_TomegesIktatasTetelek tetel = iktatoDocuments[i].Tetel;
                Result insertResult;

                if (csoport != null)
                {
                    if (doc.RegistrationNumber == null)
                    {
                        doc.RegistrationNumber = new RegistrationNumberDto();
                    }

                    respDoc.Id = doc.Id;
                    respDoc.IdSpecified = true;
                    respDoc.LineId = Convert.ToInt32(tetel.Forras_Azonosito);
                    respDoc.LineIdSpecified = true;
                    respDoc.RegistrationNumber.RegistrationBook.Prefix = doc.RegistrationNumber.RegistrationBook.Prefix;
                    respDoc.RegistrationNumber.RegistrationBook.Year = doc.RegistrationNumber.RegistrationBook.Year;
                    respDoc.RegistrationNumber.RegistrationBook.YearSpecified = true;
                    respDoc.RegistrationNumber.RegistrationBook.RegistrationBookId = doc.RegistrationNumber.RegistrationBook.RegistrationBookId;
                    respDoc.RegistrationNumber.RegistrationBook.RegistrationBookIdSpecified = true;
                    respDoc.RegistrationNumber.RegistrationBook.Name = doc.RegistrationNumber.RegistrationBook.Name;
                    respDoc.RegistrationNumber.RegistrationBook.Closed = doc.RegistrationNumber.RegistrationBook.Closed;
                    respDoc.RegistrationNumber.RegistrationBook.ClosedSpecified = true;
                    respDoc.IsComplete = false;
                    respDoc.IsCompleteSpecified = true;
                    respDoc.RegistrationNumber.Prefix = doc.RegistrationNumber.RegistrationBook.Prefix;
                    respDoc.RegistrationNumber.Year = doc.RegistrationNumber.RegistrationBook.Year;
                    respDoc.RegistrationNumber.YearSpecified = true;

                    if (null != csoport.kapottIratId && csoport.kapottAlszam != null && string.IsNullOrEmpty(csoport.ErrorMessage))
                    {
                        int foszam;
                        string alszam;
                        int index;
                        Guid iratId;
                        string azonosito;

                        if (doc.RegistrationNumber.MainNumber != 0)
                        {
                            index = Array.FindIndex(csoport.kapottFoszam, x => x == doc.RegistrationNumber.MainNumber);
                        }
                        else
                        {
                            index = 0;
                        }

                        foszam = csoport.kapottFoszam[index];
                        alszam = csoport.kapottAlszam[index];
                        iratId = csoport.kapottIratId[index];
                        azonosito = csoport.kapottAzonosito[index];

                        csoport.kapottAlszam = Remove(csoport.kapottAlszam.ToList(), index);
                        csoport.kapottIratId = Remove(csoport.kapottIratId.ToList(), index);
                        csoport.kapottAzonosito = Remove(csoport.kapottAzonosito.ToList(), index);
                        csoport.kapottFoszam = Remove(csoport.kapottFoszam.ToList(), index);

                        doc.RegistrationNumber.MainNumber = foszam;
                        doc.RegistrationNumber.MainNumberSpecified = true;
                        doc.RegistrationNumber.SubNumber = int.Parse(alszam);
                        doc.RegistrationNumber.SubNumberSpecified = true;

                        respDoc.RegistrationNumber.MainNumber = foszam;
                        respDoc.RegistrationNumber.MainNumberSpecified = true;
                        respDoc.RegistrationNumber.SubNumber = int.Parse(alszam);
                        respDoc.RegistrationNumber.SubNumberSpecified = true;

                        respDoc.RegistrationNumber.FullRegistrationNumber = azonosito;

                        //doc.RegistrationNumber.RegistrationBook.RegistrationBookId = csoport.iktatokonyv_Id;

                        tetel.Irat_Id = iratId.ToString();
                        tetel.Updated.Irat_Id = true;

                        tetel.Azonosito = azonosito;
                        tetel.Updated.Azonosito = true;

                        //insertResult = tomegesIktatasTetelekService.Insert(execParam, tetel);
                    }
                    else if (!string.IsNullOrEmpty(csoport.ErrorMessage))
                    {
                        respDoc.ErrorMessage = csoport.ErrorMessage; ;
                        //insertResult = tomegesIktatasTetelekService.Insert(execParam, tetel);
                    }
                    else
                    {
                        string err = "ASPADO BatchFiling kritikus hiba. Nem foglalhat= irat/alszám.";
                        respDoc.ErrorMessage = err; ;
                        Logger.Error(err);
                    }
                }
                else
                {
                    string err = "ASPADO BatchFiling kritikus hiba. Nem található iktatócsoport";
                    respDoc.ErrorMessage = err; ;
                    Logger.Error(err);
                }
            }

            iktatoDocuments.AddRange(badIktatoDocuments.ToArray());

            if (iktatoDocuments.Count > 0)
            {
                Result res = tomegesIktatasTetelekService.InsertBulk(execParam, iktatoDocuments.Select(x => x.Tetel).ToArray());
                if (res.IsError)
                {
                    Logger.Error("ASPADO BatchFiling BulkInsert Error", execParam, res);
                }
            }

            _helper.Folyamat.SetAllapot(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.Inicializalt);

            response.BatchFilings = batchFilings.ToArray();

            #endregion
            /*BL 12358 - az INIT után nem indulhat azonnal az EXEC, mert a következő INIT-tel összeakad
            if (prioritas == 0)
            {
                _helper.StartExec();
            }
            else if (prioritas == 1 || prioritas == 2)
            {
                Thread startExec = new Thread(_helper.StartExec);
                startExec.Start();
            }
            */

            Logger.Debug(JsonConvert.SerializeObject(response));
        }
        catch (Exception ex)
        {
            _helper.Folyamat.SetAllapot(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.InicializalasiHiba);
            Logger.Error("ASPADO BatchFiling Error: " + ex.Message + Environment.NewLine + " Stacktrace: " + ex.StackTrace);
            return response.SetErrorResponse(ex.Message);
        }

        return response.SetSuccessResponse();
    }
    private void CopyValues(BatchFilingResponseDocumentDto respDoc, BatchFilingDocumentDto doc)
    {
        if (doc.RegistrationNumber == null)
        {
            doc.RegistrationNumber = new RegistrationNumberDto();
        }

        if (doc.RegistrationNumber.RegistrationBook == null)
        {
            doc.RegistrationNumber.RegistrationBook = new RegistrationBookDto();
        }

        if (respDoc.RegistrationNumber == null)
        {
            respDoc.RegistrationNumber = new FilingRequestRegistartionNumberDto();
        }

        if (respDoc.RegistrationNumber.RegistrationBook == null)
        {
            respDoc.RegistrationNumber.RegistrationBook = new FilingRequestRegistrationBookDto();
        }

        respDoc.RegistrationNumber.MainNumber = doc.RegistrationNumber.MainNumber;
        respDoc.RegistrationNumber.MainNumberSpecified = true;
        respDoc.RegistrationNumber.SubNumber = doc.RegistrationNumber.SubNumber;
        respDoc.RegistrationNumber.SubNumberSpecified = true;
        respDoc.Id = doc.Id;
        respDoc.IdSpecified = true;
        respDoc.LineId = Convert.ToInt32(_helper.Folyamat.Forras_Azonosito.ToString() + doc.Id);
        respDoc.LineIdSpecified = true;
        respDoc.RegistrationNumber.RegistrationBook.Prefix = doc.RegistrationNumber.RegistrationBook.Prefix;
        respDoc.RegistrationNumber.RegistrationBook.Year = doc.RegistrationNumber.RegistrationBook.Year;
        respDoc.RegistrationNumber.RegistrationBook.YearSpecified = true;
        respDoc.RegistrationNumber.RegistrationBook.RegistrationBookId = doc.RegistrationNumber.RegistrationBook.RegistrationBookId;
        respDoc.RegistrationNumber.RegistrationBook.RegistrationBookIdSpecified = true;
        respDoc.RegistrationNumber.RegistrationBook.Name = doc.RegistrationNumber.RegistrationBook.Name;
        respDoc.RegistrationNumber.RegistrationBook.Closed = doc.RegistrationNumber.RegistrationBook.Closed;
        respDoc.RegistrationNumber.RegistrationBook.ClosedSpecified = true;
        respDoc.IsComplete = false;
        respDoc.IsCompleteSpecified = true;
        respDoc.RegistrationNumber.Prefix = doc.RegistrationNumber.RegistrationBook.Prefix;
        respDoc.RegistrationNumber.Year = doc.RegistrationNumber.RegistrationBook.Year;
        respDoc.RegistrationNumber.YearSpecified = true;
    }
    private T[] Remove<T>(List<T> list, int index)
    {
        list.RemoveAt(index);
        return list.ToArray();
    }

    private void StartCreateFiles()
    {

        for (int i = 0; i < iktatoDocuments.Count; i++)
        {
            if (iktatoDocuments[i].Document != null && iktatoDocuments[i].Document.Files != null)
            {
                var files = iktatoDocuments[i].Document.Files;
                string name;
                string tempFile;
                string inputDir;
                byte[] data;

                for (int j = 0; j < files.Length; j++)
                {
                    try
                    {
                        name = files[j].Name;
                        data = files[j].Data;

                        inputDir = ASPADOHelper.GetInputDir(_helper.Folyamat.Forras_Azonosito.ToString(), iktatoDocuments[i].Document.Pointer);
                        Directory.CreateDirectory(inputDir);
                        tempFile = Path.Combine(inputDir, name);

                        File.WriteAllBytes(tempFile, data);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
    }

    private void StartInitTetelek()
    {
        EREC_TomegesIktatasTetelek tetel;
        BatchFilingDocumentDto document;

        ASPADOHelper.RefreshGuidCacheByCim(localUser);
        ASPADOHelper.RefreshGuidCacheByPartner(localUser);

        List<string> partnerIds = new List<string>();
        List<string> addressIds = new List<string>();

        foreach (var i in iktatoDocuments)
        {
            if (i.Document.Partner.Addresses != null && i.Document.Partner.Addresses.Length > 0 && ASPADOHelper.GuidCache.ContainsValue(i.Document.Partner.Addresses[0].Id.ToString()))
            {
                addressIds.Add(ASPADOHelper.GuidCache.First(x => x.Value == i.Document.Partner.Addresses[0].Id.ToString()).Key);
            }
        }

        var cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();
        if (addressIds.Count > 0)
        {
            var cimeksearch = new KRT_CimekSearch();
            cimeksearch.Id.In(partnerIds);
            Result cimekResult = cimekService.GetAll(globalExecParam, cimeksearch);
            if (!cimekResult.IsError && cimekResult.GetCount > 0)
            {
                foreach (DataRow row in cimekResult.Ds.Tables[0].Rows)
                {
                    KRT_Cimek p = new KRT_Cimek();
                    Utils.LoadBusinessDocumentFromDataRow(p, row);
                    if (p != null && ASPADOHelper.GuidCache.ContainsKey(p.Id))
                    {
                        cimekCache.Add(ASPADOHelper.GuidCache[p.Id], p);
                    }
                }
            }
        }


        foreach (var i in iktatoDocuments)
        {
            if (!string.IsNullOrEmpty(i.Document.Partner.PartnerId) && i.Document.Partner.PartnerId != "0" && ASPADOHelper.GuidCache.ContainsValue(i.Document.Partner.PartnerId))
            {
                partnerIds.Add(ASPADOHelper.GuidCache.First(x => x.Value == i.Document.Partner.PartnerId).Key);
            }
        }
        var partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        if (partnerIds.Count > 0)
        {
            var partnerSearch = new KRT_PartnerekSearch();
            partnerSearch.Id.In(partnerIds);
            Result partnerekResult = partnerService.GetAll(globalExecParam, partnerSearch);
            if (!partnerekResult.IsError && partnerekResult.GetCount > 0)
            {
                foreach (DataRow row in partnerekResult.Ds.Tables[0].Rows)
                {
                    KRT_Partnerek p = new KRT_Partnerek();
                    Utils.LoadBusinessDocumentFromDataRow(p, row);
                    if (p != null && ASPADOHelper.GuidCache.ContainsKey(p.Id))
                    {
                        partnerekCache.Add(ASPADOHelper.GuidCache[p.Id], p);
                    }
                }
            }
        }

        List<IktatoObjektum> tempBadIktatoDocuments = new List<IktatoObjektum>();

        for (int i = 0; i < iktatoDocuments.Count; i++)
        {
            tetel = iktatoDocuments[i].Tetel;
            document = iktatoDocuments[i].Document;

            tetel.IktatasTipus = document.Direction == EnumsDirection.In ? "0" : "1";
            tetel.Updated.IktatasTipus = true;

            tetel.Alszamra = (document.RegistrationNumber.MainNumberSpecified && document.RegistrationNumber.MainNumber != 0) ? "1" : "0";
            tetel.Updated.Alszamra = true;

            tetel.Iktatokonyv_Id = iktatoDocuments[i].IktatoKonyvId;
            tetel.Updated.Iktatokonyv_Id = true;

            EREC_IraIratok irat = document.GetIratFromRequestForFiling(localUser);

            if (irat == null)
            {
                iktatoDocuments[i].ErrorMessage = "Hiányzó Document adat";
                tempBadIktatoDocuments.Add(iktatoDocuments[i]);
                continue;
            }

            tetel.EREC_IraIratok = JsonConvert.SerializeObject(irat);
            tetel.Updated.EREC_IraIratok = true;

            tetel.ExecParam = JsonConvert.SerializeObject(globalExecParam);
            tetel.Updated.ExecParam = true;

            IktatasiParameterek ip = ASPADOHelper.GetIktatasiParameterkFromRequestForFiling();

            string ugyiratId = "0";
            Result ugyiratResult = null;

            KRT_Partnerek partner = null;
            KRT_Cimek cim = null;

            if (!string.IsNullOrEmpty(document.Partner.PartnerId) && document.Partner.PartnerId != "0")
            {
                if (partnerekCache.Any(x => x.Key == document.Partner.PartnerId))
                {
                    partner = partnerekCache[document.Partner.PartnerId];
                }
            }

            if (document.Partner != null)
            {
                if (document.Partner.Addresses != null && document.Partner.Addresses.Length > 0)
                {
                    if (cimekCache.Any(x => x.Key == document.Partner.Addresses[0].Id.ToString()))
                    {
                        cim = cimekCache[document.Partner.Addresses[0].Id.ToString()];
                    }
                }
            }

            if (document.RegistrationNumber.MainNumberSpecified && document.RegistrationNumber.MainNumber != 0)
            {
                string key = iktatoDocuments[i].IktatoKonyvId + document.RegistrationNumber.MainNumber.ToString();
                if (ugyiratokCache.Any(x => x.Key == key))
                {
                    ugyiratResult = ugyiratokCache[key];
                }
                else
                {
                    ugyiratResult = document.GetUgyiratByIktatoKonyvFromRequestForBatchFiling(iktatoDocuments[i].IktatoKonyvId, localUser);
                }

                if (ugyiratResult.IsError)
                {
                    iktatoDocuments[i].ErrorMessage = ugyiratResult.ErrorMessage;
                    tempBadIktatoDocuments.Add(iktatoDocuments[i]);
                    continue;
                }
                else if (ugyiratResult.Ds.Tables.Count == 0 || ugyiratResult.Ds.Tables[0].Rows.Count < 1)
                {
                    iktatoDocuments[i].ErrorMessage = "Az ügyirat nem található.";
                    tempBadIktatoDocuments.Add(iktatoDocuments[i]);
                    continue;
                }

                if (!ugyiratokCache.Any(x => x.Key == key))
                {
                    ugyiratokCache.Add(key, ugyiratResult);
                }

                ugyiratId = ugyiratResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                tetel.Ugyirat_Id = ugyiratId;
                tetel.Updated.Ugyirat_Id = true;
            }
            else
            {
                DocumentResponse response = new DocumentResponse();
                EREC_UgyUgyiratok ugyirat = document.GetUgyiratFromRequestForFiling(partner, cim, response, localUser);

                if (!string.IsNullOrEmpty(response.ErrorMessage))
                {
                    iktatoDocuments[i].ErrorMessage = response.ErrorMessage;
                    badIktatoDocuments.Add(iktatoDocuments[i]);
                    iktatoDocuments.RemoveAt(i);
                    continue;
                }

                tetel.EREC_UgyUgyiratok = JsonConvert.SerializeObject(ugyirat);
                tetel.Updated.EREC_UgyUgyiratok = true;

            }

            if (document.Direction == EnumsDirection.In)
            {
                if (ugyiratResult != null)
                {
                    ip.UgykorId = ugyiratResult.Ds.Tables[0].Rows[0]["IraIrattariTetel_Id"].ToString();
                    ip.Ugytipus = ugyiratResult.Ds.Tables[0].Rows[0]["UgyTipus"].ToString();
                }
                else
                {
                    if (document.ArchivesItem != null && !string.IsNullOrEmpty(document.ArchivesItem.CaseGroup))
                    {
                        if (document.TaxTypeSpecified)
                        {
                            string kod = document.ArchivesItem.CaseGroup + "_" + document.TaxType.ToString();
                            if (ASPADOHelper.TaxTypeKodTipusok.Keys.Any(x => x.Kod == kod))
                            {
                                string mappedkod = ASPADOHelper.TaxTypeKodTipusok.First(x => x.Key.Kod == kod).Value.Kod;
                                mappedkod = mappedkod.Substring(mappedkod.IndexOf("_") + 1);
                                ip.Ugytipus = mappedkod;
                            }
                        }
                    }
                }
            }

            tetel.EREC_UgyUgyiratdarabok = JsonConvert.SerializeObject(new EREC_UgyUgyiratdarabok());
            tetel.Updated.EREC_UgyUgyiratdarabok = true;

            tetel.IktatasiParameterek = JsonConvert.SerializeObject(ip);
            tetel.Updated.IktatasiParameterek = true;

            if (document.Direction != EnumsDirection.In)
            {
                EREC_PldIratPeldanyok iratpeldany = document.GetIratPeldanyFromRequestForFiling(partner, cim, localUser);

                tetel.EREC_PldIratPeldanyok = JsonConvert.SerializeObject(iratpeldany);
                tetel.Updated.EREC_PldIratPeldanyok = true;
            }

            if (document.Direction == EnumsDirection.In)
            {
                BaseResponse resp = new BaseResponse();
                EREC_KuldKuldemenyek kuld = document.GetKuldemenyForBatchFiling(partner, cim, _helper.KuldemenyIktatokonyvId, resp, localUser);

                tetel.EREC_KuldKuldemenyek = JsonConvert.SerializeObject(kuld);
                tetel.Updated.EREC_KuldKuldemenyek = true;
            }

            ErkeztetesParameterek ep = new ErkeztetesParameterek();

            tetel.ErkeztetesParameterek = JsonConvert.SerializeObject(ep);
            tetel.Updated.ErkeztetesParameterek = true;
        }
        badIktatoDocuments.AddRange(tempBadIktatoDocuments.ToArray());
        iktatoDocuments.RemoveAll(x => tempBadIktatoDocuments.Any(y => y == x));
    }

    #endregion


    public BaseResponse BecameFinal(BecameFinalRequest becameFinalRequest)
    {
        Logger.Debug("DocumentService.BecameFinal.Start");

        string userId = headers.ValidateUnknownSoapHeaders();
        BaseResponse response = new BaseResponse();

        if (becameFinalRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }

        try
        {

            DocumentListByRegistrationNumberRequest request = new DocumentListByRegistrationNumberRequest();

            request.RegistrationNumber = new RegistrationNumberDto();
            request.RegistrationNumber = becameFinalRequest.BecameFinalRequestDocumentDto.RegistrationNumber.Map();

            Result result = ASPADOHelper.GetDocumentsData(request, 0, false, 0, false, localUser);

            if (result.IsError)
            {
                return response.SetErrorResponse(result.ErrorMessage);
            }
            else if (result.Ds.Tables[0].Rows.Count != 1)
            {
                return response.SetErrorResponse("A megadott adatok alapján nem határozható meg egyértelműen a keresett tétel");
            }

            var iratRow = result.Ds.Tables[0].Rows[0];
            string iratId = iratRow["Id"].ToString();

            EREC_TargySzavakService service = eRecordService.ServiceFactory.GetEREC_TargySzavakService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            EREC_TargySzavakSearch search = new EREC_TargySzavakSearch();
            search.BelsoAzonosito.Value = "%joger%";
            search.BelsoAzonosito.Operator = Contentum.eQuery.Query.Operators.like;

            Result targySzavakResult = service.GetAll(execParam, search);

            if (targySzavakResult.IsError)
            {
                return response.SetErrorResponse(targySzavakResult.ErrorMessage);
            }
            else if (targySzavakResult.Ds.Tables[0].Rows.Count < 1)
            {
                return response.SetErrorResponse("Nem található meta adat");
            }
            else
            {
                Dictionary<string, string> targyszavakDict = new Dictionary<string, string>();

                foreach (DataRow row in targySzavakResult.Ds.Tables[0].Rows)
                {
                    if (!ASPADOHelper.IsDBNull(row["BelsoAzonosito"]))
                    {
                        if (row["BelsoAzonosito"].ToString() == "Jogerositendo")
                        {
                            targyszavakDict.Add("Jogerositendo", "true");
                        }

                        if (row["BelsoAzonosito"].ToString() == "Jogerore_emelkedes_datuma")
                        {
                            if (becameFinalRequest.BecameFinalRequestDocumentDto.RealLegallyBindingDateSpecified && becameFinalRequest.BecameFinalRequestDocumentDto.RealLegallyBindingDate.HasValue)
                            {
                                {
                                    targyszavakDict.Add("Jogerore_emelkedes_datuma", becameFinalRequest.BecameFinalRequestDocumentDto.RealLegallyBindingDate.ToString());
                                }
                            }
                        }
                    }
                }
                if (targyszavakDict.Count > 1)
                {
                    ASPADOHelper.SetObjektumTargyszavak(execParam, iratId, Contentum.eUtility.Constants.TableNames.EREC_IraIratok, targyszavakDict, response);

                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetErrorResponse(response.ErrorMessage);
                    }
                }
                else
                {
                    return response.SetErrorResponse("Nem található meta adat");
                }

                //ASPADOHelper.UploadCsatolmanyokForRequest(becameFinalRequest, response, iratId, localUser);

                //if (!string.IsNullOrEmpty(response.ErrorMessage))
                //{
                //    return response;
                //}

            }
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.BecameFinal.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }


        return response.SetSuccessResponse();
    }

    public BaseResponse CancelDocument(DocumentByRegistrationNumberRequest documentByRegistrationNumberRequest)
    {
        Logger.Debug("DocumentService.CancelDocument.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        BaseResponse response = new BaseResponse();

        if (documentByRegistrationNumberRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }

        try
        {

            DocumentListByRegistrationNumberRequest request = new DocumentListByRegistrationNumberRequest();

            request.RegistrationNumber = new RegistrationNumberDto();
            request.RegistrationNumber = documentByRegistrationNumberRequest.RegistrationNumber;

            Result result = ASPADOHelper.GetDocumentsData(request, 0, false, 0, false, localUser);

            if (result.IsError)
            {
                return response.SetErrorResponse(result.ErrorMessage);
            }
            else if (result.Ds.Tables[0].Rows.Count != 1)
            {
                return response.SetErrorResponse("A megadott adatok alapján nem határozható meg egyértelműen a keresett tétel"); ;
            }

            var iratRow = result.Ds.Tables[0].Rows[0];
            string iratId = iratRow["Id"].ToString();
            string irany = "";

            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);

            if (!ASPADOHelper.IsDBNull(iratRow["PostazasIranya"]))
            {
                irany = iratRow["PostazasIranya"].ToString();
            }

            if (irany == "1")
            {
                Result felszabaditasResult = iratokService.Felszabaditas(execParam, iratId, false);
                if (felszabaditasResult.IsError)
                {
                    return response.SetErrorResponse(felszabaditasResult.ErrorMessage);
                }
            }
            else
            {
                EREC_PldIratPeldanyokService iratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                EREC_PldIratPeldanyokSearch iratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();
                iratPeldanyokSearch.IraIrat_Id.Value = iratId;
                iratPeldanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                Result iratPeldanyokResult = iratPeldanyokService.GetAll(execParam, iratPeldanyokSearch);

                if (iratPeldanyokResult.IsError)
                {
                    return response.SetErrorResponse(iratPeldanyokResult.ErrorMessage);
                }

                List<string> iratPeldanyokIds = iratPeldanyokResult.Ds.Tables[0].AsEnumerable().OrderByDescending(x => x["Sorszam"]).Select(x => x["Id"].ToString()).Distinct().ToList();

                foreach (string id in iratPeldanyokIds)
                {
                    Result sztornoResult = iratPeldanyokService.Sztornozas(execParam, id, "ASPADO", null, null);
                    if (sztornoResult.IsError)
                    {
                        return response.SetErrorResponse(sztornoResult.ErrorMessage);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.CancelDocument.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }
        return response.SetSuccessResponse();
    }


    public PackageResponse CreatePackage(PackageRequest packagesRequest)
    {
        Logger.Debug("DocumentService.CreatePackage.Start");
        headers.ValidateUnknownSoapHeaders();
        throw new NotImplementedException();
    }


    public PackagesResponse CreatePackages(PackageListRequest packageListRequest)
    {
        Logger.Debug("DocumentService.CreatePackages.Start");
        headers.ValidateUnknownSoapHeaders();
        throw new NotImplementedException();
    }


    public FilingDocumentResponse Filing(FilingDocumentRequest documentRequest)
    {
        Logger.Debug("DocumentService.Filing.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        FilingDocumentResponse response = new FilingDocumentResponse();

        if (documentRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }

        try
        {

            response.Document = new FilingDocumentDto();
            KRT_Partnerek partner;
            KRT_Cimek cim;

            partner = documentRequest.FilingDocument.GetPartnerFromRequestForFiling(response, localUser);

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                return response;
            }

            cim = documentRequest.FilingDocument.GetCimFromRequestForFiling(response, localUser);

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                return response;
            }

            Result iktatoKonyvekResult = documentRequest.FilingDocument.GetIktatokonyvFromRequestForFiling(response, localUser);

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                return response;
            }

            if (iktatoKonyvekResult.Ds.Tables[0].Rows.Count == 0)
            {
                return response.SetErrorResponse("Nem található az iktatókönyv");
            }

            string iktatoKonyvId = iktatoKonyvekResult.Ds.Tables[0].Rows[0]["Id"].ToString();
            string ugyiratId = "0";
            Result ugyiratResult = null;

            if (documentRequest.FilingDocument.RegistrationNumber.MainNumberSpecified && documentRequest.FilingDocument.RegistrationNumber.MainNumber != 0)
            {
                ugyiratResult = documentRequest.GetUgyiratByIktatoKonyvFromRequestForFiling(iktatoKonyvId, response, localUser);

                if (!string.IsNullOrEmpty(response.ErrorMessage))
                {
                    return response;
                }

                if (ugyiratResult.Ds.Tables.Count == 0 || ugyiratResult.Ds.Tables[0].Rows.Count < 1)
                {
                    return response.SetErrorResponse("A megadott ügyirat nem létezik");
                }

                ugyiratId = ugyiratResult.Ds.Tables[0].Rows[0]["Id"].ToString();
            }

            EREC_IraIratok irat = documentRequest.FilingDocument.GetIratFromRequestForFiling(localUser);

            if (irat == null)
            {
                return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
            }

            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam iratokExecParam = ASPADOHelper.GetExecParam(localUser);

            IktatasiParameterek ip = ASPADOHelper.GetIktatasiParameterkFromRequestForFiling();

            if (documentRequest.FilingDocument.Direction == EnumsDirection.In)
            {
                if (ugyiratResult != null)
                {
                    ip.UgykorId = ugyiratResult.Ds.Tables[0].Rows[0]["IraIrattariTetel_Id"].ToString();
                    ip.Ugytipus = ugyiratResult.Ds.Tables[0].Rows[0]["UgyTipus"].ToString();
                }
                else
                {
                    if (documentRequest.FilingDocument.ArchivesItem != null && !string.IsNullOrEmpty(documentRequest.FilingDocument.ArchivesItem.CaseGroup))
                    {
                        if (documentRequest.FilingDocument.TaxTypeSpecified)
                        {
                            string kod = documentRequest.FilingDocument.ArchivesItem.CaseGroup + "_" + documentRequest.FilingDocument.TaxType.ToString();
                            if (ASPADOHelper.TaxTypeKodTipusok.Keys.Any(x => x.Kod == kod))
                            {
                                string mappedkod = ASPADOHelper.TaxTypeKodTipusok.First(x => x.Key.Kod == kod).Value.Kod;
                                mappedkod = mappedkod.Substring(mappedkod.IndexOf("_") + 1);
                                ip.Ugytipus = mappedkod;
                            }
                        }
                    }
                }
            }

            Result iktatasResult = new Result();

            if (ugyiratId != "0")
            {
                if (documentRequest.FilingDocument.Direction == EnumsDirection.In)
                {
                    //iktatasResult = iratokService.BejovoIratIktatasa_Alszamra(iratokExecParam, iktatoKonyvId, ugyiratId, new EREC_UgyUgyiratdarabok(), irat, null, ip);
                    EREC_KuldKuldemenyek kuld = documentRequest.FilingDocument.GetKuldemenyForFiling(partner, cim, response, localUser);

                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response;
                    }

                    iktatasResult = iratokService.EgyszerusitettIktatasa_Alszamra(iratokExecParam, iktatoKonyvId, ugyiratId, new EREC_UgyUgyiratdarabok(), irat, null, kuld, ip, null);
                }
                else
                {
                    EREC_PldIratPeldanyok iratpeldany = documentRequest.FilingDocument.GetIratPeldanyFromRequestForFiling(partner, cim, localUser);
                    iktatasResult = iratokService.BelsoIratIktatasa_Alszamra(iratokExecParam, iktatoKonyvId, ugyiratId, new EREC_UgyUgyiratdarabok(), irat, null, iratpeldany, ip);
                }
            }
            else
            {
                EREC_UgyUgyiratok ugyirat = documentRequest.FilingDocument.GetUgyiratFromRequestForFiling(partner, cim, response, localUser);

                if (documentRequest.FilingDocument.Direction == EnumsDirection.In)
                {
                    //iktatasResult = iratokService.BejovoIratIktatasa(iratokExecParam, iktatoKonyvId, ugyirat, new EREC_UgyUgyiratdarabok(), irat, null, ip);
                    EREC_KuldKuldemenyek kuld = documentRequest.FilingDocument.GetKuldemenyForFiling(partner, cim, response, localUser);

                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response;
                    }

                    iktatasResult = iratokService.EgyszerusitettIktatasa(iratokExecParam, iktatoKonyvId, ugyirat, new EREC_UgyUgyiratdarabok(), irat, null, kuld, ip, null);
                }
                else
                {
                    EREC_PldIratPeldanyok iratpeldany = documentRequest.FilingDocument.GetIratPeldanyFromRequestForFiling(partner, cim, localUser);
                    iktatasResult = iratokService.BelsoIratIktatasa(iratokExecParam, iktatoKonyvId, ugyirat, new EREC_UgyUgyiratdarabok(), irat, null, iratpeldany, ip);
                }
            }

            if (!string.IsNullOrEmpty(iktatasResult.ErrorMessage))
            {
                return response.SetErrorResponse(iktatasResult.ErrorMessage);
            }


            string newIraIratId = iktatasResult.Uid;

            ASPADOHelper.UploadCsatolmanyokForRequest(documentRequest, response, newIraIratId, localUser);

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                return response;
            }

            ASPADOHelper.GetDocument(newIraIratId, response, localUser);

            ////visszaadjuk az ASP.ADÓ-s iktatószám ID mező értékét, mert enélkül nem működik az iktatás
            //if (documentRequest.FilingDocument.RegistrationNumber.RegistrationBook.RegistrationBookIdSpecified)
            //{
            //    response.Document.RegistrationNumber.RegistrationBook.RegistrationBookId = documentRequest.FilingDocument.RegistrationNumber.RegistrationBook.RegistrationBookId;
            //    response.Document.RegistrationNumber.RegistrationBook.IdSpecified = true;
            //}
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.Filing.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }
        return response;
    }

    public GetAttachmentsResponse GetAttachments(GetAttachmentsRequest getAttachmentsRequest)
    {
        var response = new GetAttachmentsResponse();
        Logger.Debug("DocumentService.GetAttachments.Start");
        string userId = headers.ValidateUnknownSoapHeaders();

        if (getAttachmentsRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }

        try
        {
            response.Files = ASPADOHelper.GetAttachments(getAttachmentsRequest.RegistrationNumber, response, localUser);
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.GetDocument.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }
        return response.SetSuccess();
    }

    public GetDocumentResponse GetDocument(DocumentByRegistrationNumberRequest documentByRegistrationNumberRequest)
    {
        Logger.Debug("DocumentService.GetDocument.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        GetDocumentResponse response = new GetDocumentResponse();

        if (documentByRegistrationNumberRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }
        response.Document = new GetDocumentDto();
        try
        {
            return ASPADOHelper.GetDocument(documentByRegistrationNumberRequest, response, localUser);
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.GetDocument.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }
    }


    public GetDocumentsResponse GetDocuments(DocumentListByRegistrationNumberRequest documentListByRegistrationNumberRequest, int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified)
    {

        Logger.Debug("DocumentService.GetDocuments.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        GetDocumentsResponse response = new GetDocumentsResponse();

        if (documentListByRegistrationNumberRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }

        response.Documents = new GetDocumentsDto[0];
        try
        {
            return ASPADOHelper.GetDocuments(documentListByRegistrationNumberRequest, response, pageNumber, pageNumberSpecified, pageSize, pageSizeSpecified, localUser);
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.GetDocuments.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }
    }


    public GetNotToBeFiledDocumentsResponse GetNotToBeFiledDocuments(GetNotToBeFiledDocumentsRequest getNotToBeFiledDocumentsRequest)
    {
        Logger.Debug("DocumentService.GetNotToBeFiledDocuments.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        var response = new GetNotToBeFiledDocumentsResponse();

        if (getNotToBeFiledDocumentsRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }

        response.GetNotToBeFiledDocumentsResponseDtos = new GetNotToBeFiledDocumentsResponseDto[0];
        try
        {
            return ASPADOHelper.GetNotToBeFiledDocuments(getNotToBeFiledDocumentsRequest, response, localUser);
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.GetNotToBeFiledDocuments.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }
    }


    public GetOrganizationUnitsResponse GetOrganizationUnits(OrganizationUnitsRequest organizationUnitsRequest)
    {
        Logger.Debug("DocumentService.GetOrganizationUnits.Start");
        string userId = headers.ValidateUnknownSoapHeaders();

        GetOrganizationUnitsResponse response = new GetOrganizationUnitsResponse(); ;

        if (organizationUnitsRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }
        try
        {

            if (organizationUnitsRequest.User == null || organizationUnitsRequest.User.ActualOrganizationUnit == null)
            {
                return ASPADOHelper.GetOrganizationUnitsData(localUser);
            }
            else
            {
                return ASPADOHelper.GetOrganizationUnits(organizationUnitsRequest, response, localUser);
            }
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.GetOrganizationUnits.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }
    }


    public PackageResponse GetPackage(PackageByDocumentRequest packageByDocumentRequest)
    {
        Logger.Debug("DocumentService.GetPackage.Start");
        headers.ValidateUnknownSoapHeaders();
        throw new NotImplementedException();
    }


    public PackagesResponse GetPackages(PackageListByDocumentsRequest packageListByDocumentsRequest, int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified)
    {
        Logger.Debug("DocumentService.GetPackages.Start");
        headers.ValidateUnknownSoapHeaders();
        throw new NotImplementedException();
    }


    public GetRegistrationBooksResponse GetRegistrationBooks(RegistrationBooksRequest registrationBooksRequest, int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified)
    {
        Logger.Debug("DocumentService.GetRegistrationBooks.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        GetRegistrationBooksResponse response = new GetRegistrationBooksResponse();

        if (registrationBooksRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }
        try
        {

            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);

            GetOrganizationUnitsResponse resp = ASPADOHelper.GetOrganizationUnitsData(localUser);

            if (!resp.IsSucceeded)
            {
                return response.SetErrorResponse(resp.ErrorMessage);
            }

            execParam.FelhasznaloSzervezet_Id = resp.OrganizationUnits[0].OrganizationUnitId;

            //if (pageNumber > 0)
            //{
            //    execParam.Paging.PageNumber = pageNumber;
            //}

            //if (pageSize > 0)
            //{
            //    execParam.Paging.PageSize = pageSize;
            //}

            EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();

            Search.Statusz.Value = registrationBooksRequest.RegistrationBook.Closed ? "0" : "1";
            Search.Statusz.Operator = Contentum.eQuery.Query.Operators.equals;

            if (registrationBooksRequest.RegistrationBook.Year != 0)
            {
                Search.Ev.Value = registrationBooksRequest.RegistrationBook.Year.ToString();
                Search.Ev.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            if (!String.IsNullOrEmpty(registrationBooksRequest.RegistrationBook.Prefix))
            {
                Search.Iktatohely.Value = registrationBooksRequest.RegistrationBook.Prefix;
                Search.Iktatohely.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            if (!String.IsNullOrEmpty(registrationBooksRequest.RegistrationBook.Name))
            {
                Search.Nev.Value = registrationBooksRequest.RegistrationBook.Name;
                Search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            Search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
            Search.IktatoErkezteto.Value = "I";
            Search.OrderBy = "Iktatohely ASC, Ev DESC";

            Result result = service.GetAllWithIktathat(execParam, Search);

            if (result.IsError)
            {
                return response.SetErrorResponse(result.ErrorMessage);
            }
            else if (result.Ds.Tables.Count == 0 || result.Ds.Tables[0].Rows.Count == 0)
            {
                return response.SetErrorResponse(ASPADOHelper.Constants.Messages.RegistrationBooksNotFoundError);
            }
            else
            {
                response.TotalItemCount = result.Ds.Tables[0].Rows.Count;
                response.RegistrationBooks = new GetRegistrationBooksDto[result.Ds.Tables[0].Rows.Count];
                int i = 0;
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    response.RegistrationBooks[i] = new GetRegistrationBooksDto();
                    response.RegistrationBooks[i].Id = Convert.ToInt32(row["Azonosito"]);
                    response.RegistrationBooks[i].IdSpecified = true;
                    response.RegistrationBooks[i].RegistrationBookId = Convert.ToInt32(row["Azonosito"]);
                    response.RegistrationBooks[i].RegistrationBookIdSpecified = true;
                    response.RegistrationBooks[i].Closed = row["Statusz"].ToString() == "0";
                    response.RegistrationBooks[i].ClosedSpecified = true;
                    response.RegistrationBooks[i].Name = row["Nev"].ToString();
                    response.RegistrationBooks[i].Prefix = row["Iktatohely"].ToString();
                    response.RegistrationBooks[i].Year = Convert.ToInt32(row["Ev"]);
                    response.RegistrationBooks[i].YearSpecified = true;

                    i++;
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.GetRegistationBooks.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }
        return response.SetSuccessResponse();
    }

    private EREC_IratAlairok GetIratAlairok(string iratId, string alairo, string alairasMod, string alairoSzerep, string alairasSorrend)
    {
        Logger.Debug("DocumentService.GetIratAlairok.Start");
        EREC_IratAlairok iratAlairok = new EREC_IratAlairok();
        iratAlairok.FelhasznaloCsoport_Id_Alairo = alairo;
        iratAlairok.Updated.FelhasznaloCsoport_Id_Alairo = true;
        if (alairasMod == "E_EMB")
        {
            iratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
            iratAlairok.Updated.Allapot = true;
            iratAlairok.Leiras = "ASP.ADÓ által iktatott irat automatikus aláírás előjegyzése";
            iratAlairok.Updated.Leiras = true;
        }
        else
        {
            iratAlairok.AlairasDatuma = DateTime.Now.ToString();
            iratAlairok.Updated.AlairasDatuma = true;
            iratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
            iratAlairok.Updated.Allapot = true;
            iratAlairok.Leiras = "ASP.ADÓ által iktatott irat automatikus aláírása";
            iratAlairok.Updated.Leiras = true;
        }

        iratAlairok.AlairoSzerep = alairoSzerep;
        iratAlairok.Updated.AlairoSzerep = true;
        iratAlairok.AlairasMod = alairasMod;
        iratAlairok.Updated.AlairasMod = true;
        iratAlairok.AlairasSorrend = alairasSorrend;
        iratAlairok.Updated.AlairasSorrend = true;
        iratAlairok.Obj_Id = iratId;
        iratAlairok.Updated.Obj_Id = true;

        return iratAlairok;
    }

    private string GetAlairoSzabalyId(string alairo, string iratId, string alirasMod, DocumentResponse response)
    {
        Logger.Debug("DocumentService.GetAlairoSzabalyId.Start");
        ExecParam execParam_alairoSzabalyok = ASPADOHelper.GetExecParam(alairo);
        AlairasokService alairoSzabalyok = eRecordService.ServiceFactory.GetAlairasokService();

        Result alairoSzabalyokResult = alairoSzabalyok.GetAll_PKI_IratAlairoSzabalyok(execParam_alairoSzabalyok, alairo, null, "Irat", iratId, "IratJovahagyas", null, KodTarak.ALAIRO_SZEREP.Kiadmanyozo);

        if (alairoSzabalyokResult.IsError)
        {
            response.SetErrorResponse(alairoSzabalyokResult.ErrorMessage);
            return null;
        }

        foreach (DataRow row in alairoSzabalyokResult.Ds.Tables[0].Rows)
        {
            if (row["AlairasMod"] != DBNull.Value && row["AlairasMod"].ToString() == alirasMod && row["AlairasSzabaly_Id"] != DBNull.Value)
            {
                return row["AlairasSzabaly_Id"].ToString();
            }
        }

        return null;
    }

    private DocumentResponse InsertAlairas(string localUser, EREC_IratAlairok iratAlairok, DocumentResponse response)
    {
        Logger.Debug("DocumentService.InsertAlairas.Start");
        ExecParam execParam_iratAlairas = ASPADOHelper.GetExecParam(localUser);

        EREC_IratAlairokService service_iratAlairok = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
        Result result_iratAlairas = service_iratAlairok.Insert(execParam_iratAlairas, iratAlairok);

        if (result_iratAlairas.IsError)
        {
            return response.SetErrorResponse(result_iratAlairas.ErrorMessage);
        }

        return response;
    }


    public UserListByRolesResponse GetUserListByRoles(UserListByRolesRequest getUserListByRolesRequest)
    {
        Logger.Debug("DocumentService.GetUserListByRoles.Start");
        string userId = headers.ValidateUnknownSoapHeaders();

        UserListByRolesResponse response = new UserListByRolesResponse(); ;

        if (getUserListByRolesRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }

        KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam execParam = ASPADOHelper.GetExecParam(localUser);

        KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();
        partnerekSearch.OrderBy = null;

        KRT_CimekSearch cimekSearch = new KRT_CimekSearch();
        cimekSearch.CimTobbi.Value = "%@%";
        cimekSearch.CimTobbi.Operator = Contentum.eQuery.Query.Operators.like;
        cimekSearch.Tipus.Value = "08";
        cimekSearch.Tipus.Operator = Contentum.eQuery.Query.Operators.equals;
        cimekSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
        cimekSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
        cimekSearch.ErvKezd.Group = "100";
        cimekSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;
        cimekSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
        cimekSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
        cimekSearch.ErvVege.Group = "100";
        cimekSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

        Result partnerekResult = service.GetAllWithCim(execParam, partnerekSearch, cimekSearch);

        if (partnerekResult.IsError)
        {
            return response.SetErrorResponse("Hiba a partnerek lekérdezésekor: " + partnerekResult.ErrorMessage);
        }

        if (partnerekResult.Ds.Tables[0].Rows.Count < 1)
        {
            response.UserByRolesResponseDtos = new UserByRolesResponseDto[0];
            return response.SetSuccessResponse();
        }

        try
        {
            var partnerekResultEnumerable = partnerekResult.Ds.Tables[0].AsEnumerable();
            var partnerIds = partnerekResultEnumerable.Select(x => x["Id"].ToString()).ToList();

            Contentum.eAdmin.Service.KRT_FelhasznalokService felhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

            KRT_FelhasznalokSearch felhasznalokSearch = new KRT_FelhasznalokSearch();
            felhasznalokSearch.Partner_id.Value = Search.GetSqlInnerString(partnerIds.Distinct().ToArray());
            felhasznalokSearch.Partner_id.Operator = Contentum.eQuery.Query.Operators.inner;
            felhasznalokSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            felhasznalokSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            felhasznalokSearch.ErvKezd.Group = "100";
            felhasznalokSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;
            felhasznalokSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            felhasznalokSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            felhasznalokSearch.ErvVege.Group = "100";
            felhasznalokSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            Result felhasznalokResult = felhasznalokService.GetAll(execParam, felhasznalokSearch);

            if (felhasznalokResult.IsError)
            {
                return response.SetErrorResponse("Hiba a felhasználók lekérdezésekor: " + felhasznalokResult.ErrorMessage);
            }

            if (felhasznalokResult.Ds.Tables[0].Rows.Count < 1)
            {
                response.UserByRolesResponseDtos = new UserByRolesResponseDto[0];
                return response.SetSuccessResponse();
            }

            Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

            List<UserByRolesResponseDto> users = new List<UserByRolesResponseDto>();

            for (int i = 0; i < felhasznalokResult.Ds.Tables[0].Rows.Count; i++)
            {
                string felhasznaloId = felhasznalokResult.Ds.Tables[0].Rows[i]["Id"].ToString();

                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();

                search_csoportok.Csoport_Id_Jogalany.Value = felhasznaloId;
                search_csoportok.Csoport_Id_Jogalany.Operator = Contentum.eQuery.Query.Operators.equals;
                search_csoportok.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                search_csoportok.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
                search_csoportok.ErvKezd.Group = "100";
                search_csoportok.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;
                search_csoportok.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                search_csoportok.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
                search_csoportok.ErvVege.Group = "100";
                search_csoportok.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;
                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(execParam, search_csoportok);

                if (result_csoportok.IsError)
                {
                    return response.SetErrorResponse(result_csoportok.ErrorMessage);
                }
                else if (result_csoportok.Ds.Tables.Count == 0 || result_csoportok.Ds.Tables[0].Rows.Count == 0)
                {
                    continue;
                }
                else
                {
                    string kod = "IraIratCsatolmanyElektronikusAlairas";

                    if (getUserListByRolesRequest.UserByRoleDto.RoleCode == EnumsRoles.exp.ToString())
                    {
                        kod = "Expedialas";
                    }
                    else if (getUserListByRolesRequest.UserByRoleDto.RoleCode == EnumsRoles.post.ToString())
                    {
                        kod = "Postazas";
                    }

                    KRT_FunkciokService funkcioService = eAdminService.ServiceFactory.GetKRT_FunkciokService();

                    foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                    {
                        execParam.Record_Id = row["Id"].ToString();
                        Result funkcioResult = funkcioService.GetAllByCsoporttagSajatJogu(execParam);

                        if (funkcioResult.IsError)
                        {
                            return response.SetErrorResponse("Hiba a jogosultságok olvasásakor: " + funkcioResult.ErrorMessage);
                        }

                        if (funkcioResult.Ds.Tables[0].Rows.Count < 1)
                        {
                            continue;
                        }

                        var table = funkcioResult.Ds.Tables[0].AsEnumerable();

                        if (table.Any(x => x["Kod"].ToString() == kod))
                        {
                            UserByRolesResponseDto dto = new UserByRolesResponseDto();
                            users.Add(dto);

                            dto.OrganizationUnitName = row["Csoport_Nev"].ToString();
                            dto.OrganizationUnitCode = row["Csoport_Id"].ToString();

                            string pId = felhasznalokResult.Ds.Tables[0].Rows[i]["Partner_id"].ToString();
                            var partnerRow = partnerekResultEnumerable.First(x => x["Id"].ToString() == pId);

                            dto.DisplayName = partnerRow["Nev"].ToString();
                            dto.UserName = partnerRow["CimTobbi"].ToString();
                        }
                    }
                }
            }

            response.UserByRolesResponseDtos = users.ToArray();

            if (response.UserByRolesResponseDtos == null)
            {
                response.UserByRolesResponseDtos = new UserByRolesResponseDto[0];
            }

        }
        catch (Exception ex)
        {
            return response.SetErrorResponse("Hiba: " + ex.Message);
        }

        return response.SetSuccessResponse();
    }


    public DocumentResponse IssueDocument(DocumentRequest documentRequest)
    {
        Logger.Debug("DocumentService.IssueDocument.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        DocumentResponse response = new DocumentResponse();

        if (documentRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }

        try
        {

            DocumentListByRegistrationNumberRequest request = new DocumentListByRegistrationNumberRequest();

            request.RegistrationNumber = new RegistrationNumberDto();
            request.IsProcessed = documentRequest.Document.IsProcessed;
            request.IsProcessedSpecified = documentRequest.Document.IsProcessedSpecified;
            request.RegistrationNumber = documentRequest.Document.RegistrationNumber;

            Result result = ASPADOHelper.GetDocumentsData(request, 0, false, 0, false, localUser);

            if (result.IsError)
            {
                return response.SetErrorResponse(result.ErrorMessage);
            }
            else if (result.Ds.Tables[0].Rows.Count != 1)
            {
                return response.SetErrorResponse("A megadott adatok alapján nem határozható meg egyértelműen a keresett tétel"); ;
            }

            //Mi legyen itt a default? vagy dobjunk hib't ha nem küldik?
            string expedialasModja = "02";

            if (!string.IsNullOrEmpty(documentRequest.Document.DefaultExpedialasCode))
            {
                string expCode = documentRequest.Document.DefaultExpedialasCode;

                if (ASPADOHelper.ExpedialasKodTipusok.Keys.Any(x => x.Kod == expCode))
                {
                    expedialasModja = ASPADOHelper.ExpedialasKodTipusok.First(x => x.Key.Kod == expCode).Value.Kod;
                }
            }

            var iratRow = result.Ds.Tables[0].Rows[0];
            string iratId = iratRow["Id"].ToString();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            string iratTipus = iratRow["Irattipus"].ToString();
            string ugyFajtaja = iratRow["Ugy_Fajtaja"].ToString();
            string ugyiratId = iratRow["Ugyirat_Id"].ToString();
            string iratAllapot = iratRow["Allapot"].ToString();

            if (iratAllapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
            {
                Logger.Info(string.Format("Az irat már kiadmányozott állapotban van (IratId: {0}) így success-et visszaadva megszakadt a további feldolgozás.", iratId));
                return response.SetSuccessResponse();
            }
            if (ugyFajtaja == "1")
            {
                //határozat, hatósági bizonyítvány, végzés
                if (iratTipus == "111" || iratTipus == "112" || iratTipus == "275")
                    if (!string.IsNullOrEmpty(documentRequest.Document.DefaultExpedialasCode))
                    {
                        //LZS - BLG_10089
                        //HatosagiStatisztikaTargyszavakTemplate template = ASPADOHelper.GetTemplateForDocumentType(documentRequest.Document.DocumentType.ToString(), localUser, documentRequest.Document.DefaultExpedialasCode);
                        string templateId = ASPADOHelper.GetTemplateForDocumentType(documentRequest.Document.DocumentType.ToString(), localUser, documentRequest.Document.DefaultExpedialasCode);
                        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                        iratokService.SetTemplateToIrat(iratId, templateId);
                        //LZS - BLG_10089
                        //END//
                    }
            }

            bool elektronikus = false;
            if (!string.IsNullOrEmpty(documentRequest.Document.DefaultExpedialasCode))
            {
                string expCode = documentRequest.Document.DefaultExpedialasCode;

                using (var iratPeldanyService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService())
                {
                    var res = iratPeldanyService.GetElsoIratPeldanyByIraIrat(ASPADOHelper.GetExecParam(localUser), iratId);

                    if (!res.IsError)
                    {
                        EREC_PldIratPeldanyok iratPeldany = (EREC_PldIratPeldanyok)res.Record;

                        var fiokok = HivataliKapuConfig.GetFiokok();

                        if (fiokok != null && fiokok.Count > 0)
                        {
                            for (int i = 0; i < fiokok.Count; i++)
                            {
                                if (fiokok[i].Name == expCode)
                                {
                                    elektronikus = true;
                                }
                            }
                        }
                        else if (expCode == "008")
                        {
                            elektronikus = true;
                        }

                        if (elektronikus)
                        {
                            iratPeldany.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                            iratPeldany.Updated.UgyintezesModja = true;
                            iratPeldany.KuldesMod = expedialasModja;
                            iratPeldany.Updated.KuldesMod = true;
                            ExecParam pldIrat = ASPADOHelper.GetExecParam(localUser);
                            pldIrat.Record_Id = iratPeldany.Id;
                            ASPADOHelper.LoadPartnerKRID(localUser, iratPeldany, ugyiratId);
                            Result pldResult = iratPeldanyService.Update(pldIrat, iratPeldany);
                            if (pldResult.IsError)
                            {
                                return response.SetErrorResponse("Nem sikerült módosítani az iratpéldányt. " + pldResult.ErrorMessage);
                            }
                        }
                    }
                    else
                    {
                        return response.SetErrorResponse("Nem sikerült módosítani az iratpéldányt. " + res.ErrorMessage);
                    }
                }
            }

            string adatHordozoTipusaNev;
            if (elektronikus)
            {
                adatHordozoTipusaNev = "Elektronikus (nincs papír)";
            }
            else
            {
                adatHordozoTipusaNev = result.Ds.Tables[0].Rows[0]["AdathordozoTipusa_Nev"].ToString();
            }

            if (string.IsNullOrEmpty(adatHordozoTipusaNev))
            {
                return response.SetErrorResponse("AdathordozoTipusa üres");
            }

            string adminuser = "";

            if (documentRequest.Document.Administrator != null && !string.IsNullOrEmpty(documentRequest.Document.Administrator.LoginName))
            {
                adminuser = documentRequest.Document.Administrator.LoginName.GetMappedUserId();
            }

            bool isDifferentFromCallingUser = (!string.IsNullOrEmpty(adminuser) ? (localUser != adminuser) : false);

            if (isDifferentFromCallingUser)
            {
                EREC_IratAlairok localIratAlairo = GetIratAlairok(iratId, localUser, "M_UTO", KodTarak.ALAIRO_SZEREP.Jovahagyo, "1");

                string localIratAlairoSzabalyId = GetAlairoSzabalyId(localUser, iratId, "M_UTO", response);

                if (!string.IsNullOrEmpty(response.ErrorMessage))
                {
                    return response.SetErrorResponse(response.ErrorMessage);
                }

                if (!string.IsNullOrEmpty(localIratAlairoSzabalyId))
                {
                    localIratAlairo.AlairasSzabaly_Id = localIratAlairoSzabalyId;
                    localIratAlairo.Updated.AlairasSzabaly_Id = true;
                }
                else
                {
                    return response.SetErrorResponse("Nem található aláírószabály."); ;
                }

                var insertAlairasResult = InsertAlairas(localUser, localIratAlairo, response);
                if (!string.IsNullOrEmpty(insertAlairasResult.ErrorMessage))
                {
                    Logger.Error(ASPADOHelper.Constants.Messages.InsertAlairoError + insertAlairasResult.ErrorMessage);
                    return response.SetErrorResponse(ASPADOHelper.Constants.Messages.InsertAlairoError);
                }

                if (adatHordozoTipusaNev == "Hagyományos, papír")
                {
                    EREC_IratAlairok adminUtolagosIratAlairo = GetIratAlairok(iratId, adminuser, "M_UTO", KodTarak.ALAIRO_SZEREP.Kiadmanyozo, "2");

                    string adminUtolagosIratAlairoSzabalyId = GetAlairoSzabalyId(adminuser, iratId, "M_UTO", response);

                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetErrorResponse(response.ErrorMessage);
                    }
                    if (!string.IsNullOrEmpty(adminUtolagosIratAlairoSzabalyId))
                    {
                        adminUtolagosIratAlairo.AlairasSzabaly_Id = adminUtolagosIratAlairoSzabalyId;
                        adminUtolagosIratAlairo.Updated.AlairasSzabaly_Id = true;
                    }
                    else
                    {
                        return response.SetErrorResponse("Nem található aláírószabály."); ;
                    }

                    var insertAlairasResultAdmin = InsertAlairas(localUser, adminUtolagosIratAlairo, response);
                    if (!string.IsNullOrEmpty(insertAlairasResultAdmin.ErrorMessage))
                    {
                        Logger.Error(ASPADOHelper.Constants.Messages.InsertAlairoError + insertAlairasResultAdmin.ErrorMessage);
                        return response.SetErrorResponse(ASPADOHelper.Constants.Messages.InsertAlairoError);
                    }
                }
                else
                {
                    EREC_IratAlairok adminElojegyzetIratAlairo = GetIratAlairok(iratId, adminuser, "E_EMB", KodTarak.ALAIRO_SZEREP.Kiadmanyozo, "2");

                    string adminElojegyzetIratAlairoSzabalyId = GetAlairoSzabalyId(adminuser, iratId, "E_EMB", response);
                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetErrorResponse(response.ErrorMessage);
                    }
                    if (string.IsNullOrEmpty(adminElojegyzetIratAlairoSzabalyId))
                    {
                        adminElojegyzetIratAlairoSzabalyId = GetAlairoSzabalyId(adminuser, iratId, "M_ADM", response);
                        if (!string.IsNullOrEmpty(response.ErrorMessage))
                        {
                            return response.SetErrorResponse(response.ErrorMessage);
                        }
                    }
                    if (string.IsNullOrEmpty(adminElojegyzetIratAlairoSzabalyId))
                    {
                        return response.SetErrorResponse("Nem található aláírószabály."); ;
                    }

                    adminElojegyzetIratAlairo.AlairasSzabaly_Id = adminElojegyzetIratAlairoSzabalyId;
                    adminElojegyzetIratAlairo.Updated.AlairasSzabaly_Id = true;

                    var adminElojegyzetIratAlairoInsertResult = InsertAlairas(localUser, adminElojegyzetIratAlairo, response);
                    if (!string.IsNullOrEmpty(adminElojegyzetIratAlairoInsertResult.ErrorMessage))
                    {
                        Logger.Error(ASPADOHelper.Constants.Messages.InsertAlairoError + adminElojegyzetIratAlairoInsertResult.ErrorMessage);
                        return response.SetErrorResponse(ASPADOHelper.Constants.Messages.InsertAlairoError);
                    }
                }
            }
            else
            {
                if (adatHordozoTipusaNev == "Hagyományos, papír")
                {
                    EREC_IratAlairok adminUtolagosIratAlairo = GetIratAlairok(iratId, localUser, "M_UTO", KodTarak.ALAIRO_SZEREP.Kiadmanyozo, "1");

                    string adminUtolagosIratAlairoSzabalyId = GetAlairoSzabalyId(localUser, iratId, "M_UTO", response);
                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetErrorResponse(response.ErrorMessage);
                    }
                    if (!string.IsNullOrEmpty(adminUtolagosIratAlairoSzabalyId))
                    {
                        adminUtolagosIratAlairo.AlairasSzabaly_Id = adminUtolagosIratAlairoSzabalyId;
                        adminUtolagosIratAlairo.Updated.AlairasSzabaly_Id = true;
                    }
                    else
                    {
                        return response.SetErrorResponse("Nem található aláírószabály."); ;
                    }

                    var adminUtolagosIratAlairosInsertResult = InsertAlairas(localUser, adminUtolagosIratAlairo, response);
                    if (!string.IsNullOrEmpty(adminUtolagosIratAlairosInsertResult.ErrorMessage))
                    {
                        Logger.Error(ASPADOHelper.Constants.Messages.InsertAlairoError + adminUtolagosIratAlairosInsertResult.ErrorMessage);
                        return response.SetErrorResponse(ASPADOHelper.Constants.Messages.InsertAlairoError);
                    }
                }
                else
                {
                    EREC_IratAlairok adminElojegyzetIratAlairo = GetIratAlairok(iratId, localUser, "E_EMB", KodTarak.ALAIRO_SZEREP.Kiadmanyozo, "1");

                    string adminElojegyzetIratAlairoSzabalyId = GetAlairoSzabalyId(localUser, iratId, "E_EMB", response);
                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetErrorResponse(response.ErrorMessage);
                    }
                    if (string.IsNullOrEmpty(adminElojegyzetIratAlairoSzabalyId))
                    {
                        adminElojegyzetIratAlairoSzabalyId = GetAlairoSzabalyId(localUser, iratId, "M_ADM", response);
                        if (!string.IsNullOrEmpty(response.ErrorMessage))
                        {
                            return response.SetErrorResponse(response.ErrorMessage);
                        }
                    }
                    if (string.IsNullOrEmpty(adminElojegyzetIratAlairoSzabalyId))
                    {
                        return response.SetErrorResponse("Nem található aláírószabály."); ;
                    }

                    adminElojegyzetIratAlairo.AlairasSzabaly_Id = adminElojegyzetIratAlairoSzabalyId;
                    adminElojegyzetIratAlairo.Updated.AlairasSzabaly_Id = true;

                    var adminElojegyzettIratAlairoInsertResult = InsertAlairas(localUser, adminElojegyzetIratAlairo, response);
                    if (!string.IsNullOrEmpty(adminElojegyzettIratAlairoInsertResult.ErrorMessage))
                    {
                        Logger.Error(ASPADOHelper.Constants.Messages.InsertAlairoError + adminElojegyzettIratAlairoInsertResult.ErrorMessage);
                        return response.SetErrorResponse(ASPADOHelper.Constants.Messages.InsertAlairoError);
                    }
                }
            }

            ASPADOHelper.UploadCsatolmanyokForRequest(documentRequest, response, iratId, localUser);

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                return response;
            }
        }
        catch (Exception ex)
        {
            Logger.Error("IssueDocument hiba", ex);
            return response.SetErrorResponse("IssueDocument hiba: " + ex.Message);
        }

        return response.SetSuccessResponse();
    }


    public DocumentResponse IssueDocumentAndDispense(DocumentRequest documentRequest)
    {
        Logger.Debug("DocumentService.IssueDocumentAndDispense.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        DocumentResponse response = new DocumentResponse();

        if (documentRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }
        try
        {

            DocumentListByRegistrationNumberRequest request = new DocumentListByRegistrationNumberRequest();

            request.RegistrationNumber = new RegistrationNumberDto();
            request.IsProcessed = documentRequest.Document.IsProcessed;
            request.IsProcessedSpecified = documentRequest.Document.IsProcessedSpecified;
            request.RegistrationNumber = documentRequest.Document.RegistrationNumber;

            Result result = ASPADOHelper.GetDocumentsData(request, 0, false, 0, false, localUser);

            if (result.IsError)
            {
                return response.SetErrorResponse(result.ErrorMessage);
            }
            else if (result.Ds.Tables[0].Rows.Count != 1)
            {
                return response.SetErrorResponse("A megadott adatok alapján nem határozható meg egyértelműen a keresett tétel"); ;
            }

            //Mi legyen itt a default? vagy dobjunk hib't ha nem küldik?
            string expedialasModja = "02";

            if (!string.IsNullOrEmpty(documentRequest.Document.DefaultExpedialasCode))
            {
                string expCode = documentRequest.Document.DefaultExpedialasCode;

                if (ASPADOHelper.ExpedialasKodTipusok.Keys.Any(x => x.Kod == expCode))
                {
                    expedialasModja = ASPADOHelper.ExpedialasKodTipusok.First(x => x.Key.Kod == expCode).Value.Kod;
                }
            }

            var iratRow = result.Ds.Tables[0].Rows[0];
            string iratId = iratRow["Id"].ToString();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
            string iratTipus = iratRow["Irattipus"].ToString();
            string ugyFajtaja = iratRow["Ugy_Fajtaja"].ToString();
            string ugyiratId = iratRow["Ugyirat_Id"].ToString();
            string iratAllapot = iratRow["Allapot"].ToString();

            if (iratAllapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
            {
                Logger.Info(string.Format("Az irat már kiadmányozott állapotban van (IratId: {0}) így success-et visszaadva megszakadt a további feldolgozás.", iratId));
                return response.SetSuccessResponse();
            }

            if (ugyFajtaja == "1")
            {
                //határozat, hatósági bizonyítvány, végzés
                if (iratTipus == "111" || iratTipus == "112" || iratTipus == "275")
                    if (!string.IsNullOrEmpty(documentRequest.Document.DefaultExpedialasCode))
                    {

                        //LZS - BLG_10089
                        //HatosagiStatisztikaTargyszavakTemplate template = ASPADOHelper.GetTemplateForDocumentType(documentRequest.Document.DocumentType.ToString(), localUser, documentRequest.Document.DefaultExpedialasCode);
                        string templateId = ASPADOHelper.GetTemplateForDocumentType(documentRequest.Document.DocumentType.ToString(), localUser, documentRequest.Document.DefaultExpedialasCode);
                        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                        iratokService.SetTemplateToIrat(iratId, templateId);
                        //LZS - BLG_10089
                        //END//
                    }
            }

            bool elektronikus = false;
            if (!string.IsNullOrEmpty(documentRequest.Document.DefaultExpedialasCode))
            {
                string expCode = documentRequest.Document.DefaultExpedialasCode;

                using (var iratPeldanyService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService())
                {
                    var res = iratPeldanyService.GetElsoIratPeldanyByIraIrat(ASPADOHelper.GetExecParam(localUser), iratId);

                    if (!res.IsError)
                    {
                        EREC_PldIratPeldanyok iratPeldany = (EREC_PldIratPeldanyok)res.Record;

                        var fiokok = HivataliKapuConfig.GetFiokok();

                        if (expCode == "008")
                        {
                            elektronikus = true;
                        }
                        else if (fiokok != null && fiokok.Count > 0)
                        {
                            for (int i = 0; i < fiokok.Count; i++)
                            {
                                if (fiokok[i].Name == expCode)
                                {
                                    elektronikus = true;
                                }
                            }
                        }


                        if (elektronikus)
                        {
                            iratPeldany.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                            iratPeldany.Updated.UgyintezesModja = true;

                            iratPeldany.KuldesMod = expedialasModja;
                            iratPeldany.Updated.KuldesMod = true;
                            ExecParam pldIrat = ASPADOHelper.GetExecParam(localUser);
                            pldIrat.Record_Id = iratPeldany.Id;

                            ASPADOHelper.LoadPartnerKRID(localUser, iratPeldany, ugyiratId);
                            Result pldResult = iratPeldanyService.Update(pldIrat, iratPeldany);
                            if (pldResult.IsError)
                            {
                                return response.SetErrorResponse("Nem sikerült módosítani az iratpéldányt. " + pldResult.ErrorMessage);
                            }
                        }
                    }
                    else
                    {
                        return response.SetErrorResponse("Nem sikerült módosítani az iratpéldányt. " + res.ErrorMessage);
                    }
                }
            }

            string adatHordozoTipusaNev;
            if (elektronikus)
            {
                adatHordozoTipusaNev = "Elektronikus (nincs papír)";
            }
            else
            {
                adatHordozoTipusaNev = result.Ds.Tables[0].Rows[0]["AdathordozoTipusa_Nev"].ToString();
            }

            if (string.IsNullOrEmpty(adatHordozoTipusaNev))
            {
                return response.SetErrorResponse("AdathordozoTipusa üres");
            }

            string adminuser = "";

            if (documentRequest.Document.Administrator != null && !string.IsNullOrEmpty(documentRequest.Document.Administrator.LoginName))
            {
                adminuser = documentRequest.Document.Administrator.LoginName.GetMappedUserId();
            }

            bool isDifferentFromCallingUser = (!string.IsNullOrEmpty(adminuser) ? (localUser != adminuser) : false);

            if (isDifferentFromCallingUser)
            {
                EREC_IratAlairok localIratAlairo = GetIratAlairok(iratId, localUser, "M_UTO", KodTarak.ALAIRO_SZEREP.Jovahagyo, "1");

                string localIratAlairoSzabalyId = GetAlairoSzabalyId(localUser, iratId, "M_UTO", response);

                if (!string.IsNullOrEmpty(response.ErrorMessage))
                {
                    return response.SetErrorResponse(response.ErrorMessage);
                }

                if (!string.IsNullOrEmpty(localIratAlairoSzabalyId))
                {
                    localIratAlairo.AlairasSzabaly_Id = localIratAlairoSzabalyId;
                    localIratAlairo.Updated.AlairasSzabaly_Id = true;
                }
                else
                {
                    return response.SetErrorResponse("Nem található aláírószabály."); ;
                }

                var insertLocalIratAlairasResult = InsertAlairas(localUser, localIratAlairo, response);
                if (!string.IsNullOrEmpty(insertLocalIratAlairasResult.ErrorMessage))
                {
                    Logger.Error(ASPADOHelper.Constants.Messages.InsertAlairoError + insertLocalIratAlairasResult.ErrorMessage);
                    return response.SetErrorResponse(ASPADOHelper.Constants.Messages.InsertAlairoError);
                }
                if (adatHordozoTipusaNev == "Hagyományos, papír")
                {
                    EREC_IratAlairok adminUtolagosIratAlairo = GetIratAlairok(iratId, adminuser, "M_UTO", KodTarak.ALAIRO_SZEREP.Kiadmanyozo, "2");

                    string adminUtolagosIratAlairoSzabalyId = GetAlairoSzabalyId(adminuser, iratId, "M_UTO", response);

                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetErrorResponse(response.ErrorMessage);
                    }
                    if (!string.IsNullOrEmpty(adminUtolagosIratAlairoSzabalyId))
                    {
                        adminUtolagosIratAlairo.AlairasSzabaly_Id = adminUtolagosIratAlairoSzabalyId;
                        adminUtolagosIratAlairo.Updated.AlairasSzabaly_Id = true;
                    }
                    else
                    {
                        return response.SetErrorResponse("Nem található aláírószabály."); ;
                    }

                    var insertAlairasResult = InsertAlairas(localUser, adminUtolagosIratAlairo, response);
                    if (!string.IsNullOrEmpty(insertAlairasResult.ErrorMessage))
                    {
                        Logger.Error(ASPADOHelper.Constants.Messages.InsertAlairoError + insertAlairasResult.ErrorMessage);
                        return response.SetErrorResponse(ASPADOHelper.Constants.Messages.InsertAlairoError);
                    }
                }
                else
                {
                    EREC_IratAlairok adminElojegyzetIratAlairo = GetIratAlairok(iratId, adminuser, "E_EMB", KodTarak.ALAIRO_SZEREP.Kiadmanyozo, "2");

                    string adminElojegyzetIratAlairoSzabalyId = GetAlairoSzabalyId(adminuser, iratId, "E_EMB", response);
                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetErrorResponse(response.ErrorMessage);
                    }
                    if (string.IsNullOrEmpty(adminElojegyzetIratAlairoSzabalyId))
                    {
                        adminElojegyzetIratAlairoSzabalyId = GetAlairoSzabalyId(adminuser, iratId, "M_ADM", response);
                        if (!string.IsNullOrEmpty(response.ErrorMessage))
                        {
                            return response.SetErrorResponse(response.ErrorMessage);
                        }
                    }
                    if (string.IsNullOrEmpty(adminElojegyzetIratAlairoSzabalyId))
                    {
                        return response.SetErrorResponse("Nem található aláírószabály."); ;
                    }

                    adminElojegyzetIratAlairo.AlairasSzabaly_Id = adminElojegyzetIratAlairoSzabalyId;
                    adminElojegyzetIratAlairo.Updated.AlairasSzabaly_Id = true;

                    var insertAdminElojegyzetAlairoResult = InsertAlairas(localUser, adminElojegyzetIratAlairo, response);
                    if (!string.IsNullOrEmpty(insertAdminElojegyzetAlairoResult.ErrorMessage))
                    {
                        Logger.Error(ASPADOHelper.Constants.Messages.InsertAlairoError + insertAdminElojegyzetAlairoResult.ErrorMessage);
                        return response.SetErrorResponse(ASPADOHelper.Constants.Messages.InsertAlairoError);
                    }
                }
            }
            else
            {
                if (adatHordozoTipusaNev == "Hagyományos, papír")
                {
                    EREC_IratAlairok adminUtolagosIratAlairo = GetIratAlairok(iratId, localUser, "M_UTO", KodTarak.ALAIRO_SZEREP.Kiadmanyozo, "1");

                    string adminUtolagosIratAlairoSzabalyId = GetAlairoSzabalyId(localUser, iratId, "M_UTO", response);
                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetErrorResponse(response.ErrorMessage);
                    }
                    if (!string.IsNullOrEmpty(adminUtolagosIratAlairoSzabalyId))
                    {
                        adminUtolagosIratAlairo.AlairasSzabaly_Id = adminUtolagosIratAlairoSzabalyId;
                        adminUtolagosIratAlairo.Updated.AlairasSzabaly_Id = true;
                    }
                    else
                    {
                        return response.SetErrorResponse("Nem található aláírószabály."); ;
                    }

                    var adminUtolagosIratAlairoInsertResult = InsertAlairas(localUser, adminUtolagosIratAlairo, response);
                    if (!string.IsNullOrEmpty(adminUtolagosIratAlairoInsertResult.ErrorMessage))
                    {
                        Logger.Error(ASPADOHelper.Constants.Messages.InsertAlairoError + adminUtolagosIratAlairoInsertResult.ErrorMessage);
                        return response.SetErrorResponse(ASPADOHelper.Constants.Messages.InsertAlairoError);
                    }
                }
                else
                {
                    EREC_IratAlairok adminElojegyzetIratAlairo = GetIratAlairok(iratId, localUser, "E_EMB", KodTarak.ALAIRO_SZEREP.Kiadmanyozo, "1");

                    string adminElojegyzetIratAlairoSzabalyId = GetAlairoSzabalyId(localUser, iratId, "E_EMB", response);
                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetErrorResponse(response.ErrorMessage);
                    }
                    if (string.IsNullOrEmpty(adminElojegyzetIratAlairoSzabalyId))
                    {
                        adminElojegyzetIratAlairoSzabalyId = GetAlairoSzabalyId(localUser, iratId, "M_ADM", response);
                        if (!string.IsNullOrEmpty(response.ErrorMessage))
                        {
                            return response.SetErrorResponse(response.ErrorMessage);
                        }
                    }
                    if (string.IsNullOrEmpty(adminElojegyzetIratAlairoSzabalyId))
                    {
                        return response.SetErrorResponse("Nem található aláírószabály."); ;
                    }

                    adminElojegyzetIratAlairo.AlairasSzabaly_Id = adminElojegyzetIratAlairoSzabalyId;
                    adminElojegyzetIratAlairo.Updated.AlairasSzabaly_Id = true;

                    var insertAdminElojegyzetIratAlairoResult = InsertAlairas(localUser, adminElojegyzetIratAlairo, response);
                    if (!string.IsNullOrEmpty(insertAdminElojegyzetIratAlairoResult.ErrorMessage))
                    {
                        Logger.Error(ASPADOHelper.Constants.Messages.InsertAlairoError + insertAdminElojegyzetIratAlairoResult.ErrorMessage);
                        return response.SetErrorResponse(ASPADOHelper.Constants.Messages.InsertAlairoError);
                    }
                }
            }




            if (adatHordozoTipusaNev == "Hagyományos, papír")
            {
                ASPADOHelper.Expedialas(iratId, expedialasModja, localUser);
            }

            ASPADOHelper.UploadCsatolmanyokForRequest(documentRequest, response, iratId, localUser);

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                return response;
            }
        }
        catch (Exception ex)
        {
            Logger.Error("IssueDocumentAndDispense hiba", ex);
            return response.SetErrorResponse("IssueDocumentAndDispense hiba: " + ex.Message);
        }

        return response.SetSuccessResponse();
    }


    public PackageResponse IssuePackage(PackageRequest packageRequest)
    {
        Logger.Debug("DocumentService.IssuePackage.Start");
        headers.ValidateUnknownSoapHeaders();
        throw new NotImplementedException();
    }


    public PackagesResponse IssuePackages(PackageListRequest packageListRequest)
    {
        Logger.Debug("DocumentService.IssuePackages.Start");
        headers.ValidateUnknownSoapHeaders();
        throw new NotImplementedException();
    }


    public BaseResponse MissedProcessDocument(MissedProcessDocumentRequest missedProcessDocumentRequest)
    {
        Logger.Debug("DocumentService.MissedProcessDocument.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        BaseResponse response = new BaseResponse();

        if (missedProcessDocumentRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }
        if (missedProcessDocumentRequest.RegistrationNumber == null)
        {
            return response.SetErrorResponse("Hiányzik a RegistrationNumber");
        }
        try
        {
            DocumentListByRegistrationNumberRequest request = new DocumentListByRegistrationNumberRequest();

            request.RegistrationNumber = new RegistrationNumberDto();
            request.RegistrationNumber = missedProcessDocumentRequest.RegistrationNumber;

            Result result = ASPADOHelper.GetDocumentsData(request, 0, false, 0, false, localUser);

            if (result.IsError)
            {
                return response.SetErrorResponse(result.ErrorMessage);
            }
            else if (!result.HasData())
            {
                return response.SetErrorResponse("A megadott adatok alapján nem határozható meg egyértelműen a keresett tétel"); ;
            }

            var iratRow = result.Ds.Tables[0].Rows[0];
            string iratId = iratRow["Id"].ToString();
            var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            var visszaVonasResult = iratokService.ElintezesVisszavonasa(ASPADOHelper.GetExecParam(localUser), iratId);

            if (visszaVonasResult.IsError)
            {
                int resultCode;
                bool canBeParsed = int.TryParse(visszaVonasResult.ErrorCode, out resultCode);
                string errorMessage = canBeParsed ? ResultError.GetErrorMessageByErrorCode(resultCode) : visszaVonasResult.ErrorMessage;
                return response.SetErrorResponse("Nem sikerült az elintézés visszavonása! " + errorMessage);
            }
            return response.SetSuccessResponse();
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.MissedProcessDocument.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }
    }


    public BaseResponse ProcessDocument(DocumentByRegistrationNumberRequest documentByRegistrationNumberRequest)
    {
        Logger.Debug("DocumentService.ProcessDocument.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        BaseResponse response = new BaseResponse();

        if (documentByRegistrationNumberRequest == null)
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }
        try
        {

            DocumentListByRegistrationNumberRequest request = new DocumentListByRegistrationNumberRequest();
            request.RegistrationNumber = documentByRegistrationNumberRequest.RegistrationNumber;
            GetDocumentsResponse documentsResponse = GetDocuments(request, 0, false, 0, false);

            if (!documentsResponse.IsSucceeded)
            {
                return response.SetErrorResponse(documentsResponse.ErrorMessage);
            }
            else if (documentsResponse.TotalItemCount == 0)
            {
                return response.SetErrorResponse("A megadott adatok alapján nem határozható meg egyértelműen a keresett tétel");
            }
            else if (documentsResponse.TotalItemCount == 1)
            {
                string azonosito = documentsResponse.Documents.FirstOrDefault().DocumentId;

                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParam = ASPADOHelper.GetExecParam(localUser);
                EREC_IraIratokSearch search = new EREC_IraIratokSearch();
                search.Azonosito.Value = azonosito;
                search.Azonosito.Operator = Contentum.eQuery.Query.Operators.equals;

                Result result = service.GetAll(execParam, search);

                if (result.IsError)
                {
                    return response.SetErrorResponse(result.ErrorMessage);
                }
                else
                {
                    EREC_IraIratok irat = new EREC_IraIratok();
                    Utility.LoadBusinessDocumentFromDataRow(irat, result.Ds.Tables[0].Rows[0]);
                    irat.Updated.SetValueAll(false);
                    irat.Base.Updated.SetValueAll(false);
                    irat.Base.Updated.Ver = true;

                    Result elintezesResult = service.Elintezes(execParam, irat, null);

                    if (elintezesResult.IsError)
                    {
                        if (elintezesResult.ErrorCode == "80012")
                        {
                            return response.SetErrorResponse("A dokumentumnak nem az örzője.");
                        }
                        return response.SetErrorResponse(elintezesResult.ErrorMessage);
                    }
                }

                return response.SetSuccessResponse();
            }
            else
            {
                return response.SetErrorResponse("A megadott adatok alapján nem határozható meg egyértelműen a keresett tétel");
            }
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.ProcessDocument.Hiba", ex);
            return response.SetErrorResponse(ex.Message);
        }
    }


    public GetBatchFilingStatusResponse GetBatchFilingStatus(GetBatchFilingStatus getBatchFilingStatus)
    {
        int[] lineIdList = getBatchFilingStatus.lineIdList;
        Logger.Debug("DocumentService.GetBatchFilingStatus.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        GetBatchFilingStatusResponse response = new GetBatchFilingStatusResponse();
        response.GetBatchFilingStatusResult = new GetBatchFilingStatusResult();

        if (lineIdList == null)
        {
            response.GetBatchFilingStatusResult.SetErrorResponse(ASPADOHelper.Constants.Messages.SoapRequestError);
            return response;
        }

        if (lineIdList.Length < 1)
        {
            response.GetBatchFilingStatusResult.SetErrorResponse("Nem található tömeges iktatás");
            return response;
        }

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            response.GetBatchFilingStatusResult.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
            return response;
        }

        try
        {
            EREC_TomegesIktatasTetelekService tetelekService = eRecordService.ServiceFactory.GetEREC_TomegesIktatasTetelekService();
            ExecParam execParam = ASPADOHelper.GetExecParam(localUser);

            EREC_TomegesIktatasTetelekSearch tetelekSearch = new EREC_TomegesIktatasTetelekSearch();
            tetelekSearch.Forras_Azonosito.Value = Search.GetSqlInnerString(lineIdList.ToArray().Select(x => x.ToString()).ToArray());
            tetelekSearch.Forras_Azonosito.Operator = Contentum.eQuery.Query.Operators.inner;

            Result tetelekResult = tetelekService.GetAll(execParam, tetelekSearch);

            if (tetelekResult.IsError)
            {
                response.GetBatchFilingStatusResult.SetErrorResponse(tetelekResult.ErrorMessage);
                return response;
            }
            else if (tetelekResult.Ds.Tables[0].Rows.Count < 1)
            {
                response.GetBatchFilingStatusResult.SetErrorResponse("Nem található tétel");
                return response;
            }
            else
            {
                var list = tetelekResult.Ds.Tables[0].AsEnumerable();

                response.GetBatchFilingStatusResult.BatchFilings = new BatchFilingResponseDocumentDto[lineIdList.Count()];

                for (int i = 0; i < lineIdList.Count(); i++)
                {
                    var row = list.FirstOrDefault(x => x["Forras_Azonosito"].ToString() == lineIdList[i].ToString());

                    response.GetBatchFilingStatusResult.BatchFilings[i] = new BatchFilingResponseDocumentDto();
                    response.GetBatchFilingStatusResult.BatchFilings[i].LineId = lineIdList[i];
                    response.GetBatchFilingStatusResult.BatchFilings[i].LineIdSpecified = true;
                    response.GetBatchFilingStatusResult.BatchFilings[i].IdSpecified = true;
                    response.GetBatchFilingStatusResult.BatchFilings[i].Id = int.Parse(row["Note"].ToString());

                    if (row != null)
                    {
                        //tetelekSearch.Allapot.Value = "2";
                        //tetelekSearch.Allapot.Operator = Contentum.eQuery.Query.Operators.equals;                    

                        string allapot = row["Allapot"].ToString();

                        if (allapot == "2" && row["Irat_Id"] != null)
                        {

                            DocumentResponse resp = new DocumentResponse();
                            resp = ASPADOHelper.GetDocumentByAzonosito(row["Irat_Id"].ToString(), resp, localUser);

                            if (string.IsNullOrEmpty(resp.ErrorMessage))
                            {
                                response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber = new FilingRequestRegistartionNumberDto();
                                response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.MainNumber = resp.Document.RegistrationNumber.MainNumber;
                                response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.MainNumberSpecified = resp.Document.RegistrationNumber.MainNumberSpecified;
                                response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.Prefix = resp.Document.RegistrationNumber.Prefix;
                                response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.SubNumber = resp.Document.RegistrationNumber.SubNumber;
                                response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.SubNumberSpecified = resp.Document.RegistrationNumber.SubNumberSpecified;
                                response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.Year = resp.Document.RegistrationNumber.Year;
                                response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.YearSpecified = resp.Document.RegistrationNumber.YearSpecified;

                                if (resp.Document.RegistrationNumber.RegistrationBook != null)
                                {
                                    response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.RegistrationBook = new FilingRequestRegistrationBookDto();
                                    response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.RegistrationBook.Name = resp.Document.RegistrationNumber.RegistrationBook.Name;
                                    response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.RegistrationBook.Prefix = resp.Document.RegistrationNumber.RegistrationBook.Prefix;
                                    response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.RegistrationBook.RegistrationBookId = resp.Document.RegistrationNumber.RegistrationBook.RegistrationBookId;
                                    response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.RegistrationBook.RegistrationBookIdSpecified = resp.Document.RegistrationNumber.RegistrationBook.RegistrationBookIdSpecified;
                                    response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.RegistrationBook.Year = resp.Document.RegistrationNumber.RegistrationBook.Year;
                                    response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.RegistrationBook.YearSpecified = resp.Document.RegistrationNumber.RegistrationBook.YearSpecified;
                                    response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.RegistrationBook.Closed = resp.Document.RegistrationNumber.RegistrationBook.Closed;
                                    response.GetBatchFilingStatusResult.BatchFilings[i].RegistrationNumber.RegistrationBook.ClosedSpecified = resp.Document.RegistrationNumber.RegistrationBook.ClosedSpecified;
                                }
                                response.GetBatchFilingStatusResult.BatchFilings[i].IsComplete = true;
                                response.GetBatchFilingStatusResult.BatchFilings[i].IsCompleteSpecified = true;
                            }
                            else
                            {
                                response.GetBatchFilingStatusResult.BatchFilings[i].ErrorMessage = resp.ErrorMessage;
                                response.GetBatchFilingStatusResult.BatchFilings[i].IsComplete = false;
                                response.GetBatchFilingStatusResult.BatchFilings[i].IsCompleteSpecified = true;
                            }
                        }
                        else
                        {
                            response.GetBatchFilingStatusResult.BatchFilings[i].IsComplete = false;
                            response.GetBatchFilingStatusResult.BatchFilings[i].IsCompleteSpecified = true;

                            if (allapot == "9")
                            {
                                response.GetBatchFilingStatusResult.BatchFilings[i].ErrorMessage = "Hiba az iktatás folyamán: " + row["Hiba"] == null ? "" : row["Hiba"].ToString();
                            }
                        }
                    }
                    else
                    {
                        response.GetBatchFilingStatusResult.BatchFilings[i].ErrorMessage = "Nem található az adott lineId";
                        response.GetBatchFilingStatusResult.BatchFilings[i].IsComplete = false;
                        response.GetBatchFilingStatusResult.BatchFilings[i].IsCompleteSpecified = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("DocumentService.GetBatchFilingStatus.Hiba", ex);
            response.GetBatchFilingStatusResult.SetErrorResponse(ex.Message);
            return response;
        }
        response.GetBatchFilingStatusResult.IsFinished = true;
        response.GetBatchFilingStatusResult.IsFinishedSpecified = true;
        response.GetBatchFilingStatusResult.SetSuccessResponse();

        foreach (var item in response.GetBatchFilingStatusResult.BatchFilings) // ki kell törölni mert az ASP nem fogadja el a választ ha benne van.
        {
            item.RegistrationNumber = null;
        }
        return response;
    }

    public GetCustomerPortalsResponse GetCustomerPortals(int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified)
    {
        Logger.Debug("DocumentService.GetCustomerPortals.Start");
        string userId = headers.ValidateUnknownSoapHeaders();
        GetCustomerPortalsResponse response = new GetCustomerPortalsResponse();

        string localUser = userId.GetMappedUserId();

        if (string.IsNullOrEmpty(localUser))
        {
            return response.SetErrorResponse(ASPADOHelper.Constants.Messages.UserError);
        }

        List<CustomerPortalResponseDto> cp = new List<CustomerPortalResponseDto>();

        try
        {
            var fiokok = HivataliKapuConfig.GetFiokok();

            if (fiokok == null || fiokok.Count < 1)
            {
                response.CustomerPortals = new CustomerPortalResponseDto[0];
                response.TotalItemCount = 0;
                response.TotalItemCountSpecified = true;
                return response.SetSuccessResponse();
            }

            for (int i = 0; i < fiokok.Count; i++)
            {
                CustomerPortalResponseDto respDTO = new CustomerPortalResponseDto();
                cp.Add(respDTO);
                respDTO.Code = fiokok[i].Name;
                respDTO.Name = fiokok[i].Name;
            }

            response.CustomerPortals = cp.ToArray();
            response.TotalItemCount = cp.Count;
            response.TotalItemCountSpecified = true;

        }
        catch (Exception ex)
        {
            return response.SetErrorResponse("Hiba a hivatalikapu adatok lekérdezésekor: " + ex.Message);
        }

        return response.SetSuccessResponse();
    }


}