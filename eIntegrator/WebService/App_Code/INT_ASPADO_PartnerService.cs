﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using System.Linq;

using Contentum.eUtility;
using System.Web;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using Contentum.eAdmin.Service;
using System.Collections.Generic;
using Contentum.eIntegrator.Service.INT_ASPADO.Partner;
using Contentum.eIntegrator.Service.INT_ASPADO;

namespace INT_ASPADO
{
    public partial class INT_ASPADO_PartnerService : IINT_ASPADO_PartnerService
    {
        public SoapUnknownHeader[] headers;

        public PartnerResponse CreatePartner(PartnerRequest partnerRequest)
        {
            Logger.Debug("PartnerService.CreatePartner.Start");
            string userId = headers.ValidateUnknownSoapHeaders();
            PartnerResponse response = new PartnerResponse();

            if (partnerRequest == null)
            {
                return response.SetError(ASPADOHelper.Constants.Messages.SoapRequestError);
            }

            string localUser = userId.GetMappedUserId();

            if (string.IsNullOrEmpty(localUser))
            {
                return response.SetError(ASPADOHelper.Constants.Messages.UserError);
            }

            try
            {

                KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
                ExecParam execParam = ASPADOHelper.GetExecParam(localUser);

                KRT_Partnerek partner = partnerRequest.GetPartnerFromRequestForInsert();

                Result partnerResult = partnerService.Insert(execParam, partner);

                if (partnerResult.IsError)
                {
                    return response.SetError(partnerResult.ErrorMessage);
                }
                else if (String.IsNullOrEmpty(partnerResult.Uid))
                {
                    return response.SetError(ASPADOHelper.Constants.Messages.PartnerCreateFailedError);
                }
                else
                {
                    response.Partner = partnerRequest.Partner;
                    response.Partner.PartnerId = partnerService.GetASPADOId(execParam, partnerResult.Uid).Record.ToString();

                    partnerRequest.Partner.Addresses.InsertAddresses(response, partnerResult.Uid, localUser);

                    if (!String.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetError(response.ErrorMessage);
                    }

                    if (!String.IsNullOrEmpty(partnerRequest.Partner.Email))
                    {
                        partnerRequest.Partner.Email.InsertEmail(response, partnerResult.Uid, localUser);

                        if (!String.IsNullOrEmpty(response.ErrorMessage))
                        {
                            return response.SetError(response.ErrorMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("PartnerService.CreatePartner.Hiba", ex);
                return response.SetError(ex.Message);
            }
            return response.SetSuccess();
        }

        public BaseResponse DeletePartnerId(PartnerIdRequest partnerIdRequest)
        {
            Logger.Debug("PartnerService.DeletePartnerId.Start");
            string userId = headers.ValidateUnknownSoapHeaders();
            var response = new BaseResponse();

            if (partnerIdRequest == null)
            {
                return response.SetError(ASPADOHelper.Constants.Messages.SoapRequestError);
            }

            string localUser = userId.GetMappedUserId();

            if (string.IsNullOrEmpty(localUser))
            {
                return response.SetError(ASPADOHelper.Constants.Messages.UserError);
            }
            try
            {
                return ASPADOHelper.DeletePartnerId(partnerIdRequest, response, localUser);
            }
            catch (Exception ex)
            {
                Logger.Error("PartnerService.DeletePartnerId.Hiba", ex);
                return response.SetError(ex.Message);
            }
        }

        public PartnerResponse GetPartner(PartnerIdRequest partnerIdRequest)
        {
            Logger.Debug("PartnerService.GetPartner.Start");
            string userId = headers.ValidateUnknownSoapHeaders();
            PartnerResponse response = new PartnerResponse();

            if (partnerIdRequest == null)
            {
                return response.SetError(ASPADOHelper.Constants.Messages.SoapRequestError);
            }

            string localUser = userId.GetMappedUserId();

            if (string.IsNullOrEmpty(localUser))
            {
                return response.SetError(ASPADOHelper.Constants.Messages.UserError);
            }
            try
            {
                return ASPADOHelper.GetPartner(partnerIdRequest, response, localUser);
            }
            catch (Exception ex)
            {
                Logger.Error("PartnerService.GetPartner.Hiba", ex);
                return response.SetError(ex.Message);
            }
        }

        public PartnersResponse GetPartners(PartnerRequest partnerRequest, int pageNumber, bool pageNumberSpecified, int pageSize, bool pageSizeSpecified)
        {
            Logger.Debug("PartnerService.GetPartners.Start");
            string userId = headers.ValidateUnknownSoapHeaders();
            PartnersResponse response = new PartnersResponse();

            if (partnerRequest == null)
            {
                return response.SetError(ASPADOHelper.Constants.Messages.SoapRequestError);
            }

            string localUser = userId.GetMappedUserId();

            if (string.IsNullOrEmpty(localUser))
            {
                return response.SetError(ASPADOHelper.Constants.Messages.UserError);
            }
            try
            {
                return ASPADOHelper.GetPartners(partnerRequest, response, pageNumber, pageNumberSpecified, pageSize, pageSizeSpecified, localUser);
            }
            catch (Exception ex)
            {
                Logger.Error("PartnerService.GetPartners.Hiba", ex);
                return response.SetError(ex.Message);
            }
        }

        public PartnerResponse MergePartner(MergePartnerRequest mergePartnerRequest)
        {
            var response = new PartnerResponse();
            Logger.Debug("PartnerService.MergePartner.Start");
            string userId = headers.ValidateUnknownSoapHeaders();

            if (mergePartnerRequest == null)
            {
                return response.SetError(ASPADOHelper.Constants.Messages.SoapRequestError);
            }

            string localUser = userId.GetMappedUserId();

            if (string.IsNullOrEmpty(localUser))
            {
                return response.SetError(ASPADOHelper.Constants.Messages.UserError);
            }
            try
            {
                return ASPADOHelper.MergePartner(mergePartnerRequest, response, localUser);
            }
            catch (Exception ex)
            {
                Logger.Error("PartnerService.MergePartner.Hiba", ex);
                return response.SetError(ex.Message);
            }
        }

        public BaseResponse ModifyPartner(PartnerRequest partnerRequest)
        {
            Logger.Debug("PartnerService.ModifyPartner.Start");
            string userId = headers.ValidateUnknownSoapHeaders();
            BaseResponse response = new BaseResponse();

            if (partnerRequest == null)
            {
                return response.SetError(ASPADOHelper.Constants.Messages.SoapRequestError);
            }

            string localUser = userId.GetMappedUserId();

            if (string.IsNullOrEmpty(localUser))
            {
                return response.SetError(ASPADOHelper.Constants.Messages.UserError);
            }

            try
            {

                KRT_PartnerekService partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService();
                ExecParam execParam = ASPADOHelper.GetExecParam(localUser);

                if (String.IsNullOrEmpty(partnerRequest.Partner.PartnerId))
                {
                    return response.SetError(ASPADOHelper.Constants.Messages.PartnerIdEmptyError);
                }

                int id;

                if (!int.TryParse(partnerRequest.Partner.PartnerId, out id))
                {
                    return response.SetError(ASPADOHelper.Constants.Messages.PartnerIdInvalidError);
                }

                Result partnerIdGuidResult = partnerService.GetASPADOPartnerId(execParam, id);

                if (partnerIdGuidResult == null)
                {
                    return response.SetError("Sikertelen PartnerId konvertálás");
                }

                if (partnerIdGuidResult.IsError)
                {
                    return response.SetError(partnerIdGuidResult.ErrorMessage);
                }

                string partnerIdGuid = partnerIdGuidResult.Record.ToString();
                execParam.Record_Id = partnerIdGuid;

                Result krtPartnerResult = partnerService.Get(execParam);

                if (krtPartnerResult == null || krtPartnerResult.Record == null)
                {
                    return response.SetError("Nem található a partner");
                }

                if (krtPartnerResult.IsError)
                {
                    return response.SetError(krtPartnerResult.ErrorMessage);
                }

                KRT_Partnerek partner;
                KRT_Partnerek krt_Partnerek = (KRT_Partnerek)krtPartnerResult.Record;

                if (partnerRequest.Partner.PartnerTypeSpecified && partnerRequest.Partner.PartnerType.HasValue)
                {
                    string tipus;
                    if (partnerRequest.Partner.PartnerType == EnumsPartnerType.Company)
                    {
                        tipus = KodTarak.Partner_Tipus.Szervezet;
                    }
                    else
                    {
                        tipus = KodTarak.Partner_Tipus.Szemely;
                    }

                    if (!String.IsNullOrEmpty(tipus) && krt_Partnerek.Tipus != tipus)
                    {
                        partner = partnerRequest.GetPartnerFromRequestForUpdateWithInvalidate(krt_Partnerek, localUser, response);
                    }
                    else
                    {
                        partner = partnerRequest.GetPartnerFromRequestForUpdate(krt_Partnerek);
                    }
                }
                else
                {
                    partner = partnerRequest.GetPartnerFromRequestForUpdateWithInvalidate(krt_Partnerek, localUser, response);
                }

                if (!string.IsNullOrEmpty(response.ErrorMessage))
                {
                    return response.SetError(response.ErrorMessage);
                }

                Result partnerUpdateResult = partnerService.Update(execParam, partner);

                if (partnerUpdateResult.IsError)
                {
                    return response.SetError(partnerUpdateResult.ErrorMessage);
                }
                else
                {
                    KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();

                    KRT_Partnerek p = new KRT_Partnerek();
                    p.Typed.Id = new Guid(partnerIdGuid);
                    KRT_PartnerCimekSearch pcSearch = new KRT_PartnerCimekSearch();

                    pcSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                    pcSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
                    pcSearch.ErvKezd.Group = "100";
                    pcSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

                    pcSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                    pcSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
                    pcSearch.ErvVege.Group = "100";
                    pcSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

                    execParam.Clean();

                    Result partnerCimekResult = cimekService.GetAllByPartner(execParam, p, "", pcSearch);

                    if (partnerCimekResult == null || partnerCimekResult.Ds.Tables.Count < 1)
                    {
                        return response.SetError("Cimek fatal error.");
                    }

                    if (partnerCimekResult.IsError)
                    {
                        return response.SetError(partnerCimekResult.ErrorMessage);
                    }

                    List<Tuple<string, AddressDto>> existingAddresses = new List<Tuple<string, AddressDto>>();
                    List<Tuple<string, AddressDto>> newAddresses = new List<Tuple<string, AddressDto>>();
                    List<Tuple<string, AddressDto>> notAssociatedAddresses = new List<Tuple<string, AddressDto>>();
                    List<Tuple<string, AddressDto>> deleteAddresses = new List<Tuple<string, AddressDto>>();

                    var emails = partnerCimekResult.Ds.Tables[0].AsEnumerable().Where(x => (x["Tipus"] != null && x["Tipus"].ToString() == KodTarak.Cim_Tipus.Email)).ToList();

                    if (emails != null && emails.Count() > 0)
                    {
                        if (partnerRequest.Partner.Email != null)
                        {
                            partnerRequest.Partner.Email.UpdateEmail(response, emails.First()[21].ToString(), localUser);
                        }

                        while (emails.Count() > 0)
                        {
                            var first = emails[0];
                            partnerCimekResult.Ds.Tables[0].Rows.Remove(first);
                            emails.Remove(first);
                        }

                        if (!String.IsNullOrEmpty(response.ErrorMessage))
                        {
                            return response.SetError(response.ErrorMessage);
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(partnerRequest.Partner.Email))
                        {
                            partnerRequest.Partner.Email.InsertEmail(response, partnerIdGuid, localUser);

                            if (!String.IsNullOrEmpty(response.ErrorMessage))
                            {
                                return response.SetError(response.ErrorMessage);
                            }
                        }
                    }

                    partnerCimekResult.Ds.Tables[0].MarkAddresses(response, execParam, partnerRequest.Partner.Addresses, existingAddresses, newAddresses, notAssociatedAddresses, deleteAddresses);

                    if (!String.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetError(response.ErrorMessage);
                    }

                    newAddresses.Select(x => x.Item2).ToArray().InsertAddresses(response, partnerIdGuid, localUser);

                    if (!String.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetError(response.ErrorMessage);
                    }

                    deleteAddresses.Select(x => x.Item1).ToArray().DeleteAddresses(response, partnerIdGuid, localUser);

                    if (!String.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetError(response.ErrorMessage);
                    }

                    notAssociatedAddresses.Select(x => x.Item2).ToArray().AssociateAddresses(response, partnerIdGuid, localUser);

                    if (!String.IsNullOrEmpty(response.ErrorMessage))
                    {
                        return response.SetError(response.ErrorMessage);
                    }

                    foreach (var address in existingAddresses)
                    {
                        execParam.Clean();
                        execParam.Record_Id = address.Item1;

                        Result cimResult = cimekService.Get(execParam);

                        if (cimResult.IsError)
                        {
                            return response.SetError(cimResult.ErrorMessage);
                        }

                        KRT_Cimek cim = address.Item2.GetCimekFromRequestForUpdate((KRT_Cimek)cimResult.Record, localUser);

                        Result cimUpdateResult = cimekService.UpdateWithFKResolution(execParam, cim);

                        if (cimUpdateResult.IsError)
                        {
                            return response.SetError(cimUpdateResult.ErrorMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("PartnerService.ModifyPartner.Hiba", ex);
                return response.SetError(ex.Message);
            }
            return response.SetSuccess();
        }
    }
}