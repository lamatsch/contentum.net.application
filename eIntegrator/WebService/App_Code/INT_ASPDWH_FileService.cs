﻿using System;
using System.Web.Services;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using System.Reflection;
using System.Web.Script.Services;
using Contentum.eIntegrator.Service;
using System.Data;
using System.IO;
using System.Text;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;
using Contentum.eDocument.Service;
using Contentum.eAdmin.Service;
using System.Linq;
using Contentum.eQuery.BusinessDocuments;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Configuration;

[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public partial class INT_ASPDWH_FileService : System.Web.Services.WebService
{
    //#region Varible declaration

    private DataContext dataContext;
    private INT_ASPDWHStoredProcedure sp = null;

    private string logId;
    private DateTime todayDate;

    public const string ugyiratModulId = "2FA6B0F7-7C8F-40A9-ABFD-33F327E01F0E";
    public const string statisztikaModulId = "73F2AC45-7127-43F4-B48D-782D3C489F9D";

    public const string baseCheckSum = "d41d8cd98f00b204e9800998ecf8427e";

    public const string ugyiratParancs = "ugyirat";
    public const string ugyiratParancsOS = "ugyiratOS";
    public const string statisztikaParancs = "statisztika";
    public const string statisztikaParancsOS = "statisztikaOS";
    public const string sp_ugyirat_irat = "[sp_ASPDWH_ugyirat_irat]";
    public const string sp_ugyirat_adatlap = "[sp_ASPDWH_ugyirat_adatlap]";
    public const string sp_ugyirat_irattipus = "[sp_ASPDWH_ugyirat_irattipus]";
    public const string sp_ugyirat_ugy = "[sp_ASPDWH_ugyirat_ugy]";
    public const string sp_statisztika_statisztika = "[sp_ASPDWH_statisztika_statisztika]";


    public const string emailSubject = "ASP adattárház";
    public const string fileNameDate = "yyyyMMdd";
    public const string mainFolder = "ASDADATTARHAZ";

    public const string modulHiba = "Modul nem található. ";
    public const string utolsoFutasParseHiba = "Hiba az utolsó futás dátumának parsolásakor. ";

    public const string adatGyujtesHiba = "Hiba az adatok gyűjtése folyamán";
    public const string atadasStartHiba = "Hiba az átadás bejegyzésekor";
    public const string fajlIrasHiba = "Hiba a .dat fájlok írása során.";
    public const string naploIrasHiba = "Hiba a .log fájl írása során.";
    public const string tarGZIrasHiba = "Hiba a tar.gz fájl írása során";
    public const string sharepointFeltoltesHiba = "Hiba a tar.gz fájl dokumentumtárba való feltöltése során.";
    public const string chechSumCalculationHiba = "Hiba a checksum számítása során. ";
    public const string statuszHiba = "Hiba a státuszlekérdezéskor";
    public const string errorLogUploadHiba = "Hiba a GetErrorLogtól kapott fájl feltöltésekor";

    public const string syncHiba = "Hiba a synceléskor";

    ExecParam execParam;
    public const string AdminUser = "54e861a5-36ed-44ca-baa7-c287d125b309";
    public const string PIR_AZONOSITO = "PIR_AZONOSITO";
    public const string FEJLESZTO_AZONOSITO = "FEJLESZTO_AZONOSITO";

    public string pirAzonosito;
    public string fejlesztoAzonosito = "axis";
    string adatKorAzon;
    string tarFileName;
    string currentCheckSum = "-1";
    Contentum.eIntegrator.Service.gyakorisag _gyakorisag;

    DateTime calculatedStartDate;
    DateTime calculatedEndDate;
    DateTime naploStartDate;
    DateTime naploEndDate;

    private int statisztikaCount;
    private int ugyiratAdatLapCount;
    private int ugyiratIratTipusCount;
    private int ugyiratUgyCount;

    private Exception lastException;

    private Contentum.eIntegrator.Service.INT_ParameterekService _paramerekService;
    private Contentum.eIntegrator.Service.INT_LogService _logService;
    private Contentum.eIntegrator.Service.INT_ModulokService _modulokService;

    private EmailService _emailService;
    DWHFileService _outerService;

    INT_Log logRecord;
    DataRow lastLog;
    int ver = 0;
    string previousCheckSum;
    string folder;
    string uploadedTarFileId;

    public INT_ASPDWH_FileService()
    {
        dataContext = new DataContext(this.Application);
        sp = new INT_ASPDWHStoredProcedure(dataContext);
        execParam = GetExecParam();
        pirAzonosito = Rendszerparameterek.Get(execParam, PIR_AZONOSITO);


        ExecParam azonExecParam = GetExecParam();
        INT_ParameterekSearch search = new INT_ParameterekSearch();
        search.Nev.Value = FEJLESZTO_AZONOSITO;
        search.Nev.Operator = Contentum.eQuery.Query.Operators.equals;

        Result res = INTParameterekService.GetAll(azonExecParam, search);
        if (!res.IsError && res.Ds.Tables.Count > 0 && res.Ds.Tables[0].Rows.Count > 0)
        {
            fejlesztoAzonosito = GetRowValueAsString(res.Ds.Tables[0].Rows[0], "Ertek");
        }

    }

    #region Properties

    private EmailService Email
    {
        get
        {
            if (_emailService == null)
            {
                _emailService = eAdminService.ServiceFactory.GetEmailService();
            }
            return _emailService;
        }
    }

    private Contentum.eIntegrator.Service.INT_ParameterekService INTParameterekService
    {
        get
        {
            if (_paramerekService == null)
            {
                _paramerekService = eIntegratorService.ServiceFactory.GetINT_ParameterekService();
            }
            return _paramerekService;
        }
    }

    private Contentum.eIntegrator.Service.INT_LogService INTLogService
    {
        get
        {
            if (_logService == null)
            {
                _logService = eIntegratorService.ServiceFactory.GetINT_LogService();
            }
            return _logService;
        }
    }

    private Contentum.eIntegrator.Service.INT_ModulokService INTModulokService
    {
        get
        {
            if (_modulokService == null)
            {
                _modulokService = eIntegratorService.ServiceFactory.GetINT_ModulokService();
            }
            return _modulokService;
        }
    }

    private string _tenant;
    private string Tenant
    {
        get
        {
            if (string.IsNullOrEmpty(_tenant))
            {
                _tenant = ConfigurationManager.AppSettings.Get(ASP_DWH_KKSZB_Helper.Enum_ASP_DWH_APPSETTINGS.ASP_DWH_KKSZB_Tenant.ToString());
                if (string.IsNullOrEmpty(_tenant))
                {
                    Logger.Debug("Missing 'ASP_DWH_Tenant' app settings value !");
                }
            }
            return _tenant;
        }
    }

    private DWHFileService DWHService
    {
        get
        {
            if (_outerService == null)
            {
                _outerService = eIntegratorService.ServiceFactory.GetDWHFileService();
                DWHFileService.@string s = new DWHFileService.@string();
                s.Text = new string[] { Tenant };
                DWHService.Tenant = s;
            }
            return _outerService;
        }
    }

    #endregion

    #region WebMethods


    public Result GetData(ExecParam ExecParam, string spName, DateTime start, DateTime end)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetData(ExecParam, spName, start, end);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public string SyncUgyiratOsFeltoltes(DateTime? startDate, DateTime? endDate)
    {
        return SyncUgyirat(Contentum.eIntegrator.Service.gyakorisag.OS, startDate, endDate);
    }

    [WebMethod()]
    public string SyncUgyirat()
    {
        return SyncUgyirat(Contentum.eIntegrator.Service.gyakorisag.NAPI, null, null);
    }

    private string SyncUgyirat(Contentum.eIntegrator.Service.gyakorisag gyakorisag, DateTime? startDate, DateTime? endDate)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        string msg = null;

        try
        {
            adatKorAzon = "ugyirat";
            _gyakorisag = gyakorisag;
            todayDate = DateTime.Now;

            #region 1. Ugyirat modul leszedése, utolsofutas date parse, atadas bejegyzése az INTLog táblába, star/end date kiszámol

            INT_Modulok modul = GetModul(ugyiratModulId);

            if (modul == null)
            {
                msg = modulHiba + " Modul Id:" + ugyiratModulId;
                SendErrorMail(msg);
                return msg;
            }

            if (string.IsNullOrEmpty(modul.Base.Note) && gyakorisag != Contentum.eIntegrator.Service.gyakorisag.OS)
            {
                msg = "Az első feltöltés csak ősfeltöltés lehet. Modul Id:" + ugyiratModulId;
                SendErrorMail(msg);
                return msg;
            }

            DateTime utolsoFutas;

            if (!DateTime.TryParse(modul.UtolsoFutas, out utolsoFutas))
            {
                msg = utolsoFutasParseHiba + " Modul Id:" + modul.Id + " Dátum:" + modul.UtolsoFutas;
                SendErrorMail(msg);
                return msg;
            }

            if (utolsoFutas >= DateTime.Today.Date)
            {
                msg = "A modul már sikeresen lefutott a legutóbbi időszakra. " + " Modul Id:" + modul.Id + " Dátum:" + modul.UtolsoFutas;
                SendErrorMail(msg);
                return msg;
            }

            if (gyakorisag == Contentum.eIntegrator.Service.gyakorisag.NAPI)
            {
                calculatedStartDate = utolsoFutas.Date;
                calculatedEndDate = utolsoFutas.AddDays(1).Date;
                naploStartDate = calculatedStartDate;
                naploEndDate = calculatedStartDate;
            }
            else
            {
                if (startDate.HasValue && endDate.HasValue && endDate.Value > startDate.Value)
                {
                    calculatedStartDate = startDate.Value;
                    calculatedEndDate = endDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }
                else
                {
                    calculatedStartDate = utolsoFutas.Date;
                    calculatedEndDate = todayDate.Date.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                naploStartDate = calculatedStartDate.Date;
                naploEndDate = calculatedEndDate.Date;
            }

            if (gyakorisag == Contentum.eIntegrator.Service.gyakorisag.OS)
            {
                if (string.IsNullOrEmpty(modul.Base.Note))
                {
                    modul.Updated.SetValueAll(false);
                    modul.Base.Updated.SetValueAll(false);
                    modul.Base.Note = calculatedStartDate.ToShortDateString() + '|' + calculatedEndDate.ToShortDateString();
                    modul.Base.Updated.Note = true;
                    ExecParam modulExecParam = GetExecParam();
                    modulExecParam.Record_Id = modul.Id;
                    modul.Base.Updated.Ver = true;
                    Result modulUpdateResult = INTModulokService.Update(modulExecParam, modul);

                    if (modulUpdateResult.IsError)
                    {
                        SendErrorMail("Hiba az első ősfeltöltés metésekor: ", modulUpdateResult);
                    }
                }
                else
                {
                    string[] dates = modul.Base.Note.Split('|');
                    calculatedStartDate = DateTime.Parse(dates[0]).Date;
                    calculatedEndDate = DateTime.Parse(dates[1]).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    naploStartDate = calculatedStartDate.Date;
                    naploEndDate = calculatedEndDate.Date;
                }
            }

            Result atadasResult = StartAdatAtadas(execParam, ugyiratModulId, gyakorisag == Contentum.eIntegrator.Service.gyakorisag.NAPI ? ugyiratParancs : ugyiratParancsOS);

            if (atadasResult.IsError)
            {
                msg = atadasStartHiba;
                SendErrorMail(atadasStartHiba, atadasResult);
                return msg;
            }

            logId = atadasResult.Uid;

            logRecord = GetLog(logId);

            if (logRecord == null)
            {
                msg = "Log nem található. Log Id: " + logId;
                SendErrorMail(msg);
                return msg;
            }

            previousCheckSum = GetPreviousHash(ugyiratModulId);

            if (string.IsNullOrEmpty(previousCheckSum))
            {
                previousCheckSum = baseCheckSum;
            }

            #endregion

            #region 2. Adatok leszedése és leválogatása fájlokba, naplófájl összeállítása 

            Result ugyiratIratResult = GetData(execParam, sp_ugyirat_irat, calculatedStartDate, calculatedEndDate);
            Result ugyiratAdatlapResult = GetData(execParam, sp_ugyirat_adatlap, calculatedStartDate, calculatedEndDate);
            Result ugyiratIratTipusResult = GetData(execParam, sp_ugyirat_irattipus, calculatedStartDate, calculatedEndDate);
            Result ugyiratUgyResult = GetData(execParam, sp_ugyirat_ugy, calculatedStartDate, calculatedEndDate);

            if (ugyiratIratResult.IsError || ugyiratIratTipusResult.IsError || ugyiratAdatlapResult.IsError || ugyiratUgyResult.IsError)
            {
                msg = "Hiba az adatgyüjtés alatt";
                SendErrorMail(adatGyujtesHiba, ugyiratIratResult, ugyiratIratTipusResult, ugyiratAdatlapResult, ugyiratUgyResult);
                SetSyncHiba("Hiba az adatgyüjtés alatt", ugyiratIratResult, ugyiratIratTipusResult, ugyiratAdatlapResult, ugyiratUgyResult);
                return msg;
            }
            else
            {
                statisztikaCount = ugyiratIratResult.Ds.Tables[0].Rows.Count;
                ugyiratAdatLapCount = ugyiratAdatlapResult.Ds.Tables[0].Rows.Count;
                ugyiratIratTipusCount = ugyiratIratTipusResult.Ds.Tables[0].Rows.Count;
                ugyiratUgyCount = ugyiratUgyResult.Ds.Tables[0].Rows.Count;

                string ugyiratIratFileName = "irat_ugyirat_" + pirAzonosito + "_irat_" + fejlesztoAzonosito + "_" + naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate) + ".dat";
                string ugyiratAdatlapFileName = "irat_ugyirat_" + pirAzonosito + "_adatlap_" + fejlesztoAzonosito + "_" + naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate) + ".dat";
                string ugyiratIratTipusFileName = "irat_ugyirat_" + pirAzonosito + "_irattipus_" + fejlesztoAzonosito + "_" + naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate) + ".dat";
                string ugyiratUgyFileName = "irat_ugyirat_" + pirAzonosito + "_ugy_" + fejlesztoAzonosito + "_" + naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate) + ".dat";

                tarFileName = "irat_ugyirat_" + pirAzonosito + "_" + fejlesztoAzonosito + "_" + naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate) + (ver == 0 ? "" : "_" + ver.ToString()) + ".tar.gz";

                Dictionary<string, int> files = new Dictionary<string, int>();
                files.Add(ugyiratIratFileName, statisztikaCount);
                files.Add(ugyiratAdatlapFileName, ugyiratAdatLapCount);
                files.Add(ugyiratIratTipusFileName, ugyiratIratTipusCount);
                files.Add(ugyiratUgyFileName, ugyiratUgyCount);

                if (files.Values.All(x => x == 0))
                {
                    TryEmptyUpload(previousCheckSum, ugyiratModulId);
                    return msg;
                }

                string naploFileName = "irat_ugyirat_" + pirAzonosito + "_naplo_" + fejlesztoAzonosito + "_" + naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate) + ".log";

                string uniqueFolderPart = naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate);

                Logger.Debug(GetInputDir(mainFolder, uniqueFolderPart));
                Directory.CreateDirectory(GetInputDir(mainFolder, uniqueFolderPart));
                folder = GetInputDir(mainFolder, uniqueFolderPart);

                if (!WriteFile(folder, ugyiratIratFileName, ugyiratIratResult) || !WriteFile(folder, ugyiratAdatlapFileName, ugyiratAdatlapResult) || !WriteFile(folder, ugyiratIratTipusFileName, ugyiratIratTipusResult)
                    || !WriteFile(folder, ugyiratUgyFileName, ugyiratUgyResult))
                {
                    msg = fajlIrasHiba + Environment.NewLine + lastException.Message;
                    SendErrorMail(fajlIrasHiba, lastException);
                    SetSyncHiba(msg);
                    return msg;
                }

                if (!WriteNaploFile(folder, naploFileName, files, naploStartDate, naploEndDate))
                {
                    msg = naploIrasHiba + Environment.NewLine + lastException.Message;
                    SendErrorMail(naploIrasHiba, lastException);
                    SetSyncHiba(msg);
                    return msg;
                }

                files.Add(naploFileName, 0);

                #endregion

                #region 3. TarGz file összeállítása, CheckSum számolás, feltöltés doktárba



                if (!CreateTarGZ(folder, files.Keys.ToList(), tarFileName))
                {
                    msg = tarGZIrasHiba + Environment.NewLine + lastException.Message;
                    SendErrorMail(tarGZIrasHiba, lastException);
                    SetSyncHiba(msg);
                    return msg;
                }

                FileStream s = File.OpenRead(Path.Combine(folder, tarFileName));
                Result uploadResult = FileUpload(execParam, s, tarFileName, false, "");
                s.Close();

                if (uploadResult.IsError)
                {
                    msg = sharepointFeltoltesHiba + Environment.NewLine + lastException.Message;
                    SendErrorMail(sharepointFeltoltesHiba, uploadResult);
                    SetSyncHiba(msg);
                    return msg;
                }

                uploadedTarFileId = uploadResult.Uid;

                currentCheckSum = CalculateCheckSum(folder, tarFileName);

                if (currentCheckSum == "-1")
                {
                    msg = chechSumCalculationHiba + Environment.NewLine + lastException.Message;
                    SendErrorMail(chechSumCalculationHiba, lastException);
                    SetSyncHiba(msg);
                    return msg;
                }

                #endregion

                #region INTLog tábla update, TarGz elküldése

                TryUpload(ugyiratModulId);

                #endregion
            }

        }
        catch (Exception ex)
        {
            msg = "Fatal hiba: " + Environment.NewLine + ex.Message;
            SendErrorMail("Fatal hiba: ", ex);
            SetSyncHiba(msg);
        }

        log.WsEnd(execParam);

        return msg;
    }


    private void TryUpload(string modulId)
    {
        logRecord = GetLog(logId);
        logRecord.Updated.SetValueAll(false);

        bool success = true;

        Contentum.eIntegrator.Service.UploadDocumentData data = new Contentum.eIntegrator.Service.UploadDocumentData();
        try
        {
            FileStream tarFile = File.OpenRead(Path.Combine(folder, tarFileName));

            data.fajlContent = new byte[tarFile.Length];
            tarFile.Read(data.fajlContent, 0, (int)tarFile.Length);
            tarFile.Close();
        }
        catch (Exception ex)
        {
            SendErrorMail("Fájl olvasás hiba", ex);
            SetSyncHiba(ex.Message);
            return;
        }

        Contentum.eIntegrator.Service.AtveteliElismerveny elismerveny;
        string elismervenyId = "-1";
        string hiba = "";
        try
        {
            DWHService.UploadDocumentMetaDataValue = GetMetadataObject(false);
            Logger.Error(JsonConvert.SerializeObject(DWHService.UploadDocumentMetaDataValue));
            elismerveny = DWHService.UploadDocument(data);
            elismervenyId = elismerveny.documentId;
        }
        catch (Exception ex)
        {
            success = false;
            hiba = "Hiba az DWHService.UploadDocument hívása során. Message: " + ex.Message + Environment.NewLine + ex.StackTrace;
        }

        logRecord.Updated.SetValueAll(false);
        logRecord.Base.Updated.Ver = true;

        //jelenleg nem használjuk, de lehet majd kelleni fog
        //int varhatoAllapotValtozasSec = elismerveny.varhatoAllapotValtozasSec;

        if (success)
        {
            logRecord.Dokumentumok_Id_Sent = uploadedTarFileId;
            logRecord.Updated.Dokumentumok_Id_Sent = true;
            logRecord.ExternalId = elismervenyId;
            logRecord.Updated.ExternalId = true;
            //logRecord.Sync_EndDate = DateTime.Now.ToString();
            //logRecord.Updated.Sync_EndDate = true;
            logRecord.PackageVer = ver.ToString();
            logRecord.Updated.PackageVer = true;
            logRecord.PackageHash = currentCheckSum;
            logRecord.Updated.PackageHash = true;
        }
        else
        {
            logRecord.Dokumentumok_Id_Sent = uploadedTarFileId;
            logRecord.Updated.Dokumentumok_Id_Sent = true;
            logRecord.Status = "9";
            logRecord.Updated.Status = true;
            logRecord.HibaUzenet = hiba;
            logRecord.Updated.HibaUzenet = true;
            logRecord.PackageVer = ver.ToString();
            logRecord.Updated.PackageVer = true;
            //logRecord.Sync_EndDate = DateTime.Now.ToString();
            //logRecord.Updated.Sync_EndDate = true;
        }

        ExecParam logUpdateExecparam = GetExecParam();
        logUpdateExecparam.Record_Id = logId;
        Result updateResult = INTLogService.Update(logUpdateExecparam, logRecord);

        if (updateResult.IsError)
        {
            SendErrorMail("Hiba a log mentésekor: ", updateResult);
        }
    }

    private void TryEmptyUpload(string lastSuccesfulHash, string modulId)
    {
        logRecord = GetLog(logId);
        logRecord.Updated.SetValueAll(false);

        bool success = true;
        Contentum.eIntegrator.Service.AtveteliElismerveny elismerveny;
        string elismervenyId = "-1";
        string hiba = "";
        try
        {
            Contentum.eIntegrator.Service.UploadDocumentData data = new Contentum.eIntegrator.Service.UploadDocumentData();
            DWHService.UploadDocumentMetaDataValue = GetMetadataObject(true);
            Logger.Error(JsonConvert.SerializeObject(DWHService.UploadDocumentMetaDataValue));
            elismerveny = DWHService.UploadDocument(data);
            elismervenyId = elismerveny.documentId;
        }
        catch (Exception ex)
        {
            success = false;
            hiba = "Hiba az DWHService.UploadDocument hívása során. Message: " + ex.Message + Environment.NewLine + ex.StackTrace;
        }

        if (success)
        {
            logRecord.ExternalId = elismervenyId;
            logRecord.Updated.ExternalId = true;
            //logRecord.Sync_EndDate = DateTime.Now.ToString();
            //logRecord.Updated.Sync_EndDate = true;
            logRecord.PackageVer = ver.ToString();
            logRecord.Updated.PackageVer = true;
            logRecord.PackageHash = lastSuccesfulHash;
            logRecord.Updated.PackageHash = true;
        }
        else
        {
            logRecord.Status = "9";
            logRecord.Updated.Status = true;
            logRecord.HibaUzenet = hiba;
            logRecord.Updated.HibaUzenet = true;
            logRecord.PackageVer = ver.ToString();
            logRecord.Updated.PackageVer = true;
            //logRecord.Sync_EndDate = DateTime.Now.ToString();
            //logRecord.Updated.Sync_EndDate = true;
        }

        logRecord.Base.Updated.Ver = true;

        ExecParam logUpdateExecparam = GetExecParam();
        logUpdateExecparam.Record_Id = logId;
        Result updateResult = INTLogService.Update(logUpdateExecparam, logRecord);

        if (updateResult.IsError)
        {
            SendErrorMail("Hiba a log mentésekor: ", updateResult);
        }
    }

    private string SyncStatisztika(Contentum.eIntegrator.Service.gyakorisag gyakorisag, DateTime? startDate, DateTime? endDate)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        string msg = null;

        try
        {
            adatKorAzon = "statisztika";
            _gyakorisag = gyakorisag;
            todayDate = DateTime.Now;

            INT_Modulok modul = GetModul(statisztikaModulId);

            if (modul == null)
            {
                msg = modulHiba + " Modul Id:" + statisztikaModulId;
                SendErrorMail(msg);
                return msg;
            }

            if (string.IsNullOrEmpty(modul.Base.Note) && gyakorisag != Contentum.eIntegrator.Service.gyakorisag.OS)
            {
                msg = "Az első feltöltés csak ősfeltöltés lehet. Modul Id:" + ugyiratModulId;
                SendErrorMail(msg);
                return msg;
            }

            DateTime utolsoFutas;

            if (!DateTime.TryParse(modul.UtolsoFutas, out utolsoFutas))
            {
                msg = utolsoFutasParseHiba + " Modul Id:" + modul.Id + " Dátum:" + modul.UtolsoFutas;
                SendErrorMail(msg);
                return msg;
            }

            DateTime today = DateTime.Today;

            if (utolsoFutas >= new DateTime(today.Year, today.Month, 1))
            {
                msg = "A modul már sikeresen lefutott a legutóbbi időszakra. " + " Modul Id:" + modul.Id + " Dátum:" + modul.UtolsoFutas;
                SendErrorMail(msg);
                return msg;
            }

            if (gyakorisag == Contentum.eIntegrator.Service.gyakorisag.HAVI)
            {
                //var date = DateTime.Today.AddMonths(-1).Date;
                var date = utolsoFutas.Date;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                calculatedStartDate = new DateTime(utolsoFutas.Year, 1, 1);
                calculatedEndDate = lastDayOfMonth;
                naploStartDate = calculatedStartDate.Date;
                naploEndDate = calculatedEndDate.Date;
            }
            else
            {
                if (startDate.HasValue && endDate.HasValue && endDate.Value > startDate.Value)
                {
                    calculatedStartDate = startDate.Value;
                    calculatedEndDate = endDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }
                else
                {
                    //var date = DateTime.Today.AddMonths(-1).Date;
                    //var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                    //var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    calculatedStartDate = new DateTime(utolsoFutas.Year, 1, 1);
                    //calculatedEndDate = lastDayOfMonth;
                    calculatedEndDate = new DateTime(utolsoFutas.Year, 12, 31, 23, 59, 59);
                }

                naploStartDate = calculatedStartDate.Date;
                naploEndDate = calculatedEndDate.Date;
            }

            if (gyakorisag == Contentum.eIntegrator.Service.gyakorisag.OS)
            {
                if (string.IsNullOrEmpty(modul.Base.Note))
                {
                    modul.Updated.SetValueAll(false);
                    modul.Base.Updated.SetValueAll(false);
                    modul.Base.Note = calculatedStartDate.ToShortDateString() + '|' + calculatedEndDate.ToShortDateString();
                    modul.Base.Updated.Note = true;
                    ExecParam modulExecParam = GetExecParam();
                    modulExecParam.Record_Id = modul.Id;
                    modul.Base.Updated.Ver = true;
                    Result modulUpdateResult = INTModulokService.Update(modulExecParam, modul);

                    if (modulUpdateResult.IsError)
                    {
                        SendErrorMail("Hiba az első ősfeltöltés metésekor: ", modulUpdateResult);
                    }
                }
                else
                {
                    string[] dates = modul.Base.Note.Split('|');
                    calculatedStartDate = DateTime.Parse(dates[0]).Date;
                    calculatedEndDate = DateTime.Parse(dates[1]).Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    naploStartDate = calculatedStartDate.Date;
                    naploEndDate = calculatedEndDate.Date;
                }
            }

            Result atadasResult = StartAdatAtadas(execParam, statisztikaModulId, gyakorisag == Contentum.eIntegrator.Service.gyakorisag.HAVI ? statisztikaParancs : statisztikaParancsOS);

            if (atadasResult.IsError)
            {
                msg = atadasStartHiba;
                SendErrorMail(atadasStartHiba, atadasResult);
                return msg;
            }

            logId = atadasResult.Uid;

            logRecord = GetLog(logId);

            if (logRecord == null)
            {
                msg = "Log nem található. Log Id: " + logId;
                SendErrorMail(msg);
                return msg;
            }

            previousCheckSum = GetPreviousHash(statisztikaModulId);

            if (string.IsNullOrEmpty(previousCheckSum))
            {
                previousCheckSum = baseCheckSum;
            }

            Result statisztikaResult = GetData(execParam, sp_statisztika_statisztika, calculatedStartDate, calculatedEndDate);

            if (statisztikaResult.IsError)
            {
                msg = "Hiba az adatgyüjtés alatt";
                SendErrorMail(adatGyujtesHiba, statisztikaResult);
                SetSyncHiba(msg, statisztikaResult);
                return msg;
            }
            else
            {
                statisztikaCount = statisztikaResult.Ds.Tables[0].Rows.Count;

                string statisztikaFileName = "irat_statisztika_" + pirAzonosito + "_statisztika_" + fejlesztoAzonosito + "_" + naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate) + ".dat";
                tarFileName = "irat_statisztika_" + pirAzonosito + "_" + fejlesztoAzonosito + "_" + naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate) + (ver == 0 ? "" : "_" + ver.ToString()) + ".tar.gz";

                Dictionary<string, int> files = new Dictionary<string, int>();
                files.Add(statisztikaFileName, statisztikaCount);

                if (files.Values.All(x => x == 0))
                {
                    TryEmptyUpload(previousCheckSum, statisztikaModulId);
                    return msg;
                }

                string naploFileName = "irat_statisztika_" + pirAzonosito + "_naplo_" + fejlesztoAzonosito + "_" + naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate) + ".log";

                string uniqueFolderPart = naploStartDate.ToString(fileNameDate) + "_" + naploEndDate.ToString(fileNameDate);
                Directory.CreateDirectory(GetInputDir(mainFolder, uniqueFolderPart));
                folder = GetInputDir(mainFolder, uniqueFolderPart);

                if (!WriteFile(folder, statisztikaFileName, statisztikaResult))
                {
                    msg = fajlIrasHiba + Environment.NewLine + lastException.Message;
                    SendErrorMail(fajlIrasHiba, lastException);
                    SetSyncHiba(msg);
                    return msg;
                }

                if (!WriteNaploFile(folder, naploFileName, files, naploStartDate, naploEndDate))
                {
                    msg = naploIrasHiba + Environment.NewLine + lastException.Message;
                    SendErrorMail(naploIrasHiba, lastException);
                    SetSyncHiba(msg);
                    return msg;
                }

                files.Add(naploFileName, 0);

                if (!CreateTarGZ(folder, files.Keys.ToList(), tarFileName))
                {
                    msg = tarGZIrasHiba + Environment.NewLine + lastException.Message;
                    SendErrorMail(tarGZIrasHiba, lastException);
                    SetSyncHiba(msg);
                    return msg;
                }

                FileStream s = File.OpenRead(Path.Combine(folder, tarFileName));
                Result uploadResult = FileUpload(execParam, s, tarFileName, false, "");
                s.Close();

                if (uploadResult.IsError)
                {
                    msg = sharepointFeltoltesHiba + Environment.NewLine + lastException.Message;
                    SendErrorMail(sharepointFeltoltesHiba, uploadResult);
                    SetSyncHiba(msg);
                    return msg;
                }

                uploadedTarFileId = uploadResult.Uid;

                currentCheckSum = CalculateCheckSum(folder, tarFileName);

                if (currentCheckSum == "-1")
                {
                    msg = chechSumCalculationHiba + Environment.NewLine + lastException.Message;
                    SendErrorMail(chechSumCalculationHiba, lastException);
                    SetSyncHiba(msg);
                    return msg;
                }

                TryUpload(statisztikaModulId);

            }
        }
        catch (Exception ex)
        {
            msg = "Fatal hiba: " + Environment.NewLine + ex.Message;
            SendErrorMail("Fwatal hiba: ", ex);
            SetSyncHiba(msg);
        }

        log.WsEnd(execParam);

        return msg;
    }

    //[WebMethod()]
    //public void SyncStatisztikaOsFeltoltes()
    //{
    //    SyncStatisztika(Contentum.eIntegrator.Service.gyakorisag.OS);
    //}

    [WebMethod()]
    public string SyncStatisztika()
    {
        return SyncStatisztika(Contentum.eIntegrator.Service.gyakorisag.HAVI, null, null);
    }

    [WebMethod()]
    public string SyncStatisztikaOSFeltoltes(DateTime? startDate, DateTime? endDate)
    {
        return SyncStatisztika(Contentum.eIntegrator.Service.gyakorisag.OS, startDate, endDate);
    }

    [WebMethod()]
    public void StatuszLekerdezesUgyirat()
    {
        ExecParam logExecParam = GetExecParam();
        INT_LogSearch logSearch = new INT_LogSearch();
        logSearch.Status.Value = "0";
        logSearch.Status.Operator = Contentum.eQuery.Query.Operators.equals;
        logSearch.ExternalId.Value = string.Empty;
        logSearch.ExternalId.Operator = Contentum.eQuery.Query.Operators.notnull;
        logSearch.Modul_id.Value = ugyiratModulId;
        logSearch.Modul_id.Operator = Contentum.eQuery.Query.Operators.equals;

        Result logAllResult = INTLogService.GetAll(execParam, logSearch);

        if (logAllResult.IsError)
        {
            SendErrorMail(statuszHiba, logAllResult);
            return;
        }

        if (logAllResult.Ds.Tables[0].Rows.Count > 0)
        {
            var rows = logAllResult.Ds.Tables[0].Rows;
            for (int i = 0; i < rows.Count; i++)
            {
                Contentum.eIntegrator.Service.GetDocumentStatusResponse response;
                string docId;
                try
                {
                    Contentum.eIntegrator.Service.GetDocumentStatusPart statusPart = new Contentum.eIntegrator.Service.GetDocumentStatusPart();
                    docId = GetRowValueAsString(rows[i], "ExternalId");
                    statusPart.documentId = docId;
                    response = DWHService.GetDocumentStatus(statusPart);
                }
                catch (Exception ex)
                {
                    SendErrorMail(statuszHiba + Environment.NewLine + ex.Message);
                    continue;
                }

                logExecParam.Record_Id = GetRowValueAsString(rows[i], "Id");
                Result logResult = INTLogService.Get(logExecParam);

                if (logResult.IsError)
                {
                    SendErrorMail(statuszHiba, logAllResult);
                    continue;
                }

                INT_Log log = (INT_Log)logResult.Record;

                log.Updated.SetValueAll(false);
                log.Base.Updated.Ver = true;

                Logger.Error("response.status:" + response.status + "|");

                if (response.status == null || response.status.StartsWith("50"))
                {
                    log.Status = "1";
                    log.Updated.Status = true;
                }
                else if (response.status.StartsWith("0"))
                {
                    log.Updated.Status = false;
                }
                else
                {
                    log.Status = "9";
                    log.Updated.Status = true;
                }

                try
                {
                    Contentum.eIntegrator.Service.GetDocumentErrorLogPart errorLogPart = new Contentum.eIntegrator.Service.GetDocumentErrorLogPart();
                    errorLogPart.documentId = docId;
                    Contentum.eIntegrator.Service.GetDocumentErrorLogResponse errorResponse = DWHService.GetDocumentErrorLog(errorLogPart);

                    Result uploadResult = FileUpload(execParam, new MemoryStream(errorResponse.fileContent), errorResponse.fileName, false, "");

                    if (uploadResult.IsError)
                    {
                        SendErrorMail(errorLogUploadHiba, uploadResult);
                    }
                    else
                    {
                        log.Dokumentumok_Id_Error = uploadResult.Uid;
                        log.Updated.Dokumentumok_Id_Error = true;
                    }
                }
                catch (Exception ex)
                {
                    SendErrorMail("GetDocumentErrorLog hiba" + Environment.NewLine + ex.Message);
                }

                INTLogService.Update(logExecParam, log);

                if (response.status == null || response.status.StartsWith("50"))
                {
                    INT_Modulok modul = GetModul(log.Modul_id);

                    DateTime utolsoFutas;

                    if (!DateTime.TryParse(modul.UtolsoFutas, out utolsoFutas))
                    {
                        SendErrorMail(utolsoFutasParseHiba + " Modul Id:" + modul.Id + " Dátum:" + modul.UtolsoFutas);
                    }

                    if (log.Parancs == ugyiratParancsOS)
                    {
                        string[] dates = modul.Base.Note.Split('|');
                        calculatedEndDate = DateTime.Parse(dates[1]).Date;
                        utolsoFutas = calculatedEndDate;
                    }

                    if (log.Modul_id.ToLower() == ugyiratModulId.ToLower())
                    {
                        utolsoFutas = utolsoFutas.AddDays(1);
                    }

                    modul.Updated.SetValueAll(false);
                    modul.UtolsoFutas = utolsoFutas.ToString();
                    modul.Updated.UtolsoFutas = true;
                    ExecParam modulExecParam = GetExecParam();
                    modulExecParam.Record_Id = modul.Id;

                    //modul.Base.Ver = (int.Parse(modul.Base.Ver) + 1).ToString();
                    modul.Base.Updated.Ver = true;
                    Result modulUpdateResult = INTModulokService.Update(modulExecParam, modul);

                    if (modulUpdateResult.IsError)
                    {
                        SendErrorMail("Hiba a modul mentésekor: ", modulUpdateResult);
                    }
                }

            }
        }
        else
        {
            Logger.Error("Nem talalt");
        }

    }

    [WebMethod()]
    public void StatuszLekerdezesStatisztika()
    {
        ExecParam logExecParam = GetExecParam();
        INT_LogSearch logSearch = new INT_LogSearch();
        logSearch.Status.Value = "0";
        logSearch.Status.Operator = Contentum.eQuery.Query.Operators.equals;
        logSearch.ExternalId.Value = string.Empty;
        logSearch.ExternalId.Operator = Contentum.eQuery.Query.Operators.notnull;
        logSearch.Modul_id.Value = statisztikaModulId;
        logSearch.Modul_id.Operator = Contentum.eQuery.Query.Operators.equals;

        Result logAllResult = INTLogService.GetAll(execParam, logSearch);

        if (logAllResult.IsError)
        {
            SendErrorMail(statuszHiba, logAllResult);
            return;
        }

        if (logAllResult.Ds.Tables[0].Rows.Count > 0)
        {
            var rows = logAllResult.Ds.Tables[0].Rows;
            for (int i = 0; i < rows.Count; i++)
            {
                Contentum.eIntegrator.Service.GetDocumentStatusResponse response;
                string docId;
                try
                {
                    Contentum.eIntegrator.Service.GetDocumentStatusPart statusPart = new Contentum.eIntegrator.Service.GetDocumentStatusPart();
                    docId = GetRowValueAsString(rows[i], "ExternalId");
                    statusPart.documentId = docId;
                    response = DWHService.GetDocumentStatus(statusPart);
                }
                catch (Exception ex)
                {
                    SendErrorMail(statuszHiba + Environment.NewLine + ex.Message);
                    continue;
                }

                logExecParam.Record_Id = GetRowValueAsString(rows[i], "Id");
                Result logResult = INTLogService.Get(logExecParam);

                if (logResult.IsError)
                {
                    SendErrorMail(statuszHiba, logAllResult);
                    continue;
                }

                INT_Log log = (INT_Log)logResult.Record;

                log.Updated.SetValueAll(false);
                log.Base.Updated.Ver = true;

                Logger.Error("response.status:" + response.status + "|");

                if (response.status == null || response.status.StartsWith("50"))
                {
                    log.Status = "1";
                    log.Updated.Status = true;
                }
                else if (response.status.StartsWith("0"))
                {
                    log.Updated.Status = false;
                }
                else
                {
                    log.Status = "9";
                    log.Updated.Status = true;
                }

                try
                {
                    Contentum.eIntegrator.Service.GetDocumentErrorLogPart errorLogPart = new Contentum.eIntegrator.Service.GetDocumentErrorLogPart();
                    errorLogPart.documentId = docId;
                    Contentum.eIntegrator.Service.GetDocumentErrorLogResponse errorResponse = DWHService.GetDocumentErrorLog(errorLogPart);

                    Result uploadResult = FileUpload(execParam, new MemoryStream(errorResponse.fileContent), errorResponse.fileName, false, "");

                    if (uploadResult.IsError)
                    {
                        SendErrorMail(errorLogUploadHiba, uploadResult);
                    }
                    else
                    {
                        log.Dokumentumok_Id_Error = uploadResult.Uid;
                        log.Updated.Dokumentumok_Id_Error = true;
                    }
                }
                catch (Exception ex)
                {
                    SendErrorMail("GetDocumentErrorLog hiba" + Environment.NewLine + ex.Message);
                }

                INTLogService.Update(logExecParam, log);

                if (response.status == null || response.status.StartsWith("50"))
                {
                    INT_Modulok modul = GetModul(log.Modul_id);

                    DateTime utolsoFutas;

                    if (!DateTime.TryParse(modul.UtolsoFutas, out utolsoFutas))
                    {
                        SendErrorMail(utolsoFutasParseHiba + " Modul Id:" + modul.Id + " Dátum:" + modul.UtolsoFutas);
                    }

                    if (log.Parancs == statisztikaParancsOS)
                    {
                        string[] dates = modul.Base.Note.Split('|');
                        calculatedEndDate = DateTime.Parse(dates[1]).Date;
                        utolsoFutas = calculatedEndDate;
                    }

                    if (log.Modul_id.ToLower() == statisztikaModulId.ToLower())
                    {
                        utolsoFutas = utolsoFutas.AddMonths(1);
                    }

                    modul.Updated.SetValueAll(false);
                    modul.UtolsoFutas = utolsoFutas.ToString();
                    modul.Updated.UtolsoFutas = true;
                    ExecParam modulExecParam = GetExecParam();
                    modulExecParam.Record_Id = modul.Id;

                    //modul.Base.Ver = (int.Parse(modul.Base.Ver) + 1).ToString();
                    modul.Base.Updated.Ver = true;
                    Result modulUpdateResult = INTModulokService.Update(modulExecParam, modul);

                    if (modulUpdateResult.IsError)
                    {
                        SendErrorMail("Hiba a modul mentésekor: ", modulUpdateResult);
                    }
                }

            }
        }
        else
        {
            Logger.Error("Nem talalt");
        }

    }

    #endregion

    #region Helpers

    private void SetSyncHiba(string error, params Result[] result)
    {
        ExecParam logUpdateExecparam = GetExecParam();
        logUpdateExecparam.Record_Id = logId;

        string body;

        if (result != null && result.Any(x => x.IsError))
        {
            body = error + result.Select(x => Environment.NewLine + "ErrorMessage: " + x.ErrorMessage + Environment.NewLine + "ErrorCode: " + x.ErrorCode).Aggregate((x, y) => x + y);
        }
        else
        {
            body = error;
        }

        INT_Log logHibas = GetLog(logId);

        if (logHibas != null)
        {
            logHibas.Updated.SetValueAll(false);

            logHibas.Status = "9";
            logHibas.Updated.Status = true;
            logHibas.HibaUzenet = body;
            logHibas.Updated.HibaUzenet = true;
            //logHibas.Sync_EndDate = DateTime.Now.ToString();
            //logHibas.Updated.Sync_EndDate = true;

            //logRecord.Base.Ver = (int.Parse(logRecord.Base.Ver) + 1).ToString();
            logHibas.Base.Updated.Ver = true;

            Result logUpdateResult = INTLogService.Update(logUpdateExecparam, logHibas);

            if (logUpdateResult.IsError)
            {
                SendErrorMail("Hiba a log írásakor", logUpdateResult);
            }
        }
    }


    public static string GetRowValueAsString(DataRow row, string columnName)
    {
        return !IsDBNull(row[columnName]) ? row[columnName] is DateTime ? ((DateTime)row[columnName]).ToString("yyyy-MM-dd") : row[columnName].ToString() : null;
    }

    public static string GetRowValueAsString(DataRow row, int index)
    {
        return !IsDBNull(row[index]) ? row[index] is DateTime ? ((DateTime)row[index]).ToString("yyyy-MM-dd") : row[index].ToString() : null;
        //   return !IsDBNull(row[index]) ? row[index].ToString() : null;
    }

    public static bool IsDBNull(object obj)
    {
        return obj == System.DBNull.Value;
    }

    [WebMethod()]
    public string HelloWorld()
    {
        return "Hello World";
    }

    private void SendErrorMail(string error, params Result[] result)
    {
        String[] MessageToAddress = { Rendszerparameterek.Get(execParam, Rendszerparameterek.HIBABEJELENTES_EMAIL_CIM) };
        string body;

        if (result != null && result.Any(x => x.IsError))
        {
            body = error + result.Select(x => Environment.NewLine + "ErrorMessage: " + x.ErrorMessage + Environment.NewLine + "ErrorCode: " + x.ErrorCode).Aggregate((x, y) => x + y);
        }
        else
        {
            body = error;
        }

        Logger.Error(body);

        Email.SendEmail(execParam,
       Rendszerparameterek.Get(execParam, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM),
       MessageToAddress,
       emailSubject,
       null,
       null,
       true,
       body);
    }

    private void SendErrorMail(string error, Exception ex)
    {
        String[] MessageToAddress = { Rendszerparameterek.Get(execParam, Rendszerparameterek.HIBABEJELENTES_EMAIL_CIM) };
        string body;

        if (ex != null)
        {
            body = error + Environment.NewLine + "ErrorMessage: " + ex.Message + Environment.NewLine + "StackTrace: " + ex.StackTrace;
        }
        else
        {
            body = error;
        }

        Logger.Error(body);

        Email.SendEmail(execParam,
       Rendszerparameterek.Get(execParam, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM),
       MessageToAddress,
       emailSubject,
       null,
       null,
       true,
       body);
    }

    static string GetTempPath()
    {
        return Path.GetTempPath();
    }

    static string GetTempDir(string erkeztetoSzam)
    {
        return Path.Combine(GetTempPath(), erkeztetoSzam);
    }

    static string GetInputDir(string asp, string uniquePart)
    {
        return Path.Combine(GetTempDir(asp), uniquePart);
    }

    private bool WriteFile(string folder, string fileName, Result result)
    {
        try
        {
            var rows = result.Ds.Tables[0].Rows;
            var columns = result.Ds.Tables[0].Columns;

            string tempFile = Path.Combine(folder, fileName);
            StreamWriter writer = new StreamWriter(tempFile);

            List<string> lines = new List<string>();

            List<string> headerLine = new List<string>();

            headerLine.Add("szr_azon");
            headerLine.Add("ak_azon");
            headerLine.Add("csomag_pir_azon");
            headerLine.Add("idoszak_tol");
            headerLine.Add("idoszak_ig");

            for (int i = 0; i < columns.Count; i++)
            {
                headerLine.Add(columns[i].ColumnName);
            }

            lines.Add(string.Join("|", headerLine.ToArray()));

            StringBuilder rowSB = new StringBuilder();

            for (int i = 0; i < rows.Count; i++)
            {
                List<string> rowLineItems = new List<string>();

                rowLineItems.Add("irat");
                rowLineItems.Add(adatKorAzon);
                rowLineItems.Add(pirAzonosito);
                rowLineItems.Add(naploStartDate.ToString("yyyy-MM-dd"));
                rowLineItems.Add(naploEndDate.ToString("yyyy-MM-dd"));

                for (int j = 0; j < rows[i].ItemArray.Length; j++)
                {
                    rowLineItems.Add(GetRowValueAsString(rows[i], j));
                }

                lines.Add(string.Join("|", rowLineItems.ToArray()));
            }
            writer.Write(string.Join("\n", lines.ToArray()));
            writer.Close();
        }
        catch (Exception ex)
        {
            lastException = ex;
            return false;
        }
        return true;
    }

    private bool WriteNaploFile(string folder, string fileName, Dictionary<string, int> files, DateTime idoTol, DateTime idoIg)
    {
        try
        {
            string tempFile = Path.Combine(folder, fileName);
            StreamWriter writer = new StreamWriter(tempFile);

            List<string> lines = new List<string>();

            foreach (var f in files)
            {
                string[] lineArr = { Path.GetFileNameWithoutExtension(f.Key), idoTol.ToString("yyyy-MM-dd"), idoIg.ToString("yyyy-MM-dd"), f.Value.ToString() };
                lines.Add(string.Join("|", lineArr));
            }
            writer.Write(string.Join("\n", lines.ToArray()));
            writer.Close();
        }
        catch (Exception ex)
        {
            lastException = ex;
            return false;
        }
        return true;
    }

    private Contentum.eIntegrator.Service.UploadDocumentMetaData GetMetadataObject(bool empty)
    {
        Contentum.eIntegrator.Service.UploadDocumentMetaData UploadDocumentMetaDataValue = new Contentum.eIntegrator.Service.UploadDocumentMetaData();
        UploadDocumentMetaDataValue.fajlNev = tarFileName;
        UploadDocumentMetaDataValue.pirCode = pirAzonosito;
        UploadDocumentMetaDataValue.adatkorAzon = adatKorAzon;
        UploadDocumentMetaDataValue.fajlVerzio = 1;
        UploadDocumentMetaDataValue.deperszonVerzio = "1.0";
        UploadDocumentMetaDataValue.feladasAzonosito = logId;
        UploadDocumentMetaDataValue.feladasIdopontja = DateTime.Now;
        UploadDocumentMetaDataValue.levalogatsIdopontja = todayDate;
        UploadDocumentMetaDataValue.idoszakTol = naploStartDate.Date;
        UploadDocumentMetaDataValue.idoszakIg = naploEndDate.Date;
        UploadDocumentMetaDataValue.gyakorisag = _gyakorisag;
        UploadDocumentMetaDataValue.szakrendszer = Contentum.eIntegrator.Service.rendszer.IRAT;
        UploadDocumentMetaDataValue.checksum = empty ? null : currentCheckSum;
        UploadDocumentMetaDataValue.felado = fejlesztoAzonosito;
        UploadDocumentMetaDataValue.ures = empty ? 1 : 0;
        UploadDocumentMetaDataValue.forras = "lokalis";

        return UploadDocumentMetaDataValue;
    }

    public static byte[] ReadFully(Stream input)
    {
        byte[] buffer = new byte[16 * 1024];
        using (MemoryStream ms = new MemoryStream())
        {
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            return ms.ToArray();
        }
    }

    private string CalculateCheckSum(string folder, string tarFileName)
    {
        try
        {
            string tarFilePath = Path.Combine(folder, tarFileName);
            string checkSumFilePath = Path.Combine(folder, "checksumtmp.tmp");

            FileStream tarStream = File.OpenRead(tarFilePath);


            StreamWriter sWriter = new StreamWriter(checkSumFilePath, false, Encoding.ASCII);
            sWriter.Write(previousCheckSum);
            sWriter.Close();

            FileStream checkSumStream = File.Open(checkSumFilePath, FileMode.Append);
            BinaryWriter writer = new BinaryWriter(checkSumStream, Encoding.ASCII);

            Logger.Error("Previous checksum: " + previousCheckSum);

            //writer.Write(Encoding.ASCII.GetBytes(previousCheckSum));            
            writer.Write(ReadFully(tarStream));
            writer.Close();
            checkSumStream.Close();

            string args = String.Format("-hashfile " + checkSumFilePath + " MD5");

            //Logger.Debug(String.Format("Process: {0} {1}", batchFilePath, args));

            ProcessStartInfo process = new ProcessStartInfo
            {
                CreateNoWindow = false,
                UseShellExecute = false,
                //WorkingDirectory = msPath,
                RedirectStandardOutput = true,
                FileName = "certutil.exe",
                Arguments = args
            };


            //Process p = Process.Start(batchFilePath, args);
            //Process p = Process.Start(process);

            Process p = new Process();
            p.StartInfo = process;

            p.Start();

            string output = p.StandardOutput.ReadToEnd();
            Logger.Debug(String.Format("Process output: {0}", output));

            p.WaitForExit();
            int result = p.ExitCode;

            if (result == 0)
            {
                string[] splitted = output.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                return splitted[1].Replace(" ", "");
            }

        }
        catch (Exception ex)
        {
            lastException = ex;
            return "-1";
        }
        return "-1";
    }

    private DataRow GetLastLog(string modulId)
    {
        ExecParam logExecParam = GetExecParam();

        INT_LogSearch search = new INT_LogSearch();
        search.Modul_id.Operator = Contentum.eQuery.Query.Operators.equals;
        search.Modul_id.Value = modulId;
        search.ExternalId.Value = string.Empty;
        search.ExternalId.Operator = Contentum.eQuery.Query.Operators.notnull;
        search.OrderBy = " ModositasIdo DESC ";
        search.TopRow = 1;

        Result result = INTLogService.GetAll(logExecParam, search);

        if (!result.IsError && result.Ds.Tables[0].Rows.Count > 0)
        {
            return result.Ds.Tables[0].Rows[0];
        }

        return null;
    }

    private Result StartAdatAtadas(ExecParam execParam, string modulId, string parancs)
    {
        lastLog = GetLastLog(modulId);
        Result result = new Result();

        if (lastLog != null)
        {
            string lastStatus = lastLog["Status"] == DBNull.Value ? null : lastLog["Status"].ToString();
            if (lastStatus == "0")
            {
                result.ErrorMessage = "Folyamatban lévő sync. ModulId: " + modulId + ". Futassa a Statuszlekerdezes webservice-t.";
                result.ErrorCode = "01";
                return result;
            }
            if (lastStatus == null || lastStatus == "9")
            {
                ver = int.Parse(lastLog["PackageVer"].ToString()) + 1;
            }
        }
        else
        {
            ver = 0;
        }

        Contentum.eIntegrator.Service.INT_LogService service = eIntegratorService.ServiceFactory.GetINT_LogService();
        INT_Log record = new INT_Log();
        record.Updated.SetValueAll(false);

        record.Status = "0";
        record.Updated.Status = true;
        record.PackageVer = ver.ToString();
        record.Updated.PackageVer = true;
        record.Modul_id = modulId;
        record.Updated.Modul_id = true;
        record.Sync_StartDate = calculatedStartDate.ToString();
        record.Updated.Sync_StartDate = true;
        record.Sync_EndDate = calculatedEndDate.ToString();
        record.Updated.Sync_EndDate = true;
        record.Parancs = parancs;
        record.Updated.Parancs = true;
        record.ErvKezd = DateTime.Now.ToString();
        record.Updated.ErvKezd = true;
        record.ErvVege = new DateTime(4700, 12, 31).ToString();
        record.Updated.ErvVege = true;

        result = service.Insert(execParam, record);
        return result;

    }

    private INT_Modulok GetModul(string modulId)
    {
        ExecParam modulExecParam = GetExecParam();
        modulExecParam.Record_Id = modulId;
        Result result = INTModulokService.Get(modulExecParam);

        if (!result.IsError)
        {
            return (INT_Modulok)result.Record;
        }

        return null;
    }

    private INT_Log GetLog(string logId)
    {
        ExecParam logExecParam = GetExecParam();
        logExecParam.Record_Id = logId;
        Result result = INTLogService.Get(logExecParam);

        if (!result.IsError)
        {
            return (INT_Log)result.Record;
        }

        return null;
    }

    private string GetPreviousHash(string modulId)
    {
        ExecParam logExecParam = GetExecParam();
        string hash = string.Empty;

        INT_LogSearch search = new INT_LogSearch();

        search.Modul_id.Value = modulId;
        search.Modul_id.Operator = Contentum.eQuery.Query.Operators.equals;
        search.PackageHash.Value = string.Empty;
        search.PackageHash.Operator = Contentum.eQuery.Query.Operators.notnull;
        search.ExternalId.Value = string.Empty;
        search.ExternalId.Operator = Contentum.eQuery.Query.Operators.notnull;
        search.Status.Value = "1";
        search.Status.Operator = Contentum.eQuery.Query.Operators.equals;
        search.TopRow = 1;
        search.OrderBy = " ModositasIdo DESC ";

        Result result = INTLogService.GetAll(logExecParam, search);

        if (!result.IsError)
        {
            if (result.Ds.Tables[0].Rows.Count > 0)
            {
                hash = GetRowValueAsString(result.Ds.Tables[0].Rows[0], "PackageHash");
            }
        }

        return hash;
    }

    private bool CreateTarGZ(string folder, List<string> filesToZip, string tarFileName)
    {
        try
        {
            //string filesFolder = "c:\\testfolder\\test\\";
            string tempFile = Path.Combine(folder, tarFileName);

            using (FileStream fs = new FileStream(tempFile, FileMode.Create, FileAccess.Write, FileShare.None))
            using (Stream gzipStream = new GZipOutputStream(fs))
            using (TarArchive tarArchive = TarArchive.CreateOutputTarArchive(gzipStream))
            {
                foreach (string filename in filesToZip)
                {
                    {
                        TarEntry tarEntry = TarEntry.CreateEntryFromFile(Path.Combine(folder, filename));
                        tarEntry.Name = Path.GetFileName(filename);
                        tarArchive.WriteEntry(tarEntry, false);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            lastException = ex;
            return false;
        }
        return true;
    }

    public static Result FileUpload(ExecParam execParam, Stream stream, string fileName, bool isSigned, string existingFileId)
    {
        #region Feltöltött file beolvasása byte[]-ba

        stream.Position = 0;
        System.IO.StreamReader sr = new System.IO.StreamReader(stream);
        string s = sr.ReadToEnd();
        byte[] buf = System.Text.ASCIIEncoding.ASCII.GetBytes(s);
        //byte[] buf = fileUpload.FileBytes;

        #endregion Feltöltött file beolvasása byte[]-ba

        DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();

        #region FileUpload
        KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents(fileName, isSigned);
        //string checkinmode = "1";
        //if (TextBoxFileName.Text == "-")
        //{
        //    checkinmode = "2";
        //    krt_Dokumentumok.Id = Request.QueryString.Get(QueryStringVars.Id);
        //}
        //Result result = documentService.DirectUploadAndDocumentInsert(execParam, Contentum.eUtility.Constants.DocumentStoreType.UCM
        //    , SITE_PATH, DOC_LIB_PATH, FOLDER_PATH, fileName, buf, checkinmode, krt_Dokumentumok);

        string ujBejegyzes = String.IsNullOrEmpty(existingFileId) ? "IGEN" : "NEM";

        String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                   "<iktatokonyv></iktatokonyv>" +
                                                   "<sourceSharePath></sourceSharePath>" +
                                                   "<foszam>{0}</foszam>" +
                                                   "<kivalasztottIratId></kivalasztottIratId>" +
                                                   "<docmetaDokumentumId>{1}</docmetaDokumentumId>" +
                                                   "<megjegyzes></megjegyzes>" +
                                                   "<munkaanyag>{2}</munkaanyag>" +
                                                   "<ujirat>{3}</ujirat>" +
                                                   "<vonalkod></vonalkod>" +
                                                   "<docmetaIratId></docmetaIratId>" +
                                                   "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                   "<ucmiktatokonyv>{5}</ucmiktatokonyv>" +
                                               "</uploadparameterek>"
                                               , String.Empty
                                               , existingFileId
                                               , "NEM"
                                               , "IGEN"
                                               , ujBejegyzes
                                               , "HK"
                                               , fileName
                                               );

        Result result_upload = documentService.UploadFromeRecordWithCTTCheckin
            (
               execParam
               , Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
               , fileName
               , buf
               , krt_Dokumentumok
               , uploadXmlStrParams
            );

        return result_upload;


        #endregion FileUpload
    }

    public static KRT_Dokumentumok GetBusinessObjectFromComponents(string fileName, bool isSigned)
    {
        KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();
        // összes mező update-elhetőségét kezdetben letiltani:
        krt_Dokumentumok.Updated.SetValueAll(false);
        krt_Dokumentumok.Base.Updated.SetValueAll(false);

        krt_Dokumentumok.FajlNev = fileName;
        krt_Dokumentumok.Updated.FajlNev = true;

        krt_Dokumentumok.Megnyithato = "1";
        krt_Dokumentumok.Updated.Megnyithato = true;

        krt_Dokumentumok.Olvashato = "1";
        krt_Dokumentumok.Updated.Olvashato = true;

        krt_Dokumentumok.Titkositas = "0";
        krt_Dokumentumok.Updated.Titkositas = true;

        krt_Dokumentumok.ElektronikusAlairas = isSigned ? KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas : String.Empty;
        krt_Dokumentumok.Updated.ElektronikusAlairas = true;

        krt_Dokumentumok.Tipus = "Alairas (Xml)";
        krt_Dokumentumok.Updated.Tipus = true;

        krt_Dokumentumok.ErvKezd = DateTime.Today.ToString();
        krt_Dokumentumok.Updated.ErvKezd = true;

        krt_Dokumentumok.Base.Ver = "1";
        krt_Dokumentumok.Base.Updated.Ver = true;

        return krt_Dokumentumok;
    }

    public static ExecParam GetExecParam()
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = AdminUser;
        execParam.LoginUser_Id = AdminUser;

        return execParam;
    }

    //private void AddRow(StringBuilder sb, DataRow row)
    //{
    //    int i = 0;
    //    sb.Append(GetRowValueAsString(row, 0));

    //    sb.Append(row.ItemArray.Select(x=> GetRowValueAsString(.Aggregate((a, b) => a + "|" + b));

    //    //while (i < row.ItemArray.Length - 1)
    //    //{
    //    //    sb.Append("|");
    //    //    sb.Append(GetRowValueAsString(row, i));
    //    //    i++;
    //    //}

    //}

    #endregion





}