﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Services;
using System.Xml.Serialization;
using System.Web.Services.Protocols;

using LoginData = Contentum.eIntegrator.WebService.ETDR.Login;
using GetIntModData = Contentum.eIntegrator.WebService.ETDR.GetIntMod;
using GetUgyintezokData = Contentum.eIntegrator.WebService.ETDR.GetUgyintezok;
using AddErkeztetoAlapData = Contentum.eIntegrator.WebService.ETDR.AddErkeztetoAlap;
using AddIktatasByUgyfData = Contentum.eIntegrator.WebService.ETDR.AddIktatasByUgyf;
using UpdIktTevesData = Contentum.eIntegrator.WebService.ETDR.UpdIktTeves;

using CommonConstants = Contentum.eIntegrator.WebService.ETDR.Constants;
using Contentum.eIntegrator.WebService.ETDR.IntServices;

[WebService(Namespace = Contentum.eIntegrator.WebService.ETDR.Constants.DEFAULT_NAMESPACE)]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class INT_ETDRService : WebService
{
    private static string ServiceURL;

    static INT_ETDRService()
    {
        ServiceURL = ConfigurationManager.AppSettings["ETDRServiceURL"];
        // <add key="ETDRServiceURL" value="http://ax-vfphtst03/INT_ETDRService"/>
    }

    [WebMethod(Description = "Bejelentkezés, ami az azonosításra szolgál.")]
    [SoapDocumentMethod(RequestElementName = "Login",
                        RequestNamespace = CommonConstants.DEFAULT_NAMESPACE,
                        // Avoid wrapper class
                        ParameterStyle = SoapParameterStyle.Bare,
                        ResponseElementName = "LoginResponse",
                        ResponseNamespace = CommonConstants.DEFAULT_NAMESPACE)]
    [return: XmlElement("LoginResponse", Namespace = CommonConstants.DEFAULT_NAMESPACE)]
    [LogExtension]
    public LoginData.LoginResponse Login(LoginData.Login Login)
    {
        return new IntServiceLogin().Login(Login);
    }

    [WebMethod(Description = "Intéző módokat adja vissza egy DataSet-ben.")]
    [SoapDocumentMethod(RequestElementName = "Get_Intmod",
                        RequestNamespace = CommonConstants.DEFAULT_NAMESPACE,
                        // Avoid wrapper class
                        ParameterStyle = SoapParameterStyle.Bare,
                        ResponseElementName = "Get_IntmodResponse",
                        ResponseNamespace = CommonConstants.DEFAULT_NAMESPACE)]
    [return: XmlElement("Get_IntmodResponse", Namespace = CommonConstants.DEFAULT_NAMESPACE)]
    [LogExtension]
    public GetIntModData.Get_IntModResponse Get_IntMod(GetIntModData.Get_IntMod Get_IntMod)
    {
        return new IntServiceGetIntmod().GetIntMod(Get_IntMod);
    }

    [WebMethod(Description = "Ügyintézőket adja vissza egy DataSet-ben")]
    [SoapDocumentMethod(RequestElementName = "Get_Ugyintezok",
                        RequestNamespace = CommonConstants.DEFAULT_NAMESPACE,
                        // Avoid wrapper class
                        ParameterStyle = SoapParameterStyle.Bare,
                        ResponseElementName = "Get_UgyintezokResponse",
                        ResponseNamespace = CommonConstants.DEFAULT_NAMESPACE)]
    [return: XmlElement("Get_UgyintezokResponse", Namespace = CommonConstants.DEFAULT_NAMESPACE)]
    [LogExtension]
    public GetUgyintezokData.Get_UgyintezokResponse Get_Ugyintezok(GetUgyintezokData.Get_Ugyintezok Get_Ugyintezok)
    {
        return new IntServiceGet_Ugyintezok().Get_Ugyintezok(Get_Ugyintezok);
    }

    [WebMethod(Description = "Érkeztetés rögzítése az iktatórendszerbe, válasz visszaadása DataSet-ben.")]
    [SoapDocumentMethod(RequestElementName = "Add_Erkezteto_Alap",
                        RequestNamespace = CommonConstants.DEFAULT_NAMESPACE,
                        // Avoid wrapper class
                        ParameterStyle = SoapParameterStyle.Bare,
                        ResponseElementName = "Add_Erkezteto_AlapResponse",
                        ResponseNamespace = CommonConstants.DEFAULT_NAMESPACE)]
    [return: XmlElement("Add_Erkezteto_AlapResponse", Namespace = CommonConstants.DEFAULT_NAMESPACE)]
    [LogExtension]
    public AddErkeztetoAlapData.Add_Erkezteto_AlapResponse Add_Erkezteto_Alap(AddErkeztetoAlapData.Add_Erkezteto_Alap Add_Erkezteto_Alap)
    {
        return new IntServiceAddErkeztetoAlap().Add_Erkezteto_Alap(Add_Erkezteto_Alap);
    }

    [WebMethod(Description = "Főszámos vagy alszámos iktatás az iktatórendszerben")]
    [SoapDocumentMethod(RequestElementName = "Add_Iktatas_By_Ugyf",
                        RequestNamespace = CommonConstants.DEFAULT_NAMESPACE,
                        // Avoid wrapper class
                        ParameterStyle = SoapParameterStyle.Bare,
                        ResponseElementName = "Add_Iktatas_By_UgyfResponse",
                        ResponseNamespace = CommonConstants.DEFAULT_NAMESPACE)]
    [return: XmlElement("Add_Iktatas_By_UgyfResponse", Namespace = CommonConstants.DEFAULT_NAMESPACE)]
    [LogExtension]
    public AddIktatasByUgyfData.Add_Iktatas_By_UgyfResponse Add_Iktatas_By_Ugyf(AddIktatasByUgyfData.Add_Iktatas_By_Ugyf Add_Iktatas_By_Ugyf)
    {
        return new IntServiceAdd_Iktatas_By_Ugyf().Add_Iktatas_By_Ugyf(Add_Iktatas_By_Ugyf);
    }

    [WebMethod(Description = "Iktatás sztornózása")]
    [SoapDocumentMethod(RequestElementName = "Upd_IktTeves",
                        RequestNamespace = CommonConstants.DEFAULT_NAMESPACE,
                        // Avoid wrapper class
                        ParameterStyle = SoapParameterStyle.Bare,
                        ResponseElementName = "Upd_IktTevesResponse",
                        ResponseNamespace = CommonConstants.DEFAULT_NAMESPACE)]
    [return: XmlElement("Upd_IktTevesResponse", Namespace = CommonConstants.DEFAULT_NAMESPACE)]
    [LogExtension]
    public UpdIktTevesData.Upd_IktTevesResponse Upd_IktTeves(UpdIktTevesData.Upd_IktTeves Upd_IktTeves)
    {
        return new IntServiceUpdIktTeves().Upd_IktTeves(Upd_IktTeves);
    }
}