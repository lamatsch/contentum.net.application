﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Web.Services;

/// <summary>
/// Summary description for INT_ETDR_ObjektumokService
/// </summary>
[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
// [System.Web.Script.Services.ScriptService]
public class INT_ETDR_ObjektumokService : System.Web.Services.WebService
{
    private INT_ETDR_ObjektumokStoredProcedure sp = null;

    private DataContext dataContext;

    public INT_ETDR_ObjektumokService()
    {
        dataContext = new DataContext(this.Application);
        sp = new INT_ETDR_ObjektumokStoredProcedure(dataContext);
    }

    public INT_ETDR_ObjektumokService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new INT_ETDR_ObjektumokStoredProcedure(dataContext);
    }

    [WebMethod()]
    public Result Get_Felhasznalok(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Get_Felhasznalok(execParam);

            if (result.IsError)
                throw new ResultException(result);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result Get_IntezesiModok(ExecParam execParam)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.SP_GetIntezesiModok(execParam);

            if (result.IsError)
                throw new ResultException(result);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result Get_By_EtdrId(ExecParam execParam, int ETDR_Id, string obj_Tip_Kod)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Get_By_EtdrId(execParam, ETDR_Id, obj_Tip_Kod);

            if (result.IsError)
                throw new ResultException(result);

            if (result.Ds.Tables[0].Rows.Count > 0)
                result.Uid = result.Ds.Tables[0].Rows[0]["Obj_Id"].ToString();

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result Get_By_ObjId(ExecParam execParam, string obj_Id, string obj_Tip_Kod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            Guid guidObj_Id;
            try
            {
                guidObj_Id = new Guid(obj_Id);
            }
            catch
            {
                throw new Exception("Obj_Id nem guid!");
            }


            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Get_By_ObjId(execParam, guidObj_Id, obj_Tip_Kod);

            if (result.IsError)
                throw new ResultException(result);

            if (result.Ds.Tables[0].Rows.Count > 0)
                result.Uid = result.Ds.Tables[0].Rows[0]["ETDR_Id"].ToString();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result Insert_ETDR_Obj(ExecParam execParam, string obj_Id, string obj_Tip_Id, string obj_Tip_Nev, string data, string note)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            Guid guidObj_Id;
            try
            {
                guidObj_Id = new Guid(obj_Id);
            }
            catch
            {
                throw new Exception("Obj_Id nem guid!");
            }

            if (string.IsNullOrEmpty(obj_Tip_Id) && string.IsNullOrEmpty(obj_Tip_Nev))
            {
                throw new ArgumentException("Missing obj_Tip");
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.SP_Insert_ETDR_Obj(execParam, obj_Id, obj_Tip_Id, obj_Tip_Nev, data, note);

            if (result.IsError)
                throw new ResultException(result);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
}