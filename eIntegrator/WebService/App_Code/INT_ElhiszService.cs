﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Elhisz;
using Elhisz.BusinessObjects;
using Elhisz.DataBase;
using NMHH.Elhitet;
using NMHH.Elhitet.BusinessObject;
using NMHH.Elhitet.Database;
using NMHH.Elhitet.Servlet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for INT_ElhiszService
/// </summary>
[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class INT_ElhiszService : System.Web.Services.WebService
{
    private DataContext dataContext;
    private static ExecParam execParam = new ExecParam();
    const string resolutionModulID = "Contentum.eIntegratorWebService.INT_ElhiszService";

    public INT_ElhiszService()
    {
        dataContext = new DataContext(this.Application);
        execParam.Felhasznalo_Id = GetElhiszUserId();
        execParam.FelhasznaloSzervezet_Id = execParam.Felhasznalo_Id;

    }

    string GetElhiszUserId()
    {
        string felhasznaloId = String.Empty;

        KRT_ParameterekService _KRT_ParameterekService = eAdminService.ServiceFactory.GetKRT_ParameterekService();
        KRT_ParameterekSearch search = new KRT_ParameterekSearch();
        search.Nev.Value = "ElhiszUserId";
        search.Nev.Operator = Query.Operators.equals;
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309"; //admin
        Result res = _KRT_ParameterekService.GetAll(execParam, search);

        if (!res.IsError)
        {
            if (res.Ds.Tables[0].Rows.Count > 0)
            {
                felhasznaloId = res.Ds.Tables[0].Rows[0]["Ertek"].ToString();
                Logger.Debug(String.Format("ElhiszUserId={0}", felhasznaloId));
            }
            else
            {
                Logger.Error(String.Format("GetElhiszUserId hiba: ElhiszUserId paraméter nem található"));
            }
        }
        else
        {
            Logger.Error("GetElhiszUserId hiba", execParam, res);
        }

        return felhasznaloId;
    }

    [WebMethod(Description = "Az Elektronikus kiküldés technikai felhasználónak átadott küldemények elküldése az elhisz-nek")]
    public void KuldemenyekToElhisz()
    {

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();

        try
        {
            KuldemenyekAtvetel();

            List<string> kuldemenyek = GetAtadandoKuldemenyek();

            if (kuldemenyek.Count > 0)
            {
                Dictionary<string, List<ElhiszAdatok>> elhiszAdatok = GetElhiszAdatok(kuldemenyek);

                if (elhiszAdatok != null)
                {
                    foreach (KeyValuePair<string, List<ElhiszAdatok>> kv in elhiszAdatok)
                    {
                        ElhiszAdatok adat = kv.Value[0];
                        List<ELhiszCimzett> cimzettek = new List<ELhiszCimzett>();
                        List<Guid> kuldemenyIds = new List<Guid>();

                        foreach (ElhiszAdatok cimzettAdat in kv.Value)
                        {
                            cimzettek.Add(cimzettAdat.Cimzett);
                            if (!kuldemenyIds.Contains(cimzettAdat.Kuldemeny_Id))
                            {
                                kuldemenyIds.Add(cimzettAdat.Kuldemeny_Id);
                            }
                        }

                        try
                        {
                            SendElhiszAdat(adat, cimzettek, kuldemenyIds);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("SendElhiszAdat hiba", ex);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(e.Message);
        }

        log.WsEnd(execParam, result);
        return;
    }

    private void KuldemenyekAtvetel()
    {
        Logger.Debug("KuldemenyekAtvetel start");
        Result result = new Result();
        using (EREC_KuldKuldemenyekService kuldService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService())
        {
            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            search.Csoport_Id_Felelos.Value = execParam.Felhasznalo_Id;
            search.Csoport_Id_Felelos.Operator = Query.Operators.equals;
            search.Allapot.Value = KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt;
            search.Allapot.Operator = Query.Operators.equals;
            search.TovabbitasAlattAllapot.Value = KodTarak.KULDEMENY_ALLAPOT.Expedialt;
            search.TovabbitasAlattAllapot.Operator = Query.Operators.equals;

            result = kuldService.GetAll(execParam, search);

            if (!result.IsError)
            {
                Logger.Debug(String.Format("Atveendo kuldemenyek szama: {0}", result.Ds.Tables[0].Rows.Count));
                foreach (DataRow dr in result.Ds.Tables[0].Rows)
                {
                    string kuldemenyId = dr["Id"].ToString();
                    Logger.Debug(String.Format("kuldemenyId: {0}", kuldemenyId));
                    Result resAtvetel = kuldService.Atvetel(execParam, kuldemenyId);

                    if (resAtvetel.IsError)
                    {
                        Logger.Error("KuldemenyekAtvetel hiba", execParam, resAtvetel);
                    }
                }
            }
            else
            {
                Logger.Error("KuldemenyekAtvetel hiba", execParam, result);
            }

        }
    }

    private List<string> GetAtadandoKuldemenyek()
    {
        Logger.Debug("GetAtadandoKuldemenyek start");

        List<string> kuldemenyek = new List<string>();

        using (EREC_KuldKuldemenyekService kuldService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService())
        {
            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            search.FelhasznaloCsoport_Id_Orzo.Value = execParam.Felhasznalo_Id;
            search.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;
            search.Allapot.Value = KodTarak.KULDEMENY_ALLAPOT.Expedialt;
            search.Allapot.Operator = Query.Operators.equals;
            search.HivatkozasiSzam.Operator = Query.Operators.isnull;

            Result result = kuldService.GetAll(execParam, search);

            if (!result.IsError)
            {
                Logger.Debug(String.Format("GetAtadandoKuldemenyek szama: {0}", result.Ds.Tables[0].Rows.Count));


                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    string kuldemenyId = row["Id"].ToString();
                    kuldemenyek.Add(kuldemenyId);
                }
            }
            else
            {
                Logger.Error("GetAtadandoKuldemenyek hiba", execParam, result);
            }

        }

        return kuldemenyek;
    }

    private string CreateRecipientsXML(ElhiszAdatok adatok, List<ELhiszCimzett> cimzettek)
    {
        Logger.Debug("CreateRecipientsXML start");

        EREC_eBeadvanyok eBeadvany = adatok.eBeadvany;

        if (String.IsNullOrEmpty(eBeadvany.KR_DokTipusHivatal))
        {
            eBeadvany.KR_DokTipusHivatal = "NMHH";
        }

        if (String.IsNullOrEmpty(eBeadvany.KR_FileNev))
        {
            eBeadvany.KR_FileNev = adatok.FajlNev;
        }

        if (String.IsNullOrEmpty(eBeadvany.KR_Valaszutvonal))
        {
            eBeadvany.KR_Valaszutvonal = "0";
        }

        PR_Parameterek pr_parameterek = eBeadvany.Get_PR_Parameterek();
        if (String.IsNullOrEmpty(pr_parameterek.InputFormat))
            pr_parameterek.InputFormat = GetInputFormat(adatok.FajlNev);
        if (String.IsNullOrEmpty(pr_parameterek.OutputFormat))
            pr_parameterek.OutputFormat = GetOutputFormat(adatok.FajlNev);
        eBeadvany.Set_PR_Parameterek(pr_parameterek);

        RecipientsXmlHelper xmlHelper = new RecipientsXmlHelper();
        return xmlHelper.CreateRecipientsXML(adatok, cimzettek);
    }

    Dictionary<string, List<ElhiszAdatok>> GetElhiszAdatok(List<string> kuldemenyek)
    {
        try
        {
            string where = String.Format("EREC_KuldKuldemenyek.Id in ('{0}')", String.Join("','", kuldemenyek.ToArray()));
            ElhiszStoredProcedure sp = new ElhiszStoredProcedure();
            Result result = sp.GetElhiszAdatok(execParam, where);

            if (!result.IsError)
            {
                Dictionary<string, List<ElhiszAdatok>> iratok = new Dictionary<string, List<ElhiszAdatok>>();
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    ElhiszAdatok adat = new ElhiszAdatok(row);
                    if (iratok.ContainsKey(adat.Irat_Azonosito))
                    {
                        iratok[adat.Irat_Azonosito].Add(adat);
                    }
                    else
                    {
                        iratok.Add(adat.Irat_Azonosito, new List<ElhiszAdatok>() { adat });
                    }
                }

                return iratok;
            }
            else
            {
                Logger.Error("GetElhiszAdatok hiba", execParam, result);
            }
        }
        catch (Exception ex)
        {
            Logger.Error("GetElhiszAdatok hiba", ex);
        }

        return null;
    }

    void SendElhiszAdat(ElhiszAdatok adat, List<ELhiszCimzett> cimzettek, List<Guid> kuldemenyIds)
    {
        Logger.Info(String.Format("SendElhiszAdat kezdete: {0}", adat.Irat_Azonosito));
        ElhitetServletManager servlet = new ElhitetServletManager();

        string recipientsXML = CreateRecipientsXML(adat, cimzettek);
        Logger.Debug(String.Format("recipientsXML={0}", recipientsXML));
        byte[] fileContent = GetDocumentContent(adat.External_Link);
        string organizationId;
        string organizationUnit = GetOrganizationUnit(adat, out organizationId);
        ELHISZServerResolutionResponse resolutionResponse = servlet.UnsignedResolution(adat.FajlNev, resolutionModulID, adat.Irat_Azonosito,
            null, organizationUnit, null, null, fileContent);

        if (resolutionResponse.IsError)
        {
            Logger.Error(String.Format("UnsignedResolution hiba: ErrorCode={0}, ErrorMessage={1}", resolutionResponse.ErrorCode, resolutionResponse.ErrorMessage));
        }
        else
        {
            Logger.Debug(String.Format("UnsignedResolution eredménye: RegistrationNumber={0}", resolutionResponse.RegistrationNumber));
            string registrationNumber = resolutionResponse.RegistrationNumber;
            ELHISZServerRecipientResponse recipientResponse = servlet.Recipients(resolutionModulID, adat.Irat_Azonosito, registrationNumber, recipientsXML);

            if (recipientResponse.IsError)
            {
                Logger.Error(String.Format("Recipients hiba: ErrorCode={0}, ErrorMessage={1}", recipientResponse.ErrorCode, recipientResponse.ErrorMessage));
            }
            else
            {
                foreach (Guid kuldemenyId in kuldemenyIds)
                {
                    SetKuldemenyRegistrationNumber(kuldemenyId, registrationNumber, organizationId);
                }
            }
        }
    }

    byte[] GetDocumentContent(string externalLink)
    {
        Logger.Debug(String.Format("GetDocumentContent kezdete: {0}", externalLink));
        using (WebClient wc = new WebClient())
        {
            wc.UseDefaultCredentials = true;
            return wc.DownloadData(externalLink);
        }
    }

    string GetOrganizationUnit(ElhiszAdatok adat, out string organizationId)
    {
        organizationId = String.Empty;

        //Felelős szervezet
        if (!String.IsNullOrEmpty(adat.Irat_Csoport_Id_Ugyfelelos))
        {
            organizationId = adat.Irat_Csoport_Id_Ugyfelelos;
            return GetCsoportNev(adat.Irat_Csoport_Id_Ugyfelelos);
        }

        //Intézkedő ügyintéző
        if(!String.IsNullOrEmpty(adat.Irat_FelhasznaloCsoport_Id_Ugyintez))
        {
            KRT_CsoportTagokService cstService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch cstSearch = new KRT_CsoportTagokSearch();
            cstSearch.Csoport_Id_Jogalany.Value = adat.Irat_FelhasznaloCsoport_Id_Ugyintez;
            cstSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            Result cstRes = cstService.GetAll(execParam, cstSearch);

            if (!cstRes.IsError && cstRes.Ds.Tables[0].Rows.Count > 0)
            {
                string csoport_id = cstRes.Ds.Tables[0].Rows[0]["Csoport_Id"].ToString();
                organizationId = csoport_id;
                return GetCsoportNev(csoport_id);
            }
        }

        return String.Empty;
    }

    string GetCsoportNev(string id)
    {
        KRT_CsoportokService csopService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
        ExecParam csopExecParam = execParam.Clone();
        csopExecParam.Record_Id = id;

        Result res = csopService.Get(csopExecParam);

        if (!res.IsError)
        {
            return (res.Record as KRT_Csoportok).Nev;
        }

        return String.Empty;
    }

    private string GetInputFormat(string fileName)
    {
        Logger.Debug(String.Format("GetInputFormat: {0}", fileName));

        string format = KodTarak.KULDEMENY_INPUT_FORMAT.UNSIGNED_DOCUMENT;

        string ext = Path.GetExtension(fileName);
        ext = ext.ToLower();

        bool isHiteles = fileName.Contains("-hiteles");

        switch (ext)
        {
            case ".pdf":
                {
                    if (isHiteles)
                        format = KodTarak.KULDEMENY_INPUT_FORMAT.SIGNED_PDF;
                    else
                        format = KodTarak.KULDEMENY_INPUT_FORMAT.UNSIGNED_PDF;
                }
                break;
            case ".xml":
                {
                    format = KodTarak.KULDEMENY_INPUT_FORMAT.SIGNED_XADES_XML;
                }
                break;
            case ".asice":
                {
                    format = KodTarak.KULDEMENY_INPUT_FORMAT.SIGNED_ASIC_E;
                }
                break;
            case ".kr":
                {
                    format = KodTarak.KULDEMENY_INPUT_FORMAT.KR;
                }
                break;
            case ".es3":
                {
                    format = KodTarak.KULDEMENY_INPUT_FORMAT.SIGNED_ES3;
                }
                break;
        }

        return format;
    }

    private string GetOutputFormat(string fileName)
    {
        Logger.Debug(String.Format("GetOutputFormat: {0}", fileName));

        string outputFormat = KodTarak.KULDEMENY_INPUT_FORMAT.SIGNED_ES3;
        string inputFormat = GetInputFormat(fileName);

        switch (inputFormat)
        {
            case KodTarak.KULDEMENY_INPUT_FORMAT.UNSIGNED_DOCUMENT:
                {
                    outputFormat = KodTarak.KULDEMENY_OUTPUT_FORMAT.SIGNED_ES3;
                }
                break;
            case KodTarak.KULDEMENY_INPUT_FORMAT.SIGNED_PDF:
            case KodTarak.KULDEMENY_INPUT_FORMAT.UNSIGNED_PDF:
                {
                    outputFormat = KodTarak.KULDEMENY_OUTPUT_FORMAT.SIGNED_PDF;
                }
                break;
            case KodTarak.KULDEMENY_INPUT_FORMAT.SIGNED_XADES_XML:
                {
                    outputFormat = KodTarak.KULDEMENY_OUTPUT_FORMAT.SIGNED_XADES_XML;
                }
                break;
            case KodTarak.KULDEMENY_INPUT_FORMAT.SIGNED_ASIC_E:
                {
                    outputFormat = KodTarak.KULDEMENY_OUTPUT_FORMAT.SIGNED_ASIC_E;
                }
                break;
            case KodTarak.KULDEMENY_INPUT_FORMAT.KR:
                {
                    outputFormat = KodTarak.KULDEMENY_OUTPUT_FORMAT.KR;
                }
                break;
            case KodTarak.KULDEMENY_INPUT_FORMAT.SIGNED_ES3:
                {
                    outputFormat = KodTarak.KULDEMENY_OUTPUT_FORMAT.SIGNED_ES3;
                }
                break;

        }

        return outputFormat;

    }

    void SetKuldemenyRegistrationNumber(Guid kuldemenyId, string registrationNumber, string organizationId)
    {
        Logger.Info(String.Format("SetKuldemenyRegistrationNumber kezdete: {0}, {1}", kuldemenyId, registrationNumber));

        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam kuldExecParam = execParam.Clone();
        kuldExecParam.Record_Id = kuldemenyId.ToString();

        Result kuldRes = service.Get(kuldExecParam);

        if (kuldRes.IsError)
        {
            Logger.Error("Küldemlény lekérése hiba", kuldExecParam, kuldRes);
        }
        else
        {
            EREC_KuldKuldemenyek kuldemeny = kuldRes.Record as EREC_KuldKuldemenyek;
            kuldemeny.Updated.SetValueAll(false);
            kuldemeny.Base.Updated.SetValueAll(false);
            kuldemeny.Base.Updated.Ver = true;

            kuldemeny.HivatkozasiSzam = registrationNumber;
            kuldemeny.Updated.HivatkozasiSzam = true;

            if (!String.IsNullOrEmpty(organizationId))
            {
                kuldemeny.Csoport_Id_Cimzett = organizationId;
                kuldemeny.Updated.Csoport_Id_Cimzett = true;
            }

            kuldRes = service.Update(kuldExecParam, kuldemeny);

            if (kuldRes.IsError)
            {
                Logger.Error("Küldemlény update hiba", kuldExecParam, kuldRes);
            }
        }
    }

    [WebMethod(Description = "Az ELhisznek elküldött küldemények állapotának visszaolvasása")]
    public void KuldemenyekFromElhisz()
    {

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();

        try
        {
            List<string> kuldemenyek = GetAtveendoKuldemenyek();

            if (kuldemenyek.Count > 0)
            {
                Dictionary<string, List<ElhiszAdatok>> elhiszAdatok = GetElhiszAdatok(kuldemenyek);

                if (elhiszAdatok != null && elhiszAdatok.Count > 0)
                {
                    List<string> resolutionNumberList = new List<string>();

                    foreach (KeyValuePair<string, List<ElhiszAdatok>> kv in elhiszAdatok)
                    {
                        string iratAzonosito = kv.Key;
                        resolutionNumberList.Add(iratAzonosito);
                    }

                    ElhiszOracleDatabaseManager db = new ElhiszOracleDatabaseManager();
                    DataSet ds = db.GetRecipientData(resolutionNumberList);
                    List<string> postazottKuldemenyek = new List<string>();

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string resolutionNumber = row["resolutionnumber"].ToString();
                        long xml_contact_id = (long)row["xml_contact_id"];
                        long errorcode = (long)row["errorcode"];
                        string kr_registrationnumber = row["kr_registrationnumber"].ToString();
                        DateTime? createdate = row["createdate"] as DateTime?;

                        Logger.Debug(String.Format("resolutionNumber={0}, xml_contact_id={1}, errorcode={2}, createdate={3}", resolutionNumber, xml_contact_id, errorcode, createdate));

                        List<ElhiszAdatok> iratAdatai = elhiszAdatok[resolutionNumber];

                        foreach (ElhiszAdatok adat in iratAdatai)
                        {
                            if (adat.Cimzett.Sorszam == xml_contact_id 
                                && (createdate == null || createdate > adat.Kuldemeny_LetrehozasIdo))
                            {
                                string kuldemenyId = adat.Kuldemeny_Id.ToString();
                                if (!postazottKuldemenyek.Contains(kuldemenyId))
                                {
                                    try
                                    {
                                        //postázott
                                        if (errorcode == 0)
                                        {
                                            Postazas(kuldemenyId, DateTime.Now.ToString(), kr_registrationnumber);
                                            postazottKuldemenyek.Add(kuldemenyId);
                                            UploadKikuldottDokumentum(adat.Irat_Id, resolutionNumber);
                                        }
                                        else
                                        {
                                            //hiba
                                            if (errorcode > 0)
                                            {
                                                KimenoKuldemenySztorno(kuldemenyId, errorcode);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error("Küldemény feldolgozás hiba", ex);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(e.Message);
        }

        log.WsEnd(execParam, result);
        return;
    }

    private List<string> GetAtveendoKuldemenyek()
    {
        Logger.Debug("GetAtveendoKuldemenyek start");

        List<string> kuldemenyek = new List<string>();

        using (EREC_KuldKuldemenyekService kuldService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService())
        {
            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            search.FelhasznaloCsoport_Id_Orzo.Value = execParam.Felhasznalo_Id;
            search.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;
            search.Allapot.Value = KodTarak.KULDEMENY_ALLAPOT.Expedialt;
            search.Allapot.Operator = Query.Operators.equals;
            search.HivatkozasiSzam.Operator = Query.Operators.notnull;

            Result result = kuldService.GetAll(execParam, search);

            if (!result.IsError)
            {
                Logger.Debug(String.Format("GetAtveendoKuldemenyek szama: {0}", result.Ds.Tables[0].Rows.Count));


                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    string kuldemenyId = row["Id"].ToString();
                    kuldemenyek.Add(kuldemenyId);
                }
            }
            else
            {
                Logger.Error("GetAtveendoKuldemenyek hiba", execParam, result);
            }

        }

        return kuldemenyek;
    }

    private void Postazas(string kuldemenyId, string postazasDatuma, string ragszam)
    {
        Logger.Info(String.Format("Postazas kezdete: {0}, {1}, {2}", kuldemenyId, postazasDatuma, ragszam));

        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam getExecParam = execParam.Clone();
        getExecParam.Record_Id = kuldemenyId;

        Result resGet = service.Get(getExecParam);

        if (!resGet.IsError)
        {
            EREC_KuldKuldemenyek kuldemeny = resGet.Record as EREC_KuldKuldemenyek;

            kuldemeny.Updated.SetValueAll(false);
            kuldemeny.Base.Updated.SetValueAll(false);
            kuldemeny.Base.Updated.Ver = true;

            kuldemeny.BelyegzoDatuma = postazasDatuma;
            kuldemeny.Updated.BelyegzoDatuma = true;

            kuldemeny.Tertiveveny = "1";
            kuldemeny.Updated.Tertiveveny = true;

            kuldemeny.RagSzam = ragszam;
            kuldemeny.Updated.RagSzam = true;

            Result res = service.Postazas(execParam, kuldemeny, "");

            if (res.IsError)
            {
                Logger.Error("Küldemény postázás hiba", execParam, res);
                throw new ResultException(res);
            }
        }
        else
        {
            Logger.Error("Küldemény lekérése hiba", getExecParam, resGet);
            throw new ResultException(resGet);
        }

        Logger.Info(String.Format("Postazas vege: {0}, {1}, {2}", kuldemenyId, postazasDatuma, ragszam));
    }

    private void UploadKikuldottDokumentum(Guid iratId, string resolutionNumber)
    {
        Logger.Info(String.Format("UploadKikuldottDokumentum kezdete: {0}, {1}", iratId, resolutionNumber));

        ElhitetOracleDatabaseManager db = new ElhitetOracleDatabaseManager();
        DataSet ds = db.GetResolutionData(resolutionNumber);

        if (ds.Tables[0].Rows.Count > 0)
        {
            DataRow row = ds.Tables[0].Rows[0];
            string fileNev = row["filenev"].ToString();
            byte[] fileContent = (byte[])row["completecontent"];

            if (!fileNev.Contains("hiteles") && !Path.GetExtension(fileNev).Equals(".kr"))
            {
                fileNev = Path.GetFileNameWithoutExtension(fileNev) + "-hiteles" + Path.GetExtension(fileNev);
            }

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            EREC_Csatolmanyok erec_csatolmany = new EREC_Csatolmanyok();
            erec_csatolmany.IraIrat_Id = iratId.ToString();
            erec_csatolmany.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.KikuldottDokumentum;

            Csatolmany csat = new Csatolmany();
            csat.Nev = fileNev;
            csat.Tartalom = fileContent;

            Result resUpload = service.CsatolmanyUpload(execParam, erec_csatolmany, csat);

            if (resUpload.IsError)
            {
                Logger.Error("CsatolmanyUpload hiba", execParam, resUpload);
                throw new ResultException(resUpload);
            }
        }
    }

    void KimenoKuldemenySztorno(string kuldemenyId, long errorcode)
    {
        Logger.Info(String.Format("KimenoKuldemenySztorno kezdete: {0}", kuldemenyId));

        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam sztornoExecParam = execParam.Clone();
        sztornoExecParam.Record_Id = kuldemenyId;


        Result resSztorno = service.KimenoKuldemenySztorno(sztornoExecParam);

        if (resSztorno.IsError)
        {
            Logger.Error("KimenoKuldemenySztorno hiba", sztornoExecParam, resSztorno);
        }

        string megjegyzes = GetErrorMessage(errorcode);
        KimenoKuldemenySetMegjegyzes(kuldemenyId, megjegyzes);
        
        Logger.Info(String.Format("KimenoKuldemenySztorno vege: {0}", kuldemenyId));
    }

    void KimenoKuldemenySetMegjegyzes(string kuldemenyId, string megjegyzes)
    {
        Logger.Info(String.Format("KimenoKuldemenySetMegjegyzes kezdete: {0} - {1}", kuldemenyId, megjegyzes));

        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam getExecParam = execParam.Clone();
        getExecParam.Record_Id = kuldemenyId;

        Result resGet = service.Get(getExecParam);

        if (resGet.IsError)
        {
            Logger.Error("Küldemény get hiba", getExecParam, resGet);
        }

        EREC_KuldKuldemenyek kuldemeny = resGet.Record as EREC_KuldKuldemenyek;

        kuldemeny.Updated.SetValueAll(false);
        kuldemeny.Base.Updated.SetValueAll(false);
        kuldemeny.Base.Updated.Ver = true;

        kuldemeny.Base.Note = megjegyzes;
        kuldemeny.Base.Updated.Note = true;

        Result resUpdate = service.Update(getExecParam, kuldemeny);

        if (resUpdate.IsError)
        {
            Logger.Error("Küldemény update hiba", getExecParam, resUpdate);
        }

        Logger.Info(String.Format("KimenoKuldemenySetMegjegyzes vege: {0} - {1}", kuldemenyId, megjegyzes));
    }

    string GetErrorMessage(long errorcode)
    {
        string msg = String.Empty;

        switch (errorcode)
        {
            case 1:
                msg = "Kiküldve, Központi Rendszer PGP hiba miatt titkosítás nélkül";
                break;
            case 100:
                msg = "Általános hiba, logok vizsgálata szükséges";
                break;
            case 101:
                msg = "Általános hiba a PGP-kulcs lekérésekor";
                break;
            case 102:
                msg = "Általános hiba a dokumentum Központi Rendszerre küldésekor";
                break;
            case 104:
                msg = "Nincs SOAP-válasz a dokumentum küldésekor";
                break;
            case 105:
                msg = "Érvénytelen hosszúságú kapcsolati kód";
                break;
            case 203:
                msg = "Érvénytelen állampolgár";
                break;
            case 205:
                msg = "Érvénytelen KRID (Hivatali kapu) azonosító";
                break;
        }

        if (String.IsNullOrEmpty(msg))
            msg = errorcode.ToString();
        else
            msg = String.Format("{0} - {1}", errorcode, msg);

        return msg;
    }

    [WebMethod(Description = "Tértivevények visszaolvasása")]
    public void TertivenyekFromElhisz()
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();

        try
        {
            List<EREC_KuldTertivevenyek> tertivevenyekList = GetTertivevenyek();

            List<string> ragszamList = new List<string>();

            foreach (EREC_KuldTertivevenyek tertiveveny in tertivevenyekList)
            {
                ragszamList.Add(tertiveveny.Ragszam);
            }

            ElhiszOracleDatabaseManager db = new ElhiszOracleDatabaseManager();
            DataSet ds = db.GetRecipientDataForTertiveveny(ragszamList);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string resolutionnumber = row["resolutionnumber"].ToString();
                string kr_registrationnumber = row["kr_registrationnumber"].ToString();
                string kr_receiptregistrationnumber = row["kr_receiptregistrationnumber"].ToString();
                string kr_nondeliveryregnumber = row["kr_nondeliveryregnumber"].ToString();
                string receiptregistrationnumber = row["receiptregistrationnumber"].ToString();

                Logger.Debug(String.Format("kr_registrationnumber={0}, kr_receiptregistrationnumber={1}, kr_nondeliveryregnumber={2}, resolutionnumber={3}, receiptregistrationnumber={4}",
                    kr_registrationnumber, kr_receiptregistrationnumber, kr_nondeliveryregnumber, resolutionnumber, receiptregistrationnumber));

                if (String.IsNullOrEmpty(kr_receiptregistrationnumber) && String.IsNullOrEmpty(kr_nondeliveryregnumber))
                {
                    Logger.Debug("Tértivevényre nem érkezett válasz");
                }
                else if (String.IsNullOrEmpty(receiptregistrationnumber))
                {
                    Logger.Error("receiptregistrationnumber üres!");
                }
                else
                {

                    foreach (EREC_KuldTertivevenyek tertiveveny in tertivevenyekList)
                    {
                        if (tertiveveny.Ragszam == kr_registrationnumber)
                        {
                            try
                            {
                                string kuldemenyId = tertiveveny.Kuldemeny_Id;
                                Logger.Debug(String.Format("Tértivevény feldolgozása: {0}, {1}", tertiveveny.Ragszam, kuldemenyId));
                                string fileName = String.Format("{0}.pdf", tertiveveny.Ragszam);
                                ResolutionReceipt receipt = GetResolutionReceipts(receiptregistrationnumber);

                                if (receipt != null && receipt.BedszPDF != null)
                                {
                                    if (!String.IsNullOrEmpty(kr_receiptregistrationnumber))
                                    {
                                        //címzett átvette
                                        TertivevenyErkeztetes(tertiveveny, KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_Cimzettnek, receipt.ReceivedDate);
                                        
                                    }
                                    else if (!String.IsNullOrEmpty(kr_nondeliveryregnumber))
                                    {
                                        //címzett ismeretlen
                                        TertivevenyErkeztetes(tertiveveny, KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_atvetelt_megtagadta, receipt.ReceivedDate);
                                        
                                    }

                                    UploadTertvivevenyFile(kuldemenyId, fileName, receipt.BedszPDF);
                                    UpdateResolutionReceipts(receiptregistrationnumber);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error("Tértivevény feldolgozása hiba", ex);
                            }
                        }
                    }
                }

                
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(e.Message);
        }

        log.WsEnd(execParam, result);
        return;
    }

    private List<EREC_KuldTertivevenyek> GetTertivevenyek()
    {
        Logger.Debug("GetTertivevenyek start");
        List<EREC_KuldTertivevenyek> tertivevenyekList = new List<EREC_KuldTertivevenyek>();

        EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
        EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
        search.Allapot.Value = KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett;
        search.Allapot.Operator = Query.Operators.equals;

        //30 napnál nem régebbi
        DateTime date = DateTime.Today.AddDays(-30);
        search.ErvKezd.Value = date.ToString();
        search.ErvKezd.Operator = Query.Operators.greaterorequal;

        Result res = service.GetAll(execParam, search);

        if (res.IsError)
        {
            Logger.Error("GetTertivevenyek hiba", execParam, res);
        }
        else
        {
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
                Utility.LoadBusinessDocumentFromDataRow(tertiveveny, row);
                tertivevenyekList.Add(tertiveveny);
            }

            Logger.Debug(String.Format("Tertivevenyek szama: {0}", tertivevenyekList.Count));
        }

        return tertivevenyekList;
    }

    private void TertivevenyErkeztetes(EREC_KuldTertivevenyek tertiveveny, string tertivisszaKod, DateTime receivedDate)
    {
        Logger.Debug(String.Format("TertivevenyErkeztetes: {0}, {1}", tertiveveny.Id, tertivisszaKod));

        tertiveveny.Updated.SetValueAll(false);
        tertiveveny.Base.Updated.SetValueAll(false);
        tertiveveny.Base.Updated.Ver = true;

        tertiveveny.TertivisszaKod = tertivisszaKod;
        tertiveveny.Updated.TertivisszaKod = true;

        tertiveveny.TertivisszaDat = receivedDate.ToString();
        tertiveveny.Updated.TertivisszaDat = true;

        if (tertivisszaKod == KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_Cimzettnek)
        {
            tertiveveny.AtvetelDat = receivedDate.ToString();
            tertiveveny.Updated.AtvetelDat = true;
        }

        EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
        Result res = service.TertivevenyErkeztetes(execParam, tertiveveny.Id, tertiveveny);

        if (res.IsError)
        {
            throw new ResultException(res);
        }
    }

    void UploadTertvivevenyFile(string kuldemenyId, string fileName, byte[] fileContent)
    {
        Logger.Debug(String.Format("UploadTertvivevenyFile: kuldemenyId={0}, fileName={1}", kuldemenyId, fileName));

        string dokumentumId = CsatolmanyUpload(kuldemenyId, fileName, fileContent);

        Logger.Debug(String.Format("dokumentumId={0}", dokumentumId));

        AttachCsatolmanyToTertiveveny(kuldemenyId, dokumentumId);

    }

    string CsatolmanyUpload(string kuldemenyId, string fileName, byte[] fileContent)
    {
        Logger.Debug(String.Format("CsatolmanyUpload: kuldemenyId={0}, fileName={1}", kuldemenyId, fileName));

        EREC_KuldKuldemenyekService erec_KuldKuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam uploadExecParam = execParam.Clone();
        uploadExecParam.Record_Id = kuldemenyId;

        EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
        erec_Csatolmanyok.KuldKuldemeny_Id = kuldemenyId;

        Csatolmany csatolmany = new Csatolmany();
        csatolmany.Nev = fileName;
        csatolmany.Tartalom = fileContent;

        Result result = erec_KuldKuldemenyekService.CsatolmanyUpload(uploadExecParam, erec_Csatolmanyok, csatolmany);

        if (result.IsError)
            throw new ResultException(result);

        string dokumentumId = result.Record as string;
        return dokumentumId;
    }

    private void AttachCsatolmanyToTertiveveny(string kimenoKuldemenyId, string dokumentumId)
    {
        Logger.Debug(String.Format("AttachCsatolmanyToTertiveveny: kimenoKuldemenyId={0}, dokumentumId={1}", kimenoKuldemenyId, dokumentumId));
        
        //csatolmanyId lekérése
        EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();

        search.KuldKuldemeny_Id.Value = kimenoKuldemenyId;
        search.KuldKuldemeny_Id.Operator = Query.Operators.equals;

        search.Dokumentum_Id.Value = dokumentumId;
        search.Dokumentum_Id.Operator = Query.Operators.equals;

        Result result = service.GetAll(execParam, search);

        if (result.IsError)
        {
            Logger.Error("Csatolmány lekérése hiba!", execParam, result);
            throw new ResultException(result);
        }

        if (result.Ds.Tables[0].Rows.Count == 0)
        {
            throw new Exception("Csatolmány nem található!");
        }


        string csatolmanyId = result.Ds.Tables[0].Rows[0]["Id"].ToString();
        Logger.Debug(String.Format("csatolmanyId: {0}", csatolmanyId));

        //csatolmány kapcsolása tértivevényhez
        EREC_Mellekletek melleklet = GetMelleklet( kimenoKuldemenyId);
        if (melleklet != null)
        {
            Logger.Debug(String.Format("melleklet: {0}", melleklet.Id));
            AddIratelemKapcsolat(melleklet.Id, csatolmanyId);
        }
    }

    private EREC_Mellekletek GetMelleklet(string kuldemenyId)
    {
        Logger.Info(String.Format("GetMelleklet kezdete: {0}", kuldemenyId));

        EREC_MellekletekSearch search = new EREC_MellekletekSearch();
        search.KuldKuldemeny_Id.Value = kuldemenyId;
        search.KuldKuldemeny_Id.Operator = Query.Operators.equals;

        search.AdathordozoTipus.Value = KodTarak.AdathordozoTipus.Tertiveveny;
        search.AdathordozoTipus.Operator = Query.Operators.equals;

        EREC_MellekletekService service = eRecordService.ServiceFactory.GetEREC_MellekletekService();

        Result res = service.GetAll(execParam, search);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Warn(String.Format("Nem található a melléklet: {0}", kuldemenyId));
            return null;
        }

        if (res.Ds.Tables[0].Rows.Count > 1)
        {
            Logger.Warn(String.Format("Több melléklet található: {0}", kuldemenyId));
            return null;
        }

        EREC_Mellekletek melleklet = new EREC_Mellekletek();
        Utility.LoadBusinessDocumentFromDataRow(melleklet, res.Ds.Tables[0].Rows[0]);

        Logger.Info(String.Format("GetMelleklet vege: {0}", kuldemenyId));

        return melleklet;
    }

    private void AddIratelemKapcsolat(string mellekletId, string csatolmanyId)
    {
        Logger.Info(String.Format("AddIratelemKapcsolat kezdete: {0}, {1}", mellekletId, csatolmanyId));

        EREC_IratelemKapcsolatokService service = eRecordService.ServiceFactory.GetEREC_IratelemKapcsolatokService();
        EREC_IratelemKapcsolatok kapcs = new EREC_IratelemKapcsolatok();
        kapcs.Csatolmany_Id = csatolmanyId;
        kapcs.Melleklet_Id = mellekletId;
        Result res = service.Insert(execParam, kapcs);


        if (res.IsError)
        {
            throw new ResultException(res);
        }

        Logger.Info(String.Format("AddIratelemKapcsolat vege: {0}, {1}", mellekletId, csatolmanyId));
    }

    ResolutionReceipt GetResolutionReceipts(string registrationNumber)
    {
        ElhitetManager manager = new ElhitetManager();
        return manager.GetResolutionReceipts(registrationNumber);
    }

    void UpdateResolutionReceipts(string registrationNumber)
    {
        ElhitetOracleDatabaseManager db = new ElhitetOracleDatabaseManager();
        db.UpdateResolutionReceipts(registrationNumber);
    }

    [WebMethod(Description = "Hivatalok betöltése az ELHISZ rendszerből.")]
    public void HivatalokFromElhisz()
    {

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();

        try
        {
            HivatalokSyncManager manager = new HivatalokSyncManager(execParam);
            manager.HivatalokFromElhisz();
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(e.Message);
        }

        log.WsEnd(execParam, result);
        return;
    }
}
