﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.HAIR;
using Contentum.eIntegrator.WebService.HAIR.PartnerSync;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
///    A(z) INT_Log táblához tartozó Web szolgáltatások.
/// </summary>
[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//[Transaction(Isolation = TransactionIsolationLevel.ReadCommitted)]
public partial class INT_HAIRAdatokService : System.Web.Services.WebService
{
    /// PartnerSync(ExecParam ExecParam)
    /// Az INT_HAIRAdatok tábla tartalmazza azokat a partnereket amiket szinkronizálni kell majd. 
    /// Ezt a WS endpointot egy job hívja a DB ből az ebből a táblából kiszedett rekordokban tárolt adoazon-okkal amik '02' es típusuak és az értékük'1'
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemeno paraméter. Az ExecParam adatai a muvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param>     
    /// <param name="adoaz">Egy string tömb ami tartalmazna az elleõrizni kívánt partnerek HAIR adoazon értékét</param>     
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adoaz tomb alapján lekerdezi az ugyfeleket a HAIR-bol és a kapott adatok alapjan frissiti a contentumban tarolt partnereket/cimeket")]
    [System.Xml.Serialization.XmlInclude(typeof(INT_HAIRAdatok))]
    public Result PartnerSync(ExecParam ExecParam, string[] adoaz)
    {
        Logger.Debug("INT_HAIRAdatokService.PartnerSync start");
        Result result = new Result();
        result.ErrorCode = "0";// 0= nincs hiba
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        var hairConnector = new ConnectorFactory().Connector;
        List<HAIRUgyfelMapped> mappedPartners;
        try
        {
            mappedPartners = new HAIRPartnerSyncer().StartSync(ExecParam, hairConnector, adoaz);
        }
        catch (HAIRCannotConnectException e)
        {
            result.ErrorCode = ErrorCodes.CannotConnectToHAIR.ToString();
            result.ErrorMessage = "Nem lehet kapcsolódni a HAIR-hoz: " + hairConnector.ServerAddress;
            return result;
        }
        //Azoknak a HAIR_Adatok rekordoknak az összegyüjtése amiknek a szinkronizálása sikerült
        var syncedPartners = new List<HAIRUgyfelMapped>();
        foreach (var mappedPartner in mappedPartners)
        {
            if (mappedPartner.ErrorResults.Count == 0)
            {
                syncedPartners.Add(mappedPartner);
            }
        }
        Logger.Debug("Successfully synced count: " + syncedPartners.Count + " from: " + mappedPartners.Count + " Successfull adoazon's:  " + syncedPartners.Select(p => p.hairUgyfel.adoaz));

        Result objTipusResult = PartnerSyncHelper.GetObjTipusFromObjectKod("KRT_Partnerek");
        KRT_ObjTipusok objTipus = new KRT_ObjTipusok();
        Utility.LoadBusinessDocumentFromDataRow(objTipus, objTipusResult.Ds.Tables[0].Rows[0]);

        foreach (var syncedPartner in syncedPartners)
        {
            Result updateOrInsertHAIRAdatokResult =
            Helper.UpdateOrInsertHAIRAdatok(syncedPartner.KRT_Partner.Id, objTipus.Id, "02", "0");
            if (!updateOrInsertHAIRAdatokResult.IsError)
            {
                continue;
            }
            Logger.Debug("Could not update INT_HAIRAdatok record from value 1 to value 0 after successfull sync");
            HAIRUgyfelMapped relatedMappedPartner = mappedPartners.First(m => m == syncedPartner);
            if (relatedMappedPartner != null)
            {
                relatedMappedPartner.ErrorResults.Add(updateOrInsertHAIRAdatokResult);
            }
        }

        string error = "\n";
        //Hibak osszegyujtese
        foreach (var item in mappedPartners)
        {
            if (item.ErrorResults.Count == 0)
            {
                continue;
            }
            string currentError = " \nAdoazon: " + item.hairUgyfel.adoaz + " :\n";
            foreach (var errorResult in item.ErrorResults)
            {
                currentError += string.Format("HibaKód:{0} Hibaüzenet{1}\n", errorResult.ErrorCode, errorResult.ErrorMessage);
            }
            error += currentError;
        }
        if (error.Length != 0)
        {
            result.ErrorMessage = error;
        }
        log.WsEnd(ExecParam, result);
        return result;
    }
}