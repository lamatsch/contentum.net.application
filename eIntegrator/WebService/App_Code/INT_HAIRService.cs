﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Services;
using System.Xml.Serialization;
using System.Web.Services.Protocols;
using Iktatas = Contentum.eIntegrator.WebService.HAIR.Iktatas;
using IratPeldanyLetrehozas = Contentum.eIntegrator.WebService.HAIR.IratPeldanyLetrehozas;
using Postazas = Contentum.eIntegrator.WebService.HAIR.Postazas;
using TertivevenyErkeztetes = Contentum.eIntegrator.WebService.HAIR.TertivevenyErkeztetes;
using CsatolmanyFeltoltes = Contentum.eIntegrator.WebService.HAIR.CsatolmanyFeltoltes;
using IratModositas = Contentum.eIntegrator.WebService.HAIR.IratModositas;
using UgyModositas = Contentum.eIntegrator.WebService.HAIR.UgyModositas;
using PartnerModositas = Contentum.eIntegrator.WebService.HAIR.PartnerModositas;
using InitialSync = Contentum.eIntegrator.WebService.HAIR.InitialSync;
using Hair = Contentum.eIntegrator.WebService.HAIR;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System.Data;
using Newtonsoft.Json;
using Contentum.eIntegrator.WebService.HAIR.IntServices;
using Contentum.eIntegrator.WebService.HAIR;
using Contentum.eDocument.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eIntegrator.Service.Helper;
using System.Threading;
using Contentum.eIntegrator.WebService.HAIR.PartnerSync;
using Contentum.eIntegrator.Service;
using System.Text;

/// <summary>
/// Summary description for INT_ONYPService
/// </summary>
[WebService(Namespace = Contentum.eIntegrator.WebService.HAIR.Constants.DEFAULT_NAMESPACE)]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class INT_HAIRService : WebService
{
    private static string ServiceURL;

    /// <summary>
    /// Ideiglenes megoldásként 1000 db folyamat futása után leállítjuk a folyamatok futását és nem küldünk több emailt, mert valószínűleg olyan lyuk van a tételek között amit a folyamat
    /// nem tud kiküszöbölni. TomegesIktatas_Init_Job_Restart metódussal lehet újraindítani.
    /// </summary>
    public static int FolyamatSzamlalo = 0;

    static INT_HAIRService()
    {
        ServiceURL = ConfigurationManager.AppSettings["HAIRServiceURL"];
        // <add key="HAIRServiceURL" value="http://ax-vfphtst03/INT_HAIRService"/>
    }

    /// <summary>
    /// Ideiglenes megoldásként 1000 db folyamat futása után leállítjuk a folyamatok futását és nem küldünk több emailt, mert valószínűleg olyan lyuk van a tételek között amit a folyamat
    /// nem tud kiküszöbölni. TomegesIktatas_Init_Job_Restart metódussal lehet újraindítani.
    /// </summary>
    [WebMethod]
    [LogExtension]
    public string TomegesIktatas_Init_Job_Restart()
    {
        FolyamatSzamlalo = 0;
        return "Újraindítva";
    }

    [LogExtension]
    [WebMethod]
    public string TomegesIktatas_Init_Job_Stop()
    {
        FolyamatSzamlalo = 2000;
        return "Leállítva";
    }
    [LogExtension]
    [WebMethod]
    public int TomegesIktatas_Init_Job_GetFolyamatSzamlaloCount()
    {
        return FolyamatSzamlalo;
    }
    [LogExtension]
    [WebMethod]
    public bool TomegesIktatas_Init_Job_EmailKuldesAllapot()
    {
        return Helper.EmailKuldesBekapcsolva;
    }
    [LogExtension]
    [WebMethod]
    public bool TomegesIktatas_Init_Job_EmailKuldesOn()
    {
        Helper.EmailKuldesBekapcsolva = true;
        return Helper.EmailKuldesBekapcsolva;
    }
    [LogExtension]
    [WebMethod]
    public bool TomegesIktatas_Init_Job_EmailKuldesOff()
    {
        Helper.EmailKuldesBekapcsolva = false;
        return Helper.EmailKuldesBekapcsolva;
    }

    #region Iktatas

    #region Helper classes

    private class FoszamosIktatoObjektum : IktatoObjektumBase
    {
        public string IktatasTipus
        {
            get
            {
                if (Tetel.IktatasTipus == "1" && Tetel.Alszamra == "0")
                    return "KIF";
                else if (Tetel.IktatasTipus == "1" && Tetel.Alszamra == "1")
                    return "KIA";
                else if (Tetel.IktatasTipus == "0" && Tetel.Alszamra == "0" && !string.IsNullOrEmpty(Tetel.Kuldemeny_Id))
                    return "BIF";
                else if (Tetel.IktatasTipus == "0" && Tetel.Alszamra == "0" && string.IsNullOrEmpty(Tetel.Kuldemeny_Id))
                    return "EIF";
                else return null;
            }
        }

        private int? _foszam;

        public override int GetFoszam()
        {
            if (_foszam.HasValue)
                return _foszam.Value;

            EREC_UgyUgyiratok ugyirat;
            try
            {
                ugyirat = JsonConvert.DeserializeObject<EREC_UgyUgyiratok>(Tetel.EREC_UgyUgyiratok);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return -1;
            }
            int f;
            if (int.TryParse(ugyirat.Foszam, out f))
            {
                _foszam = f;
                return _foszam.Value;
            }
            return -1;
        }

        public override int GetIratIrany()
        {
            if (Tetel.IktatasTipus == "1")
            {
                return 2;
            }
            else if (Tetel.IktatasTipus == "0")
            {
                return 1;
            }
            return 0;
        }

        public int? _erkeztetoSzam;

        public int GetErkeztetoszam()
        {
            if (_erkeztetoSzam.HasValue)
                return _erkeztetoSzam.Value;

            EREC_KuldKuldemenyek kuldemeny;
            try
            {
                kuldemeny = JsonConvert.DeserializeObject<EREC_KuldKuldemenyek>(Tetel.EREC_KuldKuldemenyek);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return -1;
            }
            int f;
            if (int.TryParse(kuldemeny.Erkezteto_Szam, out f))
            {
                _erkeztetoSzam = f;
                return _erkeztetoSzam.Value;
            }
            return -1;
        }

        public FoszamosIktatoObjektum()
        {
            IktatoKonyvId = "-1";
            CsoportId = -1;
        }

        public FoszamosIktatoObjektum(EREC_TomegesIktatasTetelek tetel, string folyamatId, string errorMessage)
        {
            this.Tetel = tetel;
            this.Tetel.Updated.SetValueAll(false);
            this.Tetel.Base.Updated.SetValueAll(false);

            this.Tetel.Folyamat_Id = folyamatId;
            this.Tetel.Updated.Folyamat_Id = true;

            this.Tetel.Allapot = "9";
            this.Tetel.Updated.Allapot = true;

            this.Tetel.Hiba = errorMessage;
            this.Tetel.Updated.Hiba = true;

            this.ErrorMessage = errorMessage;
        }

        public FoszamosIktatoObjektum(EREC_TomegesIktatasTetelek tetel, string folyamatId)
        {
            this.Tetel = tetel;
            this.Tetel.Updated.SetValueAll(false);
            this.Tetel.Base.Updated.SetValueAll(false);

            this.IktatoKonyvId = tetel.Iktatokonyv_Id;

            this.Tetel.Folyamat_Id = folyamatId;
            this.Tetel.Updated.Folyamat_Id = true;
        }

        public EREC_TomegesIktatasTetelek Tetel;
        private string errorMessage;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }

            set
            {
                errorMessage = value;
                if (this.Tetel != null)
                {
                    this.Tetel.Hiba = value;
                    this.Tetel.Updated.Hiba = true;

                    this.Tetel.Allapot = "9";
                    this.Tetel.Updated.Allapot = true;
                }
            }
        }
    }

    private class AlszamosIktatoObjektum : IktatoObjektumBase
    {
        public EREC_UgyUgyiratok Ugyirat;

        private int? _alszam;
        public int GetAlszam()
        {
            if (_alszam.HasValue)
                return _alszam.Value;

            EREC_IraIratok irat;
            try
            {
                irat = JsonConvert.DeserializeObject<EREC_IraIratok>(Tetel.EREC_IraIratok);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return -1;
            }
            int f;
            if (int.TryParse(irat.Alszam, out f))
            {
                _alszam = f;
                return _alszam.Value;
            }
            return -1;
        }

        public void SetFoszam(int val)
        {
            _foszam = val;
        }
        int _foszam;

        public override int GetFoszam()
        {
            return _foszam;
        }

        public override int GetIratIrany()
        {
            if (Tetel.IktatasTipus == "1")
            {
                return 2;
            }
            else if (Tetel.IktatasTipus == "0")
            {
                return 1;
            }
            return 0;
        }

        public int? _erkeztetoSzam;

        public int GetErkeztetoszam()
        {
            if (_erkeztetoSzam.HasValue)
                return _erkeztetoSzam.Value;

            EREC_KuldKuldemenyek kuldemeny;
            try
            {
                kuldemeny = JsonConvert.DeserializeObject<EREC_KuldKuldemenyek>(Tetel.EREC_KuldKuldemenyek);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return -1;
            }
            int f;
            if (int.TryParse(kuldemeny.Erkezteto_Szam, out f))
            {
                _erkeztetoSzam = f;
                return _erkeztetoSzam.Value;
            }
            return -1;
        }

        public AlszamosIktatoObjektum()
        {
            IktatoKonyvId = "-1";
            CsoportId = -1;
        }

        public AlszamosIktatoObjektum(EREC_TomegesIktatasTetelek tetel, string folyamatId, string errorMessage)
        {
            this.Tetel = tetel;
            this.Tetel.Updated.SetValueAll(false);
            this.Tetel.Base.Updated.SetValueAll(false);

            this.Tetel.Folyamat_Id = folyamatId;
            this.Tetel.Updated.Folyamat_Id = true;

            this.Tetel.Allapot = "9";
            this.Tetel.Updated.Allapot = true;

            this.Tetel.Hiba = errorMessage;
            this.Tetel.Updated.Hiba = true;

            this.ErrorMessage = errorMessage;
        }

        public AlszamosIktatoObjektum(EREC_TomegesIktatasTetelek tetel, string folyamatId)
        {
            this.IktatoKonyvId = tetel.Iktatokonyv_Id;

            this.Tetel = tetel;
            this.Tetel.Updated.SetValueAll(false);
            this.Tetel.Base.Updated.SetValueAll(false);

            this.Tetel.Folyamat_Id = folyamatId;
            this.Tetel.Updated.Folyamat_Id = true;
        }

        public EREC_TomegesIktatasTetelek Tetel;
        private string errorMessage;
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }

            set
            {
                errorMessage = value;
                if (this.Tetel != null)
                {
                    this.Tetel.Hiba = value;
                    this.Tetel.Updated.Hiba = true;

                    this.Tetel.Allapot = "9";
                    this.Tetel.Updated.Allapot = true;
                }
            }
        }
    }

    //4 típusú iktatás lehetséges HAIR esetén: 
    //IktatasTipus = 1, Alszamra = 0 --> KIF
    //IktatasTipus = 1, Alszamra = 1-- > KIA(kimenő alszámra)
    //IktatasTipus = 0, Alszamra = 0, Kuldemeny_Id is not null --> BIF
    //IktatasTipus = 0, Alszamra = 0, Kuldemeny_Id is null --> EIF
    //pl.van 10 főszámra iktatás: EIF, EIF, EIF, KIF, KIF, EIF, EIF, BIF, KIF, KIF, 
    //ebből lesz 5 csoport: [3xEIF],[2xKIF],[2xEIF],[1xBIF],[2xKIF],ezeket ebben a sorrendben kell meghívni
    //FONTOS a főszámok növekvő sorrendbe helyezése előtte
    private List<IktatoCsoport> CreateFoszamFolytonosGroups(List<FoszamosIktatoObjektum> iktatoDocuments, string kuldemenyIktatoKonyvId)
    {
        Logger.Debug("TomegesIktatas_Init_Job - CreateFoszamFolytonosGroups  START");

        List<IktatoCsoport> IktatoCsoportok = new List<IktatoCsoport>();

        var distinctIktatoKonyvIds = iktatoDocuments.Select(x => x.IktatoKonyvId).Distinct().ToList();

        //Iktatókönyvenként iterálva elkészítjül a fenti példában megadott csoportokat
        for (int j = 0; j < distinctIktatoKonyvIds.Count; j++)
        {
            var gropuediktatoDocuments = iktatoDocuments.Where(x => x.IktatoKonyvId == distinctIktatoKonyvIds[j]).ToList();

            for (int i = 0; i < gropuediktatoDocuments.Count; i++)
            {
                if (i == 0)
                {
                    IktatoCsoport csoport = new IktatoCsoport();
                    csoport.Tag = gropuediktatoDocuments[0].IktatasTipus;
                    gropuediktatoDocuments[0].CsoportId = 0;
                    csoport.Id = 0;
                    IktatoCsoportok.Add(csoport);
                }
                else
                {
                    if (gropuediktatoDocuments[i].IktatasTipus == gropuediktatoDocuments[i - 1].IktatasTipus)
                    {
                        IktatoCsoport csoport = IktatoCsoportok.First(x => x.Id == gropuediktatoDocuments[i - 1].CsoportId);
                        gropuediktatoDocuments[i].CsoportId = csoport.Id;
                    }
                    else
                    {
                        IktatoCsoport csoport = new IktatoCsoport();
                        csoport.Tag = gropuediktatoDocuments[i].IktatasTipus;
                        int csoportId = gropuediktatoDocuments[i - 1].CsoportId + 1;
                        gropuediktatoDocuments[i].CsoportId = csoportId;
                        csoport.Id = csoportId;
                        IktatoCsoportok.Add(csoport);
                    }
                }
            }
        }

        Iktatas.Valasz valasz = new Iktatas.Valasz();
        valasz.ValaszAdatok = new ValaszAdatok();


        for (int i = 0; i < IktatoCsoportok.Count; i++)
        {
            var firstInCsoport = iktatoDocuments.First(x => x.CsoportId == IktatoCsoportok[i].Id);
            IktatoCsoportok[i].iktatokonyv_Id = new Guid(firstInCsoport.IktatoKonyvId);
            IktatoCsoportok[i].iratDarabszam = iktatoDocuments.Where(x => x.CsoportId == IktatoCsoportok[i].Id).Count();
            IktatoCsoportok[i].foszam = null;
            IktatoCsoportok[i].iratIrany = firstInCsoport.GetIratIrany();

            //Csak BIF lehet küldeményazonosított
            if (firstInCsoport.IktatasTipus == "BIF")
            {
                IktatoCsoportok[i].kuldemenyAzonositott = true;

            }
            else
            {
                //string id = Hair.Helper.GetErkeztetoByKod(Hair.Helper.TECHNIKAI_ERKEZTETOKONYV_MEGKULJEL, DateTime.Now.Year, valasz).Ds.Tables[0].Rows[0]["Id"].ToString();
                IktatoCsoportok[i].erkeztetokonyv_Id = new Guid(kuldemenyIktatoKonyvId);
                //IktatoCsoportok[i].erkeztetokonyv_Id =
            }
        }

        Logger.Debug("TomegesIktatas_Init_Job - CreateFoszamFolytonosGroups  END");

        return IktatoCsoportok;

    }


    #endregion

    int prioritas = 2;
    List<FoszamosIktatoObjektum> foszamosIktatoDocuments;
    List<AlszamosIktatoObjektum> alszamosIktatoDocuments;

    private Dictionary<string, EREC_IraIktatoKonyvek> iktatoKonyvek = new Dictionary<string, EREC_IraIktatoKonyvek>();

    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod()]
    //Ütemezett jo által hívott job, ami megvizsgálja , hogy az iktatásra váró tételek között van-e lyuk, 
    //azaz pl. főszámos iktatásnál az iktatókönyvben lévő utolsó főszám az 24, viszont az EREC_TomegesIktatasTetelek lévő következő azonosító főszáma az 26.
    //Ilyenkor emailt küldünk a probléma leírásával a megadott címre.
    public void TomegesIktatas_HealthCheck_Job()
    {

        //#region Főszámos tételek vizsgálata

        //#region Lekérjük a főszámos tételeket
        //Logger.Debug("TomegesIktatas_Init_Job- Lekérjük a főszámos tételeket START");
        //var foszamosExecParam = Hair.Helper.GlobalExecParam.Clone();
        //EREC_TomegesIktatasTetelekSearch foszamosTetelekSearch = new EREC_TomegesIktatasTetelekSearch();
        //foszamosTetelekSearch.Folyamat_Id.Value = string.Empty;
        //foszamosTetelekSearch.Folyamat_Id.Operator = Contentum.eQuery.Query.Operators.isnull;
        //foszamosTetelekSearch.Alszamra.Value = "0";
        //foszamosTetelekSearch.Alszamra.Operator = Contentum.eQuery.Query.Operators.equals;
        //foszamosTetelekSearch.Allapot.Value = "1";
        //foszamosTetelekSearch.Allapot.Operator = Contentum.eQuery.Query.Operators.equals;
        //foszamosTetelekSearch.WhereByManual = " and EREC_TomegesIktatasTetelek.Note='HAIR'";
        //foszamosTetelekSearch.TopRow = 100;
        //foszamosTetelekSearch.OrderBy = " EREC_TomegesIktatasTetelek.LetrehozasIdo asc";

        //Result foszamosTetelekResult = tomegesIktatasTetelekService.GetAll(foszamosExecParam, foszamosTetelekSearch);
        //if (foszamosTetelekResult.IsError)
        //{
        //    Logger.Error("TomegesIktatas_Init_Job error.", foszamosExecParam, foszamosTetelekResult);
        //    throw new ResultException(foszamosTetelekResult);
        //}
        //Logger.Debug("TomegesIktatas_Init_Job- Lekérjük a főszámos tételeket END");
        //#endregion


        //#endregion

    }
    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod()]
    //Iktatás után hívódik egy ütemezett jobon keresztül, hogy aláírja, elintézze az iratokat, valamint feltöltse a staisztikát.
    public string TomegesIktatas_Post_Operations_Job()
    {
        EREC_TomegesIktatasTetelekService tomegesIktatasTetelekService = eRecordService.ServiceFactory.GetEREC_TomegesIktatasTetelekService();
        var tetelek = new List<EREC_TomegesIktatasTetelek>();
        var execParam = Hair.Helper.GlobalExecParam.Clone();

        try
        {
            Logger.Debug("TomegesIktatas_Post_Operations - START");

            #region Lekérjük a 2es állapotú(már iktatott), de még nem elintézett/aláírt/statisztikázott tételeket folyamattól függetlenül
            Logger.Debug("TomegesIktatas_Post_Operations- Tételek lekérése START");

            EREC_TomegesIktatasTetelekSearch tetelekSearch = new EREC_TomegesIktatasTetelekSearch();
            tetelekSearch.Allapot.Value = "2";
            tetelekSearch.Allapot.Operator = Contentum.eQuery.Query.Operators.equals;

            tetelekSearch.Lezarhato.Value = "1";
            tetelekSearch.Lezarhato.Operator = Contentum.eQuery.Query.Operators.equals;
            tetelekSearch.Lezarhato.Group = "100";
            tetelekSearch.Lezarhato.GroupOperator = Contentum.eQuery.Query.Operators.or;

            tetelekSearch.HatosagiStatAdat.Value = "";
            tetelekSearch.HatosagiStatAdat.Operator = Contentum.eQuery.Query.Operators.notnull;
            tetelekSearch.HatosagiStatAdat.Group = "100";
            tetelekSearch.HatosagiStatAdat.GroupOperator = Contentum.eQuery.Query.Operators.or;

            tetelekSearch.EREC_IratAlairok.Value = "";
            tetelekSearch.EREC_IratAlairok.Operator = Contentum.eQuery.Query.Operators.notnull;
            tetelekSearch.EREC_IratAlairok.Group = "100";
            tetelekSearch.EREC_IratAlairok.GroupOperator = Contentum.eQuery.Query.Operators.or;

            tetelekSearch.Irat_Id.Value = "";
            tetelekSearch.Irat_Id.Operator = Contentum.eQuery.Query.Operators.notnull;

            tetelekSearch.Folyamat_Id.Value = "";
            tetelekSearch.Folyamat_Id.Operator = Contentum.eQuery.Query.Operators.notnull;

            Result tetelekResult = tomegesIktatasTetelekService.GetAll(execParam, tetelekSearch);
            if (tetelekResult.IsError)
            {
                Logger.Error("TomegesIktatas_Post_Operations error.", execParam, tetelekResult);
                throw new ResultException(tetelekResult);
            }
            if (tetelekResult.GetCount < 1)
            {
                Logger.Debug("TomegesIktatas_Post_Operations - 0 találat");
                Logger.Debug("TomegesIktatas_Post_Operations - END");
            }

            Logger.Debug("TomegesIktatas_Post_Operations- Tételek lekérése END");
            #endregion

            #region Lekert Datarow tetelek konvertalasa EREC_TomegesIktatasTetelek objektumokka
            Logger.Debug("TomegesIktatas_Post_Operations- Lekert Datarow tetelek konvertalasa START");

            for (int i = 0; i < tetelekResult.Ds.Tables[0].Rows.Count; i++)
            {
                EREC_TomegesIktatasTetelek tetel = new EREC_TomegesIktatasTetelek();
                Utility.LoadBusinessDocumentFromDataRow(tetel, tetelekResult.Ds.Tables[0].Rows[i]);
                tetel.Updated.SetValueAll(false);
                tetel.Allapot = "3";
                tetel.Updated.Allapot = true;
                tetel.Base.Updated.Ver = true;
                tetelek.Add(tetel);
            }
            Logger.Debug("TomegesIktatas_Post_Operations- Lekert Datarow tetelek konvertalasa END");
            #endregion

            #region Hatósági statisztika
            Logger.Debug("TomegesIktatas_Post_Operations- Statisztika START");

            var statisztikaTetelek = tetelek.Where(x => !string.IsNullOrEmpty(x.HatosagiStatAdat)).ToList();

            for (int i = 0; i < statisztikaTetelek.Count(); i++)
            {
                try
                {
                    Iktatas.Valasz valasz = new Iktatas.Valasz();
                    valasz.ValaszAdatok = new ValaszAdatok();

                    EREC_IraIratok irat = Helper.GetIrat(statisztikaTetelek[i].Irat_Id, valasz.ValaszAdatok);
                    if (irat == null)
                        continue;

                    StatisztikaElem[] statisztikaElem = JsonConvert.DeserializeObject<StatisztikaElem[]>(statisztikaTetelek[i].HatosagiStatAdat);
                    Hair.Helper.InsertObjektumTargyszavak(statisztikaTetelek[i].Irat_Id, irat.Ugy_Fajtaja, statisztikaElem, valasz.ValaszAdatok);
                    if (Hair.Helper.IsError(valasz))
                    {
                        statisztikaTetelek[i].Hiba += "  Statisztika  hiba:" + valasz.ValaszAdatok.HibaUzenet;
                        statisztikaTetelek[i].Updated.Hiba = true;
                    }
                }
                catch (Exception ex)
                {
                    statisztikaTetelek[i].Hiba += "  Statisztika  hiba:" + ex.Message;
                    statisztikaTetelek[i].Updated.Hiba = true;
                }
            }
            Logger.Debug("TomegesIktatas_Post_Operations- Statisztika END");
            #endregion

            var tomegesIktatasHelper = GetTomegesIktatasHelper(tetelek);
            //var postOperationErrors = 
            tomegesIktatasHelper.PostOperations();
        }
        catch (Exception ex)
        {
            Logger.Error("TomegesIktatas_Post_Operations error.", ex);
        }

        for (int i = 0; i < tetelek.Count(); i++)
        {
            execParam.Record_Id = tetelek[i].Id;
            tomegesIktatasTetelekService.Update(execParam, tetelek[i]);
        }
        return "OK";
    }

    private TomegesIktatasHelper GetTomegesIktatasHelper(IEnumerable<EREC_TomegesIktatasTetelek> tetelek)
    {
        return new TomegesIktatasHelper(Helper.GlobalExecParam.Clone(), tetelek);
    }

    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod()]
    //A küldött tételek ellenőrzése főszám/alszám szerint, majd folyamathoz rögzítése a tömeges iktatáshoz
    public string TomegesIktatas_Init_Job()
    {
        TomegesIktatasHelper tomegesIktatasHelper = null;
        try
        {
            /// <summary>
            /// Ideiglenes megoldásként 1000 db folyamat futása után leállítjuk a folyamatok futását és nem küldünk több emailt, mert valószínűleg olyan lyuk van a tételek között amit a folyamat
            /// nem tud kiküszöbölni.
            /// </summary>
            if (FolyamatSzamlalo >= 1000)
                return "Folyamatok száma elérte az 1000-et. Újraindítás: TomegesIktatas_Init_Job_Restart metódussal";

            FolyamatSzamlalo += 1;

            int inicializaltTetelekCount = 0;
            StringBuilder errorMail = new StringBuilder();

            #region Lekérjük a technikai küldemény érkeztetőkönyvet, ha nincs akkor inicializálás megállítva

            Iktatas.Valasz v = new Iktatas.Valasz();
            v.ValaszAdatok = new ValaszAdatok();
            Result technikaiKuldemenyIktatoKonyvtResult = Hair.Helper.GetErkeztetoByKod(Hair.Helper.TECHNIKAI_ERKEZTETOKONYV_MEGKULJEL, DateTime.Now.Year, v);
            if (v.IsError())
            {
                throw new Exception(v.ValaszAdatok.HibaUzenet);
            }

            if (technikaiKuldemenyIktatoKonyvtResult.GetCount < 1)
            {
                throw new Exception("Nem található a technikai küldemény érkeztetőkönyv");
            }

            int utolsoKuldemenyErkeztetoSzam = -10;

            //A küldemény érkeztetőszámok sorrendje
            //List<int> kuldemenyErkeztetoSzamok = new List<int>();
            EREC_IraIktatoKonyvek technikaiKuldemenyiktatoKonyv = new EREC_IraIktatoKonyvek();
            Utility.LoadBusinessDocumentFromDataRow(technikaiKuldemenyiktatoKonyv, technikaiKuldemenyIktatoKonyvtResult.Ds.Tables[0].Rows[0]);

            Logger.Debug("TomegesIktatas_Init_Job - technikaiKuldemenyiktatoKonyv.Id:" + technikaiKuldemenyiktatoKonyv.Id.ToString());

            if (!int.TryParse(technikaiKuldemenyiktatoKonyv.UtolsoFoszam, out utolsoKuldemenyErkeztetoSzam))
            {
                string err = "A technikai iktatoKonyv UtolsoFoszam értéke hibás. IktatokonyvId: " + technikaiKuldemenyiktatoKonyv.Id;
                Logger.Error(err);
                throw new Exception(err);
            }

            Logger.Debug("TomegesIktatas_Init_Job - technikaiKuldemenyiktatoKonyv.UtolsoFoszam:" + technikaiKuldemenyiktatoKonyv.UtolsoFoszam.ToString());

            #endregion

            #region Ellenőrizzük, hogy van-e inicializálás alatt lévő folyamat, ha van kilépünk

            Result result = new Result();

            EREC_TomegesIktatasFolyamatService folyamatokService = eRecordService.ServiceFactory.GetEREC_TomegesIktatasFolyamatService();
            EREC_TomegesIktatasFolyamatSearch folyamatokSearch = new EREC_TomegesIktatasFolyamatSearch();
            folyamatokSearch.Allapot.Filter(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.InicializalasFolyamatban);
            folyamatokSearch.Forras.Filter(TomegesIktatasFolyamat.Forras_HAIR);

            Result folyamatokResult = folyamatokService.GetAll(Helper.GlobalExecParam.Clone(), folyamatokSearch);
            folyamatokResult.CheckError();

            if (folyamatokResult.GetCount > 0)
            {
                string res = "Fut inicializálás alatt lévő folyamat";
                Logger.Debug(res);
                return res;
            }

            #endregion            

            #region Folyamat Insert
            Logger.Debug("TomegesIktatas_Init_Job - Folyamat insert START");

            tomegesIktatasHelper = GetTomegesIktatasHelper(null);
            EREC_IraIratokService foszamosIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            EREC_TomegesIktatasTetelekService tomegesIktatasTetelekService = eRecordService.ServiceFactory.GetEREC_TomegesIktatasTetelekService();
            ExecParam execParam_Folyamat = Hair.Helper.GlobalExecParam.Clone();

            Result folyamatInsert = tomegesIktatasHelper.Folyamat.Insert(execParam_Folyamat, TomegesIktatasFolyamat.Forras_HAIR, prioritas, null);

            if (folyamatInsert.IsError)
            {
                Logger.Error("TomegesIktatas_Init_Job error", execParam_Folyamat, folyamatInsert);
                throw new ResultException(folyamatInsert);
            }

            var folyamatId = tomegesIktatasHelper.Folyamat.Id;
            Logger.Debug("TomegesIktatas_Init_Job - Folyamat insert END");
            #endregion            

            #region Főszámos tételek kezelése
            Logger.Debug("TomegesIktatas_Init_Job - Főszámos tételek kezelése START");

            foszamosIktatoDocuments = new List<FoszamosIktatoObjektum>();

            #region Lekérjük a főszámos tételeket
            Logger.Debug("TomegesIktatas_Init_Job - Lekérjük a főszámos tételeket START");
            var foszamosExecParam = Hair.Helper.GlobalExecParam.Clone();
            EREC_TomegesIktatasTetelekSearch foszamosTetelekSearch = new EREC_TomegesIktatasTetelekSearch();
            foszamosTetelekSearch.Folyamat_Id.Value = string.Empty;
            foszamosTetelekSearch.Folyamat_Id.Operator = Contentum.eQuery.Query.Operators.isnull;
            foszamosTetelekSearch.Alszamra.Value = "0";
            foszamosTetelekSearch.Alszamra.Operator = Contentum.eQuery.Query.Operators.equals;
            foszamosTetelekSearch.Allapot.Value = "1";
            foszamosTetelekSearch.Allapot.Operator = Contentum.eQuery.Query.Operators.equals;
            foszamosTetelekSearch.WhereByManual = " and EREC_TomegesIktatasTetelek.Note like '%HAIR%'";
            foszamosTetelekSearch.TopRow = 100;
            foszamosTetelekSearch.OrderBy = " EREC_TomegesIktatasTetelek.Note asc";

            Result foszamosTetelekResult = tomegesIktatasTetelekService.GetAll(foszamosExecParam, foszamosTetelekSearch);
            if (foszamosTetelekResult.IsError)
            {
                tomegesIktatasHelper.Folyamat.SetAllapot(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.InicializalasiHiba);
                Logger.Error("TomegesIktatas_Init_Job error.", foszamosExecParam, foszamosTetelekResult);
                throw new ResultException(foszamosTetelekResult);
            }
            Logger.Debug("TomegesIktatas_Init_Job - Lekérjük a főszámos tételeket END");
            #endregion

            Logger.Debug("TomegesIktatas_Init_Job - Tételek száma:" + foszamosTetelekResult.Ds.Tables[0].Rows.Count.ToString());

            #region Lekert Datarow tetelek konvertalasa EREC_TomegesIktatasTetelek objektumokka
            Logger.Debug("TomegesIktatas_Init_Job - Lekert Datarow tetelek konvertalasa START");
            List<EREC_TomegesIktatasTetelek> foszamosTetelek = new List<EREC_TomegesIktatasTetelek>();

            for (int i = 0; i < foszamosTetelekResult.Ds.Tables[0].Rows.Count; i++)
            {
                EREC_TomegesIktatasTetelek tetel = new EREC_TomegesIktatasTetelek();
                Utility.LoadBusinessDocumentFromDataRow(tetel, foszamosTetelekResult.Ds.Tables[0].Rows[i]);
                foszamosIktatoDocuments.Add(new FoszamosIktatoObjektum(tetel, folyamatId));
            }
            Logger.Debug("TomegesIktatas_Init_Job - Lekert Datarow tetelek konvertalasa END");
            #endregion

            #region Iktatokonyv es aztan Foszam szerinti rendezes
            Logger.Debug("TomegesIktatas_Init_Job - Iktatokonyv es aztan Foszam szerinti rendezes START");
            foszamosIktatoDocuments = foszamosIktatoDocuments.OrderBy(x => x.IktatoKonyvId).ThenBy(x => x.GetFoszam()).ToList();

            //Iktatókönyvenként készítünk egy főszámlistát
            Dictionary<string, List<FoszamosIktatoObjektum>> foszamosIktatoDocumentsOrderedByIktatoKonyv = new Dictionary<string, List<FoszamosIktatoObjektum>>();

            for (int i = 0; i < foszamosIktatoDocuments.Count; i++)
            {
                if (foszamosIktatoDocumentsOrderedByIktatoKonyv.Any(x => x.Key == foszamosIktatoDocuments[i].IktatoKonyvId))
                {
                    List<FoszamosIktatoObjektum> list = foszamosIktatoDocumentsOrderedByIktatoKonyv.First(x => x.Key == foszamosIktatoDocuments[i].IktatoKonyvId).Value;
                    list.Add(foszamosIktatoDocuments[i]);
                }
                else
                {
                    List<FoszamosIktatoObjektum> list = new List<FoszamosIktatoObjektum>();
                    list.Add(foszamosIktatoDocuments[i]);
                    foszamosIktatoDocumentsOrderedByIktatoKonyv.Add(foszamosIktatoDocuments[i].IktatoKonyvId, list);
                }
            }

            Logger.Debug("TomegesIktatas_Init_Job - foszamosIktatoDocumentsOrderedByIktatoKonyv.Count():" + foszamosIktatoDocumentsOrderedByIktatoKonyv.Count().ToString());

            Logger.Debug("TomegesIktatas_Init_Job - Iktatokonyv es aztan Foszam szerinti rendezes END");
            #endregion

            #region Iktatokonyvonkent iteralunk és elvégezzük az iktatható tételek kiválasztását, groupoljuk, meghívjuk az iratservice initet, majd visszaupdateljuk az Ugyirat_Id, Irat_Id, Kuldemenyek_Id

            foreach (var iktatokonyvId in foszamosIktatoDocumentsOrderedByIktatoKonyv)
            {
                #region Elso foszam és érkeztetőszám ellenőrzése, hogy ők következnek-e az iktatókönyveikben
                Logger.Debug("TomegesIktatas_Init_Job - Elso foszam és érkeztetőszám ellenőrzése START");
                List<FoszamosIktatoObjektum> foszamosOsszesIktatoObjektumok = foszamosIktatoDocumentsOrderedByIktatoKonyv[iktatokonyvId.Key];

                var firstTetel = foszamosOsszesIktatoObjektumok.First();

                Logger.Debug("TomegesIktatas_Init_Job - firstTetel.Tetel.Azonosito:" + firstTetel.Tetel.Azonosito.ToString());

                Iktatas.Valasz valasz = new Iktatas.Valasz();
                valasz.ValaszAdatok = new ValaszAdatok();
                EREC_IraIktatoKonyvek iktatoKonyv = Hair.Helper.GetIktatoKonyvWithoutCaching(firstTetel.IktatoKonyvId, iktatoKonyvek, valasz);
                if (valasz.IsError())
                {
                    Logger.Error("Hiba az iktatókönyv lekérése során: " + valasz.ValaszAdatok.HibaUzenet);
                    throw new Exception(valasz.ValaszAdatok.HibaUzenet);
                }

                int utolsoFoszam;
                if (!int.TryParse(iktatoKonyv.UtolsoFoszam, out utolsoFoszam))
                {
                    string err = "Cannot parse iktatoKonyv.UtolsoFoszam. IktatokonyvId: " + firstTetel.IktatoKonyvId;
                    Logger.Error(err);
                    throw new Exception(err);
                }

                Logger.Debug("TomegesIktatas_Init_Job - iktatoKonyv.UtolsoFoszam:" + iktatoKonyv.UtolsoFoszam.ToString());

                //Ha az iktatókönyvben már a legelső tétel főszáma sem a soron következő, akkor email küldés
                if (utolsoFoszam + 1 != firstTetel.GetFoszam())
                {
                    errorMail.AppendLine("Hiba a technikai iktatás során. Az utolsó főszám : " + utolsoFoszam
                       + ". Küldött főszám: " + firstTetel.GetFoszam().ToString());
                    continue;
                }

                //FoszamosIktatoObjektum firstFoszamosKuldemeny = null;

                //Ha a küldemény iktatókönyvben már a legelső küldeményes tétel főszáma sem a soron következő, akkor email küldés
                //if (foszamosOsszesIktatoObjektum.Any(x => !string.IsNullOrEmpty(x.Tetel.EREC_KuldKuldemenyek)))
                //{
                //    firstFoszamosKuldemeny = foszamosOsszesIktatoObjektum.First(x => !string.IsNullOrEmpty(x.Tetel.EREC_KuldKuldemenyek));

                //    if (firstFoszamosKuldemeny.GetErkeztetoszam() - utolsoKuldemenyErkeztetoSzam != 1)
                //    {
                //        Hair.Helper.SendErrorMail("Hiba a technikai iktatás során. A soron következő érkeztetőszám értéke nem egyezik meg az első tétel érkeztetőszámával. Soron következő: " +
                //            (utolsoKuldemenyErkeztetoSzam + 1).ToString() + ". Küldött érkeztetőszám: " + firstFoszamosKuldemeny.GetErkeztetoszam().ToString());
                //        continue;
                //    }
                //}
                Logger.Debug("TomegesIktatas_Init_Job - Elso foszam és érkeztetőszám ellenőrzése END");
                #endregion

                #region Végigmegyünk a főszámokon és kiszedjük a tételeket addig amíg folytonosak főszám szerint és küldemény érkeztetőszám szerint
                Logger.Debug("TomegesIktatas_Init_Job - kiszedjük a tételeket addig amíg folytonosak START");

                //Főszám szerinti sorrendben leválogatott tételek
                List<FoszamosIktatoObjektum> foszamosFoszamSzerintIktathatoIktatoObjektumok = new List<FoszamosIktatoObjektum>();

                //Főszám és aztán küldemény sorrend alapján leválogatott tételek
                //List<FoszamosIktatoObjektum> foszamosFoszamEsKuldemenySzerintIktathatoIktatoObjektumok = new List<FoszamosIktatoObjektum>();                

                //Főszámos válogatás
                for (int i = 0; i < foszamosOsszesIktatoObjektumok.Count; i++)
                {
                    var currentFoszamosIktatoObjektum = foszamosOsszesIktatoObjektumok[i];
                    if (i == 0)//az első elemet mindenképpen iktatjuk
                    {
                        foszamosFoszamSzerintIktathatoIktatoObjektumok.Add(currentFoszamosIktatoObjektum);
                        continue;
                    }
                    var previousFoszamosIktathatoObjektum = foszamosOsszesIktatoObjektumok[i - 1];

                    if (previousFoszamosIktathatoObjektum.GetFoszam() + 1 != currentFoszamosIktatoObjektum.GetFoszam())
                    {
                        Logger.Debug("A következő helyes főszámok: "
                            + string.Join(",", foszamosFoszamSzerintIktathatoIktatoObjektumok.Select(x => x.GetFoszam().ToString()).ToArray())
                            + " után a következő helytelen elem érkezett: " + currentFoszamosIktatoObjektum.GetFoszam());
                        break;
                    }
                    foszamosFoszamSzerintIktathatoIktatoObjektumok.Add(currentFoszamosIktatoObjektum);
                }

                Logger.Debug("TomegesIktatas_Init_Job - foszamosFoszamSzerintIktathatoIktatoObjektumok.Count():" + foszamosFoszamSzerintIktathatoIktatoObjektumok.Count().ToString());

                //A már főszám szerint leválogatott tételek küldemény főszám szerinti leválogatása
                //for (int i = 0; i < foszamosFoszamSzerintIktathatoIktatoObjektumok.Count; i++)
                //{
                //    if (!string.IsNullOrEmpty(foszamosFoszamSzerintIktathatoIktatoObjektumok[i].Tetel.EREC_KuldKuldemenyek))
                //    {
                //        if (firstFoszamosKuldemeny.Tetel.Id == foszamosFoszamSzerintIktathatoIktatoObjektumok[i].Tetel.Id)
                //        {
                //            kuldemenyErkeztetoSzamok.Add(foszamosFoszamSzerintIktathatoIktatoObjektumok[i].GetErkeztetoszam());
                //            foszamosFoszamEsKuldemenySzerintIktathatoIktatoObjektumok.Add(foszamosFoszamSzerintIktathatoIktatoObjektumok[i]);
                //        }
                //        else
                //        {
                //            int lastErkeztetoSzam = kuldemenyErkeztetoSzamok.Last();

                //            if (foszamosFoszamSzerintIktathatoIktatoObjektumok[i].GetErkeztetoszam() - lastErkeztetoSzam != 1)
                //            {
                //                break;
                //            }
                //            else
                //            {
                //                kuldemenyErkeztetoSzamok.Add(foszamosFoszamSzerintIktathatoIktatoObjektumok[i].GetErkeztetoszam());
                //                foszamosFoszamEsKuldemenySzerintIktathatoIktatoObjektumok.Add(foszamosFoszamSzerintIktathatoIktatoObjektumok[i]);
                //            }
                //        }
                //    }
                //}


                Logger.Debug("TomegesIktatas_Init_Job - kiszedjük a tételeket addig amíg folytonosak END");
                #endregion

                if (foszamosFoszamSzerintIktathatoIktatoObjektumok.Count > 0)
                {
                    #region Groupolás és Init hívása 
                    Logger.Debug("TomegesIktatas_Init_Job - Groupolás és Init hívása  START");
                    tomegesIktatasHelper.IktatoCsoportok = CreateFoszamFolytonosGroups(foszamosFoszamSzerintIktathatoIktatoObjektumok, technikaiKuldemenyiktatoKonyv.Id);
                    //_helper.CreateGroups(foszamosIktathatoIktatoObjektumok.Cast<IktatoObjektumBase>());

                    Logger.Debug("TomegesIktatas_Init_Job - foszamosFoszamSzerintIktathatoIktatoObjektumok.Count():" + tomegesIktatasHelper.IktatoCsoportok.Count().ToString());

                    tomegesIktatasHelper.CallTomegesIktatasInitForGroupsSync();

                    Logger.Debug("TomegesIktatas_Init_Job - Groupolás és Init hívása  END");
                    #endregion

                    #region Updatelj csoportjuk szerint az  Ugyirat_Id, Irat_Id, Kuldemenyek_Id értékekkel a tételeket
                    Logger.Debug("TomegesIktatas_Init_Job - Tetelek Update  START");
                    for (int i = 0; i < foszamosFoszamSzerintIktathatoIktatoObjektumok.Count(); i++)
                    {
                        IktatoCsoport csoport = tomegesIktatasHelper.IktatoCsoportok.FirstOrDefault(x => x.Id == foszamosFoszamSzerintIktathatoIktatoObjektumok[i].CsoportId);
                        Result updateResult;

                        EREC_TomegesIktatasTetelek tetel = foszamosFoszamSzerintIktathatoIktatoObjektumok[i].Tetel;
                        //tetel.Updated.SetValueAll(false);

                        if (!string.IsNullOrEmpty(foszamosFoszamSzerintIktathatoIktatoObjektumok[i].ErrorMessage))
                        {
                            var updateExecParam = Hair.Helper.GlobalExecParam.Clone();
                            updateExecParam.Record_Id = tetel.Id;

                            tetel.Hiba = foszamosFoszamSzerintIktathatoIktatoObjektumok[i].ErrorMessage;
                            tetel.Updated.Hiba = true;
                            tetel.Allapot = "9";
                            tetel.Updated.Allapot = true;
                            //tetel.Base.Ver = (int.Parse(tetel.Base.Ver) + 1).ToString();
                            tetel.Base.Updated.Ver = true;
                            updateResult = tomegesIktatasTetelekService.Update(foszamosExecParam, tetel);
                        }
                        else if (csoport != null)
                        {
                            if (null != csoport.kapottIratId && csoport.kapottAlszam != null)
                            {
                                int foszam;
                                string alszam;
                                int index;
                                Guid iratId;
                                Guid? ugyiratId;
                                Guid? kuldemenyId;
                                string azonosito;

                                index = Array.FindIndex(csoport.kapottFoszam, x => x == foszamosFoszamSzerintIktathatoIktatoObjektumok[i].GetFoszam());
                                if (index > -1)
                                {
                                    foszam = csoport.kapottFoszam[index];
                                    alszam = csoport.kapottAlszam[index];
                                    iratId = csoport.kapottIratId[index];
                                    azonosito = csoport.kapottAzonosito[index];
                                    ugyiratId = csoport.kapottUgyiratId[index];
                                    kuldemenyId = csoport.kapottKuldemenyId[index];

                                    csoport.kapottAlszam = Helper.Remove(csoport.kapottAlszam.ToList(), index);
                                    csoport.kapottIratId = Helper.Remove(csoport.kapottIratId.ToList(), index);
                                    csoport.kapottAzonosito = Helper.Remove(csoport.kapottAzonosito.ToList(), index);
                                    csoport.kapottFoszam = Helper.Remove(csoport.kapottFoszam.ToList(), index);
                                    csoport.kapottUgyiratId = Helper.Remove(csoport.kapottUgyiratId.ToList(), index);
                                    csoport.kapottKuldemenyId = Helper.Remove(csoport.kapottKuldemenyId.ToList(), index);

                                    var updateExecParam = Hair.Helper.GlobalExecParam.Clone();
                                    updateExecParam.Record_Id = tetel.Id;

                                    tetel.Irat_Id = iratId.ToString();
                                    tetel.Updated.Irat_Id = true;

                                    if (ugyiratId.HasValue && ugyiratId.Value != Guid.Empty)
                                    {
                                        tetel.Ugyirat_Id = ugyiratId.Value.ToString();
                                        tetel.Updated.Ugyirat_Id = true;
                                    }

                                    if (kuldemenyId.HasValue && kuldemenyId.Value != Guid.Empty)
                                    {
                                        tetel.Kuldemeny_Id = kuldemenyId.Value.ToString();
                                        tetel.Updated.Kuldemeny_Id = true;
                                    }
                                    //tetel.Base.Ver = (int.Parse(tetel.Base.Ver) + 1).ToString();
                                    tetel.Base.Updated.Ver = true;
                                    Logger.Debug("tetel.Id: " + tetel.Id + "tomegesIktatasHelper.folyamat_id: " + tomegesIktatasHelper.Folyamat.Id + " tetel.folyamat_id: " + tetel.Folyamat_Id);
                                    updateResult = tomegesIktatasTetelekService.Update(updateExecParam, tetel);
                                    inicializaltTetelekCount += 1;
                                }
                            }
                        }

                    }
                    Logger.Debug("TomegesIktatas_Init_Job - Tetelek Update END");
                    #endregion
                }
                else
                {
                    Logger.Debug("TomegesIktatas_Init_Job foszamosFoszamSzerintIktathatoIktatoObjektumok-ból nincs egy se");
                }
            }

            #endregion

            Logger.Debug("TomegesIktatas_Init_Job - Főszámos tételek kezelése END");
            #endregion

            #region Alszámos tételek kezelése
            Logger.Debug("TomegesIktatas_Init_Job - Alszámos tételek kezelése START");

            #region Lekérjük az alszámos tételeket
            Logger.Debug("TomegesIktatas_Init_Job - Lekérjük az alszámos tételeket START");
            var alszamosexecParam = Hair.Helper.GlobalExecParam.Clone();
            EREC_TomegesIktatasTetelekSearch alszamosSearch = new EREC_TomegesIktatasTetelekSearch();
            alszamosSearch.Folyamat_Id.Value = string.Empty;
            alszamosSearch.Folyamat_Id.Operator = Contentum.eQuery.Query.Operators.isnull;
            alszamosSearch.Alszamra.Value = "1";
            alszamosSearch.Alszamra.Operator = Contentum.eQuery.Query.Operators.equals;
            alszamosSearch.Allapot.Value = "1";
            alszamosSearch.Allapot.Operator = Contentum.eQuery.Query.Operators.equals;
            alszamosSearch.WhereByManual = " and EREC_TomegesIktatasTetelek.Note like '%HAIR%'";
            alszamosSearch.TopRow = 100;
            alszamosSearch.OrderBy = " EREC_TomegesIktatasTetelek.Note asc";

            Result alszamosResult = tomegesIktatasTetelekService.GetAll(alszamosexecParam, alszamosSearch);
            if (alszamosResult.IsError)
            {
                //Bár nem tudtuk lekérni az alszámos tételeket, a főszámosak már inicializálódtak, ezért a folyamatot is
                tomegesIktatasHelper.Folyamat.SetAllapot(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.Inicializalt);
                Logger.Error("TomegesIktatas_Init_Job error.", alszamosexecParam, alszamosResult);
                throw new ResultException(alszamosResult);
            }

            Logger.Debug("TomegesIktatas_Init_Job - alszamosResult.Count:" + alszamosResult.Ds.Tables[0].Rows.Count.ToString());

            Logger.Debug("TomegesIktatas_Init_Job - Lekérjük az alszámos tételeket END");
            #endregion

            #region Lekert Datarow tetelek konvertalasa EREC_TomegesIktatasTetelek objektumokka
            Logger.Debug("TomegesIktatas_Init_Job - Lekert Datarow tetelek konvertalasa START");
            alszamosIktatoDocuments = new List<AlszamosIktatoObjektum>();

            for (int i = 0; i < alszamosResult.Ds.Tables[0].Rows.Count; i++)
            {
                EREC_TomegesIktatasTetelek tetel = new EREC_TomegesIktatasTetelek();
                Utility.LoadBusinessDocumentFromDataRow(tetel, alszamosResult.Ds.Tables[0].Rows[i]);
                alszamosIktatoDocuments.Add(new AlszamosIktatoObjektum(tetel, folyamatId));
            }

            Logger.Debug("TomegesIktatas_Init_Job - alszamosIktatoDocuments.Count:" + alszamosIktatoDocuments.Count.ToString());

            Logger.Debug("TomegesIktatas_Init_Job - Lekert Datarow tetelek konvertalasa END");
            #endregion

            #region Iktatokonyv es aztan Foszam szerinti rendezes       
            Logger.Debug("TomegesIktatas_Init_Job - Iktatokonyv es aztan Foszam szerinti rendezes START");
            List<AlszamosIktatoObjektum> filteredAlszamosIktatoDocuments = new List<AlszamosIktatoObjektum>();

            #region Lehetnek olyan alszamosok amelyek létrehozásuk időpontjában még nem létező főszámra lettek felvéve. Itt EREC_UgyUgyiratok mezőből is megpróbájuk kiolvasni az ügyirat azonosítót

            Logger.Debug("TomegesIktatas_Init_Job - nem létező főszámos szűrés START");

            //kigyüjtjük az ugyiratIdkat és ugyirat azonosítokat, hogy egyben tudjuk lekérdezni őket igy csökkentve a db lekérdezések számát
            var ugyiratIdList = new List<string>();
            var ugyiratAzonositoList = new List<string>();
            for (int i = 0; i < alszamosIktatoDocuments.Count; i++)
            {
                string ugyiratId = alszamosIktatoDocuments[i].Tetel.Ugyirat_Id;
                if (!string.IsNullOrEmpty(ugyiratId))
                {
                    ugyiratIdList.Add(ugyiratId);
                    continue;
                }
                //Lehet benne azonosito vagy egy serializált ugyirat is jsonként ezért szürünk a { kezdésre mert a json nem kell
                var EREC_UgyUgyiratok = alszamosIktatoDocuments[i].Tetel.EREC_UgyUgyiratok;
                if (!string.IsNullOrEmpty(EREC_UgyUgyiratok) && !EREC_UgyUgyiratok.StartsWith("{"))
                {
                    string azonosito = EREC_UgyUgyiratok;
                    ugyiratAzonositoList.Add(azonosito);
                    continue;
                }
            }

            var ugyiratokFromIdList = Hair.Helper.GetUgyiratokById(ugyiratIdList);
            var ugyiratokFromAzonositoList = Hair.Helper.GetUgyiratokByAzonosito(ugyiratAzonositoList);

            var allUgyiratokList = new List<EREC_UgyUgyiratok>();
            allUgyiratokList.AddRange(ugyiratokFromIdList);
            allUgyiratokList.AddRange(ugyiratokFromAzonositoList);
            Logger.Debug("allUgyiratokList. Count before duplicate removal: " + allUgyiratokList.Count);
            allUgyiratokList = allUgyiratokList.GroupBy(u => u.Id).Select(groupt => groupt.First()).ToList(); // duplikátumok kiszűrése
            Logger.Debug("allUgyiratokList. Count after duplicate removal: " + allUgyiratokList.Count);

            Logger.Debug("ugyiratokFromIdList.Count: " + ugyiratokFromIdList.Count + " Content: " + string.Join(",", ugyiratokFromIdList.Select(u => u.Azonosito).ToArray()));
            Logger.Debug("ugyiratokFromAzonositoList.Count: " + ugyiratokFromAzonositoList.Count + " Content: " + string.Join(",", ugyiratokFromAzonositoList.Select(u => u.Azonosito).ToArray()));

            for (int i = 0; i < alszamosIktatoDocuments.Count; i++)
            {
                var currentAlszamosIktatoDocument = alszamosIktatoDocuments[i];
                var ugyirat = allUgyiratokList.Where(u => u.Id == currentAlszamosIktatoDocument.Tetel.Ugyirat_Id).SingleOrDefault();
                if (ugyirat == null)
                {
                    ugyirat = allUgyiratokList.Where(u => u.Azonosito == currentAlszamosIktatoDocument.Tetel.EREC_UgyUgyiratok).SingleOrDefault();
                }
                if (ugyirat == null)
                {
                    Logger.Debug("Nincs olyan ugyirat amelynek az Ugyirat_Id értéke = " + currentAlszamosIktatoDocument.Tetel.Ugyirat_Id
                        + " vagy a Tetel.EREC_Ugyiratok = " + currentAlszamosIktatoDocument.Tetel.EREC_UgyUgyiratok);
                    continue;
                }
                currentAlszamosIktatoDocument.Tetel.EREC_UgyUgyiratok = null;
                currentAlszamosIktatoDocument.Tetel.Updated.EREC_UgyUgyiratok = true;
                currentAlszamosIktatoDocument.SetFoszam(int.Parse(ugyirat.Foszam));
                currentAlszamosIktatoDocument.Ugyirat = ugyirat;
                filteredAlszamosIktatoDocuments.Add(currentAlszamosIktatoDocument);
            }

            Logger.Debug("TomegesIktatas_Init_Job - nem létező főszámos szűrés END");

            #endregion
            alszamosIktatoDocuments = filteredAlszamosIktatoDocuments;

            #region Rendezés iktatókönyv, főszám és alszám szerint

            Logger.Debug("TomegesIktatas_Init_Job - Iktatokonyv,Foszam order START");

            alszamosIktatoDocuments = alszamosIktatoDocuments.OrderBy(x => x.IktatoKonyvId).ThenBy(x => x.GetFoszam()).ThenBy(x => x.GetAlszam()).ToList();

            Logger.Debug("TomegesIktatas_Init_Job - Iktatokonyv,Foszam order END");


            //Iktatókönyvenként csinálunk egy főszámos listát

            Logger.Debug("TomegesIktatas_Init_Job - Iktatókönyvenként csinálunk egy főszámos listát START");
            Dictionary<string, List<AlszamosIktatoObjektum>> alszamosIktatoDocumentsOrderedByIktatoKonyv = new Dictionary<string, List<AlszamosIktatoObjektum>>();

            for (int i = 0; i < alszamosIktatoDocuments.Count; i++)
            {
                if (alszamosIktatoDocumentsOrderedByIktatoKonyv.Any(x => x.Key == alszamosIktatoDocuments[i].IktatoKonyvId))
                {
                    List<AlszamosIktatoObjektum> list = alszamosIktatoDocumentsOrderedByIktatoKonyv.First(x => x.Key == alszamosIktatoDocuments[i].IktatoKonyvId).Value;
                    list.Add(alszamosIktatoDocuments[i]);
                }
                else
                {
                    List<AlszamosIktatoObjektum> list = new List<AlszamosIktatoObjektum>();
                    list.Add(alszamosIktatoDocuments[i]);
                    alszamosIktatoDocumentsOrderedByIktatoKonyv.Add(alszamosIktatoDocuments[i].IktatoKonyvId, list);
                }
            }

            Logger.Debug("TomegesIktatas_Init_Job - Iktatókönyvenként csinálunk egy főszámos listát END");
            #endregion
            Logger.Debug("TomegesIktatas_Init_Job - Iktatokonyv es aztan Foszam szerinti rendezes END");
            #endregion

            #region Iktatokonyvonkent iteralunk és elvégezzük az iktatható tételek kiválasztását, groupoljuk, meghívjuk az iratservice initet, majd visszaupdateljuk az Ugyirat_Id, Irat_Id, Kuldemenyek_Id

            #region Végigmegyünk a főszámokon és kiszedjük a tételeket addig amíg folytonosak alszám szerint és ha van küldemény, akkor érkeztetőszám szerint is
            Logger.Debug("TomegesIktatas_Init_Job - Végigmegyünk a főszámokon és kiszedjük a tételeket addig amíg folytonosak alszám szeint START");

            //Főszám és alszám sorrend alapján leválogatott tételek
            List<AlszamosIktatoObjektum> alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok = new List<AlszamosIktatoObjektum>();

            //Főszám és alszám aztán küldemény sorrend alapján leválogatott tételek
            //List<AlszamosIktatoObjektum> alszamosFoszamEsKuldemenySzerintIktathatoIktatoObjektumok = new List<AlszamosIktatoObjektum>();
            foreach (var iktatokonyvId in alszamosIktatoDocumentsOrderedByIktatoKonyv)
            {
                List<AlszamosIktatoObjektum> alszamosOsszesIktatoObjektum = alszamosIktatoDocumentsOrderedByIktatoKonyv[iktatokonyvId.Key];

                //AlszamosIktatoObjektum firstAlszamosKuldemeny = null;

                //if (alszamosOsszesIktatoObjektum.Any(x => !string.IsNullOrEmpty(x.Tetel.EREC_KuldKuldemenyek)))
                //{
                //    firstAlszamosKuldemeny = alszamosOsszesIktatoObjektum.First(x => !string.IsNullOrEmpty(x.Tetel.EREC_KuldKuldemenyek));

                //    //Ha nem volt főszámos inicializált tétel és itt már az első tétel érkeztetőszáma sem egyezik, akkor hiba email
                //    if (kuldemenyErkeztetoSzamok.Count < 1)
                //    {
                //        if (firstAlszamosKuldemeny.GetErkeztetoszam() - utolsoKuldemenyErkeztetoSzam != 1)
                //        {
                //            Hair.Helper.SendErrorMail("Hiba a technikai iktatás során. A soron következő érkeztetőszám értéke nem egyezik meg az első tétel érkeztetőszámával. Soron következő: " +
                //                (utolsoKuldemenyErkeztetoSzam + 1).ToString() + ". Küldött érkeztetőszám: " + firstAlszamosKuldemeny.GetErkeztetoszam().ToString());
                //            continue;
                //        }
                //    }
                //    //Volt főszámosak között küldemény, ezért azok közül a legutolsó érkeztetőszámát figyeljük itt már
                //    else
                //    {
                //        if (firstAlszamosKuldemeny.GetErkeztetoszam() - kuldemenyErkeztetoSzamok.Last() != 1)
                //        {
                //            continue;
                //        }
                //    }
                //}

                //Főszámonként iterálunk és kivesszük az alszám szerint folytonos tételeket
                var distinctFoszamok = alszamosOsszesIktatoObjektum.Select(x => x.GetFoszam()).Distinct().ToList();

                for (int i = 0; i < distinctFoszamok.Count(); i++)
                {
                    var rendezettAlszamok = alszamosOsszesIktatoObjektum.Where(x => x.GetFoszam() == distinctFoszamok[i]).OrderBy(x => x.GetAlszam()).ToList();

                    int utolsoAlszam = int.Parse(rendezettAlszamok.First().Ugyirat.UtolsoAlszam);
                    if (utolsoAlszam + 1 != rendezettAlszamok.First().GetAlszam())
                    {
                        Logger.Debug("Főszám alszámai nem folytonosak. Főszám: " + rendezettAlszamok.First().Ugyirat.Foszam);
                        continue;
                    }

                    for (int j = 0; j < rendezettAlszamok.Count(); j++)
                    {
                        var currentAlszam = rendezettAlszamok[j];
                        if (j == 0)
                        {
                            alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok.Add(currentAlszam);
                            continue;
                        }

                        var previousAlszam = rendezettAlszamok[j - 1];

                        if (previousAlszam.GetAlszam() + 1 != currentAlszam.GetAlszam())
                        {
                            Logger.Debug("Főszám alszámai nem folytonosak. Főszám: " + rendezettAlszamok.First().Ugyirat.Foszam
                                + " Előző alszám: " + previousAlszam.GetAlszam()
                                + " sorban következő alszám: " + currentAlszam.GetAlszam());

                            break;
                        }
                        alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok.Add(currentAlszam);
                    }
                }

                //A már leválogatott alszámos tételeket küldemény sorrend szerint is megszűrjük, már ha van küldeményük, csak addig megyünk, míg a küldemény számok folytonosak
                //for (int i = 0; i < alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok.Count; i++)
                //{
                //    if (!string.IsNullOrEmpty(alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i].Tetel.EREC_KuldKuldemenyek))
                //    {
                //        //Előzőleg ellenőriztük, hogy az első küldemény sorszáma valid-e, ha ez pont az első tétel küldeménye volt, akkor jó
                //        if (firstAlszamosKuldemeny.Tetel.Id == alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i].Tetel.Id)
                //        {
                //            kuldemenyErkeztetoSzamok.Add(alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i].GetErkeztetoszam());
                //            alszamosFoszamEsKuldemenySzerintIktathatoIktatoObjektumok.Add(alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i]);
                //        }
                //        else
                //        {
                //            //Kivesszük az utolsó jó küldemény sorszámot és ellenőrizzük
                //            int lastErkeztetoSzam = kuldemenyErkeztetoSzamok.Last();

                //            if (alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i].GetErkeztetoszam() - lastErkeztetoSzam != 1)
                //            {
                //                break;
                //            }
                //            else
                //            {
                //                kuldemenyErkeztetoSzamok.Add(alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i].GetErkeztetoszam());
                //                alszamosFoszamEsKuldemenySzerintIktathatoIktatoObjektumok.Add(alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i]);
                //            }
                //        }
                //    }
                //}
            }

            Logger.Debug("TomegesIktatas_Init_Job - Végigmegyünk a főszámokon és kiszedjük a tételeket addig amíg folytonosak alszám szeint END");
            #endregion

            if (alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok.Count > 0)
            {
                #region Groupolás és Init hívása 

                Iktatas.Valasz valasz = new Iktatas.Valasz();
                valasz.ValaszAdatok = new ValaszAdatok();
                tomegesIktatasHelper.KuldemenyIktatokonyvId = technikaiKuldemenyiktatoKonyv.Id;
                tomegesIktatasHelper.CreateGroups(alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok.Cast<IktatoObjektumBase>());
                tomegesIktatasHelper.CallTomegesIktatasInitForGroupsSync();

                #endregion

                #region Updatelj csoportjuk szerint az  Ugyirat_Id, Irat_Id, Kuldemenyek_Id értékekkel a tételeket

                for (int i = 0; i < alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok.Count(); i++)
                {
                    IktatoCsoport csoport = tomegesIktatasHelper.IktatoCsoportok.FirstOrDefault(x => x.Id == alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i].CsoportId);
                    Result updateResult;

                    EREC_TomegesIktatasTetelek tetel = alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i].Tetel;
                    //tetel.Updated.SetValueAll(false);

                    if (!string.IsNullOrEmpty(alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i].ErrorMessage))
                    {
                        var updateExecParam = Hair.Helper.GlobalExecParam.Clone();
                        updateExecParam.Record_Id = tetel.Id;

                        tetel.Hiba = alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i].ErrorMessage;
                        tetel.Updated.Hiba = true;
                        tetel.Allapot = "9";
                        tetel.Updated.Allapot = true;
                        //tetel.Base.Ver = (int.Parse(tetel.Base.Ver) + 1).ToString();
                        tetel.Base.Updated.Ver = true;
                        updateResult = tomegesIktatasTetelekService.Update(foszamosExecParam, tetel);
                    }
                    else if (csoport != null)
                    {
                        if (null != csoport.kapottIratId && csoport.kapottAlszam != null)
                        {
                            int foszam;
                            string alszam;
                            int index;
                            Guid iratId;
                            Guid? ugyiratId;
                            Guid? kuldemenyId;
                            string azonosito;

                            index = Array.FindIndex(csoport.kapottFoszam, x => x == alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok[i].GetFoszam());
                            if (index > -1)
                            {
                                foszam = csoport.kapottFoszam[index];
                                alszam = csoport.kapottAlszam[index];
                                iratId = csoport.kapottIratId[index];
                                azonosito = csoport.kapottAzonosito[index];
                                ugyiratId = csoport.kapottUgyiratId[index];
                                kuldemenyId = csoport.kapottKuldemenyId[index];

                                csoport.kapottAlszam = Helper.Remove(csoport.kapottAlszam.ToList(), index);
                                csoport.kapottIratId = Helper.Remove(csoport.kapottIratId.ToList(), index);
                                csoport.kapottAzonosito = Helper.Remove(csoport.kapottAzonosito.ToList(), index);
                                csoport.kapottFoszam = Helper.Remove(csoport.kapottFoszam.ToList(), index);
                                csoport.kapottUgyiratId = Helper.Remove(csoport.kapottUgyiratId.ToList(), index);
                                csoport.kapottKuldemenyId = Helper.Remove(csoport.kapottKuldemenyId.ToList(), index);

                                var updateExecParam = Hair.Helper.GlobalExecParam.Clone();
                                updateExecParam.Record_Id = tetel.Id;

                                tetel.Irat_Id = iratId.ToString();
                                tetel.Updated.Irat_Id = true;

                                if (ugyiratId.HasValue && ugyiratId.Value != Guid.Empty)
                                {
                                    tetel.Ugyirat_Id = ugyiratId.Value.ToString();
                                    tetel.Updated.Ugyirat_Id = true;
                                }

                                if (kuldemenyId.HasValue && kuldemenyId.Value != Guid.Empty)
                                {
                                    tetel.Kuldemeny_Id = kuldemenyId.Value.ToString();
                                    tetel.Updated.Kuldemeny_Id = true;
                                }
                                Logger.Debug("tomegesIktatasHelper.folyamat_id: " + tomegesIktatasHelper.Folyamat.Id + " tetel.folyamat_id: " + tetel.Folyamat_Id);
                                //tetel.Base.Ver = (int.Parse(tetel.Base.Ver) + 1).ToString();
                                tetel.Base.Updated.Ver = true;
                                updateResult = tomegesIktatasTetelekService.Update(updateExecParam, tetel);
                                inicializaltTetelekCount += 1;
                            }
                        }
                    }

                }

                #endregion
            }
            else
            {
                Logger.Debug("TomegesIktatas_Init_Job alszamosFoszamSzerintLevalogatottIktathatoIktatoObjektumok-ból nincs egy se.");
            }

            #endregion

            Logger.Debug("TomegesIktatas_Init_Job - Alszámos tételek kezelése END");
            #endregion

            if (errorMail.Length > 0)
            {
                Helper.SendErrorMail(errorMail.ToString());
            }

            tomegesIktatasHelper.Folyamat.SetAllapot(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.Inicializalt);
            Logger.Debug(string.Format("TomegesIktatas_Init_Job. tomegesIktatasHelper.Folyamat.Id: {0} inicializaltTetelekCount: {1} alszamosIktatoDocuments.Count {2},foszamosIktatoDocuments.Count: {3}, inicializaltTetelekCount: {4}",
                tomegesIktatasHelper.Folyamat.Id, inicializaltTetelekCount, alszamosIktatoDocuments.Count, foszamosIktatoDocuments.Count, inicializaltTetelekCount));

            if (inicializaltTetelekCount > 0)
            {
                FolyamatSzamlalo -= 1;
                if (inicializaltTetelekCount < (alszamosIktatoDocuments.Count + foszamosIktatoDocuments.Count))
                {
                    ThreadPool.QueueUserWorkItem(TomegesIktatas_Init_JobForThread, new { });
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("TomegesIktatas_Init_Job error: " + ex.Message + "StackTrace:" + ex.StackTrace);
            if (tomegesIktatasHelper != null)
            {
                tomegesIktatasHelper.Folyamat.SetAllapot(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.InicializalasiHiba);
            }
        }

        return "OK";
    }

    private void TomegesIktatas_Init_JobForThread(object state)
    {
        Logger.Debug("TomegesIktatas_Init_Job started in thread: " + Thread.CurrentThread.ManagedThreadId);
        TomegesIktatas_Init_Job();
    }

    [WebMethod]
    [LogExtension]
    [SoapDocumentMethod(RequestElementName = "Iktatasok", RequestNamespace = Iktatas.Constants.NAMESPACE,
                        ResponseElementName = "Valaszok", ResponseNamespace = Iktatas.Constants.NAMESPACE)]
    [return: XmlElement("Valaszok", Namespace = Iktatas.Constants.NAMESPACE)]
    public Iktatas.Valaszok Iktat([XmlElement("Iktatas")]
                                    Iktatas.Iktatas[] Iktatasok)
    {
        Logger.Debug("HAIR Iktat - Iktatás START");
        if (Iktatasok == null)
            throw new SoapException(Hair.Helper.Messages.SoapRequestError, SoapException.ClientFaultCode);

        Iktatas.Valaszok valaszok = new Iktatas.Valaszok();
        valaszok.Valasz = new Iktatas.Valasz[Iktatasok.Length];
        int iktatCounter = 0;
        StringBuilder errorMail = new StringBuilder();

        try
        {
            #region Valaszok Init
            Logger.Debug("Iktat - Valaszok Init START");
            for (int i = 0; i < Iktatasok.Length; i++)
            {
                Iktatas.Iktatas iktatas = Iktatasok[i];
                valaszok.Valasz[i] = new Iktatas.Valasz();
                Iktatas.Valasz valasz = valaszok.Valasz[i];
                valasz.ValaszAdatok = new Hair.ValaszAdatok();

                Hair.Helper.SetDefaultValasz(iktatas, valasz);
            }
            Logger.Debug("Iktat - Valaszok Init END");
            #endregion

            #region Normál iktatás
            Logger.Debug("Iktat - Normál iktatás START");
            for (iktatCounter = 0; iktatCounter < Iktatasok.Length; iktatCounter++)
            {
                Iktatas.Iktatas iktatas = Iktatasok[iktatCounter];

                if (iktatas.Alapadatok != null && !iktatas.Alapadatok.TechnikaiIktatas && iktatas.AtiktatasAdatok == null)
                {
                    Iktatas.Valasz valasz = valaszok.Valasz[iktatCounter];

                    Logger.Debug("Iktat - Normál iktatás. Külsőazonosító: " + iktatas.Alapadatok.KulsoAzonosito);

                    try
                    {
                        #region Iktatokönyv keresése
                        Logger.Debug("Iktat - Iktatokönyv keresése START");
                        Result iktatoKonyvekResult = Hair.Helper.GetIktatokonyvFromRequestForFiling(iktatas.IratAdatok.Iktatoszam, valasz.ValaszAdatok);

                        if (iktatoKonyvekResult.IsError)
                        {
                            Logger.Error("Hiba az iktatókönyv lekérése során.", Helper.GlobalExecParam, iktatoKonyvekResult);
                            continue;
                        }
                        if (iktatoKonyvekResult.Ds.Tables[0].Rows.Count < 1)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIktatoKonyvNotFoundError, -11);
                            continue;
                        }

                        string iktatoKonyvId = iktatoKonyvekResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                        Logger.Debug("Iktat - Iktatokönyv keresése END");
                        #endregion

                        #region Ügyirat keresése
                        Logger.Debug("Iktat - Ügyirat keresése START");
                        string ugyiratId = "0";
                        Result ugyiratResult = null;

                        if (iktatas.IratAdatok.Iktatoszam.FoszamSpecified && iktatas.IratAdatok.Iktatoszam.Foszam != 0)
                        {
                            ugyiratResult = Hair.Helper.GetUgyiratByIktatoKonyvFromRequestForFiling(iktatas.IratAdatok.Iktatoszam, valasz.ValaszAdatok, iktatoKonyvId);

                            if (ugyiratResult.IsError)
                            {
                                Logger.Error("Hiba az ugyirat lekérése során.", Helper.GlobalExecParam, ugyiratResult);
                                continue;
                            }

                            if (ugyiratResult.Ds.Tables.Count == 0 || ugyiratResult.Ds.Tables[0].Rows.Count < 1)
                            {
                                Hair.Helper.SetError(valasz, Hair.Helper.Messages.UgyiratNotFoundError, -4);
                                continue;
                            }
                            else if (ugyiratResult.Ds.Tables.Count > 1)
                            {
                                Hair.Helper.SetError(valasz, Hair.Helper.Messages.UgyiratTooManyError, -5);
                                continue;
                            }

                            ugyiratId = ugyiratResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                        }
                        Logger.Debug("Iktat - Ügyirat keresése END");
                        #endregion

                        #region Irat és iktatási paraméterek
                        Logger.Debug("Iktat - Irat és iktatási paraméterek START");
                        EREC_IraIratok iratForFiling = Hair.Helper.GetIratFromRequestForFiling(iktatas, valasz);

                        if (Hair.Helper.IsError(valasz))
                        {
                            continue;
                        }

                        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        ExecParam iratokExecParam = Hair.Helper.GlobalExecParam.Clone();

                        IktatasiParameterek ip = Hair.Helper.GetIktatasiParameterkFromRequestForFiling();

                        if (ugyiratResult != null)
                        {
                            ip.UgykorId = ugyiratResult.Ds.Tables[0].Rows[0]["IraIrattariTetel_Id"].ToString();
                            ip.Ugytipus = ugyiratResult.Ds.Tables[0].Rows[0]["UgyTipus"].ToString();
                        }
                        Logger.Debug("Iktat - Irat és iktatási paraméterek END");
                        #endregion

                        #region Iktatás
                        Logger.Debug("Iktat - Iktatás START");
                        Result iktatasResult = new Result();

                        if (iktatas.IratAdatok.AlszamAdatok.IratIranya == KodTarak.POSTAZAS_IRANYA.Belso)
                        {
                            Logger.Debug("Iktat - Iktatás BelsoIratIktatasa_Alszamra START");
                            EREC_PldIratPeldanyok iratpeldany = Hair.Helper.GetIratPeldanyFromRequestForFiling(iktatas, valasz);
                            if (Hair.Helper.IsError(valasz))
                            {
                                continue;
                            }
                            iktatasResult = iratokService.BelsoIratIktatasa_Alszamra(iratokExecParam, iktatoKonyvId, ugyiratId, new EREC_UgyUgyiratdarabok(), iratForFiling, null, iratpeldany, ip);
                            Logger.Debug("Iktat - Iktatás BelsoIratIktatasa_Alszamra END");
                        }
                        else if (iktatas.IratAdatok.AlszamAdatok.IratIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                        {
                            bool vanKuldemeny = false;
                            Result kuldemenyResult = null;
                            if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.Vonalkod) || iktatas.KuldemenyAdatok.ErkeztetoSzam.SorszamSpecified)
                            {
                                kuldemenyResult = Hair.Helper.SearchKuldemenyForFiling(iktatas, valasz);
                                if (kuldemenyResult.IsError)
                                {
                                    continue;
                                }

                                if (kuldemenyResult.Ds.Tables[0].Rows.Count > 0)
                                {
                                    vanKuldemeny = true;
                                }
                            }
                            else
                            {
                                vanKuldemeny = false;
                            }


                            if (iktatas.IratAdatok.Iktatoszam.FoszamSpecified && iktatas.IratAdatok.Iktatoszam.Foszam != 0)
                            {
                                if (vanKuldemeny)
                                {
                                    Logger.Debug("Iktat - Iktatás BejovoIratIktatasa_Alszamra START");
                                    ip.KuldemenyId = kuldemenyResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                                    iktatasResult = iratokService.BejovoIratIktatasa_Alszamra(iratokExecParam, iktatoKonyvId, ugyiratId, new EREC_UgyUgyiratdarabok(), iratForFiling, null, ip);
                                    Logger.Debug("Iktat - Iktatás BejovoIratIktatasa_Alszamra END");
                                }
                                else
                                {
                                    Logger.Debug("Iktat - Iktatás EgyszerusitettIktatasa_Alszamra START");
                                    EREC_KuldKuldemenyek kuld = Hair.Helper.GetKuldemenyForFiling(iktatas, valasz);
                                    if (Hair.Helper.IsError(valasz))
                                    {
                                        continue;
                                    }
                                    iktatasResult = iratokService.EgyszerusitettIktatasa_Alszamra(iratokExecParam, iktatoKonyvId, ugyiratId, new EREC_UgyUgyiratdarabok(), iratForFiling, null, kuld, ip, null);
                                    Logger.Debug("Iktat - Iktatás EgyszerusitettIktatasa_Alszamra END");
                                }
                            }
                            else
                            {

                                EREC_UgyUgyiratok ugyirat = Hair.Helper.GetUgyiratFromRequestForFiling(iktatas, valasz);

                                if (Hair.Helper.IsError(valasz))
                                {
                                    continue;
                                }

                                if (ugyirat == null)
                                    continue;

                                if (vanKuldemeny)
                                {
                                    Logger.Debug("Iktat - Iktatás BejovoIratIktatasa START");
                                    ip.KuldemenyId = kuldemenyResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                                    iktatasResult = iratokService.BejovoIratIktatasa(iratokExecParam, iktatoKonyvId, ugyirat, new EREC_UgyUgyiratdarabok(), iratForFiling, null, ip);
                                    Logger.Debug("Iktat - Iktatás BejovoIratIktatasa END");
                                }
                                else
                                {
                                    Logger.Debug("Iktat - Iktatás EgyszerusitettIktatasa START");
                                    EREC_KuldKuldemenyek kuld = Hair.Helper.GetKuldemenyForFiling(iktatas, valasz);
                                    if (Hair.Helper.IsError(valasz))
                                    {
                                        continue;
                                    }
                                    iktatasResult = iratokService.EgyszerusitettIktatasa(iratokExecParam, iktatoKonyvId, ugyirat, new EREC_UgyUgyiratdarabok(), iratForFiling, null, kuld, ip, null);
                                    Logger.Debug("Iktat - Iktatás EgyszerusitettIktatasa END");
                                }
                            }
                        }

                        if (iktatasResult.IsError)
                        {
                            Hair.Helper.SetError(valasz, iktatasResult);
                            continue;
                        }

                        string newIraIratId = iktatasResult.Uid;
                        Result iktatottIratResult = Hair.Helper.GetIratWithExtension(newIraIratId, valasz);
                        DataRow iratRow = iktatottIratResult.Ds.Tables[0].Rows[0];
                        Logger.Debug("Iktat - Iktatás END");
                        #endregion

                        #region Kiadmányozás
                        Logger.Debug("Iktat - Kiadmányozás START");
                        if (iktatas.IratAdatok.AlszamAdatok.IratIranya == KodTarak.POSTAZAS_IRANYA.Belso)
                        {
                            if (iktatas.IratAdatok.KiadmanyozasAdatok != null)
                            {
                                if (!string.IsNullOrEmpty(iktatas.IratAdatok.KiadmanyozasAdatok.Alairo) && iktatas.IratAdatok.KiadmanyozasAdatok.AlairasDatumSpecified)
                                {
                                    Result felhasznaloResult = Helper.GetFelhasznaloByUserNev(iktatas.IratAdatok.KiadmanyozasAdatok.Alairo, valasz);
                                    string alairoId = "";
                                    if (felhasznaloResult.IsError)
                                    {
                                        valasz.SetError(felhasznaloResult);
                                        continue;
                                    }
                                    else if (felhasznaloResult.Ds.Tables[0].Rows.Count > 0)
                                    {
                                        alairoId = felhasznaloResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                                    }
                                    else
                                    {
                                        Result csoportResult = Helper.GetCsoportByUserNev(iktatas.IratAdatok.KiadmanyozasAdatok.Alairo, valasz);
                                        if (csoportResult.IsError)
                                        {
                                            valasz.SetError(csoportResult);
                                            continue;
                                        }
                                        else if (csoportResult.Ds.Tables[0].Rows.Count < 1)
                                        {
                                            valasz.SetError("iktatas.IratAdatok.KiadmanyozasAdatok.Alairo nem található", -2);
                                            return null;
                                        }
                                        else
                                        {
                                            alairoId = csoportResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(alairoId))
                                    {
                                        Helper.Alairas(newIraIratId, alairoId, iktatas.IratAdatok.KiadmanyozasAdatok.AlairasDatum, valasz);
                                    }
                                    if (Helper.IsError(valasz))
                                    {
                                        continue;
                                    }
                                }
                            }
                        }
                        Logger.Debug("Iktat - Kiadmányozás END");
                        #endregion

                        #region Elintézés
                        Logger.Debug("Iktat - Elintézés START");
                        if (iktatas.IratAdatok.FoszamAdatok.LezarhatoSpecified && iktatas.IratAdatok.FoszamAdatok.Lezarhato)
                        {
                            EREC_IraIratok irat = new EREC_IraIratok();
                            Utility.LoadBusinessDocumentFromDataRow(irat, iratRow);

                            Helper.Elintezes(irat, valasz);

                            if (Helper.IsError(valasz))
                            {
                                continue;
                            }
                        }
                        Logger.Debug("Iktat - Elintézés END");
                        #endregion

                        #region Statisztika adatok
                        Logger.Debug("Iktat - Statisztika adatok START");
                        if (iktatas.IratAdatok.HatosagiStatisztikaAdatok != null && iktatas.IratAdatok.HatosagiStatisztikaAdatok.StatisztikaElem != null)
                        {
                            Hair.Helper.InsertObjektumTargyszavak(newIraIratId, iratRow["Ugy_Fajtaja"].ToString(), iktatas.IratAdatok.HatosagiStatisztikaAdatok.StatisztikaElem, valasz.ValaszAdatok);
                            if (Hair.Helper.IsError(valasz))
                                continue;
                        }
                        Logger.Debug("Iktat - Statisztika adatok END");
                        #endregion

                        #region Kézbesítés értesítés
                        Logger.Debug("Iktat - Kézbesítés értesítés START");
                        if (iktatas.IratAdatok.HAIRAdatok != null && iktatas.IratAdatok.HAIRAdatok.KezbesitesErtesitesSzuksegesSpecified)
                        {
                            string objTipusGuid = Hair.Helper.GetObjTipusByKod("EREC_IraIratok", valasz.ValaszAdatok);

                            if (Hair.Helper.IsError(valasz))
                            {
                                continue;
                            }
                            if (objTipusGuid != null)
                            {
                                Result ertesitesResult = Hair.Helper.InsertHAIRAdatok(newIraIratId, objTipusGuid,
                                    KodTarak.HAIR_ADAT_TIPUSOK.KezbesitErtesites, iktatas.IratAdatok.HAIRAdatok.KezbesitesErtesitesSzukseges ? "1" : "0");

                                if (ertesitesResult.IsError)
                                {
                                    Hair.Helper.SetError(valasz, ertesitesResult);
                                    continue;
                                }
                            }
                        }
                        Logger.Debug("Iktat - Kézbesítés értesítés END");
                        #endregion

                        #region Valasz adatok 
                        Logger.Debug("Iktat - Valasz adatok START");
                        ErkeztetesIktatasResult iktatott = (ErkeztetesIktatasResult)iktatasResult.Record;

                        if (Hair.Helper.IsError(valasz))
                        {
                            continue;
                        }

                        EREC_UgyUgyiratok ugyiratValasz = Hair.Helper.GetUgyiratById(iktatott.UgyiratId, valasz);


                        if (Hair.Helper.IsError(valasz))
                        {
                            continue;
                        }

                        EREC_IraIktatoKonyvek ikatatokonyv = Hair.Helper.GetIktatoKonyv(iktatoKonyvId, iktatoKonyvek, valasz);

                        if (Hair.Helper.IsError(valasz))
                        {
                            continue;
                        }

                        int alszam;
                        int foszam;
                        if (int.TryParse(Hair.Helper.GetRowValueAsString(iratRow, "Alszam"), out alszam))
                        {
                            valasz.Iktatoszam.Alszam = alszam;
                            valasz.Iktatoszam.AlszamSpecified = true;
                            //az iktatás mindig létrehozza az 1-es iratpéldányt, a vonalkódot is arról adjuk vissza
                            valasz.Iktatoszam.Sorszam = 1;
                            valasz.Iktatoszam.SorszamSpecified = true;
                        }

                        if (int.TryParse(ugyiratValasz.Foszam, out foszam))
                        {
                            valasz.Iktatoszam.Foszam = foszam;
                            valasz.Iktatoszam.FoszamSpecified = true;
                        }

                        valasz.Iktatoszam.Elotag = ikatatokonyv.Iktatohely;
                        int ev;
                        if (int.TryParse(ikatatokonyv.Ev, out ev))
                        {
                            valasz.Iktatoszam.Ev = ev;
                            valasz.Iktatoszam.EvSpecified = true;
                        }

                        if (valasz.Vonalkodok == null)
                        {
                            valasz.Vonalkodok = new Hair.KuldemenyVonalkodok();
                        }

                        valasz.Vonalkodok.UgyiratVonalkod = ugyiratValasz.BARCODE;
                        valasz.Vonalkodok.IratpeldanyVonalkod = Hair.Helper.GetRowValueAsString(iratRow, "ElsoIratPeldanyBarCode");
                        string kuldemenyId = Hair.Helper.GetRowValueAsString(iratRow, "KuldKuldemenyek_Id");
                        if (!string.IsNullOrEmpty(kuldemenyId))
                        {
                            EREC_KuldKuldemenyek kuldemeny = Hair.Helper.GetKuldemeny(kuldemenyId, valasz);
                            if (Hair.Helper.IsError(valasz))
                            {
                                continue;
                            }

                            valasz.Vonalkodok.KuldemenyVonalkod = kuldemeny.BarCode;

                            if (valasz.ErkeztetoSzam == null)
                                valasz.ErkeztetoSzam = new Iktatas.ErkeztetoSzam();

                            int erkeztetoszam;
                            if (int.TryParse(kuldemeny.Erkezteto_Szam, out erkeztetoszam))
                            {
                                valasz.ErkeztetoSzam.Sorszam = erkeztetoszam;
                                valasz.ErkeztetoSzam.SorszamSpecified = true;
                            }

                            int erkeztetoev;
                            EREC_IraIktatoKonyvek erkeztetokonyv = Hair.Helper.GetIktatoKonyv(kuldemeny.IraIktatokonyv_Id, iktatoKonyvek, valasz);
                            if (int.TryParse(erkeztetokonyv.Ev, out erkeztetoev))
                            {
                                valasz.ErkeztetoSzam.Ev = erkeztetoev;
                                valasz.ErkeztetoSzam.EvSpecified = true;
                            }
                        }
                        Logger.Debug("Iktat - Valasz adatok END");
                        #endregion

                    }
                    catch (Exception ex)
                    {
                        Hair.Helper.SetError(valasz, ex.Message);
                    }
                }
            }
            Logger.Debug("Iktat - Normál iktatás END");
            #endregion

            #region Technikai iktatás
            Logger.Debug("Iktat - Technikai iktatás START");
            EREC_TomegesIktatasTetelekService tomegesIktatasTetelekService = eRecordService.ServiceFactory.GetEREC_TomegesIktatasTetelekService();
            ExecParam tExecParam = Hair.Helper.GlobalExecParam.Clone();

            for (iktatCounter = 0; iktatCounter < Iktatasok.Length; iktatCounter++)
            {
                Iktatas.Iktatas iktatas = Iktatasok[iktatCounter];

                if (iktatas.Alapadatok != null && iktatas.Alapadatok.TechnikaiIktatas)
                {
                    Iktatas.Valasz valasz = valaszok.Valasz[iktatCounter];

                    Logger.Debug("Iktat - Technikai iktatás. Külsőazonosító: " + iktatas.Alapadatok.KulsoAzonosito);

                    try
                    {
                        #region Data checks
                        Logger.Debug("Iktat - Data checks START");
                        #region Mandatory checks                        

                        if (iktatas.IratAdatok == null)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIratadatokMissingError, -8);
                            continue;
                        }

                        if (iktatas.IratAdatok.Iktatoszam == null)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIratAdatokIktatoszamMissingError, -9);
                            continue;
                        }

                        if (!iktatas.IratAdatok.Iktatoszam.EvSpecified || iktatas.IratAdatok.Iktatoszam.Ev == 0)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIratAdatokIktatoszamEvMissingError, -10);
                            continue;
                        }

                        if (String.IsNullOrEmpty(iktatas.IratAdatok.Iktatoszam.Elotag))
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIratAdatokIktatoszamElotagMissingError, -11);
                            continue;
                        }

                        if (!iktatas.IratAdatok.Iktatoszam.FoszamSpecified)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIratAdatokIktatoszamFoszamMissingError, -12);
                            continue;
                        }

                        if (!iktatas.IratAdatok.Iktatoszam.AlszamSpecified)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIratAdatokIktatoszamAlszamMissingError, -13);
                            continue;
                        }

                        if (iktatas.IratAdatok.HAIRAdatok == null)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIratAdatokHAIRAdatokMissingError, -14);
                            continue;
                        }

                        if (iktatas.Alapadatok == null)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasAlapAdatokMissingError, -19);
                            continue;
                        }

                        if (string.IsNullOrEmpty(iktatas.Alapadatok.KulsoAzonosito))
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasAlapAdatokKulsoAzonositoMissingError, -20);
                            continue;
                        }

                        #endregion

                        if (iktatas.IratAdatok.Iktatoszam.Elotag != Hair.Helper.TECHNIKAI_IKTATOKONYV_IKTATOHELY)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasElotagNotEqualsIktatoKonyvError, -15);
                            continue;
                        }

                        if (iktatas.IratAdatok.AlszamAdatok == null)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIratAdatokAlszamadatokMissingError, -21);
                            continue;
                        }

                        Logger.Debug("Iktat - Data checks END");
                        #endregion

                        #region EREC_TomegesIktatasTetelek alapadatok init
                        Logger.Debug("Iktat - EREC_TomegesIktatasTetelek alapadatok init START");
                        EREC_TomegesIktatasTetelek tetel = new EREC_TomegesIktatasTetelek();
                        tetel.Updated.SetValueAll(false);
                        tetel.Base.Updated.SetValueAll(false);

                        tetel.ExecParam = JsonConvert.SerializeObject(Hair.Helper.GlobalExecParam.Clone());
                        tetel.Updated.ExecParam = true;

                        tetel.Forras_Azonosito = iktatas.Alapadatok.KulsoAzonosito;
                        tetel.Updated.Forras_Azonosito = true;

                        tetel.Allapot = "1";
                        tetel.Updated.Allapot = true;

                        tetel.IktatasTipus = iktatas.IratAdatok.AlszamAdatok.IratIranya == "1" ? "0" : "1";
                        tetel.Updated.IktatasTipus = true;

                        tetel.EREC_UgyUgyiratdarabok = JsonConvert.SerializeObject(new EREC_UgyUgyiratdarabok());
                        tetel.Updated.EREC_UgyUgyiratdarabok = true;

                        if (iktatas.IratAdatok.Iktatoszam.Alszam == 1)
                        {
                            tetel.Alszamra = "0";
                        }
                        else if (iktatas.IratAdatok.Iktatoszam.Alszam > 1)
                        {
                            tetel.Alszamra = "1";
                        }
                        else
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasAlszamError, -22);
                            continue;
                        }

                        tetel.Updated.Alszamra = true;

                        if (iktatas.IratAdatok.FoszamAdatok != null && iktatas.IratAdatok.FoszamAdatok.LezarhatoSpecified)
                        {
                            tetel.Lezarhato = iktatas.IratAdatok.FoszamAdatok.Lezarhato ? "1" : "0";
                            tetel.Updated.Lezarhato = true;
                        }

                        #region Hatósági statisztika

                        if (iktatas.IratAdatok.HatosagiStatisztikaAdatok != null && iktatas.IratAdatok.HatosagiStatisztikaAdatok.StatisztikaElem != null)
                        {
                            tetel.HatosagiStatAdat = JsonConvert.SerializeObject(iktatas.IratAdatok.HatosagiStatisztikaAdatok.StatisztikaElem);
                            tetel.Updated.HatosagiStatAdat = true;

                            //if (iktatas.IratAdatok.HatosagiStatisztikaAdatok.StatisztikaElem.Length > 0)
                            //{
                            //    foreach (var elem in iktatas.IratAdatok.HatosagiStatisztikaAdatok.StatisztikaElem)
                            //    {
                            //        tetel.HatosagiStatAdat = elem.StatisztikaElemKod + "," + elem.StatisztikaElemErtek + "|";
                            //    }
                            //    tetel.HatosagiStatAdat.TrimEnd('|');
                            //    tetel.Updated.HatosagiStatAdat = true;

                            //}
                        }

                        #endregion

                        #region Aláíró

                        if (iktatas.IratAdatok.KiadmanyozasAdatok != null)
                        {
                            if (!string.IsNullOrEmpty(iktatas.IratAdatok.KiadmanyozasAdatok.Alairo) && iktatas.IratAdatok.KiadmanyozasAdatok.AlairasDatumSpecified)
                            {
                                Result felhasznaloResult = Helper.GetFelhasznaloByUserNev(iktatas.IratAdatok.KiadmanyozasAdatok.Alairo, valasz);
                                string alairoId = "";
                                if (felhasznaloResult.IsError)
                                {
                                    valasz.SetError(felhasznaloResult);
                                    continue;
                                }
                                else if (felhasznaloResult.Ds.Tables[0].Rows.Count > 0)
                                {
                                    alairoId = felhasznaloResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                                }
                                else
                                {
                                    Result csoportResult = Helper.GetCsoportByUserNev(iktatas.IratAdatok.KiadmanyozasAdatok.Alairo, valasz);
                                    if (csoportResult.IsError)
                                    {
                                        valasz.SetError(csoportResult);
                                        continue;
                                    }
                                    else if (csoportResult.Ds.Tables[0].Rows.Count < 1)
                                    {
                                        valasz.SetError("IratAdatok.KiadmanyozasAdatok.Alairo nem található", -2);
                                        continue;
                                    }
                                    else
                                    {
                                        alairoId = csoportResult.Ds.Tables[0].Rows[0].GetRowValueAsString("Id");
                                    }
                                }
                                if (!string.IsNullOrEmpty(alairoId))
                                {
                                    EREC_IratAlairok iratAlairo = Helper.GetIratAlairok(null, alairoId, iktatas.IratAdatok.KiadmanyozasAdatok.AlairasDatum, "M_UTO", KodTarak.ALAIRO_SZEREP.Kiadmanyozo, "2");
                                    tetel.EREC_IratAlairok = JsonConvert.SerializeObject(iratAlairo);
                                    tetel.Updated.EREC_IratAlairok = true;
                                }
                            }
                        }

                        #endregion
                        Logger.Debug("Iktat - EREC_TomegesIktatasTetelek alapadatok init END");
                        #endregion

                        #region Iktatokönyv keresése

                        Logger.Debug("Technikai iktatás - Iktatókönyv lekérdezés - BEGIN");
                        EREC_IraIktatoKonyvek iratIktatoKonyv = new EREC_IraIktatoKonyvek();
                        Result iktatoKonyvekResult = Hair.Helper.GetIktatokonyvFromRequestForFiling(iktatas.IratAdatok.Iktatoszam, valasz.ValaszAdatok);

                        if (iktatoKonyvekResult.IsError)
                        {
                            continue;
                        }
                        if (iktatoKonyvekResult.Ds.Tables[0].Rows.Count < 1)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIktatoKonyvNotFoundError, -16);
                            continue;
                        }

                        Utility.LoadBusinessDocumentFromDataRow(iratIktatoKonyv, iktatoKonyvekResult.Ds.Tables[0].Rows[0]);
                        string iktatoKonyvId = iktatoKonyvekResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                        tetel.Iktatokonyv_Id = iktatoKonyvId;
                        tetel.Updated.Iktatokonyv_Id = true;

                        Logger.Debug("Technikai iktatás - Iktatókönyv lekérdezés - END");
                        #endregion

                        #region Ügyirat keresése


                        Logger.Debug("Technikai iktatás - Ügyirat lekérdezés - BEGIN");
                        string ugyiratId = null;
                        Result ugyiratResult = null;
                        EREC_UgyUgyiratok foUgyirat = new EREC_UgyUgyiratok();

                        ugyiratResult = Hair.Helper.GetUgyiratByIktatoKonyvFromRequestForFiling(iktatas.IratAdatok.Iktatoszam, valasz.ValaszAdatok, iktatoKonyvId);

                        if (ugyiratResult.IsError)
                        {
                            continue;
                        }

                        if (ugyiratResult.Ds.Tables[0].Rows.Count > 1)
                        {
                            Logger.Debug("Technikai iktatás - Ügyirat lekérdezés - Több találat");
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.UgyiratTooManyError, -5);
                            continue;
                        }
                        else if (ugyiratResult.Ds.Tables[0].Rows.Count == 1 && tetel.Alszamra == "1")
                        {
                            Logger.Debug("Technikai iktatás - Ügyirat lekérdezés - Van találat és alszámra");
                            ugyiratId = ugyiratResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                            Utility.LoadBusinessDocumentFromDataRow(foUgyirat, ugyiratResult.Ds.Tables[0].Rows[0]);
                        }
                        else
                        {
                            Logger.Debug("Technikai iktatás - Ügyirat lekérdezés - Nincs találat, de főszámra");
                            foUgyirat = Hair.Helper.GetUgyiratFromRequestForFiling(iktatas, valasz);
                            if (Helper.IsError(valasz))
                            {
                                continue;
                            }
                        }

                        Logger.Debug("Technikai iktatás - Ügyirat lekérdezés - END");

                        #endregion

                        #region Létező irat keresése, ha van, hiba

                        Logger.Debug("Technikai iktatás - Irat lekérdezés - BEGIN");
                        //Result iratByKulsoAzonositoResult = Hair.Helper.SearchIratByKulsoAzonosito(iktatas.IratAdatok.HAIRAdatok.IratKulsoAzonosito, valasz);

                        //if (Hair.Helper.IsError(valasz))
                        //{
                        //    continue;
                        //}

                        //if (iratByKulsoAzonositoResult.Ds.Tables[0].Rows.Count > 0)
                        //{
                        //    Hair.Helper.SetError(valasz, Hair.Helper.Messages.IratKulsoAzonositoFoundError, -17);
                        //    Hair.Helper.SendErrorMail(Hair.Helper.Messages.IratKulsoAzonositoFoundError + "Külsőazonosító: " + iktatas.IratAdatok.HAIRAdatok.IratKulsoAzonosito);
                        //    continue;
                        //}

                        if (!string.IsNullOrEmpty(ugyiratId))
                        {
                            Result iratResult = Hair.Helper.SearchIratByUgyiratAndAlszamORKulsoAzonosito(ugyiratId, iktatas.IratAdatok.Iktatoszam.Alszam,
                                iktatas.IratAdatok.HAIRAdatok.IratKulsoAzonosito, valasz.ValaszAdatok);

                            if (Hair.Helper.IsError(valasz))
                            {
                                continue;
                            }

                            if (iratResult.Ds.Tables[0].Rows.Count > 0)
                            {
                                Hair.Helper.SetError(valasz, Hair.Helper.Messages.IratIktatoszamFoundError, -18);
                                errorMail.AppendLine(Hair.Helper.Messages.IratIktatoszamFoundError + "Azonositó: " + iratResult.Ds.Tables[0].Rows[0]["Azonosito"].ToString());
                                continue;
                            }
                        }

                        Logger.Debug("Technikai iktatás - Irat lekérdezés - END");
                        #endregion

                        #region EREC_TomegesIktatasTetelek init és Insert
                        Logger.Debug("Iktat - EREC_TomegesIktatasTetelek init és Insert START");
                        IktatasiParameterek ip = Hair.Helper.GetIktatasiParameterkFromRequestForFiling();

                        ip.UgykorId = foUgyirat.IraIrattariTetel_Id;
                        ip.Ugytipus = foUgyirat.UgyTipus;


                        if (tetel.IktatasTipus == "0")
                        {
                            Logger.Debug("Technikai iktatás - Küldemény lekérdezés - BEGIN");
                            bool vanKuldemeny = false;
                            Result kuldemenyResult = null;

                            if (iktatas.KuldemenyAdatok != null)
                            {
                                if (!string.IsNullOrEmpty(iktatas.KuldemenyAdatok.Vonalkod))
                                {
                                    kuldemenyResult = Hair.Helper.SearchKuldemenyForFiling(iktatas.KuldemenyAdatok.Vonalkod, null);
                                    if (kuldemenyResult.IsError)
                                        continue;

                                    if (kuldemenyResult.Ds.Tables[0].Rows.Count > 0)
                                    {
                                        vanKuldemeny = true;
                                        tetel.Kuldemeny_Id = kuldemenyResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                                        tetel.Updated.Kuldemeny_Id = true;
                                    }
                                }

                                if (!vanKuldemeny)
                                {
                                    if (iktatas.KuldemenyAdatok != null && iktatas.KuldemenyAdatok.ErkeztetoSzam != null)
                                    {
                                        EREC_IraIktatoKonyvek kuldemenyKonyv = Hair.Helper.SearchIktatokonyvForKuldemeny("E", iktatas.KuldemenyAdatok.ErkeztetoSzam.ErkeztetoKonyvKod, iktatas.KuldemenyAdatok.ErkeztetoSzam.Ev, iktatoKonyvek, valasz);

                                        if (Helper.IsError(valasz))
                                        {
                                            continue;
                                        }
                                        if (kuldemenyKonyv != null)
                                        {
                                            string kuldemenyIktatoKonyvId = kuldemenyKonyv.Id;

                                            kuldemenyResult = Hair.Helper.SearchKuldemenyForFilingByIktatoKonyvId(kuldemenyIktatoKonyvId, iktatas.KuldemenyAdatok.ErkeztetoSzam.Sorszam);

                                            if (kuldemenyResult.Ds.Tables[0].Rows.Count > 0)
                                            {
                                                vanKuldemeny = true;
                                                tetel.Kuldemeny_Id = kuldemenyResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                                                tetel.Updated.Kuldemeny_Id = true;
                                                ip.KuldemenyId = tetel.Kuldemeny_Id;
                                            }
                                            else
                                            {
                                                vanKuldemeny = false;
                                                if (iktatas.KuldemenyAdatok.ErkeztetoSzam.ErkeztetoKonyvKod != Hair.Helper.TECHNIKAI_ERKEZTETOKONYV_MEGKULJEL)
                                                {
                                                    Hair.Helper.SetError(valasz, Hair.Helper.Messages.ErkeztatoKonyvMegKulJelNotEqualsErkeztetoKonyvError, -23);
                                                    continue;
                                                }
                                                EREC_KuldKuldemenyek kuld = Hair.Helper.GetKuldemenyForFiling(iktatas, valasz);
                                                if (Helper.IsError(valasz))
                                                {
                                                    continue;
                                                }
                                                tetel.EREC_KuldKuldemenyek = JsonConvert.SerializeObject(kuld);
                                                tetel.Updated.EREC_KuldKuldemenyek = true;
                                            }
                                        }
                                    }
                                }
                            }

                            Logger.Debug("Technikai iktatás - Küldemény lekérdezés - END");
                        }
                        else
                        {
                            EREC_PldIratPeldanyok iratpeldany = Hair.Helper.GetIratPeldanyFromRequestForFiling(iktatas, valasz);
                            if (Hair.Helper.IsError(valasz))
                            {
                                continue;
                            }
                            tetel.EREC_PldIratPeldanyok = JsonConvert.SerializeObject(iratpeldany);
                            tetel.Updated.EREC_PldIratPeldanyok = true;
                        }

                        tetel.IktatasiParameterek = JsonConvert.SerializeObject(ip);
                        tetel.Updated.IktatasiParameterek = true;

                        if (tetel.Alszamra == "0")
                        {
                            EREC_UgyUgyiratok ugyirat = Hair.Helper.GetUgyiratFromRequestForFiling(iktatas, valasz);

                            if (Hair.Helper.IsError(valasz))
                            {
                                continue;
                            }

                            if (ugyirat == null)
                                continue;

                            tetel.EREC_UgyUgyiratok = JsonConvert.SerializeObject(ugyirat);
                            tetel.Updated.EREC_UgyUgyiratok = true;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ugyiratId))
                            {
                                tetel.Ugyirat_Id = ugyiratId;
                            }
                            else
                            {
                                tetel.EREC_UgyUgyiratok = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(Hair.Helper.GlobalExecParam.Clone(), foUgyirat, iratIktatoKonyv);
                                tetel.Updated.EREC_UgyUgyiratok = true;
                            }
                            tetel.Updated.Ugyirat_Id = true;
                        }

                        EREC_IraIratok iratForFiling = Hair.Helper.GetIratFromRequestForFiling(iktatas, valasz);

                        if (Hair.Helper.IsError(valasz))
                        {
                            continue;
                        }

                        tetel.EREC_IraIratok = JsonConvert.SerializeObject(iratForFiling);
                        tetel.Updated.EREC_IraIratok = true;

                        tetel.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(Hair.Helper.GlobalExecParam.Clone(), iratIktatoKonyv, foUgyirat, iratForFiling);
                        tetel.Updated.Azonosito = true;

                        tetel.Base.Note = "HAIR" + Helper.PadLeft(foUgyirat.Foszam, 9);
                        tetel.Base.Updated.Note = true;

                        if (Helper.CheckExistingTetelAzonosito(tetel.Azonosito, valasz))
                        {
                            valasz.ValaszAdatok.HibaKod = ErrorCodes.LetezoAzonosito;
                            valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.LetezoAzonosito);
                            errorMail.AppendLine("Technikai iktatás - Az adott iktatószámmal, már történt iktatás:" + tetel.Azonosito);
                            continue;
                        }

                        Result insertResult = tomegesIktatasTetelekService.Insert(tExecParam, tetel);

                        if (insertResult.IsError)
                        {
                            Hair.Helper.SetError(valasz, insertResult);
                        }
                        Logger.Debug("Iktat - EREC_TomegesIktatasTetelek init és Insert END");
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        Hair.Helper.SetError(valasz, ex.Message);
                    }
                }
            }
            Logger.Debug("Iktat - Technikai iktatás END");
            #endregion

            #region Átiktatás
            Logger.Debug("Iktat - Átiktatás START");
            for (iktatCounter = 0; iktatCounter < Iktatasok.Length; iktatCounter++)
            {
                Iktatas.Iktatas iktatas = Iktatasok[iktatCounter];

                if (iktatas.Alapadatok != null && !iktatas.Alapadatok.TechnikaiIktatas && iktatas.AtiktatasAdatok != null)
                {
                    Iktatas.Valasz valasz = valaszok.Valasz[iktatCounter];

                    Logger.Debug("Iktat - Átiktatás. Külsőazonosító: " + iktatas.Alapadatok.KulsoAzonosito);

                    #region Mandatory checks

                    Logger.Debug("Átiktatás iktatás - Adat ellenőrzés - BEGIN");

                    if (iktatas.AtiktatasAdatok.Iktatoszam == null)
                    {
                        valasz.ValaszAdatok.HibaKod = ErrorCodes.IktatasAtiktatasAdatokIktatoszamMissing;
                        valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.IktatasAtiktatasAdatokIktatoszamMissing);
                        continue;
                    }

                    if (iktatas.IratAdatok == null)
                    {
                        valasz.ValaszAdatok.HibaKod = ErrorCodes.IktatasIratadatokMissing;
                        valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.IktatasIratadatokMissing);
                        continue;
                    }

                    if (iktatas.IratAdatok.Iktatoszam == null)
                    {
                        valasz.ValaszAdatok.HibaKod = ErrorCodes.IktatasIratAdatokIktatoszamMissing;
                        valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.IktatasIratAdatokIktatoszamMissing);
                        continue;
                    }

                    if (string.IsNullOrEmpty(iktatas.AtiktatasAdatok.Iktatoszam.Elotag))
                    {
                        valasz.ValaszAdatok.HibaKod = ErrorCodes.IktatasAtiktatasAdatokIktatoszamElotagMissing;
                        valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.IktatasAtiktatasAdatokIktatoszamElotagMissing);
                        continue;
                    }

                    if (!iktatas.AtiktatasAdatok.Iktatoszam.FoszamSpecified)
                    {
                        valasz.ValaszAdatok.HibaKod = ErrorCodes.IktatasAtiktatasAdatokIktatoszamFoszamMissing;
                        valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.IktatasAtiktatasAdatokIktatoszamFoszamMissing);
                        continue;
                    }

                    if (!iktatas.AtiktatasAdatok.Iktatoszam.AlszamSpecified)
                    {
                        valasz.ValaszAdatok.HibaKod = ErrorCodes.IktatasAtiktatasAdatokIktatoszamAlszamMissing;
                        valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.IktatasAtiktatasAdatokIktatoszamAlszamMissing);
                        continue;
                    }

                    if (!iktatas.IratAdatok.Iktatoszam.EvSpecified)
                    {
                        valasz.ValaszAdatok.HibaKod = ErrorCodes.IktatasAtiktatasAdatokIktatoszamEvMissing;
                        valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.IktatasAtiktatasAdatokIktatoszamEvMissing);
                        continue;
                    }

                    #endregion

                    #region Iktatokönyv keresése

                    Logger.Debug("Átiktatás iktatás - Iktatókönyv lekérdezés - BEGIN");
                    EREC_IraIktatoKonyvek iratIktatoKonyv = new EREC_IraIktatoKonyvek();
                    Result iktatoKonyvekResult = Hair.Helper.GetIktatokonyvFromRequestForFiling(iktatas.AtiktatasAdatok.Iktatoszam, valasz.ValaszAdatok);

                    if (iktatoKonyvekResult.IsError)
                    {
                        continue;
                    }
                    if (iktatoKonyvekResult.Ds.Tables[0].Rows.Count < 1)
                    {
                        Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIktatoKonyvNotFoundError, -16);
                        continue;
                    }


                    Utility.LoadBusinessDocumentFromDataRow(iratIktatoKonyv, iktatoKonyvekResult.Ds.Tables[0].Rows[0]);
                    string iktatoKonyvId = iktatoKonyvekResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                    Logger.Debug("Átiktatás iktatás - Iktatókönyv lekérdezés - END");
                    #endregion

                    #region Ügyirat keresése


                    Logger.Debug("Átiktatás iktatás - Ügyirat lekérdezés - BEGIN");
                    string ugyiratId = null;
                    string atiktatandoIratId = null;
                    Result ugyiratResult = null;
                    EREC_UgyUgyiratok foUgyirat = new EREC_UgyUgyiratok();
                    EREC_IraIratok atiktatandoIrat = new EREC_IraIratok();

                    ugyiratResult = Hair.Helper.GetUgyiratByIktatoKonyvFromRequestForFiling(iktatas.AtiktatasAdatok.Iktatoszam, valasz.ValaszAdatok, iktatoKonyvId);

                    if (ugyiratResult.IsError)
                    {
                        continue;
                    }

                    if (ugyiratResult.Ds.Tables.Count > 1)
                    {
                        Logger.Debug("Átiktatás iktatás - Ügyirat lekérdezés - Több találat");
                        Hair.Helper.SetError(valasz, Hair.Helper.Messages.UgyiratTooManyError, -5);
                        continue;
                    }
                    else if ((ugyiratResult.Ds.Tables.Count == 0 || ugyiratResult.Ds.Tables[0].Rows.Count < 1))
                    {
                        Logger.Debug("Átiktatás iktatás - Ügyirat lekérdezés - Nincs találat");
                        Hair.Helper.SetError(valasz, Hair.Helper.Messages.UgyiratNotFoundError, -4);
                        continue;
                    }
                    else if (ugyiratResult.Ds.Tables[0].Rows.Count > 1)
                    {
                        Hair.Helper.SetError(valasz, Hair.Helper.Messages.UgyiratTooManyError, -5);
                        continue;
                    }

                    ugyiratId = ugyiratResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                    Logger.Debug("Átiktatás iktatás - Ügyirat lekérdezés - END");

                    #endregion

                    #region Létező irat keresése, ha nincs, hiba

                    Logger.Debug("Átiktatás iktatás - Irat lekérdezés - BEGIN");

                    Result iratResult = Hair.Helper.SearchIratByUgyiratAndAlszamWithExtension(ugyiratId, iktatas.AtiktatasAdatok.Iktatoszam.Alszam, valasz.ValaszAdatok);

                    if (Hair.Helper.IsError(valasz))
                    {
                        continue;
                    }

                    if (iratResult.Ds.Tables[0].Rows.Count != 1)
                    {
                        Hair.Helper.SetError(valasz, Hair.Helper.Messages.IratTooManyError, -18);
                        errorMail.AppendLine(Hair.Helper.Messages.IratTooManyError + "Külsőazonosító: " + iktatas.Alapadatok.KulsoAzonosito);
                        continue;
                    }

                    atiktatandoIratId = iratResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                    Utility.LoadBusinessDocumentFromDataRow(atiktatandoIrat, iratResult.Ds.Tables[0].Rows[0]);
                    atiktatandoIrat.Updated.SetValueAll(true);
                    atiktatandoIrat.Base.Updated.SetValueAll(false);
                    atiktatandoIrat.Id = null;
                    atiktatandoIrat.Updated.Id = false;
                    EREC_PldIratPeldanyok elsoIratPeldany = null;
                    if (iratResult.Ds.Tables[0].Rows[0]["ElsoIratPeldany_Id"] != DBNull.Value)
                    {
                        elsoIratPeldany = Hair.Helper.GetIratPeldanyById(iratResult.Ds.Tables[0].Rows[0]["ElsoIratPeldany_Id"].ToString());
                    }

                    Logger.Debug("Átiktatás iktatás - Irat lekérdezés - END");
                    #endregion

                    EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    Result result_atiktatas = null;
                    IktatasiParameterek p = Hair.Helper.GetIktatasiParameterkFromRequestForFiling();
                    string ujIktatoKonyvId = null;

                    if (iktatas.IratAdatok.Iktatoszam.FoszamSpecified)
                    {
                        #region Uj Iktatokönyv keresése

                        Logger.Debug("Átiktatás iktatás - Uj Iktatókönyv lekérdezés - BEGIN");
                        EREC_IraIktatoKonyvek ujIratIktatoKonyv = new EREC_IraIktatoKonyvek();
                        string ujUgyiratId = null;
                        Result ujIktatoKonyvekResult = Hair.Helper.GetIktatokonyvFromRequestForFiling(iktatas.IratAdatok.Iktatoszam, valasz.ValaszAdatok);

                        if (ujIktatoKonyvekResult.IsError)
                        {
                            continue;
                        }
                        if (ujIktatoKonyvekResult.Ds.Tables[0].Rows.Count < 1)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIktatoKonyvNotFoundError, -16);
                            continue;
                        }


                        Utility.LoadBusinessDocumentFromDataRow(ujIratIktatoKonyv, ujIktatoKonyvekResult.Ds.Tables[0].Rows[0]);
                        ujIktatoKonyvId = ujIktatoKonyvekResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                        Logger.Debug("Átiktatás iktatás - Uj Iktatókönyv lekérdezés - END");
                        #endregion

                        #region Új ügyirat keresése
                        Logger.Debug("Átiktatás iktatás - Új ügyiirat lekérdezés - START");

                        Result ujUgyiratResult = null;

                        ujUgyiratResult = Hair.Helper.GetUgyiratByIktatoKonyvFromRequestForFiling(iktatas.IratAdatok.Iktatoszam, valasz.ValaszAdatok, ujIktatoKonyvId);

                        if (ujUgyiratResult.IsError)
                        {
                            continue;
                        }

                        if (ujUgyiratResult.Ds.Tables[0].Rows.Count > 1)
                        {
                            Logger.Debug("Átiktatás iktatás - Új Ügyirat lekérdezés - Több találat");
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.UgyiratTooManyError, -5);
                            continue;
                        }
                        else if ((ujUgyiratResult.Ds.Tables.Count == 0 || ujUgyiratResult.Ds.Tables[0].Rows.Count < 1))
                        {
                            Logger.Debug("Átiktatás iktatás - Új Ügyirat lekérdezés - Nincs találat");
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.UgyiratNotFoundError, -4);
                            continue;
                        }
                        else if (ujUgyiratResult.Ds.Tables[0].Rows.Count > 1)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.UgyiratTooManyError, -5);
                            continue;
                        }

                        ujUgyiratId = ujUgyiratResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                        Logger.Debug("Átiktatás iktatás - Új ügyirat lekérdezés - END");
                        #endregion

                        if (iktatas.AtiktatasAdatok.Iktatoszam.Alszam == 1)
                        {
                            p.EmptyUgyiratSztorno = true;
                        }

                        #region Átiktatás alszámra

                        p.UgyiratUjranyitasaHaLezart = true;

                        Logger.Debug("Átiktatás iktatás - Alszámra - START");
                        result_atiktatas = iratokService.IratAtIktatasa_Alszamra(Hair.Helper.GlobalExecParam.Clone(), atiktatandoIratId,
                                                    ujIktatoKonyvId, ujUgyiratId, new EREC_UgyUgyiratdarabok(), atiktatandoIrat, elsoIratPeldany == null ? new EREC_PldIratPeldanyok() : elsoIratPeldany
                                                    , new EREC_HataridosFeladatok(), p);
                        Logger.Debug("Átiktatás iktatás - Alszámra - END");
                        #endregion
                    }
                    else
                    {
                        #region Uj Iktatokönyv keresése

                        Logger.Debug("Átiktatás iktatás - Uj Iktatókönyv lekérdezés - BEGIN");
                        EREC_IraIktatoKonyvek ujIratIktatoKonyv = new EREC_IraIktatoKonyvek();
                        Result ujIktatoKonyvekResult = Hair.Helper.GetIktatokonyvFromRequestForFiling(iktatas.IratAdatok.Iktatoszam, valasz.ValaszAdatok);

                        if (ujIktatoKonyvekResult.IsError)
                        {
                            continue;
                        }
                        if (ujIktatoKonyvekResult.Ds.Tables[0].Rows.Count < 1)
                        {
                            Hair.Helper.SetError(valasz, Hair.Helper.Messages.IktatasIktatoKonyvNotFoundError, -16);
                            continue;
                        }


                        Utility.LoadBusinessDocumentFromDataRow(ujIratIktatoKonyv, ujIktatoKonyvekResult.Ds.Tables[0].Rows[0]);
                        ujIktatoKonyvId = ujIktatoKonyvekResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                        Logger.Debug("Átiktatás iktatás - Uj Iktatókönyv lekérdezés - END");
                        #endregion

                        #region Átiktatás új főszámra

                        EREC_UgyUgyiratok ugyirat = Hair.Helper.GetUgyiratFromRequestForFiling(iktatas, valasz);
                        if (Helper.IsError(valasz))
                        {
                            continue;
                        }
                        Logger.Debug("Átiktatás iktatás - Főszámra - START");
                        result_atiktatas = iratokService.IratAtIktatasa(Hair.Helper.GlobalExecParam.Clone(), atiktatandoIratId,
                                                                            ujIktatoKonyvId, ugyirat, new EREC_UgyUgyiratdarabok(), atiktatandoIrat, elsoIratPeldany == null ? new EREC_PldIratPeldanyok() : elsoIratPeldany
                                                                            , new EREC_HataridosFeladatok(), p);
                        Logger.Debug("Átiktatás iktatás - Főszámra - END");

                        #endregion
                    }

                    #region Valasz adatok 

                    Logger.Debug("Átiktatás iktatás - Válaszadatok - START");

                    if (result_atiktatas.IsError)
                    {
                        valasz.SetError(result_atiktatas);
                        continue;
                    }

                    ErkeztetesIktatasResult iktatottErkeztetesResult = (ErkeztetesIktatasResult)result_atiktatas.Record;

                    if (iktatottErkeztetesResult == null)
                    {
                        valasz.SetError("Átiktatás hiba", -1);
                        continue;
                    }

                    EREC_UgyUgyiratok ugyiratValasz = Hair.Helper.GetUgyiratById(iktatottErkeztetesResult.UgyiratId, valasz);


                    if (Hair.Helper.IsError(valasz))
                    {
                        continue;
                    }

                    EREC_IraIktatoKonyvek ikatatokonyv = Hair.Helper.GetIktatoKonyv(ujIktatoKonyvId, iktatoKonyvek, valasz);

                    if (Hair.Helper.IsError(valasz))
                    {
                        continue;
                    }

                    EREC_IraIratok atiktatottIrat = Helper.GetIrat(iktatottErkeztetesResult.IratId, valasz.ValaszAdatok);

                    if (Hair.Helper.IsError(valasz))
                    {
                        continue;
                    }

                    int alszam;
                    int foszam;
                    if (int.TryParse(atiktatottIrat.Alszam, out alszam))
                    {
                        valasz.Iktatoszam.Alszam = alszam;
                        valasz.Iktatoszam.AlszamSpecified = true;
                        //az iktatás mindig létrehozza az 1-es iratpéldányt, a vonalkódot is arról adjuk vissza
                        valasz.Iktatoszam.Sorszam = 1;
                        valasz.Iktatoszam.SorszamSpecified = true;
                    }

                    if (int.TryParse(ugyiratValasz.Foszam, out foszam))
                    {
                        valasz.Iktatoszam.Foszam = foszam;
                        valasz.Iktatoszam.FoszamSpecified = true;
                    }

                    valasz.Iktatoszam.Elotag = ikatatokonyv.Iktatohely;
                    int ev;
                    if (int.TryParse(ikatatokonyv.Ev, out ev))
                    {
                        valasz.Iktatoszam.Ev = ev;
                        valasz.Iktatoszam.EvSpecified = true;
                    }

                    if (valasz.Vonalkodok == null)
                    {
                        valasz.Vonalkodok = new Hair.KuldemenyVonalkodok();
                    }

                    valasz.Vonalkodok.UgyiratVonalkod = ugyiratValasz.BARCODE;
                    valasz.Vonalkodok.IratpeldanyVonalkod = Hair.Helper.GetRowValueAsString(iratResult.Ds.Tables[0].Rows[0], "ElsoIratPeldanyBarCode");
                    string kuldemenyId = Hair.Helper.GetRowValueAsString(iratResult.Ds.Tables[0].Rows[0], "KuldKuldemenyek_Id");
                    if (!string.IsNullOrEmpty(kuldemenyId))
                    {
                        EREC_KuldKuldemenyek kuldemeny = Hair.Helper.GetKuldemeny(kuldemenyId, valasz);
                        if (Hair.Helper.IsError(valasz))
                        {
                            continue;
                        }

                        valasz.Vonalkodok.KuldemenyVonalkod = kuldemeny.BarCode;

                        if (valasz.ErkeztetoSzam == null)
                            valasz.ErkeztetoSzam = new Iktatas.ErkeztetoSzam();

                        int erkeztetoszam;
                        if (int.TryParse(kuldemeny.Erkezteto_Szam, out erkeztetoszam))
                        {
                            valasz.ErkeztetoSzam.Sorszam = erkeztetoszam;
                            valasz.ErkeztetoSzam.SorszamSpecified = true;
                        }

                        int erkeztetoev;
                        EREC_IraIktatoKonyvek erkeztetokonyv = Hair.Helper.GetIktatoKonyv(kuldemeny.IraIktatokonyv_Id, iktatoKonyvek, valasz);
                        if (int.TryParse(erkeztetokonyv.Ev, out erkeztetoev))
                        {
                            valasz.ErkeztetoSzam.Ev = erkeztetoev;
                            valasz.ErkeztetoSzam.EvSpecified = true;
                        }
                    }

                    Logger.Debug("Átiktatás iktatás - Válaszadatok - END");

                    #endregion

                }
            }
            Logger.Debug("Iktat - Átiktatás END");
            #endregion

            Logger.Debug("HAIR Iktat - Iktatás END");

            if (errorMail.Length > 0)
            {
                Helper.SendErrorMail(errorMail.ToString());
            }

            return valaszok;
        }

        catch (Exception ex)
        {
            Iktatas.Iktatas iktatasHiba = Iktatasok[iktatCounter];
            string kulsoAzon = "";
            if (iktatasHiba != null)
            {
                kulsoAzon = iktatasHiba.Alapadatok.KulsoAzonosito;
            }
            Helper.SetError(valaszok.Valasz[iktatCounter], ex.Message);
            Logger.Error("HAIR Iktat error - Kulsoazonosito: " + kulsoAzon + Environment.NewLine + "Message: " + ex.Message + Environment.NewLine + "Stacktrace: " + ex.StackTrace, ex);
            Logger.Debug("HAIR Iktat - Iktatás END");
            return valaszok;
        }

    }

    #endregion
    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod(RequestElementName = "UjIratpeldanyok", RequestNamespace = IratPeldanyLetrehozas.Constants.NAMESPACE,
                        ResponseElementName = "Valaszok", ResponseNamespace = IratPeldanyLetrehozas.Constants.NAMESPACE)]
    [return: XmlElement("Valaszok", Namespace = IratPeldanyLetrehozas.Constants.NAMESPACE)]
    public IratPeldanyLetrehozas.Valaszok IratpeldanytLetrehoz([XmlElement("UjIratpeldany")]
                                                                IratPeldanyLetrehozas.UjIratpeldany[] UjIratpeldanyok)
    {
        return new IntServiceIratPeldanyLetrehozas().IratpeldanytLetrehoz(UjIratpeldanyok);
    }
    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod(RequestElementName = "Postazasok", RequestNamespace = Postazas.Constants.NAMESPACE,
                        ResponseElementName = "Valaszok", ResponseNamespace = Postazas.Constants.NAMESPACE)]
    [return: XmlElement("Valaszok", Namespace = Postazas.Constants.NAMESPACE)]

    public Postazas.Valaszok Postaz([XmlElement("Postazas")]
                                     Postazas.Postazas[] Postazasok)
    {
        return new IntServicePostaz().Postaz(Postazasok);
    }
    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod(RequestElementName = "Tertivevenyek", RequestNamespace = TertivevenyErkeztetes.Constants.NAMESPACE,
                        ResponseElementName = "Valaszok", ResponseNamespace = TertivevenyErkeztetes.Constants.NAMESPACE)]
    [return: XmlElement("Valaszok", Namespace = TertivevenyErkeztetes.Constants.NAMESPACE)]
    public TertivevenyErkeztetes.Valaszok TertivevenytErkeztet([XmlElement("Tertiveveny")]
                                                              TertivevenyErkeztetes.Tertiveveny[] Tertivevenyek)
    {
        if (Tertivevenyek == null)
        {
            throw new SoapException(Helper.Messages.SoapRequestError, SoapException.ClientFaultCode);
        }

        var valaszok = new TertivevenyErkeztetes.Valaszok()
        {
            Valasz = new TertivevenyErkeztetes.Valasz[Tertivevenyek.Length]
        };

        for (int i = 0; i < Tertivevenyek.Length; i++)
        {
            var tertiveveny = Tertivevenyek[i];
            var valasz = valaszok.Valasz[i] = Helper.InitValasz<TertivevenyErkeztetes.Valasz>(tertiveveny.Alapadatok);
            var execParam = Helper.GlobalExecParam.Clone();

            tertiveveny.Erkeztetes(execParam, valasz);
        }

        return valaszok;
    }
    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod(RequestElementName = "Csatolmanyok", RequestNamespace = CsatolmanyFeltoltes.Constants.NAMESPACE,
                        ResponseElementName = "Valaszok", ResponseNamespace = CsatolmanyFeltoltes.Constants.NAMESPACE)]
    [return: XmlElement("Valaszok", Namespace = CsatolmanyFeltoltes.Constants.NAMESPACE)]
    public CsatolmanyFeltoltes.Valaszok CsatolmanytFeltolt([XmlElement("Csatolmany")]
                                                           CsatolmanyFeltoltes.Csatolmany[] Csatolmanyok)
    {
        Logger.Debug("CsatolmanyFeltoltes - Csatolmány feltöltés START");

        CsatolmanyFeltoltes.Valaszok valaszResult = new CsatolmanyFeltoltes.Valaszok();
        valaszResult.Valasz = new CsatolmanyFeltoltes.Valasz[Csatolmanyok.Length];

        BaseIntHairService service = new BaseIntHairService();
        for (int index = 0; index < Csatolmanyok.Length; index++)
        {
            var csatolmany = Csatolmanyok[index];

            string dokumentumid = null;

            CsatolmanyFeltoltes.Valasz valasz = new CsatolmanyFeltoltes.Valasz();
            valasz.KulsoAzonosito = csatolmany.Alapadatok.KulsoAzonosito;
            valasz.ValaszAdatok.HibaKod = 0;
            valaszResult.Valasz[index] = valasz;

            Result result = service.GetIrat(csatolmany.IratAdatok.Iktatoszam);
            if (result.IsError || !Utility.HasData(result))
            {
                valasz.ValaszAdatok.HibaKod = ErrorCodes.IratNotFound;
                valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.IratNotFound);
                continue;
            }

            EREC_IraIratok irat = new EREC_IraIratok();
            Utils.LoadBusinessDocumentFromDataRow(irat, result.Ds.Tables[0].Rows[0]);

            Logger.Debug(string.Format("CsatolmanyFeltoltes - Irat: {0}, Dokumentum: {1} ", irat.Azonosito, csatolmany.FileAdatok.FajlNev));

            KRT_DokumentumokService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

            //ha már van ilyen dokumentum, nem töltjük fel újra                      
            Result getDokResult = service.GetHAIRDokumentum(csatolmany.FileAdatok.Url);

            if (!getDokResult.IsError && Utility.HasData(getDokResult))
            {
                KRT_Dokumentumok dokumentum_res = new KRT_Dokumentumok();
                Utils.LoadBusinessDocumentFromDataRow(dokumentum_res, getDokResult.Ds.Tables[0].Rows[0]);
                dokumentumid = dokumentum_res.Id;
                Logger.Debug(string.Format("CsatolmanyFeltoltes - A dokumentum már fel volt töltve. Dokumentum azonosito: {0} ,id: {1} ", csatolmany.FileAdatok.Url, dokumentumid));
            }
            else
            {
                KRT_Dokumentumok dokumentum = new KRT_Dokumentumok();
                dokumentum.Org = "";
                dokumentum.FajlNev = csatolmany.FileAdatok.FajlNev;
                dokumentum.VerzioJel = "1";
                dokumentum.Nev = "NULL";
                dokumentum.Tipus = "PDF (Adobe)";
                dokumentum.Formatum = "pdf";
                dokumentum.Leiras = csatolmany.FileAdatok.FajlNev;
                dokumentum.External_Source = "HAIR";
                dokumentum.External_Link = csatolmany.FileAdatok.Url;
                dokumentum.ElektronikusAlairas = csatolmany.FileAdatok.Alairt ? "1" : "0";
                dokumentum.Titkositas = "0";
                dokumentum.Csoport_Id_Tulaj = Helper.GlobalExecParam.Felhasznalo_Id;
                dokumentum.Allapot = "1";
                Result insertResult = documentService.Insert(Helper.GlobalExecParam, dokumentum);

                if (insertResult.IsError)
                {
                    Logger.Debug(string.Format("CsatolmanyFeltoltes - Hiba a dokumentum feltöltéskor"));
                    valasz.ValaszAdatok.HibaKod = ErrorCodes.InternalError;
                    valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.InternalErrorAtItem) + csatolmany.IratAdatok.Iktatoszam;
                    continue;
                }

                dokumentumid = insertResult.Uid;
            }

            EREC_CsatolmanyokService csatolmanyokService = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            //ha már a dokumentum hozzá van kötve az irathoz nem kötjük hozzá újra
            Result getCsatResult = service.GetCsatolmany(irat.Id, dokumentumid);

            if (!getCsatResult.IsError && Utility.HasData(getCsatResult))
            {
                valasz.ValaszAdatok.HibaKod = ErrorCodes.DocumentAlreadyUploaded;
                valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.DocumentAlreadyUploaded);
                continue;
            }
            else
            {
                EREC_Csatolmanyok erecCsatolmany = new EREC_Csatolmanyok();
                erecCsatolmany.IraIrat_Id = irat.Id;
                erecCsatolmany.Dokumentum_Id = dokumentumid;
                erecCsatolmany.DokumentumSzerep = "04";
                Result csatolmanyInsertResult = csatolmanyokService.Insert(Helper.GlobalExecParam, erecCsatolmany);
                if (csatolmanyInsertResult.IsError)
                {
                    valasz.ValaszAdatok.HibaKod = ErrorCodes.InternalError;
                    valasz.ValaszAdatok.HibaUzenet = ErrorCodes.GetErrorMessage(ErrorCodes.InternalErrorAtItem) + csatolmany.IratAdatok.Iktatoszam;
                    continue;
                }
            }
        }

        Logger.Debug("CsatolmanyFeltoltes - Csatolmány feltöltés STOP");

        return valaszResult;
    }
    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod(RequestElementName = "Iratok", RequestNamespace = IratModositas.Constants.NAMESPACE,
                        ResponseElementName = "Valaszok", ResponseNamespace = IratModositas.Constants.NAMESPACE)]
    [return: XmlElement("Valaszok", Namespace = IratModositas.Constants.NAMESPACE)]
    public IratModositas.Valaszok IratotModosit([XmlElement("Irat")]
                                                IratModositas.Irat[] Iratok)
    {
        if (Iratok == null)
        {
            throw new SoapException(Helper.Messages.SoapRequestError, SoapException.ClientFaultCode);
        }

        var valaszok = new IratModositas.Valaszok()
        {
            Valasz = new IratModositas.Valasz[Iratok.Length]
        };

        for (int i = 0; i < Iratok.Length; i++)
        {
            var irat = Iratok[i];
            var valasz = valaszok.Valasz[i] = Helper.InitValasz<IratModositas.Valasz>(irat.Alapadatok);
            var execParam = Helper.GlobalExecParam.Clone();

            irat.Modositas(execParam, valasz);
        }

        return valaszok;
    }
    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod(RequestElementName = "Ugyek", RequestNamespace = UgyModositas.Constants.NAMESPACE,
                        ResponseElementName = "Valaszok", ResponseNamespace = UgyModositas.Constants.NAMESPACE)]
    [return: XmlElement("Valaszok", Namespace = UgyModositas.Constants.NAMESPACE)]
    public UgyModositas.Valaszok UgyetModosit([XmlElement("Ugy")]
                                              UgyModositas.Ugy[] Ugyek)
    {
        if (Ugyek == null)
        {
            throw new SoapException(Helper.Messages.SoapRequestError, SoapException.ClientFaultCode);
        }

        var valaszok = new UgyModositas.Valaszok()
        {
            Valasz = new UgyModositas.Valasz[Ugyek.Length]
        };

        for (int i = 0; i < Ugyek.Length; i++)
        {
            var ugy = Ugyek[i];
            var valasz = valaszok.Valasz[i] = Helper.InitValasz<UgyModositas.Valasz>(ugy.Alapadatok);
            var execParam = Helper.GlobalExecParam.Clone();

            ugy.Modositas(execParam, valasz);
        }

        return valaszok;
    }
    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod(RequestElementName = "Partnerek", RequestNamespace = PartnerModositas.Constants.NAMESPACE,
                        ResponseElementName = "Valaszok", ResponseNamespace = PartnerModositas.Constants.NAMESPACE)]
    [return: XmlElement("Valaszok", Namespace = PartnerModositas.Constants.NAMESPACE)]
    public PartnerModositas.Valaszok PartnertModosit([XmlElement("Partner")]
                                                     PartnerModositas.Partner[] Partnerek)
    {
        Logger.Debug("INT_HAIRService.PartnertModosit start");
        string[] adoazonositoArray = Partnerek.Select(p => { return p.PartnerAzonosito; }).ToArray();
        var valaszokList = new List<PartnerModositas.Valasz>();

        var hairAdatokService = eIntegratorService.ServiceFactory.GetINT_HAIRAdatokService();
        var execParam = Helper.GlobalExecParam.Clone();

        //KRT_Partnerek objtipus id lekérése
        Result objTipusResult = PartnerSyncHelper.GetObjTipusFromObjectKod("KRT_Partnerek");
        KRT_ObjTipusok objTipus = new KRT_ObjTipusok();
        Utility.LoadBusinessDocumentFromDataRow(objTipus, objTipusResult.Ds.Tables[0].Rows[0]);

        var hairConnector = new ConnectorFactory().Connector;
        foreach (string adoazon in adoazonositoArray)
        {
            Result partnerGetResult = PartnerSyncHelper.GetPartnerByKulsoAzonosito(adoazon);
            if (partnerGetResult.IsError)
            {
                valaszokList.Add(ValaszFactory.GetErrorValasz<PartnerModositas.Valasz>(adoazon, partnerGetResult));
                continue;
            }
            if (!Utility.HasData(partnerGetResult)) //nincs ilyen partner akkor kell szinkronizálnunk
            {
                valaszokList.Add(SnycNewPartner(hairConnector, adoazon, objTipus.Id));
            }
            else  // van ilyen partner ezért bejegyezzük az 1-es értékkel a HAIR_Adatok táblába amivel jelezzük, hogy szinkronizálni kell majd
            {
                valaszokList.Add(MarkPartnerToBeSyncedLater<PartnerModositas.Valasz>(adoazon, partnerGetResult, objTipus.Id));
            }
        }
        return new PartnerModositas.Valaszok()
        {
            Valasz = valaszokList.ToArray()
        };
    }

    private PartnerModositas.Valasz SnycNewPartner(IConnector connector, string adoazon, string partnerTipusObjectId)
    {
        List<HAIRUgyfelMapped> syncResult;
        try
        {
            syncResult = new HAIRPartnerSyncer().StartSync(Helper.GlobalExecParam, connector, new string[] { adoazon });
        }
        catch (HAIRCannotConnectException err)
        {
            return (ValaszFactory.GetErrorValasz<PartnerModositas.Valasz>(adoazon, new Result() { ErrorCode = ErrorCodes.CannotConnectToHAIR.ToString(), ErrorMessage = "Nem lehet a HAIR-hoz kapcsolódni: " + connector.ServerAddress }));
        }
        if (syncResult.First().ErrorResults.Count > 0)
        {
            return (ValaszFactory.GetErrorValasz<PartnerModositas.Valasz>(adoazon, syncResult.First().ErrorResults));

        }
        Result insertResult = Helper.UpdateOrInsertHAIRAdatok(syncResult.First().KRT_Partner.Id, partnerTipusObjectId, "02", "0");
        if (insertResult.IsError)
        {
            return (ValaszFactory.GetErrorValasz<PartnerModositas.Valasz>(adoazon, insertResult));
        }
        return (ValaszFactory.GetSuccessValasz<PartnerModositas.Valasz>(adoazon));
    }

    [LogExtension]
    [WebMethod]
    [SoapDocumentMethod(//RequestElementName = "PartnerekJSON", RequestNamespace = PartnerModositas.Constants.NAMESPACE,
                         ResponseElementName = "Valaszok", ResponseNamespace = InitialSync.Constants.NAMESPACE)]
    [return: XmlElement("Valaszok", Namespace = InitialSync.Constants.NAMESPACE)]
    public PartnerModositas.Valaszok InitialSyncFromJSON([XmlElement("PartnerekJSON")]
                                                    string partnerekJSON)
    {
        var valaszokList = new List<PartnerModositas.Valasz>();

        Logger.Debug("INT_HAIRService.InitialSync start");

        var execParam = Helper.GlobalExecParam.Clone();

        List<HAIRUgyfelMapped> syncResult;
        try
        {
            syncResult = new HAIRPartnerSyncer().StartSyncFromJSON(Helper.GlobalExecParam, partnerekJSON);
            foreach (HAIRUgyfelMapped mapped in syncResult)
            {
                if (mapped.ErrorResults.Count > 0)
                {
                    valaszokList.Add(ValaszFactory.GetErrorValasz<PartnerModositas.Valasz>(mapped.hairUgyfel.adoaz, mapped.ErrorResults));
                }
            }
        }
        catch (Exception err)
        {
            valaszokList.Add(ValaszFactory.GetErrorValasz<PartnerModositas.Valasz>("", new Result() { ErrorCode = ErrorCodes.InternalError.ToString(), ErrorMessage = err.Message + "\n" + err.StackTrace }));
        }
        return new PartnerModositas.Valaszok()
        {
            Valasz = valaszokList.ToArray()
        };
    }

    private T MarkPartnerToBeSyncedLater<T>(string adoazon, Result partnerGetResult, string partnerTipusObjectId) where T : Valasz, new()
    {
        KRT_Partnerek partner = new KRT_Partnerek();
        Utility.LoadBusinessDocumentFromDataRow(partner, partnerGetResult.Ds.Tables[0].Rows[0]);

        Result secondInsert = Helper.UpdateOrInsertHAIRAdatok(partner.Id, partnerTipusObjectId, "02", "1");
        if (secondInsert.IsError)
        {
            return (ValaszFactory.GetErrorValasz<T>(adoazon, secondInsert));
        }
        return (ValaszFactory.GetSuccessValasz<T>(adoazon));
    }
}
