﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using Contentum.eIntegrator.WebService.NAP;
using Contentum.eIntegrator.WebService.NAP.Config;
using Contentum.eIntegrator.WebService.NAP.Kodszotarak;
using Contentum.eIntegrator.WebService.NAP.ServiceReference;
using S001 = Contentum.eIntegrator.WebService.NAP.ServiceReference.S001;
using Contentum.eIntegrator.WebService.NAP.Utils;
using System;
using System.Collections.Generic;
using System.Web.Services;
using Contentum.eIntegrator.Service.NAP.Results;
using Contentum.eIntegrator.Service.NAP;
using System.Linq;

[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class INT_NAPService: System.Web.Services.WebService
{
    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(List<Meghatalmazas>))]
    public MeghatalmazasokResult GetMeghatalmazasok(ExecParam execParam, MeghatalmazasokRequest request)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        MeghatalmazasokResult result = new MeghatalmazasokResult();

        try
        {
            NAPLekerdezesService _NAPLekerdezesService = NAPLekerdezesServiceFactory.GetONYPLekerdezesService(ConfigManager.GetServiceParameters());

#if test
            LekerdezesParameters lekerdezesParameters = ConfigManager.GetTesztLekerdezesParameters();
#else
            LekerdezesParameters lekerdezesParameters  = ConfigManager.GetLekerdezesParameters();
#endif


            NAPLekerdezesCreator lekerdezesCreator = new NAPLekerdezesCreator(lekerdezesParameters);

            lekerdezesCreator.CheckAzonositoAdat(request.AzonositoAdat);

            NAPLekerdezesKeres keres = lekerdezesCreator.GetLekerdezesKeres();

            S001.s001 s001Lekerdezes = lekerdezesCreator.GetRendelkezesLekerdezes(request.AzonositoAdat, 
                EdokWrapper.GetFelhasznaloNev(execParam));

            if (request.Tipus != MeghatalmazasTipusok.Osszes)
            {
                lekerdezesCreator.AddKertRendelkezesAdatok(s001Lekerdezes,
                    RendelkezesLekerdezesTipusok.NamespaceAlapu,
                    Converter.ConvertToSzereplo(request.Szereplo),
                    Converter.ConvertToRendelkezesTipus(request.Tipus));
            }
            else
            {
                List<MeghatalmazasTipusok> meghatalmazasTipusok = MeghatalmazasTipusokHelper.GetOsszesMeghatalmazasTipus();
                meghatalmazasTipusok.Remove(MeghatalmazasTipusok.Osszes);
                foreach (MeghatalmazasTipusok tipus in meghatalmazasTipusok)
                {
                    lekerdezesCreator.AddKertRendelkezesAdatok(s001Lekerdezes,
                    RendelkezesLekerdezesTipusok.NamespaceAlapu,
                    Converter.ConvertToSzereplo(request.Szereplo),
                    Converter.ConvertToRendelkezesTipus(tipus));
                }
            }
 

            if (request.AzonositoAdat.SzemelyAdatok != null)
            {
                lekerdezesCreator.SetMivelKerdezSzemelyAdat(s001Lekerdezes, request.AzonositoAdat.SzemelyAdatok);
            }
            else if (!String.IsNullOrEmpty(request.AzonositoAdat.TitkosKapcsolatiKod))
            {
                lekerdezesCreator.SetMivelKerdezTitkosKapcsolatikod(s001Lekerdezes, request.AzonositoAdat.TitkosKapcsolatiKod);
            }

            lekerdezesCreator.AddLekerdezesToKeres(keres, s001Lekerdezes);

            NAPLekerdezesValasz lekerdezesValasz = _NAPLekerdezesService.NAPLekerdezes(keres);
            NAPLekerdezesValaszParser valaszParser = new NAPLekerdezesValaszParser(lekerdezesValasz);
            result = valaszParser.GetResult<MeghatalmazasokResult>();

            if (!result.IsError)
            {
                result.MeghatalmazasokList = valaszParser.GetMeghatalmazasok();
            }
        }
        catch (Exception ex)
        {
            result = NAPLekerdezesValaszParser.GetResultFromException<MeghatalmazasokResult>(ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(List<Meghatalmazas>))]
    public AlapRendelkezesekResult GetAlapRendelkezesek(ExecParam execParam, AzonositoAdat azonositoAdat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        AlapRendelkezesekResult result = new AlapRendelkezesekResult();

        try
        {
            NAPLekerdezesService _NAPLekerdezesService = NAPLekerdezesServiceFactory.GetONYPLekerdezesService(ConfigManager.GetServiceParameters());

#if test
            LekerdezesParameters lekerdezesParameters = ConfigManager.GetTesztLekerdezesParameters();
#else
            LekerdezesParameters lekerdezesParameters  = ConfigManager.GetLekerdezesParameters();
#endif

            NAPLekerdezesCreator lekerdezesCreator = new NAPLekerdezesCreator(lekerdezesParameters);

            lekerdezesCreator.CheckAzonositoAdat(azonositoAdat);

            NAPLekerdezesKeres keres = lekerdezesCreator.GetLekerdezesKeres();

            S001.s001 s001Lekerdezes = lekerdezesCreator.GetRendelkezesLekerdezes(azonositoAdat,
                EdokWrapper.GetFelhasznaloNev(execParam));

            lekerdezesCreator.AddKertRendelkezesAdatok(s001Lekerdezes,
                RendelkezesLekerdezesTipusok.NamespaceAlapu, 
                S001.Szereplo.kire, 
                RendelkezesTipusok.AlapRendelkezes);


            if (azonositoAdat.SzemelyAdatok != null)
            {
                lekerdezesCreator.SetMivelKerdezSzemelyAdat(s001Lekerdezes, azonositoAdat.SzemelyAdatok);
            }
            else if (!String.IsNullOrEmpty(azonositoAdat.TitkosKapcsolatiKod))
            {
                lekerdezesCreator.SetMivelKerdezTitkosKapcsolatikod(s001Lekerdezes, azonositoAdat.TitkosKapcsolatiKod);
            }

            lekerdezesCreator.AddLekerdezesToKeres(keres, s001Lekerdezes);

            NAPLekerdezesValasz lekerdezesValasz = _NAPLekerdezesService.NAPLekerdezes(keres);
            NAPLekerdezesValaszParser valaszParser = new NAPLekerdezesValaszParser(lekerdezesValasz);
            result = valaszParser.GetResult<AlapRendelkezesekResult>();

            if (!result.IsError)
            {
                result.ElerhetosegekList = valaszParser.GetElerhetosegek();
            }
        }
        catch (Exception ex)
        {
            result = NAPLekerdezesValaszParser.GetResultFromException<AlapRendelkezesekResult>(ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }
}