﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service.ONYP.BusinessObjects;
using Contentum.eIntegrator.WebService.ONYP;
using Contentum.eIntegrator.WebService.ONYP.Config;
using Contentum.eIntegrator.WebService.ONYP.RendelkezesTipusok;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for INT_ONYPService
/// </summary>
[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class INT_ONYPService : System.Web.Services.WebService
{
    ONYPLekerdezesService _ONYPLekerdezesService = ONYPLekerdezesServiceFactory.GetONYPLekerdezesService(
        ConfigManager.GetServiceFactoryParameters());

    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(List<KRT_Cimek>))]
    public Result GetAlapRendelkezesek(ExecParam execParam, Szemely4TAdatok azonositoAdat, string titkosKapcsolatiKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            ONYPLekerdezesKeres keres = ONYPHelper.GetKeres(ONYPConstants.MuveletKod.L002,
                ONYPConstants.Cel.CK001,
                ONYPConstants.RendelkezesTipus.Alaprendelkezes);
            if (azonositoAdat != null)
            {
                keres.Lekerdezes.Item = ONYPHelper.GetSzemelyAdat(azonositoAdat);
            }
            if (!String.IsNullOrEmpty(titkosKapcsolatiKod))
            {
                keres.Lekerdezes.Item = ONYPHelper.GetTitkosKapcsolatiKod(titkosKapcsolatiKod);
            }
            ONYPLekerdezesValasz lekerdezesValasz = _ONYPLekerdezesService.ONYPLekerdezes(keres);
            result = ONYPHelper.GetResult(lekerdezesValasz);
            result.Record = GetCimek(lekerdezesValasz);
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    List<KRT_Cimek> GetCimek(ONYPLekerdezesValasz lekerdezesValasz)
    {
        List<KRT_Cimek> cimek = new List<KRT_Cimek>();

        ONYPLekerdezesValaszValasz valasz = lekerdezesValasz.Item as ONYPLekerdezesValaszValasz;
        if (valasz != null)
        {
            if (valasz.Rendelkezesek != null)
            {
                foreach (Rendelkezes rendelkezes in valasz.Rendelkezesek)
                {
                    cimek.AddRange(GetCim(rendelkezes));
                }
            }
        }

        return cimek;
    }

    List<KRT_Cimek> GetCim(Rendelkezes rendelkezes)
    {
        List<KRT_Cimek> cimek = new List<KRT_Cimek>();
        alapRendelkezes alapRendelkezes = ONYPHelper.DeserializeAlapRendelkezes(rendelkezes.rendelkezesXML);

        if (alapRendelkezes != null)
        {
            if (!String.IsNullOrEmpty(alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek.emailCim))
            {
                KRT_Cimek cim = new KRT_Cimek();
                cim.Forras = KodTarak.Cim_Forras.RNY;
                cim.Tipus = KodTarak.Cim_Tipus.Email_Cim_RNY;
                cim.CimTobbi = alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek.emailCim;
                cimek.Add(cim);
            }
            if (alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek.elsodlegesTelefon != null)
            {
                if (!String.IsNullOrEmpty(alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek.elsodlegesTelefon))
                {
                    KRT_Cimek cim = new KRT_Cimek();
                    cim.Forras = KodTarak.Cim_Forras.RNY;
                    cim.Tipus = KodTarak.Cim_Tipus.Telefonszam_RNY;
                    cim.CimTobbi = alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek.elsodlegesTelefon;
                    cimek.Add(cim);
                }
            }
           /* if (alapRendelkezes.postaiElerhetoseg != null) A naphoz tartozó 3.0 ás xsd ben az alaprendelkezésben ezek a mezők már nem találhatóak meg
            {
                KRT_Cimek cim = new KRT_Cimek();
                cim.Forras = KodTarak.Cim_Forras.RNY;
                cim.Tipus = KodTarak.Cim_Tipus.Postai_Cim_RNY;
                cim.IRSZ = alapRendelkezes.postaiElerhetoseg.irsz;
                cim.TelepulesNev = alapRendelkezes.postaiElerhetoseg.varos;
                cim.KozteruletNev = alapRendelkezes.postaiElerhetoseg.kozteruletNeve;
                cim.KozteruletTipusNev = alapRendelkezes.postaiElerhetoseg.kozteruletJellege;
                cim.Hazszam = alapRendelkezes.postaiElerhetoseg.hazszam;
                cim.Szint = alapRendelkezes.postaiElerhetoseg.emelet;
                cim.Ajto = alapRendelkezes.postaiElerhetoseg.ajto;
                cim.HRSZ = alapRendelkezes.postaiElerhetoseg.hrsz;
                cimek.Add(cim);
            }*/
        }

        return cimek;
    }

    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(List<Meghatalmazas>))]
    public Result GetMeghatalmazasok(ExecParam execParam, Szemely4TAdatok azonositoAdat, string titkosKapcsolatiKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            ONYPLekerdezesKeres keres = ONYPHelper.GetKeres(ONYPConstants.MuveletKod.L002,
                ONYPConstants.Cel.CK001,
                ONYPConstants.RendelkezesTipus.AltalanosMeghatalmazas);
            if (azonositoAdat != null)
            {
                keres.Lekerdezes.Item = ONYPHelper.GetSzemelyAdat(azonositoAdat);
            }
            if (!String.IsNullOrEmpty(titkosKapcsolatiKod))
            {
                keres.Lekerdezes.Item = ONYPHelper.GetTitkosKapcsolatiKod(titkosKapcsolatiKod);
            }
            ONYPLekerdezesValasz lekerdezesValasz = _ONYPLekerdezesService.ONYPLekerdezes(keres);
            result = ONYPHelper.GetResult(lekerdezesValasz);
            result.Record = GetMeghatalmazasok(lekerdezesValasz);
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    List<Meghatalmazas> GetMeghatalmazasok(ONYPLekerdezesValasz lekerdezesValasz)
    {
        List<Meghatalmazas> meghatalmazasok = new List<Meghatalmazas>();

        ONYPLekerdezesValaszValasz valasz = lekerdezesValasz.Item as ONYPLekerdezesValaszValasz;
        if (valasz != null)
        {
            if (valasz.Rendelkezesek != null)
            {
                foreach (Rendelkezes rendelkezes in valasz.Rendelkezesek)
                {
                    meghatalmazasok.Add(GetMeghatalmazas(rendelkezes));
                }
            }
        }

        return meghatalmazasok;
    }

    Meghatalmazas GetMeghatalmazas(Rendelkezes rendelkezes)
    {
        Meghatalmazas meghatalmazas = new Meghatalmazas();
        meghatalmazas.Id = rendelkezes.rendelkezesId;
        meghatalmazas.Azonosito = rendelkezes.rendelkezesAzonosito;
        meghatalmazas.Tipus = rendelkezes.rendelkezesNamespace;
        meghatalmazas.RendelkezesXML = rendelkezes.rendelkezesXML;

        if (rendelkezes.ki != null)
            meghatalmazas.Ki = ONYPHelper.GetNev(rendelkezes.ki);

        if (rendelkezes.kit != null && rendelkezes.kit.Length > 0)
            meghatalmazas.Kit = ONYPHelper.GetNev(rendelkezes.kit[0]);

        if (rendelkezes.kire != null)
            meghatalmazas.Kire = ONYPHelper.GetNev(rendelkezes.kire);

        meghatalmazas.HatalyKezdete = rendelkezes.ervenyessegKezdete;

        if (rendelkezes.ervenyessegVegeSpecified)
        {
            meghatalmazas.HatalyVege = rendelkezes.ervenyessegVege;
        }
        else if (rendelkezes.visszavonasDatumaSpecified)
        {
            meghatalmazas.HatalyVege = rendelkezes.visszavonasDatuma;
        }

        return meghatalmazas;

    }
}
