﻿using System;
using System.Web.Services;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using System.Reflection;
using System.Web.Script.Services;
using Contentum.eIntegrator.Service;
using System.Data;

// CR3234: PIR interface
// Általános PIR szervíz
// Ez példányosítja a megfelelő specifikus PIR interface-t (pl EcoStat) a PIR_Rendszer paraméter alapján

[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public partial class INT_PIRService : System.Web.Services.WebService
{
    //#region Varible declaration

    private DataContext dataContext;

    private static string _pir_rendszer;
    public static string PIR_Rendszer
    {
        get { return _pir_rendszer; }
        set { _pir_rendszer = value; }
    }
       
    //private PIRExternalService pirExternalService;
    private static INT_PIRInterface pirRendszer;

    public INT_PIRService()
    {

          dataContext = new DataContext(this.Application);

    }

 
    [WebMethod()]
    [ScriptMethod(UseHttpGet = true)]
    public Result PirSzamlaFejFelvitel(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla)
    {
        return pirRendszer.PirSzamlaFejFelvitel(ExecParam, pirEdokSzamla, Context, dataContext);
    }

    [WebMethod()]
    [ScriptMethod(UseHttpGet = true)]
    public bool SetPIR_Rendszer(String pirRendszerType)
    {
        PIR_Rendszer = pirRendszerType;

        if (String.IsNullOrEmpty(PIR_Rendszer))
        {
            throw new Exception("PIR típus nélkül nem lehet meghívni a WS-t!");
        }

        if (PIR_Rendszer.ToUpper().Equals("ECOSTAT"))
        {
            Assembly ass = Assembly.Load("Contentum.eIntegrator.Service");
            Type myType = ass.GetType("Contentum.eIntegrator.Service.EcoStat");
            if (myType != null)
            {
                MethodInfo myMethod = myType.GetMethod("PirSzamlaFejFelvitel");
                if (myMethod != null)
                {
                    object instance = Activator.CreateInstance(myType);
                    pirRendszer = (INT_PIRInterface)instance;
                }
                else
                {
                    throw new Exception("Hiányzó PIR interface: " + pirRendszerType);
                }
            }
            else
            {
                throw new Exception("Hiányzó PIR interface: " + pirRendszerType);
            }

            //pirRendszer = (INT_PIRInterface)new EcoStat();
        }
        else if(PIR_Rendszer.ToUpper().Equals("PROCUSYS"))
        {
            Assembly ass = Assembly.Load("Contentum.eIntegrator.Service");
            Type myType = ass.GetType("Contentum.eIntegrator.Service.Procusys");
            if (myType != null)
            {
                MethodInfo myMethod = myType.GetMethod("PirSzamlaFejFelvitel");
                if (myMethod != null)
                {
                    object instance = Activator.CreateInstance(myType);
                    pirRendszer = (INT_PIRInterface)instance;
                }
                else
                {
                    throw new Exception("Hiányzó PIR interface: " + pirRendszerType);
                }
            }
            else
            {
                throw new Exception("Hiányzó PIR interface: " + pirRendszerType);
            }

        }
        else
        {
            throw new Exception("Ismeretlen PIR típus!");
        }

        return true;
    }

    [WebMethod()]
    [ScriptMethod(UseHttpGet = true)]
    public Result PirSzamlaFejModositas(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla)
    {
        return pirRendszer.PirSzamlaFejModositas(ExecParam, pirEdokSzamla, Context, dataContext);
    }

    [WebMethod()]
    [ScriptMethod(UseHttpGet = true)]
    public Result PirSzamlaFejSztorno(ExecParam ExecParam, String Szamlaszam)
    {
        
        // CR3412 ideiglenes/gyors javítás
        // Patch küldéskor a végleges javítás megy ki (fordítós)
        //return new Result();

        // CR3412 Számla sztornózás javítás
        return pirRendszer.PirSzamlaFejSztorno(ExecParam, Szamlaszam, Context, dataContext);
        // throw new Exception("NOT IMPLEMENTED (PirSzamlaFejSztorno)");
    }

    [WebMethod()]
    [ScriptMethod(UseHttpGet = true)]
    public Result PirSzamlaFejAllapot(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla)
    {
        return pirRendszer.PirSzamlaFejAllapot(ExecParam, pirEdokSzamla, Context, dataContext);
    }

    [WebMethod()]
    public Result PirSzamlaKepFeltoltes(ExecParam ExecParam, PIRFileContent image)
    {
        return pirRendszer.PirSzamlaKepFeltoltes(ExecParam, image, Context, dataContext);
    }

    [WebMethod()]
    public Result PirSzamlaExportToCsv(ExecParam ExecParam)
    {
        return pirRendszer.PirSzamlaExportToCsv(ExecParam, Context, dataContext);
    }


    /// <summary>
    /// a szállítói számlák érkeztető táblájába felvitt tételek állapotának lekérdezése.
    /// - Ha ki van töltve a dátum, akkor az adott dátum óta történt állapotváltozásokat adja vissza, függetlenül attól, hogy azok az állapotok le lettek-e már kérdezve.
    /// - Ha nincs kitöltve a dátum, akkor az eddig még nem lekérdezett állapot változásokat adja vissza a program
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns>nx2-es táblázat: Vonalkód	| Állapot</returns>
    [WebMethod()]
    public Result PirSzamlaFejAllapotValtozas(ExecParam ExecParam, DateTime datum)
    {
        return pirRendszer.PirSzamlaFejAllapotValtozas(ExecParam, datum, Context, dataContext);
    }

    [WebMethod]
    public string PirSzamlaKepFeltoltesJob()
    {
         SzamlakManager manager = new SzamlakManager(this.dataContext);
         Result res = manager.PirSzamlaKepFeltoltes();

         if (res.IsError)
         {
             return ResultError.GetErrorMessageFromResultObject(res);
         }
         else
         {
             return "OK";
         }
    }
}