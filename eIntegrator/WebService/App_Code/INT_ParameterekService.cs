﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for INT_ParameterekService
/// </summary>
[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class INT_ParameterekService : System.Web.Services.WebService
{
   // private INT_ParameterekStoredProcedure sp = null;

    //private DataContext dataContext;

    //public INT_ParameterekService()
    //{
    //    dataContext = new DataContext(this.Application);
    //    sp = new INT_ParameterekStoredProcedure(dataContext);
    //}

    //public INT_ParameterekService(DataContext _dataContext)
    //{
    //    this.dataContext = _dataContext;
    //    sp = new INT_ParameterekStoredProcedure(dataContext);
    //}

    public Result GetByNev(ExecParam ExecParam, string nev)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetByNev(ExecParam, nev);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod]
    public Result GetByModulNev(ExecParam ExecParam, string modulNev)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetByModulNev(ExecParam, modulNev);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}