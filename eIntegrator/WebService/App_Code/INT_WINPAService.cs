﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data;
using System.Collections.Generic;
//using Contentum.eIntegrator.ADAdapter;
using Contentum.eRecord.Service;
using System.Data.SqlClient;
using System.Configuration;

[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class INT_WINPAService : System.Web.Services.WebService
{
    private INT_WINPAStoredProcedure sp = null;
    public static string Org_id;
    private DataContext dataContext;
    private static string WinpaUserId;
    private static string dbPath;
    private static ExecParam eParam = new ExecParam();


    public INT_WINPAService()
    {
        dataContext = new DataContext(this.Application);
        sp = new INT_WINPAStoredProcedure(dataContext);
        using (var sqlConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ToString()))
        {
            sqlConnection.Open();
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "select p.Ertek from KRT_Parameterek p where p.Nev='WinpaUserId' and p.ErvKezd<=getdate() and (p.ErvVege>=getdate() or p.ErvVege is null)";
                var res = sqlCommand.ExecuteScalar();
                if (res != null)
                    WinpaUserId = res.ToString();

                sqlCommand.CommandText = "select p.Ertek from KRT_Parameterek p where p.Nev='WinpaDbPath' and p.ErvKezd<=getdate() and (p.ErvVege>=getdate() or p.ErvVege is null)";
                res = sqlCommand.ExecuteScalar();
                if (res != null)
                    dbPath = res.ToString();
                eParam.Felhasznalo_Id = WinpaUserId;
                using (var felhasznaloService = new Contentum.eAdmin.Service.KRT_FelhasznalokService(ConfigurationManager.AppSettings.Get("eAdminBusinessServiceUrl") + "KRT_FelhasznalokService.asmx"))
                {
                    ExecParam param = eParam.Clone();
                    param.Record_Id = eParam.Felhasznalo_Id;
                    Result resUser = felhasznaloService.Get(param);
                    eParam.FelhasznaloSzervezet_Id = ((KRT_Felhasznalok)resUser.Record).Org;
                }
            }
        }        
    }

    public INT_WINPAService(DataContext _dataContext)
    {
         this.dataContext = _dataContext;
         sp = new INT_WINPAStoredProcedure(dataContext);
         ExecParam execParam_KRT_Param = new ExecParam();
         using (var sqlConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ToString()))
         {
             sqlConnection.Open();
             using (var sqlCommand = new SqlCommand())
             {
                 sqlCommand.Connection = sqlConnection;
                 sqlCommand.CommandText = "select p.Id from KRT_Parameterek p where p.Nev='WinpaUserId' and p.ErvKezd<=getdate() and (p.ErvVege>=getdate() or p.ErvVege is null)";
                 var res = sqlCommand.ExecuteScalar();
                 if (res != null)
                     WinpaUserId = res.ToString();

                 sqlCommand.CommandText = "select p.Id from KRT_Parameterek p where p.Nev='WinpaDbPath' and p.ErvKezd<=getdate() and (p.ErvVege>=getdate() or p.ErvVege is null)";
                 res = sqlCommand.ExecuteScalar();
                 if (res != null)
                     dbPath = res.ToString();
                 eParam.Felhasznalo_Id = WinpaUserId;
                 using (var felhasznaloService = new Contentum.eAdmin.Service.KRT_FelhasznalokService(ConfigurationManager.AppSettings.Get("eAdminBusinessServiceUrl") + "KRT_FelhasznalokService.asmx"))
                 {
                     ExecParam param = eParam.Clone();
                     param.Record_Id = eParam.Felhasznalo_Id;
                     Result resUser = felhasznaloService.Get(param);
                     eParam.FelhasznaloSzervezet_Id = ((KRT_Felhasznalok)resUser.Record).Org;
                 }
             }
         }

    }

    /// <summary>
    /// KuldemenyekToWinpa()  
    /// A Winpa techinkai user-hez tartozó küldemények betöltése a WINPA FireBird adatbázisába
    /// </summary>   
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>
    [WebMethod( Description = "A Winpa techinkai user-hez tartozó küldemények betöltése a WINPA FireBird adatbázisába")]
    public void KuldemenyekToWinpa()
    {
        
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(eParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {            
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            KuldemenyekAtvetel();
            result = GetKuldemenyek();
            sp.KuldemenyekToWinpa(eParam, result, dbPath);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(e.Message);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(eParam, result);
        return;
    }

    /// <summary>
    /// KuldemenyekFromWinpa()  
    /// A Winpa techinkai user-hez tartozó küldemények visszaolvasása a WINPA FireBird adatbázisából, ezek postázása és a FireBird adatbázis frissítése. Azon felhasználó értesítése, aki a küldeményt átadta a winpának.
    /// </summary>   
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>
    [WebMethod(Description = "A Winpa techinkai user-hez tartozó küldemények visszaolvasása a WINPA FireBird adatbázisából, ezek postázása és a FireBird adatbázis frissítése. Azon felhasználó értesítése, aki a küldeményt átadta a winpának.")]
    public void KuldemenyekFromWinpa()
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(eParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = GetKuldemenyek();
            List<Tuple<String, List<String>>> postazottKuldemenyek = new List<Tuple<String, List<String>>>();
            List<Tuple<String, List<Tuple<String, String>>>> nemPostazottKuldemenyek = new List<Tuple<String, List<Tuple<String, String>>>>();
            Result res = sp.KuldemenyekFromWinpa(eParam, result, dbPath);
            //postázás
            string _postakonyvId = GetPostakonyv();
            using (EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService())
            {
                if (res != null && res.Ds != null && res.Ds.Tables.Count > 0 && res.Ds.Tables[0].Rows.Count > 0)
                {                   
                    Result resultKuldemenyek = new Result();
                    string postakonyvId = GetPostakonyv();
                    foreach (DataRow dr in result.Ds.Tables[0].Rows)
                    {
                        EREC_KuldKuldemenyek _erec_kuldemenyek = new EREC_KuldKuldemenyek();
						Utility.LoadBusinessDocumentFromDataRow(_erec_kuldemenyek, dr);
                        string id = "";
                        string iktSzam = "";
						Result respostazas = new Result();
                        foreach (DataRow drv in res.Ds.Tables[0].Rows)
                        {
                            if (_erec_kuldemenyek.Id == drv["KuldemenyId"].ToString())
                            {
                                _erec_kuldemenyek.Ar = drv["KOLTSEG"].ToString().Trim();
                                _erec_kuldemenyek.Updated.Ar = true;
                                _erec_kuldemenyek.RagSzam = drv["RAGSZAM"].ToString().Trim();
                                _erec_kuldemenyek.Updated.RagSzam = true;
                                _erec_kuldemenyek.BelyegzoDatuma = DateTime.Now.ToString("yyyy.MM.dd");
                                _erec_kuldemenyek.Updated.BelyegzoDatuma = true;
                                //userId = drv[""].ToString();
                                id = drv["Id"].ToString().Trim();
                                iktSzam = drv["IKTATOSZAM"].ToString().Trim();

                                //string szolgaltatasok = drv["SZOLGALTATASOK"].ToString();
                                //string rendeltetesiHely = drv["RENDELTETESI_HELY"].ToString();
                                //string kuldemenyTipus = drv["KULDEMENY_TIPUS"].ToString();
                                //string suly = drv["SULY"].ToString();

                                //KuldemenyFajta kuldemenyFajta = new KuldemenyFajta(rendeltetesiHely, kuldemenyTipus, szolgaltatasok, suly);

                                //_erec_kuldemenyek.KimenoKuldemenyFajta = kuldemenyFajta.GetKuldemenyFajta();
                                //_erec_kuldemenyek.Updated.KimenoKuldemenyFajta = true;


                                //if (_erec_kuldemenyek.KimenoKuldemenyFajta != KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat
                                //    && _erec_kuldemenyek.KimenoKuldemenyFajta != KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat_sajat_kezbe)
                                //{

                                //    _erec_kuldemenyek.Elsobbsegi = kuldemenyFajta.ElsobbsegiString;
                                //    _erec_kuldemenyek.Updated.Elsobbsegi = true;

                                //    _erec_kuldemenyek.Ajanlott = kuldemenyFajta.AjanlottString;
                                //    _erec_kuldemenyek.Updated.Ajanlott = true;

                                //    _erec_kuldemenyek.Tertiveveny = kuldemenyFajta.TertivevenyString;
                                //    _erec_kuldemenyek.Updated.Tertiveveny = true;

                                //    _erec_kuldemenyek.SajatKezbe = kuldemenyFajta.SajatKezbeString;
                                //    _erec_kuldemenyek.Updated.SajatKezbe = true;

                                //    _erec_kuldemenyek.E_ertesites = kuldemenyFajta.E_ertesitesString;
                                //    _erec_kuldemenyek.Updated.E_ertesites = true;

                                //    _erec_kuldemenyek.E_elorejelzes = kuldemenyFajta.E_elorejelzesString;
                                //    _erec_kuldemenyek.Updated.E_elorejelzes = true;

                                //    _erec_kuldemenyek.PostaiLezaroSzolgalat = kuldemenyFajta.PostaiLezaroSzolgalatString;
                                //    _erec_kuldemenyek.Updated.PostaiLezaroSzolgalat = true;
                                //}

                                //küldemény fajtája hivatalos irat
                                if (String.IsNullOrEmpty(_erec_kuldemenyek.KimenoKuldemenyFajta))
                                {
                                    _erec_kuldemenyek.KimenoKuldemenyFajta = KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat;
                                    _erec_kuldemenyek.Updated.KimenoKuldemenyFajta = true;
                                }


                                if (String.IsNullOrEmpty(_erec_kuldemenyek.IraIktatokonyv_Id))
                                {
                                    _erec_kuldemenyek.IraIktatokonyv_Id = postakonyvId;
                                    _erec_kuldemenyek.Updated.IraIktatokonyv_Id = true;
                                }

                                respostazas = service.Postazas(eParam, _erec_kuldemenyek, "");
                                break;
                            }
                        }
                        
                        //firebird refresh
                        if (respostazas.Uid != null && !respostazas.IsError)
                        {
                            sp.WinpaRefreshFeldolgozott(eParam.Clone(), id, dbPath);
                            bool isExist = false;
                            foreach (Tuple<String, List<String>> item in postazottKuldemenyek)
                            {
                                if (item.Item1 == _erec_kuldemenyek.Csoport_Id_Felelos_Elozo)
                                {
                                    item.Item2.Add(iktSzam);
                                    isExist = true;
                                }
                            }
                            if (!isExist)
                            {
                                postazottKuldemenyek.Add(Tuple.Create<String, List<String>>(_erec_kuldemenyek.Csoport_Id_Felelos_Elozo, new List<String>() { iktSzam }));
                            }
                        }
                        else
                        {
                            bool isNotExist = true;
                            foreach (Tuple<String, List<Tuple<String, String>>> item in nemPostazottKuldemenyek)
                            {
                                if (item.Item1 == _erec_kuldemenyek.Csoport_Id_Felelos_Elozo)
                                {
                                    item.Item2.Add(Tuple.Create<String, String>(iktSzam, respostazas.ErrorMessage));
                                    isNotExist = false;
                                }
                            }
                            if (isNotExist)
                            {
                                var list = new List<Tuple<String, String>>() { Tuple.Create<String, String>(iktSzam, respostazas.ErrorMessage) };
                                nemPostazottKuldemenyek.Add(Tuple.Create<String, List<Tuple<String, String>>>(_erec_kuldemenyek.Csoport_Id_Felelos_Elozo, list));
                            }
                        }

                    }
                }
            }

            sendMail(postazottKuldemenyek, nemPostazottKuldemenyek);//resultFilePath, eMailMsg, new string[] { ((KRT_Felhasznalok)Felhasznalo.Record).EMail });
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(e.Message);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(eParam, result);
        return;
    }
    private void sendMail(List<Tuple<String, List<String>>> postazottKuldemenyek, List<Tuple<String, List<Tuple<String, String>>>> nemPostazottKuldemenyek)

    {
        List<Tuple<string, string>> mailWithMsg = new List<Tuple<string, string>>();
        foreach (Tuple<String, List<String>> itemPK in postazottKuldemenyek)
        {
           using (var felhasznaloService = new Contentum.eAdmin.Service.KRT_FelhasznalokService(ConfigurationManager.AppSettings.Get("eAdminBusinessServiceUrl") + "KRT_FelhasznalokService.asmx"))
           {
                var exec = eParam.Clone();
                exec.Record_Id = itemPK.Item1;
                Result Felhasznalo = felhasznaloService.Get(exec);
                if (Felhasznalo != null && !Felhasznalo.IsError)
                {
                    mailWithMsg.Add(Tuple.Create<string, string>(((KRT_Felhasznalok)Felhasznalo.Record).EMail, "Postázott küldemények:" + Environment.NewLine + "\t-"
                        + string.Join(Environment.NewLine + "\t-", itemPK.Item2.ToArray())));
                    break;
                }
                else
                {
                    string msg = string.Format("Postázott küldemény - email összeállítás hiba: Felhasználó üres vagy hibás FELELOS={0} IKTSZAM={1}  ERROR={2}", itemPK.Item1, itemPK.Item1.ToCharArray(), Felhasznalo == null ? string.Empty : Felhasznalo.ErrorMessage);
                    Logger.Error(msg);
                }
            }
            List<Tuple<string, List<string>>> nemPostazott = new List<Tuple<string, List<string>>>();
            foreach (Tuple<String, List<Tuple<String, String>>> itemNPK in nemPostazottKuldemenyek)
            {
                List<string> list = new List<string>();
                foreach (Tuple<String, String> r in itemNPK.Item2)
                {
                    list.Add(r.Item1 + Environment.NewLine + "\tHiba:" + Environment.NewLine + "\t\t" + r.Item2);
                }
                nemPostazott.Add(Tuple.Create<String, List<String>>(itemNPK.Item1, list));
            }

            foreach (Tuple<String, List<String>> itemNP in nemPostazott)
            {
                using (var felhasznaloService = new Contentum.eAdmin.Service.KRT_FelhasznalokService(ConfigurationManager.AppSettings.Get("eAdminBusinessServiceUrl") + "KRT_FelhasznalokService.asmx"))
                {
                    var exec = eParam.Clone();
                    exec.Record_Id = itemNP.Item1;
                    Result Felhasznalo = felhasznaloService.Get(exec);
                    if (Felhasznalo != null && !Felhasznalo.IsError)
                    {
                        bool isExist = false;
                        foreach (Tuple<String, String> msg in mailWithMsg)
                        {
                            if (msg.Item1 == itemNP.Item1)
                            {
                                msg.Item2 += string.Join(Environment.NewLine + "\t-", itemNP.Item2.ToArray());//(_erec_kuldemenyek.Azonosito);
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist)
                        {
                            mailWithMsg.Add(Tuple.Create<string, string>(((KRT_Felhasznalok)Felhasznalo.Record).EMail, "Nem postázott küldemények:" + Environment.NewLine
                                + "\t-" + string.Join(Environment.NewLine + "\t-", itemNP.Item2.ToArray())));
                        }
                        break;
                    }
                    else
                    {
                        string msg = string.Format("Nem postázott küldemény - email összeállítás hiba: Felhasználó üres vagy hibás FELELOS={0} IKTSZAM={1}  ERROR={2}", itemNP.Item1, itemNP.Item1.ToCharArray(), Felhasznalo == null ? string.Empty : Felhasznalo.ErrorMessage);
                        Logger.Error(msg);
                    }
                }
            }
            using (Contentum.eAdmin.Service.EmailService mailService = new Contentum.eAdmin.Service.EmailService(ConfigurationManager.AppSettings.Get("eAdminBusinessServiceUrl") + "EmailService.asmx"))
            {
                foreach (Tuple<string, string> mail in mailWithMsg)
                {
                    var from = Rendszerparameterek.Get(eParam, Rendszerparameterek.HIBABEJELENTES_EMAIL_CIM);
                    Logger.Debug(String.Format("WINPA küldemény - FELELOS={0}, IKTSZAM{1},  FROM={2}", mail.Item1, mail.Item2, from));
                    var res = mailService.SendEmail(eParam, from, new string[] { mail.Item1 }, "Winpa - postázás", null, null, false, mail.Item2 + Environment.NewLine);
                }
            }
            return;
        }
    }
    private Result GetKuldemenyek()
    {
        Logger.Debug("GetKuldemenyek start");
        Result result = new Result();
        using (EREC_KuldKuldemenyekService kuldService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService())
        {
            result = kuldService.GetAll(
                eParam,
                new EREC_KuldKuldemenyekSearch()
                {
                    Csoport_Id_Felelos = new Field() { Name = "EREC_KuldKuldemenyek.Csoport_Id_Felelos", Type = "Guid", Operator = Query.Operators.equals, Value = WinpaUserId, GroupOperator = Query.Operators.and, Group = "100" },
                    Allapot = new Field() { Name = "EREC_KuldKuldemenyek.Allapot", Type = "String", Operator = Query.Operators.equals, Value = KodTarak.KULDEMENY_ALLAPOT.Expedialt, GroupOperator = Query.Operators.and, Group = "100" }
                        //ErvKezd = new Field() { Name = "EREC_KuldKuldemenyek.ErvKezd", Type = "DateTime", Operator = Query.Operators.lessorequal, Value = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss"), GroupOperator = Query.Operators.and, Group = "100" },
                        //ErvVege = new Field() { Name = "EREC_KuldKuldemenyek.ErvVege", Type = "DateTime", Operator = Query.Operators.greaterorequal, Value = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss") }
                }
            );
        }
        if (result != null && result.Ds != null && result.Ds.Tables.Count > 0)
        {
            Logger.Debug(String.Format("GetKuldemenyek szama: {0}", result.Ds.Tables[0].Rows.Count));
        }
        return result;
    }
    private void KuldemenyekAtvetel()
    {
        Logger.Debug("KuldemenyekAtvetel start");
        Result result = new Result();
        using (EREC_KuldKuldemenyekService kuldService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService())
        {
            result = kuldService.GetAll(
                eParam,
                new EREC_KuldKuldemenyekSearch()
                {
                    Csoport_Id_Felelos = new Field() { Name = "EREC_KuldKuldemenyek.Csoport_Id_Felelos", Type = "Guid", Operator = Query.Operators.equals, Value = WinpaUserId, GroupOperator = Query.Operators.and, Group = "100" },
                    Allapot = new Field() { Name = "EREC_KuldKuldemenyek.Allapot", Type = "String", Operator = Query.Operators.equals, Value = KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt, GroupOperator = Query.Operators.and, Group = "100" },
                    TovabbitasAlattAllapot = new Field() { Name = "EREC_KuldKuldemenyek.TovabbitasAlattAllapot", Type = "String", Operator = Query.Operators.equals, Value = KodTarak.KULDEMENY_ALLAPOT.Expedialt, GroupOperator = Query.Operators.and, Group = "100" },
                        //ErvKezd = new Field() { Name = "EREC_KuldKuldemenyek.ErvKezd", Type = "DateTime", Operator = Query.Operators.lessorequal, Value = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss"), GroupOperator = Query.Operators.and, Group = "100" },
                        //ErvVege = new Field() { Name = "EREC_KuldKuldemenyek.ErvVege", Type = "DateTime", Operator = Query.Operators.greaterorequal, Value = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss") }
                    }
            );
            if (result != null && result.Ds != null && result.Ds.Tables != null && result.Ds.Tables[0] != null)
            {
                Logger.Debug(String.Format("Atveendo kuldemenyek szama: {0}", result.Ds.Tables[0].Rows.Count));
                foreach (DataRow dr in result.Ds.Tables[0].Rows)
                {
                    kuldService.Atvetel(eParam, dr["Id"].ToString());
                }
                
            }
            
        }
    }

    private string GetPostakonyv()
    {
        Logger.Debug("GetPostakonyv");

        EREC_IraIktatoKonyvekService _EREC_IraIktatoKonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();
        search.IktatoErkezteto.Value = "P";
        search.IktatoErkezteto.Operator = Query.Operators.equals;
        search.MegkulJelzes.Value = "BOPMH";
        search.MegkulJelzes.Operator = Query.Operators.equals;

        Result res = _EREC_IraIktatoKonyvekService.GetAll(eParam, search);

        if (!res.IsError)
        {
            Logger.Debug(String.Format("GetPostakonyv eredmény: {0}", res.Ds.Tables[0].Rows.Count));
            if (res.Ds.Tables[0].Rows.Count == 1)
            {
                string postakonyvId = res.Ds.Tables[0].Rows[0]["Id"].ToString();
                Logger.Debug(String.Format("PostakonyvId: {0}", postakonyvId));
                return postakonyvId;
            }
            else
            {
                Logger.Error("Postakönyv nem található!");
            }
        }
        else
        {
            Logger.Error("GetPostakonyv hiba", eParam, res);
        }

        return String.Empty;

    }

    [WebMethod]
    public void TestMethod()
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(eParam, Context, GetType());
        try
        {
            using (var connection = new FirebirdSql.Data.FirebirdClient.FbConnection(@"User=SYSDBA;Password=masterkey;Database=" + dbPath))
            {
                if (connection.State == ConnectionState.Open) connection.Close();
                // connection.Open();
                using (var sqlCommand = new FirebirdSql.Data.FirebirdClient.FbCommand())
                {
                    sqlCommand.Connection = connection;
                    sqlCommand.Connection.Open();


                    sqlCommand.CommandText = "select * from IMPORT_LEVEL ";
                    DataSet ds1 = new DataSet();
                    FirebirdSql.Data.FirebirdClient.FbDataAdapter adapter1 = new FirebirdSql.Data.FirebirdClient.FbDataAdapter();
                    adapter1.SelectCommand = sqlCommand;
                    adapter1.Fill(ds1);

                    //ds1.WriteXml(@"c:\TEMP\Winpa\Winpa.xml");


                    sqlCommand.CommandText = "SELECT count(*) from IMPORT_LEVEL";
                    var t = sqlCommand.ExecuteScalar();

                    Logger.Debug("össz darab" + t.ToString());


                    sqlCommand.CommandText = "SELECT count(*) from IMPORT_LEVEL where allapot='1'";
                    t = sqlCommand.ExecuteScalar();

                    Logger.Debug("1-as állapotú darab " + t.ToString());
                    sqlCommand.CommandText = "SELECT count(*) from IMPORT_LEVEL where allapot='2'";
                    t = sqlCommand.ExecuteScalar();

                    Logger.Debug("2-es állapotú darab " + t.ToString());
                    sqlCommand.CommandText = "SELECT count(*) from IMPORT_LEVEL where allapot='3'";
                    t = sqlCommand.ExecuteScalar();

                    Logger.Debug("3-as állapotú darab " + t.ToString());
                }

            }

        }
        catch (System.Data.OleDb.OleDbException myOLEDBException)
        {
            //ole db exception
            //error collection sample 
            for (int i = 0; i <= myOLEDBException.Errors.Count - 1; i++)
            {
                Logger.Error("Message " + (i + 1) + ": " + myOLEDBException.Errors[i].Message);
                Logger.Error("Native: " + myOLEDBException.Errors[i].NativeError.ToString());
                Logger.Error("Source: " + myOLEDBException.Errors[i].Source);
                Logger.Error("SQL: " + myOLEDBException.Errors[i].SQLState);
                Logger.Error("----------------------------------------");
            }
        }
        catch (Exception myException)
        {
            //other exception
            //error collection sample
            Logger.Error(myException.Message);
        }

    }


    [WebMethod]
    public void SnycWinpaAdatok()
    {
         Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(eParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        try
        {
            DataTable tableWinpa = null;
            using (var connection = new FirebirdSql.Data.FirebirdClient.FbConnection(@"User=SYSDBA;Password=masterkey;Database=" + dbPath))
            {
                connection.Open();
                using (var sqlCommand = new FirebirdSql.Data.FirebirdClient.FbCommand())
                {
                    sqlCommand.Connection = connection;

                    sqlCommand.CommandText = "select * from IMPORT_LEVEL";
                    DataSet ds = new DataSet();
                    FirebirdSql.Data.FirebirdClient.FbDataAdapter adapter = new FirebirdSql.Data.FirebirdClient.FbDataAdapter();
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(ds);

                    tableWinpa = ds.Tables[0];

                    Logger.Debug(String.Format("IMPORT_LEVEL: {0}", tableWinpa.Rows.Count));
                }
            }

            DataTable tableContentum = null;

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            using (SqlCommand SqlComm = new SqlCommand("[sp_GetWinpaAdatok]"))
            {
                string where = String.Format("EREC_KuldKuldemenyek.Allapot = '06' and EREC_KuldKuldemenyek.Csoport_Id_Felelos = '{0}'", WinpaUserId);
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@ExecutorUserId"].Value = eParam.Typed.Felhasznalo_Id;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where"].Value = where;

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                tableContentum = ds.Tables[0];

                Logger.Debug(String.Format("Kuldemenyek: {0}", tableContentum.Rows.Count));
            }

            foreach (DataRow rowWinpa in tableWinpa.Rows)
            {
                string winpaId = rowWinpa["ID"].ToString();
                string winpaIktatoszam = rowWinpa["IKTATOSZAM"].ToString();
                string winpaKoltseg = rowWinpa["KOLTSEG"].ToString();
                string winpaRagszam = rowWinpa["RAGSZAM"].ToString();
                string winpaAllapot = rowWinpa["ALLAPOT"].ToString();

                if (!String.IsNullOrEmpty(winpaIktatoszam))
                {
                    winpaIktatoszam = winpaIktatoszam.Replace(" ", "");
                }

                Logger.Debug(String.Format("Winpa iktatószám: {0}", winpaIktatoszam));
                Logger.Debug(String.Format("Winpa állapot: {0}", winpaAllapot));
                Logger.Debug(String.Format("Winpa ragszám: {0}", winpaRagszam));

                //postázott vagy feldolgozott
                if (!String.IsNullOrEmpty(winpaIktatoszam) && !String.IsNullOrEmpty(winpaRagszam) &&
                    (winpaAllapot == "2" || winpaAllapot == "3"))
                {
                    foreach (DataRow rowContentum in tableContentum.Rows)
                    {
                        string contentumId = rowContentum["Id"].ToString();
                        string contentumIktatoszam = rowContentum["IKTATOSZAM"].ToString();
                        string contentumRagszam = rowContentum["RAGSZAM"].ToString();

                        contentumIktatoszam = contentumIktatoszam.Replace(" ", "");

                        if (contentumIktatoszam.Contains(winpaIktatoszam))
                        {
                            Logger.Debug(String.Format("Winpa iktatószám: {0}", winpaIktatoszam));
                            Logger.Debug(String.Format("Contentum iktatószám: {0}", contentumIktatoszam));
                            Logger.Debug(String.Format("Winpa ragszám: {0}", winpaRagszam));
                            Logger.Debug(String.Format("Contentum ragszám: {0}", contentumRagszam));

                            if (winpaRagszam != contentumRagszam)
                            {
                                Logger.Warn("Winpa ragszám != Contentum ragszám");

                                Logger.Debug(String.Format("EREC_KuldKuldemeny lekérése: {0}", contentumId));
                                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                                ExecParam xpm = eParam.Clone();
                                xpm.Record_Id = contentumId;

                                Result res = service.Get(xpm);

                                if (res.IsError)
                                {
                                    Logger.Error("EREC_KuldKuldemeny lekérése hiba", xpm, res);
                                }
                                else
                                {
                                    EREC_KuldKuldemenyek kuldemeny = res.Record as EREC_KuldKuldemenyek;
                                    kuldemeny.Updated.SetValueAll(false);
                                    kuldemeny.Base.Updated.SetValueAll(false);
                                    kuldemeny.Base.Updated.Ver = true;

                                    kuldemeny.RagSzam = winpaRagszam;
                                    kuldemeny.Updated.RagSzam = true;

                                    kuldemeny.Ar = winpaKoltseg;
                                    kuldemeny.Updated.Ar = true;

                                    Logger.Debug(String.Format("EREC_KuldKuldemeny update: {0}", contentumId));
                                    Logger.Debug(String.Format("RagSzame: {0}", winpaRagszam));
                                    Logger.Debug(String.Format("Ar: {0}", winpaKoltseg));

                                    res = service.Update(xpm, kuldemeny);

                                    if (res.IsError)
                                    {
                                        Logger.Error("EREC_KuldKuldemeny update hiba", xpm, res);
                                    }
                                    else
                                    {
                                        //postázott
                                        if (winpaAllapot == "2")
                                        {
                                            Logger.Debug(String.Format("WinpaRefreshFeldolgozott: {0}", winpaId));
                                            sp.WinpaRefreshFeldolgozott(eParam.Clone(), winpaId, dbPath);
                                        }
                                    }
                                }
                            }

                            break;
                        }
                    }
                }
            }

        }
        catch (Exception myException)
        {
            //other exception
            //error collection sample
            Logger.Error(myException.Message);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(null as ExecParam);
    }
}
