﻿using Contentum.eIntegrator.WebService.ePosta.Tertiveveny;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Ellenorzes;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;

[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class INT_ePostaiTertivevenyService : WebService
{
    #region MAIN PROERTIES

    private DataContext dataContext;

    #endregion MAIN PROERTIES

    /// <summary>
    /// Constructor
    /// </summary>
    public INT_ePostaiTertivevenyService()
    {
        dataContext = new DataContext(this.Application);
    }

    [WebMethod(Description = "Job hívja így elindítva a temp könyvtárban tárolt tertivevenyek iktatását.")]
    public ePostaiTertivevenyErkeztetesProcessResult ePostaTertiSFTPFeldolgozas()
    {
        ePostaiTertivevenyErkeztetesProcessResult results = null;

        // Log log = Log.WsStart(execParam, Context, GetType());
        try
        {
            var erkezteto = new PostaiTertivevenyErkeztetesSFTP(new ePostaiTertivevenyParameters());
            results = erkezteto.Start();
        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
        }
        // log.WsEnd(execParam);
        return results;
    }


    [WebMethod(Description = "Job hívja így elindítva az melmúlt napok feldolgozásának sikerességének ellenörzésére")]
    public string ePostaiTertiSFTPCheckJob(ePostaiTertivevenyParametersBase parameters, string kezdoDatum, string vegDatum, string napokSzama, bool isSilentMode)
    {
        if (string.IsNullOrEmpty(kezdoDatum) && string.IsNullOrEmpty(vegDatum) && !string.IsNullOrEmpty(napokSzama))
        {
            int napok = int.Parse(napokSzama) * -1;
            kezdoDatum = DateTime.Today.AddDays(-1).AddDays(napok).ToString(ePostaiTertivevenyFTPBase.BaseConstants.DATE_FORMAT);
            vegDatum = DateTime.Today.AddDays(-1).ToString(ePostaiTertivevenyFTPBase.BaseConstants.DATE_FORMAT);
        }

        if (string.IsNullOrEmpty(kezdoDatum) && string.IsNullOrEmpty(vegDatum) && string.IsNullOrEmpty(napokSzama))
        {
            kezdoDatum = DateTime.Today.AddDays(-1).ToString(ePostaiTertivevenyFTPBase.BaseConstants.DATE_FORMAT);
            vegDatum = DateTime.Today.AddDays(-1).ToString(ePostaiTertivevenyFTPBase.BaseConstants.DATE_FORMAT);
        }
        string resultMessage = "";
        if (parameters == null)
        {
            parameters = new ePostaiTertivevenyParameters();
        }
        try
        {
            var result = new TertiSFTPDownloadChecker().CheckBetweenDates(parameters, kezdoDatum, vegDatum);
            resultMessage = result.ToString();
        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
            resultMessage = "Hiba az ellenőrzés során: " + e.Message + " Trace: " + e.StackTrace;
        }
        Logger.Info(resultMessage);
        Contentum.eAdmin.Service.EmailService svcEmail = Contentum.eUtility.eAdminService.ServiceFactory.GetEmailService();
        if (!isSilentMode)
        {
            bool resEmail = svcEmail.SendEmail(parameters.ExecParam,
                Rendszerparameterek.Get(parameters.ExecParam, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM),
                new string[] { parameters.AdminEmail },
                "Tértivevény érkeztetés ellenőrzés eredmény",
                null,
                null,
                true,
                resultMessage.Replace("\n", "<br>"));
        }
        return resultMessage;
    }

    [WebMethod(Description = "Manuális teszteléshez")]
    public ePostaiTertivevenyErkeztetesProcessResult ePostaTertiSFTPFeldolgozasFixDate(string kezdoDatum, string vegDatum)
    {
        ePostaiTertivevenyErkeztetesProcessResult result = null;
        try
        {
            var parameters = new ePostaiTertivevenyParameters();
            var erkezteto = new PostaiTertivevenyErkeztetesSFTP(parameters);

            result = erkezteto.Start(kezdoDatum, vegDatum);

        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
        }
        return result;
    }

    [WebMethod(Description = "Manuális teszteléshez")]
    public ePostaiTertivevenyErkeztetesProcessResult ePostaStartFeldolgozasManual(ePostaiTertivevenyParametersManual parameters)
    {
        ePostaiTertivevenyErkeztetesProcessResult webMethodResult = new ePostaiTertivevenyErkeztetesProcessResult();

        // Log log = Log.WsStart(execParam, Context, GetType());
        try
        {
            List<IePostaTertivevenyErkeztetes> erkeztetok = new List<IePostaTertivevenyErkeztetes>();
            if (parameters.IsSFTPMode)
            {
                erkeztetok.Add(new PostaiTertivevenyErkeztetesSFTP(parameters));
            }
            if (parameters.IsHKPMode)
            {
                erkeztetok.Add(new PostaiTertivevenyErkeztetesHKP(parameters));
            }
            if (parameters.IsFTP_OVER_SSLMode)
            {
                erkeztetok.Add(new PostaiTertivevenyErkeztetesFTPOverSSL(parameters));
            }
            List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase> localResultItems = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();
            foreach (var item in erkeztetok)
            {
                var itemResult = item.Start();
                if (itemResult.HasCriticalError)
                {
                    webMethodResult = itemResult;
                    break;
                }

                localResultItems.AddRange(new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>(itemResult.Results));
            }
            localResultItems.AddRange(webMethodResult.Results);
            webMethodResult.Results = localResultItems.ToArray();
        }
        catch (Exception e)
        {
            Logger.Error("ePostaStartFeldolgozasManual kritikus hiba: " + e.Message);
            webMethodResult.CriticalError = e.Message;
        }
        // log.WsEnd(execParam);

        return webMethodResult;
    }

    [WebMethod(Description = "SFTP connection test")]
    public string ePostaTertiSFTPConnectionTest()
    {
        try
        {
            var erkezteto = new PostaiTertivevenyErkeztetesSFTP(new ePostaiTertivevenyParameters());
            erkezteto.CheckConnection();
        }
        catch (Exception e)
        {
            return string.Format("SFTP kapcsolódás sikertelen !{0}{1}", Environment.NewLine, e.Message);
        }

        return string.Format("SFTP kapcsolódás sikeres !");
    }

    [WebMethod(Description = "EPosta Terti feldolgozas SFTP interface hasznalataval.")]
    public string ePostaTertiSFTPFeldolgozasSimple()
    {
        ePostaiTertivevenyErkeztetesProcessResult result;
        try
        {
            result = ePostaTertiSFTPFeldolgozas();
            return GetResultAsString(result);
        }
        catch (Exception exc)
        {
            return string.Format("CRITICAL ERROR ! {0}", exc.Message);
        }
    }

    [WebMethod(Description = "HKP - tértik feldolgozása egyesével")]
    public ePostaiTertivevenyErkeztetesProcessResult ePostaTertiHKPFeldolgozasSingle(string customFolderPath, string fileName)
    {
        ePostaiTertivevenyErkeztetesProcessResult results = null;
        try
        {
            var param = new ePostaiTertivevenyParameters();
            var erkezteto = new PostaiTertivevenyErkeztetesHKP(param);
            var folderManager = new CustomTempFolderManager(param, customFolderPath);
            results = erkezteto.ExtractAndProcessSpecificFile(folderManager, fileName, false);
        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
        }
        return results;
    }
    [WebMethod(Description = "HKP - Az összes térti feldolgozása a mappában")]
    public ePostaiTertivevenyErkeztetesProcessResult ePostaTertiHKPFeldolgozasAllInFolder(string customFolderFullPath)
    {
        ePostaiTertivevenyErkeztetesProcessResult results = null;
        try
        {
            var param = new ePostaiTertivevenyParameters();
            var erkezteto = new PostaiTertivevenyErkeztetesHKP(param);
            var folderManager = new CustomTempFolderManager(param, customFolderFullPath);
            results = erkezteto.ExtractAndProcessAllFiles(folderManager, false);
        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
        }
        return results;
    }
    [WebMethod(Description = "HKP tértik letöltése újra az adott ebeadványhoz")]
    public string ePostaTertiHKPDownloadPDFForEbeadvany(string eBeadvanyID, string downloadFolder)
    {
        try
        {
            var param = new ePostaiTertivevenyParameters();
            var folderManager = new CustomTempFolderManager(param, downloadFolder);

            var erkezteto = new PostaiTertivevenyErkeztetesHKP(param);
            erkezteto.SavePDFToTempDir(eBeadvanyID, folderManager);
            return "OK. Mentve=> " + folderManager.RootFolder;
        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
            return e.Message;
        }
    }

    [WebMethod(Description = "A megadott eBeadvány ID alapján kikeresett adatokkal meghívja az EdokWrapper.ProcessKRXFile metódust, ami a KRX fájlt kibontja, és a csatolmányait az eBeadványhoz feltölti")]
    public string ePostaTertiHKPKRXKibontas(string ebeadvanyID)
    {
        try
        {
            Logger.Debug("ePostaTertiHKPKRXKibontas eBeadvanyID: " + ebeadvanyID);
            var param = new ePostaiTertivevenyParameters();
            Contentum.eRecord.Service.EREC_eBeadvanyokService eBeadvanyService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService(); ;
            Contentum.eDocument.Service.KRT_DokumentumokService dokumentumokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            Contentum.eRecord.Service.EREC_eBeadvanyCsatolmanyokService eBeadvanyCsatolmanyokService = eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();

            //eBeadványt megkeressük
            Logger.Debug("ePostaTertiHKPKRXKibontas eBeadvanySearch");
            var searchExecParam = param.ExecParam.Clone();
            searchExecParam.Record_Id = ebeadvanyID;
            var eBeadvanyResult = eBeadvanyService.Get(searchExecParam);

            if (eBeadvanyResult.IsError)
            {
                return "Hiba az eBeadvány lekérdezése során: " + eBeadvanyResult.ErrorMessage;
            }

            if (!eBeadvanyResult.HasData())
            {
                return "Nem található az eBeadvány!";
            }

            //eBeadványhoz tartozó csatolmányok közül a krx et kikeressük
            Logger.Debug("ePostaTertiHKPKRXKibontas eBeadvanyCsatolmanySearch");
            var eBeadvany = eBeadvanyResult.GetData<Contentum.eBusinessDocuments.EREC_eBeadvanyok>().SingleOrDefault();
            var eBeadvanyCsatolmanySearch = new Contentum.eQuery.BusinessDocuments.EREC_eBeadvanyCsatolmanyokSearch();
            eBeadvanyCsatolmanySearch.eBeadvany_Id.Filter(eBeadvany.Id);
            eBeadvanyCsatolmanySearch.Nev.Like("%.krx");

            var eBeadvanyCsatolmanyokResult = eBeadvanyCsatolmanyokService.GetAll(param.ExecParam.Clone(), eBeadvanyCsatolmanySearch);
            if (eBeadvanyCsatolmanyokResult.IsError)
            {
                return "Hiba az EREC_eBeadvanyCsatolmanyok lekérdezése során: " + eBeadvanyCsatolmanyokResult.ErrorMessage;
            }
            if (!eBeadvanyCsatolmanyokResult.HasData())
            {
                return "Nem található az EREC_eBeadvanyCsatolmanyok!";
            }
            var eBeadvanyCsatolmany = eBeadvanyCsatolmanyokResult.GetData<Contentum.eBusinessDocuments.EREC_eBeadvanyCsatolmanyok>().SingleOrDefault();

            //A megtalált csatolmány alapján megkeressük a dokumentumot
            Logger.Debug("ePostaTertiHKPKRXKibontas dokumentumSearch");
            var dokumentumokSearch = new Contentum.eQuery.BusinessDocuments.KRT_DokumentumokSearch();
            dokumentumokSearch.Id.Filter(eBeadvanyCsatolmany.Dokumentum_Id);

            var dokumentumResult = dokumentumokService.GetAll(param.ExecParam.Clone(), dokumentumokSearch);
            if (dokumentumResult.IsError)
            {
                return "Hiba az dokumentum lekérdezése során: " + dokumentumResult.ErrorMessage;
            }
            if (!dokumentumResult.HasData())
            {
                return "Nem található a dokumentum!";
            }
            //letöltjük a dokumentum alapján annak a tartalmát az external_link ből
            Logger.Debug("ePostaTertiHKPKRXKibontas dokumentum download");
            var dokumentum = dokumentumResult.GetData<Contentum.eBusinessDocuments.KRT_Dokumentumok>().SingleOrDefault();
            byte[] data = PostaiTertivevenyErkeztetesHKP.Download(dokumentum.External_Link);
            var csatolmany = new Contentum.eBusinessDocuments.Csatolmany()
            {
                Tartalom = data,
                Nev = dokumentum.FajlNev
            };
            //Ráengedjük a feldolgozást
            Logger.Debug("ePostaTertiHKPKRXKibontas ProcessKRXFile");
            eUzenet.HKP_KRService.Core.EdokWrapper.ProcessKRXFile(param.ExecParam, eBeadvany.Id, eBeadvany.KR_ErkeztetesiSzam, csatolmany);
            return "=>OK";
        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
            return e.Message;
        }
    }

    [WebMethod(Description = "Meghívja az ePostaTertiHKPKRXKibontas metódust, majd meghívja az ePostaTertiHKPDownloadPDFForEbeadvany metódust, majd a kapott mappanévvel a ePostaTertiHKPFeldolgozasSingle metódust.")]
    public string ePostaTertiHKPKRXFeldolgozas(string ebeadvanyID, string folderPathToDownloadPDFs)
    {
        string result = "";
        try
        {
            var param = new ePostaiTertivevenyParameters();
            Logger.Debug("ePostaTertiHKPKRXFeldolgozas eBeadvanyID: " + ebeadvanyID);
            result += "ePostaTertiHKPKRXKibontas: " + ePostaTertiHKPKRXKibontas(ebeadvanyID);
            Logger.Debug("ePostaTertiHKPDownloadPDFForEbeadvany eBeadvanyID: " + ebeadvanyID);
            result += "\n ePostaTertiHKPDownloadPDFForEbeadvany: " + ePostaTertiHKPDownloadPDFForEbeadvany(ebeadvanyID, folderPathToDownloadPDFs);
            Logger.Debug("ePostaTertiHKPFeldolgozasAllInFolder folder: " + folderPathToDownloadPDFs);
            result += "\n ePostaTertiHKPFeldolgozasAllInFolder: " + GetResultAsString(ePostaTertiHKPFeldolgozasAllInFolder(folderPathToDownloadPDFs));
            return result;
        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
            return e.Message;
        }
    }



    #region FTP OVER SSL

    [WebMethod(Description = "FTP OVER SSL connection test")]
    public string EPostaTertiFTPOverSSLConnectionTest()
    {
        try
        {
            var erkezteto = new PostaiTertivevenyErkeztetesFTPOverSSL(new ePostaiTertivevenyParameters());
        }
        catch (Exception e)
        {
            return string.Format("FTP OVER SSL kapcsolódás sikertelen !{0}{1}", Environment.NewLine, e.Message);
        }
        return string.Format("FTP OVER SSL kapcsolódás sikeres !");
    }

    [WebMethod(Description = "Job hívja így elindítva a temp könyvtárban tárolt tertivevenyek iktatását.")]
    public ePostaiTertivevenyErkeztetesProcessResult EPostaTertiFTPOverSSLFeldolgozas()
    {
        ePostaiTertivevenyErkeztetesProcessResult results = null;

        // Log log = Log.WsStart(execParam, Context, GetType());
        try
        {
            var erkezteto = new PostaiTertivevenyErkeztetesFTPOverSSL(new ePostaiTertivevenyParameters());
            results = erkezteto.Start();
        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
        }
        // log.WsEnd(execParam);
        return results;
    }

    [WebMethod(Description = "EPosta Terti feldolgozas FTP-OVER-SSL interface hasznalataval.")]
    public string EPostaTertiFTPOverSSLFeldolgozasSimple()
    {
        ePostaiTertivevenyErkeztetesProcessResult result;
        try
        {
            result = EPostaTertiFTPOverSSLFeldolgozas();
            return GetResultAsString(result);
        }
        catch (Exception exc)
        {
            return string.Format("CRITICAL ERROR ! {0}", exc.Message);
        }
    }

    #endregion FTP OVER SSL

    /// <summary>
    /// GetResultAsString
    /// </summary>
    /// <param name="result"></param>
    /// <returns></returns>
    public static string GetResultAsString(ePostaiTertivevenyErkeztetesProcessResult result)
    {
        string resultMessage = string.Empty;
        if (result == null)
        {
            return string.Format("CRITICAL ERROR !");
        }
        else if (result.HasCriticalError)
        {
            return string.Format("CRITICAL ERROR ! {0}", result.CriticalError);
        }

        resultMessage += result.GetItemSuccesssString();
        resultMessage += "\n";
        resultMessage += result.GetItemErrorsString();


        return resultMessage;
    }
}