using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data;
using System.Collections.Generic;

[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_EsemenyekService : System.Web.Services.WebService
{
    private string tablaNev;
    
    [WebMethod()]
    //[System.Xml.Serialization.XmlIncludeAttribute(typeof(KRT_Esemenyek.BaseTyped)), System.Xml.Serialization.XmlIncludeAttribute(typeof(KRT_Esemenyek.BaseTyped))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_EsemenyekSearch _KRT_EsemenyekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllWithExtension(ExecParam, _KRT_EsemenyekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    public Result GetParamsByFunkcioKod(ExecParam ExecParam, string TranzId, string Obj_Id, string TablaNev, string FunkcioKod)
    {
        try
        {
            this.tablaNev = TablaNev;
            KRT_Esemenyek _krt_esemenyek = (KRT_Esemenyek)sp.GetParamsByFunkcioKod(ExecParam, Obj_Id, TablaNev, FunkcioKod).Record;
            _krt_esemenyek.Felhasznalo_Id_Login = string.IsNullOrEmpty(ExecParam.LoginUser_Id) ? ExecParam.Felhasznalo_Id : ExecParam.LoginUser_Id;
            _krt_esemenyek.Felhasznalo_Id_User = ExecParam.Felhasznalo_Id;
            _krt_esemenyek.Csoport_Id_FelelosUserSzerveze = ExecParam.FelhasznaloSzervezet_Id;
            _krt_esemenyek.Helyettesites_Id = ExecParam.Helyettesites_Id;
            //bernat.laszlo added
            _krt_esemenyek.Munkaallomas = ExecParam.UserHostAddress;
                        
            //_krt_esemenyek.Tranzakcio_Id = TranzId;

            // Az audit log pageId-j�t tessz�k le tranzakcioId-k�nt az ExecParam-b�l:
            _krt_esemenyek.Tranzakcio_Id = ExecParam.Page_Id;            

            Result _result = new Result();
            _result.Record = _krt_esemenyek;

            return _result;
        }
        catch (Exception e)
        {
            Result _result = ResultException.GetResultFromException(e);
            Logger.Info("KRT_Esemenyek::GetParamsByFunkcioKod: ", e);
            return _result;
        }
    }

    public Result GetParamsByMuveletKod(ExecParam ExecParam, string TranzId, string Obj_Id, string TablaNev, string MuveletKod)
    {
        try
        {
            this.tablaNev = TablaNev;
            KRT_Esemenyek _krt_esemenyek = (KRT_Esemenyek)sp.GetParamsByMuveletKod(ExecParam, Obj_Id, TablaNev, MuveletKod).Record;
            _krt_esemenyek.Felhasznalo_Id_Login = string.IsNullOrEmpty(ExecParam.LoginUser_Id) ? ExecParam.Felhasznalo_Id : ExecParam.LoginUser_Id;
            _krt_esemenyek.Felhasznalo_Id_User = ExecParam.Felhasznalo_Id;
            _krt_esemenyek.Csoport_Id_FelelosUserSzerveze = ExecParam.FelhasznaloSzervezet_Id;
            _krt_esemenyek.Helyettesites_Id = ExecParam.Helyettesites_Id;
            //bernat.laszlo added
            _krt_esemenyek.Munkaallomas = ExecParam.UserHostAddress;

            //_krt_esemenyek.Tranzakcio_Id = TranzId;

            // Az audit log pageId-j�t tessz�k le tranzakcioId-k�nt az ExecParam-b�l:
            _krt_esemenyek.Tranzakcio_Id = ExecParam.Page_Id;            

            Result _result = new Result();
            _result.Record = _krt_esemenyek;

            return _result;
        }
        catch (Exception e)
        {
            Result _result = ResultException.GetResultFromException(e);
            Logger.Info("KRT_Esemenyek::GetParamsByMuveletKod: ", e);
            return _result;
        }
    }

    public Result GetParamsByMuveletKodTomeges(ExecParam ExecParam, string TranzId, string Obj_Ids, string TablaNev, string MuveletKod)
    {
        try
        {
            this.tablaNev = TablaNev;
            KRT_Esemenyek _krt_esemenyek = (KRT_Esemenyek)sp.GetParamsByMuveletKod(ExecParam, Obj_Ids, TablaNev, MuveletKod).Record;
            _krt_esemenyek.Felhasznalo_Id_Login = string.IsNullOrEmpty(ExecParam.LoginUser_Id) ? ExecParam.Felhasznalo_Id : ExecParam.LoginUser_Id;
            _krt_esemenyek.Felhasznalo_Id_User = ExecParam.Felhasznalo_Id;
            _krt_esemenyek.Csoport_Id_FelelosUserSzerveze = ExecParam.FelhasznaloSzervezet_Id;
            _krt_esemenyek.Helyettesites_Id = ExecParam.Helyettesites_Id;
            //bernat.laszlo added
            _krt_esemenyek.Munkaallomas = ExecParam.UserHostAddress;

            //_krt_esemenyek.Tranzakcio_Id = TranzId;

            // Az audit log pageId-j�t tessz�k le tranzakcioId-k�nt az ExecParam-b�l:
            _krt_esemenyek.Tranzakcio_Id = ExecParam.Page_Id;            

            Result _result = new Result();
            _result.Record = _krt_esemenyek;

            return _result;
        }
        catch (Exception e)
        {
            Result _result = ResultException.GetResultFromException(e);
            Logger.Info("KRT_Esemenyek::GetParamsByMuveletKod: ", e);
            return _result;
        }
    }

    public Result InsertTomeges(ExecParam ExecParam, string Obj_Ids, string TablaNev, string MuveletKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            this.tablaNev = TablaNev;

            if (this.IsLoggable)
            {
                result = sp.InsertTomeges(ExecParam, Obj_Ids, TablaNev, MuveletKod);

                Result res = ProcessEsmenyekTriggerResult(ExecParam, result);
            }
            
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    public Result InsertTomegesByFunkcioKod(ExecParam ExecParam, string Obj_Ids, string FunkcioKod)
    {
        return this.InsertTomegesByFunkcioKod(ExecParam, Obj_Ids, FunkcioKod, null);
    }

    public Result InsertTomegesByFunkcioKod(ExecParam ExecParam, string Obj_Ids, string FunkcioKod, string Note)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (this.IsLoggable)
            {
                result = sp.InsertTomegesByFunkcioKod(ExecParam, Obj_Ids, FunkcioKod, Note);

                Result res = ProcessEsmenyekTriggerResult(ExecParam, result);
            }

            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Esemenyek))]
    public Result Insert(ExecParam ExecParam, KRT_Esemenyek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // biztos, ami biztos:
            if (String.IsNullOrEmpty(Record.Tranzakcio_Id) && !String.IsNullOrEmpty(ExecParam.Page_Id))
            {
                Record.Tranzakcio_Id = ExecParam.Page_Id;
            }

            // hozz�adjuk az aktu�lis helyettes�t�s id-t is
            Logger.Info("KRT_Esemenyek.ExecParam helyettes�t�s: " + ExecParam.Helyettesites_Id);
            if (!String.IsNullOrEmpty(ExecParam.Helyettesites_Id))
            {
                Record.Helyettesites_Id = ExecParam.Helyettesites_Id;
                Logger.Info("KRT_Esemenyek.ExecParam helyettes�t�s be�ll�tva.");
            }

            if (this.IsLoggable)
            {
                if (Record.Base.Updated.LetrehozasIdo && !String.IsNullOrEmpty(Record.Base.LetrehozasIdo))
                {
                    Logger.Info("KRT_Esemenyek.Insert LetrehozadiIdo: " + Record.Base.LetrehozasIdo);
                    //bernat.laszlo added
                    Record.MuveletKimenete = "SIKERES";
                    result = sp.Insert(Constants.Insert, ExecParam, Record, Record.Base.Typed.LetrehozasIdo.Value);
                }
                else
                {
                    //bernat.laszlo added
                    Record.MuveletKimenete = "SIKERES";
                    result = sp.Insert(Constants.Insert, ExecParam, Record);
                }

                Result res = ProcessEsmenyekTriggerResult(ExecParam, result);
                result = res;
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result ProcessEsmenyekTriggerResult(ExecParam p_ExecParam, Result p_result)
    {
        //Logger.Info("ProcessEsmenyekTriggerResult start");
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(p_ExecParam, Context, GetType());
        Result res = new Result();
        //bool isConnectionOpenHere = false;
        //bool isTransactionBeginHere = false;
        //try
        //{
        //    isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
        //    isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

        //    Utility.ProcessEsmenyekTriggerResult(this.dataContext, p_ExecParam, p_result);

        //    // COMMIT
        //    dataContext.CommitIfRequired(isTransactionBeginHere);
        //}
        //catch (Exception e)
        //{
        //    dataContext.RollbackIfRequired(isTransactionBeginHere);
        //    res = ResultException.GetResultFromException(e);
        //    Logger.Error("ProcessEsmenyekTriggerResult hiba", p_ExecParam, res);
        //}

        //if (!String.IsNullOrEmpty(p_result.Uid))
        //{
        //    res.Uid = p_result.Uid;
        //}
        //log.WsEnd(p_ExecParam, res);
        //Logger.Info("ProcessEsmenyekTriggerResult end");
        return res;
    }

    private bool IsLoggable
    {
        get
        {
            switch (this.tablaNev)
            {
                case Contentum.eUtility.Constants.TableNames.KRT_UIMezoObjektumErtekek:
                case Contentum.eUtility.Constants.TableNames.KRT_Esemenyek:
                    return false;
                default:
                    return true;
            }
        }
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "T�rt�net tablap lek�rdez�s")]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Esemenyek))]
    public Result GetAllWithExtensionForTortenet(ExecParam ExecParam, string objId, KRT_EsemenyekSearch _KRT_EsemenyekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtensionForTortenet(ExecParam, objId, _KRT_EsemenyekSearch);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result InsertByFunkcioKod(ExecParam execParam, string TranzId, string Obj_Id, string TablaNev, string FunkcioKod, string KeresesiFeltetel, string TalalatokSzama)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = this.GetParamsByFunkcioKod(execParam, TranzId, Obj_Id, TablaNev, FunkcioKod);
            if (!result.IsError)
            {
                KRT_Esemenyek esemeny = (KRT_Esemenyek)result.Record;
                if (!String.IsNullOrEmpty(KeresesiFeltetel))
                {
                    esemeny.KeresesiFeltetel = KeresesiFeltetel;
                }
                if (!String.IsNullOrEmpty(TalalatokSzama))
                {
                    esemeny.TalalatokSzama = TalalatokSzama;
                }
                result = this.Insert(execParam, esemeny);
            }
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }
}