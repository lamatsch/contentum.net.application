﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.AdatKapu.PartnerSync;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Contentum.eIntegrator.WebService.NMHH
{
    partial class SZURManager
    {
        private const string PARTNERNOTEVALUE = "SZUR";
        private readonly string PartnerSyncNoteValue = "AdatKapu.PartnerSync";

        /// <summary>
        /// AddCimzettToTorzsAdat
        /// </summary>
        /// <param name="cimzett"></param>
        public FoundPartner[] CheckCimzettToTorzsAdat(Cimzett[] cimzett, bool isEVersion)
        {
            List<FoundPartner> foundPartners = new List<FoundPartner>();
            for (int i = 0; i < cimzett.Length; i++)
            {
                foundPartners.Add(CheckCimzettToTorzsAdat(cimzett[i], isEVersion));
            }
            return foundPartners.ToArray();
        }
        /// <summary>
        /// AddCimzettToTorzsAdat
        /// </summary>
        /// <param name="cimzett"></param>
        public FoundPartner CheckCimzettToTorzsAdat(Cimzett cimzett, bool isEVersion)
        {
            FoundPartner result = new FoundPartner();
            bool elektronikus = IsCimzettElektronikus(cimzett);

            #region MAIN
            string mainPartnerId = Guid.NewGuid().ToString();
            KRT_Partnerek mainPartner = GenerateMainPartner(cimzett, mainPartnerId);

            Result resultFindM = FindPartnerSimple(mainPartner.Nev, mainPartner.KulsoAzonositok);
            if (resultFindM == null || resultFindM.Ds.Tables.Count < 1 || resultFindM.Ds.Tables[0].Rows.Count < 1)
            {
                if (elektronikus)
                {
                    throw new Exception(string.Format("{0} partner nem található a partnertörzsben !", cimzett.Szervezet));
                }
            }
            else
            {
                mainPartnerId = resultFindM.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            #endregion

            #region SUB
            string subPartnerId = null;
            if (!string.IsNullOrEmpty(cimzett.Nev) && !string.IsNullOrEmpty(cimzett.Nev))
            {
                subPartnerId = Guid.NewGuid().ToString();
                KRT_Partnerek subPartner = GenerateSubPartner(cimzett, subPartnerId);

                Result resultFindS = FindPartnerSimple(subPartner.Nev, subPartner.KulsoAzonositok);
                if (resultFindS == null || resultFindS.Ds.Tables.Count < 1 || resultFindS.Ds.Tables[0].Rows.Count < 1)
                {
                    if (elektronikus)
                    {
                        throw new Exception(string.Format("{0} partner nem található a partnertörzsben !", cimzett.Nev));
                    }
                }
                else
                {
                    subPartnerId = resultFindS.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
            }
            result.Partner_Id = subPartnerId;
            #endregion

            #region KAPCSOLATTARTO

            KRT_Cimek cimTitkos = null;

            if (elektronikus && !string.IsNullOrEmpty(cimzett.KapcsolattartoAzonosito))
            {
                if (string.IsNullOrEmpty(cimzett.Nev))
                    cimTitkos = GenerateTitkosCimElektronikus(cimzett, EnumCimForras.HIRID);
                else
                    cimTitkos = GenerateTitkosCimElektronikus(cimzett, EnumCimForras.CONTACTCODE);

                Result resultFindCim = FindCim(cimTitkos);
                if (resultFindCim == null || resultFindCim.Ds.Tables[0].Rows.Count < 1)
                {                    
                    throw new Exception(string.Format("{0} kapcsolattartó nem található a partnertörzsben !", cimzett.KapcsolattartoAzonosito));
                }
                else
                {
                    cimTitkos.Id = resultFindCim.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
            }
            else if (!string.IsNullOrEmpty(cimzett.KapcsolattartoAzonosito))
            {
                #region  CIM
                if (string.IsNullOrEmpty(cimzett.Nev))
                    cimTitkos = GenerateTitkosCim(cimzett, EnumCimForras.HIRID);
                else
                    cimTitkos = GenerateTitkosCim(cimzett, EnumCimForras.CONTACTCODE);

                Result resultFindCim = FindCim(cimTitkos);
                if (resultFindCim == null || resultFindCim.Ds.Tables[0].Rows.Count < 1)
                {                    
                    throw new Exception(string.Format("{0} kapcsolattartó nem található a partnertörzsben !", cimzett.KapcsolattartoAzonosito));
                }
                else
                {
                    cimTitkos.Id = resultFindCim.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
            }
            #endregion

            #region PARTNERCIM
            if (cimTitkos != null)
            {
                if (!string.IsNullOrEmpty(subPartnerId))
                    result.PartnerCim_Id = GetPartnerCim(cimTitkos.Id, subPartnerId);
                else
                    result.PartnerCim_Id = GetPartnerCim(cimTitkos.Id, mainPartnerId);
            }
            #endregion
            #endregion

            #region POSTAI 
            #region CIM
            KRT_Cimek cimPostai;
            if (string.IsNullOrEmpty(cimzett.Nev))
                cimPostai = GeneratePostaiCim(cimzett, EnumCimForras.HIRID);
            else
                cimPostai = GeneratePostaiCim(cimzett, EnumCimForras.CONTACTCODE);

            Result resultFindCimPostai = FindPostaiCim(cimPostai);
            if (resultFindCimPostai != null && resultFindCimPostai.Ds.Tables[0].Rows.Count > 0)
            {
                cimPostai.Id = resultFindCimPostai.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            result.Cim_Id = cimPostai.Id;
            #endregion

            #region PARTNER CIM
            if (cimPostai != null)
            {
                if (!string.IsNullOrEmpty(subPartnerId))
                    result.PartnerCim_Id = GetPartnerCim(cimPostai.Id, subPartnerId);
                else
                    result.PartnerCim_Id = GetPartnerCim(cimPostai.Id, mainPartnerId);
            }
            #endregion
            #endregion
            return result;
        }




        /// <summary>
        /// AddCimzettToTorzsAdat
        /// </summary>
        /// <param name="cimzett"></param>
        public FoundPartner[] AddCimzettToTorzsAdat(Cimzett[] cimzett)
        {
            List<FoundPartner> foundPartners = new List<FoundPartner>();
            for (int i = 0; i < cimzett.Length; i++)
            {
                foundPartners.Add(AddCimzettToTorzsAdat(cimzett[i]));
            }
            return foundPartners.ToArray();
        }
        /// <summary>
        /// AddCimzettToTorzsAdat
        /// </summary>
        /// <param name="cimzett"></param>
        public FoundPartner AddCimzettToTorzsAdat(Cimzett cimzett)
        {
            FoundPartner result = new FoundPartner();

            #region MAIN
            string mainPartnerId = Guid.NewGuid().ToString();
            KRT_Partnerek mainPartner = GenerateMainPartner(cimzett, mainPartnerId);

            Result resultFindM = FindPartnerSimple(mainPartner.Nev, mainPartner.KulsoAzonositok);
            if (resultFindM == null || resultFindM.Ds.Tables.Count < 1 || resultFindM.Ds.Tables[0].Rows.Count < 1)
            {
                Result resultAdd = AddMainPartnerSimple(cimzett, mainPartnerId.ToString());
                resultAdd.CheckError();
                mainPartnerId = resultAdd.Uid;
                Logger.Info("AddMainPartner = " + cimzett.Szervezet);
            }
            else
            {
                mainPartnerId = resultFindM.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            #endregion

            #region SUB
            string subPartnerId = null;
            if (!string.IsNullOrEmpty(cimzett.Nev))
            {
                subPartnerId = Guid.NewGuid().ToString();
                KRT_Partnerek subPartner = GenerateSubPartner(cimzett, subPartnerId);

                Result resultFindS = FindPartnerSimple(subPartner.Nev, subPartner.KulsoAzonositok);
                if (resultFindS == null || resultFindS.Ds.Tables.Count < 1 || resultFindS.Ds.Tables[0].Rows.Count < 1)
                {
                    Result resultAdd = AddSubPartner(cimzett, mainPartnerId.ToString(), subPartnerId);
                    resultAdd.CheckError();
                    subPartnerId = resultAdd.Uid;
                    Logger.Info("AddSubPartner = " + cimzett.Nev);
                }
                else
                {
                    subPartnerId = resultFindS.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
            }
            result.Partner_Id = subPartnerId;
            #endregion

            #region KAPCSOLATTARTO
            KRT_Cimek cimTitkos = null;
            if (!string.IsNullOrEmpty(cimzett.KapcsolattartoAzonosito))
            {
                #region  CIM
                if (string.IsNullOrEmpty(cimzett.Nev))
                    cimTitkos = GenerateTitkosCim(cimzett, EnumCimForras.HIRID);
                else
                    cimTitkos = GenerateTitkosCim(cimzett, EnumCimForras.CONTACTCODE);

                Result resultFindCim = FindCim(cimTitkos);
                if (resultFindCim == null || resultFindCim.Ds.Tables[0].Rows.Count < 1)
                {
                    Result resultAddedCim = AddCim(cimTitkos);
                    resultAddedCim.CheckError();
                    cimTitkos.Id = resultAddedCim.Uid;
                }
                else
                {
                    cimTitkos.Id = resultFindCim.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
            }
            #endregion

            #region PARTNERCIM
            if (cimTitkos != null)
            {
                if (!string.IsNullOrEmpty(subPartnerId))
                    result.PartnerCim_Id = AddOrSetPartnerCim(cimTitkos.Id, subPartnerId);
                else
                    result.PartnerCim_Id = AddOrSetPartnerCim(cimTitkos.Id, mainPartnerId);
            }
            #endregion
            #endregion

            #region POSTAI 
            #region CIM
            KRT_Cimek cimPostai;
            if (string.IsNullOrEmpty(cimzett.Nev))
                cimPostai = GeneratePostaiCim(cimzett, EnumCimForras.HIRID);
            else
                cimPostai = GeneratePostaiCim(cimzett, EnumCimForras.CONTACTCODE);

            Result resultFindCimPostai = FindPostaiCim(cimPostai);
            if (resultFindCimPostai == null || resultFindCimPostai.Ds.Tables[0].Rows.Count < 1)
            {
                Result resultAddedCimPostai = AddCim(cimPostai);
                resultAddedCimPostai.CheckError();
                cimPostai.Id = resultAddedCimPostai.Uid;
            }
            else
            {
                cimPostai.Id = resultFindCimPostai.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            result.Cim_Id = cimPostai.Id;
            #endregion

            #region PARTNER CIM
            if (cimPostai != null)
            {
                if (!string.IsNullOrEmpty(subPartnerId))
                    result.PartnerCim_Id = AddOrSetPartnerCim(cimPostai.Id, subPartnerId);
                else
                    result.PartnerCim_Id = AddOrSetPartnerCim(cimPostai.Id, mainPartnerId);
            }
            #endregion
            #endregion
            return result;
        }

        #region GENERATE PARTNER
        private KRT_Partnerek GenerateMainPartner(Cimzett item, string id)
        {
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = Guid.NewGuid().ToString();
            partner.Nev = item.Szervezet;
            partner.Forras = KodTarak.Partner_Forras.ADATKAPU;
            partner.Tipus = KodTarak.Partner_Tipus.Szervezet;
            if (!string.IsNullOrEmpty(item.UgyfelAzonosito))
            {
                partner.KulsoAzonositok = string.Join(",", new string[] { item.UgyfelAzonosito });
            }
            else if (!string.IsNullOrEmpty(item.KapcsolattartoAzonosito))
            {
                partner.KulsoAzonositok = string.IsNullOrEmpty(item.KapcsolattartoAzonosito) ? "" : item.KapcsolattartoAzonosito;
            }
            partner.Base.Note = PARTNERNOTEVALUE;
            partner.Allapot = KodTarak.Partner_Allapot.Jovahagyott;
            return partner;
        }
        /// <summary>
        /// GenerateSubPartner
        /// </summary>
        /// <param name="item"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private KRT_Partnerek GenerateSubPartner(Cimzett item, string id)
        {
            KRT_Partnerek partner = new KRT_Partnerek();
            partner.Id = id;
            partner.Nev = item.Nev;
            if (!string.IsNullOrEmpty(item.KapcsolattartoAzonosito))
            {
                partner.KulsoAzonositok = item.KapcsolattartoAzonosito;
            }
            else if (!string.IsNullOrEmpty(item.UgyfelAzonosito))
            {
                partner.KulsoAzonositok = string.Join(",", new string[] { item.UgyfelAzonosito });
            }

            partner.Forras = KodTarak.Partner_Forras.ADATKAPU;
            partner.Tipus = KodTarak.Partner_Tipus.Szemely;
            partner.Base.Note = PARTNERNOTEVALUE;
            partner.Allapot = KodTarak.Partner_Allapot.Jovahagyott;
            return partner;
        }
        #endregion

        #region FIND
        public Result FindPartnerSimple(string name, string kulsoAzonosito)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            ExecParam execParam = ExecParamCaller.Clone();
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            KRT_PartnerekSearch searchObj = new KRT_PartnerekSearch();
            searchObj.Nev.FilterIfNotEmpty(name);
            if (!string.IsNullOrEmpty(kulsoAzonosito))
            {
                searchObj.KulsoAzonositok.Like(string.Format("%{0}%", kulsoAzonosito));
            }
            Result result = service.GetAll(execParam, searchObj);
            result.CheckError();
            return result;
        }

        /// <summary>
        /// GetPartner
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KRT_Partnerek GetPartner(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;

            ExecParam execParam = this.ExecParamCaller.Clone();
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            execParam.Record_Id = id;

            Result result = service.Get(execParam);
            result.CheckError();

            return (KRT_Partnerek)result.Record;
        }
        #endregion

        #region ADD
        private Result AddMainPartnerSimple(Cimzett item, string id)
        {
            ExecParam execParam = this.ExecParamCaller.Clone();
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();

            KRT_Partnerek newPartner = GenerateMainPartner(item, id);

            Result result = service.Insert(execParam, newPartner);
            result.CheckError();
            return result;
        }

        /// <summary>
        /// AddSubPartner
        /// </summary>
        /// <param name="item"></param>
        /// <param name="parentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private Result AddSubPartner(Cimzett item, string parentId, string id)
        {
            ExecParam execParam = ExecParamCaller.Clone();
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            KRT_PartnerKapcsolatokService serviceKapcsolat = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();

            KRT_Partnerek newPartner = GenerateSubPartner(item, id);
            id = newPartner.Id;

            Result result = service.Insert(execParam, newPartner);
            result.CheckError();

            Logger.Info("AddPartnerkapcsolat= " + newPartner.Nev);

            AddPartnerkapcsolat(parentId, id, newPartner.Nev);

            #region SZEMELY
            KRT_Szemelyek szemely = GenerateSzemely(item, newPartner.Id);
            if (szemely != null)
            {
                Result resultFindSzemely = FindSzemelyPartner(szemely);
                if (resultFindSzemely == null || resultFindSzemely.GetCount < 1)
                {
                    Logger.Info("AddSzemely= " + szemely.UjCsaladiNev + string.Empty + szemely.UjUtonev + string.Empty + szemely.UjTovabbiUtonev);
                    AddSzemely(szemely);
                }
            }
            #endregion

            return result;
        }
        private void AddPartnerkapcsolat(string parentId, string id, string partnerNev)
        {
            ExecParam execParam = this.ExecParamCaller.Clone();
            KRT_PartnerKapcsolatokService serviceKapcsolat = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
            KRT_PartnerKapcsolatokSearch src = new KRT_PartnerKapcsolatokSearch();
            src.Partner_id.Filter(id);
            src.Partner_id_kapcsolt.Filter(parentId);
            src.Tipus.Filter(KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja);

            Result resultFind = serviceKapcsolat.GetAll(execParam, src);
            resultFind.CheckError();

            if (resultFind == null || resultFind.GetCount < 1)
            {
                KRT_PartnerKapcsolatok kapcsolat = new KRT_PartnerKapcsolatok();
                kapcsolat.Id = Guid.NewGuid().ToString();
                kapcsolat.Partner_id = id;
                kapcsolat.Partner_id_kapcsolt = parentId;
                kapcsolat.Tipus = KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja;
                kapcsolat.Base.Note = PARTNERNOTEVALUE;

                Result resultKapcsolat = serviceKapcsolat.Insert(execParam, kapcsolat);
                resultKapcsolat.CheckError();

                Logger.Info("AddPartnerkapcsolat= " + partnerNev);
            }
        }
        #endregion

        #region KRT_SZEMELY
        /// <summary>
        /// GenerateSzemely
        /// </summary>
        /// <param name="item"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private KRT_Szemelyek GenerateSzemely(Cimzett item, string partnerId)
        {
            if (string.IsNullOrEmpty(item.Nev))
                return null;

            KRT_Szemelyek szemely = new KRT_Szemelyek();
            szemely.Beosztas = item.Beosztas;
            szemely.UjTitulis = item.Elotag;
            string[] splitted = item.Nev.Split(new string[] { " " }, System.StringSplitOptions.RemoveEmptyEntries);
            if (splitted == null || splitted.Length < 1)
                return null;

            byte indexCsaladiNev = 0;
            byte indexUtoNev = 1;
            byte indexUtoNevTovabbi = 2;

            if (splitted[indexCsaladiNev].ToUpper() == KodTarak.TITULUSOK.DR
                || splitted[indexCsaladiNev].ToUpper() == KodTarak.TITULUSOK.IFJ
                || splitted[indexCsaladiNev].ToUpper() == KodTarak.TITULUSOK.OZV
                || splitted[indexCsaladiNev].ToUpper() == KodTarak.TITULUSOK.II)
            {
                szemely.UjTitulis = splitted[0];

                indexCsaladiNev += 1;
                indexUtoNev += 1;
                indexUtoNevTovabbi += 1;
            }
            szemely.Id = System.Guid.NewGuid().ToString();
            szemely.Partner_Id = partnerId;
            szemely.UjCsaladiNev = splitted[indexCsaladiNev];
            if (splitted.Length >= indexCsaladiNev + 1)
                szemely.UjUtonev = splitted[indexUtoNev];
            if (splitted.Length >= indexUtoNevTovabbi + 1)
                szemely.UjTovabbiUtonev = splitted[indexUtoNevTovabbi];

            szemely.Base.Note = PARTNERNOTEVALUE;
            return szemely;
        }
        /// <summary>
        /// FindSzemelyPartner
        /// </summary>
        /// <param name="szemely"></param>
        /// <returns></returns>
        private Result FindSzemelyPartner(KRT_Szemelyek szemely)
        {
            ExecParam execParam = this.ExecParamCaller.Clone();
            KRT_SzemelyekService service = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
            KRT_SzemelyekSearch searchObj = new KRT_SzemelyekSearch();

            searchObj.UjCsaladiNev.FilterIfNotEmpty(szemely.UjCsaladiNev);
            searchObj.UjUtonev.FilterIfNotEmpty(szemely.UjUtonev);
            searchObj.UjTovabbiUtonev.FilterIfNotEmpty(szemely.UjTovabbiUtonev);
            searchObj.Partner_Id.Filter(szemely.Partner_Id);

            Result result = service.GetAll(execParam, searchObj);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error(result.ErrorCode + string.Empty + result.ErrorMessage);
                return null;
            }

            return result;
        }

        /// <summary>
        /// AddSzemely
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private Result AddSzemely(KRT_Szemelyek item)
        {
            ExecParam execParam = ExecParamCaller.Clone();
            KRT_SzemelyekService service = eAdminService.ServiceFactory.GetKRT_SzemelyekService();
            Result result = service.Insert(execParam, item);
            result.CheckError();
            return result;
        }
        #endregion

        #region KRT_CIMEK
        public KRT_Cimek GenerateTitkosCim(Cimzett item, EnumCimForras forras)
        {
            KRT_Cimek cim = new KRT_Cimek();
            cim.Id = Guid.NewGuid().ToString();
            cim.Nev = string.IsNullOrEmpty(item.Nev) ? item.Szervezet : item.Nev;
            cim.IRSZ = item.Iranyitoszam;
            cim.TelepulesNev = item.Telepules;
            cim.KozteruletNev = item.Cim;
            cim.CimTobbi = item.KapcsolattartoAzonosito;
            cim.Forras = KodTarak.Cim_Forras.AK;
            cim.Tipus = KodTarak.Cim_Tipus.TitkosKapcsolatiKod;
            if (!string.IsNullOrEmpty(item.KapcsolattartoAzonosito))
                cim.KulsoAzonositok = item.KapcsolattartoAzonosito;

            cim.Kategoria = KodTarak.Cim_Kategoria.K;
            cim.Base.Note = PARTNERNOTEVALUE;
            return cim;
        }

        public KRT_Cimek GenerateTitkosCimElektronikus(Cimzett item, EnumCimForras forras)
        {
            KRT_Cimek cim = new KRT_Cimek();
            cim.Id = Guid.NewGuid().ToString();
            cim.Nev = string.IsNullOrEmpty(item.Nev) ? item.Szervezet : item.Nev;
            cim.CimTobbi = item.KapcsolattartoAzonosito;            
            //BUG 10150
            //cim.Tipus = KodTarak.Cim_Tipus.TitkosKapcsolatiKod;
            if (!string.IsNullOrEmpty(item.KapcsolattartoAzonosito))
                cim.KulsoAzonositok = item.KapcsolattartoAzonosito;

            cim.Kategoria = KodTarak.Cim_Kategoria.K;
            cim.Base.Note = PARTNERNOTEVALUE;
            return cim;
        }

        public KRT_Cimek GeneratePostaiCim(Cimzett item, EnumCimForras forras)
        {
            KRT_Cimek cim = new KRT_Cimek();
            cim.Id = Guid.NewGuid().ToString();
            cim.Nev = string.IsNullOrEmpty(item.Nev) ? item.Szervezet : item.Nev;
            cim.IRSZ = item.Iranyitoszam;
            cim.TelepulesNev = item.Telepules;
            cim.KozteruletNev = item.Cim;
            cim.CimTobbi = string.IsNullOrEmpty(item.KapcsolattartoAzonosito) ? "" : item.KapcsolattartoAzonosito;
            cim.Forras = KodTarak.Cim_Forras.AK;
            cim.Tipus = KodTarak.Cim_Tipus.Postai;
            if (!string.IsNullOrEmpty(item.KapcsolattartoAzonosito))
                cim.KulsoAzonositok = item.KapcsolattartoAzonosito;

            cim.Kategoria = KodTarak.Cim_Kategoria.K;
            cim.Base.Note = PARTNERNOTEVALUE;
            return cim;
        }
        public Result FindPostaiCim(KRT_Cimek item)
        {
            if (string.IsNullOrEmpty(item.Nev))
                return null;

            ExecParam execParam = ExecParamCaller.Clone();
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            KRT_CimekSearch searchObj = new KRT_CimekSearch();

            searchObj.Nev.Filter(item.Nev);
            searchObj.KulsoAzonositok.FilterIfNotEmpty(item.KulsoAzonositok);
            searchObj.IRSZ.FilterIfNotEmpty(item.IRSZ);
            searchObj.TelepulesNev.FilterIfNotEmpty(item.TelepulesNev);
            searchObj.KozteruletNev.FilterIfNotEmpty(item.KozteruletNev);
            searchObj.CimTobbi.FilterIfNotEmpty(item.CimTobbi);

            //searchObj.Forras.Value = item.Forras;
            //searchObj.Forras.Operator = Query.Operators.equals;
            searchObj.Tipus.FilterIfNotEmpty(item.Tipus);

            Result result = service.GetAll(execParam, searchObj);
            result.CheckError();
            return result;
        }
        public Result FindCim(KRT_Cimek item)
        {
            if (string.IsNullOrEmpty(item.Nev))
                return null;

            ExecParam execParam = this.ExecParamCaller.Clone();
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            KRT_CimekSearch searchObj = new KRT_CimekSearch();

            // BUG#7784: Ha ki van töltve a KulsoAzonositok mező, akkor a Nev mezőre már nem szűrünk, mert az ki tudja, hogy van töltve...
            if (String.IsNullOrEmpty(item.KulsoAzonositok))
            {
                searchObj.Nev.Filter(item.Nev);
            }

            if (!string.IsNullOrEmpty(item.KulsoAzonositok))
            {
                searchObj.KulsoAzonositok.Filter(item.KulsoAzonositok);
                searchObj.KulsoAzonositok.OrGroup("azon");
            }

            searchObj.IRSZ.FilterIfNotEmpty(item.IRSZ);
            searchObj.TelepulesNev.FilterIfNotEmpty(item.TelepulesNev);
            searchObj.KozteruletNev.FilterIfNotEmpty(item.KozteruletNev);

            if (!string.IsNullOrEmpty(item.CimTobbi))
            {
                searchObj.CimTobbi.Filter(item.CimTobbi);
                searchObj.CimTobbi.OrGroup("azon");
            }

            //searchObj.Forras.Value = item.Forras;
            //searchObj.Forras.Operator = Query.Operators.equals;
            searchObj.Tipus.FilterIfNotEmpty(item.Tipus);

            Result result = service.GetAll(execParam, searchObj);
            result.CheckError();
            return result;
        }
        public KRT_Cimek FindCimById(string cimId)
        {
            if (string.IsNullOrEmpty(cimId))
                return null;

            ExecParam execParam = ExecParamCaller.Clone();
            execParam.Record_Id = cimId;
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            KRT_CimekSearch searchObj = new KRT_CimekSearch();

            Result result = service.Get(execParam);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                return null;
            return (KRT_Cimek)result.Record;
        }
        public Result AddCim(KRT_Cimek item)
        {
            ExecParam execParam = this.ExecParamCaller.Clone();
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            Result result = service.Insert(execParam, item);
            result.CheckError();
            return result;
        }

        public KRT_PartnerCimek GeneratePartnerCim(string cimId, string partnerId)
        {
            if (string.IsNullOrEmpty(partnerId))
                return null;

            KRT_PartnerCimek partnerCim = new KRT_PartnerCimek();
            partnerCim.Id = System.Guid.NewGuid().ToString();
            partnerCim.Partner_id = partnerId;
            partnerCim.Cim_Id = cimId;
            partnerCim.Base.Note = PARTNERNOTEVALUE;
            return partnerCim;
        }
        public Result AddPartnerCim(KRT_PartnerCimek item)
        {
            if (item == null)
                return null;

            KRT_Cimek cimFound = FindCimById(item.Cim_Id);
            if (cimFound == null)
            {
                Logger.Info("PartnerCim nem letrehzhato, mert cim nem talalhato:" + item.Cim_Id);
                return null;
            }

            ExecParam execParam = ExecParamCaller.Clone();
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            Result result = service.Insert(execParam, item);
            result.CheckError();
            return result;
        }
        public Result FindPartnerCim(KRT_PartnerCimek item)
        {
            if (item == null || string.IsNullOrEmpty(item.Cim_Id) || string.IsNullOrEmpty(item.Partner_id))
                return null;

            ExecParam execParam = ExecParamCaller.Clone();
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            KRT_PartnerCimekSearch searchObj = new KRT_PartnerCimekSearch();

            searchObj.Partner_id.Filter(item.Partner_id);
            searchObj.Cim_Id.Filter(item.Cim_Id);

            Result result = service.GetAll(execParam, searchObj);
            result.CheckError();
            return result;
        }
        #endregion

        #region PARTNER CIM
        /// <summary>
        /// /AddOrSetPartnerCim
        /// </summary>
        /// <param name="cimId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        private string AddOrSetPartnerCim(string cimId, string partnerId)
        {
            KRT_PartnerCimek partnerCim = GeneratePartnerCim(cimId, partnerId);
            Result resultFindPartnerCim = FindPartnerCim(partnerCim);
            if (resultFindPartnerCim == null || !string.IsNullOrEmpty(resultFindPartnerCim.ErrorCode))
            {
                Logger.Error("FindPartnerCim.ERROR=" + resultFindPartnerCim.ErrorCode + resultFindPartnerCim.ErrorMessage);
                return null;
            }

            if (resultFindPartnerCim.GetCount < 1)
            {
                Result resultAddPartnerCim = AddPartnerCim(partnerCim);
                resultAddPartnerCim.CheckError();
                Logger.Error(string.Format("AddPartnerCim.ADD= C={0} P={1}", cimId, partnerId));
                return resultAddPartnerCim.Uid;
            }
            return resultFindPartnerCim.Ds.Tables[0].Rows[0]["Id"].ToString();
        }
        /// <summary>
        /// /AddOrSetPartnerCim
        /// </summary>
        /// <param name="cimId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        private string GetPartnerCim(string cimId, string partnerId)
        {
            KRT_PartnerCimek partnerCim = GeneratePartnerCim(cimId, partnerId);
            Result resultFindPartnerCim = FindPartnerCim(partnerCim);
            if (resultFindPartnerCim == null || !string.IsNullOrEmpty(resultFindPartnerCim.ErrorCode))
            {
                Logger.Error("FindPartnerCim.ERROR=" + resultFindPartnerCim.ErrorCode + resultFindPartnerCim.ErrorMessage);
                return null;
            }

            if (resultFindPartnerCim.GetCount < 1)
            {
                return null;
            }
            return resultFindPartnerCim.Ds.Tables[0].Rows[0]["Id"].ToString();
        }
        #endregion

        #region Cimzett

        /// <summary>
        /// Cimzett objektum összeállítása a megadott DataRow adatokból (iratpéldány, partner, személy, cím)
        /// </summary>
        /// <param name="iratpeldanyRow">Kötelező</param>
        /// <param name="partnerRow"></param>
        /// <param name="szemelyRow"></param>
        /// <param name="cimRow"></param>
        /// <param name="kapcsoltPartnerSzervezetRow">"Szervezet kapcsolattartója" kapcsolatban álló partner (szervezet) DataRow-ja</param>
        /// <param name="ktFuggosegKuldKuldesMod2NmhhExpMod"></param>
        /// <returns></returns>
        public Cimzett CreateCimzett(DataRow iratpeldanyRow, DataRow partnerRow, DataRow szemelyRow, DataRow cimRow, DataRow kapcsoltPartnerSzervezetRow, KodtarFuggosegDataClass ktFuggosegKuldKuldesMod2NmhhExpMod)
        {
            Cimzett cimzett = new Cimzett();

            if (partnerRow != null)
            {
                cimzett.Nev = partnerRow["Nev"].ToString();
                cimzett.KapcsolattartoAzonosito = partnerRow["KulsoAzonositok"].ToString();
            }

            // Szervezet:
            if (kapcsoltPartnerSzervezetRow != null)
            {
                cimzett.Szervezet = kapcsoltPartnerSzervezetRow["Nev"].ToString();
                cimzett.UgyfelAzonosito = kapcsoltPartnerSzervezetRow["KulsoAzonositok"].ToString();
            }

            // Elotag: KRT_Szemelyek.UjTitulis
            if (szemelyRow != null)
            {
                cimzett.Elotag = szemelyRow["UjTitulis"].ToString();
                cimzett.Beosztas = szemelyRow["Beosztas"].ToString();
            }

            if (cimRow != null)
            {
                cimzett.Iranyitoszam = cimRow["IRSZ"].ToString();
                cimzett.Telepules = cimRow["TelepulesNev"].ToString();

                // Elvileg a Cim mezőbe nem kell az irányítószám és a település, így ide a KozteruletNev + KozteruletTipusNev + Hazszam összefűzött szöveget tesszük bele:
                string kozteruletNev = cimRow["KozteruletNev"].ToString();
                string kozteruletTipusNev = cimRow["KozteruletTipusNev"].ToString();
                string hazSzam = cimRow["Hazszam"].ToString();

                cimzett.Cim = kozteruletNev
                                + (!String.IsNullOrEmpty(kozteruletTipusNev) ? " " + kozteruletTipusNev : String.Empty)
                                + (!String.IsNullOrEmpty(hazSzam) ? " " + hazSzam : String.Empty);
            }

            if (String.IsNullOrEmpty(cimzett.Cim))
            {
                cimzett.Cim = iratpeldanyRow["CimSTR_Cimzett"].ToString();
            }

            // cimzett.Elektronikus: 'UgyintezesModja' alapján:
            if (iratpeldanyRow["UgyintezesModja"].ToString() == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
            {
                cimzett.Elektronikus = "I";
            }
            else
            {
                cimzett.Elektronikus = "N";
            }

            string kuldesModKod = iratpeldanyRow["KuldesMod"].ToString();
            if (!String.IsNullOrEmpty(kuldesModKod))
            {
                // BUG#5901: kódtár mappingből kell kiszedni a megfelelő értéket:
                KodTar_Cache.KodTarElem ktNmhhExpMod = Utility.GetNmhhExpModKodtarByKuldKuldesmod(kuldesModKod, ktFuggosegKuldKuldesMod2NmhhExpMod, ExecParamCaller);

                if (ktNmhhExpMod == null)
                {
                    // Sikertelen a mappelés, ezt jelezzük valahogy:
                    cimzett.cExpedialasMod = "?? (" + iratpeldanyRow["KuldesMod_Nev"].ToString() + ")";
                }
                else
                {
                    cimzett.cExpedialasMod = ktNmhhExpMod.Nev;
                }
            }
            else
            {
                cimzett.cExpedialasMod = String.Empty;
            }

            return cimzett;
        }

        #endregion

        /// <summary>
        /// GetOsszesTobbiKapcsolattarto
        /// </summary>
        /// <param name="cimzett"></param>
        /// <returns></returns>
        public List<Cimzett> GetOsszesTobbiKapcsolattarto(Cimzett[] cimzett)
        {
            List<Cimzett> result = new List<Cimzett>();
            List<Cimzett> tmp;
            for (int i = 0; i < cimzett.Length; i++)
            {
                if (IsCimzettElektronikus(cimzett[i]))
                {
                    tmp = GetSzervezetOsszesTobbiKapcsolattarto(cimzett[i]);
                    if (tmp != null && tmp.Count > 0)
                        result.AddRange(tmp);
                }
            }
            return result;
        }
        /// <summary>
        /// GetSzervezetOsszesTobbiKapcsolattarto
        /// </summary>
        /// <param name="cimzett"></param>
        public List<Cimzett> GetSzervezetOsszesTobbiKapcsolattarto(Cimzett cimzett)
        {
            List<Cimzett> result = new List<Cimzett>();

            ExecParam execParam = ExecParamCaller.Clone();

            FoundPartner foundMainPartner = FindMainPartnerSimple(cimzett);
            if (foundMainPartner == null || foundMainPartner.Partner_Id == null)
                return null;

            FoundPartner foundSubPartner = FindSubPartnerSimple(cimzett);

            KRT_PartnerKapcsolatokService serviceKapcsolat = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
            KRT_PartnerKapcsolatokSearch src = new KRT_PartnerKapcsolatokSearch();
            src.Partner_id_kapcsolt.Filter(foundMainPartner.Partner_Id);

            KRT_PartnerKapcsolatokSearch subPartnersrc = new KRT_PartnerKapcsolatokSearch();
            subPartnersrc.Partner_id_kapcsolt.Filter(foundMainPartner.Partner_Id);

            execParam.Record_Id = foundMainPartner.Partner_Id;

            if (foundSubPartner != null && !string.IsNullOrEmpty(foundSubPartner.Partner_Id))
            {
                src.Partner_id.NotEquals(foundSubPartner.Partner_Id);

                subPartnersrc.Partner_id.Filter(foundSubPartner.Partner_Id);
                subPartnersrc.Tipus.Filter(KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja);

                Result resultSubpartner = serviceKapcsolat.GetAll(execParam, subPartnersrc);
                if (resultSubpartner.IsError)
                {
                    Logger.Error(String.Format("A partnerhez nem sikerült lekérni a kapcsolatokat: {0},{1}", resultSubpartner.ErrorCode, resultSubpartner.ErrorMessage));
                    throw new ResultException(resultSubpartner);
                }

                if (resultSubpartner == null || resultSubpartner.GetCount < 1)
                {
                    throw new Exception(string.Format("{0} kapcsolattartó nem az adott ügyfélazonosítójú céghez tartozik. {1}  nem kapcsolattartója az {2} szervezetnek.",
                        cimzett.KapcsolattartoAzonosito, cimzett.Nev, cimzett.Szervezet));
                }
            }
            src.Tipus.Filter(KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja);

            Result resultFindAll = serviceKapcsolat.GetAllByPartner(execParam, src);

            if (resultFindAll.IsError)
            {
                Logger.Error(String.Format("A partnerhez nem sikerült lekérni a kapcsolatokat: {0},{1}", resultFindAll.ErrorCode, resultFindAll.ErrorMessage));
                throw new ResultException(resultFindAll);
            }

            List<string> partnerkapcsolatokIds = new List<string>();
            KRT_Partnerek partner;
            Cimzett tmpCimzett;
            foreach (DataRow row in resultFindAll.Ds.Tables[0].Rows)
            {
                partner = GetPartner(row["Partner_id"].ToString());
                if (partner.Forras != KodTarak.Partner_Forras.ADATKAPU)
                    continue;

                tmpCimzett = new Cimzett();
                tmpCimzett.cExpedialasMod = cimzett.cExpedialasMod;//?
                tmpCimzett.cKuldemenyTipus = cimzett.cKuldemenyTipus;//?
                tmpCimzett.Elektronikus = cimzett.Elektronikus;//?
                tmpCimzett.Szervezet = cimzett.Szervezet;
                tmpCimzett.Nev = partner.Nev;
                tmpCimzett.UgyfelAzonosito = partner.KulsoAzonositok;
                tmpCimzett.KapcsolattartoAzonosito = partner.KulsoAzonositok;

                result.Add(tmpCimzett);
            }
            return result;
        }

        /// <summary>
        /// ExtendCimzettListaWitExistingKapcsolattarto
        /// </summary>
        /// <param name="original"></param>
        /// <param name="more"></param>
        /// <returns></returns>
        public Cimzett[] KibovitOsszesTobbiKapcsolattartoval(Cimzett[] original)
        {
            if (original == null || original.Length < 1)
                return original;

            List<Cimzett> more = GetOsszesTobbiKapcsolattarto(original);
            if (more == null || more.Count < 1)
                return original;

            List<Cimzett> result = new List<Cimzett>();
            result.AddRange(original);

            for (int i = 0; i < more.Count; i++)
            {
                if (result.Any(c => c.Nev == more[i].Nev && c.KapcsolattartoAzonosito == more[i].KapcsolattartoAzonosito))
                    continue;
                else
                    result.Add(more[i]);
            }

            return result.ToArray();
        }
    }
}
