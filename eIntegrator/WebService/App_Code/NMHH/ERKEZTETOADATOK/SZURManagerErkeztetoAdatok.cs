﻿using System;
using Contentum.eUtility;
using System.Data;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;

/// <summary>
/// Summary description for SZURManagerIrat
/// </summary>
namespace Contentum.eIntegrator.WebService.NMHH
{
    /// <summary>
    /// SZURMAnager - VezetoiUtasitas
    /// </summary>
    public partial class SZURManager
    {
        /// <summary>
        /// Érkeztetési adatok átadása
        /// "A SZÜR-ből érkeztetési adatok átadása a szakrendszer részére."
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UserId"></param>
        /// <param name="ElhiszErkeztetoszam"></param>
        /// <param name="Ugyiratszam"></param>
        /// <param name="Erkeztetoszam"></param>
        /// <param name="ErkezettDatum"></param>
        public void SZURGetErkeztetoAdatokWS(string Evszam, string Konyv, string Foszam, string Alszam, string UserId
            , out string ElhiszErkeztetoszam, out string Ugyiratszam, out string Erkeztetoszam, out string ErkezettDatum)
        {
            Logger.InfoStart("SZURManager.SZURGetErkeztetoAdatokWS");

            #region Ellenőrzés
            // Kiszedve az egységes kötelezőség ellenörzés miatt
            //if (String.IsNullOrEmpty(Evszam))
            //{
            //    throw new ResultException("Kötelező évszámot megadni!");
            //}

            //if (String.IsNullOrEmpty(Konyv))
            //{
            //    throw new ResultException("Kötelező iktatókönyv azonosítót megadni!");
            //}

            //if (String.IsNullOrEmpty(Foszam))
            //{
            //    throw new ResultException("Kötelező főszámot megadni!");
            //}

            //if (String.IsNullOrEmpty(Alszam))
            //{
            //    throw new ResultException("Kötelező alszámot megadni!");
            //}

            //if (String.IsNullOrEmpty(UserId))
            //{
            //    throw new ResultException("Kötelező felhasználó azonosítót megadni!");
            //}
            #endregion

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            DataRow iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam, execParamUserId);

            Guid? kuldemenyId = iratRow["KuldKuldemenyek_Id"] as Guid?;
            if (kuldemenyId == null)
            {
                // Hiba:
                throw new ResultException("Az irathoz nem tartozik érkeztetett küldemény!");
            }

            #region Küldemény lekérése

            ExecParam execParamKuldemenyGet = execParamUserId.Clone();
            execParamKuldemenyGet.Record_Id = kuldemenyId.ToString();

            var resultKuldGet = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService().Get(execParamKuldemenyGet);
            if (resultKuldGet.IsError)
            {
                throw new ResultException(resultKuldGet);
            }

            EREC_KuldKuldemenyek kuldemenyObj = (EREC_KuldKuldemenyek)resultKuldGet.Record;

            #endregion

            #region Ügyiratszámhoz ügyirat lekérése

            ExecParam execParamUgyiratGet = execParamUserId.Clone();
            execParamUgyiratGet.Record_Id = iratRow["Ugyirat_Id"].ToString();

            var resultUgyiratGet = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().Get(execParamUgyiratGet);
            if (resultUgyiratGet.IsError)
            {
                throw new ResultException(resultUgyiratGet);
            }
            EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok)resultUgyiratGet.Record;

            #endregion

            ElhiszErkeztetoszam = null;

            #region beadvány adatok lekérdezése 

            var serviceBeadvanyok = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            ExecParam execParamBeadvanyokGet = execParamUserId.Clone();

            var searchObjBeadvanyok = new EREC_eBeadvanyokSearch();

            searchObjBeadvanyok.KuldKuldemeny_Id.Value = kuldemenyId.ToString();
            searchObjBeadvanyok.KuldKuldemeny_Id.Operator = Query.Operators.equals;

            /* // IraIrat_Id nincs mindenhol kitöltve !
             searchObjBeadvanyok.IraIrat_Id.Value = iratRow["Id"].ToString();
             searchObjBeadvanyok.IraIrat_Id.Operator = Query.Operators.equals; 
             */

            var resultBeadvanyok = serviceBeadvanyok.GetAllWithExtension(execParamBeadvanyokGet, searchObjBeadvanyok);

            if (resultBeadvanyok.IsError)
            {
                throw new ResultException(resultBeadvanyok);
            }

            /// BUG#3493/Task#3550:
            /// "nem csak az elektronikust kell visszaadni, hanem mindent
            /// ebből következően az ELHISZ mező kitöltése nem kötelező"
            /// --> ne dobjunk hibát, ha nincs ELHISZ érkeztetőszám
            /// 

            //// Ellenőrzés: csak 1 találat lehet:
            //if (resultBeadvanyok.Ds.Tables[0].Rows.Count == 0)
            //{
            //    throw new ResultException("A beadvány nem található!");
            //}
            //else if (resultBeadvanyok.Ds.Tables[0].Rows.Count > 1)
            //{
            //    throw new ResultException("Beadvány lekérdezése sikertelen: több találat is van!");
            //}

            if (resultBeadvanyok.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow BeadvanyRow = resultBeadvanyok.Ds.Tables[0].Rows[0];
                try
                {
                    ElhiszErkeztetoszam = BeadvanyRow["PR_ErkeztetesiSzam"].ToString();
                }
                catch (Exception)
                {
                    throw new ResultException("ELHISZ érkeztetőszám meghatározása nem sikerült!");
                }
            }

            #endregion

            Ugyiratszam = ugyiratObj.Azonosito;

            // "yyyy.MM.dd hh:mm" formátum
            Erkeztetoszam = kuldemenyObj.Erkezteto_Szam;

            if (!String.IsNullOrEmpty(kuldemenyObj.BeerkezesIdeje))
            {
                ErkezettDatum = ((DateTime)kuldemenyObj.Typed.BeerkezesIdeje).ToString(SZURWSDateTimeFormatString);
            }
            else
            {
                ErkezettDatum = null;
            }

            Logger.InfoEnd("SZURManager.SZURGetErkeztetoAdatokWS");
        }
    }
}