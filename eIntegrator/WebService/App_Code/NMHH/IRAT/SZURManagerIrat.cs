﻿using System;
using Contentum.eUtility;
using System.Data;
using System.Collections.Generic;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Web;

/// <summary>
/// Summary description for SZURManagerIrat
/// </summary>
namespace Contentum.eIntegrator.WebService.NMHH
{
    /// <summary>
    /// SZURMAnager - IRAT
    /// </summary>
    public partial class SZURManager
    {
        /// <summary>
        ///  Ügyirat-irat átvételének visszavonása
        ///  Feladat: az ügy/irat ügyintézésre történő átvételének visszavonása
        ///  BUG#3493/Task#3494: elvárt működés: ügyintézésre átvételt követően és továbbítás alatti állapotból is az átveendők listájára kell visszakerüljön
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZURAtvDeleteWS(string Konyv, string Foszam, string Alszam, string Evszam, string UserId)
        {
            //Kötelező paraméterek ellenőrzése
            #region Ellenőrzés
            // Kiszedve az egységes kötelezőség ellenörzés miatt
            //if (String.IsNullOrEmpty(Foszam))
            //{
            //    Hibakod = Utility.ErrorCodes.GeneralErrorCode;
            //    Hibaszoveg = "Kötelező főszámot megadni!";
            //    throw new ResultException(Hibaszoveg);
            //}

            //if (String.IsNullOrEmpty(UserId))
            //{
            //    Hibakod = Utility.ErrorCodes.GeneralErrorCode;
            //    Hibaszoveg = "Kötelező felhasználó azonosítót megadni!";
            //    throw new ResultException(Hibaszoveg);
            //}
            #endregion

            /// BUG#3493/Task#3494 feladat:
            /// - ügyintézés alatti státuszúnak kell lennie az ügyiratnak (ha továbbítás alatti állapotú, akkor nem kell csinálni vele semmit, egyébként ha nem ügyintézés alatti, akkor hibát dobunk)
            /// - erre az ügyiratra megkeressük a legutóbbi kézbesítési tételt
            /// - a kézbesítési tételben lévő feladóra állítjuk vissza az ügyirat őrzőjét + az ügyirat státuszát továbbítás alattira
            /// - a kézbesítési tétel státuszát visszaállítjuk, hogy újra megjelenjen az ügyirat az átveendők között
            /// 

            DataRow ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam);
            string ugyiratId = ugyiratRow["Id"].ToString();
            string ugyiratAllapot = ugyiratRow["Allapot"].ToString();

            if (ugyiratAllapot != eUtility.KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt
                && ugyiratAllapot != eUtility.KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt)
            {
                throw new ResultException("Az ügyintézésre átvétel nem vonható vissza, mert az ügyirat állapota nem 'Ügyintézés alatt'!");
            }

            var execParam = this.CreateExecParamByNMHHUserId(UserId);

            // Továbbítás alatti állapotnál nem kell csinálni semmit, csak az ügyintézés alattinál:
            if (ugyiratAllapot == eUtility.KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt)
            {
                // Le kell kérni az utolsó kézbesítési tételt, ami a felhasználónak van címezve és átvett, majd azt kell visszaállítani:
                
                #region Kézbesítési tételek lekérdezése

                var kezbesitesiTetelekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();

                var searchObjKezbTetelek = new EREC_IraKezbesitesiTetelekSearch();

                searchObjKezbTetelek.Csoport_Id_Cel.Value = execParam.Felhasznalo_Id;
                searchObjKezbTetelek.Csoport_Id_Cel.Operator = Query.Operators.equals;

                searchObjKezbTetelek.Allapot.Value = KodTarak.KEZBESITESITETEL_ALLAPOT.Atvett;
                searchObjKezbTetelek.Allapot.Operator = Query.Operators.equals;

                searchObjKezbTetelek.Obj_type.Value = "EREC_UgyUgyiratok";
                searchObjKezbTetelek.Obj_type.Operator = Query.Operators.equals;

                searchObjKezbTetelek.Obj_Id.Value = ugyiratId;
                searchObjKezbTetelek.Obj_Id.Operator = Query.Operators.equals;

                searchObjKezbTetelek.OrderBy = "AtvetelDat DESC";

                var resultKezbTetelek = kezbesitesiTetelekService.GetAllWithExtension(execParam, searchObjKezbTetelek);
                resultKezbTetelek.CheckError();
                
                #endregion

                if (resultKezbTetelek.Ds.Tables[0].Rows.Count == 0)
                {
                    // Átvétel visszavonás nem lehetséges:
                    throw new ResultException("Átvétel visszavonása nem lehetséges: nem található kézbesítési tétel!");
                }
                else
                {
                    #region Kézbesítési tétel UPDATE

                    /// Az állapotot visszaállítjuk átadottra, így meg fog jelenni az átveendők között.
                    /// 

                    Guid kezbesitesiTetelIdUtolso = (Guid)resultKezbTetelek.Ds.Tables[0].Rows[0]["Id"];
                    ExecParam execParamKezbTetelGet = execParam.Clone();
                    execParamKezbTetelGet.Record_Id = kezbesitesiTetelIdUtolso.ToString();

                    var resultKezbTetelUtolso = kezbesitesiTetelekService.Get(execParamKezbTetelGet);
                    resultKezbTetelUtolso.CheckError();

                    EREC_IraKezbesitesiTetelek kezbTetelUtolso = (EREC_IraKezbesitesiTetelek)resultKezbTetelUtolso.Record;

                    kezbTetelUtolso.Updated.SetValueAll(false);
                    kezbTetelUtolso.Base.Updated.SetValueAll(false);

                    kezbTetelUtolso.Base.Updated.Ver = true;

                    kezbTetelUtolso.Allapot = KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott;
                    kezbTetelUtolso.Updated.Allapot = true;

                    kezbTetelUtolso.AtvetelDat = "<null>";
                    kezbTetelUtolso.Updated.AtvetelDat = true;

                    kezbTetelUtolso.Felhasznalo_Id_AtvevoLogin = String.Empty;
                    kezbTetelUtolso.Updated.Felhasznalo_Id_AtvevoLogin = true;
                    
                    kezbTetelUtolso.Felhasznalo_Id_AtvevoUser = String.Empty;
                    kezbTetelUtolso.Updated.Felhasznalo_Id_AtvevoUser = true;

                    var resultKezbTetelUpdate = kezbesitesiTetelekService.Update(execParamKezbTetelGet, kezbTetelUtolso);
                    resultKezbTetelUpdate.CheckError();

                    #endregion

                    #region Ügyirat UPDATE

                    // Ügyirat státuszt vissza kell állítani Továbbítás alattira, az őrzőt pedig vissza kell tenni arra, aki a kézbesítési tétel feladója volt

                    ExecParam execParamUgyiratGet = execParam.Clone();
                    execParamUgyiratGet.Record_Id = ugyiratId.ToString();

                    var ugyiratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    var resultUgyiratGet = ugyiratService.Get(execParamUgyiratGet);
                    resultUgyiratGet.CheckError();

                    EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok)resultUgyiratGet.Record;

                    // Ügyirat UPDATE:
                    ugyiratObj.Updated.SetValueAll(false);
                    ugyiratObj.Base.Updated.SetValueAll(false);

                    ugyiratObj.Base.Updated.Ver = true;

                    // Továbbítás alatti állapotnak betesszük a mostanit (ügyintézés alattit) (Nem az igazi, de azt nem tudjuk, hogy mi volt az ez előtti állapot...)
                    ugyiratObj.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
                    ugyiratObj.Updated.TovabbitasAlattAllapot = true;

                    ugyiratObj.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
                    ugyiratObj.Updated.Allapot = true;

                    // Őrzőt visszatesszük arra, aki a kézbesítési tételben volt:
                    ugyiratObj.FelhasznaloCsoport_Id_Orzo = kezbTetelUtolso.Felhasznalo_Id_Atado_USER;
                    ugyiratObj.Updated.FelhasznaloCsoport_Id_Orzo = true;

                    ugyiratService.Update(execParamUgyiratGet, ugyiratObj);

                    #endregion
                }
            }

            /*

            DataRow iratRow;
           
            //Irat, ügyirat azonosítók meghatározása
            if (string.IsNullOrEmpty(Konyv))
                iratRow = this.GetIratRow(Evszam, Foszam, Alszam);
            else
                iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam);

            string iratId = iratRow["Id"].ToString();
            string ugyiratId = iratRow["Ugyirat_Id"].ToString();

            var iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
            var execParam = this.CreateExecParamByNMHHUserId(UserId);

            var iratpldService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            var searchObjIratpld = new EREC_PldIratPeldanyokSearch();

            searchObjIratpld.IraIrat_Id.Value = iratId;
            searchObjIratpld.IraIrat_Id.Operator = Query.Operators.equals;
            searchObjIratpld.OrderBy = "Sorszam DESC";
            Result result_iratpld = iratpldService.GetAll(execParam, searchObjIratpld);

            if (result_iratpld.IsError)
            {
                throw new ResultException(result_iratpld);
            }

            //átadás alatt lévő iratpéldányok visszavonása
            foreach (DataRow row in result_iratpld.Ds.Tables[0].Rows)
            {
                eRecord.BaseUtility.IratPeldanyok.Statusz iratpldStatusz = eRecord.BaseUtility.IratPeldanyok.GetAllapotByDataRow(row);

                if (eRecord.BaseUtility.IratPeldanyok.AtadasraKijelolesVisszavonhato(iratpldStatusz, execParam))
                {
                    //visszavonás
                    Result result_iratpldAtadasraKijelolesStorno = iratpldService.AtadasraKijelolesSztorno(execParam, row["id"].ToString());
                    if (result_iratpldAtadasraKijelolesStorno.IsError)
                    {
                        throw new ResultException(result_iratpldAtadasraKijelolesStorno);
                    }
                }
            }

            //ügyirat átadás alatt van -e?
            eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz = eRecord.BaseUtility.Ugyiratok.GetAllapotById(ugyiratId, execParam, new eUIControls.eErrorPanel());
            if (eRecord.BaseUtility.Ugyiratok.AtadasraKijelolesVisszavonhato(ugyiratStatusz, execParam))
            {
                //visszavonás
                var ugyiratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                Result result_ugyiratAtadasraKijelolesSztorno = ugyiratokService.AtadasraKijelolesSztorno(execParam, ugyiratId);
                if (result_ugyiratAtadasraKijelolesSztorno.IsError)
                {
                    throw new ResultException(result_ugyiratAtadasraKijelolesSztorno);
                }
            }
            */
        }

        public void SZUReGetAtvIratWS(string UserId, string Rendszerkod, out eIrat[] Iratok, out string Hibakod, out string Hibaszoveg)
        {
            Hibakod = "0";
            Hibaszoveg = String.Empty;

            Logger.InfoStart("SZURManager.SZUReGetAtvIratWS_RS");

            List<eIrat> resultIratList = new List<eIrat>();

            var execParam = this.CreateExecParamByNMHHUserId(UserId);
            var ugyiratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            var searchObjUgyUgyiratok = new EREC_UgyUgyiratokSearch();

            searchObjUgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez.Value = execParam.Felhasznalo_Id;
            searchObjUgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;

            searchObjUgyUgyiratok.Allapot.Value = KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott;
            searchObjUgyUgyiratok.Allapot.Operator = Query.Operators.equals;

            #region restricted
            Boolean restricted = false;

            if (!string.IsNullOrEmpty(Rendszerkod))
            {
                //restricted vizsgálat
                String RendszerkodKodtarId = String.Empty;
                String RendszerkodKodcsoportId = String.Empty;
                String RestrictedTrueKodtarId = String.Empty;
                String RestrictedKodcsoportId = String.Empty;

                KRT_KodTarakService kodtarakService = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                var resultKodtarak = kodtarakService.GetAllByKodcsoportKod(execParam, KodcsoportKod_Rendszerkod);

                if (String.IsNullOrEmpty(resultKodtarak.ErrorCode))
                {
                    foreach (DataRow row in resultKodtarak.Ds.Tables[0].Rows)
                    {
                        if(row["Kod"].ToString() == Rendszerkod)
                        {
                            RendszerkodKodtarId = row["Id"].ToString();
                            RendszerkodKodcsoportId = row["Kodcsoportok_Id"].ToString();
                        }
                    }
                }

                var resultKodtarakRest = kodtarakService.GetAllByKodcsoportKod(execParam, KodcsoportKod_Restricted);
                if (String.IsNullOrEmpty(resultKodtarakRest.ErrorCode))
                {
                    foreach (DataRow row in resultKodtarakRest.Ds.Tables[0].Rows)
                    {
                        if (row["Kod"].ToString() == KodtarKod_Restricted_Igen)
                        {
                            RestrictedTrueKodtarId = row["Id"].ToString();
                            RendszerkodKodcsoportId = row["Kodcsoportok_Id"].ToString();
                        }
                    }
                }

                if (!String.IsNullOrEmpty(RendszerkodKodtarId) && !String.IsNullOrEmpty(RendszerkodKodcsoportId) && !String.IsNullOrEmpty(RestrictedTrueKodtarId) && !String.IsNullOrEmpty(RendszerkodKodcsoportId))
                {
                    KRT_KodtarFuggosegService kodtarFuggosegService = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
                    
                    KRT_KodtarFuggosegSearch kodtarFuggosegSearch = new KRT_KodtarFuggosegSearch();
                    kodtarFuggosegSearch.Vezerlo_KodCsoport_Id.Value = RendszerkodKodcsoportId;
                    kodtarFuggosegSearch.Vezerlo_KodCsoport_Id.Operator = Query.Operators.equals;

                    kodtarFuggosegSearch.Fuggo_KodCsoport_Id.Value = RestrictedKodcsoportId;
                    kodtarFuggosegSearch.Fuggo_KodCsoport_Id.Operator = Query.Operators.equals;

                    kodtarFuggosegSearch.Aktiv.Value = "1";
                    kodtarFuggosegSearch.Aktiv.Operator = Query.Operators.equals;

                    kodtarFuggosegSearch.Adat.Value = "\"VezerloKodTarId\":\"" + RendszerkodKodtarId + "\",\"FuggoKodtarId\":\""+ RestrictedTrueKodtarId + "\",\"Aktiv\":true";
                    kodtarFuggosegSearch.Adat.Operator = Query.Operators.like;

                    var resultKodtarFuggoseg = kodtarFuggosegService.GetAll(execParam, kodtarFuggosegSearch);
                    if (resultKodtarFuggoseg.Ds != null && resultKodtarFuggoseg.Ds.Tables[0].Rows.Count > 0)
                    {
                        restricted = true;
                    }
                }

            }
            #endregion

            // Ügyiratok lekérése:
            var resultUgyiratok = ugyiratokService.GetAllWithExtension(execParam, searchObjUgyUgyiratok);
            resultUgyiratok.CheckError();

            // Eredménylista feltöltése:
            foreach (DataRow row in resultUgyiratok.Ds.Tables[0].Rows)
            {
                eIrat irat = CreateeIratFromUgyiratGetAllWithExt(row);
                Boolean tartalmazza = false;
                if (restricted)
                {
                    var resultIratTargyszavak = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService()
                            .GetAllMetaByDefinicioTipus(this.ExecParamCaller, new EREC_ObjektumTargyszavaiSearch()
                                    , row["Id"].ToString(), null, "EREC_UgyUgyiratok", null, false, false);
                    foreach (DataRow rowObjTargyszo in resultIratTargyszavak.Ds.Tables[0].Rows)
                    {
                        if (rowObjTargyszo["BelsoAzonosito"].ToString() == TargyszoAzon_Rendszerkod)
                        {
                            string targyszoErtek = rowObjTargyszo["Ertek"].ToString();
                            if (!string.IsNullOrEmpty(targyszoErtek) && targyszoErtek == Rendszerkod)
                                tartalmazza = true;
                        }
                    }
                }
                if(!restricted || tartalmazza)
                    resultIratList.Add(irat);
            }

            Logger.InfoStart("SZURManager.SZUReGetAtvIratWS_RS");
            
            Iratok = resultIratList.ToArray();
        }

        /// <summary>
        /// Irat státuszának lekérdezése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="User"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <param name="Statusz"></param>
        public void SZURGetIratStatuszWS(string Evszam, string Konyv, string Foszam, string Alszam, string User, out string Hibakod, out string Hibaszoveg, out string Statusz)
        {
            Logger.InfoStart("SZURManager.SZURGetIratStatuszWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            Statusz = String.Empty;
            #endregion

            DataRow iratRow;
            try
            {
                if (string.IsNullOrEmpty(Konyv))
                    iratRow = this.GetIratRow(Evszam, Foszam, Alszam);
                else
                    iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam);

                string iratId = iratRow["Id"].ToString();

                eRecord.BaseUtility.Iratok.Statusz iratStatusz = eRecord.BaseUtility.Iratok.GetAllapotByDataRow(iratRow);
                if (eRecord.BaseUtility.Iratok.Sztornozott(iratStatusz))
                {
                    Statusz = "Téves";
                }

            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);
                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }
            Logger.InfoEnd("SZURManager.SZURGetIratStatuszWS");
        }

        /// <summary>
        /// Irat státuszának beállítása
        /// (Iratpéldányok sztornózása)
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Statusz"></param>
        /// <param name="UserId"></param>
        /// <param name="RegiStatusz"></param>
        /// <param name="UjStatusz"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURSetIratStatuszWS(string Evszam, string Konyv, string Foszam, string Alszam, string Statusz, string UserId, out string RegiStatusz, out string UjStatusz, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURManager.SZURSetIratStatuszWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            RegiStatusz = String.Empty;
            UjStatusz = String.Empty;
            #endregion

            DataRow iratRow;
            try
            {
                if (string.IsNullOrEmpty(Konyv))
                    iratRow = this.GetIratRow(Evszam, Foszam, Alszam);
                else
                    iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam);

                string iratId = iratRow["Id"].ToString();
                string ugyiratId = iratRow["Ugyirat_Id"].ToString();

                var iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
                var execParam = this.CreateExecParamByNMHHUserId(UserId);

                SZURGetIratStatuszWS(Evszam, Konyv, Foszam, Alszam, UserId, out Hibakod, out Hibaszoveg, out RegiStatusz);
                if (Hibakod == Utility.ErrorCodes.SuccessCode)
                {
                    if (Statusz.ToUpper() == STATUSZ_TEVES.ToUpper())
                    {
                        var iratpldService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                        var searchObjIratpld = new EREC_PldIratPeldanyokSearch();

                        searchObjIratpld.IraIrat_Id.Value = iratId;
                        searchObjIratpld.IraIrat_Id.Operator = Query.Operators.equals;
                        searchObjIratpld.OrderBy = "Sorszam DESC";
                        Result result_iratpld = iratpldService.GetAll(execParam, searchObjIratpld);

                        if (result_iratpld.IsError)
                        {
                            throw new ResultException(result_iratpld);
                        }
                        // Az összes példány sztornózható?
                        foreach (DataRow row in result_iratpld.Ds.Tables[0].Rows)
                        {
                            eRecord.BaseUtility.IratPeldanyok.Statusz iratpldStatusz = eRecord.BaseUtility.IratPeldanyok.GetAllapotByDataRow(row);
                            eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz = eRecord.BaseUtility.Ugyiratok.GetAllapotById(ugyiratId,execParam, new eUIControls.eErrorPanel());
                            ErrorDetails errorDetail;
                            if (!eRecord.BaseUtility.IratPeldanyok.Sztornozhato(iratpldStatusz, ugyiratStatusz, execParam, out errorDetail))
                            {
                                // Nem sztornózható az irat:
                                throw new ResultException(52192, errorDetail);
                            }
                        }
                        // Iratpéldányok sztornózása
                        foreach (DataRow row in result_iratpld.Ds.Tables[0].Rows)
                        {
                            Result result_iratpldStorno = iratpldService.Sztornozas(execParam, row["Id"].ToString(), String.Empty,false,String.Empty);
                            if (result_iratpldStorno.IsError)
                            {
                                throw new ResultException(result_iratpldStorno);
                            }
                        }
                        SZURGetIratStatuszWS(Evszam, Konyv, Foszam, Alszam, UserId, out Hibakod, out Hibaszoveg, out UjStatusz);
                       
                    }
                    else
                    {
                        if (RegiStatusz.ToUpper() == STATUSZ_TEVES.ToUpper())
                        {
                            Hibakod = Utility.ErrorCodes.GeneralErrorCode;
                            Hibaszoveg = "A művelet nem hajtható végre (Sztornózást nem lehet visszavonni)! ";
                        }
                                             
                    }
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);
                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }
            Logger.InfoEnd("SZURManager.SZURSetIratStatuszWS");
        }

        public void SZURIratKeszDeleteWS(string Evszam, string IktatokonyvKod, string Foszam, string Alszam, string UserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURManager.SZURIratKeszDeleteWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            #endregion

            DataRow iratRow;
            try
            {
                if (string.IsNullOrEmpty(IktatokonyvKod))
                    iratRow = this.GetIratRow(Evszam, null, Foszam, Alszam);
                else
                    iratRow = this.GetIratRow(Evszam, IktatokonyvKod, Foszam, Alszam);

                string iratId = iratRow["Id"].ToString();
                // BUG_2742
                var execParam = this.CreateExecParamByNMHHUserId(UserId);
                //var resultElintezesVissza = eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService().ElintezesVisszavonasa(ExecParamCaller, iratId);
                var resultElintezesVissza = eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService().ElintezesVisszavonasa(execParam, iratId);

                if (resultElintezesVissza.IsError)
                {
                    throw new ResultException(resultElintezesVissza);
                }
            }


            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);
                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }
            Logger.InfoEnd("SZURManager.SZURIratKeszDeleteWS");
        }
    }
}