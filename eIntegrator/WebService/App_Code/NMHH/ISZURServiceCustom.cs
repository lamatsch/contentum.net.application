﻿using System;

namespace Contentum.eIntegrator.WebService.NMHH
{
    public partial interface ISZURService
    {
        [LogExtension]
        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace = "urn:DefaultNamespace", ResponseNamespace = "urn:DefaultNamespace")]
        [return: System.Xml.Serialization.SoapElementAttribute("ISVALID")]
        string SZURGETIKTATOSZAMWS(string IKTATOSZAM, out string HIBAKOD, out string HIBASZOVEG);

        [LogExtension]
        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace = "urn:DefaultNamespace", ResponseNamespace = "urn:DefaultNamespace")]
        [return: System.Xml.Serialization.SoapElementAttribute("SZERVEZETEK")]
        SZERVEZETEK SZURGETSZERVEZETEKWS(out string HIBAKOD, out string HIBASZOVEG);

        [LogExtension]
        [System.Web.Services.WebMethodAttribute()]
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("http://caesar", RequestNamespace = "uri:Domino", ResponseNamespace = "uri:Domino", Use = System.Web.Services.Description.SoapBindingUse.Literal)]
        void SZURChkKuldemenyTertiAllapotWS(ref string Konyv, ref string Evszam, ref string Foszam, ref string Alszam, string AtNemVett, string Szukites
                ,
                 [System.Xml.Serialization.SoapElementAttribute("kezbesitettsegi_informaciok")]
                 [System.Xml.Serialization.XmlArrayAttribute("kezbesitettsegi_informaciok")]
                 [System.Xml.Serialization.XmlArrayItemAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
                 out KuldemenyAllapot[] kuldemeny_allapot
                , out string Hibakod, out string Hibaszoveg);
    }

    //[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.18020")]
    //[System.SerializableAttribute()]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.SoapTypeAttribute(Namespace = "urn:DefaultNamespace")]
    //public partial class KezbesitesiInfok
    //{
    //    [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
    //    [System.Xml.Serialization.XmlArrayItemAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
    //    public KuldemenyAllapot[] kuldemeny_allapot { get; set; }
    //}

    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.18020")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.SoapTypeAttribute(TypeName = "kuldemeny_allapot", Namespace = "urn:DefaultNamespace")]
    [System.Xml.Serialization.XmlType(TypeName = "kuldemeny_allapot")]
    public partial class KuldemenyAllapot
    {
        [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
        public Guid? kuldemeny_id { get; set; }

        [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
        public string ragszam_kr_befogadasi_szam { get; set; }

        [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
        public string kezbesitesi_csatorna { get; set; }

        [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
        public string feladas_idopontja { get; set; }

        [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
        public string atveteli_idopont { get; set; }

        [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
        public string tertiveveny_visszaerkezes_datuma { get; set; }

        [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
        public string kezbesitettsegi_allapot { get; set; }

        [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
        public string barcode { get; set; }

        [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
        public Cimzett Cimzett { get; set; }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.18020")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.SoapTypeAttribute(Namespace = "urn:DefaultNamespace")]
    public partial class SZERVEZETEK
    {

        private SZERVEZET[] sZERVEZETField;

        /// <remarks/>
        [System.Xml.Serialization.SoapElementAttribute(IsNullable = true)]
        public SZERVEZET[] SZERVEZET
        {
            get
            {
                return this.sZERVEZETField;
            }
            set
            {
                this.sZERVEZETField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.18020")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.SoapTypeAttribute(Namespace = "urn:DefaultNamespace")]
    public partial class SZERVEZET
    {

        private string idField;

        private string nEVField;

        /// <remarks/>
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string NEV
        {
            get
            {
                return this.nEVField;
            }
            set
            {
                this.nEVField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.18020")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.SoapTypeAttribute(TypeName = "eIratok", Namespace = "urn:DefaultNamespace")]
    [System.Xml.Serialization.XmlType(TypeName = "eIratok")]
    public partial class eIratok
    {
        private eIrat[] eIratField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("eIrat", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public eIrat[] eIrat
        {
            get
            {
                return this.eIratField;
            }
            set
            {
                this.eIratField = value;
            }
        }
    }
}