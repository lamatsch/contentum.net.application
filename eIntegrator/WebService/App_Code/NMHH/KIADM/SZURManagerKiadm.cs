﻿using System;
using Contentum.eUtility;
using System.Data;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Linq;
using Contentum.eRecord.Service;
using System.Web;
using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;

/// <summary>
/// Summary description for SZURManagerIrat
/// </summary>
namespace Contentum.eIntegrator.WebService.NMHH
{
    /// <summary>
    /// SZURMAnager - VezetoiUtasitas
    /// </summary>
    public partial class SZURManager
    {
        /// <summary>
        ///  irat készre jelentése elektronikus változat, és elavúlt hívás egyben
        /// </summary>
        /// <param name="isEVersion"></param>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UgyintezoiNaplo"></param>
        /// <param name="ExpedialasMod"></param>
        /// <param name="KuldemenyTipus"></param>
        /// <param name="Rendeltetes"></param>
        /// <param name="T1_1"></param>
        /// <param name="T2_1"></param>
        /// <param name="T3_1"></param>
        /// <param name="Intezkedes"></param>
        /// <param name="CsatoltFajl"></param>
        /// <param name="SakkoraStatus"></param>
        /// <param name="SakkoraStatusOk"></param>
        /// <param name="Cimzettek"></param>
        /// <param name="UserId"></param>
        /// <param name="KiadmanyozoUserId"></param>
        /// <param name="Jogerositendo"></param>
        /// <param name="UzemzavarCsokkentes"></param>
        /// <param name="UzemzavarIratszam"></param>
        /// <param name="VisszafizOsszeg"></param>
        /// <param name="VisszafizDij"></param>
        /// <param name="VisszafizHatarozat"></param>
        /// <param name="VisszafizCimzett"></param>
        /// <param name="VisszafizHatarido"></param>
        /// <param name="UKSpecial"></param>
        /// <param name="CimzettMarad"></param>
        /// <param name="ElhiszParameters"></param>
        /// <param name="TisztviseloiAlairasParameters"></param>
        /// <param name="MunkaNapszam"></param>
        /// <param name="HosszabitasNapszam"></param>
        /// <param name="TullepesNapszam"></param>
        /// <param name="TullepesMertek"></param>
        /// <returns></returns>
        public void SZURKiadmFelWS(bool isEVersion, string Evszam, string Konyv, string Foszam, string Alszam, string UgyintezoiNaplo, string ExpedialasMod, string KuldemenyTipus, string Rendeltetes, string T1_1, string T2_1, string T3_1, string Intezkedes, string CsatoltFajl, string SakkoraStatus, string SakkoraStatusOk, Cimzett[] Cimzettek, string UserId, string KiadmanyozoUserId, string Jogerositendo, string UzemzavarCsokkentes, string UzemzavarIratszam, string VisszafizOsszeg, string VisszafizDij, string VisszafizHatarozat, string VisszafizCimzett, string VisszafizHatarido, string UKSpecial, string CimzettMarad, string ElhiszParameters, TisztviseloiAlairasParameters TisztviseloiAlairasParameters
            , out string ElteltNapszam, out string MunkaNapszam, out string HosszabitasNapszam, out string TullepesNapszam, out string TullepesMertek)
        {
            Logger.InfoStart("SZURManager.SZURKiadmFelWS");

            #region Out
            ElteltNapszam = null;
            MunkaNapszam = null; //fennmarado
            HosszabitasNapszam = null;
            TullepesNapszam = null;
            TullepesMertek = null;
            #endregion

            #region Ellenőrzés
            // Kiszedve az egységes kötelezőmező ellenörzés miatt
            //string checkResult = checkKiadmFelParams(Evszam, Konyv, Foszam, Alszam, UgyintezoiNaplo, KuldemenyTipus, Rendeltetes, T1_1, SakkoraStatus, SakkoraStatusOk, Cimzettek, UserId, KiadmanyozoUserId, Jogerositendo, UzemzavarCsokkentes, UzemzavarIratszam, VisszafizOsszeg, VisszafizDij, VisszafizHatarozat, VisszafizCimzett, VisszafizHatarido);
            //if(!string.IsNullOrEmpty(checkResult)) throw new ResultException(checkResult);

            Guid? partnerIdVisszafizCimzett = null;
            if (!string.IsNullOrEmpty(VisszafizCimzett))
            {
                partnerIdVisszafizCimzett = FindPartnerIdByNev(VisszafizCimzett);
                if (partnerIdVisszafizCimzett == null)
                {
                    Logger.Debug(string.Format("Nincs ilyen nevű partner az adatbázisban: '{0}' (VisszafizCimzett)", VisszafizCimzett));
                    //throw new ResultException(String.Format("Nincs ilyen nevű partner az adatbázisban: '{0}' (VisszafizCimzett)", VisszafizCimzett));
                }
            }

            // Számformátum-ellenőrzések:
            Utility.CheckIntFormat("VisszafizOsszeg", VisszafizOsszeg);

            // Jogerősítendő:
            bool? jogerositendoBool;
            Utility.CheckIgenNemString("Jogerositendo", Jogerositendo, out jogerositendoBool);

            #endregion

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            //irat lekérdezése
            DataRow iratRow;
            // BUG_2742
            //try
            //{
            iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam);

            string iratId = iratRow["Id"].ToString();
            string ugyiratId = iratRow["Ugyirat_Id"].ToString();
            Guid iratIdGuid = (Guid)iratRow["Id"];
            var iratObj = this.GetIratObjById(iratIdGuid);
            if (iratObj == null)
            {
                throw new ResultException("Irat nem található!");
            }

            #region Irat elintézhetőség előellenőrzés (ügyirat őrző ellenőrzése)

            // BUG#5708:
            // Ha már elintézett az irat, akkor hibát dobunk:
            if (iratObj.Elintezett == "1")
            {
                throw new ResultException("Az irat már elintézett!");
            }

            /// Ide tesszük az elintézhetőség ellenőrzését, 
            /// különben csak azután jön a hibaüzenet hogy néhány művelet már végrehajtódik,
            /// és mivel nincsenek egy tranzakcióba összevonva a műveletek, azok már nem görgetődnek vissza.
            /// 
            var ugyiratObj = this.GetUgyiratObjById(new Guid(ugyiratId));
            if (ugyiratObj == null)
            {
                throw new ResultException("Ügyirat nem található!");
            }

            string felhasznaloSajatCsoportId = Csoportok.GetFelhasznaloSajatCsoportId(execParamUserId.Clone());
            if (ugyiratObj.FelhasznaloCsoport_Id_Orzo != felhasznaloSajatCsoportId)
            {
                #region BLG_7317
                if (string.IsNullOrEmpty(iratObj.FelhasznaloCsoport_Id_Ugyintez))
                    throw new ResultException(80012); // Az ügyiratnak nem Ön az őrzője:
                else if (iratObj.FelhasznaloCsoport_Id_Ugyintez != felhasznaloSajatCsoportId)
                    throw new ResultException(800120);
                #endregion
            }

            #endregion

            #region Ellenőrzés, van-e csatolmány

            // BUG#5708: Ha nincs csatolmány feltöltve az irathoz, akkor dobjunk hibaüzenetet:
            EREC_CsatolmanyokSearch searchCsatolmanyok = new EREC_CsatolmanyokSearch();
            searchCsatolmanyok.IraIrat_Id.Value = iratId;
            searchCsatolmanyok.IraIrat_Id.Operator = Query.Operators.equals;

            EREC_CsatolmanyokService serviceCsat = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            var resultCsatolmanyok = serviceCsat.GetAllWithExtension(this.ExecParamCaller, searchCsatolmanyok);
            resultCsatolmanyok.CheckError();

            // Ha nincs találat, akkor hiba:
            if (resultCsatolmanyok.Ds.Tables[0].Rows.Count == 0)
            {
                throw new ResultException("Az irathoz nincs feltöltve csatolmány!");
            }

            #endregion

            // A Stat adatok meglétét ellenőrizzük az irat elintézésnél, így ezt előbbre kellett hozni...
            #region stat adatok
            KRT_KodTarakService svc_kodtarT1 = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            KRT_KodTarakSearch src_kodtarakT1 = new KRT_KodTarakSearch();
            src_kodtarakT1.Nev.Value = T1_1;
            src_kodtarakT1.Nev.Operator = Query.Operators.equals;
            src_kodtarakT1.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'IRATSTATISZTIKA_T1' ");

            Result resKodtarT1 = svc_kodtarT1.GetAllWithKodcsoport(ExecParamCaller, src_kodtarakT1);
            resKodtarT1.CheckError();

            if (resKodtarT1.Ds.Tables[0].Rows.Count != 1)
            {
                // BUG_2742
                //throw new ResultException(Utility.ErrorCodes.GeneralErrorCode, "Nem sikerült az FT1 mező azonosítása! Talált sorok:" + resKodtarT1.Ds.Tables[0].Rows.Count.ToString());
                throw new ResultException(Utility.ErrorCodes.GeneralErrorCode, "Nem sikerült az IT1 mező azonosítása! Talált sorok:" + resKodtarT1.Ds.Tables[0].Rows.Count.ToString());

            }
            string kodT1 = resKodtarT1.Ds.Tables[0].Rows[0]["Kod"].ToString();
            // BUG_2742
            //this.SetObjektumTargyszo(new Guid(iratId), eUtility.Constants.TableNames.EREC_IraIratok, TargyszoAzon_FT1, kodT1, ExecParamCaller);
            this.SetObjektumTargyszo(new Guid(iratId), eUtility.Constants.TableNames.EREC_IraIratok, TargyszoAzon_IT1, kodT1, ExecParamCaller);

            if (!string.IsNullOrEmpty(T2_1))
            {
                KRT_KodTarakService svc_kodtar = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                KRT_KodTarakSearch src_kodtarak = new KRT_KodTarakSearch();
                src_kodtarak.Nev.Value = T2_1;
                src_kodtarak.Nev.Operator = Query.Operators.equals;
                src_kodtarak.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'IRATSTATISZTIKA_T2' ");

                Result resKodtar = svc_kodtar.GetAllWithKodcsoport(ExecParamCaller, src_kodtarak);
                resKodtar.CheckError();

                //T2_1 nem kötelező így itt már nem adunk vissza errort
                if (resKodtar.Ds.Tables[0].Rows.Count == 1)
                {
                    string kod = resKodtar.Ds.Tables[0].Rows[0]["Kod"].ToString();
                    // BUG_2742
                    //this.SetObjektumTargyszo(new Guid(iratId), eUtility.Constants.TableNames.EREC_IraIratok, TargyszoAzon_FT2, kod, ExecParamCaller);
                    this.SetObjektumTargyszo(new Guid(iratId), eUtility.Constants.TableNames.EREC_IraIratok, TargyszoAzon_IT2, kod, ExecParamCaller);
                }
            }
            if (!string.IsNullOrEmpty(T3_1))
            {
                KRT_KodTarakService svc_kodtar = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                KRT_KodTarakSearch src_kodtarak = new KRT_KodTarakSearch();
                src_kodtarak.Nev.Value = T3_1;
                src_kodtarak.Nev.Operator = Query.Operators.equals;
                src_kodtarak.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'IRATSTATISZTIKA_T3' ");

                Result resKodtar = svc_kodtar.GetAllWithKodcsoport(ExecParamCaller, src_kodtarak);
                resKodtar.CheckError();

                //T3_1 nem kötelező így itt már nem adunk vissza errort
                if (resKodtar.Ds.Tables[0].Rows.Count == 1)
                {
                    string kod = resKodtar.Ds.Tables[0].Rows[0]["Kod"].ToString();
                    // BUG_2742
                    //this.SetObjektumTargyszo(new Guid(iratId), eUtility.Constants.TableNames.EREC_IraIratok, TargyszoAzon_FT3, kod, ExecParamCaller);
                    this.SetObjektumTargyszo(new Guid(iratId), eUtility.Constants.TableNames.EREC_IraIratok, TargyszoAzon_IT3, kod, ExecParamCaller);
                }
            }
            #endregion

            #region Irat elintézhetőség ellenőrzés

            var resultCheckElintezheto = eRecordService.ServiceFactory.GetEREC_IraIratokService().CheckIratElintezheto(execParamUserId, iratId);
            resultCheckElintezheto.CheckError();

            #endregion

            

            iratObj.Updated.SetValueAll(false);
            iratObj.Base.Updated.SetValueAll(false);
            iratObj.Base.Updated.Ver = true;

            if (!string.IsNullOrEmpty(ExpedialasMod))
                iratObj.ExpedialasModja = ExpedialasMod;

            // BLG#7145: Bejövő iratnál nem jegyezzük be a kiadmányozót
            // bejövő iratnál nem jegyezzük be a jogerősítendő értéket (bejövő irathoz nincs ilyen meta adat)
            if (iratObj.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Bejovo)
            {
                // Jogerősítendő:
                if (jogerositendoBool != null)
                {
                    this.SetObjektumTargyszo(iratIdGuid, eUtility.Constants.TableNames.EREC_IraIratok, TargyszoAzon_Jogerositendo, jogerositendoBool.ToString(), this.ExecParamCaller);
                }


                DataRow kiadmanyozoDataRow = this.GetFelhasznaloRowByUserNev(KiadmanyozoUserId);
                Guid kiadmanyozoFelhId = (Guid)kiadmanyozoDataRow["Id"];

                InvalidateExistingIratKiadmanyozok(execParamUserId, iratId);

                EREC_IratAlairok newIratAlairokObj = new EREC_IratAlairok();

                newIratAlairokObj.Obj_Id = iratId.ToString();
                newIratAlairokObj.AlairoSzerep = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
                newIratAlairokObj.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
                newIratAlairokObj.FelhasznaloCsoport_Id_Alairo = kiadmanyozoFelhId.ToString();

                var resultInsert = eRecordService.ServiceFactory.GetEREC_IratAlairokService().Insert(this.ExecParamCaller, newIratAlairokObj);
                resultInsert.CheckError();

                iratObj.FelhasznaloCsoport_Id_Kiadmany = kiadmanyozoFelhId.ToString();
            }

            #region üzemzavar
            iratObj.UzemzavarIgazoloIratSzama = UzemzavarIratszam;
            iratObj.Updated.UzemzavarIgazoloIratSzama = true;
            if (!String.IsNullOrEmpty(UzemzavarCsokkentes))
            {
                /// Üzemzavar kezdete és vége mezőink vannak:
                /// A mai nap lesz az üzemzavar vége, és ebből vonjuk le a megadott napokat a kezdetéhez
                int napokInt;
                if (int.TryParse(UzemzavarCsokkentes, out napokInt))
                {
                    iratObj.UzemzavarVege = DateTime.Today.ToString();
                    iratObj.Updated.UzemzavarVege = true;

                    iratObj.UzemzavarKezdete = DateTime.Today.Subtract(TimeSpan.FromDays(napokInt)).ToString();
                    iratObj.Updated.UzemzavarKezdete = true;
                }
                else
                {
                    throw new ResultException(string.Format("Hibás paraméter érték: 'UzemzavarCsokkentes': '{0}'", UzemzavarCsokkentes));
                }
            }
            #endregion

            #region Visszafizetes

            if (!String.IsNullOrEmpty(VisszafizOsszeg))
            {
                int visszaFizOsszegInt;
                if (int.TryParse(VisszafizOsszeg, out visszaFizOsszegInt))
                {
                    iratObj.VisszafizetesOsszege = VisszafizOsszeg;
                    iratObj.Updated.VisszafizetesOsszege = true;
                }
                else
                {
                    throw new ResultException(string.Format("Hibás paraméter érték: 'VisszafizOsszeg': '{0}'", VisszafizOsszeg));
                }
            }

            if (!String.IsNullOrEmpty(VisszafizDij))
            {
                string visszafizDijKod;
                /// A SZURSetDijVisszafizWS-ben 'D' vagy 'I' érték jöhet, így jó eséllyel itt is ilyet fognak adni.
                /// Ha mégsem, akkor a 'VISSZAFIZETES_JOGCIME' kódcsoport elemeinek nevére keresünk:
                /// 
                switch (VisszafizDij)
                {
                    case "D":
                        visszafizDijKod = KodTarak.VISSZAFIZETES_JOGCIME.Dij;
                        break;
                    case "I":
                        visszafizDijKod = KodTarak.VISSZAFIZETES_JOGCIME.Illetek;
                        break;
                    default:
                        visszafizDijKod = KodTar_Cache.GetKodtarakByKodCsoport(KCS_VISSZAFIZETES_JOGCIME, this.ExecParamCaller, HttpContext.Current.Cache)
                                // Kódtár nevére keresünk:
                                .Where(kt => String.Compare(kt.Value, VisszafizDij, true) == 0)
                                .Select(e => e.Key)
                                .FirstOrDefault();
                        break;
                }

                if (!String.IsNullOrEmpty(visszafizDijKod))
                {
                    iratObj.VisszafizetesJogcime = visszafizDijKod;
                }
                else
                {
                    // Nem sikerült a kódtár megtalálása, beírjuk a megadott szöveget:
                    iratObj.VisszafizetesJogcime = VisszafizDij;
                }
                iratObj.Updated.VisszafizetesJogcime = true;
            }

            if (!string.IsNullOrEmpty(VisszafizHatarozat))
            {
                iratObj.VisszafizetesHatarozatSzama = VisszafizHatarozat;
                iratObj.Updated.VisszafizetesHatarozatSzama = true;
            }

            if (partnerIdVisszafizCimzett != null)
            {
                iratObj.Partner_Id_VisszafizetesCimzet = partnerIdVisszafizCimzett.ToString();
                iratObj.Updated.Partner_Id_VisszafizetesCimzet = true;
            }
            else if (!string.IsNullOrEmpty(VisszafizCimzett))
            {
                iratObj.Partner_Nev_VisszafizetCimzett = VisszafizCimzett;
            }
            iratObj.VisszafizetesHatarido = VisszafizHatarido;

            if (!String.IsNullOrEmpty(VisszafizHatarido))
            {
                iratObj.VisszafizetesHatarido = VisszafizHatarido;
                iratObj.Updated.VisszafizetesHatarido = true;
            }

            #endregion

            var execParamUpdate = execParamUserId.Clone();
            execParamUpdate.Record_Id = iratId.ToString();

            #region Cimzettek ellenőrzése elosztóíven

            FoundPartner[] alreadyRegisteredPartners = null;

            if (Cimzettek != null
                && Cimzettek.Length > 0)
            {
                string elosztoivId = string.Empty;
                string elosztoivName = string.Empty;

                try
                {
                    #region PARTNER/CIM TORZS
                    if (UKSpecial != "igen")
                        alreadyRegisteredPartners = CheckCimzettToTorzsAdat(Cimzettek, isEVersion);

                    if (isEVersion)
                        Cimzettek = KibovitOsszesTobbiKapcsolattartoval(Cimzettek);
                    #endregion

                    ExecParam execParamCim = ExecParamCaller.Clone();
                    
                    EREC_IraElosztoivekService serviceElosztoIv = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
                    EREC_IraElosztoivTetelekService serviceElosztoIvTetelek = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();
                    EREC_IraElosztoivekSearch searchElosztoIv = new EREC_IraElosztoivekSearch();

                    searchElosztoIv.NEV.Value = "CimzettLista_" + Konyv + "_" + Foszam + "_" + Alszam + "_" + Evszam + "_%";
                    searchElosztoIv.NEV.Operator = Query.Operators.like;

                    searchElosztoIv.Fajta.Value = KodtarKod_ElosztoIv_Fajta_Cimzettlista;
                    searchElosztoIv.Fajta.Operator = Query.Operators.equals;

                    searchElosztoIv.Kod.Value = "0";
                    searchElosztoIv.Kod.Operator = Query.Operators.equals;

                    var resultElosztoivSearch = serviceElosztoIv.GetAll(execParamCim, searchElosztoIv);

                    resultElosztoivSearch.CheckError();

                    if (resultElosztoivSearch.Ds.Tables[0].Rows.Count >= 1)
                    {
                        elosztoivId = resultElosztoivSearch.Ds.Tables[0].Rows[0]["Id"].ToString();
                        elosztoivName = resultElosztoivSearch.Ds.Tables[0].Rows[0]["NEV"].ToString();

                        /// Lekérjük az elosztóív-tételeket, és azokat a tételeket töröljük, 
                        /// amelyek nincsenek benne a megadott Cimzettek adatokban
                        /// 

                        #region Elosztóív-tételek lekérése, feleslegesek törlése

                        EREC_IraElosztoivTetelekSearch ivtetelekSearch = new EREC_IraElosztoivTetelekSearch();

                        ivtetelekSearch.ElosztoIv_Id.Value = elosztoivId;
                        ivtetelekSearch.ElosztoIv_Id.Operator = Query.Operators.equals;

                        var resultIvTetelek = serviceElosztoIvTetelek.GetAll(execParamCim, ivtetelekSearch);
                        resultIvTetelek.CheckError();

                        foreach (DataRow rowElosztoIvTetel in resultIvTetelek.Ds.Tables[0].Rows)
                        {
                            string nevStr = rowElosztoIvTetel["NevSTR"].ToString();
                            string cimStr = rowElosztoIvTetel["CimSTR"].ToString();

                            // Ellenőrzés, hogy van-e ilyen tétel a megadott Cimzettek listában:
                            if (!Cimzettek.Any(c => (!String.IsNullOrEmpty(c.Nev) ? c.Nev : c.Szervezet) == nevStr && c.GetTeljesCim() == cimStr))
                            {
                                // Ezt az elosztóív tételt töröljük:
                                ExecParam execParamInv = ExecParamCaller.Clone();
                                execParamInv.Record_Id = rowElosztoIvTetel["Id"].ToString();

                                var resultInvalidate = serviceElosztoIvTetelek.Invalidate(execParamInv);
                                resultInvalidate.CheckError();
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region EREC_IraElosztoivek Insert

                        EREC_IraElosztoivek elosztoiv = new EREC_IraElosztoivek();
                        elosztoiv.Updated.SetValueAll(false);
                        elosztoiv.Base.Updated.SetValueAll(false);

                        elosztoivName = "CimzettLista_" + Konyv + "_" + Foszam + "_" + Alszam + "_" + Evszam + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
                        elosztoiv.NEV = elosztoivName;
                        elosztoiv.Updated.NEV = true;

                        elosztoiv.Fajta = KodtarKod_ElosztoIv_Fajta_Cimzettlista;
                        elosztoiv.Updated.Fajta = true;

                        elosztoiv.Kod = "0";
                        elosztoiv.Updated.Kod = true;

                        elosztoiv.Csoport_Id_Tulaj = execParamCim.Felhasznalo_Id;
                        elosztoiv.Updated.Csoport_Id_Tulaj = true;

                        elosztoiv.Base.Note = iratId;
                        elosztoiv.Base.Updated.Note = true;

                        Result resultElosztoIv = serviceElosztoIv.Insert(execParamCim, elosztoiv);

                        if (resultElosztoIv.IsError)
                        {
                            Logger.Error("Cimzett elosztóív létrehozási hiba");
                            throw new ResultException(resultElosztoIv);
                        }
                        elosztoivId = resultElosztoIv.Uid;

                        #endregion
                    }

                    if (string.IsNullOrEmpty(elosztoivId))
                    {
                        throw new ResultException("Elosztói Ív meghatározása sikertelen!");
                    }

                    // Kódtárfüggőség lekérése:
                    KodtarFuggosegDataClass ktFuggosegNmhhExpMod = Utility.GetKodtarFuggoseg(SZURManager.KCS_NMHH_EXPMOD, SZURManager.KCS_KULDEMENY_KULDES_MODJA, this.ExecParamCaller);

                    int sorszam = 0;
                    foreach (Cimzett cimzett in Cimzettek)
                    {
                        string partnerId = string.Empty;
                        string cimId = string.Empty;
                        string partnerCimId = string.Empty;

                        EREC_IraElosztoivTetelekSearch ivtetelekSearch = new EREC_IraElosztoivTetelekSearch();

                        ivtetelekSearch.NevSTR.Value = !String.IsNullOrEmpty(cimzett.Nev) ? cimzett.Nev : cimzett.Szervezet;
                        ivtetelekSearch.NevSTR.Operator = Query.Operators.equals;

                        ivtetelekSearch.ElosztoIv_Id.Value = elosztoivId;
                        ivtetelekSearch.ElosztoIv_Id.Operator = Query.Operators.equals;

                        ivtetelekSearch.CimSTR.Value = cimzett.GetTeljesCim();
                        ivtetelekSearch.CimSTR.Operator = Query.Operators.equals;

                        var resultIvTetelek = serviceElosztoIvTetelek.GetAll(execParamCim, ivtetelekSearch);
                        resultIvTetelek.CheckError();

                        if (resultIvTetelek.Ds.Tables[0].Rows.Count == 0)
                        {
                            #region EREC_IraElosztoivTetelek Insert

                            EREC_IraElosztoivTetelek elosztoivTetelek = new EREC_IraElosztoivTetelek();

                            #region Expediálás módja (BUG#5901: NMHH_EXPMOD - KULDEMENY_KULDES_MODJA kódtárfüggőségből)

                            string kuldKuldesModKod;
                            Utility.CheckCimzettExpedialasMod(cimzett, ktFuggosegNmhhExpMod, this.ExecParamCaller, out kuldKuldesModKod);

                            elosztoivTetelek.Kuldesmod = kuldKuldesModKod;
                            elosztoivTetelek.Updated.Kuldesmod = true;

                            #endregion

                            FoundPartner foundPartner = new FoundPartner();

                            if (string.IsNullOrEmpty(UKSpecial) || UKSpecial.ToLower() != "igen")
                            {
                                // BUG#5708
                                if (foundPartner == null)
                                {
                                    foundPartner = FindPartner(cimzett);
                                }
                            }
                            
                            elosztoivTetelek.Updated.SetValueAll(false);
                            elosztoivTetelek.Base.Updated.SetValueAll(false);

                            elosztoivTetelek.ElosztoIv_Id = elosztoivId;
                            elosztoivTetelek.Updated.ElosztoIv_Id = true;

                            if (!string.IsNullOrEmpty(foundPartner.Partner_Id))
                            {
                                elosztoivTetelek.Partner_Id = foundPartner.Partner_Id;
                                elosztoivTetelek.Updated.Partner_Id = true;
                            }

                            if (string.IsNullOrEmpty(cimzett.Nev))
                            {
                                elosztoivTetelek.NevSTR = cimzett.Szervezet;
                            }
                            else
                            {
                                elosztoivTetelek.NevSTR = cimzett.Nev;
                            }
                            elosztoivTetelek.Updated.NevSTR = true;

                            if (!string.IsNullOrEmpty(foundPartner.Cim_Id))
                            {
                                elosztoivTetelek.Cim_Id = foundPartner.Cim_Id;
                                elosztoivTetelek.Updated.Cim_Id = true;
                            }

                            elosztoivTetelek.CimSTR = cimzett.GetTeljesCim();
                            elosztoivTetelek.Updated.CimSTR = true;

                            if (IsCimzettElektronikus(cimzett))
                            {
                                //Kapcsolattarto
                                if (!string.IsNullOrEmpty(cimzett.UgyfelAzonosito) && !string.IsNullOrEmpty(cimzett.KapcsolattartoAzonosito))
                                    elosztoivTetelek.CimSTR = string.Format("{0}", cimzett.KapcsolattartoAzonosito);
                                else if (!string.IsNullOrEmpty(cimzett.UgyfelAzonosito))
                                    elosztoivTetelek.CimSTR = string.Format("{0}", cimzett.UgyfelAzonosito);
                                else if (!string.IsNullOrEmpty(cimzett.KapcsolattartoAzonosito))
                                    elosztoivTetelek.CimSTR = string.Format("{0}", cimzett.KapcsolattartoAzonosito);
                                else
                                {
                                    // BUG#7784: Nem dobunk hibát, csak ezt nem tesszük bele a címzettlistába

                                    // throw new Exception("Nincs kitöltve az UgyfelAzonosito és a KapcsolattartoAzonosito !");
                                    
                                    Logger.Error("Nincs kitöltve az UgyfelAzonosito és a KapcsolattartoAzonosito !");

                                    continue;
                                }
                            }

                            sorszam++;

                            elosztoivTetelek.Sorszam = sorszam.ToString();
                            elosztoivTetelek.Updated.Sorszam = true;

                            Result resultElosztoiIvTetelek = serviceElosztoIvTetelek.Insert(execParamCim, elosztoivTetelek);
                            if (resultElosztoiIvTetelek.IsError)
                            {
                                Logger.Error("Cimzett elosztóívtétel létrehozási hiba");
                                throw new ResultException(resultElosztoiIvTetelek);
                            }

                            #endregion
                        }
                    }
                    // BUG_2742
                    //ExecParam exec_paramPldUpdate = ExecParamCaller.Clone();

                    //EREC_PldIratPeldanyokService _PldIratPeldanyokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    //Result resultPld = _PldIratPeldanyokService.GetElsoIratPeldanyByIraIrat(exec_paramPldUpdate, iratId);
                    //resultPld.CheckError();

                    //EREC_PldIratPeldanyok erec_PldIratPeldanyokUpdate = (EREC_PldIratPeldanyok)resultPld.Record;

                    //erec_PldIratPeldanyokUpdate.NevSTR_Cimzett = elosztoivName;
                    //erec_PldIratPeldanyokUpdate.Updated.CimSTR_Cimzett = true;

                    //exec_paramPldUpdate.Record_Id = erec_PldIratPeldanyokUpdate.Id;

                    //Result result_pldUpdate = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService().Update(exec_paramPldUpdate, erec_PldIratPeldanyokUpdate);
                    //result_pldUpdate.CheckError();
                }
                catch (Exception exc)
                {
                    string innerErrorCode;
                    string innerErrorMsg;
                    Utility.SetHibakodHibaszoveg(exc, out innerErrorCode, out innerErrorMsg);
                    throw new ResultException("Címzett elosztóív létrehozási hiba: " + innerErrorMsg);
                }

                // BUG#7951: Az első iratpéldány címzettjébe beírjuk az elosztóívet, ha nincs beírva:
                try
                {
                    DataRow pldRow1 = this.GetIratPeldanyRow(Evszam, Konyv, Foszam, Alszam, "1");

                    // Ha az első iratpéldány  címzettjébe nem az elosztóív Id van beírva, akkor beírjuk
                    if (!String.IsNullOrEmpty(elosztoivId)
                        && pldRow1["Partner_Id_Cimzett"] as Guid? != new Guid(elosztoivId))
                    {

                        var pldObj = this.GetIratPeldanyObjById((Guid)pldRow1["Id"]);
                        
                        pldObj.Updated.SetValueAll(false);
                        pldObj.Base.Updated.SetValueAll(false);
                        pldObj.Base.Updated.Ver = true;

                        pldObj.Partner_Id_Cimzett = elosztoivId;
                        pldObj.Updated.Partner_Id_Cimzett = true;

                        pldObj.NevSTR_Cimzett = elosztoivName;
                        pldObj.Updated.NevSTR_Cimzett = true;

                        pldObj.CimSTR_Cimzett = "Címzettlista szerint";
                        pldObj.Updated.CimSTR_Cimzett = true;

                        pldObj.Cim_id_Cimzett = "<null>";
                        pldObj.Updated.Cim_id_Cimzett = true;

                        // Iratpéldány UPDATE:
                        var execParamPldUpdate = execParamUserId.Clone();
                        execParamPldUpdate.Record_Id = pldObj.Id;
                        var resultPldUpdate = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService().Update(execParamPldUpdate, pldObj);
                        resultPldUpdate.CheckError();
                    }
                }
                catch (Exception exc)
                {
                    // Nem dobjuk tovább a hibát, csak logolunk:
                    Logger.Error("Elosztóív beírása iratpéldány címzettjébe hiba: " + exc.ToString());
                }
            }

            #endregion

            #region eVerzio
            if (isEVersion)
            {
                #region ElhiszParameters
                if (!string.IsNullOrEmpty(ElhiszParameters))
                {
                    var execParameBeadv = execParamUserId.Clone();
                    var eBeadvService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
                    Result resulteBeadvService = eBeadvService.SetKuldesiAdatokFromXML(execParameBeadv, iratId, ElhiszParameters);
                }
                #endregion

                #region TisztviseloiAlairasParameters
                if (TisztviseloiAlairasParameters != null)
                {
                    //"true" szerepelt a specifikációban, de "igen" -t is elfogad
                    if (!string.IsNullOrEmpty(TisztviseloiAlairasParameters.Required) && (TisztviseloiAlairasParameters.Required.ToLower() == "true" || TisztviseloiAlairasParameters.Required.ToLower() == "igen"))
                    {
                        if (string.IsNullOrEmpty(TisztviseloiAlairasParameters.SignOutputFormat))
                        {
                            throw new ResultException("HALK alkalmazás hívásához kimeneti formátum megadása kötelező (TisztviseloiAlairasParameters.SignOutputFormat)!");
                        }
                        if (string.IsNullOrEmpty(TisztviseloiAlairasParameters.InputFiles))
                        {
                            throw new ResultException("HALK alkalmazás hívásához fájl név lista megadása kötelező (TisztviseloiAlairasParameters.InputFiles)!");
                        }
                        //TODO paraméterek letárolása HALK híváshoz - beszúrás - irat aláíró (felvett) - irataláírókat updatelni, hogy elektronikus aláírás legyen a típus; note -ba beleírom a paramétereket

                    }
                }
                #endregion

            }
            #endregion

            #region sakkora
            var execParamSakkora = execParamUserId.Clone();
            eRecord.Service.SakkoraService sakkoraService = eRecordService.ServiceFactory.Get_SakkoraService();
            Result resultSakkora = sakkoraService.SetSakkoraStatus(execParamSakkora, iratId, ugyiratId, SakkoraStatus, SakkoraStatusOk, UserId);
            resultSakkora.CheckError();
            GetUgyHataridokKiadm(ugyiratId, out ElteltNapszam, out MunkaNapszam, out TullepesMertek, out HosszabitasNapszam, out TullepesNapszam);
            #endregion

            #region elintézetté nyilvánítás

            ExecParam execParam_felj = execParamUserId.Clone();
            execParam_felj.Record_Id = iratId;

            EREC_HataridosFeladatok erec_HataridosFeladatok = new EREC_HataridosFeladatok();
            erec_HataridosFeladatok.Obj_Id = iratId;
            erec_HataridosFeladatok.Updated.Obj_Id = true;

            erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
            erec_HataridosFeladatok.Updated.Obj_type = true;

            erec_HataridosFeladatok.Azonosito = iratRow["IktatoSzam_Merge"].ToString(); //REC_UgyUgyiratok.Azonosito as Foszam_Merge
            erec_HataridosFeladatok.Updated.Azonosito = true;

            erec_HataridosFeladatok.Leiras = UgyintezoiNaplo;
            erec_HataridosFeladatok.Updated.Leiras = true;

            erec_HataridosFeladatok.Tipus = eUtility.KodTarak.FELADAT_TIPUS.ElintezesMegjegyzes;
            erec_HataridosFeladatok.Updated.Tipus = true;

            erec_HataridosFeladatok.Altipus = eUtility.KodTarak.FELADAT_ALTIPUS.Megjegyzes;
            erec_HataridosFeladatok.Updated.Tipus = true;

            // BUG_2742
            // iratObj újralekérés (változott a ver)

            var iratObjSeged = this.GetIratObjById(iratIdGuid);
            if (iratObjSeged == null)
            {
                throw new ResultException("Irat nem található!");
            }
            // Ver frissítés:
            iratObj.Base.Ver = iratObjSeged.Base.Ver;

            var resultElintezett = eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService().Elintezes(execParam_felj, iratObj, erec_HataridosFeladatok);

            resultElintezett.CheckError();
            #endregion

            //}
            //catch (Exception exc)
            //{
            //    // BUG_2742
            //    //throw new ResultException(exc.Message);

            //}

            Logger.InfoEnd("SZURManager.SZURKiadmFelWS");
        }

        /// <summary>
        /// InvalidateExistingIratKiadmanyozok
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="iratId"></param>
        private void InvalidateExistingIratKiadmanyozok(ExecParam execParam, string iratId)
        {
            var service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
            EREC_IratAlairokSearch src = new EREC_IratAlairokSearch();

            src.Allapot.Value = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
            src.Allapot.Operator = Query.Operators.equals;

            src.AlairoSzerep.Value = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
            src.AlairoSzerep.Operator = Query.Operators.equals;

            src.Obj_Id.Value = iratId;
            src.Obj_Id.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, src);
            result.CheckError();

            ExecParam xpmInvalidate = execParam.Clone();
            if (result.Ds.Tables[0].Rows.Count > 0)
            {
                Result resultInvalidate = null;
                for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                {
                    xpmInvalidate.Record_Id = result.Ds.Tables[0].Rows[i]["Id"].ToString();
                    resultInvalidate = service.Invalidate(xpmInvalidate);
                    resultInvalidate.CheckError();
                }
            }
        }

        public string checkKiadmFelParams(string Evszam, string Konyv, string Foszam, string Alszam, string UgyintezoiNaplo, string KuldemenyTipus, string Rendeltetes, string T1_1, string SakkoraStatus, string SakkoraStatusOk, Cimzett[] Cimzettek, string UserId, string KiadmanyozoUserId, string Jogerositendo, string UzemzavarCsokkentes, string UzemzavarIratszam, string VisszafizOsszeg, string VisszafizDij, string VisszafizHatarozat, string VisszafizCimzett, string VisszafizHatarido)
        {
            if (String.IsNullOrEmpty(Evszam)) return "Kötelező évszámot megadni!";

            if (String.IsNullOrEmpty(Konyv)) return "Kötelező iktatókönyv azonosítót megadni!";

            if (String.IsNullOrEmpty(Foszam)) return "Kötelező főszámot megadni!";

            if (String.IsNullOrEmpty(Alszam)) return "Kötelező alszámot megadni!";

            if (String.IsNullOrEmpty(UgyintezoiNaplo)) return "Kötelező ügyintézői naplóbejegyzést megadni!";

            if (String.IsNullOrEmpty(KuldemenyTipus)) return "Kötelező a küldemény típusát megadni!";

            if (String.IsNullOrEmpty(Rendeltetes)) return "Kötelező az irat rendeltetési helyét megadni!";

            if (String.IsNullOrEmpty(T1_1)) return "Kötelező az irat tulajdonság első tagját megadni!";

            if (String.IsNullOrEmpty(SakkoraStatus)) return "Kötelező a sakkóra státuszt megadni!";

            if (String.IsNullOrEmpty(SakkoraStatusOk)) return "Ok kód megadása kötelező!";

            if (Cimzettek == null) return "Címzettek listájának megadása kötelező!";

            if (String.IsNullOrEmpty(UserId)) return "Kötelező felhasználó azonosítót megadni!";

            if (String.IsNullOrEmpty(KiadmanyozoUserId)) return "Kötelező a kiadmányozó felhasználó azonosítóját megadni!";

            if (String.IsNullOrEmpty(UzemzavarCsokkentes)) return "Kötelező az igazolt üzemzavar időtartamát megadni!";

            if (String.IsNullOrEmpty(UzemzavarIratszam)) return "Kötelező az üzemzavart igazoló irat iktatószámát megadni!";

            if (String.IsNullOrEmpty(VisszafizOsszeg)) return "Kötelező a visszafizetendő összeget megadni!";

            if (String.IsNullOrEmpty(VisszafizDij)) return "Kötelező a visszafizetendő összeg jogcímét megadni!";

            if (String.IsNullOrEmpty(VisszafizHatarozat)) return "Kötelező a visszafizetést elrendelő irat iktatószámát megadni!";

            if (String.IsNullOrEmpty(VisszafizCimzett)) return "Kötelező a visszafizetendő összeg címzettjét megadni!";

            if (String.IsNullOrEmpty(VisszafizHatarido)) return "Kötelező a visszafizetés határidejét megadni!";

            /*if (String.IsNullOrEmpty(Jogerositendo)) return "Kötelező ... megadni!";*/

            return string.Empty;
        }

        /// <summary>
        /// GetUgyHataridok függvény átalakítása a KiadmFel webservice -k igényeihez
        /// </summary>
        /// <param name="ugyiratId"></param>
        /// <param name="hataridoMarNovelve"></param>
        /// <param name="Eltelt"></param>
        /// <param name="Fennmarado"></param>
        /// <param name="TullepMertekKod"></param>
        /// <param name="HosszabitasNapszam"></param>
        /// <param name="TullepesNapszam"></param>
        public void GetUgyHataridokKiadm(string ugyiratId, /*bool hataridoMarNovelve,*/ out string Eltelt, out string FennmaradoMunkanap, out string TullepMertekKod, out string HosszabitasNapszam, out string TullepesNapszam)
        {
            // BUG_2742
            //try
            //{

            //eRecord.Service.SakkoraService service = eRecordService.ServiceFactory.Get_SakkoraService();
            //ExecParam execParam = this.ExecParamCaller;
            //Result resultHatarido = service.GetSakkoraHataridok(execParam, ugyiratId, hataridoMarNovelve);
            //resultHatarido.CheckError();

            //DateTime letrehozasIdo;
            //DateTime.TryParse(resultHatarido.Ds.Tables[0].Rows[0]["LetrehozasIdo"].ToString(), out letrehozasIdo);

            //DateTime hatarido;
            //DateTime.TryParse(resultHatarido.Ds.Tables[0].Rows[0]["Hatarido"].ToString(), out hatarido);

            //Eltelt = resultHatarido.Ds.Tables[0].Rows[0]["Eltelt_Nap"].ToString();
            //Fennmarado = resultHatarido.Ds.Tables[0].Rows[0]["Fennmarado_Nap"].ToString();

            //int elteltNap;
            //int.TryParse(resultHatarido.Ds.Tables[0].Rows[0]["Eltelt_Nap"].ToString(), out elteltNap);

            //int elteltMunkaNap;
            //int.TryParse(resultHatarido.Ds.Tables[0].Rows[0]["Eltelt_MunkaNap"].ToString(), out elteltMunkaNap);

            //bool hataridoTullepve;
            //bool.TryParse(resultHatarido.Ds.Tables[0].Rows[0]["Hatarido_Tullepve"].ToString(), out hataridoTullepve);

            //TODO HosszabitasNapszam meghatározása - ügyirat historyból! első berzió határidejét
            HosszabitasNapszam = string.Empty;
            //EREC_UgyUgyiratokService ugyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            //EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
            //ugyiratokSearch.WhereByManual

            eRecord.Service.EREC_UgyUgyiratokService serviceUgyirat = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParamUgy = this.ExecParamCaller.Clone();
            execParamUgy.Record_Id = ugyiratId;
            Result resultUgy = serviceUgyirat.Get(execParamUgy);
            resultUgy.CheckError();

            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)resultUgy.Record;

            /// BUG#5907: sp_EREC_UgyUgyiratok_Sakkora_GetHataridok hívása helyett 
            /// egyszerűen kivesszük az ügyirat rekord ElteltIdo, HatralevoNapok és HatralevoMunkaNapok mezőiből
            /// 
            Eltelt = ugyirat.ElteltIdo;
            int elteltNap;
            int.TryParse(Eltelt, out elteltNap);

            bool hataridoTullepve;
            if (!String.IsNullOrEmpty(ugyirat.Hatarido))
            {
                DateTime hatarido = ugyirat.Typed.Hatarido.Value;

                FennmaradoMunkanap = ugyirat.HatralevoMunkaNapok;
                hataridoTullepve = (hatarido.Date < DateTime.Today);
            }
            else
            {
                FennmaradoMunkanap = String.Empty;
                hataridoTullepve = false;
            }

            //alapértelmezett, ha nincs túllépés
            int TullepesNapszamInt = 0;
            TullepMertekKod = "0";

            if (hataridoTullepve)
            {
                int elteltMunkaNap = elteltNap; // TODO: ez nem tűnik így jónak, de a tárolt eljárásban (sp_EREC_UgyUgyiratok_Sakkora_GetHataridok) is így volt, így most nem nyúltam hozzá
                bool isMunkanap;
                int ugyintezesiNapokSzama = GetUgyiratUgyintezesiIdoNapokban(ugyirat, out isMunkanap);
                TullepMertekKod = isMunkanap && elteltMunkaNap > (ugyintezesiNapokSzama * 2) || (!isMunkanap && elteltNap > (ugyintezesiNapokSzama * 2)) ? "2" : "1";
                TullepesNapszamInt = isMunkanap ? elteltMunkaNap - ugyintezesiNapokSzama : elteltNap - ugyintezesiNapokSzama;
            }
            TullepesNapszam = TullepesNapszamInt.ToString();
            //    }catch(Exception ex)
            //    {
            //        throw new ResultException(ex.Message);
            //    }
        }
    }
}