﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.NMHH
{
    /// <summary>
    /// SZURManager - SAKKORA
    /// </summary>
    public partial class SZURManager
    {
        /// <summary>
        /// Sakkóra leállításának oka
        /// "Visszaadja a sakkóra megállításának lehetséges okait"
        /// </summary>
        /// <returns></returns>
        public string[] SZURGetSakkLeallWS()
        {
            Logger.InfoStart("SZURManager.SZURGetSakkLeallWS");

            var kodtarElemek = KodTar_Cache.GetKodtarakByKodCsoportList(KCS_UGYIRAT_FELFUGGESZTES_OKA, this.ExecParamCaller, HttpContext.Current.Cache, null);

            string[] result = kodtarElemek.Select(k => k.Nev).ToArray();

            Logger.InfoEnd("SZURManager.SZURGetSakkLeallWS");

            return result;
        }

        /// <summary>
        /// Ügy lezárásának oka
        /// "Visszaadja a sakkóra ügy lezáró állapotának lehetséges okait."
        /// </summary>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public string[] SZURGetUgyLezarWS()
        {
            Logger.InfoStart("SZURManager.SZURGetUgyLezarWS");

            var kodtarElemek = KodTar_Cache.GetKodtarakByKodCsoportList(KCS_LEZARAS_OKA, this.ExecParamCaller, HttpContext.Current.Cache, null);

            string[] result = kodtarElemek.Select(k => k.Nev).ToArray();

            Logger.InfoEnd("SZURManager.SZURGetUgyLezarWS");

            return result;
        }

        /// <summary>
        /// Leállító és lezáró sakkóra kódhoz tartozó okok listája
        /// "Az átadott sakkóra hatás kódja alapján a webservice visszaadja 2-es, 5-ös, 9-es, 11-es, 3-as, 6-os, 8-as, 10-es Skod-nak megfelelő, 
        /// a SZÜR által kezelt sakkóra hatásra értelmezett okok listáját."
        /// </summary>
        /// <param name="Skod"></param>
        /// <returns></returns>
        public string[] SZURGetSakkoraOkKodWS(string Skod)
        {
            Logger.InfoStart("SZURManager.SZURGetSakkoraOkKodWS");

            /// A megadott sakkóra hatás kód alapján visszaadjuk 
            /// vagy az "Ügyirat felfüggesztés  oka" 
            /// vagy az "Ügyirat lezárásának oka" kódtár listát
            /// 

            var sakkoraHatasList = KodTar_Cache.GetKodtarakByKodCsoportList(KCS_IRAT_HATASA_UGYINTEZESRE, this.ExecParamCaller, HttpContext.Current.Cache, null);
            // A paraméterben jött sakkóra kódtár elem:
            var sakkoraHatasElem = sakkoraHatasList.FirstOrDefault(e => e.Kod == Skod);
            if (sakkoraHatasElem == null)
            {
                // Hiba:
                throw new ResultException(String.Format("Nincs ilyen kódú elem: {0}", Skod));
            }

            string[] sakkoraOkok = null;

            List<Sakkora.EnumIratHatasaUgyintezesre> from = null;
            Sakkora.EnumIratHatasaUgyintezesre to = new Sakkora.EnumIratHatasaUgyintezesre();
            Contentum.eUtility.Sakkora.ExtractIratHatasStates(sakkoraHatasElem.Egyeb, ref from, ref to);

            switch (to)
            {
                case Sakkora.EnumIratHatasaUgyintezesre.PAUSE:
                    sakkoraOkok = KodTar_Cache.GetKodtarakByKodCsoportList(KCS_UGYIRAT_FELFUGGESZTES_OKA, this.ExecParamCaller, HttpContext.Current.Cache, null)
                                   .Select(k => k.Nev).ToArray();
                    break;
                case Sakkora.EnumIratHatasaUgyintezesre.STOP:
                    sakkoraOkok = KodTar_Cache.GetKodtarakByKodCsoportList(KCS_LEZARAS_OKA, this.ExecParamCaller, HttpContext.Current.Cache, null)
                                    .Select(k => k.Nev).ToArray();
                    break;
            }

            string[] result = sakkoraOkok;

            Logger.InfoEnd("SZURManager.SZURGetSakkoraOkKodWS");

            return result;
        }

        /// <summary>
        /// Irat sakkóra státusz lekérdezése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="SakkoraStatusz"></param>
        public void SZURGetSakkoraStatusWS(string Evszam, string Konyv, string Foszam, string Alszam, out string SakkoraStatusz)
        {
            Logger.InfoStart("SZURManager.SZURGetSakkoraStatusWS");

            // A megadott irat "IratHatasaUgyintezesre" mezőjének érékte kell

            DataRow iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam);

            SakkoraStatusz = iratRow["IratHatasaUgyintezesre"].ToString();

            Logger.InfoEnd("SZURManager.SZURGetSakkoraStatusWS");
        }

        /// <summary>
        /// Egy ügyben (megadott főszám alapján a teljes láncolatra) listázza az iktatószámokat és hatásukat a sakkórára.
        /// "A megadott főszámhoz tartozó összes alszám sakkóra kódjának, a hozzátartozó szöveges érték, 
        /// az esetleges ok (megállítás, lezárás) és a sakkóra hatás időpontjának listázása"
        /// </summary>
        /// <param name="Ev"></param>
        /// <param name="Konyv">A Konyv eredetileg nem volt benne a WSDL-ben!</param>
        /// <param name="Foszam"></param>
        /// <param name="SakkoraElemList"></param>
        public void SZURGetsakkoraListWS(string Ev, string Foszam, out SakkoraElem[] SakkoraElemList)
        {
            Logger.InfoStart("SZURManager.SZURGetsakkoraListWS");

            // Ügyirat lekérése:
            DataRow ugyiratRow = this.GetUgyiratRow(Ev, String.Empty, Foszam);
            Guid ugyiratId = (Guid)ugyiratRow["Id"];

            #region Ügyirat szerelési lánc lekérése

            var execParamSzereltek = this.ExecParamCaller.Clone();
            execParamSzereltek.Record_Id = ugyiratId.ToString();
            var resultSzerelesiLanc = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().GetAllSzereltByNode(execParamSzereltek);

            if (resultSzerelesiLanc.IsError)
            {
                throw new ResultException(resultSzerelesiLanc);
            }

            // Ügyirat Id-k összegyűjtése:
            Dictionary<Guid, DataRow> ugyiratok = new Dictionary<Guid, DataRow>();

            foreach (DataRow rowSzerelt in resultSzerelesiLanc.Ds.Tables[0].Rows)
            {
                ugyiratok[(Guid)rowSzerelt["Id"]] = rowSzerelt;
            }

            #endregion

            #region Iratok lekérése (ügyiratlista alapján)

            EREC_IraIratokSearch searchIratok = new EREC_IraIratokSearch();
            searchIratok.Ugyirat_Id.Value = "'" + String.Join("','", ugyiratok.Select(e => e.Key.ToString()).ToArray()) + "'";
            searchIratok.Ugyirat_Id.Operator = Query.Operators.inner;

            var resultIratGetAll = eRecordService.ServiceFactory.GetEREC_IraIratokService().GetAll(this.ExecParamCaller, searchIratok);
            if (resultIratGetAll.IsError)
            {
                throw new ResultException(resultIratGetAll);
            }

            #endregion

            #region Visszatérési érték összeállítása

            List<SakkoraElem> sakkoraElemek = new List<SakkoraElem>();

            foreach (DataRow rowIrat in resultIratGetAll.Ds.Tables[0].Rows)
            {
                Guid iratUgyiratId = (Guid)rowIrat["Ugyirat_Id"];

                // Hozzá tartozó ügyirat DataRow megkeresése:
                DataRow iratUgyiratRow = ugyiratok[iratUgyiratId];

                // Iktatókönyv:
                var iktatoKonyv = GetIktatokonyv((Guid)iratUgyiratRow["IraIktatokonyv_Id"], true);

                SakkoraElem sakkoraElem = new SakkoraElem();

                // TODO: Eloszam
                //sakkoraElem.Eloszam == 
                sakkoraElem.Foszam = iratUgyiratRow["Foszam"].ToString();
                sakkoraElem.Alszam = rowIrat["Alszam"].ToString();
                sakkoraElem.Ev = iktatoKonyv.Ev;
                sakkoraElem.Iktatoszam = rowIrat["Azonosito"].ToString();
                sakkoraElem.Skod = rowIrat["IratHatasaUgyintezesre"].ToString();
                KodTar_Cache.KodTarElem sakkoraKodtarElem = null;

                // Sakkóra kódtárelem lekérése:
                if (!String.IsNullOrEmpty(sakkoraElem.Skod))
                {
                    var sakkoraKodok = KodTar_Cache.GetKodtarakByKodCsoportList(KCS_IRAT_HATASA_UGYINTEZESRE, this.ExecParamCaller, HttpContext.Current.Cache, null);
                    sakkoraKodtarElem = sakkoraKodok.FirstOrDefault(e => e.Kod == sakkoraElem.Skod);
                    if (sakkoraKodtarElem != null)
                    {
                        sakkoraElem.Stext = sakkoraKodtarElem.Nev;
                    }
                }
                //Visszafele csak egy 0-as lehet.
                sakkoraElem.Skod = Sakkora.ChangebBackSakkoraStatusIfNull(rowIrat["IratHatasaUgyintezesre"].ToString());

                /// Okkod:
                /// A sakkóra típusától függően vagy az EREC_UgyUgyiratok.FelfuggesztesOka, vagy az EREC_UgyUgyiratok.LezarasOka mező értéke kell itt
                /// 
                if (sakkoraKodtarElem != null)
                {
                    List<Sakkora.EnumIratHatasaUgyintezesre> from = null;
                    Sakkora.EnumIratHatasaUgyintezesre to = new Sakkora.EnumIratHatasaUgyintezesre();
                    Contentum.eUtility.Sakkora.ExtractIratHatasStates(sakkoraKodtarElem.Egyeb, ref from, ref to);

                    switch (to)
                    {
                        case Sakkora.EnumIratHatasaUgyintezesre.PAUSE:
                            sakkoraElem.Okkod = iratUgyiratRow["FelfuggesztesOka"].ToString();
                            break;
                        case Sakkora.EnumIratHatasaUgyintezesre.STOP:
                            sakkoraElem.Okkod = iratUgyiratRow["LezarasOka"].ToString();
                            break;
                    }
                }

                // TODO: Datum: ezt honnan vegyük?
                //sakkoraElem.Datum = 

                sakkoraElemek.Add(sakkoraElem);
            }

            SakkoraElemList = sakkoraElemek.ToArray();

            #endregion

            Logger.InfoEnd("SZURManager.SZURGetsakkoraListWS");
        }

        /// <summary>
        /// irat sakkórastátuszának beállítása
        /// Az irat "IratHatasaUgyintezesre" mezőjének beállítása, és ennek értékétől függően az ügyirat esetleges felfüggesztése/folytatása/lezárása.
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="SakkoraStatus"></param>
        /// <param name="OkKod"></param>
        /// <param name="UserID"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZURSetSakkoraStatusWS(string Evszam, string Konyv, string Foszam, string Alszam, string SakkoraStatus, string OkKod, string UserID, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURManager.SZURSetSakkoraStatusWS");
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            try
            {
                DataRow iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam);
                string iratId = iratRow["Id"].ToString();
                string ugyiratId = iratRow["Ugyirat_Id"].ToString();

                eRecord.Service.SakkoraService service = eRecordService.ServiceFactory.Get_SakkoraService();

                var execParamCan = this.CreateExecParamByNMHHUserId(UserID);

                #region SAKKORA 0 ATFROGATAS
                if (SakkoraStatus.StartsWith("0"))
                {
                    Result resultLathatoAllapotok = service.GetSakkoraLathatoAllapotok(execParamCan, iratId, ugyiratId);
                    resultLathatoAllapotok.CheckError();
                    if (resultLathatoAllapotok == null || resultLathatoAllapotok.Record == null)
                        throw new Exception("Sakkóra átmenet nem engedélyezett !");

                    string lathatoAllapotokResult = resultLathatoAllapotok.Record.ToString();
                    if (!string.IsNullOrEmpty(lathatoAllapotokResult))
                    {
                        string[] lathatoAllapotok = lathatoAllapotokResult.Split(',');
                        string newSakkoraStatus = lathatoAllapotok.FirstOrDefault(l => l.StartsWith("0"));
                        if (!string.IsNullOrEmpty(newSakkoraStatus))
                            SakkoraStatus = newSakkoraStatus;
                    }
                }
                #endregion

                #region CAN SET SAKKOR ATMENET
                Result resultCan = service.CanSetSakkora(execParamCan, iratId, ugyiratId, SakkoraStatus);
                resultCan.CheckError();

                bool canSetSakkoraAllapot = (bool)resultCan.Record;
                if (canSetSakkoraAllapot == false)
                    throw new Exception("Sakkóra átmenet nem engedélyezett !");
                #endregion

                var execParam = this.CreateExecParamByNMHHUserId(UserID);
                Result result = service.SetSakkoraStatus(execParam, iratId, ugyiratId, SakkoraStatus, OkKod, UserID);
                if (result.IsError)
                {
                    Hibakod = result.ErrorCode;
                    Hibaszoveg = result.ErrorMessage;
                    Logger.Error("SZURManager.SZURSetSakkoraStatusWS.SetSakkoraStatus: " + result.ErrorMessage);
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);
                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }
            Logger.InfoEnd("SZURManager.SZURSetSakkoraStatusWS");

        }
        /// <summary>
        ///Az irat "IratHatasaUgyintezesre" mezőjének beállítása, és ennek értékétől függően az ügyirat esetleges felfüggesztése/folytatása/lezárása.
        ///határidő újraszámítása
        /// </summary>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Ev"></param>
        /// <param name="Skod"></param>
        /// <param name="Okkod"></param>
        /// <param name="User"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <param name="Eltelt"></param>
        /// <param name="Fennmarado"></param>
        /// <param name="TullepMertekKod"></param>
        /// <param name="TullepMertekText"></param>
        public void SZURSetSakkoraWS(string Foszam, string Alszam, string Ev, string Skod, string Okkod, string User, out string Eltelt, out string Fennmarado, out string TullepMertekKod, out string TullepMertekText)
        {
            Logger.InfoStart("SZURManager.SZURSetSakkoraWS");

            Eltelt = string.Empty;
            Fennmarado = string.Empty;
            TullepMertekKod = string.Empty;
            TullepMertekText = string.Empty;

            DataRow iratRow = this.GetIratRow(Ev, string.Empty, Foszam, Alszam);
            string iratId = iratRow["Id"].ToString();
            string ugyiratId = iratRow["Ugyirat_Id"].ToString();

            eRecord.Service.SakkoraService service = eRecordService.ServiceFactory.Get_SakkoraService();

            var execParamCan = this.CreateExecParamByNMHHUserId(User);

            #region SAKKORA 0 ATFROGATAS
            if (Skod.StartsWith("0"))
            {
                Result resultLathatoAllapotok = service.GetSakkoraLathatoAllapotok(execParamCan, iratId, ugyiratId);
                resultLathatoAllapotok.CheckError();
                if (resultLathatoAllapotok == null || resultLathatoAllapotok.Record == null)
                    throw new Exception("Sakkóra átmenet nem engedélyezett !");

                string lathatoAllapotokResult = resultLathatoAllapotok.Record.ToString();
                if (!string.IsNullOrEmpty(lathatoAllapotokResult))
                {
                    string[] lathatoAllapotok = lathatoAllapotokResult.Split(',');
                    string newSakkoraStatus = lathatoAllapotok.FirstOrDefault(l => l.StartsWith("0"));
                    if (!string.IsNullOrEmpty(newSakkoraStatus))
                        Skod = newSakkoraStatus;
                }
            }
            #endregion

            #region CAN SET SAKKOR ATMENET
            Result resultCan = service.CanSetSakkora(execParamCan, iratId, ugyiratId, Skod);
            resultCan.CheckError();

            bool canSetSakkoraAllapot = (bool)resultCan.Record;
            if (canSetSakkoraAllapot == false)
                throw new Exception("Sakkóra átmenet nem engedélyezett !");
            #endregion

            var execParam = this.CreateExecParamByNMHHUserId(User);
            Result result = service.SetSakkoraStatus(execParam, iratId, ugyiratId, Skod, Okkod, User);
            result.CheckError();

            GetUgyHataridok(ugyiratId, true, out Eltelt, out Fennmarado, out TullepMertekKod, out TullepMertekText);

            Logger.InfoEnd("SZURManager.SZURSetSakkoraWS");
        }

        /// <summary>
        /// Visszaadja az ügyirat határidő értékeit
        /// </summary>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <param name="Eltelt"></param>
        /// <param name="Fennmarado"></param>
        /// <param name="TullepMertekKod"></param>
        /// <param name="TullepMertekText"></param>
        /// <param name="ugyiratId"></param>
        public void GetUgyHataridok(string ugyiratId, bool hataridoMarNovelve, out string Eltelt, out string Fennmarado, out string TullepMertekKod, out string TullepMertekText)
        {
            eRecord.Service.SakkoraService service = eRecordService.ServiceFactory.Get_SakkoraService();
            ExecParam execParam = ExecParamCaller.Clone();
            Result resultHatarido = service.GetSakkoraHataridok(execParam, ugyiratId, hataridoMarNovelve);
            resultHatarido.CheckError();

            DateTime letrehozasIdo;
            DateTime.TryParse(resultHatarido.Ds.Tables[0].Rows[0]["LetrehozasIdo"].ToString(), out letrehozasIdo);

            DateTime hatarido;
            DateTime.TryParse(resultHatarido.Ds.Tables[0].Rows[0]["Hatarido"].ToString(), out hatarido);

            Eltelt = resultHatarido.Ds.Tables[0].Rows[0]["Eltelt_Nap"].ToString();
            Fennmarado = resultHatarido.Ds.Tables[0].Rows[0]["Fennmarado_Nap"].ToString();

            int elteltNap;
            int.TryParse(resultHatarido.Ds.Tables[0].Rows[0]["Eltelt_Nap"].ToString(), out elteltNap);

            int elteltMunkaNap;
            int.TryParse(resultHatarido.Ds.Tables[0].Rows[0]["Eltelt_MunkaNap"].ToString(), out elteltMunkaNap);

            bool hataridoTullepve;
            bool.TryParse(resultHatarido.Ds.Tables[0].Rows[0]["Hatarido_Tullepve"].ToString(), out hataridoTullepve);

            eRecord.Service.EREC_UgyUgyiratokService serviceUgyirat = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParamUgy = this.ExecParamCaller.Clone();
            execParamUgy.Record_Id = ugyiratId;
            Result resultUgy = serviceUgyirat.Get(execParamUgy);
            resultUgy.CheckError();

            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)resultUgy.Record;

            if (!hataridoTullepve)
            {
                TullepMertekKod = "0";
                TullepMertekText = "Nincs túllépés";
            }
            else
            {
                bool isMunkanap;
                int ugyintezesiNapokSzama = GetUgyiratUgyintezesiIdoNapokban(ugyirat, out isMunkanap);
                if (ugyintezesiNapokSzama == 0)
                {
                    if (!string.IsNullOrEmpty(ugyirat.UgyintezesKezdete) && !string.IsNullOrEmpty(ugyirat.Hatarido))
                    {
                        DateTime dateStart = DateTime.Parse(ugyirat.UgyintezesKezdete);
                        DateTime dateStop = DateTime.Parse(ugyirat.Hatarido);
                        double diffPre = (dateStop - dateStart).TotalDays;

                        int eltelt = int.Parse(ugyirat.ElteltIdo);

                        if (diffPre * 2 > eltelt)
                        {
                            TullepMertekKod = "2";
                            TullepMertekText = "Az ügyintézés időtartama meghaladta az irányadó határidő kétszeresét";
                        }
                        else
                        {
                            TullepMertekKod = "1";
                            TullepMertekText = "Az ügyintézés időtartama meghaladta az irányadó határidőt";
                        }
                    }
                    else
                    {
                        TullepMertekKod = "1";
                        TullepMertekText = "Az ügyintézés időtartama meghaladta az irányadó határidőt";
                    }
                }
                else if (isMunkanap && elteltMunkaNap > (ugyintezesiNapokSzama * 2)
                     ||
                     (!isMunkanap && elteltNap > (ugyintezesiNapokSzama * 2)))
                {
                    TullepMertekKod = "2";
                    TullepMertekText = "Az ügyintézés időtartama meghaladta az irányadó határidő kétszeresét";
                }
                else
                {
                    TullepMertekKod = "1";
                    TullepMertekText = "Az ügyintézés időtartama meghaladta az irányadó határidőt";
                }
            }
        }

        /// <summary>
        /// Ugy tipus lekérése
        /// </summary>
        /// <param name="execparam"></param>
        /// <param name="ugyTipusId"></param>
        /// <returns></returns>
        private Result GetUgyTipus(ExecParam execparam, string ugyTipusId)
        {
            var resUgytipus = new Result();
            #region Ügytípus
            using (var iratMetadefinicioService = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService())
            {
                var execiratMetadefinicio = execparam.Clone();
                execiratMetadefinicio.Record_Id = ugyTipusId;
                resUgytipus = iratMetadefinicioService.Get(execiratMetadefinicio);
            }
            #endregion
            return resUgytipus;
        }
        /// <summary>
        /// Visszaadja az ügyirat intézési idejét napokban
        /// </summary>
        /// <param name="ugyirat"></param>
        /// <returns></returns>
        private int GetUgyiratUgyintezesiIdoNapokban(EREC_UgyUgyiratok ugyirat, out bool isMunkaNap)
        {
            int days = 0;
            int ugyintezesiIdo;
            isMunkaNap = false;
            if (int.TryParse(ugyirat.IntezesiIdo, out ugyintezesiIdo))
            {
                switch (ugyirat.IntezesiIdoegyseg)
                {
                    case "10080"://hét
                        days = ugyintezesiIdo * 7;
                        break;
                    case "1440"://nap
                        days = ugyintezesiIdo;
                        break;
                    case "60"://óra
                        days = ugyintezesiIdo / 24;
                        break;
                    case "1"://perc
                        days = (ugyintezesiIdo / 60) / 24;
                        break;
                    case "-1440"://munkanap
                        days = ugyintezesiIdo;
                        isMunkaNap = true;
                        break;
                    default:
                        break;
                }
            }
            return days;
        }

        /// <summary>
        /// A megadott sakkóra ok ha nem kódként, hanem megjelenítendő névként van megadva, akkor a kódját adja vissza.
        /// (Ha nem találunk ilyen kódú vagy nevű elemet, akkor nincs konverzió, visszaadjuk a megadott értéket.
        /// </summary>
        /// <param name="sakkoraOk"></param>
        /// <returns></returns>
        public static string ConvertSakkoraOkToKodtarKod(string sakkoraOk, ExecParam execParamCaller)
        {
            return Sakkora.ConvertSakkoraOkToKodtarKod(sakkoraOk, execParamCaller);
        }
    }
}
