﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.NMHH
{
    /// <summary>
    /// Summary description for SZURManager
    /// </summary>
    public partial class SZURManager
    {
       
        #region Constructor

        public SZURManager(SZURService service)
        {
            this.SZURService = service;
            if (!String.IsNullOrEmpty(service.ErrorMessage))
            {
                throw new ResultException(service.ErrorMessage);
            }
        }

        #endregion

        #region Properties

        public SZURService SZURService { get; private set; }

        public ExecParam ExecParamCaller
        {
            get
            {
                return this.SZURService.ExecParamCaller;
            }
        }

        #endregion

        #region Ügyirat/irat adatok lekérdezése

        /// <summary>
        /// Ügyintéző átveendő iratainak listája
        /// "Egy ügyintézőre szignált, még át nem vett ügyiratborítók és ügyiratok listáját adja vissza."
        /// Átveendő ügyirat esetén az összes iratot adjuk vissza!
        /// TODO: Szakrendszerkódra szűrni kell (ha nincs megadva szakrendszer kód, akkor az összes átveendő kell.)
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="rendszerkod"></param>
        /// <returns></returns>
        public Irat[] SZURGetAtvIratWS(string userId, string rendszerkod)
        {
            Logger.InfoStart("SZURManager.SZURGetAtvIratWS");
            var execParam = this.CreateExecParamByNMHHUserId(userId);

            List<Irat> resultIratList = new List<Irat>();


            // Az iratpéldányok alapján lekért irat Id lista
            // Ebbe gyűjtjük ki, hogy egy adott irathoz melyik kézbesítési sor tartozik 
            // (ha pl. iratpéldányra volt kézbesítési tétel, akkor az irathoz rendeljük azt a kézb. tétel sort 
            // --> ebből fogjuk kiszedni az irathoz a szignálót/szignálás dátumát)
            List<Guid> iratIdListByPld = new List<Guid>();
            var iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();


            // Jelenlegi userre szignált iratok lekérdezése
            AddSignedIratsForUser(iratokService, execParam, iratIdListByPld);


            #region Kézbesítési tételek lekérdezése

            var kezbesitesiTetelekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();

            var searchObjKezbTetelek = new EREC_IraKezbesitesiTetelekSearch();

            // Szűrés azokra a rekordokra, ahol a felhasználó benne van a Címzett csoportban:            

            // Ha a jogosultsága engedi, akkor a szervezetére átadott tételeket is visszaadjuk:
            if (!String.IsNullOrEmpty(execParam.FelhasznaloSzervezet_Id) && NMHH.Utility.Security.HasFunctionRights(this.ExecParamCaller, userId, FunkcioKod_AtvetelSzervezettol))
            {
                searchObjKezbTetelek.Csoport_Id_Cel.Value = "'" + execParam.Felhasznalo_Id + "','" + execParam.FelhasznaloSzervezet_Id + "'";
                searchObjKezbTetelek.Csoport_Id_Cel.Operator = Query.Operators.inner;
            }
            else
            {
                searchObjKezbTetelek.Csoport_Id_Cel.Value = execParam.Felhasznalo_Id;
                searchObjKezbTetelek.Csoport_Id_Cel.Operator = Query.Operators.equals;
            }

            searchObjKezbTetelek.Allapot.Value = "'" + KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott + "','" + KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott + "'";
            searchObjKezbTetelek.Allapot.Operator = Query.Operators.inner;

            searchObjKezbTetelek.Obj_type.Value = "'EREC_UgyUgyiratok','EREC_PldIratPeldanyok'";
            searchObjKezbTetelek.Obj_type.Operator = Query.Operators.inner;

            var resultKezbTetelek = kezbesitesiTetelekService.GetAllWithExtension(execParam, searchObjKezbTetelek);
            resultKezbTetelek.CheckError();

            #endregion

            // Kézbesítési tételek alapján 
            // Kigyűjtjük az ügyirat Id-kat, illetve az iratpéldány id-kat:

            #region Iratok lekérése

            List<Guid> ugyiratIdList = new List<Guid>();
            List<Guid> iratPldIdList = new List<Guid>();
            // A kézbesítési tételek ObjId alapú szótárba kigyűjtve a gyorsabb keresés végett:
            Dictionary<Guid, DataRow> kezbTetelRowsByObjId = new Dictionary<Guid, DataRow>();
            // Kézbesítési tételekből kiszedjük az ügyirat és iratpéldány Id-kat:
            foreach (DataRow row in resultKezbTetelek.Ds.Tables[0].Rows)
            {
                string objType = row["Obj_type"].ToString();
                if (objType == "EREC_UgyUgyiratok")
                {
                    ugyiratIdList.Add((Guid)row["Obj_Id"]);
                }
                else if (objType == "EREC_PldIratPeldanyok")
                {
                    iratPldIdList.Add((Guid)row["Obj_Id"]);
                }

                // Átadó nevének kigyűjtése:
                kezbTetelRowsByObjId[(Guid)row["Obj_Id"]] = row;
            }


            Dictionary<Guid, DataRow> kezbTetelRowsByIratId = new Dictionary<Guid, DataRow>();
            // Iratpéldányok lekérése az iratId-k meghatározásához:
            if (iratPldIdList.Count > 0)
            {
                var searchObjIratPldGetAll = new EREC_PldIratPeldanyokSearch();
                searchObjIratPldGetAll.Id.Value = "'" + String.Join("','", iratPldIdList.Select(e => e.ToString()).ToArray()) + "'";
                searchObjIratPldGetAll.Id.Operator = Query.Operators.inner;
                // Iratpéldányok lekérése:
                var resultIratPldGetAll = eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService().GetAll(execParam, searchObjIratPldGetAll);
                resultIratPldGetAll.CheckError();

                // IratId-k kigyűjtése:
                foreach (DataRow pldRow in resultIratPldGetAll.Ds.Tables[0].Rows)
                {
                    Guid iratId = (Guid)pldRow["IraIrat_Id"];
                    if (!iratIdListByPld.Contains(iratId))
                    {
                        iratIdListByPld.Add(iratId);
                    }

                    // Irat Id-hoz hozzárendeljük az iratpéldány kézbesítési tétel sorát:
                    Guid pldId = (Guid)pldRow["Id"];
                    if (kezbTetelRowsByObjId.ContainsKey(pldId))
                    {
                        kezbTetelRowsByIratId[iratId] = kezbTetelRowsByObjId[pldId];
                    }
                }
            }

            if (ugyiratIdList.Count > 0 || iratIdListByPld.Count > 0)
            {
                // Iratok lekérése: azok, amelyek a kigyűjtött ügyiratokhoz tartoznak + amelyek a kigyűjtött iratpéldányokhoz

                List<DataRow> iratokDataRowsList = new List<DataRow>();

                // Nem működik a VAGY kapcsolat az Extended Search objektumokkal, így külön kell lekérnünk az iratokat:

                //if (ugyiratIdList.Count > 0)
                //{
                //    var searchObjIratok = new EREC_IraIratokSearch(true);
                //    searchObjIratok.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = "'" + String.Join("','", ugyiratIdList.Select(e => e.ToString()).ToArray()) + "'";
                //    searchObjIratok.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.inner;

                //    // Iratok lekérése:
                //    var resultIratok = iratokService.GetAllWithExtension(execParam, searchObjIratok);
                //    resultIratok.CheckError();

                //    foreach (DataRow row in resultIratok.Ds.Tables[0].Rows)
                //    {
                //        iratokDataRowsList.Add(row);
                //    }
                //}

                if (iratIdListByPld.Count > 0)
                {
                    EREC_IraIratokSearch searchObjIratok = new EREC_IraIratokSearch();
                    searchObjIratok.Id.Value = "'" + String.Join("','", iratIdListByPld.Select(e => e.ToString()).ToArray()) + "'";
                    searchObjIratok.Id.Operator = Query.Operators.inner;

                    // Iratok lekérése:
                    var resultIratok = iratokService.GetAllWithExtension(execParam, searchObjIratok);
                    resultIratok.CheckError();

                    foreach (DataRow row in resultIratok.Ds.Tables[0].Rows)
                    {
                        iratokDataRowsList.Add(row);
                    }
                }

                if (iratokDataRowsList.Count > 0)
                {
                    // Néhány ügyirat adat nincs benne az IratokGetAllWithExtension-ben, lekérjük az ügyiratok adatait is külön:
                    // Az összes irat ügyiratId-ja (nem csak ami a kézbesítési tételekből jött):
                    List<Guid> ugyiratIdListAll = iratokDataRowsList.Select(row => (Guid)row["UgyiratId"]).ToList();

                    var searchObjUgyiratok = new EREC_UgyUgyiratokSearch();
                    searchObjUgyiratok.Id.Value = "'" + String.Join("','", ugyiratIdListAll.Select(e => e.ToString()).ToArray()) + "'";
                    searchObjUgyiratok.Id.Operator = Query.Operators.inner;

                    // Ügyiratok lekérése:
                    var resultUgyiratokList = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().GetAllWithExtension(execParam, searchObjUgyiratok);
                    resultUgyiratokList.CheckError();

                    // Eredménylista feltöltése:
                    foreach (DataRow iratRow in iratokDataRowsList)
                    {
                        // Megkeressük az irathoz tartozó ügyirat sort:
                        DataRow ugyiratRow = null;
                        Guid ugyiratId = (Guid)iratRow["UgyiratId"];
                        foreach (DataRow r in resultUgyiratokList.Ds.Tables[0].Rows)
                        {
                            if ((Guid)r["Id"] == ugyiratId)
                            {
                                ugyiratRow = r;
                                break;
                            }
                        }

                        Guid iratId = (Guid)iratRow["Id"];

                        Irat irat = CreateIratFromIratGetAllWithExt(iratRow, ugyiratRow);
                        /// BUG#4050/Task#4104: a szignálónak állítsuk be az átadót 
                        /// (iratpéldánynál és sima ügyirat átadásnál is szeretnék látni, így az ügyirat szignáló mezője nem jó)
                        /// 
                        // Az irathoz megkeressük a kézbesítési tétel sort (ami vagy az iratpéldányhoz vagy az ügyirathoz tartozott):
                        DataRow kezbTetelRow = null;
                        if (kezbTetelRowsByIratId.ContainsKey(iratId))
                        {
                            kezbTetelRow = kezbTetelRowsByIratId[iratId];
                        }
                        // Ügyirat id-ra keressük meg a kézbesítési sort:
                        else if (kezbTetelRowsByObjId.ContainsKey(ugyiratId))
                        {
                            kezbTetelRow = kezbTetelRowsByObjId[ugyiratId];
                        }
                        // Szignáló és szignálás dátuma: a kézbesítési tétel átadója és átadás dátuma lesz:
                        if (kezbTetelRow != null)
                        {
                            irat.Szignalo = kezbTetelRow["Atado_Nev"].ToString();
                            irat.Szignalas = Helper.GetFormattedDateAndTimeString(kezbTetelRow["AtadasDat"]); // Ez elvileg EEEE.HH.NN formátumban jön
                        }
                        else
                        {//kézbesités tétel nélküli iratnál az utolsó módosító nevét és módosítás dátumát vesszük alapul
                            var felhasznaloObj = GetFelhasznloById(iratRow["Modosito_id"].ToString(), this.ExecParamCaller.Clone());
                            irat.Szignalo = felhasznaloObj.Nev; ;
                            string dateTimeStamp = Helper.GetFormattedDateAndTimeString(iratRow["ModositasIdo"]);
                            irat.Szignalas = string.IsNullOrEmpty(dateTimeStamp) ? string.Empty : Helper.GetFormattedDateOnlyString(dateTimeStamp);
                        }

                        resultIratList.Add(irat);
                    }
                }

                // BUG#7879: Ha ügyirat is volt az átveendők között, akkor az ügyiratokra is kell visszaadni külön tételt 0-ás alszámmal:
                if (ugyiratIdList.Count > 0)
                {
                    var searchObjUgyiratok = new EREC_UgyUgyiratokSearch();
                    searchObjUgyiratok.Id.Value = "'" + String.Join("','", ugyiratIdList.Select(e => e.ToString()).ToArray()) + "'";
                    searchObjUgyiratok.Id.Operator = Query.Operators.inner;

                    var resultUgyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().GetAllWithExtension(execParam, searchObjUgyiratok);
                    resultUgyiratok.CheckError();

                    foreach (DataRow ugyiratRow in resultUgyiratok.Ds.Tables[0].Rows)
                    {
                        // Irat objektum létrehozása az ügyirathoz 0-ás alszámmal:
                        Irat irat = CreateIratFromUgyiratGetAllWithExt(ugyiratRow);
                        resultIratList.Add(irat);
                    }
                }
            }

            #endregion

            Logger.InfoEnd("SZURManager.SZURGetAtvIratWS");

            // Sorbarendezzük a találatokat (csak hogy az ügyirat sort kövessék a hozzá tartozó iratok):
            return resultIratList.OrderBy(e => e.Ugy).ThenBy(e => e.Alszam).ToArray();
        }

        /// <summary>
        /// Ügyintéző átveendő iratainak listája
        /// "Egy ügyintézőre szignált, még át nem vett ügyiratborítók és ügyiratok listáját adja vissza."
        /// Átveendő ügyirat esetén az összes iratot adjuk vissza!
        /// TODO: Szakrendszerkódra szűrni kell (ha nincs megadva szakrendszer kód, akkor az összes átveendő kell.)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="rendszerkod"></param>
        /// <returns></returns>
        public eIrat[] SZUReGetAtvIratWS(string userId, string rendszerkod)
        {
            Logger.InfoStart("SZURManager.SZURGetAtvIratWS");
            var execParam = this.CreateExecParamByNMHHUserId(userId);

            List<eIrat> resultIratList = new List<eIrat>();

            // Az iratpéldányok alapján lekért irat Id lista
            // Ebbe gyűjtjük ki, hogy egy adott irathoz melyik kézbesítési sor tartozik 
            // (ha pl. iratpéldányra volt kézbesítési tétel, akkor az irathoz rendeljük azt a kézb. tétel sort 
            // --> ebből fogjuk kiszedni az irathoz a szignálót/szignálás dátumát)

            List<Guid> iratIdListByPld = new List<Guid>();
            var iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();


            // Jelenlegi userre szignált iratok lekérdezése
            AddSignedIratsForUser(iratokService, execParam, iratIdListByPld);


            #region Kézbesítési tételek lekérdezése

            var kezbesitesiTetelekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();

            var searchObjKezbTetelek = new EREC_IraKezbesitesiTetelekSearch();

            // Szűrés azokra a rekordokra, ahol a felhasználó benne van a Címzett csoportban:            

            // Ha a jogosultsága engedi, akkor a szervezetére átadott tételeket is visszaadjuk:
            if (!String.IsNullOrEmpty(execParam.FelhasznaloSzervezet_Id) && NMHH.Utility.Security.HasFunctionRights(this.ExecParamCaller, userId, FunkcioKod_AtvetelSzervezettol))
            {
                searchObjKezbTetelek.Csoport_Id_Cel.Value = "'" + execParam.Felhasznalo_Id + "','" + execParam.FelhasznaloSzervezet_Id + "'";
                searchObjKezbTetelek.Csoport_Id_Cel.Operator = Query.Operators.inner;
            }
            else
            {
                searchObjKezbTetelek.Csoport_Id_Cel.Value = execParam.Felhasznalo_Id;
                searchObjKezbTetelek.Csoport_Id_Cel.Operator = Query.Operators.equals;
            }

            searchObjKezbTetelek.Allapot.Value = "'" + KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott + "','" + KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott + "'";
            searchObjKezbTetelek.Allapot.Operator = Query.Operators.inner;

            searchObjKezbTetelek.Obj_type.Value = "'EREC_UgyUgyiratok','EREC_PldIratPeldanyok'";
            searchObjKezbTetelek.Obj_type.Operator = Query.Operators.inner;

            var resultKezbTetelek = kezbesitesiTetelekService.GetAllWithExtension(execParam, searchObjKezbTetelek);
            resultKezbTetelek.CheckError();

            #endregion

            // Kézbesítési tételek alapján 
            // Kigyűjtjük az ügyirat Id-kat, illetve az iratpéldány id-kat:

            #region Iratok lekérése

            List<Guid> ugyiratIdList = new List<Guid>();
            List<Guid> iratPldIdList = new List<Guid>();
            // A kézbesítési tételek ObjId alapú szótárba kigyűjtve a gyorsabb keresés végett:
            Dictionary<Guid, DataRow> kezbTetelRowsByObjId = new Dictionary<Guid, DataRow>();
            // Kézbesítési tételekből kiszedjük az ügyirat és iratpéldány Id-kat:
            foreach (DataRow row in resultKezbTetelek.Ds.Tables[0].Rows)
            {
                string objType = row["Obj_type"].ToString();
                if (objType == "EREC_UgyUgyiratok")
                {
                    ugyiratIdList.Add((Guid)row["Obj_Id"]);
                }
                else if (objType == "EREC_PldIratPeldanyok")
                {
                    iratPldIdList.Add((Guid)row["Obj_Id"]);
                }

                // Átadó nevének kigyűjtése:
                kezbTetelRowsByObjId[(Guid)row["Obj_Id"]] = row;
            }

            // Az iratpéldányok alapján lekért irat Id lista:

            /// Ebbe gyűjtjük ki, hogy egy adott irathoz melyik kézbesítési sor tartozik 
            /// (ha pl. iratpéldányra volt kézbesítési tétel, akkor az irathoz rendeljük azt a kézb. tétel sort --> ebből fogjuk kiszedni az irathoz a szignálót/szignálás dátumát
            /// )
            Dictionary<Guid, DataRow> kezbTetelRowsByIratId = new Dictionary<Guid, DataRow>();
            // Iratpéldányok lekérése az iratId-k meghatározásához:
            if (iratPldIdList.Count > 0)
            {
                var searchObjIratPldGetAll = new EREC_PldIratPeldanyokSearch();
                searchObjIratPldGetAll.Id.Value = "'" + String.Join("','", iratPldIdList.Select(e => e.ToString()).ToArray()) + "'";
                searchObjIratPldGetAll.Id.Operator = Query.Operators.inner;
                // Iratpéldányok lekérése:
                var resultIratPldGetAll = eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService().GetAll(execParam, searchObjIratPldGetAll);
                resultIratPldGetAll.CheckError();

                // IratId-k kigyűjtése:
                foreach (DataRow pldRow in resultIratPldGetAll.Ds.Tables[0].Rows)
                {
                    Guid iratId = (Guid)pldRow["IraIrat_Id"];
                    if (!iratIdListByPld.Contains(iratId))
                    {
                        iratIdListByPld.Add(iratId);
                    }

                    // Irat Id-hoz hozzárendeljük az iratpéldány kézbesítési tétel sorát:
                    Guid pldId = (Guid)pldRow["Id"];
                    if (kezbTetelRowsByObjId.ContainsKey(pldId))
                    {
                        kezbTetelRowsByIratId[iratId] = kezbTetelRowsByObjId[pldId];
                    }
                }
            }

            if (ugyiratIdList.Count > 0 || iratIdListByPld.Count > 0)
            {

                // Iratok lekérése: azok, amelyek a kigyűjtött ügyiratokhoz tartoznak + amelyek a kigyűjtött iratpéldányokhoz

                List<DataRow> iratokDataRowsList = new List<DataRow>();

                // Nem működik a VAGY kapcsolat az Extended Search objektumokkal, így külön kell lekérnünk az iratokat:

                //if (ugyiratIdList.Count > 0)
                //{
                //    var searchObjIratok = new EREC_IraIratokSearch(true);
                //    searchObjIratok.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = "'" + String.Join("','", ugyiratIdList.Select(e => e.ToString()).ToArray()) + "'";
                //    searchObjIratok.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.inner;

                //    // Iratok lekérése:
                //    var resultIratok = iratokService.GetAllWithExtension(execParam, searchObjIratok);
                //    resultIratok.CheckError();

                //    foreach (DataRow row in resultIratok.Ds.Tables[0].Rows)
                //    {
                //        iratokDataRowsList.Add(row);
                //    }
                //}

                if (iratIdListByPld.Count > 0)
                {
                    var searchObjIratok = new EREC_IraIratokSearch();
                    searchObjIratok.Id.Value = "'" + String.Join("','", iratIdListByPld.Select(e => e.ToString()).ToArray()) + "'";
                    searchObjIratok.Id.Operator = Query.Operators.inner;

                    // Iratok lekérése:
                    var resultIratok = iratokService.GetAllWithExtension(execParam, searchObjIratok);
                    resultIratok.CheckError();

                    foreach (DataRow row in resultIratok.Ds.Tables[0].Rows)
                    {
                        iratokDataRowsList.Add(row);
                    }
                }

                if (iratokDataRowsList.Count > 0)
                {
                    // Néhány ügyirat adat nincs benne az IratokGetAllWithExtension-ben, lekérjük az ügyiratok adatait is külön:
                    // Az összes irat ügyiratId-ja (nem csak ami a kézbesítési tételekből jött):
                    List<Guid> ugyiratIdListAll = iratokDataRowsList.Select(row => (Guid)row["UgyiratId"]).ToList();

                    var searchObjUgyiratok = new EREC_UgyUgyiratokSearch();
                    searchObjUgyiratok.Id.Value = "'" + String.Join("','", ugyiratIdListAll.Select(e => e.ToString()).ToArray()) + "'";
                    searchObjUgyiratok.Id.Operator = Query.Operators.inner;

                    // Ügyiratok lekérése:
                    var resultUgyiratokList = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().GetAllWithExtension(execParam, searchObjUgyiratok);
                    resultUgyiratokList.CheckError();

                    // Eredménylista feltöltése:
                    foreach (DataRow iratRow in iratokDataRowsList)
                    {
                        // Megkeressük az irathoz tartozó ügyirat sort:
                        DataRow ugyiratRow = null;
                        Guid ugyiratId = (Guid)iratRow["UgyiratId"];
                        foreach (DataRow r in resultUgyiratokList.Ds.Tables[0].Rows)
                        {
                            if ((Guid)r["Id"] == ugyiratId)
                            {
                                ugyiratRow = r;
                                break;
                            }
                        }

                        Guid iratId = (Guid)iratRow["Id"];

                        eIrat irat = CreateNeweIratFromIratGetAllWithExt(iratRow, ugyiratRow);
                        /// BUG#4050/Task#4104: a szignálónak állítsuk be az átadót 
                        /// (iratpéldánynál és sima ügyirat átadásnál is szeretnék látni, így az ügyirat szignáló mezője nem jó)
                        /// 
                        // Az irathoz megkeressük a kézbesítési tétel sort (ami vagy az iratpéldányhoz vagy az ügyirathoz tartozott):
                        DataRow kezbTetelRow = null;
                        if (kezbTetelRowsByIratId.ContainsKey(iratId))
                        {
                            kezbTetelRow = kezbTetelRowsByIratId[iratId];
                        }
                        // Ügyirat id-ra keressük meg a kézbesítési sort:
                        else if (kezbTetelRowsByObjId.ContainsKey(ugyiratId))
                        {
                            kezbTetelRow = kezbTetelRowsByObjId[ugyiratId];
                        }
                        // Szignáló és szignálás dátuma: a kézbesítési tétel átadója és átadás dátuma lesz:
                        if (kezbTetelRow != null)
                        {
                            irat.Szignalo = kezbTetelRow["Atado_Nev"].ToString();
                            irat.Szignalas = Helper.GetFormattedDateOnlyString(kezbTetelRow["AtadasDat"]);
                        }
                        else
                        {//kézbesités tétel nélküli iratnál az utolsó módosító nevét és módosítás dátumát vesszük alapul
                            var felhasznaloObj = GetFelhasznloById(iratRow["Modosito_id"].ToString(), this.ExecParamCaller.Clone());
                            irat.Szignalo = felhasznaloObj.Nev; ;
                            string dateTimeStamp = Helper.GetFormattedDateAndTimeString(iratRow["ModositasIdo"]);
                            irat.Szignalas = string.IsNullOrEmpty(dateTimeStamp) ? string.Empty : Helper.GetFormattedDateOnlyString(dateTimeStamp);
                        }
                        resultIratList.Add(irat);
                    }
                }

                // BUG#7879: Ha ügyirat is volt az átveendők között, akkor az ügyiratokra is kell visszaadni külön tételt 0-ás alszámmal:
                if (ugyiratIdList.Count > 0)
                {
                    var searchObjUgyiratok = new EREC_UgyUgyiratokSearch();
                    searchObjUgyiratok.Id.Value = "'" + String.Join("','", ugyiratIdList.Select(e => e.ToString()).ToArray()) + "'";
                    searchObjUgyiratok.Id.Operator = Query.Operators.inner;

                    var resultUgyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().GetAllWithExtension(execParam, searchObjUgyiratok);
                    resultUgyiratok.CheckError();

                    foreach (DataRow ugyiratRow in resultUgyiratok.Ds.Tables[0].Rows)
                    {
                        // Irat objektum létrehozása az ügyirathoz 0-ás alszámmal:
                        eIrat irat = CreateNeweIratFromUgyiratGetAllWithExt(ugyiratRow);
                        resultIratList.Add(irat);
                    }
                }
            }

            #endregion

            Logger.InfoEnd("SZURManager.SZURGetAtvIratWS");

            // Sorbarendezzük a találatokat (csak hogy az ügyirat sort kövessék a hozzá tartozó iratok):
            return resultIratList.OrderBy(e => e.Ugy).ThenBy(e => e.Alszam).ToArray();
        }

        /// <summary>
        /// Adott főszámhoz listázza a létrehozott alszámokat.
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <returns></returns>
        public string[] SZURAlszamListWS(string Evszam, string Konyv, string Foszam)
        {
            Logger.InfoStart("SZURManager.SZURAlszamListWS");

            // Ügyirat lekérése
            DataRow rowUgyirat = this.GetUgyiratRow(Evszam, Konyv, Foszam);
            Guid ugyiratId = (Guid)rowUgyirat["Id"];

            #region Iratok lekérése

            // Ügyirat iratainak lekérése:
            var iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();

            var searchObjIratok = new EREC_IraIratokSearch();

            searchObjIratok.Ugyirat_Id.Value = ugyiratId.ToString();
            searchObjIratok.Ugyirat_Id.Operator = Query.Operators.equals;

            var resultIratok = iratokService.GetAll(this.ExecParamCaller, searchObjIratok);
            resultIratok.CheckError();

            #endregion

            List<string> alszamLista = new List<string>();

            foreach (DataRow row in resultIratok.Ds.Tables[0].Rows)
            {
                alszamLista.Add(row["Alszam"].ToString());
            }

            string[] resultAlszamok = alszamLista.ToArray();

            Logger.InfoEnd("SZURManager.SZURAlszamListWS");

            return resultAlszamok;
        }

        /// <summary>
        /// Egy ügy főszám elemeinek lekérdezése ügyszám vagy könyv, évszám, főszám alapján. 
        /// </summary>
        /// <param name="Ugy">A lekérdezett ügy teljes iktatószáma.
        /// Bemeneti és kimeneti paraméter is!! (Emiatt generálta "ref"-esre a wsdl.exe.)</param>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="UserID"></param>
        /// <param name="AktualisEvszam"></param>
        /// <param name="AktualisKonyv"></param>
        /// <param name="AktualisFoszam"></param>
        /// <param name="FelelosUserID"></param>
        /// <param name="Foszamok"></param>
        /// <param name="KapcsoltUgyek"></param>
        /// <param name="Ugyiratjelleg"></param>
        /// <param name="Ugyjellegkod"></param>
        /// <param name="UI_Hatarido"></param>
        /// <param name="UI_HiMod"></param>
        /// <param name="UI_Munkanap"></param>
        /// <returns></returns>
        public void SZURGetUgyAdatokWS(string Evszam, string Konyv, string Foszam, string UserID,
          out string AktualisEvszam, ref string Ugy, out string AktualisKonyv, out string AktualisFoszam, out string FelelosUserID, out Foszam2[] Foszamok, out KapcsoltUgy[] KapcsoltUgyek, out string Ugyiratjelleg, out string Ugyjellegkod, out string UI_Hatarido, out string UI_HiMod, out string UI_Munkanap)
        {
            Logger.InfoStart("SZURManager.SZURGetUgyAdatokWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserID);

            #region Ügyirat lekérdezése

            /// - Ügyirat lekérdezése vagy az iktatószám alapján, vagy pedig az évszám/iktatókönyv/főszám alapján
            /// 
            DataRow ugyiratRow;

            // Ha meg van adva az évszám/iktatókönyv/főszám, akkor erre szűrünk:
            if (!String.IsNullOrEmpty(Evszam)
                && !String.IsNullOrEmpty(Konyv)
                && !String.IsNullOrEmpty(Foszam))
            {
                ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
            }
            // Egyébként ha az iktatószám van megadva, akkor arra szűrünk:
            else if (!String.IsNullOrEmpty(Ugy))
            {
                ugyiratRow = this.GetUgyiratRow(Ugy, execParamUserId);
            }
            else
                throw new ResultException("Nincs megadva minden adat az ügyirat azonosításához!");

            Guid ugyiratId = (Guid)ugyiratRow["Id"];

            #endregion

            #region Szerelési lánc lekérése

            var execParamSzereltek = execParamUserId.Clone();
            execParamSzereltek.Record_Id = ugyiratId.ToString();
            var resultSzerelesiLanc = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().GetAllSzereltByNode(execParamSzereltek);
            resultSzerelesiLanc.CheckError();

            // Utóügyirat meghatározása:
            DataRow rowUtoUgyirat = null;
            Guid currentUgyiratId = ugyiratId;
            do
            {
                foreach (DataRow rowSzerelt in resultSzerelesiLanc.Ds.Tables[0].Rows)
                {
                    if ((Guid)rowSzerelt["Id"] == currentUgyiratId)
                    {
                        // Megvan az ügyirat DataRow-ja, megnézzük van-e utóügyirata:
                        Guid? ugyiratIdSzulo = rowSzerelt["UgyUgyirat_Id_Szulo"] as Guid?;
                        // Ha ennek már nincs utóügyirata, akkor ez lesz a teljes szerelési lánc utóügyirata:
                        if (ugyiratIdSzulo == null)
                        {
                            rowUtoUgyirat = rowSzerelt;
                        }
                        else
                        {
                            currentUgyiratId = ugyiratIdSzulo.Value;
                        }
                    }
                }
            }
            // Addig megyünk, míg el nem jutunk a szerelési lánc végére:
            while (rowUtoUgyirat == null);

            #endregion

            #region Out paraméterek értékadása

            Ugy = ugyiratRow["Foszam_Merge"].ToString();

            // Ha volt utóügyirat (különbözik az eredeti ügyirattól), akkor lekérjük az utóügyirat iktatókönyvét
            if ((Guid)rowUtoUgyirat["Id"] != ugyiratId)
            {
                EREC_IraIktatoKonyvek utoUgyiratIktatoKonyv = this.GetIktatokonyv((Guid)rowUtoUgyirat["IraIktatokonyv_Id"], true);

                AktualisEvszam = utoUgyiratIktatoKonyv.Ev;
                AktualisKonyv = utoUgyiratIktatoKonyv.Iktatohely;
            }
            else
            {
                // Nincs utóügyirata:
                AktualisEvszam = ugyiratRow["Ev"].ToString();
                AktualisKonyv = ugyiratRow["Iktatohely"].ToString();
            }

            AktualisFoszam = rowUtoUgyirat["Foszam"].ToString();

            // Az aktuális főszám felelőse kell:
            // A felhasználó bejelentkezési neve kell? --> le kell kérni a KRT_Felhasznalok-ból
            // BUG_2330
            // EREC_UgyUgyiratok.Felhasznalo_CsoportId_Ugyintezo kell
            //Guid? csoportIdFelelos = rowUtoUgyirat["Csoport_Id_Felelos"] as Guid?;
            Guid? csoportIdUgyintezo = rowUtoUgyirat["FelhasznaloCsoport_Id_Ugyintez"] as Guid?;
            if (csoportIdUgyintezo != null)
            {
                // KRT_Felhasznalok rekord lekérése:
                var felhasznalokService = eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                ExecParam execParamFelhGet = this.ExecParamCaller.Clone();
                execParamFelhGet.Record_Id = csoportIdUgyintezo.ToString();

                var resultFelhGet = felhasznalokService.Get(execParamFelhGet);
                resultFelhGet.CheckError();

                KRT_Felhasznalok felhasznaloObj = resultFelhGet.Record as KRT_Felhasznalok;

                FelelosUserID = felhasznaloObj.UserNev;
            }
            else
            {
                FelelosUserID = String.Empty;
            }

            #region Foszamok

            // Az ügyben szerelt főszámok strukturált listája:
            Foszamok = new NMHH.Foszam2[] { };
            // Ha többelemű a szerelési lánc (alapból saját magát tartalmazza a lista):
            if (resultSzerelesiLanc.Ds.Tables[0].Rows.Count > 1)
            {
                List<Foszam2> szerelesiLancElemek = new List<Foszam2>();

                // Foszam2 objektumok létrehozása, feltöltése:
                foreach (DataRow rowSzerelesiLancElem in resultSzerelesiLanc.Ds.Tables[0].Rows)
                {
                    /// Csak a szerelteket adjuk vissza, az utolsó, aktuális főszámot nem kell: (BUG#3493/Task3547)
                    /// 
                    if (currentUgyiratId == (Guid)rowSzerelesiLancElem["Id"])
                    {
                        continue;
                    }

                    Foszam2 foszamObj = new Foszam2();

                    Guid iktatoKonyvId = (Guid)rowSzerelesiLancElem["IraIktatokonyv_Id"];

                    EREC_IraIktatoKonyvek iktatoKonyv = GetIktatokonyv(iktatoKonyvId, true);

                    foszamObj.Evszam = iktatoKonyv.Ev;
                    foszamObj.Konyv = iktatoKonyv.Iktatohely;
                    foszamObj.Foszam = rowSzerelesiLancElem["Foszam"].ToString();

                    szerelesiLancElemek.Add(foszamObj);
                }

                Foszamok = szerelesiLancElemek.ToArray();
            }
            #endregion

            #region KapcsoltUgyek

            var kapcsoltUgyekList = this.GetKapcsoltUgyekList(ugyiratId, execParamUserId);

            KapcsoltUgyek = kapcsoltUgyekList.ToArray();

            #endregion

            /// TODO:
            /// Az ügyirat jellege, nem hatósági ügy esetén állandó "nem hatósági ügyek" értéket kap a mező, 
            /// egyébként pedig a hatósági ügy szöveges megnevezését. 
            /// --> UgyFajtaja mező alapján
            /// A Segédállomány IF_UGYJELLEG nézetének 1. oszlopában felsorolt értékek
            /// 
            string ugyfajtaja = ugyiratRow["Ugy_Fajtaja"].ToString();

            // Ha a kódtárértékbe be van jegyezve a 'NEM_HATOSAGI' érték, akkor az Ugyiratjelleg fixen legyen "nem hatósági ügyek" érték
            if (!String.IsNullOrEmpty(ugyfajtaja))
            {
                var ugyiratJellegKodtar = KodTar_Cache.GetKodtarakByKodCsoportList(KodTarak.UGY_FAJTAJA.KodcsoportKod, this.ExecParamCaller
                                    , HttpContext.Current.Cache, null).FirstOrDefault(e => e.Kod == ugyfajtaja);
                // BUG#3493/Task#3509: Nem hatósági ügy esetén fixen a "nem hatósági" kódot kapja meg:
                if (ugyiratJellegKodtar != null
                    && ugyiratJellegKodtar.Egyeb == KodTarak.UGY_JELLEG_FAJTAJA_HATOSAGI.NEM_HATOSAGI)
                {
                    ugyfajtaja = KodTarak.UGY_FAJTAJA.Nem_Hatosagi_Ugy;
                }

                // Kódtár névfeloldás:
                Ugyiratjelleg = KodTar_Cache.GetKodtarakByKodCsoport(KodTarak.UGY_FAJTAJA.KodcsoportKod, this.ExecParamCaller
                                    , HttpContext.Current.Cache)[ugyfajtaja];
            }
            else
            {
                Ugyiratjelleg = String.Empty;
            }

            /// Az Ügyjelleg mezőhöz tartozó, a Segédállomány IF_UGYJELLEG nézetének 2. oszlopában felsorolt értékek
            /// Az UgyFajtaja kódtár kódja kell!
            /// 
            Ugyjellegkod = ugyfajtaja;

            // UI_Hatarido: Az ügyintézési határidő dátuma vagy üres, ha nincs megadva. Formátum: éééé.hh.nn,            
            DateTime? hatarido;
            int? hatralevoNapok;
            string szamitasiMod;
            int? ugyintezesiIdo;

            this.GetUgyiratHatarido(ugyiratId, ugyiratRow, out hatarido, out hatralevoNapok, out szamitasiMod, out ugyintezesiIdo);

            // "yyyy.MM.dd" formátumban
            UI_Hatarido = (hatarido != null) ? Helper.GetFormattedDateAndTimeString(hatarido.Value) : string.Empty;
            UI_HiMod = szamitasiMod;
            UI_Munkanap = ugyintezesiIdo.ToString();
            #endregion

            Logger.InfoEnd("SZURManager.SZURGetUgyAdatokWS");
        }

        /// <summary>
        /// Az ügy adatainak lekérdezése
        /// "Egy ügy főszám elemeinek lekérdezése könyv, évszám, főszám alapján."
        /// </summary>
        /// <param name="Ugy"></param>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UserID"></param>
        /// <param name="Eloszam"></param>
        /// <param name="AktualisEvszam"></param>
        /// <param name="AktualisKonyv"></param>
        /// <param name="AktualisFoszam"></param>
        /// <param name="FelelosUserID"></param>
        /// <param name="Foszamok"></param>
        /// <param name="KapcsoltUgyek"></param>
        /// <param name="Ugyiratjelleg"></param>
        /// <param name="Ugyjellegkod"></param>
        /// <param name="UI_Hatarido"></param>
        /// <param name="UI_HiMod"></param>
        /// <param name="UI_Munkanap"></param>
        /// <param name="Eltelt"></param>
        /// <param name="Fennmarado"></param>
        /// <param name="TullepMertekKod"></param>
        /// <param name="TullepMertekText"></param>
        /// <param name="Teves"></param>
        /// <returns></returns>
        public void SZUReGetUgyAdatokWS(
                   string Evszam,
                   string Konyv,
                   string Foszam,
                   string Alszam,
                   string UserID,
                   out string Eloszam,
                   ref string Ugy,
                   out string AktualisEvszam,
                   out string AktualisKonyv,
                   out string AktualisFoszam,
                   out string FelelosUserID,
                   out Foszam2[] Foszamok,
                   out KapcsoltUgy[] KapcsoltUgyek,
                   out string Ugyiratjelleg,
                   out string Ugyjellegkod,
                   out string UI_Hatarido,
                   out string UI_HiMod,
                   out string UI_Munkanap,
                   out string Eltelt,
                   out string Fennmarado,
                   out string TullepMertekKod,
                   out string TullepMertekText,
                   out string Teves)
        {
            Logger.InfoStart("SZURManager.SZUReGetUgyAdatokWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserID);

            #region SET OUT PARAMS VALUE
            Eloszam = string.Empty;
            Ugy = string.Empty;
            AktualisEvszam = string.Empty;
            AktualisKonyv = string.Empty;
            AktualisFoszam = string.Empty;
            FelelosUserID = string.Empty;
            Foszamok = null;
            KapcsoltUgyek = null;
            Ugyiratjelleg = string.Empty;
            Ugyjellegkod = string.Empty;
            UI_Hatarido = string.Empty;
            UI_HiMod = string.Empty;
            UI_Munkanap = string.Empty;
            Eltelt = string.Empty;
            Fennmarado = string.Empty;
            TullepMertekKod = string.Empty;
            TullepMertekText = string.Empty;
            Teves = string.Empty;
            #endregion

            #region Ügyirat lekérdezése

            /// - Ügyirat lekérdezése vagy az iktatószám alapján, vagy pedig az évszám/iktatókönyv/főszám alapján
            /// 
            DataRow ugyiratRow;

            // Ha meg van adva az évszám/iktatókönyv/főszám, akkor erre szűrünk:
            if (!String.IsNullOrEmpty(Evszam)
                && !String.IsNullOrEmpty(Konyv)
                && !String.IsNullOrEmpty(Foszam))
            {
                ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
            }
            /// BUG#6589: Ha csak évszám/főszám van megadva, akkor is megpróbáljuk lekérni, de ha nem egyértelmű ez alapján, 
            /// akkor próbáljuk meg az Ugy mező alapján, ha az ki van töltve
            else if (!String.IsNullOrEmpty(Evszam)
                && !String.IsNullOrEmpty(Foszam))
            {
                try
                {
                    ugyiratRow = this.GetUgyiratRow(Evszam, null, Foszam, execParamUserId);
                }
                catch
                {
                    // Ha ki van töltve az Ugy mező, akkor még megpróbáljuk az alapján lekérni:
                    if (!String.IsNullOrEmpty(Ugy))
                    {
                        ugyiratRow = this.GetUgyiratRow(Ugy, execParamUserId);
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            // Egyébként ha az iktatószám van megadva, akkor arra szűrünk:
            else if (!String.IsNullOrEmpty(Ugy))
            {
                ugyiratRow = this.GetUgyiratRow(Ugy, execParamUserId);
            }
            else
                throw new ResultException("Nincs megadva minden adat az ügyirat azonosításához!");

            Guid ugyiratId = (Guid)ugyiratRow["Id"];

            #endregion

            #region Szerelési lánc lekérése

            var execParamSzereltek = execParamUserId.Clone();
            execParamSzereltek.Record_Id = ugyiratId.ToString();
            var resultSzerelesiLanc = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().GetAllSzereltByNode(execParamSzereltek);
            resultSzerelesiLanc.CheckError();

            // Utóügyirat meghatározása:
            DataRow rowUtoUgyirat = null;
            Guid currentUgyirat = ugyiratId;
            do
            {
                foreach (DataRow rowSzerelt in resultSzerelesiLanc.Ds.Tables[0].Rows)
                {
                    if ((Guid)rowSzerelt["Id"] == currentUgyirat)
                    {
                        // Megvan az ügyirat DataRow-ja, megnézzük van-e utóügyirata:
                        Guid? ugyiratIdSzulo = rowSzerelt["UgyUgyirat_Id_Szulo"] as Guid?;
                        // Ha ennek már nincs utóügyirata, akkor ez lesz a teljes szerelési lánc utóügyirata:
                        if (ugyiratIdSzulo == null)
                        {
                            rowUtoUgyirat = rowSzerelt;
                        }
                        else
                        {
                            currentUgyirat = ugyiratIdSzulo.Value;
                        }
                    }
                }
            }
            // Addig megyünk, míg el nem jutunk a szerelési lánc végére:
            while (rowUtoUgyirat == null);

            #endregion

            #region Out paraméterek értékadása

            Ugy = ugyiratRow["Foszam_Merge"].ToString();

            // Előszámot nem töltjük...
            Eloszam = string.Empty;

            // Ha volt utóügyirat (különbözik az eredeti ügyirattól), akkor lekérjük az utóügyirat iktatókönyvét
            if ((Guid)rowUtoUgyirat["Id"] != ugyiratId)
            {
                EREC_IraIktatoKonyvek utoUgyiratIktatoKonyv = this.GetIktatokonyv((Guid)rowUtoUgyirat["IraIktatokonyv_Id"], true);

                AktualisEvszam = utoUgyiratIktatoKonyv.Ev;
                AktualisKonyv = utoUgyiratIktatoKonyv.Iktatohely;
            }
            else
            {
                // Nincs utóügyirata:
                AktualisEvszam = ugyiratRow["Ev"].ToString();
                AktualisKonyv = ugyiratRow["Iktatohely"].ToString();
            }

            AktualisFoszam = rowUtoUgyirat["Foszam"].ToString();

            // Az aktuális főszám felelőse kell:
            // A felhasználó bejelentkezési neve kell? --> le kell kérni a KRT_Felhasznalok-ból
            Guid? csoportIdFelelos = rowUtoUgyirat["Csoport_Id_Felelos"] as Guid?;
            if (csoportIdFelelos != null)
            {
                // KRT_Felhasznalok rekord lekérése:
                var felhasznalokService = eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                ExecParam execParamFelhGet = this.ExecParamCaller.Clone();
                execParamFelhGet.Record_Id = csoportIdFelelos.ToString();

                var resultFelhGet = felhasznalokService.Get(execParamFelhGet);
                if (resultFelhGet.IsError)
                {
                    // Nincs meg a felhasználó, valószínűleg csoport van beállítva:

                    // throw new ResultException(resultFelhGet);

                    var resultCsoportGet = eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService().Get(execParamFelhGet);
                    resultCsoportGet.CheckError();

                    KRT_Csoportok csoportObj = resultCsoportGet.Record as KRT_Csoportok;

                    FelelosUserID = csoportObj.Nev;
                }
                else
                {
                    KRT_Felhasznalok felhasznaloObj = resultFelhGet.Record as KRT_Felhasznalok;

                    FelelosUserID = felhasznaloObj.UserNev;
                }
            }
            else
            {
                FelelosUserID = String.Empty;
            }

            #region Foszamok

            // Az ügyben szerelt főszámok strukturált listája:
            Foszamok = new Foszam2[] { };
            // Ha többelemű a szerelési lánc (alapból saját magát tartalmazza a lista):
            if (resultSzerelesiLanc.Ds.Tables[0].Rows.Count > 1)
            {
                List<Foszam2> szerelesiLancElemek = new List<Foszam2>();

                // Foszam2 objektumok létrehozása, feltöltése:
                foreach (DataRow rowSzerelesiLancElem in resultSzerelesiLanc.Ds.Tables[0].Rows)
                {
                    Foszam2 foszamObj = new Foszam2();

                    Guid iktatoKonyvId = (Guid)rowSzerelesiLancElem["IraIktatokonyv_Id"];

                    EREC_IraIktatoKonyvek iktatoKonyv = GetIktatokonyv(iktatoKonyvId, true);

                    foszamObj.Evszam = iktatoKonyv.Ev;
                    foszamObj.Konyv = iktatoKonyv.Iktatohely;
                    foszamObj.Foszam = rowSzerelesiLancElem["Foszam"].ToString();

                    szerelesiLancElemek.Add(foszamObj);
                }

                Foszamok = szerelesiLancElemek.ToArray();
            }
            #endregion

            #region KapcsoltUgyek

            var kapcsoltUgyekList = this.GetKapcsoltUgyekList(ugyiratId, execParamUserId);

            KapcsoltUgyek = kapcsoltUgyekList.ToArray();

            #endregion

            /// TODO:
            /// Az ügyirat jellege, nem hatósági ügy esetén állandó "nem hatósági ügyek" értéket kap a mező, 
            /// egyébként pedig a hatósági ügy szöveges megnevezését. 
            /// --> UgyFajtaja mező alapján
            /// A Segédállomány IF_UGYJELLEG nézetének 1. oszlopában felsorolt értékek
            /// 
            string ugyfajtaja = ugyiratRow["Ugy_Fajtaja"].ToString();
            if (ugyfajtaja == eUtility.KodTarak.UGY_FAJTAJA.Nem_Hatosagi_Ugy)
            {
                Ugyiratjelleg = "nem hatósági ügyek";
            }
            else if (!String.IsNullOrEmpty(ugyfajtaja))
            {
                // Kódtár névfeloldás:
                Ugyiratjelleg = KodTar_Cache.GetKodtarakByKodCsoport(KodTarak.UGY_FAJTAJA.KodcsoportKod, this.ExecParamCaller
                                    , HttpContext.Current.Cache)[ugyfajtaja];
            }
            else
            {
                Ugyiratjelleg = String.Empty;
            }

            /// TODO:
            /// Az Ügyjelleg mezőhöz tartozó, a Segédállomány IF_UGYJELLEG nézetének 2. oszlopában felsorolt értékek
            /// 
            Ugyjellegkod = ugyfajtaja;

            // UI_Hatarido: Az ügyintézési határidő dátuma vagy üres, ha nincs megadva. Formátum: éééé.hh.nn,            
            DateTime? hatarido;
            int? hatralevoNapok;
            string szamitasiMod;
            int? ugyintezesiIdo;

            this.GetUgyiratHatarido(ugyiratId, ugyiratRow, out hatarido, out hatralevoNapok, out szamitasiMod, out ugyintezesiIdo);

            // "yyyy.MM.dd" formátumban:
            UI_Hatarido = (hatarido != null) ? Helper.GetFormattedDateOnlyString(hatarido.Value) : String.Empty;
            UI_HiMod = szamitasiMod;
            UI_Munkanap = ugyintezesiIdo.ToString();

            Fennmarado = hatralevoNapok.ToString();

            Eltelt = ugyiratRow["ElteltIdo"].ToString();
            // Eltelt idő: az össz. ügyintésézi idő - a már eltelt napok száma

            /* //OBSOLETE
            if (ugyintezesiIdo != null && hatralevoNapok != null)
            {
                Eltelt = (ugyintezesiIdo.Value - hatralevoNapok.Value).ToString();
            }
            else
            {
                Eltelt = null;
            }
            */

            /// TullepMertekKod:
            /// Az ügyintézési határidő esetleges túllépésének kódja:
            /// "0" = Nincs túllépés
            /// "1" = az ügyintézés időtartama meghaladta az irányadó határidőt
            /// "2" = az ügyintézés időtartama meghaladta az irányadó határidő kétszeresét
            /// 
            TullepMertekKod = string.Empty;
            TullepMertekText = string.Empty;

            if (hatralevoNapok != null
                && ugyintezesiIdo != null)
            {
                if (hatralevoNapok >= 0)
                {
                    // "0" = Nincs túllépés
                    TullepMertekKod = "0";
                    TullepMertekText = "Nincs túllépés";
                }
                else if (hatralevoNapok < 0 && (Math.Abs(hatralevoNapok.Value) <= ugyintezesiIdo.Value))
                {
                    // "1" = az ügyintézés időtartama meghaladta az irányadó határidőt
                    TullepMertekKod = "1";
                    TullepMertekText = "Az ügyintézés időtartama meghaladta az irányadó határidőt";
                }
                else if (hatralevoNapok < 0 && (Math.Abs(hatralevoNapok.Value) > ugyintezesiIdo.Value))
                {
                    // "2" = az ügyintézés időtartama meghaladta az irányadó határidő kétszeresét
                    TullepMertekKod = "2";
                    TullepMertekText = "Az ügyintézés időtartama meghaladta az irányadó határidő kétszeresét";
                }
            }

            // Téves státusz: ha sztornózott az ügyirat
            if (ugyiratRow["Allapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Sztornozott)
            {
                Teves = "I";
            }
            else
            {
                Teves = "N";
            }
            Eloszam = ugyiratRow["UgyFelelos_SzervezetKod"] == null ? String.Empty : ugyiratRow["UgyFelelos_SzervezetKod"].ToString();
            #endregion

            Logger.InfoEnd("SZURManager.SZUReGetUgyAdatokWS");
        }

        /// <summary>
        /// Alszámos irat adatainak lekérdezése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UserID"></param>
        /// <param name="Ugy"></param>
        /// <param name="IratTargy"></param>
        /// <param name="IratTipus"></param>
        /// <param name="IratJelleg"></param>
        /// <param name="Szignalo"></param>
        /// <param name="Szignalas"></param>
        /// <param name="Atvevo"></param>
        /// <param name="Munkatars"></param>
        /// <param name="Atveve"></param>
        /// <param name="Bekuldo"></param>
        /// <param name="BekuldoIrsz"></param>
        /// <param name="BekuldoTelepules"></param>
        /// <param name="BekuldoCim"></param>
        /// <param name="Erkezett"></param>
        /// <param name="Hatarido"></param>
        /// <param name="UgyintezoiNaplo"></param>
        /// <param name="ExpedialasMod"></param>
        /// <param name="KuldemenyTipus"></param>
        /// <param name="Rendeltetes"></param>
        /// <param name="Intezkedes"></param>
        /// <param name="Keszrejelentette"></param>
        /// <param name="Keszrejelentve"></param>
        /// <param name="Jovahagyo"></param>
        /// <param name="Jovahagyva"></param>
        /// <param name="KikuldottTerti"></param>
        /// <param name="ErkezettTerti"></param>
        /// <param name="AtnemvettTerti"></param>
        /// <param name="UtolsoTerti"></param>
        /// <param name="Cimzettek"></param>
        /// <param name="CimzettekFile"></param>
        /// <param name="SakkoraStatusz"></param>
        /// <param name="Expedialva"></param>
        public void SZURGetIratAdatokWS(
                    string Evszam,
                    string Konyv,
                    string Foszam,
                    string Alszam,
                    string UserID,
                    out string Ugy,
                    out string IratTargy,
                    out string IratTipus,
                    out string IratJelleg,
                    out string Szignalo,
                    out string Szignalas,
                    out string Atvevo,
                    out string Munkatars,
                    out string Atveve,
                    out string Bekuldo,
                    out string BekuldoIrsz,
                    out string BekuldoTelepules,
                    out string BekuldoCim,
                    out string Erkezett,
                    out string Hatarido,
                    out string UgyintezoiNaplo,
                    out string ExpedialasMod,
                    out string KuldemenyTipus,
                    out string Rendeltetes,
                    out string Intezkedes,
                    out string Keszrejelentette,
                    out string Keszrejelentve,
                    out string Jovahagyo,
                    out string Jovahagyva,
                    out string KikuldottTerti,
                    out string ErkezettTerti,
                    out string AtnemvettTerti,
                    out string UtolsoTerti,
                    out Cimzett[] Cimzettek,
                    out string CimzettekFile,
                    out string SakkoraStatusz,
                    out string Expedialva)
        {
            Logger.InfoStart("SZURManager.SZURGetIratAdatokWS");

            #region SET DEFAULTS
            Ugy = string.Empty;
            IratTargy = string.Empty;
            IratTipus = string.Empty;
            IratJelleg = string.Empty;
            Szignalo = string.Empty;
            Szignalas = string.Empty;
            Atvevo = string.Empty;
            Munkatars = string.Empty;
            Atveve = string.Empty;
            Bekuldo = string.Empty;
            BekuldoIrsz = string.Empty;
            BekuldoTelepules = string.Empty;
            BekuldoCim = string.Empty;
            Erkezett = string.Empty;
            Hatarido = string.Empty;
            UgyintezoiNaplo = string.Empty;
            ExpedialasMod = string.Empty;
            KuldemenyTipus = string.Empty;
            Rendeltetes = string.Empty;
            Intezkedes = string.Empty;
            Keszrejelentette = string.Empty;
            Keszrejelentve = string.Empty;
            Jovahagyo = string.Empty;
            Jovahagyva = string.Empty;
            KikuldottTerti = string.Empty;
            ErkezettTerti = string.Empty;
            AtnemvettTerti = string.Empty;
            UtolsoTerti = string.Empty;
            CimzettekFile = string.Empty;
            SakkoraStatusz = string.Empty;
            Expedialva = string.Empty;
            Cimzettek = new Cimzett[] { };
            #endregion

            var execParamUserId = this.CreateExecParamByNMHHUserId(UserID);
            Guid? iratId = null;
            DataRow iratRow = null;
            Guid? elsodlegesIratpeldanyId = null;

            if (Alszam.Equals("0"))
            {
                ErkezettTerti = "0";
            }
            else
            {
                #region IRAT
                // Irat lekérése:
                iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam, execParamUserId);
                iratId = (Guid)iratRow["Id"];

                //Guid iratPeldanyId = (Guid)iratRow["ElsoIratPeldany_Id"];

                #region Kimenő paraméterek értékadása

                Ugy = iratRow["IktatoSzam_Merge"].ToString();
                IratTargy = iratRow["Targy1"].ToString();
                // BUG_2742
                IratTipus = iratRow["IrattipusNev"].ToString();
                // BUG#7879: Az kell, ami a felületen az "Irat iránya" (Bejövő/Kimenő)
                IratJelleg = iratRow["PostazasIranyaNev"].ToString();

                Szignalo = String.Empty;
                Szignalas = String.Empty;
                Atvevo = String.Empty;
                Atveve = String.Empty;
                KuldemenyTipus = string.Empty;
                Rendeltetes = string.Empty;

                // Munkatars: 
                // A felhasználó neve kell, vagy a loginneve?
                // BUG#7879: A login név kell
                Munkatars = String.Empty; //iratRow["FelhasznaloCsoport_Id_Ugyintezo_Nev"].ToString();
                Guid? felhIdUgyintezo = iratRow["FelhasznaloCsoport_Id_Ugyintez"] as Guid?;
                if (felhIdUgyintezo != null)
                {
                    ExecParam execParamFelhGet = this.ExecParamCaller.Clone();
                    execParamFelhGet.Record_Id = felhIdUgyintezo.ToString();
                    var resultFelhGet = eAdminService.ServiceFactory.GetKRT_FelhasznalokService().Get(execParamFelhGet);
                    resultFelhGet.CheckError();

                    KRT_Felhasznalok felhObj = resultFelhGet.Record as KRT_Felhasznalok;
                    if (felhObj != null)
                    {
                        Munkatars = felhObj.UserNev;
                    }
                }

                #region Beküldő, beküldő címe

                KRT_Cimek cimObj = null;

                /// Kell a küldemény Cim_Id alapján a KRT_Cimek rekord:
                /// -> le kell kérni a küldemények, és a címek sort is
                /// 
                Guid? kuldemenyId = iratRow["KuldKuldemenyek_Id"] as Guid?;
                if (kuldemenyId != null)
                {
                    ExecParam execParamKuldGet = execParamUserId.Clone();
                    execParamKuldGet.Record_Id = kuldemenyId.ToString();

                    var resultKuldGet = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService().Get(execParamKuldGet);
                    if (resultKuldGet.IsError)
                    {
                        Logger.Error("Küldemény lekérdezés hiba", execParamKuldGet);
                        throw new ResultException(resultKuldGet);
                    }

                    EREC_KuldKuldemenyek kuldemenyObj = (EREC_KuldKuldemenyek)resultKuldGet.Record;
                    if (kuldemenyObj != null)
                    {
                        Bekuldo = kuldemenyObj.NevSTR_Bekuldo;

                        // Cím rekord lekérése:
                        if (!String.IsNullOrEmpty(kuldemenyObj.Cim_Id))
                        {
                            ExecParam execParamCimGet = execParamUserId.Clone();
                            execParamCimGet.Record_Id = kuldemenyObj.Cim_Id;

                            var resultCimGet = eAdminService.ServiceFactory.GetKRT_CimekService().Get(execParamCimGet);
                            if (resultCimGet.IsError)
                            {
                                Logger.Error("Cím rekord lekérése sikertelen", execParamCimGet);
                                throw new ResultException(resultCimGet);
                            }

                            cimObj = (KRT_Cimek)resultCimGet.Record;
                        }
                        else
                        {
                            BekuldoCim = kuldemenyObj.CimSTR_Bekuldo;
                        }

                        var kuldemenyTipusKodtarList = KodTar_Cache.GetKodtarakByKodCsoportList(KCS_KULDEMENY_TIPUS, this.ExecParamCaller, HttpContext.Current.Cache, null);
                        if (kuldemenyTipusKodtarList != null && kuldemenyTipusKodtarList.Count > 0)
                        {
                            var kuldemenyTipusKod = kuldemenyTipusKodtarList.FirstOrDefault(e => e.Kod == kuldemenyObj.Tipus);
                            if (kuldemenyTipusKod != null)
                                KuldemenyTipus = kuldemenyTipusKod.Nev;
                        }

                        if (kuldemenyObj.KimenoKuldemenyFajta.StartsWith(Contentum.eUtility.Constants.OrszagViszonylatKod.Europai))
                        {
                            Rendeltetes = "Europai";
                        }
                        else if (kuldemenyObj.KimenoKuldemenyFajta.StartsWith(Contentum.eUtility.Constants.OrszagViszonylatKod.EgyebKulfoldi))
                        {
                            Rendeltetes = "EgyebKulfoldi";
                        }
                        else
                        {
                            Rendeltetes = "Belfoldi";
                        }
                    }
                }

                // BUG_11574
                if (cimObj != null)
                {
                    if (!String.IsNullOrEmpty(cimObj.Nev))
                    {
                        Bekuldo = cimObj.Nev;
                    }
                    BekuldoIrsz = cimObj.IRSZ;
                    BekuldoTelepules = cimObj.TelepulesNev;
                    BekuldoCim = CimUtility.MergeCim(cimObj, false);
                }
                else
                {
                    string irsz, telepules, cim;
                    ParseBekuldoCim(BekuldoCim, out irsz, out telepules, out cim);
                    BekuldoIrsz = irsz;
                    BekuldoTelepules = telepules;
                    BekuldoCim = cim;
                }

                #endregion

                Erkezett = Helper.GetFormattedDateOnlyString(iratRow["BeerkezesIdeje"]);
                Hatarido = Helper.GetFormattedDateAndTimeString(iratRow["Ugyirat_Hatarido"]);
                UgyintezoiNaplo = iratRow["KezelesiFelj"].ToString();
                ExpedialasMod = iratRow["PeldanyKuldesMod"].ToString();

                // TODO: ezek még kérdésesek:
                Intezkedes = String.Empty;
                Keszrejelentette = String.Empty;

                Keszrejelentve = Helper.GetFormattedDateAndTimeString(iratRow["IntezesIdopontja"]);

                #region Irat aláírók lekérése

                DataTable iratAlairokTable_Jovahagyo = this.GetIratAlairok(iratId.Value, KodTarak.ALAIRO_SZEREP.Jovahagyo);

                if (iratAlairokTable_Jovahagyo.Rows.Count > 0)
                {
                    var jovahagyoRow = iratAlairokTable_Jovahagyo.Rows[0];

                    Jovahagyo = jovahagyoRow["Alairo_Nev"].ToString();
                    Jovahagyva = Helper.GetFormattedDateAndTimeString(jovahagyoRow["AlairasDatuma"]);
                }
                else
                {
                    Jovahagyo = string.Empty;
                    Jovahagyva = string.Empty;
                }

                // NOTE: az ExpedialasDatuma helyett a küldemény ExpedialasIdeje kell, később állítjuk be
                //Expedialva = Helper.GetFormattedDateAndTimeString(iratRow["ExpedialasDatuma"]);

                #endregion

                #region Iratpéldányok adatok lekérése (+ partnerek, címek lekérése)

                EREC_PldIratPeldanyokSearch searchIratPld = new EREC_PldIratPeldanyokSearch();
                searchIratPld.IraIrat_Id.Value = iratId.ToString();
                searchIratPld.IraIrat_Id.Operator = Query.Operators.equals;

                var resultIratPld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService().GetAllWithExtension(execParamUserId, searchIratPld);
                resultIratPld.CheckError();

                // Partner_Id_Cimzett-ek összegyűjtése:
                List<Guid> partnerIdCimzettList = new List<Guid>();
                List<Guid> cimIdCimzettList = new List<Guid>();
                // Iratpéldány Id-k:
                List<Guid> iratPeldanyIdList = new List<Guid>();
                // Elsődleges iratpéldány Id:
                elsodlegesIratpeldanyId = null;
                // Partner_Id lista az elosztóív-tételekből:
                List<Guid> partnerIdListEoIvTetelek = new List<Guid>();

                foreach (DataRow rowIratPld in resultIratPld.Ds.Tables[0].Rows)
                {
                    iratPeldanyIdList.Add((Guid)rowIratPld["Id"]);

                    Guid? partner_Id_Cimzett = rowIratPld["Partner_Id_Cimzett"] as Guid?;
                    if (partner_Id_Cimzett != null)
                    {
                        partnerIdCimzettList.Add(partner_Id_Cimzett.Value);
                    }

                    Guid? cimIdCimzett = rowIratPld["Cim_id_Cimzett"] as Guid?;
                    if (cimIdCimzett != null)
                    {
                        cimIdCimzettList.Add(cimIdCimzett.Value);
                    }

                    if (rowIratPld["Sorszam"].ToString() == "1")
                    {
                        elsodlegesIratpeldanyId = rowIratPld["Id"] as Guid?;
                    }
                }

                #region Partnerek lekérése (Id lista alapján)

                Dictionary<Guid, DataRow> partnerDataRowDict = new Dictionary<Guid, DataRow>();
                /// Ha valamelyik Partner_Id_Cimzett érték egy elosztóív Id, akkor azok ebbe a struktúrába kerülnek:
                /// (ElosztoIvId - (ElosztoivTetelek DataRow lista))
                /// 
                Dictionary<Guid, List<DataRow>> elosztoIvTetelekDict = new Dictionary<Guid, List<DataRow>>();
                /// Az elosztóív-tételekben lévő Partner_Id értékek alapján lekért partnerek (partnerId - DataRow párok)
                Dictionary<Guid, DataRow> elosztoIvTetelPartnerekDict = new Dictionary<Guid, DataRow>();

                if (partnerIdCimzettList.Count > 0)
                {
                    KRT_PartnerekSearch searchPartnerek = new KRT_PartnerekSearch();
                    searchPartnerek.Id.Value = partnerIdCimzettList.ToSqlInnerSearchString();
                    searchPartnerek.Id.Operator = Query.Operators.inner;

                    var resultPartnerek = eAdminService.ServiceFactory.GetKRT_PartnerekService().GetAll(execParamUserId, searchPartnerek);
                    resultPartnerek.CheckError();

                    // Id alapú szótár a partneradatokra:
                    partnerDataRowDict = resultPartnerek.Ds.Tables[0].ToDictionary();

                    #region Elosztóív-tételek lekérése

                    // Ha van olyan partnerIdCimzett érték, amire nem jött találat a partnerlekérdezéssel, akkor az elvileg elosztóív, amit külön le kell kérdezni:
                    var elosztoIvIds = partnerIdCimzettList.Where(e => !partnerDataRowDict.ContainsKey(e)).ToList();
                    // Ha van ilyen, ami elvileg elosztóív, akkor lekérdezzük ezeknek az elosztóív-tételeit:
                    if (elosztoIvIds.Count > 0)
                    {
                        var searchObjEoIvTetelek = new EREC_IraElosztoivTetelekSearch();
                        searchObjEoIvTetelek.ElosztoIv_Id.Value = elosztoIvIds.ToSqlInnerSearchString();
                        searchObjEoIvTetelek.ElosztoIv_Id.Operator = Query.Operators.inner;

                        // Elosztóív-tételek lekérdezése:
                        var resultElosztoivTetelek = eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService()
                                            .GetAll(execParamUserId, searchObjEoIvTetelek);
                        resultElosztoivTetelek.CheckError();

                        // Struktúra feltöltése:
                        foreach (DataRow rowEoIvTetel in resultElosztoivTetelek.Ds.Tables[0].Rows)
                        {
                            Guid elosztoIvId = (Guid)rowEoIvTetel["ElosztoIv_Id"];
                            if (!elosztoIvTetelekDict.ContainsKey(elosztoIvId))
                            {
                                elosztoIvTetelekDict.Add(elosztoIvId, new List<DataRow>());
                            }

                            elosztoIvTetelekDict[elosztoIvId].Add(rowEoIvTetel);

                            // Ha ki van töltve a Partner_Id mező, akkor azokat kigyűjtjük:
                            Guid? partnerId = rowEoIvTetel["Partner_Id"] as Guid?;
                            if (partnerId != null)
                            {
                                partnerIdListEoIvTetelek.Add(partnerId.Value);
                            }

                            // Ha ki van töltve a Cim_Id mező, akkor felvesszük a cím Id listába a cím adatok lekéréséhez:
                            Guid? cimId = rowEoIvTetel["Cim_Id"] as Guid?;
                            if (cimId != null)
                            {
                                cimIdCimzettList.Add(cimId.Value);
                            }
                        }

                        #region Partnerek lekérése (elosztóív tételekből kigyűjtött partnerek)

                        if (partnerIdListEoIvTetelek.Count > 0)
                        {
                            searchPartnerek = new KRT_PartnerekSearch();
                            searchPartnerek.Id.Value = partnerIdListEoIvTetelek.ToSqlInnerSearchString();
                            searchPartnerek.Id.Operator = Query.Operators.inner;

                            resultPartnerek = eAdminService.ServiceFactory.GetKRT_PartnerekService().GetAll(execParamUserId, searchPartnerek);
                            resultPartnerek.CheckError();

                            // Id alapú szótár a partneradatokra:
                            elosztoIvTetelPartnerekDict = resultPartnerek.Ds.Tables[0].ToDictionary();
                        }

                        #endregion
                    }
                    #endregion
                }

                #endregion

                #region Személy adatok lekérése (partnerId listára szűrünk)

                Dictionary<Guid, DataRow> szemelyDataRowDict = new Dictionary<Guid, DataRow>();
                // Az összes partnerId, amire a személyeket le kell kérni (iratpéldányok Partner_Id_Cimzett értékei, plusz az esetleges elosztóív-tételekből a Partner_Id-k):
                List<Guid> partnerIdListAll = new List<Guid>();
                partnerIdListAll.AddRange(partnerIdCimzettList);
                partnerIdListAll.AddRange(partnerIdListEoIvTetelek);

                if (partnerIdCimzettList.Count > 0)
                {
                    KRT_SzemelyekSearch searchSzemelyek = new KRT_SzemelyekSearch();
                    searchSzemelyek.Partner_Id.Value = partnerIdListAll.ToSqlInnerSearchString();
                    searchSzemelyek.Partner_Id.Operator = Query.Operators.inner;

                    var resultSzemelyek = eAdminService.ServiceFactory.GetKRT_SzemelyekService().GetAll(execParamUserId, searchSzemelyek);
                    resultSzemelyek.CheckError();

                    // Id alapú szótár a személyekre (Partner_Id lesz a szótárban a kulcs):
                    szemelyDataRowDict = resultSzemelyek.Ds.Tables[0].ToDictionary("Partner_Id");
                }

                #endregion

                #region Partnerkapcsolatok: Szervezet kapcsolattartója lekérdezés

                // A partnerekhez tartozó 
                Dictionary<Guid, Guid> kapcsolattartoPartnerSzervezeteDict = new Dictionary<Guid, Guid>();
                Dictionary<Guid, DataRow> partnerSzervezetekDataRowDict = new Dictionary<Guid, DataRow>();

                /// A lekért partnerekhez a szervezetek lekérése
                /// (Partner_Id_Kapcsolt a szervezet, Partner_Id-ban van a kapcsolattartó)
                /// 
                if (partnerIdListAll.Count > 0)
                {
                    KRT_PartnerKapcsolatokSearch searchPartnerKapcs = new KRT_PartnerKapcsolatokSearch();
                    searchPartnerKapcs.Partner_id.Value = partnerIdListAll.ToSqlInnerSearchString();
                    searchPartnerKapcs.Partner_id.Operator = Query.Operators.inner;

                    searchPartnerKapcs.Tipus.Value = KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja;
                    searchPartnerKapcs.Tipus.Operator = Query.Operators.equals;

                    var resultPartnerKapcs = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService().GetAll(this.ExecParamCaller, searchPartnerKapcs);
                    resultPartnerKapcs.CheckError();

                    foreach (DataRow rowPartnerKapcs in resultPartnerKapcs.Ds.Tables[0].Rows)
                    {
                        // Kigyűjtjük a találatokat:
                        kapcsolattartoPartnerSzervezeteDict[(Guid)rowPartnerKapcs["Partner_id"]] = (Guid)rowPartnerKapcs["Partner_id_kapcsolt"];
                    }

                    List<Guid> szervezetPartnerIds = kapcsolattartoPartnerSzervezeteDict.Select(e => e.Value).Distinct().ToList();

                    // Szervezet partnerek lekérése:
                    if (szervezetPartnerIds.Count > 0)
                    {
                        KRT_PartnerekSearch searchPartnerSzervezetek = new KRT_PartnerekSearch();
                        searchPartnerSzervezetek.Id.Value = szervezetPartnerIds.ToSqlInnerSearchString();
                        searchPartnerSzervezetek.Id.Operator = Query.Operators.inner;

                        var resultPartnerSzervezetek = eAdminService.ServiceFactory.GetKRT_PartnerekService().GetAll(this.ExecParamCaller, searchPartnerSzervezetek);
                        resultPartnerSzervezetek.CheckError();

                        // Kigyűjtjük a találatokat egy Dictionary-ba:
                        partnerSzervezetekDataRowDict = resultPartnerSzervezetek.Ds.Tables[0].ToDictionary();
                    }
                }

                #endregion

                #region Cím adatok lekérése (cimId lista alapján)

                Dictionary<Guid, DataRow> cimekDataRowDict = new Dictionary<Guid, DataRow>();
                if (cimIdCimzettList.Count > 0)
                {
                    KRT_CimekSearch searchCimek = new KRT_CimekSearch();
                    searchCimek.Id.Value = cimIdCimzettList.ToSqlInnerSearchString();
                    searchCimek.Id.Operator = Query.Operators.inner;

                    var resultCimek = eAdminService.ServiceFactory.GetKRT_CimekService().GetAll(execParamUserId, searchCimek);
                    resultCimek.CheckError();

                    // Id alapú szótár a címekre:
                    cimekDataRowDict = resultCimek.Ds.Tables[0].ToDictionary();
                }

                #endregion

                // Kódtárfüggőség lekérése:
                KodtarFuggosegDataClass ktFuggosegKuldKuldesMod2NmhhExpMod = Utility.GetKodtarFuggoseg(SZURManager.KCS_KULDEMENY_KULDES_MODJA, SZURManager.KCS_NMHH_EXPMOD, this.ExecParamCaller);

                // Iratpéldányok alapján Cimzettek struktúra felépítése:
                List<Cimzett> cimzettekList = new List<Cimzett>();
                foreach (DataRow rowIratPld in resultIratPld.Ds.Tables[0].Rows)
                {
                    Guid? partnerIdCimzett = rowIratPld["Partner_Id_Cimzett"] as Guid?;
                    if (partnerIdCimzett != null)
                    {
                        // BUG#7778:
                        // Ha elosztóíves a címzett, akkor az elosztóív minden tételére kell külön Cimzett objektum:
                        if (elosztoIvTetelekDict.ContainsKey(partnerIdCimzett.Value)
                            && elosztoIvTetelekDict[partnerIdCimzett.Value].Count > 0)
                        {
                            #region Elosztóív tételek --> Cimzett

                            foreach (DataRow rowEoIvTetel in elosztoIvTetelekDict[partnerIdCimzett.Value])
                            {
                                Guid? partnerId = rowEoIvTetel["Partner_Id"] as Guid?;
                                DataRow partnerRow = null;
                                if (partnerId != null
                                    && elosztoIvTetelPartnerekDict.ContainsKey(partnerId.Value))
                                {
                                    partnerRow = elosztoIvTetelPartnerekDict[partnerId.Value];
                                }

                                DataRow szemelyRow = null;
                                if (partnerId != null
                                    && szemelyDataRowDict.ContainsKey(partnerId.Value))
                                {
                                    szemelyRow = szemelyDataRowDict[partnerId.Value];
                                }

                                Guid? cimId = rowEoIvTetel["Cim_Id"] as Guid?;
                                DataRow cimRow = null;
                                if (cimId != null
                                    && cimekDataRowDict.ContainsKey(cimId.Value))
                                {
                                    cimRow = cimekDataRowDict[cimId.Value];
                                }

                                DataRow kapcsoltPartnerSzervezetRow = null;
                                if (partnerId != null
                                    && kapcsolattartoPartnerSzervezeteDict.ContainsKey(partnerId.Value))
                                {
                                    Guid partnerIdSzervezet = kapcsolattartoPartnerSzervezeteDict[partnerId.Value];
                                    if (partnerSzervezetekDataRowDict.ContainsKey(partnerIdSzervezet))
                                    {
                                        kapcsoltPartnerSzervezetRow = partnerSzervezetekDataRowDict[partnerIdSzervezet];
                                    }
                                }

                                // Cimzett objektum létrehozása az adatokból:
                                Cimzett cimzett = this.CreateCimzett(rowIratPld, partnerRow, szemelyRow, cimRow, kapcsoltPartnerSzervezetRow, ktFuggosegKuldKuldesMod2NmhhExpMod);

                                // Ha a partnerId null, akkor az elosztóív-tételből vesszük ki a címet:
                                if (partnerRow == null)
                                {
                                    cimzett.Nev = rowEoIvTetel["NevSTR"].ToString();
                                }
                                if (cimRow == null)
                                {
                                    cimzett.Cim = rowEoIvTetel["CimSTR"].ToString();
                                }

                                cimzettekList.Add(cimzett);
                            }

                            #endregion
                        }
                        else
                        {
                            #region Cimzett az iratpéldányból

                            DataRow partnerRow = null;
                            if (partnerDataRowDict.ContainsKey(partnerIdCimzett.Value))
                            {
                                partnerRow = partnerDataRowDict[partnerIdCimzett.Value];
                            }

                            DataRow rowSzemely = null;
                            if (szemelyDataRowDict.ContainsKey(partnerIdCimzett.Value))
                            {
                                rowSzemely = szemelyDataRowDict[partnerIdCimzett.Value];
                            }

                            Guid? cimIdCimzett = rowIratPld["Cim_id_Cimzett"] as Guid?;

                            DataRow cimRow = null;
                            if (cimIdCimzett != null
                                && cimekDataRowDict.ContainsKey(cimIdCimzett.Value))
                            {
                                cimRow = cimekDataRowDict[cimIdCimzett.Value];
                            }

                            DataRow kapcsoltPartnerSzervezetRow = null;
                            if (partnerIdCimzett != null
                                && kapcsolattartoPartnerSzervezeteDict.ContainsKey(partnerIdCimzett.Value))
                            {
                                Guid partnerIdSzervezet = kapcsolattartoPartnerSzervezeteDict[partnerIdCimzett.Value];
                                if (partnerSzervezetekDataRowDict.ContainsKey(partnerIdSzervezet))
                                {
                                    kapcsoltPartnerSzervezetRow = partnerSzervezetekDataRowDict[partnerIdSzervezet];
                                }
                            }

                            // Cimzett objektum létrehozása az adatokból:
                            Cimzett cimzett = this.CreateCimzett(rowIratPld, partnerRow, rowSzemely, cimRow, kapcsoltPartnerSzervezetRow, ktFuggosegKuldKuldesMod2NmhhExpMod);

                            cimzettekList.Add(cimzett);

                            #endregion
                        }
                    }
                }

                Cimzettek = cimzettekList.ToArray();

                // TODO:
                CimzettekFile = null;

                #endregion

                #region Iratpéldányok kimenő küldeményei

                EREC_Kuldemeny_IratPeldanyaiSearch searchKuldIratPld = new EREC_Kuldemeny_IratPeldanyaiSearch();
                searchKuldIratPld.Peldany_Id.Value = iratPeldanyIdList.ToSqlInnerSearchString();
                searchKuldIratPld.Peldany_Id.Operator = Query.Operators.inner;

                var resultGetAllKuldPld = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService()
                        .GetAllWithExtension(execParamUserId, searchKuldIratPld);
                resultGetAllKuldPld.CheckError();

                // Küldemény Id-k összeszedése, és kimenő küldemény lekérdezés, szűrve ezekre az id-kra:
                List<Guid> pldKuldemenyIdList = new List<Guid>();
                foreach (DataRow rowKuldPld in resultGetAllKuldPld.Ds.Tables[0].Rows)
                {
                    Guid pldKuldemenyId = (Guid)rowKuldPld["KuldKuldemeny_Id"];
                    pldKuldemenyIdList.Add(pldKuldemenyId);
                }

                if (pldKuldemenyIdList.Count > 0)
                {
                    EREC_KuldKuldemenyekSearch searchKimenoKuld = new EREC_KuldKuldemenyekSearch();
                    searchKimenoKuld.Id.Value = pldKuldemenyIdList.ToSqlInnerSearchString();
                    searchKimenoKuld.Id.Operator = Query.Operators.inner;

                    // Küldemények lekérése:
                    var resultKimenoKuldGetAll = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService()
                                                        .GetAll(execParamUserId, searchKimenoKuld);
                    resultKimenoKuldGetAll.CheckError();

                    // KikuldottTerti: azon sorok száma, ahol a Tertiveveny = '1'
                    int kikuldottTertiSzam = 0;
                    foreach (DataRow rowKimenoKuld in resultKimenoKuldGetAll.Ds.Tables[0].Rows)
                    {
                        if (rowKimenoKuld["Tertiveveny"].ToString() == "1")
                        {
                            kikuldottTertiSzam++;
                        }

                        // BUG_12535: az Expedialva az első küldemény ExpedialasIdeje lesz, ahol ki van töltve
                        if (String.IsNullOrEmpty(Expedialva))
                        { 
                            var expedialasIdeje = rowKimenoKuld["ExpedialasIdeje"];
                            if (!DBNull.Value.Equals(expedialasIdeje))
                            {
                                Expedialva = Helper.GetFormattedDateAndTimeString(expedialasIdeje);
                            }
                        }
                    }

                    KikuldottTerti = kikuldottTertiSzam.ToString();

                    #region EREC_KuldTertivevenyek lekérése

                    EREC_KuldTertivevenyekSearch searchKuldTerti = new EREC_KuldTertivevenyekSearch();
                    searchKuldTerti.Kuldemeny_Id.Value = pldKuldemenyIdList.ToSqlInnerSearchString();
                    searchKuldTerti.Kuldemeny_Id.Operator = Query.Operators.inner;

                    // Kimenő küldeményekhez tartozó EREC_KuldTertivevenyek lekérése:
                    var resultKuldTertivevenyek = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService()
                                                    .GetAll(execParamUserId, searchKuldTerti);
                    resultKuldTertivevenyek.CheckError();

                    int atvettTertiSzam = 0;
                    int atNemVettTertiSzam = 0;
                    // Az utoljára visszaérkezett tértivevény dátuma:
                    DateTime? tertiVisszaDatUtolso = null;

                    // TertivisszaKod tartalmazza, hogy átvett/nem átvett
                    foreach (DataRow rowKuldTerti in resultKuldTertivevenyek.Ds.Tables[0].Rows)
                    {
                        switch (rowKuldTerti["TertivisszaKod"].ToString())
                        {
                            case KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_Cimzettnek:
                            case KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_kozvetett_kezbesitonek:
                                atvettTertiSzam++;
                                break;
                            default:
                                atNemVettTertiSzam++;
                                break;
                        }

                        DateTime? tertiVisszaDat = rowKuldTerti["TertivisszaDat"] as DateTime?;
                        if (tertiVisszaDat != null
                            && (tertiVisszaDatUtolso == null || tertiVisszaDatUtolso < tertiVisszaDat))
                        {
                            tertiVisszaDatUtolso = tertiVisszaDat;
                        }
                    }

                    ErkezettTerti = atvettTertiSzam.ToString();
                    AtnemvettTerti = atNemVettTertiSzam.ToString();
                    if (tertiVisszaDatUtolso != null)
                    {
                        UtolsoTerti = tertiVisszaDatUtolso.Value.ToString(SZURWSDateFormatString);
                    }
                    else
                    {
                        UtolsoTerti = String.Empty;
                    }

                    #endregion
                }
                else
                {
                    KikuldottTerti = "0";
                    ErkezettTerti = "0";
                    AtnemvettTerti = "0";
                    UtolsoTerti = String.Empty;
                }

                #endregion

                SakkoraStatusz = iratRow["IratHatasaUgyintezesre"].ToString();

                #endregion

                #endregion IRAT
            }

            #region UGY ADATOK

            DataRow ugyRow = this.GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
            string ugyiratId = ugyRow["Id"].ToString();

            if (Alszam.Equals("0"))
            {
                Ugy = ugyRow["Foszam_Merge"].ToString();

                Hatarido = Helper.GetFormattedDateAndTimeString(ugyRow["Hatarido"]);
                IratTargy = ugyRow["Targy"].ToString();
                Intezkedes = IratTargy;

                #region SZIGNÁLÁS ESEMÉNY(EK) LEKÉRÉSE

                KRT_EsemenyekSearch esemenyekSearch = new KRT_EsemenyekSearch(true);
                esemenyekSearch.Obj_Id.Value = ugyiratId.ToString();
                esemenyekSearch.Obj_Id.Operator = Query.Operators.equals;
                esemenyekSearch.Extended_KRT_FunkciokSearch.Kod.Value = "'UgyiratSzignalas'";
                esemenyekSearch.Extended_KRT_FunkciokSearch.Kod.Operator = Query.Operators.inner;
                esemenyekSearch.OrderBy = " KRT_Esemenyek.LetrehozasIdo DESC ";
                var resultSzignalasEsemenyek = eAdminService.ServiceFactory.GetKRT_EsemenyekService().GetAllWithExtension(this.ExecParamCaller, esemenyekSearch);
                resultSzignalasEsemenyek.CheckError();

                #endregion

                #region KEZB TETELEK
                DateTime? szignalasEsemenyDat = null;

                if (resultSzignalasEsemenyek.Ds.Tables[0].Rows.Count > 0)
                {
                    DataRow rowSzignalasEsemeny = resultSzignalasEsemenyek.Ds.Tables[0].Rows[0];
                    // A szignáló és a szignálás dátuma az eseményből jön:
                    Szignalo = rowSzignalasEsemeny["Felhasznalo_Id_User_Nev"].ToString();

                    szignalasEsemenyDat = rowSzignalasEsemeny["LetrehozasIdo"] as DateTime?;
                    Szignalas = Helper.GetFormattedDateAndTimeString(szignalasEsemenyDat.Value);

                    if (ugyRow.Table.Columns.Contains("Ugyintezo_Nev"))
                        Atvevo = ugyRow["Ugyintezo_Nev"].ToString();

                    /// Átvétel dátumának meghatározása:
                    /// Le kell kérni a szignálás esemény utáni első olyan kézbesítési tételt (akár az ügyiratra, akár az elsődleges iratpéldányra), ahol az átvétel már megtörtént. 
                    /// (Az átvétel dátuma legyen nagyobb, mint a szignálás dátuma, és ezek közül a legkisebb érték kell.)

                    #region Kézbesítési tételek lekérése

                    EREC_IraKezbesitesiTetelekSearch searchKezbTetelekSzignUtaniak = new EREC_IraKezbesitesiTetelekSearch();
                    searchKezbTetelekSzignUtaniak.Obj_Id.Value = ugyiratId;
                    searchKezbTetelekSzignUtaniak.Obj_Id.Operator = Query.Operators.equals;

                    searchKezbTetelekSzignUtaniak.AtvetelDat.Value = Helper.GetFormattedDateOnlyString(szignalasEsemenyDat);
                    searchKezbTetelekSzignUtaniak.AtvetelDat.Operator = Query.Operators.greater;

                    searchKezbTetelekSzignUtaniak.OrderBy = " EREC_IraKezbesitesiTetelek.AtvetelDat";

                    var resultUgyKezbTetelekSzignUtaniak = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService().GetAllWithExtension(this.ExecParamCaller, searchKezbTetelekSzignUtaniak);
                    resultUgyKezbTetelekSzignUtaniak.CheckError();

                    #endregion

                    if (resultUgyKezbTetelekSzignUtaniak.Ds.Tables[0].Rows.Count > 0)
                    {
                        Atveve = Helper.GetFormattedDateAndTimeString(resultUgyKezbTetelekSzignUtaniak.Ds.Tables[0].Rows[0]["AtvetelDat"]);
                    }
                    // Ha magára szignálta:
                    else if (Szignalo == Atvevo)
                    {
                        Atveve = Szignalas;
                    }
                }
                else
                {
                    // Ha nincs szignálási esemény, hagyjuk meg a régi működést...

                    #region kézbesítési tételek lekérdezése

                    var kezbesitesiTetelekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                    var searchObjKezbTetelek = new EREC_IraKezbesitesiTetelekSearch();
                    searchObjKezbTetelek.Obj_Id.Value = ugyiratId.ToString();
                    searchObjKezbTetelek.Obj_Id.Operator = Query.Operators.equals;
                    searchObjKezbTetelek.Obj_type.Value = "EREC_UgyUgyiratok";
                    searchObjKezbTetelek.Obj_type.Operator = Query.Operators.equals;
                    searchObjKezbTetelek.OrderBy = " EREC_IraKezbesitesiTetelek.LetrehozasIdo DESC";
                    var resultUgyKezbTetelek = kezbesitesiTetelekService.GetAllWithExtension(ExecParamCaller, searchObjKezbTetelek);
                    resultUgyKezbTetelek.CheckError();

                    #endregion

                    if (resultUgyKezbTetelek.Ds.Tables[0].Rows.Count > 0)
                    {
                        // A legutolsó kézbesítési tétel kell (LetrehozasIdo DESC -re volt rendezve, így az első az utolsó):

                        DataRow rowKezbTetel = resultUgyKezbTetelek.Ds.Tables[0].Rows[0];

                        Szignalo = rowKezbTetel["Atado_Nev"].ToString();
                        Szignalas = Helper.GetFormattedDateAndTimeString(rowKezbTetel["LetrehozasIdo"]);
                        string atvevoStr = rowKezbTetel["Atvevo_Nev"].ToString();
                        string cimzettStr = rowKezbTetel["Cimzett_Nev"].ToString();
                        // Ha már átvették, akkor az kell az Atvevo-be, egyébként a Cimzett (ami lehet szervezet is)
                        Atvevo = (!String.IsNullOrEmpty(atvevoStr) ? atvevoStr : cimzettStr);
                        Atveve = Helper.GetFormattedDateAndTimeString(rowKezbTetel["AtvetelDat"]);
                    }
                    else
                    {
                        // Az ügyirat adatokból szedjük össze a szignálás adatokat:
                        // Ha ki van töltve az ügyiraton a Szignalo vagy SzignalasIdeje mező, akkor volt már szignálás -> kitöltjük ezeket a szignálós mezőket

                        string ugyiratSzignalo = ugyRow["Szignalo"].ToString();
                        string ugyiratSzignalasIdo = Helper.GetFormattedDateAndTimeString(ugyRow["SzignalasIdeje"]);

                        if (!String.IsNullOrEmpty(ugyiratSzignalo) || !String.IsNullOrEmpty(ugyiratSzignalasIdo))
                        {
                            Szignalo = ugyiratSzignalo;
                            Szignalas = ugyiratSzignalasIdo;

                            // Átvevő az vagy az ügyintéző lesz, vagy ha az még üres, akkor a felelős
                            Atvevo = ugyRow["Ugyintezo_Nev"].ToString();
                            if (String.IsNullOrEmpty(Atvevo))
                            {
                                // Ha már szignálták, de még nincs ügyintéző, akkor a felelős mezőben van elvileg az, akire szignálták:
                                Atvevo = ugyRow["Felelos_Nev"].ToString();
                            }

                            /// Átvétel dátumához le kell kérni a kézbesítési tételt:
                            /// Azt a kézbesítési tételt keressük, ahol az átadó a szignáló, az átvevő pedig az ügyintéző, és át lett véve
                            /// Ha a szignáló magára szignálta, akkor nem keletkezett kézbesítési tétel, ilyenkor a szignálás dátuma lesz az átvétel dátuma
                            /// 
                            Guid? szignaloId = ugyRow["SzignaloId"] as Guid?;
                            Guid? ugyintezoId = ugyRow["FelhasznaloCsoport_Id_Ugyintez"] as Guid?;
                            if (szignaloId != null && ugyintezoId != null)
                            {
                                // Ha magára szignálta:
                                if (szignaloId == ugyintezoId)
                                {
                                    Atveve = Szignalas;
                                }
                                else
                                {
                                    // Kézbesítési tétel lekérése:
                                    searchObjKezbTetelek = new EREC_IraKezbesitesiTetelekSearch();

                                    searchObjKezbTetelek.Obj_Id.Value = ugyiratId;
                                    searchObjKezbTetelek.Obj_Id.Operator = Query.Operators.equals;

                                    // Ide valamiért nem a szignáló kerül be, így erre inkább nem szűrünk...
                                    //searchObjKezbTetelek.Felhasznalo_Id_Atado_USER.Value = szignaloId.ToString();
                                    //searchObjKezbTetelek.Felhasznalo_Id_Atado_USER.Operator = Query.Operators.equals;

                                    searchObjKezbTetelek.Felhasznalo_Id_AtvevoUser.Value = ugyintezoId.ToString();
                                    searchObjKezbTetelek.Felhasznalo_Id_AtvevoUser.Operator = Query.Operators.equals;

                                    searchObjKezbTetelek.Allapot.Value = KodTarak.KEZBESITESITETEL_ALLAPOT.Atvett;
                                    searchObjKezbTetelek.Allapot.Operator = Query.Operators.equals;

                                    searchObjKezbTetelek.OrderBy = " EREC_IraKezbesitesiTetelek.LetrehozasIdo DESC";

                                    resultUgyKezbTetelek = kezbesitesiTetelekService.GetAllWithExtension(ExecParamCaller, searchObjKezbTetelek);
                                    resultUgyKezbTetelek.CheckError();

                                    // Ha van találat, akkor abból szedjük ki az átvétel dátumát:
                                    if (resultUgyKezbTetelek.Ds.Tables[0].Rows.Count > 0)
                                    {
                                        DataRow rowKezbTetel = resultUgyKezbTetelek.Ds.Tables[0].Rows[0];

                                        Atveve = Helper.GetFormattedDateAndTimeString(rowKezbTetel["AtvetelDat"]);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region IRAT
                if (iratRow != null)
                {
                    #region SZIGNALT

                    /// BUG#5691: Szignalo, Szignalas, Atvevo mezőket kézbesítési tételekből próbáljuk meg kinyerni:
                    /// - Ha az elsődleges iratpéldányhoz van kézbesítési tétel, akkor az alapján (az utolsó kézb. tétel alapján)
                    /// - Ha az elsődleges iratpéldányhoz nincsen kézb. tétel, akkor az ügyirat Szignalo, SzignalasIdeje mezőt nézzük 
                    ///            
                    /// BUG#7778: Most már van irat szignálás:
                    /// Ha az elsődleges iratpéldányhoz nincsen kézb. tétel, és az irat Szignált, akkor valószínűleg az iktatással került Szignált állapotba,
                    /// így az iktatót tesszük bele a Szignalo mezőbe...
                    /// 
                    /// BUG#7879 (Task#7933): KRT_Esemenyek táblából az utolsó "Irat szignálás" vagy "Automatikus irat szignálás" eseményt le kell kérni, abból jön a szignáló és a szignálás dátuma
                    /// 

                    #region Szignálás esemény(ek) lekérése

                    KRT_EsemenyekSearch esemenyekSearch = new KRT_EsemenyekSearch(true);
                    esemenyekSearch.Obj_Id.Value = iratId.ToString();
                    esemenyekSearch.Obj_Id.Operator = Query.Operators.equals;

                    esemenyekSearch.Extended_KRT_FunkciokSearch.Kod.Value = "'IratSzignalas', 'AutomatikusIratSzignalas'";
                    esemenyekSearch.Extended_KRT_FunkciokSearch.Kod.Operator = Query.Operators.inner;

                    // Rendezés: a legutolsó esemény legyen elől:
                    esemenyekSearch.OrderBy = " KRT_Esemenyek.LetrehozasIdo DESC ";

                    var resultSzignalasEsemenyek = eAdminService.ServiceFactory.GetKRT_EsemenyekService().GetAllWithExtension(this.ExecParamCaller, esemenyekSearch);
                    resultSzignalasEsemenyek.CheckError();

                    #endregion

                    DateTime? szignalasEsemenyDat = null;

                    if (resultSzignalasEsemenyek.Ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow rowSzignalasEsemeny = resultSzignalasEsemenyek.Ds.Tables[0].Rows[0];
                        // A szignáló és a szignálás dátuma az eseményből jön:
                        Szignalo = rowSzignalasEsemeny["Felhasznalo_Id_User_Nev"].ToString();

                        szignalasEsemenyDat = rowSzignalasEsemeny["LetrehozasIdo"] as DateTime?;
                        Szignalas = Helper.GetFormattedDateAndTimeString(szignalasEsemenyDat.Value);

                        Atvevo = iratRow["FelhasznaloCsoport_Id_Ugyintezo_Nev"].ToString();

                        /// Átvétel dátumának meghatározása:
                        /// Le kell kérni a szignálás esemény utáni első olyan kézbesítési tételt (akár az ügyiratra, akár az elsődleges iratpéldányra), ahol az átvétel már megtörtént. 
                        /// (Az átvétel dátuma legyen nagyobb, mint a szignálás dátuma, és ezek közül a legkisebb érték kell.)

                        #region Kézbesítési tételek lekérése

                        if (elsodlegesIratpeldanyId == null)
                        {
                            throw new ResultException("Nem található az elsődleges iratpéldány!");
                        }

                        EREC_IraKezbesitesiTetelekSearch searchKezbTetelekSzignUtaniak = new EREC_IraKezbesitesiTetelekSearch();
                        searchKezbTetelekSzignUtaniak.Obj_Id.Value = "'" + elsodlegesIratpeldanyId.ToString() + "', '" + ugyRow["Id"].ToString() + "'";
                        searchKezbTetelekSzignUtaniak.Obj_Id.Operator = Query.Operators.inner;

                        searchKezbTetelekSzignUtaniak.AtvetelDat.Value = Helper.GetFormattedDateOnlyString(szignalasEsemenyDat);
                        searchKezbTetelekSzignUtaniak.AtvetelDat.Operator = Query.Operators.greater;

                        searchKezbTetelekSzignUtaniak.OrderBy = " EREC_IraKezbesitesiTetelek.AtvetelDat";

                        var resultKezbTetelekSzignUtaniak = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService().GetAllWithExtension(this.ExecParamCaller, searchKezbTetelekSzignUtaniak);
                        resultKezbTetelekSzignUtaniak.CheckError();

                        #endregion

                        if (resultKezbTetelekSzignUtaniak.Ds.Tables[0].Rows.Count > 0)
                        {
                            Atveve = Helper.GetFormattedDateAndTimeString(resultKezbTetelekSzignUtaniak.Ds.Tables[0].Rows[0]["AtvetelDat"]);
                        }
                        // Ha magára szignálta:
                        else if (Szignalo == Atvevo)
                        {
                            Atveve = Szignalas;
                        }
                    }
                    else
                    {
                        // Ha nincs szignálási esemény, hagyjuk meg a régi működést...

                        #region Iratpéldány kézbesítési tételek lekérdezése

                        var kezbesitesiTetelekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();

                        var searchObjKezbTetelek = new EREC_IraKezbesitesiTetelekSearch();

                        // Szűrés az elsődleges iratpéldányra      

                        if (elsodlegesIratpeldanyId == null)
                        {
                            throw new ResultException("Nem található az elsődleges iratpéldány!");
                        }

                        searchObjKezbTetelek.Obj_Id.Value = elsodlegesIratpeldanyId.ToString();
                        searchObjKezbTetelek.Obj_Id.Operator = Query.Operators.equals;

                        searchObjKezbTetelek.Obj_type.Value = "EREC_PldIratPeldanyok";
                        searchObjKezbTetelek.Obj_type.Operator = Query.Operators.equals;

                        //searchObjKezbTetelek.Allapot.Value = "'" + KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott + "','" + KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott + "'";
                        //searchObjKezbTetelek.Allapot.Operator = Query.Operators.inner;

                        searchObjKezbTetelek.OrderBy = " EREC_IraKezbesitesiTetelek.LetrehozasIdo DESC";

                        var resultKezbTetelek = kezbesitesiTetelekService.GetAllWithExtension(ExecParamCaller, searchObjKezbTetelek);
                        resultKezbTetelek.CheckError();

                        #endregion

                        if (resultKezbTetelek.Ds.Tables[0].Rows.Count > 0)
                        {
                            // A legutolsó kézbesítési tétel kell (LetrehozasIdo DESC -re volt rendezve, így az első az utolsó):

                            DataRow rowKezbTetel = resultKezbTetelek.Ds.Tables[0].Rows[0];

                            Szignalo = rowKezbTetel["Atado_Nev"].ToString();
                            Szignalas = Helper.GetFormattedDateAndTimeString(rowKezbTetel["LetrehozasIdo"]);
                            string atvevoStr = rowKezbTetel["Atvevo_Nev"].ToString();
                            string cimzettStr = rowKezbTetel["Cimzett_Nev"].ToString();
                            // Ha már átvették, akkor az kell az Atvevo-be, egyébként a Cimzett (ami lehet szervezet is)
                            Atvevo = (!String.IsNullOrEmpty(atvevoStr) ? atvevoStr : cimzettStr);
                            Atveve = Helper.GetFormattedDateAndTimeString(rowKezbTetel["AtvetelDat"]);
                        }
                        // Ha az irat szignált, az ügyirat még nem volt szignálva, (és nem volt kézbesítési tétel az iratpéldányra), akkor valószínűleg iktatáskor került Szignált állapotba
                        else if (iratRow["Allapot"].ToString() == KodTarak.IRAT_ALLAPOT.Szignalt
                                && String.IsNullOrEmpty(ugyRow["Szignalo"].ToString()))
                        {
                            // Az irat iktatáskor került valószínűleg Szignált állapotba, így az iktatót tesszük bele
                            Szignalo = iratRow["FelhasznaloCsoport_Id_Iktato_Nev"].ToString();
                            Szignalas = Helper.GetFormattedDateAndTimeString(iratRow["IktatasDatuma"]);
                            Atvevo = iratRow["FelhasznaloCsoport_Id_Ugyintezo_Nev"].ToString();
                            Atveve = Szignalas;
                        }
                        else
                        {
                            // Az ügyirat adatokból szedjük össze a szignálás adatokat:

                            // Ha ki van töltve az ügyiraton a Szignalo vagy SzignalasIdeje mező, akkor volt már szignálás -> kitöltjük ezeket a szignálós mezőket

                            string ugyiratSzignalo = ugyRow["Szignalo"].ToString();
                            string ugyiratSzignalasIdo = Helper.GetFormattedDateAndTimeString(ugyRow["SzignalasIdeje"]);

                            if (!String.IsNullOrEmpty(ugyiratSzignalo) || !String.IsNullOrEmpty(ugyiratSzignalasIdo))
                            {
                                Szignalo = ugyiratSzignalo;
                                Szignalas = ugyiratSzignalasIdo;

                                // Átvevő az vagy az ügyintéző lesz, vagy ha az még üres, akkor a felelős
                                Atvevo = ugyRow["Ugyintezo_Nev"].ToString();
                                if (String.IsNullOrEmpty(Atvevo))
                                {
                                    // Ha már szignálták, de még nincs ügyintéző, akkor a felelős mezőben van elvileg az, akire szignálták:
                                    Atvevo = ugyRow["Felelos_Nev"].ToString();
                                }

                                /// Átvétel dátumához le kell kérni a kézbesítési tételt:
                                /// Azt a kézbesítési tételt keressük, ahol az átadó a szignáló, az átvevő pedig az ügyintéző, és át lett véve
                                /// Ha a szignáló magára szignálta, akkor nem keletkezett kézbesítési tétel, ilyenkor a szignálás dátuma lesz az átvétel dátuma
                                /// 
                                Guid? szignaloId = ugyRow["SzignaloId"] as Guid?;
                                Guid? ugyintezoId = ugyRow["FelhasznaloCsoport_Id_Ugyintez"] as Guid?;
                                if (szignaloId != null && ugyintezoId != null)
                                {
                                    // Ha magára szignálta:
                                    if (szignaloId == ugyintezoId)
                                    {
                                        Atveve = Szignalas;
                                    }
                                    else
                                    {
                                        // Kézbesítési tétel lekérése:
                                        searchObjKezbTetelek = new EREC_IraKezbesitesiTetelekSearch();

                                        searchObjKezbTetelek.Obj_Id.Value = ugyRow["Id"].ToString();
                                        searchObjKezbTetelek.Obj_Id.Operator = Query.Operators.equals;

                                        // Ide valamiért nem a szignáló kerül be, így erre inkább nem szűrünk...
                                        //searchObjKezbTetelek.Felhasznalo_Id_Atado_USER.Value = szignaloId.ToString();
                                        //searchObjKezbTetelek.Felhasznalo_Id_Atado_USER.Operator = Query.Operators.equals;

                                        searchObjKezbTetelek.Felhasznalo_Id_AtvevoUser.Value = ugyintezoId.ToString();
                                        searchObjKezbTetelek.Felhasznalo_Id_AtvevoUser.Operator = Query.Operators.equals;

                                        searchObjKezbTetelek.Allapot.Value = KodTarak.KEZBESITESITETEL_ALLAPOT.Atvett;
                                        searchObjKezbTetelek.Allapot.Operator = Query.Operators.equals;

                                        searchObjKezbTetelek.OrderBy = " EREC_IraKezbesitesiTetelek.LetrehozasIdo DESC";

                                        resultKezbTetelek = kezbesitesiTetelekService.GetAllWithExtension(ExecParamCaller, searchObjKezbTetelek);
                                        resultKezbTetelek.CheckError();

                                        // Ha van találat, akkor abból szedjük ki az átvétel dátumát:
                                        if (resultKezbTetelek.Ds.Tables[0].Rows.Count > 0)
                                        {
                                            DataRow rowKezbTetel = resultKezbTetelek.Ds.Tables[0].Rows[0];

                                            Atveve = Helper.GetFormattedDateAndTimeString(rowKezbTetel["AtvetelDat"]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
                #endregion IRAT
            }

            #region ELINTEZETT
            if (!string.IsNullOrEmpty(ugyRow["Allapot"].ToString()) && ugyRow["Allapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Elintezett)
            {
                string ujCsoport = ugyRow["Csoport_Id_Felelos"].ToString();//Csoport_Id_Felelos_Elozo
                if (!string.IsNullOrEmpty(ujCsoport))
                {
                    DataRow drCsoportUj = this.GetCsoportRowById(ujCsoport);
                    if (drCsoportUj != null)
                    {
                        Keszrejelentette = drCsoportUj["Nev"].ToString();
                    }
                }
                Keszrejelentve = Helper.GetFormattedDateAndTimeString(ugyRow["ElintezesDat"]);
            }
            #endregion

            #endregion UGY ADATOK

            Logger.InfoEnd("SZURManager.SZURGetIratAdatokWS");
        }

        /// <summary>
        /// Hatósági ügy statisztikai űrlapja adatainak előzetes lekérdezése
        /// "Az ügy készre jelentése előtt le lehet kérdezni a SZÜR által meghatározott statisztikai űrlap adatait, 
        /// melyet a szakrendszerben az ügyintéző módosíthat, majd a főszám készre jelentésekor véglegesíthet."
        /// </summary>
        /// <param name="Foszam"></param>
        /// <param name="Ev"></param>
        /// <param name="User"></param>
        /// <param name="statElsofok"></param>
        /// <param name="statJogorv"></param>
        /// <param name="statElsofokNap"></param>
        /// <param name="statElsoJogNap"></param>
        /// <param name="statMasodNap"></param>
        /// <param name="statAtlag"></param>
        /// <param name="statOsszesDb"></param>
        /// <param name="statKarigeny"></param>
        /// <param name="statFelugyelet"></param>
        public void SZURgetFoszamstatisztikaWS(string Foszam, string Ev, string User
            , out string statElsofok, out string statJogorv, out string statElsofokNap, out string statElsoJogNap, out string statMasodNap, out string statAtlag, out string statOsszesDb, out string statKarigeny, out string statFelugyelet)
        {
            Logger.InfoStart("SZURManager.SZURgetFoszamstatisztikaWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(User);

            // Ügyirat lekérdezése:
            DataRow ugyiratRow = this.GetUgyiratRow(Ev, Utility.KulsoIktatoKonyv, Foszam);
            Guid ugyiratId = (Guid)ugyiratRow["Id"];

            // Ügyirat tárgyszóértékeinek lekérése: (BUG#5909)
            Dictionary<string, string> targyszoErtekek = this.GetObjektumTargyszoErtekek(ugyiratId, eUtility.Constants.TableNames.EREC_UgyUgyiratok);

            statElsofok = targyszoErtekek.ContainsKey(TargyszoAzon_statElsofok) ? targyszoErtekek[TargyszoAzon_statElsofok] : null;
            statJogorv = targyszoErtekek.ContainsKey(TargyszoAzon_statJogorv) ? targyszoErtekek[TargyszoAzon_statJogorv] : null;
            statElsofokNap = targyszoErtekek.ContainsKey(TargyszoAzon_statElsofokNap) ? targyszoErtekek[TargyszoAzon_statElsofokNap] : null;
            statElsoJogNap = targyszoErtekek.ContainsKey(TargyszoAzon_statElsoJogNap) ? targyszoErtekek[TargyszoAzon_statElsoJogNap] : null;
            statMasodNap = targyszoErtekek.ContainsKey(TargyszoAzon_statMasodNap) ? targyszoErtekek[TargyszoAzon_statMasodNap] : null;
            statAtlag = targyszoErtekek.ContainsKey(TargyszoAzon_statAtlag) ? targyszoErtekek[TargyszoAzon_statAtlag] : null;
            statOsszesDb = targyszoErtekek.ContainsKey(TargyszoAzon_statOsszesDb) ? targyszoErtekek[TargyszoAzon_statOsszesDb] : null;
            statKarigeny = targyszoErtekek.ContainsKey(TargyszoAzon_statKarigeny) ? targyszoErtekek[TargyszoAzon_statKarigeny] : null;
            statFelugyelet = targyszoErtekek.ContainsKey(TargyszoAzon_statFelugyelet) ? targyszoErtekek[TargyszoAzon_statFelugyelet] : null;

            Logger.InfoEnd("SZURManager.SZURgetFoszamstatisztikaWS");
        }

        #endregion

        #region Iktatás

        /// <summary>
        /// Új főszám létrehozása az aktuális évi iktatókönyvben
        /// </summary>
        /// <param name="IktatokonyvKod"></param>
        /// <param name="FCim"></param>
        /// <param name="FTargyszo"></param>
        /// <param name="FelelosUserID"></param>
        /// <param name="UserId"></param>
        /// <param name="Ugyiratjelleg"></param>
        /// <param name="Ugyjellegkod"></param>
        /// <param name="UI_Hatarido"></param>
        /// <param name="UI_HiMod"></param>
        /// <param name="UI_Munkanap"></param>
        /// <param name="Foszam"></param>
        /// <param name="Eloszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="objTargyszoHiba"></param>
        public void SZURGetFoszamWS(string IktatokonyvKod, string FCim, string FTargyszo, string FelelosUserID, string UserId, string Ugyiratjelleg, string Ugyjellegkod, string UI_Hatarido, string UI_HiMod, string UI_Munkanap
            , out string Foszam, out string Eloszam, out string Evszam, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURManager.SZURGetFoszamWS");

            #region OUT
            Foszam = String.Empty;
            Eloszam = String.Empty;
            Evszam = String.Empty;
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            #endregion
            // BLG_2502
            //try
            //{

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            #region Szükséges adatok lekérése

            // Iktatókönyv lekérése (Id-hoz)
            DataRow iktatokonyvRow = this.GetIktatokonyvRow(IktatokonyvKod, DateTime.Today.Year);
            Guid iktatokonyvId = (Guid)iktatokonyvRow["Id"];

            // Felhasználó Id-hoz felhasználó sorok lekérése:
            DataRow felelosUserRow = this.GetFelhasznaloRowByUserNev(FelelosUserID);
            DataRow userRow = this.GetFelhasznaloRowByUserNev(UserId);

            #endregion

            #region Iktatáshoz szükséges objektumok feltöltése

            EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();
            // összes mező update-elhetőségét kezdetben letiltani:
            erec_UgyUgyiratok.Updated.SetValueAll(false);
            erec_UgyUgyiratok.Base.Updated.SetValueAll(false);

            erec_UgyUgyiratok.Targy = FCim;
            erec_UgyUgyiratok.Updated.Targy = true;

            erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = felelosUserRow["Id"].ToString();
            erec_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

            erec_UgyUgyiratok.Csoport_Id_Felelos = felelosUserRow["Id"].ToString();
            erec_UgyUgyiratok.Updated.Csoport_Id_Felelos = true;

            erec_UgyUgyiratok.Jelleg = KodTarak.UGYIRAT_JELLEG.Elektronikus;
            erec_UgyUgyiratok.Updated.Jelleg = true;


            #region Ügy fajtája (Ugyiratjelleg)

            KRT_KodTarakService svc_kodtar = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            KRT_KodTarakSearch src_kodtarak = new KRT_KodTarakSearch();

            /// BUG#4050/Task#4098:
            /// "  '(15) nem hatósági ügyek</Ugyiratjelleg>'
            /// Az ügyjelleg kódot az ügyjelleg szövegben nem kívánjuk átadni.Jelenleg csak ezzel együtt működik."
            /// --> Ha a kódtár névbe bele van fűzve a kód is, akkor a kód nélküli változatot is el kellene fogadnunk
            /// --> Tartalmazásra szűrünk, és utána a találat(ok)at megvizsgáljuk, melyik a megfelelő
            /// 
            src_kodtarak.Nev.Value = "%" + Ugyiratjelleg;
            src_kodtarak.Nev.Operator = Query.Operators.like;

            src_kodtarak.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'UGY_FAJTAJA' ");

            Result res = svc_kodtar.GetAllWithKodcsoport(execParamUserId, src_kodtarak);
            res.CheckError();

            DataRow ktRowUgyiratJelleg = null;
            foreach (DataRow kodtarRow in res.Ds.Tables[0].Rows)
            {
                string ktNev = kodtarRow["Nev"].ToString();

                // A megadott UgyiratJelleg előtt lehet a kód zárójelben:
                if (ktNev.EndsWith(Ugyiratjelleg, StringComparison.CurrentCultureIgnoreCase)
                    && ktNev.IndexOf(Ugyiratjelleg, StringComparison.CurrentCultureIgnoreCase) <= 5 // legalább az 5. karaktertől kezdődjön a keresett szöveg
                    && (ktNev.StartsWith("(") || ktNev.Equals(Ugyiratjelleg, StringComparison.CurrentCultureIgnoreCase)) // Vagy zárójellel kezdődjön, vagy teljes egyezés legyen
                    )
                {
                    // Megvan a kódtár sor:
                    if (ktRowUgyiratJelleg == null
                        || ktRowUgyiratJelleg["Nev"].ToString().IndexOf(Ugyiratjelleg, StringComparison.CurrentCultureIgnoreCase)
                            > ktNev.IndexOf(Ugyiratjelleg, StringComparison.CurrentCultureIgnoreCase))
                    {
                        ktRowUgyiratJelleg = kodtarRow;
                    }
                }
            }

            if (ktRowUgyiratJelleg == null)
            {
                throw new Exception("Az Ügyirat jellegét (Ügy fajtája) nem sikerült azonosítani! ('" + Ugyiratjelleg + "')");
            }

            string kod = ktRowUgyiratJelleg["Kod"].ToString();
            string egyeb = ktRowUgyiratJelleg["Egyeb"].ToString();

            // BUG#3493/Task#3509: Nem hatósági ügy esetén fixen a "nem hatósági" kódot kapja meg:
            if (egyeb == KodTarak.UGY_JELLEG_FAJTAJA_HATOSAGI.NEM_HATOSAGI)
            {
                kod = KodTarak.UGY_FAJTAJA.Nem_Hatosagi_Ugy;
            }

            erec_UgyUgyiratok.Ugy_Fajtaja = kod;  //KodTarak.UGY_FAJTAJA.
            erec_UgyUgyiratok.Updated.Ugy_Fajtaja = true;

            #endregion

            #region Határidő beállítása/számítása

            int munkanapInt;
            bool munkanapIntOk = false;

            IdoEgyseg findIdoEgyseg = GetIdoEgseg(UI_HiMod);
            if (!string.IsNullOrEmpty(UI_HiMod))
            {
                if (findIdoEgyseg == IdoEgyseg.Munkanap)
                {
                    erec_UgyUgyiratok.IntezesiIdoegyseg = KodTarak.IDOEGYSEG.Munkanap;
                    erec_UgyUgyiratok.Updated.IntezesiIdoegyseg = true;
                }
                else if (findIdoEgyseg == IdoEgyseg.Nap)
                {
                    erec_UgyUgyiratok.IntezesiIdoegyseg = KodTarak.IDOEGYSEG.Nap;
                    erec_UgyUgyiratok.Updated.IntezesiIdoegyseg = true;
                }
                else
                {
                    erec_UgyUgyiratok.IntezesiIdoegyseg = KodTarak.IDOEGYSEG.Nap;
                    erec_UgyUgyiratok.Updated.IntezesiIdoegyseg = true;
                }
            }
            else
            {
                erec_UgyUgyiratok.IntezesiIdoegyseg = KodTarak.IDOEGYSEG.Nap;
                erec_UgyUgyiratok.Updated.IntezesiIdoegyseg = true;
            }
            munkanapIntOk = int.TryParse(UI_Munkanap, out munkanapInt);

            // Ha az UI_Hatarido van megadva, akkor az lesz a határidő,
            // ha az nincs, és az UI_HiMod és UI_Munkanap, akkor számoljuk a határidőt
            if (!string.IsNullOrEmpty(UI_Hatarido))
            {
                erec_UgyUgyiratok.Hatarido = UI_Hatarido;
                erec_UgyUgyiratok.Updated.Hatarido = true;
            }
            else if (!string.IsNullOrEmpty(UI_HiMod) && !string.IsNullOrEmpty(UI_Munkanap) && munkanapIntOk)
            {
                DateTime? hataridoDat = null;
                if (findIdoEgyseg == IdoEgyseg.Munkanap)
                {
                    hataridoDat = this.GetKovetkezoMunkanap(DateTime.Today, munkanapInt);
                }
                else
                {
                    hataridoDat = DateTime.Today.AddDays(munkanapInt);
                }

                if (hataridoDat.HasValue)
                {
                    erec_UgyUgyiratok.Hatarido = Helper.GetFormattedDateAndTimeString(hataridoDat.Value);
                    erec_UgyUgyiratok.Updated.Hatarido = true;
                }
            }

            if (!string.IsNullOrEmpty(UI_Munkanap))
            {
                erec_UgyUgyiratok.IntezesiIdo = UI_Munkanap;
                erec_UgyUgyiratok.Updated.IntezesiIdo = true;
            }

            #endregion

            #region Irat adatok
            EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
            // összes mező update-elhetőségét kezdetben letiltani:
            erec_IraIratok.Updated.SetValueAll(false);
            erec_IraIratok.Base.Updated.SetValueAll(false);

            erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez = userRow["Id"].ToString();
            erec_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

            // Tárgy:
            erec_IraIratok.Targy = FCim;
            erec_IraIratok.Updated.Targy = true;

            // Ügyintézés alapja
            erec_IraIratok.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_IraIratok.Updated.AdathordozoTipusa = true;

            // BUG_8369: elsődleges adathordozó legyen NULL
            //erec_IraIratok.UgyintezesAlapja = KodTarak.ELSODLEGES_ADATHORDOZO.Elektronikus_irat;
            //erec_IraIratok.Updated.UgyintezesAlapja = true;

            erec_IraIratok.Jelleg = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_IraIratok.Updated.Jelleg = true;

            erec_IraIratok.Hatarido = erec_UgyUgyiratok.Hatarido;
            erec_IraIratok.Updated.Hatarido = true;

            erec_IraIratok.IntezesiIdo = erec_UgyUgyiratok.IntezesiIdo;
            erec_IraIratok.Updated.IntezesiIdo = true;

            erec_IraIratok.IntezesiIdoegyseg = erec_UgyUgyiratok.IntezesiIdoegyseg;
            erec_IraIratok.Updated.IntezesiIdoegyseg = true;

            erec_IraIratok.Base.Note = Note_Technikai;
            erec_IraIratok.Base.Updated.Note = true;

            #endregion

            #region Iratpéldány adatok
            EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
            // összes mező update-elhetőségét kezdetben letiltani:
            erec_PldIratPeldanyok.Updated.SetValueAll(false);
            erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);
            //try
            //{
            KRT_PartnerekService service_partnerek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            Result result_partner = service_partnerek.GetByCsoportId(execParamUserId, userRow["Partner_id"].ToString());
            if (result_partner.IsError)
            {
                Logger.Error("Címzett nem állítható be (partnerid): " + userRow["Partner_id"].ToString());
                throw new ResultException("Címzett nem állítható be (partnerid): " + userRow["Partner_id"].ToString());
            }

            KRT_Partnerek partner_cimzett = (KRT_Partnerek)result_partner.Record;

            erec_PldIratPeldanyok.Partner_Id_Cimzett = userRow["Partner_id"].ToString();
            erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;

            erec_PldIratPeldanyok.NevSTR_Cimzett = partner_cimzett.Nev;
            erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;
            //}
            //catch (ResultException exc)
            //{
            //    Logger.Error(exc.ToString());

            //    Logger.Error("Címzett nem állítható be (partnerid): " + userRow["Partner_id"].ToString());

            //    // Nem dobjuk tovább a hibát, hanem beállítjuk a Hiba out paraméterben, hogy volt hiba

            //    Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

            //}

            erec_PldIratPeldanyok.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_PldIratPeldanyok.Updated.UgyintezesModja = true;

            #endregion

            IktatasiParameterek ikt_par = new IktatasiParameterek();
            ikt_par.MunkaPeldany = true;
            //        ikt_par.UgyiratUjHatarido = erec_UgyUgyiratok.Hatarido;
            ikt_par.UgyiratPeldanySzukseges = false;

            // BUG_6117
            ikt_par.IsSZUR = true;
            #endregion

            /// BUG#3493/Task#3509: Továbbítás alatti állapot helyett ügyintézés alatti állapot kell
            /// --> ha a felelős más, mint az iktató, ilyenkor történik egy átadásra kijelölés, ezért van a továbbítás alatti állapot
            /// --> Indítunk egy ügyintézésre átvételt a felelősre, így lesz ügyintézés alatti az ügyirat.
            /// 
            // Iktatás webservice hívása:
            var resultIktatas = eRecordService.ServiceFactory.GetEREC_IraIratokService()
                    .BelsoIratIktatasaUgyintezesreAtvetellel(execParamUserId, iktatokonyvId.ToString(), erec_UgyUgyiratok, new EREC_UgyUgyiratdarabok()
                                , erec_IraIratok, new EREC_HataridosFeladatok(), erec_PldIratPeldanyok
                                , ikt_par);
            resultIktatas.CheckError();

            ErkeztetesIktatasResult iktatasResult = (ErkeztetesIktatasResult)resultIktatas.Record;
            string ujUgyiratId = iktatasResult.UgyiratId;

            #region out paraméterek beállítása

            // Új ügyirat objektum lekérése:

            DataRow ujUgyiratRow = this.GetUgyiratRow(new Guid(ujUgyiratId));

            Foszam = ujUgyiratRow["Foszam"].ToString();
            Eloszam = ujUgyiratRow["UgyFelelos_SzervezetKod"] == null ? String.Empty : ujUgyiratRow["UgyFelelos_SzervezetKod"].ToString();
            Evszam = iktatokonyvRow["Ev"].ToString();

            //objTargyszoHiba = null;

            #endregion

            #region Obj. tárgyszó beállítása

            if (!String.IsNullOrEmpty(FTargyszo))
            {
                Logger.Debug("Iktatás ok, obj. tárgyszó beállítás Start...");

                /// Tárgyszó érték mentése az objektumhoz:
                /// Iktatás után hívható csak, nincs egy tranzakcióba összevonva az iktatással. 
                /// (Az eRecordWebSite-on is így van, az iktatás után külön hívással megy. Ha ott jó így, itt is jó lesz...)
                /// 
                try
                {
                    // BUG_8369
                    bool found = false;

                    KRT_KodTarakSearch src_targyszo = new KRT_KodTarakSearch();
                    src_targyszo.Nev.Filter(FTargyszo);
                    src_targyszo.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'SZUR_TARGYSZOK' ");

                    res = svc_kodtar.GetAllWithKodcsoport(execParamUserId, src_targyszo);
                    res.CheckError();
                    if (!res.IsError)
                    {
                        foreach (DataRow row in res.Ds.Tables[0].Rows)
                        {
                            found = row["Nev"].ToString() == FTargyszo;
                            if (found)
                            {
                                break;
                            }
                        }
                    }

                    if (found)
                    {
                        this.SetObjektumTargyszo(new Guid(ujUgyiratId), eUtility.Constants.TableNames.EREC_UgyUgyiratok, TargyszoAzon_TARGYSZOK, FTargyszo, execParamUserId);
                    }
                    else
                    {
                        Utility.SetHibakodHibaszoveg(new Exception("Nincs ilyen tárgyszó: " + FTargyszo), out Hibakod, out Hibaszoveg);
                    }
                }
                catch (Exception exc)
                {
                    Logger.Error(exc.ToString());

                    // Nem dobjuk tovább a hibát, hanem beállítjuk az objTargyszoHiba out paraméterben, hogy volt hiba a tárgyszó mentésnél
                    // string hibaKodTargyszo;
                    Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
                }
            }
            #endregion
            // BLG_2502
            //}
            //catch (Exception exc)
            //{
            //    Logger.Error(exc.ToString(), exc);
            //    Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            //}
            Logger.InfoEnd("SZURManager.SZURGetFoszamWS");
        }
        /// <summary>
        /// GetIdoEgseg
        /// </summary>
        /// <param name="UI_HiMod"></param>
        /// <returns></returns>
        private IdoEgyseg GetIdoEgseg(string UI_HiMod)
        {
            IdoEgyseg idoEgyseg = IdoEgyseg.Nap;

            if (string.IsNullOrEmpty(UI_HiMod))
                return IdoEgyseg.Nap;

            if (UI_HiMod.Equals("munkanap") || UI_HiMod == KodTarak.IDOEGYSEG.Munkanap)
            {
                return IdoEgyseg.Munkanap;
            }
            else if (UI_HiMod.Equals("naptári nap") || UI_HiMod.Equals("nap") || UI_HiMod == KodTarak.IDOEGYSEG.Nap)
            {
                return IdoEgyseg.Nap;
            }

            return idoEgyseg;
        }
        public void SZURStartKorozWS(string IratCim, Kategoriak Kategoriak, string IratTipus, Szervezetek Tajekoztat, Szervezetek Velemeny, string Form, string KoroztetoUserID, string Megjegyzes, string UserId,
            out string Evszam, out string Foszam, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURManager.SZURStartKorozWS");

            #region OUT
            Foszam = string.Empty;
            Evszam = string.Empty;
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = string.Empty;
            #endregion


            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            #region Szükséges adatok lekérése

            // Felhasználó Id-hoz felhasználó sorok lekérése:
            // TODO
            //DataRow felelosUserRow = this.GetFelhasznaloRowByUserNev(FelelosUserID);
            DataRow userRow = this.GetFelhasznaloRowByUserNev(UserId);

            // Iktatókönyv lekérése (Id-hoz)
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();

            EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
            Search.IktatoErkezteto.Value = "I";
            Search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;


            Search.Ev.Value = DateTime.Today.Year.ToString();
            Search.Ev.Operator = Query.Operators.equals;

            Search.LezarasDatuma.Value = "";
            Search.LezarasDatuma.Operator = Query.Operators.isnull;

            Search.TopRow = 1;

            Result result = service.GetAllWithIktathat(execParamUserId, Search);

            if (!string.IsNullOrEmpty(result.ErrorCode) || (result.Ds.Tables[0].Rows.Count == 0))
            {
                throw new ResultException("", "Hiba az iktatókönyv megállapításnál!");
            }

            DataRow iktatokonyvRow = result.Ds.Tables[0].Rows[0];
            string iktatokonyvId = iktatokonyvRow["Id"].ToString();

            #endregion

            #region Iktatáshoz szükséges objektumok feltöltése

            EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();
            // összes mező update-elhetőségét kezdetben letiltani:
            erec_UgyUgyiratok.Updated.SetValueAll(false);
            erec_UgyUgyiratok.Base.Updated.SetValueAll(false);

            erec_UgyUgyiratok.Targy = IratCim;
            erec_UgyUgyiratok.Updated.Targy = true;

            // TODO
            //erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = felelosUserRow["Id"].ToString();
            //erec_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

            // TODO
            erec_UgyUgyiratok.Csoport_Id_Felelos = userRow["Id"].ToString();
            erec_UgyUgyiratok.Updated.Csoport_Id_Felelos = true;

            // TODO
            erec_UgyUgyiratok.Jelleg = KodTarak.UGYIRAT_JELLEG.Elektronikus;
            erec_UgyUgyiratok.Updated.Jelleg = true;


            #region Ügy fajtája (Ugyiratjelleg)
            //try
            //{
            //    KRT_KodTarakService svc_kodtar = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            //    KRT_KodTarakSearch src_kodtarak = new KRT_KodTarakSearch();
            //    src_kodtarak.Nev.Value = Ugyiratjelleg;
            //    src_kodtarak.Nev.Operator = Query.Operators.equals;
            //    src_kodtarak.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'UGY_FAJTAJA' ");

            //    Result res = svc_kodtar.GetAllWithKodcsoport(execParamUserId, src_kodtarak);

            //    if (res.IsError || res.Ds == null || res.Ds.Tables[0].Rows.Count != 1)
            //    {

            //        throw new ResultException(res);
            //    }

            //    string kod = res.Ds.Tables[0].Rows[0]["Kod"].ToString();

            //    erec_UgyUgyiratok.Ugy_Fajtaja = kod;  //KodTarak.UGY_FAJTAJA.
            //    erec_UgyUgyiratok.Updated.Ugy_Fajtaja = true;

            //}
            //catch (ResultException exc)
            //{
            //    // Nem dobjuk tovább a hibát, hanem beállítjuk a Hiba out paraméterben, hogy volt hiba
            //    Logger.Error(exc.ToString());

            //    Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            //    if (String.IsNullOrEmpty(Hibakod))
            //    {
            //        int rowCount = 0;
            //        Result res = exc.GetResult();
            //        if (res != null || res.Ds != null)
            //        {
            //            rowCount = res.Ds.Tables[0].Rows.Count;
            //            if (rowCount != 1)
            //            {
            //                Hibakod = Utility.ErrorCodes.GeneralErrorCode;
            //                Hibaszoveg = "Az Ügyirat jellegét (Ügy fajtája) nem sikerült azonosítani! Visszakapott sorok száma:" + rowCount.ToString();
            //            }
            //        }
            //        Logger.Error("Az Ügyirat jellegét (Ügy fajtája) nem sikerült azonosítani! Hiba:" + res.ErrorMessage + "(" + res.ErrorCode + "); Visszakapott sorok:" + rowCount.ToString());

            //    }
            //}
            #endregion

            #region Határidő beállítása/számítása

            //int munkanapInt;

            //string idoegysegkod = KodTarak.IDOEGYSEG.Nap;

            //// Ha az UI_Hatarido van megadva, akkor az lesz a határidő,
            //// ha az nincs, és az UI_HiMod és UI_Munkanap, akkor számoljuk a határidőt
            //if (!String.IsNullOrEmpty(UI_Hatarido))
            //{
            //    erec_UgyUgyiratok.Hatarido = UI_Hatarido;
            //    erec_UgyUgyiratok.Updated.Hatarido = true;
            //}
            //else if (!String.IsNullOrEmpty(UI_HiMod) && !String.IsNullOrEmpty(UI_Munkanap)
            //        && int.TryParse(UI_Munkanap, out munkanapInt))
            //{
            //    DateTime hataridoDat;

            //    if (UI_HiMod == "munkanap")
            //    {
            //        hataridoDat = this.GetKovetkezoMunkanap(DateTime.Today, munkanapInt);
            //        idoegysegkod = KodTarak.IDOEGYSEG.Munkanap;
            //    }
            //    else if (UI_HiMod == "naptári nap")
            //    {
            //        // Naptári nap:
            //        hataridoDat = DateTime.Today.AddDays(munkanapInt);
            //        idoegysegkod = KodTarak.IDOEGYSEG.Nap;
            //    }
            //    else
            //    {
            //        // Hiba:
            //        throw new ResultException("Az UI_HiMod mező lehetséges értékei: 'naptári nap' vagy 'munkanap'");
            //    }

            //    erec_UgyUgyiratok.Hatarido = hataridoDat.ToString();
            //    erec_UgyUgyiratok.Updated.Hatarido = true;

            //    erec_UgyUgyiratok.IntezesiIdo = UI_Munkanap;
            //    erec_UgyUgyiratok.Updated.IntezesiIdo = true;

            //    erec_UgyUgyiratok.IntezesiIdoegyseg = idoegysegkod;
            //    erec_UgyUgyiratok.Updated.IntezesiIdoegyseg = true;

            //}

            #endregion

            #region Irat adatok
            EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
            erec_IraIratok.Id = Guid.NewGuid().ToString();
            erec_IraIratok.Updated.Id = true;

            // összes mező update-elhetőségét kezdetben letiltani:
            erec_IraIratok.Updated.SetValueAll(false);
            erec_IraIratok.Base.Updated.SetValueAll(false);

            erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez = userRow["Id"].ToString();
            erec_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

            // Tárgy:
            erec_IraIratok.Targy = IratCim;
            erec_IraIratok.Updated.Targy = true;

            if (string.IsNullOrEmpty(IratTipus))
                throw new ResultException("Nincs megadva Irattipus!");

            string irattipusKod = KodTar_Cache.GetKodtarakByKodCsoport(KodTarak.IRATTIPUS.KodcsoportKod, this.ExecParamCaller
            , HttpContext.Current.Cache).FirstOrDefault(x => x.Value == IratTipus).Key;
            // BLG_2502
            if (string.IsNullOrEmpty(irattipusKod))
                throw new ResultException("Irattipus nem található!");
            erec_IraIratok.Irattipus = irattipusKod;
            erec_IraIratok.Updated.Irattipus = true;

            DataRow kiadmanyUserRow = this.GetFelhasznaloRowByUserNev(KoroztetoUserID);

            erec_IraIratok.FelhasznaloCsoport_Id_Kiadmany = kiadmanyUserRow["Id"].ToString();
            erec_IraIratok.Updated.FelhasznaloCsoport_Id_Kiadmany = true;

            erec_IraIratok.KiadmanyozniKell = "1";
            erec_IraIratok.Updated.KiadmanyozniKell = true;

            // Ügyintézés alapja
            erec_IraIratok.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_IraIratok.Updated.AdathordozoTipusa = true;

            //erec_IraIratok.UgyintezesAlapja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            //erec_IraIratok.Updated.UgyintezesAlapja = true;

            erec_IraIratok.Jelleg = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_IraIratok.Updated.Jelleg = true;

            // erec_IraIratok.UgyintezesAlapja = KodTarak.UGYIRAT_JELLEG.Elektronikus;
            //erec_IraIratok.Updated.UgyintezesAlapja = true;

            /// BUG#3493/Task#3546: Belső kell, nem kimenő
            /// (Viszont a kódban a Belso a Kimeno, a Kimeno a Belso, mert meg lett cserélve az elnevezés             
            /// --> jó lenne átnevezni a kódot valami egyértelműre, mert így csak a keveredés van mindig...)
            /// 
            erec_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;
            erec_IraIratok.Updated.PostazasIranya = true;

            //
            //erec_IraIratok.Hatarido = erec_UgyUgyiratok.Hatarido;
            //erec_IraIratok.Updated.Hatarido = true;

            //erec_IraIratok.IntezesiIdo = erec_UgyUgyiratok.IntezesiIdo;
            //erec_IraIratok.Updated.IntezesiIdo = true;

            //erec_IraIratok.IntezesiIdoegyseg = erec_UgyUgyiratok.IntezesiIdoegyseg;
            //erec_IraIratok.Updated.IntezesiIdoegyseg = true;

            erec_IraIratok.Base.Note = Note_Technikai;
            erec_IraIratok.Base.Updated.Note = true;

            #endregion
            #region Iratpéldány adatok
            EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
            // összes mező update-elhetőségét kezdetben letiltani:
            erec_PldIratPeldanyok.Updated.SetValueAll(false);
            erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);
            //try
            //{
            // TODO
            //KRT_PartnerekService service_partnerek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            //Result result_partner = service_partnerek.GetByCsoportId(execParamUserId, userRow["Partner_id"].ToString());
            //if (result_partner.IsError)
            //{
            //    Logger.Error("Címzett nem állítható be (partnerid): " + userRow["Partner_id"].ToString());
            //    throw new ResultException("Címzett nem állítható be (partnerid): " + userRow["Partner_id"].ToString());
            //}

            //KRT_Partnerek partner_cimzett = (KRT_Partnerek)result_partner.Record;

            //erec_PldIratPeldanyok.Partner_Id_Cimzett = userRow["Partner_id"].ToString();
            //erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;

            //erec_PldIratPeldanyok.NevSTR_Cimzett = partner_cimzett.Nev;
            //erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;
            //}
            //catch (ResultException exc)
            //{
            //    Logger.Error(exc.ToString());

            //    Logger.Error("Címzett nem állítható be (partnerid): " + userRow["Partner_id"].ToString());

            //    // Nem dobjuk tovább a hibát, hanem beállítjuk a Hiba out paraméterben, hogy volt hiba

            //    Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

            //}

            erec_PldIratPeldanyok.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_PldIratPeldanyok.Updated.UgyintezesModja = true;

            #endregion

            IktatasiParameterek ikt_par = new IktatasiParameterek();
            ikt_par.MunkaPeldany = true;
            //        ikt_par.UgyiratUjHatarido = erec_UgyUgyiratok.Hatarido;
            ikt_par.UgyiratPeldanySzukseges = false;
            #endregion

            #region EREC_HATARIDOSFELADATOK
            EREC_HataridosFeladatok erec_HataridosFeladatok = new EREC_HataridosFeladatok();
            erec_HataridosFeladatok.Obj_Id = erec_IraIratok.Id;
            erec_HataridosFeladatok.Updated.Obj_Id = true;

            erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;
            erec_HataridosFeladatok.Updated.Obj_type = true;

            erec_HataridosFeladatok.Azonosito = erec_IraIratok.Azonosito;
            erec_HataridosFeladatok.Updated.Azonosito = true;

            erec_HataridosFeladatok.Leiras = Megjegyzes;
            erec_HataridosFeladatok.Updated.Leiras = true;

            erec_HataridosFeladatok.Tipus = eUtility.KodTarak.FELADAT_TIPUS.Megjegyzes;
            erec_HataridosFeladatok.Updated.Tipus = true;

            erec_HataridosFeladatok.Altipus = eUtility.KodTarak.FELADAT_ALTIPUS.Megjegyzes;
            erec_HataridosFeladatok.Updated.Tipus = true;
            #endregion EREC_HATARIDOSFELADATOK

            // Iktatás webservice hívása:
            var resultIktatas = eRecordService.ServiceFactory.GetEREC_IraIratokService()
                    .BelsoIratIktatasa(execParamUserId, iktatokonyvId, erec_UgyUgyiratok, new EREC_UgyUgyiratdarabok()
                                , erec_IraIratok, erec_HataridosFeladatok, erec_PldIratPeldanyok
                                , ikt_par);
            resultIktatas.CheckError();

            ErkeztetesIktatasResult iktatasResult = (ErkeztetesIktatasResult)resultIktatas.Record;
            string ujUgyiratId = iktatasResult.UgyiratId;

            #region out paraméterek beállítása

            // Új ügyirat objektum lekérése:

            DataRow ujUgyiratRow = this.GetUgyiratRow(new Guid(ujUgyiratId));

            Foszam = ujUgyiratRow["Foszam"].ToString();
            Evszam = iktatokonyvRow["Ev"].ToString(); ;

            //objTargyszoHiba = null;

            #endregion

            #region Obj. tárgyszó beállítása

            if (!string.IsNullOrEmpty(Kategoriak.Kategoria))
            {
                Logger.Debug("Iktatás ok, obj. tárgyszó beállítás Start...");

                /// Tárgyszó érték mentése az objektumhoz:
                /// Iktatás után hívható csak, nincs egy tranzakcióba összevonva az iktatással. 
                /// (Az eRecordWebSite-on is így van, az iktatás után külön hívással megy. Ha ott jó így, itt is jó lesz...)
                /// 
                try
                {

                    this.SetObjektumTargyszo(new Guid(ujUgyiratId), eUtility.Constants.TableNames.EREC_UgyUgyiratok, TargyszoAzon_TARGYSZOK, Kategoriak.Kategoria, execParamUserId);
                }
                catch (Exception exc)
                {
                    Logger.Error(exc.ToString());

                    // Nem dobjuk tovább a hibát, hanem beállítjuk az objTargyszoHiba out paraméterben, hogy volt hiba a tárgyszó mentésnél
                    // string hibaKodTargyszo;
                    Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
                }
            }
            #endregion

            Logger.InfoEnd("SZURManager.SZURGetFoszamWS");
        }

        /// <summary>
        /// Kimenő irat iktatása (elavult hívás)
        /// "Új kimenő alszám felvétele egy már létező, megadott főszámhoz."
        /// </summary>
        /// <param name="Ugy"></param>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="IratTipus"></param>
        /// <param name="IratTargy"></param>
        /// <param name="HelybenMarad"></param>
        /// <param name="Cimzettek"></param>
        /// <param name="UserId"></param>
        /// <param name="Alszam"></param>
        /// <param name="Eloszam"></param>
        // Összevonásra került a SZUReGetAlszammal
        //public void SZURGetalszamWS(string Ugy, string Evszam, string Konyv, string Foszam, string IratTipus, string IratTargy, string HelybenMarad, Cimzett[] Cimzettek, string UserId
        //    , out string EvszamOut, out string KonyvOut, out string FoszamOut, out string Alszam, out string Eloszam, out string cimzettekHiba)
        //{
        //    Logger.InfoStart("SZURManager.SZURGetalszamWS");

        //    ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

        //    #region Szükséges adatok lekérése

        //    // Ügyirat lekérése vagy az iktatószám alapján, vagy pedig a könyv, évszám, főszám hármas alapján:
        //    DataRow ugyiratRow;
        //    if (!String.IsNullOrEmpty(Evszam) && !String.IsNullOrEmpty(Konyv) && !String.IsNullOrEmpty(Foszam))
        //    {
        //        ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
        //    }
        //    else
        //    {
        //        ugyiratRow = this.GetUgyiratRow(Ugy, execParamUserId);
        //    }
        //    Guid ugyiratId = (Guid)ugyiratRow["Id"];

        //    Guid iktatokonyvId = (Guid)ugyiratRow["IraIktatokonyv_Id"];
        //    var iktatokonyv = this.GetIktatokonyv(iktatokonyvId, true);

        //    var iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();

        //    var searchObjIratok = new EREC_IraIratokSearch(true);

        //    searchObjIratok.Ugyirat_Id.Value = ugyiratId.ToString();
        //    searchObjIratok.Ugyirat_Id.Operator = Query.Operators.equals;

        //    // Csak egy munkaügyirat lehet benne
        //    searchObjIratok.Sorszam.Value = "1";
        //    searchObjIratok.Sorszam.Operator = Query.Operators.equals;

        //    searchObjIratok.Alszam.Value = "";
        //    searchObjIratok.Alszam.Operator = Query.Operators.isnull;


        //    // Lekérdezés adatbázisból:
        //    var resultIrat = iratokService.GetAllWithExtension(execParamUserId, searchObjIratok);

        //    if (resultIrat.IsError)
        //    {
        //        throw new ResultException(resultIrat);
        //    }

        //    if (resultIrat.Ds.Tables[0].Rows.Count > 1)
        //    {
        //        throw new ResultException("MunkaIrat lekérdezése sikertelen: több találat is van!");
        //    }

        //    EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
        //    // Ha van technikai munkairat akkor munkairat beiktatás, ha nincs akor új alszám kérés
        //    bool ismunkairat = false;
        //    if (resultIrat.Ds.Tables[0].Rows.Count == 1)
        //    {
        //        if (resultIrat.Ds.Tables[0].Rows[0]["Note"].ToString() == Note_Technikai)
        //        {
        //            ismunkairat = true;
        //            Utility.LoadBusinessDocumentFromDataRow(erec_IraIratok, resultIrat.Ds.Tables[0].Rows[0]);
        //        }
        //    }

        //    #endregion

        //    #region Iktatáshoz szükséges objektumok feltöltése

        //    //EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
        //    // összes mező update-elhetőségét kezdetben letiltani:
        //    erec_IraIratok.Updated.SetValueAll(false);
        //    erec_IraIratok.Base.Updated.SetValueAll(false);

        //    // erec_IraIratok.Irattipus =IratTipus;
        //    if (String.IsNullOrEmpty(IratTipus))
        //        throw new ResultException("Nincs megadva Irattipus!");

        //    string irattipusKod = KodTar_Cache.GetKodtarakByKodCsoport(KodTarak.IRATTIPUS.KodcsoportKod, this.ExecParamCaller
        //   , HttpContext.Current.Cache).FirstOrDefault(x => x.Value == IratTipus).Key;

        //    // BLG_2502
        //    if (String.IsNullOrEmpty(irattipusKod))
        //        throw new ResultException("Irattipus nem található!");

        //    erec_IraIratok.Irattipus =irattipusKod;
        //    erec_IraIratok.Updated.Irattipus = true;

        //    // Tárgy:
        //    erec_IraIratok.Targy = IratTargy;
        //    erec_IraIratok.Updated.Targy = true;



        //    if (HelybenMarad == "Helyben marad")
        //    {
        //        erec_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Belso;
        //    }
        //    else
        //        erec_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;
        //    erec_IraIratok.Updated.PostazasIranya = true;

        //    EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
        //    erec_PldIratPeldanyok.Updated.SetValueAll(false);
        //    erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

        //    // BUG_2742
        //    //Cimzettek - ből az elsőt kivesszük, az lesz az iratpéldány címzettje, a többit xml fájlként csatoljuk az irathoz
        //   if (Cimzettek != null  && Cimzettek.Length > 0)
        //    {

        //        //Dictionary<string, Guid> kapcsolatiKodPartnerIdDict;
        //        //List<string> kapcsolatiKodokList = Cimzettek.Where(e => !String.IsNullOrEmpty(e.KapcsolattartoAzonosito))
        //        //                                                .Select(e => e.KapcsolattartoAzonosito).ToList();

        //        //// Kapcsolati kód ellenőrzés:
        //        //this.PartnerKapcsolatiKodokEllenorzes(kapcsolatiKodokList, out kapcsolatiKodPartnerIdDict);

        //        Cimzett cimzett1 = Cimzettek[0];
        //       // Partner_Id_Cimzett:
        //        FoundPartner foundPartner = FindPartner(cimzett1);
        //        //if (!String.IsNullOrEmpty(cimzett1.KapcsolattartoAzonosito)
        //        //    && kapcsolatiKodPartnerIdDict.ContainsKey(cimzett1.KapcsolattartoAzonosito))
        //        //{
        //        //    erec_PldIratPeldanyok.Partner_Id_Cimzett = kapcsolatiKodPartnerIdDict[cimzett1.KapcsolattartoAzonosito].ToString();
        //        //        erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;
        //        //    }

        //        // Első címzett beírása
        //        if (!String.IsNullOrEmpty(foundPartner.Partner_Id))
        //        {
        //            erec_PldIratPeldanyok.Partner_Id_Cimzett = foundPartner.Partner_Id;
        //            erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;
        //        }

        //        if (String.IsNullOrEmpty(cimzett1.Nev))
        //        {
        //            erec_PldIratPeldanyok.NevSTR_Cimzett = cimzett1.Szervezet;

        //        } else erec_PldIratPeldanyok.NevSTR_Cimzett = cimzett1.Nev;
        //       erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;

        //        // Teljes cím kell:
        //        if (!String.IsNullOrEmpty(foundPartner.PartnerCim_Id))
        //        {
        //            erec_PldIratPeldanyok.Cim_id_Cimzett = foundPartner.PartnerCim_Id;
        //            erec_PldIratPeldanyok.Updated.Cim_id_Cimzett = true;
        //        }

        //        erec_PldIratPeldanyok.CimSTR_Cimzett = cimzett1.GetTeljesCim();
        //        erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = true;

        //        if (cimzett1.Elektronikus == "I")
        //            {
        //                //erec_IraIratok.UgyintezesAlapja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
        //                //erec_IraIratok.Updated.UgyintezesAlapja = true;

        //                erec_IraIratok.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
        //                erec_IraIratok.Updated.AdathordozoTipusa = true;

        //                erec_IraIratok.Jelleg = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
        //                erec_IraIratok.Updated.Jelleg = true;

        //                erec_PldIratPeldanyok.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
        //                erec_PldIratPeldanyok.Updated.UgyintezesModja = true;
        //            }
        //            else if (cimzett1.Elektronikus == "N")
        //            {
        //                erec_IraIratok.UgyintezesAlapja = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
        //                erec_IraIratok.Updated.UgyintezesAlapja = true;

        //                erec_IraIratok.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
        //                erec_IraIratok.Updated.AdathordozoTipusa = true;

        //                erec_IraIratok.Jelleg = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
        //                erec_IraIratok.Updated.Jelleg = true;

        //                erec_PldIratPeldanyok.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
        //                erec_PldIratPeldanyok.Updated.UgyintezesModja = true;
        //            }

        //        // Expediálás módja:
        //        string expedialasKod = string.Empty;
        //        if (!String.IsNullOrEmpty(cimzett1.cExpedialasMod))
        //        {
        //            expedialasKod = KodTar_Cache.GetKodtarakByKodCsoport(KodTarak.KULDEMENY_KULDES_MODJA.KodcsoportKod, this.ExecParamCaller
        //       , HttpContext.Current.Cache).FirstOrDefault(x => x.Value == cimzett1.cExpedialasMod).Key;
        //        }
        //        // BLG_2502
        //        if (!String.IsNullOrEmpty(expedialasKod))
        //        {
        //            //erec_PldIratPeldanyok.KuldesMod = cimzett.cExpedialasMod;
        //            erec_PldIratPeldanyok.KuldesMod = expedialasKod;
        //            erec_PldIratPeldanyok.Updated.KuldesMod = true;
        //        }
        //    }

        //    #endregion


        //    //var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

        //    Result resultIktatas = new Result();
        //    if (ismunkairat)
        //    {
        //        // Munkairat beiktatás
        //        resultIktatas = iratokService.MunkapeldanyBeiktatasa(execParamUserId, erec_IraIratok.Id, erec_IraIratok, erec_PldIratPeldanyok);

        //    }
        //    else
        //    {
        //        // Iktatás webservice hívása:
        //        resultIktatas = iratokService.BelsoIratIktatasa_Alszamra(execParamUserId, iktatokonyvId.ToString(), ugyiratId.ToString()
        //                                    , new EREC_UgyUgyiratdarabok(), erec_IraIratok, new EREC_HataridosFeladatok()
        //                                    , erec_PldIratPeldanyok, new IktatasiParameterek());
        //    }

        //    resultIktatas.CheckError();

        //    ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)(resultIktatas.Record);
        //    string iratId = erkeztetesIktatasResult.IratId;


        //    #region Out paraméterek feltöltése

        //    //Létrejött irat objektum lekérése:
        //    DataRow ujIratRow = this.GetIratRow(new Guid(iratId));

        //    Alszam = ujIratRow["Alszam"].ToString();
        //    // Irat ügyintéző csoportjában lesz ez az adat
        //    Eloszam = ujIratRow["IratFelelos_SzervezetKod"].ToString();
        //    EvszamOut = iktatokonyv.Ev;
        //    KonyvOut = iktatokonyv.Iktatohely;
        //    FoszamOut = ugyiratRow["Foszam"].ToString();

        //    cimzettekHiba = null;

        //    #endregion

        //    #region Cimzettek ElosztóÍvre rögzítése

        //    // BUG_2742
        //    if (Cimzettek != null
        //        && Cimzettek.Length > 1)  // Ha csak egy címzett van, akkor az beírásra került az iratpld-be, nem kell elosztóív
        //    {

        //        try
        //        {
        //            ExecParam execParamCim = ExecParamCaller.Clone();

        //            EREC_IraElosztoivekService serviceElosztoIv = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();

        //            EREC_IraElosztoivek elosztoiv = new EREC_IraElosztoivek();
        //            elosztoiv.Updated.SetValueAll(false);
        //            elosztoiv.Base.Updated.SetValueAll(false);

        //            elosztoiv.NEV = "CimzettLista_" + Konyv + "_" + Alszam + "_" + Evszam + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss"); 
        //            elosztoiv.Updated.NEV = true;

        //            elosztoiv.Fajta = KodtarKod_ElosztoIv_Fajta_Cimzettlista;
        //            elosztoiv.Updated.Fajta = true;

        //            elosztoiv.Kod = "0";
        //            elosztoiv.Updated.Kod = true;

        //            elosztoiv.Csoport_Id_Tulaj = execParamCim.Felhasznalo_Id;
        //            elosztoiv.Updated.Csoport_Id_Tulaj = true;

        //            elosztoiv.Base.Note = iratId;
        //            elosztoiv.Base.Updated.Note = true;


        //            Result resultElosztoIv = serviceElosztoIv.Insert(execParamCim, elosztoiv);

        //            if (resultElosztoIv.IsError)
        //            {
        //                Logger.Error("Cimzett elosztóív létrehozási hiba");
        //                throw new ResultException(resultElosztoIv);

        //            }
        //            string elosztoivId = resultElosztoIv.Uid;

        //            EREC_IraElosztoivTetelekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();

        //            int sorszam = 0;
        //            foreach (Cimzett cimzett in Cimzettek)
        //            {
        //                string partnerId = String.Empty;
        //                string cimId = String.Empty;
        //                string partnerCimId = String.Empty;

        //                FoundPartner foundPartner =  FindPartner(cimzett);

        //                EREC_IraElosztoivTetelek elosztoivTetelek = new EREC_IraElosztoivTetelek();

        //                sorszam++;

        //                elosztoivTetelek.Updated.SetValueAll(false);
        //                elosztoivTetelek.Base.Updated.SetValueAll(false);

        //                elosztoivTetelek.ElosztoIv_Id = elosztoivId;
        //                elosztoivTetelek.Updated.ElosztoIv_Id = true;

        //                if (!string.IsNullOrEmpty(foundPartner.Partner_Id))
        //                {
        //                    elosztoivTetelek.Partner_Id = foundPartner.Partner_Id;
        //                    elosztoivTetelek.Updated.Partner_Id = true;
        //                }

        //                //BLG_2502
        //                //BUG_2742
        //                //elosztoivTetelek.NevSTR = cimzett.Nev;
        //                if (String.IsNullOrEmpty(cimzett.Nev))
        //                {
        //                    elosztoivTetelek.NevSTR = cimzett.Szervezet;

        //                }
        //                else elosztoivTetelek.NevSTR = cimzett.Nev;
        //                elosztoivTetelek.Updated.NevSTR = true;

        //                if (!string.IsNullOrEmpty(foundPartner.Cim_Id))
        //                {
        //                    elosztoivTetelek.Cim_Id = foundPartner.Cim_Id;
        //                    elosztoivTetelek.Updated.Cim_Id = true;
        //                }

        //                elosztoivTetelek.CimSTR = cimzett.GetTeljesCim();
        //                elosztoivTetelek.Updated.CimSTR = true;

        //                // Expediálás módja:
        //                string expKod = string.Empty;
        //                if (!String.IsNullOrEmpty(cimzett.cExpedialasMod))
        //                {
        //                    expKod = KodTar_Cache.GetKodtarakByKodCsoport(KodTarak.KULDEMENY_KULDES_MODJA.KodcsoportKod, this.ExecParamCaller
        //                    , HttpContext.Current.Cache).FirstOrDefault(x => x.Value == cimzett.cExpedialasMod).Key;
        //                    // BLG_2502
        //                    if (!String.IsNullOrEmpty(expKod))
        //                    {
        //                        elosztoivTetelek.Kuldesmod = expKod;
        //                        elosztoivTetelek.Updated.Kuldesmod = true;
        //                    }
        //                }

        //                elosztoivTetelek.Sorszam = sorszam.ToString();
        //                elosztoivTetelek.Updated.Sorszam = true;

        //                DataRow userRow = GetFelhasznaloRowByUserNev(UserId);

        //                elosztoivTetelek.Base.Note = GetXMLCimzettListaIktatasCimzett(cimzett, foundPartner.Cim_Fajta, userRow["Nev"].ToString());

        //                Result resultElosztoiIvTetelek = service.Insert(execParamCim, elosztoivTetelek);
        //                if (resultElosztoiIvTetelek.IsError)
        //                {
        //                    Logger.Error("Cimzett elosztóívtétel létrehozási hiba");
        //                    throw new ResultException(resultElosztoiIvTetelek);

        //                }
        //            }

        //            // BUG_2742
        //            //ExecParam exec_paramPldUpdate = ExecParamCaller.Clone();

        //            //EREC_PldIratPeldanyokService _PldIratPeldanyokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        //            //Result resultPld = _PldIratPeldanyokService.GetElsoIratPeldanyByIraIrat(exec_paramPldUpdate, iratId);
        //            //if (resultPld.IsError)
        //            //{
        //            //    Logger.Error("Iratpéldány cím update hiba! (Első iratpéldány)");
        //            //    throw new ResultException(resultPld);
        //            //}

        //            //EREC_PldIratPeldanyok erec_PldIratPeldanyokUpdate = (EREC_PldIratPeldanyok)resultPld.Record; 
        //            //erec_PldIratPeldanyok.Updated.SetValueAll(false);
        //            //erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

        //            //erec_PldIratPeldanyokUpdate.NevSTR_Cimzett = elosztoiv.NEV;
        //            //erec_PldIratPeldanyokUpdate.Updated.CimSTR_Cimzett = true;

        //            //exec_paramPldUpdate.Record_Id = erec_PldIratPeldanyokUpdate.Id;

        //            //Result result_pldUpdate = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService().Update(exec_paramPldUpdate,erec_PldIratPeldanyokUpdate);
        //            //if (result_pldUpdate.IsError)
        //            //{
        //            //    Logger.Error("Iratpéldány cím update hiba!" );
        //            //    throw new ResultException(result_pldUpdate);    
        //            //}
        //        }
        //        catch (Exception exc)
        //        {
        //            Logger.Error("Címzett elosztóív létrehozási hiba" + exc.ToString());

        //            // Nem dobjuk tovább a hibát, hanem beállítjuk az cimzettekXmlCsatolasHiba out paraméterben, hogy volt hiba a tárgyszó mentésnél
        //            string hibaKodCimzettXml;
        //            Utility.SetHibakodHibaszoveg(exc, out hibaKodCimzettXml, out cimzettekHiba);
        //        }
        //    }

        //    #endregion

        //    Logger.InfoEnd("SZURManager.SZURGetalszamWS");
        //}

        public void SZUReGetalszamWS(Boolean eGetAlszam, string Ugy, string Evszam, string Konyv, string Foszam, string IratTipus, string IratTargy, string HelybenMarad, string UKSpecial, string CimzettMarad, Cimzett[] Cimzettek, string UserId
                  , out string Alszam, out string EvszamOut, out string KonyvOut, out string FoszamOut, out string Eloszam, out string cimzettekHiba)
        {
            // BUG_2742
            // GetAlszam és eGetAlszam összevonás
            string logkieg = (eGetAlszam ? "" : " (SZURGetAlszamWS)");
            Logger.InfoStart("SZURManager.SZUReGetalszamWS" + logkieg);


            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            #region Szükséges adatok lekérése

            // Ügyirat lekérése vagy az iktatószám alapján, vagy pedig a könyv, évszám, főszám hármas alapján:
            DataRow ugyiratRow;
            if (!String.IsNullOrEmpty(Evszam) && !String.IsNullOrEmpty(Konyv) && !String.IsNullOrEmpty(Foszam))
            {
                ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
            }
            else
            {
                ugyiratRow = this.GetUgyiratRow(Ugy, execParamUserId);
            }
            Guid ugyiratId = (Guid)ugyiratRow["Id"];

            Guid iktatokonyvId = (Guid)ugyiratRow["IraIktatokonyv_Id"];
            var iktatokonyv = this.GetIktatokonyv(iktatokonyvId, true);

            var iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();

            var searchObjIratok = new EREC_IraIratokSearch(true);

            searchObjIratok.Ugyirat_Id.Value = ugyiratId.ToString();
            searchObjIratok.Ugyirat_Id.Operator = Query.Operators.equals;

            // Csak egy munkaügyirat lehet benne
            searchObjIratok.Sorszam.Value = "1";
            searchObjIratok.Sorszam.Operator = Query.Operators.equals;

            searchObjIratok.Alszam.Value = "";
            searchObjIratok.Alszam.Operator = Query.Operators.isnull;


            // Lekérdezés adatbázisból:
            var resultIrat = iratokService.GetAllWithExtension(execParamUserId, searchObjIratok);

            if (resultIrat.IsError)
            {
                throw new ResultException(resultIrat);
            }

            if (resultIrat.Ds.Tables[0].Rows.Count > 1)
            {
                throw new ResultException("MunkaIrat lekérdezése sikertelen: több találat is van!");
            }

            EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
            // Ha van technikai munkairat akkor munkairat beiktatás, ha nincs akor új alszám kérés
            bool ismunkairat = false;
            if (resultIrat.Ds.Tables[0].Rows.Count == 1)
            {
                if (resultIrat.Ds.Tables[0].Rows[0]["Note"].ToString() == Note_Technikai)
                {
                    ismunkairat = true;
                    Utility.LoadBusinessDocumentFromDataRow(erec_IraIratok, resultIrat.Ds.Tables[0].Rows[0]);
                }
            }

            #endregion

            #region Iktatáshoz szükséges objektumok feltöltése

            //EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
            // összes mező update-elhetőségét kezdetben letiltani:
            erec_IraIratok.Updated.SetValueAll(false);
            erec_IraIratok.Base.Updated.SetValueAll(false);

            // erec_IraIratok.Irattipus =IratTipus;
            if (String.IsNullOrEmpty(IratTipus))
                throw new ResultException("Nincs megadva Irattipus!");

            string irattipusKod = KodTar_Cache.GetKodtarakByKodCsoport(KodTarak.IRATTIPUS.KodcsoportKod, this.ExecParamCaller
           , HttpContext.Current.Cache).FirstOrDefault(x => x.Value == IratTipus).Key;

            // BLG_2502
            if (String.IsNullOrEmpty(irattipusKod))
                throw new ResultException("Irattipus nem található!");
            erec_IraIratok.Irattipus = irattipusKod;
            erec_IraIratok.Updated.Irattipus = true;

            // Tárgy:
            erec_IraIratok.Targy = IratTargy;
            erec_IraIratok.Updated.Targy = true;

            // BUG#7682: Intézkedő ügyintéző beállítása:
            erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez = execParamUserId.Felhasznalo_Id;
            erec_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

            // BUG_2742
            // A Kimenő a belső, a belső a kimenő?????
            //if (HelybenMarad == "Helyben marad")
            //{
            //    erec_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Belso;
            //}
            //else
            //    erec_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;

            bool ugyiratpeldanySzukseges;

            if (HelybenMarad == "Helyben marad")
            {
                // Ez a belső irat:
                erec_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;
                ugyiratpeldanySzukseges = false;
            }
            else
            {
                // BUG#3493/Task#3502: Kiadmányozni kell flag állítódjon be:
                erec_IraIratok.KiadmanyozniKell = "1";
                erec_IraIratok.Updated.KiadmanyozniKell = true;

                // Ez a kimenő irat:
                erec_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Belso;

                // BUG#5901: Kimenő iktatáskor kell ügyiratban maradó példány:
                ugyiratpeldanySzukseges = true;
            }

            erec_IraIratok.Updated.PostazasIranya = true;

            EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
            erec_PldIratPeldanyok.Updated.SetValueAll(false);
            erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

            // Kódtárfüggőség lekérése:
            KodtarFuggosegDataClass ktFuggosegNmhhExpMod = Utility.GetKodtarFuggoseg(SZURManager.KCS_NMHH_EXPMOD, SZURManager.KCS_KULDEMENY_KULDES_MODJA, this.ExecParamCaller);

            FoundPartner[] alreadyRegisteredPartners = null;
            // BUG_2742
            //Cimzettek - ből az elsőt kivesszük, az lesz az iratpéldány címzettje, a többit xml fájlként csatoljuk az irathoz
            if (Cimzettek != null && Cimzettek.Length > 0)
            {
                #region PARTNER/CIM TORZS
                bool isEVersion = true;

                if (UKSpecial != "igen")
                    alreadyRegisteredPartners = CheckCimzettToTorzsAdat(Cimzettek, isEVersion);

                if (isEVersion)
                    Cimzettek = KibovitOsszesTobbiKapcsolattartoval(Cimzettek);
                #endregion

                Cimzett cimzett1 = Cimzettek[0];
                FoundPartner foundPartner;

                foundPartner = FindPartner(cimzett1);

                // Első címzett beírása
                if (!String.IsNullOrEmpty(foundPartner.Partner_Id))
                {
                    erec_PldIratPeldanyok.Partner_Id_Cimzett = foundPartner.Partner_Id;
                    erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;
                }
                // BUG_8333
                //if (String.IsNullOrEmpty(cimzett1.Nev))
                if (string.IsNullOrEmpty(cimzett1.Nev) || string.IsNullOrEmpty(cimzett1.Nev))
                {
                    erec_PldIratPeldanyok.NevSTR_Cimzett = cimzett1.Szervezet;

                }
                else erec_PldIratPeldanyok.NevSTR_Cimzett = cimzett1.Nev;
                erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;

                // Teljes cím kell:
                if (!String.IsNullOrEmpty(foundPartner.Cim_Id))
                {
                    erec_PldIratPeldanyok.Cim_id_Cimzett = foundPartner.Cim_Id;
                    erec_PldIratPeldanyok.Updated.Cim_id_Cimzett = true;
                }

                erec_PldIratPeldanyok.CimSTR_Cimzett = cimzett1.GetTeljesCim();
                erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = true;

                if (IsCimzettElektronikus(cimzett1))
                {
                    //erec_IraIratok.UgyintezesAlapja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                    //erec_IraIratok.Updated.UgyintezesAlapja = true;

                    erec_IraIratok.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                    erec_IraIratok.Updated.AdathordozoTipusa = true;

                    //erec_IraIratok.Jelleg = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                    //erec_IraIratok.Updated.Jelleg = true;

                    erec_PldIratPeldanyok.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                    erec_PldIratPeldanyok.Updated.UgyintezesModja = true;

                    //KapcsolattartoAzonosito
                    if (!string.IsNullOrEmpty(cimzett1.UgyfelAzonosito) && !string.IsNullOrEmpty(cimzett1.KapcsolattartoAzonosito))
                        erec_PldIratPeldanyok.CimSTR_Cimzett = string.Format("{0}", cimzett1.KapcsolattartoAzonosito);
                    else if (!string.IsNullOrEmpty(cimzett1.UgyfelAzonosito))
                        erec_PldIratPeldanyok.CimSTR_Cimzett = string.Format("{0}", cimzett1.UgyfelAzonosito);
                    else if (!string.IsNullOrEmpty(cimzett1.KapcsolattartoAzonosito))
                        erec_PldIratPeldanyok.CimSTR_Cimzett = string.Format("{0}", cimzett1.KapcsolattartoAzonosito);
                    else
                        throw new Exception("Nincs kitöltve az UgyfelAzonosito és a KapcsolattartoAzonosito !");
                }
                else
                {
                    //erec_IraIratok.UgyintezesAlapja = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
                    //erec_IraIratok.Updated.UgyintezesAlapja = true;

                    erec_IraIratok.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
                    erec_IraIratok.Updated.AdathordozoTipusa = true;

                    //erec_IraIratok.Jelleg = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
                    //erec_IraIratok.Updated.Jelleg = true;

                    erec_PldIratPeldanyok.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
                    erec_PldIratPeldanyok.Updated.UgyintezesModja = true;
                }

                #region Expediálás módja (BUG#5901: NMHH_EXPMOD - KULDEMENY_KULDES_MODJA kódtárfüggőségből)

                string kuldKuldesModKod;
                Utility.CheckCimzettExpedialasMod(cimzett1, ktFuggosegNmhhExpMod, this.ExecParamCaller, out kuldKuldesModKod);

                erec_PldIratPeldanyok.KuldesMod = kuldKuldesModKod;
                erec_PldIratPeldanyok.Updated.KuldesMod = true;

                #endregion
            }

            #region 11580
            if (!string.IsNullOrEmpty(execParamUserId.FelhasznaloSzervezet_Id))
            {
                erec_IraIratok.Csoport_Id_Ugyfelelos = execParamUserId.FelhasznaloSzervezet_Id;
                erec_IraIratok.Updated.Csoport_Id_Ugyfelelos = true;
            }
            #endregion 11580

            #endregion


            //var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            Result resultIktatas = new Result();
            if (ismunkairat)
            {
                // Munkairat beiktatás
                resultIktatas = iratokService.MunkapeldanyBeiktatasa(execParamUserId, erec_IraIratok.Id, erec_IraIratok, erec_PldIratPeldanyok);

            }
            else
            {
                // Iktatás webservice hívása:
                resultIktatas = iratokService.BelsoIratIktatasa_Alszamra(execParamUserId, iktatokonyvId.ToString(), ugyiratId.ToString()
                                            , new EREC_UgyUgyiratdarabok(), erec_IraIratok, new EREC_HataridosFeladatok()
                                            , erec_PldIratPeldanyok
                                            , new IktatasiParameterek()
                                            {
                                                UgyiratPeldanySzukseges = ugyiratpeldanySzukseges
                                                ,
                                                IsSZUR = true
                                            }
                                            );
            }

            resultIktatas.CheckError();

            ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)(resultIktatas.Record);
            string iratId = erkeztetesIktatasResult.IratId;


            #region Out paraméterek feltöltése

            //Létrejött irat objektum lekérése:
            DataRow ujIratRow = this.GetIratRow(new Guid(iratId));

            Alszam = ujIratRow["Alszam"].ToString();
            // Irat ügyintéző csoportjában lesz ez az adat
            Eloszam = ujIratRow["IratFelelos_SzervezetKod"] == null ? string.Empty : ujIratRow["IratFelelos_SzervezetKod"].ToString();
            EvszamOut = iktatokonyv.Ev;
            KonyvOut = iktatokonyv.Iktatohely;
            FoszamOut = ugyiratRow["Foszam"].ToString();

            cimzettekHiba = null;

            #endregion

            #region Cimzettek ElosztóÍvre rögzítése

            // BUG_2742
            //if (Cimzettek != null
            //    && Cimzettek.Length > 0)
            if (Cimzettek != null && Cimzettek.Length > 0) // Mindig kell elosztóív
            {
                try
                {
                    ExecParam execParamCim = ExecParamCaller.Clone();

                    EREC_IraElosztoivekService serviceElosztoIv = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();

                    EREC_IraElosztoivek elosztoiv = new EREC_IraElosztoivek();
                    elosztoiv.Updated.SetValueAll(false);
                    elosztoiv.Base.Updated.SetValueAll(false);

                    elosztoiv.NEV = "CimzettLista_" + Konyv + "_" + Foszam + "_" + Alszam + "_" + Evszam + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
                    elosztoiv.Updated.NEV = true;

                    elosztoiv.Fajta = KodtarKod_ElosztoIv_Fajta_Cimzettlista;
                    elosztoiv.Updated.Fajta = true;

                    elosztoiv.Kod = "0";
                    elosztoiv.Updated.Kod = true;

                    elosztoiv.Csoport_Id_Tulaj = execParamCim.Felhasznalo_Id;
                    elosztoiv.Updated.Csoport_Id_Tulaj = true;

                    elosztoiv.Base.Note = iratId;
                    elosztoiv.Base.Updated.Note = true;


                    Result resultElosztoIv = serviceElosztoIv.Insert(execParamCim, elosztoiv);

                    if (resultElosztoIv.IsError)
                    {
                        Logger.Error("Cimzett elosztóív létrehozási hiba");
                        throw new ResultException(resultElosztoIv);

                    }
                    string elosztoivId = resultElosztoIv.Uid;

                    EREC_IraElosztoivTetelekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();

                    int sorszam = 0;
                    foreach (Cimzett cimzett in Cimzettek)
                    {
                        if (String.IsNullOrEmpty(cimzett.Nev) && !String.IsNullOrEmpty(cimzett.Szervezet)) // BUG_11240
                        {
                            continue;
                        }

                        string partnerId = String.Empty;
                        string cimId = String.Empty;
                        string partnerCimId = String.Empty;

                        FoundPartner foundPartner = new FoundPartner();

                        // BUG_2742
                        // GetAlszam és eGetAlszam összevonás
                        //if (UKSpecial != "igen")
                        if (eGetAlszam && UKSpecial != "igen")
                        {
                            foundPartner = FindPartner(cimzett);
                        }

                        EREC_IraElosztoivTetelek elosztoivTetelek = new EREC_IraElosztoivTetelek();

                        elosztoivTetelek.Updated.SetValueAll(false);
                        elosztoivTetelek.Base.Updated.SetValueAll(false);

                        elosztoivTetelek.ElosztoIv_Id = elosztoivId;
                        elosztoivTetelek.Updated.ElosztoIv_Id = true;

                        if (!string.IsNullOrEmpty(foundPartner.Partner_Id))
                        {
                            elosztoivTetelek.Partner_Id = foundPartner.Partner_Id;
                            elosztoivTetelek.Updated.Partner_Id = true;
                        }

                        //BUG_2742
                        //elosztoivTetelek.NevSTR = cimzett.Nev;
                        if ((String.IsNullOrEmpty(cimzett.Nev)))
                        {
                            elosztoivTetelek.NevSTR = cimzett.Szervezet;

                        }
                        else elosztoivTetelek.NevSTR = cimzett.Nev;
                        elosztoivTetelek.Updated.NevSTR = true;

                        if (!string.IsNullOrEmpty(foundPartner.Cim_Id))
                        {
                            elosztoivTetelek.Cim_Id = foundPartner.Cim_Id;
                            elosztoivTetelek.Updated.Cim_Id = true;
                        }

                        elosztoivTetelek.CimSTR = cimzett.GetTeljesCim();
                        elosztoivTetelek.Updated.CimSTR = true;

                        if (IsCimzettElektronikus(cimzett))
                        {
                            //Kapcsolattarto
                            if (!string.IsNullOrEmpty(cimzett.UgyfelAzonosito) && !string.IsNullOrEmpty(cimzett.KapcsolattartoAzonosito))
                                elosztoivTetelek.CimSTR = string.Format("{0}", cimzett.KapcsolattartoAzonosito);
                            else if (!string.IsNullOrEmpty(cimzett.UgyfelAzonosito))
                                elosztoivTetelek.CimSTR = string.Format("{0}", cimzett.UgyfelAzonosito);
                            else if (!string.IsNullOrEmpty(cimzett.KapcsolattartoAzonosito))
                                elosztoivTetelek.CimSTR = string.Format("{0}", cimzett.KapcsolattartoAzonosito);
                            else
                            {
                                // BUG#7784: Nem dobunk hibát, csak ezt nem tesszük bele a címzettlistába

                                // throw new Exception("Nincs kitöltve az UgyfelAzonosito és a KapcsolattartoAzonosito !");

                                Logger.Error("Nincs kitöltve az UgyfelAzonosito és a KapcsolattartoAzonosito !");

                                continue;
                            }
                        }

                        #region Expediálás módja (BUG#5901: NMHH_EXPMOD - KULDEMENY_KULDES_MODJA kódtárfüggőségből)

                        string kuldKuldesModKod;
                        Utility.CheckCimzettExpedialasMod(cimzett, ktFuggosegNmhhExpMod, this.ExecParamCaller, out kuldKuldesModKod);

                        elosztoivTetelek.Kuldesmod = kuldKuldesModKod;
                        elosztoivTetelek.Updated.Kuldesmod = true;

                        #endregion

                        sorszam++;

                        elosztoivTetelek.Sorszam = sorszam.ToString();
                        elosztoivTetelek.Updated.Sorszam = true;

                        DataRow userRow = GetFelhasznaloRowByUserNev(UserId);

                        elosztoivTetelek.Base.Note = GetXMLCimzettListaIktatasCimzett(cimzett, foundPartner.Cim_Fajta, userRow["Nev"].ToString(), kuldKuldesModKod);

                        Result resultElosztoiIvTetelek = service.Insert(execParamCim, elosztoivTetelek);
                        if (resultElosztoiIvTetelek.IsError)
                        {
                            Logger.Error("Cimzett elosztóívtétel létrehozási hiba");
                            throw new ResultException(resultElosztoiIvTetelek);

                        }
                    }

                    // BUG#7687: Az első iratpéldány címzettjébe beírjuk az elosztóívet:
                    try
                    {
                        DataRow pldRow1 = this.GetIratPeldanyRow(EvszamOut, KonyvOut, FoszamOut, Alszam, "1");

                        var pldObj = this.GetIratPeldanyObjById((Guid)pldRow1["Id"]);

                        pldObj.Updated.SetValueAll(false);
                        pldObj.Base.Updated.SetValueAll(false);
                        pldObj.Base.Updated.Ver = true;

                        pldObj.Partner_Id_Cimzett = elosztoivId;
                        pldObj.Updated.Partner_Id_Cimzett = true;

                        pldObj.NevSTR_Cimzett = elosztoiv.NEV;
                        pldObj.Updated.NevSTR_Cimzett = true;

                        pldObj.CimSTR_Cimzett = "Címzettlista szerint";
                        pldObj.Updated.CimSTR_Cimzett = true;

                        pldObj.Cim_id_Cimzett = "<null>";
                        pldObj.Updated.Cim_id_Cimzett = true;

                        // Iratpéldány UPDATE:
                        var execParamPldUpdate = execParamUserId.Clone();
                        execParamPldUpdate.Record_Id = pldObj.Id;
                        var resultPldUpdate = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService().Update(execParamPldUpdate, pldObj);
                        resultPldUpdate.CheckError();
                    }
                    catch (Exception exc)
                    {
                        // Nem dobjuk tovább a hibát, csak logolunk:
                        Logger.Error("Elosztóív beírása iratpéldány címzettjébe hiba: " + exc.ToString());
                    }

                    // BUG_2742
                    //ExecParam exec_paramPldUpdate = ExecParamCaller.Clone();

                    //EREC_PldIratPeldanyokService _PldIratPeldanyokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    //Result resultPld = _PldIratPeldanyokService.GetElsoIratPeldanyByIraIrat(exec_paramPldUpdate, iratId);
                    //if (resultPld.IsError)
                    //{
                    //    Logger.Error("Iratpéldány cím update hiba! (Első iratpéldány)");
                    //    throw new ResultException(resultPld);
                    //}

                    //EREC_PldIratPeldanyok erec_PldIratPeldanyokUpdate = (EREC_PldIratPeldanyok)resultPld.Record;
                    //erec_PldIratPeldanyok.Updated.SetValueAll(false);
                    //erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

                    //erec_PldIratPeldanyokUpdate.NevSTR_Cimzett = elosztoiv.NEV;
                    //erec_PldIratPeldanyokUpdate.Updated.CimSTR_Cimzett = true;

                    //exec_paramPldUpdate.Record_Id = erec_PldIratPeldanyokUpdate.Id;

                    //Result result_pldUpdate = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService().Update(exec_paramPldUpdate, erec_PldIratPeldanyokUpdate);
                    //if (result_pldUpdate.IsError)
                    //{
                    //    Logger.Error("Iratpéldány cím update hiba!");
                    //    throw new ResultException(result_pldUpdate);
                    //}
                }
                catch (Exception exc)
                {
                    Logger.Error("Címzett elosztóív létrehozási hiba" + exc.ToString());

                    // Nem dobjuk tovább a hibát, hanem beállítjuk az cimzettekXmlCsatolasHiba out paraméterben, hogy volt hiba a tárgyszó mentésnél
                    string hibaKodCimzettXml;
                    Utility.SetHibakodHibaszoveg(exc, out hibaKodCimzettXml, out cimzettekHiba);
                }
            }

            #endregion

            Logger.InfoEnd("SZURManager.SZURGetalszamWS" + logkieg);
        }

        // Obsolete
        //public void SZUReGetalszamWS(string Ugy, string Evszam, string Konyv, string Foszam, string IratTipus, string IratTargy, string HelybenMarad, string UKSpecial, string CimzettMarad, Cimzettek Cimzettek, string UserId
        //    , out string EvszamOut, out string KonyvOut, out string FoszamOut, out string Alszam, out string Eloszam, out string cimzettekXmlCsatolasHiba)
        //{
        //    Logger.InfoStart("SZURManager.SZUReGetalszamWS");

        //    ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

        //    #region Szükséges adatok lekérése

        //    // Ügyirat lekérése vagy az iktatószám alapján, vagy pedig a könyv, évszám, főszám hármas alapján:
        //    DataRow ugyiratRow;
        //    if (!String.IsNullOrEmpty(Evszam) && !String.IsNullOrEmpty(Konyv) && !String.IsNullOrEmpty(Foszam))
        //    {
        //        ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
        //    }
        //    else
        //    {
        //        ugyiratRow = this.GetUgyiratRow(Ugy, execParamUserId);
        //    }
        //    Guid ugyiratId = (Guid)ugyiratRow["Id"];

        //    Guid iktatokonyvId = (Guid)ugyiratRow["IraIktatokonyv_Id"];
        //    var iktatokonyv = this.GetIktatokonyv(iktatokonyvId, true);

        //    #endregion

        //    #region Iktatáshoz szükséges objektumok feltöltése

        //    EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
        //    // összes mező update-elhetőségét kezdetben letiltani:
        //    erec_IraIratok.Updated.SetValueAll(false);
        //    erec_IraIratok.Base.Updated.SetValueAll(false);

        //    erec_IraIratok.Irattipus = IratTipus;
        //    erec_IraIratok.Updated.Irattipus = true;

        //    // Tárgy:
        //    erec_IraIratok.Targy = IratTargy;
        //    erec_IraIratok.Updated.Targy = true;

        //    EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
        //    erec_PldIratPeldanyok.Updated.SetValueAll(false);
        //    erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

        //    // Cimzettek-ből az elsőt kivesszük, az lesz az iratpéldány címzettje, a többit xml fájlként csatoljuk az irathoz
        //    if (Cimzettek != null
        //        && Cimzettek.Cimzett != null
        //        && Cimzettek.Cimzett.Length > 0)
        //    {
        //        Dictionary<string, Guid> kapcsolatiKodPartnerIdDict;
        //        List<string> kapcsolatiKodokList = Cimzettek.Cimzett.Where(e => !String.IsNullOrEmpty(e.KapcsolattartoAzonosito))
        //                                                        .Select(e => e.KapcsolattartoAzonosito).ToList();

        //        // Kell-e partnertörzsben ellenőrizni a kapcsolati kódot (Cimzett.KapcsolattartoAzonosito)
        //        if (UKSpecial != "igen")
        //        {
        //            // Kell kapcsolati kód ellenőrzés:                    
        //            this.PartnerKapcsolatiKodokEllenorzes(kapcsolatiKodokList, out kapcsolatiKodPartnerIdDict);
        //        }
        //        else
        //        {
        //            kapcsolatiKodPartnerIdDict = this.GetPartnerIdsForKapcsolatiKodok(kapcsolatiKodokList);
        //        }

        //        Cimzett cimzett = Cimzettek.Cimzett[0];

        //        erec_PldIratPeldanyok.NevSTR_Cimzett = cimzett.Nev;
        //        erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;

        //        // Partner_Id_Cimzett:
        //        if (!String.IsNullOrEmpty(cimzett.KapcsolattartoAzonosito)
        //            && kapcsolatiKodPartnerIdDict.ContainsKey(cimzett.KapcsolattartoAzonosito))
        //        {
        //            erec_PldIratPeldanyok.Partner_Id_Cimzett = kapcsolatiKodPartnerIdDict[cimzett.KapcsolattartoAzonosito].ToString();
        //            erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;
        //        }

        //        if (String.IsNullOrEmpty(cimzett.Nev))
        //        {
        //            erec_PldIratPeldanyok.NevSTR_Cimzett = cimzett.Szervezet;
        //        }

        //        // Teljes cím kell:
        //        erec_PldIratPeldanyok.CimSTR_Cimzett = cimzett.GetTeljesCim();
        //        erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = true;

        //        if (cimzett.Elektronikus == "I")
        //        {
        //            erec_IraIratok.UgyintezesAlapja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
        //            erec_IraIratok.Updated.UgyintezesAlapja = true;

        //            erec_IraIratok.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
        //            erec_IraIratok.Updated.AdathordozoTipusa = true;
        //        }
        //        else if (cimzett.Elektronikus == "N")
        //        {
        //            erec_IraIratok.UgyintezesAlapja = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
        //            erec_IraIratok.Updated.UgyintezesAlapja = true;

        //            erec_IraIratok.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
        //            erec_IraIratok.Updated.AdathordozoTipusa = true;
        //        }

        //        // Expediálás módja:
        //        erec_PldIratPeldanyok.KuldesMod = cimzett.cExpedialasMod;
        //        erec_PldIratPeldanyok.Updated.KuldesMod = true;
        //    }

        //    #endregion

        //    var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

        //    // Iktatás webservice hívása:
        //    var resultIktatas = iratokService.BelsoIratIktatasa_Alszamra(execParamUserId, iktatokonyvId.ToString(), ugyiratId.ToString()
        //                                , new EREC_UgyUgyiratdarabok(), erec_IraIratok, new EREC_HataridosFeladatok()
        //                                , erec_PldIratPeldanyok, new IktatasiParameterek());
        //    resultIktatas.CheckError();

        //    Guid iratId = new Guid(resultIktatas.Uid);

        //    #region Out paraméterek feltöltése

        //    // Létrejött irat objektum lekérése:
        //    var ujIratObj = this.GetIratObjById(iratId);

        //    Alszam = ujIratObj.Alszam;
        //    // TODO: Ügyirat ügyintéző csoportjában lesz ez az adat
        //    Eloszam = String.Empty;

        //    EvszamOut = iktatokonyv.Ev;
        //    KonyvOut = iktatokonyv.Iktatohely;
        //    FoszamOut = ugyiratRow["Foszam"].ToString();

        //    cimzettekXmlCsatolasHiba = null;

        //    #endregion

        //    #region Cimzettek xml fájlként csatolása az irathoz

        //    /// Cimzettek paraméter:
        //    /// xml fájlba le kellene tárolni, és csatolmányként hozzákapcsolni az irathoz
        //    /// 
        //    if (Cimzettek != null
        //        && Cimzettek.Cimzett != null
        //        && Cimzettek.Cimzett.Length > 0)
        //    {
        //        try
        //        {
        //            // Cimzettek objektum serializálása xml stringgé:
        //            string cimzettekXml = Utility.SerializeObjToXML(Cimzettek);

        //            Csatolmany csatolmany = new Csatolmany();
        //            // Xml szöveg binárissá alakítása:
        //            csatolmany.Tartalom = System.Text.Encoding.UTF8.GetBytes(cimzettekXml);
        //            csatolmany.Nev = "cimzettek.xml";

        //            ExecParam execParamCsatolmany = execParamUserId.Clone();
        //            execParamCsatolmany.Record_Id = iratId.ToString();

        //            EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
        //            erec_Csatolmanyok.Updated.SetValueAll(false);
        //            erec_Csatolmanyok.Base.Updated.SetValueAll(false);

        //            erec_Csatolmanyok.IraIrat_Id = iratId.ToString();
        //            erec_Csatolmanyok.Updated.IraIrat_Id = true;

        //            erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Segeddokumentum;
        //            erec_Csatolmanyok.Updated.DokumentumSzerep = true;

        //            erec_Csatolmanyok.Leiras = "Címzettek (xml)";
        //            erec_Csatolmanyok.Updated.Leiras = true;

        //            // Csatolmány feltöltése:
        //            var resultCsatolmany = iratokService.CsatolmanyUpload(execParamCsatolmany, erec_Csatolmanyok, csatolmany);
        //            resultCsatolmany.CheckError();
        //        }
        //        catch (Exception exc)
        //        {
        //            Logger.Error(exc.ToString());

        //            // Nem dobjuk tovább a hibát, hanem beállítjuk az cimzettekXmlCsatolasHiba out paraméterben, hogy volt hiba a tárgyszó mentésnél
        //            string hibaKodCimzettXml;
        //            Utility.SetHibakodHibaszoveg(exc, out hibaKodCimzettXml, out cimzettekXmlCsatolasHiba);
        //        }
        //    }

        //    #endregion

        //    Logger.InfoEnd("SZURManager.SZUReGetalszamWS");
        //}

        #endregion

        /// <summary>
        /// Az ügy vagy irat kiadmányozási állapotának ellenőrzése
        /// (Az irat Aláírók fülön az a sor kell, ahol az Aláíró szerepe = Jóváhagyó)
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Jovahagyo"></param>
        /// <param name="Jovahagyva"></param>
        public void SZURChkKiadmWS(string Evszam, string Konyv, string Foszam, string Alszam, out string Jovahagyo, out string Jovahagyva)
        {
            Logger.InfoStart("SZURManager.SZURChkKiadmWS");

            DataRow iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam);
            Guid iratId = (Guid)iratRow["Id"];

            #region Irat aláírók lekérdezése

            // Az irat Aláírók fülön az a sor kell, ahol az Aláíró szerepe = Kiadmanyozo

            var iratAlairokService = eUtility.eRecordService.ServiceFactory.GetEREC_IratAlairokService();

            var searchObjIratAlairok = new EREC_IratAlairokSearch();

            searchObjIratAlairok.Obj_Id.Value = iratId.ToString();
            searchObjIratAlairok.Obj_Id.Operator = Query.Operators.equals;

            searchObjIratAlairok.AlairoSzerep.Value = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
            searchObjIratAlairok.AlairoSzerep.Operator = Query.Operators.equals;

            var resultAlairok = iratAlairokService.GetAllWithExtension(this.ExecParamCaller, searchObjIratAlairok);

            if (resultAlairok.IsError)
            {
                throw new ResultException(resultAlairok);
            }

            #endregion

            // Out paraméterek értékadása:

            // Ha nincs ilyen aláírás az iraton, akkor üres értékkel megy vissza a hívás:
            if (resultAlairok.Ds.Tables[0].Rows.Count == 0)
            {
                Jovahagyo = String.Empty;
                Jovahagyva = String.Empty;
            }
            else
            {
                DataRow rowAlairas = resultAlairok.Ds.Tables[0].Rows[0];

                Jovahagyo = rowAlairas["Alairo_Nev"].ToString();
                Jovahagyva = Helper.GetFormattedDateAndTimeString(rowAlairas["AlairasDatuma"]);
            }

            Logger.InfoEnd("SZURManager.SZURChkKiadmWS");
        }

        /// <summary>
        /// A jogerősítő által láttamozandó alszámos iratok teljes listája 
        /// "Az aktuális évi iktatókönyvben adja vissza a jogerősítők által még nem láttamozott összes irat listáját."
        /// --> Azok az iratok kellenek, ahol az aláírók fülön be van jegyezve a láttamozó, de a státusza még "Aláírandó",
        /// plusz szűrni kell a megadott ügyintézőre is.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public LattamozIrat[] SZURGetJogeroLattamozandoWS(string userId)
        {
            Logger.InfoStart("SZURManager.SZURGetJogeroLattamozandoWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(userId);

            #region Iratok lekérése

            var iratokService = eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();

            /// BUG#3439/Task#3552: nem az aláírókból kell lekérdezni, hanem
            /// azokat az iratokat kell visszaadni, ahol be van állítva a "Jogerősítendő" mező (metaadat), ki van töltve a "Jogerősítést végző", de a "Jogerősítést végző látta" mező még üres.
            /// (Mindhárom mező a metaadatok között van.)
            /// 

            // Lekérjük a láttamozandó iratokat (csak irat Id-kat lehet belőle kinyerni, az irat adatokhoz plusz lekérdezés kell):
            var resultLattamozandoIratok = iratokService.GetAllJogeroLattamozandoIratok(execParamUserId);
            resultLattamozandoIratok.CheckError();

            // Irat Id-k kigyűjtése:
            List<Guid> iratIdList = new List<Guid>();
            foreach (DataRow rowLattamozando in resultLattamozandoIratok.Ds.Tables[0].Rows)
            {
                iratIdList.Add((Guid)rowLattamozando["Obj_Id"]);
            }

            // Ha nincs találat, üres tömböt adunk vissza:
            if (iratIdList.Count == 0)
            {
                return new LattamozIrat[] { };
            }

            var searchObjIratok = new EREC_IraIratokSearch();

            // Szűrés a kigyűjtött irat Id-kra:
            searchObjIratok.Id.Value = "'" + String.Join("','", iratIdList.Select(e => e.ToString()).ToArray()) + "'";
            searchObjIratok.Id.Operator = Query.Operators.inner;

            //searchObjIratok.Extended_EREC_IratAlairokSearch.AlairoSzerep.Value = KodTarak.ALAIRO_SZEREP.Lattamozo;
            //searchObjIratok.Extended_EREC_IratAlairokSearch.AlairoSzerep.Operator = Query.Operators.equals;

            //searchObjIratok.Extended_EREC_IratAlairokSearch.Allapot.Value = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
            //searchObjIratok.Extended_EREC_IratAlairokSearch.Allapot.Operator = Query.Operators.equals;

            //// Szűrni kell az aláíróra:
            //searchObjIratok.Extended_EREC_IratAlairokSearch.FelhasznaloCsoport_Id_Alairo.Value = execParamUserId.Felhasznalo_Id;
            //searchObjIratok.Extended_EREC_IratAlairokSearch.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;

            var resultIratok = iratokService.GetAllWithExtension(execParamUserId, searchObjIratok);

            if (resultIratok.IsError)
            {
                throw new ResultException(resultIratok);
            }

            #endregion

            // Eredmény lista összeállítása:

            List<LattamozIrat> iratList = new List<LattamozIrat>();

            /// Az ügyintéző szervezetének és felhasználónevének feloldásához az Id-k és a hozzátartozó LattamozIrat objektumok kigyűjtése
            /// Felhasználó Id - objektum lista párok
            Dictionary<Guid, List<LattamozIrat>> ugyintezoIdSeged = new Dictionary<Guid, List<LattamozIrat>>();
            /// Irat id - objektum párok
            Dictionary<Guid, LattamozIrat> iratObjSeged = new Dictionary<Guid, LattamozIrat>();

            foreach (DataRow rowIrat in resultIratok.Ds.Tables[0].Rows)
            {
                Guid iratId = (Guid)rowIrat["Id"];

                // többször is hozhatja ugyanazt az iratot a lekérdezés
                if (iratObjSeged.ContainsKey(iratId))
                    continue;

                LattamozIrat lattamozIrat = new LattamozIrat();
                iratList.Add(lattamozIrat);

                lattamozIrat.KESZRE = string.Empty;
                lattamozIrat.JOVAHAGYVA = string.Empty;
                lattamozIrat.KIKULDVE = string.Empty;
                lattamozIrat.Eloszam = string.Empty;

                iratObjSeged.Add(iratId, lattamozIrat);

                lattamozIrat.Foszam = rowIrat["Foszam"].ToString();
                lattamozIrat.Alszam = rowIrat["Alszam"].ToString();
                lattamozIrat.Evszam = rowIrat["Ev"].ToString();

                lattamozIrat.UI_SZERV = string.Empty;
                lattamozIrat.UI_NEV = rowIrat["FelhasznaloCsoport_Id_Ugyintezo_Nev"].ToString();
                lattamozIrat.UI_NID = string.Empty;

                #region ELOSZAM

                DataRow drUgy = GetUgyiratRow(new Guid(rowIrat["Ugyirat_Id"].ToString()));
                if (drUgy != null && !string.IsNullOrEmpty(drUgy["Csoport_Id_Ugyfelelos"].ToString()))
                {
                    DataRow drCsoport = this.GetCsoportRowById(drUgy["Csoport_Id_Ugyfelelos"].ToString());
                    if (drCsoport != null)
                    {
                        if (!string.IsNullOrEmpty(drCsoport["Kod"].ToString()))
                            lattamozIrat.Eloszam = drCsoport["Kod"].ToString();
                        else
                            lattamozIrat.Eloszam = drCsoport["Nev"].ToString();
                    }
                }
                #endregion

                Guid? ugyintezoId = rowIrat["FelhasznaloCsoport_Id_Ugyintez"] as Guid?;

                if (ugyintezoId != null)
                {
                    List<LattamozIrat> objList;
                    if (ugyintezoIdSeged.ContainsKey(ugyintezoId.Value))
                    {
                        objList = ugyintezoIdSeged[ugyintezoId.Value];
                    }
                    else
                    {
                        objList = new List<LattamozIrat>();
                        ugyintezoIdSeged.Add(ugyintezoId.Value, objList);
                    }

                    objList.Add(lattamozIrat);
                }

                lattamozIrat.KESZRE = Helper.GetFormattedDateAndTimeString(rowIrat["IntezesIdopontja"]);

                // JOVAHAGYVA mező: Aláírásokat kell hozzá lekérni, ott lesz beállítva
                // KIKULDVE mező: Iratpéldányokat kell hozzá lekérni, ott lesz beállítva                
            }

            #region Ügyintéző felhasználó szervezet és login név lekérése

            if (ugyintezoIdSeged.Count > 0)
            {
                string felhasznaloIdSearchStr = "'" + String.Join("','", ugyintezoIdSeged.Select(e => e.Key.ToString()).ToArray()) + "'";

                var csoportTagokService = eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

                var searchObjCsoportTagok = new KRT_CsoportTagokSearch();

                searchObjCsoportTagok.Csoport_Id_Jogalany.Value = felhasznaloIdSearchStr;
                searchObjCsoportTagok.Csoport_Id_Jogalany.Operator = Query.Operators.inner;

                var resultCsoportTagok = csoportTagokService.GetAllWithExtension(this.ExecParamCaller, searchObjCsoportTagok);
                if (resultCsoportTagok.IsError)
                {
                    throw new ResultException(resultCsoportTagok);
                }

                foreach (DataRow rowCst in resultCsoportTagok.Ds.Tables[0].Rows)
                {
                    Guid felhId = (Guid)rowCst["Csoport_Id_Jogalany"];

                    // Szervezetnév beállítása az összes érintett objektumban (ha esetleg több szervezete is van, akkor az utolsó felülírja a többit...)
                    ugyintezoIdSeged[felhId].ForEach(e =>
                                        {
                                            e.UI_SZERV = rowCst["Csoport_Nev"].ToString();
                                        });

                    //foreach(LattamozIrat lattamozIrat in ugyintezoIdSeged[felhId])
                    //{
                    //    lattamozIrat.UI_SZERV = rowCst["Csoport_Nev"].ToString();
                    //}
                }

                // UserNev-ek lekérése:
                var felhasznaloService = eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

                var searchObjFel = new KRT_FelhasznalokSearch();
                searchObjFel.Id.Value = felhasznaloIdSearchStr;
                searchObjFel.Id.Operator = Query.Operators.inner;

                var resultFelh = felhasznaloService.GetAll(this.ExecParamCaller, searchObjFel);
                if (resultFelh.IsError)
                {
                    throw new ResultException(resultFelh);
                }

                foreach (DataRow rowFelh in resultFelh.Ds.Tables[0].Rows)
                {
                    Guid felhId = (Guid)rowFelh["Id"];

                    // Loginnév beállítása az összes érintett objektumon:
                    ugyintezoIdSeged[felhId].ForEach(e =>
                                        {
                                            e.UI_NID = rowFelh["UserNev"].ToString();
                                        });
                }
            }

            #endregion

            #region Irat aláírók lekérése a jóváhagyás dátumához

            string iratIdSearchStr = String.Empty;
            if (iratObjSeged.Count > 0)
            {
                iratIdSearchStr = "'" + String.Join("','", iratObjSeged.Select(e => e.Key.ToString()).ToArray()) + "'";
            }

            if (iratObjSeged.Count > 0)
            {
                // Az irat Aláírók fülön az a sor kell, ahol az Aláíró szerepe = Jóváhagyó (BUG#5725: a Kiadmányozó kell)

                var iratAlairokService = eUtility.eRecordService.ServiceFactory.GetEREC_IratAlairokService();

                var searchObjIratAlairok = new EREC_IratAlairokSearch();

                searchObjIratAlairok.Obj_Id.Value = iratIdSearchStr;
                searchObjIratAlairok.Obj_Id.Operator = Query.Operators.inner;

                // BUG#5725: "4. kiadmányozás dátumát kell a JOVAHAGYVA mezőben átadni"
                searchObjIratAlairok.AlairoSzerep.Value = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
                searchObjIratAlairok.AlairoSzerep.Operator = Query.Operators.equals;

                searchObjIratAlairok.Allapot.Value = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                searchObjIratAlairok.Allapot.Operator = Query.Operators.equals;

                var resultAlairok = iratAlairokService.GetAll(this.ExecParamCaller, searchObjIratAlairok);

                if (resultAlairok.IsError)
                {
                    throw new ResultException(resultAlairok);
                }

                // Eredmény feldolgozása:
                foreach (DataRow rowIratAlairok in resultAlairok.Ds.Tables[0].Rows)
                {
                    Guid iratId = (Guid)rowIratAlairok["Obj_Id"];

                    iratObjSeged[iratId].JOVAHAGYVA = rowIratAlairok["AlairasDatuma"].ToString();
                }
            }
            #endregion

            #region Iratpéldányok lekérése a kiküldés dátumához

            if (iratObjSeged.Count > 0)
            {
                // Az iratok elsődleges iratpéldányai kellenek:
                // BUG#5725: Nem feltétlenül az elsődleges iratpéldány kell, hanem az iratpéldányok közül az utoljára kiküldött

                var iratPeldanyService = eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

                var searchObjIratPld = new EREC_PldIratPeldanyokSearch();

                searchObjIratPld.IraIrat_Id.Value = iratIdSearchStr;
                searchObjIratPld.IraIrat_Id.Operator = Query.Operators.inner;

                //searchObjIratPld.Sorszam.Value = "1";
                //searchObjIratPld.Sorszam.Operator = Query.Operators.equals;

                searchObjIratPld.PostazasDatuma.Value = "";
                searchObjIratPld.PostazasDatuma.Operator = Query.Operators.notnull;

                var resultPld = iratPeldanyService.GetAll(this.ExecParamCaller, searchObjIratPld);
                if (resultPld.IsError)
                {
                    throw new ResultException(resultPld);
                }

                // Eredmény feldolgozása:
                foreach (DataRow rowPld in resultPld.Ds.Tables[0].Rows)
                {
                    Guid iratId = (Guid)rowPld["IraIrat_Id"];

                    DateTime? postazasDat = rowPld["PostazasDatuma"] as DateTime?;
                    if (postazasDat != null)
                    {
                        string meglevoKikuldveDatStr = iratObjSeged[iratId].KIKULDVE;
                        DateTime? meglevoKikuldveDat = null;
                        if (!String.IsNullOrEmpty(meglevoKikuldveDatStr))
                        {
                            DateTime dtSeged;
                            if (DateTime.TryParse(meglevoKikuldveDatStr, out dtSeged))
                            {
                                meglevoKikuldveDat = dtSeged;
                            }
                        }
                        if (meglevoKikuldveDat == null
                            || meglevoKikuldveDat < postazasDat)
                        {
                            iratObjSeged[iratId].KIKULDVE = Helper.GetFormattedDateAndTimeString(postazasDat);
                        }
                    }
                }
            }

            #endregion

            LattamozIrat[] resultList = iratList.ToArray();

            Logger.InfoEnd("SZURManager.SZURGetJogeroLattamozandoWS");

            return resultList;
        }

        /// <summary>
        /// Az irat más szervezeti egységhez történő átadásának kezdeményezése
        /// "Feladat: A megadott iratot, vagy ügyiratcsomagot felkínálja átvételre a FelhivottSzerv paraméterben kijelölt szervezeti egység számára."
        /// --> Átadásra kijelölés hívása. Ha meg van adva az alszám, akkor az irat elsődleges iratpéldányára hívjuk meg,
        /// egyébként az ügyiratra hívjuk meg az átadásra kijelölést.
        /// BUG#3493/Task#3500: Nem átadásra kijelölés kell, hanem átadás
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="FelhivoSzerv"></param>
        /// <param name="FelhivottSzerv"></param>
        /// <param name="KiadmanyozoUserId"></param>
        public void SZURFelhivasAtvetelWS(string Evszam, string Konyv, string Foszam, string Alszam, string FelhivoSzerv, string FelhivottSzerv, string KiadmanyozoUserId)
        {
            Logger.InfoStart("SZURManager.SZURFelhivasAtvetelWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(KiadmanyozoUserId);

            // TODO: jogosultság-ellenőrzés

            // csoport_Id_Felelos_Kovetkezo meghatározása:
            DataRow csoportRowKovFelelos = this.GetCsoportRowByNev(FelhivottSzerv);
            Guid csoportIdKovFelelos = (Guid)csoportRowKovFelelos["Id"];

            if (String.IsNullOrEmpty(Alszam)
                || Alszam == "0")
            {
                #region Ügyirat átadásra kijelölése

                // Ügyirat lekérése:
                DataRow ugyiratRow = GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
                Guid ugyiratId = (Guid)ugyiratRow["Id"];

                var ugyiratService = eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                // Átadás hívása:
                var resultAtadas = ugyiratService.Atadas(execParamUserId, ugyiratId.ToString(), csoportIdKovFelelos.ToString(), null);

                if (resultAtadas.IsError)
                {
                    throw new ResultException(resultAtadas);
                }

                #endregion
            }
            else
            {
                #region Iratpéldányok átadása

                // Iratpéldányok lekérése:
                DataRowCollection iratPeldanyRows = GetAllIratPeldanyRows(Evszam, Konyv, Foszam, Alszam, execParamUserId);
                EREC_PldIratPeldanyokService iratPeldanyService = eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                if (iratPeldanyRows != null && iratPeldanyRows.Count > 0)
                    for (int i = 0; i < iratPeldanyRows.Count; i++)
                    {
                        Guid iratpeldanyId = (Guid)iratPeldanyRows[i]["Id"];
                        ErrorDetails errorDetail;
                        var iratPeldanyStatusz = Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotById(iratpeldanyId.ToString(), execParamUserId, null);

                        // Azokra hívjuk csak meg, amik átadhatók (átadásra kijelölhetők):
                        if (Contentum.eRecord.BaseUtility.IratPeldanyok.AtadasraKijelolheto(
                                iratPeldanyStatusz, execParamUserId, out errorDetail))
                        {
                            // Átadás hívása:
                            Result resultAtadas = iratPeldanyService.Atadas(execParamUserId, iratpeldanyId.ToString(), csoportIdKovFelelos.ToString(), null);
                            resultAtadas.CheckError();
                        }
                        else
                        {
                            Logger.Debug(String.Format("SZURFelhivasAtvetelWS - Az iratpéldány átadása nem hívódott meg, mert AtadasraKijelolheto(...) == false."
                                + " IratpéldányId: '{0}'", iratpeldanyId.ToString()));
                        }
                    }

                #endregion
            }

            Logger.InfoEnd("SZURManager.SZURFelhivasAtvetelWS");
        }

        /// <summary>
        /// Munkanap nyilvántartás figyelembevételével határidő számítási szolgáltatás
        /// "Az átadott paraméterek alapján vagy a kezdő időpont és a vég időpont közötti munkanapok vagy naptári napok számát adja meg,
        /// vagy a kezdőidőpont és a napok valamint a számítás módja alapján megadja a befejező időpontot."
        /// </summary>
        /// <param name="Kezdete"></param>
        /// <param name="Vege"></param>
        /// <param name="Napok"></param>
        /// <param name="HatidoMod"></param>
        public void SZURGetHataridoWS(ref string Kezdete, ref string Vege, ref string Napok, string HatidoMod)
        {
            Logger.InfoStart("SZURManager.SZURGetHataridoWS");

            if (String.IsNullOrEmpty(HatidoMod))
            {
                throw new ArgumentNullException("HatidoMod");
            }

            bool munkaNapSzamitas;
            if (HatidoMod == "0")
            {
                munkaNapSzamitas = true;
            }
            else if (HatidoMod == "1")
            {
                munkaNapSzamitas = false;
            }
            else
            {
                // Hiba:
                throw new ResultException("A HatidoMod mező lehetséges értékei: '0' vagy '1'");
            }

            #region Adatkonverzió

            // A dátumokat "yyyy.MM.dd" formátumban várjuk. Ha nem így jön, hibát dobunk:

            DateTime? kezdDat = null;
            DateTime datumSeged;
            if (!String.IsNullOrEmpty(Kezdete))
            {
                if (DateTime.TryParseExact(Kezdete, SZURWSDateFormatString, CultureInfo.InvariantCulture, DateTimeStyles.None, out datumSeged))
                {
                    kezdDat = datumSeged;
                }
                else
                {
                    throw new ResultException("A várt dátumformátum: " + SZURWSDateFormatString);
                }
            }

            DateTime? vegDat = null;
            if (!String.IsNullOrEmpty(Vege))
            {
                if (DateTime.TryParseExact(Vege, SZURManager.SZURWSDateFormatString, CultureInfo.InvariantCulture, DateTimeStyles.None, out datumSeged))
                {
                    vegDat = datumSeged;
                }
                else
                {
                    throw new ResultException("A várt dátumformátum: " + SZURManager.SZURWSDateFormatString);
                }
            }

            int? napokSzama = null;
            if (!String.IsNullOrEmpty(Napok))
            {
                napokSzama = int.Parse(Napok);
            }

            #endregion

            #region Számítás

            if (napokSzama == null)
            {
                // A másik két adatnak kitöltöttnek kell lennie:
                if (kezdDat == null || vegDat == null)
                {
                    throw new ResultException("Nincs megadva elegendő adat a számításhoz!");
                }

                if (!munkaNapSzamitas)
                {
                    int naptariNapokSzama = (int)(vegDat.Value - kezdDat.Value).TotalDays;

                    // Naptári nap különbség:
                    napokSzama = naptariNapokSzama;
                }
                else
                {
                    // Munkanap számítás:
                    napokSzama = this.GetMunkanapokSzama(kezdDat.Value, vegDat.Value);
                }
            }
            else if (vegDat == null)
            {
                // A másik két adatnak kitöltöttnek kell lennie:
                if (kezdDat == null || napokSzama == null)
                {
                    throw new ResultException("Nincs megadva elegendő adat a számításhoz!");
                }

                if (munkaNapSzamitas)
                {
                    vegDat = this.GetKovetkezoMunkanap(kezdDat.Value, napokSzama.Value);
                }
                else
                {
                    // Naptári nap számítás:
                    vegDat = kezdDat.Value.AddDays(napokSzama.Value);
                }
            }
            else if (kezdDat == null)
            {
                // A másik két adatnak kitöltöttnek kell lennie:
                if (vegDat == null || napokSzama == null)
                {
                    throw new ResultException("Nincs megadva elegendő adat a számításhoz!");
                }

                if (munkaNapSzamitas)
                {
                    kezdDat = this.GetKovetkezoMunkanap(vegDat.Value, napokSzama.Value);
                }
                else
                {
                    // Naptári nap számítás:
                    kezdDat = kezdDat.Value.AddDays(0 - napokSzama.Value);
                }
            }
            else
            {
                // Mindhárom adat ki van töltve, melyiket kellene számolni?
                throw new ResultException("A 'Kezdete', 'Vege', 'Napok' mezők közül egynek kitöltetlennek kell lennie!");
            }

            #endregion

            // Kimenő paraméterértékek beállítása:
            Kezdete = Helper.GetFormattedDateAndTimeString(kezdDat.Value);
            Vege = Helper.GetFormattedDateAndTimeString(vegDat.Value);
            Napok = napokSzama.ToString();

            Logger.InfoEnd("SZURManager.SZURGetHataridoWS");
        }

        /// <summary>
        /// Szerelési adatok lekérdezése
        /// "Adott ügyszám alapján átadja az aktuális ügyre vonatkozó szerelési adatokat (elő- és utóiratok) a szakrendszer részére."
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="UserId"></param>
        /// <param name="UtoIrat"></param>
        /// <param name="EloIratok"></param>
        public void SZURGetSzerelesiAdatokWS(string Evszam, string Konyv, string Foszam, string UserId, out Foszam2[] UtoIrat, out Foszam2[] EloIratok)
        {
            Logger.InfoStart("SZURManager.SZURGetSzerelesiAdatokWS");

            /// Az adott ügyirat előiratai és utóiratának lekérdezése:
            /// - Ügyirat lekérése az azonosítói alapján
            /// - Összes előzmény ügyirat lekérése: EREC_UgyUgyiratokService.GetSzuloHiearchy ?
            /// - Utóirat lekérése (EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo)"
            /// 

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            // Ügyirat lekérése:
            DataRow ugyiratRow = GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
            Guid ugyiratId = (Guid)ugyiratRow["Id"];

            // Teljes szerelési lánc lekérése:
            var execParamSzereltek = execParamUserId.Clone();
            execParamSzereltek.Record_Id = ugyiratId.ToString();
            var resultSzerelesiLanc = eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().GetAllSzereltByNode(execParamSzereltek);

            if (resultSzerelesiLanc.IsError)
            {
                throw new ResultException(resultSzerelesiLanc);
            }

            #region Szerelési lánc feldolgozása

            // ügyiratId - (ügyirat DataRow, előzmény ügyiratId lista)            
            Dictionary<Guid, Tuple<DataRow, List<Guid>>> szereltUgyiratAdatok = new Dictionary<Guid, Tuple<DataRow, List<Guid>>>();

            foreach (DataRow row in resultSzerelesiLanc.Ds.Tables[0].Rows)
            {
                Guid szereltUgyId = (Guid)row["Id"];
                Guid? ugyiratIdSzulo = row["UgyUgyirat_Id_Szulo"] as Guid?;

                if (!szereltUgyiratAdatok.ContainsKey(szereltUgyId))
                {
                    szereltUgyiratAdatok.Add(szereltUgyId, new Tuple<DataRow, List<Guid>>(row
                            , new List<Guid>()));
                }
                else
                {
                    // DataRow beállítása:
                    szereltUgyiratAdatok[szereltUgyId].Item1 = row;
                }

                if (ugyiratIdSzulo != null)
                {
                    // Bejegyzés a szülő ügyirat előzményei közé:

                    // Ha az még nem létezik, létrehozzuk:
                    if (!szereltUgyiratAdatok.ContainsKey(ugyiratIdSzulo.Value))
                    {
                        szereltUgyiratAdatok.Add(ugyiratIdSzulo.Value, new Tuple<DataRow, List<Guid>>(null, new List<Guid>()));
                    }

                    // Hozzáadás az előzmény listához:
                    szereltUgyiratAdatok[ugyiratIdSzulo.Value].Item2.Add(szereltUgyId);
                }
            }

            // Előiratok összeszedése:
            Func<List<Guid>, List<Foszam2>> getElozmenyFoszamok = null;
            /// A megadott előzmény ügyirat Id-kból Foszam2 objektumok létrehozása, 
            /// és mindegyiknél azoknak is megnézzük az előzményeit, és azokat is a listához adjuk (rekurzív hívások)
            /// 
            getElozmenyFoszamok = (elozmenyUgyIdList) =>
                            {
                                List<Foszam2> foszamList = new List<Foszam2>();

                                foreach (Guid elozmenyUgyId in elozmenyUgyIdList)
                                {
                                    // Előzmény ügyhoz tartozó DataRow:
                                    DataRow rowElozmeny = szereltUgyiratAdatok[elozmenyUgyId].Item1;

                                    var iktatoKonyv = GetIktatokonyv((Guid)rowElozmeny["IraIktatokonyv_Id"], true);

                                    Foszam2 foszamObj = new Foszam2()
                                    {
                                        Evszam = iktatoKonyv.Ev,
                                        Foszam = rowElozmeny["Foszam"].ToString(),
                                        Konyv = iktatoKonyv.Iktatohely
                                    };

                                    foszamList.Add(foszamObj);

                                    // Rekurzív függvényhívás, ha vannak ennek is előzmény ügyiratai, azokat is hozzáadjuk a listához:
                                    List<Guid> elozmenyElozmenyei = szereltUgyiratAdatok[elozmenyUgyId].Item2;
                                    if (elozmenyElozmenyei != null
                                        && elozmenyElozmenyei.Count > 0)
                                    {
                                        foszamList.AddRange(getElozmenyFoszamok(elozmenyElozmenyei));
                                    }
                                }

                                return foszamList;
                            };

            // Előiratok:
            EloIratok = getElozmenyFoszamok(szereltUgyiratAdatok[ugyiratId].Item2).ToArray();

            /// Utóirat megkeresése, ha van:
            /// Nem a teljes lánc kell, hanem a legutolsó:
            /// 

            Guid? szuloUgyiratId = ugyiratRow["UgyUgyirat_Id_Szulo"] as Guid?;
            Foszam2 utoUgyiratObj = null;
            while (szuloUgyiratId != null)
            {
                // Megnézzük, ennek van-e szülője:
                DataRow szuloUgyiratDataRow = szereltUgyiratAdatok[szuloUgyiratId.Value].Item1;
                // A szülő ügyirat szülő ügyiratId-ja:
                Guid? szuloUgyiratSzuloUgyiratId = szuloUgyiratDataRow["UgyUgyirat_Id_Szulo"] as Guid?;

                if (szuloUgyiratSzuloUgyiratId == null)
                {
                    // Megvan a vége, létrehozzuk az objektumot:
                    var iktatoKonyv = this.GetIktatokonyv((Guid)szuloUgyiratDataRow["IraIktatokonyv_Id"], true);
                    utoUgyiratObj = new Foszam2()
                    {
                        Evszam = iktatoKonyv.Ev,
                        Foszam = szuloUgyiratDataRow["Foszam"].ToString(),
                        Konyv = iktatoKonyv.Iktatohely
                    };
                }

                szuloUgyiratId = szuloUgyiratSzuloUgyiratId;
            }

            UtoIrat = new Foszam2[] { };

            if (utoUgyiratObj != null)
            {
                UtoIrat = new Foszam2[] { utoUgyiratObj };
            }

            #endregion

            Logger.InfoEnd("SZURManager.SZURGetSzerelesiAdatokWS");
        }

        /// <summary>
        /// Ügyek összeszerelése
        /// "A szakrendszertől kapott összeszerelendő ügyiratokat a SZÜR összeszereli."
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam">Felesleges paraméter, értékét nem vesszük figyelembe</param>
        /// <param name="EloIratok"></param>
        /// <param name="UserId"></param>
        public void SZUReUgyekSzereleseWS(string Evszam, string Konyv, string Foszam, string Alszam, Foszam2[] EloIratok, string UserId)
        {
            Logger.InfoStart("SZURManager.SZUReUgyekSzereleseWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            // Ügyirat lekérése:
            DataRow ugyiratRow = GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
            Guid ugyiratId = (Guid)ugyiratRow["Id"];

            List<Guid> eloUgyiratIdList = new List<Guid>();
            // Előügyirat Id-k összegyűjtése:
            foreach (var eloUgyirat in EloIratok)
            {
                DataRow eloUgyiratRow = GetUgyiratRow(eloUgyirat.Evszam, eloUgyirat.Konyv, eloUgyirat.Foszam);
                Guid eloUgyiratId = (Guid)eloUgyiratRow["Id"];

                eloUgyiratIdList.Add(eloUgyiratId);
            }

            // WS hívás:
            var resultSzereles = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService()
                                                    .SzerelesTomeges(execParamUserId, eloUgyiratIdList.ToArray(), ugyiratId);

            if (resultSzereles.IsError)
            {
                throw new ResultException(resultSzereles);
            }

            Logger.InfoEnd("SZURManager.SZUReUgyekSzereleseWS");
        }

        /// <summary>
        /// Iratcsomag határidős irattárba helyezésének javaslata (skontró)
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="IratHatidoHely"></param>
        /// <param name="IratHatido"></param>
        /// <param name="IratHatidoOk"></param>
        /// <param name="Torles"></param>
        /// <param name="UserId"></param>
        public void SZURSetIratHatIdoIratTarWS(string Konyv, string Foszam, string Evszam, string IratHatidoHely, string IratHatido, string IratHatidoOk
            , string Torles, string UserId)
        {
            Logger.InfoStart("SZURManager.SZURSetIratHatIdoIratTarWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            DataRow ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
            Guid ugyiratId = (Guid)ugyiratRow["Id"];

            // Ha Torles == "1", akkor SkontroVisszavonas kell, egyébként SkontroInditas

            if (Torles == "1")
            {
                // Skontró visszavonás:
                var resultSkontroVisszavonas = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().SkontroVisszavonas(
                                execParamUserId, ugyiratId.ToString(), IratHatidoOk);

                if (resultSkontroVisszavonas.IsError)
                {
                    throw new ResultException(resultSkontroVisszavonas);
                }
            }
            else
            {
                // Ezzel mit kezdjünk: IratHatidoHely?
                // --> Nem tároljuk le, nem foglalkozunk vele.

                // Skontró indítás:
                var resultSkontro = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().SkontroInditas(execParamUserId, ugyiratId.ToString()
                            , IratHatido, IratHatidoOk);

                if (resultSkontro.IsError)
                {
                    throw new ResultException(resultSkontro);
                }
            }

            Logger.InfoEnd("SZURManager.SZURSetIratHatIdoIratTarWS");
        }

        /// <summary>
        /// Irat átvétele ügyintézésre
        /// "A megadott iktatószámú ügyirat ügyintézői átvételi időpontját rögzíti a SZÜR-ben."
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="SakkoraStatus"></param>
        /// <param name="OkKod"></param>
        /// <param name="UserId"></param>
        public void SZURSetIratAtveteleWS(string Evszam, string Konyv, string Foszam, string Alszam, string SakkoraStatus, string OkKod, string UserId)
        {
            Logger.InfoStart("SZURManager.SZURSetIratAtveteleWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            #region Ellenőrzés

            if (!string.IsNullOrEmpty(Alszam) && Alszam != "0")
            {
                var iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam);
                Guid iratId = (Guid)iratRow["Id"];

                var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                var resultIratAtvetelUgyintezesre = iratokService.AtvetelIntezkedesre(execParamUserId, new string[] { iratId.ToString() }, SakkoraStatus, OkKod);

                if (resultIratAtvetelUgyintezesre.IsError)
                {
                    throw new ResultException(resultIratAtvetelUgyintezesre);
                }
            }
            else
            {
                #region UGY
                // Ügyirat lekérése:
                var ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
                Guid ugyiratId = (Guid)ugyiratRow["Id"];

                // Átvétel ügyintézésre webservice hívása:
                var ugyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                var resultAtvetelUgyintezesre = ugyiratokService.AtvetelUgyintezesreSakkoraAllitassal(execParamUserId, ugyiratId.ToString(), SakkoraStatus, OkKod);
                if (resultAtvetelUgyintezesre.IsError)
                {
                    throw new ResultException(resultAtvetelUgyintezesre);
                }
                #endregion
            }
            #endregion

            Logger.InfoEnd("SZURManager.SZURSetIratAtveteleWS");
        }

        /// <summary>
        /// Iratcsomag átmeneti irattárba helyezése
        /// "Az átmeneti irattárba helyezésre átvett iratcsomag letétele az irattárban."
        /// (Torles == "1" esetén visszaküldés irattárból.)
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="IrattarTipus"></param>
        /// <param name="Irattar"></param>
        /// <param name="AttetelMasIrattarba"></param>
        /// <param name="Torles"></param>
        /// <param name="UserId"></param>
        public void SZURSetIratAtmIratTarWS(string Konyv, string Foszam, string Alszam, string Evszam, string IrattarTipus, string Irattar, string AttetelMasIrattarba, string Torles, string UserId)
        {
            Logger.InfoStart("SZURManager.SZURSetIratAtmIratTarWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            // Ügyirat lekérése:
            DataRow ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
            Guid ugyiratId = (Guid)ugyiratRow["Id"];

            Logger.Debug("Ügyirat Id: " + ugyiratId.ToString());

            var ugyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            // IrattarTipus: Szervezeti egység neve
            DataRow irattarCsoportRow = this.GetCsoportRowByNev(IrattarTipus);
            Guid irattarCsoportId = (Guid)irattarCsoportRow["Id"];
            if (Torles == "1")
            {
                execParamUserId = this.CreateExecParamByNMHHUserId(UserId, Utility.EnumXpmFelhasznaloSzervezetTipus.Irattaros, irattarCsoportId.ToString());
            }
            else
            {
                execParamUserId = this.CreateExecParamByNMHHUserId(UserId, Utility.EnumXpmFelhasznaloSzervezetTipus.Irattar, irattarCsoportId.ToString());
            }

            // Törlése esetén EREC_UgyUgyiratokService.VisszakuldesIrattarbol hívása
            if (Torles == "1")
            {
                string ugyiratAllapot = ugyiratRow["Allapot"].ToString();
                if (ugyiratAllapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
                {
                    #region Irattározásra jóváhagyás állapot visszavonása

                    Logger.Debug("Irattározásra jóváhagyás állapot visszavonása");

                    /// Ha "Irattározásra jóváhagyás alatt" státuszban van az ügyirat, akkor nem jó a VisszakuldesIrattarbol eljárás hívása,
                    /// ilyenkor a jóváhagyás alatti állapotot kell visszavonni:
                    /// - Státuszt visszaállítani Lezártra; Kovetkezo_Felelos_Id és Csoport_Id_Cimzett mezőt nullozni
                    /// 

                    // Ügyirat lekérése az Update-hez:
                    ExecParam execParamUgyiratGet = execParamUserId.Clone();
                    execParamUgyiratGet.Record_Id = ugyiratId.ToString();
                    var resultUgyiratGet = ugyiratokService.Get(execParamUgyiratGet);
                    resultUgyiratGet.CheckError();

                    EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)resultUgyiratGet.Record;

                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    erec_UgyUgyirat.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                    erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

                    erec_UgyUgyirat.Allapot = Contentum.eUtility.KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                    erec_UgyUgyirat.Updated.Allapot = true;

                    //Következő felelős törlése
                    erec_UgyUgyirat.Kovetkezo_Felelos_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
                    erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

                    //Címzett törlése
                    erec_UgyUgyirat.Csoport_Id_Cimzett = Contentum.eUtility.Constants.BusinessDocument.nullString;
                    erec_UgyUgyirat.Updated.Csoport_Id_Cimzett = true;

                    var resultUgyiratUpdate = ugyiratokService.Update(execParamUgyiratGet, erec_UgyUgyirat);
                    resultUgyiratUpdate.CheckError();

                    #endregion
                }
                else
                {
                    #region Irattárból visszaküldés

                    Logger.Debug("Irattárból visszaküldés");

                    // Ellenőrzés, hogy az ügyirat irattárba küldött állapotban van-e:
                    if (ugyiratAllapot != KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
                    {
                        throw new ResultException("Az ügyirat nincs irattárba küldve!");
                    }

                    var resultIrattarbolVissza = ugyiratokService.VisszakuldesIrattarbol(execParamUserId, ugyiratId.ToString(), String.Empty);
                    resultIrattarbolVissza.CheckError();

                    #endregion
                }
            }
            else
            {
                #region Irattárba adás
                string irattariTetelId = ugyiratRow["IraIrattariTetel_Id"].ToString();

                string irattarJellege = eUtility.Constants.IrattarJellege.AtmenetiIrattar;

                // Service hívása:
                var resultIrattarbaAdas = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService()
                                            .AtadasIrattarba(execParamUserId, ugyiratId.ToString(), irattariTetelId, String.Empty, irattarJellege
                                                        , irattarCsoportId.ToString());
                resultIrattarbaAdas.CheckError();

                #endregion
            }

            Logger.InfoEnd("SZURManager.SZURSetIratAtmIratTarWS");
        }

        /// <summary>
        /// Ügy vagy irat jóváhagyása (csak FMS könnyített frekvencia engedélyezési eljáráshoz)
        /// "A megadott főszámos vagy alszámos iraton beállítja a kiadmányozás adatait az átadott paramétereknek megfelelően"
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="KiadmanyozoSzerv"></param>
        /// <param name="KiadmanyozoUserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURJovahagyasWS(string Evszam, string Konyv, string Foszam, string Alszam, string KiadmanyozoSzerv, string KiadmanyozoUserId)
        {
            Logger.InfoStart("SZURManager.SZURJovahagyasWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(KiadmanyozoUserId);

            // Irat lekérése:            
            DataRow iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam);
            Guid iratId = (Guid)iratRow["Id"];

            #region Kiadmányozó felhasználó/csoport lekérése, ellenőrzése

            // Kiadmányozó felhasználó lekérése:
            DataRow kiadmanyozoDataRow = this.GetFelhasznaloRowByUserNev(KiadmanyozoUserId);
            Guid kiadmanyozoFelhId = (Guid)kiadmanyozoDataRow["Id"];

            // Kiadmányozó szervezet:
            if (!String.IsNullOrEmpty(KiadmanyozoSzerv))
            {
                DataRow kiadmSzervezetDataRow = this.GetCsoportRowByNev(KiadmanyozoSzerv);
                Guid kiadmSzervezetCsoportId = (Guid)kiadmSzervezetDataRow["Id"];

                // Ellenőrzés: a megadott szervezet a kiadmányozó szervezete-e:
                this.CheckCsoporttagsag(kiadmanyozoFelhId, kiadmSzervezetCsoportId, kiadmSzervezetDataRow["Nev"].ToString(), kiadmSzervezetDataRow["Nev"].ToString());
            }

            #endregion

            #region Irat aláírók ellenőrzése

            // Ha már van aláírás bejegyzése a megadott kiadmányozóra, hibaüzenet:
            var dataTableIratAlairok = this.GetIratAlairok(iratId, KodTarak.ALAIRO_SZEREP.Kiadmanyozo);
            ExecParam execParamInvalidate;
            foreach (DataRow iratAlairoRow in dataTableIratAlairok.Rows)
            {
                if (iratAlairoRow["FelhasznaloCsoport_Id_Alairo"].ToString() == kiadmanyozoFelhId.ToString())
                {
                    throw new ResultException("Az iraton már korábban bejegyzésre kerültek a kiadmányozásra vonatkozó adatok!");
                }

                #region INVALIDATE EXISTING KIADMANYOZOK
                execParamInvalidate = execParamUserId.Clone();
                execParamInvalidate.Record_Id = iratAlairoRow["Id"].ToString();
                eRecordService.ServiceFactory.GetEREC_IratAlairokService().Invalidate(execParamInvalidate);
                #endregion
            }

            #endregion

            // Ha nincs bejegyezve a kiadmányozni kell mező, akkor UPDATE:
            if (iratRow["KiadmanyozniKell"].ToString() != "1")
            {
                #region Irat UPDATE

                var iratObj = this.GetIratObjById(iratId);
                iratObj.Updated.SetValueAll(false);
                iratObj.Base.Updated.SetValueAll(false);

                iratObj.Base.Updated.Ver = true;

                iratObj.KiadmanyozniKell = "1";
                iratObj.Updated.KiadmanyozniKell = true;

                var execParamUpdate = execParamUserId.Clone();
                execParamUpdate.Record_Id = iratId.ToString();

                // Irat UPDATE service:
                var resultIratUpdate = eRecordService.ServiceFactory.GetEREC_IraIratokService()
                                                .Update(execParamUpdate, iratObj);
                resultIratUpdate.CheckError();

                #endregion                
            }

            #region Irat aláírók (kiadmányozó) bejegyzése

            EREC_IratAlairok newIratAlairokObj = new EREC_IratAlairok();

            newIratAlairokObj.Obj_Id = iratId.ToString();
            newIratAlairokObj.AlairasDatuma = Helper.GetFormattedDateAndTimeString(DateTime.Now);
            newIratAlairokObj.AlairoSzerep = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
            newIratAlairokObj.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
            newIratAlairokObj.FelhasznaloCsoport_Id_Alairo = kiadmanyozoFelhId.ToString();

            string alairasiSzabalyId =
              GetAlairasTipus(iratId.ToString(), kiadmanyozoFelhId.ToString(), KodTarak.ALAIRO_SZEREP.Kiadmanyozo, KodTarak.ALAIRAS_MOD.M_ADM);
            if (!string.IsNullOrEmpty(alairasiSzabalyId))
                newIratAlairokObj.AlairasSzabaly_Id = alairasiSzabalyId;

            var resultInsert = eRecordService.ServiceFactory.GetEREC_IratAlairokService().Insert(this.ExecParamCaller, newIratAlairokObj);
            resultInsert.CheckError();

            #endregion

            Logger.InfoEnd("SZURManager.SZURJovahagyasWS");
        }

        /// <summary>
        /// Az ügyintézési idő túllépése miatt visszafizetendő díj, illeték összege, címzettje, határideje
        /// "Az ügyintézési idő túllépése esetén a visszafizetendő díj, illeték összegének, címzettjének és a visszafizetés határidejének megadása az ügyintézés végét jelző alszámos iraton."
        /// </summary>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Ev"></param>
        /// <param name="Osszeg"></param>
        /// <param name="Jogcim"></param>
        /// <param name="Hatarido"></param>
        /// <param name="Cimzett"></param>
        /// <param name="Hatarozatszam"></param>
        /// <param name="User"></param>
        public void SZURSetDijVisszafizWS(string Foszam, string Alszam, string Ev, string Osszeg, string Jogcim, string Hatarido, string Cimzett, string Hatarozatszam, string User)
        {
            Logger.InfoStart("SZURManager.SZURSetDijVisszafizWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(User);

            // Irat lekérése:
            DataRow iratRow = this.GetIratRow(Ev, string.Empty, Foszam, Alszam);
            Guid iratId = (Guid)iratRow["Id"];

            #region Irat UPDATE

            var iratObj = this.GetIratObjById(iratId);
            iratObj.Updated.SetValueAll(false);
            iratObj.Base.Updated.SetValueAll(false);

            iratObj.Base.Updated.Ver = true;

            // Osszeg:
            if (!string.IsNullOrEmpty(Osszeg))
            {
                int osszegInt;
                if (int.TryParse(Osszeg, out osszegInt))
                {
                    iratObj.VisszafizetesOsszege = osszegInt.ToString();
                    iratObj.Updated.VisszafizetesOsszege = true;
                }
                else
                {
                    throw new ResultException(string.Format("'Osszeg' mező formátuma nem megfelelő: '{0}'"), Osszeg);
                }
            }

            string jogcimKod;
            switch (Jogcim)
            {
                case "D":
                    jogcimKod = KodTarak.VISSZAFIZETES_JOGCIME.Dij;
                    break;
                case "I":
                    jogcimKod = KodTarak.VISSZAFIZETES_JOGCIME.Illetek;
                    break;
                default:
                    jogcimKod = Jogcim;
                    break;
            }

            iratObj.VisszafizetesJogcime = jogcimKod;
            iratObj.Updated.VisszafizetesJogcime = true;

            // A dátumokat "yyyy.MM.dd" formátumban várjuk. Ha nem így jön, hibát dobunk:
            DateTime datumSeged;
            if (!string.IsNullOrEmpty(Hatarido))
            {
                if (DateTime.TryParseExact(Hatarido, SZURWSDateFormatString, CultureInfo.InvariantCulture, DateTimeStyles.None, out datumSeged))
                {
                    iratObj.VisszafizetesHatarido = datumSeged.ToString();
                    iratObj.Updated.VisszafizetesHatarido = true;
                }
                else
                {
                    throw new ResultException("A várt dátumformátum: " + SZURWSDateFormatString);
                }
            }

            iratObj.VisszafizetesHatarozatSzama = Hatarozatszam;
            iratObj.Updated.VisszafizetesHatarozatSzama = true;

            #region Partner keresése

            if (!string.IsNullOrEmpty(Cimzett))
            {
                Logger.Debug("Cimzett keresése: '" + Cimzett + "'");

                KRT_PartnerekSearch searchObj = new KRT_PartnerekSearch();
                searchObj.Nev.Value = Cimzett;
                searchObj.Nev.Operator = Query.Operators.equals;

                var resultPartnerek = eAdminService.ServiceFactory.GetKRT_PartnerekService().GetAll(this.ExecParamCaller, searchObj);
                resultPartnerek.CheckError();

                if (resultPartnerek.Ds.Tables[0].Rows.Count > 0)
                {
                    iratObj.Partner_Id_VisszafizetesCimzet = resultPartnerek.Ds.Tables[0].Rows[0]["Id"].ToString();
                    iratObj.Updated.Partner_Id_VisszafizetesCimzet = true;
                }
                else
                {
                    iratObj.Partner_Nev_VisszafizetCimzett = Cimzett;
                    iratObj.Updated.Partner_Nev_VisszafizetCimzett = true;
                }
            }

            #endregion

            var execParamUpdate = execParamUserId.Clone();
            execParamUpdate.Record_Id = iratId.ToString();

            // Irat UPDATE service:
            var resultIratUpdate = eRecordService.ServiceFactory.GetEREC_IraIratokService()
                                            .Update(execParamUpdate, iratObj);
            resultIratUpdate.CheckError();

            #endregion

            Logger.InfoEnd("SZURManager.SZURSetDijVisszafizWS");
        }

        /// <summary>
        /// Az "üzemzavar, elháríthatatlan akadály" elhárítására fordított napok megadása
        /// "az ügyintézési idő túllépése esetén az ügyintézés során esetlegesen fellépő üzemzavar, elháríthatatlan akadály időtartamának megadása, amely csökkenti az ügyintézésre fordított napok számát."
        /// </summary>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Ev"></param>
        /// <param name="Napok"></param>
        /// <param name="Iratszam"></param>
        /// <param name="User"></param>
        public void SZURSetUzemzavarWS(string Foszam, string Alszam, string Ev, string Napok, string Iratszam, string User)
        {
            Logger.InfoStart("SZURManager.SZURSetUzemzavarWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(User);

            // Irat lekérése: (nincs megadva iktatókönyv a webserviceben!)
            DataRow iratRow = this.GetIratRow(Ev, string.Empty, Foszam, Alszam);
            Guid iratId = (Guid)iratRow["Id"];

            // Irat UPDATE:

            var iratObj = this.GetIratObjById(iratId);
            iratObj.Updated.SetValueAll(false);
            iratObj.Base.Updated.SetValueAll(false);

            iratObj.Base.Updated.Ver = true;

            iratObj.UzemzavarIgazoloIratSzama = Iratszam;
            iratObj.Updated.UzemzavarIgazoloIratSzama = true;

            if (!string.IsNullOrEmpty(Napok))
            {
                /// Üzemzavar kezdete és vége mezőink vannak:
                /// A mai nap lesz az üzemzavar vége, és ebből vonjuk le a megadott napokat a kezdetéhez
                int napokInt;
                if (int.TryParse(Napok, out napokInt))
                {
                    if (napokInt < 0)
                        throw new ResultException(string.Format("Negatív paraméter érték: 'Napok': '{0}'", Napok));

                    iratObj.UzemzavarVege = Helper.GetFormattedDateOnlyString(DateTime.Today);
                    iratObj.Updated.UzemzavarVege = true;

                    iratObj.UzemzavarKezdete = DateTime.Today.Subtract(TimeSpan.FromDays(napokInt)).ToString();
                    iratObj.Updated.UzemzavarKezdete = true;
                }
                else
                {
                    throw new ResultException(string.Format("Hibás paraméter érték: 'Napok': '{0}'", Napok));
                }
            }

            var execParamUpdate = execParamUserId.Clone();
            execParamUpdate.Record_Id = iratId.ToString();

            // Irat UPDATE service:
            var resultIratUpdate = eRecordService.ServiceFactory.GetEREC_IraIratokService()
                                            .Update(execParamUpdate, iratObj);
            resultIratUpdate.CheckError();

            Logger.InfoEnd("SZURManager.SZURSetUzemzavarWS");
        }

        /// <summary>
        /// Ügyjellegek listája ('UGY_FAJTAJA' kódcsoport elemei)
        /// </summary>
        /// <returns></returns>
        public UgyJellegList SZURGetUgyJellegWS()
        {
            Logger.InfoStart("SZURManager.SZURGetUgyJellegWS");

            var ktListUgyjellegek = KodTar_Cache.GetKodtarakByKodCsoportList(KodTarak.UGY_FAJTAJA.KodcsoportKod, this.ExecParamCaller
                                    , HttpContext.Current.Cache, null);

            var result = new UgyJellegList();
            result.Ugyjelleg = ktListUgyjellegek.Select(e => e.Nev).ToArray();
            result.Ugyjellegkod = ktListUgyjellegek.Select(e => e.Kod).ToArray();

            Logger.InfoEnd("SZURManager.SZURGetUgyJellegWS");

            return result;
        }

        #region Partnerek

        /// <summary>
        /// A partnernyilvántartásban új ügyfél létrehozása
        /// </summary>
        /// <param name="HirUgyfelAzonosito"></param>
        /// <param name="Szervezet"></param>
        /// <param name="Iranyitoszam"></param>
        /// <param name="Telepules"></param>
        /// <param name="Cim"></param>
        /// <param name="Telefon"></param>
        /// <param name="Mobil"></param>
        /// <param name="Email"></param>
        /// <param name="Fax"></param>
        /// <param name="Ok"></param>
        /// <param name="KapcsolatTartok"></param>
        /// <param name="UserID"></param>
        public void SZURCreateUgyfelWS(string HirUgyfelAzonosito, string Szervezet, string Iranyitoszam, string Telepules, string Cim, string Telefon, string Mobil, string Email, string Fax, string Ok, KapcsolatTarto[] KapcsolatTartok, string UserID)
        {
            Logger.InfoStart("SZURManager.SZURCreateUgyfelWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserID);

            #region Ellenőrzés

            if (String.IsNullOrEmpty(HirUgyfelAzonosito))
            {
                throw new ArgumentNullException("HirUgyfelAzonosito");
            }

            // Van-e már felvéve ilyen partner (HirUgyfelAzonosito-ra ellenőrzünk)
            var partnerekService = eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
            var partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.KulsoAzonositok.Value = HirUgyfelAzonosito;
            partnerekSearch.KulsoAzonositok.Operator = Query.Operators.equals;
            var resultPartnerCheck = partnerekService.GetAll(execParamUserId, partnerekSearch);
            if (resultPartnerCheck.IsError)
            {
                throw new ResultException(resultPartnerCheck);
            }

            // Ellenőrzés, van-e már találat erre a HirUgyfelAzonosito-ra
            if (resultPartnerCheck.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = resultPartnerCheck.Ds.Tables[0].Rows[0];

                // Hiba:
                throw new ResultException(String.Format("Már van ilyen partner az adatbázisban! HirUgyfelAzonosito: '{0}', Partner név: '{1}'"
                                , HirUgyfelAzonosito, row["Nev"].ToString()));
            }

            #endregion

            #region Adatstruktúra összeállítása

            KRT_Partnerek partner = new KRT_Partnerek();

            partner.Tipus = KodTarak.Partner_Tipus.Szervezet;

            partner.Nev = Szervezet;
            partner.KulsoAzonositok = HirUgyfelAzonosito;
            // TODO: 'Ok' mező hova kerüljön?
            partner.Belso = "0";

            // Cím adatok
            var partnerCimAdatok = new eAdmin.Service.KRT_PartnerekService.PartnerCimek();
            partnerCimAdatok.Iranyitoszam = Iranyitoszam;
            partnerCimAdatok.Telepules = Telepules;
            partnerCimAdatok.Cim = Cim;
            partnerCimAdatok.Telefon = Telefon;
            partnerCimAdatok.Mobil = Mobil;
            partnerCimAdatok.Email = Email;
            partnerCimAdatok.Fax = Fax;

            KRT_Partnerek[] kapcsoltPartnerek = null;
            eAdmin.Service.KRT_PartnerekService.PartnerCimek[] kapcsoltPartnerCimek = null;

            if (KapcsolatTartok != null
                && KapcsolatTartok.Length > 0)
            {
                kapcsoltPartnerek = new KRT_Partnerek[KapcsolatTartok.Length];
                kapcsoltPartnerCimek = new eAdmin.Service.KRT_PartnerekService.PartnerCimek[KapcsolatTartok.Length];

                for (int i = 0; i < KapcsolatTartok.Length; i++)
                {
                    KapcsolatTarto kapcsolatTarto = KapcsolatTartok[i];

                    KRT_PartnerSzemelyek kapcsoltPartner = new KRT_PartnerSzemelyek();
                    var kapcsoltPartnerCim = new eAdmin.Service.KRT_PartnerekService.PartnerCimek();

                    kapcsoltPartnerek[i] = kapcsoltPartner;
                    kapcsoltPartnerCimek[i] = kapcsoltPartnerCim;

                    // Letiltjuk, hogy felülírja a KRT_Partnerek.Nev-et a KRT_Szemelyek-ben lévő adatokkal
                    kapcsoltPartner.UpdatePartnerNevBySzemelyAdatok = false;

                    // TODO: Ha Szemely-re van állítva, akkor hiba jön a PartnerKapcsolatTipus.alarendeltPartnere beállításra
                    kapcsoltPartner.Tipus = KodTarak.Partner_Tipus.Szemely;

                    #region SZEMELY
                    if (!string.IsNullOrEmpty(kapcsolatTarto.Nev))
                    {
                        string[] splittedNev = kapcsolatTarto.Nev.Split(' ');

                        if (splittedNev.Length == 1)
                        {
                            kapcsoltPartner.Szemelyek.UjCsaladiNev = splittedNev[0];
                            kapcsoltPartner.Szemelyek.Updated.UjCsaladiNev = true;
                        }
                        else if (splittedNev.Length == 2)
                        {
                            kapcsoltPartner.Szemelyek.UjCsaladiNev = splittedNev[0];
                            kapcsoltPartner.Szemelyek.Updated.UjCsaladiNev = true;

                            kapcsoltPartner.Szemelyek.UjUtonev = splittedNev[1];
                            kapcsoltPartner.Szemelyek.Updated.UjUtonev = true;
                        }
                        else if (splittedNev.Length >= 3)
                        {
                            kapcsoltPartner.Szemelyek.UjCsaladiNev = splittedNev[0];
                            kapcsoltPartner.Szemelyek.Updated.UjCsaladiNev = true;

                            kapcsoltPartner.Szemelyek.UjUtonev = kapcsolatTarto.Nev.Replace(splittedNev[0], string.Empty).Trim();
                            kapcsoltPartner.Szemelyek.Updated.UjUtonev = true;
                        }
                    }
                    if (!string.IsNullOrEmpty(kapcsolatTarto.Elotag))
                        // Elotag: Titulus mezőbe
                        kapcsoltPartner.Szemelyek.UjTitulis = kapcsolatTarto.Elotag;

                    if (!string.IsNullOrEmpty(kapcsolatTarto.Beosztas))
                        // Beosztás:
                        kapcsoltPartner.Szemelyek.Beosztas = kapcsolatTarto.Beosztas;
                    #endregion

                    if (!string.IsNullOrEmpty(kapcsolatTarto.HirKapcsTAzonosito))
                        kapcsoltPartner.KulsoAzonositok = kapcsolatTarto.HirKapcsTAzonosito;

                    kapcsoltPartner.Nev = kapcsolatTarto.Nev;
                    kapcsoltPartner.Belso = "0";

                    kapcsoltPartnerCim.Iranyitoszam = kapcsolatTarto.Iranyitoszam;
                    kapcsoltPartnerCim.Telepules = kapcsolatTarto.Telepules;
                    kapcsoltPartnerCim.Cim = kapcsolatTarto.Cim;
                    kapcsoltPartnerCim.Telefon = kapcsolatTarto.Telefon;
                    kapcsoltPartnerCim.Mobil = kapcsolatTarto.Mobil;
                    kapcsoltPartnerCim.Email = kapcsolatTarto.Email;
                    kapcsoltPartnerCim.Fax = kapcsolatTarto.Fax;
                }
            }

            #endregion

            // TODO: A kapcsolattartóra kellene egy kódtárérték
            //string partnerKapcsolatTipus = KodTarak.PartnerKapcsolatTipus.alarendeltPartnere;
            //string partnerKapcsolatTipus = KodTarak.PartnerKapcsolatTipus.felhasznaloSzervezete;
            string partnerKapcsolatTipus = KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja;

            var resultPartnerInsert = partnerekService.InsertWithCimekEsKapcsolatok(execParamUserId
                    , partner, partnerCimAdatok, partnerKapcsolatTipus, kapcsoltPartnerek, kapcsoltPartnerCimek);

            if (resultPartnerInsert.IsError)
            {
                throw new ResultException(resultPartnerInsert);
            }

            Logger.InfoEnd("SZURManager.SZURCreateUgyfelWS");
        }

        /// <summary>
        /// Ügyfél módosítása a SZÜR Partnertörzsében
        /// </summary>
        /// <param name="HirUgyfelAzonosito"></param>
        /// <param name="Szervezet"></param>
        /// <param name="Iranyitoszam"></param>
        /// <param name="Telepules"></param>
        /// <param name="Cim"></param>
        /// <param name="Telefon"></param>
        /// <param name="Mobil"></param>
        /// <param name="Email"></param>
        /// <param name="Fax"></param>
        /// <param name="Ok"></param>
        /// <param name="UserID"></param>
        public void SZURModUgyfelWS(string HirUgyfelAzonosito, string Szervezet, string Iranyitoszam, string Telepules, string Cim, string Telefon, string Mobil, string Email, string Fax, string Ok, string UserID)
        {
            Logger.InfoStart("SZURManager.SZURModUgyfelWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserID);

            #region Ellenőrzés

            if (String.IsNullOrEmpty(HirUgyfelAzonosito))
            {
                throw new ArgumentNullException("HirUgyfelAzonosito");
            }

            // Van-e már felvéve ilyen partner (HirUgyfelAzonosito-ra ellenőrzünk)
            var partnerekService = eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
            var partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.KulsoAzonositok.Value = HirUgyfelAzonosito;
            partnerekSearch.KulsoAzonositok.Operator = Query.Operators.equals;
            var resultPartnerCheck = partnerekService.GetAll(execParamUserId, partnerekSearch);
            if (resultPartnerCheck.IsError)
            {
                throw new ResultException(resultPartnerCheck);
            }

            // Ha nincs találat, hiba:
            if (resultPartnerCheck.Ds.Tables[0].Rows.Count == 0)
            {
                // Hiba:
                throw new ResultException(String.Format("Nincs ilyen partner az adatbázisban! HirUgyfelAzonosito: '{0}'"
                                , HirUgyfelAzonosito));
            }
            else if (resultPartnerCheck.Ds.Tables[0].Rows.Count > 1)
            {
                // Ha több találat is van, hiba:
                throw new ResultException(String.Format("Ezzel az azonosítóval több partner is szerepel az adatbázisban! HirUgyfelAzonosito: '{0}'"
                                , HirUgyfelAzonosito));
            }

            Guid partnerId = (Guid)resultPartnerCheck.Ds.Tables[0].Rows[0]["Id"];

            #endregion

            #region Adatstruktúra összeállítása

            // KRT_Partnerek az UPDATE-hez:
            KRT_Partnerek partner = new KRT_Partnerek();

            partner.Updated.SetValueAll(false);
            partner.Base.Updated.SetValueAll(false);

            // Ver értékére szükség van:
            partner.Base.Ver = resultPartnerCheck.Ds.Tables[0].Rows[0]["Ver"].ToString();

            partner.Nev = Szervezet;
            partner.Updated.Nev = true;

            // TODO: 'Ok' mező hova kerüljön?

            // Cím adatok
            var partnerCimAdatok = new eAdmin.Service.KRT_PartnerekService.PartnerCimek();
            partnerCimAdatok.Iranyitoszam = Iranyitoszam;
            partnerCimAdatok.Telepules = Telepules;
            partnerCimAdatok.Cim = Cim;
            partnerCimAdatok.Telefon = Telefon;
            partnerCimAdatok.Mobil = Mobil;
            partnerCimAdatok.Email = Email;
            partnerCimAdatok.Fax = Fax;

            #endregion

            execParamUserId.Record_Id = partnerId.ToString();

            // Update service hívása:
            var resultPartnerUpdate = partnerekService.UpdateWithCimek(execParamUserId, partner, partnerCimAdatok);

            if (resultPartnerUpdate.IsError)
            {
                throw new ResultException(resultPartnerUpdate);
            }

            Logger.InfoEnd("SZURManager.SZURModUgyfelWS");
        }

        /// <summary>
        /// Kapcsolattartó módosítása a SZÜR Partnertörzsében
        /// </summary>
        /// <param name="HirUgyfelAzonosito"></param>
        /// <param name="HirKapcsTAzonosito"></param>
        /// <param name="Elotag"></param>
        /// <param name="Nev"></param>
        /// <param name="Beosztas"></param>
        /// <param name="Iranyitoszam"></param>
        /// <param name="Telepules"></param>
        /// <param name="Cim"></param>
        /// <param name="Telefon"></param>
        /// <param name="Mobil"></param>
        /// <param name="Email"></param>
        /// <param name="Fax"></param>
        /// <param name="Ok"></param>
        /// <param name="UserID"></param>
        public void SZURModKapcsolatTartoWS(string HirUgyfelAzonosito, string HirKapcsTAzonosito, string Elotag, string Nev, string Beosztas, string Iranyitoszam, string Telepules, string Cim, string Telefon, string Mobil, string Email, string Fax, string Ok, string UserID)
        {
            Logger.InfoStart("SZURManager.SZURModKapcsolatTartoWS");

            #region Ellenőrzés

            if (string.IsNullOrEmpty(HirUgyfelAzonosito))
            {
                throw new ArgumentNullException("HirUgyfelAzonosito");
            }
            if (string.IsNullOrEmpty(HirKapcsTAzonosito))
            {
                throw new ArgumentNullException("HirKapcsTAzonosito");
            }

            #endregion

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserID);

            #region Partnerek lekérése

            var partnerekService = eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            #region Ügyfél lekérése 
            Result resultPartnerCheck = GetPartnerByKulsoAzonosito(execParamUserId, HirUgyfelAzonosito, "NULL", string.Empty);
            resultPartnerCheck.CheckError();

            if (resultPartnerCheck.Ds.Tables[0].Rows.Count < 1)
            {
                Logger.Debug(string.Format("Nincs ilyen partner az adatbázisban! Azonosito: '{0}','{1}'", HirUgyfelAzonosito, "NULL"));

                resultPartnerCheck = GetPartnerByKulsoAzonosito(execParamUserId, HirUgyfelAzonosito, string.Empty);
                resultPartnerCheck.CheckError();
                if (resultPartnerCheck.Ds.Tables[0].Rows.Count == 0)
                {
                    throw new ResultException(string.Format("Nincs ilyen partner az adatbázisban! Azonosito: '{0}'", HirUgyfelAzonosito));
                }
            }
            else if (resultPartnerCheck.Ds.Tables[0].Rows.Count > 1)
            {
                // Ha több találat is van, hiba:
                throw new ResultException(string.Format("Ezzel az azonosítóval több partner is szerepel az adatbázisban! Azonosito: '{0}','{1}'", HirUgyfelAzonosito, "NULL"));
            }

            Guid partnerIdFelettes = (Guid)resultPartnerCheck.Ds.Tables[0].Rows[0]["Id"];

            #endregion

            #region KAPCSOLATTARTO LINK
            Result resKapcs = GetPartnerByKulsoAzonosito(execParamUserId, HirKapcsTAzonosito, string.Empty);
            resKapcs.CheckError();
            if (resKapcs.Ds.Tables[0].Rows.Count < 1)
            {
                Logger.Debug(string.Format("Nincs ilyen partner az adatbázisban! HirKapcsTAzonosito: '{0}'", HirKapcsTAzonosito));

                resultPartnerCheck = GetPartnerByKulsoAzonosito(execParamUserId, HirUgyfelAzonosito, HirKapcsTAzonosito, string.Empty);
                resultPartnerCheck.CheckError();
                if (resKapcs.Ds.Tables[0].Rows.Count < 1)
                {
                    Logger.Debug(string.Format("Nincs ilyen partner az adatbázisban! HirKapcsTAzonosito: '{0},{1}'", HirUgyfelAzonosito, HirKapcsTAzonosito));

                    resultPartnerCheck = GetPartnerByKulsoAzonosito(execParamUserId, "NULL", HirKapcsTAzonosito, string.Empty);
                    resultPartnerCheck.CheckError();
                    if (resKapcs.Ds.Tables[0].Rows.Count < 1)
                    {
                        throw new Exception(string.Format("Nincs ilyen partner az adatbázisban! HirKapcsTAzonosito: '{0}'", HirKapcsTAzonosito));
                    }
                }
            }

            Add2PartnerKapcsolat(execParamUserId, partnerIdFelettes, new Guid(resKapcs.Ds.Tables[0].Rows[0]["Id"].ToString()), HirUgyfelAzonosito + " " + HirKapcsTAzonosito);
            #endregion

            #region Ügyfél partnerkapcsolatainak lekérése

            // Kapcsolattartó partner lekérése HirKapcsTAzonosito alapján:
            var partnerKapcsService = eUtility.eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();

            var execParamGetPartnerKapcs = execParamUserId.Clone();
            execParamGetPartnerKapcs.Record_Id = partnerIdFelettes.ToString();

            var resultGetPartnerKapcs = partnerKapcsService.GetAllByPartner(execParamGetPartnerKapcs, new KRT_PartnerKapcsolatokSearch());
            if (resultGetPartnerKapcs.IsError)
            {
                throw new ResultException(resultGetPartnerKapcs);
            }

            List<Guid> kapcsoltPartnerIds = new List<Guid>();
            foreach (DataRow rowPartnerKapcs in resultGetPartnerKapcs.Ds.Tables[0].Rows)
            {
                Guid partnerId = (Guid)rowPartnerKapcs["Partner_id"];
                Guid partnerIdKapcsolt = (Guid)rowPartnerKapcs["Partner_id_kapcsolt"];

                if (partnerIdKapcsolt != partnerIdFelettes)
                {
                    kapcsoltPartnerIds.Add(partnerIdKapcsolt);
                }
                else if (partnerId != partnerIdFelettes)
                {
                    kapcsoltPartnerIds.Add(partnerId);
                }
            }

            #endregion

            #region Kapcsolattartó partner lekérése

            int resultsCount;
            DataRow rowKapcsolattarto = null;

            if (kapcsoltPartnerIds.Count > 0)
            {
                // Partner lekérése, szűrve az Id listára, és a HirKapcsTAzonosito-ra:
                KRT_PartnerekSearch searchPartnerHirKapcs = new KRT_PartnerekSearch();

                searchPartnerHirKapcs.Id.Value = "'" + String.Join("','", kapcsoltPartnerIds.Select(e => e.ToString()).ToArray()) + "'";
                searchPartnerHirKapcs.Id.Operator = Query.Operators.inner;

                searchPartnerHirKapcs.KulsoAzonositok.Value = HirKapcsTAzonosito;
                searchPartnerHirKapcs.KulsoAzonositok.Operator = Query.Operators.equals;

                // Partner lekérés hívás:
                var resultPartnerGetAll = partnerekService.GetAll(execParamUserId, searchPartnerHirKapcs);
                if (resultPartnerGetAll.IsError)
                {
                    throw new ResultException(resultPartnerGetAll);
                }

                resultsCount = resultPartnerGetAll.Ds.Tables[0].Rows.Count;
                if (resultsCount > 0)
                {
                    rowKapcsolattarto = resultPartnerGetAll.Ds.Tables[0].Rows[0];
                }
            }
            else
            {
                resultsCount = 0;
            }

            // Ellenőrzés, egy találat van-e:
            if (resultsCount == 0)
            {
                // Hiba:
                throw new ResultException(String.Format("Nincs ilyen partner az adatbázisban! HirKapcsTAzonosito: '{0}', HirUgyfelAzonosito: '{1}'"
                                , HirKapcsTAzonosito, HirUgyfelAzonosito));
            }
            else if (resultsCount > 1)
            {
                // Ha több találat is van, hiba:
                throw new ResultException(String.Format("Ezzel az azonosítóval több partner is szerepel az adatbázisban! HirKapcsTAzonosito: '{0}', HirUgyfelAzonosito: '{1}'"
                                , HirKapcsTAzonosito, HirUgyfelAzonosito));
            }

            Guid partnerIdKapcsolattarto = (Guid)rowKapcsolattarto["Id"];

            #endregion

            #region KRT_Szemelyek rekord lekérése

            KRT_SzemelyekSearch szemelySearch = new KRT_SzemelyekSearch();
            szemelySearch.Partner_Id.Value = partnerIdKapcsolattarto.ToString();
            szemelySearch.Partner_Id.Operator = Query.Operators.equals;

            var resultSzemelyGetAll = eUtility.eAdminService.ServiceFactory.GetKRT_SzemelyekService().GetAll(execParamUserId, szemelySearch);
            if (resultSzemelyGetAll.IsError)
            {
                throw new ResultException(resultSzemelyGetAll);
            }

            #endregion

            #endregion

            #region Adatstruktúra összeállítása

            // KRT_PartnerSzemelyek az UPDATE-hez:
            KRT_PartnerSzemelyek partnerSzemely = new KRT_PartnerSzemelyek();

            partnerSzemely.Updated.SetValueAll(false);
            partnerSzemely.Base.Updated.SetValueAll(false);

            partnerSzemely.UpdatePartnerNevBySzemelyAdatok = false;

            // Ver értékére szükség van:
            partnerSzemely.Base.Ver = rowKapcsolattarto["Ver"].ToString();

            partnerSzemely.Tipus = rowKapcsolattarto["Tipus"].ToString();

            partnerSzemely.Nev = Nev;
            partnerSzemely.Updated.Nev = true;

            // KRT_Szemelyek:
            if (resultSzemelyGetAll.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow rowSzemely = resultSzemelyGetAll.Ds.Tables[0].Rows[0];

                KRT_Szemelyek szemelyObj = new KRT_Szemelyek();

                szemelyObj.Updated.SetValueAll(false);
                szemelyObj.Base.Updated.SetValueAll(false);

                szemelyObj.Id = rowSzemely["Id"].ToString();
                szemelyObj.Base.Ver = rowSzemely["Ver"].ToString();

                // Elotag == Titulus
                szemelyObj.UjTitulis = Elotag;
                szemelyObj.Updated.UjTitulis = true;

                szemelyObj.Beosztas = Beosztas;
                szemelyObj.Updated.Beosztas = true;

                if (!string.IsNullOrEmpty(Nev))
                {
                    string[] splittedNev = Nev.Split(' ');

                    if (splittedNev.Length == 1)
                    {
                        szemelyObj.UjCsaladiNev = splittedNev[0];
                        szemelyObj.Updated.UjCsaladiNev = true;
                    }
                    else if (splittedNev.Length == 2)
                    {
                        szemelyObj.UjCsaladiNev = splittedNev[0];
                        szemelyObj.Updated.UjCsaladiNev = true;

                        szemelyObj.UjUtonev = splittedNev[1];
                        szemelyObj.Updated.UjUtonev = true;
                    }
                    else if (splittedNev.Length >= 3)
                    {
                        szemelyObj.UjCsaladiNev = splittedNev[0];
                        szemelyObj.Updated.UjCsaladiNev = true;

                        szemelyObj.UjUtonev = Nev.Replace(splittedNev[0], string.Empty).Trim();
                        szemelyObj.Updated.UjUtonev = true;
                    }
                }
                partnerSzemely.Szemelyek = szemelyObj;
            }

            // Cím adatok
            var partnerCimAdatok = new eAdmin.Service.KRT_PartnerekService.PartnerCimek();
            partnerCimAdatok.Iranyitoszam = Iranyitoszam;
            partnerCimAdatok.Telepules = Telepules;
            partnerCimAdatok.Cim = Cim;
            partnerCimAdatok.Telefon = Telefon;
            partnerCimAdatok.Mobil = Mobil;
            partnerCimAdatok.Email = Email;
            partnerCimAdatok.Fax = Fax;

            #endregion

            execParamUserId.Record_Id = partnerIdKapcsolattarto.ToString();

            // Update service hívása:
            var resultPartnerUpdate = partnerekService.UpdateWithCimek(execParamUserId, partnerSzemely, partnerCimAdatok);

            if (resultPartnerUpdate.IsError)
            {
                throw new ResultException(resultPartnerUpdate);
            }

            Logger.InfoEnd("SZURManager.SZURModKapcsolatTartoWS");
        }
        /// <summary>
        /// GetPartnerByKulsoAzonosito
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="kulsoAzonosito"></param>
        private Result GetPartnerByKulsoAzonosito(ExecParam execParam, string kulsoAzonosito, string partnerTipus)
        {
            var partnerekService = eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
            var partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.KulsoAzonositok.Value = kulsoAzonosito;
            partnerekSearch.KulsoAzonositok.Operator = Query.Operators.equals;

            /*partnerekSearch.Forras.Value = KodTarak.Partner_Forras.ADATKAPU;
            partnerekSearch.Forras.Operator = Query.Operators.equals;*/

            if (!string.IsNullOrEmpty(partnerTipus))
            {
                partnerekSearch.Tipus.Value = partnerTipus;
                partnerekSearch.Tipus.Operator = Query.Operators.equals;
            }
            var resultPartnerCheck = partnerekService.GetAll(execParam, partnerekSearch);
            if (resultPartnerCheck.IsError)
            {
                throw new ResultException(resultPartnerCheck);
            }
            return resultPartnerCheck;
        }
        /// <summary>
        /// GetPartnerByKulsoAzonosito
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="kulsoAzonosito"></param>
        private Result GetPartnerByKulsoAzonosito(ExecParam execParam, string hirUgyfelAzonosito, string hirKapcsTAzonosito, string partnerTipus)
        {
            var partnerekService = eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
            var partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.KulsoAzonositok.Value = string.Format("{0},{1}", string.IsNullOrEmpty(hirUgyfelAzonosito) ? "NULL" : hirUgyfelAzonosito, string.IsNullOrEmpty(hirKapcsTAzonosito) ? "NULL" : hirKapcsTAzonosito);
            partnerekSearch.KulsoAzonositok.Operator = Query.Operators.equals;

            if (!string.IsNullOrEmpty(partnerTipus))
            {
                partnerekSearch.Tipus.Value = partnerTipus;
                partnerekSearch.Tipus.Operator = Query.Operators.equals;
            }
            var resultPartnerCheck = partnerekService.GetAll(execParam, partnerekSearch);
            if (resultPartnerCheck.IsError)
            {
                throw new ResultException(resultPartnerCheck);
            }
            return resultPartnerCheck;
        }
        /// <summary>
        /// AddPartnerkapcsolat
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="parentId"></param>
        /// <param name="id"></param>
        /// <param name="note"></param>
        public void Add2PartnerKapcsolat(ExecParam execParam, Guid parentId, Guid id, string note)
        {
            if (parentId == null || parentId == Guid.Empty || id == null || id == Guid.Empty)
                return;

            KRT_PartnerKapcsolatokService serviceKapcsolat = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
            KRT_PartnerKapcsolatokSearch src = new KRT_PartnerKapcsolatokSearch();
            src.Partner_id.Value = id.ToString();
            src.Partner_id.Operator = Query.Operators.equals;

            src.Partner_id_kapcsolt.Value = parentId.ToString();
            src.Partner_id_kapcsolt.Operator = Query.Operators.equals;

            src.Tipus.Value = KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja;
            src.Tipus.Operator = Query.Operators.equals;

            Result resultFind = serviceKapcsolat.GetAll(execParam, src);
            if (!string.IsNullOrEmpty(resultFind.ErrorCode))
                throw new ResultException(resultFind);

            if (resultFind == null || resultFind.Ds.Tables[0].Rows.Count < 1)
            {
                KRT_PartnerKapcsolatok kapcsolat = new KRT_PartnerKapcsolatok();
                kapcsolat.Id = Guid.NewGuid().ToString();
                kapcsolat.Partner_id = id.ToString();
                kapcsolat.Partner_id_kapcsolt = parentId.ToString();
                kapcsolat.Tipus = KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja;
                kapcsolat.Base.Note = note;

                Result resultKapcsolat = serviceKapcsolat.Insert(execParam, kapcsolat);
                if (!string.IsNullOrEmpty(resultKapcsolat.ErrorCode))
                    throw new ResultException(resultKapcsolat);
            }
        }

        #endregion

        #region Jogerősítés

        /// <summary>
        /// Határozat típusú iratok jogerősítési állapotának lekérdezése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv">A Konyv eredetileg nem volt benne a WSDL-ben!</param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="JogEroStatusz">Az irat jogerős státusza, értéke a határozat jogerőre emelkedésének dátuma ÉÉÉÉ.HH.NN fomában,
        /// vagy üres "", ha a határozat még nem emelkedett jogerőre.</param>
        public void SZURGetHatJogEroStatusWS(string Evszam, string Foszam, string Alszam, out string JogEroStatusz)
        {
            Logger.InfoStart("SZURManager.SZURGetHatJogEroStatusWS");

            // Irat lekérése:
            DataRow rowIrat = this.GetIratRow(Evszam, String.Empty, Foszam, Alszam);
            Guid iratId = (Guid)rowIrat["Id"];

            #region Irat tárgyszavainak lekérése

            var resultIratTargyszavak = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService()
                            .GetAllMetaByDefinicioTipus(this.ExecParamCaller, new EREC_ObjektumTargyszavaiSearch()
                                    , iratId.ToString(), null, "EREC_IraIratok", null, false, false);

            if (resultIratTargyszavak.IsError)
            {
                // hiba:
                throw new ResultException(resultIratTargyszavak);
            }

            #endregion

            JogEroStatusz = String.Empty;

            // Jogerore_emelkedes_datuma tárgyszó értéke kell:
            foreach (DataRow rowObjTargyszo in resultIratTargyszavak.Ds.Tables[0].Rows)
            {
                if (rowObjTargyszo["BelsoAzonosito"].ToString() == TargyszoAzon_Jogerore_emelkedes_datuma)
                {
                    string targyszoErtek = rowObjTargyszo["Ertek"].ToString();

                    Logger.Debug("Jogerőre emelkedés dátuma tárgyszó érték: " + targyszoErtek);

                    // Konvertálni kell az elvárt formátumra (ÉÉÉÉ.HH.NN): 
                    if (!String.IsNullOrEmpty(targyszoErtek))
                    {
                        DateTime dat;
                        if (DateTime.TryParse(targyszoErtek, out dat))
                        {
                            JogEroStatusz = Helper.GetFormattedDateOnlyString(dat);
                        }
                        else
                        {
                            JogEroStatusz = targyszoErtek;
                        }
                    }

                    // Megvan a keresett tárgyszó, kiugrunk a ciklusból:
                    break;
                }
            }

            Logger.InfoEnd("SZURManager.SZURGetHatJogEroStatusWS");
        }

        /// <summary>
        /// Jogerős határozatok listájának lekérdezése főszám-lista alapján
        /// "Egy főszám-lista alapján visszaad egy határozatlistát."
        /// </summary>
        /// <param name="eredetiFoszamLista"></param>
        /// <param name="hatarozatLista"></param>
        public void SZURGetJogerosHatarozatokWS(Foszam[] eredetiFoszamLista, out JogerosHatarozat[] hatarozatLista)
        {
            Logger.InfoStart("SZURManager.SZURGetHatJogEroStatusWS");

            /// Lépések:
            /// - Főszámonként iratok lekérése
            /// - A főszámonként lekért iratokhoz rendelt tárgyszavak lekérése
            /// 

            List<JogerosHatarozat> jogerosHatarozatok = new List<JogerosHatarozat>();

            var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            var objTargyszavakService = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

            foreach (var foszam in eredetiFoszamLista)
            {
                #region Főszám iratainak lekérése

                var searchObjIratok = new EREC_IraIratokSearch(true);

                searchObjIratok.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value = foszam.evszam;
                searchObjIratok.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.equals;

                // Konyv: IktatoHely mezőre szűrünk
                searchObjIratok.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Value = foszam.konyv;
                searchObjIratok.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Operator = Query.Operators.equals;

                searchObjIratok.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Value = foszam.foszam;
                searchObjIratok.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Operator = Query.Operators.equals;

                // Lekérdezés adatbázisból:
                var resultIrat = iratokService.GetAllWithExtension(this.ExecParamCaller, searchObjIratok);
                if (resultIrat.IsError)
                {
                    throw new ResultException(resultIrat);
                }

                // Irat Id-k összeszedése
                //List<Guid> iratIds = new List<Guid>();
                Dictionary<Guid, DataRow> iratRows = new Dictionary<Guid, DataRow>();

                foreach (DataRow rowIrat in resultIrat.Ds.Tables[0].Rows)
                {
                    Guid iratId = (Guid)rowIrat["Id"];
                    //iratIds.Add(iratId);
                    iratRows[iratId] = rowIrat;
                }

                // Ellenőrzés, van-e irat:
                if (iratRows.Count == 0)
                {
                    throw new ResultException(String.Format("Iratok lekérése sikertelen ehhez a főszámhoz: {0} /{1} /{2}"
                                    , foszam.konyv, foszam.foszam, foszam.evszam));
                }

                #endregion

                #region EREC_ObjektumTargyszavai lekérése iratId-kra szűrve

                EREC_ObjektumTargyszavaiSearch searchObjTargyszavak = new EREC_ObjektumTargyszavaiSearch();
                searchObjTargyszavak.Obj_Id.Value = "'" + String.Join("','", iratRows.Select(e => e.Key.ToString()).ToArray()) + "'";
                searchObjTargyszavak.Obj_Id.Operator = Query.Operators.inner;

                searchObjTargyszavak.ObjTip_Id.Value = this.GetObjtipusId("EREC_IraIratok").ToString();
                searchObjTargyszavak.ObjTip_Id.Operator = Query.Operators.equals;

                var resultObjTargySzavak = objTargyszavakService.GetAllWithExtension(this.ExecParamCaller, searchObjTargyszavak);
                if (resultObjTargySzavak.IsError)
                {
                    throw new ResultException(resultObjTargySzavak);
                }

                #endregion

                #region JogerosHatarozat objektumok létrehozása

                foreach (DataRow rowObjTargyszo in resultObjTargySzavak.Ds.Tables[0].Rows)
                {
                    // Azokat kell nézni, ahol a tárgyszó BelsoAzonosito mezője "Jogerore_emelkedes_datuma"
                    if (rowObjTargyszo["BelsoAzonosito"].ToString() == TargyszoAzon_Jogerore_emelkedes_datuma)
                    {
                        string targyszoErtek = rowObjTargyszo["Ertek"].ToString();

                        Logger.Debug("Jogerőre emelkedés dátuma tárgyszó érték: " + targyszoErtek);

                        // Azok a jogerős iratok (határozatok), ahol ez ki van töltve:
                        if (!String.IsNullOrEmpty(targyszoErtek))
                        {
                            JogerosHatarozat jogerosHat = new JogerosHatarozat();
                            jogerosHat.eredetiFoszam = foszam;
                            // TODO: a hatarozatFoszam lehet más, mint az eredetiFoszam?
                            jogerosHat.hatarozatFoszam = foszam;

                            Guid iratId = (Guid)rowObjTargyszo["Obj_Id"];
                            DataRow iratRow = iratRows[iratId];

                            jogerosHat.hatarozatAlszam = iratRow["Alszam"].ToString();

                            // jogeroDatum:                            
                            // Konvertálni kell az elvárt formátumra (ÉÉÉÉ.HH.NN): 
                            DateTime dat;
                            if (DateTime.TryParse(targyszoErtek, out dat))
                            {
                                jogerosHat.jogeroDatum = Helper.GetFormattedDateOnlyString(dat);
                            }
                            else
                            {
                                jogerosHat.jogeroDatum = targyszoErtek;
                            }

                            jogerosHatarozatok.Add(jogerosHat);
                        }
                    }
                }

                #endregion
            }

            hatarozatLista = jogerosHatarozatok.ToArray();

            Logger.InfoEnd("SZURManager.SZURGetHatJogEroStatusWS");
        }

        /// <summary>
        /// Jogerősítő láttamozásának bejegyzése, törlése.
        /// "A kijelölt iraton bejegyzi a jogerősítő általi láttamozás tényét vagy törli azt."
        /// BUG#3493/Task#3510: Nem az aláírók közé kell bejegyzés, hanem az irat mezői közül ki kell tölteni a "Jogerősítést végző" és a "Jogerősítést végző látta" mezőket (Tárgyszavas mezők)
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="Torles"></param>
        /// <param name="UserId"></param>
        public void SZURJogEroLattaWS(string Konyv, string Foszam, string Alszam, string Evszam, string Torles, string UserId)
        {
            Logger.InfoStart("SZURManager.SZURJogEroLattaWS");

            // Irat lekérése:
            DataRow iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam);
            Guid iratId = (Guid)iratRow["Id"];

            Guid felhasznaloId = (Guid)this.GetFelhasznaloRowByUserNev(UserId)["Id"];

            // Tárgyszó értékek beállítása:
            if (Torles == "1")
            {
                this.SetObjektumTargyszavak(iratId, eUtility.Constants.TableNames.EREC_IraIratok, this.ExecParamCaller
                            , new KeyValuePair<string, string>(TargyszoAzon_Jogerositest_vegzo, String.Empty)
                            , new KeyValuePair<String, string>(TargyszoAzon_Jogerositest_vegzo_latta, String.Empty));
            }
            else
            {
                this.SetObjektumTargyszavak(iratId, eUtility.Constants.TableNames.EREC_IraIratok, this.ExecParamCaller
                                , new KeyValuePair<string, string>(TargyszoAzon_Jogerositest_vegzo, felhasznaloId.ToString())
                                , new KeyValuePair<String, string>(TargyszoAzon_Jogerositest_vegzo_latta, Helper.GetFormattedDateAndTimeString(DateTime.Now)));
            }

            /*
            /// Aláírások lekérése:
            /// Ha van Láttamozó, de még Aláírandó státuszú, akkor ott bejegyezzük az aláírást,
            /// egyébként új aláírás felvétele.
            /// 

            var iratAlairokService = eRecordService.ServiceFactory.GetEREC_IratAlairokService();

            EREC_IratAlairokSearch searchIratAlairok = new EREC_IratAlairokSearch();
            searchIratAlairok.Obj_Id.Value = iratId.ToString();
            searchIratAlairok.Obj_Id.Operator = Query.Operators.equals;

            searchIratAlairok.AlairoSzerep.Value = KodTarak.ALAIRO_SZEREP.Lattamozo;
            searchIratAlairok.AlairoSzerep.Operator = Query.Operators.equals;

            //// A megadott felhasználóra szűrünk:
            //searchIratAlairok.FelhasznaloCsoport_Id_Alairo.Value = felhasznaloId.ToString();
            //searchIratAlairok.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;

            var resultAlairokGetAll = iratAlairokService.GetAll(this.ExecParamCaller, searchIratAlairok);
            if (resultAlairokGetAll.IsError)
            {
                throw new ResultException(resultAlairokGetAll);
            }

            // Az aláírandó státuszú sor, ha van:
            DataRow alairandoRow = null;
            // Az aláírt státuszú sor, ha van:
            DataRow alairtRow = null;

            foreach (DataRow row in resultAlairokGetAll.Ds.Tables[0].Rows)
            {
                string alairasAllapot = row["Allapot"].ToString();

                if (alairasAllapot == KodTarak.IRATALAIRAS_ALLAPOT.Alairando)
                {
                    alairandoRow = row;
                }
                else if (alairasAllapot == KodTarak.IRATALAIRAS_ALLAPOT.Alairt)
                {
                    alairtRow = row;
                }
            }

            // Ha Törlés kell, akkor kell lennie aláírt sornak --> ilyenkor ezt visszaállítjuk aláírandóra.
            if (Torles == "1")
            {
                #region Aláírás tényének törlése

                if (alairtRow == null)
                {
                    // Hiba:
                    throw new ResultException("Törlés hiba: Az iraton még nincs bejegyezve a láttamozás!");
                }

                this.UpdateIratAlairok((Guid)alairtRow["Id"], KodTarak.IRATALAIRAS_ALLAPOT.Alairando, "<null>", alairtRow["FelhasznaloCsoport_Id_Alairo"].ToString());

                #endregion
            }
            else
            {
                #region Aláírás bejegyzése

                // Ellenőrzés, ha már aláírt, hiba:
                if (alairtRow != null)
                {
                    throw new ResultException("Az iraton már be van jegyezve a láttamozás ténye!");
                }

                if (alairandoRow != null)
                {
                    // Update:
                    this.UpdateIratAlairok((Guid)alairandoRow["Id"], KodTarak.IRATALAIRAS_ALLAPOT.Alairt, DateTime.Today.ToString(), felhasznaloId.ToString());
                }
                else
                {
                    #region Insert

                    EREC_IratAlairok newIratAlairokObj = new EREC_IratAlairok();

                    newIratAlairokObj.Obj_Id = iratId.ToString();
                    newIratAlairokObj.AlairasDatuma = DateTime.Now.ToString();
                    newIratAlairokObj.AlairoSzerep = KodTarak.ALAIRO_SZEREP.Lattamozo;
                    newIratAlairokObj.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                    newIratAlairokObj.FelhasznaloCsoport_Id_Alairo = felhasznaloId.ToString();

                    var resultInsert = iratAlairokService.Insert(this.ExecParamCaller, newIratAlairokObj);
                    if (resultInsert.IsError)
                    {
                        throw new ResultException(resultInsert);
                    }

                    #endregion
                }

                #endregion
            }
            */

            Logger.InfoEnd("SZURManager.SZURJogEroLattaWS");
        }

        /// <summary>
        /// Jogerősítendő irat jogerősítése
        /// "Beállítja a jogerősítendő irat jogerőre emelkedésének időpontját."
        /// --> "Jogerore_emelkedes_datuma" tárgyszóba kell beírni a dátumot
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="Datum"></param>
        /// <param name="UserId"></param>
        public void SZURjogerdatumWS(string Konyv, string Foszam, string Alszam, string Evszam, string Datum, string UserId)
        {
            Logger.InfoStart("SZURManager.SZURjogerdatumWS");

            ExecParam execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            // Irat lekérése:
            DataRow iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam, execParamUserId);
            Guid iratId = (Guid)iratRow["Id"];

            // Obj. tárgyszó mentése:
            this.SetObjektumTargyszo(iratId, eUtility.Constants.TableNames.EREC_IraIratok, TargyszoAzon_Jogerore_emelkedes_datuma, Datum, execParamUserId);

            Logger.InfoEnd("SZURManager.SZURjogerdatumWS");
        }
        #endregion


        /// <summary>
        /// A SOAP kérésben megadja, az ELHITET felületen beírt iktatószámot és a WebService a SZÜR meghatározott nézetében megkeresi azt, 
        /// a keresés eredményét a SOAP válaszban visszaadja.
        /// </summary>
        /// <param name="IKTATOSZAM"></param>
        /// <param name="HIBAKOD"></param>
        /// <param name="HIBASZOVEG"></param>
        /// <returns></returns>
        public string SZURGETIKTATOSZAMWS(string IKTATOSZAM, out string HIBAKOD, out string HIBASZOVEG)
        {
            Logger.InfoStart("SZURManager.SZURGetIktatoszamWS");

            #region OUT
            HIBAKOD = Utility.ErrorCodes.SuccessCode;
            HIBASZOVEG = string.Empty;
            string Isvalid = Utility.EnumIgenNem.Nem.ToString();
            #endregion

            ExecParam execParam = this.ExecParamCaller;
            var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            var searchObjIratok = new EREC_IraIratokSearch(true);
            searchObjIratok.Azonosito.Value = IKTATOSZAM;
            searchObjIratok.Azonosito.Operator = Query.Operators.equals;
            searchObjIratok.TopRow = 1;

            var resultIratok = iratokService.GetAll(execParam, searchObjIratok);
            resultIratok.CheckError();

            if (resultIratok == null || resultIratok.Ds.Tables[0].Rows.Count < 1)
                Isvalid = Utility.EnumIgenNem.Nem.ToString();
            else
                Isvalid = Utility.EnumIgenNem.Igen.ToString();

            Logger.InfoEnd("SZURManager.SZURGetIktatoszamWS");

            return Isvalid;
        }
        /// <summary>
        /// Ha érvényes iktatószámot nem adott meg az ügyintéző az ELHITET felületen, akkor lehetősége van a SZÜR-ből lekérdezni, 
        /// és SOAP válaszként megkapni a Szervezeti egységek listáját.
        /// </summary>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public SZERVEZETEK SZURGETSZERVEZETEKWS(out string HIBAKOD, out string HIBASZOVEG)
        {
            Logger.InfoStart("SZURManager.SZURGetSzervezetekWS");

            #region OUT
            HIBAKOD = Utility.ErrorCodes.SuccessCode;
            HIBASZOVEG = String.Empty;
            SZERVEZETEK result = new SZERVEZETEK();
            #endregion

            ExecParam execParam = this.ExecParamCaller;
            try
            {
                #region SEARCH OBJ
                eQuery.BusinessDocuments.KRT_PartnerekSearch src = new eQuery.BusinessDocuments.KRT_PartnerekSearch();

                // BUG#5709: Nem kell HKP-s szűrés, annyi elég, hogy belső partner, jóváhagyott, és a típusa szervezet

                //src.Forras.Value = KodTarak.Partner_Forras.HKP;
                //src.Forras.Operator = eQuery.Query.Operators.equals;

                src.Tipus.Value = KodTarak.Partner_Tipus.Szervezet;
                src.Tipus.Operator = eQuery.Query.Operators.equals;

                src.Belso.Value = Contentum.eUtility.Constants.Database.Yes;
                src.Belso.Operator = eQuery.Query.Operators.equals;

                src.Allapot.Value = KodTarak.Partner_Allapot.Jovahagyott;
                src.Allapot.Operator = eQuery.Query.Operators.equals;

                //src.KulsoAzonositok.Value = string.Empty;
                //src.KulsoAzonositok.Operator = eQuery.Query.Operators.notnull;

                src.TopRow = 5000;
                #endregion

                KRT_PartnerekService partnerServcie = eAdminService.ServiceFactory.GetKRT_PartnerekService();

                Result partnerResult = partnerServcie.GetAll(this.ExecParamCaller, src);
                partnerResult.CheckError();

                if (partnerResult == null || partnerResult.Ds.Tables[0].Rows.Count < 1)
                    return null;

                List<SZERVEZET> list = new List<SZERVEZET>();
                SZERVEZET tempSzervezet = null;
                if (partnerResult.Ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < partnerResult.Ds.Tables[0].Rows.Count; i++)
                    {
                        tempSzervezet = new SZERVEZET();
                        tempSzervezet.NEV = partnerResult.Ds.Tables[0].Rows[i]["Nev"].ToString();
                        tempSzervezet.ID = partnerResult.Ds.Tables[0].Rows[i]["KulsoAzonositok"].ToString();
                        list.Add(tempSzervezet);
                    }
                    result.SZERVEZET = list.ToArray();
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out HIBAKOD, out HIBASZOVEG);
                if (string.IsNullOrEmpty(HIBAKOD))
                    HIBAKOD = Utility.ErrorCodes.GeneralErrorCode;
                result = null;
            }
            Logger.InfoEnd("SZURService.SZURGetSzervezetekWS");

            if (result == null)
                return null;

            return result;
        }

        /// <summary>
        /// Tértivevények lekérdezése iktatószám alapján 
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Evszam"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="AtNemVett"></param>
        /// <param name="Szukites"></param>
        /// <param name="kezbesitettsegi_informaciok"></param>
        public void SZURChkKuldemenyTertiAllapotWS(ref string Konyv, ref string Evszam, ref string Foszam, ref string Alszam, string AtNemVett, string Szukites
                , out KuldemenyAllapot[] kuldemeny_allapot)
        {
            Logger.InfoStart("SZURManager.SZURChkKuldemenyTertiAllapotWS");

            // Irat lekérése:
            DataRow iratRow = GetIratRow(Evszam, Konyv, Foszam, Alszam);
            Guid iratId = (Guid)iratRow["Id"];
            EREC_IraIratok iratObj = GetIratObjById(iratId);

            #region Kimenő küldemény(ek) lekérése

            var kuldemenyekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            EREC_KuldKuldemenyekSearch kimenoKuldSearch = new EREC_KuldKuldemenyekSearch(true);
            // Irat Id-ra szűrünk:
            kimenoKuldSearch.Extended_EREC_IraIratokSearch.Id.Value = iratId.ToString();
            kimenoKuldSearch.Extended_EREC_IraIratokSearch.Id.Operator = Query.Operators.equals;

            var resultKimenoKuld = kuldemenyekService.KimenoKuldGetAllWithExtensionAndJogosultak(this.ExecParamCaller, kimenoKuldSearch, false);
            resultKimenoKuld.CheckError();

            // Kigyűjtjük a küldemény Id-kat, és azzal szűrjük meg a tértivevények listát:
            List<Guid> kimenoKuldIdList = new List<Guid>();
            foreach (DataRow row in resultKimenoKuld.Ds.Tables[0].Rows)
            {
                kimenoKuldIdList.Add((Guid)row["Id"]);
            }
            #endregion

            #region Tértivevények lekérése

            var tertivevenyekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
            EREC_KuldTertivevenyekSearch tertiSearch = new EREC_KuldTertivevenyekSearch();
            // Szűrés a küldemény Id listára
            tertiSearch.Kuldemeny_Id.Value = "'" + String.Join("','", kimenoKuldIdList.Select(e => e.ToString()).ToArray()) + "'";
            tertiSearch.Kuldemeny_Id.Operator = Query.Operators.inner;

            var resultTerti = tertivevenyekService.GetAllWithExtension(this.ExecParamCaller, tertiSearch);
            resultTerti.CheckError();

            #endregion

            #region Eredmény összeállítása

            List<KuldemenyAllapot> kuldemenyAllapotok = new List<KuldemenyAllapot>();

            foreach (DataRow row in resultTerti.Ds.Tables[0].Rows)
            {
                KuldemenyAllapot kuldAllapot = new KuldemenyAllapot();

                kuldAllapot.kuldemeny_id = row["Kuldemeny_Id"] as Guid?;

                /// ragszam_kr_befogadasi_szam: Ragszám vagy elektronikus esetben a központi rendszer befogadási száma
                /// TODO: "elektronikus esetben a központi rendszer befogadási száma": Ez is a RagSzam mezőben van?
                /// 
                kuldAllapot.ragszam_kr_befogadasi_szam = row["Ragszam"].ToString();

                /// kezbesitesi_csatorna: DMS konfigurációs állománytól függő értékkészlet Pl: e = elektronikus, p = papír alapú.
                /// --> Irat adathordozó típusa alapján tudjuk (EREC_IraIratok.AdathordozoTipusa mező, UGYINTEZES_ALAPJA kódcsoport)
                /// 
                bool elektronikus = iratObj.AdathordozoTipusa == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                kuldAllapot.kezbesitesi_csatorna = elektronikus ? "e" : "p";

                kuldAllapot.feladas_idopontja = Helper.GetFormattedDateAndTimeString(row["BelyegzoDatuma"]);

                kuldAllapot.atveteli_idopont = Helper.GetFormattedDateOnlyString(row["AtvetelDat"]);

                kuldAllapot.tertiveveny_visszaerkezes_datuma = Helper.GetFormattedDateOnlyString(row["TertivisszaDat"]);

                ///(-1) = kézbesítés alatt, 0 = kézbesítve,
                /// 0 - tól nagyobb:   tértivevény információk alapján hibakódok
                /// TODO: 'kézbesítve (0)' akkor van, ha a TertivisszaKod mező alapján Címzett átvette vagy Más átvette?
                /// 'kézbesítés alatt (-1)' akkor van, ha nincs kézbesítve, de még nincs kitöltve a TertivisszaDat mező? 
                /// 
                string tertiVisszaKod = row["TertivisszaKod"].ToString();
                if (tertiVisszaKod == KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_Cimzettnek
                    || tertiVisszaKod == KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_kozvetett_kezbesitonek)
                {
                    // Kézbesítve:
                    kuldAllapot.kezbesitettsegi_allapot = "0";
                }
                else if (String.IsNullOrEmpty(kuldAllapot.tertiveveny_visszaerkezes_datuma))
                {
                    // Kézbesítés alatt:
                    kuldAllapot.kezbesitettsegi_allapot = "-1";
                }
                else
                {
                    kuldAllapot.kezbesitettsegi_allapot = tertiVisszaKod;
                }

                kuldAllapot.barcode = row["BarCode"].ToString();

                if (Szukites != "I")
                {
                    kuldAllapot.Cimzett = new Cimzett();
                    kuldAllapot.Cimzett.Nev = row["Cimzett_Nev_Partner"].ToString();
                    kuldAllapot.Cimzett.Cim = row["Cimzett_Cim_Partner"].ToString();
                }

                /// AtNemVett == "I" érték esetén csak az át nem vett küldeményekre vonatkozó információkat adja vissza. 
                if (AtNemVett == "I"
                    && (tertiVisszaKod == KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_Cimzettnek
                        || tertiVisszaKod == KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_kozvetett_kezbesitonek))
                {
                    // Nem adjuk hozzá a listához:
                    continue;
                }

                kuldemenyAllapotok.Add(kuldAllapot);
            }

            kuldemeny_allapot = kuldemenyAllapotok.ToArray();

            #endregion

            Logger.InfoEnd("SZURManager.SZURChkKuldemenyTertiAllapotWS");
        }
    }
}
