﻿namespace Contentum.eIntegrator.WebService.NMHH
{
    // SZURManager segédeljárásai
    public partial class SZURManager
    {
        public static class Kodtar_NMHH_EXPMOD
        {
            /// <summary>
            /// 04
            /// </summary>
            public const string ELEKTRONIKUS_UTON_HKP = "elektronikus úton (Hivatali kapu)";
            /// <summary>
            /// 20
            /// </summary>
            public const string POSTAI_UTON = "postai úton";
        }
    }
}