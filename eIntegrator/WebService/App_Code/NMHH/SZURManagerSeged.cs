﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Contentum.eUtility.ExcelImport;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Contentum.eIntegrator.WebService.NMHH
{
    // SZURManager segédeljárásai
    public partial class SZURManager
    {
        public const string SZURWSDateFormatString = "yyyy.MM.dd.";
        public const string SZURWSDateTimeFormatString = "yyyy.MM.dd. HH:mm";

        public const string KCS_UGYIRAT_FELFUGGESZTES_OKA = "UGYIRAT_FELFUGGESZTES_OKA";
        public const string KCS_LEZARAS_OKA = "LEZARAS_OKA";
        public const string KCS_IRAT_HATASA_UGYINTEZESRE = "IRAT_HATASA_UGYINTEZESRE";
        public const string KCS_KULDEMENY_TIPUS = "KULDEMENY_TIPUS";
        public const string KCS_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";
        public const string KCS_NMHH_EXPMOD = "NMHH_EXPMOD";
        public const string KCS_VISSZAFIZETES_JOGCIME = "VISSZAFIZETES_JOGCIME";
        //public const string SakkoraHatasTipus_START = "START";
        //public const string SakkoraHatasTipus_PAUSE = "PAUSE";
        //public const string SakkoraHatasTipus_STOP = "STOP";

        public const string TargyszoAzon_Jogerore_emelkedes_datuma = "Jogerore_emelkedes_datuma";
        public const string TargyszoAzon_Jogerositest_vegzo = "Jogerositest_vegzo";
        public const string TargyszoAzon_Jogerositest_vegzo_latta = "Jogerositest_vegzo_latta";
        public const string TargyszoAzon_Jogerositendo = "Jogerositendo";
        //public const string TargyszoAzon_KODTARGYSZO_KOR = "KODTARGYSZO_KOR";
        public const string TargyszoAzon_TARGYSZOK = "Targyszok";
        public const string TargyszoAzon_Visszafizetendo_dij = "Visszafizetendo_dij";
        public const string TargyszoAzon_Visszafizetes_cimzettje = "Visszafizetes_cimzettje";
        public const string TargyszoAzon_Visszafizetes_hatarideje = "Visszafizetes_hatarideje";
        public const string TargyszoAzon_Visszafizetes_hatarozatszama = "Visszafizetes_hatarozatszama";
        public const string TargyszoAzon_Visszafizetes_jogcime = "Visszafizetes_jogcime";
        public const string TargyszoAzon_Rendszerkod = "Rendszerkod";
        public const string KodcsoportKod_Rendszerkod = "RENDSZERKOD";
        public const string KodcsoportKod_Restricted = "RESTRICTED";
        public const string KodtarKod_Restricted_Igen = "IGEN";

        public const string TargyszoAzon_FT1 = "FT1";
        public const string TargyszoAzon_FT2 = "FT2";
        public const string TargyszoAzon_FT3 = "FT3";

        // BUG_2742
        public const string TargyszoAzon_IT1 = "IT1";
        public const string TargyszoAzon_IT2 = "IT2";
        public const string TargyszoAzon_IT3 = "IT3";

        public const string TargyszoAzon_statElsofok = "statElsofok";
        public const string TargyszoAzon_statJogorv = "statJogorv";
        public const string TargyszoAzon_statElsofokNap = "statElsofokNap";
        public const string TargyszoAzon_statElsoJogNap = "statElsoJogNap";
        public const string TargyszoAzon_statMasodNap = "statMasodNap";
        public const string TargyszoAzon_statAtlag = "statAtlag";
        public const string TargyszoAzon_statOsszesDb = "statOsszesDb";
        public const string TargyszoAzon_statKarigeny = "statKarigeny";
        public const string TargyszoAzon_statFelugyelet = "statFelugyelet";

        private const string FunkcioKod_AtvetelSzervezettol = "AtvetelSzervezettol";

        public const string Ugyirat_ElintezesiMod = "90"; // Egyéb

        public const string Note_Technikai = "Technikai munkairat"; // Egyéb
        public const string KodtarKod_ElosztoIv_Fajta_Cimzettlista = "10";
        private const string ParamNameCsoporttagsagParameterError = "Hiányoznak a paraméterek a csoporttagság meghatározásához !";

        public class FoundPartner
        {
            public bool Found { get { return !String.IsNullOrEmpty(PartnerCim_Id); } }
            public string Partner_Id { get; set; }
            public string Cim_Id { get; set; }
            public string PartnerCim_Id { get; set; }
            public string Cim_Fajta { get; set; }

            public FoundPartner()
            {
            }

            public FoundPartner(Result resultPartner)
            {
                if (resultPartner != null && resultPartner.GetCount > 0)
                {
                    var row = resultPartner.Ds.Tables[0].Rows[0];
                    Partner_Id = row["Partner_Id"].ToString();
                    Cim_Id = row["Cim_Id"].ToString();
                    PartnerCim_Id = row["Partner_Id"].ToString();
                    Cim_Fajta = row["Fajta"].ToString();
                }
            }
        }

        #region Iktatókönyv

        /// <summary>
        /// Iktatókönyvek cache (Nem statikus, hanem példányszintű, tehát minden webservice híváskor újrainicializálódik.)
        /// </summary>
        private Dictionary<Guid, EREC_IraIktatoKonyvek> _iktatoKonyvekCache = new Dictionary<Guid, EREC_IraIktatoKonyvek>();

        public static string ErrorMsgParameterErrorCsoportTagsagLekerdezes { get; private set; }

        /// <summary>
        /// Iktatókönyv objektum lekérdezése Id alapján
        /// </summary>
        /// <param name="iktatokonyvId"></param>
        /// <returns></returns>
        private EREC_IraIktatoKonyvek GetIktatokonyv(Guid iktatokonyvId, bool useCache)
        {
            if (useCache
                && _iktatoKonyvekCache.ContainsKey(iktatokonyvId))
            {
                return _iktatoKonyvekCache[iktatokonyvId];
            }

            var iktatokonyvekService = eUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            var execParamGetIktatokonyv = ExecParamCaller.Clone();
            execParamGetIktatokonyv.Record_Id = iktatokonyvId.ToString();

            // Iktatókönyv lekérése:
            var resultIktatoKonyv = iktatokonyvekService.Get(execParamGetIktatokonyv);
            resultIktatoKonyv.CheckError();

            EREC_IraIktatoKonyvek iktatoKonyv = resultIktatoKonyv.Record as EREC_IraIktatoKonyvek;

            // Elmentjük a cachebe:
            _iktatoKonyvekCache[iktatokonyvId] = iktatoKonyv;

            return iktatoKonyv;
        }
        /// <summary>
        /// Hozzáadja a paraméterként kapott listához azokat a IratID-kat amik az execparamban levő userre vannak szignálva.
        /// </summary>       
        public void AddSignedIratsForUser(EREC_IraIratokService service, ExecParam execParam, List<Guid> iratIdList)
        {
            var searchObjIratok = new EREC_IraIratokSearch();
            searchObjIratok.Allapot.Filter(KodTarak.IRAT_ALLAPOT.Szignalt);
            searchObjIratok.FelhasznaloCsoport_Id_Ugyintez.Filter(execParam.Felhasznalo_Id);

            var felhasznaloraSzignaltIratok = service.GetAllWithExtension(execParam, searchObjIratok);
            felhasznaloraSzignaltIratok.CheckError();

            foreach (DataRow row in felhasznaloraSzignaltIratok.Ds.Tables[0].Rows)
            {
                Guid iratId = (Guid)row["Id"];
                if (!iratIdList.Contains(iratId))
                {
                    iratIdList.Add(iratId);
                }
            }
        }

        /// <summary>
        /// Megadott iktatóhely alapján iktatókönyv DataRow lekérése
        /// </summary>
        /// <param name="iktatoHely"></param>
        /// <returns></returns>
        private DataRow GetIktatokonyvRow(string iktatoHely, int ev)
        {
            var iktatokonyvekService = eUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();

            EREC_IraIktatoKonyvekSearch searchObj = new EREC_IraIktatoKonyvekSearch();
            searchObj.Iktatohely.Filter(iktatoHely);

            searchObj.Ev.Filter(ev.ToString());

            var resultGetAll = iktatokonyvekService.GetAll(ExecParamCaller, searchObj);
            resultGetAll.CheckError();

            // Ellenőrzés: csak 1 találat lehet:
            if (resultGetAll.GetCount == 0)
            {
                throw new ResultException("Az iktatókönyv nem található!");
            }
            else if (resultGetAll.GetCount > 1)
            {
                throw new ResultException("Iktatókönyv lekérdezése sikertelen: több találat is van!");
            }

            return resultGetAll.Ds.Tables[0].Rows[0];
        }

        #endregion

        /// <summary>
        /// Irat objektum létrehozása az ügyirathoz 0-ás alszámmal (EREC_UgyUgyiratokService.GetAllWithExtension-nel lekért DataRow-ból)
        /// </summary>
        /// <param name="rowUgyirat"></param>
        /// <returns></returns>
        private Irat CreateIratFromUgyiratGetAllWithExt(DataRow rowUgyirat)
        {
            Irat irat = new Irat();

            SetIratObjDefaultVales(ref irat);

            irat.Ugy = rowUgyirat["Foszam_Merge"].ToString();
            // TODO:
            //irat.Eloszam == ??
            irat.Evszam = rowUgyirat["Ev"].ToString();
            irat.Konyv = rowUgyirat["Iktatohely"].ToString();

            irat.Foszam = rowUgyirat["Foszam"].ToString();
            // Ügyiratnál 0-ás alszámot állítunk be:
            irat.Alszam = "0";

            irat.Szignalo = rowUgyirat["Szignalo"].ToString();
            DateTime? szignalasDat = rowUgyirat["SzignalasIdeje"] as DateTime?;
            if (szignalasDat != null)
            {
                irat.Szignalas = szignalasDat.Value.ToString(SZURWSDateFormatString);
            }
            DateTime? hatarido = rowUgyirat["Hatarido"] as DateTime?;
            if (hatarido != null)
            {
                irat.Hatarido = hatarido.Value.ToString(SZURWSDateFormatString);
            }
            irat.IratTargya = rowUgyirat["Targy"].ToString();
            if (rowUgyirat.Table.Columns.Contains("UgyFelelos_SzervezetKod"))
                irat.Eloszam = rowUgyirat["UgyFelelos_SzervezetKod"].ToString();
            return irat;
        }

        /// <summary>
        /// Irat objektum létrehozása az ügyirathoz 0-ás alszámmal (EREC_UgyUgyiratokService.GetAllWithExtension-nel lekért DataRow-ból)
        /// </summary>
        /// <param name="rowUgyirat"></param>
        /// <returns></returns>
        private eIrat CreateeIratFromUgyiratGetAllWithExt(DataRow rowUgyirat)
        {
            eIrat irat = new eIrat();

            SetIratObjDefaultVales(ref irat);

            irat.Ugy = rowUgyirat["Foszam_Merge"].ToString();
            // TODO:
            //irat.Eloszam == ??
            irat.Evszam = rowUgyirat["Ev"].ToString();
            irat.Konyv = rowUgyirat["Iktatohely"].ToString();

            irat.Foszam = rowUgyirat["Foszam"].ToString();
            // Ügyiratnál 0-ás alszámot állítunk be:
            irat.Alszam = "0";

            irat.Szignalo = rowUgyirat["Szignalo"].ToString();
            DateTime? szignalasDat = rowUgyirat["SzignalasIdeje"] as DateTime?;
            if (szignalasDat != null)
            {
                irat.Szignalas = szignalasDat.Value.ToString(SZURWSDateFormatString);
            }
            DateTime? hatarido = rowUgyirat["Hatarido"] as DateTime?;
            if (hatarido != null)
            {
                irat.Hatarido = hatarido.Value.ToString(SZURWSDateFormatString);
            }
            irat.IratTargya = rowUgyirat["Targy"].ToString();
            if (rowUgyirat.Table.Columns.Contains("UgyFelelos_SzervezetKod"))
                irat.Eloszam = rowUgyirat["UgyFelelos_SzervezetKod"].ToString();
            return irat;
        }

        // BUG_11574
        public static void ParseBekuldoCim(string bekuldoCim, out string irsz, out string telepules, out string cim)
        {
            irsz = "";
            telepules = "";
            if (!String.IsNullOrEmpty(bekuldoCim) && bekuldoCim.Contains(" "))
            {
                bekuldoCim = bekuldoCim.Replace(", ", " ").Trim();
                bekuldoCim = bekuldoCim.Replace(",", " ");
                var p = bekuldoCim.IndexOfAny("0123456789".ToCharArray());
                if (p >= 0)
                {
                    // "Magyarország 1234 Település Cím további része 123" kezelése
                    bekuldoCim = bekuldoCim.Substring(p);

                    // "1234 Település Cím további része 123"
                    var parts = bekuldoCim.Split(' ');
                    if (parts.Length > 1 && parts[0].Length == 4)
                    {
                        irsz = parts[0];
                        telepules = parts[1];
                        if (parts.Length > 2)
                        {
                            bekuldoCim = bekuldoCim.Substring(irsz.Length + 1 + telepules.Length + 1);
                        }
                    }
                }
            }

            cim = bekuldoCim;
        }

        /// <summary>
        /// EREC_IraIratokService.GetAllWithExtension -nel lekért adatsorból Irat objektum létrehozása
        /// (+ néhány ügyiratra vonatkozó adat miatt az ügyirat DataRow-ja is kell (ami az EREC_UgyUgyIratokService.GetAllWithExtension-nel lett lekérve))
        /// </summary>
        /// <param name="rowIrat"></param>
        /// <returns></returns>
        private Irat CreateIratFromIratGetAllWithExt(DataRow rowIrat, DataRow rowUgyirat)
        {
            Irat irat = new Irat();

            SetIratObjDefaultVales(ref irat);

            irat.Ugy = rowIrat["IktatoSzam_Merge"].ToString();
            // TODO:
            //irat.Eloszam == ??
            irat.Evszam = rowIrat["Ev"].ToString();
            irat.Konyv = rowIrat["Iktatohely"].ToString();

            irat.Foszam = rowIrat["Foszam"].ToString();
            irat.Alszam = rowIrat["Alszam"].ToString();

            irat.IratTipus = rowIrat["IrattipusNev"].ToString();

            if (rowUgyirat != null)
            {
                irat.Szignalo = rowUgyirat["Szignalo"].ToString();
                DateTime? szignalasDat = rowUgyirat["SzignalasIdeje"] as DateTime?;
                if (szignalasDat != null)
                {
                    irat.Szignalas = szignalasDat.Value.ToString(SZURWSDateFormatString);
                }
                if (rowUgyirat.Table.Columns.Contains("UgyFelelos_SzervezetKod"))
                    irat.Eloszam = rowUgyirat["UgyFelelos_SzervezetKod"].ToString();
            }

            irat.Bekuldo = rowIrat["NevSTR_Bekuldo"].ToString();
            irat.BekuldoCim = rowIrat["CimSTR_Bekuldo"].ToString();

            irat.BekuldoCim = ExtendCimWithComma(irat.BekuldoCim);
            string irsz, telepules, cim;
            ParseBekuldoCim(irat.BekuldoCim, out irsz, out telepules, out cim);
            irat.BekuldoIrsz = irsz;
            irat.BekuldoTelepules = telepules;
            irat.BekuldoCim = cim;
            //irat.BekuldoHirAzonosito

            DateTime? beerkezesIdeje = rowIrat["BeerkezesIdeje"] as DateTime?;
            if (beerkezesIdeje != null)
            {
                irat.Erkezett = beerkezesIdeje.Value.ToString(SZURWSDateFormatString);
            }
            DateTime? hatarido = rowIrat["Hatarido"] as DateTime?;
            if (hatarido != null)
            {
                irat.Hatarido = hatarido.Value.ToString(SZURWSDateFormatString);
            }
            irat.IratTargya = rowIrat["Targy1"].ToString();
            // TODO: ezek honnan jönnek?
            //irat.DokTipusHivatal
            //irat.DokTipusAzonosito
            //irat.DokTipusLeiras

            return irat;
        }

        /// <summary>
        /// EREC_IraIratokService.GetAllWithExtension -nel lekért adatsorból Irat objektum létrehozása
        /// (+ néhány ügyiratra vonatkozó adat miatt az ügyirat DataRow-ja is kell (ami az EREC_UgyUgyIratokService.GetAllWithExtension-nel lett lekérve))
        /// </summary>
        /// <param name="rowIrat"></param>
        /// <returns></returns>
        private eIrat CreateNeweIratFromIratGetAllWithExt(DataRow rowIrat, DataRow rowUgyirat)
        {
            eIrat irat = new eIrat();

            SetIratObjDefaultVales(ref irat);

            irat.Ugy = rowIrat["IktatoSzam_Merge"].ToString();
            // TODO:
            //irat.Eloszam == ??
            irat.Evszam = rowIrat["Ev"].ToString();
            irat.Konyv = rowIrat["Iktatohely"].ToString();

            irat.Foszam = rowIrat["Foszam"].ToString();
            irat.Alszam = rowIrat["Alszam"].ToString();

            irat.IratTipus = rowIrat["IrattipusNev"].ToString();

            if (rowUgyirat != null)
            {
                irat.Szignalo = rowUgyirat["Szignalo"].ToString();
                DateTime? szignalasDat = rowUgyirat["SzignalasIdeje"] as DateTime?;
                if (szignalasDat != null)
                {
                    irat.Szignalas = szignalasDat.Value.ToString(SZURWSDateFormatString);
                }
                if (rowUgyirat.Table.Columns.Contains("UgyFelelos_SzervezetKod"))
                    irat.Eloszam = rowUgyirat["UgyFelelos_SzervezetKod"].ToString();
            }

            irat.Bekuldo = rowIrat["NevSTR_Bekuldo"].ToString();
            irat.BekuldoCim = rowIrat["CimSTR_Bekuldo"].ToString();

            irat.BekuldoCim = ExtendCimWithComma(irat.BekuldoCim);
            string irsz, telepules, cim;
            ParseBekuldoCim(irat.BekuldoCim, out irsz, out telepules, out cim);
            irat.BekuldoIrsz = irsz;
            irat.BekuldoTelepules = telepules;
            irat.BekuldoCim = cim;
            // TODO: ez így nincs meg külön a GetAllWithExtension-ben:
            //irat.BekuldoHirAzonosito

            DateTime? beerkezesIdeje = rowIrat["BeerkezesIdeje"] as DateTime?;
            if (beerkezesIdeje != null)
            {
                irat.Erkezett = beerkezesIdeje.Value.ToString(SZURWSDateFormatString);
            }
            DateTime? hatarido = rowIrat["Hatarido"] as DateTime?;
            if (hatarido != null)
            {
                irat.Hatarido = hatarido.Value.ToString(SZURWSDateFormatString);
            }
            irat.IratTargya = rowIrat["Targy1"].ToString();
            // TODO: ezek honnan jönnek?
            //irat.DokTipusHivatal
            //irat.DokTipusAzonosito
            //irat.DokTipusLeiras

            return irat;
        }

        /// <summary>
        /// EREC_UgyUgyiratokService.GetAllWithExtension -nel lekért adatsorból eIrat objektum létrehozása
        /// </summary>
        /// <param name="rowUgyirat"></param>
        /// <returns></returns>
        private eIrat CreateNeweIratFromUgyiratGetAllWithExt(DataRow rowUgyirat)
        {
            eIrat irat = new eIrat();

            SetIratObjDefaultVales(ref irat);

            irat.Ugy = rowUgyirat["Foszam_Merge"].ToString();
            //TODO - tisztázni: Az átvételre váró irat előszáma, Contentumban a felelős szerv. egység. Kódot kell adni, ami a rövid név.
            if (rowUgyirat != null && rowUgyirat.Table.Columns.Contains("UgyFelelos_SzervezetKod"))
                irat.Eloszam = rowUgyirat["UgyFelelos_SzervezetKod"].ToString();
            irat.Evszam = rowUgyirat["Ev"].ToString();
            // A SZÜR rendszerben ez K/B/N (Külső/Belső/Érkeztetőkönyv) értékek voltak? ==> Az Iktatókönyv azonosítóját adjuk neki
            irat.Konyv = rowUgyirat["Iktatohely"].ToString();

            irat.Foszam = rowUgyirat["Foszam"].ToString();
            irat.Alszam = "0";

            return irat;
        }

        #region Felhasználók, csoportok

        /// <summary>
        /// NMHH-s webService kérésében megadott UserId alapján ExecParam létrehozása
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ExecParam CreateExecParamByNMHHUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ResultException("Felhasználó azonosító megadása kötelező !");

            return CreateExecParamByNMHHUserId(userId, this.ExecParamCaller);
        }

        public ExecParam CreateExecParamByNMHHUserId(string userId, Utility.EnumXpmFelhasznaloSzervezetTipus xpmSzervTipus, string xpmSzervFilter)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ResultException("Felhasználó azonosító megadása kötelező !");

            return CreateExecParamByNMHHUserId(userId, this.ExecParamCaller, xpmSzervTipus, xpmSzervFilter);
        }


        /// <summary>
        /// NMHH-s webService kérésében megadott UserId alapján ExecParam létrehozása
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="execParamCaller"></param>
        /// <returns></returns>
        public static ExecParam CreateExecParamByNMHHUserId(string userId, ExecParam execParamCaller)
        {
            ExecParam execParam;
            Result resultCsoportTagok;
            SetExecParameterByUser(userId, execParamCaller, out execParam, out resultCsoportTagok);

            // TODO: Mi van, ha több szervezete is van?
            // --> Egyelőre nem dobunk hibát, az elsőt választjuk ki
            if (resultCsoportTagok.Ds.Tables[0].Rows.Count > 0)
            {
                execParam.FelhasznaloSzervezet_Id = resultCsoportTagok.Ds.Tables[0].Rows[0]["Csoport_Id"].ToString();
                execParam.CsoportTag_Id = resultCsoportTagok.Ds.Tables[0].Rows[0]["Id"].ToString();
            }

            return execParam;
        }
        /// <summary>
        /// CreateExecParamByNMHHUserId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="execParamCaller"></param>
        /// <param name="xpmSzervTipus"></param>
        /// <param name="xpmSzervFilter"></param>
        /// <returns></returns>
        public static ExecParam CreateExecParamByNMHHUserId(string userId, ExecParam execParamCaller, Utility.EnumXpmFelhasznaloSzervezetTipus xpmSzervTipus, string xpmSzervFilter)
        {
            ExecParam execParam;
            Result resultCsoportTagok;
            SetExecParameterByUser(userId, execParamCaller, out execParam, out resultCsoportTagok);

            if (resultCsoportTagok.Ds.Tables[0].Rows.Count < 1)
                return execParam;

            string specSzervId = null;
            switch (xpmSzervTipus)
            {
                case Utility.EnumXpmFelhasznaloSzervezetTipus.Iktatokonyv:
                    specSzervId = KodTarak.SPEC_SZERVEK.GetKozpontiIktato(execParam).Obj_Id;
                    break;
                case Utility.EnumXpmFelhasznaloSzervezetTipus.Irattar:
                    specSzervId = xpmSzervFilter;
                    break;
                case Utility.EnumXpmFelhasznaloSzervezetTipus.Irattaros:
                    specSzervId = xpmSzervFilter;
                    break;
                case Utility.EnumXpmFelhasznaloSzervezetTipus.ElektronikusIrattar:
                    specSzervId = KodTarak.SPEC_SZERVEK.GetElektronikusIrattar(execParam);
                    break;
                case Utility.EnumXpmFelhasznaloSzervezetTipus.EgyebSzervezetiIrattar:
                    specSzervId = KodTarak.SPEC_SZERVEK.GetEgyebSzervezetIrattar(execParam);
                    break;
                case Utility.EnumXpmFelhasznaloSzervezetTipus.Elintezett:
                    specSzervId = xpmSzervFilter;
                    break;
                default:
                    specSzervId = null;
                    break;
            }

            if (string.IsNullOrEmpty(specSzervId))
            {
                execParam.FelhasznaloSzervezet_Id = resultCsoportTagok.Ds.Tables[0].Rows[0]["Csoport_Id"].ToString();
                execParam.CsoportTag_Id = resultCsoportTagok.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            else
            {
                DataRow row = CheckAndGetDataSetContainsItem(resultCsoportTagok.Ds.Tables[0].Rows, "Csoport_Id", specSzervId);
                if (row != null)
                {
                    execParam.FelhasznaloSzervezet_Id = row["Csoport_Id"].ToString();
                    execParam.CsoportTag_Id = row["Id"].ToString();
                }
                else
                {
                    execParam.FelhasznaloSzervezet_Id = resultCsoportTagok.Ds.Tables[0].Rows[0]["Csoport_Id"].ToString();
                    execParam.CsoportTag_Id = resultCsoportTagok.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
            }

            return execParam;
        }

        /// <summary>
        /// SetExecParameterByUser
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="execParamCaller"></param>
        /// <param name="execParam"></param>
        /// <param name="resultCsoportTagok"></param>
        private static void SetExecParameterByUser(string userId, ExecParam execParamCaller, out ExecParam execParam, out Result resultCsoportTagok)
        {
            DataRow rowFelhasznalo = GetFelhasznaloRowByUserNev(userId, execParamCaller);

            execParam = new ExecParam
            {
                Felhasznalo_Id = rowFelhasznalo["Id"].ToString(),
                Org_Id = rowFelhasznalo["Org"].ToString(),
                LoginUser_Id = rowFelhasznalo["Id"].ToString(),
            };
            string csoport = rowFelhasznalo.Table.Columns.Contains("Partner_Id_Munkahely") ? rowFelhasznalo["Partner_Id_Munkahely"].ToString() : string.Empty;

            if (string.IsNullOrEmpty(csoport))
            {
                resultCsoportTagok = GetCsoportTagok(execParamCaller, execParam.Felhasznalo_Id, null);
            }
            else
            {
                resultCsoportTagok = GetCsoportTagok(execParamCaller, execParam.Felhasznalo_Id, csoport);
                if (resultCsoportTagok.IsError || resultCsoportTagok.Ds.Tables.Count < 1)
                {
                    resultCsoportTagok = GetCsoportTagok(execParamCaller, execParam.Felhasznalo_Id, null);
                }
            }
        }
        /// <summary>
        /// GetCsoportTagok
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="jogalany"></param>
        /// <param name="csoport"></param>
        /// <returns></returns>
        private static Result GetCsoportTagok(ExecParam execParam, string jogalany, string csoport)
        {
            if (execParam == null || string.IsNullOrEmpty(jogalany))
            {
                throw new ArgumentNullException(ErrorMsgParameterErrorCsoportTagsagLekerdezes);
            }
            eAdmin.Service.KRT_CsoportTagokService csoportTagokService = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch searchObjCsoportTagok = new KRT_CsoportTagokSearch();
            searchObjCsoportTagok.Csoport_Id_Jogalany.Filter(jogalany);
            if (!string.IsNullOrEmpty(csoport))
            {
                searchObjCsoportTagok.Csoport_Id.Filter(csoport);
            }
            Result resultCsoportTagok = csoportTagokService.GetAll(execParam, searchObjCsoportTagok);
            resultCsoportTagok.CheckError();
            return resultCsoportTagok;
        }
        /// <summary>
        /// UserNev alapján KRT_Felhasznalok DataRow visszaadása
        /// </summary>
        /// <param name="userNev"></param>
        /// <returns></returns>
        private DataRow GetFelhasznaloRowByUserNev(string userNev)
        {
            return GetFelhasznaloRowByUserNev(userNev, this.ExecParamCaller);
        }

        /// <summary>
        /// UserNev alapján KRT_Felhasznalok DataRow visszaadása
        /// </summary>
        /// <param name="userNev"></param>
        /// <param name="execParamCaller"></param>
        /// <returns></returns>
        private static DataRow GetFelhasznaloRowByUserNev(string userNev, ExecParam execParamCaller)
        {
            var felhasznalokService = eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

            var searchObj = new Contentum.eQuery.BusinessDocuments.KRT_FelhasznalokSearch();

            searchObj.UserNev.Filter(userNev);

            var result = felhasznalokService.GetAll(execParamCaller, searchObj);
            result.CheckError();

            // Egy ilyen felhasználó lehet csak elvileg:
            if (result.GetCount != 1)
            {
                throw new ResultException(String.Format("Felhasználó beazonosítása sikertelen! ({0})", userNev));
            }

            return result.Ds.Tables[0].Rows[0];
        }

        /// <summary>
        /// ID alapján KRT_Felhasznalo visszaadása
        /// </summary>
        /// <param name="userNev"></param>
        /// <param name="execParamCaller"></param>
        /// <returns></returns>
        private static KRT_Felhasznalok GetFelhasznloById(string userID, ExecParam execParamCaller)
        {
            var felhasznalokService = eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            ExecParam execParamFelhGet = execParamCaller.Clone();
            execParamFelhGet.Record_Id = userID;

            var resultFelhGet = felhasznalokService.Get(execParamFelhGet);
            resultFelhGet.CheckError();

            KRT_Felhasznalok felhasznaloObj = resultFelhGet.Record as KRT_Felhasznalok;

            return felhasznaloObj;
        }

        /// <summary>
        /// Csoport DataRow lekérése a csoport neve alapján
        /// </summary>
        /// <param name="csoportNev"></param>
        /// <returns></returns>
        private DataRow GetCsoportRowByNev(string csoportNev)
        {
            KRT_CsoportokSearch searchObj = new KRT_CsoportokSearch();
            searchObj.Nev.Filter(csoportNev);

            var result = eAdminService.ServiceFactory.GetKRT_CsoportokService().GetAll(this.ExecParamCaller, searchObj);
            result.CheckError();

            // Ellenőrzés: csak 1 találat lehet:
            if (result.GetCount == 0)
            {
                throw new ResultException("Nem található ilyen nevű csoport: " + csoportNev);
            }
            else if (result.GetCount > 1)
            {
                /// BUG#5714: Ha több csoport is van ilyen névvel, akkor kis-nagybetűre is próbálunk egyeztetni, 
                /// és ha úgy már csak 1 találat van, akkor OK.
                /// 
                List<DataRow> rowsFullEqual = new List<DataRow>();
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    // C# string egyezőség már kis-nagybetű érzékeny:
                    if (row["Nev"].ToString() == csoportNev)
                    {
                        rowsFullEqual.Add(row);
                    }
                }

                // Ha pont 1 db van, amelyiknél pontosan egyezik a név, akkor azt adjuk vissza, egyébként hiba:
                if (rowsFullEqual.Count == 1)
                {
                    return rowsFullEqual[0];
                }
                else
                {
                    throw new ResultException("Csoport beazonosítása sikertelen, mert ilyen névvel több csoport is létezik a rendszerben: " + csoportNev);
                }
            }
            else
            {
                return result.Ds.Tables[0].Rows[0];
            }
        }

        /// <summary>
        /// Ellenőrzés, hogy a megadott felhasználó a megadott csoportba tartozik-e (közvetlenül)
        /// </summary>
        /// <param name="felhasznaloId"></param>
        /// <param name="csoportId"></param>
        /// <param name="felhasznaloNev">Csak az esetleges hibaüzenet miatt kell megadni</param>
        /// <param name="csoportNev">Csak az esetleges hibaüzenet miatt kell megadni</param>
        private void CheckCsoporttagsag(Guid felhasznaloId, Guid csoportId, string felhasznaloNev, string csoportNev)
        {
            var csoportTagokService = eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

            var searchObjCsoportTagok = new eQuery.BusinessDocuments.KRT_CsoportTagokSearch();
            searchObjCsoportTagok.Csoport_Id_Jogalany.Filter(felhasznaloId.ToString());
            searchObjCsoportTagok.Csoport_Id.Filter(csoportId.ToString());

            var resultCsoportTagok = csoportTagokService.GetAll(ExecParamCaller, searchObjCsoportTagok);
            resultCsoportTagok.CheckError();

            // Ha nincs találat, akkor nem tagja a csoportnak:
            if (resultCsoportTagok.GetCount == 0)
            {
                throw new ResultException(String.Format("{0} felhasználó nem tagja a(z) {1} szervezetnek!", felhasznaloNev, csoportNev));
            }
        }

        #endregion

        #region Ügyirat adatok lekérése

        /// <summary>
        /// Ügyirat lekérése (GetAllWithExtension eljárással)
        /// </summary>
        /// <returns></returns>
        public DataRow GetUgyiratRow(string evszam, string konyv, string foszam)
        {
            return this.GetUgyiratRow(evszam, konyv, foszam, this.ExecParamCaller);
        }

        /// <summary>
        /// Ügyirat lekérése (GetAllWithExtension eljárással)
        /// </summary>
        /// <returns></returns>
        public DataRow GetUgyiratRow(string evszam, string konyv, string foszam, ExecParam execParam)
        {
            #region Ellenőrzés

            if (String.IsNullOrEmpty(evszam))
            {
                throw new ArgumentNullException("Evszam");
            }
            //if (String.IsNullOrEmpty(konyv))
            //{
            //    throw new ArgumentNullException("Konyv");
            //}
            if (String.IsNullOrEmpty(foszam))
            {
                throw new ArgumentNullException("Foszam");
            }

            #endregion

            var ugyiratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            var searchObjUgyiratok = new EREC_UgyUgyiratokSearch(true);

            searchObjUgyiratok.Extended_EREC_IraIktatoKonyvekSearch.Ev.Filter(evszam);

            // Előfordul, hogy a webserviceben nincs iktatókönyv paraméter (merthogy úgyis csak egy iktatókönyv van NMHH-ban), ilyenkor erre nem szűrünk:
            if (!String.IsNullOrEmpty(konyv))
            {
                // Konyv: IktatoHely mezőre szűrünk
                searchObjUgyiratok.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Filter(konyv);
            }

            searchObjUgyiratok.Foszam.Filter(foszam);

            // Lekérdezés adatbázisból:
            var resultUgyirat = ugyiratokService.GetAllWithExtension(execParam, searchObjUgyiratok);
            resultUgyirat.CheckError();

            // Ellenőrzés: csak 1 találat lehet:
            if (resultUgyirat.GetCount == 0)
            {
                throw new ResultException("Az ügyirat nem található!");
            }
            else if (resultUgyirat.GetCount > 1)
            {
                throw new ResultException("Ügyirat lekérdezése sikertelen: több találat is van!");
            }

            return resultUgyirat.Ds.Tables[0].Rows[0];
        }

        public DataRow GetUgyiratRow(string ugyAzonosito, ExecParam execParam)
        {
            if (string.IsNullOrEmpty(ugyAzonosito))
                throw new ArgumentNullException("Ugy");

            var ugyiratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            if (ugyiratokService == null)
                throw new ResultException("Belső hiba: GetEREC_UgyUgyiratokService");

            var searchObjUgyiratok = new EREC_UgyUgyiratokSearch(true);

            searchObjUgyiratok.Azonosito.Filter(ugyAzonosito);

            // Lekérdezés adatbázisból:
            var resultUgyirat = ugyiratokService.GetAllWithExtension(execParam, searchObjUgyiratok);
            resultUgyirat.CheckError();

            // Ellenőrzés: csak 1 találat lehet:
            if (resultUgyirat.GetCount == 0)
            {
                throw new ResultException("Az ügyirat nem található!");
            }
            else if (resultUgyirat.GetCount > 1)
            {
                throw new ResultException("Ügyirat lekérdezése sikertelen: több találat is van!");
            }

            return resultUgyirat.Ds.Tables[0].Rows[0];
        }

        public DataRow GetUgyiratRow(Guid ugyiratId)
        {
            var ugyiratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            if (ugyiratokService == null)
                throw new ResultException("Belső hiba: GetEREC_UgyUgyiratokService");


            ExecParam execParamUgyiratGet = this.ExecParamCaller.Clone();
            execParamUgyiratGet.Record_Id = ugyiratId.ToString();

            var searchObjUgyiratok = new EREC_UgyUgyiratokSearch(true);
            searchObjUgyiratok.Id.Filter(ugyiratId.ToString());

            // Lekérdezés adatbázisból:
            var resultUgyirat = ugyiratokService.GetAllWithExtension(execParamUgyiratGet, searchObjUgyiratok);
            resultUgyirat.CheckError();

            // Ellenőrzés: csak 1 találat lehet:
            if (resultUgyirat.GetCount == 0)
            {
                throw new ResultException("Az ügyirat nem található!");
            }
            else if (resultUgyirat.GetCount > 1)
            {
                throw new ResultException("Ügyirat lekérdezése sikertelen: több találat is van!");
            }

            return resultUgyirat.Ds.Tables[0].Rows[0];
        }

        /// <summary>
        /// EREC_UgyUgyiratok objektum lekérése ügyirat Id alapján
        /// </summary>
        /// <param name="ugyiratId"></param>
        /// <returns></returns>
        private EREC_UgyUgyiratok GetUgyiratObjById(Guid ugyiratId)
        {
            var ugyiratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            ExecParam execParamUgyiratGet = this.ExecParamCaller.Clone();
            execParamUgyiratGet.Record_Id = ugyiratId.ToString();

            var resultUgyiratGet = ugyiratService.Get(execParamUgyiratGet);
            resultUgyiratGet.CheckError();

            EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok)resultUgyiratGet.Record;
            return ugyiratObj;
        }

        /// <summary>
        /// Ügyirat határidejének és hátralévő napoknak a számítása (napban, vagy munkanapban megadva, amit az ügy típusa határoz meg).
        /// </summary>
        /// <param name="ugyiratId"></param>
        /// <param name="ugyRow"></param>
        /// <param name="hatarido"></param>
        /// <param name="hatralevoNapok"></param>
        /// <param name="szamitasModja"></param>
        /// <param name="ugyintezesiIdo"></param>
        private void GetUgyiratHatarido(Guid ugyiratId, DataRow ugyRow, out DateTime? hatarido, out int? hatralevoNapok, out string szamitasModja, out int? ugyintezesiIdo)
        {
            #region INIT
            hatarido = null;
            hatralevoNapok = null;
            szamitasModja = null;
            ugyintezesiIdo = null;
            #endregion

            if (!string.IsNullOrEmpty(ugyRow["IntezesiIdo"].ToString()))
            {
                int tmpIntIdo;
                if (int.TryParse(ugyRow["IntezesiIdo"].ToString(), out tmpIntIdo))
                {
                    ugyintezesiIdo = tmpIntIdo;
                }
            }

            #region IDOEGYSEG
            string idoEgyseg = null;
            if (!string.IsNullOrEmpty(ugyRow["IntezesiIdoegyseg"].ToString()))
            {
                idoEgyseg = ugyRow["IntezesiIdoegyseg"].ToString();
            }

            if (string.IsNullOrEmpty(idoEgyseg))
            {
                // Rendszerparaméterből olvassuk ki:
                idoEgyseg = eUtility.Rendszerparameterek.Get(this.ExecParamCaller, eUtility.Rendszerparameterek.DEFAULT_INTEZESI_IDO_IDOEGYSEG);
            }

            #region IDOEGYSEG
            if (idoEgyseg == "-1440")
            {
                szamitasModja = Utility.SzamitasModjaMunkanap;
            }
            else
            {
                szamitasModja = Utility.SzamitasModjaNap;
            }
            #endregion

            #endregion

            #region HATARIDO
            if (string.IsNullOrEmpty(ugyRow["Hatarido"].ToString()))
                hatarido = null;
            else
            {
                DateTime tmpDate;
                if (DateTime.TryParse(ugyRow["Hatarido"].ToString(), out tmpDate))
                {
                    hatarido = tmpDate;
                }
            }
            #endregion

            if (hatarido == null)
            {
                hatralevoNapok = null;
                szamitasModja = string.Empty;
                return;
            }

            #region LEZARAS DATE
            DateTime? lezarasDat;
            if (string.IsNullOrEmpty(ugyRow["LezarasDat"].ToString()))
                lezarasDat = null;
            else
            {
                DateTime tmpDate;
                if (DateTime.TryParse(ugyRow["LezarasDat"].ToString(), out tmpDate))
                {
                    lezarasDat = tmpDate;
                }
            }
            #endregion

            #region HATRALEVO NAPOK
            if (string.IsNullOrEmpty(ugyRow["HatralevoNapok"].ToString()))
                hatralevoNapok = null;
            else
            {
                int tmpHatNap;
                if (int.TryParse(ugyRow["HatralevoNapok"].ToString(), out tmpHatNap))
                {
                    hatralevoNapok = tmpHatNap;
                }
            }
            #endregion
        }
        /// <summary>
        /// Ügyirat határidejének és hátralévő napoknak a számítása (napban, vagy munkanapban megadva, amit az ügy típusa határoz meg).
        /// (CR#3263-ban leírva)
        /// </summary>
        /// <param name="ugyiratId"></param>
        /// <param name="hatarido"></param>
        /// <param name="hatralevoNapok"></param>
        /// <param name="szamitasModja">Elvárt értékek: „naptári nap” vagy „munkanap”</param>
        private void GetUgyiratHatarido(Guid ugyiratId, out DateTime? hatarido, out int? hatralevoNapok, out string szamitasModja, out int? ugyintezesiIdo)
        {
            // Ügyirat lekérése:
            EREC_UgyUgyiratok ugyiratObj = GetUgyiratObjById(ugyiratId);

            #region Számítási mód lekérése

            /// Vagy az EREC_IratMetaDefinicio-ból, vagy rendszerparaméterből kérjük le
            /// Ha az időegység == -1440, akkor munkanapban számolunk, egyébként napban
            /// 

            string idoEgyseg = null;
            ugyintezesiIdo = null;
            if (!string.IsNullOrEmpty(ugyiratObj.IntezesiIdo))
            {
                int tmpIntIdo;
                if (int.TryParse(ugyiratObj.IntezesiIdo, out tmpIntIdo))
                {
                    ugyintezesiIdo = tmpIntIdo;
                }
            }

            if (!string.IsNullOrEmpty(ugyiratObj.IntezesiIdoegyseg))
            {
                idoEgyseg = ugyiratObj.IntezesiIdoegyseg;
            }

            /*
            if (!String.IsNullOrEmpty(ugyiratObj.IratMetadefinicio_Id))
            {
                var serviceIratMetaDef = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

                var execParamGetIratMetaDef = this.ExecParamCaller.Clone();
                execParamGetIratMetaDef.Record_Id = ugyiratObj.IratMetadefinicio_Id;

                var resultIratMetaDef = serviceIratMetaDef.Get(execParamGetIratMetaDef);
                if (resultIratMetaDef.IsError)
                {
                    throw new ResultException(resultIratMetaDef);
                }

                EREC_IratMetaDefinicio iratMetaDef = (EREC_IratMetaDefinicio)resultIratMetaDef.Record;

                idoEgyseg = iratMetaDef.Idoegyseg;

                ugyintezesiIdo = (!iratMetaDef.Typed.UgyiratIntezesiIdo.IsNull) ? iratMetaDef.Typed.UgyiratIntezesiIdo.Value : null as int?;
            }

            if (ugyintezesiIdo == null || ugyintezesiIdo == 0)
            {
                // Rendszerparaméterből kell kiolvasni:
                ugyintezesiIdo = eUtility.Rendszerparameterek.GetInt(this.ExecParamCaller,
                        eUtility.Rendszerparameterek.DEFAULT_INTEZESI_IDO);
            }
            */
            if (string.IsNullOrEmpty(idoEgyseg))
            {
                // Rendszerparaméterből olvassuk ki:
                idoEgyseg = eUtility.Rendszerparameterek.Get(this.ExecParamCaller, eUtility.Rendszerparameterek.DEFAULT_INTEZESI_IDO_IDOEGYSEG);
            }

            #endregion

            hatarido = (!ugyiratObj.Typed.Hatarido.IsNull) ? ugyiratObj.Typed.Hatarido.Value : (DateTime?)null;

            if (hatarido == null)
            {
                hatralevoNapok = null;
                szamitasModja = string.Empty;

                return;
            }

            #region Hátralévő napok számítása

            /// Ha még nincs lezárva, akkor a mai naphoz nézzük, hogy mennyi van még hátra/mennyi telt el.
            /// Ha már lezárták, akkor a lezárás dátuma és a határidő közti különbséget nézzük.
            /// 
            DateTime? lezarasDat = (!ugyiratObj.Typed.LezarasDat.IsNull) ? ugyiratObj.Typed.LezarasDat.Value : (DateTime?)null;
            // Amihez viszonyítjuk a hátralévő napok számát: vagy a mai nap, vagy a lezárás napja.
            DateTime refDat = lezarasDat ?? DateTime.Today;

            if (idoEgyseg == "-1440")
            {
                int? munkanapokSzama;

                // Munkanap
                szamitasModja = "munkanap";
                // Ha már a határidőn túl vagyunk, akkor meg kell cserélni a függvényben a paramétereket, és az eredményt minuszba át kell tenni:
                if (hatarido.Value < refDat)
                {
                    munkanapokSzama = Utility.DataBaseFunctions.GetMunkanapokSzama(this.SZURService, hatarido.Value, refDat);
                    hatralevoNapok = (munkanapokSzama != null) ? (0 - munkanapokSzama.Value) : (int?)null;
                }
                else
                {
                    munkanapokSzama = Utility.DataBaseFunctions.GetMunkanapokSzama(this.SZURService, refDat, hatarido.Value);
                    hatralevoNapok = (munkanapokSzama != null) ? munkanapokSzama.Value : (int?)null;
                }
            }
            else
            {
                // Naptári nap:
                szamitasModja = "naptári nap";
                hatralevoNapok = (int)(refDat - hatarido.Value).TotalDays;
            }

            #endregion
        }

        #endregion

        #region Irat adatok lekérése

        /// <summary>
        /// Irat lekérdezése id alapján
        /// </summary>
        /// <param name="iratId"></param>
        /// <returns></returns>
        public DataRow GetIratRow(Guid iratId)
        {
            var iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
            if (iratokService == null)
                throw new ResultException("Belső hiba: GetEREC_IraIratokService");

            ExecParam execParamIratGet = this.ExecParamCaller.Clone();
            execParamIratGet.Record_Id = iratId.ToString();

            var searchObjIratok = new EREC_IraIratokSearch(true);
            searchObjIratok.Id.Filter(iratId.ToString());

            // Lekérdezés adatbázisból:
            var resultIrat = iratokService.GetAllWithExtension(execParamIratGet, searchObjIratok);
            resultIrat.CheckError();

            // Ellenőrzés: csak 1 találat lehet:
            if (resultIrat.GetCount == 0)
            {
                throw new ResultException("Az irat nem található!");
            }
            else if (resultIrat.GetCount > 1)
            {
                throw new ResultException("Irat lekérdezése sikertelen: több találat is van!");
            }

            return resultIrat.Ds.Tables[0].Rows[0];
        }

        /// <summary>
        /// Irat lekérése (GetAllWithExtension eljárással)
        /// </summary>
        /// <param name="evszam"></param>
        /// <param name="foszam"></param>
        /// <param name="alszam"></param>
        /// <returns></returns>
        public DataRow GetIratRow(string evszam, string foszam, string alszam)
        {
            return this.GetIratRow(evszam, null, foszam, alszam, this.ExecParamCaller);
        }

        /// <summary>
        /// Irat lekérése (GetAllWithExtension eljárással)
        /// </summary>
        /// <param name="evszam"></param>
        /// <param name="konyv"></param>
        /// <param name="foszam"></param>
        /// <param name="alszam"></param>
        /// <returns></returns>
        public DataRow GetIratRow(string evszam, string konyv, string foszam, string alszam)
        {
            return this.GetIratRow(evszam, konyv, foszam, alszam, this.ExecParamCaller);
        }

        /// <summary>
        /// Irat lekérése (GetAllWithExtension eljárással)
        /// </summary>
        /// </summary>
        /// <param name="evszam"></param>
        /// <param name="konyv"></param>
        /// <param name="foszam"></param>
        /// <param name="alszam"></param>
        /// <returns></returns>
        public DataRow GetIratRow(string evszam, string konyv, string foszam, string alszam, ExecParam execParam)
        {
            var iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();

            var searchObjIratok = new EREC_IraIratokSearch(true);

            searchObjIratok.Extended_EREC_IraIktatoKonyvekSearch.Ev.Filter(evszam);

            // Előfordul, hogy a webserviceben nincs iktatókönyv paraméter (merthogy úgyis csak egy iktatókönyv van NMHH-ban), ilyenkor erre nem szűrünk:
            // Konyv: IktatoHely mezőre szűrünk
            searchObjIratok.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.FilterIfNotEmpty(konyv);

            searchObjIratok.Extended_EREC_UgyUgyiratdarabokSearch.Foszam.Filter(foszam);
            searchObjIratok.Alszam.Filter(alszam);

            // Lekérdezés adatbázisból:
            var resultIrat = iratokService.GetAllWithExtension(execParam, searchObjIratok);
            resultIrat.CheckError();

            // Ellenőrzés: csak 1 találat lehet:
            if (resultIrat.GetCount == 0)
            {
                throw new ResultException("Az irat nem található!");
            }
            else if (resultIrat.GetCount > 1)
            {
                throw new ResultException("Irat lekérdezése sikertelen: több találat is van!");
            }

            return resultIrat.Ds.Tables[0].Rows[0];
        }

        /// <summary>
        /// EREC_IraIratok objektum lekérése irat Id alapján
        /// </summary>
        /// <param name="ugyiratId"></param>
        /// <returns></returns>
        private EREC_IraIratok GetIratObjById(Guid iratId)
        {
            var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            ExecParam execParamIratGet = this.ExecParamCaller.Clone();
            execParamIratGet.Record_Id = iratId.ToString();

            var resultIratGet = iratokService.Get(execParamIratGet);
            resultIratGet.CheckError();

            EREC_IraIratok iratObj = (EREC_IraIratok)resultIratGet.Record;
            return iratObj;
        }

        #endregion

        #region Iratpéldány adatok lekérése

        /// <summary>
        /// Iratpéldány lekérése (GetAllWithExtension eljárással)
        /// </summary>
        /// <param name="evszam"></param>
        /// <param name="konyv"></param>
        /// <param name="foszam"></param>
        /// <param name="alszam"></param>
        /// <param name="sorszam"></param>
        /// <returns></returns>
        public DataRow GetIratPeldanyRow(string evszam, string konyv, string foszam, string alszam, string sorszam)
        {
            return this.GetIratPeldanyRow(evszam, konyv, foszam, alszam, sorszam, this.ExecParamCaller);
        }

        /// <summary>
        /// Iratpéldány lekérése (GetAllWithExtension eljárással)
        /// </summary>
        /// <param name="evszam"></param>
        /// <param name="konyv"></param>
        /// <param name="foszam"></param>
        /// <param name="alszam"></param>
        /// <param name="sorszam"></param>
        /// <returns></returns>
        public DataRow GetIratPeldanyRow(string evszam, string konyv, string foszam, string alszam, string sorszam, ExecParam execParam)
        {
            var iratPldService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

            var searchObjIratPld = new EREC_PldIratPeldanyokSearch(true);

            searchObjIratPld.Extended_EREC_IraIktatoKonyvekSearch.Ev.Filter(evszam);

            // Konyv: IktatoHely mezőre szűrünk
            searchObjIratPld.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Filter(konyv);
            searchObjIratPld.Extended_EREC_UgyUgyiratokSearch.Foszam.Filter(foszam);
            searchObjIratPld.Extended_EREC_IraIratokSearch.Alszam.Filter(alszam);
            searchObjIratPld.Sorszam.Filter(sorszam);

            // Lekérdezés adatbázisból:
            var resultIratPld = iratPldService.GetAllWithExtension(execParam, searchObjIratPld);
            resultIratPld.CheckError();

            // Ellenőrzés: csak 1 találat lehet:
            if (resultIratPld.GetCount == 0)
            {
                throw new ResultException("Iratpéldány nem található!");
            }
            else if (resultIratPld.GetCount > 1)
            {
                throw new ResultException("Iratpéldány lekérdezése sikertelen: több találat is van!");
            }

            return resultIratPld.Ds.Tables[0].Rows[0];
        }

        /// <summary>
        /// Iratpéldány lekérése (GetAllWithExtension eljárással)
        /// </summary>
        /// <param name="evszam"></param>
        /// <param name="konyv"></param>
        /// <param name="foszam"></param>
        /// <param name="alszam"></param>
        /// <returns></returns>
        public DataRowCollection GetIratPeldanyRow(string evszam, string konyv, string foszam, string alszam, ExecParam execParam)
        {
            var iratPldService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

            var searchObjIratPld = new EREC_PldIratPeldanyokSearch(true);

            searchObjIratPld.Extended_EREC_IraIktatoKonyvekSearch.Ev.Filter(evszam);

            // Konyv: IktatoHely mezőre szűrünk
            searchObjIratPld.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Filter(konyv);
            searchObjIratPld.Extended_EREC_UgyUgyiratokSearch.Foszam.Filter(foszam);
            searchObjIratPld.Extended_EREC_IraIratokSearch.Alszam.Filter(alszam);

            // Lekérdezés adatbázisból:
            var resultIratPld = iratPldService.GetAllWithExtension(execParam, searchObjIratPld);
            resultIratPld.CheckError();

            // Ellenőrzés: csak 1 találat lehet:
            if (resultIratPld.GetCount == 0)
            {
                throw new ResultException("Iratpéldány nem található!");
            }

            return resultIratPld.Ds.Tables[0].Rows;
        }

        /// <summary>
        /// Irat összes iratpéldányának lekérése (GetAllWithExtension eljárással)
        /// </summary>
        /// <param name="evszam"></param>
        /// <param name="konyv"></param>
        /// <param name="foszam"></param>
        /// <param name="alszam"></param>
        /// <returns></returns>
        public DataRowCollection GetAllIratPeldanyRows(string evszam, string konyv, string foszam, string alszam, ExecParam execParam)
        {
            var iratPldService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

            var searchObjIratPld = new EREC_PldIratPeldanyokSearch(true);

            searchObjIratPld.Extended_EREC_IraIktatoKonyvekSearch.Ev.Filter(evszam);

            // Konyv: IktatoHely mezőre szűrünk
            searchObjIratPld.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Filter(konyv);
            searchObjIratPld.Extended_EREC_UgyUgyiratokSearch.Foszam.Filter(foszam);
            searchObjIratPld.Extended_EREC_IraIratokSearch.Alszam.Filter(alszam);

            // Lekérdezés adatbázisból:
            var resultIratPld = iratPldService.GetAllWithExtension(execParam, searchObjIratPld);
            resultIratPld.CheckError();

            return resultIratPld.Ds.Tables[0].Rows;
        }

        /// <summary>
        /// EREC_PldIratPeldanyok objektum lekérése iratpéldány Id alapján
        /// </summary>
        /// <param name="pldId"></param>
        /// <returns></returns>
        private EREC_PldIratPeldanyok GetIratPeldanyObjById(Guid pldId)
        {
            var pldService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

            ExecParam execParamPldGet = ExecParamCaller.Clone();
            execParamPldGet.Record_Id = pldId.ToString();

            var resultPldGet = pldService.Get(execParamPldGet);
            resultPldGet.CheckError();

            EREC_PldIratPeldanyok pldObj = (EREC_PldIratPeldanyok)resultPldGet.Record;
            return pldObj;
        }

        #endregion

        /// <summary>
        /// Megadott irat aláírás bejegyzései (EREC_IratAlairok)
        /// </summary>
        /// <param name="iratId"></param>
        /// <param name="filterAlairoSzerep"></param>
        /// <returns></returns>
        public DataTable GetIratAlairok(Guid iratId, string filterAlairoSzerep)
        {
            var iratAlairokService = eRecordService.ServiceFactory.GetEREC_IratAlairokService();

            EREC_IratAlairokSearch searchIratAlairok = new EREC_IratAlairokSearch();
            searchIratAlairok.Obj_Id.Filter(iratId.ToString());
            searchIratAlairok.AlairoSzerep.FilterIfNotEmpty(filterAlairoSzerep);

            var resultAlairokGetAll = iratAlairokService.GetAllWithExtension(ExecParamCaller, searchIratAlairok);
            resultAlairokGetAll.CheckError();

            return resultAlairokGetAll.Ds.Tables[0];
        }

        /// <summary>
        /// A megadott dátumtól számított x. munkanapot adja vissza
        /// </summary>
        /// <param name="datumTol"></param>
        /// <param name="munkanapokSzama"></param>
        /// <returns></returns>
        public DateTime GetKovetkezoMunkanap(DateTime datumTol, int munkanapokSzama)
        {
            var extraNapokService = eUtility.eAdminService.ServiceFactory.GetKRT_Extra_NapokService();

            var result = extraNapokService.KovetkezoMunkanap(this.ExecParamCaller, datumTol, munkanapokSzama);
            result.CheckError();

            return (DateTime)result.Record;
        }

        /// <summary>
        /// A megadott két dátum közti munkanapok számát adja vissza
        /// </summary>
        /// <param name="datumTol"></param>
        /// <param name="datumIg"></param>
        /// <returns></returns>
        public int GetMunkanapokSzama(DateTime datumTol, DateTime datumIg)
        {
            int naptariNapKulonbseg = (int)(datumIg - datumTol).TotalDays;

            if (naptariNapKulonbseg <= 0) { return 0; }

            /// Nincs ilyen függvényünk, ezért tippelgetünk a napokkal...
            /// A naptári nap különbséggel tippelünk...
            /// Addig kell tippelgetni, amíg vagy el nem találjuk a végdátumot, 
            /// vagy pedig addig szűkítjük a tippeket, amíg a végdátum bele nem esik két egymást közvetlenül követő tipp eredménye közé.
            /// --> Ehhez eltároljuk a végdátumhoz legközelebb eső kisebb és nagyobb tippeket...
            ///             
            int tipp = (int)(naptariNapKulonbseg * 0.7);

            /// Azok a tippek, amik közé kell esnie a keresett munkanapszámnak.
            /// E két érték között tippelünk, és egyre szűkítjuk ezt az intervallumot.
            /// 
            //int tippMin = 0;
            //int tippMax = naptariNapKulonbseg;
            int? tippMin = null;
            int? tippMax = null;

            // Ciklusszámláló:
            int n = 0;

            do
            {
                n++;
                DateTime tippEredmeny = this.GetKovetkezoMunkanap(datumTol, tipp);

                int elteres = (int)(tippEredmeny - datumIg).TotalDays;

                if (elteres == 0)
                {
                    // Megvan az eredmény:
                    return tipp;
                }
                else if (tippEredmeny > datumIg)
                {
                    tippMax = tipp;
                }
                else if (tippEredmeny < datumIg)
                {
                    tippMin = tipp;
                }

                /// Ha 1 napra csökkent a tippMin és tippMax különbsége, (és nem találtuk el a céldátumot),
                /// akkor az azt jelenti, hogy a tippMinre kisebb dátum jött ki, a tippMax-ra nagyobb, 
                /// tehát 'tippMin' db munkanap van a céldátumig. (És a céldátum nem munkanapra esik.)
                /// 
                if (tippMax != null && tippMin != null
                    && tippMax - tippMin == 1)
                {
                    return tippMin.Value;
                }

                // Új tipp meghatározása: tippMin és tippMax közé kell esnie
                tipp = (tippMin ?? 0) + (int)(((tippMax ?? naptariNapKulonbseg) - (tippMin ?? 0)) * 0.7);
                // tippMin-nél nagyobbnak kell lennie:
                if (tippMin != null)
                {
                    tipp = Math.Max(tipp, tippMin.Value + 1);
                }
                // tippMax-nál kisebbnek kell lennie:
                if (tippMax != null)
                {
                    tipp = Math.Min(tipp, tippMax.Value - 1);
                }
            }
            // Elvileg csak return-nel jöhetünk ki a ciklusból, de biztonság kedvéért, hogy biztosan ne legyen végtelen ciklus:
            while (n <= naptariNapKulonbseg);

            return 0;
        }

        /// <summary>
        /// A megadott ügyirathoz kapcsolt ügyek listája
        /// (EREC_UgyiratObjKapcsolatok táblából azok, ahol az adott ügyirat vagy az Obj_Id_Elozmeny, vagy pedig az Obj_Id_Kapcsolt)
        /// </summary>
        /// <param name="ugyiratId"></param>
        /// <returns></returns>
        private List<KapcsoltUgy> GetKapcsoltUgyekList(Guid ugyiratId, ExecParam execParamUserId)
        {
            // EREC_UgyiratObjKapcsolatok lekérése, ahol az adott ügyirat vagy az Obj_Id_Elozmeny, vagy pedig az Obj_Id_Kapcsolt

            var serviceUgyiratObjKapcsolatok = eRecordService.ServiceFactory.GetEREC_UgyiratObjKapcsolatokService();

            var searchUgyiratObjKapcsolatok = new EREC_UgyiratObjKapcsolatokSearch();

            searchUgyiratObjKapcsolatok.Obj_Id_Elozmeny.Filter(ugyiratId.ToString());
            searchUgyiratObjKapcsolatok.Obj_Id_Elozmeny.OrGroup("69");

            searchUgyiratObjKapcsolatok.Obj_Id_Kapcsolt.Filter(ugyiratId.ToString());
            searchUgyiratObjKapcsolatok.Obj_Id_Kapcsolt.OrGroup("69");

            searchUgyiratObjKapcsolatok.Obj_Type_Elozmeny.Filter("EREC_UgyUgyiratok");
            searchUgyiratObjKapcsolatok.Obj_Type_Kapcsolt.Filter("EREC_UgyUgyiratok");

            var resultUgyiratObjKapcsolatok = serviceUgyiratObjKapcsolatok.GetAll(execParamUserId, searchUgyiratObjKapcsolatok);
            resultUgyiratObjKapcsolatok.CheckError();

            // Az eredményből kigyűjtjük a kapcsolt ügyirat Id-kat:
            List<Guid> kapcsoltUgyiratIdList = new List<Guid>();

            foreach (DataRow rowObjKapcsolat in resultUgyiratObjKapcsolatok.Ds.Tables[0].Rows)
            {
                Guid objIdElozmeny = (Guid)rowObjKapcsolat["Obj_Id_Elozmeny"];
                Guid objIdKapcsolt = (Guid)rowObjKapcsolat["Obj_Id_Kapcsolt"];

                if (objIdElozmeny != ugyiratId)
                {
                    kapcsoltUgyiratIdList.Add(objIdElozmeny);
                }
                else if (objIdKapcsolt != ugyiratId)
                {
                    kapcsoltUgyiratIdList.Add(objIdKapcsolt);
                }
            }

            List<KapcsoltUgy> kapcsoltUgyekList = new List<KapcsoltUgy>();

            /// Kapcsolt ügyirat adatok lekérdezése:
            /// BUG#3493/Task#3547:
            /// "a válasz egy struktúrált lista kell legyen, főszám + a hozzá létező alszámok"
            /// --> az ügyiratok összes iratát kell lekérni, és azokat visszaadni
            /// 
            if (kapcsoltUgyiratIdList.Count > 0)
            {
                var searchKapcsoltUgyiratokIratai = new EREC_IraIratokSearch(true);
                // Id listára szűrünk:
                searchKapcsoltUgyiratokIratai.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.In(kapcsoltUgyiratIdList.Select(e => e.ToString()));

                var resultKapcsoltUgyiratokIratai = eRecordService.ServiceFactory.GetEREC_IraIratokService().GetAllWithExtension(execParamUserId, searchKapcsoltUgyiratokIratai);
                resultKapcsoltUgyiratokIratai.CheckError();

                // Eredmény feldolgozása:
                foreach (DataRow rowKapcsoltUgyiratIrata in resultKapcsoltUgyiratokIratai.Ds.Tables[0].Rows)
                {
                    KapcsoltUgy kapcsoltUgy = new KapcsoltUgy();

                    kapcsoltUgy.Alszam = rowKapcsoltUgyiratIrata["Alszam"].ToString();
                    kapcsoltUgy.Evszam = rowKapcsoltUgyiratIrata["Ev"].ToString();
                    kapcsoltUgy.Foszam = rowKapcsoltUgyiratIrata["Foszam"].ToString();
                    kapcsoltUgy.Konyv = rowKapcsoltUgyiratIrata["Iktatohely"].ToString();

                    kapcsoltUgyekList.Add(kapcsoltUgy);
                }
            }

            return kapcsoltUgyekList;
        }

        /// <summary>
        /// Megadott tábla típus KRT_ObjTipusok Id-ja
        /// </summary>
        /// <param name="tablaKod"></param>
        /// <returns></returns>
        private Guid GetObjtipusId(string tablaKod)
        {
            KRT_ObjTipusokSearch searchObj = new KRT_ObjTipusokSearch();
            searchObj.Kod.Filter(tablaKod);
            searchObj.Obj_Id_Szulo.IsNull();

            var result = eAdminService.ServiceFactory.GetKRT_ObjTipusokService().GetAll(ExecParamCaller, searchObj);
            result.CheckError();

            if (result.GetCount != 1)
            {
                Logger.Debug("Találatszám: " + result.GetCount.ToString());
                // Hiba:
                throw new ResultException("Objektumtípus lekérése sikertelen: " + tablaKod);
            }

            return (Guid)result.Ds.Tables[0].Rows[0]["Id"];
        }

        /// <summary>
        /// A megadott EREC_IratAlairok rekord főbb mezőinek módosítása
        /// </summary>
        /// <param name="erec_IratAlairok_Id"></param>
        private void UpdateIratAlairok(Guid erec_IratAlairok_Id, string allapot, string alairasDatuma, string felhasznaloIdAlairo)
        {
            var iratAlairokService = eRecordService.ServiceFactory.GetEREC_IratAlairokService();

            // EREC_IratAlairok UPDATE:                
            ExecParam execParamAlairokGet = ExecParamCaller.Clone();
            execParamAlairokGet.Record_Id = erec_IratAlairok_Id.ToString();
            var resultAlairokGet = iratAlairokService.Get(execParamAlairokGet);
            resultAlairokGet.CheckError();

            EREC_IratAlairok iratAlairokObj = (EREC_IratAlairok)resultAlairokGet.Record;
            iratAlairokObj.Updated.SetValueAll(false);
            iratAlairokObj.Base.Updated.SetValueAll(false);

            iratAlairokObj.Allapot = allapot;
            iratAlairokObj.Updated.Allapot = true;

            iratAlairokObj.AlairasDatuma = alairasDatuma;
            iratAlairokObj.Updated.AlairasDatuma = true;

            iratAlairokObj.FelhasznaloCsoport_Id_Alairo = felhasznaloIdAlairo;
            iratAlairokObj.Updated.FelhasznaloCsoport_Id_Alairo = true;

            iratAlairokObj.Base.Updated.Ver = true;

            // Mentés:
            var resultUpdate = iratAlairokService.Update(execParamAlairokGet, iratAlairokObj);
            resultUpdate.CheckError();
        }

        #region Tárgyszavak kezelése

        /// <summary>
        /// Megadott objektumhoz a megadott tárgyszó bejegyzése (Insert/Update)
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="objTableName"></param>
        /// <param name="targyszoAzon"></param>
        /// <param name="ertek"></param>
        /// <param name="execParamUserId"></param>
        private void SetObjektumTargyszo(Guid objId, string objTableName, string targyszoAzon, string ertek, ExecParam execParamUserId)
        {
            this.SetObjektumTargyszavak(objId, objTableName, execParamUserId, new KeyValuePair<string, string>(targyszoAzon, ertek));
        }

        /// <summary>
        /// Megadott objektumhoz a megadott tárgyszó/érték párok bejegyzése (tömeges)
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="objTableName"></param>
        /// <param name="execParamUserId"></param>
        /// <param name="targyszoErtekek">Tárgyszó azonosító - tárgyszó érték párok</param>
        private void SetObjektumTargyszavak(Guid objId, string objTableName, ExecParam execParamUserId, params KeyValuePair<string, string>[] targyszoErtekek)
        {
            #region Objektum tárgyszavak lekérése

            var serviceObjTargyszavai = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

            EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

            //Result resultGetAll = serviceObjTargyszavai.GetAllMetaByObjMetaDefinicio(execParamUserId, search, objId.ToString(), null
            //    , objTableName, null, null, false
            //    , null, false, false);  // KodTarak.OBJMETADEFINICIO_TIPUS.B1

            Result resultGetAll = serviceObjTargyszavai.GetAllMetaByDefinicioTipus(execParamUserId, search, objId.ToString(), null, objTableName, null, false, false);
            resultGetAll.CheckError();

            Dictionary<string, DataRow> objTargyszavakDict = new Dictionary<string, DataRow>();

            foreach (DataRow row in resultGetAll.Ds.Tables[0].Rows)
            {
                string belsoAzonosito = row["BelsoAzonosito"].ToString();

                // Paraméterben jött-e ilyen tárgyszó:
                var keresettTargyszo = targyszoErtekek.FirstOrDefault(e => e.Key == belsoAzonosito);

                objTargyszavakDict[belsoAzonosito] = row;
            }

            #endregion

            #region EREC_ObjektumTargyszavai objektumok feltöltése

            List<EREC_ObjektumTargyszavai> modositandoObjTargyszavak = new List<EREC_ObjektumTargyszavai>();

            foreach (KeyValuePair<string, string> modositandoTargyszo in targyszoErtekek)
            {
                // Ha a lekérdezés nem adott vissza ilyen tárgyszót, akkor hiba:
                if (!objTargyszavakDict.ContainsKey(modositandoTargyszo.Key))
                {
                    throw new ResultException("Hiányzó tárgyszó: " + modositandoTargyszo.Key);
                }

                DataRow rowObjTargyszo = objTargyszavakDict[modositandoTargyszo.Key];

                // Tárgyszavas objektum feltöltése:
                EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                erec_ObjektumTargyszavai.Updated.SetValueAll(false);
                erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

                erec_ObjektumTargyszavai.Id = rowObjTargyszo["Id"].ToString();
                erec_ObjektumTargyszavai.Updated.Id = true;

                erec_ObjektumTargyszavai.Obj_Metaadatai_Id = rowObjTargyszo["Obj_Metaadatai_Id"].ToString();
                erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

                erec_ObjektumTargyszavai.Targyszo_Id = rowObjTargyszo["Targyszo_Id"].ToString();
                erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

                erec_ObjektumTargyszavai.Targyszo = rowObjTargyszo["Targyszo"].ToString();
                erec_ObjektumTargyszavai.Updated.Targyszo = true;

                erec_ObjektumTargyszavai.Ertek = modositandoTargyszo.Value;
                erec_ObjektumTargyszavai.Updated.Ertek = true;

                modositandoObjTargyszavak.Add(erec_ObjektumTargyszavai);
            }

            #endregion

            #region Objektum tárgyszó insert/update

            // Insert/update webservice hívása:
            var resultInsertUpdate = serviceObjTargyszavai.InsertOrUpdateValuesByObjMetaDefinicio(
                            execParamUserId
                            , modositandoObjTargyszavak.ToArray()
                            , objId.ToString()
                            , null
                            , objTableName, "*", null, false, null, false, false);

            resultInsertUpdate.CheckError();

            #endregion
        }

        /// <summary>
        /// Megadott objektumhoz tartozó tárgyszó/érték párok lekérdezése
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="objTableName"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetObjektumTargyszoErtekek(Guid objId, string objTableName)
        {
            // Tárgyszóértékek lekérése:
            var result = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService()
                            .GetAllMetaByDefinicioTipus(this.ExecParamCaller, new EREC_ObjektumTargyszavaiSearch()
                                    , objId.ToString(), null, objTableName, null, false, false);
            result.CheckError();

            Dictionary<string, string> targyszoErtekek = new Dictionary<string, string>();

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                string targyszoKod = row["BelsoAzonosito"].ToString();
                string targyszoErtek = row["Ertek"].ToString();

                targyszoErtekek[targyszoKod] = targyszoErtek;
            }

            return targyszoErtekek;
        }

        #endregion

        #region Partnerek

        /// <summary>
        /// Kapcsolati kódok meglétének ellenőrzése a partnertörzsben (KRT_Partnerek.KulsoAzonosito -ra ellenőrzünk)
        /// Hiányzó kapcsolati kód esetén Exception.
        /// </summary>
        /// <param name="kapcsolatiKodLista"></param>
        /// <param name="kapcsolatiKodPartnerIdDict">Kapcsolati kód - partner Id párok</param>
        private void PartnerKapcsolatiKodokEllenorzes(List<string> kapcsolatiKodLista, out Dictionary<string, Guid> kapcsolatiKodPartnerIdDict)
        {
            kapcsolatiKodPartnerIdDict = new Dictionary<string, Guid>();

            if (kapcsolatiKodLista.Count > 0)
            {
                KRT_PartnerekSearch searchObj = new KRT_PartnerekSearch();
                searchObj.KulsoAzonositok.In(kapcsolatiKodLista);

                var resultPartnerek = eAdminService.ServiceFactory.GetKRT_PartnerekService().GetAll(this.ExecParamCaller, searchObj);
                resultPartnerek.CheckError();

                // Ebbe tesszük az eredménylistából a KulsoAzonositok értékeket:
                List<string> resultKulsoAzonositok = new List<string>();

                foreach (DataRow row in resultPartnerek.Ds.Tables[0].Rows)
                {
                    string kulsoAzon = row["KulsoAzonositok"].ToString();
                    resultKulsoAzonositok.Add(kulsoAzon);

                    kapcsolatiKodPartnerIdDict[kulsoAzon] = (Guid)row["Id"];
                }

                /// Bejövő adatok összevetése az eredménylistával:
                /// Ha van olyan, ami az eredménylistában nem szerepel, akkor hiba:
                /// 
                var nemletezoKodok = kapcsolatiKodLista.Where(e => !resultKulsoAzonositok.Contains(e)).ToList();
                if (nemletezoKodok.Count > 0)
                {
                    throw new ResultException("Az alábbi kapcsolati kód nem szerepel a partnerállományban: " + nemletezoKodok.ToSqlInnerSearchString());
                }
            }
        }

        /// <summary>
        /// A megadott kapcsolati kódokhoz partner Id-k lekérése
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        private Dictionary<string, Guid> GetPartnerIdsForKapcsolatiKodok(List<string> kapcsolatiKodLista)
        {
            var kapcsolatiKodPartnerIdDict = new Dictionary<string, Guid>();

            if (kapcsolatiKodLista.Count > 0)
            {
                KRT_PartnerekSearch searchObj = new KRT_PartnerekSearch();
                searchObj.KulsoAzonositok.In(kapcsolatiKodLista);

                var resultPartnerek = eAdminService.ServiceFactory.GetKRT_PartnerekService().GetAll(this.ExecParamCaller, searchObj);
                resultPartnerek.CheckError();

                foreach (DataRow row in resultPartnerek.Ds.Tables[0].Rows)
                {
                    string kulsoAzon = row["KulsoAzonositok"].ToString();

                    kapcsolatiKodPartnerIdDict[kulsoAzon] = (Guid)row["Id"];
                }
            }

            return kapcsolatiKodPartnerIdDict;
        }

        private FoundPartner FindPartner(Cimzett cimzett)
        {
            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();

            // BLG_2502
            //BUG_2742
            //partnerekSearch.Nev.Value = cimzett.Nev;
            if (String.IsNullOrEmpty(cimzett.Nev))
            {
                partnerekSearch.Nev.Filter(cimzett.Szervezet);

            }
            else partnerekSearch.Nev.Filter(cimzett.Nev);

            KRT_CimekSearch cimekSearch = new KRT_CimekSearch();
            cimekSearch.IRSZ.FilterIfNotEmpty(cimzett.Iranyitoszam);
            cimekSearch.TelepulesNev.FilterIfNotEmpty(cimzett.Telepules);
            cimekSearch.KozteruletNev.FilterIfNotEmpty(cimzett.Cim);

            Result resultPartner = eAdminService.ServiceFactory.GetKRT_PartnerekService().FindByPartnerAndCim(this.ExecParamCaller, partnerekSearch, cimekSearch);
            resultPartner.CheckError();

            return new FoundPartner(resultPartner);
        }

        private FoundPartner FindMainPartnerSimple(Cimzett cimzett)
        {
            if (string.IsNullOrEmpty(cimzett.Szervezet) || string.IsNullOrEmpty(cimzett.UgyfelAzonosito))
                return null;

            FoundPartner foundPartner = new FoundPartner();

            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.Nev.Filter(cimzett.Szervezet);
            partnerekSearch.Tipus.Filter(KodTarak.Partner_Tipus.Szervezet);
            partnerekSearch.Forras.Filter(KodTarak.Partner_Forras.ADATKAPU);

            //string[] azonositok = new string[] { "," + cimzett.UgyfelAzonosito, cimzett.UgyfelAzonosito };
            //partnerekSearch.KulsoAzonositok.Value = "'" + String.Join("','", azonositok) + "'";
            //partnerekSearch.KulsoAzonositok.Operator = Query.Operators.inner;

            // BUG#7784:
            partnerekSearch.KulsoAzonositok.LikeFull(cimzett.UgyfelAzonosito);

            KRT_CimekSearch cimekSearch = new KRT_CimekSearch();

            Result resultPartner = eAdminService.ServiceFactory.GetKRT_PartnerekService().GetAll(ExecParamCaller, partnerekSearch);
            resultPartner.CheckError();

            if (resultPartner.GetCount > 0)
            {
                foundPartner.Partner_Id = resultPartner.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            return foundPartner;
        }
        private FoundPartner FindSubPartnerSimple(Cimzett cimzett)
        {
            if (string.IsNullOrEmpty(cimzett.Nev) || string.IsNullOrEmpty(cimzett.KapcsolattartoAzonosito))
                return null;

            FoundPartner foundPartner = new FoundPartner();

            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();
            partnerekSearch.Nev.Filter(cimzett.Nev);
            partnerekSearch.Forras.Filter(KodTarak.Partner_Forras.ADATKAPU);

            var azonositok = new string[] { cimzett.KapcsolattartoAzonosito + ",", cimzett.KapcsolattartoAzonosito };
            partnerekSearch.KulsoAzonositok.In(azonositok);

            KRT_CimekSearch cimekSearch = new KRT_CimekSearch();

            Result resultPartner = eAdminService.ServiceFactory.GetKRT_PartnerekService().GetAll(ExecParamCaller, partnerekSearch);
            resultPartner.CheckError();

            if (resultPartner.GetCount > 0)
            {
                foundPartner.Partner_Id = resultPartner.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            return foundPartner;
        }

        private FoundPartner FindMainPartner(Cimzett cimzett)
        {
            if (string.IsNullOrEmpty(cimzett.Szervezet))
                return null;

            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();

            partnerekSearch.Nev.Filter(cimzett.Szervezet);

            KRT_CimekSearch cimekSearch = new KRT_CimekSearch();
            cimekSearch.IRSZ.FilterIfNotEmpty(cimzett.Iranyitoszam);
            cimekSearch.TelepulesNev.FilterIfNotEmpty(cimzett.Telepules);
            cimekSearch.KozteruletNev.FilterIfNotEmpty(cimzett.Cim);

            Result resultPartner = eAdminService.ServiceFactory.GetKRT_PartnerekService().FindByPartnerAndCim(ExecParamCaller, partnerekSearch, cimekSearch);
            resultPartner.CheckError();

            return new FoundPartner(resultPartner);
        }
        private FoundPartner FindSubPartner(Cimzett cimzett)
        {
            if (string.IsNullOrEmpty(cimzett.Nev))
                return null;

            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();

            partnerekSearch.Nev.Filter(cimzett.Nev);

            KRT_CimekSearch cimekSearch = new KRT_CimekSearch();
            cimekSearch.IRSZ.FilterIfNotEmpty(cimzett.Iranyitoszam);
            cimekSearch.TelepulesNev.FilterIfNotEmpty(cimzett.Telepules);
            cimekSearch.KozteruletNev.FilterIfNotEmpty(cimzett.Cim);

            Result resultPartner = eAdminService.ServiceFactory.GetKRT_PartnerekService().FindByPartnerAndCim(ExecParamCaller, partnerekSearch, cimekSearch);
            resultPartner.CheckError();

            return new FoundPartner(resultPartner);
        }

        /// <summary>
        /// Partner megkeresése név alapján
        /// (Ha többet talál ilyen néven, nem dob hibát, hanem az elsőt kiválasztja.)
        /// </summary>
        /// <param name="partnerNev"></param>
        /// <returns></returns>
        private Guid? FindPartnerIdByNev(string partnerNev)
        {
            KRT_PartnerekSearch partnerekSearch = new KRT_PartnerekSearch();

            partnerekSearch.Nev.Filter(partnerNev);

            Result resultPartner = eAdminService.ServiceFactory.GetKRT_PartnerekService().GetAll(this.ExecParamCaller, partnerekSearch);
            resultPartner.CheckError();

            if (resultPartner.GetCount > 0)
            {
                return (Guid)resultPartner.Ds.Tables[0].Rows[0]["Id"];
            }
            else
                return null;
        }

        #endregion

        public static string GetXMLCimzettListaIktatasCimzett(Cimzett cimzett, string cim_tipus, string pld_helye, string expedialasModja)
        {

            ExcelClassCimzettListaIktatas tmp = null;
            //List<string> classColumns = Reflection.GetAttributeNames(typeof(ExcelClassCimzettListaIktatas));

            try
            {
                tmp = new ExcelClassCimzettListaIktatas();
                tmp.Id = String.Empty;
                tmp.Allapot = String.Empty;
                tmp.Cimzett_Ajto = String.Empty;
                tmp.Cimzett_Ajto_Betujele = String.Empty;
                tmp.Cimzett_Emelet = String.Empty;
                tmp.Cimzett_Hazszam1 = String.Empty;
                tmp.Cimzett_Hazszam2 = String.Empty;
                tmp.Cimzett_Hazszam_Betujele = String.Empty;
                tmp.Cimzett_IRSZ = cimzett.Iranyitoszam;
                tmp.Cimzett_Kozterulet_Neve = cimzett.Cim;
                tmp.Cimzett_Kozterulet_Tipusa = String.Empty;
                tmp.Cimzett_Lepcsohaz = String.Empty;
                tmp.Cimzett_neve = cimzett.Nev;
                tmp.Cimzett_Orszag = String.Empty;
                tmp.Cimzett_Telepules = cimzett.Telepules;
                tmp.Cim_Tipus = cim_tipus;
                tmp.Expedialas_Modja = expedialasModja;
                tmp.Hivatali_Kapus_Azonosito = cimzett.KapcsolattartoAzonosito; //???
                //tmp.Peldany_helye = pld_helye;

            }
            catch (Exception exc)
            {
                Logger.Error("GetExcelClassCimzettListaIktatasCimzett hiba:", exc);
                return null;
            }
            return tmp.GetAsXml();

        }
        public enum IdoEgyseg
        {
            Perc,
            Ora,
            Nap,
            Munkanap,
            Het,
            Honap,
            Ev,
            DEFAULT
        }
        /// <summary>
        /// CheckAndGetDataSetContainsItem
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="find"></param>
        /// <returns></returns>
        private static DataRow CheckAndGetDataSetContainsItem(DataRowCollection row, string column, string find)
        {
            for (int i = 0; i < row.Count; i++)
            {
                if (row[i][column].ToString().Equals(find))
                    return row[i];
            }
            return null;
        }

        private string GetAlairasTipus(string iratId, string alairofelhasznalo, string alairoSzerep, string keresettAlairasMod)
        {
            eRecord.Service.AlairasokService service = eRecordService.ServiceFactory.GetAlairasokService();

            ExecParam execParamAlairokGet = this.ExecParamCaller.Clone();
            string objektumTip = "Irat";
            string objektumId = iratId;
            string folyamatKod = "IratJovahagyas";
            string muveletKod = null;

            Result result = service.GetAll_PKI_IratAlairoSzabalyok(
                execParamAlairokGet, alairofelhasznalo, null, objektumTip, objektumId, folyamatKod, muveletKod, alairoSzerep);

            result.CheckError();
            for (int i = 0; i < result.GetCount; i++)
            {
                if (result.Ds.Tables[0].Rows[i]["AlairasMod"].ToString().Equals(keresettAlairasMod))
                    return result.Ds.Tables[0].Rows[i]["AlairasSzabaly_Id"].ToString();
            }
            return null;
        }

        /// <summary>
        /// Csoport DataRow lekérése a csoport id alapján
        /// </summary>
        /// <param name="csoportId"></param>
        /// <returns></returns>
        private DataRow GetCsoportRowById(string csoportId)
        {
            KRT_CsoportokSearch searchObj = new KRT_CsoportokSearch();
            searchObj.Id.Filter(csoportId);

            var result = eAdminService.ServiceFactory.GetKRT_CsoportokService().GetAll(ExecParamCaller, searchObj);
            result.CheckError();

            // Ellenőrzés: csak 1 találat lehet:
            if (result.GetCount == 0)
            {
                throw new ResultException("Nem található ilyen id-val csoport: " + csoportId);
            }
            else
            {
                return result.Ds.Tables[0].Rows[0];
            }
        }
        /// <summary>
        /// IsCimzettElektronikus
        /// </summary>
        /// <param name="cimzett"></param>
        /// <returns></returns>
        public static bool IsCimzettElektronikus(Cimzett cimzett)
        {
            if (cimzett == null || cimzett.Elektronikus == null)
                return false;

            return cimzett.Elektronikus.Equals("1", StringComparison.InvariantCultureIgnoreCase)
                    ||
                    cimzett.Elektronikus.Equals("i", StringComparison.InvariantCultureIgnoreCase)
                    ||
                    cimzett.Elektronikus.Equals("igen", StringComparison.InvariantCultureIgnoreCase);
        }
        /// <summary>
        /// Because the address is not segmented, add comma after zip and city.
        /// </summary>
        /// <param name="cim"></param>
        /// <returns></returns>
        public static string ExtendCimWithComma(string cim)
        {
            if (string.IsNullOrEmpty(cim))
                return cim;
            StringBuilder result = new StringBuilder();
            string[] splitted = cim.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            if (splitted.Length < 3)
                return cim;

            int zip;
            try
            {
                if (int.TryParse(splitted[0].ToString(), out zip))
                {
                    result.Append(zip);
                    result.Append(", ");
                    result.Append(splitted[1]);
                    result.Append(", ");
                    for (int i = 2; i < splitted.Length; i++)
                    {
                        result.Append(splitted[i]);
                        result.Append(" ");
                    }
                    return result.ToString().TrimEnd();
                }
                else if (splitted.Length >= 3 && int.TryParse(splitted[1].ToString(), out zip))
                {
                    result.Append(splitted[0]);
                    result.Append(" ");
                    result.Append(zip);
                    result.Append(", ");
                    result.Append(splitted[2]);
                    result.Append(", ");
                    for (int i = 3; i < splitted.Length; i++)
                    {
                        result.Append(splitted[i]);
                        result.Append(" ");
                    }
                    return result.ToString().TrimEnd();
                }
                return cim;
            }
            catch (Exception exc)
            {
                Logger.Error("ExtendCimWithComma error", exc);
                return cim;
            }
        }

        /// <summary>
        /// SetIratObjDefaultVales
        /// </summary>
        /// <param name="irat"></param>
        private static void SetIratObjDefaultVales(ref Irat irat)
        {
            irat.Eloszam = string.Empty;
            irat.Foszam = string.Empty;
            irat.Konyv = string.Empty;
            irat.Evszam = string.Empty;
            irat.Alszam = string.Empty;
            irat.IratTipus = string.Empty;
            irat.Szignalo = string.Empty;
            irat.Szignalas = string.Empty;
            irat.Bekuldo = string.Empty;
            irat.BekuldoCim = string.Empty;
            irat.Erkezett = string.Empty;
            irat.IratTargya = string.Empty;
            irat.Hatarido = string.Empty;
        }
        /// <summary>
        /// SetIratObjDefaultVales
        /// </summary>
        /// <param name="irat"></param>
        private static void SetIratObjDefaultVales(ref eIrat irat)
        {
            irat.Eloszam = string.Empty;
            irat.Foszam = string.Empty;
            irat.Konyv = string.Empty;
            irat.Evszam = string.Empty;
            irat.Alszam = string.Empty;
            irat.IratTipus = string.Empty;
            irat.Szignalo = string.Empty;
            irat.Szignalas = string.Empty;
            irat.Bekuldo = string.Empty;
            irat.BekuldoCim = string.Empty;
            irat.Erkezett = string.Empty;
            irat.IratTargya = string.Empty;
            irat.Hatarido = string.Empty;
        }

        /// <summary>
        /// SetCimzettExpedialasModFromElektronikusField
        /// </summary>
        /// <param name="cimzettek"></param>
        public static void SetNMHHCimzettExpedialasModFromElektronikusField(ref Cimzett[] cimzettek)
        {
            if (cimzettek == null || cimzettek.Length < 1)
            {
                return;
            }
            foreach (Cimzett item in cimzettek)
            {
                #region Ha meg akarjuk tartani cExpedialasMod erteket !
                //if (!string.IsNullOrEmpty(item.cExpedialasMod))
                //{
                //    continue;
                //}
                #endregion Ha meg akarjuk tartani cExpedialasMod erteket !

                if (IsCimzettElektronikus(item))
                {
                    item.cExpedialasMod = Kodtar_NMHH_EXPMOD.ELEKTRONIKUS_UTON_HKP;
                }
                else
                {
                    item.cExpedialasMod = Kodtar_NMHH_EXPMOD.POSTAI_UTON;
                }
            }
        }
        public static class Helper
        {
            public static string GetFormattedDateAndTimeString(object data)
            {
                return FormatDate(data, SZURWSDateTimeFormatString);
            }

            public static string GetFormattedDateOnlyString(object data)
            {
                return FormatDate(data, SZURWSDateFormatString);
            }

            private static string FormatDate(object data, string format)
            {
                if (data == null || data.ToString() == string.Empty) { return string.Empty; }
                DateTime dateTime = DateTime.Parse(data.ToString());
                string result = dateTime.ToString(format);
                return result;
            }
        }
    }
}