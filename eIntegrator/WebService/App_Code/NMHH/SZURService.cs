﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;

namespace Contentum.eIntegrator.WebService.NMHH
{
    // Ez azért kell, mert a WSDL-ben minden metódusnál ugyanaz a SoapAction van beállítva, így nem lehet a SoapAction alapján meghatározni a metódust.
    [SoapDocumentService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    [WebService(Namespace = "Contentum.eIntegrator.WebService.NMHH")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public partial class SZURService : System.Web.Services.WebService, ISZURService
    {
        #region Properties

        /// <summary>
        /// A webservice hívás során hitelesített felhasználó azonosítója
        /// </summary>
        public Utility.LoginData LoginData { get; private set; }

        private ExecParam _execParamCaller;
        public ExecParam ExecParamCaller
        {
            get
            {
                if (this._execParamCaller == null)
                {
                    this._execParamCaller = CreateExecParam();
                }

                return this._execParamCaller;
            }
        }

        /// <summary>
        /// to return errors from SZURManager
        /// </summary>
        public string ErrorMessage;

        #endregion

        #region Segéd

        private ExecParam CreateExecParam()
        {
            ExecParam execParam = new ExecParam();
            if (this.LoginData != null)
            {
                execParam.LoginUser_Id = this.LoginData.FelhasznaloId.ToString();
                execParam.Felhasznalo_Id = this.LoginData.FelhasznaloId.ToString();
                execParam.FelhasznaloSzervezet_Id = this.LoginData.SzervezetId.ToString();
                execParam.CsoportTag_Id = this.LoginData.CsoportTag_Id.ToString();
            }

            return execParam;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Konstruktor, ide tesszük az authentikációt
        /// </summary>
        public SZURService()
        {
            // Authentication --> Request headerben jön a felhasználónév/jelszó pár            

            try
            {
                this.LoginData = Utility.Security.AuthenticateRequest(this.Context.Request);

                // ExecParam-ot elmentjük a HttpContext.Current.Items adatokhoz (az aktuális kérésben él csak), hogy máshonnan is elérhessük:
                HttpContext.Current.Items["CurrentExecParam"] = this.ExecParamCaller;
            }
            catch (HttpException httpExc)
            {
                Context.Response.StatusCode = httpExc.GetHttpCode();
                if (Context.Response.StatusCode == 403 && !String.IsNullOrEmpty(httpExc.Message))
                {
                    Context.Response.StatusDescription = httpExc.Message;
                }
                Context.Response.End();
            }
            catch (InvalidUserException iux)
            {
                ErrorMessage = iux.Message;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.Message, exc);
                throw;
            }
        }

        #endregion

        /// <summary>
        /// Ügyintéző átveendő iratainak listája
        /// "Egy ügyintézőre szignált, még át nem vett ügyiratborítók és ügyiratok listáját adja vissza."
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="Rendszerkod"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public Irat[] SZURGetAtvIratWS(string UserId, string Rendszerkod, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetAtvIratWS");
            Irat[] result; 

            try
            {
                #region Kötelező mezők
                Utility.CheckUserID(UserId);
                #endregion

                result = new SZURManager(this).SZURGetAtvIratWS(UserId, Rendszerkod);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
                result = new Irat[] { };
            }

            Logger.InfoEnd("SZURService.SZURGetAtvIratWS");
            return result;
        }

        /// <summary>
        /// Irat átvétele ügyintézésre
        /// "A megadott iktatószámú ügyirat ügyintézői átvételi időpontját rögzíti a SZÜR-ben."
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="SakkoraStatus"></param>
        /// <param name="OkKod"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURSetIratAtveteleWS(string Evszam, string Konyv, string Foszam, string Alszam, string SakkoraStatus, string OkKod, string UserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURSetIratAtveteleWS");

            try
            {
                #region SAKKORA KOD ATFORGAT
                if (SakkoraStatus.Equals("0"))
                    SakkoraStatus = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
                #endregion

                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckSakkoraStatusz(SakkoraStatus, this.ExecParamCaller);
                // BUG_2742
                //Utility.CheckOkKod(OkKod);
                Utility.CheckOkKod(this.ExecParamCaller, SakkoraStatus, OkKod, "OkKod");
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this).SZURSetIratAtveteleWS(Evszam, Konyv, Foszam, Alszam, SakkoraStatus, OkKod, UserId);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURSetIratAtveteleWS");
        }

        /// <summary>
        /// Kimenő irat iktatása (elavult hívás)
        /// "Új kimenő alszám felvétele egy már létező, megadott főszámhoz."
        /// </summary>
        /// <param name="Ugy"></param>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="IratTipus"></param>
        /// <param name="IratTargy"></param>
        /// <param name="HelybenMarad"></param>
        /// <param name="Cimzettek"></param>
        /// <param name="UserId"></param>
        /// <param name="Alszam"></param>
        /// <param name="Eloszam"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetalszamWS(string Ugy, out string Alszam, ref string Evszam, ref string Konyv, ref string Foszam, string IratTipus, string IratTargy, string HelybenMarad, Cimzett[] Cimzettek, string UserId
            ,  out string Eloszam, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetalszamWS");

            try
            {
                SZURManager.SetNMHHCimzettExpedialasModFromElektronikusField(ref Cimzettek);

                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckFoszam(Foszam);
                Utility.CheckField("IratTipus", IratTipus);
                Utility.CheckField("IratTargy", IratTargy);
                Utility.CheckUserID(UserId);
                Utility.CheckCimzettek(Cimzettek, this.ExecParamCaller);
                #endregion

                string cimzettekXmlCsatolasHiba;
                // BUG_2742
                // GetAlszam és eGetAlszam összevonás
                //new SZURManager(this).SZURGetalszamWS(Ugy, Evszam, Konyv, Foszam, IratTipus, IratTargy, HelybenMarad, Cimzettek, UserId
                //    , out Evszam, out Konyv, out Foszam, out Alszam, out Eloszam, out cimzettekXmlCsatolasHiba);
                new SZURManager(this).SZUReGetalszamWS(false, Ugy, Evszam, Konyv, Foszam, IratTipus, IratTargy, HelybenMarad, "", "", Cimzettek, UserId
                    , out Alszam, out Evszam, out Konyv, out Foszam,  out Eloszam, out cimzettekXmlCsatolasHiba);

                // Ha a cimzettekXmlCsatolasHiba ki van töltve, akkor hiba volt a Címzettek csatolmányként feltöltése során, de az iktatás attól még végrehajtódott
                if (!String.IsNullOrEmpty(cimzettekXmlCsatolasHiba))
                {
                    Hibakod = Utility.ErrorCodes.GeneralErrorCode;
                    Hibaszoveg = "Az iktatás sikeresen végrehajtódott, de hiba történt a címzettek Elszámolóíven történő rögzítése során: " + cimzettekXmlCsatolasHiba;
                }
                else
                {
                    Hibakod = Utility.ErrorCodes.SuccessCode;
                    Hibaszoveg = String.Empty;
                }
                //át kell venni az iratot intézkedésre
                new SZURManager(this).SZURSetIratAtveteleWS(Evszam, Konyv, Foszam, Alszam, "", "", UserId);
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                Evszam = null;
                Konyv = null;
                Foszam = null;
                Alszam = null;
                Eloszam = null;
            }

            Logger.InfoEnd("SZURService.SZURGetalszamWS");
        }
        /// <summary>
        ///  szakrendszerből irat készre jelentése (elavult hívás)
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UgyintezoiNaplo"></param>
        /// <param name="ExpedialasMod"></param>
        /// <param name="KuldemenyTipus"></param>
        /// <param name="Rendeltetes"></param>
        /// <param name="T1_1"></param>
        /// <param name="T2_1"></param>
        /// <param name="T3_1"></param>
        /// <param name="Intezkedes"></param>
        /// <param name="CsatoltFajl"></param>
        /// <param name="SakkoraStatus"></param>
        /// <param name="SakkoraStatusOk"></param>
        /// <param name="Cimzettek"></param>
        /// <param name="UserId"></param>
        /// <param name="KiadmanyozoUserId"></param>
        /// <param name="Jogerositendo"></param>
        /// <param name="UzemzavarCsokkentes"></param>
        /// <param name="UzemzavarIratszam"></param>
        /// <param name="VisszafizOsszeg"></param>
        /// <param name="VisszafizDij"></param>
        /// <param name="VisszafizHatarozat"></param>
        /// <param name="VisszafizCimzett"></param>
        /// <param name="VisszafizHatarido"></param>
        /// <param name="MunkaNapszam"></param>
        /// <param name="HosszabitasNapszam"></param>
        /// <param name="TullepesNapszam"></param>
        /// <param name="TullepesMertek"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZURKiadmFelWS(string Evszam, string Konyv, string Foszam, string Alszam, string UgyintezoiNaplo, string ExpedialasMod, string KuldemenyTipus, string Rendeltetes, string T1_1, string T2_1, string T3_1, string Intezkedes, string CsatoltFajl, string SakkoraStatus, string SakkoraStatusOk, Cimzett[] Cimzettek, string UserId, string KiadmanyozoUserId, string Jogerositendo, string UzemzavarCsokkentes, string UzemzavarIratszam, string VisszafizOsszeg, string VisszafizDij, string VisszafizHatarozat, string VisszafizCimzett, string VisszafizHatarido
            , out string ElteltNapszam, out string MunkaNapszam, out string HosszabitasNapszam, out string TullepesNapszam, out string TullepesMertek, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURKiadmFelWS");
            try
            {
                #region SAKKORA KOD ATFORGAT
                if (SakkoraStatus.Equals("0"))
                    SakkoraStatus = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
                #endregion

                SZURManager.SetNMHHCimzettExpedialasModFromElektronikusField(ref Cimzettek);

                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckField("UgyintezoiNaplo", UgyintezoiNaplo);
                //Utility.CheckField("KuldemenyTipus", KuldemenyTipus);
                Utility.CheckField("Rendeltetes", Rendeltetes);
                Utility.CheckField("T1_1", T1_1);
                Utility.CheckSakkoraStatusz(SakkoraStatus, this.ExecParamCaller);
                Utility.CheckUserID(UserId);                

                //irat lekérdezése, mert bizonyos paraméterek csak kimenő iratok esetén relevánsak (kötelezők)
                DataRow iratRow;
                iratRow = new SZURManager(this).GetIratRow(Evszam, Konyv, Foszam, Alszam);

                if (iratRow["PostazasIranya"].ToString() == "0")
                {
                    // Ha esetleg nem kódot adtak át, hanem megjelenítendő nevet, akkor kódra átkonvertáljuk:
                    SakkoraStatusOk = SZURManager.ConvertSakkoraOkToKodtarKod(SakkoraStatusOk, this.ExecParamCaller);
                    // BUG_2742
                    //Utility.CheckOkKod(SakkoraStatusOk);
                    Utility.CheckOkKod(this.ExecParamCaller, SakkoraStatus, SakkoraStatusOk, "SakkoraStatusOk");
                    Utility.CheckField("KiadmanyozoUserId", KiadmanyozoUserId);
                    /* --keresre kiveve-- 11874
                    Utility.CheckCimzettek(Cimzettek, this.ExecParamCaller);                    
                    Utility.CheckField("UzemzavarCsokkentes", UzemzavarCsokkentes);
                    Utility.CheckField("UzemzavarIratszam", UzemzavarIratszam);
                    Utility.CheckField("VisszafizOsszeg", VisszafizOsszeg);
                    Utility.CheckField("VisszafizDij", VisszafizDij);            
                    Utility.CheckField("VisszafizHatarozat", VisszafizHatarozat);
                    Utility.CheckField("VisszafizCimzett", VisszafizCimzett);                    
                    Utility.CheckDateField("VisszafizHatarido", VisszafizHatarido);
                    */
                    Utility.CheckField("Jogerositendo", Jogerositendo);
                }
                #endregion

                new SZURManager(this).SZURKiadmFelWS(false, Evszam, Konyv, Foszam, Alszam, UgyintezoiNaplo, ExpedialasMod, KuldemenyTipus, Rendeltetes, T1_1, T2_1, T3_1, Intezkedes, CsatoltFajl, SakkoraStatus, SakkoraStatusOk, Cimzettek, UserId, KiadmanyozoUserId, Jogerositendo, UzemzavarCsokkentes, UzemzavarIratszam, VisszafizOsszeg, VisszafizDij, VisszafizHatarozat, VisszafizCimzett, VisszafizHatarido,
                    null, null, null, null,
                    out ElteltNapszam, out MunkaNapszam, out HosszabitasNapszam, out TullepesNapszam, out TullepesMertek);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.Message, exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                ElteltNapszam = null;
                MunkaNapszam = null;
                HosszabitasNapszam = null;
                TullepesNapszam = null;
                TullepesMertek = null;
            }

            Logger.InfoEnd("SZURService.SZURKiadmFelWS");
        }

        /// <summary>
        /// Kimenő irat iktatása (elavult hívás)
        /// "Új kimenő alszám felvétele egy már létező, megadott főszámhoz."
        /// </summary>
        /// <param name="Ugy"></param>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="IratTipus"></param>
        /// <param name="IratTargy"></param>
        /// <param name="HelybenMarad"></param>
        /// <param name="UKSpecial"></param>
        /// <param name="CimzettMarad"></param>
        /// <param name="Cimzettek"></param>
        /// <param name="UserId"></param>
        /// <param name="Alszam"></param>
        /// <param name="Eloszam"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZUReGetalszamWS(string Ugy,out string Alszam, ref string Evszam, ref string Konyv, ref string Foszam, string IratTipus, string IratTargy, string HelybenMarad, string UKSpecial, string CimzettMarad, Cimzett[] Cimzettek, string UserId,  out string Eloszam, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZUReGetalszamWS");

            try
            {
                SZURManager.SetNMHHCimzettExpedialasModFromElektronikusField(ref Cimzettek);

                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckFoszam(Foszam);
                Utility.CheckField("IratTipus", IratTipus);
                Utility.CheckField("IratTargy", IratTargy);
                Utility.CheckCimzettek(Cimzettek, this.ExecParamCaller);
                Utility.CheckUserID(UserId);
                #endregion

                string cimzettekXmlCsatolasHiba;

                // BUG_2742
                // GetAlszam és eGetAlszam összekapcsolás
                //new SZURManager(this).SZUReGetalszamWS(Ugy, Evszam, Konyv, Foszam, IratTipus, IratTargy, HelybenMarad, UKSpecial, CimzettMarad, Cimzettek, UserId
                //    , out Evszam, out Konyv, out Foszam, out Alszam, out Eloszam, out cimzettekXmlCsatolasHiba);
                new SZURManager(this).SZUReGetalszamWS(true, Ugy, Evszam, Konyv, Foszam, IratTipus, IratTargy, HelybenMarad, UKSpecial, CimzettMarad, Cimzettek, UserId
                    , out Alszam, out Evszam, out Konyv, out Foszam,  out Eloszam, out cimzettekXmlCsatolasHiba);

                // Ha a cimzettekXmlCsatolasHiba ki van töltve, akkor hiba volt a Címzettek csatolmányként feltöltése során, de az iktatás attól még végrehajtódott
                if (!String.IsNullOrEmpty(cimzettekXmlCsatolasHiba))
                {
                    Hibakod = Utility.ErrorCodes.GeneralErrorCode;
                    Hibaszoveg = "Az iktatás sikeresen végrehajtódott, de hiba történt a címzettek struktúra XML csatolmányként történő feltöltése során: " + cimzettekXmlCsatolasHiba;
                }
                else
                {
                    Hibakod = Utility.ErrorCodes.SuccessCode;
                    Hibaszoveg = String.Empty;
                }
                //át kell venni az iratot intézkedésre
                new SZURManager(this).SZURSetIratAtveteleWS(Evszam, Konyv, Foszam, Alszam, "", "", UserId);
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                Evszam = null;
                Konyv = null;
                Foszam = null;
                Alszam = null;
                Eloszam = null;
            }

            Logger.InfoEnd("SZURService.SZUReGetalszamWS");
        }
        /// <summary>
        ///  irat készre jelentése (elektronikus változat)
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UgyintezoiNaplo"></param>
        /// <param name="ExpedialasMod"></param>
        /// <param name="KuldemenyTipus"></param>
        /// <param name="Rendeltetes"></param>
        /// <param name="T1_1"></param>
        /// <param name="T2_1"></param>
        /// <param name="T3_1"></param>
        /// <param name="Intezkedes"></param>
        /// <param name="CsatoltFajl"></param>
        /// <param name="SakkoraStatus"></param>
        /// <param name="SakkoraStatusOk"></param>
        /// <param name="Cimzettek"></param>
        /// <param name="UserId"></param>
        /// <param name="KiadmanyozoUserId"></param>
        /// <param name="Jogerositendo"></param>
        /// <param name="UzemzavarCsokkentes"></param>
        /// <param name="UzemzavarIratszam"></param>
        /// <param name="VisszafizOsszeg"></param>
        /// <param name="VisszafizDij"></param>
        /// <param name="VisszafizHatarozat"></param>
        /// <param name="VisszafizCimzett"></param>
        /// <param name="VisszafizHatarido"></param>
        /// <param name="UKSpecial"></param>
        /// <param name="CimzettMarad"></param>
        /// <param name="ElhiszParameters"></param>
        /// <param name="TisztviseloiAlairasParameters"></param>
        /// <param name="MunkaNapszam"></param>
        /// <param name="HosszabitasNapszam"></param>
        /// <param name="TullepesNapszam"></param>
        /// <param name="TullepesMertek"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZUReKiadmFelWS(string Evszam, string Konyv, string Foszam, string Alszam, string UgyintezoiNaplo, string ExpedialasMod, string KuldemenyTipus, string Rendeltetes, string T1_1, string T2_1, string T3_1, string Intezkedes, string CsatoltFajl, string SakkoraStatus, string SakkoraStatusOk, Cimzett[] Cimzettek, string UserId, string KiadmanyozoUserId, string Jogerositendo, string UzemzavarCsokkentes, string UzemzavarIratszam, string VisszafizOsszeg, string VisszafizDij, string VisszafizHatarozat, string VisszafizCimzett, string VisszafizHatarido, string UKSpecial, string CimzettMarad, string ElhiszParameters, TisztviseloiAlairasParameters TisztviseloiAlairasParameters
            , out string ElteltNapszam, out string MunkaNapszam, out string HosszabitasNapszam, out string TullepesNapszam, out string TullepesMertek, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZUReKiadmFelWS");
            try
            {
                #region SAKKORA KOD ATFORGAT
                if (SakkoraStatus.Equals("0"))
                    SakkoraStatus = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
                #endregion

                SZURManager.SetNMHHCimzettExpedialasModFromElektronikusField(ref Cimzettek);

                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);                
                Utility.CheckField("UgyintezoiNaplo", UgyintezoiNaplo);                
                Utility.CheckField("Rendeltetes", Rendeltetes);
                Utility.CheckField("T1_1", T1_1);
                Utility.CheckSakkoraStatusz(SakkoraStatus, this.ExecParamCaller);                
                Utility.CheckUserID(UserId);

                //irat lekérdezése, mert bizonyos paraméterek csak kimenő iratok esetén relevánsak (kötelezők)
                DataRow iratRow;
                iratRow = new SZURManager(this).GetIratRow(Evszam, Konyv, Foszam, Alszam);

                if (iratRow["PostazasIranya"].ToString() == "0")
                {
                    //Utility.CheckField("KuldemenyTipus", KuldemenyTipus);
                    
                    // Ha esetleg nem kódot adtak át, hanem megjelenítendő nevet, akkor kódra átkonvertáljuk:
                    SakkoraStatusOk = SZURManager.ConvertSakkoraOkToKodtarKod(SakkoraStatusOk, this.ExecParamCaller);
                    // BUG_2742
                    //Utility.CheckOkKod(SakkoraStatusOk);
                    Utility.CheckOkKod(this.ExecParamCaller, SakkoraStatus, SakkoraStatusOk, "SakkoraStatusOk");                    
                    Utility.CheckField("KiadmanyozoUserId", KiadmanyozoUserId);
                    /* kérésre kivéve 11874 --keresre kiveve--
                    Utility.CheckCimzettek(Cimzettek, this.ExecParamCaller);
                    Utility.CheckField("UzemzavarCsokkentes", UzemzavarCsokkentes);
                    Utility.CheckField("UzemzavarIratszam", UzemzavarIratszam);
                    Utility.CheckField("VisszafizOsszeg", VisszafizOsszeg);
                    Utility.CheckField("VisszafizDij", VisszafizDij);
                    Utility.CheckField("VisszafizHatarozat", VisszafizHatarozat);
                    //Utility.CheckField("VisszafizCimzett", VisszafizCimzett);
                    Utility.CheckDateField("VisszafizHatarido", VisszafizHatarido);
                    */                
                    Utility.CheckField("Jogerositendo", Jogerositendo);
                }

                #endregion
                new SZURManager(this).SZURKiadmFelWS(true, Evszam, Konyv, Foszam, Alszam, UgyintezoiNaplo, ExpedialasMod, KuldemenyTipus, Rendeltetes, T1_1, T2_1, T3_1, Intezkedes, CsatoltFajl, SakkoraStatus, SakkoraStatusOk, Cimzettek, UserId, KiadmanyozoUserId, Jogerositendo, UzemzavarCsokkentes, UzemzavarIratszam, VisszafizOsszeg, VisszafizDij, VisszafizHatarozat, VisszafizCimzett, VisszafizHatarido, UKSpecial, CimzettMarad, ElhiszParameters, TisztviseloiAlairasParameters,
                    out ElteltNapszam, out MunkaNapszam, out HosszabitasNapszam, out TullepesNapszam, out TullepesMertek);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = string.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                ElteltNapszam = null;
                MunkaNapszam = null;
                HosszabitasNapszam = null;
                TullepesNapszam = null;
                TullepesMertek = null;
            }

            Logger.InfoEnd("SZURService.SZUReKiadmFelWS");
        }

        /// <summary>
        /// Az ügy vagy irat kiadmányozási állapotának ellenőrzése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Jovahagyva"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZURChkKiadmWS(string Evszam, string Konyv, string Foszam, string Alszam, out string Jovahagyo, out string Jovahagyva, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURChkKiadmWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                #endregion

                new SZURManager(this).SZURChkKiadmWS(Evszam, Konyv, Foszam, Alszam, out Jovahagyo, out Jovahagyva);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                // out paramétereknek értéket kell adni:
                Jovahagyo = null;
                Jovahagyva = null;
            }

            Logger.InfoEnd("SZURService.SZURChkKiadmWS");
        }

        /// <summary>
        /// Egy ügy főszám elemeinek lekérdezése ügyszám vagy könyv, évszám, főszám alapján. 
        /// </summary>
        /// <param name="Ugy"></param>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="UserID"></param>
        /// <param name="AktualisEvszam"></param>
        /// <param name="AktualisKonyv"></param>
        /// <param name="AktualisFoszam"></param>
        /// <param name="FelelosUserID"></param>
        /// <param name="Foszamok"></param>
        /// <param name="KapcsoltUgyek"></param>
        /// <param name="Ugyiratjelleg"></param>
        /// <param name="Ugyjellegkod"></param>
        /// <param name="UI_Hatarido"></param>
        /// <param name="UI_HiMod"></param>
        /// <param name="UI_Munkanap"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZURGetUgyAdatokWS(
                   out string AktualisEvszam,
                    ref string Ugy,
                    out string AktualisKonyv,
                    out string AktualisFoszam,
                    out string FelelosUserID,                   
                    out Foszam2[] Foszamok,                   
                    out KapcsoltUgy[] KapcsoltUgyek,
                    out string Ugyiratjelleg,
                    out string Ugyjellegkod,
                    out string UI_Hatarido,
                    out string UI_HiMod,
                    out string UI_Munkanap,
                    out string Hibakod,
                    out string Hibaszoveg,
                    string Evszam,
                    string Konyv,
                    string Foszam,
                    string UserID)
        {
            Logger.InfoStart("SZURService.SZURGetUgyAdatokWS");
            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckFoszam(Foszam);
                Utility.CheckUserID(UserID);
                #endregion

                #region TASK 5508
                if (string.IsNullOrEmpty(Konyv))
                    Konyv = "K";
                #endregion

                new SZURManager(this).SZURGetUgyAdatokWS(Evszam, Konyv, Foszam, UserID                    
                    , out AktualisEvszam, ref Ugy, out AktualisKonyv, out AktualisFoszam, out FelelosUserID
                    , out Foszamok, out KapcsoltUgyek, out Ugyiratjelleg, out Ugyjellegkod
                    , out UI_Hatarido, out UI_HiMod, out UI_Munkanap);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                // out paramétereknek értéket kell adni:
                AktualisEvszam = null;
                AktualisKonyv = null;
                AktualisFoszam = null;
                FelelosUserID = null;
                Foszamok = null;
                KapcsoltUgyek = null;
                Ugyiratjelleg = null;
                Ugyjellegkod = null;
                UI_Hatarido = null;
                UI_HiMod = null;
                UI_Munkanap = null;
            }

            Logger.InfoEnd("SZURService.SZURGetUgyAdatokWS");
        }

        /// <summary>
        /// Alszámos irat adatainak lekérdezése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UserID"></param>
        /// <param name="Ugy"></param>
        /// <param name="IratTargy"></param>
        /// <param name="IratTipus"></param>
        /// <param name="IratJelleg"></param>
        /// <param name="Szignalo"></param>
        /// <param name="Szignalas"></param>
        /// <param name="Atvevo"></param>
        /// <param name="Munkatars"></param>
        /// <param name="Atveve"></param>
        /// <param name="Bekuldo"></param>
        /// <param name="BekuldoIrsz"></param>
        /// <param name="BekuldoTelepules"></param>
        /// <param name="BekuldoCim"></param>
        /// <param name="Erkezett"></param>
        /// <param name="Hatarido"></param>
        /// <param name="UgyintezoiNaplo"></param>
        /// <param name="ExpedialasMod"></param>
        /// <param name="KuldemenyTipus"></param>
        /// <param name="Rendeltetes"></param>
        /// <param name="Intezkedes"></param>
        /// <param name="Keszrejelentette"></param>
        /// <param name="Keszrejelentve"></param>
        /// <param name="Jovahagyo"></param>
        /// <param name="Jovahagyva"></param>
        /// <param name="KikuldottTerti"></param>
        /// <param name="ErkezettTerti"></param>
        /// <param name="AtnemvettTerti"></param>
        /// <param name="UtolsoTerti"></param>
        /// <param name="Cimzettek"></param>
        /// <param name="CimzettekFile"></param>
        /// <param name="SakkoraStatusz"></param>
        /// <param name="Expedialva"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetIratAdatokWS(
                    string Evszam,
                    string Konyv,
                    string Foszam,
                    string Alszam,
                    string UserID,
                    out string Ugy,
                    out string IratTargy,
                    out string IratTipus,
                    out string IratJelleg,
                    out string Szignalo,
                    out string Szignalas,
                    out string Atvevo,
                    out string Munkatars,
                    out string Atveve,
                    out string Bekuldo,
                    out string BekuldoIrsz,
                    out string BekuldoTelepules,
                    out string BekuldoCim,
                    out string Erkezett,
                    out string Hatarido,
                    out string UgyintezoiNaplo,
                    out string ExpedialasMod,
                    out string KuldemenyTipus,
                    out string Rendeltetes,
                    out string Intezkedes,
                    out string Keszrejelentette,
                    out string Keszrejelentve,
                    out string Jovahagyo,
                    out string Jovahagyva,
                    out string KikuldottTerti,
                    out string ErkezettTerti,
                    out string AtnemvettTerti,
                    out string UtolsoTerti,
                    out Cimzett[] Cimzettek,
                    out string CimzettekFile,
                    out string SakkoraStatusz,
                    out string Expedialva,
                    out string Hibakod,
                    out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetIratAdatokWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckUserID(UserID);
                #endregion

                new SZURManager(this).SZURGetIratAdatokWS(Evszam, Konyv, Foszam, Alszam, UserID
                            , out Ugy, out IratTargy, out IratTipus, out IratJelleg, out Szignalo, out Szignalas
                            , out Atvevo, out Munkatars, out Atveve, out Bekuldo, out BekuldoIrsz
                            , out BekuldoTelepules, out BekuldoCim, out Erkezett, out Hatarido, out UgyintezoiNaplo
                            , out ExpedialasMod, out KuldemenyTipus, out Rendeltetes, out Intezkedes
                            , out Keszrejelentette, out Keszrejelentve, out Jovahagyo, out Jovahagyva, out KikuldottTerti
                            , out ErkezettTerti, out AtnemvettTerti, out UtolsoTerti, out Cimzettek
                            , out CimzettekFile, out SakkoraStatusz, out Expedialva);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                // out paramétereknek értéket kell adni:
                Ugy = null; IratTargy = null; IratTipus = null; IratJelleg = null;
                Szignalo = null; Szignalas = null; Atvevo = null; Munkatars = null;
                Atveve = null; Bekuldo = null; BekuldoIrsz = null; BekuldoTelepules = null; BekuldoCim = null;
                Erkezett = null; Hatarido = null; UgyintezoiNaplo = null; ExpedialasMod = null; KuldemenyTipus = null;
                Rendeltetes = null; Intezkedes = null; Keszrejelentette = null; Keszrejelentve = null;
                Jovahagyo = null; Jovahagyva = null; KikuldottTerti = null; ErkezettTerti = null; AtnemvettTerti = null; UtolsoTerti = null;
                Cimzettek = null; CimzettekFile = null; SakkoraStatusz = null; Expedialva = null;
            }

            Logger.InfoEnd("SZURService.SZURGetIratAdatokWS");
        }

        /// <summary>
        /// Irat sakkóra státusz lekérdezése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="SakkoraStatusz"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetSakkoraStatusWS(string Evszam, string Konyv, string Foszam, string Alszam, out string SakkoraStatusz, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetSakkoraStatusWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                #endregion

                new SZURManager(this).SZURGetSakkoraStatusWS(Evszam, Konyv, Foszam, Alszam, out SakkoraStatusz);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                SakkoraStatusz = null;
            }

            Logger.InfoEnd("SZURService.SZURGetSakkoraStatusWS");
        }
        /// <summary>
        /// Irat sakkóra státusz beállítása
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="SakkoraStatus"></param>
        /// <param name="OkKod"></param>
        /// <param name="UserID"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURSetSakkoraStatusWS(string Evszam, string Konyv, string Foszam, string Alszam, string SakkoraStatus, string OkKod, string UserID, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURSetSakkoraStatusWS");
            try
            {
                #region SAKKORA KOD ATFORGAT
                if (SakkoraStatus.Equals("0"))
                    SakkoraStatus = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
                #endregion

                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckSakkoraStatusz(SakkoraStatus, this.ExecParamCaller);
                // BUG_2742
                //Utility.CheckOkKod(OkKod);
                Utility.CheckOkKod(this.ExecParamCaller, SakkoraStatus, OkKod, "OkKod");
                //Ha nem kódot adtak át, hanem megjelenítendő nevet, akkor kódra átkonvertáljuk:
                OkKod = SZURManager.ConvertSakkoraOkToKodtarKod(OkKod, this.ExecParamCaller);
                Utility.CheckUserID(UserID);

                if (Alszam.Equals("0"))
                    throw new Exception("Nem támogatott funkció 0-s alszámra !");
                #endregion
                SakkoraStatus = Sakkora.ChangeSakkoraStatus(SakkoraStatus);

                new SZURManager(this).SZURSetSakkoraStatusWS(Evszam, Konyv, Foszam, Alszam, SakkoraStatus, OkKod, UserID, out Hibakod, out Hibaszoveg);
                if (string.IsNullOrEmpty(Hibakod))
                {
                    Hibakod = Utility.ErrorCodes.SuccessCode;
                    Hibaszoveg = String.Empty;
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }
            Logger.InfoEnd("SZURService.SZURSetSakkoraStatusWS");
        }

        /// <summary>
        /// A partnernyilvántartásban új ügyfél létrehozása
        /// </summary>
        /// <param name="HirUgyfelAzonosito"></param>
        /// <param name="Szervezet"></param>
        /// <param name="Iranyitoszam"></param>
        /// <param name="Telepules"></param>
        /// <param name="Cim"></param>
        /// <param name="Telefon"></param>
        /// <param name="Mobil"></param>
        /// <param name="Email"></param>
        /// <param name="Fax"></param>
        /// <param name="Ok"></param>
        /// <param name="KapcsolatTartok"></param>
        /// <param name="UserID"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURCreateUgyfelWS(string HirUgyfelAzonosito, string Szervezet, string Iranyitoszam, string Telepules, string Cim, string Telefon, string Mobil, string Email, string Fax, string Ok, KapcsolatTarto[] KapcsolatTartok, string UserID
            , out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURCreateUgyfelWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckField("HirUgyfelAzonosito", HirUgyfelAzonosito);
                Utility.CheckField("Szervezet", Szervezet);
                Utility.CheckField("Iranyitoszam", Iranyitoszam);
                Utility.CheckField("Telepules", Telepules);
                Utility.CheckField("Cim", Cim);
                Utility.CheckField("Ok", Ok);
                Utility.CheckUserID(UserID);
                #endregion

                new SZURManager(this).SZURCreateUgyfelWS(HirUgyfelAzonosito, Szervezet, Iranyitoszam, Telepules, Cim
                    , Telefon, Mobil, Email, Fax, Ok, KapcsolatTartok, UserID);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURCreateUgyfelWS");
        }

        /// <summary>
        /// Ügyfél módosítása a SZÜR Partnertörzsében
        /// </summary>
        /// <param name="HirUgyfelAzonosito"></param>
        /// <param name="Szervezet"></param>
        /// <param name="Iranyitoszam"></param>
        /// <param name="Telepules"></param>
        /// <param name="Cim"></param>
        /// <param name="Telefon"></param>
        /// <param name="Mobil"></param>
        /// <param name="Email"></param>
        /// <param name="Fax"></param>
        /// <param name="Ok"></param>
        /// <param name="UserID"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURModUgyfelWS(string HirUgyfelAzonosito, string Szervezet, string Iranyitoszam, string Telepules, string Cim
            , string Telefon, string Mobil, string Email, string Fax, string Ok, string UserID, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURModUgyfelWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckField("HirUgyfelAzonosito", HirUgyfelAzonosito);
                Utility.CheckField("Szervezet", Szervezet);
                Utility.CheckField("Iranyitoszam", Iranyitoszam);
                Utility.CheckField("Telepules", Telepules);
                Utility.CheckField("Cim", Cim);
                Utility.CheckField("Ok", Ok);
                Utility.CheckUserID(UserID);
                #endregion
                new SZURManager(this).SZURModUgyfelWS(HirUgyfelAzonosito, Szervezet, Iranyitoszam, Telepules, Cim
                    , Telefon, Mobil, Email, Fax, Ok, UserID);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURModUgyfelWS");
        }

        /// <summary>
        /// Kapcsolattartó módosítása a SZÜR Partnertörzsében
        /// </summary>
        /// <param name="HirUgyfelAzonosito"></param>
        /// <param name="HirKapcsTAzonosito"></param>
        /// <param name="Elotag"></param>
        /// <param name="Nev"></param>
        /// <param name="Beosztas"></param>
        /// <param name="Iranyitoszam"></param>
        /// <param name="Telepules"></param>
        /// <param name="Cim"></param>
        /// <param name="Telefon"></param>
        /// <param name="Mobil"></param>
        /// <param name="Email"></param>
        /// <param name="Fax"></param>
        /// <param name="Ok"></param>
        /// <param name="UserID"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURModKapcsolatTartoWS(string HirUgyfelAzonosito, string HirKapcsTAzonosito, string Elotag, string Nev, string Beosztas, string Iranyitoszam, string Telepules, string Cim, string Telefon, string Mobil, string Email, string Fax, string Ok, string UserID, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURModKapcsolatTartoWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckField("HirUgyfelAzonosito", HirUgyfelAzonosito);
                Utility.CheckField("HirKapcsTAzonosito", HirKapcsTAzonosito);
                Utility.CheckField("Nev", Nev);
                Utility.CheckField("Ok", Ok);
                Utility.CheckUserID(UserID);
                #endregion

                new SZURManager(this).SZURModKapcsolatTartoWS(HirUgyfelAzonosito, HirKapcsTAzonosito, Elotag, Nev, Beosztas
                    , Iranyitoszam, Telepules, Cim, Telefon, Mobil, Email, Fax, Ok, UserID);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURModKapcsolatTartoWS");
        }
        /// <summary>
        ///  elektronikus előadói ív adatainak lekérdezése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="UserID"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZURGetEivAdatokWS(string Evszam, string Konyv, string Foszam, string UserID, out string Eiv, out string Hibakod, out string Hibaszoveg)
        {
            // throw new NotImplementedException();
            Logger.InfoStart("SZURService.SZURGetEivAdatokWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            Eiv = String.Empty;
            #endregion
            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckUserID(UserID);
                #endregion

                new SZURManager(this)
                   .SZURGetEivAdatokWS(Evszam, Konyv, Foszam, UserID, out Eiv,
                                    out Hibakod, out Hibaszoveg);
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURGetEivAdatokWS");
        }

        public string[] SZURAlszamListWS(string Evszam, string Konyv, string Foszam, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURAlszamListWS");
            string[] result;

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                #endregion

                result = new SZURManager(this).SZURAlszamListWS(Evszam, Konyv, Foszam);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
                result = new string[] { };
            }

            Logger.InfoEnd("SZURService.SZURAlszamListWS");
            return result;
        }
        /// <summary>
        ///  belső körlevél készítésének indítása
        /// </summary>
        /// <param name="IratCim"></param>
        /// <param name="Kategoriak"></param>
        /// <param name="IratTipus"></param>
        /// <param name="Tajekoztat"></param>
        /// <param name="Velemeny"></param>
        /// <param name="Form"></param>
        /// <param name="KoroztetoUserID"></param>
        /// <param name="Megjegyzes"></param>
        /// <param name="UserId"></param>
        /// <param name="Foszam"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZURStartKorozWS(string IratCim, Kategoriak Kategoriak, string IratTipus, Szervezetek Tajekoztat, Szervezetek Velemeny, string Form, string KoroztetoUserID, string Megjegyzes, string UserId, out string Evszam, out string Foszam, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURStartKorozWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckField("IratCim", IratCim);
                if (Kategoriak == null || Kategoriak.Kategoria == null) throw new Exception("A Kategoriak adatait kötelező kitölteni!");
                Utility.CheckField("IratTipus", IratTipus);
                Utility.CheckField("KoroztetoNotesUserID", KoroztetoUserID);
                Utility.CheckField("Megjegyzes", Megjegyzes);
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this).SZURStartKorozWS(IratCim, Kategoriak, IratTipus, Tajekoztat, Velemeny, Form, KoroztetoUserID, Megjegyzes, UserId
                            , out Evszam, out Foszam, out Hibakod, out Hibaszoveg);

                // Ha az objTargyszoHiba ki van töltve, akkor hiba volt a tárgyszó mentése során, de az iktatás attól még végrehajtódott
                if (!String.IsNullOrEmpty(Hibaszoveg))
                {
                    Hibakod = Utility.ErrorCodes.GeneralErrorCode;
                    Hibaszoveg = "Az iktatás sikeresen végrehajtódott, de hiba történt a tárgyszó mentése vagy az ügyirat jelleg meghatározása során: " + Hibaszoveg;
                }
                else
                {
                    Hibakod = Utility.ErrorCodes.SuccessCode;
                    Hibaszoveg = String.Empty;
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                Foszam = null;
                Evszam = null;
            }

            Logger.InfoEnd("SZURService.SZURStartKorozWS");
        }

        /// <summary>
        /// Új főszám létrehozása az aktuális évi iktatókönyvben
        /// </summary>
        /// <param name="IktatokonyvKod"></param>
        /// <param name="FCim"></param>
        /// <param name="FTargyszo"></param>
        /// <param name="FelelosUserID"></param>
        /// <param name="UserId"></param>
        /// <param name="Ugyiratjelleg"></param>
        /// <param name="Ugyjellegkod"></param>
        /// <param name="UI_Hatarido"></param>
        /// <param name="UI_HiMod"></param>
        /// <param name="UI_Munkanap"></param>
        /// <param name="Foszam"></param>
        /// <param name="Eloszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetFoszamWS(string IktatokonyvKod, string FCim, string FTargyszo, string FelelosUserID, string UserId, string Ugyiratjelleg, string Ugyjellegkod, string UI_Hatarido, string UI_HiMod, string UI_Munkanap
            , out string Foszam, out string Eloszam, out string Evszam, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetFoszamWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckKonyv(IktatokonyvKod);
                Utility.CheckField("FCim", FCim);
                Utility.CheckField("FTargyszo", FTargyszo);
                Utility.CheckField("FelelosUserID", FelelosUserID);
                Utility.CheckUserID(UserId);
                Utility.CheckField("Ugyiratjelleg", Ugyiratjelleg);
                Utility.CheckField("Ugyjellegkod", Ugyjellegkod);
                //Utility.CheckField("UI_HiMod", UI_HiMod);
                //Utility.CheckDateField("UI_Hatarido", UI_Hatarido);
                //Utility.CheckField("UI_Munkanap", UI_Munkanap);
                #endregion

                new SZURManager(this).SZURGetFoszamWS(IktatokonyvKod, FCim, FTargyszo, FelelosUserID, UserId, Ugyiratjelleg, Ugyjellegkod, UI_Hatarido, UI_HiMod, UI_Munkanap
                            , out Foszam, out Eloszam, out Evszam, out Hibakod, out Hibaszoveg);

                // Ha az objTargyszoHiba ki van töltve, akkor hiba volt a tárgyszó mentése során, de az iktatás attól még végrehajtódott
                if (!String.IsNullOrEmpty(Hibaszoveg))
                {
                    Hibakod = Utility.ErrorCodes.GeneralErrorCode;
                    Hibaszoveg = "Az iktatás sikeresen végrehajtódott, de hiba történt a tárgyszó mentése vagy az ügyirat jelleg meghatározása során: " + Hibaszoveg;
                }
                else
                {
                    Hibakod = Utility.ErrorCodes.SuccessCode;
                    Hibaszoveg = String.Empty;
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                Foszam = null;
                Eloszam = null;
                Evszam = null;
            }

            Logger.InfoEnd("SZURService.SZURGetFoszamWS");
        }

        /// <summary>
        ///  főszám készre jelentése
        /// </summary>
        /// <param name="IktatokonyvKod"></param>
        /// <param name="Foszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="IrattariTetelszam"></param>
        /// <param name="FT1_1"></param>
        /// <param name="FT2_1"></param>
        /// <param name="FT3_1"></param>
        /// <param name="UgyintezoiNaplo"></param>
        /// <param name="UserId"></param>
        /// <param name="statElsofok"></param>
        /// <param name="statJogorv"></param>
        /// <param name="statElsofokNap"></param>
        /// <param name="statElsoJogNap"></param>
        /// <param name="statMasodNap"></param>
        /// <param name="statAtlag"></param>
        /// <param name="statOsszesDb"></param>
        /// <param name="statKarigeny"></param>
        /// <param name="statFelugyelet"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZURFoszamKeszWS(string IktatokonyvKod, string Foszam, string Evszam, string IrattariTetelszam, string FT1_1, string FT2_1, string FT3_1, string UgyintezoiNaplo, string UserId, string statElsofok, string statJogorv, string statElsofokNap, string statElsoJogNap, string statMasodNap, string statAtlag, string statOsszesDb, string statKarigeny, string statFelugyelet, out string Hibakod, out string Hibaszoveg)
        {
            //throw new NotImplementedException();
            Logger.InfoStart("SZURService.SZURFoszamKeszWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            #endregion

            try
            {
                #region Kötelező mezők
                Utility.CheckKonyv(IktatokonyvKod);
                Utility.CheckFoszam(Foszam);
                Utility.CheckEvszam(Evszam);
                Utility.CheckField("IrattariTetelszam", IrattariTetelszam);
                Utility.CheckField("FT1_1", FT1_1);
                Utility.CheckField("UgyintezoiNaplo", UgyintezoiNaplo);

                // 11885 keresre ha üres akkor legyen 0
                if (string.IsNullOrEmpty(statElsofok.TrimEnd())) statElsofok = "0";
                if (string.IsNullOrEmpty(statJogorv.TrimEnd())) statJogorv = "0";
                if (string.IsNullOrEmpty(statElsofokNap.TrimEnd())) statElsofokNap = "0";
                if (string.IsNullOrEmpty(statElsoJogNap.TrimEnd())) statElsoJogNap = "0";
                if (string.IsNullOrEmpty(statMasodNap.TrimEnd())) statMasodNap = "0";
                if (string.IsNullOrEmpty(statAtlag.TrimEnd())) statAtlag = "0";
                if (string.IsNullOrEmpty(statOsszesDb.TrimEnd())) statOsszesDb = "0";
                if (string.IsNullOrEmpty(statKarigeny.TrimEnd())) statKarigeny = "0";
                if (string.IsNullOrEmpty(statFelugyelet.TrimEnd())) statFelugyelet = "0";               
              
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this)
                  .SZURFoszamKeszWS(IktatokonyvKod, Foszam, Evszam, IrattariTetelszam, FT1_1, FT2_1, FT3_1, UgyintezoiNaplo, UserId,
                        statElsofok, statJogorv, statElsofokNap, statElsoJogNap, statMasodNap, statAtlag, statOsszesDb, statKarigeny, statFelugyelet,
                                   out Hibakod, out Hibaszoveg);

            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURFoszamKeszWS");
        }

        /// <summary>
        /// Határozat típusú iratok jogerősítési állapotának lekérdezése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="JogEroStatusz"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetHatJogEroStatusWS(string Evszam, string Foszam, string Alszam
            , out string JogEroStatusz, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetHatJogEroStatusWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                #endregion

                new SZURManager(this).SZURGetHatJogEroStatusWS(Evszam, Foszam, Alszam, out JogEroStatusz);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                JogEroStatusz = null;
            }

            Logger.InfoEnd("SZURService.SZURGetHatJogEroStatusWS");
        }

        /// <summary>
        /// Jogerős határozatok listájának lekérdezése főszám-lista alapján
        /// "Egy főszám-lista alapján visszaad egy határozatlistát."
        /// </summary>
        /// <param name="eredetiFoszamLista"></param>
        /// <param name="hatarozatLista"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetJogerosHatarozatokWS(Foszam[] eredetiFoszamLista, out JogerosHatarozat[] hatarozatLista, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetJogerosHatarozatokWS");

            try
            {
                //*****
                new SZURManager(this).SZURGetJogerosHatarozatokWS(eredetiFoszamLista, out hatarozatLista);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                hatarozatLista = null;
            }

            Logger.InfoEnd("SZURService.SZURGetJogerosHatarozatokWS");
        }

        /// <summary>
        /// Egy ügyben (megadott főszám alapján a teljes láncolatra) listázza az iktatószámokat és hatásukat a sakkórára.
        /// "A megadott főszámhoz tartozó összes alszám sakkóra kódjának, a hozzátartozó szöveges érték, 
        /// az esetleges ok (megállítás, lezárás) és a sakkóra hatás időpontjának listázása"
        /// </summary>
        /// <param name="Ev"></param>
        /// <param name="Foszam"></param>
        /// <param name="SakkoraElemList"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetsakkoraListWS(string Ev, string Foszam, out SakkoraElem[] SakkoraElemList, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetsakkoraListWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckFoszam(Foszam);
                #endregion

                new SZURManager(this).SZURGetsakkoraListWS(Ev, Foszam, out SakkoraElemList);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                SakkoraElemList = null;
            }

            Logger.InfoEnd("SZURService.SZURGetsakkoraListWS");
        }

        /// <summary>
        ///Az irat "IratHatasaUgyintezesre" mezőjének beállítása, és ennek értékétől függően az ügyirat esetleges felfüggesztése/folytatása/lezárása.
        ///határidő újraszámítása
        /// </summary>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Ev"></param>
        /// <param name="Skod"></param>
        /// <param name="Okkod"></param>
        /// <param name="User"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <param name="Eltelt"></param>
        /// <param name="Fennmarado"></param>
        /// <param name="TullepMertekKod"></param>
        /// <param name="TullepMertekText"></param>
        public void SZURSetSakkoraWS(string Foszam, string Alszam, string Ev, string Skod, string Okkod, string User, out string Hibakod, out string Hibaszoveg, out string Eltelt, out string Fennmarado, out string TullepMertekKod, out string TullepMertekText)
        {
            Logger.InfoStart("SZURService.SZURSetSakkoraWS");
            Hibakod = null;
            Hibaszoveg = null;
            Eltelt = null;
            Fennmarado = null;
            TullepMertekKod = null;
            TullepMertekText = null;
            try
            {
                #region SAKKORA KOD ATFORGAT
                if (Skod.Equals("0"))
                    Skod = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
                #endregion

                #region Kötelező mezők
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckEvszam(Ev);
                Utility.CheckSakkoraStatusz(Skod, this.ExecParamCaller);
                // BUG_2742
                // Utility.CheckOkKod(Okkod);
                Utility.CheckOkKod(this.ExecParamCaller, Skod, Okkod, "Okkod");
                Utility.CheckUserID(User);
                #endregion

                new SZURManager(this).SZURSetSakkoraWS(Foszam, Alszam, Ev, Skod, Okkod, User, out Eltelt, out Fennmarado, out TullepMertekKod, out TullepMertekText);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURSetSakkoraWS");
        }

        /// <summary>
        /// Leállító és lezáró sakkóra kódhoz tartozó okok listája
        /// "Az átadott sakkóra hatás kódja alapján a webservice visszaadja 2-es, 5-ös, 9-es, 11-es, 3-as, 6-os, 8-as, 10-es Skod-nak megfelelő, 
        /// a SZÜR által kezelt sakkóra hatásra értelmezett okok listáját."
        /// </summary>
        /// <param name="Skod"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public string[] SZURGetSakkoraOkKodWS(string Skod, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetSakkoraOkKodWS");

            string[] result;

            try
            {
                #region SAKKORA KOD ATFORGAT
                if (Skod.Equals("0"))
                    Skod = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
                #endregion

                #region Kötelező mezők
                // BUG_2742
                //Utility.CheckOkKod(Skod);
                Utility.CheckField("SKod", Skod);
                #endregion

                result = new SZURManager(this).SZURGetSakkoraOkKodWS(Skod);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
                result = null;
            }

            Logger.InfoEnd("SZURService.SZURGetSakkoraOkKodWS");

            return result;
        }

        /// <summary>
        /// Sakkóra leállításának oka
        /// "Visszaadja a sakkóra megállításának lehetséges okait"
        /// </summary>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public string[] SZURGetSakkLeallWS(out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetSakkLeallWS");

            string[] result;

            try
            {
                result = new SZURManager(this).SZURGetSakkLeallWS();

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
                result = null;
            }

            Logger.InfoEnd("SZURService.SZURGetSakkLeallWS");

            return result;
        }

        /// <summary>
        /// Ügy lezárásának oka
        /// "Visszaadja a sakkóra ügy lezáró állapotának lehetséges okait."
        /// </summary>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public string[] SZURGetUgyLezarWS(out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetUgyLezarWS");

            string[] result;

            try
            {
                result = new SZURManager(this).SZURGetUgyLezarWS();

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
                result = null;
            }

            Logger.InfoEnd("SZURService.SZURGetUgyLezarWS");

            return result;
        }
        /// <summary>
        /// Az "üzemzavar, elháríthatatlan akadály" elhárítására fordított napok megadása
        /// "az ügyintézési idő túllépése esetén az ügyintézés során esetlegesen fellépő üzemzavar, elháríthatatlan akadály időtartamának megadása, amely csökkenti az ügyintézésre fordított napok számát."
        /// </summary>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Ev"></param>
        /// <param name="Napok"></param>
        /// <param name="Iratszam"></param>
        /// <param name="User"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURSetUzemzavarWS(string Foszam, string Alszam, string Ev, string Napok, string Iratszam, string User, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURSetUzemzavarWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckEvszam(Ev);
                Utility.CheckField("Napok", Napok);
                Utility.CheckField("Iratszam", Iratszam);
                Utility.CheckUserID(User);
                #endregion

                new SZURManager(this).SZURSetUzemzavarWS(Foszam, Alszam, Ev, Napok, Iratszam, User);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURSetUzemzavarWS");
        }

        /// <summary>
        /// Az ügyintézési idő túllépése miatt visszafizetendő díj, illeték összege, címzettje, határideje
        /// "Az ügyintézési idő túllépése esetén a visszafizetendő díj, illeték összegének, címzettjének és a visszafizetés határidejének megadása az ügyintézés végét jelző alszámos iraton."
        /// </summary>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Ev"></param>
        /// <param name="Osszeg"></param>
        /// <param name="Jogcim"></param>
        /// <param name="Hatarido"></param>
        /// <param name="Cimzett"></param>
        /// <param name="Hatarozatszam"></param>
        /// <param name="User"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURSetDijVisszafizWS(string Foszam, string Alszam, string Ev, string Osszeg, string Jogcim, string Hatarido, string Cimzett, string Hatarozatszam, string User, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURSetDijVisszafizWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckEvszam(Ev);
                Utility.CheckField("Osszeg", Osszeg);
                Utility.CheckField("Jogcim", Jogcim);
                Utility.CheckField("Hatarido", Hatarido);
                Utility.CheckField("Cimzett", Cimzett);
                Utility.CheckField("Hatarozatszam", Hatarozatszam);
                Utility.CheckUserID(User);
                #endregion

                new SZURManager(this).SZURSetDijVisszafizWS(Foszam, Alszam, Ev, Osszeg, Jogcim, Hatarido, Cimzett, Hatarozatszam, User);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURSetDijVisszafizWS");
        }

        /// <summary>
        /// Hatósági ügy statisztikai űrlapja adatainak előzetes lekérdezése
        /// "Az ügy készre jelentése előtt le lehet kérdezni a SZÜR által meghatározott statisztikai űrlap adatait, 
        /// melyet a szakrendszerben az ügyintéző módosíthat, majd a főszám készre jelentésekor véglegesíthet."
        /// </summary>
        /// <param name="Foszam"></param>
        /// <param name="Ev"></param>
        /// <param name="User"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <param name="statElsofok"></param>
        /// <param name="statJogorv"></param>
        /// <param name="statElsofokNap"></param>
        /// <param name="statElsoJogNap"></param>
        /// <param name="statMasodNap"></param>
        /// <param name="statAtlag"></param>
        /// <param name="statOsszesDb"></param>
        /// <param name="statKarigeny"></param>
        /// <param name="statFelugyelet"></param>
        public void SZURgetFoszamstatisztikaWS(string Foszam, string Ev, string User
            , out string Hibakod, out string Hibaszoveg, out string statElsofok, out string statJogorv, out string statElsofokNap, out string statElsoJogNap, out string statMasodNap, out string statAtlag, out string statOsszesDb, out string statKarigeny, out string statFelugyelet)
        {
            Logger.InfoStart("SZURService.SZURgetFoszamstatisztikaWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Ev);
                Utility.CheckEvszam(Foszam);
                Utility.CheckUserID(User);
                #endregion

                new SZURManager(this).SZURgetFoszamstatisztikaWS(Foszam, Ev, User, out statElsofok, out statJogorv, out statElsofokNap
                                , out statElsoJogNap, out statMasodNap, out statAtlag, out statOsszesDb, out statKarigeny, out statFelugyelet);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                // out paramétereknek értéket kell adni:
                statElsofok = null;
                statJogorv = null;
                statElsofokNap = null;
                statElsoJogNap = null;
                statMasodNap = null;
                statAtlag = null;
                statOsszesDb = null;
                statKarigeny = null;
                statFelugyelet = null;
            }

            Logger.InfoEnd("SZURService.SZURgetFoszamstatisztikaWS");
        }

        /// <summary>
        /// Jogerősítendő irat jogerősítése
        /// "Beállítja a jogerősítendő irat jogerőre emelkedésének időpontját."
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="Datum"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURjogerdatumWS(string Konyv, string Foszam, string Alszam, string Evszam, string Datum, string UserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURjogerdatumWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckDateField("Datum", Datum);
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this).SZURjogerdatumWS(Konyv, Foszam, Alszam, Evszam, Datum, UserId);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURjogerdatumWS");
        }

        /// <summary>
        ///  ügyirat-irat átvételének visszavonása
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZURAtvDeleteWS(string Konyv, string Foszam, string Alszam, string Evszam, string UserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURAtvDeleteWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this).SZURAtvDeleteWS(Konyv, Foszam, Alszam, Evszam, UserId);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURAtvDeleteWS");
        }

        /// <summary>
        /// Ügyirat-irat készre jelentésének visszavonása
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURKeszDeleteWS(string Konyv, string Foszam, string Alszam, string Evszam, string UserId, out string Hibakod, out string Hibaszoveg)
        {
            //throw new NotImplementedException();

            Logger.InfoStart("SZURService.SZURKeszDeleteWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            #endregion

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckUserID(UserId);
                #endregion

                if (String.IsNullOrEmpty(Alszam) || Alszam == "0")
                {
                    throw new Exception("Az alszámot kötelező megadni!");
                    // new SZURManager(this)
                    //.SZURUgyiratKeszDeleteWS(Evszam, Konyv, Foszam, UserId, out Hibakod, out Hibaszoveg);


                }
                else
                    new SZURManager(this)
                       .SZURIratKeszDeleteWS(Evszam, Konyv, Foszam, Alszam, UserId, out Hibakod, out Hibaszoveg);

                // Hibakod = Utility.ErrorCodes.SuccessCode;
                // Hibaszoveg = String.Empty;

            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURKeszDeleteWS");
        }

        /// <summary>
        /// A jogerősítő által láttamozandó alszámos iratok teljes listája 
        /// "Az aktuális évi iktatókönyvben adja vissza a jogerősítők által még nem láttamozott összes irat listáját."
        /// --> Azok az iratok kellenek, ahol az aláírók fülön be van jegyezve a láttamozó, de a státusza még "Aláírandó".
        /// TODO: Kérdés, hogy kell-e szűrni a megadott ügyintézőre, vagy a rendszerben lévő összes ilyen irat kell?
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public LattamozIrat[] SZURGetJogeroLattamozandoWS(string UserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetJogeroLattamozandoWS");
            LattamozIrat[] result;

            try
            {
                #region Kötelező mezők
                Utility.CheckUserID(UserId);
                #endregion

                result = new SZURManager(this).SZURGetJogeroLattamozandoWS(UserId);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
                result = new LattamozIrat[] { };
            }

            Logger.InfoEnd("SZURService.SZURGetJogeroLattamozandoWS");
            return result;
        }

        /// <summary>
        /// Jogerősítő láttamozásának bejegyzése, törlése.
        /// "A kijelölt iraton bejegyzi a jogerősítő általi láttamozás tényét vagy törli azt."        /// 
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="Torles"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURJogEroLattaWS(string Konyv, string Foszam, string Alszam, string Evszam, string Torles, string UserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURJogEroLattaWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckField("Torles", Torles);
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this).SZURJogEroLattaWS(Konyv, Foszam, Alszam, Evszam, Torles, UserId);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURJogEroLattaWS");
        }

        /// <summary>
        /// Iratcsomag határidős irattárba helyezésének javaslata (skontró)
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="IratHatidoHely"></param>
        /// <param name="IratHatido"></param>
        /// <param name="IratHatidoOk"></param>
        /// <param name="Torles"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURSetIratHatIdoIratTarWS(string Konyv, string Foszam, string Alszam, string Evszam, string IratHatidoHely, string IratHatido, string IratHatidoOk, string Torles, string UserId
            , out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURSetIratHatIdoIratTarWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckField("IratHatidoHely", IratHatidoHely);
                Utility.CheckField("IratHatidoOk", IratHatidoOk);
                Utility.CheckField("Torles", Torles);
                Utility.CheckUserID(UserId);
                Utility.CheckRequiredDateFormat("IratHatido", IratHatido);
                Utility.CheckDateGreatertahnToday("IratHatido", IratHatido);
                #endregion

                new SZURManager(this).SZURSetIratHatIdoIratTarWS(Konyv, Foszam, Evszam, IratHatidoHely, IratHatido, IratHatidoOk, Torles, UserId);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURSetIratHatIdoIratTarWS");
        }

        /// <summary>
        /// Iratcsomag átmeneti irattárba helyezése
        /// "Az átmeneti irattárba helyezésre átvett iratcsomag letétele az irattárban."
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="IrattarTipus"></param>
        /// <param name="Irattar"></param>
        /// <param name="AttetelMasIrattarba"></param>
        /// <param name="Torles"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURSetIratAtmIratTarWS(string Konyv, string Foszam, string Alszam, string Evszam, string IrattarTipus, string Irattar, string AttetelMasIrattarba, string Torles, string UserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURSetIratAtmIratTarWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckField("IrattarTipus", IrattarTipus);
                Utility.CheckField("Irattar", Irattar);
                Utility.CheckField("AttetelMasIrattarba", AttetelMasIrattarba);
                Utility.CheckField("Torles", Torles);
                Utility.CheckUserID(UserId);
                #endregion

                //Funkciójog-ellenőrzés:
                Utility.Security.CheckFunctionRights(this.ExecParamCaller, UserId, "UgyiratAtadasIrattarba");

                new SZURManager(this).SZURSetIratAtmIratTarWS(Konyv, Foszam, Alszam, Evszam, IrattarTipus, Irattar, AttetelMasIrattarba, Torles, UserId);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURSetIratAtmIratTarWS");
        }

        /// <summary>
        ///  Munkanap nyilvántartás figyelembevételével határidő számítási szolgáltatás
        /// "Az átadott paraméterek alapján vagy a kezdő időpont és a vég időpont közötti munkanapok vagy naptári napok számát adja meg,
        /// vagy a kezdőidőpont és a napok valamint a számítás módja alapján megadja a befejező időpontot."
        /// </summary>
        /// <param name="Kezdete"></param>
        /// <param name="Vege"></param>
        /// <param name="Napok"></param>
        /// <param name="HatidoMod"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetHataridoWS(ref string Kezdete, ref string Vege, ref string Napok, ref string HatidoMod, string UserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetHataridoWS");

            try
            {
                #region Kötelező mezők
                //Utility.CheckField("Kezdete", Kezdete);
                //Utility.CheckField("Vege", Vege);
                //Utility.CheckField("Napok", Napok);
                Utility.CheckField("HatidoMod", HatidoMod);
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this).SZURGetHataridoWS(ref Kezdete, ref Vege, ref Napok, HatidoMod);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURGetHataridoWS");
        }

        public string SZUReBeadvanyIktatasWS(string SubmissionFormId, out string Hibaszoveg)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Ügyjellegek listája ('UGY_FAJTAJA' kódcsoport elemei)
        /// </summary>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public UgyJellegList SZURGetUgyJellegWS(out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetUgyJellegWS");

            UgyJellegList result = null;

            try
            {
                result = new SZURManager(this).SZURGetUgyJellegWS();

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
                result = null;
            }

            Logger.InfoEnd("SZURService.SZURGetUgyJellegWS");

            return result;
        }

        /// <summary>
        /// Ügy vagy irat jóváhagyása (csak FMS könnyített frekvencia engedélyezési eljáráshoz)
        /// "A megadott főszámos vagy alszámos iraton beállítja a kiadmányozás adatait az átadott paramétereknek megfelelően"
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="KiadmanyozoSzerv"></param>
        /// <param name="KiadmanyozoUserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURJovahagyasWS(string Evszam, string Konyv, string Foszam, string Alszam, string KiadmanyozoSzerv, string KiadmanyozoUserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURJovahagyasWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckField("KiadmanyozoSzerv", KiadmanyozoSzerv);
                Utility.CheckField("KiadmanyozoUserId", KiadmanyozoUserId);
                #endregion

                new SZURManager(this).SZURJovahagyasWS(Evszam, Konyv, Foszam, Alszam, KiadmanyozoSzerv, KiadmanyozoUserId);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURJovahagyasWS");
        }

        /// <summary>
        /// Az irat más szervezeti egységhez történő átadásának kezdeményezése
        /// "Feladat: A megadott iratot, vagy ügyiratcsomagot felkínálja átvételre a FelhivottSzerv paraméterben kijelölt szervezeti egység számára."
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="FelhivoSzerv"></param>
        /// <param name="FelhivottSzerv"></param>
        /// <param name="KiadmanyozoUserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURFelhivasAtvetelWS(string Evszam, string Konyv, string Foszam, string Alszam, string FelhivoSzerv, string FelhivottSzerv, string KiadmanyozoUserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURFelhivasAtvetelWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckField("FelhivottSzerv", FelhivottSzerv);
                Utility.CheckField("FelhivoSzerv", FelhivoSzerv);
                Utility.CheckField("KiadmanyozoUserId", KiadmanyozoUserId);
                #endregion

                new SZURManager(this).SZURFelhivasAtvetelWS(Evszam, Konyv, Foszam, Alszam, FelhivoSzerv, FelhivottSzerv, KiadmanyozoUserId);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURFelhivasAtvetelWS");
        }

        /// <summary>
        /// SZURGetHivatalokWS
        /// </summary>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public Hivatal[] SZURGetHivatalokWS(out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetHivatalokWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            #endregion

            System.Collections.Generic.List<Hivatal> result = new System.Collections.Generic.List<Hivatal>();
            try
            {
                #region SEARCH OBJ
                eQuery.BusinessDocuments.KRT_PartnerekSearch src = new eQuery.BusinessDocuments.KRT_PartnerekSearch();
                src.Forras.Value = KodTarak.Partner_Forras.HKP;
                src.Forras.Operator = eQuery.Query.Operators.equals;

                src.Tipus.Value = KodTarak.Partner_Tipus.Szervezet;
                src.Tipus.Operator = eQuery.Query.Operators.equals;

                src.Allapot.Value = KodTarak.Partner_Allapot.Jovahagyott;
                src.Allapot.Operator = eQuery.Query.Operators.equals;

                src.TopRow = 5000;
                #endregion

                KRT_PartnerekService partnerServcie = eAdminService.ServiceFactory.GetKRT_PartnerekService();

                Result partnerResult = partnerServcie.GetAll(this.ExecParamCaller, src);
                partnerResult.CheckError();

                if (partnerResult == null || partnerResult.Ds.Tables[0].Rows.Count < 1)
                    return null;

                Hivatal tempHivatal = null;
                for (int i = 0; i < partnerResult.Ds.Tables[0].Rows.Count; i++)
                {
                    tempHivatal = new Hivatal();
                    tempHivatal.Nev = partnerResult.Ds.Tables[0].Rows[i]["Nev"].ToString();

                    if (!string.IsNullOrEmpty(partnerResult.Ds.Tables[0].Rows[i]["KulsoAzonositok"].ToString()))
                    {
                        string[] splitted = partnerResult.Ds.Tables[0].Rows[i]["KulsoAzonositok"].ToString().Split(',');
                        if (splitted.Length >= 3)
                        {
                            tempHivatal.RovidNev = splitted[0];
                            tempHivatal.MAKKod = splitted[1];
                            tempHivatal.KRID = splitted[2];
                        }
                    }
                    result.Add(tempHivatal);
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
                if (string.IsNullOrEmpty(Hibakod))
                    Hibakod = Utility.ErrorCodes.GeneralErrorCode;
                result = null;
            }
            Logger.InfoEnd("SZURService.SZURGetHivatalokWS");

            if (result == null || result.Count < 1)
                return null;

            return result.ToArray();
        }
        /// <summary>
        ///  vezetői utasítás vagy észrevétel lekérdezése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UserId"></param>
        /// <param name="Kezeles"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZURGetVezetoiUtasitasWS(string Evszam, string Konyv, string Foszam, string Alszam, string UserId, out string FeladatMeghatarozas, out string Kezeles, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetVezetoiUtasitasWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                //Utility.CheckAlszam(Alszam);
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this).SZURGetVezetoiUtasitasWS(Evszam, Konyv, Foszam, Alszam, UserId, out FeladatMeghatarozas, out Kezeles);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                FeladatMeghatarozas = null;
                Kezeles = null;
            }

            Logger.InfoEnd("SZURService.SZURGetVezetoiUtasitasWS");
        }

        /// <summary>
        ///  Az ügyintéző átveendő iratainak a lekérdezése
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="Rendszerkod"></param>
        /// <param name="Iratok"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public eIrat[] SZUReGetAtvIratWS(string UserId, string Rendszerkod, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZUReGetAtvIratWS");
            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            eIrat[] Iratok = null;
            #endregion

            try
            {
                #region Kötelező mezők
                Utility.CheckUserID(UserId);
                #endregion
                Iratok = new SZURManager(this).SZUReGetAtvIratWS(UserId, Rendszerkod);

                ////new SZURManager(this).SZUReGetAtvIratWS(UserId, Rendszerkod, out Iratok, out Hibakod, out Hibaszoveg);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZUReGetAtvIratWS");

            return Iratok;
        }

        /// <summary>
        /// Az ügy adatainak lekérdezése
        /// "Egy ügy főszám elemeinek lekérdezése könyv, évszám, főszám alapján."
        /// </summary>
        /// <param name="Ugy"></param>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UserID"></param>
        /// <param name="Eloszam"></param>
        /// <param name="AktualisEvszam"></param>
        /// <param name="AktualisKonyv"></param>
        /// <param name="AktualisFoszam"></param>
        /// <param name="FelelosUserID"></param>
        /// <param name="Foszamok"></param>
        /// <param name="KapcsoltUgyek"></param>
        /// <param name="Ugyiratjelleg"></param>
        /// <param name="Ugyjellegkod"></param>
        /// <param name="UI_Hatarido"></param>
        /// <param name="UI_HiMod"></param>
        /// <param name="UI_Munkanap"></param>
        /// <param name="Eltelt"></param>
        /// <param name="Fennmarado"></param>
        /// <param name="TullepMertekKod"></param>
        /// <param name="TullepMertekText"></param>
        /// <param name="Teves"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZUReGetUgyAdatokWS(
                 out string Eloszam,
                 ref string Ugy,
                 string Evszam,
                 string Konyv,
                 string Foszam,
                 string Alszam,
                 string UserID,
            out string AktualisEvszam,
            out string AktualisKonyv,
            out string AktualisFoszam,
            out string FelelosUserID,
            out Foszam2[] Foszamok,
            out KapcsoltUgy[] KapcsoltUgyek,
            out string Ugyiratjelleg,
            out string Ugyjellegkod,
            out string UI_Hatarido,
            out string UI_HiMod,
            out string UI_Munkanap,
            out string Eltelt,
            out string Fennmarado,
            out string TullepMertekKod,
            out string TullepMertekText,
            out string Teves,
            out string Hibakod,
            out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZUReGetUgyAdatokWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckUserID(UserID);
                #endregion

                new SZURManager(this).SZUReGetUgyAdatokWS(Evszam, Konyv, Foszam, Alszam, UserID
                    , out Eloszam, ref Ugy, out AktualisEvszam, out AktualisKonyv, out AktualisFoszam, out FelelosUserID, out Foszamok, out KapcsoltUgyek
                    , out Ugyiratjelleg, out Ugyjellegkod, out UI_Hatarido, out UI_HiMod, out UI_Munkanap, out Eltelt
                    , out Fennmarado, out TullepMertekKod, out TullepMertekText, out Teves);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                Eloszam = null;
                AktualisEvszam = null;
                AktualisKonyv = null;
                AktualisFoszam = null;
                FelelosUserID = null;
                Foszamok = null;
                KapcsoltUgyek = null;
                Ugyiratjelleg = null;
                Ugyjellegkod = null;
                UI_Hatarido = null;
                UI_HiMod = null;
                UI_Munkanap = null;
                Eltelt = null;
                Fennmarado = null;
                TullepMertekKod = null;
                TullepMertekText = null;
                Teves = null;
            }

            Logger.InfoEnd("SZURService.SZUReGetUgyAdatokWS");
        }

        /// <summary>
        /// SZURGetIratAdatokWS egy másik változata 
        /// (Interfészdoksiban nincsen benne, de ugyanaz a paraméterlistája a kettőnek.)
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UserID"></param>
        /// <param name="IratTargy"></param>
        /// <param name="IratTipus"></param>
        /// <param name="IratJelleg"></param>
        /// <param name="Szignalo"></param>
        /// <param name="Szignalas"></param>
        /// <param name="Atvevo"></param>
        /// <param name="Munkatars"></param>
        /// <param name="Atveve"></param>
        /// <param name="Bekuldo"></param>
        /// <param name="BekuldoIrsz"></param>
        /// <param name="BekuldoTelepules"></param>
        /// <param name="BekuldoCim"></param>
        /// <param name="Erkezett"></param>
        /// <param name="Hatarido"></param>
        /// <param name="UgyintezoiNaplo"></param>
        /// <param name="ExpedialasMod"></param>
        /// <param name="KuldemenyTipus"></param>
        /// <param name="Rendeltetes"></param>
        /// <param name="Intezkedes"></param>
        /// <param name="Keszrejelentette"></param>
        /// <param name="Keszrejelentve"></param>
        /// <param name="Jovahagyo"></param>
        /// <param name="Jovahagyva"></param>
        /// <param name="KikuldottTerti"></param>
        /// <param name="ErkezettTerti"></param>
        /// <param name="AtnemvettTerti"></param>
        /// <param name="UtolsoTerti"></param>
        /// <param name="Cimzettek"></param>
        /// <param name="CimzettekFile"></param>
        /// <param name="SakkoraStatusz"></param>
        /// <param name="Expedialva"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public void SZUReGetIratAdatokWS(string Evszam, string Konyv, string Foszam, string Alszam, string UserID, out string Ugy, out string IratTargy, out string IratTipus, out string IratJelleg, out string Szignalo, out string Szignalas, out string Atvevo, out string Munkatars, out string Atveve, out string Bekuldo, out string BekuldoIrsz, out string BekuldoTelepules, out string BekuldoCim, out string Erkezett, out string Hatarido, out string UgyintezoiNaplo, out string ExpedialasMod, out string KuldemenyTipus, out string Rendeltetes, out string Intezkedes, out string Keszrejelentette, out string Keszrejelentve, out string Jovahagyo, out string Jovahagyva, out string KikuldottTerti, out string ErkezettTerti, out string AtnemvettTerti, out string UtolsoTerti, out Cimzett[] Cimzettek, out string CimzettekFile, out string SakkoraStatusz, out string Expedialva, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZUReGetIratAdatokWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckUserID(UserID);
                #endregion

                new SZURManager(this).SZURGetIratAdatokWS(Evszam, Konyv, Foszam, Alszam, UserID
                            , out Ugy, out IratTargy, out IratTipus, out IratJelleg, out Szignalo, out Szignalas
                            , out Atvevo, out Munkatars, out Atveve, out Bekuldo, out BekuldoIrsz
                            , out BekuldoTelepules, out BekuldoCim, out Erkezett, out Hatarido, out UgyintezoiNaplo
                            , out ExpedialasMod, out KuldemenyTipus, out Rendeltetes, out Intezkedes
                            , out Keszrejelentette, out Keszrejelentve, out Jovahagyo, out Jovahagyva, out KikuldottTerti
                            , out ErkezettTerti, out AtnemvettTerti, out UtolsoTerti, out Cimzettek
                            , out CimzettekFile, out SakkoraStatusz, out Expedialva);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                // out paramétereknek értéket kell adni:
                Ugy = null; IratTargy = null; IratTipus = null; IratJelleg = null;
                Szignalo = null; Szignalas = null; Atvevo = null; Munkatars = null;
                Atveve = null; Bekuldo = null; BekuldoIrsz = null; BekuldoTelepules = null; BekuldoCim = null;
                Erkezett = null; Hatarido = null; UgyintezoiNaplo = null; ExpedialasMod = null; KuldemenyTipus = null;
                Rendeltetes = null; Intezkedes = null; Keszrejelentette = null; Keszrejelentve = null;
                Jovahagyo = null; Jovahagyva = null; KikuldottTerti = null; ErkezettTerti = null; AtnemvettTerti = null; UtolsoTerti = null;
                Cimzettek = null; CimzettekFile = null; SakkoraStatusz = null; Expedialva = null;
            }

            Logger.InfoEnd("SZURService.SZUReGetIratAdatokWS");
        }

        public void SZURGetStatuszWS(string Evszam, string Konyv, string Foszam, string Alszam, string UserId, out string Statusz, out string Hibakod, out string Hibaszoveg)
        {

            Logger.InfoStart("SZURService.SZURGetStatusWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            Statusz = String.Empty;

            #endregion
            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckUserID(UserId);
                #endregion

                if (String.IsNullOrEmpty(Alszam) || Alszam == "0")
                {
                    new SZURManager(this)
                   .SZURGetUgyiratStatuszWS(Evszam, Konyv, Foszam, UserId,
                                    out Hibakod, out Hibaszoveg, out Statusz);

                }
                else
                    new SZURManager(this)
                       .SZURGetIratStatuszWS(Evszam, Konyv, Foszam, Alszam, UserId,
                                        out Hibakod, out Hibaszoveg, out Statusz);

                // Hibakod = Utility.ErrorCodes.SuccessCode;
                // Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURGetStatusWS");
        }

        public void SZURSetStatuszWS(string Evszam, string Konyv, string Foszam, string Alszam, string Statusz, string UserId, out string RegiStatusz, out string UjStatusz, out string Hibakod, out string Hibaszoveg)
        {
            //throw new NotImplementedException();

            Logger.InfoStart("SZURService.SZURSetStatuszWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            RegiStatusz = String.Empty;
            UjStatusz = String.Empty;

            #endregion
            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckField("Statusz", Statusz);
                Utility.CheckUserID(UserId);
                #endregion

                if (String.IsNullOrEmpty(Alszam) || Alszam == "0")
                {
                    new SZURManager(this)
                   .SZURSetUgyiratStatuszWS(Evszam, Konyv, Foszam, Statusz, UserId,
                                            out RegiStatusz, out UjStatusz, out Hibakod, out Hibaszoveg);

                }
                else
                    new SZURManager(this)
                       .SZURSetIratStatuszWS(Evszam, Konyv, Foszam, Alszam, Statusz, UserId,
                                            out RegiStatusz, out UjStatusz, out Hibakod, out Hibaszoveg);

                // Hibakod = Utility.ErrorCodes.SuccessCode;
                // Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURSetStatusWS");
        }

        /// <summary>
        /// Érkeztetési adatok átadása
        /// "A SZÜR-ből érkeztetési adatok átadása a szakrendszer részére."
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UserId"></param>
        /// <param name="ElhiszErkeztetoszam"></param>
        /// <param name="Ugyiratszam"></param>
        /// <param name="Erkeztetoszam"></param>
        /// <param name="ErkezettDatum"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetErkeztetoAdatokWS(string Evszam, string Konyv, string Foszam, string Alszam, string UserId, out string ElhiszErkeztetoszam, out string Ugyiratszam, out string Erkeztetoszam, out string ErkezettDatum, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetErkeztetoAdatokWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this).SZURGetErkeztetoAdatokWS(Evszam, Konyv, Foszam, Alszam, UserId
                    , out ElhiszErkeztetoszam, out Ugyiratszam, out Erkeztetoszam, out ErkezettDatum);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                ElhiszErkeztetoszam = null;
                Ugyiratszam = null;
                Erkeztetoszam = null;
                ErkezettDatum = null;
            }

            Logger.InfoEnd("SZURService.SZURGetErkeztetoAdatokWS");
        }

        public EIVAdatok SZURGetEloadoiIvAdatokWS(string Evszam, string Konyv, string Foszam, string UserId, out string Hibakod, out string Hibaszoveg)
        {
            throw new NotImplementedException();
        }

        public string SZURSetUgyAdatokWS(string Evszam, string Konyv, string Foszam, string Ugyiratjelleg, string Ugyjellegkod, string IrattariTetelszam, string FT1_1, string FT2_1, string FT3_1, string UserId, out string Hibaszoveg)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Szerelési adatok lekérdezése
        /// "Adott ügyszám alapján átadja az aktuális ügyre vonatkozó szerelési adatokat (elő- és utóiratok) a szakrendszer részére."
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="UserId"></param>
        /// <param name="UtoIrat"></param>
        /// <param name="EloIratok"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetSzerelesiAdatokWS(string Evszam, string Konyv, string Foszam, string UserId, out Foszam2[] UtoIrat, out Foszam2[] EloIratok, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURGetSzerelesiAdatokWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this).SZURGetSzerelesiAdatokWS(Evszam, Konyv, Foszam, UserId, out UtoIrat, out EloIratok);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                EloIratok = null;
                UtoIrat = null;
            }

            Logger.InfoEnd("SZURService.SZURGetSzerelesiAdatokWS");
        }

        /// <summary>
        /// Ügyek összeszerelése
        /// "A szakrendszertől kapott összeszerelendő ügyiratokat a SZÜR összeszereli."
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="EloIratok"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZUReUgyekSzereleseWS(string Evszam, string Konyv, string Foszam, string Alszam, Foszam2[] EloIratok, string UserId, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZUReUgyekSzereleseWS");

            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Evszam);
                Utility.CheckKonyv(Konyv);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                if (EloIratok == null) throw new Exception("Az EloIratok adatait kötelező kitölteni!");
                Utility.CheckUserID(UserId);
                #endregion

                new SZURManager(this).SZUReUgyekSzereleseWS(Evszam, Konyv, Foszam, Alszam, EloIratok, UserId);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZUReUgyekSzereleseWS");
        }

        /// <summary>
        ///  ügyre (megadott főszám alapján a teljes láncolatra) visszaadja az eltelt és a hátralévő ügyintézési időt
        /// </summary>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Ev"></param>
        /// <param name="User"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <param name="Eltelt"></param>
        /// <param name="Fennmarado"></param>
        /// <param name="TullepMertekKod"></param>
        /// <param name="TullepMertekTxt"></param>
        public void SZURGetUgyidoWS(string Foszam, string Alszam, string Ev, string User, out string Hibakod, out string Hibaszoveg, out string Eltelt, out string Fennmarado, out string TullepMertekKod, out string TullepMertekTxt)
        {
            Logger.InfoStart("SZURService.SZURGetUgyidoWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            Eltelt = null;
            Fennmarado = null;
            TullepMertekKod = null;
            TullepMertekTxt = null;
            #endregion
            try
            {
                #region Kötelező mezők
                Utility.CheckEvszam(Ev);
                Utility.CheckFoszam(Foszam);
                Utility.CheckAlszam(Alszam);
                #endregion

                new SZURManager(this)
                    .SZURGetUgyidoWS(Foszam, Alszam, Ev, User, null,
                                     out Hibakod, out Hibaszoveg, out Eltelt, out Fennmarado, out TullepMertekKod, out TullepMertekTxt);

                if (string.IsNullOrEmpty(Hibakod))
                {
                    Hibakod = Utility.ErrorCodes.SuccessCode;
                    Hibaszoveg = String.Empty;
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }

            Logger.InfoEnd("SZURService.SZURGetUgyidoWS");
        }

        /// <summary>
        /// A SOAP kérésben megadja, az ELHITET felületen beírt iktatószámot és a WebService a SZÜR meghatározott nézetében megkeresi azt, 
        /// a keresés eredményét a SOAP válaszban visszaadja.
        /// </summary>
        /// <param name="Iktatoszam"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public string SZURGETIKTATOSZAMWS(string IKTATOSZAM, out string HIBAKOD, out string HIBASZOVEG)
        {
            Logger.InfoStart("SZURService.SZURGetIktatoszamWS");
            #region OUT
            HIBAKOD = Utility.ErrorCodes.SuccessCode;
            HIBASZOVEG = string.Empty;
            string Isvalid = Utility.EnumIgenNem.Nem.ToString();
            #endregion

            try
            {
                SZURManager szm = new SZURManager(this);
                Isvalid = szm.SZURGETIKTATOSZAMWS(IKTATOSZAM, out HIBAKOD, out HIBASZOVEG);

                if (string.IsNullOrEmpty(HIBAKOD))
                {
                    HIBAKOD = Utility.ErrorCodes.SuccessCode;
                    HIBASZOVEG = string.Empty;
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out HIBAKOD, out HIBASZOVEG);
            }
            Logger.InfoEnd("SZURService.SZURGetIktatoszamWS");
            return Isvalid;
        }
        /// <summary>
        /// Ha érvényes iktatószámot nem adott meg az ügyintéző az ELHITET felületen, akkor lehetősége van a SZÜR-ből lekérdezni, 
        /// és SOAP válaszként megkapni a Szervezeti egységek listáját.
        /// </summary>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <returns></returns>
        public SZERVEZETEK SZURGETSZERVEZETEKWS(out string HIBAKOD, out string HIBASZOVEG)
        {
            Logger.InfoStart("SZURService.SZURGetSzervezetekWS");

            #region OUT
            HIBAKOD = Utility.ErrorCodes.SuccessCode;
            HIBASZOVEG = string.Empty;
            SZERVEZETEK result = null;
            #endregion

            try
            {
                result = new SZURManager(this).SZURGETSZERVEZETEKWS(out HIBAKOD, out HIBASZOVEG);

                if (string.IsNullOrEmpty(HIBAKOD))
                {
                    HIBAKOD = Utility.ErrorCodes.SuccessCode;
                    HIBASZOVEG = string.Empty;
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out HIBAKOD, out HIBASZOVEG);
            }
            Logger.InfoEnd("SZURService.SZURGetSzervezetekWS");
            return result;
        }

        /// <summary>
        /// Tértivevények lekérdezése iktatószám alapján 
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Evszam"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="AtNemVett"></param>
        /// <param name="Szukites"></param>
        /// <param name="kezbesitettsegi_informaciok"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURChkKuldemenyTertiAllapotWS(ref string Konyv, ref string Evszam, ref string Foszam, ref string Alszam, string AtNemVett, string Szukites
                , out KuldemenyAllapot[] kuldemeny_allapot
                , out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURService.SZURChkKuldemenyTertiAllapotWS");

            try
            {
                #region Kötelező mezők

                Utility.CheckField("Konyv", Konyv);
                Utility.CheckField("Evszam", Evszam);
                Utility.CheckField("Foszam", Foszam);
                Utility.CheckField("Alszam", Alszam);

                #endregion

                new SZURManager(this).SZURChkKuldemenyTertiAllapotWS(ref Konyv, ref Evszam, ref Foszam, ref Alszam, AtNemVett, Szukites
                                , out kuldemeny_allapot);

                Hibakod = Utility.ErrorCodes.SuccessCode;
                Hibaszoveg = String.Empty;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);

                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);

                Konyv = null;
                Evszam = null;
                Foszam = null;
                Alszam = null;
                kuldemeny_allapot = null;
            }

            Logger.InfoEnd("SZURService.SZURChkKuldemenyTertiAllapotWS");
        }
    }
}