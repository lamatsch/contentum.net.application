﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.BaseUtility;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace Contentum.eIntegrator.WebService.NMHH
{
    /// <summary>
    /// SZURManager - UGYIRAT
    /// </summary>
    public partial class SZURManager
    {
        public const string STATUSZ_TEVES = "Téves";

        /// <summary>
        /// ügyre (megadott főszám alapján a teljes láncolatra) visszaadja az eltelt és a hátralévő ügyintézési időt
        /// </summary>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Ev"></param>
        /// <param name="User"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <param name="Eltelt"></param>
        /// <param name="Fennmarado"></param>
        /// <param name="TullepMertekKod"></param>
        /// <param name="TullepMertekTxt"></param>
        public void SZURGetUgyidoWS(string Foszam, string Alszam, string Ev, string User, string Konyv, out string Hibakod, out string Hibaszoveg, out string Eltelt, out string Fennmarado, out string TullepMertekKod, out string TullepMertekTxt)
        {
            Logger.InfoStart("SZURManager.SZURGetUgyidoWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            Eltelt = null;
            Fennmarado = null;
            TullepMertekKod = null;
            TullepMertekTxt = null;
            #endregion

            try
            {
                string ugyiratId;
                // BUG#5699: 0-ás alszámra is menjen
                if (Alszam == "0")
                {
                    DataRow ugyiratRow = this.GetUgyiratRow(Ev, Konyv, Foszam);
                    ugyiratId = ugyiratRow["Id"].ToString();
                }
                else
                {
                    DataRow iratRow;
                    if (string.IsNullOrEmpty(Konyv))
                        iratRow = this.GetIratRow(Ev, Foszam, Alszam);
                    else
                        iratRow = this.GetIratRow(Ev, Konyv, Foszam, Alszam);

                    ugyiratId = iratRow["Ugyirat_Id"].ToString();
                }

                GetUgyHataridok(ugyiratId, true, out Eltelt, out Fennmarado, out TullepMertekKod, out TullepMertekTxt);
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);
                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }
            Logger.InfoEnd("SZURManager.SZURGetUgyidoWS");
        }



        /// <summary>
        /// Ügyirat-irat készre jelentésének visszavonása
        /// </summary>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="Evszam"></param>
        /// <param name="UserId"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        //public void SZURUgyiratKeszDeleteWS(string Konyv, string Foszam, string Evszam, string UserId, out string Hibakod, out string Hibaszoveg)
        //{
        //    Logger.InfoStart("SZURManager.SZURUgyiratKeszDeleteWS");

        //    #region OUT
        //    Hibakod = Utility.ErrorCodes.SuccessCode;
        //    Hibaszoveg = String.Empty;
        //    #endregion

        //    DataRow ugyiratRow;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(Konyv))
        //            ugyiratRow = this.GetUgyiratRow(Evszam, null, Foszam);
        //        else
        //            ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam);

        //        string ugyiratId = ugyiratRow["Id"].ToString();

        //        var ugyiratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        //        var execParam = this.CreateExecParamByNMHHUserId(UserId);

        //        ugyiratokService.AtvetelUgyintezesre(execParam, ugyiratId);
        //    }
        //    catch (Exception exc)
        //    {
        //        Logger.Error(exc.ToString(), exc);
        //        Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
        //    }
        //    Logger.InfoEnd("SZURManager.SZURUgyiratKeszDeleteWS");
        //}

        /// <summary>
        /// Ügyirat státuszának (Sztornózott/Téves) lekérdezése
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="User"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        /// <param name="Statusz"></param>
        public void SZURGetUgyiratStatuszWS(string Evszam, string Konyv, string Foszam, string UserId, out string Hibakod, out string Hibaszoveg, out string Statusz)
        {
            Logger.InfoStart("SZURManager.SZURGetUgyiratStatuszWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            Statusz = String.Empty;
            #endregion

            DataRow ugyiratRow;
            try
            {

                if (string.IsNullOrEmpty(Konyv))
                    ugyiratRow = this.GetUgyiratRow(Evszam, null, Foszam);
                else
                    ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam);

                string ugyiratId = ugyiratRow["Id"].ToString();

                Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByDataRow(ugyiratRow);
                if (Ugyiratok.Sztornozott(ugyiratStatusz))
                {
                    Statusz = STATUSZ_TEVES;
                }

            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);
                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }
            Logger.InfoEnd("SZURManager.SZURGetUgyiratStatuszWS");
        }

        /// <summary>
        /// Ügyirat státuszának (Sztornózott/Téves) állítása 
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Statusz"></param>
        /// <param name="User"></param>
        /// <param name="RegiStatusz"></param>
        /// <param name="UjStatusz"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURSetUgyiratStatuszWS(string Evszam, string Konyv, string Foszam, string Statusz, string UserId, out string RegiStatusz, out string UjStatusz, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURManager.SZURSetUgyiratStatuszWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            RegiStatusz = String.Empty;
            UjStatusz = String.Empty;
            #endregion

            DataRow ugyiratRow;
            try
            {
                if (string.IsNullOrEmpty(Konyv))
                    ugyiratRow = this.GetUgyiratRow(Evszam, null, Foszam);
                else
                    ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam);

                string ugyiratId = ugyiratRow["Id"].ToString();

                var ugyiratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                var execParam = this.CreateExecParamByNMHHUserId(UserId);

                SZURGetUgyiratStatuszWS(Evszam, Konyv, Foszam, UserId, out Hibakod, out Hibaszoveg, out RegiStatusz);
                if (Hibakod == Utility.ErrorCodes.SuccessCode)
                {
                    if (Statusz.ToUpper() == STATUSZ_TEVES.ToUpper())
                    {
                        Result ugyiratStorno_result = ugyiratokService.Sztornozas(execParam, ugyiratId, String.Empty);
                        if (ugyiratStorno_result.IsError)
                        {
                            throw new ResultException(ugyiratStorno_result);
                        }
                        SZURGetUgyiratStatuszWS(Evszam, Konyv, Foszam, UserId, out Hibakod, out Hibaszoveg, out UjStatusz);
                    }
                    else
                    {
                        if (RegiStatusz.ToUpper() == STATUSZ_TEVES.ToUpper())
                        {
                            Hibakod = Utility.ErrorCodes.GeneralErrorCode;
                            Hibaszoveg = "A művelet nem hajtható végre (Sztornózást nem lehet visszavonni)! ";
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);
                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }
            Logger.InfoEnd("SZURManager.SZURSetUgyiratStatuszWS");
        }

        public void SZURGetEivAdatokWS(string Evszam, string Konyv, string Foszam, string UserId, out string Eiv, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURManager.SZURGetEivAdatokWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            Eiv = String.Empty;
            #endregion

            DataRow ugyiratRow;
            try
            {
                if (string.IsNullOrEmpty(Konyv))
                    ugyiratRow = this.GetUgyiratRow(Evszam, null, Foszam);
                else
                    ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam);

                EREC_IraIratokSearch searchIratok = new EREC_IraIratokSearch();
                searchIratok.Ugyirat_Id.Value = ugyiratRow["Id"].ToString();
                searchIratok.Ugyirat_Id.Operator = Query.Operators.equals;


                var resultIratGetAll = eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService().GetAllWithExtensionAndMetaAndJogosultak(this.ExecParamCaller, searchIratok, null, null, null, null, false, "B3", false, false, true);
                if (resultIratGetAll.IsError)
                {
                    throw new ResultException(resultIratGetAll);
                }
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0} - {1}", ugyiratRow["Foszam_Merge"].ToString(), ugyiratRow["Targy"].ToString());
                sb.AppendLine();
                sb.AppendFormat("Előiratok: {0}", ugyiratRow["Leszarmazott_Iktatoszam"].ToString());
                sb.AppendLine();
                sb.AppendFormat("Utóirat: {0}", ugyiratRow["Foszam_Merge_szulo"].ToString());
                sb.AppendLine();
                sb.AppendFormat("Ügy statisztikai jellemzői: {0}; {1}; {2}", ugyiratRow["FT1"].ToString(), ugyiratRow["FT2"].ToString(), ugyiratRow["FT2"].ToString());
                sb.AppendLine();
                sb.AppendFormat("Az iratcsomag fellelési helye: {0} - {1}", ugyiratRow["UgyFelelos_Nev"].ToString(), ugyiratRow["KRT_Csoportok_Orzo_Nev"].ToString());
                sb.AppendLine();
                string intidoegys = String.Empty;
                if (!String.IsNullOrEmpty(ugyiratRow["IntezesiIdo"].ToString()))
                {
                    if (!String.IsNullOrEmpty(ugyiratRow["IntezesiIdoegyseg"].ToString()))
                    {
                        if (ugyiratRow["IntezesiIdoegyseg"].ToString() == eRecord.BaseUtility.KodTarak.IDOEGYSEG.Munkanap)
                        {
                            intidoegys = "Munkanap";
                        }
                        else
                            intidoegys = "Nap";
                    }
                }
                sb.AppendFormat("Ügy átfutási ideje: {0} {1}", ugyiratRow["IntezesiIdo"].ToString(), intidoegys);
                sb.AppendLine();
                sb.AppendFormat("Szignáló / Szignálás dátuma: {0} / {1}", ugyiratRow["Szignalo"].ToString(), ugyiratRow["SzignalasIdeje"].ToString());
                sb.AppendLine();
                sb.AppendFormat("Felelős ügyintéző / határidő / elintézés dátuma: {0} / {1} / {2}", ugyiratRow["Ugyintezo_Nev"].ToString(), ugyiratRow["Hatarido"].ToString(), ugyiratRow["ElintezesDat"].ToString());
                sb.AppendLine();
                sb.AppendFormat("Elintézés módja/feladat: {0}", ugyiratRow["ElintezesMod"].ToString());
                sb.AppendLine();
                sb.AppendLine();
                foreach (DataRow iratRow in resultIratGetAll.Ds.Tables[0].Rows)
                {
                    sb.AppendFormat("{0} - {1} - {2}", iratRow["IktatoSzam_Merge"].ToString(), iratRow["PostazasIranyaNev"].ToString(), iratRow["Targy"].ToString());
                    sb.AppendLine();
                    sb.AppendFormat("Irat statisztikai jellemzői: {0}; {1}; {2}", iratRow["IT1"].ToString(), iratRow["IT2"].ToString(), iratRow["IT2"].ToString());
                    sb.AppendLine();
                    sb.AppendFormat("Az irat fellelési helye: {0} - {1}", iratRow["IratFelelos_Nev"].ToString(), iratRow["Csoport_Id_OrzoNev"].ToString());
                    sb.AppendLine();
                    if (iratRow["PostazasIranya"].ToString() == eRecord.BaseUtility.KodTarak.POSTAZAS_IRANYA.Bejovo)
                    {
                        sb.AppendFormat("Irat küldője / érkezés ideje: {0} / {1}", iratRow["NevSTR_Bekuldo"].ToString(), iratRow["BeerkezesIdeje"].ToString());
                    }
                    else
                    {
                        sb.AppendFormat("Irat cimzettje / elküldés ideje: {0} / {1}", iratRow["NevSTR_Cimzett"].ToString(), iratRow["PostazasDatuma"].ToString());
                    }
                    sb.AppendLine();
                    sb.AppendFormat("Intézkedő ügyintéző / határidő / elintézés dátuma: {0} / {1} / {2}", iratRow["FelhasznaloCsoport_Id_Ugyintezo_Nev"].ToString(), iratRow["Hatarido"].ToString(), iratRow["IntezesIdopontja"].ToString());
                    sb.AppendLine();
                    sb.AppendFormat("Elintézés módja/feladat: {0}", iratRow["UgyintezesMod_nev"].ToString());
                    sb.AppendLine();
                    sb.AppendLine();
                }

                Eiv = sb.ToString();
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);
                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }
            Logger.InfoEnd("SZURManager.SZURGetUgyiratStatuszWS");
        }

        public void SZURFoszamKeszWS(string IktatokonyvKod, string Foszam, string Evszam, string IrattariTetelszam, string FT1_1, string FT2_1, string FT3_1, string UgyintezoiNaplo, string UserId, string statElsofok, string statJogorv, string statElsofokNap, string statElsoJogNap, string statMasodNap, string statAtlag, string statOsszesDb, string statKarigeny, string statFelugyelet, out string Hibakod, out string Hibaszoveg)
        {
            Logger.InfoStart("SZURManager.SZURFoszamKeszWS");

            #region OUT
            Hibakod = Utility.ErrorCodes.SuccessCode;
            Hibaszoveg = String.Empty;
            #endregion

            DataRow ugyiratRow;
            try
            {
                if (string.IsNullOrEmpty(FT1_1)) throw new Exception("FT1_1 mezőt kötelező kitölteni");

                if (string.IsNullOrEmpty(IktatokonyvKod))
                    ugyiratRow = this.GetUgyiratRow(Evszam, null, Foszam);
                else
                    ugyiratRow = this.GetUgyiratRow(Evszam, IktatokonyvKod, Foszam);

                string ugyiratId = ugyiratRow["Id"].ToString();

                ExecParam execParam = this.CreateExecParamByNMHHUserId(UserId, Utility.EnumXpmFelhasznaloSzervezetTipus.Elintezett, ugyiratRow["FelhasznaloCsoport_Id_Orzo"].ToString());

                #region irattári tétel
                var resIrattar = new Result();

                Logger.Debug("Irattari tételszám beállítás Start...");
                string irattariTetelId = String.Empty;
                using (var irattariTetelService = eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService())
                {
                    var execirattariTetel = execParam;
                    EREC_IraIrattariTetelekSearch src_itsz = new EREC_IraIrattariTetelekSearch();
                    src_itsz.IrattariTetelszam.Value = IrattariTetelszam;
                    src_itsz.IrattariTetelszam.Operator = Query.Operators.equals;
                    resIrattar = irattariTetelService.GetAllWithExtension(execirattariTetel, src_itsz);
                    resIrattar.CheckError();

                    if (resIrattar.Ds.Tables[0].Rows.Count == 0)
                    {
                        string errorMsg = "A megadott irattári tételszám nem található!";
                        Logger.Error(errorMsg);
                        throw new ResultException(errorMsg);
                    }
                    irattariTetelId = resIrattar.Ds.Tables[0].Rows[0]["Id"].ToString();
                }

                if (!String.IsNullOrEmpty(irattariTetelId))
                {
                    var execparam_Ugyirat = execParam.Clone();
                    EREC_UgyUgyiratok erec_UgyUgyiratok = this.GetUgyiratObjById(new Guid(ugyiratId)); ;
                    // összes mező update-elhetőségét kezdetben letiltani:
                    erec_UgyUgyiratok.Updated.SetValueAll(false);
                    erec_UgyUgyiratok.Base.Updated.SetValueAll(false);
                    erec_UgyUgyiratok.Base.Updated.Ver = true;

                    execparam_Ugyirat.Record_Id = ugyiratId;

                    erec_UgyUgyiratok.IraIrattariTetel_Id = irattariTetelId;
                    erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = true;


                    Result resultITSZUpdate = eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().Update(execparam_Ugyirat, erec_UgyUgyiratok);
                    if (resultITSZUpdate.IsError)
                    {
                        throw new ResultException(resultITSZUpdate);
                    }
                }

                #endregion
                
                #region Obj. tárgyszó beállítása

                Logger.Debug("Elintézetté nyilvánítás ok, obj. tárgyszó beállítás Start...");

                /// Tárgyszó érték mentése az objektumhoz:
                /// Iktatás után hívható csak, nincs egy tranzakcióba összevonva az iktatással. 
                /// (Az eRecordWebSite-on is így van, az iktatás után külön hívással megy. Ha ott jó így, itt is jó lesz...)
                /// 

                if (!String.IsNullOrEmpty(FT1_1))
                {

                    Logger.Debug("FT1 tárgyszó beállítás...");

                    KRT_KodTarakService svc_kodtar = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                    KRT_KodTarakSearch src_kodtarak = new KRT_KodTarakSearch();
                    src_kodtarak.Nev.Value = FT1_1;
                    src_kodtarak.Nev.Operator = Query.Operators.equals;
                    src_kodtarak.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'UGYSTATISZTIKA_T1' ");

                    Result res = svc_kodtar.GetAllWithKodcsoport(execParam, src_kodtarak);

                    if (res.IsError || res.Ds == null)
                    {

                        throw new ResultException(res);
                    }

                    if (res.Ds.Tables[0].Rows.Count != 1)
                    {
                        throw new ResultException(Utility.ErrorCodes.GeneralErrorCode, "Nem sikerült az FT1 mező azonosítása! Talált sorok:" + res.Ds.Tables[0].Rows.Count.ToString());
                    }

                    string kod = res.Ds.Tables[0].Rows[0]["Kod"].ToString();

                    this.SetObjektumTargyszo(new Guid(ugyiratId), eUtility.Constants.TableNames.EREC_UgyUgyiratok, TargyszoAzon_FT1, kod, execParam);
                }
                if (!String.IsNullOrEmpty(FT2_1))
                {

                    Logger.Debug("FT2 tárgyszó beállítás...");
                    KRT_KodTarakService svc_kodtar = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                    KRT_KodTarakSearch src_kodtarak = new KRT_KodTarakSearch();
                    src_kodtarak.Nev.Value = FT2_1;
                    src_kodtarak.Nev.Operator = Query.Operators.equals;
                    src_kodtarak.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'UGYSTATISZTIKA_T2' ");

                    Result res = svc_kodtar.GetAllWithKodcsoport(execParam, src_kodtarak);

                    if (res.IsError || res.Ds == null)
                    {

                        throw new ResultException(res);
                    }

                    if (res.Ds.Tables[0].Rows.Count != 1)
                    {
                        throw new ResultException(Utility.ErrorCodes.GeneralErrorCode, "Nem sikerült az FT2 mező azonosítása! Talált sorok:" + res.Ds.Tables[0].Rows.Count.ToString());
                    }

                    string kod = res.Ds.Tables[0].Rows[0]["Kod"].ToString();
                    this.SetObjektumTargyszo(new Guid(ugyiratId), eUtility.Constants.TableNames.EREC_UgyUgyiratok, TargyszoAzon_FT2, kod, execParam);
                }
                if (!String.IsNullOrEmpty(FT3_1))
                {

                    Logger.Debug("FT3 tárgyszó beállítás...");
                    KRT_KodTarakService svc_kodtar = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                    KRT_KodTarakSearch src_kodtarak = new KRT_KodTarakSearch();
                    src_kodtarak.Nev.Value = FT3_1;
                    src_kodtarak.Nev.Operator = Query.Operators.equals;
                    src_kodtarak.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'UGYSTATISZTIKA_T3' ");

                    Result res = svc_kodtar.GetAllWithKodcsoport(execParam, src_kodtarak);

                    if (res.IsError || res.Ds == null)
                    {

                        throw new ResultException(res);
                    }

                    if (res.Ds.Tables[0].Rows.Count != 1)
                    {
                        throw new ResultException(Utility.ErrorCodes.GeneralErrorCode, "Nem sikerült az FT2_1 mező azonosítása! Talált sorok:" + res.Ds.Tables[0].Rows.Count.ToString());
                    }

                    string kod = res.Ds.Tables[0].Rows[0]["Kod"].ToString();
                    this.SetObjektumTargyszo(new Guid(ugyiratId), eUtility.Constants.TableNames.EREC_UgyUgyiratok, TargyszoAzon_FT3, kod, execParam);
                }
                #endregion
                
                #region Statisztika tárgyszavak elmentése

                this.SetObjektumTargyszavak(new Guid(ugyiratId), eUtility.Constants.TableNames.EREC_UgyUgyiratok, execParam
                        , new KeyValuePair<string, string>(TargyszoAzon_statElsofok, statElsofok)
                        , new KeyValuePair<string, string>(TargyszoAzon_statJogorv, statJogorv)
                        , new KeyValuePair<string, string>(TargyszoAzon_statElsofokNap, statElsofokNap)
                        , new KeyValuePair<string, string>(TargyszoAzon_statElsoJogNap, statElsoJogNap)
                        , new KeyValuePair<string, string>(TargyszoAzon_statMasodNap, statMasodNap)
                        , new KeyValuePair<string, string>(TargyszoAzon_statAtlag, statAtlag)
                        , new KeyValuePair<string, string>(TargyszoAzon_statOsszesDb, statOsszesDb)
                        , new KeyValuePair<string, string>(TargyszoAzon_statKarigeny, statKarigeny)
                        , new KeyValuePair<string, string>(TargyszoAzon_statFelugyelet, statFelugyelet));

                #endregion

                ExecParam execParam_felj = execParam.Clone();
                execParam_felj.Record_Id = ugyiratId;

                EREC_HataridosFeladatok erec_HataridosFeladatok = new EREC_HataridosFeladatok();
                erec_HataridosFeladatok.Obj_Id = ugyiratId;
                erec_HataridosFeladatok.Updated.Obj_Id = true;

                erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                erec_HataridosFeladatok.Updated.Obj_type = true;

                erec_HataridosFeladatok.Azonosito = ugyiratRow["Foszam_Merge"].ToString();
                erec_HataridosFeladatok.Updated.Azonosito = true;

                erec_HataridosFeladatok.Leiras = UgyintezoiNaplo;
                erec_HataridosFeladatok.Updated.Leiras = true;

                erec_HataridosFeladatok.Tipus = eUtility.KodTarak.FELADAT_TIPUS.Megjegyzes;
                erec_HataridosFeladatok.Updated.Tipus = true;

                erec_HataridosFeladatok.Altipus = eUtility.KodTarak.FELADAT_ALTIPUS.Megjegyzes;
                erec_HataridosFeladatok.Updated.Tipus = true;

                var ugyiratokService = eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                var resultElintezett = ugyiratokService.ElintezetteNyilvanitas(execParam_felj, DateTime.Now.ToString(), Ugyirat_ElintezesiMod, erec_HataridosFeladatok);
                resultElintezett.CheckError();

                // BLG#7145: Ügyirat lezárást is meg kell hívni:
                var resultLezaras = ugyiratokService.Lezaras(execParam, ugyiratId, DateTime.Now.ToString(), String.Empty);
                resultLezaras.CheckError();

            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString(), exc);
                Utility.SetHibakodHibaszoveg(exc, out Hibakod, out Hibaszoveg);
            }
            Logger.InfoEnd("SZURManager.SZURFoszamKeszWS");
        }
    }
}