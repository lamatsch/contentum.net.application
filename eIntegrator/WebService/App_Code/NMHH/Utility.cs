﻿using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.NMHH
{
    /// <summary>
    /// Summary description for Utility
    /// </summary>
    public static class Utility
    {
        #region Extension methods

        /// <summary>
        /// Id listából SQL szűrési feltétel string létrehozása ('[Guid1]','[Guid2]',...,[Guidn]')
        /// </summary>
        /// <param name="idList"></param>
        /// <returns></returns>
        public static string ToSqlInnerSearchString(this List<Guid> idList)
        {
            return "'" + String.Join("','", idList.Select(e => e.ToString()).ToArray()) + "'";
        }

        /// <summary>
        /// String listából SQL szűrési feltétel string létrehozása ('[string1]','[string2]',...,[stringn]')
        /// </summary>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public static string ToSqlInnerSearchString(this List<string> itemList)
        {
            return "'" + String.Join("','", itemList.ToArray()) + "'";
        }

        /// <summary>
        /// Dictionary felépítése a tábla soraira: 'Id' mező a kulcs, DataRow típusú az érték
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        public static Dictionary<Guid, DataRow> ToDictionary(this DataTable dataTable)
        {
            return dataTable.ToDictionary("Id");
        }

        /// <summary>
        /// Dictionary felépítése a tábla soraira: a megadott mező a kulcs, DataRow típusú az érték
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="keyFieldName"></param>
        /// <returns></returns>
        public static Dictionary<Guid, DataRow> ToDictionary(this DataTable dataTable, string keyFieldName)
        {
            Dictionary<Guid, DataRow> dataRowDict = new Dictionary<Guid, DataRow>();
            foreach (DataRow row in dataTable.Rows)
            {
                Guid key = (Guid)row[keyFieldName];
                dataRowDict[key] = row;
            }

            return dataRowDict;
        }

        public static void LoadBusinessDocumentFromDataRow(object BusinessDocument, DataRow row)
        {
            try
            {
                if (row != null)
                {
                    System.Reflection.PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                    foreach (System.Reflection.PropertyInfo P in Properties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BusinessDocument, row[P.Name].ToString(), null);
                            }
                        }
                    }
                    System.Reflection.FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);

                    System.Reflection.PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                    foreach (System.Reflection.PropertyInfo P in BaseProperties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BaseObject, row[P.Name].ToString(), null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        public static class ErrorCodes
        {
            /// <summary>
            /// Az általános hibakód NMHH-s válasznál (ha hiba van, de a hiba nem tartalmaz konkrét hibakódot, csak hibaüzenetet)
            /// </summary>
            public const string GeneralErrorCode = "1";
            /// <summary>
            /// 
            /// </summary>
            public const string SuccessCode = "0";
        }

        public class LoginData
        {
            public Guid FelhasznaloId { get; set; }

            public Guid SzervezetId { get; set; }

            public Guid CsoportTag_Id { get; set; }
        }

        public static class Security
        {
            /// <summary>
            /// HttpRequest authentikáció:
            /// Basic authentikációra ellenőrzünk: a megadott felhasználónév/jelszó párost próbáljuk meg bejelentkeztetni a Contentumba
            /// </summary>
            /// <param name="request"></param>
            public static LoginData AuthenticateRequest(HttpRequest request)
            {
                // requestHeader["Authorization"]	"Basic QWRtaW46MTIzNDU2
                // System.Text.Encoding.UTF8.GetString(Convert.FromBase64String("QWRtaW46MTIzNDU2"))

                var authString = request.Headers["Authorization"];

                const string basicPrefix = "Basic ";

                // "Basic " stringgel kezdődik az Authorization header értéke, ez után jön a Base64-ben kódolt felhasználónév:jelszó pár
                if (String.IsNullOrEmpty(authString)
                    || !authString.StartsWith(basicPrefix))
                {
                    throw new HttpException(401, "Basic authentication required");
                }

                string basicAuthContentBase64 = authString.Substring(basicPrefix.Length);

                string basicAuthContent = System.Text.Encoding.GetEncoding("iso-8859-2").GetString(Convert.FromBase64String(basicAuthContentBase64));

                // ':' karakterrel elválasztva jön a felhasználónév:jelszó páros:
                int i = basicAuthContent.IndexOf(':');
                if (i < 0)
                {
                    Logger.Error("Authorization header: Hiányzó ':' karakter a felhasználónév:jelszó párosban...");
                    throw new HttpException(401, "Basic authentication required");
                }

                string userName = basicAuthContent.Substring(0, i);
                string password = basicAuthContent.Substring(i + 1);

                LoginData loginData = new LoginData();

                #region Login metódus meghívása

                var authenticationService = eUtility.eAdminService.ServiceFactory.GetAuthenticationService();
                var loginResult = authenticationService.Login(new ExecParam()
                        , new Login()
                        {
                            FelhasznaloNev = userName,
                            Jelszo = password,
                            Tipus = "Basic"
                        }
                        );

                if (loginResult.IsError)
                {
                    Logger.Error(String.Format("Sikertelen bejelentkezés: ErrorCode: {0}, ErrorMessage: {1}", loginResult.ErrorCode, loginResult.ErrorMessage));
                    throw new HttpException(401, "Sikertelen bejelentkezés: A megadott felhasználónév/jelszó páros nem megfelelő!");
                }

                Guid userId = new Guid(loginResult.Uid);
                loginData.FelhasznaloId = userId;

                #endregion

                #region Szervezet Id (KRT_CsoportTagok) lekérdezése

                var csoportTagokService = eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

                var searchObjCsoportTagok = new eQuery.BusinessDocuments.KRT_CsoportTagokSearch();
                searchObjCsoportTagok.Csoport_Id_Jogalany.Value = loginData.FelhasznaloId.ToString();
                searchObjCsoportTagok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                var xpm = new ExecParam()
                {
                    LoginUser_Id = userId.ToString(),
                    Felhasznalo_Id = userId.ToString()
                };
                var resultCsoportTagok = csoportTagokService.GetAll(xpm, searchObjCsoportTagok);

                if (resultCsoportTagok.IsError)
                {
                    throw new ResultException(resultCsoportTagok);
                }

                // TODO: Mi van, ha több szervezete is van?
                // --> Egyelőre nem dobunk hibát, az elsőt választjuk ki

                if (resultCsoportTagok.Ds.Tables[0].Rows.Count > 0)
                {
                    loginData.SzervezetId = (Guid)resultCsoportTagok.Ds.Tables[0].Rows[0]["Csoport_Id"];
                    loginData.CsoportTag_Id = (Guid)resultCsoportTagok.Ds.Tables[0].Rows[0]["Id"];
                }

                #endregion

                #region Check existing user if UserId field is set in SOAP request
                string userIdValue = null;
                try
                {
                    var xml = GetInputStream();
                    var xmlDoc = XmlHelper.GetXmlDocumentFromText(xml);
                    var userIdNode = xmlDoc.SelectSingleNode("//UserID");
                    if (userIdNode != null)
                    {
                        userIdValue = userIdNode.InnerText;
                    }
                }
                catch (Exception x)
                {
                    Logger.Error("", x);
                }

                if (userIdValue != null)
                {
                    if (userIdValue.Length > 0)
                    {
                        var svcFelhasznalok = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                        var searchFelhasznalok = new KRT_FelhasznalokSearch();
                        searchFelhasznalok.UserNev.Filter(userIdValue);
                        Result res = svcFelhasznalok.GetAll(xpm, searchFelhasznalok);

                        if (res.IsError)
                        {
                            var err = String.Format("KRT_FelhasznalokService.GetAll hiba: {0},{1}", res.ErrorCode, res.ErrorMessage);
                            Logger.Error(err);
                            throw new HttpException(500, err);
                        }
                        else if (res.GetCount == 0)
                        {
                            //var err = "Érvénytelen UserID";
                            //Logger.Error(err);
                            //throw new HttpException(403, err);
                            throw new InvalidUserException();
                        }
                    }
                    else
                    {
                        //var err = "Érvénytelen UserID";
                        //Logger.Error(err);
                        //throw new HttpException(403, err);
                        throw new InvalidUserException();
                    }
                }
                #endregion

                return loginData;
            }

            /// <summary>
            /// Ellenőrzés, hogy a megadott felhasználónak (NMHH UserId) megvan-e az adott funkciójoga.
            /// Ha nincs, Exception.
            /// </summary>
            /// <param name="execParam"></param>
            /// <param name="nmhhUserId"></param>
            /// <param name="funkcioKod"></param>
            public static void CheckFunctionRights(ExecParam execParam, string nmhhUserId, string funkcioKod)
            {
                bool hasFunctionRight = HasFunctionRights(execParam, nmhhUserId, funkcioKod);

                if (hasFunctionRight == false)
                {
                    throw new ResultException("Nincs jogosultsága a művelet végrehajtásához!");
                }
            }

            /// <summary>
            /// Ellenőrzés, hogy a megadott felhasználónak (NMHH UserId) megvan-e az adott funkciójoga.
            /// </summary>
            /// <param name="execParam"></param>
            /// <param name="nmhhUserId"></param>
            /// <param name="funkcioKod"></param>
            /// <returns></returns>
            public static bool HasFunctionRights(ExecParam execParam, string nmhhUserId, string funkcioKod)
            {
                ExecParam execParamUserId = SZURManager.CreateExecParamByNMHHUserId(nmhhUserId, execParam);

                bool hasFunctionRight = eAdminService.ServiceFactory.GetKRT_FunkciokService().HasFunctionRight(execParamUserId, funkcioKod);

                return hasFunctionRight;
            }
        }

        public static class DataBaseFunctions
        {
            /// <summary>
            /// A megadott két dátum közti munkanapok száma.
            /// Ha datumTol > datumIg, akkor a visszatérési érték null lesz.
            /// </summary>
            /// <param name="service"></param>
            /// <param name="datumTol"></param>
            /// <param name="datumIg"></param>
            /// <returns></returns>
            public static int? GetMunkanapokSzama(System.Web.Services.WebService service, DateTime datumTol, DateTime datumIg)
            {
                DataContext dataContext = new DataContext(service.Application);

                bool isConnectionOpenHere = false;

                try
                {
                    isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm.CommandType = System.Data.CommandType.Text;
                    sqlComm.Connection = dataContext.Connection;
                    sqlComm.Transaction = dataContext.Transaction;

                    string sqlCmd = @"select dbo.fn_munkanapok_szama(@dateFrom, @dateTo)";

                    sqlComm.CommandText = sqlCmd;
                    sqlComm.Parameters.Add(new SqlParameter("@dateFrom", SqlDbType.DateTime) { Value = datumTol });
                    sqlComm.Parameters.Add(new SqlParameter("@dateTo", SqlDbType.DateTime) { Value = datumIg });

                    var result = sqlComm.ExecuteScalar();

                    return result as int?;
                }
                finally
                {
                    dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
                }
            }
        }

        #region Static methods

        /// <summary>
        /// Hibakód/hibaszöveg beállítása NMHH-s webservice válaszüzenetéhez a megadott Exceptionből
        /// </summary>
        /// <param name="exc"></param>
        /// <param name="hibakod"></param>
        /// <param name="hibaszoveg"></param>
        public static void SetHibakodHibaszoveg(Exception exc, out string hibakod, out string hibaszoveg)
        {
            if (exc is ResultException)
            {
                hibakod = (exc as ResultException).ErrorCode;
                hibaszoveg = (exc as ResultException).ErrorMessage;

                hibaszoveg = ResultError.ErrorMessageCodeToDetailedText(hibaszoveg);

                // Ha nincs megadva hibakód, az általános hibakódot adjuk meg: 1
                if (String.IsNullOrEmpty(hibakod))
                {
                    hibakod = ErrorCodes.GeneralErrorCode;
                }

                if (String.IsNullOrEmpty(hibaszoveg))
                {
                    hibaszoveg = exc.Message;
                }

                // BUG_2742
                if (hibakod == hibaszoveg)
                    hibakod = ErrorCodes.GeneralErrorCode;

                // Ha van megadva ErrorDetail, akkor azt még hozzátesszük a hibaszöveghez: (BUG#4050/Task#4097):
                var result = (exc as ResultException).GetResult();
                if (result != null && result.ErrorDetail != null && !result.ErrorDetail.IsEmpty)
                {
                    try
                    {
                        //if (!string.IsNullOrEmpty(result.ErrorDetail.Message))
                        //{
                        //    hibaszoveg += " (" + result.ErrorDetail.Message + ")";
                        //}

                        var execParam = HttpContext.Current.Items["CurrentExecParam"] as ExecParam;
                        if (execParam != null)
                        {
                            var errorDetailMsg = ResultError.GetErrorDetailInfoMsgSzur(result.ErrorDetail, execParam, HttpContext.Current.Cache);
                            if (!String.IsNullOrEmpty(errorDetailMsg))
                            {
                                hibaszoveg += " (" + errorDetailMsg + ")";
                            }
                        }
                    }
                    catch (Exception excErrorDetail)
                    {
                        Logger.Warn("SetHibakodHibaszoveg hiba (ErrorDetail): " + excErrorDetail.ToString());
                    }
                }
            }
            else
            {
                hibakod = ErrorCodes.GeneralErrorCode;
                hibaszoveg = exc.Message;
                hibaszoveg = ResultError.ErrorMessageCodeToDetailedText(hibaszoveg);
            }
        }

        /// <summary>
        /// Megadott objektum serializálása xml stringre.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SerializeObjToXML(object obj)
        {
            using (System.IO.StringWriter stringwriter = new Utf8StringWriter())
            {
                var serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(stringwriter, obj);

                return stringwriter.ToString();
            }
        }

        public class Utf8StringWriter : System.IO.StringWriter
        {
            // Use UTF8 encoding but write no BOM to the wire
            public override Encoding Encoding
            {
                get { return new UTF8Encoding(false); } // in real code I'll cache this encoding.
            }
        }


        #region Extensions

        /// <summary>
        /// Cimzett objektum irányítószám, település, többi cím összefűzése egy teljes címmé.
        /// Pl.: 8000 Székesfehérvár, Berényi út 72-100.
        /// </summary>
        /// <param name="cimzett"></param>
        /// <returns></returns>
        public static string GetTeljesCim(this Cimzett cimzett)
        {
            return String.Format("{0} {1}, {2}", cimzett.Iranyitoszam, cimzett.Telepules, cimzett.Cim).Trim();
        }

        #endregion

        #endregion

        private static void SetLevelFromResult(Result result)
        {
            if (!String.IsNullOrEmpty(result.ErrorMessage) && !String.IsNullOrEmpty(result.ErrorCode))
            {
                int errorNumber = ResultError.GetErrorNumberFromResult(result);
            }
        }
        /// <summary>
        /// Result ErrorMessage lefordítása kódról szövegre.
        /// </summary>
        /// <param name="result"></param>
        public static void SetErrorMessageFromResultObject(ref Result result)
        {
            result.ErrorMessage = ResultError.GetErrorMessageFromResultObject(result);
        }

        #region Kötelező mezők
        public static void CheckEvszam(String evszam)
        {
            CheckField("Evszam", evszam);
        }

        public static void CheckKonyv(String konyv)
        {
            CheckField("Konyv", konyv);
        }

        public static void CheckFoszam(String foszam)
        {
            CheckField("Foszam", foszam);
        }

        public static void CheckAlszam(String alszam)
        {
            CheckField("Alszam", alszam);
        }
        public static void CheckUserID(String userID)
        {
            CheckField("UserID", userID);
        }
        public static void CheckSakkoraStatusz(string sakkoraStatus, ExecParam execParam)
        {
            CheckField("SakkoraStatusz", sakkoraStatus);

            #region SAKKORA KOD ATFORGAT
            if (sakkoraStatus.Equals("0"))
                sakkoraStatus = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
            #endregion

            // Sakkóra státusz érték ellenőrzése: benne van-e ez a kód a kódcsoportban:
            CheckKodtarKodExists(Sakkora.IRAT_HATASA_UGYINTEZESRE, sakkoraStatus, execParam);
        }
        /// <summary>
        /// Sakkora ok mező ellenőrzése
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="sakkoraStatus"></param>
        /// <param name="okKod"></param>
        /// <param name="okKodFieldName">Az esetleges hibaüzenetben a hivatkozott mező neve ("OkKod" vagy "SakkoraStatusOk")</param>
        public static void CheckOkKod(ExecParam execParam, String sakkoraStatus, String okKod, string okKodFieldName)
        {
            // BUG_2742
            // CheckField("OkKod", okKod);
            List<string> nincsHatassalValues = new List<string> { "0", "0_NONE", "0_ON_PROGRESS", "0_PAUSE", "0_STOP" };
            List<string> pauseValues = new List<string> { "2", "6", "9", "11" };
            List<string> stopValues = new List<string> { "3", "7", "8", "10" };

            string[] lehetsegesOkok = { string.Empty };

            #region 0 - Nincs hatassal eset - nem kell ok
            if (nincsHatassalValues.Contains(sakkoraStatus))
                return;
            #endregion

            if (pauseValues.Contains(sakkoraStatus))
            {
                CheckField(okKodFieldName, okKod);

                List<KodTar_Cache.KodTarElem> lehetsegesOk = Sakkora.SakkoraFelfuggesztesOkaiKodtarak(execParam, sakkoraStatus);
                Sakkora.CheckLehetsegesFelfuggesztesOk(execParam, lehetsegesOk, okKod);
            }
            if (stopValues.Contains(sakkoraStatus))
            {
                // "Ha a SakkoraStatus paraméter értéke 3, 7, 8, vagy 10, akkor a kód megadása lehetséges"
                if (string.IsNullOrEmpty(okKod)) return;

                List<KodTar_Cache.KodTarElem> lehetsegesOk = Sakkora.SakkoraLezarasOkaiKodtarak(execParam, sakkoraStatus);
                Sakkora.CheckLehetsegesLezarasOk(execParam, lehetsegesOk, okKod);
            }
        }

        public static void CheckField(String fieldName, String field)
        {
            if (string.IsNullOrEmpty(field.TrimEnd())) throw new Exception("A " + fieldName + " mezőt kötelező kitölteni!");
        }

        public static void CheckDateField(String fieldName, String field)
        {
            if (string.IsNullOrEmpty(field)) throw new Exception("A " + fieldName + "mezőt kötelező kitölteni!");

            // Dátumformátum-ellenőrzés:
            CheckDateFormat(fieldName, field);
        }

        public static void CheckCimzettek(Cimzett[] cimzettek, ExecParam execParam)
        {
            if (cimzettek == null
                    || cimzettek.Length == 0
                    ) throw new Exception("A Cimzettek adatait kötelező kitölteni!");

            // Kódtárfüggőség lekérése:
            KodtarFuggosegDataClass ktFuggosegNmhhExpMod = GetKodtarFuggoseg(SZURManager.KCS_NMHH_EXPMOD, SZURManager.KCS_KULDEMENY_KULDES_MODJA, execParam);

            // Expediálás módjának ellenőrzése:
            foreach (var cimzett in cimzettek)
            {
                string kuldKuldesModKod;
                CheckCimzettExpedialasMod(cimzett, ktFuggosegNmhhExpMod, execParam, out kuldKuldesModKod);
            }
        }

        /// <summary>
        /// Az "igen"/"nem" -et váró mező értékének ellenőrzése
        /// Megengedett értékek: "igen", "i", "1" ==> true; "nem", "n", "0" ==> false
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="fieldValue"></param> 
        /// <returns></returns>
        public static void CheckIgenNemString(string fieldName, string fieldValue, out bool? convertedValue)
        {
            if (!String.IsNullOrEmpty(fieldValue))
            {
                switch (fieldValue.ToLower())
                {
                    case "igen":
                    case "i":
                    case "1":
                        convertedValue = true;
                        break;
                    case "nem":
                    case "n":
                    case "0":
                        convertedValue = false;
                        break;
                    default:
                        throw new ResultException(String.Format("A '{0}' mező értéke nem megfelelő: '{1}'. (Megengedett értékek: 'igen', 'i', '1', 'nem', 'n', '0')"
                                    , fieldName, fieldValue));
                }
            }
            else
            {
                convertedValue = null;
            }
        }

        /// <summary>
        /// Cimzett.cExpedialasMod mező ellenőrzése, hogy benne van-e az 'NMHH_EXPMOD' kódcsoportban,
        /// illetve hogy sikerül-e lemappelni az értékét a 'KULDEMENY_KULDES_MODJA' kódcsoport egy elemére
        /// </summary>
        /// <param name="cimzett"></param>
        /// <param name="ktFuggosegNmhhExpMod"></param>
        /// <param name="execParam"></param>
        /// <param name="kuldKuldesModKod"></param>
        public static void CheckCimzettExpedialasMod(Cimzett cimzett, KodtarFuggosegDataClass ktFuggosegNmhhExpMod, ExecParam execParam, out string kuldKuldesModKod)
        {
            // Expediálás módja:                
            if (!String.IsNullOrEmpty(cimzett.cExpedialasMod))
            {
                // BUG#5901: NMHH_EXPMOD - KULDEMENY_KULDES_MODJA mappingből kell kiszedni a megadott expediálásmódot

                var ktListNmhhExpMod = KodTar_Cache.GetKodtarakByKodCsoportList(SZURManager.KCS_NMHH_EXPMOD, execParam, HttpContext.Current.Cache, null);
                var ktListKuldKuldesMod = KodTar_Cache.GetKodtarakByKodCsoportList(SZURManager.KCS_KULDEMENY_KULDES_MODJA, execParam, HttpContext.Current.Cache, null);

                KodTar_Cache.KodTarElem ktNmhhExpMod = ktListNmhhExpMod.FirstOrDefault(x => x.Nev == cimzett.cExpedialasMod);
                if (ktNmhhExpMod == null)
                {
                    throw new ResultException("A megadott 'Cimzett.cExpedialasMod' érték nem található az 'NMHH_EXPMOD' kódcsoportban! ('" + cimzett.cExpedialasMod + "')");
                }

                // Kódtárfüggőségből kivesszük, hogy melyik kódtárelem(ek) tartozik hozzá:
                List<string> mappedKodtarIdList = null;
                if (ktFuggosegNmhhExpMod.Items != null)
                {
                    mappedKodtarIdList = ktFuggosegNmhhExpMod.Items.Where(e => e.VezerloKodTarId == ktNmhhExpMod.Id).Select(e => e.FuggoKodtarId).ToList();
                }

                if (mappedKodtarIdList == null || mappedKodtarIdList.Count == 0)
                {
                    throw new ResultException("A megadott 'Cimzett.cExpedialasMod' értékhez nincs megadva kódtárelem az 'NMHH_EXPMOD' - 'KULDEMENY_KULDES_MODJA' kódtárfüggőségben! ('" + cimzett.cExpedialasMod + "')");
                }

                var ktKuldesMod = ktListKuldKuldesMod.FirstOrDefault(kt => kt.Id == mappedKodtarIdList[0]);

                if (ktKuldesMod == null)
                {
                    throw new ResultException("Hiba az NMHH_EXPMOD - KULDEMENY_KULDES_MODJA kódtárfüggőségben, nem létező kódtár Id: '" + mappedKodtarIdList[0] + "'");
                }

                kuldKuldesModKod = ktKuldesMod.Kod;
            }
            else
            {
                // Kötelező a mező:
                throw new ResultException("Expediálás módjának megadása kötelező! (Cimzett.cExpedialasMod)");
            }
        }

        /// <summary>
        /// A megadott 'KULDEMENY_KULDES_MODJA' kódcsoportbeli kódtárkód leképzése egy 'NMHH_EXPMOD' kódcsoportbeli kódtárelemre
        /// </summary>
        /// <param name="kuldKuldesmodKod"></param>
        /// <returns></returns>
        public static KodTar_Cache.KodTarElem GetNmhhExpModKodtarByKuldKuldesmod(string kuldKuldesmodKod, KodtarFuggosegDataClass ktFuggosegKuldKuldesMod2NmhhExpMod, ExecParam execParam)
        {
            var ktListNmhhExpMod = KodTar_Cache.GetKodtarakByKodCsoportList(SZURManager.KCS_NMHH_EXPMOD, execParam, HttpContext.Current.Cache, null);
            var ktListKuldKuldesMod = KodTar_Cache.GetKodtarakByKodCsoportList(SZURManager.KCS_KULDEMENY_KULDES_MODJA, execParam, HttpContext.Current.Cache, null);

            var ktKuldesmod = ktListKuldKuldesMod.FirstOrDefault(e => e.Kod == kuldKuldesmodKod);
            if (ktKuldesmod == null) { return null; }

            // Kódtárfüggőségből kivesszük, hogy melyik kódtárelem(ek) tartozik hozzá:
            List<string> mappedKodtarIdList = null;
            if (ktFuggosegKuldKuldesMod2NmhhExpMod.Items != null)
            {
                mappedKodtarIdList = ktFuggosegKuldKuldesMod2NmhhExpMod.Items.Where(e => e.VezerloKodTarId == ktKuldesmod.Id).Select(e => e.FuggoKodtarId).ToList();
            }

            if (mappedKodtarIdList == null || mappedKodtarIdList.Count == 0)
            {
                return null;
            }
            else
            {
                var ktNmhhExpMod = ktListNmhhExpMod.FirstOrDefault(e => e.Id == mappedKodtarIdList[0]);
                return ktNmhhExpMod;
            }
        }

        /// <summary>
        /// Kódtárfüggőség lekérése a megadott vezérlő - függő kódcsoportokra.
        /// Ha nincs találat, Exceptiont dobunk.
        /// </summary>
        /// <returns></returns>
        public static KodtarFuggosegDataClass GetKodtarFuggoseg(string kcsKodVezerlo, string kcsKodFuggo, ExecParam execParam)
        {
            Contentum.eAdmin.Service.KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();

            KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
            search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", kcsKodVezerlo);
            search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
            search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", kcsKodFuggo);
            search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

            Result res = service.GetAll(execParam, search);
            res.CheckError();

            if (res.Ds.Tables[0].Rows.Count == 0)
            {
                throw new ResultException(String.Format("Kódtárfüggőség lekérése sikertelen, nincs találat. ({0} - {1} kódcsoportok)", kcsKodVezerlo, kcsKodFuggo));
            }
            else
            {
                string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();
                KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

                return fuggosegek;
            }
        }

        #endregion

        /// <summary>
        /// A megadott 'value' paraméter int formátumra alakítható-e (kötelezőséget nem ellenőriz, lehet üres string is)
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        public static void CheckIntFormat(string fieldName, string value)
        {
            if (String.IsNullOrEmpty(value)) { return; }

            int intResult;
            if (!Int32.TryParse(value, out intResult))
            {
                throw new Exception("A '" + fieldName + "' mező formátuma nem megfelelő!");
            }
        }

        /// <summary>
        /// Ellenőrzés, hogy a megadott 'value' paraméter dátum formátumra alakítható-e (kötelezőséget nem ellenőriz, lehet üres string is)
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        public static void CheckDateFormat(string fieldName, string value)
        {
            if (String.IsNullOrEmpty(value)) { return; }

            DateTime parsedDate;
            if (!DateTime.TryParse(value, out parsedDate))
                throw new Exception("A '" + fieldName + "' mező formátuma nem megfelelő!");
        }

        /// <summary>
        /// Ellenőrzés, hogy a megadott 'value' paraméter helyes és nem üres dátum
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        public static void CheckRequiredDateFormat(string fieldName, string value)
        {
            if (string.IsNullOrEmpty(value)) { throw new Exception("A '" + fieldName + "' mező megadása kötelező !"); }

            DateTime parsedDate;
            if (!DateTime.TryParse(value, out parsedDate))
                throw new Exception("A '" + fieldName + "' mező formátuma nem megfelelő!");
        }

        /// <summary>
        /// Ellenőrzés, hogy a megadott 'value' paraméter helyes és nem üres dátum
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        public static void CheckDateGreatertahnToday(string fieldName, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new Exception("A '" + fieldName + "' mező megadása kötelező !");
            }

            DateTime parsedDate;
            if (!DateTime.TryParse(value, out parsedDate))
                throw new Exception("A '" + fieldName + "' mező formátuma nem megfelelő !");

            if (parsedDate < DateTime.Today)
                throw new Exception("A '" + fieldName + "' mező nem lehet a mai dátumnál kisebb !");
        }

        /// <summary>
        /// Ellenőrzés, hogy a megadott kódtárkód létezik-e a megadott kódcsoportban
        /// (Ha nem, Exception-t dobunk.)
        /// </summary>
        /// <param name="kodcsoportKod"></param>
        /// <param name="kodtarKod"></param>
        public static void CheckKodtarKodExists(string kodcsoportKod, string kodtarKod, ExecParam execParam)
        {
            List<KodTar_Cache.KodTarElem> kodtarakList = KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoportKod, execParam, HttpContext.Current.Cache, null);

            if (!kodtarakList.Any(kt => kt.Kod == kodtarKod))
            {
                throw new Exception(String.Format("Nem található ilyen kódú kódtárelem: '{0}' (Kódcsoport: '{1}')!", kodtarKod, kodcsoportKod));
            }
        }

        public enum EnumXpmFelhasznaloSzervezetTipus
        {
            Iktatokonyv,
            Irattar,
            Irattaros,
            ElektronikusIrattar,
            EgyebSzervezetiIrattar,
            AtmenetiIrattar,
            Elintezett
        }

        public static string KulsoIktatoKonyv = "K";

        public enum EnumIgenNem
        {
            Igen,
            Nem
        }

        public static readonly string SzamitasModjaMunkanap = "munkanap";
        public static readonly string SzamitasModjaNap = "naptári nap";

        public static string GetInputStream()
        {
            byte[] inputStream = new byte[HttpContext.Current.Request.ContentLength];
            //Get current stream position so we can set it back to that after logging 
            Int64 currentStreamPosition = HttpContext.Current.Request.InputStream.Position;
            HttpContext.Current.Request.InputStream.Position = 0;
            HttpContext.Current.Request.InputStream.Read(inputStream, 0, HttpContext.Current.Request.ContentLength);
            //Set back stream position to original position 
            HttpContext.Current.Request.InputStream.Position = currentStreamPosition;
            string xml = System.Text.Encoding.UTF8.GetString(inputStream);
            return xml;
        }
    }

    public class InvalidUserException : Exception
    {
        public InvalidUserException() : base("Érvénytelen UserID")
        {
        }
    }
}