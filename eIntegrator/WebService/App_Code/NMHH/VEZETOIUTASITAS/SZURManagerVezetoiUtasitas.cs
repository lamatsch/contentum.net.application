﻿using System;
using Contentum.eUtility;
using System.Data;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Text;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for SZURManagerIrat
/// </summary>
namespace Contentum.eIntegrator.WebService.NMHH
{
    /// <summary>
    /// SZURMAnager - VezetoiUtasitas
    /// </summary>
    public partial class SZURManager
    {
        /// <summary>
        /// SZURGetVezetoiUtasitasWS
        /// </summary>
        /// <param name="Evszam"></param>
        /// <param name="Konyv"></param>
        /// <param name="Foszam"></param>
        /// <param name="Alszam"></param>
        /// <param name="UserId"></param>
        /// <param name="Kezeles"></param>
        /// <param name="Hibakod"></param>
        /// <param name="Hibaszoveg"></param>
        public void SZURGetVezetoiUtasitasWS(string Evszam, string Konyv, string Foszam, string Alszam, string UserId, out string FeladatMeghatarozas, out string Kezeles)
        {
            FeladatMeghatarozas = string.Empty;
            Kezeles = string.Empty;

            var execParamUserId = this.CreateExecParamByNMHHUserId(UserId);

            string contentumId;
            // objType: EREC_UgyUgyiratok vagy EREC_IraIratok
            string objType;
            if (string.IsNullOrEmpty(Alszam)
                || Alszam == "0")
            {
                if (!string.IsNullOrEmpty(Evszam)
                    && !string.IsNullOrEmpty(Konyv)
                    && !string.IsNullOrEmpty(Foszam))
                {
                    DataRow ugyiratRow = this.GetUgyiratRow(Evszam, Konyv, Foszam, execParamUserId);
                    contentumId = ugyiratRow["Id"].ToString();
                    objType = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                }
                else
                    throw new ResultException("Nincs megadva minden adat az ügyirat azonosításához!");
            }
            else
            {
                if (!string.IsNullOrEmpty(Evszam)
                    && !string.IsNullOrEmpty(Konyv)
                    && !string.IsNullOrEmpty(Foszam)
                    && !string.IsNullOrEmpty(Alszam))
                {
                    DataRow iratRow = this.GetIratRow(Evszam, Konyv, Foszam, Alszam, execParamUserId);
                    contentumId = iratRow["Id"].ToString();
                    objType = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;
                }
                else
                    throw new ResultException("Nincs megadva minden adat az irat azonosításához!");
            }

            var hatFeladatService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
            EREC_HataridosFeladatokSearch src = new EREC_HataridosFeladatokSearch();
            src.Obj_Id.Value = contentumId;
            src.Obj_Id.Operator = Query.Operators.equals;

            // A hierarchikus lekérdezést be kell állítani: (hogy ügyiratnál is iratnál az alatta lévő objektumok tételei is jöjjenek) (BUG#5711)
            src.UseUgyiratHierarchy = true;
            src.Obj_type.Value = objType;
            src.Obj_type.Operator = Query.Operators.equals;

            src.Tipus.Value = KodTarak.FELADAT_TIPUS.Feladat;
            src.Tipus.Operator = Query.Operators.equals;
            // BUG#5711: mindenki láthat minden adatot, így nem kell szűrni jogosultságra:
            //src.Jogosultak = true;

            src.OrderBy = " EREC_HataridosFeladatok.LetrehozasIdo DESC";

            var resultHatFeladat = hatFeladatService.GetAllWithExtension(execParamUserId, src);
            resultHatFeladat.CheckError();

            if (resultHatFeladat.Ds.Tables[0].Rows.Count > 0)
            {
                // Kezeléstípusonként feljegyzések listája:                
                Dictionary<string, List<string>> kezelesTipusokFeljegyzesei = new Dictionary<string, List<string>>();

                foreach(DataRow row in resultHatFeladat.Ds.Tables[0].Rows)
                {
                    string kezelesTipus = row["KezelesTipusNev"].ToString();
                    string leiras = row["Leiras"].ToString();

                    if (!kezelesTipusokFeljegyzesei.ContainsKey(kezelesTipus))
                    {
                        kezelesTipusokFeljegyzesei.Add(kezelesTipus, new List<string>());
                    }

                    // Leírás felvétele a listába:
                    kezelesTipusokFeljegyzesei[kezelesTipus].Add(leiras);
                }

                // Rendezés kezeléstípusra, de ha van benne üres elem, az kerüljön a végére:
                kezelesTipusokFeljegyzesei = kezelesTipusokFeljegyzesei.OrderBy(kvp => String.IsNullOrEmpty(kvp.Key) ? "zzz" : kvp.Key)
                                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

                Kezeles = String.Join(";", kezelesTipusokFeljegyzesei.Select(kvp => kvp.Key).ToArray());
                // BUG#5711: "Azonos típuson belül létező több bejegyzés esetén az egyes bejegyzéseket egy-egy soremelés válassza el egymástól"
                FeladatMeghatarozas = String.Join(";", kezelesTipusokFeljegyzesei.Select(
                                            kvp => String.Join(Environment.NewLine, kvp.Value.ToArray())).ToArray());
            }
        }
    }
}