﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.KKSZB
{
    public static class KKSZBHeaders
    {
        public const string RequestId = "x-request-id";
        public const string AuthenticationToken = "x-kk-authentication";
    }
}