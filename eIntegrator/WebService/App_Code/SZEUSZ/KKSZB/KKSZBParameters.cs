﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.KKSZB
{
    public class KKSZBParameters
    {
        public string RequestId { get; set; }
        public string AuthenticationToken { get; set; }
    }
}