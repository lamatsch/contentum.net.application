﻿namespace Contentum.eIntegrator.WebService.NAP.Config
{
    public class AppKeys
    {
        public const string NAPLekerdezesService_URL = "NAPLekerdezesService_URL";
        public const string NAPLekerdezesService_Mode = "NAPLekerdezesService_Mode";
        public const string NAPLekerdezesService_KKSZBAuthToken = "NAPLekerdezesService_KKSZBAuthToken";
    }
}