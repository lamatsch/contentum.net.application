﻿using System.Configuration;

namespace Contentum.eIntegrator.WebService.NAP.Config
{
    public class AppSettings
    {
        public static string NAPLekerdezesServiceURL { get; private set; }

        public static string NAPLekerdezesServiceMode { get; private set; }

        public static string NAPLekerdezesServiceKKSZBAutToken { get; private set; }

        static AppSettings()
        {
            NAPLekerdezesServiceURL = ConfigurationManager.AppSettings[AppKeys.NAPLekerdezesService_URL];
            NAPLekerdezesServiceMode = ConfigurationManager.AppSettings[AppKeys.NAPLekerdezesService_Mode];
            NAPLekerdezesServiceKKSZBAutToken = ConfigurationManager.AppSettings[AppKeys.NAPLekerdezesService_KKSZBAuthToken];
        }
    }
}