﻿using Contentum.eIntegrator.WebService.KKSZB;

namespace Contentum.eIntegrator.WebService.NAP.Config
{
    public class ConfigManager
    {
        public static KKSZBParameters GetKSZBParameters()
        {
            KKSZBParameters parameters = new KKSZBParameters();
            parameters.AuthenticationToken = AppSettings.NAPLekerdezesServiceKKSZBAutToken;
            return parameters;
        }

        public static ServiceParameters GetServiceParameters()
        {
            ServiceParameters serviceParameters = new ServiceParameters();
            serviceParameters.Url = AppSettings.NAPLekerdezesServiceURL;
            serviceParameters.Mode = "KKSZB".Equals(AppSettings.NAPLekerdezesServiceMode) ? ServiceMode.KKSZB : ServiceMode.WS;
            return serviceParameters;
        }

        public static LekerdezesParameters GetLekerdezesParameters()
        {
            LekerdezesParameters lekerdezesParameters = new LekerdezesParameters();
            lekerdezesParameters.SzervKod = INT_Parameterek.NAP_Lekerdezes_SzervKod;
            lekerdezesParameters.Jogalap = INT_Parameterek.NAP_Lekerdezes_Jogalap;
            lekerdezesParameters.Adatigenylonev = INT_Parameterek.NAP_Lekerdezes_Adatigenylonev;
            lekerdezesParameters.SzakrendszerHash = INT_Parameterek.NAP_Lekerdezes_SzakrendszerHash;
            return lekerdezesParameters;
        }

        public static LekerdezesParameters GetTesztLekerdezesParameters()
        {
            LekerdezesParameters lekerdezesParameters = new LekerdezesParameters
            {
                Jogalap = "JKAP23",
                SzervKod = "BUDAORS",
                Adatigenylonev = "BUDAORS",
                SzakrendszerHash = "39a3bbb27bd0cd249062efd0c68cebdb"
            };

            return lekerdezesParameters;
        }
    }
}