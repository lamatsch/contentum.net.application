﻿namespace Contentum.eIntegrator.WebService.NAP.Config
{
    public class INT_ParameterNevek
    {
        public const string NAP_Lekerdezes_SzervKod = "NAP.Lekerdezes.SzervKod";
        public const string NAP_Lekerdezes_Jogalap = "NAP.Lekerdezes.Jogalap";
        public const string NAP_Lekerdezes_Adatigenylonev = "NAP.Lekerdezes.Adatigenylonev";
        public const string NAP_Lekerdezes_SzakrendszerHash = "NAP.Lekerdezes.SzakrendszerHash";
    }
}