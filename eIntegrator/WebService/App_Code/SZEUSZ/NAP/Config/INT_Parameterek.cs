﻿namespace Contentum.eIntegrator.WebService.NAP.Config
{
    public class INT_Parameterek
    {
        public static string NAP_Lekerdezes_SzervKod
        {
            get
            {
                return IntegratorParameterek.GetParameter(INT_ParameterNevek.NAP_Lekerdezes_SzervKod);
            }
        }

        public static string NAP_Lekerdezes_Jogalap
        {
            get
            {
                return IntegratorParameterek.GetParameter(INT_ParameterNevek.NAP_Lekerdezes_Jogalap);
            }
        }

        public static string NAP_Lekerdezes_Adatigenylonev
        {
            get
            {
                return IntegratorParameterek.GetParameter(INT_ParameterNevek.NAP_Lekerdezes_Adatigenylonev);
            }
        }

        public static string NAP_Lekerdezes_SzakrendszerHash
        {
            get
            {
                return IntegratorParameterek.GetParameter(INT_ParameterNevek.NAP_Lekerdezes_SzakrendszerHash);
            }
        }
    }
}