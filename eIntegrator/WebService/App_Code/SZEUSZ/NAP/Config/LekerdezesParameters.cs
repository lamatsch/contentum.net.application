﻿namespace Contentum.eIntegrator.WebService.NAP.Config
{
    public class LekerdezesParameters
    {
        public string SzervKod { get; set; }
        public string Jogalap { get; set; }
        public string Adatigenylonev { get; set; }

        public string SzakrendszerHash { get; set; }
    }
}