﻿namespace Contentum.eIntegrator.WebService.NAP.Config
{
    public class ServiceParameters
    {
        public string Url { get; set; }
        public ServiceMode Mode { get; set; }

    }
}