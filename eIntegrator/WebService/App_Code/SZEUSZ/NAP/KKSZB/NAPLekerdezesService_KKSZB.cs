﻿using Contentum.eIntegrator.WebService.KKSZB;
using Contentum.eIntegrator.WebService.NAP.ServiceReference;
using System;
using System.Net;
using System.Text;

namespace Contentum.eIntegrator.WebService.NAP.KKSZB
{
    public class NAPLekerdezesService_KKSZB: NAPLekerdezesService
    {
        KKSZBParameters parameters { get; set; }

        public NAPLekerdezesService_KKSZB(string url, KKSZBParameters kKSZBParameters) : base(url)
        {
            this.parameters = kKSZBParameters;
        }

        protected override WebRequest GetWebRequest(Uri uri)
        {
            HttpWebRequest request;
            request = (HttpWebRequest)base.GetWebRequest(uri);

            if (PreAuthenticate)
            {
                NetworkCredential networkCredentials =
                    Credentials.GetCredential(uri, "Basic");

                if (networkCredentials != null)
                {
                    byte[] credentialBuffer = new UTF8Encoding().GetBytes(
                        networkCredentials.UserName + ":" +
                        networkCredentials.Password);
                    request.Headers["Authorization"] =
                                    "Basic " + Convert.ToBase64String(credentialBuffer);
                }
                else
                {
                    throw new ApplicationException("No network credentials");
                }
            }

            parameters.RequestId = Guid.NewGuid().ToString();
            request.Headers.Add(KKSZBHeaders.RequestId, parameters.RequestId);
            request.Headers.Add(KKSZBHeaders.AuthenticationToken, parameters.AuthenticationToken);

            return request;
        }
    }
}