﻿namespace Contentum.eIntegrator.WebService.NAP.Kodszotarak
{
    public static class Celok
    {
        //[KAÜ(UKAPU)] Ügyfél azonosítását követően elektronikus ügyintézési oldalra érkeztetése céljából.
        public const string CK001 = "CK001";
        //[KAÜ(UKAPU)] Ügyfél elektronikus ügyintézésre tett rendelkezésének ellenőrzése céljából.
        public const string CK002 = "CK002";
    }
}