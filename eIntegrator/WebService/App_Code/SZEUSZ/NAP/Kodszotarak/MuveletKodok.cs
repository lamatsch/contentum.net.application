﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.NAP.Kodszotarak
{
    public static class MuveletKodok
    {
        //TKK alapján történő lekérdezés műveletkódja
        public const string Lekerdezes_TKK_Alapajan = "L001";
        //4T alapján történő lekérdezés műveletkódja
        public const string Lekerdezes_4T_Alapjan = "L002";
        //Okmány vagy szakrendszeri azonosító alapján történő lekérdezés műveletkódja
        public const string Lekerdezes_OkmanyAzonosito_Alapjan = "L003";
    }
}