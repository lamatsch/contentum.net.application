﻿namespace Contentum.eIntegrator.WebService.NAP.Kodszotarak
{
    public class RendelkezesLekerdezesTipusok
    {
        public const string NamespaceAlapu = "RL01";
        public const string RendelkezesAzonositoAlapu = "RL02";
        public const string AliasAlapu = "RL03";
        public const string KatalogusKodAlapu = "RL04";
        public const string OsszesRendelkezes = "RL05";
    }
}