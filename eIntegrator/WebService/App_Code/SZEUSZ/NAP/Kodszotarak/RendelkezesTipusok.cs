﻿namespace Contentum.eIntegrator.WebService.NAP.Kodszotarak
{
    public class RendelkezesTipusok
    {
        public const string AlapRendelkezes = "urn:eksz.gov.hu:1.0:rendelkezes:alap";
        public const string AltalanosMeghatalmazas = "urn:eksz.gov.hu:1.0:rendelkezes:meghatalmazas:altalanos";
        public const string SzabadSzovegesMeghatalmazas = "urn:eksz.gov.hu:1.0:rendelkezes:meghatalmazas:szabadszoveges";
        public const string AltalanosRendelkezes = "urn:eksz.gov.hu:1.0:rendelkezes:PTK616altalanos";
    }
}