﻿using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using Contentum.eIntegrator.WebService.NAP.Config;
using Contentum.eIntegrator.WebService.NAP.Kodszotarak;
using Contentum.eIntegrator.WebService.NAP.ServiceReference;
using Contentum.eIntegrator.WebService.NAP.ServiceReference.S001;
using Contentum.eIntegrator.WebService.NAP.Utils;
using System;
using System.Collections.Generic;

namespace Contentum.eIntegrator.WebService.NAP
{
    public class NAPLekerdezesCreator
    {
        const string NincsMegadvaFelhasznalo = "Nincs megadva";
        LekerdezesParameters lekerdezesParameters;
        public NAPLekerdezesCreator(LekerdezesParameters lekerdezesParameters)
        {
            this.lekerdezesParameters = lekerdezesParameters;
        }

        public NAPLekerdezesKeres GetLekerdezesKeres()
        {
            NAPLekerdezesKeres keres = new NAPLekerdezesKeres
            {
                Fejlec = GetFejlec()
            };

            return keres;
        }

        public s001 GetRendelkezesLekerdezes(Contentum.eIntegrator.Service.NAP.BusinessObjects.AzonositoAdat azonositoAdat, string felhasznalo)
        {
            string muveletKod = MuveletKodok.Lekerdezes_4T_Alapjan;

            if (azonositoAdat.SzemelyAdatok == null && !String.IsNullOrEmpty(azonositoAdat.TitkosKapcsolatiKod))
                muveletKod = MuveletKodok.Lekerdezes_TKK_Alapajan;

            return new s001
            {
                Adatigenylo = GetAdatIgenylo(muveletKod,
                                            felhasznalo,
                                            lekerdezesParameters.Jogalap,
                                            Celok.CK001,
                                            lekerdezesParameters.Adatigenylonev)
            };
        }

        public void AddKertRendelkezesAdatok(s001 s001Lekerdezes, string lekerdezesTipus, Szereplo szereplo, string rendelkezesTipus)
        {
            if (s001Lekerdezes.KertAdatok == null)
                s001Lekerdezes.KertAdatok = new KertAdatokKertAdat[0];

            List<KertAdatokKertAdat> kertAdatokList = new List<KertAdatokKertAdat>(s001Lekerdezes.KertAdatok);

            KertAdatokKertAdat kertRendelkezesAdatok = GetRendelkezes(lekerdezesTipus, szereplo, rendelkezesTipus);
            kertAdatokList.Add(kertRendelkezesAdatok);

            s001Lekerdezes.KertAdatok = kertAdatokList.ToArray();
        }

        public void SetMivelKerdezSzemelyAdat(s001 s001Lekerdezes, Szemely4TAdatok szemely4TAdatok)
        {
            s001Lekerdezes.MivelKerdez = new MivelKerdez
            {
                SzemelyAdat = GetMivelKerdezSzemelyAdatok(szemely4TAdatok)
            };
        }

        public void SetMivelKerdezTitkosKapcsolatikod(s001 s001Lekerdezes, string titkoskapcsolatiKod)
        {
            s001Lekerdezes.MivelKerdez = new MivelKerdez
            {
                TitkosKapcsolatiKod = new MivelKerdezTitkosKapcsolatiKod
                {
                    SzakrendszerKod = SzakrendszerKodok.Ugyfelkapu,
                    Value = titkoskapcsolatiKod
                }
            };
        }

        public void AddLekerdezesToKeres(NAPLekerdezesKeres keres, s001 s001Lekerdezes)
        {
            keres.Lekerdezes = new NAPLekerdezesKeresLekerdezes()
            {
                Any = new System.Xml.XmlElement[] { XMLHelper.SerializeToXmlElement(s001Lekerdezes) }
            };
        }

        public void CheckAzonositoAdat(Contentum.eIntegrator.Service.NAP.BusinessObjects.AzonositoAdat azonositoAdat)
        {
            if (azonositoAdat == null)
                throw new Exception("Nincs megadva azonosító adat");

            if(azonositoAdat.SzemelyAdatok == null && String.IsNullOrEmpty(azonositoAdat.TitkosKapcsolatiKod))
                throw new Exception("Nincs megadva azonosító adat");
        }

        Fejlec GetFejlec()
        {
            return new Fejlec
            {
                TranzakcioKod = Guid.NewGuid().ToString(),
                Ido = DateTime.Now,
                IdoSpecified = true,
                Szervkod = lekerdezesParameters.SzervKod
            };
        } 

        Adatigenylo GetAdatIgenylo(string muveletKod, string felhasznalo, string jogalap, string cel, string adatigenylonev)
        {
            return new Adatigenylo
            {
                MuveletKod = muveletKod,
                Felhasznalo = String.IsNullOrEmpty(felhasznalo) ? NincsMegadvaFelhasznalo : felhasznalo.PadRight(6,'_'),
                Jogalap = jogalap,
                Cel = cel,
                Adatigenylonev = adatigenylonev
            };
        }

        KertAdatokKertAdat GetRendelkezes(string lekerdezesTipus, Szereplo szereplo, string rendelkezesTipus)
        {
            return new KertAdatokKertAdat
            {
                azonosito = KertAdatok.RENDELKEZES,
                RendelkezesLekeredezes = new KertAdatokKertAdatRendelkezesLekeredezes
                {
                    szereplo = szereplo,
                    szereploSpecified = true,
                    szakrendszerHash = lekerdezesParameters.SzakrendszerHash,
                    lekerdezesTipus = lekerdezesTipus,
                    tipus = rendelkezesTipus
                }
            };
        }

        MivelKerdezSzemelyAdat GetMivelKerdezSzemelyAdatok(Szemely4TAdatok szemely4TAdatok)
        {
            return new MivelKerdezSzemelyAdat
            {
                Nevek = new MivelKerdezSzemelyAdatNevek
                {
                    ViseltNev = new MivelKerdezSzemelyAdatNevekViseltNev
                    {
                        TotalNev = szemely4TAdatok.Nev
                    },
                    SzuletesiNev = new MivelKerdezSzemelyAdatNevekSzuletesiNev
                    {
                        TotalNev = szemely4TAdatok.SzuletesiNev
                    }
                },
                Anyjaneve = new MivelKerdezSzemelyAdatAnyjaneve
                {
                      BontottNev = new MivelKerdezSzemelyAdatAnyjaneveBontottNev
                      {
                          VezNev = szemely4TAdatok.AnyjaNeve.VezNev,
                          UtoNev = szemely4TAdatok.AnyjaNeve.UtoNev
                      }
                },
                SzuletesiIdo = new MivelKerdezSzemelyAdatSzuletesiIdo
                {
                    SzulDatum = szemely4TAdatok.SzuletesiIdo
                },
                SzuletesiHely = new MivelKerdezSzemelyAdatSzuletesiHely
                {
                    Szulhely1 = szemely4TAdatok.SzuletesiHely
                }
            };
        }

        MivelKerdezTitkosKapcsolatiKod GetMivelKerdezTitkosKapcsolatiKod(string szakrendszerKod, string titkosKapcsolatiKod)
        {
            return new MivelKerdezTitkosKapcsolatiKod
            {
                SzakrendszerKod = szakrendszerKod,
                Value = titkosKapcsolatiKod
            };
        }

    }
}