﻿using Contentum.eIntegrator.WebService.KKSZB;
using Contentum.eIntegrator.WebService.NAP.Config;
using Contentum.eIntegrator.WebService.NAP.KKSZB;
using Contentum.eIntegrator.WebService.NAP.ServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.NAP
{
    public class NAPLekerdezesServiceFactory
    {
        public static NAPLekerdezesService GetONYPLekerdezesService(ServiceParameters serviceParameters)
        {
            if (serviceParameters.Mode == ServiceMode.KKSZB)
            {
                KKSZBParameters kKSZBParameters = ConfigManager.GetKSZBParameters();
                return new NAPLekerdezesService_KKSZB(serviceParameters.Url, kKSZBParameters);
            }

            return new NAPLekerdezesService(serviceParameters.Url);
        }
    }
}