﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.NAP.ServiceReference;
using S001 = Contentum.eIntegrator.WebService.NAP.ServiceReference.S001;
using Contentum.eIntegrator.WebService.NAP.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using Contentum.eUtility;
using Contentum.eIntegrator.WebService.NAP.Rendelkezesek;

namespace Contentum.eIntegrator.WebService.NAP
{
    public class NAPLekerdezesValaszParser
    {
        NAPLekerdezesValasz nAPLekerdezesValasz;
        public NAPLekerdezesValaszParser(NAPLekerdezesValasz nAPLekerdezesValasz)
        {
            this.nAPLekerdezesValasz = nAPLekerdezesValasz;
        }

        public T GetResult<T>() where T : Result, new()
        {
            T res = new T();
            var uzenetek = nAPLekerdezesValasz.Uzenetek;
            if (uzenetek != null)
            {
                StringBuilder errorMessage = new StringBuilder();
                StringBuilder errorCode = new StringBuilder();
                foreach (ServiceReference.Uzenet uzenet in uzenetek)
                {
                    errorMessage.Append(uzenet.Leiras);
                    errorMessage.Append(",");
                    errorCode.Append(uzenet.Kod);
                    errorCode.Append(",");
                }
                res.ErrorMessage = errorMessage.ToString();
                res.ErrorCode = errorCode.ToString();
            }

            return res;
        }

        public static T GetResultFromException<T>(Exception ex) where T : Result, new()
        {
            T res = new T();
            Result error = ResultException.GetResultFromException(ex);
            res.ErrorCode = error.ErrorCode;
            res.ErrorMessage = error.ErrorMessage;
            return res;
        }

        public List<Meghatalmazas> GetMeghatalmazasok()
        {
            S001.s001Valasz valasz = this.GetValasz();

            List<Meghatalmazas> meghatalmazasok = new List<Meghatalmazas>();
            foreach (S001.Rendelkezes s001Rendelkezes in valasz.Rendelkezesek)
            {
                Meghatalmazas Meghatalmazas = GetMeghatalmazas(s001Rendelkezes);
                meghatalmazasok.Add(Meghatalmazas);
            }

            meghatalmazasok = meghatalmazasok.OrderByDescending(m => m.HatalyKezdete).ToList();

            return meghatalmazasok;
        }

        public List<KRT_Cimek> GetElerhetosegek()
        {
            S001.s001Valasz valasz = this.GetValasz();

            List<KRT_Cimek> cimek = new List<KRT_Cimek>();
            RendelkezesSerializer rendelkezesSerializer = new RendelkezesSerializer();
            foreach (S001.Rendelkezes s001Rendelkezes in valasz.Rendelkezesek)
            {
                alapRendelkezes alapRendelkezes = rendelkezesSerializer.DeserializeAlapRendelkezes(s001Rendelkezes.rendelkezesXML);
                if (alapRendelkezes != null)
                    cimek.AddRange(GetElerhetosegek(alapRendelkezes));
            }

            return cimek;
        }

        S001.s001Valasz GetValasz()
        {
            if (nAPLekerdezesValasz.Valasz == null)
                throw new Exception("Valasz elem null!");

            if (nAPLekerdezesValasz.Valasz.Any == null)
                throw new Exception("Valasz.Any elem null!");

            if (nAPLekerdezesValasz.Valasz.Any.Length == 0)
                throw new Exception("Valasz.Any elem üres!");

            S001.s001Valasz valasz = XMLHelper.DeserializeFromXmlElement<S001.s001Valasz>(nAPLekerdezesValasz.Valasz.Any[0]);
            return valasz;
        }

        Meghatalmazas GetMeghatalmazas(S001.Rendelkezes s001Rendelkezes)
        {
            Meghatalmazas meghatalmazas = new Meghatalmazas();
            meghatalmazas.Id = s001Rendelkezes.rendelkezesId;
            meghatalmazas.Azonosito = s001Rendelkezes.rendelkezesAzonosito;
            meghatalmazas.Tipus = s001Rendelkezes.rendelkezesNamespace;
            meghatalmazas.TipusNev = Converter.ConvertToRendelkezesTipusNev(s001Rendelkezes.rendelkezesNamespace);
            meghatalmazas.RendelkezesXML = s001Rendelkezes.rendelkezesXML;

            TextCreator textCreator = new TextCreator();
            if (s001Rendelkezes.ki != null)
            {
                meghatalmazas.Ki = textCreator.GetText(s001Rendelkezes.ki);
            }

            if(s001Rendelkezes.kire != null)
                meghatalmazas.Kire = textCreator.GetText(s001Rendelkezes.kire);

            if (s001Rendelkezes.kit!= null)
                meghatalmazas.Kit = textCreator.GetText(s001Rendelkezes.kit);

            meghatalmazas.HatalyKezdete = s001Rendelkezes.ervenyessegKezdete;

            if (s001Rendelkezes.ervenyessegVegeSpecified)
                meghatalmazas.HatalyVege = s001Rendelkezes.ervenyessegVege;
            if (s001Rendelkezes.visszavonasDatumaSpecified)
                meghatalmazas.HatalyVege = s001Rendelkezes.visszavonasDatuma;

            RendelkezesSerializer rendelkezesSerializer = new RendelkezesSerializer();
            object rendelkezesObj = rendelkezesSerializer.DeserializeRendelkezes(s001Rendelkezes.rendelkezesNamespace, s001Rendelkezes.rendelkezesXML);

            if (rendelkezesObj != null)
            {
                meghatalmazas.RendelkezesText = rendelkezesObj.ToString();
            }

            return meghatalmazas;
        }

        List<KRT_Cimek> GetElerhetosegek(alapRendelkezes alapRendelkezes)
        {
            List<KRT_Cimek> cimek = new List<KRT_Cimek>();

            if (alapRendelkezes != null)
            {
                if (alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek != null && !String.IsNullOrEmpty(alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek.emailCim))
                {
                    KRT_Cimek cim = new KRT_Cimek();
                    cim.Forras = KodTarak.Cim_Forras.RNY;
                    cim.Tipus = KodTarak.Cim_Tipus.Email_Cim_RNY;
                    cim.CimTobbi = alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek.emailCim;
                    cimek.Add(cim);
                }
                if (alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek != null && !String.IsNullOrEmpty(alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek.elsodlegesTelefon))
                {
                    KRT_Cimek cim = new KRT_Cimek();
                    cim.Forras = KodTarak.Cim_Forras.RNY;
                    cim.Tipus = KodTarak.Cim_Tipus.Telefonszam_RNY;
                    cim.CimTobbi = alapRendelkezes.tajekoztatasiCeluEgyebKapcsolattartasiCimek.elsodlegesTelefon;
                    cimek.Add(cim);
                }
                /*A naphoz tartozó 3.0 ás xsd ben az alaprendelkezésben ezek a mezők már nem találhatóak meg
                {
                if (alapRendelkezes.postaiElerhetoseg != null)
                {
                    KRT_Cimek cim = new KRT_Cimek();
                    cim.Forras = KodTarak.Cim_Forras.RNY;
                    cim.Tipus = KodTarak.Cim_Tipus.Postai_Cim_RNY;
                    cim.IRSZ = alapRendelkezes.postaiElerhetoseg.irsz;
                    cim.TelepulesNev = alapRendelkezes.postaiElerhetoseg.varos;
                    cim.KozteruletNev = alapRendelkezes.postaiElerhetoseg.kozteruletNeve;
                    cim.KozteruletTipusNev = alapRendelkezes.postaiElerhetoseg.kozteruletJellege;
                    cim.Hazszam = alapRendelkezes.postaiElerhetoseg.hazszam;
                    cim.Szint = alapRendelkezes.postaiElerhetoseg.emelet;
                    cim.Ajto = alapRendelkezes.postaiElerhetoseg.ajto;
                    cim.HRSZ = alapRendelkezes.postaiElerhetoseg.hrsz;
                    cimek.Add(cim);
                }*/
            }

            return cimek;
        }
    }

}