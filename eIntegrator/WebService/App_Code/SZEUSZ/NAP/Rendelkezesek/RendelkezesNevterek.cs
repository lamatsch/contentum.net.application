﻿namespace Contentum.eIntegrator.WebService.NAP.Rendelkezesek
{
    public class RendelkezesNevterek
    {
        public const string AlapRendelkezes10 = "urn:eksz.gov.hu:1.0:rendelkezes:alap:3.0";
        public const string AltalanosMeghatalmazas10 = "urn:eksz.gov.hu:1.0:rendelkezes:meghatalmazas:altalanos:1.0";
        public const string SzabadSzovegesMeghatalmazas10 = "urn:eksz.gov.hu:1.0:rendelkezes:meghatalmazas:szabadszoveges:1.0";
    }
}