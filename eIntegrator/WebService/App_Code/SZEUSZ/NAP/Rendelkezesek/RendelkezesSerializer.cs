﻿using Contentum.eUtility;
using System;
using System.IO;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.NAP.Rendelkezesek
{
    /// <summary>
    /// Summary description for RendelkezesSerializer
    /// </summary>
    public class RendelkezesSerializer
    {
        public alapRendelkezes DeserializeAlapRendelkezes(string rendelkezesXML)
        {
            object o = DeserializeRendelkezes(RendelkezesNevterek.AlapRendelkezes10, rendelkezesXML);

            if (o == null)
                return null;

            return o as alapRendelkezes;
        }

        public object DeserializeRendelkezes(string rendelkezesNevter, string rendelkezesXML)
        {
            try
            {
                Type type = GetRendelkezesObjectType(rendelkezesNevter);

                if (type == null)
                    return null;

                XmlSerializer serializer = new XmlSerializer(type);
                using (StringReader reader = new StringReader(rendelkezesXML))
                {
                    return serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Hibás xml:\n" + rendelkezesXML);
                Logger.Error("DeserializeRendelkezes", ex);
            }

            return null;
        }

        Type GetRendelkezesObjectType(string rendelkezesNevter)
        {
            if (rendelkezesNevter == RendelkezesNevterek.AlapRendelkezes10)
                return typeof(alapRendelkezes);

            if (rendelkezesNevter == RendelkezesNevterek.AltalanosMeghatalmazas10)
                return typeof(Warrant);

            if (rendelkezesNevter == RendelkezesNevterek.SzabadSzovegesMeghatalmazas10)
                return typeof(freeTextWarranty);

            return null;
        }
    }
}