﻿namespace Contentum.eIntegrator.WebService.NAP.Rendelkezesek
{
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "urn:eksz.gov.hu:1.0:rendelkezes:meghatalmazas:szabadszoveges:1.0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "urn:eksz.gov.hu:1.0:rendelkezes:meghatalmazas:szabadszoveges:1.0", IsNullable = false)]
    public partial class freeTextWarranty
    {
        public string megnevezes { get; set; }
        public string template { get; set; }
        public string rendelkezes { get; set; }

        public override string ToString()
        {
            return rendelkezes;
        }
    }
}