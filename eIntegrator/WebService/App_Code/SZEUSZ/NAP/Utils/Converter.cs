﻿using S001 = Contentum.eIntegrator.WebService.NAP.ServiceReference.S001;
using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using Contentum.eIntegrator.Service.NAP;

namespace Contentum.eIntegrator.WebService.NAP.Utils
{
    public class Converter
    {
        public static S001.Szereplo ConvertToSzereplo(SzereploTipusok szereploTipus)
        {
            if (szereploTipus == SzereploTipusok.Kit)
                return S001.Szereplo.kit;

            if (szereploTipus == SzereploTipusok.Kire)
                return S001.Szereplo.kire;

            return S001.Szereplo.kit;
        }

        public static string ConvertToRendelkezesTipus(MeghatalmazasTipusok meghatalmazasTipus)
        {
            if (meghatalmazasTipus == MeghatalmazasTipusok.AltalanosMeghatalmazas)
                return Kodszotarak.RendelkezesTipusok.AltalanosMeghatalmazas;

            if (meghatalmazasTipus == MeghatalmazasTipusok.SzabadSzovegesMeghatalmazas)
                return Kodszotarak.RendelkezesTipusok.SzabadSzovegesMeghatalmazas;

            if (meghatalmazasTipus == MeghatalmazasTipusok.AltalanosRendelkezes)
                return Kodszotarak.RendelkezesTipusok.AltalanosRendelkezes;

            return Kodszotarak.RendelkezesTipusok.AltalanosMeghatalmazas;

        }

        public static string ConvertToRendelkezesTipusNev(string rendelkezesTipus)
        {
            if (string.IsNullOrEmpty(rendelkezesTipus))
                return rendelkezesTipus;

            if (rendelkezesTipus.StartsWith(Kodszotarak.RendelkezesTipusok.AltalanosMeghatalmazas))
                return RendelkezesTipusNevek.AltalanosMeghatalmazas;

            if (rendelkezesTipus.StartsWith(Kodszotarak.RendelkezesTipusok.SzabadSzovegesMeghatalmazas))
                return RendelkezesTipusNevek.SzabadSzovegesMeghatalmazas;

            if (rendelkezesTipus.StartsWith(Kodszotarak.RendelkezesTipusok.AltalanosRendelkezes))
                return RendelkezesTipusNevek.AltalanosRendelkezes;

            return rendelkezesTipus;
        }
    }
}