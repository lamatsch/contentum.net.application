﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;

namespace Contentum.eIntegrator.WebService.NAP.Utils
{
    public class EdokWrapper
    {
        public static string GetFelhasznaloNev(ExecParam execParam)
        {
            if (String.IsNullOrEmpty(execParam.Felhasznalo_Id))
                return String.Empty;

            string felhasznaloId = execParam.Felhasznalo_Id;

            KRT_FelhasznalokService felhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            ExecParam felhasznaloGetExecParam = execParam.Clone();
            felhasznaloGetExecParam.Record_Id = felhasznaloId;

            Result felhasznalokResult = felhasznalokService.Get(felhasznaloGetExecParam);

            if (felhasznalokResult.IsError)
            {
                Logger.Error(String.Format("A felhasználó ({0}) lekérése sikertelen!", felhasznaloId), felhasznaloGetExecParam, felhasznalokResult);
                return String.Empty;
            }

            KRT_Felhasznalok felhasznalo = felhasznalokResult.Record as KRT_Felhasznalok;
            return felhasznalo.UserNev;
        }
    }
}