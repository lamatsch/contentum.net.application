﻿using Contentum.eIntegrator.WebService.NAP.ServiceReference;
using S001 = Contentum.eIntegrator.WebService.NAP.ServiceReference.S001;
using System.Collections.Generic;
using System;

namespace Contentum.eIntegrator.WebService.NAP.Utils
{
    public class TextCreator
    {
        public TextCreator()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string GetText(S001.KiAlanyTipus kiAlanyTipus)
        {
            if (kiAlanyTipus.termeszetesSzemelyAlany != null)
                return GetText(kiAlanyTipus.termeszetesSzemelyAlany);
            else if (kiAlanyTipus.rendszerAlany != null)
                return GetText(kiAlanyTipus.rendszerAlany);

            return String.Empty;
        }

        public string GetText(S001.RendelkezesSzereploTipus rendelkezesSzereploTipus)
        {
            if (rendelkezesSzereploTipus.alany == null)
                return String.Empty;

            return GetText(rendelkezesSzereploTipus.alany);
        }

        public string GetText(S001.RendelkezesSzereploTipus[] rendelkezesSzereploTipusArray)
        {
            List<string> nevList = new List<string>();

            foreach (S001.RendelkezesSzereploTipus rendelkezesSzereploTipus in rendelkezesSzereploTipusArray)
            {
                string text = GetText(rendelkezesSzereploTipus);
                if(!String.IsNullOrEmpty(text))
                    nevList.Add(text);
            }

            return String.Join(", ", nevList.ToArray());
        }

        string GetText(S001.TermeszetesSzemelyTipus termeszetesSzemelyTipus)
        {
            if (termeszetesSzemelyTipus.SzemelyAdat != null)
                return GetText(termeszetesSzemelyTipus.SzemelyAdat);

            if (termeszetesSzemelyTipus.AzonositoAdat != null)
                return GetText(termeszetesSzemelyTipus.AzonositoAdat);

            if (termeszetesSzemelyTipus.TitkosKapcsolatiKod != null)
                return GetText(termeszetesSzemelyTipus.TitkosKapcsolatiKod);

            return String.Empty;
        }

        string GetText(S001.RendszerTipus rendszerTipus)
        {
            return rendszerTipus.azonosito;
        }

        string GetText(S001.SzervezetTipus szervezetTipus)
        {
            return szervezetTipus.nev;
        }

        string GetText(S001.ValaszAlanyTipus valaszAlanyTipus)
        {
            if (valaszAlanyTipus.termeszetesSzemely != null)
                return GetText(valaszAlanyTipus.termeszetesSzemely);
            else if (valaszAlanyTipus.szervezet != null)
                return GetText(valaszAlanyTipus.szervezet);

            return String.Empty;
        }

        string GetText(S001.SzemelyAdatokTipus szemelyAdat)
        {
            S001.BontottNev bontottNev = szemelyAdat.Nevek.ViseltNev.BontottNev;
            if (bontottNev != null)
            {
                return String.Format("{0} {1}", bontottNev.VezNev, bontottNev.UtoNev);
            }
            else
            {
                return szemelyAdat.Nevek.ViseltNev.TotalNev;
            }
        }

        string GetText(S001.AzonositoAdat azonositoAdat)
        {
            return azonositoAdat.Azonosito;
        }

        string GetText(S001.TitkosKapcsolatiKod titkosKapcsolatiKod)
        {
            return titkosKapcsolatiKod.Value;
        }
    }
}