﻿using Contentum.eIntegrator.WebService.KKSZB;
using Contentum.eIntegrator.WebService.ONYP.KKSZB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.ONYP.Config
{
    /// <summary>
    /// Summary description for ConfigManager
    /// </summary>
    public static class ConfigManager
    {
        class AppKeys
        {
            public const string ONYPLekerdezesServiceURL = "ONYPLekerdezesServiceURL";
            public const string ONYPLekerdezesServiceMode = "ONYPLekerdezesServiceMode";
            public const string ONYPLekerdezesServiceKKSZBAutToken= "ONYPLekerdezesServiceKKSZBAutToken";
        }

        static string ONYPLekerdezesServiceURL;
        static string ONYPLekerdezesServiceMode;
        static string ONYPLekerdezesServiceKKSZBAutToken;

        static ConfigManager()
        {
            ONYPLekerdezesServiceURL = ConfigurationManager.AppSettings[AppKeys.ONYPLekerdezesServiceURL];
            ONYPLekerdezesServiceMode = ConfigurationManager.AppSettings[AppKeys.ONYPLekerdezesServiceMode];
            ONYPLekerdezesServiceKKSZBAutToken = ConfigurationManager.AppSettings[AppKeys.ONYPLekerdezesServiceKKSZBAutToken];
        }

        public static KKSZBParameters GetKSZBParameters()
        {
            KKSZBParameters parameters = new KKSZBParameters();
            parameters.AuthenticationToken = ONYPLekerdezesServiceKKSZBAutToken;
            return parameters;
        }

        public static ONYPLekerdezesServiceFactory.ServiceFactoryParameters GetServiceFactoryParameters()
        {
            ONYPLekerdezesServiceFactory.ServiceFactoryParameters parameters = new ONYPLekerdezesServiceFactory.ServiceFactoryParameters();
            parameters.ServiceUrl = ONYPLekerdezesServiceURL;
            parameters.ServiceMode = "KKSZB".Equals(ONYPLekerdezesServiceMode, StringComparison.InvariantCultureIgnoreCase) ? ONYPLekerdezesServiceFactory.ServiceMode.KKSZB : ONYPLekerdezesServiceFactory.ServiceMode.WS;
            return parameters;
        }
    }
}