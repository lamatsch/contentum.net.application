﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.ONYP
{
    /// <summary>
    /// Summary description for Constants
    /// </summary>
    public static class ONYPConstants
    {
        public static class MuveletKod
        {
            //L001	TKK alapján történő lekérdezés műveletkódja
            public const string L001 = "L001";
            //L002	4T alapján történő lekérdezés műveletkódja
            public const string L002 = "L002";
        }

        public static class Cel
        {
            //CK001 Technikai kód, rendelkezések lekérdezése
            public const string CK001 = "CK001";
            //CK002 Technikai kód, személy adatok lekérdezése
            public const string CK002 = "CK002";
        }

        public static class RendelkezesTipus
        {
            public const string Alaprendelkezes = "urn:eksz.gov.hu:1.0:rendelkezes:alap";
            public const string AltalanosMeghatalmazas = "urn:eksz.gov.hu:1.0:rendelkezes:meghatalmazas:altalanos";
        }

        public static class KertAdatAzonosito
        {
            public const string RendelkezesLekerdezes = "RENDELKEZES";
            public const string SzemelyAdatok = "4T";
        }

        public static class Rendszer
        {
            public const string UgyfelKapu = "UK";
        }
    }
}