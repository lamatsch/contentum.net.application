﻿using Contentum.eIntegrator.WebService.KKSZB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Contentum.eIntegrator.WebService.ONYP.KKSZB
{
    /// <summary>
    /// Summary description for ONYPLekerdezesService_KKSZB
    /// </summary>
    public class ONYPLekerdezesService_KKSZB : ONYPLekerdezesService
    {
        KKSZBParameters parameters { get; set; }

        public ONYPLekerdezesService_KKSZB(string url, KKSZBParameters kKSZBParameters): base(url)
        {
            this.parameters = kKSZBParameters;
        }

        protected override WebRequest GetWebRequest(Uri uri)
        {
            HttpWebRequest request;
            request = (HttpWebRequest)base.GetWebRequest(uri);

            if (PreAuthenticate)
            {
                NetworkCredential networkCredentials =
                    Credentials.GetCredential(uri, "Basic");

                if (networkCredentials != null)
                {
                    byte[] credentialBuffer = new UTF8Encoding().GetBytes(
                        networkCredentials.UserName + ":" +
                        networkCredentials.Password);
                    request.Headers["Authorization"] =
                                    "Basic " + Convert.ToBase64String(credentialBuffer);
                }
                else
                {
                    throw new ApplicationException("No network credentials");
                }
            }

            parameters.RequestId = Guid.NewGuid().ToString();
            request.Headers.Add(KKSZBHeaders.RequestId, parameters.RequestId);
            request.Headers.Add(KKSZBHeaders.AuthenticationToken, parameters.AuthenticationToken);

            return request;
        }
    }
}