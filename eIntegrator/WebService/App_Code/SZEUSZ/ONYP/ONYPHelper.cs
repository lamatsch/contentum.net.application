﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.ONYP.RendelkezesTipusok;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.ONYP
{
    /// <summary>
    /// Summary description for ONYPHelper
    /// </summary>
    public class ONYPHelper
    {
        static ServiceParameters ServiceParams = new ServiceParameters();
        public static Fejlec GetFejlec()
        {
            return new Fejlec
            {
                TranzakcioKod = Guid.NewGuid().ToString(),
                Ido = DateTime.Now,
                Szervkod = ServiceParams.Szervkod
            };
        }

        public static ONYPLekerdezesKeresLekerdezes GetLekerdezes(string muveletKod, string cel)
        {
            ONYPLekerdezesKeresLekerdezes lekerdezes = new ONYPLekerdezesKeresLekerdezes();
            lekerdezes.MuveletKod = muveletKod;
            lekerdezes.Felhasznalo = ServiceParams.Felhasznalo;
            lekerdezes.JogalapCelAdatigenylo = new IgenylesJogalap
            {
                Jogalap = ServiceParams.Jogalap,
                Cel = cel,
                Adatigenylo = ServiceParams.AdatIgenylo
            };

            return lekerdezes;
        }

        public static KertAdat GetKertAdat(string rendelkezesTipus)
        {
            return new KertAdat
            {
                azonosito = "RENDELKEZES",
                Item = new RendelkezesLekerdezes
                {
                    ItemElementName = ItemChoiceType.tipus,
                    Item = rendelkezesTipus
                }
            };
        }

        public static ONYPLekerdezesKeres GetKeres(string muveletKod, string cel, string rendelkezesTipus)
        {
            ONYPLekerdezesKeres keres = new ONYPLekerdezesKeres();
            keres.Fejlec = GetFejlec();
            keres.Lekerdezes = GetLekerdezes(muveletKod, cel);
            keres.Lekerdezes.KertAdat = new KertAdat[]
            {
                GetKertAdat(rendelkezesTipus)
            };
            return keres;
        }

        public static SzemelyAdatok GetSzemelyAdat(Service.ONYP.BusinessObjects.Szemely4TAdatok szemely4Tadatok)
        {
            SzemelyAdatok szemely = new SzemelyAdatok();
            szemely.Nevek = new Nevek
            {
                ViseltNev = new Nev
                {
                    Item = szemely4Tadatok.Nev
                },
                SzuletesiNev = new Nev
                {
                    Item = szemely4Tadatok.SzuletesiNev
                }
            };
            szemely.Anyaneve = new Nev
            {
                Item = szemely4Tadatok.AnyjaNeve
            };
            szemely.SzuletesiHely = new SzuletesiHely
            {
                Szulhely1 = szemely4Tadatok.SzuletesiHely
            };
            szemely.SzuletesiIdo = new SzuletesiDatum
            {
                Item = szemely4Tadatok.SzuletesiIdo,
                ItemElementName = ItemChoiceType1.SzulDatum
            };

            return szemely;
        }

        public static TitkosKapcsolatiKod GetTitkosKapcsolatiKod(string kod)
        {
            TitkosKapcsolatiKod titkosKapcsolatiKod = new TitkosKapcsolatiKod();
            titkosKapcsolatiKod.SzakrendszerKod = ONYPConstants.Rendszer.UgyfelKapu;
            titkosKapcsolatiKod.Value = kod;
            return titkosKapcsolatiKod;
        }

        public static alapRendelkezes DeserializeAlapRendelkezes(string xml)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(alapRendelkezes));
                using (StringReader reader = new StringReader(xml))
                {
                    return serializer.Deserialize(reader) as alapRendelkezes;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("DeserializeAlapRendelkezes", ex);
            }

            return null;
        }

        public static Warrant DeserializerMeghatalmazas(string xml)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Warrant));
                using (StringReader reader = new StringReader(xml))
                {
                    return serializer.Deserialize(reader) as Warrant;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("DeserializerMeghatalmazas", ex);
            }

            return null;
        }

        public static Result GetResult(ONYPLekerdezesValasz lekerdezesValasz)
        {
            Result res = new Result();
            ONYPLekerdezesValaszUzenetek uzenetek = lekerdezesValasz.Item as ONYPLekerdezesValaszUzenetek;
            if (uzenetek != null)
            {
                StringBuilder errorMessage = new StringBuilder();
                StringBuilder errorCode = new StringBuilder();
                foreach (Uzenet uzenet in uzenetek.Uzenet)
                {
                    errorMessage.Append(uzenet.Leiras);
                    errorMessage.Append(",");
                    errorCode.Append(uzenet.Kod);
                    errorCode.Append(",");
                }
                res.ErrorMessage = errorMessage.ToString();
                res.ErrorCode = errorCode.ToString();
            }

            return res;
        }

        public static string GetNev(KiAlanyTipus kiAlanyTipus)
        {
            object item = kiAlanyTipus.Item;
            if (item is RendszerTipus)
            {
                return GetNev(item as RendszerTipus);
            }
            else if (item is TermeszetesSzemelyTipus)
            {
                return GetNev(item as TermeszetesSzemelyTipus);
            }

            return String.Empty;
        }

        public static string GetNev(RendelkezesSzereploTipus rendelkezesSzereploTipus)
        {
            object item = rendelkezesSzereploTipus.alany.Item;

            if (item is SzervezetTipus)
            {
                return GetNev(item as SzervezetTipus);
            }
            else if (item is TermeszetesSzemelyTipus)
            {
                return GetNev(item as TermeszetesSzemelyTipus);
            }

            return String.Empty;
        }

        static string GetNev(RendszerTipus rendszerTipus)
        {
            return rendszerTipus.azonosito;
        }

        static string GetNev(SzervezetTipus szervezetTipus)
        {
            return szervezetTipus.nev;
        }

        static string GetNev(TermeszetesSzemelyTipus termeszetesSzemelyTipus)
        {
            object item = termeszetesSzemelyTipus.Item;
            if (item is AzonositoAdat)
            {
                return GetNev(item as AzonositoAdat);
            }
            else if (item is SzemelyAdatokTipus)
            {
                return GetNev(item as SzemelyAdatokTipus);
            }
            else if (item is TitkosKapcsolatiKod)
            {
                return GetNev(item as TitkosKapcsolatiKod);
            }

            return String.Empty;
        }

        static string GetNev(AzonositoAdat azonositoAdat)
        {
            return azonositoAdat.Azonosito;
        }

        static string GetNev(SzemelyAdatokTipus szemelyAdat)
        {
            BontottNev bontottNev = szemelyAdat.Nevek.ViseltNev.Item as BontottNev;
            if (bontottNev != null)
            {
                return String.Format("{0} {1}", bontottNev.VezNev, bontottNev.UtoNev);
            }
            else
            {
                return szemelyAdat.Nevek.ViseltNev.Item as string;
            }
        }

        static string GetNev(TitkosKapcsolatiKod titkosKapcsolatiKod)
        {
            return titkosKapcsolatiKod.Value;
        }
    }
}