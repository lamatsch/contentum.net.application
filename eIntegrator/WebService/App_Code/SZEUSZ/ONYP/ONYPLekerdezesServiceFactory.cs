﻿using Contentum.eIntegrator.WebService.KKSZB;
using Contentum.eIntegrator.WebService.ONYP.Config;
using Contentum.eIntegrator.WebService.ONYP.KKSZB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.ONYP
{
    public class ONYPLekerdezesServiceFactory
    {
        public enum ServiceMode
        {
            WS,
            KKSZB
        }

        public class ServiceFactoryParameters
        {
            public ServiceMode ServiceMode { get; set; }

            public string ServiceUrl {get; set; }
        }

        public static ONYPLekerdezesService GetONYPLekerdezesService(ServiceFactoryParameters serviceFactoryParameters)
        {
            if (serviceFactoryParameters.ServiceMode == ServiceMode.KKSZB)
            {
                KKSZBParameters kKSZBParameters = ConfigManager.GetKSZBParameters();
                return new ONYPLekerdezesService_KKSZB(serviceFactoryParameters.ServiceUrl, kKSZBParameters);
            }

            return new ONYPLekerdezesService(serviceFactoryParameters.ServiceUrl);
        }
    }
}