﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.ONYP
{
    /// <summary>
    /// Summary description for ServiceParameters
    /// </summary>
    public class ServiceParameters
    {
        public string Szervkod { get; set; }
        public string Felhasznalo { get; set; }
        public string Jogalap { get; set; }
        public string AdatIgenylo { get; set; }

        public ServiceParameters()
        {
            Szervkod = "FOPOLGHIV";
            Felhasznalo = "SOAPUI_VA";
            Jogalap = "JKAP23";
            AdatIgenylo = "WU";
        }
    }
}