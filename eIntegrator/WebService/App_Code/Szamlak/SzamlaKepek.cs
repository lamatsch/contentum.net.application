﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

/// <summary>
/// Summary description for SzamlaKepek
/// </summary>
public class SzamlaKepek
{
    public Guid? Id {get; set; }
    public Guid? Modosito_id { get; set; }
    public string SzamlaSorszam { get; set; }
    // BLG_3876
    public string Adoszam { get; set; }
    // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
    public string VevoBesorolas { get; set; }

    public Guid? Dokumentum_id { get; set; }
    public string External_link { get; set; }
    public string FajlNev { get; set; }
    public Guid? Org { get; set; }
    public int? Ver { get; set; }

    public static SzamlaKepek Create(System.Data.DataRow row)
    {
        SzamlaKepek szk = new SzamlaKepek();

        szk.Id = row["Id"] as Guid?;
        szk.Modosito_id = row["modosito_id"] as Guid?;
        szk.SzamlaSorszam = row["szamlasorszam"] as string;
        // BLG_3876
        szk.Adoszam = row["adoszam"] as string;
        // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
        szk.VevoBesorolas = row["vevobesorolas"] as string;

        szk.Dokumentum_id = row["dokumentum_id"] as Guid?;
        szk.External_link = row["external_link"] as string;
        szk.FajlNev = row["fajlnev"] as string;
        szk.Org = row["org"] as Guid?;
        szk.Ver = row["ver"] as int?;

        return szk;
    }

    public bool Check(out string error)
    {
        bool isError = false;
        StringBuilder sb = new StringBuilder();

        if (Id == null)
        {
            isError = true;
            sb.AppendLine("Id üres");
        }

        if (Modosito_id == null)
        {
            isError = true;
            sb.AppendLine("Modosito_id üres");
        }

        if (String.IsNullOrEmpty(SzamlaSorszam))
        {
            isError = true;
            sb.AppendLine("SzamlaSorszam üres");
        }

        // BLG_3876
        if (String.IsNullOrEmpty(Adoszam))
        {
            isError = true;
            sb.AppendLine("Adószám üres");
        }

        // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
        if (String.IsNullOrEmpty(VevoBesorolas))
        {
            isError = true;
            sb.AppendLine("Vevőbesorolás üres");
        }

        if (Dokumentum_id == null)
        {
            isError = true;
            sb.AppendLine("Dokumentum_id üres");
        }

        if (String.IsNullOrEmpty(External_link))
        {
            isError = true;
            sb.AppendLine("External_link üres");
        }

        if (String.IsNullOrEmpty(FajlNev))
        {
            isError = true;
            sb.AppendLine("FajlNev üres");
        }

        if (Org == null)
        {
            isError = true;
            sb.AppendLine("Org üres");
        }

        if (Ver == null)
        {
            isError = true;
            sb.AppendLine("Ver üres");
        }

        error = sb.ToString();
        return !isError;
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat("Id = {0}", Id);
        sb.AppendLine();
        sb.AppendFormat("Modosito_id = {0}", Modosito_id);
        sb.AppendLine();
        sb.AppendFormat("SzamlaSorszam = {0}", SzamlaSorszam);
        sb.AppendLine();
        // BLG_3876
        sb.AppendFormat("Adoszam = {0}", Adoszam);
        sb.AppendLine();
        // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
        sb.AppendFormat("Vevőbesorolás = {0}", VevoBesorolas);
        sb.AppendLine();

        sb.AppendFormat("Dokumentum_id = {0}", Dokumentum_id);
        sb.AppendLine();
        sb.AppendFormat("External_link = {0}", External_link);
        sb.AppendLine();
        sb.AppendFormat("FajlNev = {0}", FajlNev);
        sb.AppendLine();
        sb.AppendFormat("Org = {0}", Org);
        sb.AppendLine();
        sb.AppendFormat("Ver = {0}", Ver);
        return sb.ToString();
    }
}