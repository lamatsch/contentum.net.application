﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System.Data;
using System.Net;
using System.Text;

/// <summary>
/// Summary description for EREC_SzamlakService
/// </summary>
public class SzamlakManager : System.Web.Services.WebService
{
    private SzamlakStoredProcedure sp = null;

    private DataContext dataContext;

    public SzamlakManager()
    {
        dataContext = new DataContext(this.Application);
        sp = new SzamlakStoredProcedure(dataContext);
    }

    public SzamlakManager(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new SzamlakStoredProcedure(dataContext);
    }

    public Result GetFeltoltendoSzamlaKepek(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetFeltoltendoSzamlaKepek(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    public Result PirSzamlaKepFeltoltes()
    {
        Logger.Debug("PirSzamlaKepFeltoltes kezdete");

        Result result = new Result();
        try
        {
            ExecParam execParam = new ExecParam();
            SzamlakManager svc = new SzamlakManager(this.dataContext);
            Logger.Debug("PirSzamlaKepFeltoltes - GetFeltoltendoSzamlaKepek");
            Result res = svc.GetFeltoltendoSzamlaKepek(execParam);

            if (res.IsError)
            {
                Logger.Error("PirSzamlaKepFeltoltes - GetFeltoltendoSzamlaKepek", execParam, res);
                throw new ResultException(res);
            }

            Logger.Debug(String.Format("PirSzamlaKepFeltoltes - GetFeltoltendoSzamlaKepek: {0}", res.Ds.Tables[0].Rows.Count));

            StringBuilder sb = new StringBuilder();

            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                SzamlaKepek szk = SzamlaKepek.Create(row);

                try
                {
                    PirSzamlaKepFeltoltes(szk);
                }
                catch (Exception ex)
                {
                    Result resError = ResultException.GetResultFromException(ex);
                    string error = ResultError.GetErrorMessageFromResultObject(resError);
                    sb.AppendLine(error);
                    Logger.Error(String.Format("PirSzamlaKepFeltoltes hiba: {0}", error));
                }
            }

            if (sb.Length > 0)
            {
                result.ErrorCode = sb.ToString();
                result.ErrorMessage = sb.ToString();
            }
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
        }

        Logger.Debug("PirSzamlaKepFeltoltes vege");
        return result;
    }

    private void PirSzamlaKepFeltoltes(SzamlaKepek SzamlaKep)
    {
        Logger.Debug(String.Format("PirSzamlaKepFeltoltes kezdete: {0}", SzamlaKep.ToString()));

        string error;

        if (!SzamlaKep.Check(out error))
        {
            Logger.Error(String.Format("PirSzamlaKepFeltoltes SzamlaKep.Check: {0}", error));
            throw new Exception(error);
        }

        ExecParam xpm = new ExecParam();
        xpm.Felhasznalo_Id = SzamlaKep.Modosito_id.ToString();
        xpm.Org_Id = SzamlaKep.Org.ToString();

        bool isPIREnabled = Rendszerparameterek.GetBoolean(xpm, Rendszerparameterek.PIR_INTERFACE_ENABLED);

        Logger.Debug(String.Format("PirSzamlaKepFeltoltes - PIR_INTERFACE_ENABLED: {0}", isPIREnabled));

        if (isPIREnabled)
        {
            string PIR_Rendszer = Rendszerparameterek.Get(xpm, Rendszerparameterek.PIR_RENDSZER);

            Logger.Debug(String.Format("PirSzamlaKepFeltoltes - PIR_RENDSZER: {0}", PIR_Rendszer));

            if (PIR_Rendszer != "PIR")
            {
                Logger.Debug(String.Format("PirSzamlaKepFeltoltes - GetDokTartalom: {0}", SzamlaKep.External_link));
                byte[] content = GetDokTartalom(SzamlaKep.External_link);

                INT_PIRService pirService = new INT_PIRService();

                Logger.Debug(String.Format("PirSzamlaKepFeltoltes - INT_PIRService.SetPIR_Rendszer: {0}", PIR_Rendszer));
                pirService.SetPIR_Rendszer(PIR_Rendszer);
                // CR3359 Számla átadásnál (PIR interface) új csoportosító mező (ettől függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
                //PIRFileContent fileContent = new PIRFileContent(SzamlaKep.SzamlaSorszam, SzamlaKep.FajlNev, content);
                // BLG_3876
                //PIRFileContent fileContent = new PIRFileContent(SzamlaKep.SzamlaSorszam, SzamlaKep.VevoBesorolas, SzamlaKep.FajlNev, content);
                PIRFileContent fileContent = new PIRFileContent(SzamlaKep.SzamlaSorszam, SzamlaKep.Adoszam, SzamlaKep.VevoBesorolas, SzamlaKep.FajlNev, content);

                Logger.Debug(String.Format("PirSzamlaKepFeltoltes - INT_PIRService.PirSzamlaKepFeltoltes: {0}, Adószám: {1}, {2}", SzamlaKep.SzamlaSorszam, SzamlaKep.Adoszam, SzamlaKep.FajlNev));
                Result result = pirService.PirSzamlaKepFeltoltes(xpm, fileContent);

                if (result.IsError)
                {
                    Logger.Error("PirSzamlaKepFeltoltes - INT_PIRService.PirSzamlaKepFeltoltes hiba", xpm, result);
                    throw new ResultException(result);
                }

                Contentum.eRecord.Service.EREC_SzamlakService szamlakService = eRecordService.ServiceFactory.GetEREC_SzamlakService();
                EREC_Szamlak szamla = new EREC_Szamlak();
                szamla.Updated.SetValueAll(false);
                szamla.Base.Updated.SetValueAll(false);

                szamla.Base.Ver = SzamlaKep.Ver.ToString();
                szamla.Base.Updated.Ver = true;

                szamla.Dokumentum_Id = SzamlaKep.Dokumentum_id.ToString();
                szamla.Updated.Dokumentum_Id = true;

                ExecParam xpmSzamla = xpm.Clone();
                xpmSzamla.Record_Id = SzamlaKep.Id.ToString();

                Logger.Debug(String.Format("PirSzamlaKepFeltoltes - EREC_SzamlakService.Update: {0}", xpmSzamla.Record_Id));
                Result resSzamla = szamlakService.Update(xpmSzamla, szamla);

                if (resSzamla.IsError)
                {
                    Logger.Error("PirSzamlaKepFeltoltes - EREC_SzamlakService.Update hiba", xpmSzamla, resSzamla);
                    throw new ResultException(resSzamla);
                }
            }
            
        }

        Logger.Debug("PirSzamlaKepFeltoltes vege");
    }

    private byte[] GetDokTartalom(string externalLink)
    {
        using (WebClient client = new WebClient())
        {
            client.Credentials = new NetworkCredential(UI.GetAppSetting("eSharePointBusinessServiceUserName"), UI.GetAppSetting("eSharePointBusinessServicePassword"), UI.GetAppSetting("eSharePointBusinessServiceUserDomain"));
            return client.DownloadData(externalLink);
        }
    }
}