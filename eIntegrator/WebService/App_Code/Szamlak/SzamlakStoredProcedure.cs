﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for EREC_SzamlakStoredProcedure
/// </summary>
public class SzamlakStoredProcedure
{
    private DataContext dataContext;

    public SzamlakStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }

    public Result GetFeltoltendoSzamlaKepek(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_SzamlakGetFeltoltendoSzamlaKepek");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_SzamlakGetFeltoltendoSzamlaKepek]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}