﻿using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for KuldemenyFajta
/// </summary>
public class KuldemenyFajta
{
    public string RendeltetesiHely { get; set; }
    public string KuldemenyTipus { get; set; }
    public string Szolgaltatasok { get; set; }
    public string Suly { get; set; }

    public KuldemenyFajta(string rendeltetesiHely, string kuldemenyTipus, string szolgaltatasok, string suly)
    {
        this.RendeltetesiHely = rendeltetesiHely;
        this.KuldemenyTipus = kuldemenyTipus;
        this.Szolgaltatasok = szolgaltatasok;
        this.Suly = suly;
    }

    bool IsBelfoldi
    {
        get
        {
            if (!IsEuropai && !IsEgyebKulfoldi)
                return true;
            else
                return false;
        }
    }

    bool IsEuropai
    {
        get
        {
            return "EU".Equals(this.RendeltetesiHely, StringComparison.InvariantCultureIgnoreCase);
        }
    }

    bool IsEgyebKulfoldi
    {
        get
        {
            return "EKULF".Equals(this.RendeltetesiHely, StringComparison.InvariantCultureIgnoreCase);
        }
    }

    bool Elsobbsegi
    {
        get
        {
            return Szolgaltatasok.Contains("K_PRI");
        }
    }

    public string ElsobbsegiString
    {
        get
        {
            return ToDBString(Elsobbsegi);
        }
    }

    bool Ajanlott
    {
        get
        {
            return Szolgaltatasok.Contains("K_AJN");
        }
    }

    public string AjanlottString
    {
        get
        {
            return ToDBString(Ajanlott);
        }
    }

    bool Tertiveveny
    {
        get
        {
            return Szolgaltatasok.Contains("K_TEV");
        }
    }

    public string TertivevenyString
    {
        get
        {
            return ToDBString(Tertiveveny);
        }
    }

    bool PostaiLezaroSzolgalat
    {
        get
        {
            return Szolgaltatasok.Contains("K_LEZ");
        }
    }

    public string PostaiLezaroSzolgalatString
    {
        get
        {
            return ToDBString(PostaiLezaroSzolgalat);
        }
    }

    bool SajatKezbe
    {
        get
        {
            return Szolgaltatasok.Contains("K_SKZ");
        }
    }

    public string SajatKezbeString
    {
        get
        {
            return ToDBString(SajatKezbe);
        }
    }

    bool E_ertesites
    {
        get
        {
            return Szolgaltatasok.Contains("K_EFF");
        }
    }

    public string E_ertesitesString
    {
        get
        {
            return ToDBString(E_ertesites);
        }
    }

    bool E_elorejelzes
    {
        get
        {
            return Szolgaltatasok.Contains("K_EFC");
        }
    }

    public string E_elorejelzesString
    {
        get
        {
            return ToDBString(E_elorejelzes);
        }
    }

    string ToDBString(bool value)
    {
        return value ? "1" : "0";
    }


    public string GetKuldemenyFajta()
    {
        string kuldemenyFajta = KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat;

        switch (KuldemenyTipus)
        {
            //levél
            case "A_111_LEV":
                if (IsBelfoldi)
                {
                    kuldemenyFajta = GetBelfoldiLevelFajta(this.Suly);
                }
                else
                {
                    kuldemenyFajta = GetKulfoldiLevelFajta(this.Suly);
                }
                break;
            //levelezőlap
            case "A_111_LLP":
                if (IsEuropai)
                    kuldemenyFajta = "K1_10";
                else if (IsEgyebKulfoldi)
                    kuldemenyFajta = "K2_10";
                else
                    kuldemenyFajta = "18";
                break;
            //hivatalos irat
            case "A_15_HIV":
                if (SajatKezbe)
                {
                    kuldemenyFajta = KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat_sajat_kezbe;
                }
                else
                {
                    kuldemenyFajta = KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat;
                }
                break;
        }

        return kuldemenyFajta;
    }

    string GetBelfoldiLevelFajta(string suly)
    {
        switch (suly)
        {
            case "30":
                return "01";
            case "50":
                return "02";
            case "100":
                return "03";
            case "250":
                return "04";
            case "500":
                return "05";
            case "750":
                return "06";
            case "2000":
                return "07";
        }

        return "01";
    }

    string GetKulfoldiLevelFajta(string suly)
    {
        string viszonylat = "K1";

        if (IsEgyebKulfoldi)
            viszonylat = "K2";

        string kuldemenyFajta = "{0}_01";


        switch (suly)
        {
            case "20":
                kuldemenyFajta = "{0}_01";
                break;
            case "50":
                kuldemenyFajta = "{0}_03";
                break;
            case "100":
                kuldemenyFajta = "{0}_04";
                break;
            case "250":
                kuldemenyFajta = "{0}_05";
                break;
            case "500":
                kuldemenyFajta = "{0}_06";
                break;
            case "1000":
                kuldemenyFajta = "{0}_07";
                break;
            case "1500":
                kuldemenyFajta = "{0}_08";
                break;
            case "2000":
                kuldemenyFajta = "{0}_09";
                break;
        }

        return String.Format(kuldemenyFajta, viszonylat);
    }

}