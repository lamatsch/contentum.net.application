﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Ellenorzes
{
    [XmlRoot(ElementName = "KezbesitesiIgazolasExportXml")]
    public class KezbesitesiIgazolasExportXml
    {
        [XmlElement(ElementName = "Azonosito")]
        public string Azonosito { get; set; }
        [XmlElement(ElementName = "FilePath")]
        public string FilePath { get; set; }
    }

    [XmlRoot(ElementName = "ArrayOfKezbesitesiIgazolasExportXml")]
    public class ArrayOfKezbesitesiIgazolasExportXml
    {
        [XmlElement(ElementName = "KezbesitesiIgazolasExportXml")]
        public List<KezbesitesiIgazolasExportXml> KezbesitesiIgazolasExportXml { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
    }

    public class NaploFile
    {
        public NaploFile(string filePath)
        {
            var serializer = new XmlSerializer(typeof(ArrayOfKezbesitesiIgazolasExportXml));

            using (Stream reader = new FileStream(filePath, FileMode.Open))
            {
                Content = (ArrayOfKezbesitesiIgazolasExportXml)serializer.Deserialize(reader);
            }
        }
        public ArrayOfKezbesitesiIgazolasExportXml Content { get; set; }
    }
}