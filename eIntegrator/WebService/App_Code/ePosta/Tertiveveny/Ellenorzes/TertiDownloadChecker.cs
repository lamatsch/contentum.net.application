﻿using Contentum.eIntegrator.WebService.ePosta.Tertiveveny;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Contentum.eQuery.Utility;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes.Utils;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eRecord.Service;
using Contentum.eRecord.BaseUtility;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Ellenorzes
{
    public interface ITertiDownloadChecker
    {
        CheckResult CheckBetweenDates(ePostaiTertivevenyParametersBase parameters, string startDate, string endDate);
    }
    public class TertiDownloadCheckerBase
    {
        public void CheckNaploFileContent(FolderCheckResult currentFolderCheckResult, NaploFile naploFile, ExecParam execParam)
        {
            foreach (var item in naploFile.Content.KezbesitesiIgazolasExportXml)
            {
                EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
                search.Ragszam.Value = item.Azonosito;
                search.Ragszam.Operator = Query.Operators.equals;

                EREC_KuldTertivevenyekService service = eRecordService.ServiceFactory.GetEREC_KuldTertivevenyekService();
                Result res = service.GetAll(execParam, search);

                if (res.IsError)
                {
                    currentFolderCheckResult.TertiChecks.Add(new CheckTertiFailedResult()
                    {
                        Error = "Hiba a térti lekérdezése során",
                        Ragszam = item.Azonosito,
                        FilePath = item.FilePath
                    });
                    continue;
                }
                var tertiList = res.GetData<EREC_KuldTertivevenyek>();

                if (tertiList.Count > 1)
                {
                    currentFolderCheckResult.TertiChecks.Add(new CheckTertiFailedResult()
                    {
                        Error = "Több mint egy darab tértivevény található a ragszámra",
                        Ragszam = item.Azonosito,
                        FilePath = item.FilePath
                    });
                    continue;
                }
               
                if (tertiList.Count == 0)
                {
                    currentFolderCheckResult.TertiChecks.Add(new CheckTertiFailedResult()
                    {
                        Error = "Tértivevény nem található!",
                        Ragszam = item.Azonosito,
                        FilePath = item.FilePath
                    });
                    continue;
                }
                var terti = tertiList.First();
                if (string.IsNullOrEmpty(terti.TertivisszaDat))
                {
                    currentFolderCheckResult.TertiChecks.Add(new CheckTertiFailedResult()
                    {
                        Error = "Tértivevény Tertivisszadat érvénytelen!",
                        Ragszam = item.Azonosito,
                        FilePath = item.FilePath,
                        KuldemenyID = terti.Kuldemeny_Id,
                        TertiVisszaDat = terti.TertivisszaDat,
                        TertiVisszaKod = terti.TertivisszaKod
                    });
                    continue;
                }
                if (string.IsNullOrEmpty(terti.TertivisszaKod))
                {
                    currentFolderCheckResult.TertiChecks.Add(new CheckTertiFailedResult()
                    {
                        Error = "Tértivevény TertivisszaKod érvénytelen!",
                        Ragszam = item.Azonosito,
                        FilePath = item.FilePath,
                        KuldemenyID = terti.Kuldemeny_Id,
                        TertiVisszaDat = terti.TertivisszaDat,
                        TertiVisszaKod = terti.TertivisszaKod
                    });
                    continue;
                }
                currentFolderCheckResult.TertiChecks.Add(new CheckTertiOkResult()
                {
                    Ragszam = item.Azonosito,
                    FilePath = item.FilePath,
                    KuldemenyID = terti.Kuldemeny_Id,
                    TertiVisszaDat = terti.TertivisszaDat,
                    TertiVisszaKod = terti.TertivisszaKod
                });
            }
        }
    }

    public class TertiSFTPDownloadChecker : TertiDownloadCheckerBase, ITertiDownloadChecker
    {
        public CheckResult CheckBetweenDates(ePostaiTertivevenyParametersBase parameters, string start, string end)
        {
            var result = new CheckResult()
            {
                Start = start,
                End = end

            };
            DateTime startDate = DateTime.Parse(start);
            DateTime endDate = DateTime.Parse(end);
            DateTime currentDate = startDate;


            using (SftpClient sftpClient = SFTPUtil.GetClient(parameters.SFTP))
            {
                while (currentDate <= endDate)
                {
                    string currentDateFolder = string.Format("{0}/{1}", parameters.SFTP.RemoteFolderPath, currentDate.ToString(ePostaiTertivevenyFTPBase.BaseConstants.DATE_FORMAT));
                    var currentFolderCheckResult = new FolderCheckResult();
                    currentFolderCheckResult.Folder = currentDateFolder;

                    if (!sftpClient.Exists(currentDateFolder))
                    {
                        currentFolderCheckResult.Error = "Nincs egyetlen fájl se a célmappában!";
                        result.Result.Add(currentFolderCheckResult);
                        currentDate = currentDate.AddDays(1);
                        continue;
                    }

                    IEnumerable<SftpFile> files = sftpClient.ListDirectory(currentDateFolder, null);
                    if (files == null || files.Where(f => !f.IsDirectory).Count() < 1)
                    {
                        currentFolderCheckResult.Error = "Nincs egyetlen fájl se a célmappában!";
                        result.Result.Add(currentFolderCheckResult);
                        currentDate = currentDate.AddDays(1);
                        continue;
                    }

                    string naploFilePath = string.Empty;
                    string localDownloadPath = Path.Combine(parameters.SFTP.ArchiveFolderPath, currentDateFolder);
                    foreach (SftpFile file in files.Where(f => !f.IsDirectory))
                    {
                        if (file.Name.StartsWith(PostaiTertivevenyErkeztetesFTPBase.Constants.NAPLO_PREFIX))
                        {
                            naploFilePath = Path.Combine(localDownloadPath, file.Name);
                        }
                        SFTPUtil.DownloadFileFromSFTP(Path.Combine(parameters.SFTP.ArchiveFolderPath, currentDateFolder), sftpClient, file);
                    }

                    if (naploFilePath == string.Empty)
                    {
                        currentFolderCheckResult.Error = "Nem létezik a naplófájl a mappában!";
                        result.Result.Add(currentFolderCheckResult);
                        currentDate = currentDate.AddDays(1);
                        continue;
                    }

                    base.CheckNaploFileContent(currentFolderCheckResult, new NaploFile(naploFilePath), parameters.ExecParam);
                    result.Result.Add(currentFolderCheckResult);
                    currentDate = currentDate.AddDays(1);
                }

                sftpClient.Disconnect();
            }
            return result;
        }
    }
}