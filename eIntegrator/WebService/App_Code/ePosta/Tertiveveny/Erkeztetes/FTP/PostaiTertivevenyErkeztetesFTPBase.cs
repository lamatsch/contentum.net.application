﻿using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using Contentum.eUtility;
using Contentum.eUtility.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes
{
    public abstract class PostaiTertivevenyErkeztetesFTPBase : PostaiTertivevenyErkeztetes
    {
        protected ePostaiTertivevenyParametersBase Parameters { get; set; }


        public PostaiTertivevenyErkeztetesFTPBase(ePostaiTertivevenyParametersBase parameters)
        {
            Parameters = parameters;
        }

        public class Constants
        {
            public const string NAPLO_PREFIX = "naplo";
        }

        public static bool IsNaploFileExists(string[] files)
        {
            if (files.Where(f => f.StartsWith(PostaiTertivevenyErkeztetesFTPBase.Constants.NAPLO_PREFIX)).Count() == 0)
            {
                Logger.Error("Fajlok: " + string.Join(",", files.ToArray()));
                Logger.Error(string.Format("Nem létezik a {0} kezdetű fájl.", Constants.NAPLO_PREFIX));
                return false;
            }
            return true;
        }

        public ePostaiTertivevenyErkeztetesProcessResult Start(string kezdoDatum,string vegDatum)
        {
            Logger.Debug("FTPErkeztetes Start() kezdoDatum" + kezdoDatum + " vegDatum " + vegDatum);
            ePostaiTertivevenyErkeztetesProcessResult result = new ePostaiTertivevenyErkeztetesProcessResult();
            List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase> resultItems = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();

            DateTime startDate = DateTime.Parse(kezdoDatum);
            DateTime endDate = DateTime.Parse(vegDatum);
            DateTime currentDate = startDate;
            while (currentDate <= endDate)
            {
                string dateString = currentDate.ToString(ePostaiTertivevenyFTPBase.BaseConstants.DATE_FORMAT);
                Logger.Debug("FTPErkeztetes Start()" + dateString);

                 try
                {
                    StartDownload(RemoteFolderFullPath(dateString));
                }
                catch (Exception e)
                {
                    Logger.Error("FTPErkeztetes Download error: ", e);
                    result.CriticalError = e.Message;
                    base.HandleCriticalError(Parameters, e.Message);
                    return result;
                }
                List<ePostaTertivevenyDataPdfClass> tertivevenyek = null;
                try
                {
                    var tempFolderManager = GetTempFolderManager();
                    var failed = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();
                    PDFDataExtractor extractor = new PDFDataExtractor(tempFolderManager);
                    tertivevenyek = extractor.StartExtract(Parameters.DataXMLName, out failed);
                    resultItems.AddRange(failed);
                }
                catch (Exception e)
                {
                    Logger.Error("FTPErkeztetes Extractor error: ", e);
                    result.CriticalError = e.Message;
                    base.HandleCriticalError(Parameters, e.Message);
                    return result;
                }
                var erkeztetResults = base.Erkeztet(tertivevenyek, Parameters);
                resultItems.AddRange(erkeztetResults);
                GetTempFolderManager().Clear();

                base.HandleErrors(Parameters, resultItems); 
                //Ez az utolsó müvelet a ciklus iterációban
                currentDate = currentDate.AddDays(1);
            }
            result.Results = resultItems.ToArray();
            return result;
        }

        public ePostaiTertivevenyErkeztetesProcessResult Start()
        {
            Logger.Debug("FTPErkeztetes Start()");
            ePostaiTertivevenyErkeztetesProcessResult result = new ePostaiTertivevenyErkeztetesProcessResult();
            List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase> resultItems = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();
            try
            {
                DownloadAllFoldersForDays();
            }
            catch (Exception e)
            {
                Logger.Error("FTPErkeztetes Download error: ", e);
                result.CriticalError = e.Message;
                base.HandleCriticalError(Parameters, e.Message);
                return result;
            }
            List<ePostaTertivevenyDataPdfClass> tertivevenyek = null;
            try
            {
                var tempFolderManager = GetTempFolderManager();
                PDFDataExtractor extractor = new PDFDataExtractor(tempFolderManager);
                var failedItems = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();
                tertivevenyek = extractor.StartExtract(Parameters.DataXMLName, out failedItems);
                resultItems.AddRange(failedItems);
            }
            catch (Exception e)
            {
                Logger.Error("FTPErkeztetes Extractor error: ", e);
                result.CriticalError = e.Message;
                base.HandleCriticalError(Parameters, e.Message);
                return result;
            }
            var erkeztetResults = base.Erkeztet(tertivevenyek, Parameters);
            resultItems.AddRange(erkeztetResults);
            GetTempFolderManager().Clear();

            base.HandleErrors(Parameters, resultItems);
            result.Results = resultItems.ToArray();
            return result;
        }

        private void DownloadAllFoldersForDays()
        {
            //Ez a szám mondja meg, hogy hány napot kell az aktuális napból kivonni pl
            // DaysToProcess = 3 akkor 3 napra visszamenőleg kell megnéznünk a tértiket

            //max az előző napig nézzük meg ezért 1 ig megyünk
            for (int daysToSubstact = GetFTPParameters().DaysToProcess; daysToSubstact >= 1; daysToSubstact--)
            {
                //AddDaysnek ha negatív számot adsz akkor kivonja 
                string folderForDate = DateTime.Today.AddDays(daysToSubstact * -1).ToString(ePostaiTertivevenyFTPBase.BaseConstants.DATE_FORMAT);
                StartDownload(RemoteFolderFullPath(folderForDate));
            }
        }

        protected abstract void StartDownload(string folder);

        protected abstract TempFolderManager GetTempFolderManager();

        public abstract void CheckConnection();

        protected abstract ePostaiTertivevenyFTPBase GetFTPParameters();

        private string RemoteFolderFullPath(string subfolderForTheDay)
        {
            return string.Format(@"{0}/{1}", GetFTPParameters().RemoteFolderPath, subfolderForTheDay);
        }

        protected string RemoteFilePath(ePostaiTertivevenyFTPBase parameters, string subfolderForTheDay, string file)
        {
            return string.Format(@"{0}/{1}/{2}", parameters.RemoteFolderPath, subfolderForTheDay, file);
        }
    }
}