﻿using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using Contentum.eUtility;
using Contentum.eUtility.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes
{
    public class PostaiTertivevenyErkeztetesFTPOverSSL : PostaiTertivevenyErkeztetesFTPBase, IePostaTertivevenyErkeztetes
    {

        public PostaiTertivevenyErkeztetesFTPOverSSL(ePostaiTertivevenyParametersBase parameters) : base(parameters)
        {
            CheckParameters();
        }
    
        #region HELPER

        protected override void StartDownload(string folder)
        {
            Logger.Debug("Mappa feldolgozása: " + folder);
            if (!IsTenantFolderExists(folder))
            {
                string error = string.Format("Cél mappa nem létezik: {0}", folder);
                Logger.Debug(error);
                throw new Exception(error);
            }
            List<string> fileList = ListRemoteTenantFolderFiles(folder);
            if (fileList == null || fileList.Count < 1)
            {
                Logger.Debug("Nincs egyetlen fájl se a célmappában");
                return;
            }
            if (!PostaiTertivevenyErkeztetesFTPBase.IsNaploFileExists(fileList.ToArray()))
            {
                Logger.Error("Nem létezik a naplófájl a mappában.");
                return;
            }
            for (int i = 0; i < fileList.Count; i++)
            {
                DownloadFile(folder, fileList[i]);
            }

        }

        private bool IsTenantFolderExists(string folderForDate)
        {
            FtpWebRequest ftpRequest = PrepareFtpConnection(null);
            ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;

            List<string> directories = new List<string>();
            using (FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse())
            {
                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    string remoteFolder = streamReader.ReadLine();
                    while (!string.IsNullOrEmpty(remoteFolder))
                    {
                        directories.Add(remoteFolder);
                        remoteFolder = streamReader.ReadLine();
                    }

                    streamReader.Close();
                }
                response.Close();
            }
            if (directories == null || directories.Count < 1 || !directories.Contains(folderForDate))
            {
                string error = string.Format("FTPS tenant mappa nem létezik:", folderForDate);
                Logger.Debug(error);
                return false;
            }
            return true;
        }

        private List<string> ListRemoteTenantFolderFiles(string folder)
        {
            FtpWebRequest ftpRequest = PrepareFtpConnection(folder);
            ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;

            List<string> fileList = new List<string>();
            using (FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse())
            {
                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    string remoteFile = streamReader.ReadLine();
                    while (!string.IsNullOrEmpty(remoteFile))
                    {
                        if (IsDownloadAble(remoteFile))
                        {
                            fileList.Add(remoteFile);
                        }
                        remoteFile = streamReader.ReadLine();
                    }
                    streamReader.Close();
                }
                response.Close();
            }
            return fileList;
        }

        private FtpWebRequest PrepareFtpConnection(string SubFolder)
        {
            FtpWebRequest ftpRequest = null;
            if (string.IsNullOrEmpty(SubFolder))
            {
                ftpRequest = (FtpWebRequest)WebRequest.Create(Parameters.FTPOverSSL.URL);
            }
            else
            {
                ftpRequest = (FtpWebRequest)WebRequest.Create(Parameters.FTPOverSSL.URL + "/" + SubFolder);
            }
            PrepareFtpConnectionParameters(ftpRequest);
            return ftpRequest;
        }

        public override void CheckConnection()
        {
            try
            {
                FtpWebRequest ftpRequest = PrepareFtpConnection(null);
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;

                List<string> directories = new List<string>();
                using (FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        streamReader.Close();
                    }
                    response.Close();
                }
                return;
            }
            catch (Exception e)
            {
                Logger.Error("PostaiTertivevenyErkeztetesFTPOverSSL CheckConnection error: ", e);
                throw e;
            }
        }

        private void PrepareFtpConnectionParameters(FtpWebRequest ftpRequest)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            ftpRequest.Credentials = new NetworkCredential(Parameters.FTPOverSSL.UserName, Parameters.FTPOverSSL.Password);
            ftpRequest.EnableSsl = true;
            ftpRequest.UsePassive = true;
            ftpRequest.UseBinary = true;

            X509Certificate cert;

            if (!string.IsNullOrEmpty(Parameters.FTPOverSSL.KeyFilePassphrase))
            {
                cert = new X509Certificate();
                cert.Import(Parameters.FTPOverSSL.KeyFilePath, Parameters.FTPOverSSL.KeyFilePassphrase, X509KeyStorageFlags.MachineKeySet);
            }
            else
            {
                cert = X509Certificate.CreateFromCertFile(Parameters.FTPOverSSL.KeyFilePath);
            }

            X509CertificateCollection certCollection = new X509CertificateCollection
                {
                    cert
                };

            ftpRequest.ClientCertificates = certCollection;
        }

        private void DownloadFile(string filePath, string file)
        {
            Logger.Debug(string.Format("Downloading FTPOverSSL File:", file));
            if (!Directory.Exists(Parameters.TempFilesFolder))
            {
                Directory.CreateDirectory(Parameters.TempFilesFolder);
            }
            string fileDestinationPath = Path.Combine(Parameters.TempFilesFolder, file);
            if (File.Exists(fileDestinationPath))
            {
                Logger.Debug(string.Format("FTPOverSSL file már létezik a cél mappában:", fileDestinationPath));
                return;
            }

            FtpWebRequest ftpRequest = PrepareFtpConnection(RemoteFilePath(Parameters.FTPOverSSL, filePath, file));
            ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
            ftpRequest.KeepAlive = true;
            ftpRequest.UsePassive = false;
            ftpRequest.UseBinary = true;

            List<string> fileList = new List<string>();
            using (FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse())
            {
                using (Stream streamReader = response.GetResponseStream())
                {
                    using (FileStream fileStream = File.OpenWrite(fileDestinationPath))
                    {
                        byte[] byteBuffer = new byte[32 * 1024];
                        int intRead;
                        while ((intRead = streamReader.Read(byteBuffer, 0, byteBuffer.Length)) > 0)
                        {
                            fileStream.Write(byteBuffer, 0, intRead);
                        }
                    }
                    streamReader.Close();
                }
                response.Close();
            }
        }

        private void CheckParameters()
        {
            if (string.IsNullOrEmpty(Parameters.FTPOverSSL.URL))
            {
                string msg = string.Format("{0}: Hiányzó paraméter: {1}", this.GetType().FullName, "URL");
                Logger.Error(msg);
                throw new Exception(msg);
            }

            if (string.IsNullOrEmpty(Parameters.FTPOverSSL.UserName))
            {
                string msg = string.Format("{0}: Hiányzó paraméter: {1}", this.GetType().FullName, "USERNAME");
                Logger.Error(msg);
                throw new Exception(msg);
            }

            if (string.IsNullOrEmpty(Parameters.FTPOverSSL.Password))
            {
                string msg = string.Format("{0}: Hiányzó paraméter: {1}", this.GetType().FullName, "PASSWORD");
                Logger.Error(msg);
                throw new Exception(msg);
            }

            if (string.IsNullOrEmpty(Parameters.FTPOverSSL.RemoteFolderPath))
            {
                string msg = string.Format("{0}: Hiányzó paraméter: {1}", this.GetType().FullName, "REMOTE_FOLDER_PATH");
                Logger.Error(msg);
                throw new Exception(msg);
            }

            if (string.IsNullOrEmpty(Parameters.TempFilesFolder))
            {
                string msg = string.Format("{0}: Hiányzó paraméter: {1}", this.GetType().FullName, "TEMP_DOWNLOAD_FOLDER_PATH");
                Logger.Error(msg);
                throw new Exception(msg);
            }

            if (!Directory.Exists(Parameters.TempFilesFolder))
            {
                Directory.CreateDirectory(Parameters.TempFilesFolder);
            }
        }

        private bool IsDownloadAble(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)
                || !fileName.ToUpper().EndsWith(Parameters.FTPOverSSL.FileFilter, StringComparison.CurrentCultureIgnoreCase))
            {
                return false;
            }
            return true;
        }

        protected override TempFolderManager GetTempFolderManager()
        {
            return new FTPOverSSLTempFolderManager(Parameters);
        }

        protected override ePostaiTertivevenyFTPBase GetFTPParameters()
        {
            return Parameters.FTPOverSSL;
        }


        #endregion HELPER
    }
}