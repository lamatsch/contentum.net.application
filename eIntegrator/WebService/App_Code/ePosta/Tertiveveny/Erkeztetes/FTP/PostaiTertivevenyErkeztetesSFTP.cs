﻿using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes.Utils;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using Contentum.eUtility;
using Contentum.eUtility.Models;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes
{
    public class PostaiTertivevenyErkeztetesSFTP : PostaiTertivevenyErkeztetesFTPBase, IePostaTertivevenyErkeztetes
    {
        public PostaiTertivevenyErkeztetesSFTP(ePostaiTertivevenyParametersBase parameters) : base(parameters)
        {
            CheckParameters();
        }

        protected override void StartDownload(string folder)
        {
            try
            {
                using (SftpClient sftpClient = SFTPUtil.GetClient(Parameters.SFTP))
                {                    
                    if (!sftpClient.Exists(folder))
                    {
                        string error = string.Format("SFTP cél mappa nem létezik: {0}", folder);
                        Logger.Error(error);
                        throw new Exception(error);
                    }

                    IEnumerable<SftpFile> files = sftpClient.ListDirectory(folder, null);
                    if (files == null || files.Where(f => !f.IsDirectory).Count() < 1)
                    {
                        Logger.Error("Nincs egyetlen fájl se a célmappában!");
                        return;
                    }

                    if (!PostaiTertivevenyErkeztetesFTPBase.IsNaploFileExists(files.Where(f => !f.IsDirectory).Select(f => f.Name).ToArray()))
                    {
                        Logger.Error("Nem létezik a naplófájl a mappában!");
                        return;
                    }

                    foreach (SftpFile file in files.Where(f => !f.IsDirectory))
                    {
                        if (!IsDownloadable(file.Name))
                        {
                            Logger.Error("A következő fájlt nem lehet letölteni: " + file.FullName);
                            continue;
                        }
                        SFTPUtil.DownloadFileFromSFTP(Parameters.TempSFTPFilesFolder, sftpClient, file);
                    }
                    sftpClient.Disconnect();
                }
            }
            catch (Exception e)
            {
                Logger.Error("SFTP letöltés hiba ", e);
                base.HandleCriticalError(Parameters, "SFTP letöltés hiba. Mappa: " + folder + " Error: " + e.Message);
            }
        }

        public override void CheckConnection()
        {
            try
            {
                using (SftpClient client = SFTPUtil.GetClient(Parameters.SFTP)) { }
            }
            catch (Exception e)
            {
                Logger.Error("PostaiTertivevenyErkeztetesSFTP CheckFTPConnection error: ", e);
                throw e;
            }
        }

        private void CheckParameters()
        {
            if (string.IsNullOrEmpty(Parameters.SFTP.URL))
            {
                string msg = string.Format("{0}: Hiányzó paraméter: {1}", this.GetType().FullName, "SFTP_URL");
                Logger.Error(msg);
                throw new Exception(msg);
            }

            if (string.IsNullOrEmpty(Parameters.SFTP.UserName))
            {
                string msg = string.Format("{0}: Hiányzó paraméter: {1}", this.GetType().FullName, "SFTP_USERNAME");
                Logger.Error(msg);
                throw new Exception(msg);
            }

            if (string.IsNullOrEmpty(Parameters.SFTP.RemoteFolderPath))
            {
                string msg = string.Format("{0}: Hiányzó paraméter: {1}", this.GetType().FullName, "SFTP_REMOTE_FOLDER_PATH");
                Logger.Error(msg);
                throw new Exception(msg);
            }

            if (string.IsNullOrEmpty(Parameters.TempFilesFolder))
            {
                string msg = string.Format("{0}: Hiányzó paraméter: {1}", this.GetType().FullName, "SFTP_TEMP_DOWNLOAD_FOLDER_PATH");
                Logger.Error(msg);
                throw new Exception(msg);
            }
        }

        private bool IsDownloadable(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)
                || !fileName.ToUpper().EndsWith(Parameters.SFTP.FileFilter, StringComparison.CurrentCultureIgnoreCase))
            {
                return false;
            }
            return true;
        }



        protected override TempFolderManager GetTempFolderManager()
        {
            return new SFTPTempFolderManager(Parameters);
        }

        protected override ePostaiTertivevenyFTPBase GetFTPParameters()
        {
            return Parameters.SFTP;
        }        
    }
}