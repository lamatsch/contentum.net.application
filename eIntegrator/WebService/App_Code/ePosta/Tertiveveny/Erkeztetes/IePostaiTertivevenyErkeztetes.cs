﻿using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes
{
    public interface IePostaTertivevenyErkeztetes
    {
        ePostaiTertivevenyErkeztetesProcessResult Start();       
    }
}