﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using Contentum.eUtility;
using Contentum.eUtility.Models;
using Contentum.eUtility.Models.ePosta;
using Contentum.Net.Pdf.Extractor.Common;
using Contentum.Net.Pdf.Extractor.PdfLibrary;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes
{
    public class PDFDataExtractor
    {
        #region PROPERTIES
        private readonly string PDF_FILE_EXTESNIONS = "*.pdf";

        private readonly string DATA_XML_NAME = "data.xml";

        private readonly string ATVETEL_DATUM_FORMAT = "yyMMdd_HHmmss";
        #endregion

        private TempFolderManager FileManager { get; set; }

        public PDFDataExtractor(TempFolderManager folderManager)
        {
            FileManager = folderManager;
        }

        public PDFDataExtractor(string atvetelDatumFormatum)
        {
            if (!string.IsNullOrEmpty(atvetelDatumFormatum))
            {
                ATVETEL_DATUM_FORMAT = atvetelDatumFormatum;
            }
        }

        /// <summary>
        /// ExtractExactFiles
        /// </summary>
        /// <param name="files"></param>
        /// <param name="folder"></param>
        /// <param name="dataXMLName"></param>
        /// <returns></returns>
        public List<ePostaTertivevenyDataPdfClass> ExtractExactFiles(List<string> files, string folder, string dataXMLName,out List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase> failed)
        {
            failed = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();
            List<ePostaTertivevenyDataPdfClass> collection = new List<ePostaTertivevenyDataPdfClass>();
            ePostaTertivevenyDataPdfClass item;
            string path = string.Empty;
            for (int i = 0; i < files.Count; i++)
            {
                try
                {
                    path = Path.Combine(folder, files[i]);
                    item = ExtractPdfFile(path);
                    if (item != null)
                    {
                        collection.Add(item);
                    }
                }
                catch (Exception e)
                {
                    failed.Add(new ePostaiTertivevenyErkeztetesProcessResult.ItemErrorResult(path,e.Message,null));
                    Logger.Error("Nem sikerült a pdf kibontása: " + e.Message + " Path: " + path);
                }
            }
            return collection;
        }

        /// <summary>
        /// ExtractPDFFromFolder
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="dataXMLName"></param>
        /// <returns></returns>
        public List<ePostaTertivevenyDataPdfClass> StartExtract(string dataXMLName,out List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase> failedItems)
        {

            string[] fileNames = FileManager.GetFileList(FileManager.RootFolder, PDF_FILE_EXTESNIONS);
            if (fileNames == null || fileNames.Length < 1)
            {
                throw new Exception("Egyetlen pdf sincs a " + FileManager.RootFolder + " mappában.");
            }

            return ExtractExactFiles(fileNames.ToList(), FileManager.RootFolder, dataXMLName,out failedItems);
        }

        /// <summary>
        /// Extract
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public List<ePostaTertivevenyDataPdfClass> ExtractPDFFromFile(string filePath)
        {
            FileManager.CheckFile(filePath, PDF_FILE_EXTESNIONS);

            List<ePostaTertivevenyDataPdfClass> collection = new List<ePostaTertivevenyDataPdfClass>();
            ePostaTertivevenyDataPdfClass item;

            try
            {
                item = ExtractPdfFile(filePath);

                if (item != null)
                {
                    collection.Add(item);
                }
            }
            catch (Exception exc)
            {
                Logger.Error("ePosta.Terti.ExtractPDFFromFile", exc);
                throw;
            }
            return collection;
        }

        /// <summary>
        /// ExtractPdfFile
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private ePostaTertivevenyDataPdfClass ExtractPdfFile(string filePath)
        {
            byte[] fileContent = LoadFile(filePath);
            if (fileContent == null) { return null; }

            List<PdfFileAttachment> attachments = ExtractPdfAttachments(filePath);
            if (attachments == null || attachments.Count < 1) { return null; }

            ePostaTertivevenyDataPdfClass item = new ePostaTertivevenyDataPdfClass();
            item.PdfFileContent = fileContent;
            item.PdfAttachments = Convert(attachments);
            item.PdfFileName = new FileInfo(filePath).Name;

            ct_kezbesitesi_igazolas igazolas = ExtractIgazolasFromXml(ref item);
            if (igazolas == null)
            {
                return item;
            }
            ExtractIgazolasData(ref item, igazolas);

            return item;
        }

        /// <summary>
        /// LoadFile
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private byte[] LoadFile(string file)
        {
            return FileManager.LoadFileFromFullPath(file);
        }

        /// <summary>
        /// ExtractPdfAttachments
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private List<PdfFileAttachment> ExtractPdfAttachments(string file)
        {
            if (File.Exists(file))
            {
                byte[] fileData = LoadFile(file);
                PDFExtractor<byte[]> pdfe = new PDFExtractor<byte[]>();
                return pdfe.ExtractAttachments(fileData);
            }
            return null;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private ct_kezbesitesi_igazolas ExtractIgazolasFromXml(ref ePostaTertivevenyDataPdfClass item)
        {
            if (item.PdfAttachments == null || item.PdfAttachments.Count < 1) { return null; }
            ePostaTertivevenyDataPdfAttachmentClass xmlAttachment = item.PdfAttachments.FirstOrDefault(f => f.Name.Equals(DATA_XML_NAME));
            if (xmlAttachment == null) { return null; }

            string xml = System.Text.Encoding.Default.GetString(xmlAttachment.Content);
            object obj = XmlFunction.XmlToObject(xml, typeof(ct_kezbesitesi_igazolas));
            if (obj is ct_kezbesitesi_igazolas)
            {
                return obj as ct_kezbesitesi_igazolas;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// ExtractIgazolasData
        /// </summary>
        /// <param name="item"></param>
        /// <param name="igazolas"></param>
        private void ExtractIgazolasData(ref ePostaTertivevenyDataPdfClass item, ct_kezbesitesi_igazolas igazolas)
        {
            if (igazolas == null) { return; }
            if (igazolas.kuldemeny != null && !string.IsNullOrEmpty(igazolas.kuldemeny.azonosito))
            {
                item.Igazolas.Ragszam = igazolas.kuldemeny.azonosito;
                item.Ragszam = item.Igazolas.Ragszam;
            }
            else
            {
                return;
            }
            item.Igazolas.KezbesitesiVelelem = false;
            if (igazolas.Item is ct_atvetel)
            {
                ct_atvetel atvetel = igazolas.Item as ct_atvetel;
                item.Igazolas.AtvetelSikeres = string.IsNullOrEmpty(atvetel.visszakuldes_oka.ToString()) ? true : false;
                item.Igazolas.AtvevoNeve = atvetel.atvevo_nev;
                if (!string.IsNullOrEmpty(atvetel.idopont))
                {
                    DateTime idopont;
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    if (DateTime.TryParseExact(atvetel.idopont, ATVETEL_DATUM_FORMAT, provider, DateTimeStyles.None, out idopont))
                    {
                        item.Igazolas.AtvetelIdopont = idopont.ToString();
                    }
                }

                if (!atvetel.visszakuldes_okaSpecified || string.IsNullOrEmpty(atvetel.visszakuldes_oka.ToString()))
                {
                    if (atvetel.atvetel_jogcimSpecified && !string.IsNullOrEmpty(atvetel.atvetel_jogcim.ToString()))
                    {
                        switch (atvetel.atvetel_jogcim.ToString().ToUpper())
                        {
                            case "C":
                                item.Igazolas.AtvetelJogcim = KodTarak.ETERTI_ATVETELJOGCIM.C;
                                item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Kezbesitve_Cimzettnek);// KodTarak.TERTIVEVENY_VISSZA_KOD.Cimzett;
                                break;

                            case "M":
                                item.Igazolas.AtvetelJogcim = KodTarak.ETERTI_ATVETELJOGCIM.M;
                                item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Kezbesitve_Meghatalmazottnak); //KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_meghatalmazottnak;
                                break;

                            case "H":
                                item.Igazolas.AtvetelJogcim = KodTarak.ETERTI_ATVETELJOGCIM.H;
                                item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Kezbesitve_Helyettes_Atvevonek); //KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_helyettes_atvevonek;
                                break;

                            case "K":
                                item.Igazolas.AtvetelJogcim = KodTarak.ETERTI_ATVETELJOGCIM.K;
                                item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Kezbesitve_Kozvetett_kezbesitonek); // KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_kozvetett_kezbesitonek;
                                break;

                            default:
                                break;
                        }
                    }
                }
                else if (atvetel.visszakuldes_okaSpecified && !string.IsNullOrEmpty(atvetel.visszakuldes_oka.ToString()))
                {
                    switch (atvetel.visszakuldes_oka.ToString().ToUpper())
                    {
                        case "C":
                            item.Igazolas.VisszakuldesOka = KodTarak.ETERTI_VISSZAKULDES_OKA.C;
                            item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Visszakuldott_Cim_nem_azonosithato); // KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_cim_nem_azonosithato;
                            break;

                        case "I":
                            item.Igazolas.VisszakuldesOka = KodTarak.ETERTI_VISSZAKULDES_OKA.I;
                            item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Visszakuldott_Cimzett_ismeretlen); // KodTarak.TERTIVEVENY_VISSZA_KOD.Ismeretlen_cimzett;
                            break;

                        case "A":
                            item.Igazolas.VisszakuldesOka = KodTarak.ETERTI_VISSZAKULDES_OKA.A;
                            item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Visszakuldott_Kezbesites_akadalyozott); // KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_kezbesites_akadalyozott;
                            break;

                        case "N":
                            item.Igazolas.VisszakuldesOka = KodTarak.ETERTI_VISSZAKULDES_OKA.N;
                            item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Visszakuldott_Nem_kereste); // KodTarak.TERTIVEVENY_VISSZA_KOD.Nem_vette_at;
                            item.Igazolas.KezbesitesiVelelem = true;
                            item.Igazolas.KezbesitesiVelelemDatuma = item.Igazolas.AtvetelIdopont;
                            break;

                        case "E":
                            item.Igazolas.VisszakuldesOka = KodTarak.ETERTI_VISSZAKULDES_OKA.E;
                            item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Visszakuldott_Elkoltozott); // KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldés_oka_elkoltozott;
                            break;

                        case "M":
                            item.Igazolas.VisszakuldesOka = KodTarak.ETERTI_VISSZAKULDES_OKA.M;
                            item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Visszakuldott_Atvetelt_megtagadta); // KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_atvetelt_megtagadta;
                            item.Igazolas.KezbesitesiVelelem = true;
                            item.Igazolas.KezbesitesiVelelemDatuma = item.Igazolas.AtvetelIdopont;
                            break;

                        case "B":
                            item.Igazolas.VisszakuldesOka = KodTarak.ETERTI_VISSZAKULDES_OKA.B;
                            item.Igazolas.TertivevenyVisszateresKod = GetTertivisszaKodFromPostaKod(KodTarak.TERTI_VISSZA_KOD_POSTA.Visszakuldott_Bejelentve_Meghalt_megszunt); //  KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_bejelentve_meghalt_megszunt;
                            break;

                        default:
                            break;
                    }
                }
            }

            if (igazolas.ertesites != null && igazolas.ertesites.Length > 1)
            {
                List<string> ertesitesek = new List<string>();
                for (int i = 0; i < igazolas.ertesites.Length; i++)
                {
                    ertesitesek.Add(igazolas.ertesites[i].idopont);
                }
                item.Igazolas.ErtesitesIdopontok = ertesitesek.ToArray();
            }
            item.Igazolas.TertivisszaDat = DateTime.Now.ToString();
        }

        #region HELPER

        /// <summary>
        /// Convert
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        private List<ePostaTertivevenyDataPdfAttachmentClass> Convert(List<PdfFileAttachment> items)
        {
            List<ePostaTertivevenyDataPdfAttachmentClass> result = new List<ePostaTertivevenyDataPdfAttachmentClass>();
            for (int i = 0; i < items.Count; i++)
            {
                result.Add(Convert(items[i]));
            }
            return result;
        }

        /// <summary>
        /// Convert
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private ePostaTertivevenyDataPdfAttachmentClass Convert(PdfFileAttachment item)
        {
            return new ePostaTertivevenyDataPdfAttachmentClass()
            {
                Name = item.Name,
                ShortName = item.ShortName,
                Content = item.Content
            };
        }

        private string GetTertivisszaKodFromPostaKod(string kod)
        {
            if (FileManager == null || FileManager.GetParam() == null)
            { return null; }

            ExecParam execParam = FileManager.GetParam().ExecParam;
            bool nincsItem;
            var tvKodok = KodtarFuggoseg.GetFuggoKodtarakKodList(execParam, "TERTI_VISSZA_KOD_POSTA", "TERTIVEVENY_VISSZA_KOD", kod, out nincsItem);
            if (nincsItem || tvKodok == null || tvKodok.Count == 0)
            {
                return null;
            }
            return tvKodok.First();
        }

        #endregion HELPER
    }
}