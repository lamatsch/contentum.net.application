﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using Contentum.eQuery.Utility;
using Contentum.eUtility;
using Contentum.eUtility.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Logger = Contentum.eQuery.Utility.Logger;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes
{
    public class PostaiTertivevenyErkeztetes
    {
        public bool IsValid(ePostaTertivevenyDataPdfClass item)
        {
            if (string.IsNullOrEmpty(item.Ragszam))
            {
                Logger.Debug("ePostaTertivevenyDataPdfClass.IsValid:Ragszam is null");
                return false;
            }
            if (item.Igazolas == null)
            {
                Logger.Debug("ePostaTertivevenyDataPdfClass.IsValid:Igazolas is null");
                return false;
            }

            /* if (string.IsNullOrEmpty(Igazolas.TertivevenyVisszateresKod))
             {
                 Logger.Debug("ePostaTertivevenyDataPdfClass.IsValid:Igazolas.TertivevenyVisszateresKod is null");
                 return false;
             }           
             if (string.IsNullOrEmpty(Igazolas.AtvetelJogcim))
             {
                 Logger.Debug("ePostaTertivevenyDataPdfClass.IsValid:Igazolas.AtvetelJogcim is null");
                 return false;
             }*/
            return true;
        }

        public List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase> Erkeztet(List<ePostaTertivevenyDataPdfClass> tertivevenyek, ePostaiTertivevenyParametersBase parameters)
        {
            var results = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();

            if (tertivevenyek == null || tertivevenyek.Count == 0)
            {
                eQuery.Utility.Logger.Debug("Üres List<ePostaTertivevenyDataPdfClass> tertivevenyek et kapott a PostaiTertivevenyErkeztetes.Erkeztet");
                return results;
            }

            EREC_KuldTertivevenyek terti;
            EREC_KuldKuldemenyek kuldemeny;
            foreach (ePostaTertivevenyDataPdfClass item in tertivevenyek)
            {
                Logger.Debug("PostaiTertivevenyErkeztetes.Erkeztet. Ragszam: " + item.Ragszam);
                if (!IsValid(item))
                {
                    results.Add(new ePostaiTertivevenyErkeztetesProcessResult.ItemErrorResult(item.Ragszam, "Hibás data.xml adat !", item));
                    continue;
                }
                try
                {
                    #region TERTI KERESES

                    terti = eUtility.ePosta.Tertiveveny.ePostaiTertivevenyErkeztet.ServiceFindTertiveveny(parameters.ExecParam, item.Ragszam);
                    if (terti == null)
                    {
                        Logger.Debug("PostaiTertivevenyErkeztetes.Erkeztet. Tertiveveny nem található !");
                        results.Add(new ePostaiTertivevenyErkeztetesProcessResult.ItemErrorResult(item.Ragszam, "Tertiveveny nem található !", item));
                        continue;
                    }
                    #endregion TERTI KERESES

                    #region ERKEZTET
                    eUtility.ePosta.Tertiveveny.ePostaiTertivevenyErkeztet.Erkeztet(parameters.ExecParam, item);
                    #endregion ERKEZTET

                    #region PDF FELTOLT
                    //Ha változott a tertivisszakod vagy átvételi jogcím akkor fel kell tölteni a pdf et mindenképpen
                    if (item.Igazolas.TertivevenyVisszateresKod != terti.TertivisszaKod || item.Igazolas.AtvetelJogcim != terti.Base.Note)
                    {
                        Logger.Debug(string.Format("PostaiTertivevenyErkeztetes.Erkeztet. A térti adatai változtak. item.Igazolas.TertivevenyVisszateresKod: {0} terti.TertivisszaKod: {1} item.Igazolas.AtvetelJogcim: {2}: terti.Base.Note:{3} ",
                            terti.TertivisszaKod, item.Igazolas.AtvetelJogcim, item.Igazolas.AtvetelJogcim, terti.Base.Note));
                        eUtility.ePosta.Tertiveveny.ePostaiTertivevenyErkeztet.ServiceUploadPDF(parameters.ExecParam, item.PdfFileContent, item.PdfFileName, item.Ragszam);
                    }
                    else
                    {
                        Logger.Debug("PostaiTertivevenyErkeztetes.Erkeztet. Nem változtak a térti adatai ezér nem töltjük fel a pdf-et.");
                    }
                    #endregion PDF FELTOLT

                    #region ERKEZTETESISZAM
                    kuldemeny = eUtility.ePosta.Tertiveveny.ePostaiTertivevenyErkeztet.GetKuldemeny(parameters.ExecParam, terti.Kuldemeny_Id);
                    if (kuldemeny == null)
                    {
                        Logger.Debug("PostaiTertivevenyErkeztetes.Erkeztet. Küldemény nem található !");
                        results.Add(new ePostaiTertivevenyErkeztetesProcessResult.ItemErrorResult(item.Ragszam, "Küldemény nem található", item));
                        continue;
                    }
                    results.Add(new ePostaiTertivevenyErkeztetesProcessResult.ItemSuccessResult(kuldemeny.Azonosito, item.Ragszam));
                    #endregion ERKEZTETESISZAM
                }
                catch (Exception exc)
                {
                    results.Add(new ePostaiTertivevenyErkeztetesProcessResult.ItemErrorResult(item.Ragszam, exc.Message, item));
                    continue;
                }
            }
            HandleErrors(parameters, results);
            return results;
        }
        public void HandleErrors(ePostaiTertivevenyParametersBase parameters, List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase> results)
        {
            var errorResults = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemErrorResult>();
            foreach (var item in results)
            {
                if (!(item is ePostaiTertivevenyErkeztetesProcessResult.ItemErrorResult))
                {
                    continue;
                }
                errorResults.Add(item as ePostaiTertivevenyErkeztetesProcessResult.ItemErrorResult);
            }
            if (errorResults.Count == 0) { return; }

            string errorSummary = string.Format("{0} db tértivevény feldolgozása során {1} db hiba történt {2} napon. Hibás elemek: ", results.Count, errorResults.Count, DateTime.Today); ;

            foreach (var item in errorResults)
            {
                var pdf = item.GetPDF();
                if (pdf == null)
                {
                    errorSummary += "<br>Ragszám: " + item.RagSzam + "<br>Hibák: " + String.Join(",", item.Errors) + " Nincs PDF.";
                    continue;
                }

                errorSummary += "<br>Ragszám: " + item.RagSzam + "<br>Hibák: " + String.Join(",", item.Errors) + "<br>Letöltési link: ";

                Result uploadResult = SharePointUtil.UploadToSharePoint(parameters.ExecParam, new SharePointUploadDTO(pdf));
                if (uploadResult.IsError)
                {
                    Logger.Error("Nem sikerült feltölteni a dokumentum tárba a tértivevényt:" + uploadResult.ErrorMessage);
                    errorSummary += "Sikertelen feltöltés a dokumentum tárba: " + uploadResult.ErrorMessage;
                    continue;
                }
                string requestURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                requestURL = requestURL.Replace("INT_ePostaiTertivevenyService.asmx", "");
                requestURL = requestURL.Replace("eIntegratorWebService", "eRecord");
                requestURL += "GetDocumentContent.aspx?Id=" + uploadResult.Uid;

                errorSummary += string.Format("<a href=\"{0}\">Letöltés</a><br>", requestURL);
            }
            Logger.Error("Hiba ePostaTertivevény érkeztetés során: \n" + errorSummary.Replace("<br>", "\n"));
            SendEmail(parameters, "Nem sikerült maradéktalanul érkeztetni minden etértivevényt automatikusan", errorSummary);

        }

        public void HandleCriticalError(ePostaiTertivevenyParametersBase parameters, string error)
        {
            SendEmail(parameters, "Kritikus hiba automatikus érkeztetés során", error);
        }

        private void SendEmail(ePostaiTertivevenyParametersBase parameters, string subject, string message)
        {
            eAdmin.Service.EmailService svcEmail = eUtility.eAdminService.ServiceFactory.GetEmailService();
            bool resEmail = svcEmail.SendEmail(parameters.ExecParam,
               Rendszerparameterek.Get(parameters.ExecParam, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM),
                new string[] { parameters.AdminEmail },
                subject,
                null,
                null,
                true,
                message);
        }
    }
}
