﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eUtility.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes
{
    public class PostaiTertivevenyErkeztetesHKP : PostaiTertivevenyErkeztetes, IePostaTertivevenyErkeztetes
    {
        private ePostaiTertivevenyParametersBase Parameters;

        public static class Constansts
        {
            public const string DOKUMENTUM_TIPUS_AZONOSITO = "KEZBIG";
        }

        public PostaiTertivevenyErkeztetesHKP(ePostaiTertivevenyParametersBase parameters)
        {
            Parameters = parameters;
        }

        //Csak tesztelésre van használva ez a method
        public ePostaiTertivevenyErkeztetesProcessResult Start()
        {
            Logger.Debug("PostaiTertivevenyErkeztetesHKP Start()");
            ePostaiTertivevenyErkeztetesProcessResult result = new ePostaiTertivevenyErkeztetesProcessResult();
            List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase> resultItems = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();
            List<ePostaTertivevenyDataPdfClass> tertivevenyek = null;
            try
            {
                Logger.Debug("PostaiTertivevenyErkeztetesHKP  pdfeket kibontása");
                var failed = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();
                PDFDataExtractor extractor = new PDFDataExtractor(new HKPTempFolderManager(Parameters));
                tertivevenyek = extractor.StartExtract(Parameters.DataXMLName, out failed);
                resultItems.AddRange(failed);
                Logger.Debug("PostaiTertivevenyErkeztetesHKP kibontott pdfeket:  " + tertivevenyek.Count);
            }
            catch (Exception e)
            {
                Logger.Error("PostaiTertivevenyErkeztetesHKP hiba a kibontás során: ", e);
                result.CriticalError = e.Message;
                base.HandleCriticalError(Parameters, e.Message);
                return result;
            }

            resultItems.AddRange(base.Erkeztet(tertivevenyek, Parameters));
            base.HandleErrors(Parameters, resultItems);
            result.Results = resultItems.ToArray();
            return result;
        }

        public ePostaiTertivevenyErkeztetesProcessResult ExtractAndProcessSpecificFile(TempFolderManager folderManager, string fileName, bool removeFolder)
        {
            var fileNames = new List<string>() { fileName };
            return ExtractAndProcess(fileNames.ToArray(), folderManager, removeFolder);
        }

        //Ez az éles method amit a ProcessManager hivogat amikor jönnek az eBeadványok
        public ePostaiTertivevenyErkeztetesProcessResult ExtractAndProcessAllFiles(TempFolderManager folderManager, bool removeFolder)
        {
            var fileNames = new List<string>(folderManager.GetFileList());
            return ExtractAndProcess(fileNames.ToArray(), folderManager, removeFolder);
        }
        private ePostaiTertivevenyErkeztetesProcessResult ExtractAndProcess(string[] fileNamesToProcess, TempFolderManager folderManager, bool removeFolder)
        {
            Logger.Debug("PostaiTertivevenyErkeztetesHKP ExtractAndProcess() FileNames: " + string.Join(",", fileNamesToProcess));
            ePostaiTertivevenyErkeztetesProcessResult result = new ePostaiTertivevenyErkeztetesProcessResult();
            List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase> resultItems = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();
            List<ePostaTertivevenyDataPdfClass> tertivevenyek = null;
            try
            {
                Logger.Debug("PostaiTertivevenyErkeztetesHKP.ExtractAndProcess  pdfeket kibontása");
                PDFDataExtractor extractor = new PDFDataExtractor(folderManager);
                var failedExtrations = new List<ePostaiTertivevenyErkeztetesProcessResult.ItemResultBase>();
                tertivevenyek = extractor.ExtractExactFiles(new List<string>(fileNamesToProcess), folderManager.RootFolder, Parameters.DataXMLName, out failedExtrations);
                resultItems.AddRange(failedExtrations);
            }
            catch (Exception e)
            {
                Logger.Error("PostaiTertivevenyErkeztetesHKP.ExtractAndProcess hiba a kibontás során ", e);
                result.CriticalError = e.Message;
                base.HandleCriticalError(Parameters, e.Message);
                folderManager.Clear(fileNamesToProcess);
                if (removeFolder)
                {
                    folderManager.RemoveFolder();
                }                
                return result;
            }

            var erkeztetResults = base.Erkeztet(tertivevenyek, Parameters);
            resultItems.AddRange(erkeztetResults);
            result.Results = resultItems.ToArray();
            folderManager.Clear(fileNamesToProcess);
            if (removeFolder)
            {
                folderManager.RemoveFolder();
            }
            return result;
        }    

        public static bool IseTertiveveny(string type)
        {
            if (type == Constansts.DOKUMENTUM_TIPUS_AZONOSITO) { return true; }
            return false;
        }

        public List<string> SavePDFToTempDir(string eBeadvanyId, TempFolderManager folderManager)
        {
            Logger.Debug("SavePDFToTempDir. eBeadvanyID:" + eBeadvanyId);
            var csatolmanyokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();

            EREC_eBeadvanyCsatolmanyokSearch csatolmanyokSearch = new EREC_eBeadvanyCsatolmanyokSearch();
            csatolmanyokSearch.eBeadvany_Id.Value = eBeadvanyId;
            csatolmanyokSearch.eBeadvany_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result csatolmanyokResult = csatolmanyokService.GetAll(Parameters.ExecParam, csatolmanyokSearch);

            if (csatolmanyokResult.IsError)
            {
                throw new Exception(String.Format("EREC_eBeadvanyCsatolmanyokService.GetAll hiba - {0}", csatolmanyokResult.ErrorMessage));
            }

            List<string> dokumentumIds = new List<string>();

            foreach (System.Data.DataRow row in csatolmanyokResult.Ds.Tables[0].Rows)
            {
                EREC_eBeadvanyCsatolmanyok csatolmany = new EREC_eBeadvanyCsatolmanyok();
                Utility.LoadBusinessDocumentFromDataRow(csatolmany, row);
                dokumentumIds.Add(csatolmany.Dokumentum_Id);
            }
            Logger.Debug("SavePDFToTempDir.CsatolmanyIds:" + string.Join(",", dokumentumIds.ToArray()));

            var documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            var dokumentumSearch = new KRT_DokumentumokSearch();
            dokumentumSearch.Id.Value = Search.GetSqlInnerString(dokumentumIds.ToArray());
            dokumentumSearch.Id.Operator = Query.Operators.inner;

            Result dokumentumokResult = documentService.GetAll(Parameters.ExecParam, dokumentumSearch);

            if (dokumentumokResult.IsError)
            {
                throw new Exception(String.Format("EREC_eBeadvanyCsatolmanyokService.GetAll hiba - {0}", csatolmanyokResult.ErrorMessage));
            }

            List<string> fileNames = new List<string>();
            foreach (System.Data.DataRow row in dokumentumokResult.Ds.Tables[0].Rows)
            {
                var dokumentum = new KRT_Dokumentumok();
                Utility.LoadBusinessDocumentFromDataRow(dokumentum, row);
                string extension = Path.GetExtension(dokumentum.FajlNev).ToLower();
                if (extension != ".pdf")
                {
                    continue;
                }
                if (fileNames.Contains(dokumentum.FajlNev))
                {
                    continue;
                }
                Logger.Debug("SavePDFToTempDir. Downloading :" + dokumentum.External_Link + " FileName: " + dokumentum.FajlNev);

                try
                {
                    byte[] data = Download(dokumentum.External_Link);
                    folderManager.Save(data, dokumentum.FajlNev);
                }
                catch (Exception err)
                {
                    Logger.Error("SavePDFToTempDir.Could not save file: ", err);
                    continue;
                }
                fileNames.Add(dokumentum.FajlNev);
            }
            return fileNames;
        }

        public static byte[] Download(string link)
        {
            using (WebClient wc = new System.Net.WebClient())
            {
                string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserName");
                string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServicePassword");
                string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserDomain");
                wc.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                return wc.DownloadData(link);
            }
        }
    }
}
