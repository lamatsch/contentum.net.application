﻿using System;

public class eTertivevenyException : Exception
{
    public eTertivevenyException(string message) : base("eTertivevenyException-" + message) { }
}
public class TempFolderException : eTertivevenyException
{
    public TempFolderException(string action,string error) : base("TempFolderException: " + action + "E: "+ error) { }
}

