﻿using System.Collections.Generic;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny
{
    public class CheckResult
    {
        public CheckResult()
        {
            Result = new List<FolderCheckResult>();
        }
        public string Start { get; set; }
        public string End { get; set; }
        public List<FolderCheckResult> Result { get; set; }
        public override string ToString()
        {
            int failedCount = 0;
            foreach (var item in Result)
            {
                int count = item.ErrorCount;
                if (count > 0)
                {
                    failedCount += count;
                }
                if (!string.IsNullOrEmpty(item.Error))
                {
                    failedCount++;
                }
            }

            string result = "";

            if (failedCount == 0)
            {
                result = string.Format("Az összes mappa feldolgozás sikeresen befejeződött {0} és {1} között.", Start, End);
            }
            else
            {
                result = string.Format("{0} db hiba történt a következő intervallum feldolgozása során {1} és {2}.", failedCount, Start, End);
            }
            foreach (var item in Result)
            {
                result += item.ToString();
            }
            return result;
        }
    }
    public class FolderCheckResult
    {
        public int ErrorCount
        {
            get
            {
                int result = 0;
                foreach (var item in TertiChecks)
                {
                    if (item is CheckTertiFailedResult)
                    {
                        result++;
                    }
                }
                return result;
            }
        }
        public FolderCheckResult()
        {
            TertiChecks = new List<CheckTertiResult>();
        }
        public string Folder { get; set; }
        public string Error { get; set; }
        public List<CheckTertiResult> TertiChecks { get; set; }

        public override string ToString()
        {
            string result = string.Format("\nMappa {0}: ", Folder);

            if (!string.IsNullOrEmpty(Error))
            {
                result += "\nHiba: " + Error;
            }
            foreach (var item in TertiChecks)
            {
                result += "\n" + item.ToString();
            }

            return result;
        }
    }
    public class CheckTertiResult
    {
        public string Ragszam { get; set; }
        public string TertiVisszaKod { get; set; }
        public string TertiVisszaDat { get; set; }
        public string KuldemenyID { get; set; }
        public string FilePath { get; set; }
        public override string ToString()
        {
            if (Ragszam == null) { Ragszam = "NULL"; }
            if (TertiVisszaKod == null) { TertiVisszaKod = "NULL"; }
            if (TertiVisszaDat == null) { TertiVisszaDat = "NULL"; }
            if (KuldemenyID == null) { KuldemenyID = "NULL"; }
            if (FilePath == null) { FilePath = "NULL"; }

            string result = string.Format("Ragszam: {0} TertiVisszaKod: {1} TertiVisszaDat: {2} KuldemenyID: {3} FilePath: {4}", Ragszam, TertiVisszaKod, TertiVisszaDat, KuldemenyID, FilePath);
            return result;
        }
    }
    public class CheckTertiOkResult : CheckTertiResult
    {
        public override string ToString()
        {
            string result = "OK | " + base.ToString();
            return result;
        }
    }

    public class CheckTertiFailedResult : CheckTertiResult
    {
        public string Error { get; set; }
        public override string ToString()
        {
            if (Error == null) { Error = "NULL"; }
            string result = "ERROR | " + base.ToString() + string.Format(" ERROR: {0}", Error);
            return result;
        }
    }
}