﻿using System;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models
{
    public class ePostaiTertivevenyParametersManual : ePostaiTertivevenyParametersBase {}

    public class ePostaiTertivevenyParameters : ePostaiTertivevenyParametersBase
    {
        public ePostaiTertivevenyParameters()
        {
            ErkezetetesMod = GetParameter(Constants.ERKEZTETES_MOD);
            AdminEmail = GetParameter(Constants.ADMIN_EMAIL);
            UseDocumentStorage = GetParameter(Constants.USE_DOCUMENT_STORAGE) == Constants.BOOL_TRUE_DB_VALUE;
            DocumentStoragePath = GetParameter(Constants.DOCUMENT_STORAGE_PATH);
            TempFilesFolder = GetParameter(Constants.TEMP_FILES_FOLDER);
            ExecutorUserId = GetParameter(Constants.EXECUTOR_USER_ID);
            ExecutorOrganizationId = GetParameter(Constants.EXECUTOR_ORGANIZATION_GROUP_ID);
            if (IsSFTPMode)
            {
                SFTP = new ePostaiTertivevenySFTPParameters();
            }
            if (IsFTP_OVER_SSLMode)
            {
                FTPOverSSL = new EPostaiTertivevenyFTPOverSSLParameters();
            }
        }
    }

    public class ePostaiTertivevenyFTPBase : ParameterLoader
    {
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string URL { get; set; }
        public int  DaysToProcess { get; set; }
        public string RemoteFolderPath { get; set; }
        public string ArchiveFolderPath { get; set; }
        public static class BaseConstants
        {
            public const string FILE_FILTER = ".PDF";
            public const string DATE_FORMAT = "yyyy-MM-dd";
        }
        public string FileFilter { get; set; }
        public ePostaiTertivevenyFTPBase()
        {
            FileFilter = BaseConstants.FILE_FILTER;
        }
    }

    public class ePostaiTertivevenySFTPParameters : ePostaiTertivevenyFTPBase
    {
        public static class Constants
        {
            public const string PORT = ePostaiTertivevenyParameters.Constants.PREFIX + "SFTP_PORT";
            public const string USER_NAME = ePostaiTertivevenyParameters.Constants.PREFIX + "SFTP_USER_NAME";
            public const string PASSWORD = ePostaiTertivevenyParameters.Constants.PREFIX + "SFTP_PASSWORD";
            public const string SFTP_URL = ePostaiTertivevenyParameters.Constants.PREFIX + "SFTP_URL";
            public const string PDF_PATH = ePostaiTertivevenyParameters.Constants.PREFIX + "SFTP_PDF_PATH";
            public const string SFTP_KEY_FILE_PATH = ePostaiTertivevenyParameters.Constants.PREFIX + "SFTP_KEY_FILE_PATH";
            public const string SFTP_KEY_FILE_PASSPHRASE = ePostaiTertivevenyParameters.Constants.PREFIX + "SFTP_KEY_FILE_PASSPHRASE";
            public const string DAYS_TO_PROCESS = ePostaiTertivevenyParameters.Constants.PREFIX + "SFTP_DAYS_TO_PROCESS";
            public const string ARCHIVE_FOLDER_PATH = ePostaiTertivevenyParameters.Constants.PREFIX + "ARCHIVE_FOLDER_PATH";
        }

        public ePostaiTertivevenySFTPParameters()
        {
            Password = GetParameter(ePostaiTertivevenySFTPParameters.Constants.PASSWORD);
            Port = GetParameter(ePostaiTertivevenySFTPParameters.Constants.PORT,22);
            URL = GetParameter(ePostaiTertivevenySFTPParameters.Constants.SFTP_URL);
            UserName = GetParameter(ePostaiTertivevenySFTPParameters.Constants.USER_NAME);
            RemoteFolderPath = GetParameter(ePostaiTertivevenySFTPParameters.Constants.PDF_PATH);
            SFTPKeyFilePath = GetParameter(ePostaiTertivevenySFTPParameters.Constants.SFTP_KEY_FILE_PATH);
            SFTPKeyFilePassphrase = GetParameter(ePostaiTertivevenySFTPParameters.Constants.SFTP_KEY_FILE_PASSPHRASE);
            DaysToProcess = GetParameter(ePostaiTertivevenySFTPParameters.Constants.DAYS_TO_PROCESS, 1);
            ArchiveFolderPath = GetParameter(ePostaiTertivevenySFTPParameters.Constants.ARCHIVE_FOLDER_PATH);
        }

       
        public string SFTPKeyFilePath { get; set; }
        public string SFTPKeyFilePassphrase { get; set; }           
    }

    public class EPostaiTertivevenyFTPOverSSLParameters : ePostaiTertivevenyFTPBase
    {
        public static class Constants
        {
            public const string URL = ePostaiTertivevenyParametersBase.Constants.PREFIX + "FTP-OVER-SSL_URL";
            public const string PORT = ePostaiTertivevenyParametersBase.Constants.PREFIX + "FTP-OVER-SSL_PORT";
            public const string USER_NAME = ePostaiTertivevenyParametersBase.Constants.PREFIX + "FTP-OVER-SSL_USER_NAME";
            public const string PASSWORD = ePostaiTertivevenyParametersBase.Constants.PREFIX + "FTP-OVER-SSL_PASSWORD";
            public const string PDF_PATH = ePostaiTertivevenyParametersBase.Constants.PREFIX + "FTP-OVER-SSL_PDF_PATH";
            public const string KEY_FILE_PATH = ePostaiTertivevenyParametersBase.Constants.PREFIX + "FTP-OVER-SSL_KEY_FILE_PATH";
            public const string KEY_FILE_PASSPHRASE = ePostaiTertivevenyParametersBase.Constants.PREFIX + "FTP-OVER-SSL_KEY_FILE_PASSPHRASE";
            public const string DAYS_TO_PROCESS = ePostaiTertivevenyParametersBase.Constants.PREFIX + "FTP-OVER-SSL_DAYS_TO_PROCESS";
            public const string ARCHIVE_FOLDER_PATH = ePostaiTertivevenyParametersBase.Constants.PREFIX + "FTP-OVER-SSL_ARCHIVE_FOLDER_PATH";
        }

        public EPostaiTertivevenyFTPOverSSLParameters()
        {
            Password = GetParameter(EPostaiTertivevenyFTPOverSSLParameters.Constants.PASSWORD);
            Port = GetParameter(EPostaiTertivevenyFTPOverSSLParameters.Constants.PORT,21);
            URL = GetParameter(EPostaiTertivevenyFTPOverSSLParameters.Constants.URL);
            UserName = GetParameter(EPostaiTertivevenyFTPOverSSLParameters.Constants.USER_NAME);
            RemoteFolderPath = GetParameter(EPostaiTertivevenyFTPOverSSLParameters.Constants.PDF_PATH);
            KeyFilePath = GetParameter(EPostaiTertivevenyFTPOverSSLParameters.Constants.KEY_FILE_PATH);
            KeyFilePassphrase = GetParameter(EPostaiTertivevenyFTPOverSSLParameters.Constants.KEY_FILE_PASSPHRASE);
            DaysToProcess = GetParameter(EPostaiTertivevenyFTPOverSSLParameters.Constants.DAYS_TO_PROCESS,1);
            ArchiveFolderPath = GetParameter(EPostaiTertivevenyFTPOverSSLParameters.Constants.ARCHIVE_FOLDER_PATH);
        }

        public string KeyFilePath { get; set; }
        public string KeyFilePassphrase { get; set; }     
    }
}