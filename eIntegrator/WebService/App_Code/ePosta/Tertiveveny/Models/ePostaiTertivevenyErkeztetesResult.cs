﻿using Contentum.eUtility.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Xml.Serialization;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models
{
    [XmlInclude(typeof(ItemSuccessResult))]
    [XmlInclude(typeof(ItemErrorResult))]
    [KnownType(typeof(ItemSuccessResult))]
    [KnownType(typeof(ItemErrorResult))]
    [Serializable()]
    public class ePostaiTertivevenyErkeztetesProcessResult
    {
        public ItemResultBase[] Results { get; set; }

        public ePostaiTertivevenyErkeztetesProcessResult()
        {
            Results = new ItemResultBase[] { };
        }
        public void AddResultItem(ItemResultBase result)
        {
            var list = new List<ItemResultBase>(Results);
            list.Add(result);
            Results = list.ToArray();
        }
        public string CriticalError { get; set; }

        public bool HasCriticalError { get { return !string.IsNullOrEmpty(CriticalError); } }

        public List<ItemErrorResult> GetItemErrors()
        {
            var filtered = new List<ItemErrorResult>();
            if (Results != null)
            {
                foreach (var item in Results)
                {
                    if (item is ItemErrorResult)
                    {
                        filtered.Add(item as ItemErrorResult);
                    }
                }
            }
            return filtered;
        }

        public List<ItemSuccessResult> GetSuccessItems()
        {
            var filtered = new List<ItemSuccessResult>();
            if (Results != null)
            {
                foreach (var item in Results)
                {
                    if (item is ItemSuccessResult)
                    {
                        filtered.Add(item as ItemSuccessResult);
                    }
                }
            }
            return filtered;
        }

        public string GetItemSuccesssString()
        {
            List<ItemSuccessResult> successItems = GetSuccessItems();
            if (successItems == null || successItems.Count < 1) { return null; }

            StringBuilder sb = new StringBuilder();
            sb.Append("Sikeres elemek száma: " + successItems.Count);
            sb.Append(Environment.NewLine);
            for (int i = 0; i < successItems.Count; i++)
            {
                sb.Append(string.Format("Ragszám: {0}, Érkeztetési szám: {1}.  | SIKERES ", GetLogValue(successItems[i].RagSzam), GetLogValue(successItems[i].ErkeztetesiSzam)));
                sb.Append(Environment.NewLine);
            }
            return sb.ToString();
        }

        public string GetItemErrorsString()
        {
            List<ItemErrorResult> errors = GetItemErrors();
            if (errors == null || errors.Count < 1) { return null; }

            StringBuilder sb = new StringBuilder();
            sb.Append("Hibás elemek száma: " + errors.Count);
            sb.Append(Environment.NewLine);
            for (int i = 0; i < errors.Count; i++)
            {
                if (errors[i].HasErrors)
                {
                    sb.Append(string.Format("Ragszám: {0}, Érkeztetési szám: {1}. PdfFileName: {2} | Hibák: ", GetLogValue(errors[i].RagSzam), GetLogValue(errors[i].ErkeztetesiSzam), GetLogValue(errors[i].PdfFileName)));
                    sb.Append(string.Join(";", errors[i].Errors));
                    sb.Append(Environment.NewLine);
                }
            }
            return sb.ToString();
        }

        private string GetLogValue(string value)
        {
            if (string.IsNullOrEmpty(value)) { return "-EMPTY-"; }
            return value;
        }

        public class ItemResultBase
        {
            protected ItemResultBase() { }
            public string ErkeztetesiSzam { get; set; }
            public string RagSzam { get; set; }
        }


        [System.Xml.Serialization.XmlType("ePostaiTertivevenyErkeztetesResultErrorResult")]
        public class ItemErrorResult : ItemResultBase
        {
            private ePostaTertivevenyDataPdfClass PDF { get; set; }
            public ItemErrorResult() { }
            public ItemErrorResult(string ragszam, string error, ePostaTertivevenyDataPdfClass pdfData)
            {
                RagSzam = ragszam;
                var errorList = new List<string>();
                errorList.Add(error);
                Errors = errorList.ToArray();
                PDF = pdfData;
                if (pdfData != null)
                {
                    PdfFileName = pdfData.PdfFileName;
                }
            }

            public ePostaTertivevenyDataPdfClass GetPDF()
            {
                return PDF;
            }

            public string[] Errors { get; set; }
            public string PdfFileName { get; set; }
            public byte[] PdfFileContent { get; set; }
            public bool HasErrors { get { return Errors == null || Errors.Length < 1 ? false : true; } }
        }

        [System.Xml.Serialization.XmlType("ePostaiTertivevenyErkeztetesResultSuccessResult")]
        public class ItemSuccessResult : ItemResultBase
        {
            public ItemSuccessResult() { }
            public ItemSuccessResult(string erkeztetesiSzam, string ragSzam)
            {
                ErkeztetesiSzam = erkeztetesiSzam;
                RagSzam = ragSzam;
            }
        }
    }
}
