﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.Utility;
using System;
using System.IO;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models
{

    public class ParameterLoader
    {
        protected string GetParameter(string key)
        {
            try
            {
                return IntegratorParameterek.GetParameter(key);
            }
            catch (Exception e)
            {
                Logger.Info("ePostaiTertiveveny cannot load parameter:" + key + " " + e.Message);
                throw new Exception("Az ePostaiTertiveveny erkeztető nem tud elindulni, mert hiányzik a következő paraméter: " + key);
            }
        }


        protected int GetParameter(string key,int defaultValue)
        {
            int paramInt = defaultValue;
            try
            {
                string  result = IntegratorParameterek.GetParameter(key);
                 int.TryParse(result,out paramInt);
                return paramInt;
            }
            catch (Exception e)
            {
                Logger.Info("ePostaiTertiveveny cannot load parameter:" + key + " " + e.Message);
                throw new Exception("Az ePostaiTertiveveny erkeztető nem tud elindulni, mert hiányzik a következő paraméter: " + key);
            }
        }
    }
    public class ePostaiTertivevenyParametersBase : ParameterLoader
    {
        public static class Constants
        {
            public const string PREFIX = "EPOSTAI_TERTIVEVENY.";
            public const string ADMIN_EMAIL = PREFIX + "ADMIN_EMAIL";
            public const string ERKEZTETES_MOD = PREFIX + "ERKEZTETES_MOD";
            public const string USE_DOCUMENT_STORAGE = PREFIX + "USE_DOCUMENT_STORAGE";
            public const string DOCUMENT_STORAGE_PATH = PREFIX + "DOCUMENT_STORAGE_PATH";
            public const string EXECUTOR_USER_ID = PREFIX + "EXECUTOR_USER_ID";
            public const string EXECUTOR_ORGANIZATION_GROUP_ID = PREFIX + "EXECUTOR_ORGANIZATION_GROUP_ID";
            public const string TEMP_FILES_FOLDER = PREFIX + "TEMP_FILES_FOLDER";

            public const string ERKEZTETES_MOD_HKP_VALUE = "HKP";
            public const string ERKEZTETES_MOD_SFTP_VALUE = "SFTP";
            public const string ERKEZTETES_MOD_FTP_OVER_SSL_VALUE = "FTPS";
            public const string BOOL_TRUE_DB_VALUE = "1";
            public const string DATA_XML_FILENAME = "data.xml";

            public const string DATE_FORMAT = "mm-dd";
        }

        public ePostaiTertivevenyParametersBase()
        {
            SFTP = new ePostaiTertivevenySFTPParameters();
            FTPOverSSL = new EPostaiTertivevenyFTPOverSSLParameters();
        }


        public string ExecutorUserId { get; set; }
        public string ExecutorOrganizationId { get; set; }
        public string ErkezetetesMod { get; set; }
        public string TempFilesFolder { get; set; }
        public string AdminEmail { get; set; }
        public bool UseDocumentStorage { get; set; }
        public string DocumentStoragePath { get; set; }
        public ePostaiTertivevenySFTPParameters SFTP { get; set; }
        public EPostaiTertivevenyFTPOverSSLParameters FTPOverSSL { get; set; }
        public ExecParam ExecParam
        {
            get
            {
                return new ExecParam
                {
                    Felhasznalo_Id = ExecutorUserId,
                    FelhasznaloSzervezet_Id = ExecutorOrganizationId,
                    Org_Id = "450B510A-7CAA-46B0-83E3-18445C0C53A9"
                };
            }
        }


        public bool IsSFTPMode
        {
            get
            {
                if (ErkezetetesMod == null)
                {
                    Logger.Error("ePosta: ErkeztetesMod is null");
                    return false;
                }
                return ErkezetetesMod.Contains(Constants.ERKEZTETES_MOD_SFTP_VALUE);
            }
        }
        public bool IsHKPMode
        {
            get
            {
                if (ErkezetetesMod == null)
                {
                    Logger.Error("ePosta: ErkeztetesMod is null");
                    return false;
                }
                return ErkezetetesMod.Contains(Constants.ERKEZTETES_MOD_HKP_VALUE);
            }
        }

        public bool IsFTP_OVER_SSLMode
        {
            get
            {
                if (ErkezetetesMod == null)
                {
                    Logger.Error("ePosta: ErkeztetesMod is null");
                    return false;
                }
                return ErkezetetesMod.Contains(Constants.ERKEZTETES_MOD_FTP_OVER_SSL_VALUE);
            }
        }

        public string TempHKPFilesFolder { get { return Path.Combine(TempFilesFolder, Constants.ERKEZTETES_MOD_HKP_VALUE); } }
        public string TempSFTPFilesFolder { get { return Path.Combine(TempFilesFolder, Constants.ERKEZTETES_MOD_SFTP_VALUE); } }
        public string TempFTPOverSSLFilesFolder { get { return Path.Combine(TempFilesFolder, Constants.ERKEZTETES_MOD_FTP_OVER_SSL_VALUE); } }

        public string DataXMLName { get { return Constants.DATA_XML_FILENAME; } }
    }
}