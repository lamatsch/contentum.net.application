﻿using Contentum.eIntegrator.WebService.ePosta.Tertiveveny;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Contentum.eQuery.Utility;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny
{
    public class TempFolderManager
    {
        protected ePostaiTertivevenyParametersBase _parameters;
        public string RootFolder;
        protected TempFolderManager(ePostaiTertivevenyParametersBase param, string rootFolder) // Do NOT instantiate it directly. Only the descendants.
        {
            try
            {
                if (!Directory.Exists(rootFolder))
                {
                    Logger.Debug("TempFolderManager mappa létrehozása : " + rootFolder);
                    Directory.CreateDirectory(rootFolder);
                }
            }
            catch (Exception e)
            {
                Logger.Debug("TempFolderManager nem sikerült a mappa létrehozása: " + e.Message);
            }
            _parameters = param;
            RootFolder = rootFolder;
        }
        public virtual void Save(byte[] data, string fileName)
        {
            try
            {
                string filePath = Path.Combine(RootFolder, fileName);
                Logger.Debug("TempFolderManager.Save(" + filePath + ")");
                FileInfo fi = new FileInfo(filePath);
                fi.Directory.Create();
                File.WriteAllBytes(filePath, data);
            }
            catch (Exception e)
            {
                Logger.Error("TempFolderManager.Save", e);
                throw new TempFolderException("Save", e.Message);
            }
        }
        public virtual string[] GetFileList()
        {
            if (Directory.Exists(RootFolder))
            {
                var files = Directory.GetFiles(RootFolder);
                Logger.Debug("TempFolderManager.GetFileList=>" + String.Join(",", files));
                return files;
            }
            else
            {
                Directory.CreateDirectory(RootFolder);
            }
            return new string[] { };
        }
        public virtual string[] GetFileList(string folder, string withFileExtensionFilter)
        {
            if (Directory.Exists(folder))
            {
                var files = Directory.GetFiles(folder, withFileExtensionFilter, SearchOption.TopDirectoryOnly);
                Logger.Debug("TempFolderManager.GetFileList=>" + String.Join(",", files));
                return files;
            }
            else
            {
                Directory.CreateDirectory(folder);
            }
            return new string[] { };
        }
        public virtual void DeleteFile(string fileName)
        {
            string filePath = Path.Combine(RootFolder, fileName);
            Logger.Debug("TempFolderManager.DeleteFile(" + filePath + ")");
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("DeleteFile:" + filePath);
            }
            File.Delete(filePath);
        }

        public virtual byte[] LoadFile(string fileName)
        {
            string filePath = Path.Combine(RootFolder, fileName);
            Logger.Debug("TempFolderManager.LoadFile(" + filePath + ")");
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("LoadFile:" + filePath);
            }
            return File.ReadAllBytes(filePath);
        }

        public virtual byte[] LoadFileFromFullPath(string filePath)
        {
            Logger.Debug("TempFolderManager.LoadFileFromFullPath(" + filePath + ")");
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("LoadFileFromFullPath:" + filePath);
            }
            return File.ReadAllBytes(filePath);
        }


        //NOT USED
        public virtual void CheckFile(string filePath, string fileExtension)
        {
            if (string.IsNullOrEmpty(filePath) || !File.Exists(filePath) || !filePath.EndsWith(fileExtension))
            {
                throw new Exception("Nem található a pdf: " + filePath);
            }
        }
        public virtual void Clear(string[] fileNames)
        {
            var fileNameList = new List<string>(fileNames);
            Logger.Debug("TempDataManager.Clear  RootFolder: " + RootFolder);
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(RootFolder);

                foreach (FileInfo file in di.GetFiles())
                {
                    try
                    {
                        if (fileNameList.Contains(file.Name) || fileNameList.Contains(file.FullName))
                        {
                            file.Delete();
                            Logger.Debug("TempDataManager.Clear Fájl törlése: " + file.FullName);
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Debug("TempDataManager.Clear Nem sikerült a fájl törlése: " + e.Message + " file: " + file.FullName);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Debug("TempDataManager.Clear Nem sikerült a directory info lekérése: " + e.Message);
            }
        }
        public virtual void Clear()
        {
            Logger.Debug("TempDataManager.Clear  RootFolder: " + RootFolder);
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(RootFolder);

                foreach (FileInfo file in di.GetFiles())
                {
                    try
                    {
                        if (file.Extension == ".pdf" || file.Extension == ".xml")
                        {
                            file.Delete();
                            Logger.Debug("TempDataManager.Clear Fájl törlése: " + file.FullName);
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Debug("TempDataManager.Clear Nem sikerült a fájl törlése: " + e.Message + " file: " + file.FullName);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Debug("TempDataManager.Clear Nem sikerült a directory info lekérése: " + e.Message);
            }
        }

        public void RemoveFolder()
        {
            try
            {
                Directory.Delete(RootFolder);
                Logger.Debug("TempDataManager.RemoveFolder: " + RootFolder);
            }
            catch (Exception e)
            {
                Logger.Debug("TempDataManager.RemoveFolder Nem sikerült a mappa törlése: " + e.Message);
            }
        }
        public ePostaiTertivevenyParametersBase GetParam()
        {
            return _parameters;
        }
        /// <summary>
        /// ReadFile
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static byte[] ReadFile(string fileName)
        {
            FileStream f = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            int size = (int)f.Length;
            byte[] data = new byte[size];
            size = f.Read(data, 0, size);
            f.Close();
            return data;
        }
    }

    public class SFTPTempFolderManager : TempFolderManager
    {
        public SFTPTempFolderManager(ePostaiTertivevenyParametersBase param) : base(param, param.TempSFTPFilesFolder)
        {
            Logger.Debug("SFTPTempFolderManager.rootFolder:" + param.TempSFTPFilesFolder);
        }
    }

    public class HKPTempFolderManager : TempFolderManager
    {
        public HKPTempFolderManager(ePostaiTertivevenyParametersBase param) : base(param, param.TempHKPFilesFolder)
        {
            Logger.Debug("HKPTempFolderManager.rootFolder:" + RootFolder);
        }
        public HKPTempFolderManager(ePostaiTertivevenyParametersBase param, string subdir) : base(param, Path.Combine(param.TempHKPFilesFolder, subdir))
        {
            Logger.Debug("HKPTempFolderManager.rootFolder:" + RootFolder);
        }
    }

    public class CustomTempFolderManager : TempFolderManager
    {
        public CustomTempFolderManager(ePostaiTertivevenyParametersBase param, string directoryFullPath) : base(param, directoryFullPath)
        {
            Logger.Debug("CustomTempFolderManager.rootFolder:" + RootFolder);
        }
    }
    public class FTPOverSSLTempFolderManager : TempFolderManager
    {
        public FTPOverSSLTempFolderManager(ePostaiTertivevenyParametersBase param) : base(param, param.TempFTPOverSSLFilesFolder)
        {
            Logger.Debug("FTPOverSSLTempFolderManager.rootFolder:" + param.TempFTPOverSSLFilesFolder);
        }
    }
}