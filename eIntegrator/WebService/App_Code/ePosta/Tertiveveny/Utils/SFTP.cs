﻿using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using Contentum.eUtility;
using Contentum.eUtility.Models;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes.Utils
{
    public class SFTPUtil
    {
        public static  SftpClient GetClient(ePostaiTertivevenySFTPParameters parameters)
        {
            ConnectionInfo keyConnection = GetSFTPConnectionInfoByKeyFile(parameters);
            SftpClient result;
            if (keyConnection != null)
            {
                result = new SftpClient(keyConnection);
            }
            else
            {
                result = new SftpClient(parameters.URL, parameters.Port, parameters.UserName, parameters.Password);
            }
            result.Connect();
            return result;
        }

        public static void DownloadFileFromSFTP(string saveLocation ,SftpClient client, SftpFile file)
        {
            try
            {
                if (file.IsDirectory) { return; }

                Logger.Debug(string.Format("Downloading SFTP File: {0}", file.FullName));
                if (!Directory.Exists(saveLocation))
                {
                    Directory.CreateDirectory(saveLocation);
                }
                string fileDestinationPath = Path.Combine(saveLocation, file.Name);
                if (File.Exists(fileDestinationPath))
                {
                    Logger.Debug(string.Format("SFTP file már létezik a cél mappában: {0}", fileDestinationPath));
                    return;
                }
                using (FileStream fileStream = File.OpenWrite(fileDestinationPath))
                {
                    client.DownloadFile(file.FullName, fileStream, null);
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format("Hiba történt a fájl: {0} letöltése során. E: {1}", file.FullName, e.Message));
            }
        }


        public static ConnectionInfo GetSFTPConnectionInfoByKeyFile(ePostaiTertivevenySFTPParameters parameters)
        {
            if (string.IsNullOrEmpty(parameters.SFTPKeyFilePath)) { return null; }

            if (!File.Exists(parameters.SFTPKeyFilePath))
            {
                Logger.Debug(string.Format("SFTP kulcs file nem található : {0}", parameters.SFTPKeyFilePath));
                return null;
            }

            ConnectionInfo con;
            PrivateKeyFile keyFile;
            try
            {
                FileStream stream = new FileStream(parameters.SFTPKeyFilePath, FileMode.Open, FileAccess.Read);
                if (!string.IsNullOrEmpty(parameters.SFTPKeyFilePassphrase))
                {
                    keyFile = new PrivateKeyFile(stream, parameters.SFTPKeyFilePassphrase);
                }
                else
                {
                    keyFile = new PrivateKeyFile(stream);
                }
            }
            catch (Exception exc)
            {
                Logger.Error("PrivateKeyFile loading error", exc);
                throw;
            }

            PrivateKeyFile[] keyFiles = new[] { keyFile };

            PasswordAuthenticationMethod passMethod = new PasswordAuthenticationMethod(parameters.UserName, parameters.Password);
            PrivateKeyAuthenticationMethod pkeyMethod = new PrivateKeyAuthenticationMethod(parameters.UserName, keyFiles);
            KeyboardInteractiveAuthenticationMethod kiAuth = new KeyboardInteractiveAuthenticationMethod(parameters.UserName);
            kiAuth.AuthenticationPrompt += new EventHandler<Renci.SshNet.Common.AuthenticationPromptEventArgs>((sender,e)=> {
                foreach (Renci.SshNet.Common.AuthenticationPrompt prompt in e.Prompts)
                {
                    if (prompt.Request.IndexOf("UserId:", StringComparison.InvariantCultureIgnoreCase) != -1)
                    {
                        prompt.Response = parameters.UserName;
                    }
                    if (prompt.Request.IndexOf("Password:", StringComparison.InvariantCultureIgnoreCase) != -1)
                    {
                        prompt.Response = parameters.Password;
                    }
                }
            });
            List<AuthenticationMethod> methods = new List<AuthenticationMethod>
                {
                    passMethod,
                    pkeyMethod,
                    kiAuth
                };

            con = new ConnectionInfo(parameters.URL, parameters.Port, parameters.UserName, methods.ToArray());

            return con;
        }      
    }
}