﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Security.Permissions;
using System.Web;
using System.Xml;

namespace eUzenet.Cryptography
{
    /// <summary>
    /// Summary description for CustomSignedXml
    /// </summary>
    public class CustomSignedXml: SignedXml
    {
        private XmlElement m_context = null;
        private XmlDocument m_containingDocument = null;

        public string BasePath { get; set; }

        public CustomSignedXml(): base()
        {
            Initialize(null);
        }

        public CustomSignedXml(XmlDocument document): base(document)
        {
            Initialize(document.DocumentElement);
        }

        public CustomSignedXml(XmlElement elem): base(elem)
        {
           Initialize(elem);
        }

        private void Initialize(XmlElement element)
        {
            m_containingDocument = (element == null ? null : element.OwnerDocument);
            m_context = element;
        }

        public new void LoadXml(XmlElement value)
        {
            base.LoadXml(value);

            if (m_context == null)
            {
                m_context = value;
            }
        }

        public new bool CheckSignature()
        {
            AsymmetricAlgorithm signingKey;
            return CheckSignatureReturningKey(out signingKey);
        }

        public new bool CheckSignatureReturningKey(out AsymmetricAlgorithm signingKey)
        {
            SignedXmlDebugLog.LogBeginSignatureVerification(this, m_context);

            signingKey = null;
            bool bRet = false;
            AsymmetricAlgorithm key = null;

            if (!CheckSignatureFormat())
            {
                return false;
            }

            do
            {
                key = GetPublicKey();
                if (key != null)
                {
                    bRet = CheckSignature(key);
                    SignedXmlDebugLog.LogVerificationResult(this, key, bRet);
                }
            } while (key != null && bRet == false);

            signingKey = key;
            return bRet;
        }

        public new bool CheckSignature(AsymmetricAlgorithm key)
        {
            if (!CheckSignatureFormat())
            {
                return false;
            }

            if (!CheckSignedInfo(key))
            {
                SignedXmlDebugLog.LogVerificationFailure(this, "Log_VerificationFailed_SignedInfo");
                return false;
            }

            // Now is the time to go through all the references and see if their DigestValues are good
            if (!CheckDigestedReferences())
            {
                SignedXmlDebugLog.LogVerificationFailure(this, "Log_VerificationFailed_References");
                return false;
            }

            SignedXmlDebugLog.LogVerificationResult(this, key, true);
            return true;
        }

        private bool CheckDigestedReferences()
        {
            ArrayList references = m_signature.SignedInfo.References;
            XmlUrlResolver resolver = new XmlUrlResolver();
            for (int i = 0; i < references.Count; ++i)
            {
                Reference digestedReference = (Reference)references[i];

                if (!ReferenceUsesSafeTransformMethods(digestedReference))
                {
                    return false;
                }

                SignedXmlDebugLog.LogVerifyReference(this, digestedReference);
                byte[] calculatedHash = null;
                try
                {
                    calculatedHash = CalculateReferenceHash(digestedReference);
                }
                catch (CryptoSignedXmlRecursionException)
                {
                    SignedXmlDebugLog.LogSignedXmlRecursionLimit(this, digestedReference);
                    return false;
                }
                // Compare both hashes
                SignedXmlDebugLog.LogVerifyReferenceHash(this, digestedReference, calculatedHash, digestedReference.DigestValue);

                if (!CryptographicEquals(calculatedHash, digestedReference.DigestValue))
                {
                    return false;
                }
            }

            return true;
        }

        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        private static bool CryptographicEquals(byte[] a, byte[] b)
        {
            System.Diagnostics.Debug.Assert(a != null);
            System.Diagnostics.Debug.Assert(b != null);

            int result = 0;

            // Short cut if the lengths are not identical
            if (a.Length != b.Length)
                return false;

            unchecked
            {
                // Normally this caching doesn't matter, but with the optimizer off, this nets a non-trivial speedup.
                int aLength = a.Length;

                for (int i = 0; i < aLength; i++)
                    // We use subtraction here instead of XOR because the XOR algorithm gets ever so
                    // slightly faster as more and more differences pile up.
                    // This cannot overflow more than once (and back to 0) because bytes are 1 byte
                    // in length, and result is 4 bytes. The OR propagates all set bytes, so the differences
                    // can't add up and overflow a second time.
                    result = result | (a[i] - b[i]);
            }

            return (0 == result);
        }

        private byte[] CalculateReferenceHash(Reference digestedReference)
        {
            Reference newReference = digestedReference;
            string uri = digestedReference.Uri;

            if (!String.IsNullOrEmpty(uri))
            {
                //fájl hivatkozás
                if (uri[0] != '#')
                {
                    Stream fileStream = GetFileStream(uri);
                    newReference = GetSreamReference(digestedReference, fileStream);
                }
            }

            return CalculateReferenceHashValue(newReference);
        }

        Stream GetFileStream(string uri)
        {
            if (!BasePath.EndsWith("/"))
                BasePath += "/";

            Uri baseUri = new Uri(BasePath);
            XmlUrlResolver resolver = new XmlUrlResolver();
            Uri absolutUri = resolver.ResolveUri(baseUri, uri);
            Stream fileStream = resolver.GetEntity(absolutUri, null, typeof(Stream)) as Stream;
            return fileStream;
        }

        void SetSignedXml(Reference reference)
        {
            PropertyInfo pi = typeof(Reference).GetProperty("SignedXml", BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(reference, this, null);
        }

        Reference GetSreamReference(Reference digestedReference, Stream stream)
        {
            System.Security.Cryptography.Xml.Reference streamReference = new System.Security.Cryptography.Xml.Reference(stream);
            streamReference.Id = digestedReference.Id;
            streamReference.DigestMethod = digestedReference.DigestMethod;
            streamReference.DigestValue = digestedReference.DigestValue;
            streamReference.Uri = digestedReference.Uri;
            SetSignedXml(streamReference);
            return streamReference;
        }

        IList GetSignatureReferencedItems()
        {
            PropertyInfo pi = typeof(Signature).GetProperty("ReferencedItems", BindingFlags.Instance | BindingFlags.NonPublic);
            IList references = pi.GetValue(m_signature, null) as IList;
            return references;
        }

        byte[] CalculateReferenceHashValue(Reference reference)
        {
            MethodInfo mi = typeof(Reference).GetMethod("CalculateHashValue", BindingFlags.Instance | BindingFlags.NonPublic);
            byte[] hash = mi.Invoke(reference, new object[] { m_containingDocument, GetSignatureReferencedItems() }) as byte[];
            return hash;
        }

        #region refactoring

        private bool CheckSignatureFormat()
        {
            //MethodInfo mi = typeof(SignedXml).GetMethod("CheckSignatureFormat", BindingFlags.Instance | BindingFlags.NonPublic);
            //return (bool)mi.Invoke(this, null);
            return true;
        }

        private bool CheckSignedInfo(AsymmetricAlgorithm key)
        {
            MethodInfo mi = typeof(SignedXml).GetMethod("CheckSignedInfo", BindingFlags.Instance | BindingFlags.NonPublic, null,
            new Type[] { typeof(AsymmetricAlgorithm) }, null);
            return (bool)mi.Invoke(this, new object[] { key });
        }

        private bool ReferenceUsesSafeTransformMethods(Reference reference)
        {
            MethodInfo mi = typeof(SignedXml).GetMethod("ReferenceUsesSafeTransformMethods", BindingFlags.Instance | BindingFlags.NonPublic);
            return (bool)mi.Invoke(this, new object[] { reference });
        }

        #endregion


    }
}