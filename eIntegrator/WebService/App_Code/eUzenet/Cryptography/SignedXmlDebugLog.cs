﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Web;
using System.Xml;

namespace eUzenet.Cryptography
{
    /// <summary>
    /// Summary description for SignedXmlDebugLog
    /// </summary>
    static class SignedXmlDebugLog
    {
        internal static void LogBeginSignatureVerification(SignedXml signedXml, XmlElement context)
        {
        }

        internal static void LogVerificationResult(SignedXml signedXml, object key, bool verified)
        {
        }

        internal static void LogUnsafeCanonicalizationMethod(SignedXml signedXml, string algorithm, IEnumerable<string> validAlgorithms)
        {
        }

        internal static void LogBeginCheckSignatureFormat(SignedXml signedXml, Func<CustomSignedXml, bool> formatValidator)
        {
        }

        internal static void LogFormatValidationResult(SignedXml signedXml, bool result)
        {
        }

        internal static void LogVerificationFailure(SignedXml signedXml, string failureLocation)
        {
        }

        internal static void LogBeginCheckSignedInfo(SignedXml signedXml, SignedInfo signedInfo)
        {
        }

        internal static void LogVerifySignedInfo(SignedXml signedXml,
                                                AsymmetricAlgorithm key,
                                                SignatureDescription signatureDescription,
                                                HashAlgorithm hashAlgorithm,
                                                AsymmetricSignatureDeformatter asymmetricSignatureDeformatter,
                                                byte[] actualHashValue,
                                                byte[] signatureValue)
        {
        }

        internal static void LogUnsafeTransformMethod(
            SignedXml signedXml,
            string algorithm,
            IEnumerable<string> validC14nAlgorithms,
            IEnumerable<string> validTransformAlgorithms)
        {
        }

        internal static void LogVerifyReference(SignedXml signedXml, Reference reference)
        {
        }

        internal static void LogSignedXmlRecursionLimit(SignedXml signedXml,
                                                       Reference reference)
        {
        }

        internal static void LogVerifyReferenceHash(SignedXml signedXml,
                                                   Reference reference,
                                                   byte[] actualHash,
                                                   byte[] expectedHash)
        {
        }
        }
}