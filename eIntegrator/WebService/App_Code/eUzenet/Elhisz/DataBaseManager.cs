﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace eUzenet.Elhisz
{
    /// <summary>
    /// Summary description for DataBaseManager
    /// </summary>
    public class DataBaseManager
    {
        public static String GetConnectionString()
        {
            ConnectionStringSettings conn = new ConnectionStringSettings();
            conn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ElhitetdConnectionString"];

            return conn.ConnectionString;
        }

        string schema;
        public DataBaseManager()
        {
            schema = IntegratorParameterek.GetParameter(IntegratorParameterek.eUzenetSchema);

            if (String.IsNullOrEmpty(schema))
            {
                schema = "elhiszdb";
            }
        }

        private string GetCommandText(string cmd)
        {
            return cmd.Replace("#schema#", schema);
        }

        const string _cmdGetSubmissionInformations = @"select si.registrationnumber ""PR_ErkeztetesiSzam"",
                                                            si.CREATEDATE ""KR_ErkeztetesiDatum"",
                                                            si.SUBMISSIONFORMID ""UzenetTipusa"",
                                                            si.SUBMITTINGMODULEID ""KuldoRendszer"",
                                                            si.DokTipusHivatal ""KR_DokTipusHivatal"",
                                                            si.DokTipusAzonosito ""KR_DokTipusAzonosito"",
                                                            si.DokTipusLeiras ""KR_DokTipusLeiras"",
                                                            si.Res_KR_RegistrationNumber ""KR_HivatkozasiSzam"",
                                                            si.Res_NMHH_ResolutionNumber ""Contentum_HivatkozasiSzam"",
                                                            si.Res_NMHH_RegistrationNumber ""PR_HivatkozasiSzam"",
                                                            si.Sub_KR_RegistrationNumber ""KR_ErkeztetesiSzam"",
                                                            si.Submission_Reference,
                                                            si.NMHH_OU,
                                                            si.PARTNER_CODE ""PartnerKapcsolatiKod"",
                                                            si.Partner_Name ""PartnerNev"",
                                                            si.OrganizationUnit ""PartnerRovidNev"",
                                                            si.Partner_Email ""PartnerEmail"",
                                                            si.Submission_Reference ""Contentum_HivatkozasiSzam2""
                                                from #schema#.SubmissionInformation si, #schema#.SubmissionData sd
                                                where si.ActualState in (4, 6)
                                                and sd.completecontentsize is not null
                                                and si.RegistrationNumber = sd.RegistrationNumber
                                                and si.SubmissionFormId in ({0})";
        public DataSet GetSubmissionInformations()
        {
            Logger.Info("DataBaseManager.GetSubmissionInformations kezdete");

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                string urlapok = GetUrlapok();
                string _cmd = String.Format(GetCommandText(_cmdGetSubmissionInformations), urlapok);

                OracleCommand cmd = new OracleCommand(_cmd, conn);
                cmd.CommandType = CommandType.Text;

                DataSet ds = new DataSet();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                Logger.Info("DataBaseManager.GetSubmissionInformations vege");
                return ds;

            }
        }

        private string GetUrlapok()
        {
            Logger.Debug("GetUrlapok kezdete");

            string kodCsoport = "ELHISZ_URLAPOK";
            Contentum.eBusinessDocuments.ExecParam execParam = EdokWrapper.GetExecParam();
            var kodtarak = GetKodtarakByKodCsoportList(kodCsoport, execParam);

            Logger.Debug(String.Format("Urlapok száma: {0}", kodtarak.Count));

            List<string> kodok = new List<string>();
            foreach (var kodtar in kodtarak)
            {
                kodok.Add(kodtar.Kod);
            }

            string inner = String.Join("','", kodok.ToArray());
            inner = "'" + inner + "'";

            Logger.Debug("GetUrlapok vege");
            return inner;

        }

        const string _cmdGetSubmissionData = @"select sd.registrationnumber ""PR_ErkeztetesiSzam"", sd.completecontent ""Content""
                                                    from #schema#.SubmissionData sd
                                                    where registrationNumber = :registrationNumber";
        public DataSet GetSubmissionData(string registrationNumber)
        {
            Logger.Info("DataBaseManager.GetSubmissionData kezdete");
            Logger.Debug(String.Format("registrationNumber={0}", registrationNumber));

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(GetCommandText(_cmdGetSubmissionData), conn);
                cmd.CommandType = CommandType.Text;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("registrationNumber", registrationNumber));

                DataSet ds = new DataSet();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                Logger.Info("DataBaseManager.GetSubmissionData vege");

                return ds;

            }
        }

        const string _cmdUpdateSubmissionInformation = @"update #schema#.SubmissionInformation
                                                         set ActualState = :actualState
                                                         where RegistrationNumber = :registrationNumber";

        const int actualState = 9;
        public int UpdateSubmissionInformation(string registrationNumber)
        {
            Logger.Info("DataBaseManager.UpdateSubmissionInformation kezdete");
            Logger.Debug(String.Format("registrationNumber={0}", registrationNumber));

            using (OracleConnection conn = new OracleConnection(GetConnectionString()))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(GetCommandText(_cmdUpdateSubmissionInformation), conn);
                cmd.CommandType = CommandType.Text;
                cmd.BindByName = true;

                cmd.Parameters.Add(new OracleParameter("registrationNumber", registrationNumber));
                cmd.Parameters.Add(new OracleParameter("actualState", actualState));

                int ret = cmd.ExecuteNonQuery();

                Logger.Info("DataBaseManager.UpdateSubmissionInformation vege");

                return ret;

            }
        }

        List<KodTar_Cache.KodTarElem> GetKodtarakByKodCsoportList(String kodcsoport_kod, ExecParam execParam)
        {
            DateTime ervenyesseg = DateTime.Now;
            List<KodTar_Cache.KodTarElem> kodtarakDictionary = new List<KodTar_Cache.KodTarElem>();
            Contentum.eAdmin.Service.KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            Result result = service.GetAllByKodcsoportKod(execParam, kodcsoport_kod);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    KodTar_Cache.KodTarElem kte = new KodTar_Cache.KodTarElem(row);
                    if (kte.IsErvenyes(ervenyesseg))
                    {
                        kodtarakDictionary.Add(kte);
                    }
                }
                return kodtarakDictionary;

            }

            return null;
        }
    }
    
}