﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace eUzenet.Elhisz
{
    /// <summary>
    /// Summary description for EdokWrapper
    /// </summary>
    public class EdokWrapper
    {
        static string eDok_FelhasznaloId;
        static EdokWrapper()
        {
            eDok_FelhasznaloId = ConfigurationManager.AppSettings["KR_eDok_FelhasznaloId"];
        }

        public static ExecParam GetExecParam()
        {
            ExecParam param = new ExecParam();
            param.Felhasznalo_Id = eDok_FelhasznaloId;
            return param;
        }
    }
}