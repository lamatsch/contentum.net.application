﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Contentum.eUtility.eBeadvany;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace eUzenet.Elhisz
{
    /// <summary>
    /// Summary description for ElhiszManager
    /// </summary>
    public class ElhiszManager
    {
        private const string CONST_ASICE = "asice";
        private const string CONST_ASICE_WITH_DOT = ".asice";
        private static readonly object _lockObject = new object();
        public ElhiszManager()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public void PostaFiokFeldolgozas()
        {
            lock (_lockObject)// prevent to start download in parallel. If not used the same Küldemény will be downloaded multiple times.
            {
                Logger.Info("ElhiszManager.PostaFiokFeldolgozas kezdete");

                try
                {

                    List<EREC_eBeadvanyok> eBeadvanyok = GeteBeadvanyok();

                    EREC_eBeadvanyokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
                    service.Timeout = (int)TimeSpan.FromMinutes(20).TotalMilliseconds;
                    ExecParam execParam = EdokWrapper.GetExecParam();
                    DataBaseManager db = new DataBaseManager();

                    foreach (EREC_eBeadvanyok eBeadvany in eBeadvanyok)
                    {
                        try
                        {
                            string erkeztetesiSzam = eBeadvany.PR_ErkeztetesiSzam;

                            if (string.IsNullOrEmpty(eBeadvany.PR_HivatkozasiSzam))
                            {
                                eBeadvany.PR_HivatkozasiSzam = new eBeadvanyFormXmlUtility().GetPRHivatkozasiSzamFromFormXml(eBeadvany, execParam) ?? String.Empty;
                            }

                            if (!string.IsNullOrEmpty(eBeadvany.PR_HivatkozasiSzam))
                            {
                                eBeadvany.Contentum_HivatkozasiSzam = GetContentumHivatkozasId(eBeadvany.PR_HivatkozasiSzam, string.Empty);
                            }

                            eBeadvany.FeladoTipusa = eBeadvanyUtility.FeladoTipusMeghatarozasa(eBeadvany.PartnerKapcsolatiKod);

                            Logger.Debug(string.Format("eBedvany feldolgozása: {0}", erkeztetesiSzam));

                            eBeadvanyokCsatolmanyokkal eBeadvanyCsatolmanyokkal = GetCsatolmanyok(eBeadvany);

                            Logger.Debug(string.Format("EREC_eBeadvanyokService.InsertAndAttachDocument"));

                            Result res = service.InsertAndAttachDocument(execParam, eBeadvanyCsatolmanyokkal.eBeadvany, eBeadvanyCsatolmanyokkal.GetAllCsatolmany().ToArray());

                            if (res.IsError)
                            {
                                throw new ResultException(res);
                            }

                            db.UpdateSubmissionInformation(erkeztetesiSzam);

                            string eBeadvanyId = res.Uid;
                            foreach (Csatolmany csatolmany in eBeadvanyCsatolmanyokkal.Csatolmanyok)
                            {
                                HKP_KRService.Core.EdokWrapper.ProcessKRXFile(execParam, eBeadvanyId, eBeadvanyCsatolmanyokkal.eBeadvany.KR_ErkeztetesiSzam, csatolmany);
                            }

                            service.ExecuteEUzenetSzabaly(eBeadvanyId, execParam);

                            eBeadvanyCsatolmanyokkal = null;

                        }
                        catch (Exception ex)
                        {
                            Logger.Error(string.Format("eBedvany feldolgozási hiba: {0}", eBeadvany.PR_ErkeztetesiSzam), ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(string.Format("ElhiszManager.PostaFiokFeldolgozas hiba"), ex);
                }

                Logger.Info("ElhiszManager.PostaFiokFeldolgozas vege");
            }

        }

        public List<EREC_eBeadvanyok> GeteBeadvanyok()
        {
            Logger.Info("ElhiszManager.GeteBeadvanyok kezdete");

            DataBaseManager db = new DataBaseManager();
            DataSet ds = db.GetSubmissionInformations();

            List<EREC_eBeadvanyok> eBeadvanyok = new List<EREC_eBeadvanyok>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string erkeztetesiSzam = row["PR_ErkeztetesiSzam"].ToString();

                if (!ErkeztetesiSzamExists(erkeztetesiSzam))
                {
                    EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();

                    //Default adatok
                    eBeadvany.Irany = "0"; //Bejövő
                    eBeadvany.Cel = "WEB";

                    eBeadvany.PR_ErkeztetesiSzam = row["PR_ErkeztetesiSzam"].ToString();
                    eBeadvany.UzenetTipusa = row["UzenetTipusa"].ToString();
                    eBeadvany.KR_DokTipusHivatal = row["KR_DokTipusHivatal"].ToString();
                    eBeadvany.KR_DokTipusAzonosito = row["KR_DokTipusAzonosito"].ToString();
                    eBeadvany.KR_DokTipusLeiras = row["KR_DokTipusLeiras"].ToString();
                    eBeadvany.KR_HivatkozasiSzam = row["KR_HivatkozasiSzam"].ToString();
                    eBeadvany.Contentum_HivatkozasiSzam = GetContentumHivatkozasId(row["Contentum_HivatkozasiSzam"].ToString(), row["Contentum_HivatkozasiSzam2"].ToString());
                    eBeadvany.PR_HivatkozasiSzam = row["PR_HivatkozasiSzam"].ToString();
                    eBeadvany.KR_ErkeztetesiSzam = row["KR_ErkeztetesiSzam"].ToString();
                    eBeadvany.PartnerKapcsolatiKod = row["PartnerKapcsolatiKod"].ToString();
                    eBeadvany.PartnerNev = row["PartnerNev"].ToString();
                    eBeadvany.PartnerRovidNev = row["PartnerRovidNev"].ToString();
                    eBeadvany.PartnerEmail = row["PartnerEmail"].ToString();
                    eBeadvany.KR_ErkeztetesiDatum = row["KR_ErkeztetesiDatum"].ToString(); // BUG_8380

                    eBeadvany.KuldoRendszer = eBeadvanyUtility.ErkezesModjaMeghatarozasa(row["KuldoRendszer"].ToString(), row["PartnerKapcsolatiKod"].ToString());

                    eBeadvanyok.Add(eBeadvany);
                }
            }

            Logger.Info("ElhiszManager.GeteBeadvanyok vege");
            return eBeadvanyok;
        }

        /// <summary>
        /// Iktatószám feloldása
        /// </summary>
        /// <param name="azonosito"></param>
        /// <returns></returns>
        private string GetContentumHivatkozasId(string azonosito1, string azonosito2)
        {
            if (string.IsNullOrEmpty(azonosito1) && string.IsNullOrEmpty(azonosito2))
                return string.Empty;

            #region FIND AZONOSITO IN IRAT

            EREC_IraIratokService serviceIrat = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParamIrat = EdokWrapper.GetExecParam();
            EREC_IraIratokSearch searchIrat = new EREC_IraIratokSearch();

            if (!string.IsNullOrEmpty(azonosito1) && !string.IsNullOrEmpty(azonosito2))
            {
                searchIrat.Azonosito.Value = Search.GetSqlInnerString(new string[] { azonosito1, azonosito2 });
                searchIrat.Azonosito.Operator = Contentum.eQuery.Query.Operators.inner;
            }
            else if (!string.IsNullOrEmpty(azonosito1))
            {
                searchIrat.Azonosito.Value = azonosito1;
                searchIrat.Azonosito.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            else if (!string.IsNullOrEmpty(azonosito2))
            {
                searchIrat.Azonosito.Value = azonosito2;
                searchIrat.Azonosito.Operator = Contentum.eQuery.Query.Operators.equals;
            }
            else
            {
                return string.Empty;
            }

            searchIrat.TopRow = 1;
            Result resIrat = serviceIrat.GetAll(execParamIrat, searchIrat);

            if (resIrat != null && !resIrat.IsError)
            {
                if (resIrat.Ds.Tables[0].Rows.Count > 0)
                {
                    Logger.Info(string.Format("ElhiszManager.IratAzonosito letezik : {0} {1}", azonosito1, azonosito2));
                    return resIrat.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
            }
            else
            {
                Logger.Error("EREC_IraIratokService.GetAll hiba", execParamIrat, resIrat);
            }
            #endregion

            return string.Empty;
        }

        private Csatolmany GetCsatolmany(string erkeztetesiSzam)
        {
            Logger.Info("ElhiszManager.GetCsatolmany kezdete");
            Logger.Debug(string.Format("erkeztetesiSzam={0}", erkeztetesiSzam));

            DataBaseManager db = new DataBaseManager();
            DataSet ds = db.GetSubmissionData(erkeztetesiSzam);

            if (ds.Tables[0].Rows.Count == 0)
            {
                throw new Exception(string.Format("Csatolmány nem található: {0}", erkeztetesiSzam));
            }

            DataRow row = ds.Tables[0].Rows[0];

            Csatolmany csatolmany = new Csatolmany();
            csatolmany.Nev = string.Format("{0}.asice", erkeztetesiSzam);
            csatolmany.Tartalom = (byte[])row["Content"];

            Logger.Info("ElhiszManager.GetCsatolmany vege");

            return csatolmany;
        }

        void ProcessAsiceFile(eBeadvanyokCsatolmanyokkal eBeadvany)
        {
            Logger.Info("ElhiszManager.ProcessAsiceFile kezdete");

            Csatolmany asiceFile = eBeadvany.AsiceFile;

            string error;
            List<string> referencedFiles = new List<string>();
            AsiceFileManager manager = new AsiceFileManager();

            if (manager.ProcessFile(asiceFile.Nev, asiceFile.Tartalom, out referencedFiles, out error))
            {
                List<Csatolmany> referencedCsatolmanyok = GetReferencedCsatolmanyok(referencedFiles);
                eBeadvany.AddCsatolmanyok(referencedCsatolmanyok);
                eBeadvany.eBeadvany.KR_Megjegyzes = manager.GetMegjegyzes(referencedFiles);
                manager.DeleteTempDirectory();
            }
            else
            {
                manager.DeleteTempDirectory();
                throw new Exception(error);
            }

            Logger.Info("ElhiszManager.ProcessAsiceFile vege");
        }

        private List<Csatolmany> GetReferencedCsatolmanyok(List<string> referencedFiles)
        {
            Logger.Info("ElhiszManager.GetReferencedCsatolmanyok kezdete");

            List<Csatolmany> csatolmanyok = new List<Csatolmany>();
            foreach (string file in referencedFiles)
            {
                Csatolmany csatolmany = new Csatolmany();
                csatolmany.Nev = Path.GetFileName(file);
                csatolmany.Tartalom = File.ReadAllBytes(file);
                csatolmany.SablonAzonosito = AsiceFileManager.GetUrlapSablon(file);
                csatolmanyok.Add(csatolmany);
            }

            Logger.Info("ElhiszManager.GetReferencedCsatolmanyok vege");

            return csatolmanyok;
        }

        public bool ErkeztetesiSzamExists(string erkeztetesiSzam)
        {
            Logger.Info("ElhiszManager.ErkeztetesiSzamExists kezdete");
            Logger.Debug(string.Format("erkeztetesiSzam={0}", erkeztetesiSzam));

            if (!string.IsNullOrEmpty(erkeztetesiSzam))
            {
                try
                {
                    EREC_eBeadvanyokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
                    ExecParam execParam = EdokWrapper.GetExecParam();
                    EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
                    search.PR_ErkeztetesiSzam.Value = erkeztetesiSzam;
                    search.PR_ErkeztetesiSzam.Operator = Contentum.eQuery.Query.Operators.equals;

                    Result res = service.GetAll(execParam, search);

                    if (!res.IsError)
                    {
                        bool exists = res.Ds.Tables[0].Rows.Count > 0;
                        Logger.Info(string.Format("ElhiszManager.ErkeztetesiSzamExists vege: {0}", exists));
                        return exists;
                    }
                    else
                    {
                        Logger.Error("EREC_eBeadvanyokService.GetAll hiba", execParam, res);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("ElhiszManager.ErkeztetesiSzamExists hiba", ex);
                }
            }

            return false;
        }

        public eBeadvanyokCsatolmanyokkal GetCsatolmanyok(EREC_eBeadvanyok eBeadvany)
        {
            eBeadvanyokCsatolmanyokkal eBeadvanyCsatolmanyokkal = new eBeadvanyokCsatolmanyokkal(eBeadvany);

            Csatolmany csatolmany = GetCsatolmany(eBeadvany.PR_ErkeztetesiSzam);
            eBeadvanyCsatolmanyokkal.AsiceFile = csatolmany;

            if (eBeadvanyCsatolmanyokkal.AsiceFile != null && eBeadvanyCsatolmanyokkal.AsiceFile.Nev.EndsWith(CONST_ASICE_WITH_DOT, StringComparison.CurrentCultureIgnoreCase) && !string.IsNullOrEmpty(eBeadvany.UzenetTipusa))
            {
                eBeadvanyCsatolmanyokkal.AsiceFile.Nev = string.Format("{0}.{1}", eBeadvany.UzenetTipusa, CONST_ASICE);
            }
            eBeadvany.KR_FileNev = csatolmany.Nev;

            ProcessAsiceFile(eBeadvanyCsatolmanyokkal);

            //Bug_5944_E-beadvány_XML_állomány_átnevezése
            foreach (Csatolmany csat in eBeadvanyCsatolmanyokkal.Csatolmanyok)
            {
                if ("form.xml".Equals(csat.Nev, StringComparison.CurrentCultureIgnoreCase))
                {
                    csat.Nev = string.Format("{0}.xml", eBeadvany.UzenetTipusa);
                    break;
                }
            }

            return eBeadvanyCsatolmanyokkal;
        }
    }
}