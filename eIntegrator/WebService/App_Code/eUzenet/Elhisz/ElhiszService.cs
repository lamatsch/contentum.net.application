﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using eUzenet.HKP_KRService.Core;

namespace eUzenet.Elhisz
{
    /// <summary>
    /// Summary description for ElhiszService
    /// </summary>
    public class ElhiszService : IeUzenetService
    {
        public ElhiszService()
        {           
        }

        public Result Azonositas(string fiok, HKP2.AzonositasKerdes azonKerdes)
        {
            throw new NotImplementedException();
        }

        public Result CsoportosDokumentumFeltoltes(string fiok, List<Csatolmany> csatolmanyList, List<EREC_eBeadvanyok> dokAdatokList)
        {
            return new HKP_KRService.TestKRService().CsoportosDokumentumFeltoltes(fiok, csatolmanyList, dokAdatokList);
        }

        public Result CsoportosDokumentumLetoltesVisszaigazolas(List<HKP2.VisszaigazolasAdatok> visszaIgAdatok)
        {
            throw new NotImplementedException();
        }

        public Result HivatalokListajaFeldolgozas_Szurt(string fiok, HivatalSzuroParameterek szuroParameterek)
        {
            throw new NotImplementedException();
        }

        public void PostaFiokFeldolgozas()
        {
            
                ElhiszManager manager = new ElhiszManager();
                manager.PostaFiokFeldolgozas();
            
        }
    }
}