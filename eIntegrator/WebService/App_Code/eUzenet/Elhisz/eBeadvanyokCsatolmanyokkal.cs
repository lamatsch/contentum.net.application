﻿using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eUzenet.Elhisz
{
    /// <summary>
    /// Summary description for eBeadvanyokCsatolmanyokkal
    /// </summary>
    public class eBeadvanyokCsatolmanyokkal
    {
        EREC_eBeadvanyok _eBeadvany;

        public EREC_eBeadvanyok eBeadvany
        {
            get { return _eBeadvany; }
            set { _eBeadvany = value; }
        }

        Csatolmany _AsiceFile = null;

        public Csatolmany AsiceFile
        {
            get
            {
                return _AsiceFile;
            }

            set
            {
                _AsiceFile = value;
            }
        }

        List<Csatolmany> _Csatolmanyok = new List<Csatolmany>();

        public List<Csatolmany> Csatolmanyok
        {
            get
            {
                return _Csatolmanyok;
            }

            set
            {
                _Csatolmanyok = value;
            }
        }

        public eBeadvanyokCsatolmanyokkal(EREC_eBeadvanyok eBeadvany)
        {
            _eBeadvany = eBeadvany;
        }

        public void AddCsatolmany(Csatolmany csatolmany)
        {
            _Csatolmanyok.Add(csatolmany);
        }

        public void AddCsatolmanyok(List<Csatolmany> csatolmanyok)
        {
            _Csatolmanyok.AddRange(csatolmanyok);
        }

        public List<Csatolmany> GetAllCsatolmany()
        {
            List<Csatolmany> csatolmanyok = new List<Csatolmany>();
            csatolmanyok.Add(AsiceFile);
            csatolmanyok.AddRange(_Csatolmanyok);
            return csatolmanyok;
        }

    }
}