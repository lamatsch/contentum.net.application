﻿using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace eUzenet.FileManager
{
    /// <summary>
    /// Summary description for AbevJavaManager
    /// </summary>
    public class AbevJavaManager
    {
        static string abevJavaFilePath;

        public static string AbevJavaFilePath
        {
            get
            {
                return abevJavaFilePath;
            }
        }

        const string cmdArgumet = "cmd:\"pdf.print.xml.silent {0};{1}\"";
        static AbevJavaManager()
        {
            abevJavaFilePath = ConfigurationManager.AppSettings.Get("KR_AbevJava_FilePath");
        }

        static string GetOuputFilePath(string filePath)
        {
            return String.Format("{0}.txt", filePath);
        }

        static string GetOuputDirectory(string filePath)
        {
            return new FileInfo(filePath).DirectoryName;
        }

        static string GetHibaFilePath(string outputFilePath)
        {
            return String.Format("{0}_hiba", outputFilePath);
        }

        public static string PrintXmlToPDF(string filePath, string outputDirectory, out string error)
        {
            Logger.Info(String.Format("AbevJavaManager.PrintPDF indul: {0}", filePath));
            error = null;

            if (!File.Exists(abevJavaFilePath))
            {
                error = String.Format("AbevJavaManager.PrintPDF hiba, batch file nem talalhato: {0}", abevJavaFilePath);
                Logger.Error(error);
                return null;
            }

            if (!File.Exists(filePath))
            {
                error = String.Format("AbevJavaManager.PrintPDF hiba, input file nem talalhato: {0}", filePath);
                Logger.Error(error);
                return null;
            }

            try
            {
                string outpuFilePath = GetOuputFilePath(filePath);

                Logger.Debug(String.Format("outpuFilePath: {0}", outpuFilePath));

                string args = String.Format(cmdArgumet, filePath, outpuFilePath);

                Logger.Debug(String.Format("Process: {0} {1}", abevJavaFilePath, args));

                Process p = Process.Start(abevJavaFilePath, args);
                p.WaitForExit();
                int result = p.ExitCode;

                Logger.Debug(String.Format("ExitCode: {0}", result));

                if (result == 0)
                {
                    string[] lines = null;
                    if (File.Exists(outpuFilePath))
                    {
                        lines = File.ReadAllLines(outpuFilePath);
                    }
                    else
                    {
                        error = String.Format("AbevJavaManager.PrintPDF hiba, output file nem talalhato: {0}", outpuFilePath);
                        Logger.Error(error);
                    }

                    if (lines != null && lines.Length > 0)
                    {
                        string pdfFilePath = lines[0];

                        Logger.Debug(String.Format("pdfFilePath: {0}", pdfFilePath));

                        string destFilePath = Path.Combine(outputDirectory, Path.GetFileName(pdfFilePath));

                        Logger.Debug(String.Format("destFilePath: {0}", destFilePath));

                        Logger.Debug(String.Format("File.Move: {0} - {1}", pdfFilePath, destFilePath));

                        File.Move(pdfFilePath, destFilePath);

                        Logger.Debug(String.Format("File.Delete: {0}", outpuFilePath));

                        File.Delete(outpuFilePath);

                        return destFilePath;
                    }
                    else
                    {
                        string hibaFilePath = GetHibaFilePath(outpuFilePath);
                        Logger.Debug(String.Format("hibaFilePath: {0}", hibaFilePath));

                        if (File.Exists(hibaFilePath))
                        {
                            error = String.Format("AbevJavaManager.PrintPDF hiba: {0}", File.ReadAllText(hibaFilePath));
                            Logger.Error(error);
                        }
                    }
                }
                else
                {
                    error = String.Format("AbevJavaManager.PrintPDF hiba, batch exit code: {0}", result);
                    Logger.Error(error);
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                Logger.Error("AbevJavaManager.PrintPDF hiba", ex);
            }

            return null;
        }
    }
}