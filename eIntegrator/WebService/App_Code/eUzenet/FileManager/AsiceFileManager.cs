﻿using Contentum.eUtility;
using eUzenet.Cryptography;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

/// <summary>
/// Summary description for AsiceFileManager
/// </summary>
public class AsiceFileManager
{
    Guid tempDir = Guid.NewGuid();

    public AsiceFileManager()
    {
    }
    string GetTempPath()
    {
        return Path.GetTempPath();
    }

    string GetOutputDir()
    {
        return Path.Combine(GetTempPath(), tempDir.ToString());
    }

    static void CreateDirectory(string directoryPath)
    {
        Logger.Debug(String.Format("CreateDirectory: {0}", directoryPath));
        Directory.CreateDirectory(directoryPath);
    }

    public bool ProcessFile(string fileName, byte[] fileContent, out List<string> referencedFiles, out string error)
    {
        Logger.Info(String.Format("AsiceFileManager.ProcessFile indul: {0}", fileName));
        referencedFiles = null;
        error = null;

        try
        {
            string outDir = GetOutputDir();

            Logger.Debug(String.Format("ExtractZipFile: {0}", outDir));

            ExtractZipFile(fileContent, outDir);

            string sigFilePath = GetSigFile(outDir);

            Logger.Debug(String.Format("VerifyXmlFile: {0}", sigFilePath));

            bool check = VerifyXmlFile(outDir, sigFilePath);

            if (!check)
            {
                error = "Az asice fájl ellenőrzése sikertelen!";
                Logger.Error(error);
                return false;
            }

            Logger.Debug(String.Format("GetReferencedFiles: {0}", sigFilePath));

            referencedFiles = GetReferencedFiles(outDir, sigFilePath);

            return true;
        }
        catch (Exception ex)
        {
            error = ex.ToString();
            Logger.Error("AsiceFileManager.ProcessFile hiba", ex);
        }

        Logger.Info(String.Format("AsiceFileManager.ProcessFile indul: {0}", fileName));

        return false;
    }

    public void DeleteTempDirectory()
    {
        Logger.Info("DeleteTempDirectory kezdete");
        try
        {
            string outDir = GetOutputDir();
            Logger.Debug(String.Format("outDir={0}", outDir));
            Directory.Delete(outDir, true);
        }
        catch (Exception ex)
        {
            Logger.Error("DeleteTempDirectory hiba", ex);
        }
        Logger.Info("DeleteTempDirectory vege");
    }

    static void ExtractZipFile(byte[] fileContent, string extractPath)
    {
        MemoryStream inputStream = new MemoryStream(fileContent);
        FastZip fastZip = new FastZip();
        fastZip.ExtractZip(inputStream, extractPath, FastZip.Overwrite.Always, null, null, null, true, true);
    }

    static string GetSigFile(string directoryPath)
    {
        DirectoryInfo di = new DirectoryInfo(directoryPath);

        DirectoryInfo[] diMeta = di.GetDirectories("META-INF");

        if (diMeta.Length > 0)
        {
            FileInfo[] xmlFiles = diMeta[0].GetFiles("*.xml");

            if (xmlFiles.Length == 1)
            {
                return xmlFiles[0].FullName;
            }
        }

        return null;
    }

    static bool VerifyXmlFile(string basePath, string fileName)
    {
        // Create a new XML document.
        XmlDocument xmlDocument = new XmlDocument();

        // Format using white spaces.
        xmlDocument.PreserveWhitespace = true;

        // Load the passed XML file into the document. 
        xmlDocument.Load(fileName);

        // Create a new SignedXml object and pass it
        // the XML document class.
        CustomSignedXml signedXml = new CustomSignedXml(xmlDocument);

        signedXml.BasePath = basePath;

        // Find the "Signature" node and create a new
        // XmlNodeList object.
        XmlNodeList nodeList = xmlDocument.SelectNodes("//*[local-name()='Signature']");

        // Load the signature node.
        signedXml.LoadXml((XmlElement)nodeList[0]);

        // Check the signature and return the result.
        return signedXml.CheckSignature(); 

    }

    static List<string> GetReferencedFiles(string baseDirectory, string sigFile)
    {
        XDocument doc = XDocument.Load(sigFile);

        var references = from signature in doc.Root.Elements()
                         from signedInfo in signature.Elements()
                         from reference in signedInfo.Elements()
                         where signature.Name.LocalName == "Signature"
                         && signedInfo.Name.LocalName == "SignedInfo"
                         && reference.Name.LocalName == "Reference"
                         && reference.Attribute("URI") != null
                         select reference.Attribute("URI").Value;

        var resolvedUrls = from url in references
                           where IsFileUrl(url)
                           select ResolveUrl(baseDirectory, url);

        return resolvedUrls.ToList();
    }

    static string ResolveUrl(string baseDirectory, string url)
    {
        Uri baseUri = new Uri(Path.Combine(baseDirectory, "x"));
        Uri uri = new Uri(baseUri, url);
        return uri.LocalPath;
    }

    static bool IsFileUrl(string url)
    {
        if (!String.IsNullOrEmpty(url) && url[0] != '#')
        {
            return true;
        }

        return false;
    }

    public string GetMegjegyzes(List<string> referencedFiles)
    {
        Logger.Debug("AsiceFileManager.GetMegjegyzes kezdete");

        try
        {

            foreach (string filePath in referencedFiles)
            {
                FileInfo fi = new FileInfo(filePath);

                if (fi.Exists)
                {
                    string fileName = fi.Name;
                    string directoryName = fi.Directory.Name;

                    if ("form.xml".Equals(fileName, StringComparison.CurrentCultureIgnoreCase)
                        && "signed_xml".Equals(directoryName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        Logger.Debug(String.Format("form: {0}", filePath));

                        XDocument doc = XDocument.Load(filePath);

                        var megjegyzesek = from a2 in doc.Root.Elements()
                                           from a7 in a2.Elements()
                                           from a10 in a7.Elements()
                                           where doc.Root.Name.LocalName == "esf00998"
                                           && a2.Name.LocalName == "A2"
                                           && a7.Name.LocalName == "A7"
                                           && a10.Name.LocalName == "A10"
                                           select a10.Value;

                        string megjegyzes = megjegyzesek.FirstOrDefault();

                        Logger.Debug(String.Format("megjegyzes: {0}", megjegyzes));

                        if (String.IsNullOrEmpty(megjegyzes))
                            return String.Empty;

                        return megjegyzes;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("AsiceFileManager.GetMegjegyzes hiba", ex);
        }

        return String.Empty;
    }

    public static string GetUrlapSablon(string filePath)
    {
        Logger.Debug("AsiceFileManager.GetUrlapSablon kezdete");
        Logger.Debug(String.Format("filePath={0}", filePath));

        string sablon = null;

        try
        {
            FileInfo fi = new FileInfo(filePath);
            string fileName = fi.Name;
            string directoryName = fi.Directory.Name;

            if ("form.xml".Equals(fileName, StringComparison.CurrentCultureIgnoreCase))
            {
                if ("signed_xml".Equals(directoryName, StringComparison.CurrentCultureIgnoreCase))
                {
                    sablon = "ADATKAPU";
                }
                else if ("signed_files".Equals(directoryName, StringComparison.CurrentCultureIgnoreCase))
                {
                    sablon = "MEDIASZABALYOZAS";
                }
                else
                {
                    sablon = "ELEKTRONIKUSBEADVANY";
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("AsiceFileManager.GetUrlapSablon hiba", ex);
        }

        Logger.Debug(String.Format("AsiceFileManager.GetUrlapSablon vege: {0}", sablon));
        return sablon;
    }
}