﻿using Contentum.eQuery.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace eUzenet.FileManager
{
    /// <summary>
    /// Summary description for CstFileManager
    /// </summary>
    public class CstFileManager
    {
        static readonly XNamespace ns = "http://www.apeh.hu/abev/csatolmanyok/2007/01";
        static readonly XName xnFileInformacio = ns + "fileinformacio";
        static readonly XName xnCsatolmanyFile = ns + "csatolmanyfile";
        static readonly XName xnFileNev = ns + "filenev";
        static readonly XName xnAzonosito = "azonosito";

        public static void Process(string fileName, string outputDirectory, out string error)
        {
            Logger.Info(String.Format("CstFileManager.Process indul: {0}, {1}", fileName, outputDirectory));
            error = null;

            try
            {
                XDocument doc = XDocument.Load(fileName);

                var fileInformaciok = from info in doc.Root.Descendants(xnFileInformacio)
                                      select new FileInformacio
                                      {
                                          Azonosito = info.Attribute(xnAzonosito).Value,
                                          FileNev = info.Element(xnFileNev).Value
                                      };

                foreach (FileInformacio info in fileInformaciok)
                {
                    var file = (from cst in doc.Root.Descendants(xnCsatolmanyFile)
                                where cst.Attribute(xnAzonosito).Value == info.Azonosito
                                select cst.Value).FirstOrDefault();

                    string filePath = Path.Combine(outputDirectory, info.FileNev);
                    byte[] content = Convert.FromBase64String(file);
                    Logger.Debug(String.Format("Write file: {0}", filePath));
                    File.WriteAllBytes(filePath, content);
                }

                Logger.Info("CstFileManager.Process vege");
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                Logger.Error("CstFileManager.Process hiba", ex);
            }
        }

        class FileInformacio
        {
            public string Azonosito { get; set; }
            public string FileNev { get; set; }
        }
    }
}