﻿using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace eUzenet.FileManager
{
    /// <summary>
    /// Summary description for KRFileManager
    /// </summary>
    public class KRFileManager
    {
        static string GetTempPath()
        {
            return Path.GetTempPath();
        }

        static string GetTempDir(string erkeztetoSzam)
        {
            return Path.Combine(GetTempPath(), erkeztetoSzam);
        }

        static string GetInputDir(string erkeztetoSzam)
        {
            return Path.Combine(GetTempDir(erkeztetoSzam), "In");
        }

        static string GetKROutputDir(string erkeztetoSzam)
        {
            return Path.Combine(GetTempDir(erkeztetoSzam), "KROut");
        }

        static string GetOutputDir(string erkeztetoSzam)
        {
            return Path.Combine(GetTempDir(erkeztetoSzam), "Out");
        }

        public static string ProcessFile(string erkeztetoSzam, string fileName, byte[] fileContent, out string error)
        {
            Logger.Info(String.Format("KRFileManager.ProcessFile indul: {0}, {1}", erkeztetoSzam, fileName));
            error = null;

            try
            {
                Logger.Debug("CreateDirectories");

                CreateDirectories(erkeztetoSzam);

                string tempFile = Path.Combine(GetInputDir(erkeztetoSzam), fileName);

                Logger.Debug(String.Format("File mentese indul: {0}", tempFile));

                File.WriteAllBytes(tempFile, fileContent);

                string krOutputDir = GetKROutputDir(erkeztetoSzam);

                KRTitokManager.Kititkosit(tempFile, krOutputDir, out error);

                if (error == null)
                {
                    string outputDirectory = GetOutputDir(erkeztetoSzam);

                    DirectoryInfo di = new DirectoryInfo(krOutputDir);

                    if (di.Exists)
                    {
                        foreach (FileInfo fi in di.GetFiles("*.*", SearchOption.AllDirectories))
                        {
                            Logger.Debug(String.Format("File: {0}", fi.FullName));

                            string ext = fi.Extension;

                            //űrlap
                            if (".xml".Equals(ext, StringComparison.CurrentCultureIgnoreCase))
                            {
                                AbevJavaManager.PrintXmlToPDF(fi.FullName, outputDirectory, out error);
                                continue;
                            }
                            //csatolmány
                            if (".cst".Equals(ext, StringComparison.CurrentCultureIgnoreCase))
                            {
                                CstFileManager.Process(fi.FullName, outputDirectory, out error);
                                continue;
                            }
                        }
                    }

                    return outputDirectory;
                }
                else
                {
                    Logger.Error(String.Format("KRFileManager.ProcessFile hiba: {0}", error));
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                Logger.Error("KRFileManager.ProcessFile hiba", ex);
            }

            return null;
        }

        public static void DeleteTempDirectory(string erkeztetoSzam)
        {
            Logger.Info(String.Format("KRFileManager.DeleteTempDirectory indul: {0}", erkeztetoSzam));

            try
            {
                string tempDir = GetTempDir(erkeztetoSzam);

                Directory.Delete(tempDir, true);
            }
            catch (Exception ex)
            {
                Logger.Error("KRFileManager.DeleteTempDirectory hiba", ex);
            }
        }

        static void CreateDirectories(string erkeztetoSzam)
        {
            string inputDirectory = GetInputDir(erkeztetoSzam);
            CreateDirectory(inputDirectory);
            string krOutputDirectory = GetKROutputDir(erkeztetoSzam);
            CreateDirectory(krOutputDirectory);
            string outputDirectory = GetOutputDir(erkeztetoSzam);
            CreateDirectory(outputDirectory);
        }

        static void CreateDirectory(string directoryPath)
        {
            Logger.Debug(String.Format("CreateDirectory: {0}", directoryPath));
            Directory.CreateDirectory(directoryPath);
        }
    }

    
}