﻿using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace eUzenet.FileManager
{
    /// <summary>
    /// Summary description for KRTitokManager
    /// </summary>
    public class KRTitokManager
    {
        static string batchFilePath;

        public static string BatchFilePath
        {
            get
            {
                return batchFilePath;
            }
        }
           
        static KRTitokManager()
        {
            batchFilePath = ConfigurationManager.AppSettings.Get("KR_Titok_FilePath");
        }

        static string GetTempPath()
        {
            return Path.GetTempPath();
        }

        static string GetTempPath(string erkeztetoSzam)
        {
            return Path.Combine(GetTempPath(), erkeztetoSzam);
        }

        static string GetInputPath(string erkeztetoSzam)
        {
            return Path.Combine(GetTempPath(erkeztetoSzam), "In");
        }

        static string GetOutputPath(string erkeztetoSzam)
        {
            return Path.Combine(GetTempPath(erkeztetoSzam), "Out");
        }

        public static void Kititkosit(string filePath, string outputDirectory, out string error)
        {
            Logger.Info(String.Format("KRTitokManager.Kititkosit indul: {0}, {1}", filePath, outputDirectory));
            error = null;

            if (!File.Exists(batchFilePath))
            {
                error = String.Format("KRTitokManager.Kititkosit hiba, batch file nem talalhato: {0}", batchFilePath);
                Logger.Error(error);
                return;
            }

            try
            {
                string args = String.Format("\"{0}\" \"{1}\"", filePath, outputDirectory);

                Logger.Debug(String.Format("Process: {0} {1}", batchFilePath, args));

                Process p = Process.Start(batchFilePath, args);
                p.WaitForExit();
                int result = p.ExitCode;

                Logger.Debug(String.Format("ExitCode: {0}", result));

                if (result == 0)
                {
                    Logger.Info(String.Format("KRTitokManager.Kititkosit vege"));
                }
                else
                {
                    error = String.Format("KRTitokManager.Kititkosit hiba, batch exit code: {0}", result);
                    Logger.Error(error);
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                Logger.Error("KRTitokManager.Kititkosit hiba", ex);
            }
        }

    }
}