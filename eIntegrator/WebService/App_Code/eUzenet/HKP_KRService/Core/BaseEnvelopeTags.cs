﻿using eUzenet.HKP_KRService.Core.Constans;
using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;

namespace eUzenet.HKP_KRService.Core
{
    public class BaseEnvelopeTags
    {
        public class hkpHibaUzenet
        {
            private int _szam;
            private string _tartalom;

            public int Szam
            {
                get { return _szam; }
                set { _szam = value; }
            }
            public string Tartalom
            {
                get { return _tartalom; }
                set { _tartalom = value; }
            }

            public hkpHibaUzenet()
            {
                this._szam = -1;
                this._tartalom = string.Empty;
            }
        }

        public class hkpValaszFejlec
        {
            private string _felhasznalo;
            private string _uzenetIdopont;

            public string Felhasznalo
            {
                get { return _felhasznalo; }
                set { _felhasznalo = value; }
            }
            public string UzenetIdopont
            {
                get { return _uzenetIdopont; }
                set { _uzenetIdopont = value; }
            }
            public DateTime UzenetIdopontDateTime
            {
                get { return KRDateTimeHandler.getDateTime_FromKR(_uzenetIdopont); }
                set { _uzenetIdopont = KRDateTimeHandler.getKRDateTime(value); }
            }

            public hkpValaszFejlec()
            {
                this._felhasznalo = String.Empty;
                this._uzenetIdopont = KRDateTimeHandler.getKRDateTime(DateTime.Today);
            }
        }

        public class hkpValasz
        {
            public hkpValaszFejlec ValaszFejlec { get; set; }

            public hkpHibaUzenet HibaUzenet { get; set; }

            public void GetBaseData(XmlNode valaszNode)
            {
                foreach (XmlNode valaszItem in valaszNode.ChildNodes)
                {
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.valaszFejlec))
                    {
                        ValaszFejlec = new hkpValaszFejlec
                        {
                            Felhasznalo = valaszItem.FirstChild.InnerText,
                            UzenetIdopont = valaszItem.LastChild.InnerText

                        };
                    }
                    else if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.hibaUzenet))
                    {
                        int szam;
                        if (!Int32.TryParse(valaszItem.FirstChild.InnerText, out szam))
                        {
                            szam = -1;
                        }
                        HibaUzenet = new hkpHibaUzenet
                        {
                            Szam = szam,
                            Tartalom = valaszItem.LastChild.InnerText
                        };
                    }
                }
            }

            public void GetBaseError(Exception ex)
            {
                HibaUzenet = new hkpHibaUzenet
                {
                    Szam = 5000,
                    Tartalom = ex.ToString()
                };
            }
        }
    }
}