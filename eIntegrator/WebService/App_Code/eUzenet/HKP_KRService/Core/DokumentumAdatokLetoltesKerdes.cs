﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eUzenet.HKP_KRService.Core
{
    /// <summary>
    /// Summary description for DokumentumAdatokLetoltesKerdes
    /// </summary>
    public class DokumentumAdatokLetoltesKerdes
    {
        public int? FeladoTipusa { get; set; }

        public string DokumentumTipusAzonosito { get; set; }

        public string FeladoHivatalKRID { get; set; }
    }
}