﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using eUzenet.FileManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace eUzenet.HKP_KRService.Core
{
    public static class EdokWrapper
    {
        static string eDok_FelhasznaloId;
        static EdokWrapper()
        {
            eDok_FelhasznaloId = ConfigurationManager.AppSettings["KR_eDok_FelhasznaloId"];
        }

        public static EREC_eBeadvanyokService GetService()
        {
            EREC_eBeadvanyokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            service.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            return service;
        }

        public static ExecParam GetExecParam()
        {
            ExecParam param = new ExecParam();
            param.Felhasznalo_Id = eDok_FelhasznaloId;
            return param;
        }

        // CR3114 : HAIR adatok letöltése
        public static class Rendszer
        {
            public const string HAIR = "HAIR";
            public const string WEB = "WEB";
            public const string FileShare = "FileShare";

        }

        public static bool CheckHivatkozasiSzam(HKP2.hkpDokumentumAdatok dokumentumItem, out bool insert, out string rendszer)
        {
            LogMaster logger = new LogMaster();
            logger.Info("CheckHivatkozasiSzam start");
            logger.Info(String.Format("HivatkozasiSzam: {0}", dokumentumItem.HivatkozasiSzam));

            bool hiba = false;
            insert = true;
            rendszer = EdokWrapper.Rendszer.WEB;

            if (!String.IsNullOrEmpty(dokumentumItem.HivatkozasiSzam))
            {
                insert = false;
                try
                {
                    string doktipusAzonosito = dokumentumItem.DokTipusAzonosito;
                    logger.Info(String.Format("doktipusAzonosito={0}", doktipusAzonosito));

                    bool isLetoltesiIgazolas = "LetoltesiIgazolas".Equals(doktipusAzonosito, StringComparison.CurrentCultureIgnoreCase);
                    bool isMeghiusulasiIgazolas = "MeghiusulasiIgazolas".Equals(doktipusAzonosito, StringComparison.CurrentCultureIgnoreCase);
                    bool isFeladasiIgazolas = "FeladasiIgazolas".Equals(doktipusAzonosito, StringComparison.CurrentCultureIgnoreCase);
                    bool isAtveteliErtesito = "AtveteliErtesito".Equals(doktipusAzonosito, StringComparison.CurrentCultureIgnoreCase);

                    EREC_eBeadvanyokService service = GetService();
                    ExecParam param = GetExecParam();
                    EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
                    search.KR_ErkeztetesiSzam.Value = dokumentumItem.HivatkozasiSzam;
                    search.KR_ErkeztetesiSzam.Operator = "=";
                    Result eDokUzenetekSearchResult = service.GetAll(param, search);

                    if (eDokUzenetekSearchResult.IsError)
                    {
                        logger.Error(String.Format("EREC_eBeadvanyokService.GetAll hiba: {0}", eDokUzenetekSearchResult.ErrorMessage));
                        return true;
                    }

                    if (eDokUzenetekSearchResult.Ds.Tables[0].Rows.Count == 0)
                    {
                        logger.Info("EDOK-ban nincs benne!");
                        insert = true;
                        if (isLetoltesiIgazolas || isMeghiusulasiIgazolas || isFeladasiIgazolas || isAtveteliErtesito)
                        {
                            rendszer = EdokWrapper.Rendszer.HAIR;
                        }
                        else
                        {
                            rendszer = EdokWrapper.Rendszer.WEB;
                        }
                        logger.Info(String.Format("rendszer={0}", rendszer));
                        return false;
                    }
                    else
                    {
                        logger.Info("EDOK-ban benne van!");

                        insert = true;
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("CheckHivatkozasiSzam hiba", ex);
                    hiba = true;
                }
            }

            logger.Info("CheckHivatkozasiSzam vege");
            return hiba;
        }

        // CR3114 : HAIR adatok letöltése
        //HAIR jelenesetben csak az, aminél a a DokTipusHivatal = 'NAV' és DokTipusAzonosito utolsó 4 karaktere = 'HIPA'
        public static string RendszerSelect(HKP2.hkpDokumentumAdatok dokumentumItem)
        {
            //NAV és HIPA vagy HIPAEK
            if ((dokumentumItem.DokTipusHivatal.ToUpper() == "NAV") && (dokumentumItem.DokTipusAzonosito.ToUpper().EndsWith("HIPA")
                || dokumentumItem.DokTipusAzonosito.ToUpper().EndsWith("HIPAEK")))
                return Rendszer.HAIR;
            else if ((dokumentumItem.DokTipusHivatal.ToUpper() == "ORFK") && (dokumentumItem.DokTipusAzonosito.ToUpper().EndsWith("IVH")))
                return Rendszer.HAIR;
            else if (dokumentumItem.FileNev.ToUpper().StartsWith("NAV_ONKOR_"))
                return Rendszer.HAIR;
            else if (dokumentumItem.FileNev.ToUpper().StartsWith("NAV2ONKOR_"))
                return Rendszer.HAIR;
            else if (!String.IsNullOrEmpty(dokumentumItem.DokTipusLeiras) && dokumentumItem.DokTipusLeiras.IndexOf("VHATAD", StringComparison.CurrentCultureIgnoreCase) > -1)
                return Rendszer.FileShare;
            else
                return Rendszer.WEB;

        }

        public static void ProcessKRFile(ExecParam execParam, string eBeadvanyId, string erkeztetesiSzam, Csatolmany csatolmany)
        {
            try
            {
                string fileNev = csatolmany.Nev;

                Logger.Info(String.Format("EdokWrapper.ProcessKRFile kezdete: {0} {1} {2}", eBeadvanyId, erkeztetesiSzam, fileNev));

                string ext = Path.GetExtension(fileNev);

                Logger.Debug(String.Format("ext: {0}", ext));

                if (".kr".Equals(ext, StringComparison.InvariantCultureIgnoreCase))
                {
                    Logger.Debug(String.Format("Kr file"));

                    string error;
                    string outDirectory = KRFileManager.ProcessFile(erkeztetesiSzam, fileNev, csatolmany.Tartalom, out error);

                    if (String.IsNullOrEmpty(error))
                    {
                        Logger.Debug(String.Format("out directory: {0}", outDirectory));

                        DirectoryInfo di = new DirectoryInfo(outDirectory);
                        List<Csatolmany> csatolmanyok = new List<Csatolmany>();
                        foreach (FileInfo fi in di.GetFiles())
                        {
                            Logger.Debug(String.Format("file olvasasa: {0}", fi.FullName));

                            Csatolmany csat = new Csatolmany();
                            csat.Nev = fi.Name;
                            csat.Tartalom = File.ReadAllBytes(fi.FullName);

                            csatolmanyok.Add(csat);
                        }

                        EREC_eBeadvanyCsatolmanyokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();


                        foreach (Csatolmany csat in csatolmanyok)
                        {
                            Logger.Debug("EREC_eBeadvanyCsatolmanyokService.DocumentumCsatolas kezdete");

                            Result res = service.DocumentumCsatolas(execParam, eBeadvanyId, new Csatolmany[] { csat });

                            if (!String.IsNullOrEmpty(res.ErrorCode))
                            {
                                Logger.Error(String.Format("EREC_eBeadvanyCsatolmanyokService.DocumentumCsatolas hiba! Hibakód: {0}; HibaÜzenet: {1}"
                                                , res.ErrorCode, res.ErrorMessage));
                            }

                            Logger.Debug("EREC_eBeadvanyCsatolmanyokService.DocumentumCsatolas vege");
                        }

                        KRFileManager.DeleteTempDirectory(erkeztetesiSzam);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("EdokWrapper.ProcessKRFile hiba", ex);
            }

            Logger.Info(String.Format("EdokWrapper.ProcessKRFile vege"));
        }

        public static void ProcessKRXFile(ExecParam execParam, string eBeadvanyId, string erkeztetesiSzam, Csatolmany csatolmany)
        {
            try
            {
                string fileNev = csatolmany.Nev;

                Logger.Info(String.Format("EdokWrapper.ProcessKRXFile kezdete: {0} {1} {2}", eBeadvanyId, erkeztetesiSzam, fileNev));

                string ext = Path.GetExtension(fileNev);

                Logger.Debug(String.Format("ext: {0}", ext));

                if (".krx".Equals(ext, StringComparison.InvariantCultureIgnoreCase))
                {
                    Logger.Debug(String.Format("Krx file"));

                    Contentum.eUtility.KRX.KRXFileManager krxManager = new Contentum.eUtility.KRX.KRXFileManager();
                    List<Csatolmany> csatolmanyok = krxManager.ProcessKRXFile(csatolmany);

                    EREC_eBeadvanyCsatolmanyokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();

                    foreach (Csatolmany csat in csatolmanyok)
                    {
                        Logger.Debug("EREC_eBeadvanyCsatolmanyokService.DocumentumCsatolas kezdete");

                        Result res = service.DocumentumCsatolas(execParam, eBeadvanyId, new Csatolmany[] { csat });

                        if (!String.IsNullOrEmpty(res.ErrorCode))
                        {
                            Logger.Error(String.Format("EREC_eBeadvanyCsatolmanyokService.DocumentumCsatolas hiba! Hibakód: {0}; HibaÜzenet: {1}"
                                            , res.ErrorCode, res.ErrorMessage));
                        }

                        Logger.Debug("EREC_eBeadvanyCsatolmanyokService.DocumentumCsatolas vege");
                    }

                    KRFileManager.DeleteTempDirectory(erkeztetesiSzam);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("EdokWrapper.ProcessKRXFile hiba", ex);
            }

            Logger.Info(String.Format("EdokWrapper.ProcessKRXFile vege"));
        }

        public static List<Csatolmany> ProcessKRXFile(ExecParam execParam, Csatolmany csatolmany)
        {
            List<Csatolmany> krxCsatolmanyok = new List<Csatolmany>();

            try
            {
                string fileNev = csatolmany.Nev;

                Logger.Info(String.Format("EdokWrapper.ProcessKRXFile kezdete: {0}", fileNev));

                string ext = Path.GetExtension(fileNev);

                Logger.Debug(String.Format("ext: {0}", ext));

                if (".krx".Equals(ext, StringComparison.InvariantCultureIgnoreCase))
                {
                    Logger.Debug(String.Format("Krx file"));

                    Contentum.eUtility.KRX.KRXFileManager krxManager = new Contentum.eUtility.KRX.KRXFileManager();
                    krxCsatolmanyok = krxManager.ProcessKRXFile(csatolmany);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("EdokWrapper.ProcessKRXFile hiba", ex);
            }

            Logger.Info(String.Format("EdokWrapper.ProcessKRXFile vege"));

            return krxCsatolmanyok;
        }

    }
}