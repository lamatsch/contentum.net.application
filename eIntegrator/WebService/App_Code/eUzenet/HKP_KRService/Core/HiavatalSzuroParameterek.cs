﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eUzenet.HKP_KRService.Core
{
    /// <summary>
    /// Summary description for HiavatalSzuroParameterek
    /// </summary>
    public class HivatalSzuroParameterek
    {
        public string RovidNev{ get; set;}
        public string Nev { get; set; }
        public OperatorTipus NevOperator { get; set; }
        public string KRID { get; set; }
        public HivatalTipus Tipus { get; set; }
        public bool TamogatottSzolgaltatasok { get; set; }

        public HivatalSzuroParameterek()
        {
            NevOperator = OperatorTipus.And;
            Tipus = HivatalTipus.Mind;
        }
    }

    public enum HivatalTipus
    {
        Mind = 0,
        HivataliKapu = 1,
        Cegkapu = 2,
        Perkapu = 7
    }

    public enum OperatorTipus
    {
        And,
        Or
    }
}