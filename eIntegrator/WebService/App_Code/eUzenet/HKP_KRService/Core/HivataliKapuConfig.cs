﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace eUzenet.HKP_KRService.Core
{
    public class HivataliKapuConfig
    {
        public static HivataliKapuConfigSection _Config = ConfigurationManager.GetSection("hivataliKapu") as HivataliKapuConfigSection;
        public static FiokElementCollection GetFiokok()
        {
            return _Config.Fiokok;
        }
    }

    public class HivataliKapuConfigSection : ConfigurationSection
    {
        //Decorate the property with the tag for your collection.
        [ConfigurationProperty("fiokok")]
        public FiokElementCollection Fiokok
        {
            get { return (FiokElementCollection)this["fiokok"]; }
        }
    }

    //Extend the ConfigurationElementCollection class.
    //Decorate the class with the class that represents a single element in the collection.
    [ConfigurationCollection(typeof(FiokElement))]
    public class FiokElementCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }
        protected override string ElementName
        {
            get
            {
                return "fiok";
            }
        }

        public FiokElement this[int index]
        {
            get { return (FiokElement)BaseGet(index); }
        }
        protected override ConfigurationElement CreateNewElement()
        {
            return new FiokElement();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FiokElement)element).Name;
        }
        public new FiokElement this[string name]
        {
            get { return (FiokElement)BaseGet(name); }
        }
    }

    public class FiokElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("settings", IsRequired = true)]
        public KeyValueConfigurationCollection Settings
        {
            get { return (KeyValueConfigurationCollection)this["settings"]; }
            set { this["settings"] = value; }
        }
    }
}
