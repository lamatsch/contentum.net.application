﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace eUzenet.HKP_KRService.Core
{
    /// <summary>
    /// Summary description for IgazgatasiSzunetManager
    /// </summary>
    public class IgazgatasiSzunetManager
    {
        const string EUZENET_DOKTIPAZON_LETOLT_IGSZUNETBEN = "EUZENET_DOKTIPAZON_LETOLT_IGSZUNETBEN";
        const string EUZENET_FELADO_LETOLT_IGSZUNETBEN = "EUZENET_FELADO_LETOLT_IGSZUNETBEN";

        List<KRT_KodTarak> GetKodtarak(string kodcsoportKod)
        {
            Contentum.eAdmin.Service.KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            ExecParam execParam = EdokWrapper.GetExecParam();
            KRT_KodTarakSearch search = new KRT_KodTarakSearch();
            search.KodCsoport_Id.Value = String.Format("select Id from KRT_KodCsoportok where Kod = '{0}' and getdate() between ErvKezd and ErvVege", kodcsoportKod);
            search.KodCsoport_Id.Operator = Query.Operators.inner;

            Result result = service.GetAll(execParam, search);

            if (result.IsError)
            {
                Logger.Error(String.Format("KRT_KodTarakService.GetAllByKodcsoportKod: {0}", kodcsoportKod), execParam, result);
                return null;
            }

            List<KRT_KodTarak> kodtarak = new List<KRT_KodTarak>();

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                KRT_KodTarak kodtar = new KRT_KodTarak();
                Utils.LoadBusinessDocumentFromDataRow(kodtar, row);
                kodtarak.Add(kodtar);
            }

            return kodtarak;
        }

        Contentum.eAdmin.BaseUtility.KodtarFuggoseg.KodtarFuggosegDataClass GetKotarFuggoseg(string vezerloKodcsoportId, string fuggoKodcsoportId)
        {

            Contentum.eAdmin.Service.KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
            ExecParam execParam = EdokWrapper.GetExecParam();

            KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
            search.Vezerlo_KodCsoport_Id.Value = vezerloKodcsoportId;
            search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.equals;
            search.Fuggo_KodCsoport_Id.Value = fuggoKodcsoportId;
            search.Fuggo_KodCsoport_Id.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, search);

            if (result.IsError)
            {
                Logger.Error(String.Format("KRT_KodtarFuggosegService.GetAll: {0} - {1}", vezerloKodcsoportId, fuggoKodcsoportId), execParam, result);
                return null;
            }


            if (result.Ds.Tables[0].Rows.Count > 0)
            {
                string adat = result.Ds.Tables[0].Rows[0]["Adat"].ToString();

                if (!String.IsNullOrEmpty(adat))
                {
                    Contentum.eAdmin.BaseUtility.KodtarFuggoseg.KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);
                    return fuggosegek;
                }
            }

            return null;
        }

        List<string> GetFuggoKodok(Contentum.eAdmin.BaseUtility.KodtarFuggoseg.KodtarFuggosegDataClass fuggosegek, string vezerloKodtarId, List<KRT_KodTarak> fuggoKodtarak)
        {
            List<string> fuggoKodok = new List<string>();

            if (fuggosegek != null && fuggosegek.Items != null)
            {
                foreach (Contentum.eAdmin.BaseUtility.KodtarFuggoseg.KodtarFuggosegDataItemClass item in fuggosegek.Items)
                {
                    if (item.VezerloKodTarId.Equals(vezerloKodtarId, StringComparison.InvariantCultureIgnoreCase))
                    {
                        string fuggoKod = fuggoKodtarak.Where(k => k.Id.Equals(item.FuggoKodtarId, StringComparison.InvariantCultureIgnoreCase)).Select(k => k.Kod).FirstOrDefault();

                        if (!String.IsNullOrEmpty(fuggoKod))
                        {
                            fuggoKodok.Add(fuggoKod);
                        }
                    }
                }
            }

            return fuggoKodok;
        }

        public List<DokumentumAdatokLetoltesKerdes> GetLetoltesKerdesek()
        {
            Logger.Debug("IgazgatasiSzunetManager.GetLetoltesKerdesek kezdete");
            List <DokumentumAdatokLetoltesKerdes> kerdesek = new List<DokumentumAdatokLetoltesKerdes>();

            try
            {
                List<KRT_KodTarak> dokTipusok = GetKodtarak(EUZENET_DOKTIPAZON_LETOLT_IGSZUNETBEN);
                List<KRT_KodTarak> feladok = GetKodtarak(EUZENET_FELADO_LETOLT_IGSZUNETBEN);

                bool isDokTipusok = (dokTipusok != null && dokTipusok.Count > 0);
                bool isFeladok = (feladok != null && feladok.Count > 0);

                Contentum.eAdmin.BaseUtility.KodtarFuggoseg.KodtarFuggosegDataClass fuggosegek = null;

                if (isDokTipusok && isFeladok)
                {
                    fuggosegek = GetKotarFuggoseg(dokTipusok[0].KodCsoport_Id, feladok[0].KodCsoport_Id);
                }

                if (isDokTipusok)
                {
                    foreach (KRT_KodTarak dokTipus in dokTipusok)
                    {
                        List<string> fuggoFeladoKodok = GetFuggoKodok(fuggosegek, dokTipus.Id, feladok);

                        if (fuggoFeladoKodok.Count == 0)
                        {
                            kerdesek.Add(new DokumentumAdatokLetoltesKerdes()
                            {
                                DokumentumTipusAzonosito = dokTipus.Kod,
                            });
                        }
                        else
                        {
                            foreach (string feladoKod in fuggoFeladoKodok)
                            {
                                kerdesek.Add(new DokumentumAdatokLetoltesKerdes()
                                {
                                    DokumentumTipusAzonosito = dokTipus.Kod,
                                    FeladoHivatalKRID = feladoKod
                                });
                            }
                        }
                    }
                }

                if (isFeladok)
                {
                    foreach (KRT_KodTarak felado in feladok)
                    {
                        bool isFeladoLekerdezve = kerdesek.Exists(k => felado.Kod == k.FeladoHivatalKRID);

                        if (!isFeladoLekerdezve)
                        {
                            kerdesek.Add(new DokumentumAdatokLetoltesKerdes()
                            {
                                FeladoHivatalKRID = felado.Kod,
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("IgazgatasiSzunetManager.GetLetoltesKerdesek hiba", ex);
            }

            return kerdesek;
          
        }
    }
}