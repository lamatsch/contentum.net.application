﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Erkeztetes;
using Contentum.eIntegrator.WebService.ePosta.Tertiveveny.Models;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace eUzenet.HKP_KRService.Core
{
    /// <summary>
    /// Summary description for ProcessManager
    /// </summary>
    public class ProcessManager
    {
        ExecParam execParam = null;

        public ProcessManager(ExecParam execParam)
        {
            this.execParam = execParam;
        }

        class ProcessData
        {
            public string eBeadvanyId { get; set; }
            public EREC_eBeadvanyok eBeadvany { get; set; }
            public Csatolmany Csatolmany { get; set; }
        }

        public void PostProcess_eBeadvany(string eBeadvanyId, EREC_eBeadvanyok ebeadvany, Csatolmany csatolmany)
        {
            ProcessData data = new ProcessData()
            {
                eBeadvanyId = eBeadvanyId,
                eBeadvany = ebeadvany,
                Csatolmany = csatolmany
            };

            ThreadPool.QueueUserWorkItem(Execute, data);
        }

        void Execute(object data)
        {
            EREC_eBeadvanyokService service = EdokWrapper.GetService();
            ProcessData procData = data as ProcessData;

            if (procData == null)
            {
                return;
            }

            EdokWrapper.ProcessKRFile(execParam, procData.eBeadvanyId, procData.eBeadvany.KR_ErkeztetesiSzam, procData.Csatolmany);
            EdokWrapper.ProcessKRXFile(execParam, procData.eBeadvanyId, procData.eBeadvany.KR_ErkeztetesiSzam, procData.Csatolmany);

            Result result = service.PostProcess(execParam, procData.eBeadvanyId);
            if (result.IsError)
            {
                Logger.Error(String.Format("EREC_eBeadvanyokService.PostProcess({0})", procData.eBeadvanyId), execParam, result);
            }
            ProcessETerti(service, procData);
            service.ExecuteEUzenetSzabaly(procData.eBeadvanyId, execParam);
        }

        private void ProcessETerti(EREC_eBeadvanyokService service, ProcessData procData)
        {
            try
            {
                if (!PostaiTertivevenyErkeztetesHKP.IseTertiveveny(procData.eBeadvany.KR_DokTipusAzonosito))
                {
                    return;
                }
                Logger.Debug("ProcessManager.ProcessEterti. Az eBeadvany eTertiket tartalmaz!  Feldolgozás elkezdődött." + procData.eBeadvanyId);
                var parameters = new ePostaiTertivevenyParameters();
                var erkezteto = new PostaiTertivevenyErkeztetesHKP(parameters);
                var folderManager = new HKPTempFolderManager(parameters, procData.eBeadvanyId);

                var files = erkezteto.SavePDFToTempDir(procData.eBeadvanyId, folderManager);

                ePostaiTertivevenyErkeztetesProcessResult result = erkezteto.ExtractAndProcessAllFiles(folderManager, true);

                //  Invalidate HKP message to prevent further processing!
                execParam.Record_Id = procData.eBeadvanyId;
                service.Invalidate(execParam);
                Logger.Debug("ProcessManager.ProcessEterti result: " + INT_ePostaiTertivevenyService.GetResultAsString(result));
            }
            catch (Exception e)
            {
                Logger.Error("ProcessManager.ProcessETerti. Nem sikerült a feldolgozás. Error: " + e.Message, e);
            }
        }
    }
}