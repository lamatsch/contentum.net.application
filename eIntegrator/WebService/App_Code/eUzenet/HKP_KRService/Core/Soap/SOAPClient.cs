﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace eUzenet.HKP_KRService.Core.Soap
{
    public class SoapCaller
    {
        public string requestString;
        public SoapMessage SoapResult;
        public string destinationUrl;

        public SoapCaller(string destination)
        {
            destinationUrl = destination;
            this.SoapResult = new SoapMessage();
        }

        public void setRequestEnvelope(string inputString)
        {
            requestString = inputString;
        }

        public void call()
        {
            call(false);
        }

        public void call(bool isAttachmentNeeded)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            request.Timeout = (int)TimeSpan.FromMinutes(5).TotalMilliseconds;
            byte[] bytes;
            bytes = System.Text.Encoding.UTF8.GetBytes(requestString);
            request.ContentType = "text/xml; charset=UTF-8";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (isAttachmentNeeded)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    SoapExtractor extr = new SoapExtractor();
                    SoapResult = new SoapMessage(response, "--");
                    SoapResult = extr.getEnvelopeWithAttachments(response.GetResponseStream(), SoapResult.Header.ContentType.Boundary);
                }
            }
            else
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                SoapResult.Envelope.LoadXml(reader.ReadToEnd());
                SoapResult.Header = new SoapHeader(response, "--");
            }

            //LogMaster logger = new LogMaster();
            //logger.Info(requestString);
            //StreamReader reader1 = new StreamReader(response.GetResponseStream());
            //logger.Info(SoapResult.Envelope.OuterXml);
        }

        public void callCustomRequest(HttpWebRequest request)
        {
            callCustomRequest(request, false);
        }

        public void callCustomRequest(HttpWebRequest request, bool isAttachmentNeeded)
        {
            HttpWebResponse response;
            request.Timeout = (int)TimeSpan.FromMinutes(5).TotalMilliseconds;
            response = (HttpWebResponse)request.GetResponse();
            if (isAttachmentNeeded)
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    SoapExtractor extr = new SoapExtractor();
                    SoapResult = new SoapMessage(response, "--");
                    SoapResult = extr.getEnvelopeWithAttachments(response.GetResponseStream(), SoapResult.Header.ContentType.Boundary);
                }
            }
            else
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                SoapResult.Envelope.LoadXml(reader.ReadToEnd());
                SoapResult.Header = new SoapHeader(response, "--");
            }
        }
    }

    public class SoapMessage
    {
        private XmlDocument _Envelope;
        private List<SoapAttachment> _Attachments;
        private SoapHeader _header;
        private SoapAttachmentHeader _EnvelopeHeader;

        public XmlDocument Envelope
        {
            get { return _Envelope; }
            set { _Envelope = value; }
        }
        public List<SoapAttachment> Attachments
        {
            get { return _Attachments; }
            set { _Attachments = value; }
        }
        public SoapHeader Header
        {
            get { return _header; }
            set { _header = value; }
        }
        public SoapAttachmentHeader EnvelopeHeader
        {
            get { return _EnvelopeHeader; }
            set { _EnvelopeHeader = value; }
        }

        public SoapMessage()
        {
            this._Envelope = new XmlDocument();
            this._Attachments = new List<SoapAttachment>();
            this._header = new SoapHeader();
            this._EnvelopeHeader = new SoapAttachmentHeader();
        }

        public SoapMessage(HttpWebResponse response, string boundaryExtension)
        {
            this._Envelope = new XmlDocument();
            this._Attachments = new List<SoapAttachment>();
            this._header = new SoapHeader(response, boundaryExtension);
            this._EnvelopeHeader = new SoapAttachmentHeader();
        }
    }

    public class SoapHeader
    {
        private SoapContentType _contentType;
        private long _contentLength;
        private String _contentEncoding;
        private String _charset;
        private HttpStatusCode _statusCode;
        private String _statusDescription;

        public SoapContentType ContentType
        {
            get { return _contentType; }
            set { _contentType = value; }
        }
        public long ContentLength
        {
            get { return _contentLength; }
            set { _contentLength = value; }
        }
        public String ContentEncoding
        {
            get { return _contentEncoding; }
            set { _contentEncoding = value; }
        }
        public String Charset
        {
            get { return _charset; }
            set { _charset = value; }
        }
        public HttpStatusCode StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }
        public String StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }

        public SoapHeader()
        {
            this._contentType = new SoapContentType();
            this._contentLength = 0;
            this._contentEncoding = String.Empty;
            this._charset = String.Empty;
            this._statusCode = HttpStatusCode.Unused;
            this._statusDescription = String.Empty;
        }

        public SoapHeader(HttpWebResponse response, string boundaryExtension)
        {
            this._contentType = new SoapContentType(response.ContentType, boundaryExtension);
            this._contentLength = response.ContentLength;
            this._contentEncoding = response.ContentEncoding;
            this._charset = response.CharacterSet;
            this._statusCode = response.StatusCode;
            this._statusDescription = response.StatusDescription;
        }
    }

    public class SoapContentType
    {
        private string _MessageType;
        private string _boundary;
        private string _contentType;
        private string _charset;

        public string MessageType
        {
            get { return _MessageType; }
            set { _MessageType = value; }
        }
        public string Boundary
        {
            get { return _boundary; }
            set { _boundary = value; }
        }
        public string ContentType
        {
            get { return _contentType; }
            set { _contentType = value; }
        }
        public string Charset
        {
            get { return _charset; }
            set { _charset = value; }
        }

        public SoapContentType()
        {
            this._MessageType = String.Empty;
            this._boundary = String.Empty;
            this._contentType = String.Empty;
            this._charset = String.Empty;
        }

        public SoapContentType(string header_ContentType, string boundaryExtension)
        {
            this._MessageType = header_ContentType.Substring(0, header_ContentType.IndexOf(";") - 1);
            this._boundary = boundaryExtension + Regex.Match(header_ContentType, @"boundary=([A-Z..0-9.._\%\/\\\&\?\,\'\:\!\-+*..a-z]+)").Groups[1].Value;
            this._contentType = Regex.Match(header_ContentType, @"type=\\""([A-Z..0-9.._\%\/\\\&\?\,\'\:\!\""-+*..a-z]+)\\""").Groups[1].Value;
            this._charset = Regex.Match(header_ContentType, @"charset=([A-Z..0-9.._\%\/\\\&\?\,\'\:\!\""\-\+\*..a-z]+)").Groups[1].Value;
        }
    }

    public class SoapAttachment
    {
        private byte[] _contentStream;
        private SoapAttachmentHeader _header;
        private String _filename;

        public byte[] ContentStream
        {
            get { return _contentStream; }
            set { _contentStream = value; }
        }
        public SoapAttachmentHeader Header
        {
            get { return _header; }
            set { _header = value; }
        }
        public String Filename
        {
            get { return _filename; }
            set { _filename = value; }
        }

        public SoapAttachment()
        {
            this._header = new SoapAttachmentHeader();
            this._filename = String.Empty;
        }

        public SoapAttachment(byte[] content, SoapAttachmentHeader header, string fileName)
        {
            this._header = header;
            this._contentStream = content;
            this._filename = fileName;
        }
    }

    public class SoapAttachmentHeader
    {
        private string _contentType;
        private string _name;
        private string _contentDisposition;
        private string _filename;
        private string _contentID;
        private string _contentTransfer_Encoding;
        private string _charset;

        public string ContentType
        {
            get { return _contentType; }
            set { _contentType = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string ContentDisposition
        {
            get { return _contentDisposition; }
            set { _contentDisposition = value; }
        }
        public string Filename
        {
            get { return _filename; }
            set { _filename = value; }
        }
        public string ContentID
        {
            get { return _contentID; }
            set { _contentID = value; }
        }
        public string ContentTransfer_Encoding
        {
            get { return _contentTransfer_Encoding; }
            set { _contentTransfer_Encoding = value; }
        }
        public string Charset
        {
            get { return _charset; }
            set { _charset = value; }
        }

        public SoapAttachmentHeader()
        {
            this._contentType = String.Empty;
            this._name = String.Empty;
            this._contentDisposition = String.Empty;
            this._filename = String.Empty;
            this._contentID = String.Empty;
            this._contentTransfer_Encoding = String.Empty;
            this._charset = String.Empty;
        }

        public SoapAttachmentHeader(String contentType_rawString, string content_transfer_encoding, string contentID)
        {
            if (!String.IsNullOrEmpty(contentType_rawString))
            {
                this._charset = Regex.Match(contentType_rawString, @"charset=([A-Z..0-9.._\%\/\\\&\?\,\'\:\!\""\-\+\*..a-z]+)").Groups[1].Value;
                this._name = Regex.Match(contentType_rawString, @" name=\""([A-Z..0-9.._\%\/\\\&\?\,\'\:\!\""\-\+\*..a-z]+)\""").Groups[1].Value;
                this._filename = Regex.Match(contentType_rawString, @"filename=\""([A-Z..0-9.._\<\>\%\/\\\&\?\,\'\:\!\""\-\+\*\@..a-z]+)\""").Groups[1].Value;
                this._contentDisposition = Regex.Match(contentType_rawString, @"Content-Disposition: ([A-Z..0-9.._\<\>\%\/\\\&\?\,\'\:\!\""\-\+\*\@..a-z]+)").Groups[1].Value;
                this._contentType = contentType_rawString.TrimStart(' ').Substring(0, contentType_rawString.IndexOf(';') - 1);
            }
            this._contentTransfer_Encoding = content_transfer_encoding;
            if (String.IsNullOrEmpty(Regex.Match(contentID, @"Content-ID: ([A-Z..0-9.._\<\>\%\/\\\&\?\,\'\:\!\""\-\+\*\@..a-z]+)").Groups[1].Value))
            {
                this._contentID = contentID.Trim();
            }
            else
            {
                this._contentID = Regex.Match(contentID, @"Content-ID: ([A-Z..0-9.._\<\>\%\/\\\&\?\,\'\:\!\""\-\+\*\@..a-z]+)").Groups[1].Value;
            }
        }
    }

    public class SoapExtractor
    {
        public SoapMessage getEnvelopeWithAttachments(Stream httpWebResponseStream, string boundaryEnding)
        {
            SoapMessage _return = new SoapMessage();

            byte[] dataRaw = GetData(httpWebResponseStream);
            byte[] boundary1 = System.Text.Encoding.UTF8.GetBytes("\r\n\r\n"); //This is fixed
            byte[] boundary2 = System.Text.Encoding.UTF8.GetBytes("\r\n" + boundaryEnding);

            List<SoapAttachment> attachmentDataList = GetAttachmentData(dataRaw, boundary1, boundary2);
            if (attachmentDataList.Count != 0)
            {
                _return.Envelope.LoadXml(Encoding.UTF8.GetString(attachmentDataList[0].ContentStream));
                _return.EnvelopeHeader = attachmentDataList[0].Header;
                attachmentDataList.RemoveAt(0);
                _return.Attachments = attachmentDataList;
            }
            else
            {
                string data = Encoding.UTF8.GetString(dataRaw);
                _return.Envelope.LoadXml(data);
            }

            return _return;
        }

        private byte[] GetData(Stream input)
        {
            MemoryStream ms = new MemoryStream();
            int read = 0;
            byte[] buffer = new byte[1024];
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                ms.Write(buffer, 0, read);
            }
            return ms.ToArray();
        }

        private List<SoapAttachment> GetAttachmentData(byte[] data, byte[] boundary1, byte[] boundary2)
        {
            List<SoapAttachment> _returnList = new List<SoapAttachment>();
            List<byte[]> attachmentFullList = new List<byte[]>();
            bool firstBoundaryFounded = false;

            bool listEnded = false;

            if (data == null || boundary1 == null || boundary2 == null)
                return null;

            if (boundary1.LongLength > data.LongLength)
                return null;

            long i, j, startIndex = 0;
            bool match;

            int boundary1Pos = 0;
            int boundary2Pos = 0;

            #region split the raw byte array to attachment-pieces
            do
            {
                bool skip = false;
                for (i = boundary1Pos; i < data.LongLength; i++)
                {
                    startIndex = i;
                    match = true;
                    for (j = 0; j < boundary2.LongLength; j++)
                    {
                        if (data[startIndex] != boundary2[j])
                        {
                            match = false;
                            break;
                        }
                        else if (startIndex < data.LongLength - 1)
                        {
                            startIndex++;
                        }
                    }

                    if (match)
                    {
                        if (!firstBoundaryFounded)
                        {
                            firstBoundaryFounded = true;
                            boundary1Pos = Convert.ToInt32(boundary1Pos + boundary2.LongLength);
                            boundary2Pos = boundary1Pos;
                            skip = true;
                        }
                        else
                        {
                            boundary2Pos = Convert.ToInt32(startIndex);
                        }
                        break;
                    }
                }

                if (!skip)
                {
                    int pos1 = boundary1Pos;
                    int pos2 = boundary2Pos;
                    int length = pos2 - pos1;

                    if (length > 0)
                    {
                        try
                        {
                            byte[] output = new byte[length];
                            Array.Copy(data, pos1, output, 0, length);
                            attachmentFullList.Add(output);
                            boundary1Pos = pos2;
                            boundary2Pos = pos2;
                        }
                        catch
                        {
                            listEnded = true;
                        }
                    }
                    if (length == 0)
                    {
                        listEnded = true;
                    }
                }
            }
            while (!listEnded);
            #endregion

            foreach (byte[] dataItem in attachmentFullList)
            {
                listEnded = false;
                boundary1Pos = 0;
                boundary2Pos = 0;
                SoapAttachment attachment;
                SoapAttachmentHeader header;

                do
                {
                    byte[] boundary_contentType = Encoding.UTF8.GetBytes("Content-Type:");
                    byte[] boundary_contentTransferEncoding = Encoding.UTF8.GetBytes("Content-Transfer-Encoding:");
                    byte[] boundary_contentID = Encoding.UTF8.GetBytes("Content-ID:");

                    header = new SoapAttachmentHeader(
                        getAttachmentHeaderData(dataItem, boundary_contentType),
                        getAttachmentHeaderData(dataItem, boundary_contentTransferEncoding),
                        getAttachmentHeaderData(dataItem, boundary_contentID));

                    #region search for the binary part - starting position
                    for (i = boundary1Pos; i < dataItem.LongLength; i++)
                    {
                        startIndex = i;
                        match = true;
                        for (j = 0; j < boundary1.LongLength; j++)
                        {
                            if (dataItem[startIndex] != boundary1[j])
                            {
                                match = false;
                                break;
                            }
                            else if (startIndex < dataItem.LongLength)
                            {
                                startIndex++;
                            }
                        }

                        if (match)
                        {
                            boundary1Pos = Convert.ToInt32(startIndex);
                            boundary2Pos = boundary1Pos;
                            break;
                        }
                    }
                    #endregion

                    #region search for the binary part - boundary ending position;
                    for (i = boundary2Pos; i < dataItem.LongLength; i++)
                    {
                        startIndex = i;
                        match = true;
                        for (j = 0; j < boundary2.LongLength; j++)
                        {
                            if (dataItem[startIndex] != boundary2[j])
                            {
                                match = false;
                                break;
                            }
                            else if (startIndex < dataItem.LongLength)
                            {
                                startIndex++;
                            }
                        }

                        if (match)
                        {
                            boundary2Pos = Convert.ToInt32(startIndex - boundary2.LongLength);
                            break;
                        }
                    }
                    #endregion

                    #region calculation the positions and lengths
                    int pos1 = boundary1Pos;
                    int pos2 = boundary2Pos;
                    int length = pos2 - pos1;
                    #endregion

                    #region extracting the attachment datas
                    if (length > 0)
                    {
                        try
                        {
                            byte[] output = new byte[length];
                            Array.Copy(dataItem, pos1, output, 0, length);
                            attachment = new SoapAttachment(output, header, header.Filename);
                            _returnList.Add(attachment);
                            boundary1Pos = pos2;
                            boundary2Pos = pos2;
                        }
                        catch
                        {
                            listEnded = true;
                        }
                    }
                    if (length == 0)
                    {
                        listEnded = true;
                    }
                    #endregion
                }
                while (!listEnded);
            }
            return _returnList;
        }

        private string getAttachmentHeaderData(byte[] data, byte[] headerBoundary)
        {
            byte[] boundary_contentEnding = Encoding.UTF8.GetBytes("\r\n");

            int boundary1Pos = 0;
            int boundary2Pos = 0;
            int i, j, startIndex = 0;
            bool match = false;

            //for (i = boundary1Pos; i < data.LongLength; i++)
            //{
            //    startIndex = i;
            //    match = true;
                #region search for the header part - starting position
                for (i = boundary1Pos; i < data.LongLength; i++)
                {
                    startIndex = i;
                    match = true;
                    for (j = 0; j < headerBoundary.LongLength; j++)
                    {
                        if (data[startIndex] != headerBoundary[j])
                        {
                            match = false;
                            break;
                        }
                        else if (startIndex < data.LongLength)
                        {
                            startIndex++;
                        }
                    }

                    if (match)
                    {
                        boundary1Pos = Convert.ToInt32(startIndex);
                        boundary2Pos = boundary1Pos;
                        break;
                    }
                }
                #endregion

                #region search for the header part - boundary ending position;
                for (i = boundary2Pos; i < data.LongLength; i++)
                {
                    startIndex = i;
                    match = true;
                    for (j = 0; j < boundary_contentEnding.LongLength; j++)
                    {
                        if (data[startIndex] != boundary_contentEnding[j])
                        {
                            match = false;
                            break;
                        }
                        else if (startIndex < data.LongLength)
                        {
                            startIndex++;
                        }
                    }

                    if (match)
                    {
                        boundary2Pos = Convert.ToInt32(startIndex - boundary_contentEnding.LongLength);
                        break;
                    }
                }
                #endregion

                #region calculation the positions and lengths
                int pos1 = boundary1Pos;
                int pos2 = boundary2Pos;
                int length = pos2 - pos1;
                #endregion

                if (length > 0)
                {
                    try
                    {
                        byte[] output = new byte[length];
                        Array.Copy(data, pos1, output, 0, length);
                        return Encoding.UTF8.GetString(output);
                    }
                    catch
                    {

                    }
                }
            //}
            return String.Empty;
        }
    }

    public class SoapBuilder
    {
        private string _boundaryExtension;

        public string BoundaryExtension
        {
            get { return _boundaryExtension; }
            set { _boundaryExtension = value; }
        }

        public SoapBuilder()
        {
            this._boundaryExtension = String.Empty;
        }

        public SoapBuilder(string boundaryExtension)
        {
            this._boundaryExtension = boundaryExtension;
        }

        public HttpWebRequest buildMessage(SoapMessage message, string destinationUrl)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);

            byte[] boundary_ByteArray = Encoding.UTF8.GetBytes(this.BoundaryExtension + message.Header.ContentType.Boundary);
            byte[] binaryDataStart_ByteArray = System.Text.Encoding.UTF8.GetBytes("\r\n\r\n");
            message.EnvelopeHeader.ContentID = "<message>";
            SoapAttachment envelopeAttachment = new SoapAttachment(Encoding.UTF8.GetBytes(message.Envelope.OuterXml), message.EnvelopeHeader, String.Empty);
            message.Attachments.Insert(0, envelopeAttachment);
            List<byte[]> attachmentFullData_ByteArray = new List<byte[]>();
            foreach (SoapAttachment attachment in message.Attachments)
            {
                string attachmentHeader_string = String.Empty;

                if (!String.IsNullOrEmpty(attachment.Header.ContentType))
                    attachmentHeader_string += "Content-Type: " + attachment.Header.ContentType + "; ";
                if (!String.IsNullOrEmpty(attachment.Header.Name))
                    attachmentHeader_string += @" name=""" + attachment.Header.Name + @""";";
                if (!String.IsNullOrEmpty(attachment.Header.ContentDisposition))
                    attachmentHeader_string += " Content-Disposition: " + attachment.Header.ContentDisposition + ";";
                if (!String.IsNullOrEmpty(attachment.Header.Filename))
                    attachmentHeader_string += @" filename=""" + attachment.Header.Filename + @""";";
                if (!String.IsNullOrEmpty(attachment.Header.ContentID))
                    attachmentHeader_string += @" Content-ID: " + attachment.Header.ContentID + ";";
                if (!String.IsNullOrEmpty(attachment.Header.Charset))
                    attachmentHeader_string += " charset=" + attachment.Header.Charset + ";";
                attachmentHeader_string = attachmentHeader_string.TrimEnd(';');
                attachmentHeader_string += "\r\n";
                if (!String.IsNullOrEmpty(attachment.Header.ContentTransfer_Encoding))
                    attachmentHeader_string += "Content-Transfer-Encoding: " + attachment.Header.ContentTransfer_Encoding + "\r\n";
                if (!String.IsNullOrEmpty(attachment.Header.ContentID))
                    attachmentHeader_string += "Content-ID: " + attachment.Header.ContentID;
                byte[] attachmentHeader_ByteArray = Encoding.UTF8.GetBytes(attachmentHeader_string);

                List<byte[]> loader = new List<byte[]>();
                loader.Add(boundary_ByteArray);
                loader.Add(Encoding.UTF8.GetBytes("\r\n"));
                loader.Add(attachmentHeader_ByteArray);
                loader.Add(binaryDataStart_ByteArray);
                loader.Add(attachment.ContentStream);
                loader.Add(Encoding.UTF8.GetBytes("\r\n"));

                byte[] insert = CombineByteArrays(loader.ToArray());

                attachmentFullData_ByteArray.Add(insert);
            }
            attachmentFullData_ByteArray.Add(Encoding.UTF8.GetBytes(this.BoundaryExtension + message.Header.ContentType.Boundary + this.BoundaryExtension));
            byte[] fullData = CombineByteArrays(attachmentFullData_ByteArray.ToArray());

            //TODO - Request header adatainak belövése
            string request_contentType = String.Empty;
            if (!String.IsNullOrEmpty(message.Header.ContentType.MessageType))
                request_contentType += message.Header.ContentType.MessageType + ";";
            if (!String.IsNullOrEmpty(message.Header.ContentType.Boundary))
                request_contentType += " boundary=" + message.Header.ContentType.Boundary + ";";
            if (!String.IsNullOrEmpty(message.Header.ContentType.ContentType))
                request_contentType += @" type=" + message.Header.ContentType.ContentType + @";";
            request_contentType += @" start=""<message>""";

            request.ContentType = request_contentType.TrimEnd(';');
            request.ContentLength = fullData.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(fullData, 0, fullData.Length);
            requestStream.Close();
            return request;
        }

        private byte[] CombineByteArrays(params byte[][] arrays)
        {
            int maxLength = 0;
            foreach (byte[] array in arrays)
            {
                maxLength += array.Length;
            }
            byte[] rv = new byte[maxLength];

            int offset = 0;
            foreach (byte[] array in arrays)
            {
                System.Buffer.BlockCopy(array, 0, rv, offset, array.Length);
                offset += array.Length;
            }
            return rv;
        }
    }

    public class Soapifier
    {
        public static string getContentType_ByFilename(string filename)
        {
            string fileExtension = String.Empty;
            if (filename.Contains("."))
            {
                fileExtension = Reverse(Reverse(filename).Substring(0, Reverse(filename).IndexOf('.')));
            }
            switch (fileExtension.ToLower())
            {
                case "docx": return "application/octet-stream";
                case "tiff": return "image/tiff";
                case "pdf": return "application/pdf";
                case "txt": return "text/plain";
                case "jpg":
                case "jpeg": return "image/jpeg";
                case "xlsx": return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case "evy": return "application/envoy";	//Corel Envoy
                case "fif": return "application/fractals";	//fractal image file
                case "spl": return "application/futuresplash";	//Windows print spool file
                case "hta": return "application/hta";	//HTML application
                case "acx": return "application/internet-property-stream";	//Atari ST Program
                case "hqx": return "application/mac-binhex40";	//BinHex encoded file
                case "doc": return "application/msword";	//Word document
                case "dot": return "application/msword";	//Word document template
                case "*": return "application/octet-stream";	//
                case "bin": return "application/octet-stream";	//binary disk image
                case "class": return "application/octet-stream";	//Java class file
                case "dms": return "application/octet-stream";	//Disk Masher image
                case "exe": return "application/octet-stream";	//executable file
                case "lha": return "application/octet-stream";	//LHARC compressed archive
                case "lzh": return "application/octet-stream";	//LZH compressed file
                case "oda": return "application/oda";	//CALS raster image
                case "axs": return "application/olescript";	//ActiveX script
                case "prf": return "application/pics-rules";	//Outlook profile file
                case "p10": return "application/pkcs10";	//certificate request file
                case "crl": return "application/pkix-crl";	//certificate revocation list file
                case "ai": return "application/postscript";	//Adobe Illustrator file
                case "eps": return "application/postscript";	//postscript file
                case "ps": return "application/postscript";	//postscript file
                case "rtf": return "application/rtf";	//rich text format file
                case "setpay": return "application/set-payment-initiation";	//set payment initiation
                case "setreg": return "application/set-registration-initiation";	//set registration initiation
                case "xla": return "application/vnd.ms-excel";	//Excel Add-in file
                case "xlc": return "application/vnd.ms-excel";	//Excel chart
                case "xlm": return "application/vnd.ms-excel";	//Excel macro
                case "xls": return "application/vnd.ms-excel";	//Excel spreadsheet
                case "xlt": return "application/vnd.ms-excel";	//Excel template
                case "xlw": return "application/vnd.ms-excel";	//Excel worspace
                case "msg": return "application/vnd.ms-outlook";	//Outlook mail message
                case "sst": return "application/vnd.ms-pkicertstore";	//serialized certificate store file
                case "cat": return "application/vnd.ms-pkiseccat";	//Windows catalog file
                case "stl": return "application/vnd.ms-pkistl";	//stereolithography file
                case "pot": return "application/vnd.ms-powerpoint";	//PowerPoint template
                case "pps": return "application/vnd.ms-powerpoint";	//PowerPoint slide show
                case "ppt": return "application/vnd.ms-powerpoint";	//PowerPoint presentation
                case "mpp": return "application/vnd.ms-project";	//Microsoft Project file
                case "wcm": return "application/vnd.ms-works";	//WordPerfect macro
                case "wdb": return "application/vnd.ms-works";	//Microsoft Works database
                case "wks": return "application/vnd.ms-works";	//Microsoft Works spreadsheet
                case "wps": return "application/vnd.ms-works";	//Microsoft Works word processsor document
                case "hlp": return "application/winhlp";	//Windows help file
                case "bcpio": return "application/x-bcpio";	//binary CPIO archive
                case "cdf": return "application/x-cdf";	//computable document format file
                case "z": return "application/x-compress";	//Unix compressed file
                case "tgz": return "application/x-compressed";	//gzipped tar file
                case "cpio": return "application/x-cpio";	//Unix CPIO archive
                case "csh": return "application/x-csh";	//Photoshop custom shapes file
                case "dcr": return "application/x-director";	//Kodak RAW image file
                case "dir": return "application/x-director";	//Adobe Director movie
                case "dxr": return "application/x-director";	//Macromedia Director movie
                case "dvi": return "application/x-dvi";	//device independent format file
                case "gtar": return "application/x-gtar";	//Gnu tar archive
                case "gz": return "application/x-gzip";	//Gnu zipped archive
                case "hdf": return "application/x-hdf";	//hierarchical data format file
                case "ins": return "application/x-internet-signup";	//internet settings file
                case "isp": return "application/x-internet-signup";	//IIS internet service provider settings
                case "iii": return "application/x-iphone";	//ARC+ architectural file
                case "js": return "application/x-javascript";	//JavaScript file
                case "latex": return "application/x-latex";	//LaTex document
                case "mdb": return "application/x-msaccess";	//Microsoft Access database
                case "crd": return "application/x-mscardfile";	//Windows CardSpace file
                case "clp": return "application/x-msclip";	//CrazyTalk clip file
                case "dll": return "application/x-msdownload";	//dynamic link library
                case "m13": return "application/x-msmediaview";	//Microsoft media viewer file
                case "m14": return "application/x-msmediaview";	//Steuer2001 file
                case "mvb": return "application/x-msmediaview";	//multimedia viewer book source file
                case "wmf": return "application/x-msmetafile";	//Windows meta file
                case "mny": return "application/x-msmoney";	//Microsoft Money file
                case "pub": return "application/x-mspublisher";	//Microsoft Publisher file
                case "scd": return "application/x-msschedule";	//Turbo Tax tax schedule list
                case "trm": return "application/x-msterminal";	//FTR media file
                case "wri": return "application/x-mswrite";	//Microsoft Write file
                case "nc": return "application/x-netcdf";	//Mastercam numerical control file
                case "pma": return "application/x-perfmon";	//MSX computers archive format
                case "pmc": return "application/x-perfmon";	//performance monitor counter file
                case "pml": return "application/x-perfmon";	//process monitor log file
                case "pmr": return "application/x-perfmon";	//Avid persistant media record file
                case "pmw": return "application/x-perfmon";	//Pegasus Mail draft stored message
                case "p12": return "application/x-pkcs12";	//personal information exchange file
                case "pfx": return "application/x-pkcs12";	//PKCS #12 certificate file
                case "p7b": return "application/x-pkcs7-certificates";	//PKCS #7 certificate file
                case "spc": return "application/x-pkcs7-certificates";	//software publisher certificate file
                case "p7r": return "application/x-pkcs7-certreqresp";	//certificate request response file
                case "p7c": return "application/x-pkcs7-mime";	//PKCS #7 certificate file
                case "p7m": return "application/x-pkcs7-mime";	//digitally encrypted message
                case "p7s": return "application/x-pkcs7-signature";	//digitally signed email message
                case "sh": return "application/x-sh";	//Bash shell script
                case "shar": return "application/x-shar";	//Unix shar archive
                case "swf": return "application/x-shockwave-flash";	//Flash file
                case "sit": return "application/x-stuffit";	//Stuffit archive file
                case "sv4cpio": return "application/x-sv4cpio";	//system 5 release 4 CPIO file
                case "sv4crc": return "application/x-sv4crc";	//system 5 release 4 CPIO checksum data
                case "tar": return "application/x-tar";	//consolidated Unix file archive
                case "tcl": return "application/x-tcl";	//Tcl script
                case "tex": return "application/x-tex";	//LaTeX source document
                case "texi": return "application/x-texinfo";	//LaTeX info document
                case "texinfo": return "application/x-texinfo";	//LaTeX info document
                case "roff": return "application/x-troff";	//unformatted manual page
                case "t": return "application/x-troff";	//Turing source code file
                case "tr": return "application/x-troff";	//TomeRaider 2 ebook file
                case "man": return "application/x-troff-man";	//Unix manual
                case "me": return "application/x-troff-me";	//readme text file
                case "ms": return "application/x-troff-ms";	//3ds Max script file
                case "ustar": return "application/x-ustar";	//uniform standard tape archive format file
                case "src": return "application/x-wais-source";	//source code
                case "cer": return "application/x-x509-ca-cert";	//internet security certificate
                case "crt": return "application/x-x509-ca-cert";	//security certificate
                case "der": return "application/x-x509-ca-cert";	//DER certificate file
                case "pko": return "application/ynd.ms-pkipko";	//public key security object
                case "zip": return "application/zip";	//zipped file
                case "au": return "audio/basic";	//audio file
                case "snd": return "audio/basic";	//sound file
                case "mid": return "audio/mid";	//midi file
                case "rmi": return "audio/mid";	//media processing server studio
                case "mp3": return "audio/mpeg";	//MP3 file
                case "aif": return "audio/x-aiff";	//audio interchange file format
                case "aifc": return "audio/x-aiff";	//compressed audio interchange file
                case "aiff": return "audio/x-aiff";	//audio interchange file format
                case "m3u": return "audio/x-mpegurl";	//media playlist file
                case "ra": return "audio/x-pn-realaudio";	//Real Audio file
                case "ram": return "audio/x-pn-realaudio";	//Real Audio metadata file
                case "wav": return "audio/x-wav";	//WAVE audio file
                case "bmp": return "image/bmp";	//Bitmap
                case "cod": return "image/cis-cod";	//compiled source code
                case "gif": return "image/gif";	//graphic interchange format
                case "ief": return "image/ief";	//image file
                case "jpe": return "image/jpeg";	//JPEG image
                case "jfif": return "image/pipeg";	//JPEG file interchange format
                case "svg": return "image/svg+xml";	//scalable vector graphic
                case "tif": return "image/tiff";	//TIF image
                case "ras": return "image/x-cmu-raster";	//Sun raster graphic
                case "cmx": return "image/x-cmx";	//Corel metafile exchange image file
                case "ico": return "image/x-icon";	//icon
                case "pnm": return "image/x-portable-anymap";	//portable any map image
                case "pbm": return "image/x-portable-bitmap";	//portable bitmap image
                case "pgm": return "image/x-portable-graymap";	//portable graymap image
                case "ppm": return "image/x-portable-pixmap";	//portable pixmap image
                case "rgb": return "image/x-rgb";	//RGB bitmap
                case "xbm": return "image/x-xbitmap";	//X11 bitmap
                case "xpm": return "image/x-xpixmap";	//X11 pixmap
                case "xwd": return "image/x-xwindowdump";	//X-Windows dump image
                case "mht": return "message/rfc822";	//MHTML web archive
                case "mhtml": return "message/rfc822";	//MIME HTML file
                case "nws": return "message/rfc822";	//Windows Live Mail newsgroup file
                case "css": return "text/css";	//Cascading Style Sheet
                case "323": return "text/h323";	//H.323 internet telephony file
                case "htm": return "text/html";	//HTML file
                case "html": return "text/html";	//HTML file
                case "stm": return "text/html";	//Exchange streaming media file
                case "uls": return "text/iuls";	//NetMeeting user location service file
                case "bas": return "text/plain";	//BASIC source code file
                case "c": return "text/plain";	//C/C++ source code file
                case "h": return "text/plain";	//C/C++/Objective C header file
                case "rtx": return "text/richtext";	//rich text file
                case "sct": return "text/scriptlet";	//Scitext continuous tone file
                case "tsv": return "text/tab-separated-values";	//tab separated values file
                case "htt": return "text/webviewhtml";	//hypertext template file
                case "htc": return "text/x-component";	//HTML component file
                case "etx": return "text/x-setext";	//TeX font encoding file
                case "vcf": return "text/x-vcard";	//vCard file
                case "mp2": return "video/mpeg";	//MPEG-2 audio file
                case "mpa": return "video/mpeg";	//MPEG-2 audio file
                case "mpe": return "video/mpeg";	//MPEG movie file
                case "mpeg": return "video/mpeg";	//MPEG movie file
                case "mpg": return "video/mpeg";	//MPEG movie file
                case "mpv2": return "video/mpeg";	//MPEG-2 video stream
                case "mov": return "video/quicktime";	//Apple QuickTime movie
                case "qt": return "video/quicktime";	//Apple QuickTime movie
                case "lsf": return "video/x-la-asf";	//Logos library system file
                case "lsx": return "video/x-la-asf";	//streaming media shortcut
                case "asf": return "video/x-ms-asf";	//advanced systems format file
                case "asr": return "video/x-ms-asf";	//ActionScript remote document
                case "asx": return "video/x-ms-asf";	//Microsoft ASF redirector file
                case "avi": return "video/x-msvideo";	//audio video interleave file
                case "movie": return "video/x-sgi-movie";	//Apple QuickTime movie
                case "flr": return "x-world/x-vrml";	//Flare decompiled actionscript file
                case "vrml": return "x-world/x-vrml";	//VRML file
                case "wrl": return "x-world/x-vrml";	//VRML world
                case "wrz": return "x-world/x-vrml";	//compressed VRML world
                case "xaf": return "x-world/x-vrml";	//3ds max XML animation file
                case "xof": return "x-world/x-vrml";	//Reality Lab 3D image file
                case "dotx": return "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
                case "docm": return "application/vnd.ms-word.document.macroEnabled.12";
                case "dotm": return "application/vnd.ms-word.template.macroEnabled.12";
                case "xltx": return "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
                case "xlsm": return "application/vnd.ms-excel.sheet.macroEnabled.12";
                case "xltm": return "application/vnd.ms-excel.template.macroEnabled.12";
                case "xlam": return "application/vnd.ms-excel.addin.macroEnabled.12";
                case "xlsb": return "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                case "pptx": return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case "potx": return "application/vnd.openxmlformats-officedocument.presentationml.template";
                case "ppsx": return "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
                case "ppam": return "application/vnd.ms-powerpoint.addin.macroEnabled.12";
                case "pptm": return "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
                case "potm": return "application/vnd.ms-powerpoint.template.macroEnabled.12";
                case "ppsm": return "application/vnd.ms-powerpoint.slideshow.macroEnabled.12";
                default: return "text/plain";
            }
        }

        public static string getContentTransferEncoding_ByFilename(string filename)
        {
            string fileExtension = String.Empty;
            if (filename.Contains("."))
            {
                fileExtension = Reverse(Reverse(filename).Substring(0, Reverse(filename).IndexOf('.')));
            }
            switch (fileExtension.ToLower())
            {
                case "docx":
                case "pdf":
                case "txt":
                    return "binary";
                default:
                    return "8bit";
            }
        }

        private static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
