﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Security.Cryptography;
using System.Security;

namespace eUzenet.HKP_KRService.Core
{
    public class KRDateTimeHandler
    {
        public static DateTime getDateTime_FromKR(string inputDate)
        {
            DateTime outDateTime = new DateTime();
            //inputDate = inputDate.Replace("T", " ").Replace("Z", "");
            if (DateTime.TryParse(inputDate, out outDateTime))
            {
                return outDateTime;
            }
            else
                return DateTime.Today;
        }

        public static string getKRDateTime(DateTime inputDate)
        {
            return inputDate.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }
    }
}
