﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eUzenet.HKP_KRService.Core
{
    /// <summary>
    /// Summary description for UzenetLetoltesManager
    /// </summary>
    public class UzenetLetoltesManager
    {
        const string EUZENET_LETOLTES_MUNKAIDO_ALAPJAN = "EUZENET_LETOLTES_MUNKAIDO_ALAPJAN";
        public UzenetLetoltesManager()
        {
        }

        bool? _LetoltesMunkaidoAlapjan = null;
        bool LetoltesMunkaidoAlapjan()
        {
            if (_LetoltesMunkaidoAlapjan == null)
            {
                string value = GetParameterValue(EUZENET_LETOLTES_MUNKAIDO_ALAPJAN);
                _LetoltesMunkaidoAlapjan = "1".Equals(value);
            }

            return _LetoltesMunkaidoAlapjan ?? false;
        }
        string GetParameterValue(string parameterName)
        {
            Contentum.eAdmin.Service.KRT_ParameterekService service = eAdminService.ServiceFactory.GetKRT_ParameterekService();
            ExecParam execParam = EdokWrapper.GetExecParam();

            KRT_ParameterekSearch paramSearch = new KRT_ParameterekSearch();
            paramSearch.Nev.Value = parameterName;
            paramSearch.Nev.Operator = Query.Operators.equals;

            Result res = service.GetAll(execParam, paramSearch);
            if (res.IsError)
            {
                Logger.Error("Rendszerparameterek GetAll hiba", execParam, res);
                return String.Empty;
            }
            else if (res.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Error(String.Format("A \"{0}\" rendszerparameter nem talalhato", parameterName));
                return String.Empty;
            }
            else
            {
                string ertek = res.Ds.Tables[0].Rows[0]["Ertek"].ToString();
                return ertek;
            }
        }
        string GetExtraNapJelzo(DateTime date)
        {
            Contentum.eAdmin.Service.KRT_Extra_NapokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
            ExecParam execParam = EdokWrapper.GetExecParam();

            KRT_Extra_NapokSearch search = new KRT_Extra_NapokSearch();
            search.Datum.Value = date.ToShortDateString();
            search.Datum.ValueTo = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59).ToString();
            search.Datum.Operator = Query.Operators.between;

            Result res = service.GetAll(execParam, search);

            if (!res.IsError)
            {
                if (res.Ds.Tables[0].Rows.Count == 0)
                    return "0";
                else
                    return res.Ds.Tables[0].Rows[0]["Jelzo"].ToString();
            }
            else
            {
                Logger.Error("KRT_Extra_NapokService GetAll hiba", execParam, res);
                return "0";
            }
        }
        bool IsMunakanap(DateTime date)
        {
            string jelzo = GetExtraNapJelzo(date);

            if ("-1".Equals(jelzo))
                return false;

            if ("1".Equals(jelzo))
                return true;

            if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        bool IsUtolsoMunakanap(DateTime date)
        {
            DateTime tomorrow = date.AddDays(1);
            return !IsMunakanap(tomorrow);
        }
        public bool CsakRendszerUzenetekLetoltese(DateTime date)
        {
            try
            {
                Logger.Debug(String.Format("CsakRendszerUzenetekLetoltese:date={0} kezdete", date));

                bool letoltesMunkaidoAlapjan = LetoltesMunkaidoAlapjan();
                Logger.Debug(String.Format("letoltesMunkaidoAlapjan={0}", letoltesMunkaidoAlapjan));

                if (!letoltesMunkaidoAlapjan)
                    return false;

                if (!IsMunakanap(date))
                {
                    return true;
                }
                else if (IsUtolsoMunakanap(date) && date.Hour >= 12)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("CsakRendszerUzenetekLetoltese:date={0} hiba", date), ex);
            }

            return false;
        }
    }
}