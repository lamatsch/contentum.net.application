﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Xml.Serialization;
using eUzenet.HKP_KRService.Core;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using Contentum.eAdmin.Service;
using System.Net;
using System.IO;

namespace eUzenet.HKP_KRService
{
    public class KRService: IeUzenetService
    {
        public void PostaFiokFeldolgozas()
        {
            LogMaster logger = new LogMaster();
            try
            {
                logger.Info("KRService#PostaFiokFeldolgozas - Lekérdezés kezdet: " + DateTime.Now.ToString());
                List<string> fiokok = GetHivataliKapuFiokok();

                foreach (string fiok in fiokok)
                {
                    logger.Info(String.Format("Fiok: {0}", fiok));
                    KRCaller caller = new KRCaller(fiok);
                    Result callerResult = caller.PostaFiokFeldolgozas();
                    if (!String.IsNullOrEmpty(callerResult.ErrorCode))
                    {
                        logger.Error(String.Format("HKPService#Postafioklekerdezes - lekérdezés hiba! Hibakód: {0}; HibaÜzenet: {1}"
                            , callerResult.ErrorCode, callerResult.ErrorMessage));
                    }
                }
                logger.Info("KRService#PostaFiokFeldolgozas  - Lekérdezés vége: " + DateTime.Now.ToString());
            }
            catch (Exception ex)
            {
                logger.Error("PostaFiokFeldolgozas hiba!", ex);
            }
        }

        public Result HivatalokListajaFeldolgozas_Szurt(string fiok, HivatalSzuroParameterek szuroParameterek)
        {
            LogMaster logger = new LogMaster();
            try
            {
                logger.Info("KRService#HivatalokListajaFeldolgozas - Lekérdezés kezdet: " + DateTime.Now.ToString());
                KRCaller caller = new KRCaller(fiok);
                Result callerResult = caller.HivatalokListajaFeldolgozas(szuroParameterek);
                if (!String.IsNullOrEmpty(callerResult.ErrorCode))
                {
                    logger.Error(String.Format("HKPService#HivatalokListajaFeldolgozas - lekérdezés hiba! Hibakód: {0}; HibaÜzenet: {1}"
                        , callerResult.ErrorCode, callerResult.ErrorMessage));
                }
                logger.Info("KRService#HivatalokListajaFeldolgozas  - Lekérdezés vége: " + DateTime.Now.ToString());
                return callerResult;
            }
            catch (Exception ex)
            {
                logger.Error("HivatalokListajaFeldolgozas hiba!", ex);
            }
            return new Result();
        }

        public Result CsoportosDokumentumFeltoltes(string fiok, List<Csatolmany>csatolmanyList, List<EREC_eBeadvanyok> dokAdatokList)
        {
            LogMaster logger = new LogMaster();
            try
            {
                logger.Info("KRService#CsoportosDokumentumFeltoltes - Feltöltés kezdete: " + DateTime.Now.ToString());
                KRCaller caller = new KRCaller(fiok);
                Result callerResult = caller.CsoportosDokumentumFeltoltes(csatolmanyList, dokAdatokList);
                if (!String.IsNullOrEmpty(callerResult.ErrorCode))
                {
                    logger.Error(String.Format("HKPService#CsoportosDokumentumFeltoltes - feltöltési hiba! Hibakód: {0}; HibaÜzenet: {1}"
                        , callerResult.ErrorCode, callerResult.ErrorMessage));
                }
                logger.Info("KRService#CsoportosDokumentumFeltoltes  - Feltöltés vége: " + DateTime.Now.ToString());
                return callerResult;
            }
            catch (Exception ex)
            {
                logger.Error("CsoportosDokumentumFeltoltes hiba!", ex);
            }
            return new Result();
        }

        public Result Azonositas(string fiok, HKP2.AzonositasKerdes azonKerdes)
        {
            LogMaster logger = new LogMaster();
            try
            {
                logger.Info("KRService#Azonositas - Lekérdezés kezdet: " + DateTime.Now.ToString());
                KRCaller caller = new KRCaller(fiok);
                Result callerResult = caller.Azonositas(azonKerdes);
                if (!String.IsNullOrEmpty(callerResult.ErrorCode))
                {
                    logger.Error(String.Format("HKPService#Azonositas - lekérdezés hiba! Hibakód: {0}; HibaÜzenet: {1}"
                        , callerResult.ErrorCode, callerResult.ErrorMessage));
                }
                logger.Info("KRService#Azonositas  - Lekérdezés vége: " + DateTime.Now.ToString());
                return callerResult;
            }
            catch (Exception ex)
            {
                logger.Error("Azonositas hiba!", ex);
            }
            return new Result();
        }

        public string DownloadFileShareDokumentumok(string destination, string filter)
        {
            try
            {
                ExecParam execParam = EdokWrapper.GetExecParam();

                EREC_eBeadvanyokService eBeadvanyokService = EdokWrapper.GetService();
                EREC_eBeadvanyokSearch eBeadvanyokSearch = new EREC_eBeadvanyokSearch();

                eBeadvanyokSearch.Cel.Value = EdokWrapper.Rendszer.FileShare;
                eBeadvanyokSearch.Cel.Operator = Contentum.eQuery.Query.Operators.equals;

                if (!String.IsNullOrEmpty(filter))
                {
                    eBeadvanyokSearch.WhereByManual = " and " + filter;
                }

                Result eBeadvanyokResult = eBeadvanyokService.GetAll(execParam, eBeadvanyokSearch);

                if (eBeadvanyokResult.IsError)
                {
                    throw new Exception(String.Format("EREC_eBeadvanyokService.GetAll hiba - {0}", eBeadvanyokResult.ErrorMessage));
                }

                DataTable eBeadvanyokTable = eBeadvanyokResult.Ds.Tables[0];

                if (eBeadvanyokTable.Rows.Count > 0)
                {
                    List<string> eBeadvanyIds = GetColumnValues(eBeadvanyokTable, "Id");

                    EREC_eBeadvanyCsatolmanyokService csatolmanyokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();

                    EREC_eBeadvanyCsatolmanyokSearch csatolmanyokSearch = new EREC_eBeadvanyCsatolmanyokSearch();
                    csatolmanyokSearch.eBeadvany_Id.Value = Contentum.eUtility.Search.GetSqlInnerString(eBeadvanyIds.ToArray());
                    csatolmanyokSearch.eBeadvany_Id.Operator = Contentum.eQuery.Query.Operators.inner;

                    Result csatolmanyokResult = csatolmanyokService.GetAll(execParam, csatolmanyokSearch);

                    if (csatolmanyokResult.IsError)
                    {
                        throw new Exception(String.Format("EREC_eBeadvanyCsatolmanyokService.GetAll hiba - {0}", csatolmanyokResult.ErrorMessage));
                    }

                    DataTable csatolmanyokTable = csatolmanyokResult.Ds.Tables[0];

                    if (csatolmanyokTable.Rows.Count > 0)
                    {
                        List<string> dokumentumIds = GetColumnValues(csatolmanyokTable, "Dokumentum_Id");

                        KRT_DokumentumokService dokumentumokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_DokumentumokService();

                        KRT_DokumentumokSearch dokumentumokSearch = new KRT_DokumentumokSearch();
                        dokumentumokSearch.Id.Value = Contentum.eUtility.Search.GetSqlInnerString(dokumentumIds.ToArray());
                        dokumentumokSearch.Id.Operator = Contentum.eQuery.Query.Operators.inner;

                        Result dokumentumokResult = dokumentumokService.GetAll(execParam, dokumentumokSearch);

                        if (dokumentumokResult.IsError)
                        {
                            throw new Exception(String.Format("KRT_DokumentumokService.GetAll hiba - {0}", dokumentumokResult.ErrorMessage));
                        }

                        DataTable dokumentumokTable = dokumentumokResult.Ds.Tables[0];

                        foreach (DataRow row in dokumentumokTable.Rows)
                        {
                            string fileName = row["FajlNev"].ToString();
                            string externalLink = row["External_Link"].ToString();
                            SaveFile(destination, fileName, externalLink);
                        }

                    }

                    foreach (string eBeadvanyId in eBeadvanyIds)
                    {
                        ExecParam invalidateExecParam = execParam.Clone();
                        invalidateExecParam.Record_Id = eBeadvanyId;

                        Result invalidateResult = eBeadvanyokService.Invalidate(invalidateExecParam);

                        if (invalidateResult.IsError)
                        {
                            throw new Exception(String.Format("EREC_eBeadvanyCsatolmanyokService.Invalidate hiba - {0}", invalidateResult.ErrorMessage));
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return String.Format("HIBA: {0}", ex.Message);
            }

            return "SIKERES";
        }

        List<string> GetColumnValues(DataTable table, string colName)
        {
            List<string> values = new List<string>();

            foreach (DataRow row in table.Rows)
            {
                values.Add(row[colName].ToString());
            }

            return values;
        }

        byte[] GetDocumentContent(string externalLink)
        {
            using (WebClient wc = new System.Net.WebClient())
            {
                string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserName");
                string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServicePassword");
                string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserDomain");
                wc.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                return wc.DownloadData(externalLink);
            }
        }

        void SaveFile(string destination, string fileName, string externalLink)
        {
            byte[] fileContent = GetDocumentContent(externalLink);

            string filePath = Path.Combine(destination, fileName);

            FileInfo fi = new FileInfo(filePath);
            fi.Directory.Create();

            File.WriteAllBytes(filePath, fileContent);
        }

        public List<string> GetHivataliKapuFiokok()
        {
            List<string> fiokok = new List<string>();

            var fiokConfigElements = HivataliKapuConfig.GetFiokok();

            foreach (FiokElement fiok in fiokConfigElements)
            {
                fiokok.Add(fiok.Name);
            }

            return fiokok;
        }

        public Result PublikusKulcsLekerdezese(string fiok, string kapcsolatiKod, string KRID)
        {
            LogMaster logger = new LogMaster();
            logger.Info("KRService#PublikusKulcsLekerdezese - Lekérdezés kezdete: " + DateTime.Now.ToString());

            Result result =  new Result();
            try
            {
                KRCaller caller = new KRCaller(fiok);
                HKP2.KulcsLekerdezesKerdes kerdes = new HKP2.KulcsLekerdezesKerdes();
                kerdes.AddTulajdonos(kapcsolatiKod, KRID);
                HKP2.KulcsLekerdezesValasz valasz = caller.KulcsokLekerdezese(kerdes);
                if (valasz.HibaUzenet != null)
                {
                    result.ErrorCode = valasz.HibaUzenet.Szam.ToString();
                    result.ErrorMessage = valasz.HibaUzenet.Tartalom;
                }
                else
                {
                    if (valasz.PublikusKulcsAdatokList[0].Hiba != null)
                    {
                        result.ErrorCode = valasz.PublikusKulcsAdatokList[0].Hiba.HibaLeiras.HibaKod;
                        result.ErrorMessage = valasz.PublikusKulcsAdatokList[0].Hiba.HibaLeiras.HibaSzoveg;
                    }
                    else
                    {
                        result.Record = valasz.PublikusKulcsAdatokList[0].PublikusKulcs;
                    }
                }

            }
            catch (Exception ex)
            {
                result = Contentum.eUtility.ResultException.GetResultFromException(ex);
            }

            if (result.IsError)
            {
                logger.Error(String.Format("KRService#PublikusKulcsLekerdezese hiba: {0}, {1}", result.ErrorCode, result.ErrorMessage));
            }

            logger.Info("KRService#PublikusKulcsLekerdezese  - Lekérdezés vége: " + DateTime.Now.ToString());
            return result;

        }
    }
}