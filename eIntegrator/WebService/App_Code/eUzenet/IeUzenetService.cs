﻿using Contentum.eBusinessDocuments;
using eUzenet.HKP_KRService.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eUzenet
{
    /// <summary>
    /// Summary description for IeUzenetService
    /// </summary>
    public interface IeUzenetService
    {
        void PostaFiokFeldolgozas();
        Result HivatalokListajaFeldolgozas_Szurt(string fiok, HivatalSzuroParameterek szuroParameterek);
        Result CsoportosDokumentumFeltoltes(string fiok, List<Csatolmany> csatolmanyList, List<EREC_eBeadvanyok> dokAdatokList);
        Result Azonositas(string fiok, HKP2.AzonositasKerdes azonKerdes);
    }
}