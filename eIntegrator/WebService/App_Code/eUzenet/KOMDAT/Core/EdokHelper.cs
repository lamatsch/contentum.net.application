﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using eUzenet.KOMDAT.ServiceReference;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace eUzenet.KOMDAT.Core
{
    /// <summary>
    /// Summary description for EdokHelper
    /// </summary>
    public class EdokHelper
    {
        public static List<string> GetVevenyreVaroUzenetek()
        {
            Logger.Info("GetVevenyreVaroUzenetek kezdete");

            List<string> erkeztetesiSzamok = new List<string>();

            EREC_eBeadvanyokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            ExecParam execParam = HKP_KRService.Core.EdokWrapper.GetExecParam();
            EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
            search.Irany.Value = "1";
            search.Irany.Operator = Query.Operators.equals;
            search.Allapot.Value = "11"; //elküldött
            search.Allapot.Operator = Query.Operators.equals;


            Logger.Debug("EREC_eBeadvanyokService.GetAll");
            Result result = service.GetAll(execParam, search);

            if (result.IsError)
            {
                throw new Exception(String.Format("EREC_eBeadvanyokService.GetAll hiba: {0}", result.ErrorMessage));
            }

            Logger.Debug(String.Format("Üzenetek száma: {0}", result.Ds.Tables[0].Rows.Count));

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                string erkSzam = row["KR_ErkeztetesiSzam"].ToString();
                erkeztetesiSzamok.Add(erkSzam);
            }

            Logger.Debug(String.Format("Érkeztetési számok: {0}", String.Join(",", erkeztetesiSzamok.ToArray())));

            Logger.Info("GetVevenyreVaroUzenetek vege");

            return erkeztetesiSzamok;
        }

        public static eBeadvanyWithCsatolmany GetBuisnessObjectFormIgazolas(string erkeztetesiSzam, Igazolas igazolas)
        {
            Logger.Info("GetBuisnessObjectFormIgazolas kezdete");
            Logger.Debug(String.Format("erkezteteisSzam={0}", erkeztetesiSzam));
            Logger.Debug(String.Format("igazolas={0}", igazolas.FajlNev));

            EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();
            eBeadvany.Irany = "0"; //bejövő
            eBeadvany.Cel = HKP_KRService.Core.EdokWrapper.Rendszer.WEB;
            eBeadvany.KR_ErkeztetesiSzam = igazolas.Href;
            eBeadvany.PR_ErkeztetesiSzam = igazolas.BelsoId.ToString();
            eBeadvany.KR_HivatkozasiSzam = erkeztetesiSzam;
            eBeadvany.KR_ErkeztetesiDatum = igazolas.Bekuldve.ToString();
            eBeadvany.KR_FileNev = igazolas.FajlNev;
            eBeadvany.KR_DokTipusAzonosito = GetDoktipusAzonosito(igazolas.FajlNev);

            Csatolmany csatolmany = new Csatolmany();
            csatolmany.Nev = igazolas.FajlNev;
            csatolmany.Tartalom = igazolas.FajlContent;

            Logger.Info("GetBuisnessObjectFormIgazolas vege");
            return new eBeadvanyWithCsatolmany {eBeadvany = eBeadvany, Csatolmany = csatolmany };
        }

        static string GetDoktipusAzonosito(string fajlNev)
        {
            Logger.Info("GetDoktipusAzonosito kezdete");
            Logger.Debug(String.Format("fajlNev={0}", fajlNev));

            string dokTipus = fajlNev;
            if (!String.IsNullOrEmpty(fajlNev))
            {
                int index = fajlNev.IndexOf('-');

                if (index > -1)
                {
                    dokTipus = fajlNev.Substring(0, index);
                    dokTipus = dokTipus.Replace("_", "");
                }
                else
                {
                    Logger.Error("A DoktipusAzonosito meghatározása sikertelen!");
                }
            }
            else
            {
                Logger.Error("A fajlNev üres!");
            }

            Logger.Debug(String.Format("dokTipus={0}", dokTipus));
            Logger.Info("GetDoktipusAzonosito vege");
            return dokTipus;
        }

        public class eBeadvanyWithCsatolmany
        {
            public EREC_eBeadvanyok eBeadvany { get; set; }
            public Csatolmany Csatolmany { get; set; }
        }

        private static bool eBeadvanyExists(string erkeztetesiSzam)
        {
            Logger.Info("eBeadvanyExists kezdete");
            Logger.Debug(String.Format("erkezteteisSzam={0}", erkeztetesiSzam));

            bool exists = false;

            EREC_eBeadvanyokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            ExecParam execParam = HKP_KRService.Core.EdokWrapper.GetExecParam();
            EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
            search.KR_ErkeztetesiSzam.Value = erkeztetesiSzam;
            search.KR_ErkeztetesiSzam.Operator = "=";
            search.Irany.Value = "0";
            search.Irany.Operator = "=";
            search.ErvKezd.Clear();
            search.ErvVege.Clear();

            Result result = service.GetAll(execParam, search);

            if (result.IsError)
            {
                throw new Exception(String.Format("EREC_eBeadvanyokService.GetAll hiba: {0}", result.ErrorMessage));
            }
            else
            {
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    exists = true;
                }
            }

            Logger.Debug(String.Format("exists={0}", exists));
            Logger.Info("eBeadvanyExists vege");
            return exists;
        }

        public static bool InsertAndAttachDocument(eBeadvanyWithCsatolmany eBeadvanyWithCsatolmany)
        {
            bool sikeres = false;
            Logger.Info("InsertAndAttachDocument kezdete");
            Logger.Debug(String.Format("KR_ErkeztetesiSzam={0}", eBeadvanyWithCsatolmany.eBeadvany.KR_ErkeztetesiSzam));

            try
            {
                EREC_eBeadvanyokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
                ExecParam execParam = HKP_KRService.Core.EdokWrapper.GetExecParam();

                if (!eBeadvanyExists(eBeadvanyWithCsatolmany.eBeadvany.KR_ErkeztetesiSzam))
                {
                    Logger.Info("EREC_eBeadvanyokService.InsertAndAttachDocument kezdete");
                    Result result = service.InsertAndAttachDocument(execParam, eBeadvanyWithCsatolmany.eBeadvany, new Csatolmany[] { eBeadvanyWithCsatolmany.Csatolmany });
                    Logger.Info("EREC_eBeadvanyokService.InsertAndAttachDocument vege");

                    if (result.IsError)
                    {
                        Logger.Error("EREC_eBeadvanyokService.EREC_eBeadvanyokService hiba", execParam, result);
                    }
                    else
                    {
                        sikeres = true;
                        string eBeadvanyId = result.Uid;
                        result = service.PostProcess(execParam, eBeadvanyId);

                        if (result.IsError)
                        {
                            Logger.Error("EREC_eBeadvanyokService.PostProcess hiba", execParam, result);
                        }
                    }
                }
                else
                {
                    sikeres = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("InsertAndAttachDocument hiba", ex);
            }

            Logger.Info("InsertAndAttachDocument vege");
            return sikeres;
        }
    }
}