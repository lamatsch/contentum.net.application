﻿using Contentum.eUtility;
using eUzenet.KOMDAT.ServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eUzenet.KOMDAT.Core
{
    /// <summary>
    /// Summary description for KapuWsHelper
    /// </summary>
    public class KapuWsHelper
    {
        public static List<Igazolas> GetIgazolasok(string erkeztetesiSzam)
        {
            Logger.Info("GetIgazolasok kezdete");
            Logger.Debug(String.Format("erkeztetesiSzam={0}", erkeztetesiSzam));
            List<Igazolas> igazolasok = null;

            try
            {
                KapuWs ws = ServiceFactory.GetKapuWs();

                string error;
                KapuWsIgazolasokResponse result = ws.GetIgazolasok(erkeztetesiSzam, IdType.Href, true, out error);

                if (!String.IsNullOrEmpty(error))
                {
                    throw new Exception(error);
                }
                else if (!String.IsNullOrEmpty(result.Error))
                {
                    throw new Exception(result.Error);
                }
                else
                {
                    if (result.Igazolasok != null)
                    {
                        igazolasok = new List<Igazolas>(result.Igazolasok);
                        string fajlNevek = String.Join(",", igazolasok.Select(i => i.FajlNev).ToArray());
                        Logger.Debug(String.Format("Igazolasok: {0}", fajlNevek));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetIgazolasok hiba", ex);
            }

            Logger.Info("GetIgazolasok vege");
            return igazolasok;
        }

        public static List<Igazolas> GetIgazolasok(List<string> erkeztetesiSzamok)
        {
            List<Igazolas> igazolasok = new List<Igazolas>();

            foreach (string erkeztetesiSzam in erkeztetesiSzamok)
            {
                List<Igazolas> erkSzamIgazolasai = GetIgazolasok(erkeztetesiSzam);
                if (erkSzamIgazolasai != null)
                {
                    igazolasok.AddRange(erkSzamIgazolasai);
                }
            }

            return igazolasok;
        }
    }
}