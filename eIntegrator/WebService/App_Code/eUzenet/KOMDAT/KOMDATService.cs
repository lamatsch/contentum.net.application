﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using eUzenet.KOMDAT.Core;
using eUzenet.HKP_KRService.Core;
using eUzenet.KOMDAT.ServiceReference;

namespace eUzenet.KOMDAT
{
    /// <summary>
    /// Summary description for KRService
    /// </summary>
    public class KOMDATService : IeUzenetService
    {
        eUzenet.HKP_KRService.KRService _KRService = new HKP_KRService.KRService();
        public KOMDATService()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public void PostaFiokFeldolgozas()
        {
            Logger.Debug("KOMDATService.PostaFiokFeldolgozas kezdete");

            try
            {
                List<string> erkeztetesiSzamok = EdokHelper.GetVevenyreVaroUzenetek();

                foreach (string erkeztetesiSzam in erkeztetesiSzamok)
                {
                    List<Igazolas> igazolasok = KapuWsHelper.GetIgazolasok(erkeztetesiSzam);

                    if (igazolasok != null)
                    {
                        foreach (Igazolas igazolas in igazolasok.OrderBy(i => i.Bekuldve))
                        {
                            EdokHelper.eBeadvanyWithCsatolmany eBeadvanyWithCsatolmany = EdokHelper.GetBuisnessObjectFormIgazolas(erkeztetesiSzam, igazolas);
                            EdokHelper.InsertAndAttachDocument(eBeadvanyWithCsatolmany);
                        }
                    }
                }

                
            }
            catch (Exception ex)
            {
                Logger.Error("KOMDATService.PostaFiokFeldolgozas hiba!", ex);
            }

            Logger.Debug("KOMDATService.PostaFiokFeldolgozas vege");
        }

        public Result Azonositas(string fiok, HKP2.AzonositasKerdes azonKerdes)
        {
            return _KRService.Azonositas(fiok, azonKerdes);
        }

        public Result CsoportosDokumentumFeltoltes(string fiok, List<Csatolmany> csatolmanyList, List<EREC_eBeadvanyok> dokAdatokList)
        {
            return _KRService.CsoportosDokumentumFeltoltes(fiok, csatolmanyList, dokAdatokList);
        }

        public Result HivatalokListajaFeldolgozas_Szurt(string fiok, HivatalSzuroParameterek szuroParameterek)
        {
            return _KRService.HivatalokListajaFeldolgozas_Szurt(fiok, szuroParameterek);
        }
    }
}