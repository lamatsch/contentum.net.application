﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace eUzenet.KOMDAT.ServiceReference
{
    /// <summary>
    /// Summary description for ServiceFactory
    /// </summary>
    public class ServiceFactory
    {
        public static KapuWs GetKapuWs()
        {
            string serviceUrl = ConfigurationManager.AppSettings.Get("KOMDAT_KapuWsUrl");
            KapuWs _Service = new KapuWs(serviceUrl);
            return _Service;
        }
    }
}