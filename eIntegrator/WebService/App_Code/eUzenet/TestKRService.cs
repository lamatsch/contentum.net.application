﻿using eUzenet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using eUzenet.HKP_KRService.Core;

namespace eUzenet.HKP_KRService
{
    /// <summary>
    /// Summary description for TestKRService
    /// </summary>
    public class TestKRService : IeUzenetService
    {
        public TestKRService()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public Result Azonositas(string fiok, HKP2.AzonositasKerdes azonKerdes)
        {
            throw new NotImplementedException();
        }

        public Result CsoportosDokumentumFeltoltes(string fiok, List<Csatolmany> csatolmanyList, List<EREC_eBeadvanyok> dokAdatokList)
        {
            HKP2.CsoportosDokumentumFeltoltesValasz csopdokFelValasz = new HKP2.CsoportosDokumentumFeltoltesValasz();
            csopdokFelValasz.UjDokumentumAdatokList = new List<HKP2.UjDokumentumAdatokValasz>();

            foreach (EREC_eBeadvanyok ebeadvany in dokAdatokList)
            {
                csopdokFelValasz.UjDokumentumAdatokList.Add(new HKP2.UjDokumentumAdatokValasz
                {
                    ErkeztetesiSzam = Guid.NewGuid().ToString(),
                    ErkeztetesiDatum = DateTime.Now.ToString()
                });
            }

            Result _return = new Result();
            _return.Record = csopdokFelValasz;
            return _return;
            
        }

        public Result HivatalokListajaFeldolgozas_Szurt(string fiok, HivatalSzuroParameterek szuroParameterek)
        {
            throw new NotImplementedException();
        }

        public void PostaFiokFeldolgozas()
        {
            throw new NotImplementedException();
        }

        public Result PublikusKulcsLekerdezese(string fiok, string kapcsolatiKod, string KRID)
        {
            return new Result
            {
                Record = @"-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: BCPG v1.33

mQELBERPc9gBCADTQHC7WsUtf2n/GkS6DrqYi2Cd4gB2vhdY9hQT4ShOYxJ96M12
Visyc/4fIn+PsBKkG8tpXPfDs6PLEX349PXOsk0il1PDJ9joEN5a1FPPkmw2r/2D
FjvJ2gpHVjNXsrlgBo6ongWgCaQ9VblnNqu/SuRxBn167n3sZLqba5znkfiXQpcp
wMjBTfl0paXMKH4qY+s1UMOLbuoSMngtrPdSZYFpPkvi9Zr2QZ4gyw5eAHH7OLMI
vjZdjh4U7e6EuvukVxsEkvAiqWAPh8yxIBf2VGSViiNpOJ3gkM8kEtRtbZOB818r
wl1pcn8gFFe4SJw1x3awH3c+0W9x4g+H/RA/AAYptBNBUEVIIDxlYmV2QGFwZWgu
aHU+iQE2BBMBAgAgBQJET3PYAhsDBgsJCAcDAgQVAggDBBYCAwECHgECF4AACgkQ
Q1Dz7lBclTapPQgAoh0um31+lUJDES7gS79Jt9PM0pBBYcP2GaLvYERHgGDo1zEC
w6Lop3F+5YBVWxnwurf8+iCUe/tWL7goCxWThW/jpQKqf9ujX+PV4DggljQZ4qux
ecMUCVHsHFRU6MsCGIBhk/Lledbi3P7G3+b9UN27E4geKrJ7MtEYTtXQxGbioXBI
IhvzhrJIkejUW8QN5EAxC5PzZE9PhvjjSlIy2TWoZcnl/bnCSfI9qKR4rDejN8MY
xh9cn/vWhORtsvvU8w37Xdfgcztb9dDHCIGl/sHt6xsyHIrJjsrB1MSW/pCgZB61
s/HDYdeUFe/2Nr9cVe0l9ePuw0x5PxIyUuuiH7kBCwRET3PlAQgA7y9IBraIJCet
d6iCWImJ1AmqFJWHV9DvMs/HHqONv63d7NZQA4lQQLH2NX61za50h52ODtNZL0eK
rz4XfDRYJEZyNywrz/UqBIvVIo3s/jxuxqFJCoP22IKrftVGxSLMpkeuRjIXl7br
c7i5nGcEOhdA64JAvRLbggX4Uyh/YEoNZKxwjAQvEcZ7B7xcHtx4kkGOk+Mrwnde
mBaqYmh3WlByhMNFTJnf/aeZ8g3KQL8puLQ+EXhNV8bhUcV50COtw+X4/MXN7gb5
nNdHyU6OGOib6FDYL/7vVysIv9DG3C8on5Z5RyATpJxGDtx+N/iCX1YdCyQko9c6
EX2Lzkil6wAGKYkCPgQYAQIACQUCRE9z5QIbLgEpCRBDUPPuUFyVNsBdIAQZAQIA
BgUCRE9z5QAKCRCPhkU4WA22pSbiCACotm5TU3weKTm33zCwV2THrtkyjmBfjNRE
jZ3vf5BkdSgTWSm5TfUwG0Wm4eDsS2WxMynteDGPB9QNyHq0u9jlreu6iQwFp8hf
xQzPesz6oYqti3dqOmID4VFKH6MaOow9MmIO1TtqnUGaNVAg3tmRQzIAuxsdv1rX
IciWRIAAUI1rPufbUVT3+BXw+BRA1pkGuHDcd9NFSwzRPO9DtD831Vf0JxQG9tmE
ilRY7x5Sl6AUQpiOCOopkNOB+H0NtcJq3fnvqqANgyz8SHGRUug0QmsVPNh5kzdS
ekcMDDdykFu7xehKM3jHAhq0A5rWQvcDN7mtt1B0e4ce5kv6XQtiif8H/iTpdIlh
IsD8pk+6jYKCWmipxKv8dlREivQPKp5yaGd3JKyOfrpb6NCZIRP6W7lh5F6zDaem
MWhPgI8+h+l1hMrlSg19IJeNg+IvDfFMY0+t1R/0SjEriSWlbhc6gi25aAWPRFrT
KRjx81gGXM3OcBLRC4mRyS+/0gfsxRf8q0Nk621X+FalOFodQNt31R7bf3QYVK33
hHzZMs0MFwWGLr3eo74whKX9ulJuF1UrRM1ur/ah3lrskKxyJ22ibwHLLuRp9087
OL5tbUevzcDLHM/mUcidw1SHo+DmXWjrL5JyfcMqzvLyo6hr5dgwMQJOR+BgwPEO
e7dlqSH7DWxVTzc=
=9O0z
-----END PGP PUBLIC KEY BLOCK-----"
            };
        }
    }
}