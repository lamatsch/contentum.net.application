﻿using Contentum.eBusinessDocuments;
using eUzenet;
using eUzenet.HKP_KRService.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;

/// <summary>
/// Summary description for eUzenetService
/// </summary>
[WebService(Namespace = "Contentum.eIntegrator.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class eUzenetService : System.Web.Services.WebService
{
    IeUzenetService _Service;
    string forras;

    private IeUzenetService CreateService()
    {
        if ("ELHISZ".Equals(forras))
        {
            return new eUzenet.Elhisz.ElhiszService();
        }
        else if ("KOMDAT".Equals(forras))
        {
            return new eUzenet.KOMDAT.KOMDATService();
        }
        else if ("TestHivataliKapu".Equals(forras))
        {
            return new eUzenet.HKP_KRService.TestKRService();
        }
        else
        {
            return new eUzenet.HKP_KRService.KRService();
        }
    }

    public eUzenetService()
    {
        forras = IntegratorParameterek.GetParameter(IntegratorParameterek.eUzenetForras);
        _Service = CreateService();
    }

    [WebMethod]
    public void PostaFiokFeldolgozas()
    {
        _Service.PostaFiokFeldolgozas();
    }

    [WebMethod]
    [XmlInclude(typeof(HKP2.HivatalokListajaValasz))]
    public Result HivatalokListajaFeldolgozas(string fiok)
    {
        return HivatalokListajaFeldolgozas_Szurt(fiok, new HivatalSzuroParameterek());
    }

    [WebMethod]
    [XmlInclude(typeof(HKP2.HivatalokListajaValasz))]
    public Result HivatalokListajaFeldolgozas_Szurt(string fiok, HivatalSzuroParameterek szuroParameterek)
    {
        return _Service.HivatalokListajaFeldolgozas_Szurt(fiok, szuroParameterek);
    }

    [WebMethod]
    [XmlInclude(typeof(HKP2.CsoportosDokumentumFeltoltesValasz))]
    public Result CsoportosDokumentumFeltoltes(string fiok, List<Csatolmany> csatolmanyList, List<EREC_eBeadvanyok> dokAdatokList)
    {
        return _Service.CsoportosDokumentumFeltoltes(fiok, csatolmanyList, dokAdatokList);
    }

    [WebMethod]
    [XmlInclude(typeof(HKP2.AzonositasValasz))]
    public Result Azonositas(string fiok, HKP2.AzonositasKerdes azonKerdes)
    {
        return _Service.Azonositas(fiok, azonKerdes);
    }

    [WebMethod]
    public string DownloadFileShareDokumentumok(string destination, string filter)
    {
        eUzenet.HKP_KRService.KRService krService = new eUzenet.HKP_KRService.KRService();
        return krService.DownloadFileShareDokumentumok(destination, filter);
    }

    [WebMethod]
    public List<string> GetHivataliKapuFiokok()
    {
        eUzenet.HKP_KRService.KRService krService = new eUzenet.HKP_KRService.KRService();
        return krService.GetHivataliKapuFiokok();
    }
    [WebMethod]
    public Result PublikusKulcsLekerdezese(string fiok, string kapcsolatiKod, string KRID)
    {
        if ("TestHivataliKapu".Equals(forras))
        {
            eUzenet.HKP_KRService.TestKRService krService = new eUzenet.HKP_KRService.TestKRService();
            return krService.PublikusKulcsLekerdezese(fiok, kapcsolatiKod, KRID);
        }
        else
        {
            eUzenet.HKP_KRService.KRService krService = new eUzenet.HKP_KRService.KRService();
            return krService.PublikusKulcsLekerdezese(fiok, kapcsolatiKod, KRID);
        }
    }
}
