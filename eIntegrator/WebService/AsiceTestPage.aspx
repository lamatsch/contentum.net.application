﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AsiceTestPage.aspx.cs" Inherits="AsiceTestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            Asice fájl
        </div>
        <div>
            <asp:FileUpload ID="AsiceFile" runat="server" Width="500px"/>
        </div>
        <div style="margin-top: 10px;">
            <asp:Button ID="btnProcess" runat="server" Text="Feldolgozás" OnClick="btnProcess_Click"/>
        </div>
        <div>
            <asp:Label ID="output" runat="server" />
        </div>
        <div>
            RegistrationNumber
        </div>
        <div>
            <asp:TextBox ID="RegistrationNumber" runat="server" />
        </div>
        <div style="margin-top: 10px;">
            <asp:Button ID="btnOracle" runat="server" Text="Oracle kapcsolat" OnClick="btnOracle_Click"/>
        </div>
        <div>
            <asp:Label ID="outputOracle" runat="server" />
            <asp:GridView ID="grid" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
