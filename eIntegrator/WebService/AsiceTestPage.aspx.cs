﻿using eUzenet.FileManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AsiceTestPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnProcess_Click(object sender, EventArgs e)
    {
        output.Text = String.Empty;
        if (!AsiceFile.HasFile)
        {
            output.Text = "Nincs megadva fájl!";
            return;
        }

        string error;
        List<string> referencedFiles;
        AsiceFileManager manager = new AsiceFileManager();
        bool success = manager.ProcessFile(AsiceFile.FileName, AsiceFile.FileBytes, out referencedFiles, out error);

        if (!success)
        {
            output.Text = error;
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ul>");

            foreach (string file in referencedFiles)
            {
                sb.Append("<li>");
                sb.Append(file);
                sb.Append("</li>");

            }

            sb.Append("</ul>");

            output.Text = sb.ToString();
        }
    }

    protected void btnOracle_Click(object sender, EventArgs e)
    {
        try
        {
            eUzenet.Elhisz.DataBaseManager db = new eUzenet.Elhisz.DataBaseManager();
//           DataSet ds;

            eUzenet.Elhisz.ElhiszManager manager = new eUzenet.Elhisz.ElhiszManager();


            //if (String.IsNullOrEmpty(RegistrationNumber.Text))
            //{
            //    ds = db.GetSubmissionInformations();
            //}
            //else
            //{
            //    ds = db.GetSubmissionData(RegistrationNumber.Text);
            //}

            //var result = db.GetSubmissionInformations();

            //grid.DataSource = result;
            //grid.DataBind();

            outputOracle.Text = IntegratorParameterek.GetParameter(IntegratorParameterek.eUzenetForras);
        }
        catch (Exception ex)
        {
            outputOracle.Text = ex.ToString();
        }
        
    }
}