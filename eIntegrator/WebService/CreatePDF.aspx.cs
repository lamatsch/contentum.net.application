﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using NMHH.Elhitet.BusinessObject;
using NMHH.Elhitet.Database;
using NMHH.Elhitet.Servlet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CreatePDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string dokumentumId = Request.QueryString.Get("DokumentumId");
        string method = Request.QueryString.Get("Method");

        Response.Clear();
        Response.ClearContent();
        Response.ClearHeaders();

        try
        {
            if (String.IsNullOrEmpty(dokumentumId))
                throw new Exception("A dokumentum id paraméter nincs megadva!");

            ElhitetStoredProcedure sp = new ElhitetStoredProcedure();
            ExecParam execParam = new ExecParam();
            Result result = sp.GetPDFAdatok(execParam, dokumentumId);

            if (result.IsError)
                throw new ResultException(result);

            PdfAdatok pdfAdatok = result.Record as PdfAdatok;

            ElhitetServletManager manager = new ElhitetServletManager();
            ServletResponse response;
            if (method == "POST")
            {
                byte[] fileContent = GetDokTartalom(pdfAdatok.External_Link);
                response = manager.CreatePDF(fileContent);
            }
            else
            {
                response = manager.CreatePDF(pdfAdatok.ElhiszErkeztetoszam, pdfAdatok.Ugyiratszam, pdfAdatok.Erkeztetoszam, pdfAdatok.GetErkezettDatumString());
            }

            Response.OutputStream.Write(response.ResponseData, 0, response.ResponseData.Length);

            foreach (string header in response.ResponseHeaders)
            {
                Response.AddHeader(header, response.ResponseHeaders[header]);
            }

            string fileName = String.Format("{0}{1:yyyyMMddHHmmss}.pdf", pdfAdatok.FajlNev, DateTime.Now);
            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + EncodeFileName(fileName) + "\";");


            Response.OutputStream.Flush();
            Response.OutputStream.Close();

            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            Response.ClearHeaders();
            Response.Write(ex.Message);
        }

        Response.Flush();
        Response.Close();
        Response.End();
    }

    private string EncodeFileName(string fileName)
    {
        if (String.IsNullOrEmpty(fileName))
            return String.Empty;

        fileName = HttpUtility.UrlEncode(fileName);
        fileName = fileName.Replace("+", "%20");

        return fileName;
    }

    private byte[] GetDokTartalom(string externalLink)
    {
        using (WebClient client = new WebClient())
        {
            client.UseDefaultCredentials = true;
            return client.DownloadData(externalLink);
        }
    }
}