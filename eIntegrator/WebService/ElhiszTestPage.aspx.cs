﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Elhisz.DataBase;
using NMHH.Elhitet.Database;
using NMHH.Elhitet.Servlet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ElhiszTestPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnTest_Click2(object sender, EventArgs e)
    {
        List<string> parameters = new List<string>();
        parameters.Add("B /61 - 1 /2018");
        parameters.Add("B /63 - 1 /2018");
        ElhitetOracleDatabaseManager db = new ElhitetOracleDatabaseManager();
        DataSet ds = db.GetResolutionData("B /61 - 1 /2018");

        string fileName = (string)ds.Tables[0].Rows[0]["filenev"];
        byte[] fileContent = (byte[])ds.Tables[0].Rows[0]["completecontent"];

        System.IO.File.WriteAllBytes(System.IO.Path.Combine(@"C:\Temp", fileName), fileContent);
    }

    protected void btnTest_Click1(object sender, EventArgs e)
    {
        List<string> parameters = new List<string>();
        parameters.Add("B /61 - 1 /2018");
        parameters.Add("B /63 - 1 /2018");
        ElhiszOracleDatabaseManager db = new ElhiszOracleDatabaseManager();


        DataSet ds = db.GetRecipientData(parameters);

        grid.DataSource = ds;
        grid.DataBind();

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            string resolutionNumber = (string)row["resolutionnumber"];
            long xml_contact_id = (long)row["xml_contact_id"];
            long errorcode = (long)row["errorcode"];
            string kr_registrationnumber = (string)row["kr_registrationnumber"];
        }
    }

    protected void btnTest_Click(object sender, EventArgs e)
    {
        ElhitetServletManager servlet = new ElhitetServletManager();
        ELHISZServerResolutionResponse resolutionResponse = servlet.UnsignedResolution(File.FileName, "Contentum.eIntegratorWebService.INT_ElhiszService", "teszt",
            null, "Informatikai fejlesztési osztály", null, null, File.FileBytes);

        if (resolutionResponse.IsError)
        {
            output.Text = String.Format("UnsignedResolution hiba: ErrorCode={0}, ErrorMessage={1}", resolutionResponse.ErrorCode, resolutionResponse.ErrorMessage);
        }
    }

    void CsatolmanyUpload(string kuldemenyId, string fileName, byte[] fileContent)
    {
        EREC_KuldKuldemenyekService erec_KuldKuldemenyekService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam uploadExecParam = new ExecParam();
        uploadExecParam.Felhasznalo_Id = "37821BB7-54BA-E711-80C7-00155D027E9B";
        uploadExecParam.Record_Id = kuldemenyId;

        EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
        erec_Csatolmanyok.KuldKuldemeny_Id = kuldemenyId;

        Csatolmany csatolmany = new Csatolmany();
        csatolmany.Nev = fileName;
        csatolmany.Tartalom = fileContent;

        Result result = erec_KuldKuldemenyekService.CsatolmanyUpload(uploadExecParam, erec_Csatolmanyok, csatolmany);

        if (result.IsError)
            throw new ResultException(result);
    }
}