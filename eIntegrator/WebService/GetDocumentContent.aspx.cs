﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Net;
using System.IO;

using Contentum.eUtility;
using Contentum.eDocument.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.Text.RegularExpressions;

public partial class GetDocumentContent : System.Web.UI.Page
{
    ContentDownloader contentDownloader = new ContentDownloader();
    protected void Page_PreRender(object sender, EventArgs e)
    {
        string externalLink = string.Empty;
        string filename = string.Empty;
        string guid = string.Empty;
        string currentVersion = String.Empty;
        string version = Request.QueryString.Get("version");
        string mode = Request.QueryString.Get("mode") ?? String.Empty;
        string filesource = string.Empty;

        switch (mode)
        {
            case "zip":
                CompressFiles();
                return;
        }

        try
        {

            #region GUID alapján externalLink lekérése

            KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            ExecParam ep = new ExecParam();
            ep.Felhasznalo_Id = "54e861a5-36ed-44ca-baa7-c287d125b309";
            ep.LoginUser_Id = "54e861a5-36ed-44ca-baa7-c287d125b309";

            if (!string.IsNullOrEmpty(Request.QueryString.Get("id")))
            {
                ep.Record_Id = Request.QueryString.Get("id");
//                char jogszint = 'O';

                Result dokGetResult = dokService.Get(ep);
                if (dokGetResult.IsError)
                {
                    throw new ResultException(dokGetResult);
                }

                externalLink = (dokGetResult.Record as KRT_Dokumentumok).External_Link;
                filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
                guid = ep.Record_Id;
                currentVersion = (dokGetResult.Record as KRT_Dokumentumok).VerzioJel;
                filesource = (dokGetResult.Record as KRT_Dokumentumok).External_Source;
            }
            else if (!string.IsNullOrEmpty(Request.QueryString.Get("barcode")))
            {
                string barCode = Request.QueryString.Get("barcode");

                KRT_DokumentumokSearch search = new KRT_DokumentumokSearch();
                //search.BarCode.Value = Request.QueryString.Get("barcode");
                //search.BarCode.Operator = Query.Operators.equals;

                search.Manual_BarCodeFilter.Value = barCode;
                search.Manual_BarCodeFilter.Operator = String.Empty; // biztosan legyen üres, hogy ne generálódjon bele a where feltételbe

                Result dokGetResult = dokService.GetAllWithExtensionAndJogosultak(ep, search, true);

                if (dokGetResult.IsError)
                {
                    throw new ResultException(dokGetResult);
                }

                if (dokGetResult.Ds.Tables.Count < 1 || dokGetResult.Ds.Tables[0].Rows.Count < 1)
                {
                    //throw new ResultException(52794); // Önnek nincs joga, hogy megtekintse a dokumentum tartalmát!
                    throw new ResultException(52796); // A dokumentum keresése eredménytelen: Önnek nincs joga, hogy megtekintse a dokumentum tartalmát, vagy a dokumentum nem azonosítható!
                }
                else if (dokGetResult.Ds.Tables[0].Rows.Count > 1)
                {
                    throw new ResultException(52797); // A dokumentum keresése több dokumentumot is eredményezett: A dokumentum azonosítása sikertelen!
                }
                // Mivel a dokumentum egyelőre nincs mindig vonalkódja, és emiatt az iratkezelési elem felől is vizsgálunk,
                // ez az ellenőrzés jelenleg nem használható:
                //else if (dokGetResult.Ds.Tables[0].Rows[0]["BarCode"].ToString() != barCode)
                //{
                //    throw new ResultException(52798); // A dokumentum keresése vonalkód alapján eredménytelen: A talált dokumentum vonalkódja nem egyezik a keresési feltételben megadottal!	
                //}

                externalLink = dokGetResult.Ds.Tables[0].Rows[0]["External_Link"].ToString();
                filename = dokGetResult.Ds.Tables[0].Rows[0]["FajlNev"].ToString();
                guid = dokGetResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                currentVersion = dokGetResult.Ds.Tables[0].Rows[0]["VerzioJel"].ToString();
                filesource = dokGetResult.Ds.Tables[0].Rows[0]["External_Source"].ToString();
            }

            #endregion GUID alapján externalLink lekérése

            if (string.IsNullOrEmpty(Request.QueryString.Get("redirected")))
            {
                Response.Redirect(Request.Url.AbsolutePath + "/" + EncodeFileName(filename) + "?id=" + guid +
                    (String.IsNullOrEmpty(version) ? String.Empty : ("&version=" + version)) +
                    "&redirected=true", false);
            }
            else
            {
                //verziók kezelése
                //ha van megadva verzió és nem az aktuális, akkor le kell kérni a verzió url-jét
                if (!String.IsNullOrEmpty(version) && version != currentVersion)
                {
                    ExecParam xpmVersion = UI.SetExecParamDefault(Page);
                    xpmVersion.Record_Id = guid;
                    Contentum.eDocument.Service.DocumentService serviceVersion = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
                    Result resVersion = serviceVersion.GetDocumentVersionsFromMOSS(xpmVersion);

                    if (resVersion.IsError)
                    {
                        Logger.Error(String.Format("Verzio ({0}) lekerese hiba!", version), xpmVersion, resVersion);
                        throw new ResultException(resVersion);
                    }

                    bool isVersion = false;
                    foreach (DataRow rowVersion in resVersion.Ds.Tables[0].Rows)
                    {
                        if (version == rowVersion["version"].ToString())
                        {
                            externalLink = rowVersion["url"].ToString();
                            isVersion = true;
                            break;
                        }
                    }

                    if (!isVersion)
                    {
                        Logger.Error(String.Format("A verzio ({0}) nem talalhato!", version));
                        throw new ResultException(String.Format("A kért verzió ({0}) nem található!", version));
                    }
                }

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();

                if (filesource.Contains("SharePoint"))
                    GetFileBySharePoint(externalLink, filename);
                else if (filesource.Contains("FileShare"))
                    GetFileByFileShare(externalLink, filename);
                else if (filesource.Contains(HAIRCsatolmanyFeltoltesParameters.Constants.HAIR_FILE_SOURCE))
                    contentDownloader.GetFileByHAIR(Page,externalLink, filename,string.Empty);
                else
                    throw new NotImplementedException(string.Format("Hibás external_source mező:{0}", filesource));
            }
        }
        catch (ResultException resEx)
        {
            Response.ClearHeaders();
            Response.Write(ResultError.GetErrorMessageFromResultObject(resEx.GetResult()));
        }
        catch (Exception exp)
        {
            Response.ClearHeaders();
            Response.Write("A kért dokumentum nem érhető el! Kérem, próbálja meg később."); // A kért dokumentum nem érhető el! Kérem, próbálja meg később.
            Logger.Warn("GetDocumentumContent hiba: " + exp.Message);
        }
        finally
        {
            Response.End();
        }
    }
    private void CompressFiles()
    {
        string externalLink = string.Empty;
        string filename = string.Empty;
        string guid = string.Empty;
        string currentVersion = String.Empty;
        string version = Request.QueryString.Get("version");
        string mode = Request.QueryString.Get("mode") ?? String.Empty;
        string filesource = string.Empty;
        string nev = String.Empty;

        string ids = Request.QueryString.Get("id");
        nev = Request.QueryString.Get("nev");

        if (String.IsNullOrEmpty(ids))
        { throw new ResultException("id cannot be empty"); }

        try
        {
            var idArr = ids.Split('_');
            DateTime current = DateTime.Now;

            if (string.IsNullOrEmpty(nev))
            {
                nev = "Request";
            }
            else
            {
                Regex illegalInFileName = new Regex(@"[\\/:*?""<>|]");
                nev = illegalInFileName.Replace(nev, "_");
            }

            string zipfName = nev + current.Date.Day.ToString() + current.Date.Month.ToString() + current.Date.Year.ToString() + current.TimeOfDay.Duration().Hours.ToString() + current.TimeOfDay.Duration().Minutes.ToString() + current.TimeOfDay.Duration().Seconds.ToString() + ".zip";

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();

            //----------IDIDIDIDDIDIDD

            Response.ContentType = "application / zip";
            //Response.ContentType = "application/x-gzip-compressed";
            Response.CacheControl = "public";
            Response.HeaderEncoding = System.Text.Encoding.UTF8;
            //Response.Charset = "utf-8";
            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + zipfName + "\";");
            Response.AddHeader("filename", filename);
            Response.AddHeader("success", true.ToString());

            byte[] buffer = new byte[4096];

            ZipOutputStream zipOutputStream = new ZipOutputStream(Response.OutputStream);
            zipOutputStream.SetLevel(9);

            foreach (string id in idArr)
            {
                if (id.Trim() != string.Empty)
                {
                    KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                    ExecParam ep = UI.SetExecParamDefault(Page);

                    ep.Record_Id = id;
                    char jogszint = 'O';

                    Result dokGetResult = dokService.GetWithRightCheck(ep, jogszint);
                    if (dokGetResult.IsError)
                    {
                        throw new ResultException(dokGetResult);
                    }

                    externalLink = (dokGetResult.Record as KRT_Dokumentumok).External_Link;
                    filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
                    guid = ep.Record_Id;
                    currentVersion = (dokGetResult.Record as KRT_Dokumentumok).VerzioJel;
                    filesource = (dokGetResult.Record as KRT_Dokumentumok).External_Source;

                    //verziók kezelése
                    //ha van megadva verzió és nem az aktuális, akkor le kell kérni a verzió url-jét
                    if (!String.IsNullOrEmpty(version) && version != currentVersion)
                    {
                        ExecParam xpmVersion = UI.SetExecParamDefault(Page);
                        xpmVersion.Record_Id = guid;
                        Contentum.eDocument.Service.DocumentService serviceVersion = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
                        Result resVersion = serviceVersion.GetDocumentVersionsFromMOSS(xpmVersion);

                        if (resVersion.IsError)
                        {
                            Logger.Error(String.Format("Verzio ({0}) lekerese hiba!", version), xpmVersion, resVersion);
                            throw new ResultException(resVersion);
                        }

                        bool isVersion = false;
                        foreach (DataRow rowVersion in resVersion.Ds.Tables[0].Rows)
                        {
                            if (version == rowVersion["version"].ToString())
                            {
                                externalLink = rowVersion["url"].ToString();
                                isVersion = true;
                                break;
                            }
                        }

                        if (!isVersion)
                        {
                            Logger.Error(String.Format("A verzio ({0}) nem talalhato!", version));
                            throw new ResultException(String.Format("A kért verzió ({0}) nem található!", version));
                        }
                    }

                    System.IO.Stream fileToAdd;

                    if (filesource.Contains("SharePoint"))
                        fileToAdd = GetFileBySharePointForZip(externalLink, filename);
                    else if (filesource.Contains("FileShare"))
                        fileToAdd = GetFileByFileShareForZip(externalLink, filename);
                    else if (filesource.Contains(HAIRCsatolmanyFeltoltesParameters.Constants.HAIR_FILE_SOURCE))
                        fileToAdd = contentDownloader.GetFileByHAIRForZip(Page, externalLink, filename);
                    else
                        throw new NotImplementedException(string.Format("Hibás external_source mező:{0}", filesource));

                    ZipEntry entry = new ZipEntry(ZipEntry.CleanName(filename));
                    entry.DateTime = DateTime.Now;
                    //entry.Size = fileToAdd.Length;

                    zipOutputStream.PutNextEntry(entry);

                    int count = fileToAdd.Read(buffer, 0, buffer.Length);
                    while (count > 0)
                    {
                        zipOutputStream.Write(buffer, 0, count);
                        count = fileToAdd.Read(buffer, 0, buffer.Length);
                        if (!Response.IsClientConnected)
                        {
                            break;
                        }
                        Response.Flush();
                    }

                    zipOutputStream.CloseEntry();

                    fileToAdd.Close();
                }
            }

            zipOutputStream.IsStreamOwner = false;
            zipOutputStream.Close();

            //Response.Flush();
            //Response.End();

            //HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        catch (ResultException resEx)
        {
            Response.ClearHeaders();
            Response.Write(ResultError.GetErrorMessageFromResultObject(resEx.GetResult()));
        }
        catch (Exception exp)
        {
            Response.ClearHeaders();
            Response.Write("A kért dokumentum nem érhető el! Kérem, próbálja meg később."); // A kért dokumentum nem érhető el! Kérem, próbálja meg később.
            Logger.Warn("GetDocumentumContent hiba: " + exp.Message);
        }
        finally
        {
            Response.Flush();
            //Response.Close();
            Response.End();
        }
    }

    /// <summary>
    /// Filetartalom letőltése és továbbítása a kliensnek SharePoint esetén
    /// </summary>
    /// <param name="externalLink">a file ürlje</param>
    /// <param name="filename">a fájl neve és kiterjesztése kliens oldalon</param>
    private void GetFileBySharePoint(string externalLink, string filename)
    {
        #region Header bejegyzések beállítása

        WebRequest request = WebRequest.Create(externalLink);

        request.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
        WebResponse response = request.GetResponse();


        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"] != null && Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        // BUG_12301
        // int csatolmanyLetoltesConfig = Rendszerparameterek.GetInt(Page, Rendszerparameterek.CSATOLMANY_LETOLTES_MOD);

        ExecParam execParam = new ExecParam();
        if (!string.IsNullOrEmpty(Request.QueryString.Get("fid")))
        {
            execParam.Felhasznalo_Id = Request.QueryString.Get("fid");
        }
        if (String.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            execParam = UI.SetExecParamDefault(Page, new ExecParam());
        }
        int csatolmanyLetoltesConfig = Rendszerparameterek.GetInt(execParam, Rendszerparameterek.CSATOLMANY_LETOLTES_MOD);

        string letoltesMod = csatolmanyLetoltesConfig == 1 ? "attachment" : "inline";

        // Content-Type és fájl nevének beállítása
        Response.ContentType = response.ContentType;
        Response.CacheControl = "public";
        Response.HeaderEncoding = System.Text.Encoding.UTF8;
        Response.Charset = "utf-8";
        Response.AddHeader("Content-Disposition", string.Format("{0};filename=\"", letoltesMod) + filename + "\";");
        Response.AddHeader("filename", filename);
        Response.AddHeader("success", true.ToString());


        #endregion Header bejegyzések beállítása

        #region Fájltartalom letöltése és továbbítása a kliensnek

        System.IO.Stream s = response.GetResponseStream();
        byte[] buf = new byte[20480];

        while (true)
        {
            int readBytes = s.Read(buf, 0, buf.Length);

            if (readBytes == 0)
                break;

            Response.OutputStream.Write(buf, 0, readBytes);
        }

        s.Close();
        #endregion Fájltartalom letöltése és továbbítása a kliensnek

    }

    private System.IO.Stream GetFileBySharePointForZip(string externalLink, string filename)
    {
        WebRequest request = WebRequest.Create(externalLink);

        request.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
        WebResponse response = request.GetResponse();

        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"] != null && Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        return response.GetResponseStream();
    }

    /// <summary>
    /// Filetartalom letőltése és továbbítása a kliensnek FileShare esetén
    /// </summary>
    /// <param name="externalLink">a file ürlje</param>
    /// <param name="filename">a fájl neve és kiterjesztése kliens oldalon</param>
    private void GetFileByFileShare(string externalLink, string filename)
    {
        #region Header bejegyzések beállítása        

        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"] != null && Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }
        // Content-Type és fájl nevének beállítása
        Response.ContentType = "application/octet-stream";
        Response.CacheControl = "public";
        Response.HeaderEncoding = System.Text.Encoding.UTF8;
        Response.Charset = "utf-8";
        Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filename + "\";");
        Response.AddHeader("filename", filename);
        Response.AddHeader("success", true.ToString());

        #endregion Header bejegyzések beállítása
        using (var file = File.OpenRead(externalLink))
        {
            byte[] buf = new byte[20480];

            while (true)
            {
                int readBytes = file.Read(buf, 0, buf.Length);

                if (readBytes == 0)
                    break;

                Response.OutputStream.Write(buf, 0, readBytes);
            }
        }
    }

    private System.IO.Stream GetFileByFileShareForZip(string externalLink, string filename)
    {
        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"] != null && Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        return File.OpenRead(externalLink);
    }



    bool CheckJogosultsag()
    {
        try
        {
            //jogosultság ellenőrzés
            KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            ExecParam ep = UI.SetExecParamDefault(Page);

            if (!string.IsNullOrEmpty(Request.QueryString.Get("id")))
            {
                ep.Record_Id = Request.QueryString.Get("id");
                char jogszint = 'O';

                Result dokGetResult = dokService.GetWithRightCheck(ep, jogszint);
                if (dokGetResult.IsError)
                {
                    throw new ResultException(dokGetResult);
                }
            }
        }
        catch (Exception ex)
        {
            string errorMessage = ResultError.GetErrorMessageFromResultObject(ResultException.GetResultFromException(ex));
            Response.Write(errorMessage);
            return false;
        }

        return true;
    }



    private string EncodeFileName(string fileName)
    {
        if (String.IsNullOrEmpty(fileName))
            return String.Empty;

        fileName = HttpUtility.UrlEncode(fileName);
        fileName = fileName.Replace("+", "%20");

        return fileName;
    }
}
