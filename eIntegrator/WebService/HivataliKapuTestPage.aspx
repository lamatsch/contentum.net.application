﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HivataliKapuTestPage.aspx.cs" Inherits="HivataliKapuTestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <fieldset style="margin-top: 10px;">
            <legend>Bejövő üzenet küldése Contentumba</legend>
            <div>
                Érkeztetési szám:
            <asp:TextBox ID="ErkeztetesiSzam" runat="server" />
            </div>
            <div>
                Hivatkozási szám:
            <asp:TextBox ID="HivatkozasiSzam" runat="server" />
            </div>
              <div>
                Partner kapcsolati kód:
            <asp:TextBox ID="PartnerKapcsolatiKod" runat="server" />
            </div>
            <div>
               Kapcsolati kód:
            <asp:TextBox ID="kapcsolatiKod" runat="server" />
            </div>
            <div>
                DokTipusAzonosito:
                <asp:DropDownList ID="DokTipusAzonosito" runat="server">
                    <asp:ListItem Text="" Value="" />
                    <asp:ListItem Text="feladasiigazolas" Value="feladasiigazolas" />
                    <asp:ListItem Text="atveteliertesito" Value="atveteliertesito" />
                    <asp:ListItem Text="letoltesiigazolas" Value="letoltesiigazolas" />
                    <asp:ListItem Text="meghiusulasiigazolas" Value="meghiusulasiigazolas" />
                    <asp:ListItem Text="KEZBIG" Value="KEZBIG" />
                </asp:DropDownList>
            </div>
            <div>
                Rendszerüzenet: <asp:CheckBox ID="cbRendszer" runat="server" />
            </div>
            <div>
                Fájl:
            <asp:FileUpload ID="File" runat="server" />
            </div>
            <div>
                <asp:Button ID="SendButton" runat="server" Text="Küldés" OnClick="SendButton_Click" />
            </div>
            <div>
                <asp:Label ID="Output" runat="server" />
            </div>
        </fieldset>
        <hr />
        <fieldset style="margin-top: 10px;">
            <legend>Kimenő üzenet küldése hivatali kapura</legend>
             <div>
                 Fiók:
                 <asp:DropDownList ID="HKPFiok" runat="server" />
            </div>
            <div>
                DokTipusHivatal:
                <asp:TextBox ID="DokTipusHivatalToHKP" runat="server" />
            </div>
            <div>
                DokTipusAzonosito:
                <asp:TextBox ID="DokTipusAzonositoToHKP" runat="server" />
            </div>
            <div>
                DokTipusLeiras:
                <asp:TextBox ID="DokTipusLeirasToHKP" runat="server"/>
            </div>
            <div>
                Hivatal rövid neve:
                <asp:TextBox ID="HivatalRovidNeve" runat="server" AutoPostBack="true" OnTextChanged="HivatalRovidNeve_TextChanged" />
            </div>
            <div>
                Hivatal KRID-ja:
                <asp:TextBox ID="HivatalKRID" runat="server" />
            </div>
            <div>
                Fájl:
                <asp:FileUpload ID="fileToHKP" runat="server" />
            </div>
            <div>
                <asp:Button ID="btnSendToHKP" runat="server" Text="Küldés hivatali kapura" OnClick="btnSendToHKP_Click"/>
            </div>
            <div>
                <asp:Label ID="lblSendToHKPError" runat="server" />
            </div>
        </fieldset>
         <hr />
        <fieldset style="margin-top: 10px;">
            <legend>Üzenetek letöltése</legend>
            <div>
                <asp:Button ID="btnLetoltes" runat="server" Text="Letöltés" OnClick="btnLetoltes_Click" />
            </div>
            <div>
                <asp:Label ID="lblLetoltesError" runat="server" />
                <asp:GridView ID="gridUzenetek" runat="server" />
            </div>
        </fieldset>
    </form>
</body>
</html>
