﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using eUzenet.HKP_KRService.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HivataliKapuTestPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillFiokokDropDownList();
    }

    protected void SendButton_Click(object sender, EventArgs e)
    {
        Output.Text = String.Empty;

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";

        EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();
        eBeadvany.KR_HivatkozasiSzam = HivatkozasiSzam.Text;
        eBeadvany.KR_ErkeztetesiDatum = DateTime.Now.ToString();
        eBeadvany.KR_ErkeztetesiSzam = String.IsNullOrEmpty(ErkeztetesiSzam.Text) ? Guid.NewGuid().ToString() : ErkeztetesiSzam.Text;
        eBeadvany.KR_DokTipusAzonosito = DokTipusAzonosito.Text;
        eBeadvany.Irany = "0";
        eBeadvany.Cel = "WEB";
        eBeadvany.KR_FileNev = File.FileName;
        eBeadvany.PartnerKapcsolatiKod = kapcsolatiKod.Text;
        //eBeadvany.PartnerKRID = "506341775";
        eBeadvany.KR_Rendszeruzenet = cbRendszer.Checked ? "1" : "0";
        eBeadvany.PartnerKapcsolatiKod = PartnerKapcsolatiKod.Text;

        Csatolmany csatolmany = new Csatolmany();
        csatolmany.Nev = File.FileName;
        csatolmany.Tartalom = File.FileBytes;

        EREC_eBeadvanyokService servie = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
        servie.Timeout = (int)TimeSpan.FromMinutes(30).TotalMilliseconds;
        Result res = servie.InsertAndAttachDocument(execParam, eBeadvany, new [] { csatolmany });

        if (res.IsError)
        {
            Output.Text = String.Format("ErrorCode={0}, ErrorMessage={1}", res.ErrorCode, res.ErrorMessage);
        }
        else
        {
            string eBeadvanyId = res.Uid;
            ProcessManager pm = new ProcessManager(execParam);
            pm.PostProcess_eBeadvany(eBeadvanyId, eBeadvany, csatolmany);
            Output.Text = "OK";
        }
      
    }

    public void CsoportosDokumentumFeltoltes(String KR_Fiok, List<EREC_eBeadvanyok> eBeadvanyok, List<Csatolmany> csatolmanyok, ExecParam execParam)
    {
        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        svc.Timeout = (int)TimeSpan.FromMinutes(30).TotalMilliseconds;
        Result resultFeltoltes = svc.CsoportosDokumentumFeltoltes(KR_Fiok, csatolmanyok.ToArray(), eBeadvanyok.ToArray());

        if (resultFeltoltes.IsError)
        {
            throw new ResultException(resultFeltoltes);
        }
    }

    protected void btnSendToHKP_Click(object sender, EventArgs e)
    {
        lblSendToHKPError.Text = String.Empty;

        try
        {
            if (!fileToHKP.HasFile)
                throw new Exception("Nincs kiválasztva fájl!");

            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";

            Csatolmany csatolmany = new Csatolmany();
            csatolmany.Nev = fileToHKP.FileName;
            csatolmany.Tartalom = fileToHKP.FileBytes;

            EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();
            eBeadvany.Updated.SetValueAll(false);
            eBeadvany.Base.Updated.SetValueAll(false);

            eBeadvany.Irany = "1"; //Kimenő
            eBeadvany.Updated.Irany = true;
            eBeadvany.Allapot = "11"; //Hivatali kapunak elküldött
            eBeadvany.Updated.Allapot = true;

            //felülketről bekérjük ezeket az adatokat
            eBeadvany.KR_DokTipusHivatal = DokTipusHivatalToHKP.Text;
            eBeadvany.Updated.KR_DokTipusHivatal = true;

            eBeadvany.KR_DokTipusAzonosito = DokTipusAzonositoToHKP.Text;
            eBeadvany.Updated.KR_DokTipusAzonosito = true;

            eBeadvany.KR_DokTipusLeiras = DokTipusLeirasToHKP.Text;
            eBeadvany.Updated.KR_DokTipusLeiras = true;

            eBeadvany.KR_FileNev = csatolmany.Nev;
            eBeadvany.Updated.KR_FileNev = true;

            eBeadvany.KR_Valaszutvonal = "0"; // Közmû
            eBeadvany.Updated.KR_Valaszutvonal = true;
            eBeadvany.KR_Valasztitkositas = "0"; //nem
            eBeadvany.Updated.KR_Valasztitkositas = true;
            eBeadvany.KR_Rendszeruzenet = "0"; //nem
            eBeadvany.Updated.KR_Rendszeruzenet = true;

            #region címzett adatok

            //eBeadvany.PartnerKapcsolatiKod = _EREC_eBeadvanyok.PartnerKapcsolatiKod;
            //eBeadvany.Updated.PartnerKapcsolatiKod = true;
            //eBeadvany.PartnerNev = _EREC_eBeadvanyok.PartnerNev;
            //eBeadvany.Updated.PartnerNev = true;
            //eBeadvany.PartnerEmail = _EREC_eBeadvanyok.PartnerEmail;
            //eBeadvany.Updated.PartnerEmail = true;

            eBeadvany.PartnerKRID = HivatalKRID.Text;
            eBeadvany.Updated.PartnerKRID = true;
            eBeadvany.PartnerRovidNev = HivatalRovidNeve.Text;
            eBeadvany.Updated.PartnerRovidNev = true;

            #endregion

            eBeadvany.KR_Fiok = HKPFiok.SelectedValue;
            eBeadvany.Updated.KR_Fiok = true;

            eBeadvany.Cel = "WEB";
            eBeadvany.Updated.Cel = true;

            CsoportosDokumentumFeltoltes(HKPFiok.SelectedValue, new List<EREC_eBeadvanyok>() { eBeadvany }, new List<Csatolmany>() { csatolmany }, execParam);
        }
        catch (Exception ex)
        {
            Result error = ResultException.GetResultFromException(ex);
            lblSendToHKPError.Text = error.ErrorMessage;
        }
    }

    void FillFiokokDropDownList()
    {
        eUzenetService svc = new eUzenetService();
        List<string> fiokok = svc.GetHivataliKapuFiokok();

        HKPFiok.Items.Clear();
        foreach (string fiok in fiokok)
        {
            HKPFiok.Items.Add(new ListItem(fiok, fiok));
        }
    }

    public string GetKRID(string rovidNev)
    {
        HKP2.HivatalListaElem cimzett = null;

        ExecParam execParam = UI.SetExecParamDefault(Page);

        eUzenetService svc = new eUzenetService();
        eUzenet.HKP_KRService.Core.HivatalSzuroParameterek szuroParameterk = new eUzenet.HKP_KRService.Core.HivatalSzuroParameterek();
        szuroParameterk.RovidNev = rovidNev;
        szuroParameterk.Tipus = eUzenet.HKP_KRService.Core.HivatalTipus.HivataliKapu;
        Result result = svc.HivatalokListajaFeldolgozas_Szurt(HKPFiok.SelectedValue, szuroParameterk);

        if (!result.IsError)
        {
            HKP2.HivatalokListajaValasz valasz = result.Record as HKP2.HivatalokListajaValasz;
            foreach (HKP2.HivatalListaElem hivatal in valasz.HivatalokLista)
            {
                if (hivatal.RovidNev.Equals(rovidNev, StringComparison.CurrentCultureIgnoreCase))
                {
                    cimzett = hivatal;
                    break;
                }
            }
        }
        else
        {
            lblSendToHKPError.Text = result.ErrorMessage;
        }

        return cimzett == null ? String.Empty : cimzett.KRID;
    }

    protected void HivatalRovidNeve_TextChanged(object sender, EventArgs e)
    {
        string krId = GetKRID(HivatalRovidNeve.Text);
        HivatalKRID.Text = krId;
    }

    protected void btnLetoltes_Click(object sender, EventArgs e)
    {
        DateTime now = DateTime.Now;

        eUzenet.HKP_KRService.KRService uzenetService = new eUzenet.HKP_KRService.KRService();
        uzenetService.PostaFiokFeldolgozas();

        EREC_eBeadvanyokService eBeadvanyokService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";

        EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
        search.ErvKezd.Value = now.ToString("yyyy-MM-dd HH:mm:ss");
        search.ErvKezd.Operator = Query.Operators.greaterorequal;
        search.ErvVege.Clear();

        Result res = eBeadvanyokService.GetAll(execParam, search);

        if (res.IsError)
        {
            lblLetoltesError.Text = String.Format("ErrorCode={0}, ErrorMessage={1}", res.ErrorCode, res.ErrorMessage);
        }
        else
        {
            lblLetoltesError.Text = String.Format("Üzenetek száma: {0}", res.Ds.Tables[0].Rows.Count);
            gridUzenetek.DataSource = res.Ds.Tables[0];
            gridUzenetek.DataBind();
        }


    }
}
