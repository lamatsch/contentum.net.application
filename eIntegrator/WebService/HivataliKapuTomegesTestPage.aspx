﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HivataliKapuTomegesTestPage.aspx.cs" Inherits="HivataliKapuTomegesTestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
       <fieldset style="margin-top: 10px;">
            <legend>Üzenet küldése</legend>
           <div>
                DokTipusAzonosito:
                <asp:DropDownList ID="DokTipusAzonosito" runat="server">
                    <asp:ListItem Text="" Value="" />
                    <asp:ListItem Text="feladasiigazolas" Value="feladasiigazolas" />
                    <asp:ListItem Text="atveteliertesito" Value="atveteliertesito" />
                    <asp:ListItem Text="letoltesiigazolas" Value="letoltesiigazolas" />
                    <asp:ListItem Text="meghiusulasiigazolas" Value="meghiusulasiigazolas" />
                </asp:DropDownList>
            </div>
            <div>
                Fájl:
                <asp:FileUpload ID="File" runat="server" />
            </div>
            <div>
                Hivatkozási szám:
            <asp:TextBox ID="HivatkozasiSzam" runat="server" />
            </div>
           <div>
               Üzenetek száma:
               <asp:TextBox ID="numberOfItemsInput" runat="server" Text="1" />
           </div>
            <div>
                <asp:Button ID="SendButton" runat="server" Text="Küldés" OnClick="SendButton_Click"/>
            </div>
           <br />
            <div>
                <asp:TextBox ID="Output" runat="server" Rows="20" Width="1000px" TextMode="MultiLine" />
            </div>
        </fieldset>
    </form>
</body>
</html>
