﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using eUzenet.HKP_KRService.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HivataliKapuTomegesTestPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void SendButton_Click(object sender, EventArgs e)
    {
        Output.Text = String.Empty;

        Stopwatch watch = new Stopwatch();
        watch.Start();

        int numberOfItems;
        if (!Int32.TryParse(numberOfItemsInput.Text, out numberOfItems))
            numberOfItems = 1;

        for (int i = 0; i < numberOfItems; i++)
        {
            SendUzenet(i);
        }

        watch.Stop();

        Debug(String.Format("Összesen: {0}", watch.Elapsed));
    }

    void SendUzenet(int i)
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";
        execParam.Org_Id = "450B510A-7CAA-46B0-83E3-18445C0C53A9";

        string fileNev = String.Format("{0}-{1}{2}",Path.GetFileNameWithoutExtension(File.FileName), Guid.NewGuid(), Path.GetExtension(File.FileName));

        EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();
        eBeadvany.KR_ErkeztetesiDatum = DateTime.Now.ToString();
        eBeadvany.KR_ErkeztetesiSzam = Guid.NewGuid().ToString();
        eBeadvany.KR_DokTipusAzonosito = String.Format("{0}. üzenet", i+1);
        eBeadvany.Irany = "0";
        eBeadvany.Cel = "WEB";
        eBeadvany.KR_FileNev = fileNev;
        eBeadvany.KR_DokTipusAzonosito = DokTipusAzonosito.Text;
        eBeadvany.KR_HivatkozasiSzam = HivatkozasiSzam.Text;

        Csatolmany csatolmany = new Csatolmany();
        csatolmany.Nev = fileNev;
        csatolmany.Tartalom = File.FileBytes;

        List<Csatolmany> csatolmanyok = new List<Csatolmany>();
        csatolmanyok.Add(csatolmany);

        Debug(String.Format("{0}. üzenet küldése", i+1));

        EREC_eBeadvanyokService servie = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
        Result res = servie.InsertAndAttachDocument(execParam, eBeadvany, csatolmanyok.ToArray());

        if (res.IsError)
        {
            Debug(String.Format("ErrorCode={0}, ErrorMessage={1}", res.ErrorCode, res.ErrorMessage));
        }
        else
        {
            string eBeadvanyId = res.Uid;
            ProcessManager procManager = new ProcessManager(execParam);
            procManager.PostProcess_eBeadvany(eBeadvanyId, eBeadvany, csatolmany);
            Debug("OK");
        }
    }

    void Debug(string text)
    {
        Output.Text += "\r\n" + String.Format("{0} - {1}", DateTime.Now,  text);
    }
}