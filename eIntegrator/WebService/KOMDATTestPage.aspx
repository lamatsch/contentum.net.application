﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="KOMDATTestPage.aspx.cs" Inherits="KOMDATTestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="btnTest" runat="server" Text="Teszt" OnClick="btnTest_Click" />
        </div>
        <div>
            <asp:Label ID="lblOutput" runat="server" />
        </div>
        <div>
            <asp:GridView ID="gridOutput" runat="server" />
        </div>
    </form>
</body>
</html>
