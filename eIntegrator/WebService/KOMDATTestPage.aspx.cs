﻿using eUzenet.KOMDAT.ServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class KOMDATTestPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnTest_Click(object sender, EventArgs e)
    {
        try
        {
            KapuWs ws = ServiceFactory.GetKapuWs();
            string error;
            KapuWsIgazolasokResponse result = ws.GetIgazolasok("406132725201805251345047996", IdType.Href, true, out error);

            if (!String.IsNullOrEmpty(error))
            {
                lblOutput.Text = error;
            }
            else if (!String.IsNullOrEmpty(result.Error))
            {
                lblOutput.Text = result.Error;
            }
            else
            {
                gridOutput.DataSource = result.Igazolasok;
                gridOutput.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblOutput.Text = ex.Message;
        }
    }
}