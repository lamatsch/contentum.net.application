﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="KRTitokTestPage.aspx.cs" Inherits="KRTitokTestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            Kititkosítandó fájl
        </div>
        <div>
            <asp:FileUpload ID="KRFile" runat="server" Width="500px"/>
        </div>
        <div>
            KR titok batch fájl
        </div>
        <div>
            <asp:Label ID="batchFile" runat="server"/>
        </div>
        <div>
            AbevJava fájl
        </div>
        <div>
            <asp:Label ID="abevJavaBatchFile" runat="server"/>
        </div>
        <div>
            Eredmény
        </div>
        <div>
            <asp:Label ID="output" runat="server" />
        </div>
        <div style="margin-top: 10px;">
            <asp:Button ID="btnKititkositas" runat="server" Text="Feldolgozás" OnClick="btnKititkositas_Click" />
        </div>
    </div>

    </form>
</body>
</html>
