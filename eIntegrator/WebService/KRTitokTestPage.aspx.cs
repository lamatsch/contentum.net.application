﻿using eUzenet.FileManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class KRTitokTestPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        batchFile.Text = KRTitokManager.BatchFilePath;
        abevJavaBatchFile.Text = AbevJavaManager.AbevJavaFilePath;
    }

    protected void btnKititkositas_Click(object sender, EventArgs e)
    {
        output.Text = String.Empty;
        if (!KRFile.HasFile)
        {
            output.Text = "Nincs megadva fájl!";
            return;
        }

        Random r = new Random();
        string erkeztetoszam = r.Next(1, 1000).ToString();
        string error;

        string outDirectory = KRFileManager.ProcessFile(erkeztetoszam, KRFile.FileName, KRFile.FileBytes, out error);

        if (!String.IsNullOrEmpty(error))
        {
            output.Text = error;
        }
        else
        {
            DirectoryInfo di = new DirectoryInfo(outDirectory);

            StringBuilder sb = new StringBuilder();
            sb.Append("<ul>");

            foreach (FileInfo fi in di.GetFiles())
            {
                sb.Append("<li>");
                sb.Append(fi.FullName);
                sb.Append("</li>");

            }

            sb.Append("</ul>");

            output.Text = sb.ToString();
        }
    }
}