﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NAPTestPage.aspx.cs" Inherits="NAPTestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr class="urlapSor">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelRnyNev" runat="server" Text="Viselt név:" />
                    </td>
                    <td class="mrUrlapMezo">
                        <asp:TextBox ID="rnyNev" runat="server" CssClass="mrUrlapInputSearch" Text="DARMOS MÓNIKA"/>
                    </td>
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelRnySzuletesiNev" runat="server" Text="Születési név:"/>
                    </td>
                    <td class="mrUrlapMezo">
                        <asp:TextBox ID="rnySzuletesiNev" runat="server" CssClass="mrUrlapInputSearch" Text="DARMOS MÓNIKA"/>
                    </td>
                </tr>
                <tr class="urlapSor">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelRnyAnyjaNeveVezNev" runat="server" Text="Anyja születési vezeték neve:" />
                    </td>
                    <td class="mrUrlapMezo">
                        <asp:TextBox ID="rnyAnjaNeveVezNev" runat="server" CssClass="mrUrlapInputSearch" Text="SIPOS"/>
                    </td>
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelRnyAnyjaNeveUtoNev" runat="server" Text="Anyja születési utó neve:" />
                    </td>
                    <td class="mrUrlapMezo">
                        <asp:TextBox ID="rnyAnjaNeveUtoNev" runat="server" CssClass="mrUrlapInputSearch" Text="ERZSÉBET"/>
                    </td>
                </tr>
                <tr class="urlapSor">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelRnySzuletesiHely" runat="server" Text="Születési hely:" />
                    </td>
                    <td class="mrUrlapMezo">
                        <asp:TextBox id="rnySzuletesiHely" runat="server" cssclass="mrUrlapInputSearch" Text="KÖRMEND"/>
                    </td>
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelRnySzuletesDatuma" runat="server" Text="Születési idő:" />
                    </td>
                    <td class="mrUrlapMezo">
                        <asp:TextBox id="rnySzuletesDatuma" runat="server" cssclass="mrUrlapInput" Text="1981-10-08"/>
                    </td>
                </tr>
                <tr class="urlapSor">
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelSzereplo" runat="server" Text="Szereplő:" />
                    </td>
                    <td class="mrUrlapMezo">
                        <asp:DropDownList id="ddownSzereplo" runat="server" cssclass="mrUrlapInputComboBox"/>
                    </td>
                    <td class="mrUrlapCaption">
                        <asp:Label ID="labelTipus" runat="server" Text="Meghatalmazás típusa:" />
                    </td>
                    <td class="mrUrlapMezo">
                        <asp:DropDownList id="ddownMeghatalmasTipusa" runat="server" cssclass="mrUrlapInputComboBox"/>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <asp:Button ID="btnTest" runat="server" Text="Teszt" OnClick="btnTest_Click" />
        </div>
        <div>
            <asp:GridView runat="server" ID="resultGrid"></asp:GridView>
            <asp:Label runat="server" ID="errorLabel" />
        </div>
    </form>
</body>
</html>
