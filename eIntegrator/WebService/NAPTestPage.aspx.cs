﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service.NAP;
using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using Contentum.eIntegrator.Service.NAP.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NAPTestPage : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        //NMHH.Elhitet.Servlet.ElhitetServletManager.InitiateSSLTrust();

        if (!IsPostBack)
        {
            ddownSzereplo.Items.Add(new ListItem() { Text = "Kit", Value = ((int)SzereploTipusok.Kit).ToString() });
            ddownSzereplo.Items.Add(new ListItem() { Text = "Kire", Value = ((int)SzereploTipusok.Kire).ToString() });

            foreach (MeghatalmazasTipusok meghatalmazasTipus in MeghatalmazasTipusokHelper.GetOsszesMeghatalmazasTipus())
            {
                ddownMeghatalmasTipusa.Items.Add(new ListItem()
                {
                    Text = RendelkezesTipusNevek.GetNev(meghatalmazasTipus),
                    Value = ((int)meghatalmazasTipus).ToString()
                });
            }
        }
    }

    protected void btnTest_Click(object sender, EventArgs e)
    {
        INT_NAPService service = new INT_NAPService();
        Szemely4TAdatok azonositoAdat = null;
        azonositoAdat = new Szemely4TAdatok();
        azonositoAdat.AnyjaNeve.VezNev = rnyAnjaNeveVezNev.Text;
        azonositoAdat.AnyjaNeve.UtoNev = rnyAnjaNeveUtoNev.Text;
        azonositoAdat.Nev = rnyNev.Text;
        azonositoAdat.SzuletesiHely = rnySzuletesiHely.Text;
        azonositoAdat.SzuletesiIdo = DateTime.Parse(rnySzuletesDatuma.Text);
        azonositoAdat.SzuletesiNev = rnySzuletesiNev.Text;

        MeghatalmazasokRequest request = new MeghatalmazasokRequest();
        request.AzonositoAdat = new AzonositoAdat
        {
            SzemelyAdatok = azonositoAdat
        };

        request.Szereplo = (SzereploTipusok)Enum.Parse(typeof(SzereploTipusok), ddownSzereplo.SelectedValue);
        request.Tipus = (MeghatalmazasTipusok)Enum.Parse(typeof(MeghatalmazasTipusok), ddownMeghatalmasTipusa.SelectedValue);

        errorLabel.Text = "";

        bool isMeghatalmazasResultMode = false;
        if (isMeghatalmazasResultMode)
        {
            MeghatalmazasokResult meghatalmazasokResult = service.GetMeghatalmazasok(new ExecParam(), request);

            if (meghatalmazasokResult.IsError)
            {
                errorLabel.Text = meghatalmazasokResult.ErrorMessage;
            }
            else
            {
                resultGrid.DataSource = meghatalmazasokResult.MeghatalmazasokList;
                resultGrid.DataBind();
            }
        }
        else
        {
            AlapRendelkezesekResult alapRendelkezesekResult = service.GetAlapRendelkezesek(new ExecParam(), request.AzonositoAdat);

            if (alapRendelkezesekResult.IsError)
            {
                errorLabel.Text = alapRendelkezesekResult.ErrorMessage;
            }
            else
            {
                resultGrid.DataSource = alapRendelkezesekResult.ElerhetosegekList;
                resultGrid.DataBind();
            }
        }

    }
}