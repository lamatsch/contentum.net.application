
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_Alszam BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_Alszam
    {
        [System.Xml.Serialization.XmlType("BaseMIG_AlszamBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _UI_SAV = true;
            public bool UI_SAV
            {
                get { return _UI_SAV; }
                set { _UI_SAV = value; }
            }


            private bool _UI_YEAR = true;
            public bool UI_YEAR
            {
                get { return _UI_YEAR; }
                set { _UI_YEAR = value; }
            }


            private bool _UI_NUM = true;
            public bool UI_NUM
            {
                get { return _UI_NUM; }
                set { _UI_NUM = value; }
            }


            private bool _ALNO = true;
            public bool ALNO
            {
                get { return _ALNO; }
                set { _ALNO = value; }
            }


            private bool _ERK = true;
            public bool ERK
            {
                get { return _ERK; }
                set { _ERK = value; }
            }


            private bool _IKTAT = true;
            public bool IKTAT
            {
                get { return _IKTAT; }
                set { _IKTAT = value; }
            }


            private bool _BKNEV = true;
            public bool BKNEV
            {
                get { return _BKNEV; }
                set { _BKNEV = value; }
            }


            private bool _ELINT = true;
            public bool ELINT
            {
                get { return _ELINT; }
                set { _ELINT = value; }
            }

            private bool _ELOAD = true;
            public bool ELOAD
            {
                get { return _ELOAD; }
                set { _ELOAD = value; }
            }


            private bool _ROGZIT = true;
            public bool ROGZIT
            {
                get { return _ROGZIT; }
                set { _ROGZIT = value; }
            }


            private bool _MJ = true;
            public bool MJ
            {
                get { return _MJ; }
                set { _MJ = value; }
            }


            private bool _IDNUM = true;
            public bool IDNUM
            {
                get { return _IDNUM; }
                set { _IDNUM = value; }
            }


            private bool _MIG_Foszam_Id = true;
            public bool MIG_Foszam_Id
            {
                get { return _MIG_Foszam_Id; }
                set { _MIG_Foszam_Id = value; }
            }


            private bool _MIG_Eloado_Id = true;
            public bool MIG_Eloado_Id
            {
                get { return _MIG_Eloado_Id; }
                set { _MIG_Eloado_Id = value; }
            }

            private bool _UGYHOL = true;
            public bool UGYHOL
            {
                get { return _UGYHOL; }
                set { _UGYHOL = value; }
            }


            private bool _IRATTARBA = true;
            public bool IRATTARBA
            {
                get { return _IRATTARBA; }
                set { _IRATTARBA = value; }
            }

            private bool _Adathordozo_tipusa = true;
            public bool Adathordozo_tipusa
            {
                get { return _Adathordozo_tipusa; }
                set { _Adathordozo_tipusa = value; }
            }


            private bool _Irattipus = true;
            public bool Irattipus
            {
                get { return _Irattipus; }
                set { _Irattipus = value; }
            }


            private bool _Feladat = true;
            public bool Feladat
            {
                get { return _Feladat; }
                set { _Feladat = value; }
            }


            public void SetValueAll(bool Value)
            {

                Id = Value;

                UI_SAV = Value;

                UI_YEAR = Value;

                UI_NUM = Value;

                ALNO = Value;

                ERK = Value;

                IKTAT = Value;

                BKNEV = Value;

                ELINT = Value;

                ELOAD = Value;

                ROGZIT = Value;

                MJ = Value;

                IDNUM = Value;

                MIG_Foszam_Id = Value;

                MIG_Eloado_Id = Value;

                UGYHOL = Value;

                IRATTARBA = Value;

                Adathordozo_tipusa = Value;

                Irattipus = Value;

                Feladat = Value;

            }

        }

        [System.Xml.Serialization.XmlType("BaseMIG_AlszamBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlInt32 _UI_SAV = SqlInt32.Null;

            /// <summary>
            /// UI_SAV Base property </summary>
            public SqlInt32 UI_SAV
            {
                get { return _UI_SAV; }
                set { _UI_SAV = value; }
            }


            private SqlInt32 _UI_YEAR = SqlInt32.Null;

            /// <summary>
            /// UI_YEAR Base property </summary>
            public SqlInt32 UI_YEAR
            {
                get { return _UI_YEAR; }
                set { _UI_YEAR = value; }
            }


            private SqlInt32 _UI_NUM = SqlInt32.Null;

            /// <summary>
            /// UI_NUM Base property </summary>
            public SqlInt32 UI_NUM
            {
                get { return _UI_NUM; }
                set { _UI_NUM = value; }
            }


            private SqlInt32 _ALNO = SqlInt32.Null;

            /// <summary>
            /// ALNO Base property </summary>
            public SqlInt32 ALNO
            {
                get { return _ALNO; }
                set { _ALNO = value; }
            }


            private SqlDateTime _ERK = SqlDateTime.Null;

            /// <summary>
            /// ERK Base property </summary>
            public SqlDateTime ERK
            {
                get { return _ERK; }
                set { _ERK = value; }
            }


            private SqlDateTime _IKTAT = SqlDateTime.Null;

            /// <summary>
            /// IKTAT Base property </summary>
            public SqlDateTime IKTAT
            {
                get { return _IKTAT; }
                set { _IKTAT = value; }
            }


            private SqlString _BKNEV = SqlString.Null;

            /// <summary>
            /// BKNEV Base property </summary>
            public SqlString BKNEV
            {
                get { return _BKNEV; }
                set { _BKNEV = value; }
            }


            private SqlDateTime _ELINT = SqlDateTime.Null;

            /// <summary>
            /// ELINT Base property </summary>
            public SqlDateTime ELINT
            {
                get { return _ELINT; }
                set { _ELINT = value; }
            }

            private SqlString _ELOAD = SqlString.Null;

            /// <summary>
            /// ELOAD Base property </summary>
            public SqlString ELOAD
            {
                get { return _ELOAD; }
                set { _ELOAD = value; }
            }


            private SqlString _ROGZIT = SqlString.Null;

            /// <summary>
            /// ROGZIT Base property </summary>
            public SqlString ROGZIT
            {
                get { return _ROGZIT; }
                set { _ROGZIT = value; }
            }


            private SqlString _MJ = SqlString.Null;

            /// <summary>
            /// MJ Base property </summary>
            public SqlString MJ
            {
                get { return _MJ; }
                set { _MJ = value; }
            }


            private SqlString _IDNUM = SqlString.Null;

            /// <summary>
            /// IDNUM Base property </summary>
            public SqlString IDNUM
            {
                get { return _IDNUM; }
                set { _IDNUM = value; }
            }


            private SqlGuid _MIG_Foszam_Id = SqlGuid.Null;

            /// <summary>
            /// MIG_Foszam_Id Base property </summary>
            public SqlGuid MIG_Foszam_Id
            {
                get { return _MIG_Foszam_Id; }
                set { _MIG_Foszam_Id = value; }
            }


            private SqlGuid _MIG_Eloado_Id = SqlGuid.Null;

            /// <summary>
            /// MIG_Eloado_Id Base property </summary>
            public SqlGuid MIG_Eloado_Id
            {
                get { return _MIG_Eloado_Id; }
                set { _MIG_Eloado_Id = value; }
            }

            private SqlString _UGYHOL = SqlString.Null;

            /// <summary>
            /// UGYHOL Base property </summary>
            public SqlString UGYHOL
            {
                get { return _UGYHOL; }
                set { _UGYHOL = value; }
            }

            private SqlDateTime _IRATTARBA = SqlDateTime.Null;

            /// <summary>
            /// IRATTARBA Base property </summary>
            public SqlDateTime IRATTARBA
            {
                get { return _IRATTARBA; }
                set { _IRATTARBA = value; }
            }

            private SqlString _Adathordozo_tipusa = SqlString.Null;

            /// <summary>
            /// Adathordozo_tipusa Base property </summary>
            public SqlString Adathordozo_tipusa
            {
                get { return _Adathordozo_tipusa; }
                set { _Adathordozo_tipusa = value; }
            }


            private SqlString _Irattipus = SqlString.Null;

            /// <summary>
            /// Irattipus Base property </summary>
            public SqlString Irattipus
            {
                get { return _Irattipus; }
                set { _Irattipus = value; }
            }


            private SqlString _Feladat = SqlString.Null;

            /// <summary>
            /// Feladat Base property </summary>
            public SqlString Feladat
            {
                get { return _Feladat; }
                set { _Feladat = value; }
            }

        }
    }
}