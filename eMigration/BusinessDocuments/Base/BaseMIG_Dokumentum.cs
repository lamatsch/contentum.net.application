
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// MIG_Dokumentum BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_Dokumentum
    {
        [System.Xml.Serialization.XmlType("BaseMIG_DokumentumBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _FajlNev = true;
               public bool FajlNev
               {
                   get { return _FajlNev; }
                   set { _FajlNev = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Formatum = true;
               public bool Formatum
               {
                   get { return _Formatum; }
                   set { _Formatum = value; }
               }
                                                            
                                 
               private bool _Leiras = true;
               public bool Leiras
               {
                   get { return _Leiras; }
                   set { _Leiras = value; }
               }
                                                            
                                 
               private bool _External_Source = true;
               public bool External_Source
               {
                   get { return _External_Source; }
                   set { _External_Source = value; }
               }
                                                            
                                 
               private bool _External_Link = true;
               public bool External_Link
               {
                   get { return _External_Link; }
                   set { _External_Link = value; }
               }
                                                            
                                 
               private bool _BarCode = true;
               public bool BarCode
               {
                   get { return _BarCode; }
                   set { _BarCode = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _ElektronikusAlairas = true;
               public bool ElektronikusAlairas
               {
                   get { return _ElektronikusAlairas; }
                   set { _ElektronikusAlairas = value; }
               }
                                                            
                                 
               private bool _AlairasFelulvizsgalat = true;
               public bool AlairasFelulvizsgalat
               {
                   get { return _AlairasFelulvizsgalat; }
                   set { _AlairasFelulvizsgalat = value; }
               }
                                                            
                                 
               private bool _MIG_Alszam_Id = true;
               public bool MIG_Alszam_Id
               {
                   get { return _MIG_Alszam_Id; }
                   set { _MIG_Alszam_Id = value; }
               }
                                                            
                                 
               private bool _Ver = true;
               public bool Ver
               {
                   get { return _Ver; }
                   set { _Ver = value; }
               }
                                                            
                                 
               private bool _ErvVege = true;
               public bool ErvVege
               {
                   get { return _ErvVege; }
                   set { _ErvVege = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   FajlNev = Value;               
                    
                   Tipus = Value;               
                    
                   Formatum = Value;               
                    
                   Leiras = Value;               
                    
                   External_Source = Value;               
                    
                   External_Link = Value;               
                    
                   BarCode = Value;               
                    
                   Allapot = Value;               
                    
                   ElektronikusAlairas = Value;               
                    
                   AlairasFelulvizsgalat = Value;               
                    
                   MIG_Alszam_Id = Value;               
                    
                   Ver = Value;               
                    
                   ErvVege = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseMIG_DokumentumBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _FajlNev = SqlString.Null;
           
        /// <summary>
        /// FajlNev Base property </summary>
            public SqlString FajlNev
            {
                get { return _FajlNev; }
                set { _FajlNev = value; }                                                        
            }        
                   
           
        private SqlString _Tipus = SqlString.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlString Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlString _Formatum = SqlString.Null;
           
        /// <summary>
        /// Formatum Base property </summary>
            public SqlString Formatum
            {
                get { return _Formatum; }
                set { _Formatum = value; }                                                        
            }        
                   
           
        private SqlString _Leiras = SqlString.Null;
           
        /// <summary>
        /// Leiras Base property </summary>
            public SqlString Leiras
            {
                get { return _Leiras; }
                set { _Leiras = value; }                                                        
            }        
                   
           
        private SqlString _External_Source = SqlString.Null;
           
        /// <summary>
        /// External_Source Base property </summary>
            public SqlString External_Source
            {
                get { return _External_Source; }
                set { _External_Source = value; }                                                        
            }        
                   
           
        private SqlString _External_Link = SqlString.Null;
           
        /// <summary>
        /// External_Link Base property </summary>
            public SqlString External_Link
            {
                get { return _External_Link; }
                set { _External_Link = value; }                                                        
            }        
                   
           
        private SqlString _BarCode = SqlString.Null;
           
        /// <summary>
        /// BarCode Base property </summary>
            public SqlString BarCode
            {
                get { return _BarCode; }
                set { _BarCode = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlString _ElektronikusAlairas = SqlString.Null;
           
        /// <summary>
        /// ElektronikusAlairas Base property </summary>
            public SqlString ElektronikusAlairas
            {
                get { return _ElektronikusAlairas; }
                set { _ElektronikusAlairas = value; }                                                        
            }        
                   
           
        private SqlDateTime _AlairasFelulvizsgalat = SqlDateTime.Null;
           
        /// <summary>
        /// AlairasFelulvizsgalat Base property </summary>
            public SqlDateTime AlairasFelulvizsgalat
            {
                get { return _AlairasFelulvizsgalat; }
                set { _AlairasFelulvizsgalat = value; }                                                        
            }        
                   
           
        private SqlGuid _MIG_Alszam_Id = SqlGuid.Null;
           
        /// <summary>
        /// MIG_Alszam_Id Base property </summary>
            public SqlGuid MIG_Alszam_Id
            {
                get { return _MIG_Alszam_Id; }
                set { _MIG_Alszam_Id = value; }                                                        
            }        
                   
           
        private SqlInt32 _Ver = SqlInt32.Null;
           
        /// <summary>
        /// Ver Base property </summary>
            public SqlInt32 Ver
            {
                get { return _Ver; }
                set { _Ver = value; }                                                        
            }        
                   
           
        private SqlDateTime _ErvVege = SqlDateTime.Null;
           
        /// <summary>
        /// ErvVege Base property </summary>
            public SqlDateTime ErvVege
            {
                get { return _ErvVege; }
                set { _ErvVege = value; }                                                        
            }        
                           }
    }    
}