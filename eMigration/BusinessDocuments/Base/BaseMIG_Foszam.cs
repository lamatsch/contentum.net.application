
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_Foszam BusinessDocument Class </summary>
    [Serializable()]
    public class  BaseMIG_Foszam
    {
        [System.Xml.Serialization.XmlType("BaseMIG_FoszamBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _UI_SAV = true;
            public bool UI_SAV
            {
                get { return _UI_SAV; }
                set { _UI_SAV = value; }
            }


            private bool _UI_YEAR = true;
            public bool UI_YEAR
            {
                get { return _UI_YEAR; }
                set { _UI_YEAR = value; }
            }


            private bool _UI_NUM = true;
            public bool UI_NUM
            {
                get { return _UI_NUM; }
                set { _UI_NUM = value; }
            }


            private bool _UI_NAME = true;
            public bool UI_NAME
            {
                get { return _UI_NAME; }
                set { _UI_NAME = value; }
            }


            private bool _UI_IRSZ = true;
            public bool UI_IRSZ
            {
                get { return _UI_IRSZ; }
                set { _UI_IRSZ = value; }
            }


            private bool _UI_UTCA = true;
            public bool UI_UTCA
            {
                get { return _UI_UTCA; }
                set { _UI_UTCA = value; }
            }


            private bool _UI_HSZ = true;
            public bool UI_HSZ
            {
                get { return _UI_HSZ; }
                set { _UI_HSZ = value; }
            }


            private bool _UI_HRSZ = true;
            public bool UI_HRSZ
            {
                get { return _UI_HRSZ; }
                set { _UI_HRSZ = value; }
            }


            private bool _UI_TYPE = true;
            public bool UI_TYPE
            {
                get { return _UI_TYPE; }
                set { _UI_TYPE = value; }
            }


            private bool _UI_IRJ = true;
            public bool UI_IRJ
            {
                get { return _UI_IRJ; }
                set { _UI_IRJ = value; }
            }


            private bool _UI_PERS = true;
            public bool UI_PERS
            {
                get { return _UI_PERS; }
                set { _UI_PERS = value; }
            }


            private bool _UI_OT_ID = true;
            public bool UI_OT_ID
            {
                get { return _UI_OT_ID; }
                set { _UI_OT_ID = value; }
            }

            private bool _Ugyirat_tipus = true;
            public bool Ugyirat_tipus
            {
                get { return _Ugyirat_tipus; }
                set { _Ugyirat_tipus = value; }
            }

            private bool _MEMO = true;
            public bool MEMO
            {
                get { return _MEMO; }
                set { _MEMO = value; }
            }


            private bool _IRSZ_PLUSS = true;
            public bool IRSZ_PLUSS
            {
                get { return _IRSZ_PLUSS; }
                set { _IRSZ_PLUSS = value; }
            }


            private bool _IRJ2000 = true;
            public bool IRJ2000
            {
                get { return _IRJ2000; }
                set { _IRJ2000 = value; }
            }


            private bool _Conc = true;
            public bool Conc
            {
                get { return _Conc; }
                set { _Conc = value; }
            }


            private bool _Selejtezve = true;
            public bool Selejtezve
            {
                get { return _Selejtezve; }
                set { _Selejtezve = value; }
            }


            private bool _Selejtezes_Datuma = true;
            public bool Selejtezes_Datuma
            {
                get { return _Selejtezes_Datuma; }
                set { _Selejtezes_Datuma = value; }
            }


            private bool _Csatolva_Rendszer = true;
            public bool Csatolva_Rendszer
            {
                get { return _Csatolva_Rendszer; }
                set { _Csatolva_Rendszer = value; }
            }


            private bool _Csatolva_Id = true;
            public bool Csatolva_Id
            {
                get { return _Csatolva_Id; }
                set { _Csatolva_Id = value; }
            }


            private bool _MIG_Sav_Id = true;
            public bool MIG_Sav_Id
            {
                get { return _MIG_Sav_Id; }
                set { _MIG_Sav_Id = value; }
            }


            private bool _MIG_Varos_Id = true;
            public bool MIG_Varos_Id
            {
                get { return _MIG_Varos_Id; }
                set { _MIG_Varos_Id = value; }
            }


            private bool _MIG_IktatasTipus_Id = true;
            public bool MIG_IktatasTipus_Id
            {
                get { return _MIG_IktatasTipus_Id; }
                set { _MIG_IktatasTipus_Id = value; }
            }


            private bool _MIG_Eloado_Id = true;
            public bool MIG_Eloado_Id
            {
                get { return _MIG_Eloado_Id; }
                set { _MIG_Eloado_Id = value; }
            }

            private bool _Edok_Ugyintezo_Csoport_Id = true;
            public bool Edok_Ugyintezo_Csoport_Id
            {
                get { return _Edok_Ugyintezo_Csoport_Id; }
                set { _Edok_Ugyintezo_Csoport_Id = value; }
            }

            private bool _Edok_Ugyintezo_Nev = true;
            public bool Edok_Ugyintezo_Nev
            {
                get { return _Edok_Ugyintezo_Nev; }
                set { _Edok_Ugyintezo_Nev = value; }
            }

            private bool _Edok_Utoirat_Id = true;
            public bool Edok_Utoirat_Id
            {
                get { return _Edok_Utoirat_Id; }
                set { _Edok_Utoirat_Id = value; }
            }

            private bool _Edok_Utoirat_Azon = true;
            public bool Edok_Utoirat_Azon
            {
                get { return _Edok_Utoirat_Azon; }
                set { _Edok_Utoirat_Azon = value; }
            }

            private bool _UGYHOL = true;
            public bool UGYHOL
            {
                get { return _UGYHOL; }
                set { _UGYHOL = value; }
            }

            private bool _IRATTARBA = true;
            public bool IRATTARBA
            {
                get { return _IRATTARBA; }
                set { _IRATTARBA = value; }
            }

            private bool _Hatarido = true;
            public bool Hatarido
            {
                get { return _Hatarido; }
                set { _Hatarido = value; }
            }


            private bool _SCONTRO = true;
            public bool SCONTRO
            {
                get { return _SCONTRO; }
                set { _SCONTRO = value; }
            }

            private bool _EdokSav = true;
            public bool EdokSav
            {
                get { return _EdokSav; }
                set { _EdokSav = value; }
            }

            private bool _Feladat = true;
            public bool Feladat
            {
                get { return _Feladat; }
                set { _Feladat = value; }
            }

            private bool _Szervezet = true;
            public bool Szervezet
            {
                get { return _Szervezet; }
                set { _Szervezet = value; }
            }

            private bool _IrattarbolKikeroNev = true;
            public bool IrattarbolKikeroNev
            {
                get { return _IrattarbolKikeroNev; }
                set { _IrattarbolKikeroNev = value; }
            }

            private bool _IrattarbolKikeres_Datuma = true;
            public bool IrattarbolKikeres_Datuma
            {
                get { return _IrattarbolKikeres_Datuma; }
                set { _IrattarbolKikeres_Datuma = value; }
            }

            private bool _MegorzesiIdo = true;
            public bool MegorzesiIdo
            {
                get { return _MegorzesiIdo; }
                set { _MegorzesiIdo = value; }
            }

                                 
               private bool _IrattarId = true;
               public bool IrattarId
               {
                   get { return _IrattarId; }
                   set { _IrattarId = value; }
               }
                                                                           
                                 
               private bool _IrattariHely = true;
               public bool IrattariHely
               {
                   get { return _IrattariHely; }
                   set { _IrattariHely = value; }
               }
                                                                           
            public void SetValueAll(bool Value)
            {

                Id = Value;

                UI_SAV = Value;

                UI_YEAR = Value;

                UI_NUM = Value;

                UI_NAME = Value;

                UI_IRSZ = Value;

                UI_UTCA = Value;

                UI_HSZ = Value;

                UI_HRSZ = Value;

                UI_TYPE = Value;

                UI_IRJ = Value;

                UI_PERS = Value;

                UI_OT_ID = Value;

                MEMO = Value;

                IRSZ_PLUSS = Value;

                IRJ2000 = Value;

                Conc = Value;

                Selejtezve = Value;

                Selejtezes_Datuma = Value;

                Csatolva_Rendszer = Value;

                Csatolva_Id = Value;

                MIG_Sav_Id = Value;

                MIG_Varos_Id = Value;

                MIG_IktatasTipus_Id = Value;

                MIG_Eloado_Id = Value;

                Edok_Ugyintezo_Csoport_Id = Value;

                Edok_Ugyintezo_Nev = Value;

                Edok_Utoirat_Id = Value;

                Edok_Utoirat_Azon = Value;

                UGYHOL = Value;

                Hatarido = Value;

                IRATTARBA = Value;

                SCONTRO = Value;

                EdokSav = Value;

                Feladat = Value;

                Szervezet = Value;

                IrattarbolKikeroNev = Value;

                Ugyirat_tipus = Value;

                IrattarbolKikeres_Datuma = Value;

                MegorzesiIdo = Value;
                    
                   IrattarId = Value;               
                    
                   IrattariHely = Value;                              }

        }

        [System.Xml.Serialization.XmlType("BaseMIG_FoszamBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlString _UI_SAV = SqlString.Null;

            /// <summary>
            /// UI_SAV Base property </summary>
            public SqlString UI_SAV
            {
                get { return _UI_SAV; }
                set { _UI_SAV = value; }
            }

            private SqlInt32 _OLD_UI_SAV = SqlInt32.Null;

            /// <summary>
            /// UI_YEAR Base property </summary>
            public SqlInt32 OLD_UI_SAV
            {
                get { return _OLD_UI_SAV; }
                set { _OLD_UI_SAV = value; }
            }

            private SqlInt32 _UI_YEAR = SqlInt32.Null;

            /// <summary>
            /// UI_YEAR Base property </summary>
            public SqlInt32 UI_YEAR
            {
                get { return _UI_YEAR; }
                set { _UI_YEAR = value; }
            }


            private SqlInt32 _UI_NUM = SqlInt32.Null;

            /// <summary>
            /// UI_NUM Base property </summary>
            public SqlInt32 UI_NUM
            {
                get { return _UI_NUM; }
                set { _UI_NUM = value; }
            }


            private SqlString _UI_NAME = SqlString.Null;

            /// <summary>
            /// UI_NAME Base property </summary>
            public SqlString UI_NAME
            {
                get { return _UI_NAME; }
                set { _UI_NAME = value; }
            }


            private SqlString _UI_IRSZ = SqlString.Null;

            /// <summary>
            /// UI_IRSZ Base property </summary>
            public SqlString UI_IRSZ
            {
                get { return _UI_IRSZ; }
                set { _UI_IRSZ = value; }
            }


            private SqlString _UI_UTCA = SqlString.Null;

            /// <summary>
            /// UI_UTCA Base property </summary>
            public SqlString UI_UTCA
            {
                get { return _UI_UTCA; }
                set { _UI_UTCA = value; }
            }


            private SqlString _UI_HSZ = SqlString.Null;

            /// <summary>
            /// UI_HSZ Base property </summary>
            public SqlString UI_HSZ
            {
                get { return _UI_HSZ; }
                set { _UI_HSZ = value; }
            }


            private SqlString _UI_HRSZ = SqlString.Null;

            /// <summary>
            /// UI_HRSZ Base property </summary>
            public SqlString UI_HRSZ
            {
                get { return _UI_HRSZ; }
                set { _UI_HRSZ = value; }
            }


            private SqlString _UI_TYPE = SqlString.Null;

            /// <summary>
            /// UI_TYPE Base property </summary>
            public SqlString UI_TYPE
            {
                get { return _UI_TYPE; }
                set { _UI_TYPE = value; }
            }


            private SqlString _UI_IRJ = SqlString.Null;

            /// <summary>
            /// UI_IRJ Base property </summary>
            public SqlString UI_IRJ
            {
                get { return _UI_IRJ; }
                set { _UI_IRJ = value; }
            }


            private SqlString _UI_PERS = SqlString.Null;

            /// <summary>
            /// UI_PERS Base property </summary>
            public SqlString UI_PERS
            {
                get { return _UI_PERS; }
                set { _UI_PERS = value; }
            }


            private SqlString _UI_OT_ID = SqlString.Null;

            /// <summary>
            /// UI_OT_ID Base property </summary>
            public SqlString UI_OT_ID
            {
                get { return _UI_OT_ID; }
                set { _UI_OT_ID = value; }
            }


            private SqlString _MEMO = SqlString.Null;

            /// <summary>
            /// MEMO Base property </summary>
            public SqlString MEMO
            {
                get { return _MEMO; }
                set { _MEMO = value; }
            }


            private SqlInt32 _IRSZ_PLUSS = SqlInt32.Null;

            /// <summary>
            /// IRSZ_PLUSS Base property </summary>
            public SqlInt32 IRSZ_PLUSS
            {
                get { return _IRSZ_PLUSS; }
                set { _IRSZ_PLUSS = value; }
            }


            private SqlString _IRJ2000 = SqlString.Null;

            /// <summary>
            /// IRJ2000 Base property </summary>
            public SqlString IRJ2000
            {
                get { return _IRJ2000; }
                set { _IRJ2000 = value; }
            }


            private SqlString _Conc = SqlString.Null;

            /// <summary>
            /// Conc Base property </summary>
            public SqlString Conc
            {
                get { return _Conc; }
                set { _Conc = value; }
            }


            private SqlInt32 _Selejtezve = SqlInt32.Null;

            /// <summary>
            /// Selejtezve Base property </summary>
            public SqlInt32 Selejtezve
            {
                get { return _Selejtezve; }
                set { _Selejtezve = value; }
            }


            private SqlDateTime _Selejtezes_Datuma = SqlDateTime.Null;

            /// <summary>
            /// Selejtezes_Datuma Base property </summary>
            public SqlDateTime Selejtezes_Datuma
            {
                get { return _Selejtezes_Datuma; }
                set { _Selejtezes_Datuma = value; }
            }


            private SqlInt32 _Csatolva_Rendszer = SqlInt32.Null;

            /// <summary>
            /// Csatolva_Rendszer Base property </summary>
            public SqlInt32 Csatolva_Rendszer
            {
                get { return _Csatolva_Rendszer; }
                set { _Csatolva_Rendszer = value; }
            }


            private SqlGuid _Csatolva_Id = SqlGuid.Null;

            /// <summary>
            /// Csatolva_Id Base property </summary>
            public SqlGuid Csatolva_Id
            {
                get { return _Csatolva_Id; }
                set { _Csatolva_Id = value; }
            }


            private SqlGuid _MIG_Sav_Id = SqlGuid.Null;

            /// <summary>
            /// MIG_Sav_Id Base property </summary>
            public SqlGuid MIG_Sav_Id
            {
                get { return _MIG_Sav_Id; }
                set { _MIG_Sav_Id = value; }
            }


            private SqlGuid _MIG_Varos_Id = SqlGuid.Null;

            /// <summary>
            /// MIG_Varos_Id Base property </summary>
            public SqlGuid MIG_Varos_Id
            {
                get { return _MIG_Varos_Id; }
                set { _MIG_Varos_Id = value; }
            }


            private SqlGuid _MIG_IktatasTipus_Id = SqlGuid.Null;

            /// <summary>
            /// MIG_IktatasTipus_Id Base property </summary>
            public SqlGuid MIG_IktatasTipus_Id
            {
                get { return _MIG_IktatasTipus_Id; }
                set { _MIG_IktatasTipus_Id = value; }
            }


            private SqlGuid _MIG_Eloado_Id = SqlGuid.Null;

            /// <summary>
            /// MIG_Eloado_Id Base property </summary>
            public SqlGuid MIG_Eloado_Id
            {
                get { return _MIG_Eloado_Id; }
                set { _MIG_Eloado_Id = value; }
            }

            private SqlGuid _Edok_Ugyintezo_Csoport_Id = SqlGuid.Null;

            /// <summary>
            /// Edok_Ugyintezo_Csoport_Id Base property </summary>
            public SqlGuid Edok_Ugyintezo_Csoport_Id
            {
                get { return _Edok_Ugyintezo_Csoport_Id; }
                set { _Edok_Ugyintezo_Csoport_Id = value; }
            }

            private SqlString _Edok_Ugyintezo_Nev = SqlString.Null;

            /// <summary>
            /// Edok_Ugyintezo_Nev Base property </summary>
            public SqlString Edok_Ugyintezo_Nev
            {
                get { return _Edok_Ugyintezo_Nev; }
                set { _Edok_Ugyintezo_Nev = value; }
            }

            private SqlGuid _Edok_Utoirat_Id = SqlGuid.Null;

            /// <summary>
            /// Edok_Utoirat_Id Base property </summary>
            public SqlGuid Edok_Utoirat_Id
            {
                get { return _Edok_Utoirat_Id; }
                set { _Edok_Utoirat_Id = value; }
            }

            private SqlString _Edok_Utoirat_Azon = SqlString.Null;

            /// <summary>
            /// Edok_Utoirat_Azon Base property </summary>
            public SqlString Edok_Utoirat_Azon
            {
                get { return _Edok_Utoirat_Azon; }
                set { _Edok_Utoirat_Azon = value; }
            }

            private SqlString _UGYHOL = SqlString.Null;

            /// <summary>
            /// UGYHOL Base property </summary>
            public SqlString UGYHOL
            {
                get { return _UGYHOL; }
                set { _UGYHOL = value; }
            }

            private SqlDateTime _IRATTARBA = SqlDateTime.Null;

            /// <summary>
            /// IRATTARBA Base property </summary>
            public SqlDateTime IRATTARBA
            {
                get { return _IRATTARBA; }
                set { _IRATTARBA = value; }
            }

            private SqlDateTime _Ugy_kezdete = SqlDateTime.Null;

            /// <summary>
            /// IRATTARBA Base property </summary>
            public SqlDateTime Ugy_kezdete
            {
                get { return _Ugy_kezdete; }
                set { _Ugy_kezdete = value; }
            }

            private SqlDateTime _Hatarido = SqlDateTime.Null;
            /// <summary>
            /// Hatarido Base property </summary>            
            public SqlDateTime Hatarido 
            {
                get { return _Hatarido; }
                set { _Hatarido = value; }
            }

            private SqlDateTime _SCONTRO = SqlDateTime.Null;

            /// <summary>
            /// SCONTRO Base property </summary>
            public SqlDateTime SCONTRO
            {
                get { return _SCONTRO; }
                set { _SCONTRO = value; }
            }

            private SqlString _EdokSav = SqlString.Null;

            /// <summary>
            /// EdokSav Base property </summary>
            public SqlString EdokSav
            {
                get { return _EdokSav; }
                set { _EdokSav = value; }
            }

            private SqlString _Feladat = SqlString.Null;

            /// <summary>
            /// Feladat Base property </summary>
            public SqlString Feladat
            {
                get { return _Feladat; }
                set { _Feladat = value; }
            }

            private SqlString _Szervezet = SqlString.Null;

            /// <summary>
            /// Szervezet Base property </summary>
            public SqlString Szervezet
            {
                get { return _Szervezet; }
                set { _Szervezet = value; }
            }

            private SqlString _IrattarbolKikeroNev = SqlString.Null;

            /// <summary>
            /// IrattarbolKikeroNev Base property </summary>
            public SqlString IrattarbolKikeroNev
            {
                get { return _IrattarbolKikeroNev; }
                set { _IrattarbolKikeroNev = value; }
            }
            private SqlString _Ugyirat_tipus = SqlString.Null;
            /// <summary>
            /// Ugyirat_tipus Base property </summary>
            public SqlString Ugyirat_tipus
            {
                get { return _Ugyirat_tipus; }
                set { _Ugyirat_tipus = value; }
            }

            private SqlDateTime _IrattarbolKikeres_Datuma = SqlDateTime.Null;

            /// <summary>
            /// IrattarbolKikeres_Datuma Base property </summary>
            public SqlDateTime IrattarbolKikeres_Datuma
            {
                get { return _IrattarbolKikeres_Datuma; }
                set { _IrattarbolKikeres_Datuma = value; }
            }

            private SqlDateTime _MegorzesiIdo = SqlDateTime.Null;

            /// <summary>
            /// MegorzesiIdo Base property </summary>
            public SqlDateTime MegorzesiIdo
            {
                get { return _MegorzesiIdo; }
                set { _MegorzesiIdo = value; }
            }
                   
           
        private SqlGuid _IrattarId = SqlGuid.Null;
           
        /// <summary>
        /// IrattarId Base property </summary>
            public SqlGuid IrattarId
            {
                get { return _IrattarId; }
                set { _IrattarId = value; }                                                        
            }        
                   
           
        private SqlString _IrattariHely = SqlString.Null;
           
        /// <summary>
        /// IrattariHely Base property </summary>
            public SqlString IrattariHely
            {
                get { return _IrattariHely; }
                set { _IrattariHely = value; }                                                        
            }        
                           }
    }    
}