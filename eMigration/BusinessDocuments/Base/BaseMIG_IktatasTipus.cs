
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_IktatasTipus BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_IktatasTipus
    {
        [System.Xml.Serialization.XmlType("BaseMIG_IktatasTipusBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _EV = true;
            public bool EV
            {
                get { return _EV; }
                set { _EV = value; }
            }


            private bool _KOD = true;
            public bool KOD
            {
                get { return _KOD; }
                set { _KOD = value; }
            }


            private bool _NAME = true;
            public bool NAME
            {
                get { return _NAME; }
                set { _NAME = value; }
            }


            private bool _IRJEL = true;
            public bool IRJEL
            {
                get { return _IRJEL; }
                set { _IRJEL = value; }
            }


            public void SetValueAll(bool Value)
            {

                Id = Value;

                EV = Value;

                KOD = Value;

                NAME = Value;

                IRJEL = Value;

            }

        }

        [System.Xml.Serialization.XmlType("BaseMIG_IktatasTipusBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlInt32 _EV = SqlInt32.Null;

            /// <summary>
            /// EV Base property </summary>
            public SqlInt32 EV
            {
                get { return _EV; }
                set { _EV = value; }
            }


            private SqlString _KOD = SqlString.Null;

            /// <summary>
            /// KOD Base property </summary>
            public SqlString KOD
            {
                get { return _KOD; }
                set { _KOD = value; }
            }


            private SqlString _NAME = SqlString.Null;

            /// <summary>
            /// NAME Base property </summary>
            public SqlString NAME
            {
                get { return _NAME; }
                set { _NAME = value; }
            }


            private SqlString _IRJEL = SqlString.Null;

            /// <summary>
            /// IRJEL Base property </summary>
            public SqlString IRJEL
            {
                get { return _IRJEL; }
                set { _IRJEL = value; }
            }

        }
    }
}