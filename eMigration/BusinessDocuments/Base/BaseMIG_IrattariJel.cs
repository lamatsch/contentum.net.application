﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments.Base
{
    /// <summary>
    /// MIG_IrattariJel BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_IrattariJel
    {
        [System.Xml.Serialization.XmlType("BaseMIG_IrattariJelBaseUpdated")]
        public class BaseUpdated
        {

            private bool _EV = true;
            public bool EV
            {
                get { return _EV; }
                set { _EV = value; }
            }


            private bool _NAME = true;
            public bool NAME
            {
                get { return _NAME; }
                set { _NAME = value; }
            }


            private bool _KOD = true;
            public bool KOD
            {
                get { return _KOD; }
                set { _KOD = value; }
            }


            private bool _IRJEL = true;
            public bool IRJEL
            {
                get { return _IRJEL; }
                set { _IRJEL = value; }
            }


            private bool _UI = true;
            public bool UI
            {
                get { return _UI; }
                set { _UI = value; }
            }


            private bool _HATOS = true;
            public bool HATOS
            {
                get { return _HATOS; }
                set { _HATOS = value; }
            }


            private bool _KIADM = true;
            public bool KIADM
            {
                get { return _KIADM; }
                set { _KIADM = value; }
            }


            private bool _IND = true;
            public bool IND
            {
                get { return _IND; }
                set { _IND = value; }
            }


            private bool _ERR_KOD = true;
            public bool ERR_KOD
            {
                get { return _ERR_KOD; }
                set { _ERR_KOD = value; }
            }

            public void SetValueAll(bool Value)
            {

                EV = Value;

                NAME = Value;

                KOD = Value;

                IRJEL = Value;

                UI = Value;

                HATOS = Value;

                KIADM = Value;

                IND = Value;

                ERR_KOD = Value;
            }

        }

        [System.Xml.Serialization.XmlType("BaseMIG_IrattariJelBaseTyped")]
        public class BaseTyped
        {

            private SqlString _EV = SqlString.Null;

            /// <summary>
            /// EV Base property </summary>
            public SqlString EV
            {
                get { return _EV; }
                set { _EV = value; }
            }


            private SqlString _NAME = SqlString.Null;

            /// <summary>
            /// NAME Base property </summary>
            public SqlString NAME
            {
                get { return _NAME; }
                set { _NAME = value; }
            }


            private SqlString _KOD = SqlString.Null;

            /// <summary>
            /// KOD Base property </summary>
            public SqlString KOD
            {
                get { return _KOD; }
                set { _KOD = value; }
            }


            private SqlString _IRJEL = SqlString.Null;

            /// <summary>
            /// IRJEL Base property </summary>
            public SqlString IRJEL
            {
                get { return _IRJEL; }
                set { _IRJEL = value; }
            }


            private SqlString _UI = SqlString.Null;

            /// <summary>
            /// UI Base property </summary>
            public SqlString UI
            {
                get { return _UI; }
                set { _UI = value; }
            }


            private SqlString _HATOS = SqlString.Null;

            /// <summary>
            /// HATOS Base property </summary>
            public SqlString HATOS
            {
                get { return _HATOS; }
                set { _HATOS = value; }
            }


            private SqlString _KIADM = SqlString.Null;

            /// <summary>
            /// KIADM Base property </summary>
            public SqlString KIADM
            {
                get { return _KIADM; }
                set { _KIADM = value; }
            }


            private SqlInt32 _IND = SqlInt32.Null;

            /// <summary>
            /// IND Base property </summary>
            public SqlInt32 IND
            {
                get { return _IND; }
                set { _IND = value; }
            }


            private SqlInt32 _ERR_KOD = SqlInt32.Null;

            /// <summary>
            /// ERR_KOD Base property </summary>
            public SqlInt32 ERR_KOD
            {
                get { return _ERR_KOD; }
                set { _ERR_KOD = value; }
            }
        }
    }    
}
