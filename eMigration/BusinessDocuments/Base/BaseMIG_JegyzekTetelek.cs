
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments
{    
    /// <summary>
    /// MIG_JegyzekTetelek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_JegyzekTetelek
    {
        [System.Xml.Serialization.XmlType("BaseMIG_JegyzekTetelekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Jegyzek_Id = true;
               public bool Jegyzek_Id
               {
                   get { return _Jegyzek_Id; }
                   set { _Jegyzek_Id = value; }
               }
                                                            
                                 
               private bool _MIG_Foszam_Id = true;
               public bool MIG_Foszam_Id
               {
                   get { return _MIG_Foszam_Id; }
                   set { _MIG_Foszam_Id = value; }
               }
                                                            
                                 
               private bool _UGYHOL = true;
               public bool UGYHOL
               {
                   get { return _UGYHOL; }
                   set { _UGYHOL = value; }
               }
                                                            
                                 
               private bool _AtvevoIktatoszama = true;
               public bool AtvevoIktatoszama
               {
                   get { return _AtvevoIktatoszama; }
                   set { _AtvevoIktatoszama = value; }
               }
                                                            
                                 
               private bool _Megjegyzes = true;
               public bool Megjegyzes
               {
                   get { return _Megjegyzes; }
                   set { _Megjegyzes = value; }
               }
                                                            
                                 
               private bool _Azonosito = true;
               public bool Azonosito
               {
                   get { return _Azonosito; }
                   set { _Azonosito = value; }
               }
                                                            
                                 
               private bool _SztornozasDat = true;
               public bool SztornozasDat
               {
                   get { return _SztornozasDat; }
                   set { _SztornozasDat = value; }
               }
                                                            
                                 
               private bool _AtadasDatuma = true;
               public bool AtadasDatuma
               {
                   get { return _AtadasDatuma; }
                   set { _AtadasDatuma = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Jegyzek_Id = Value;               
                    
                   MIG_Foszam_Id = Value;               
                    
                   UGYHOL = Value;               
                    
                   AtvevoIktatoszama = Value;               
                    
                   Megjegyzes = Value;               
                    
                   Azonosito = Value;               
                    
                   SztornozasDat = Value;               
                    
                   AtadasDatuma = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseMIG_JegyzekTetelekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _Jegyzek_Id = SqlGuid.Null;
           
        /// <summary>
        /// Jegyzek_Id Base property </summary>
            public SqlGuid Jegyzek_Id
            {
                get { return _Jegyzek_Id; }
                set { _Jegyzek_Id = value; }                                                        
            }        
                   
           
        private SqlGuid _MIG_Foszam_Id = SqlGuid.Null;
           
        /// <summary>
        /// MIG_Foszam_Id Base property </summary>
            public SqlGuid MIG_Foszam_Id
            {
                get { return _MIG_Foszam_Id; }
                set { _MIG_Foszam_Id = value; }                                                        
            }        
                   
           
        private SqlString _UGYHOL = SqlString.Null;
           
        /// <summary>
        /// UGYHOL Base property </summary>
            public SqlString UGYHOL
            {
                get { return _UGYHOL; }
                set { _UGYHOL = value; }                                                        
            }        
                   
           
        private SqlString _AtvevoIktatoszama = SqlString.Null;
           
        /// <summary>
        /// AtvevoIktatoszama Base property </summary>
            public SqlString AtvevoIktatoszama
            {
                get { return _AtvevoIktatoszama; }
                set { _AtvevoIktatoszama = value; }                                                        
            }        
                   
           
        private SqlString _Megjegyzes = SqlString.Null;
           
        /// <summary>
        /// Megjegyzes Base property </summary>
            public SqlString Megjegyzes
            {
                get { return _Megjegyzes; }
                set { _Megjegyzes = value; }                                                        
            }        
                   
           
        private SqlString _Azonosito = SqlString.Null;
           
        /// <summary>
        /// Azonosito Base property </summary>
            public SqlString Azonosito
            {
                get { return _Azonosito; }
                set { _Azonosito = value; }                                                        
            }        
                   
           
        private SqlDateTime _SztornozasDat = SqlDateTime.Null;
           
        /// <summary>
        /// SztornozasDat Base property </summary>
            public SqlDateTime SztornozasDat
            {
                get { return _SztornozasDat; }
                set { _SztornozasDat = value; }                                                        
            }        
                   
           
        private SqlDateTime _AtadasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// AtadasDatuma Base property </summary>
            public SqlDateTime AtadasDatuma
            {
                get { return _AtadasDatuma; }
                set { _AtadasDatuma = value; }                                                        
            }        
                           }
    }    
}