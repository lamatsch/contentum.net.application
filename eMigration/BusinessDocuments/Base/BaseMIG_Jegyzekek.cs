
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments
{    
    /// <summary>
    /// MIG_Jegyzekek BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_Jegyzekek
    {
        [System.Xml.Serialization.XmlType("BaseMIG_JegyzekekBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _Tipus = true;
               public bool Tipus
               {
                   get { return _Tipus; }
                   set { _Tipus = value; }
               }
                                                            
                                 
               private bool _Csoport_Id_Felelos = true;
               public bool Csoport_Id_Felelos
               {
                   get { return _Csoport_Id_Felelos; }
                   set { _Csoport_Id_Felelos = value; }
               }
                                                            
                                 
               private bool _Felelos_Nev = true;
               public bool Felelos_Nev
               {
                   get { return _Felelos_Nev; }
                   set { _Felelos_Nev = value; }
               }
                                                            
                                 
               private bool _FelhasznaloCsoport_Id_Vegrehaj = true;
               public bool FelhasznaloCsoport_Id_Vegrehaj
               {
                   get { return _FelhasznaloCsoport_Id_Vegrehaj; }
                   set { _FelhasznaloCsoport_Id_Vegrehaj = value; }
               }
                                                            
                                 
               private bool _Vegrehajto_Nev = true;
               public bool Vegrehajto_Nev
               {
                   get { return _Vegrehajto_Nev; }
                   set { _Vegrehajto_Nev = value; }
               }
                                                            
                                 
               private bool _Atvevo_Nev = true;
               public bool Atvevo_Nev
               {
                   get { return _Atvevo_Nev; }
                   set { _Atvevo_Nev = value; }
               }
                                                            
                                 
               private bool _LezarasDatuma = true;
               public bool LezarasDatuma
               {
                   get { return _LezarasDatuma; }
                   set { _LezarasDatuma = value; }
               }
                                                            
                                 
               private bool _SztornozasDat = true;
               public bool SztornozasDat
               {
                   get { return _SztornozasDat; }
                   set { _SztornozasDat = value; }
               }
                                                            
                                 
               private bool _Nev = true;
               public bool Nev
               {
                   get { return _Nev; }
                   set { _Nev = value; }
               }
                                                            
                                 
               private bool _VegrehajtasDatuma = true;
               public bool VegrehajtasDatuma
               {
                   get { return _VegrehajtasDatuma; }
                   set { _VegrehajtasDatuma = value; }
               }
                                                            
                                 
               private bool _Allapot = true;
               public bool Allapot
               {
                   get { return _Allapot; }
                   set { _Allapot = value; }
               }
                                                            
                                 
               private bool _Allapot_Nev = true;
               public bool Allapot_Nev
               {
                   get { return _Allapot_Nev; }
                   set { _Allapot_Nev = value; }
               }
                                                            
                                 
               private bool _VegrehajtasKezdoDatuma = true;
               public bool VegrehajtasKezdoDatuma
               {
                   get { return _VegrehajtasKezdoDatuma; }
                   set { _VegrehajtasKezdoDatuma = value; }
               }
                                                            
                                 
               private bool _MegsemmisitesDatuma = true;
               public bool MegsemmisitesDatuma
               {
                   get { return _MegsemmisitesDatuma; }
                   set { _MegsemmisitesDatuma = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   Tipus = Value;               
                    
                   Csoport_Id_Felelos = Value;               
                    
                   Felelos_Nev = Value;               
                    
                   FelhasznaloCsoport_Id_Vegrehaj = Value;               
                    
                   Vegrehajto_Nev = Value;               
                    
                   Atvevo_Nev = Value;               
                    
                   LezarasDatuma = Value;               
                    
                   SztornozasDat = Value;               
                    
                   Nev = Value;               
                    
                   VegrehajtasDatuma = Value;               
                    
                   Allapot = Value;               
                    
                   Allapot_Nev = Value;               
                    
                   VegrehajtasKezdoDatuma = Value;               
                    
                   MegsemmisitesDatuma = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseMIG_JegyzekekBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlChars _Tipus = SqlChars.Null;
           
        /// <summary>
        /// Tipus Base property </summary>
            public SqlChars Tipus
            {
                get { return _Tipus; }
                set { _Tipus = value; }                                                        
            }        
                   
           
        private SqlGuid _Csoport_Id_Felelos = SqlGuid.Null;
           
        /// <summary>
        /// Csoport_Id_Felelos Base property </summary>
            public SqlGuid Csoport_Id_Felelos
            {
                get { return _Csoport_Id_Felelos; }
                set { _Csoport_Id_Felelos = value; }                                                        
            }        
                   
           
        private SqlString _Felelos_Nev = SqlString.Null;
           
        /// <summary>
        /// Felelos_Nev Base property </summary>
            public SqlString Felelos_Nev
            {
                get { return _Felelos_Nev; }
                set { _Felelos_Nev = value; }                                                        
            }        
                   
           
        private SqlGuid _FelhasznaloCsoport_Id_Vegrehaj = SqlGuid.Null;
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Vegrehaj Base property </summary>
            public SqlGuid FelhasznaloCsoport_Id_Vegrehaj
            {
                get { return _FelhasznaloCsoport_Id_Vegrehaj; }
                set { _FelhasznaloCsoport_Id_Vegrehaj = value; }                                                        
            }        
                   
           
        private SqlString _Vegrehajto_Nev = SqlString.Null;
           
        /// <summary>
        /// Vegrehajto_Nev Base property </summary>
            public SqlString Vegrehajto_Nev
            {
                get { return _Vegrehajto_Nev; }
                set { _Vegrehajto_Nev = value; }                                                        
            }        
                   
           
        private SqlString _Atvevo_Nev = SqlString.Null;
           
        /// <summary>
        /// Atvevo_Nev Base property </summary>
            public SqlString Atvevo_Nev
            {
                get { return _Atvevo_Nev; }
                set { _Atvevo_Nev = value; }                                                        
            }        
                   
           
        private SqlDateTime _LezarasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// LezarasDatuma Base property </summary>
            public SqlDateTime LezarasDatuma
            {
                get { return _LezarasDatuma; }
                set { _LezarasDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _SztornozasDat = SqlDateTime.Null;
           
        /// <summary>
        /// SztornozasDat Base property </summary>
            public SqlDateTime SztornozasDat
            {
                get { return _SztornozasDat; }
                set { _SztornozasDat = value; }                                                        
            }        
                   
           
        private SqlString _Nev = SqlString.Null;
           
        /// <summary>
        /// Nev Base property </summary>
            public SqlString Nev
            {
                get { return _Nev; }
                set { _Nev = value; }                                                        
            }        
                   
           
        private SqlDateTime _VegrehajtasDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// VegrehajtasDatuma Base property </summary>
            public SqlDateTime VegrehajtasDatuma
            {
                get { return _VegrehajtasDatuma; }
                set { _VegrehajtasDatuma = value; }                                                        
            }        
                   
           
        private SqlString _Allapot = SqlString.Null;
           
        /// <summary>
        /// Allapot Base property </summary>
            public SqlString Allapot
            {
                get { return _Allapot; }
                set { _Allapot = value; }                                                        
            }        
                   
           
        private SqlString _Allapot_Nev = SqlString.Null;
           
        /// <summary>
        /// Allapot_Nev Base property </summary>
            public SqlString Allapot_Nev
            {
                get { return _Allapot_Nev; }
                set { _Allapot_Nev = value; }                                                        
            }        
                   
           
        private SqlDateTime _VegrehajtasKezdoDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// VegrehajtasKezdoDatuma Base property </summary>
            public SqlDateTime VegrehajtasKezdoDatuma
            {
                get { return _VegrehajtasKezdoDatuma; }
                set { _VegrehajtasKezdoDatuma = value; }                                                        
            }        
                   
           
        private SqlDateTime _MegsemmisitesDatuma = SqlDateTime.Null;
           
        /// <summary>
        /// MegsemmisitesDatuma Base property </summary>
            public SqlDateTime MegsemmisitesDatuma
            {
                get { return _MegsemmisitesDatuma; }
                set { _MegsemmisitesDatuma = value; }                                                        
            }        
                           }
    }    
}