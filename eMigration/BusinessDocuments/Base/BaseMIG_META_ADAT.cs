
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments
{    
    /// <summary>
    /// MIG_META_ADAT BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_META_ADAT
    {
        [System.Xml.Serialization.XmlType("BaseMIG_META_ADATBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _OBJ_ID = true;
               public bool OBJ_ID
               {
                   get { return _OBJ_ID; }
                   set { _OBJ_ID = value; }
               }
                                                            
                                 
               private bool _META_ID = true;
               public bool META_ID
               {
                   get { return _META_ID; }
                   set { _META_ID = value; }
               }
                                                            
                                 
               private bool _ERTEK = true;
               public bool ERTEK
               {
                   get { return _ERTEK; }
                   set { _ERTEK = value; }
               }
                                                            
                                 
               private bool _Ver = true;
               public bool Ver
               {
                   get { return _Ver; }
                   set { _Ver = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   OBJ_ID = Value;               
                    
                   META_ID = Value;               
                    
                   ERTEK = Value;               
                    
                   Ver = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseMIG_META_ADATBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlGuid _OBJ_ID = SqlGuid.Null;
           
        /// <summary>
        /// OBJ_ID Base property </summary>
            public SqlGuid OBJ_ID
            {
                get { return _OBJ_ID; }
                set { _OBJ_ID = value; }                                                        
            }        
                   
           
        private SqlGuid _META_ID = SqlGuid.Null;
           
        /// <summary>
        /// META_ID Base property </summary>
            public SqlGuid META_ID
            {
                get { return _META_ID; }
                set { _META_ID = value; }                                                        
            }        
                   
           
        private SqlString _ERTEK = SqlString.Null;
           
        /// <summary>
        /// ERTEK Base property </summary>
            public SqlString ERTEK
            {
                get { return _ERTEK; }
                set { _ERTEK = value; }                                                        
            }        
                   
           
        private SqlInt32 _Ver = SqlInt32.Null;
           
        /// <summary>
        /// Ver Base property </summary>
            public SqlInt32 Ver
            {
                get { return _Ver; }
                set { _Ver = value; }                                                        
            }        
                           }
    }    
}