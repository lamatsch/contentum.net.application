
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments
{    
    /// <summary>
    /// MIG_META_TIPUS BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_META_TIPUS
    {
        [System.Xml.Serialization.XmlType("BaseMIG_META_TIPUSBaseUpdated")]
           public class BaseUpdated
           {
                                                
               private bool _Id = true;
               public bool Id
               {
                   get { return _Id; }
                   set { _Id = value; }
               }
                                                            
                                 
               private bool _TIPUS = true;
               public bool TIPUS
               {
                   get { return _TIPUS; }
                   set { _TIPUS = value; }
               }
                                                            
                                 
               private bool _CIMKE = true;
               public bool CIMKE
               {
                   get { return _CIMKE; }
                   set { _CIMKE = value; }
               }
                                                            
                                 
               private bool _Ver = true;
               public bool Ver
               {
                   get { return _Ver; }
                   set { _Ver = value; }
               }
                                                                           
               public void SetValueAll(bool Value)
               { 
                                                    
                   Id = Value;               
                    
                   TIPUS = Value;               
                    
                   CIMKE = Value;               
                    
                   Ver = Value;                              }
               
        }
    
        [System.Xml.Serialization.XmlType("BaseMIG_META_TIPUSBaseTyped")]
        public class BaseTyped
        {
                       
        private SqlGuid _Id = SqlGuid.Null;
           
        /// <summary>
        /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }                                                        
            }        
                   
           
        private SqlString _TIPUS = SqlString.Null;
           
        /// <summary>
        /// TIPUS Base property </summary>
            public SqlString TIPUS
            {
                get { return _TIPUS; }
                set { _TIPUS = value; }                                                        
            }        
                   
           
        private SqlString _CIMKE = SqlString.Null;
           
        /// <summary>
        /// CIMKE Base property </summary>
            public SqlString CIMKE
            {
                get { return _CIMKE; }
                set { _CIMKE = value; }                                                        
            }        
                   
           
        private SqlInt32 _Ver = SqlInt32.Null;
           
        /// <summary>
        /// Ver Base property </summary>
            public SqlInt32 Ver
            {
                get { return _Ver; }
                set { _Ver = value; }                                                        
            }        
                           }
    }    
}