
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_MigralasiHiba BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_MigralasiHiba
    {
        [System.Xml.Serialization.XmlType("BaseMIG_MigralasiHibaBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _ERR_KOD = true;
            public bool ERR_KOD
            {
                get { return _ERR_KOD; }
                set { _ERR_KOD = value; }
            }


            private bool _ERR_NAME = true;
            public bool ERR_NAME
            {
                get { return _ERR_NAME; }
                set { _ERR_NAME = value; }
            }


            public void SetValueAll(bool Value)
            {

                Id = Value;

                ERR_KOD = Value;

                ERR_NAME = Value;

            }

        }

        [System.Xml.Serialization.XmlType("BaseMIG_MigralasiHibaBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlInt32 _ERR_KOD = SqlInt32.Null;

            /// <summary>
            /// ERR_KOD Base property </summary>
            public SqlInt32 ERR_KOD
            {
                get { return _ERR_KOD; }
                set { _ERR_KOD = value; }
            }


            private SqlString _ERR_NAME = SqlString.Null;

            /// <summary>
            /// ERR_NAME Base property </summary>
            public SqlString ERR_NAME
            {
                get { return _ERR_NAME; }
                set { _ERR_NAME = value; }
            }

        }
    }
}