
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_Tobbes BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_Tobbes
    {
        [System.Xml.Serialization.XmlType("BaseMIG_TobbesBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _UI_SAV = true;
            public bool UI_SAV
            {
                get { return _UI_SAV; }
                set { _UI_SAV = value; }
            }


            private bool _UI_YEAR = true;
            public bool UI_YEAR
            {
                get { return _UI_YEAR; }
                set { _UI_YEAR = value; }
            }


            private bool _UI_NUM = true;
            public bool UI_NUM
            {
                get { return _UI_NUM; }
                set { _UI_NUM = value; }
            }


            private bool _ALNO = true;
            public bool ALNO
            {
                get { return _ALNO; }
                set { _ALNO = value; }
            }


            private bool _NEV = true;
            public bool NEV
            {
                get { return _NEV; }
                set { _NEV = value; }
            }


            private bool _IRSZ = true;
            public bool IRSZ
            {
                get { return _IRSZ; }
                set { _IRSZ = value; }
            }


            private bool _UTCA = true;
            public bool UTCA
            {
                get { return _UTCA; }
                set { _UTCA = value; }
            }


            private bool _HSZ = true;
            public bool HSZ
            {
                get { return _HSZ; }
                set { _HSZ = value; }
            }


            private bool _IRSZ_PLUSS = true;
            public bool IRSZ_PLUSS
            {
                get { return _IRSZ_PLUSS; }
                set { _IRSZ_PLUSS = value; }
            }


            private bool _MIG_Alszam_Id = true;
            public bool MIG_Alszam_Id
            {
                get { return _MIG_Alszam_Id; }
                set { _MIG_Alszam_Id = value; }
            }


            private bool _MIG_Foszam_Id = true;
            public bool MIG_Foszam_Id
            {
                get { return _MIG_Foszam_Id; }
                set { _MIG_Foszam_Id = value; }
            }


            private bool _MIG_Varos_Id = true;
            public bool MIG_Varos_Id
            {
                get { return _MIG_Varos_Id; }
                set { _MIG_Varos_Id = value; }
            }


            public void SetValueAll(bool Value)
            {

                Id = Value;

                UI_SAV = Value;

                UI_YEAR = Value;

                UI_NUM = Value;

                ALNO = Value;

                NEV = Value;

                IRSZ = Value;

                UTCA = Value;

                HSZ = Value;

                IRSZ_PLUSS = Value;

                MIG_Alszam_Id = Value;

                MIG_Foszam_Id = Value;

                MIG_Varos_Id = Value;

            }

        }

        [System.Xml.Serialization.XmlType("BaseMIG_TobbesBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlInt32 _UI_SAV = SqlInt32.Null;

            /// <summary>
            /// UI_SAV Base property </summary>
            public SqlInt32 UI_SAV
            {
                get { return _UI_SAV; }
                set { _UI_SAV = value; }
            }


            private SqlInt32 _UI_YEAR = SqlInt32.Null;

            /// <summary>
            /// UI_YEAR Base property </summary>
            public SqlInt32 UI_YEAR
            {
                get { return _UI_YEAR; }
                set { _UI_YEAR = value; }
            }


            private SqlInt32 _UI_NUM = SqlInt32.Null;

            /// <summary>
            /// UI_NUM Base property </summary>
            public SqlInt32 UI_NUM
            {
                get { return _UI_NUM; }
                set { _UI_NUM = value; }
            }


            private SqlInt32 _ALNO = SqlInt32.Null;

            /// <summary>
            /// ALNO Base property </summary>
            public SqlInt32 ALNO
            {
                get { return _ALNO; }
                set { _ALNO = value; }
            }


            private SqlString _NEV = SqlString.Null;

            /// <summary>
            /// NEV Base property </summary>
            public SqlString NEV
            {
                get { return _NEV; }
                set { _NEV = value; }
            }


            private SqlString _IRSZ = SqlString.Null;

            /// <summary>
            /// IRSZ Base property </summary>
            public SqlString IRSZ
            {
                get { return _IRSZ; }
                set { _IRSZ = value; }
            }


            private SqlString _UTCA = SqlString.Null;

            /// <summary>
            /// UTCA Base property </summary>
            public SqlString UTCA
            {
                get { return _UTCA; }
                set { _UTCA = value; }
            }


            private SqlString _HSZ = SqlString.Null;

            /// <summary>
            /// HSZ Base property </summary>
            public SqlString HSZ
            {
                get { return _HSZ; }
                set { _HSZ = value; }
            }


            private SqlInt32 _IRSZ_PLUSS = SqlInt32.Null;

            /// <summary>
            /// IRSZ_PLUSS Base property </summary>
            public SqlInt32 IRSZ_PLUSS
            {
                get { return _IRSZ_PLUSS; }
                set { _IRSZ_PLUSS = value; }
            }


            private SqlGuid _MIG_Alszam_Id = SqlGuid.Null;

            /// <summary>
            /// MIG_Alszam_Id Base property </summary>
            public SqlGuid MIG_Alszam_Id
            {
                get { return _MIG_Alszam_Id; }
                set { _MIG_Alszam_Id = value; }
            }


            private SqlGuid _MIG_Foszam_Id = SqlGuid.Null;

            /// <summary>
            /// MIG_Foszam_Id Base property </summary>
            public SqlGuid MIG_Foszam_Id
            {
                get { return _MIG_Foszam_Id; }
                set { _MIG_Foszam_Id = value; }
            }


            private SqlGuid _MIG_Varos_Id = SqlGuid.Null;

            /// <summary>
            /// MIG_Varos_Id Base property </summary>
            public SqlGuid MIG_Varos_Id
            {
                get { return _MIG_Varos_Id; }
                set { _MIG_Varos_Id = value; }
            }


        }
    }
}