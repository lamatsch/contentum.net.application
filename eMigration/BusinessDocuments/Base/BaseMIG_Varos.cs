
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_Varos BusinessDocument Class </summary>
    [Serializable()]
    public class BaseMIG_Varos
    {
        [System.Xml.Serialization.XmlType("BaseMIG_VarosBaseUpdated")]
        public class BaseUpdated
        {

            private bool _Id = true;
            public bool Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private bool _KOD = true;
            public bool KOD
            {
                get { return _KOD; }
                set { _KOD = value; }
            }


            private bool _SKOD = true;
            public bool SKOD
            {
                get { return _SKOD; }
                set { _SKOD = value; }
            }


            private bool _NAME = true;
            public bool NAME
            {
                get { return _NAME; }
                set { _NAME = value; }
            }


            public void SetValueAll(bool Value)
            {

                Id = Value;

                KOD = Value;

                SKOD = Value;

                NAME = Value;

            }

        }

        [System.Xml.Serialization.XmlType("BaseMIG_VarosBaseTyped")]
        public class BaseTyped
        {

            private SqlGuid _Id = SqlGuid.Null;

            /// <summary>
            /// Id Base property </summary>
            public SqlGuid Id
            {
                get { return _Id; }
                set { _Id = value; }
            }


            private SqlString _KOD = SqlString.Null;

            /// <summary>
            /// KOD Base property </summary>
            public SqlString KOD
            {
                get { return _KOD; }
                set { _KOD = value; }
            }


            private SqlInt32 _SKOD = SqlInt32.Null;

            /// <summary>
            /// SKOD Base property </summary>
            public SqlInt32 SKOD
            {
                get { return _SKOD; }
                set { _SKOD = value; }
            }


            private SqlString _NAME = SqlString.Null;

            /// <summary>
            /// NAME Base property </summary>
            public SqlString NAME
            {
                get { return _NAME; }
                set { _NAME = value; }
            }

        }
    }
}