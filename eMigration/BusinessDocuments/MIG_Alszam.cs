
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_Alszam BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_Alszam : BaseMIG_Alszam
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();

        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }
        }


        /// <summary>
        /// UI_SAV property </summary>
        public String UI_SAV
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UI_SAV); }
            set { Typed.UI_SAV = Utility.SetSqlInt32FromString(value, Typed.UI_SAV); }
        }


        /// <summary>
        /// UI_YEAR property </summary>
        public String UI_YEAR
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UI_YEAR); }
            set { Typed.UI_YEAR = Utility.SetSqlInt32FromString(value, Typed.UI_YEAR); }
        }

        /// <summary>
        /// UI_NUM property </summary>
        public String UI_NUM
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UI_NUM); }
            set { Typed.UI_NUM = Utility.SetSqlInt32FromString(value, Typed.UI_NUM); }
        }

        /// <summary>
        /// ALNO property </summary>
        public String ALNO
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ALNO); }
            set { Typed.ALNO = Utility.SetSqlInt32FromString(value, Typed.ALNO); }
        }


        /// <summary>
        /// ERK property </summary>
        public String ERK
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ERK); }
            set { Typed.ERK = Utility.SetSqlDateTimeFromString(value, Typed.ERK); }
        }


        /// <summary>
        /// IKTAT property </summary>
        public String IKTAT
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IKTAT); }
            set { Typed.IKTAT = Utility.SetSqlDateTimeFromString(value, Typed.IKTAT); }
        }


        /// <summary>
        /// BKNEV property </summary>
        public String BKNEV
        {
            get { return Utility.GetStringFromSqlString(Typed.BKNEV); }
            set { Typed.BKNEV = Utility.SetSqlStringFromString(value, Typed.BKNEV); }
        }


        /// <summary>
        /// ELINT property </summary>
        public String ELINT
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ELINT); }
            set { Typed.ELINT = Utility.SetSqlDateTimeFromString(value, Typed.ELINT); }
        }

        /// <summary>
        /// ELOAD property </summary>
        public String ELOAD
        {
            get { return Utility.GetStringFromSqlString(Typed.ELOAD); }
            set { Typed.ELOAD = Utility.SetSqlStringFromString(value, Typed.ELOAD); }
        }


        /// <summary>
        /// ROGZIT property </summary>
        public String ROGZIT
        {
            get { return Utility.GetStringFromSqlString(Typed.ROGZIT); }
            set { Typed.ROGZIT = Utility.SetSqlStringFromString(value, Typed.ROGZIT); }
        }


        /// <summary>
        /// MJ property </summary>
        public String MJ
        {
            get { return Utility.GetStringFromSqlString(Typed.MJ); }
            set { Typed.MJ = Utility.SetSqlStringFromString(value, Typed.MJ); }
        }


        /// <summary>
        /// UI_HRSZ property </summary>
        public String IDNUM
        {
            get { return Utility.GetStringFromSqlString(Typed.IDNUM); }
            set { Typed.IDNUM = Utility.SetSqlStringFromString(value, Typed.IDNUM); }
        }


        /// <summary>
        /// MIG_Foszam_Id property </summary>
        public String MIG_Foszam_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_Foszam_Id); }
            set { Typed.MIG_Foszam_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_Foszam_Id); }
        }


        /// <summary>
        /// MIG_Eloado_Id property </summary>
        public String MIG_Eloado_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_Eloado_Id); }
            set { Typed.MIG_Eloado_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_Eloado_Id); }
        }

        /// <summary>
        /// UGYHOL property </summary>
        public String UGYHOL
        {
            get { return Utility.GetStringFromSqlString(Typed.UGYHOL); }
            set { Typed.UGYHOL = Utility.SetSqlStringFromString(value, Typed.UGYHOL); }
        }

        /// <summary>
        /// IRATTARBA property </summary>
        public String IRATTARBA
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IRATTARBA); }
            set { Typed.IRATTARBA = Utility.SetSqlDateTimeFromString(value, Typed.IRATTARBA); }
        }

        /// <summary>
        /// Adathordozo_tipusa property
        /// 
        /// </summary>
        public String Adathordozo_tipusa
        {
            get { return Utility.GetStringFromSqlString(Typed.Adathordozo_tipusa); }
            set { Typed.Adathordozo_tipusa = Utility.SetSqlStringFromString(value, Typed.Adathordozo_tipusa); }
        }

        /// <summary>
        /// Irattipus property
        /// 
        /// </summary>
        public String Irattipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Irattipus); }
            set { Typed.Irattipus = Utility.SetSqlStringFromString(value, Typed.Irattipus); }
        }


        /// <summary>
        /// Feladat property
        /// 
        /// </summary>
        public String Feladat
        {
            get { return Utility.GetStringFromSqlString(Typed.Feladat); }
            set { Typed.Feladat = Utility.SetSqlStringFromString(value, Typed.Feladat); }
        }
    }

}