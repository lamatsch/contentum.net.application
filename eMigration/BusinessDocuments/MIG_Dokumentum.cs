
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eBusinessDocuments
{    
    /// <summary>
    /// MIG_Dokumentum BusinessDocument Class
    /// Az alsz�mokhoz migr�lt dokumentumok
    /// </summary>
    [Serializable()]
    public class MIG_Dokumentum : BaseMIG_Dokumentum
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// FajlNev property
        /// 
        /// </summary>
        public String FajlNev
        {
            get { return Utility.GetStringFromSqlString(Typed.FajlNev); }
            set { Typed.FajlNev = Utility.SetSqlStringFromString(value, Typed.FajlNev); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// 
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlStringFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Formatum property
        /// 
        /// </summary>
        public String Formatum
        {
            get { return Utility.GetStringFromSqlString(Typed.Formatum); }
            set { Typed.Formatum = Utility.SetSqlStringFromString(value, Typed.Formatum); }                                            
        }
                   
           
        /// <summary>
        /// Leiras property
        /// 
        /// </summary>
        public String Leiras
        {
            get { return Utility.GetStringFromSqlString(Typed.Leiras); }
            set { Typed.Leiras = Utility.SetSqlStringFromString(value, Typed.Leiras); }                                            
        }
                   
           
        /// <summary>
        /// External_Source property
        /// 
        /// </summary>
        public String External_Source
        {
            get { return Utility.GetStringFromSqlString(Typed.External_Source); }
            set { Typed.External_Source = Utility.SetSqlStringFromString(value, Typed.External_Source); }                                            
        }
                   
           
        /// <summary>
        /// External_Link property
        /// 
        /// </summary>
        public String External_Link
        {
            get { return Utility.GetStringFromSqlString(Typed.External_Link); }
            set { Typed.External_Link = Utility.SetSqlStringFromString(value, Typed.External_Link); }                                            
        }
                   
           
        /// <summary>
        /// BarCode property
        /// 
        /// </summary>
        public String BarCode
        {
            get { return Utility.GetStringFromSqlString(Typed.BarCode); }
            set { Typed.BarCode = Utility.SetSqlStringFromString(value, Typed.BarCode); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// 
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// ElektronikusAlairas property
        /// 
        /// </summary>
        public String ElektronikusAlairas
        {
            get { return Utility.GetStringFromSqlString(Typed.ElektronikusAlairas); }
            set { Typed.ElektronikusAlairas = Utility.SetSqlStringFromString(value, Typed.ElektronikusAlairas); }                                            
        }
                   
           
        /// <summary>
        /// AlairasFelulvizsgalat property
        /// 
        /// </summary>
        public String AlairasFelulvizsgalat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AlairasFelulvizsgalat); }
            set { Typed.AlairasFelulvizsgalat = Utility.SetSqlDateTimeFromString(value, Typed.AlairasFelulvizsgalat); }                                            
        }
                   
           
        /// <summary>
        /// MIG_Alszam_Id property
        /// 
        /// </summary>
        public String MIG_Alszam_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_Alszam_Id); }
            set { Typed.MIG_Alszam_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_Alszam_Id); }                                            
        }
                   
           
        /// <summary>
        /// Ver property
        /// 
        /// </summary>
        public String Ver
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Ver); }
            set { Typed.Ver = Utility.SetSqlInt32FromString(value, Typed.Ver); }                                            
        }
                   
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        public String ErvVege
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.ErvVege); }
            set { Typed.ErvVege = Utility.SetSqlDateTimeFromString(value, Typed.ErvVege); }                                            
        }
                           }
   
}