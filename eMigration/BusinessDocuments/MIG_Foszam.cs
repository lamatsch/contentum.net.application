
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_Foszam BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_Foszam : BaseMIG_Foszam
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();

        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }
        }


        /// <summary>
        /// UI_SAV property </summary>
        public String UI_SAV
        {
            get { return Utility.GetStringFromSqlString(Typed.UI_SAV); }
            set { Typed.UI_SAV = Utility.SetSqlStringFromString(value, Typed.UI_SAV); }
        }

        public String OLD_UI_SAV
        {
            get { return Utility.GetStringFromSqlInt32(Typed.OLD_UI_SAV); }
            set { Typed.OLD_UI_SAV = Utility.SetSqlInt32FromString(value, Typed.OLD_UI_SAV); }
        }



        /// <summary>
        /// UI_YEAR property </summary>
        public String UI_YEAR
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UI_YEAR); }
            set { Typed.UI_YEAR = Utility.SetSqlInt32FromString(value, Typed.UI_YEAR); }
        }

        /// <summary>
        /// UI_NUM property </summary>
        public String UI_NUM
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UI_NUM); }
            set { Typed.UI_NUM = Utility.SetSqlInt32FromString(value, Typed.UI_NUM); }
        }


        /// <summary>
        /// UI_NAME property </summary>
        public String UI_NAME
        {
            get { return Utility.GetStringFromSqlString(Typed.UI_NAME); }
            set { Typed.UI_NAME = Utility.SetSqlStringFromString(value, Typed.UI_NAME); }
        }


        /// <summary>
        /// UI_IRSZ property </summary>
        public String UI_IRSZ
        {
            get { return Utility.GetStringFromSqlString(Typed.UI_IRSZ); }
            set { Typed.UI_IRSZ = Utility.SetSqlStringFromString(value, Typed.UI_IRSZ); }
        }


        /// <summary>
        /// UI_UTCA property </summary>
        public String UI_UTCA
        {
            get { return Utility.GetStringFromSqlString(Typed.UI_UTCA); }
            set { Typed.UI_UTCA = Utility.SetSqlStringFromString(value, Typed.UI_UTCA); }
        }


        /// <summary>
        /// UI_HSZ property </summary>
        public String UI_HSZ
        {
            get { return Utility.GetStringFromSqlString(Typed.UI_HSZ); }
            set { Typed.UI_HSZ = Utility.SetSqlStringFromString(value, Typed.UI_HSZ); }
        }


        /// <summary>
        /// UI_HRSZ property </summary>
        public String UI_HRSZ
        {
            get { return Utility.GetStringFromSqlString(Typed.UI_HRSZ); }
            set { Typed.UI_HRSZ = Utility.SetSqlStringFromString(value, Typed.UI_HRSZ); }
        }


        /// <summary>
        /// UI_TYPE property </summary>
        public String UI_TYPE
        {
            get { return Utility.GetStringFromSqlString(Typed.UI_TYPE); }
            set { Typed.UI_TYPE = Utility.SetSqlStringFromString(value, Typed.UI_TYPE); }
        }


        /// <summary>
        /// UI_IRJ property </summary>
        public String UI_IRJ
        {
            get { return Utility.GetStringFromSqlString(Typed.UI_IRJ); }
            set { Typed.UI_IRJ = Utility.SetSqlStringFromString(value, Typed.UI_IRJ); }
        }


        /// <summary>
        /// UI_PERS property </summary>
        public String UI_PERS
        {
            get { return Utility.GetStringFromSqlString(Typed.UI_PERS); }
            set { Typed.UI_PERS = Utility.SetSqlStringFromString(value, Typed.UI_PERS); }
        }


        /// <summary>
        /// UI_OT_ID property </summary>
        public String UI_OT_ID
        {
            get { return Utility.GetStringFromSqlString(Typed.UI_OT_ID); }
            set { Typed.UI_OT_ID = Utility.SetSqlStringFromString(value, Typed.UI_OT_ID); }
        }


        /// <summary>
        /// MEMO property </summary>
        public String MEMO
        {
            get { return Utility.GetStringFromSqlString(Typed.MEMO); }
            set { Typed.MEMO = Utility.SetSqlStringFromString(value, Typed.MEMO); }
        }


        /// <summary>
        /// IRSZ_PLUSS property </summary>
        public String IRSZ_PLUSS
        {
            get { return Utility.GetStringFromSqlInt32(Typed.IRSZ_PLUSS); }
            set { Typed.IRSZ_PLUSS = Utility.SetSqlInt32FromString(value, Typed.IRSZ_PLUSS); }
        }


        /// <summary>
        /// IRJ2000 property </summary>
        public String IRJ2000
        {
            get { return Utility.GetStringFromSqlString(Typed.IRJ2000); }
            set { Typed.IRJ2000 = Utility.SetSqlStringFromString(value, Typed.IRJ2000); }
        }


        /// <summary>
        /// Conc property </summary>
        public String Conc
        {
            get { return Utility.GetStringFromSqlString(Typed.Conc); }
            set { Typed.Conc = Utility.SetSqlStringFromString(value, Typed.Conc); }
        }


        /// <summary>
        /// Selejtezve property </summary>
        public String Selejtezve
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Selejtezve); }
            set { Typed.Selejtezve = Utility.SetSqlInt32FromString(value, Typed.Selejtezve); }
        }


        /// <summary>
        /// Selejtezes_Datuma property </summary>
        public String Selejtezes_Datuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Selejtezes_Datuma); }
            set { Typed.Selejtezes_Datuma = Utility.SetSqlDateTimeFromString(value, Typed.Selejtezes_Datuma); }
        }


        /// <summary>
        /// Csatolva_Rendszer property </summary>
        public String Csatolva_Rendszer
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Csatolva_Rendszer); }
            set { Typed.Csatolva_Rendszer = Utility.SetSqlInt32FromString(value, Typed.Csatolva_Rendszer); }
        }


        /// <summary>
        /// Csatolva_Id property </summary>
        public String Csatolva_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csatolva_Id); }
            set { Typed.Csatolva_Id = Utility.SetSqlGuidFromString(value, Typed.Csatolva_Id); }
        }


        /// <summary>
        /// MIG_Sav_Id property </summary>
        public String MIG_Sav_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_Sav_Id); }
            set { Typed.MIG_Sav_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_Sav_Id); }
        }


        /// <summary>
        /// MIG_Varos_Id property </summary>
        public String MIG_Varos_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_Varos_Id); }
            set { Typed.MIG_Varos_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_Varos_Id); }
        }


        /// <summary>
        /// MIG_IktatasTipus_Id property </summary>
        public String MIG_IktatasTipus_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_IktatasTipus_Id); }
            set { Typed.MIG_IktatasTipus_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_IktatasTipus_Id); }
        }


        /// <summary>
        /// MIG_Eloado_Id property </summary>
        public String MIG_Eloado_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_Eloado_Id); }
            set { Typed.MIG_Eloado_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_Eloado_Id); }
        }

        /// <summary>
        /// Edok_Ugyintezo_Csoport_Id property </summary>
        public String Edok_Ugyintezo_Csoport_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Edok_Ugyintezo_Csoport_Id); }
            set { Typed.Edok_Ugyintezo_Csoport_Id = Utility.SetSqlGuidFromString(value, Typed.Edok_Ugyintezo_Csoport_Id); }
        }

        /// <summary>
        /// Edok_Ugyintezo_Nev property </summary>
        public String Edok_Ugyintezo_Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Edok_Ugyintezo_Nev); }
            set { Typed.Edok_Ugyintezo_Nev = Utility.SetSqlStringFromString(value, Typed.Edok_Ugyintezo_Nev); }
        }

        /// <summary>
        /// Edok_Utoirat_Id property </summary>
        public String Edok_Utoirat_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Edok_Utoirat_Id); }
            set { Typed.Edok_Utoirat_Id = Utility.SetSqlGuidFromString(value, Typed.Edok_Utoirat_Id); }
        }

        /// <summary>
        /// Edok_Utoirat_Azon property </summary>
        public String Edok_Utoirat_Azon
        {
            get { return Utility.GetStringFromSqlString(Typed.Edok_Utoirat_Azon); }
            set { Typed.Edok_Utoirat_Azon = Utility.SetSqlStringFromString(value, Typed.Edok_Utoirat_Azon); }
        }

        /// <summary>
        /// MIG_Eloado_Id property </summary>
        public String UGYHOL
        {
            get { return Utility.GetStringFromSqlString(Typed.UGYHOL); }
            set { Typed.UGYHOL = Utility.SetSqlStringFromString(value, Typed.UGYHOL); }
        }

        /// <summary>
        /// Hatarido property </summary>
        public String Hatarido
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Hatarido); }
            set { Typed.Hatarido = Utility.SetSqlDateTimeFromString(value, Typed.Hatarido); }
        }

        /// <summary>
        /// IRATTARBA property </summary>
        public String IRATTARBA
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IRATTARBA); }
            set { Typed.IRATTARBA = Utility.SetSqlDateTimeFromString(value, Typed.IRATTARBA); }
        }

        /// <summary>
        /// Ugy_Kezdete property </summary>
        public String Ugy_kezdete
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.Ugy_kezdete); }
            set { Typed.Ugy_kezdete = Utility.SetSqlDateTimeFromString(value, Typed.Ugy_kezdete); }
        }


        /// <summary>
        /// MIG_Eloado_Id property </summary>
        public String SCONTRO
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SCONTRO); }
            set { Typed.SCONTRO = Utility.SetSqlDateTimeFromString(value, Typed.SCONTRO); }
        }

        /// <summary>
        /// EdokSav property </summary>
        public String EdokSav
        {
            get { return Utility.GetStringFromSqlString(Typed.EdokSav); }
            set { Typed.EdokSav = Utility.SetSqlStringFromString(value, Typed.EdokSav); }
        }

        /// <summary>
        /// Feladat property
        /// 
        /// </summary>
        public String Feladat
        {
            get { return Utility.GetStringFromSqlString(Typed.Feladat); }
            set { Typed.Feladat = Utility.SetSqlStringFromString(value, Typed.Feladat); }
        }

        /// <summary>
        /// Szervezet property
        /// 
        /// </summary>
        public String Szervezet
        {
            get { return Utility.GetStringFromSqlString(Typed.Szervezet); }
            set { Typed.Szervezet = Utility.SetSqlStringFromString(value, Typed.Szervezet); }
        }

        /// <summary>
        /// IrattarbolKikeroNev property
        /// 
        /// </summary>
        public String IrattarbolKikeroNev
        {
            get { return Utility.GetStringFromSqlString(Typed.IrattarbolKikeroNev); }
            set { Typed.IrattarbolKikeroNev = Utility.SetSqlStringFromString(value, Typed.IrattarbolKikeroNev); }
        }
        /// <summary>
        /// Ugyirat_tipus property </summary>
        public String Ugyirat_tipus
        {
            get { return Utility.GetStringFromSqlString(Typed.Ugyirat_tipus); }
            set { Typed.Ugyirat_tipus = Utility.SetSqlStringFromString(value, Typed.Ugyirat_tipus); }
        }

        // <summary>
        /// IrattarbolKikeres_Datuma property
        /// 
        /// </summary>
        public String IrattarbolKikeres_Datuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.IrattarbolKikeres_Datuma); }
            set { Typed.IrattarbolKikeres_Datuma = Utility.SetSqlDateTimeFromString(value, Typed.IrattarbolKikeres_Datuma); }
        }

        // <summary>
        /// MegorzesiIdo property
        /// 
        /// </summary>
        public String MegorzesiIdo
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MegorzesiIdo); }
            set { Typed.MegorzesiIdo = Utility.SetSqlDateTimeFromString(value, Typed.MegorzesiIdo); }
        }
   

        /// <summary>
        /// IrattarId property
        /// 
        /// </summary>
        public String IrattarId
        {
            get { return Utility.GetStringFromSqlGuid(Typed.IrattarId); }
            set { Typed.IrattarId = Utility.SetSqlGuidFromString(value, Typed.IrattarId); }                                            
        }
                   
   
        /// <summary>
        /// IrattariHely property
        /// 
        /// </summary>
        public String IrattariHely
        {
            get { return Utility.GetStringFromSqlString(Typed.IrattariHely); }
            set { Typed.IrattariHely = Utility.SetSqlStringFromString(value, Typed.IrattariHely); }                                            
        }
                           }
   
}