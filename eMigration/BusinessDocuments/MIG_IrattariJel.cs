﻿using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eMigration.BusinessDocuments.Base;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_IrattariJel BusinessDocument Class
    /// 
    /// </summary>
    [Serializable()]
    public class MIG_IrattariJel : BaseMIG_IrattariJel
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();

        /// <summary>
        /// EV property
        /// 
        /// </summary>
        public String EV
        {
            get { return Utility.GetStringFromSqlString(Typed.EV); }
            set { Typed.EV = Utility.SetSqlStringFromString(value, Typed.EV); }
        }


        /// <summary>
        /// NAME property
        /// 
        /// </summary>
        public String NAME
        {
            get { return Utility.GetStringFromSqlString(Typed.NAME); }
            set { Typed.NAME = Utility.SetSqlStringFromString(value, Typed.NAME); }
        }


        /// <summary>
        /// KOD property
        /// 
        /// </summary>
        public String KOD
        {
            get { return Utility.GetStringFromSqlString(Typed.KOD); }
            set { Typed.KOD = Utility.SetSqlStringFromString(value, Typed.KOD); }
        }


        /// <summary>
        /// IRJEL property
        /// 
        /// </summary>
        public String IRJEL
        {
            get { return Utility.GetStringFromSqlString(Typed.IRJEL); }
            set { Typed.IRJEL = Utility.SetSqlStringFromString(value, Typed.IRJEL); }
        }


        /// <summary>
        /// UI property
        /// 
        /// </summary>
        public String UI
        {
            get { return Utility.GetStringFromSqlString(Typed.UI); }
            set { Typed.UI = Utility.SetSqlStringFromString(value, Typed.UI); }
        }


        /// <summary>
        /// HATOS property
        /// 
        /// </summary>
        public String HATOS
        {
            get { return Utility.GetStringFromSqlString(Typed.HATOS); }
            set { Typed.HATOS = Utility.SetSqlStringFromString(value, Typed.HATOS); }
        }


        /// <summary>
        /// KIADM property
        /// 
        /// </summary>
        public String KIADM
        {
            get { return Utility.GetStringFromSqlString(Typed.KIADM); }
            set { Typed.KIADM = Utility.SetSqlStringFromString(value, Typed.KIADM); }
        }


        /// <summary>
        /// IND property
        /// 
        /// </summary>
        public String IND
        {
            get { return Utility.GetStringFromSqlInt32(Typed.IND); }
            set { Typed.IND = Utility.SetSqlInt32FromString(value, Typed.IND); }
        }


        /// <summary>
        /// ERR_KOD property
        /// 
        /// </summary>
        public String ERR_KOD
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ERR_KOD); }
            set { Typed.ERR_KOD = Utility.SetSqlInt32FromString(value, Typed.ERR_KOD); }
        }
    }
}
