
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_JegyzekTetelek BusinessDocument Class
    /// Jegyz�khez rendelt MIG_Foszam rekordok (�gyiratok)
    /// </summary>
    [Serializable()]
    public class MIG_JegyzekTetelek : BaseMIG_JegyzekTetelek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Jegyzek_Id property
        /// 
        /// </summary>
        public String Jegyzek_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Jegyzek_Id); }
            set { Typed.Jegyzek_Id = Utility.SetSqlGuidFromString(value, Typed.Jegyzek_Id); }                                            
        }
                   
           
        /// <summary>
        /// MIG_Foszam_Id property
        /// Ez annak a valaminek a GUID -ja ("allObjGuids"-b�l)  amit csatoltunk
        /// </summary>
        public String MIG_Foszam_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_Foszam_Id); }
            set { Typed.MIG_Foszam_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_Foszam_Id); }                                            
        }
                   
           
        /// <summary>
        /// UGYHOL property
        /// �gyirat (f�sz�m) helye a jegyz�kre helyez�s el�tt:
        /// 0 - �SZI iratt�r
        /// 1 - Skontr�
        /// 2 - Iratt�rban
        /// 3 - Oszt�lyon
        /// 4 - K�zt.Hiv
        /// 5 - B�r�s�g
        /// 6 - �gy�szs�g
        /// 7 - Vezet�s�g
        /// 8 - K�zjegyz�
        /// 9 - FPH
        /// S - Selejtezett
        /// V - Selejtez�sre v�r
        /// I - Iratt�rb�l elk�rt
        /// E - Skontr�b�l elk�rt
        /// L - Lomt�r
        /// T - Lev�lt�rban
        /// J - Jegyz�ken
        /// Z - Lez�rt jegyz�ken
        /// A - egy�b szervezetnek �tadott
        /// 
        /// </summary>
        public String UGYHOL
        {
            get { return Utility.GetStringFromSqlString(Typed.UGYHOL); }
            set { Typed.UGYHOL = Utility.SetSqlStringFromString(value, Typed.UGYHOL); }                                            
        }
                   
           
        /// <summary>
        /// AtvevoIktatoszama property
        /// 
        /// </summary>
        public String AtvevoIktatoszama
        {
            get { return Utility.GetStringFromSqlString(Typed.AtvevoIktatoszama); }
            set { Typed.AtvevoIktatoszama = Utility.SetSqlStringFromString(value, Typed.AtvevoIktatoszama); }                                            
        }
                   
           
        /// <summary>
        /// Megjegyzes property
        /// Megjegyz�s
        /// </summary>
        public String Megjegyzes
        {
            get { return Utility.GetStringFromSqlString(Typed.Megjegyzes); }
            set { Typed.Megjegyzes = Utility.SetSqlStringFromString(value, Typed.Megjegyzes); }                                            
        }
                   
           
        /// <summary>
        /// Azonosito property
        /// Objektum azonos�t� sz�vegesen
        /// </summary>
        public String Azonosito
        {
            get { return Utility.GetStringFromSqlString(Typed.Azonosito); }
            set { Typed.Azonosito = Utility.SetSqlStringFromString(value, Typed.Azonosito); }                                            
        }
                   
           
        /// <summary>
        /// SztornozasDat property
        /// Sztorn�z�s d�tuma (t�tel t�rl�se a jegyz�kr�l)
        /// </summary>
        public String SztornozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornozasDat); }
            set { Typed.SztornozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornozasDat); }                                            
        }
                   
           
        /// <summary>
        /// AtadasDatuma property
        /// 
        /// </summary>
        public String AtadasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.AtadasDatuma); }
            set { Typed.AtadasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.AtadasDatuma); }                                            
        }
                           }
   
}