
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{    
    /// <summary>
    /// MIG_Jegyzekek BusinessDocument Class
    /// Jegyz�kek (pl. egy�b szervezetnek val� �tad�shoz)
    /// </summary>
    [Serializable()]
    public class MIG_Jegyzekek : BaseMIG_Jegyzekek
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// Tipus property
        /// E - Egy�b szervezetnek �tad�si jegyz�k
        /// </summary>
        public String Tipus
        {
            get { return Utility.GetStringFromSqlChars(Typed.Tipus); }
            set { Typed.Tipus = Utility.SetSqlCharsFromString(value, Typed.Tipus); }                                            
        }
                   
           
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Felel�s felhaszn�l� Id
        /// </summary>
        public String Csoport_Id_Felelos
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Csoport_Id_Felelos); }
            set { Typed.Csoport_Id_Felelos = Utility.SetSqlGuidFromString(value, Typed.Csoport_Id_Felelos); }                                            
        }
                   
           
        /// <summary>
        /// Felelos_Nev property
        /// Felel�s felhaszn�l� n�v
        /// </summary>
        public String Felelos_Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Felelos_Nev); }
            set { Typed.Felelos_Nev = Utility.SetSqlStringFromString(value, Typed.Felelos_Nev); }                                            
        }
                   
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Vegrehaj property
        /// V�grehajt� felhaszn�l�
        /// </summary>
        public String FelhasznaloCsoport_Id_Vegrehaj
        {
            get { return Utility.GetStringFromSqlGuid(Typed.FelhasznaloCsoport_Id_Vegrehaj); }
            set { Typed.FelhasznaloCsoport_Id_Vegrehaj = Utility.SetSqlGuidFromString(value, Typed.FelhasznaloCsoport_Id_Vegrehaj); }                                            
        }
                   
           
        /// <summary>
        /// Vegrehajto_Nev property
        /// V�grehajt� felhaszn�l� n�v
        /// </summary>
        public String Vegrehajto_Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Vegrehajto_Nev); }
            set { Typed.Vegrehajto_Nev = Utility.SetSqlStringFromString(value, Typed.Vegrehajto_Nev); }                                            
        }
                   
           
        /// <summary>
        /// Atvevo_Nev property
        /// �tvev� megnevez�se
        /// </summary>
        public String Atvevo_Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Atvevo_Nev); }
            set { Typed.Atvevo_Nev = Utility.SetSqlStringFromString(value, Typed.Atvevo_Nev); }                                            
        }
                   
           
        /// <summary>
        /// LezarasDatuma property
        /// Lez�r�s d�tuma
        /// </summary>
        public String LezarasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.LezarasDatuma); }
            set { Typed.LezarasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.LezarasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// SztornozasDat property
        /// Sztorn�z�s d�tuma
        /// </summary>
        public String SztornozasDat
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.SztornozasDat); }
            set { Typed.SztornozasDat = Utility.SetSqlDateTimeFromString(value, Typed.SztornozasDat); }                                            
        }
                   
           
        /// <summary>
        /// Nev property
        /// Jegyz�k megnevez�se
        /// </summary>
        public String Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Nev); }
            set { Typed.Nev = Utility.SetSqlStringFromString(value, Typed.Nev); }                                            
        }
                   
           
        /// <summary>
        /// VegrehajtasDatuma property
        /// �tad�s id�pontja
        /// </summary>
        public String VegrehajtasDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VegrehajtasDatuma); }
            set { Typed.VegrehajtasDatuma = Utility.SetSqlDateTimeFromString(value, Typed.VegrehajtasDatuma); }                                            
        }
                   
           
        /// <summary>
        /// Allapot property
        /// 
        /// </summary>
        public String Allapot
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot); }
            set { Typed.Allapot = Utility.SetSqlStringFromString(value, Typed.Allapot); }                                            
        }
                   
           
        /// <summary>
        /// Allapot_Nev property
        /// 
        /// </summary>
        public String Allapot_Nev
        {
            get { return Utility.GetStringFromSqlString(Typed.Allapot_Nev); }
            set { Typed.Allapot_Nev = Utility.SetSqlStringFromString(value, Typed.Allapot_Nev); }                                            
        }
                   
           
        /// <summary>
        /// VegrehajtasKezdoDatuma property
        /// 
        /// </summary>
        public String VegrehajtasKezdoDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.VegrehajtasKezdoDatuma); }
            set { Typed.VegrehajtasKezdoDatuma = Utility.SetSqlDateTimeFromString(value, Typed.VegrehajtasKezdoDatuma); }                                            
        }
                   
           
        /// <summary>
        /// MegsemmisitesDatuma property
        /// 
        /// </summary>
        public String MegsemmisitesDatuma
        {
            get { return Utility.GetStringFromSqlDateTime(Typed.MegsemmisitesDatuma); }
            set { Typed.MegsemmisitesDatuma = Utility.SetSqlDateTimeFromString(value, Typed.MegsemmisitesDatuma); }                                            
        }
                           }
   
}