
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{    
    /// <summary>
    /// MIG_META_ADAT BusinessDocument Class
    /// "..., hogy a csak egyes �gyfelekn�l megjelen�  m�sodlagos/le�r� META adatokat tudjuk t�rolni ..."
    /// </summary>
    [Serializable()]
    public class MIG_META_ADAT : BaseMIG_META_ADAT
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// OBJ_ID property
        /// MIG_Alszam.ID, ha META_ID -> TIPUS='A'-ra mutat; MIG_Foszam.ID, ha META_ID -> TIPUS='F' -ra mutat
        /// </summary>
        public String OBJ_ID
        {
            get { return Utility.GetStringFromSqlGuid(Typed.OBJ_ID); }
            set { Typed.OBJ_ID = Utility.SetSqlGuidFromString(value, Typed.OBJ_ID); }                                            
        }
                   
           
        /// <summary>
        /// META_ID property
        /// 
        /// </summary>
        public String META_ID
        {
            get { return Utility.GetStringFromSqlGuid(Typed.META_ID); }
            set { Typed.META_ID = Utility.SetSqlGuidFromString(value, Typed.META_ID); }                                            
        }
                   
           
        /// <summary>
        /// ERTEK property
        /// 
        /// </summary>
        public String ERTEK
        {
            get { return Utility.GetStringFromSqlString(Typed.ERTEK); }
            set { Typed.ERTEK = Utility.SetSqlStringFromString(value, Typed.ERTEK); }                                            
        }
                   
           
        /// <summary>
        /// Ver property
        /// 
        /// </summary>
        public String Ver
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Ver); }
            set { Typed.Ver = Utility.SetSqlInt32FromString(value, Typed.Ver); }                                            
        }
                           }
   
}