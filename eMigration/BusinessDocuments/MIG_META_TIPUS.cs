
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{    
    /// <summary>
    /// MIG_META_TIPUS BusinessDocument Class
    /// m�sodlagos META adat t�pusa
    /// </summary>
    [Serializable()]
    public class MIG_META_TIPUS : BaseMIG_META_TIPUS
    {
           [NonSerializedAttribute]           
           public BaseTyped Typed = new BaseTyped();
           [NonSerializedAttribute]           
           public BaseDocument Base = new BaseDocument();
           [NonSerializedAttribute]           
           public BaseUpdated Updated = new BaseUpdated();
                      
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }                                            
        }
                   
           
        /// <summary>
        /// TIPUS property
        /// A - alsz�m; F - f�sz�m
        /// </summary>
        public String TIPUS
        {
            get { return Utility.GetStringFromSqlString(Typed.TIPUS); }
            set { Typed.TIPUS = Utility.SetSqlStringFromString(value, Typed.TIPUS); }                                            
        }
                   
           
        /// <summary>
        /// CIMKE property
        /// 
        /// </summary>
        public String CIMKE
        {
            get { return Utility.GetStringFromSqlString(Typed.CIMKE); }
            set { Typed.CIMKE = Utility.SetSqlStringFromString(value, Typed.CIMKE); }                                            
        }
                   
           
        /// <summary>
        /// Ver property
        /// 
        /// </summary>
        public String Ver
        {
            get { return Utility.GetStringFromSqlInt32(Typed.Ver); }
            set { Typed.Ver = Utility.SetSqlInt32FromString(value, Typed.Ver); }                                            
        }
                           }
   
}