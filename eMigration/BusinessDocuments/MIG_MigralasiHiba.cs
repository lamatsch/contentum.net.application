
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_MigralasiHiba BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_MigralasiHiba : BaseMIG_MigralasiHiba
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();

        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }
        }


        /// <summary>
        /// ERR_KOD property </summary>
        public String ERR_KOD
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ERR_KOD); }
            set { Typed.ERR_KOD = Utility.SetSqlInt32FromString(value, Typed.ERR_KOD); }
        }


        /// <summary>
        /// ERR_NAME property </summary>
        public String ERR_NAME
        {
            get { return Utility.GetStringFromSqlString(Typed.ERR_NAME); }
            set { Typed.ERR_NAME = Utility.SetSqlStringFromString(value, Typed.ERR_NAME); }
        }

    }

}