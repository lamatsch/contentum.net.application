
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_Sav BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_Sav : BaseMIG_Sav
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();

        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }
        }


        /// <summary>
        /// UI_SAV property </summary>
        public String EV
        {
            get { return Utility.GetStringFromSqlInt32(Typed.EV); }
            set { Typed.EV = Utility.SetSqlInt32FromString(value, Typed.EV); }
        }


        /// <summary>
        /// KOD property </summary>
        public String KOD
        {
            get { return Utility.GetStringFromSqlInt32(Typed.KOD); }
            set { Typed.KOD = Utility.SetSqlInt32FromString(value, Typed.KOD); }
        }


        /// <summary>
        /// NAME property </summary>
        public String NAME
        {
            get { return Utility.GetStringFromSqlString(Typed.NAME); }
            set { Typed.NAME = Utility.SetSqlStringFromString(value, Typed.NAME); }
        }

    }

}