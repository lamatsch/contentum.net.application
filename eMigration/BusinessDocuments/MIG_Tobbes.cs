
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_Tobbes BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_Tobbes : BaseMIG_Tobbes
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();

        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }
        }


        /// <summary>
        /// UI_SAV property </summary>
        public String UI_SAV
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UI_SAV); }
            set { Typed.UI_SAV = Utility.SetSqlInt32FromString(value, Typed.UI_SAV); }
        }


        /// <summary>
        /// UI_YEAR property </summary>
        public String UI_YEAR
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UI_YEAR); }
            set { Typed.UI_YEAR = Utility.SetSqlInt32FromString(value, Typed.UI_YEAR); }
        }

        /// <summary>
        /// UI_NUM property </summary>
        public String UI_NUM
        {
            get { return Utility.GetStringFromSqlInt32(Typed.UI_NUM); }
            set { Typed.UI_NUM = Utility.SetSqlInt32FromString(value, Typed.UI_NUM); }
        }

        /// <summary>
        /// ALNO property </summary>
        public String ALNO
        {
            get { return Utility.GetStringFromSqlInt32(Typed.ALNO); }
            set { Typed.ALNO = Utility.SetSqlInt32FromString(value, Typed.ALNO); }
        }


        /// <summary>
        /// NEV property </summary>
        public String NEV
        {
            get { return Utility.GetStringFromSqlString(Typed.NEV); }
            set { Typed.NEV = Utility.SetSqlStringFromString(value, Typed.NEV); }
        }


        /// <summary>
        /// IRSZ property </summary>
        public String IRSZ
        {
            get { return Utility.GetStringFromSqlString(Typed.IRSZ); }
            set { Typed.IRSZ = Utility.SetSqlStringFromString(value, Typed.IRSZ); }
        }


        /// <summary>
        /// UTCA property </summary>
        public String UTCA
        {
            get { return Utility.GetStringFromSqlString(Typed.UTCA); }
            set { Typed.UTCA = Utility.SetSqlStringFromString(value, Typed.UTCA); }
        }


        /// <summary>
        /// HSZ property </summary>
        public String HSZ
        {
            get { return Utility.GetStringFromSqlString(Typed.HSZ); }
            set { Typed.HSZ = Utility.SetSqlStringFromString(value, Typed.HSZ); }
        }


        /// <summary>
        /// IRSZ_PLUSS property </summary>
        public String IRSZ_PLUSS
        {
            get { return Utility.GetStringFromSqlInt32(Typed.IRSZ_PLUSS); }
            set { Typed.IRSZ_PLUSS = Utility.SetSqlInt32FromString(value, Typed.IRSZ_PLUSS); }
        }


        /// <summary>
        /// MIG_Alszam_Id property </summary>
        public String MIG_Alszam_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_Alszam_Id); }
            set { Typed.MIG_Alszam_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_Alszam_Id); }
        }


        /// <summary>
        /// MIG_Foszam_Id property </summary>
        public String MIG_Foszam_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_Foszam_Id); }
            set { Typed.MIG_Foszam_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_Foszam_Id); }
        }


        /// <summary>
        /// MIG_Varos_Id property </summary>
        public String MIG_Varos_Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.MIG_Varos_Id); }
            set { Typed.MIG_Varos_Id = Utility.SetSqlGuidFromString(value, Typed.MIG_Varos_Id); }
        }

    }

}