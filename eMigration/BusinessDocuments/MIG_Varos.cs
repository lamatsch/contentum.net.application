
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;

namespace Contentum.eMigration.BusinessDocuments
{
    /// <summary>
    /// MIG_Varos BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_Varos : BaseMIG_Varos
    {
        [NonSerializedAttribute]
        public BaseTyped Typed = new BaseTyped();
        [NonSerializedAttribute]
        public BaseDocument Base = new BaseDocument();
        [NonSerializedAttribute]
        public BaseUpdated Updated = new BaseUpdated();

        /// <summary>
        /// Id property </summary>
        public String Id
        {
            get { return Utility.GetStringFromSqlGuid(Typed.Id); }
            set { Typed.Id = Utility.SetSqlGuidFromString(value, Typed.Id); }
        }


        /// <summary>
        /// KOD property </summary>
        public String KOD
        {
            get { return Utility.GetStringFromSqlString(Typed.KOD); }
            set { Typed.KOD = Utility.SetSqlStringFromString(value, Typed.KOD); }
        }


        /// <summary>
        /// SKOD property </summary>
        public String SKOD
        {
            get { return Utility.GetStringFromSqlInt32(Typed.SKOD); }
            set { Typed.SKOD = Utility.SetSqlInt32FromString(value, Typed.SKOD); }
        }


        /// <summary>
        /// NAME property </summary>
        public String NAME
        {
            get { return Utility.GetStringFromSqlString(Typed.NAME); }
            set { Typed.NAME = Utility.SetSqlStringFromString(value, Typed.NAME); }
        }

    }

}