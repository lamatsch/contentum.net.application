
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    /// <summary>
    /// MIG_Alszam Query.BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_AlszamSearch:BaseSearchObject
    {
        public MIG_AlszamSearch()
        {
            _Id.Name = "MIG_Alszam.Id";
            _Id.Type = "Guid";
            _UI_SAV.Name = "MIG_Alszam.UI_SAV";
            _UI_SAV.Type = "Int32";
            _UI_YEAR.Name = "MIG_Alszam.UI_YEAR";
            _UI_YEAR.Type = "Int32";
            _UI_NUM.Name = "MIG_Alszam.UI_NUM";
            _UI_NUM.Type = "Int32";
            _ALNO.Name = "MIG_Alszam.ALNO";
            _ALNO.Type = "Int32";
            _ERK.Name = "MIG_Alszam.ERK";
            _ERK.Type = "DateTime";
            _ERK.Value = "getdate()";
            _IKTAT.Name = "MIG_Alszam.IKTAT";
            _IKTAT.Type = "DateTime";
            _IKTAT.Value = "getdate()";
            _BKNEV.Name = "MIG_Alszam.BKNEV";
            _BKNEV.Type = "String";
            _ELINT.Name = "MIG_Alszam.ELINT";
            _ELINT.Type = "DateTime";
            _ELINT.Value = "getdate()";
            _ELOAD.Name = "MIG_Alszam.ELOAD";
            _ELOAD.Type = "String";
            _ROGZIT.Name = "MIG_Alszam.ROGZIT";
            _ROGZIT.Type = "String";
            _MJ.Name = "MIG_Alszam.MJ";
            _MJ.Type = "String";
            _IDNUM.Name = "MIG_Alszam.IDNUM";
            _IDNUM.Type = "String";
            _MIG_Foszam_Id.Name = "MIG_Alszam.MIG_Foszam_Id";
            _MIG_Foszam_Id.Type = "Guid";
            _MIG_Eloado_Id.Name = "MIG_Alszam.MIG_Eloado_Id";
            _MIG_Eloado_Id.Type = "Guid";
            _UGYHOL.Name = "MIG_Alszam.UGYHOL";
            _UGYHOL.Type = "String";
            _IRATTARBA.Name = "MIG_Alszam.IRATTARBA";
            _IRATTARBA.Type = "DateTime";
            _IRATTARBA.Value = "getdate()";
            _Adathordozo_tipusa.Name = "MIG_Alszam.Adathordozo_tipusa";
            _Adathordozo_tipusa.Type = "String";
            _Irattipus.Name = "MIG_Alszam.Irattipus";
            _Irattipus.Type = "String";
            _Feladat.Name = "MIG_Alszam.Feladat";
            _Feladat.Type = "String";


        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// UI_SAV property </summary>
        private Field _UI_SAV = new Field();

        public Field UI_SAV
        {
            get { return _UI_SAV; }
            set { _UI_SAV = value; }
        }


        /// <summary>
        /// UI_YEAR property </summary>
        private Field _UI_YEAR = new Field();

        public Field UI_YEAR
        {
            get { return _UI_YEAR; }
            set { _UI_YEAR = value; }
        }


        /// <summary>
        /// UI_NUM property </summary>
        private Field _UI_NUM = new Field();

        public Field UI_NUM
        {
            get { return _UI_NUM; }
            set { _UI_NUM = value; }
        }


        /// <summary>
        /// ALNO property </summary>
        private Field _ALNO = new Field();

        public Field ALNO
        {
            get { return _ALNO; }
            set { _ALNO = value; }
        }


        /// <summary>
        /// ERK property </summary>
        private Field _ERK = new Field();

        public Field ERK
        {
            get { return _ERK; }
            set { _ERK = value; }
        }


        /// <summary>
        /// IKTAT property </summary>
        private Field _IKTAT = new Field();

        public Field IKTAT
        {
            get { return _IKTAT; }
            set { _IKTAT = value; }
        }


        /// <summary>
        /// BKNEV property </summary>
        private Field _BKNEV = new Field();

        public Field BKNEV
        {
            get { return _BKNEV; }
            set { _BKNEV = value; }
        }


        /// <summary>
        /// ELINT property </summary>
        private Field _ELINT = new Field();

        public Field ELINT
        {
            get { return _ELINT; }
            set { _ELINT = value; }
        }

        /// <summary>
        /// ELOAD property </summary>
        private Field _ELOAD = new Field();

        public Field ELOAD
        {
            get { return _ELOAD; }
            set { _ELOAD = value; }
        }


        /// <summary>
        /// ROGZIT property </summary>
        private Field _ROGZIT = new Field();

        public Field ROGZIT
        {
            get { return _ROGZIT; }
            set { _ROGZIT = value; }
        }


        /// <summary>
        /// MJ property </summary>
        private Field _MJ = new Field();

        public Field MJ
        {
            get { return _MJ; }
            set { _MJ = value; }
        }


        /// <summary>
        /// IDNUM property </summary>
        private Field _IDNUM = new Field();

        public Field IDNUM
        {
            get { return _IDNUM; }
            set { _IDNUM = value; }
        }


        /// <summary>
        /// MIG_Foszam_Id property </summary>
        private Field _MIG_Foszam_Id = new Field();

        public Field MIG_Foszam_Id
        {
            get { return _MIG_Foszam_Id; }
            set { _MIG_Foszam_Id = value; }
        }


        /// <summary>
        /// MIG_Eloado_Id property </summary>
        private Field _MIG_Eloado_Id = new Field();

        public Field MIG_Eloado_Id
        {
            get { return _MIG_Eloado_Id; }
            set { _MIG_Eloado_Id = value; }
        }

        /// <summary>
        /// UGYHOL property </summary>
        private Field _UGYHOL = new Field();

        public Field UGYHOL
        {
            get { return _UGYHOL; }
            set { _UGYHOL = value; }
        }

        /// <summary>
        /// IRATTARBA property </summary>
        private Field _IRATTARBA = new Field();

        public Field IRATTARBA
        {
            get { return _IRATTARBA; }
            set { _IRATTARBA = value; }
        }

        /// <summary>
        /// Adathordozo_tipusa property
        /// 
        /// </summary>
        private Field _Adathordozo_tipusa = new Field();

        public Field Adathordozo_tipusa
        {
            get { return _Adathordozo_tipusa; }
            set { _Adathordozo_tipusa = value; }
        }


        /// <summary>
        /// Irattipus property
        /// 
        /// </summary>
        private Field _Irattipus = new Field();

        public Field Irattipus
        {
            get { return _Irattipus; }
            set { _Irattipus = value; }
        }


        /// <summary>
        /// Feladat property
        /// 
        /// </summary>
        private Field _Feladat = new Field();

        public Field Feladat
        {
            get { return _Feladat; }
            set { _Feladat = value; }
        }

    }

}