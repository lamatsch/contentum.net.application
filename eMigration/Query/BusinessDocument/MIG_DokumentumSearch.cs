
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// MIG_Dokumentum eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class MIG_DokumentumSearch: BaseSearchObject    {
      public MIG_DokumentumSearch()
      {            
                     _Id.Name = "MIG_Dokumentum.Id";
               _Id.Type = "Guid";            
               _FajlNev.Name = "MIG_Dokumentum.FajlNev";
               _FajlNev.Type = "String";            
               _Tipus.Name = "MIG_Dokumentum.Tipus";
               _Tipus.Type = "String";            
               _Formatum.Name = "MIG_Dokumentum.Formatum";
               _Formatum.Type = "String";            
               _Leiras.Name = "MIG_Dokumentum.Leiras";
               _Leiras.Type = "String";            
               _External_Source.Name = "MIG_Dokumentum.External_Source";
               _External_Source.Type = "String";            
               _External_Link.Name = "MIG_Dokumentum.External_Link";
               _External_Link.Type = "String";            
               _BarCode.Name = "MIG_Dokumentum.BarCode";
               _BarCode.Type = "String";            
               _Allapot.Name = "MIG_Dokumentum.Allapot";
               _Allapot.Type = "String";            
               _ElektronikusAlairas.Name = "MIG_Dokumentum.ElektronikusAlairas";
               _ElektronikusAlairas.Type = "String";            
               _AlairasFelulvizsgalat.Name = "MIG_Dokumentum.AlairasFelulvizsgalat";
               _AlairasFelulvizsgalat.Type = "DateTime";            
               _MIG_Alszam_Id.Name = "MIG_Dokumentum.MIG_Alszam_Id";
               _MIG_Alszam_Id.Type = "Guid";            
               _Ver.Name = "MIG_Dokumentum.Ver";
               _Ver.Type = "Int32";            
               _ErvVege.Name = "ISNULL(MIG_Dokumentum.ErvVege, '4700-12-31')";
               _ErvVege.Type = "DateTime";
               _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// FajlNev property
        /// 
        /// </summary>
        private Field _FajlNev = new Field();

        public Field FajlNev
        {
            get { return _FajlNev; }
            set { _FajlNev = value; }
        }
                          
           
        /// <summary>
        /// Tipus property
        /// 
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Formatum property
        /// 
        /// </summary>
        private Field _Formatum = new Field();

        public Field Formatum
        {
            get { return _Formatum; }
            set { _Formatum = value; }
        }
                          
           
        /// <summary>
        /// Leiras property
        /// 
        /// </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// External_Source property
        /// 
        /// </summary>
        private Field _External_Source = new Field();

        public Field External_Source
        {
            get { return _External_Source; }
            set { _External_Source = value; }
        }
                          
           
        /// <summary>
        /// External_Link property
        /// 
        /// </summary>
        private Field _External_Link = new Field();

        public Field External_Link
        {
            get { return _External_Link; }
            set { _External_Link = value; }
        }
                          
           
        /// <summary>
        /// BarCode property
        /// 
        /// </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// 
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// ElektronikusAlairas property
        /// 
        /// </summary>
        private Field _ElektronikusAlairas = new Field();

        public Field ElektronikusAlairas
        {
            get { return _ElektronikusAlairas; }
            set { _ElektronikusAlairas = value; }
        }
                          
           
        /// <summary>
        /// AlairasFelulvizsgalat property
        /// 
        /// </summary>
        private Field _AlairasFelulvizsgalat = new Field();

        public Field AlairasFelulvizsgalat
        {
            get { return _AlairasFelulvizsgalat; }
            set { _AlairasFelulvizsgalat = value; }
        }
                          
           
        /// <summary>
        /// MIG_Alszam_Id property
        /// 
        /// </summary>
        private Field _MIG_Alszam_Id = new Field();

        public Field MIG_Alszam_Id
        {
            get { return _MIG_Alszam_Id; }
            set { _MIG_Alszam_Id = value; }
        }
                          
           
        /// <summary>
        /// Ver property
        /// 
        /// </summary>
        private Field _Ver = new Field();

        public Field Ver
        {
            get { return _Ver; }
            set { _Ver = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}