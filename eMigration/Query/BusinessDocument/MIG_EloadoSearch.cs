
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    /// <summary>
    /// MIG_Eloado Query.BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_EloadoSearch
    {
        public MIG_EloadoSearch()
        {
            _Id.Name = "MIG_Eloado.Id";
            _Id.Type = "Guid";
            _EV.Name = "MIG_Eloado.EV";
            _EV.Type = "Int32";
            _KOD.Name = "MIG_Eloado.KOD";
            _KOD.Type = "String";
            _NAME.Name = "MIG_Eloado.NAME";
            _NAME.Type = "String";

        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// EV property </summary>
        private Field _EV = new Field();

        public Field EV
        {
            get { return _EV; }
            set { _EV = value; }
        }


        /// <summary>
        /// KOD property </summary>
        private Field _KOD = new Field();

        public Field KOD
        {
            get { return _KOD; }
            set { _KOD = value; }
        }


        /// <summary>
        /// NAME property </summary>
        private Field _NAME = new Field();

        public Field NAME
        {
            get { return _NAME; }
            set { _NAME = value; }
        }

    }

}