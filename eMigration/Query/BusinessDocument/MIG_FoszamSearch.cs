
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
	/// <summary>
	/// MIG_Foszam Query.BusinessDocument Class </summary>
	[Serializable( )]
	public class MIG_FoszamSearch : BaseSearchObject
	{
		public MIG_FoszamSearch( )
		{
			_Id.Name = "MIG_Foszam.Id";
			_Id.Type = "Guid";
			_UI_SAV.Name = "MIG_Foszam.UI_SAV";
			_UI_SAV.Type = "Int32";
			_UI_YEAR.Name = "MIG_Foszam.UI_YEAR";
			_UI_YEAR.Type = "Int32";
			_UI_NUM.Name = "MIG_Foszam.UI_NUM";
			_UI_NUM.Type = "Int32";
			_UI_NAME.Name = "MIG_Foszam.UI_NAME";
			_UI_NAME.Type = "String";
			_UI_IRSZ.Name = "MIG_Foszam.UI_IRSZ";
			_UI_IRSZ.Type = "String";
			_UI_UTCA.Name = "MIG_Foszam.UI_UTCA";
			_UI_UTCA.Type = "String";
			_UI_HSZ.Name = "MIG_Foszam.UI_HSZ";
			_UI_HSZ.Type = "String";
			_UI_HRSZ.Name = "MIG_Foszam.UI_HRSZ";
			_UI_HRSZ.Type = "String";
			_UI_TYPE.Name = "MIG_Foszam.UI_TYPE";
			_UI_TYPE.Type = "String";
			_UI_IRJ.Name = "MIG_Foszam.UI_IRJ";
			_UI_IRJ.Type = "String";
			_UI_PERS.Name = "MIG_Foszam.UI_PERS";
			_UI_PERS.Type = "String";
			_UI_OT_ID.Name = "MIG_Foszam.UI_OT_ID";
			_UI_OT_ID.Type = "String";
			_MEMO.Name = "MIG_Foszam.MEMO";
			_MEMO.Type = "String";
			_IRSZ_PLUSS.Name = "MIG_Foszam.IRSZ_PLUSS";
			_IRSZ_PLUSS.Type = "Int32";
			_IRJ2000.Name = "MIG_Foszam.IRJ2000";
			_IRJ2000.Type = "String";
			_Conc.Name = "MIG_Foszam.CONC";
			_Conc.Type = "FTSString";
			_Selejtezve.Name = "MIG_Foszam.Selejtezve";
			_Selejtezve.Type = "Int32";
			_Selejtezes_Datuma.Name = "MIG_Foszam.Selejtezes_Datuma";
			_Selejtezes_Datuma.Type = "DateTime";
			_Csatolva_Rendszer.Name = "MIG_Foszam.Csatolva_Rendszer";
			_Csatolva_Rendszer.Type = "Int32";
			_Csatolva_Id.Name = "MIG_Foszam.Csatolva_Id";
			_Csatolva_Id.Type = "Guid";
			_MIG_Sav_Id.Name = "MIG_Foszam.MIG_Sav_Id";
			_MIG_Sav_Id.Type = "Guid";
			_MIG_Varos_Id.Name = "MIG_Foszam.MIG_Varos_Id";
			_MIG_Varos_Id.Type = "Guid";
			_MIG_IktatasTipus_Id.Name = "MIG_Foszam.MIG_IktatasTipus_Id";
			_MIG_IktatasTipus_Id.Type = "Guid";
			_MIG_Eloado_Id.Name = "MIG_Foszam.MIG_Eloado_Id";
			_MIG_Eloado_Id.Type = "Guid";
			_Edok_Ugyintezo_Csoport_Id.Name = "MIG_Foszam.Edok_Ugyintezo_Csoport_Id";
			_Edok_Ugyintezo_Csoport_Id.Type = "Guid";
			_Edok_Ugyintezo_Nev.Name = "MIG_Foszam.Edok_Ugyintezo_Nev";
			_Edok_Ugyintezo_Nev.Type = "String";
			_Edok_Utoirat_Id.Name = "MIG_Foszam.Edok_Utoirat_Id";
			_Edok_Utoirat_Id.Type = "Guid";
			_Edok_Utoirat_Azon.Name = "MIG_Foszam.Edok_Utoirat_Azon";
			_Edok_Utoirat_Azon.Type = "String";
			_UGYHOL.Name = "MIG_Foszam.UGYHOL";
			_UGYHOL.Type = "String";
			_IRATTARBA.Name = "MIG_Foszam.IRATTARBA";
			_IRATTARBA.Type = "DateTime";
			_IRATTARBA.Value = "";
			_Hatarido.Name = "MIG_Foszam.Hatarido";
			_Hatarido.Type = "DateTime";
			_Hatarido.Value = "";
			_SCONTRO.Name = "MIG_Foszam.SCONTRO";
			_SCONTRO.Type = "DateTime";
			_SCONTRO.Value = "";
			_EdokSav.Name = "MIG_Foszam.EdokSav";
			_EdokSav.Type = "String";


			// CR 3090 : Feladat mez� alapj�n t�rt�n� keres�s
			_Feladat.Name = "MIG_Foszam.Feladat";
			_Feladat.Type = "String";

			_Manual_MIG_Eloado_NAME.Name = "MIG_Eloado.NAME";
			_Manual_MIG_Eloado_NAME.Type = "String";

			_Manual_EdokSav.Name = "MIG_Foszam.EdokSav";
			_Manual_EdokSav.Type = "String";

			_Manual_UGYHOL.Name = "MIG_Foszam.UGYHOL";
			_Manual_UGYHOL.Type = "String";

			_Szervezet.Name = "MIG_Foszam.Szervezet";
			_Szervezet.Type = "String";

			_IrattarbolKikeroNev.Name = "MIG_Foszam.IrattarbolKikeroNev";
			_IrattarbolKikeroNev.Type = "String";

			//ExtendedMIG_SavSearch = new MIG_SavSearch();
			ExtendedMIG_AlszamSearch = new MIG_AlszamSearch( );
			ExtendedMIG_EloadoSearch = new MIG_EloadoSearch( );

			_Ugyirat_tipus.Name = "MIG_Foszam.Ugyirat_tipus";
			_Ugyirat_tipus.Type = "String";

			_IrattarbolKikeres_Datuma.Name = "MIG_Foszam.IrattarbolKikeres_Datuma";
			_IrattarbolKikeres_Datuma.Type = "DateTime";

            _MegorzesiIdo.Name = "MIG_Foszam.MegorzesiIdo";
            _MegorzesiIdo.Type = "DateTime";
               _IrattarId.Name = "MIG_Foszam.IrattarId";
               _IrattarId.Type = "Guid";            
               _IrattariHely.Name = "MIG_Foszam.IrattariHely";
               _IrattariHely.Type = "String";
            _Csatolmany.Name = "Csatolmanyok";
            _Csatolmany.Type = "Int32";

        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

		//private MIG_SavSearch _extendedMIG_SavSearch;

		//public MIG_SavSearch ExtendedMIG_SavSearch;
		public MIG_AlszamSearch ExtendedMIG_AlszamSearch;
		public MIG_EloadoSearch ExtendedMIG_EloadoSearch;
		public MIG_JegyzekTetelekSearch ExtendedMIG_JegyzekTetelekSearch;

		/// <summary>
		/// Id property </summary>
		private Field _Id = new Field( );

		public Field Id
		{
			get { return _Id; }
			set { _Id = value; }
		}


		/// <summary>
		/// UI_SAV property </summary>
		private Field _UI_SAV = new Field( );

		public Field UI_SAV
		{
			get { return _UI_SAV; }
			set { _UI_SAV = value; }
		}


		/// <summary>
		/// UI_YEAR property </summary>
		private Field _UI_YEAR = new Field( );

		public Field UI_YEAR
		{
			get { return _UI_YEAR; }
			set { _UI_YEAR = value; }
		}


		/// <summary>
		/// UI_NUM property </summary>
		private Field _UI_NUM = new Field( );

		public Field UI_NUM
		{
			get { return _UI_NUM; }
			set { _UI_NUM = value; }
		}


		/// <summary>
		/// UI_NAME property </summary>
		private Field _UI_NAME = new Field( );

		public Field UI_NAME
		{
			get { return _UI_NAME; }
			set { _UI_NAME = value; }
		}


		/// <summary>
		/// UI_IRSZ property </summary>
		private Field _UI_IRSZ = new Field( );

		public Field UI_IRSZ
		{
			get { return _UI_IRSZ; }
			set { _UI_IRSZ = value; }
		}


		/// <summary>
		/// UI_UTCA property </summary>
		private Field _UI_UTCA = new Field( );

		public Field UI_UTCA
		{
			get { return _UI_UTCA; }
			set { _UI_UTCA = value; }
		}


		/// <summary>
		/// UI_HSZ property </summary>
		private Field _UI_HSZ = new Field( );

		public Field UI_HSZ
		{
			get { return _UI_HSZ; }
			set { _UI_HSZ = value; }
		}


		/// <summary>
		/// UI_HRSZ property </summary>
		private Field _UI_HRSZ = new Field( );

		public Field UI_HRSZ
		{
			get { return _UI_HRSZ; }
			set { _UI_HRSZ = value; }
		}


		/// <summary>
		/// UI_TYPE property </summary>
		private Field _UI_TYPE = new Field( );

		public Field UI_TYPE
		{
			get { return _UI_TYPE; }
			set { _UI_TYPE = value; }
		}


		/// <summary>
		/// UI_IRJ property </summary>
		private Field _UI_IRJ = new Field( );

		public Field UI_IRJ
		{
			get { return _UI_IRJ; }
			set { _UI_IRJ = value; }
		}


		/// <summary>
		/// UI_PERS property </summary>
		private Field _UI_PERS = new Field( );

		public Field UI_PERS
		{
			get { return _UI_PERS; }
			set { _UI_PERS = value; }
		}


		/// <summary>
		/// UI_OT_ID property </summary>
		private Field _UI_OT_ID = new Field( );

		public Field UI_OT_ID
		{
			get { return _UI_OT_ID; }
			set { _UI_OT_ID = value; }
		}


		/// <summary>
		/// MEMO property </summary>
		private Field _MEMO = new Field( );

		public Field MEMO
		{
			get { return _MEMO; }
			set { _MEMO = value; }
		}


		/// <summary>
		/// IRSZ_PLUSS property </summary>
		private Field _IRSZ_PLUSS = new Field( );

		public Field IRSZ_PLUSS
		{
			get { return _IRSZ_PLUSS; }
			set { _IRSZ_PLUSS = value; }
		}


		/// <summary>
		/// IRJ2000 property </summary>
		private Field _IRJ2000 = new Field( );

		public Field IRJ2000
		{
			get { return _IRJ2000; }
			set { _IRJ2000 = value; }
		}


		/// <summary>
		/// Conc property </summary>
		private Field _Conc = new Field( );

		public Field Conc
		{
			get { return _Conc; }
			set { _Conc = value; }
		}


		/// <summary>
		/// Selejtezve property </summary>
		private Field _Selejtezve = new Field( );

		public Field Selejtezve
		{
			get { return _Selejtezve; }
			set { _Selejtezve = value; }
		}


		/// <summary>
		/// Selejtezes_Datuma property </summary>
		private Field _Selejtezes_Datuma = new Field( );

		public Field Selejtezes_Datuma
		{
			get { return _Selejtezes_Datuma; }
			set { _Selejtezes_Datuma = value; }
		}


		/// <summary>
		/// Csatolva_Rendszer property </summary>
		private Field _Csatolva_Rendszer = new Field( );

		public Field Csatolva_Rendszer
		{
			get { return _Csatolva_Rendszer; }
			set { _Csatolva_Rendszer = value; }
		}


		/// <summary>
		/// Csatolva_Id property </summary>
		private Field _Csatolva_Id = new Field( );

		public Field Csatolva_Id
		{
			get { return _Csatolva_Id; }
			set { _Csatolva_Id = value; }
		}


		/// <summary>
		/// MIG_Sav_Id property </summary>
		private Field _MIG_Sav_Id = new Field( );

		public Field MIG_Sav_Id
		{
			get { return _MIG_Sav_Id; }
			set { _MIG_Sav_Id = value; }
		}


		/// <summary>
		/// MIG_Varos_Id property </summary>
		private Field _MIG_Varos_Id = new Field( );

		public Field MIG_Varos_Id
		{
			get { return _MIG_Varos_Id; }
			set { _MIG_Varos_Id = value; }
		}


		/// <summary>
		/// MIG_IktatasTipus_Id property </summary>
		private Field _MIG_IktatasTipus_Id = new Field( );

		public Field MIG_IktatasTipus_Id
		{
			get { return _MIG_IktatasTipus_Id; }
			set { _MIG_IktatasTipus_Id = value; }
		}


		/// <summary>
		/// MIG_Eloado_Id property </summary>
		private Field _MIG_Eloado_Id = new Field( );

		public Field MIG_Eloado_Id
		{
			get { return _MIG_Eloado_Id; }
			set { _MIG_Eloado_Id = value; }
		}


		/// <summary>
		/// Edok_Ugyintezo_Csoport_Id property </summary>
		private Field _Edok_Ugyintezo_Csoport_Id = new Field( );

		public Field Edok_Ugyintezo_Csoport_Id
		{
			get { return _Edok_Ugyintezo_Csoport_Id; }
			set { _Edok_Ugyintezo_Csoport_Id = value; }
		}

		/// <summary>
		/// Edok_Ugyintezo_Nev property </summary>
		private Field _Edok_Ugyintezo_Nev = new Field( );

		public Field Edok_Ugyintezo_Nev
		{
			get { return _Edok_Ugyintezo_Nev; }
			set { _Edok_Ugyintezo_Nev = value; }
		}

		/// <summary>
		/// Edok_Utoirat_Id property </summary>
		private Field _Edok_Utoirat_Id = new Field( );

		public Field Edok_Utoirat_Id
		{
			get { return _Edok_Utoirat_Id; }
			set { _Edok_Utoirat_Id = value; }
		}

		/// <summary>
		/// Edok_Utoirat_Azon property </summary>
		private Field _Edok_Utoirat_Azon = new Field( );

		public Field Edok_Utoirat_Azon
		{
			get { return _Edok_Utoirat_Azon; }
			set { _Edok_Utoirat_Azon = value; }
		}

		/// <summary>
		/// UGYHOL property </summary>
		private Field _UGYHOL = new Field( );

		public Field UGYHOL
		{
			get { return _UGYHOL; }
			set { _UGYHOL = value; }
		}
		/// <summary>
		/// Hatarido property </summary>
		private Field _Hatarido = new Field( );

		public Field Hatarido
		{
			get { return _Hatarido; }
			set { _Hatarido = value; }
		}

		/// <summary>
		/// IRATTARBA property </summary>
		private Field _IRATTARBA = new Field( );

		public Field IRATTARBA
		{
			get { return _IRATTARBA; }
			set { _IRATTARBA = value; }
		}

		/// <summary>
		/// SCONTRO property </summary>
		private Field _SCONTRO = new Field( );

		public Field SCONTRO
		{
			get { return _SCONTRO; }
			set { _SCONTRO = value; }
		}

		/// <summary>
		/// EdokSav property </summary>
		private Field _EdokSav = new Field( );

		public Field EdokSav
		{
			get { return _EdokSav; }
			set { _EdokSav = value; }
		}

		// CR 3090 : Feladat mez� szerinti keres�s
		/// <summary>
		/// EdokSav property </summary>
		private Field _Feladat = new Field( );

		public Field Feladat
		{
			get { return _Feladat; }
			set { _Feladat = value; }
		}


		/// <summary>
		/// Manual_MIG_Eloado_NAME property </summary>
		private Field _Manual_MIG_Eloado_NAME = new Field( );

		public Field Manual_MIG_Eloado_NAME
		{
			get { return _Manual_MIG_Eloado_NAME; }
			set { _Manual_MIG_Eloado_NAME = value; }
		}

		/// <summary>
		/// Manual_EdokSav property </summary>
		private Field _Manual_EdokSav = new Field( );

		public Field Manual_EdokSav
		{
			get { return _Manual_EdokSav; }
			set { _Manual_EdokSav = value; }
		}

		/// <summary>
		/// Manual_UGYHOL property </summary>
		private Field _Manual_UGYHOL = new Field( );

		public Field Manual_UGYHOL
		{
			get { return _Manual_UGYHOL; }
			set { _Manual_UGYHOL = value; }
		}

		/// <summary>
		/// Szervezet property
		/// 
		/// </summary>
		private Field _Szervezet = new Field( );

		public Field Szervezet
		{
			get { return _Szervezet; }
			set { _Szervezet = value; }
		}

		/// <summary>
		/// IrattarbolKikeroNev property
		/// 
		/// </summary>
		private Field _IrattarbolKikeroNev = new Field( );

		public Field IrattarbolKikeroNev
		{
			get { return _IrattarbolKikeroNev; }
			set { _IrattarbolKikeroNev = value; }
		}
		/// <summary>
		/// Ugyirat_tipus property
		/// 
		/// </summary>
		private Field _Ugyirat_tipus = new Field( );

		public Field Ugyirat_tipus
		{
			get { return _Ugyirat_tipus; }
			set { _Ugyirat_tipus = value; }
		}

		/// <summary>
		/// IrattarbolKikeres_Datuma property
		/// 
		/// </summary>
		private Field _IrattarbolKikeres_Datuma = new Field( );

		public Field IrattarbolKikeres_Datuma
		{
			get { return _IrattarbolKikeres_Datuma; }
			set { _IrattarbolKikeres_Datuma = value; }
		}

		/// <summary>
		/// MegorzesiIdo property
		/// 
		/// </summary>
		private Field _MegorzesiIdo = new Field( );

        public Field MegorzesiIdo
        {
            get { return _MegorzesiIdo; }
            set { _MegorzesiIdo = value; }
        }

        /// <summary>
        /// IrattarId property
        /// 
        /// </summary>
        private Field _IrattarId = new Field();

        public Field IrattarId
        {
            get { return _IrattarId; }
            set { _IrattarId = value; }
        }
                          

        /// <summary>
        /// IrattariHely property
        /// 
        /// </summary>
        private Field _IrattariHely = new Field();

        public Field IrattariHely
        {
            get { return _IrattariHely; }
            set { _IrattariHely = value; }
        }

        /// <summary>
		/// Csatolm�ny property
		/// 
		/// </summary>
		private Field _Csatolmany = new Field();

        public Field Csatolmany
        {
            get { return _Csatolmany; }
            set { _Csatolmany = value; }
        }

        public string FilterFelulvizsgalat { get; set; }
    }

}