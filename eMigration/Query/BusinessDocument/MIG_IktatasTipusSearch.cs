
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    /// <summary>
    /// MIG_IktatasTipus Query.BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_IktatasTipusSearch
    {
        public MIG_IktatasTipusSearch()
        {
            _Id.Name = "MIG_IktatasTipus.Id";
            _Id.Type = "Guid";
            _EV.Name = "MIG_IktatasTipus.EV";
            _EV.Type = "Int32";
            _KOD.Name = "MIG_IktatasTipus.KOD";
            _KOD.Type = "String";
            _NAME.Name = "MIG_IktatasTipus.NAME";
            _NAME.Type = "String";
            _IRJEL.Name = "MIG_IktatasTipus.IRJEL";
            _IRJEL.Type = "String";

        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// EV property </summary>
        private Field _EV = new Field();

        public Field EV
        {
            get { return _EV; }
            set { _EV = value; }
        }


        /// <summary>
        /// KOD property </summary>
        private Field _KOD = new Field();

        public Field KOD
        {
            get { return _KOD; }
            set { _KOD = value; }
        }


        /// <summary>
        /// NAME property </summary>
        private Field _NAME = new Field();

        public Field NAME
        {
            get { return _NAME; }
            set { _NAME = value; }
        }


        /// <summary>
        /// IRJEL property </summary>
        private Field _IRJEL = new Field();

        public Field IRJEL
        {
            get { return _IRJEL; }
            set { _IRJEL = value; }
        }

    }

}