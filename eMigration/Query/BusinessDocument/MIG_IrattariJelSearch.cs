﻿using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    public class MIG_IrattariJelSearch : BaseSearchObject
    {
        public String WhereByManual = String.Empty;
        public String OrderBy = String.Empty;
        public Int32 TopRow = 0;

        private Field _EV = new Field("CTXX_DBF.EV","String");

        public Field EV
        {
            get { return _EV; }
            set { _EV = value; }
        }

        private Field _NAME = new Field("CTXX_DBF.NAME","String");

        public Field NAME
        {
            get { return _NAME; }
            set { _NAME = value; }
        }

        private Field _KOD = new Field("CTXX_DBF.KOD","String");

        public Field KOD
        {
            get { return _KOD; }
            set { _KOD = value; }
        }

        private Field _IRJEL = new Field("CTXX_DBF.IRJEL","String");

        public Field IRJEL
        {
            get { return _IRJEL; }
            set { _IRJEL = value; }
        }

        private Field _UI = new Field("CTXX_DBF.UI","String");

        public Field UI
        {
            get { return _UI; }
            set { _UI = value; }
        }

        private Field _HATOS = new Field("CTXX_DBF.HATOS","String");

        public Field HATOS
        {
            get { return _HATOS; }
            set { _HATOS = value; }
        }

        private Field _KIADM = new Field("CTXX_DBF.KIADM","String");

        public Field KIADM
        {
            get { return _KIADM; }
            set { _KIADM = value; }
        }

        private Field _IND = new Field("CTXX_DBF.IND","Int32");

        public Field IND
        {
            get { return _IND; }
            set { _IND = value; }
        }

        private Field _ERR_KOD = new Field("CTXX_DBF.ERR_KOD","Int32");

        public Field ERR_KOD
        {
            get { return _ERR_KOD; }
            set { _ERR_KOD = value; }
        }

        public MIG_IrattariJelSearch()
        {
        }
    }
}
