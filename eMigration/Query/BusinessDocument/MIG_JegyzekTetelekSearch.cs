
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    /// <summary>
    /// MIG_JegyzekTetelek Contentum.eMigration.Query.BusinessDocuments Class </summary>
    [Serializable()]
    public partial class MIG_JegyzekTetelekSearch : BaseSearchObject
    {
        public MIG_JegyzekTetelekSearch()
      {            
                     _Id.Name = "MIG_JegyzekTetelek.Id";
               _Id.Type = "Guid";            
               _Jegyzek_Id.Name = "MIG_JegyzekTetelek.Jegyzek_Id";
               _Jegyzek_Id.Type = "Guid";            
               _MIG_Foszam_Id.Name = "MIG_JegyzekTetelek.MIG_Foszam_Id";
               _MIG_Foszam_Id.Type = "Guid";            
               _UGYHOL.Name = "MIG_JegyzekTetelek.UGYHOL";
               _UGYHOL.Type = "String";            
               _AtvevoIktatoszama.Name = "MIG_JegyzekTetelek.AtvevoIktatoszama";
               _AtvevoIktatoszama.Type = "String";            
               _Megjegyzes.Name = "MIG_JegyzekTetelek.Megjegyzes";
               _Megjegyzes.Type = "String";            
               _Azonosito.Name = "MIG_JegyzekTetelek.Azonosito";
               _Azonosito.Type = "String";            
               _SztornozasDat.Name = "MIG_JegyzekTetelek.SztornozasDat";
               _SztornozasDat.Type = "DateTime";            
               _AtadasDatuma.Name = "MIG_JegyzekTetelek.AtadasDatuma";
               _AtadasDatuma.Type = "DateTime";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Jegyzek_Id property
        /// 
        /// </summary>
        private Field _Jegyzek_Id = new Field();

        public Field Jegyzek_Id
        {
            get { return _Jegyzek_Id; }
            set { _Jegyzek_Id = value; }
        }
                          
           
        /// <summary>
        /// MIG_Foszam_Id property
        /// Ez annak a valaminek a GUID -ja ("allObjGuids"-b�l)  amit csatoltunk
        /// </summary>
        private Field _MIG_Foszam_Id = new Field();

        public Field MIG_Foszam_Id
        {
            get { return _MIG_Foszam_Id; }
            set { _MIG_Foszam_Id = value; }
        }
                          
           
        /// <summary>
        /// UGYHOL property
        /// �gyirat (f�sz�m) helye a jegyz�kre helyez�s el�tt:
        /// 0 - �SZI iratt�r
        /// 1 - Skontr�
        /// 2 - Iratt�rban
        /// 3 - Oszt�lyon
        /// 4 - K�zt.Hiv
        /// 5 - B�r�s�g
        /// 6 - �gy�szs�g
        /// 7 - Vezet�s�g
        /// 8 - K�zjegyz�
        /// 9 - FPH
        /// S - Selejtezett
        /// V - Selejtez�sre v�r
        /// I - Iratt�rb�l elk�rt
        /// E - Skontr�b�l elk�rt
        /// L - Lomt�r
        /// T - Lev�lt�rban
        /// J - Jegyz�ken
        /// Z - Lez�rt jegyz�ken
        /// A - egy�b szervezetnek �tadott
        /// 
        /// </summary>
        private Field _UGYHOL = new Field();

        public Field UGYHOL
        {
            get { return _UGYHOL; }
            set { _UGYHOL = value; }
        }
                          
           
        /// <summary>
        /// AtvevoIktatoszama property
        /// 
        /// </summary>
        private Field _AtvevoIktatoszama = new Field();

        public Field AtvevoIktatoszama
        {
            get { return _AtvevoIktatoszama; }
            set { _AtvevoIktatoszama = value; }
        }
                          
           
        /// <summary>
        /// Megjegyzes property
        /// Megjegyz�s
        /// </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// Azonosito property
        /// Objektum azonos�t� sz�vegesen
        /// </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// SztornozasDat property
        /// Sztorn�z�s d�tuma (t�tel t�rl�se a jegyz�kr�l)
        /// </summary>
        private Field _SztornozasDat = new Field();

        public Field SztornozasDat
        {
            get { return _SztornozasDat; }
            set { _SztornozasDat = value; }
        }
                          
           
        /// <summary>
        /// AtadasDatuma property
        /// 
        /// </summary>
        private Field _AtadasDatuma = new Field();

        public Field AtadasDatuma
        {
            get { return _AtadasDatuma; }
            set { _AtadasDatuma = value; }
        }
                              }

}