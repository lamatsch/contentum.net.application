
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    /// <summary>
    /// MIG_Jegyzekek Contentum.eMigration.Query.BusinessDocuments Class </summary>
    [Serializable()]
    public partial class MIG_JegyzekekSearch : BaseSearchObject
    {
        public MIG_JegyzekekSearch()
      {            
                     _Id.Name = "MIG_Jegyzekek.Id";
               _Id.Type = "Guid";            
               _Tipus.Name = "MIG_Jegyzekek.Tipus";
               _Tipus.Type = "Char";            
               _Csoport_Id_Felelos.Name = "MIG_Jegyzekek.Csoport_Id_Felelos";
               _Csoport_Id_Felelos.Type = "Guid";            
               _Felelos_Nev.Name = "MIG_Jegyzekek.Felelos_Nev";
               _Felelos_Nev.Type = "String";            
               _FelhasznaloCsoport_Id_Vegrehaj.Name = "MIG_Jegyzekek.FelhasznaloCsoport_Id_Vegrehaj";
               _FelhasznaloCsoport_Id_Vegrehaj.Type = "Guid";            
               _Vegrehajto_Nev.Name = "MIG_Jegyzekek.Vegrehajto_Nev";
               _Vegrehajto_Nev.Type = "String";            
               _Atvevo_Nev.Name = "MIG_Jegyzekek.Atvevo_Nev";
               _Atvevo_Nev.Type = "String";            
               _LezarasDatuma.Name = "MIG_Jegyzekek.LezarasDatuma";
               _LezarasDatuma.Type = "DateTime";            
               _SztornozasDat.Name = "MIG_Jegyzekek.SztornozasDat";
               _SztornozasDat.Type = "DateTime";            
               _Nev.Name = "MIG_Jegyzekek.Nev";
               _Nev.Type = "String";            
               _VegrehajtasDatuma.Name = "MIG_Jegyzekek.VegrehajtasDatuma";
               _VegrehajtasDatuma.Type = "DateTime";            
               _Allapot.Name = "MIG_Jegyzekek.Allapot";
               _Allapot.Type = "String";            
               _Allapot_Nev.Name = "MIG_Jegyzekek.Allapot_Nev";
               _Allapot_Nev.Type = "String";            
               _VegrehajtasKezdoDatuma.Name = "MIG_Jegyzekek.VegrehajtasKezdoDatuma";
               _VegrehajtasKezdoDatuma.Type = "DateTime";            
               _MegsemmisitesDatuma.Name = "MIG_Jegyzekek.MegsemmisitesDatuma";
               _MegsemmisitesDatuma.Type = "DateTime";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Tipus property
        /// E - Egy�b szervezetnek �tad�si jegyz�k
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Felel�s felhaszn�l� Id
        /// </summary>
        private Field _Csoport_Id_Felelos = new Field();

        public Field Csoport_Id_Felelos
        {
            get { return _Csoport_Id_Felelos; }
            set { _Csoport_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// Felelos_Nev property
        /// Felel�s felhaszn�l� n�v
        /// </summary>
        private Field _Felelos_Nev = new Field();

        public Field Felelos_Nev
        {
            get { return _Felelos_Nev; }
            set { _Felelos_Nev = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Vegrehaj property
        /// V�grehajt� felhaszn�l�
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Vegrehaj = new Field();

        public Field FelhasznaloCsoport_Id_Vegrehaj
        {
            get { return _FelhasznaloCsoport_Id_Vegrehaj; }
            set { _FelhasznaloCsoport_Id_Vegrehaj = value; }
        }
                          
           
        /// <summary>
        /// Vegrehajto_Nev property
        /// V�grehajt� felhaszn�l� n�v
        /// </summary>
        private Field _Vegrehajto_Nev = new Field();

        public Field Vegrehajto_Nev
        {
            get { return _Vegrehajto_Nev; }
            set { _Vegrehajto_Nev = value; }
        }
                          
           
        /// <summary>
        /// Atvevo_Nev property
        /// �tvev� megnevez�se
        /// </summary>
        private Field _Atvevo_Nev = new Field();

        public Field Atvevo_Nev
        {
            get { return _Atvevo_Nev; }
            set { _Atvevo_Nev = value; }
        }
                          
           
        /// <summary>
        /// LezarasDatuma property
        /// Lez�r�s d�tuma
        /// </summary>
        private Field _LezarasDatuma = new Field();

        public Field LezarasDatuma
        {
            get { return _LezarasDatuma; }
            set { _LezarasDatuma = value; }
        }
                          
           
        /// <summary>
        /// SztornozasDat property
        /// Sztorn�z�s d�tuma
        /// </summary>
        private Field _SztornozasDat = new Field();

        public Field SztornozasDat
        {
            get { return _SztornozasDat; }
            set { _SztornozasDat = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// Jegyz�k megnevez�se
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// VegrehajtasDatuma property
        /// �tad�s id�pontja
        /// </summary>
        private Field _VegrehajtasDatuma = new Field();

        public Field VegrehajtasDatuma
        {
            get { return _VegrehajtasDatuma; }
            set { _VegrehajtasDatuma = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// 
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Allapot_Nev property
        /// 
        /// </summary>
        private Field _Allapot_Nev = new Field();

        public Field Allapot_Nev
        {
            get { return _Allapot_Nev; }
            set { _Allapot_Nev = value; }
        }
                          
           
        /// <summary>
        /// VegrehajtasKezdoDatuma property
        /// 
        /// </summary>
        private Field _VegrehajtasKezdoDatuma = new Field();

        public Field VegrehajtasKezdoDatuma
        {
            get { return _VegrehajtasKezdoDatuma; }
            set { _VegrehajtasKezdoDatuma = value; }
        }
                          
           
        /// <summary>
        /// MegsemmisitesDatuma property
        /// 
        /// </summary>
        private Field _MegsemmisitesDatuma = new Field();

        public Field MegsemmisitesDatuma
        {
            get { return _MegsemmisitesDatuma; }
            set { _MegsemmisitesDatuma = value; }
        }
                              }

}