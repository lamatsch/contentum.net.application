
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    /// <summary>
    /// MIG_META_ADAT eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class MIG_META_ADATSearch: BaseSearchObject    {
      public MIG_META_ADATSearch()
      {            
                     _Id.Name = "MIG_META_ADAT.Id";
               _Id.Type = "Guid";            
               _OBJ_ID.Name = "MIG_META_ADAT.OBJ_ID";
               _OBJ_ID.Type = "Guid";            
               _META_ID.Name = "MIG_META_ADAT.META_ID";
               _META_ID.Type = "Guid";            
               _ERTEK.Name = "MIG_META_ADAT.ERTEK";
               _ERTEK.Type = "String";            
               _Ver.Name = "MIG_META_ADAT.Ver";
               _Ver.Type = "Int32";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// OBJ_ID property
        /// MIG_Alszam.ID, ha META_ID -> TIPUS='A'-ra mutat; MIG_Foszam.ID, ha META_ID -> TIPUS='F' -ra mutat
        /// </summary>
        private Field _OBJ_ID = new Field();

        public Field OBJ_ID
        {
            get { return _OBJ_ID; }
            set { _OBJ_ID = value; }
        }
                          
           
        /// <summary>
        /// META_ID property
        /// 
        /// </summary>
        private Field _META_ID = new Field();

        public Field META_ID
        {
            get { return _META_ID; }
            set { _META_ID = value; }
        }
                          
           
        /// <summary>
        /// ERTEK property
        /// 
        /// </summary>
        private Field _ERTEK = new Field();

        public Field ERTEK
        {
            get { return _ERTEK; }
            set { _ERTEK = value; }
        }
                          
           
        /// <summary>
        /// Ver property
        /// 
        /// </summary>
        private Field _Ver = new Field();

        public Field Ver
        {
            get { return _Ver; }
            set { _Ver = value; }
        }
                              }

}