
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    /// <summary>
    /// MIG_META_TIPUS eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class MIG_META_TIPUSSearch: BaseSearchObject    {
      public MIG_META_TIPUSSearch()
      {            
                     _Id.Name = "MIG_META_TIPUS.Id";
               _Id.Type = "Guid";            
               _TIPUS.Name = "MIG_META_TIPUS.TIPUS";
               _TIPUS.Type = "String";            
               _CIMKE.Name = "MIG_META_TIPUS.CIMKE";
               _CIMKE.Type = "String";            
               _Ver.Name = "MIG_META_TIPUS.Ver";
               _Ver.Type = "Int32";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// TIPUS property
        /// A - alsz�m; F - f�sz�m
        /// </summary>
        private Field _TIPUS = new Field();

        public Field TIPUS
        {
            get { return _TIPUS; }
            set { _TIPUS = value; }
        }
                          
           
        /// <summary>
        /// CIMKE property
        /// 
        /// </summary>
        private Field _CIMKE = new Field();

        public Field CIMKE
        {
            get { return _CIMKE; }
            set { _CIMKE = value; }
        }
                          
           
        /// <summary>
        /// Ver property
        /// 
        /// </summary>
        private Field _Ver = new Field();

        public Field Ver
        {
            get { return _Ver; }
            set { _Ver = value; }
        }
                              }

}