
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    /// <summary>
    /// MIG_MigralasiHiba Query.BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_MigralasiHibaSearch
    {
        public MIG_MigralasiHibaSearch()
        {
            _Id.Name = "MIG_MigralasiHiba.Id";
            _Id.Type = "Guid";
            _ERR_KOD.Name = "MIG_MigralasiHiba.ERR_KOD";
            _ERR_KOD.Type = "Int32";
            _ERR_NAME.Name = "MIG_MigralasiHiba.ERR_NAME";
            _ERR_NAME.Type = "String";

        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// ERR_KOD property </summary>
        private Field _ERR_KOD = new Field();

        public Field ERR_KOD
        {
            get { return _ERR_KOD; }
            set { _ERR_KOD = value; }
        }


        /// <summary>
        /// ERR_NAME property </summary>
        private Field _ERR_NAME = new Field();

        public Field ERR_NAME
        {
            get { return _ERR_NAME; }
            set { _ERR_NAME = value; }
        }

    }

}