
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    /// <summary>
    /// MIG_Tobbes Query.BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_TobbesSearch
    {
        public MIG_TobbesSearch()
        {
            _Id.Name = "MIG_Tobbes.Id";
            _Id.Type = "Guid";
            _UI_SAV.Name = "MIG_Tobbes.UI_SAV";
            _UI_SAV.Type = "Int32";
            _UI_YEAR.Name = "MIG_Tobbes.UI_YEAR";
            _UI_YEAR.Type = "Int32";
            _UI_NUM.Name = "MIG_Tobbes.UI_NUM";
            _UI_NUM.Type = "Int32";
            _ALNO.Name = "MIG_Tobbes.ALNO";
            _ALNO.Type = "Int32";
            _NEV.Name = "MIG_Tobbes.NEV";
            _NEV.Type = "String";
            _IRSZ.Name = "MIG_Tobbes.IRSZ";
            _IRSZ.Type = "String";
            _UTCA.Name = "MIG_Tobbes.UTCA";
            _UTCA.Type = "String";
            _HSZ.Name = "MIG_Tobbes.HSZ";
            _HSZ.Type = "String";
            _IRSZ_PLUSS.Name = "MIG_Tobbes.IRSZ_PLUSS";
            _IRSZ_PLUSS.Type = "Int32";
            _MIG_Alszam_Id.Name = "MIG_Tobbes.MIG_Alszam_Id";
            _MIG_Alszam_Id.Type = "Guid";
            _MIG_Foszam_Id.Name = "MIG_Tobbes.MIG_Foszam_Id";
            _MIG_Foszam_Id.Type = "Guid";
            _MIG_Varos_Id.Name = "MIG_Tobbes.MIG_Varos_Id";
            _MIG_Varos_Id.Type = "Guid";


        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// UI_SAV property </summary>
        private Field _UI_SAV = new Field();

        public Field UI_SAV
        {
            get { return _UI_SAV; }
            set { _UI_SAV = value; }
        }


        /// <summary>
        /// UI_YEAR property </summary>
        private Field _UI_YEAR = new Field();

        public Field UI_YEAR
        {
            get { return _UI_YEAR; }
            set { _UI_YEAR = value; }
        }


        /// <summary>
        /// UI_NUM property </summary>
        private Field _UI_NUM = new Field();

        public Field UI_NUM
        {
            get { return _UI_NUM; }
            set { _UI_NUM = value; }
        }


        /// <summary>
        /// ALNO property </summary>
        private Field _ALNO = new Field();

        public Field ALNO
        {
            get { return _ALNO; }
            set { _ALNO = value; }
        }


        /// <summary>
        /// NEV property </summary>
        private Field _NEV = new Field();

        public Field NEV
        {
            get { return _NEV; }
            set { _NEV = value; }
        }


        /// <summary>
        /// IRSZ property </summary>
        private Field _IRSZ = new Field();

        public Field IRSZ
        {
            get { return _IRSZ; }
            set { _IRSZ = value; }
        }


        /// <summary>
        /// UTCA property </summary>
        private Field _UTCA = new Field();

        public Field UTCA
        {
            get { return _UTCA; }
            set { _UTCA = value; }
        }


        /// <summary>
        /// HSZ property </summary>
        private Field _HSZ = new Field();

        public Field HSZ
        {
            get { return _HSZ; }
            set { _HSZ = value; }
        }


        /// <summary>
        /// IRSZ_PLUSS property </summary>
        private Field _IRSZ_PLUSS = new Field();

        public Field IRSZ_PLUSS
        {
            get { return _IRSZ_PLUSS; }
            set { _IRSZ_PLUSS = value; }
        }


        /// <summary>
        /// MIG_Alszam_Id property </summary>
        private Field _MIG_Alszam_Id = new Field();

        public Field MIG_Alszam_Id
        {
            get { return _MIG_Alszam_Id; }
            set { _MIG_Alszam_Id = value; }
        }


        /// <summary>
        /// MIG_Foszam_Id property </summary>
        private Field _MIG_Foszam_Id = new Field();

        public Field MIG_Foszam_Id
        {
            get { return _MIG_Foszam_Id; }
            set { _MIG_Foszam_Id = value; }
        }


        /// <summary>
        /// MIG_Varos_Id property </summary>
        private Field _MIG_Varos_Id = new Field();

        public Field MIG_Varos_Id
        {
            get { return _MIG_Varos_Id; }
            set { _MIG_Varos_Id = value; }
        }


    }

}