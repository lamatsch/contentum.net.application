
using System;
using System.Data;
using System.Configuration;
using System.Web;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eMigration.Query.BusinessDocuments
{
    /// <summary>
    /// MIG_Varos Query.BusinessDocument Class </summary>
    [Serializable()]
    public class MIG_VarosSearch
    {
        public MIG_VarosSearch()
        {
            _Id.Name = "MIG_Varos.Id";
            _Id.Type = "Guid";
            _KOD.Name = "MIG_Varos.KOD";
            _KOD.Type = "String";
            _SKOD.Name = "MIG_Varos.SKOD";
            _SKOD.Type = "Int32";
            _NAME.Name = "MIG_Varos.NAME";
            _NAME.Type = "String";

        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// KOD property </summary>
        private Field _KOD = new Field();

        public Field KOD
        {
            get { return _KOD; }
            set { _KOD = value; }
        }


        /// <summary>
        /// SKOD property </summary>
        private Field _SKOD = new Field();

        public Field SKOD
        {
            get { return _SKOD; }
            set { _SKOD = value; }
        }


        /// <summary>
        /// NAME property </summary>
        private Field _NAME = new Field();

        public Field NAME
        {
            get { return _NAME; }
            set { _NAME = value; }
        }

    }

}