
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Text;
namespace Contentum.eMigration.Service{

    public partial class ServiceFactory
    {
        private static void SetDefaults(System.Web.Services.Protocols.SoapHttpClientProtocol service)
        {
            service.PreAuthenticate = true;
            // BUG_11753
            service.AllowAutoRedirect = true;
        }

        public MIG_AlszamService GetMIG_AlszamService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_AlszamService _Service = new MIG_AlszamService(_BusinessServiceUrl + "MIG_AlszamService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_EloadoService GetMIG_EloadoService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_EloadoService _Service = new MIG_EloadoService(_BusinessServiceUrl + "MIG_EloadoService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_FoszamService GetMIG_FoszamService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_FoszamService _Service = new MIG_FoszamService(_BusinessServiceUrl + "MIG_FoszamService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }
            
            return null;
        }

        public MIG_IktatasTipusService GetMIG_IktatasTipusService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_IktatasTipusService _Service = new MIG_IktatasTipusService(_BusinessServiceUrl + "MIG_IktatasTipusService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_MigralasiHibaService GetMIG_MigralasiHibaService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_MigralasiHibaService _Service = new MIG_MigralasiHibaService(_BusinessServiceUrl + "MIG_MigralasiHibaService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_SavService GetMIG_SavService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_SavService _Service = new MIG_SavService(_BusinessServiceUrl + "MIG_SavService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_TobbesService GetMIG_TobbesService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_TobbesService _Service = new MIG_TobbesService(_BusinessServiceUrl + "MIG_TobbesService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_VarosService GetMIG_VarosService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_VarosService _Service = new MIG_VarosService(_BusinessServiceUrl + "MIG_VarosService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
        public MIG_DokumentumService GetMIG_DokumentumService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_DokumentumService _Service = new MIG_DokumentumService(_BusinessServiceUrl + "MIG_DokumentumService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public RecordHistoryService GetRecordHistoryService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                RecordHistoryService _Service = new RecordHistoryService(_BusinessServiceUrl + "RecordHistoryService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_IrattariJelService GetMIG_IrattariJelService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_IrattariJelService _Service = new MIG_IrattariJelService(_BusinessServiceUrl + "MIG_IrattariJelService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_JegyzekekService GetMIG_JegyzekekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_JegyzekekService _Service = new MIG_JegyzekekService(_BusinessServiceUrl + "MIG_JegyzekekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_JegyzekTetelekService GetMIG_JegyzekTetelekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_JegyzekTetelekService _Service = new MIG_JegyzekTetelekService(_BusinessServiceUrl + "MIG_JegyzekTetelekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_META_ADATService GetMIG_META_ADATService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_META_ADATService _Service = new MIG_META_ADATService(_BusinessServiceUrl + "MIG_META_ADATService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_META_TIPUSService GetMIG_META_TIPUSService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_META_TIPUSService _Service = new MIG_META_TIPUSService(_BusinessServiceUrl + "MIG_META_TIPUSService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public MIG_IrattariHelyekService GetMIG_IrattariHelyekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                MIG_IrattariHelyekService _Service = new MIG_IrattariHelyekService(_BusinessServiceUrl + "MIG_IrattariHelyekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
    }
}