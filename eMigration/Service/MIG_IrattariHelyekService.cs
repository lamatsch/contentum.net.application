﻿

namespace Contentum.eMigration.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "MIG_IrattariHelyekServiceSoap", Namespace = "Contentum.eMigration.WebService")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseEREC_IrattariHelyek))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class MIG_IrattariHelyekService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        public MIG_IrattariHelyekService(string ServiceUrl)
        {
            this.Url = ServiceUrl;//ServiceUrl;
        }

        #region IrattarRendezes

        private System.Threading.SendOrPostCallback IrattarRendezesOperationCompleted;

        /// <remarks/>
        public event IrattarRendezesCompletedEventHandler IrattarRendezesCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eMigration.WebService/IrattarRendezes", RequestNamespace = "Contentum.eMigration.WebService", ResponseNamespace = "Contentum.eMigration.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result IrattarRendezes(ExecParam ExecParam, string UgyiratIds, string IrattarId, string Ertek)
        {
            object[] results = this.Invoke("IrattarRendezes", new object[] {
                        ExecParam,
                        UgyiratIds,
                        IrattarId,
                        Ertek});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginIrattarRendezes(ExecParam ExecParam, string UgyiratIds, string IrattarId, string Ertek, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("IrattarRendezes", new object[] {
                        ExecParam,
                        UgyiratIds,
                        IrattarId,
                        Ertek}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndIrattarRendezes(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void IrattarRendezesAsync(ExecParam ExecParam, string UgyiratIds, string IrattarId, string Ertek)
        {
            this.IrattarRendezesAsync(ExecParam, UgyiratIds, IrattarId, Ertek, null);
        }

        /// <remarks/>
        public void IrattarRendezesAsync(ExecParam ExecParam, string UgyiratIds, string IrattarId, string Ertek, object userState)
        {
            if ((this.IrattarRendezesOperationCompleted == null))
            {
                this.IrattarRendezesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnIrattarRendezesOperationCompleted);
            }
            this.InvokeAsync("IrattarRendezes", new object[] {
                        ExecParam,
                        UgyiratIds,
                        IrattarId,
                        Ertek}, this.IrattarRendezesOperationCompleted, userState);
        }

        private void OnIrattarRendezesOperationCompleted(object arg)
        {
            if ((this.IrattarRendezesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.IrattarRendezesCompleted(this, new IrattarRendezesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void IrattarRendezesCompletedEventHandler(object sender, IrattarRendezesCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class IrattarRendezesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal IrattarRendezesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion
    }
}
