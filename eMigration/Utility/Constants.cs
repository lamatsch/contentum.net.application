using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eMigration.BaseUtility
{
    public class Constants : Contentum.eUtility.Constants
    {
        public static class ParentForms
        {
            public const string Foszam = "Foszam";
            public const string Alszam = "Alszam";
        }

        public static class FoszamAlszamTerkepNodeTypes
        {
            public const int Foszam = 0;
            public const int Alszam = 1;
        }
    }
}
