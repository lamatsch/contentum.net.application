﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Collections;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Service;
using System.Data;
using Contentum.eMigration.Query.BusinessDocuments;

namespace Contentum.eMigration.BaseUtility
{
    [Serializable]
    public class Foszamok
    {
        [Serializable]
        public class Statusz
        {
            private string id;
            public string Id
            {
                get { return id; }
            }

            private String ui_year;
            public String UI_YEAR
            {
                get { return ui_year; }
                set { ui_year = value; }
            }

            private String ugyhol;
            public String Ugyhol
            {
                get { return ugyhol; }
                set { ugyhol = value; }
            }

            private String csatolva_Id;
            public String Csatolva_Id
            {
                get { return csatolva_Id; }
                set { csatolva_Id = value; }
            }


            private String edok_Utoirat_Id;
            public String Edok_Utoirat_Id
            {
                get { return edok_Utoirat_Id; }
                set { edok_Utoirat_Id = value; }
            }
            private String mig_eloado_id;
            public String Mig_Eloado_Id
            {
                get { return mig_eloado_id; }
                set { mig_eloado_id = value; }
            }
            private String irattarbolkikeronev;
            public String IrattarbolKikeroNev
            {
                get { return irattarbolkikeronev; }
                set { irattarbolkikeronev = value; }
            }



            private String irattarba;
            public String Irattarba
            {
                get { return irattarba; }
                set { irattarba = value; }
            }

            private String ugy_kezdete;
            public String Ugy_kezdete
            {
                get { return ugy_kezdete; }
                set { ugy_kezdete = value; }
            }

            private String scontro;
            public String Scontro
            {
                get { return scontro; }
                set { scontro = value; }
            }

            public bool IsSzerelt
            {
                get
                {
                    if (String.IsNullOrEmpty(edok_Utoirat_Id) && String.IsNullOrEmpty(csatolva_Id))
                    {
                        return false;
                    }
                    return true;
                }
            }

            public bool IsSztornozott
            {
                get
                {
                    return this.Ugyhol == Constants.MIG_IratHelye.Sztornozott;
                }
            }

            public Statusz()
            {
            }

            public Statusz(string Id, string Ugyhol, string Csatolva_Id, string Edok_Utoirat_Id, string Irattarba, string Scontro, string ui_year,string mig_eloado_id,string irattarbolkikeronev,string ugy_kezdete)
            {
                this.id = Id;
                this.Ugyhol = Ugyhol;
                this.Csatolva_Id = Csatolva_Id;
                this.Edok_Utoirat_Id = Edok_Utoirat_Id;
                this.Irattarba = Irattarba;
                this.Scontro = Scontro;
                this.UI_YEAR = ui_year;
                this.Mig_Eloado_Id = mig_eloado_id;
                this.IrattarbolKikeroNev = irattarbolkikeronev;
                this.Ugy_kezdete = ugy_kezdete;
            }
            public ErrorDetails CreateErrorDetail(string message)
            {
                return new ErrorDetails(message,"MIG_FOSZAM", this.Id, String.Empty, String.Empty);
            }            

        }

        protected static class ErrorDetailMessages
        {

            public const string UgyiratOrzojenekNemtagja = "Az ügyirat nem Önnél vagy a szervezeténél található.";
            public const string UgyiratNemUgyintezesen = "Az ügyirat helye nem ügyintézésen van.";
            public const string SzerelendoUgyiratNemUgyintezesen = "A szerelendő ügyirat helye nem ügyintézésen van.";
            public const string UgyiratNemSzereltAllapotu = "Az ügyirat nem szerelt állapotú.";
            public const string UgyiratMarSzereltAllapotu = "Az ügyiratba már van szerelve irat.";
            public const string SzerelendoNemElobbiMintAzIrat = "A szerelendő előzmény irat nem lehet későbbi mint az irat.";
            public const string SzerelendoUgyiratnakNemOnAzOrzoje = "A szerelendő ügyirat nem Önnél van.";
            // BLG_236
            public const string UgyiratNincsAzIrattarban = "Az ügyirat nincs az irattárban.";
        }
        public static Statusz GetAllapotById(String Id, Page ParentPage, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
            return GetAllapotById(Id, execParam, EErrorPanel);
        }
        
        public static Statusz GetAllapotById(String Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            MIG_Foszam mig_foszam;
            MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            execParam.Record_Id = Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                mig_foszam = (MIG_Foszam)result.Record;
                return GetAllapotByBusinessDocument(mig_foszam);
            }
            else
            {
                if (EErrorPanel != null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                }
                return null;
            }
        }

        public static Statusz GetAllapotByBusinessDocument(MIG_Foszam mig_foszam)
        {
            Statusz statusz = new Statusz(mig_foszam.Id, mig_foszam.UGYHOL, mig_foszam.Csatolva_Id
                , mig_foszam.Edok_Utoirat_Id, mig_foszam.IRATTARBA, mig_foszam.SCONTRO, mig_foszam.UI_YEAR, mig_foszam.MIG_Eloado_Id, mig_foszam.IrattarbolKikeroNev,mig_foszam.Ugy_kezdete);

            return statusz;
        }

        /// <summary>
        /// Több ügyirat állapotát adja vissza egyszerre DataSet-ből
        /// </summary>
        /// <param name="ep">execParam</param>
        /// <param name="ds">DataSet</param>
        /// <returns>HashTable Statusz elemekkel. Hivatkozni rájuk: statusz["Id"]-val lehet.</returns>
        public static Hashtable GetAllapotByDataSet(System.Data.DataSet ds)
        {
            if (ds.Tables.Count == 0) return new Hashtable(0);
            if (ds.Tables[0].Rows.Count == 0) return new Hashtable(0);
            Hashtable statusz = new Hashtable(ds.Tables[0].Rows.Count);

            try
            {
                foreach (System.Data.DataRow row in ds.Tables[0].Rows)
                {
                    statusz[row["Id"].ToString()] = GetAllapotByDataRow(row);
                }
            }
            catch
            {
                return new Hashtable(0);
            }

            return statusz;
        }

        public static Statusz GetAllapotByDataRow(System.Data.DataRow row)
        {
            Statusz statusz = new Statusz(row["Id"].ToString(),
                row["UGYHOL"].ToString(),
                row["Csatolva_Id"].ToString(),
                row["Edok_Utoirat_Id"].ToString(),
                row["IRATTARBA"].ToString(),
                row["SCONTRO"].ToString(),
                row["UI_YEAR"].ToString(),
                row["MIG_Eloado_Id"].ToString(),
                row["IrattarbolKikeroNev"].ToString(),
                row["Ugy_kezdete"].ToString()              
                );
            return statusz;

        }

        public static Statusz GetAllapotByDataRowView(DataRowView drv)
        {
            return GetAllapotByDataRow(drv.Row);
        }

        public static bool SelejtezesreKijelolheto(Statusz statusz)
        {
            if (statusz.IsSzerelt)
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.Irattarban:
                    return true;
                default:
                    return false;
            }
        }

        public static bool SelejtezesreKijelolheto(MIG_FoszamSearch search)
        {
            if (search.Csatolva_Id.Operator != Contentum.eQuery.Query.Operators.isnull)
                return false;

            if (search.Edok_Utoirat_Id.Operator != Contentum.eQuery.Query.Operators.isnull)
                return false;

            if (search.UGYHOL.Operator != Contentum.eQuery.Query.Operators.equals)
                return false;

            if (search.UGYHOL.Value != Constants.MIG_IratHelye.Irattarban)
                return false;

            return true;
        }

        public static bool SelejtezesreKijelolesVisszavonhato(Statusz statusz)
        {
            if (statusz.IsSzerelt)
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.SelejtezesreVar:
                    return true;
                default:
                    return false;
            }
        }

        public static bool SelejtezesreKijelolesVisszavonhato(MIG_FoszamSearch search)
        {
            if (search.Csatolva_Id.Operator != Contentum.eQuery.Query.Operators.isnull)
                return false;

            if (search.Edok_Utoirat_Id.Operator != Contentum.eQuery.Query.Operators.isnull)
                return false;

            if (search.UGYHOL.Operator != Contentum.eQuery.Query.Operators.equals)
                return false;

            if (search.UGYHOL.Value != Constants.MIG_IratHelye.SelejtezesreVar)
                return false;

            return true;
        }        
        public static bool Selejtezheto(Statusz statusz)
        {
            if (statusz.IsSzerelt)
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.SelejtezesreVar:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Selejtezheto(MIG_FoszamSearch search)
        {
            if (search.Csatolva_Id.Operator != Contentum.eQuery.Query.Operators.isnull)
                return false;

            if (search.Edok_Utoirat_Id.Operator != Contentum.eQuery.Query.Operators.isnull)
                return false;

            if (search.UGYHOL.Operator != Contentum.eQuery.Query.Operators.equals)
                return false;

            if (search.UGYHOL.Value != Constants.MIG_IratHelye.SelejtezesreVar)
                return false;

            return true;
        }

        public static bool SkontrobaHelyezheto(Statusz statusz)
        {
            if (statusz.IsSzerelt)
                return false;

            if(statusz.IsSztornozott)
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.Skontro:
                case Constants.MIG_IratHelye.Irattarban:
                case Constants.MIG_IratHelye.SelejtezesreVar:
                case Constants.MIG_IratHelye.Selejtezett:
                    return false;
                default:
                    return true;
            }
        }      

        public static bool SzerelhetoBeleIrat(Statusz statusz,ExecParam execParam,Page page,string FelhNev,out ErrorDetails errorDetails)
        {
            bool NalaVanAzIrat = false;
            errorDetails = null;            

            
            if (statusz.Ugyhol != Constants.MIG_IratHelye.Osztalyon)
            {
                errorDetails = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratNemUgyintezesen);
                return false;
            }
            //if (!string.IsNullOrEmpty(statusz.IrattarbolKikeroNev))
            //{
            //    if (string.Equals(statusz.IrattarbolKikeroNev, FelhNev))
            //        NalaVanAzIrat = true;                
            //}
            //else
            //{
            //    if(string.Equals(statusz.Mig_Eloado_Id,execParam.Felhasznalo_Id))
            //        NalaVanAzIrat = true;
            //}
            //if (!NalaVanAzIrat)
            //{
            //    errorDetails = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratOrzojenekNemtagja);
            //    return NalaVanAzIrat;
            //}
            return true;
        }

        public static bool SzerelhetoIrat(Statusz statusz,Statusz celStatusz, ExecParam execParam, Page page, string FelhNev, out ErrorDetails errorDetails)
        {
            bool NalaVanAzIrat = false;
            errorDetails = null;
            if (statusz.IsSzerelt)
            {
                errorDetails = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratMarSzereltAllapotu);
                return false;
            }
            if (statusz.Ugyhol != Constants.MIG_IratHelye.Osztalyon)
            {
                errorDetails = statusz.CreateErrorDetail(Foszamok.ErrorDetailMessages.SzerelendoUgyiratNemUgyintezesen);
                return false;
            }
            //if (!string.IsNullOrEmpty(statusz.IrattarbolKikeroNev))
            //{
            //    if (string.Equals(statusz.IrattarbolKikeroNev, FelhNev))
            //        NalaVanAzIrat = true;                              
            //}
            //else
            //{
            //    if(string.Equals(statusz.Mig_Eloado_Id,execParam.Felhasznalo_Id))
            //        NalaVanAzIrat = true;
            //}
            //if (!NalaVanAzIrat)
            //{
            //    errorDetails = statusz.CreateErrorDetail(Foszamok.ErrorDetailMessages.SzerelendoUgyiratnakNemOnAzOrzoje);
            //    return NalaVanAzIrat;
            //}
            if (!String.IsNullOrEmpty(statusz.Ugy_kezdete) && !String.IsNullOrEmpty(celStatusz.Ugy_kezdete))
            {
                if (DateTime.Parse(statusz.Ugy_kezdete) > DateTime.Parse(celStatusz.Ugy_kezdete))
                {
                    errorDetails = statusz.CreateErrorDetail(Foszamok.ErrorDetailMessages.SzerelendoNemElobbiMintAzIrat);
                    return false;
                }
            }
            else
            {
                if (int.Parse(statusz.UI_YEAR) > int.Parse(celStatusz.UI_YEAR))
                {
                    errorDetails = statusz.CreateErrorDetail(Foszamok.ErrorDetailMessages.SzerelendoNemElobbiMintAzIrat);
                    return false;
                }
            }
            return true;
        }

        public static bool SzerelesVisszavonhato(Statusz statusz, ExecParam execParam, Page page,string FelhNev, out ErrorDetails errorDetails)
        {
            bool NalaVanAzIrat = false;
            errorDetails = null;            
            
            if (!statusz.IsSzerelt)
            {
                errorDetails = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratNemSzereltAllapotu);
                return false;
            }
            //if (!string.IsNullOrEmpty(statusz.IrattarbolKikeroNev))
            //{
            //    if (string.Equals(statusz.IrattarbolKikeroNev,FelhNev))
            //        NalaVanAzIrat = true;
            //}
            //else
            //{
            //    if (string.Equals(statusz.Mig_Eloado_Id, execParam.Felhasznalo_Id))
            //        NalaVanAzIrat = true;
            //}
            //if (!NalaVanAzIrat)
            //{
            //    errorDetails = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratOrzojenekNemtagja);
            //    return NalaVanAzIrat;
            //}
            return true;
        }
        public static bool SkontrobolKiveheto(Statusz statusz)
        {
            if (statusz.IsSzerelt)
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.Skontro:
                //case Constants.MIG_IratHelye.SkontrobolElkert:
                    return true;
                default:
                    return false;
            }
        }

        public static bool KiadhatoOsztalyra(Statusz statusz)
        {
            if (!String.IsNullOrEmpty(statusz.Csatolva_Id))
                return false;

            if (!String.IsNullOrEmpty(statusz.Edok_Utoirat_Id) && statusz.Ugyhol != Constants.MIG_IratHelye.IrattarbolElkert
                && statusz.Ugyhol != Constants.MIG_IratHelye.SkontrobolElkert)
                return false;

            if (statusz.IsSztornozott)
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.Osztalyon:
                case Constants.MIG_IratHelye.SelejtezesreVar:
                case Constants.MIG_IratHelye.Selejtezett:
                    return false;
                default:
                    return true;
            }
        }

        public static bool IrattarbaKuldheto(Statusz statusz)
        {
            if (statusz.IsSzerelt)
                return false;

            if (statusz.IsSztornozott)
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.Irattarban:
                case Constants.MIG_IratHelye.SelejtezesreVar:
                case Constants.MIG_IratHelye.Selejtezett:
                    return false;
                default:
                    return true;
            }
        }

        public static bool IrattarbaVeheto(Statusz statusz)
        {
            if (statusz.IsSzerelt)
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.IratTarbaKuldott:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IrattarbolKikerheto(Statusz statusz)
        {
            if (statusz.IsSzerelt)
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.Irattarban:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Modosithato(Statusz statusz)
        {
            if (statusz.IsSzerelt)
                return false;

            if (statusz.IsSztornozott)
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.SelejtezesreVar:
                case Constants.MIG_IratHelye.Selejtezett:
                case Constants.MIG_IratHelye.Irattarban:
                    return false;
                default:
                    return true;
            }
        }

        public static bool LomtarbaHelyezheto(Statusz statusz)
        {
            if (!Modosithato(statusz))
                return false;

            switch (statusz.Ugyhol)
            {
                case Constants.MIG_IratHelye.Lomtar:
                    return false;
                default:
                    return true;
            }
        }

        // BLG_236
        public static bool CheckIrattarban(Page page, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

           

            // Átadható?
             bool atadasraKijelolheto = false;

            
            //List<string> irattarAllapotok = new List<string>()
            //{
                
            //    KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott,
            //    KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert,
            //    KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo,
            //    KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott,
            //    KodTarak.UGYIRAT_ALLAPOT.Skontroban,
            //    KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert
            //};


            //if (irattarAllapotok.Contains(statusz.Allapot))
            if (statusz.Ugyhol == Constants.MIG_IratHelye.Irattarban) 
            {
                atadasraKijelolheto = true;
            }
            else
            {
                //Nem megfelelő állapot
                //errorDetail = statusz.CreateErrorDetail(Resources.Error.UIUgyiratNincsIrattarban, Statusz.ColumnTypes.Allapot);
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratNincsAzIrattarban);
            }

            return atadasraKijelolheto;
        }
    }
}
