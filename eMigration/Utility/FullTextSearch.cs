using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FullTextSearch
{
    public sealed class SQLContainsCondition
    {

        private string _strCondition;
        private string _strNormalized;
        private List<string> _lstSearchTerms;

        private SQLContainsCondition() { }

        public SQLContainsCondition(string condition)
        {

            _strCondition = condition;

            ConditionParser parser = new ConditionParser(condition);

            _strNormalized = parser.RootExpression.ToString();

            _lstSearchTerms = new List<string>();

            foreach (ConditionExpression exp in parser.RootExpression)
            {
                if (exp.IsTerm && exp.Term.Length != 0)
                {
                    _lstSearchTerms.Add(exp.Term);
                }
            }

        }

        public string Condition
        {
            get { return _strCondition; }
        }

        public string Normalized
        {
            get { return _strNormalized; }
        }

        public string[] SearchTerms
        {
            get { return _lstSearchTerms.ToArray(); }
        }

        private class Token {
            private StringBuilder _sbToken;

            public Token() {
                _sbToken = new StringBuilder();
            }

            public Token(string token)
            {
                _sbToken = new StringBuilder(token);
            }

            public Token(char c): this(c.ToString()) { }

            public string Value {
                get { return _sbToken.ToString(); }
                set { _sbToken = new StringBuilder(value); }
            }

            public void Reset() {
                _sbToken = new StringBuilder();
            }

            public void Append(char c) {
                _sbToken.Append(c);
            }

            public override string ToString()
            {
                return _sbToken.ToString();
            }

        }

        private sealed class ConditionParser
        {
            private Token _token;
            private ConditionOperator _copLastOp;
            private bool _isQuoted;

            private ConditionExpression _cexpRootExpression;
            private ConditionExpression _cexpCurrentExpression;

            private ConditionParser() { }

            public ConditionParser(String condition)
            {

                ConditionStream conditionStream = new ConditionStream(condition);

                _cexpRootExpression = new ConditionExpression();
                _cexpCurrentExpression = _cexpRootExpression;

                Reset();

                while (conditionStream.Read())
                {
                    if (ConditionOperator.IsSymbol(conditionStream.CurrentChar))
                    {
                        PutToken();
                        _token.Value = conditionStream.CurrentChar.ToString();
                        PutToken();
                        continue;
                    }
                    switch (conditionStream.CurrentChar)
                    {
                        case ' ': PutToken(); continue;
                        case '(': PushExpression(); continue;
                        case ')': PopExpression(); continue;
                        case '"':
                            PutToken();
                            _isQuoted = true;
                            _token.Value = conditionStream.ReadQuoted();
                            PutToken();
                            _isQuoted = false;
                            continue;
                    }

                    _token.Append(conditionStream.CurrentChar);
                }
                PutToken();

            }

            public ConditionExpression RootExpression
            {
                get { return _cexpRootExpression; }
            }

            private void Reset()
            {
                if (_token == null)
                {
                    _token = new Token();
                }
                else
                {
                    _token.Reset();
                }
                _copLastOp = ConditionOperator.And;
            }

            private void PushExpression()
            {
                PutToken();
                _cexpCurrentExpression = _cexpCurrentExpression.AddSubexpression(_copLastOp);
            }

            private void PopExpression()
            {
                PutToken();
                if (!_cexpCurrentExpression.IsRoot)
                {
                    _cexpCurrentExpression = _cexpCurrentExpression.Parent;
                }
                Reset();
            }

            private void PutToken()
            {
                if (!_isQuoted && ConditionOperator.TryParse(_token.Value, ref _copLastOp))
                {
                    // operator
                    _token.Reset();
                }
                else
                {
                    // term
                    string term = _token.Value;
                    if (_isQuoted)
                    {
                        term = Regex.Replace(term.Trim(), @"[ ]{2,}", " ");
                    }

                    if ((term.Length == 0) && !_isQuoted) return;

                    _cexpCurrentExpression.AddTerm(_copLastOp, term);

                    Reset();
                }

            }

        }

        private sealed class ConditionStream
        {
            private string _strCondition;
            private int _iIndex;

            private ConditionStream() { }

            public ConditionStream(string condition)
            {
                _strCondition = condition;
                _iIndex = -1;
            }

            public char CurrentChar
            {
                get { return (Eos() || Bos()) ? (char)0 : _strCondition[_iIndex]; }
            }

            public bool Read()
            {
                _iIndex++;
                if (Eos()) return false;
                return true;
            }

            public string ReadQuoted()
            {
                StringBuilder sb = new StringBuilder();
                while (Read())
                {
                    if (CurrentChar.Equals('"'))
                    {
                        if ((_iIndex + 1) == _strCondition.Length)
                        {
                            _iIndex = _strCondition.Length;
                            return sb.ToString();
                        }
                        char nextChar = _strCondition[_iIndex + 1];
                        if ((nextChar == ' ') || (nextChar == ')') || (nextChar == '(') || (ConditionOperator.IsSymbol(nextChar)))
                        {
                            return sb.ToString();
                        }
                        if (nextChar == '"')
                        {
                            _iIndex++;
                        }
                    }
                    sb.Append(CurrentChar);
                }
                return sb.ToString();
            }

            private bool Bos()
            {
                return (_iIndex < 0);
            }

            private bool Eos()
            {
                return (_iIndex >= _strCondition.Length);
            }

        }

        private sealed class ConditionExpression : IEnumerable<ConditionExpression>
        {
            private int _iIndex;
            private ConditionExpression _cexpParent;
            private ConditionOperator _copOperator;
            private string _strTerm;
            private List<ConditionExpression> _lstSubexpressions;
            private bool _isTerm;
            private bool _isPhrase;
            private bool _isPrefix;

            public ConditionExpression()
            {
                _strTerm = String.Empty;
                _lstSubexpressions = new List<ConditionExpression>();
            }

            private ConditionExpression(ConditionExpression parent, ConditionOperator op)
                : this()
            {
                _iIndex = parent._lstSubexpressions.Count;
                _cexpParent = parent;
                _copOperator = op;
            }

            private ConditionExpression(ConditionExpression parent, ConditionOperator op, string term)
                : this(parent, op)
            {

                _strTerm = term;

                _isTerm = true;

                _isPhrase = (term.IndexOf(' ') != -1);
                int prefixIndex = term.IndexOf('*');
                _isPrefix = (prefixIndex != -1);

                if (!_isPrefix) return;

                if (!_isPhrase)
                {
                    if (prefixIndex == (term.Length - 1)) return;
                    _strTerm = (prefixIndex == 0) ? "" : term.Remove(prefixIndex + 1);
                    return;
                }

                term = Regex.Replace(term, @"(\*[^ ]+)|(\*)", "");
                term = Regex.Replace(term.Trim(), @"[ ]{2,}", " ");
                _strTerm = term + "*";

            }

            public ConditionExpression Parent
            {
                get { return _cexpParent; }
            }

            public bool IsRoot
            {
                get { return (_cexpParent == null); }
            }

            public bool IsLastSubexpression
            {
                get { return (IsRoot || (!IsRoot && (_iIndex == (_cexpParent._lstSubexpressions.Count - 1)))); }
            }

            public ConditionOperator Operator
            {
                get { return _copOperator; }
            }

            public bool IsTerm
            {
                get { return _isTerm; }
            }

            public bool HasSubexpressions
            {
                get { return _lstSubexpressions.Count > 0; }
            }

            public ConditionExpression LastSubexpression
            {
                get { return (HasSubexpressions) ? _lstSubexpressions[_lstSubexpressions.Count - 1] : null; }
            }

            public ConditionExpression AddSubexpression(ConditionOperator op)
            {

                ConditionOperator newOp = op;
                if (op == ConditionOperator.Near)
                {
                    newOp = ConditionOperator.And;
                }

                ConditionExpression exp = new ConditionExpression(this, newOp);

                _lstSubexpressions.Add(exp);

                return exp;

            }

            public void AddTerm(ConditionOperator op, string term)
            {
                ConditionOperator newOp = op;
                if (!HasSubexpressions)
                {
                    newOp = ConditionOperator.And;
                }
                else
                {
                    if (op == ConditionOperator.Near)
                    {
                        if (LastSubexpression.HasSubexpressions)
                        {
                            newOp = ConditionOperator.And;
                        }
                    }
                }

                ConditionExpression exp = new ConditionExpression(this, newOp, term);

                _lstSubexpressions.Add(exp);

            }

            public string Term
            {
                get { return _strTerm; }
            }

            public override string ToString()
            {

                StringBuilder sb = new StringBuilder();

                if (IsTerm)
                {
                    sb.Append(String.Format("\"{0}\"", _strTerm.Replace("\"", "\"\"")));
                }
                else
                {

                    if (!IsRoot) sb.Append("(");

                    if (!HasSubexpressions)
                    {
                        sb.Append("\"\"");  // to avoid 'Null or empty full-text predicate' exception.
                    }
                    else
                    {
                        for (int i = 0; i < _lstSubexpressions.Count; i++)
                        {
                            ConditionExpression exp = _lstSubexpressions[i];
                            if (i > 0)
                            {
                                sb.Append(String.Format(" {0} ", exp._copOperator.ToString()));
                            }
                            sb.Append(exp.ToString());
                        }
                    }

                    if (!IsRoot) sb.Append(")");

                }

                return sb.ToString();

            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public IEnumerator<ConditionExpression> GetEnumerator()
            {
                foreach (ConditionExpression subexp in _lstSubexpressions)
                {
                    yield return subexp;
                    if (subexp.HasSubexpressions)
                    {
                        foreach (ConditionExpression subsubexp in subexp)
                        {
                            yield return subsubexp;
                        }
                    }
                }
            }

        }

        private struct ConditionOperator
        {
            private const char symbolAnd1 = '&';
            private const char symbolAnd2 = '+';
            private const char symbolAnd3 = ',';
            private const char symbolAnd4 = ';';
            private const char symbolAndNot1 = '-';
            private const char symbolAndNot2 = '!';
            private const char symbolOr1 = '|';
            private const char symbolNear1 = '~';

            private const int opAnd = 0;
            private const int opAndNot = 1;
            private const int opOr = 2;
            private const int opNear = 3;

            public static ConditionOperator And = new ConditionOperator(opAnd);
            public static ConditionOperator AndNot = new ConditionOperator(opAndNot);
            public static ConditionOperator Or = new ConditionOperator(opOr);
            public static ConditionOperator Near = new ConditionOperator(opNear);

            private int _iValue;

            private ConditionOperator(int value)
            {
                _iValue = value;
            }

            public override string ToString()
            {
                switch (_iValue)
                {
                    case opAndNot: return "and not";
                    case opOr: return "or";
                    case opNear: return "near";
                    default:
                        return "and";
                }
            }

            public static bool IsSymbol(char c)
            {
                switch (c)
                {
                    case symbolAnd1: case symbolAnd2: case symbolAnd3: case symbolAnd4:
                        return true;
                    case symbolAndNot1: case symbolAndNot2:
                        return true;
                    case symbolOr1:
                        return true;
                    case symbolNear1:
                        return true;
                    default:
                        return false;
                }
                
            }

            public static bool TryParse(string s, ref ConditionOperator op)
            {

                if (s.Length == 1)
                {
                    switch (s[0])
                    {
                        case symbolAnd1: case symbolAnd2: case symbolAnd3: case symbolAnd4:
                            op = ConditionOperator.And;
                            return true;
                        case symbolAndNot1:
                            op = ConditionOperator.AndNot;
                            return true;
                        case symbolAndNot2:
                            if (op != ConditionOperator.And) return false;
                            op = ConditionOperator.AndNot;
                            return true;
                        case symbolOr1:
                            op = ConditionOperator.Or;
                            return true;
                        case symbolNear1:
                            op = ConditionOperator.Near;
                            return true;
                        default:
                            return false;
                    }
                    
                }

                if (s.Equals(ConditionOperator.And.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    op = ConditionOperator.And;
                    return true;
                }
                if (s.Equals("not", StringComparison.OrdinalIgnoreCase) && (op == ConditionOperator.And))
                {
                    op = ConditionOperator.AndNot;
                    return true;
                }
                if (s.Equals(ConditionOperator.Or.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    op = ConditionOperator.Or;
                    return true;
                }
                if (s.Equals(ConditionOperator.Near.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    op = ConditionOperator.Near;
                    return true;
                }

                return false;

            }
            // operator overloads
            public static bool operator ==(ConditionOperator cop1, ConditionOperator cop2)
            {
                return cop1.Equals(cop2);
            }
            public static bool operator !=(ConditionOperator cop1, ConditionOperator cop2)
            {
                return !cop1.Equals(cop2);
            }
            public override bool Equals(object obj)
            {
                return (obj is ConditionOperator) && (Equals((ConditionOperator)obj));
            }
            private bool Equals(ConditionOperator cop)
            {
                return (_iValue == cop._iValue);
            }
            public override int GetHashCode()
            {
                return _iValue.GetHashCode();
            }

        }

    }

}
