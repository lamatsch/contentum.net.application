using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Contentum.eMigration.BaseUtility
{
    public class IratHelye
    {
        public static readonly ListItem Skontro = new ListItem("Skontr�", Constants.MIG_IratHelye.Skontro);
        public static readonly ListItem Irattarban = new ListItem("Iratt�rban", Constants.MIG_IratHelye.Irattarban);
        public static readonly ListItem Ugyintezesen = new ListItem("�gyint�z�sen", Constants.MIG_IratHelye.Osztalyon);
        public static readonly ListItem Osztalyon = new ListItem("Oszt�lyon", Constants.MIG_IratHelye.Osztalyon);
        public static readonly ListItem KoztHiv = new ListItem("K�zt. Hiv.", Constants.MIG_IratHelye.KoztHiv);
        public static readonly ListItem Birosag = new ListItem("B�r�s�g", Constants.MIG_IratHelye.Birosag);
        public static readonly ListItem Ugyeszseg = new ListItem("�gy�szs�g", Constants.MIG_IratHelye.Ugyeszseg);
        public static readonly ListItem Vezetoseg = new ListItem("Vezet�s�g", Constants.MIG_IratHelye.Vezetoseg);
        public static readonly ListItem Kozjegyzonel = new ListItem("K�zjegyz�n�l", Constants.MIG_IratHelye.Kozjegyzonel);
        public static readonly ListItem FopolgarmesteriHivatal = new ListItem("F�polg�rmesteri Hiv.", Constants.MIG_IratHelye.FopolgarmesteriHivatal);
        public static readonly ListItem SelejtezesreVar = new ListItem("Selejtez�sre v�r", Constants.MIG_IratHelye.SelejtezesreVar);
        public static readonly ListItem Selejtezett = new ListItem("Selejtezett", Constants.MIG_IratHelye.Selejtezett);
        public static readonly ListItem IrattarbolElkert = new ListItem("Iratt�rb�l elk�rt", Constants.MIG_IratHelye.IrattarbolElkert);
        public static readonly ListItem IrattarbaKuldott = new ListItem("Iratt�rba k�ld�tt", Constants.MIG_IratHelye.IratTarbaKuldott);
        public static readonly ListItem SkontrobolElkert = new ListItem("Skontr�b�l elk�rt", Constants.MIG_IratHelye.SkontrobolElkert);
        public static readonly ListItem Lomtar = new ListItem("Lomt�r", Constants.MIG_IratHelye.Lomtar);
		public static readonly ListItem Sztornozott = new ListItem("Sztorn�zott", Constants.MIG_IratHelye.Sztornozott);
		
        public static readonly ListItem Leveltar = new ListItem("Lev�lt�rba adott", Constants.MIG_IratHelye.Leveltar);
        public static readonly ListItem Jegyzek = new ListItem("Jegyz�kre helyezett", Constants.MIG_IratHelye.Jegyzek);
        public static readonly ListItem LezartJegyzek = new ListItem(" Lez�rt jegyz�kben l�v�", Constants.MIG_IratHelye.LezartJegyzek);
        public static readonly ListItem EgyebSzervezetnekAtadott = new ListItem("Egy�b szervezetnek �tadott", Constants.MIG_IratHelye.EgyebSzervezetnekAtadott);
        public static readonly ListItem Szerelt = new ListItem("Szerelt", Constants.MIG_IratHelye.Szerelt);

    }
}
