using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eMigration.BaseUtility
{
    public class eMigrationService
    {
        public static Contentum.eMigration.Service.ServiceFactory ServiceFactory
        {
            get { return GeteMigrationServiceFactory(); }
        }

        private static Contentum.eMigration.Service.ServiceFactory GeteMigrationServiceFactory()
        {
            Contentum.eMigration.Service.ServiceFactory sc = new Contentum.eMigration.Service.ServiceFactory();
            sc.BusinessServiceType = UI.GetAppSetting("eMigrationBusinessServiceType");
            sc.BusinessServiceUrl = UI.GetAppSetting("eMigrationBusinessServiceUrl");
            sc.BusinessServiceAuthentication = UI.GetAppSetting("eMigrationBusinessServiceAuthentication");
            sc.BusinessServiceUserDomain = UI.GetAppSetting("eMigrationBusinessServiceUserDomain");
            sc.BusinessServiceUserName = UI.GetAppSetting("eMigrationBusinessServiceUserName");
            sc.BusinessServicePassword = UI.GetAppSetting("eMigrationBusinessServicePassword");
            return sc;
        }
    }
}
