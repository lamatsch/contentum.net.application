<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="AlszamForm.aspx.cs" Inherits="AlszamForm" Title="R�gi adatok (alsz�m) karbantart�sa" EnableEventValidation="false" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="eMigrationComponent/AlszamFormTab.ascx" TagName="AlszamFormTab"
    TagPrefix="tp1" %>
<%@ Register Src="eMigrationComponent/FoszamAlszamTerkepTab.ascx" TagName="FoszamAlszamTerkepTab"
    TagPrefix="tp11" %>
<%@ Register Src="eMigrationComponent/AlszamCsatolmanyokTab.ascx" TagName="AlszamCsatolmanyokTab"
    TagPrefix="tp111" %>
<%--BLG_632--%>
<%@ Register Src="eMigrationComponent/FoszamAlszamEgyebAdatTab.ascx" TagName="FoszamAlszamEgyebAdatTab"
    TagPrefix="tp12" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,MIG_AlszamFormHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <asp:Panel ID="AlszamFormPanel" runat="server">
        <ajaxToolkit:TabContainer ID="AlszamTabContainer" runat="server" Width="100%"
            OnActiveTabChanged="AlszamTabContainer_ActiveTabChanged" OnClientActiveTabChanged="ActiveTabChanged"
            ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="TabAlszamPanel" runat="server" TabIndex="0">
                <HeaderTemplate>
                    <asp:Label ID="TabAlszamPanelHeader" runat="server" Text="Alsz�m" />
                </HeaderTemplate>
                <ContentTemplate>
                    <%-- Irat --%>
                    <tp1:AlszamFormTab ID="AlszamFormTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="TabFoszamAlszamTerkepPanel" runat="server" TabIndex="1">
                <HeaderTemplate>
                    <asp:Label ID="Label1" runat="server" Text="F�sz�m-alsz�m t�rk�p" />
                </HeaderTemplate>
                <ContentTemplate>
                    <tp11:FoszamAlszamTerkepTab ID="FoszamAlszamTerkepTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="TabAlszamCsatolmanyokPanel" runat="server" TabIndex="1">
                <HeaderTemplate>
                    <asp:Label ID="Label2" runat="server" Text="Csatolm�nyok" />
                </HeaderTemplate>
                <ContentTemplate>
                    <tp111:AlszamCsatolmanyokTab ID="AlszamCsatolmanyokTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
             <%--BLG_632--%>
             <ajaxToolkit:TabPanel ID="TabEgyebAdatokPanel" runat="server" TabIndex="2">
                <HeaderTemplate>
                    <asp:Label ID="Label3" runat="server" Text="Egy�b adatok" />
                </HeaderTemplate>
                <ContentTemplate>
                    <tp12:FoszamAlszamEgyebAdatTab ID="FoszamAlszamEgyebAdatTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
    </asp:Panel>
    <uc2:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>
