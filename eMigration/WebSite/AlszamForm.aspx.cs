using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Service;

public partial class AlszamForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";

    private bool FoszamAlszamTerkepMode = false;

    private MIG_Alszam obj_MIG_Alszam = null; 

    protected void Page_Init(object sender, EventArgs e)
    {
        JavaScripts.RegisterActiveTabChangedClientScript(Page, AlszamTabContainer);
        ScriptManager1.RegisterAsyncPostBackControl(AlszamTabContainer);
               
        Command = Request.QueryString.Get(CommandName.Command);

        string mode = Request.QueryString.Get(QueryStringVars.Mode);
        if (mode == Constants.UgyiratTerkep)
        {
            FoszamAlszamTerkepMode = true;
        }

        // FormHeader be�ll�t�sa az AlszamFormTab1 -nak:
        AlszamFormTab1.FormHeader = FormHeader1;

        // BLG_632
        FoszamAlszamEgyebAdatTab1.Active = false;
        FoszamAlszamEgyebAdatTab1.ParentForm = Constants.ParentForms.Foszam;

        if (Command != CommandName.View)
        {
            FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
        }

        // New tiltva, csak iktat�ssal j�het l�tre �j �gyirat, ez meg nem az...
        if (Command == CommandName.New)
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

        SetEnabledAllTabsByFunctionRights();
      
        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                AlszamFormPanel.Visible = false;
            }
            else
            {
                // Rekord lek�r�se jogosults�gellen�rz�ssel egy�tt

                if (!IsPostBack)
                {
                    obj_MIG_Alszam = CheckRights(id);                                 
                }

                #region Felesleges tabf�lek elt�ntet�se

                if (FoszamAlszamTerkepMode == true)
                {
                    // Csak az �gyiratt�rk�p-panelt jelen�tj�k meg?
                    SetAllTabsInvisible();

                    FoszamAlszamTerkepTab1.Visible = true;
                    TabFoszamAlszamTerkepPanel.Enabled = true;
                }

                #endregion

            }
        }

        AlszamFormTab1.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);
        FoszamAlszamTerkepTab1.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);

        AlszamFormTab1.Active = true;
        AlszamFormTab1.ParentForm = Constants.ParentForms.Alszam;

        FoszamAlszamTerkepTab1.Active = false;
        FoszamAlszamTerkepTab1.ParentForm = Constants.ParentForms.Alszam;

        if (FoszamAlszamTerkepMode == true)
        {
            AlszamFormTab1.Active = false;

            FoszamAlszamTerkepTab1.Active = true;
        }        
    }

   
    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        if (!IsPostBack)
        {
            string selectedTab = Request.QueryString.Get(QueryStringVars.SelectedTab);
            // Default panel beallitasa:
            if (FoszamAlszamTerkepMode == true)
            {
                selectedTab = "FoszamAlszamTerkep";
            }
            if (!String.IsNullOrEmpty(selectedTab))
            {
                bool selectedTabFound = false;
                foreach(AjaxControlToolkit.TabPanel tab in AlszamTabContainer.Tabs)
                {
                    if (tab.ID.ToLower().Contains(selectedTab.ToLower()))
                    {
                        selectedTabFound = true;
                        AlszamTabContainer.ActiveTab = tab;
                        break;
                    }
                }

                if (!selectedTabFound)
                {
                    AlszamTabContainer.ActiveTab = TabAlszamPanel;
                }
            }
            else
            {
                AlszamTabContainer.ActiveTab = TabAlszamPanel;
            }

            ActiveTabRefresh(AlszamTabContainer);
        }

        // Atverjuk a formfooter-t, mert ugy kell mukodni-e, mint ha nezet lenne, csak a Bezar gombra van szukseg
        FormFooter1.Command = CommandName.View;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }


    /// <summary>
    /// Ha v�ltozott valamilyen tulajdons�ga az �gyiratnak, jogosults�gellen�rz�s
    /// </summary>    
    void CallBack_OnChangedObjectProperties(object sender, EventArgs e)
    {
        string id = Request.QueryString.Get(QueryStringVars.Id);
        CheckRights(id);
    }


    private MIG_Alszam CheckRights(string id)
    {
        // TODO: jogosults�gellen�rz�s?
        MIG_Alszam mig_Alszam = null;

        MIG_AlszamService service = eMigrationService.ServiceFactory.GetMIG_AlszamService();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;
        Result result = service.Get(execParam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            AlszamFormPanel.Visible = false;
            return null;
        }
        else
        {
            mig_Alszam = (MIG_Alszam)result.Record;
        }

        return mig_Alszam;
    }



    #region Forms Tab
                   
    protected void AlszamTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
        AlszamFormTab1.Active = false;
        FoszamAlszamTerkepTab1.Active = false;

        if (FoszamAlszamTerkepMode == true)
        {
            // Csak az �gyiratt�rk�p l�tsz�dik:
            FoszamAlszamTerkepTab1.Active = true;
            FoszamAlszamTerkepTab1.ReLoadTab();
            return;
        }
        else
        {

            if (AlszamTabContainer.ActiveTab.Equals(TabAlszamPanel))
            {
                AlszamFormTab1.Active = true;

                // a m�r esetleg lek�rt �gyirat objektum �tad�sa
                AlszamFormTab1.obj_MIG_Alszam = this.obj_MIG_Alszam;

                AlszamFormTab1.ReLoadTab();
            }
            else if (AlszamTabContainer.ActiveTab.Equals(TabFoszamAlszamTerkepPanel))
            {
                FoszamAlszamTerkepTab1.Active = true;
                FoszamAlszamTerkepTab1.ReLoadTab();
            }
            // BLG_632
            else if (AlszamTabContainer.ActiveTab.Equals(TabEgyebAdatokPanel))
            {
                FoszamAlszamEgyebAdatTab1.Active = true;
                FoszamAlszamEgyebAdatTab1.ReLoadTab();
            }

        }
    }

    #endregion


    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    //// (Itt nem kell)
    //private void SetEnabledAllTabs(Boolean value)
    //{

    //}

    private void SetAllTabsInvisible()
    {
        AlszamFormTab1.Visible = false;
        TabAlszamPanel.Enabled = false;

        FoszamAlszamTerkepTab1.Visible = false;
        TabFoszamAlszamTerkepPanel.Enabled = false;

        AlszamCsatolmanyokTab1.Visible = false;
        TabAlszamCsatolmanyokPanel.Enabled = false;

        // BLG_632
        FoszamAlszamEgyebAdatTab1.Visible = false;
        TabEgyebAdatokPanel.Enabled = false;

    }


    private void SetEnabledAllTabsByFunctionRights()
    {
        // a default panelt nem kell ellenorizni, azt majd ott helyben
        TabAlszamPanel.Enabled = true;

        // TODO: Funkci�jogosults�gok!!
        TabFoszamAlszamTerkepPanel.Enabled = true;

        // BLG_632
        TabEgyebAdatokPanel.Enabled = true;
    }

}
