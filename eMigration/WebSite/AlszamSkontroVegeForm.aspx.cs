using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;

public partial class MIG_AlszamSkontroVegeForm : System.Web.UI.Page
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private string GetSkontroVegeDatum()
    {
        DateTime now = DateTime.Now;
        DateTime end;
        end = now.AddMonths(1);
        if (now.Day >= 15)
        {
            end = end.AddDays(15 - end.Day);
        }
        else
        {
            end = end.AddDays(-end.Day);
        }

        return end.ToString();
    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        CalendarSkontro.ReadOnly = true;
        labelSkontro.CssClass = "mrUrlapInputWaterMarked";

    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
    }

    /// <summary>
    /// A leg�rd�l� list�ban kiv�lasztott �rt�knek megfelel� panel aktiv�l�sa
    /// </summary>
    private void SetActivePanel()
    {

    }


    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Cim" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.HiddenFieldId);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }

            CalendarSkontro.Text = GetSkontroVegeDatum();
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }

        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }

        registerJavascripts();

    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            //pageView.SetViewOnPage();
        }

    }


    private void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(CalendarSkontro);

            FormFooter1.SaveEnabled = false;
        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            switch (Command)
            {
                case CommandName.Modify:
                    {
                        String HiddenFieldId = Request.QueryString.Get(QueryStringVars.HiddenFieldId);
                        if (String.IsNullOrEmpty(HiddenFieldId))
                        {
                            // nincs Id megadva:
                            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(CalendarSkontro.Text))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_56008);
                            }
                            else if ((DateTime.Parse(CalendarSkontro.Text)) <= DateTime.Today.AddDays(1.0))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_56009);
                            }
                            else
                            {
                                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                                dictionary.Add(HiddenFieldId, CalendarSkontro.Text);

                                JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, dictionary, "ReturnValuesToParentWindowTipus", false);
                                JavaScripts.RegisterCloseWindowClientScript(Page, true);
                            }
                        }
                        break;
                    }
            }
        }
    }

    protected void registerJavascripts()
    {
    }

}
