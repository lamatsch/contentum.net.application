﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using AjaxControlToolkit;

/// <summary>
/// Summary description for CustomMaskEditExtender
/// </summary>
namespace Contentum.eUIControls
{
    public class CustomMaskEditExtender : AjaxControlToolkit.MaskedEditExtender
    {
        public CustomMaskEditExtender()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        [DefaultValue("")]
        [ExtenderControlProperty]
        [RefreshProperties(RefreshProperties.All)]
        public string CultureName
        {
            get
            {
                return GetPropertyValue<string>("CultureName", "");
            }
            set
            {
                System.Globalization.CultureInfo ControlCulture = null;
                if (String.IsNullOrEmpty(value))
                {
                    ControlCulture = System.Globalization.CultureInfo.CurrentCulture;
                    OverridePageCulture = false;
                }
                else
                {
                    ControlCulture = System.Globalization.CultureInfo.GetCultureInfo(value);
                    //If the Culturename is ACTUALLY set in the control, use it
                    OverridePageCulture = true;
                }
                SetPropertyValue<string>("CultureName", ControlCulture.Name);
                CultureDatePlaceholder = ControlCulture.DateTimeFormat.DateSeparator;
                CultureTimePlaceholder = ControlCulture.DateTimeFormat.TimeSeparator;
                CultureDecimalPlaceholder = ControlCulture.NumberFormat.NumberDecimalSeparator;
                CultureThousandsPlaceholder = ControlCulture.NumberFormat.NumberGroupSeparator;
                //string sep = ControlCulture.DateTimeFormat.DateSeparator;
                string sep = ".";
                string[] arrDate = ControlCulture.DateTimeFormat.ShortDatePattern.Split(new string[] { sep }, StringSplitOptions.RemoveEmptyEntries);
                string ret = arrDate[0].Substring(0, 1).ToUpper(ControlCulture);
                ret += arrDate[1].Substring(0, 1).ToUpper(ControlCulture);
                ret += arrDate[2].Substring(0, 1).ToUpper(ControlCulture);
                CultureDateFormat = ret;
                CultureCurrencySymbolPlaceholder = ControlCulture.NumberFormat.CurrencySymbol;
                if (String.IsNullOrEmpty(ControlCulture.DateTimeFormat.AMDesignator + ControlCulture.DateTimeFormat.PMDesignator))
                {
                    CultureAMPMPlaceholder = "";
                }
                else
                {
                    CultureAMPMPlaceholder = ControlCulture.DateTimeFormat.AMDesignator + ";" + ControlCulture.DateTimeFormat.PMDesignator;
                }
            }
        }

        internal bool OverridePageCulture
        {
            get
            {
                return GetPropertyValue<bool>("OverridePageCulture", false);
            }
            set
            {
                SetPropertyValue<bool>("OverridePageCulture", value);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (String.IsNullOrEmpty(CultureName))
            {
                CultureName = "";
            }
            base.OnPreRender(e);
        }

    }
}