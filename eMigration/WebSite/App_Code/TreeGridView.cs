﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.ComponentModel;
using System.Drawing.Design;
using System.Collections;
using System.Collections.Specialized;

/// <summary>
/// Summary description for TreeGridView
/// </summary>
namespace Contentum.eUIControls
{
    public class TreeGridView : TreeView, IPostBackEventHandler
    {
        private static readonly object EventRowCommand = new object();
        private static readonly object EventRowCreated = new object();
        private static readonly object EventSorted = new object();
        private static readonly object EventSorting = new object();
        private static readonly object EventRowSelect = new object();

        private string _defaultSortExpression = String.Empty;

        public string DefaultSortExpression
        {
            get { return _defaultSortExpression; }
            set { _defaultSortExpression = value; }
        }

        private SortDirection _defaultSortDirection = SortDirection.Ascending;

        public SortDirection DefaultSortDirection
        {
            get { return _defaultSortDirection; }
            set { _defaultSortDirection = value; }
        } 

        private const char EventArgumentDelimeter = '$';

        public string GetSortEventArgument(string SortExpression)
        {
            if (String.IsNullOrEmpty(SortExpression)) return String.Empty;
            return GetEventArgument(DataControlCommands.SortCommandName, SortExpression); ;
        }

        public string GetEventArgument(string commandName,string commandArgument)
        {
            if (String.IsNullOrEmpty(commandName)) return String.Empty;

            return commandName + EventArgumentDelimeter + commandArgument;
        }

        public CommandEventArgs GetCommandEventArgs(string eventArgument)
        {
            if (String.IsNullOrEmpty(eventArgument)) return (CommandEventArgs)CommandEventArgs.Empty;

            if (eventArgument.StartsWith("s"))
            {
                return new CommandEventArgs(DataControlCommands.SelectCommandName, eventArgument.Remove(0, 1));
            }

            string[] eventArgumentParts = eventArgument.Split(EventArgumentDelimeter);
            string commandName = eventArgumentParts[0];
            string commandArgument = String.Empty;
            if (eventArgumentParts.Length > 1)
                commandArgument = eventArgumentParts[1];

            return new CommandEventArgs(commandName, commandArgument);

        
        }

        private DataControlFieldCollection _fieldCollection;


        [DefaultValue(null),
        MergableProperty(false),
        PersistenceMode(PersistenceMode.InnerProperty)]
        public DataControlFieldCollection Columns
        {
            get
            {
                if (_fieldCollection == null)
                {
                    _fieldCollection = new DataControlFieldCollection();
                    _fieldCollection.FieldsChanged += new EventHandler(OnFieldsChanged);
                    //if (IsTrackingViewState)
                    //    ((IStateManager)_fieldCollection).TrackViewState();
                    ((IStateManager)_fieldCollection).TrackViewState();
                }
                return _fieldCollection;
            }
        }

        [
        DefaultValue(false)
        ]
        public virtual bool ShowFooter
        {
            get
            {
                object o = ViewState["ShowFooter"];
                if (o != null)
                    return (bool)o;
                return false;
            }
            set
            {
                bool oldValue = ShowFooter;
                if (value != oldValue)
                {
                    ViewState["ShowFooter"] = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        [
        DefaultValue(true)
        ]
        public virtual bool ShowHeader
        {
            get
            {
                object o = ViewState["ShowHeader"];
                if (o != null)
                    return (bool)o;
                return true;
            }
            set
            {
                bool oldValue = ShowHeader;
                if (value != oldValue)
                {
                    ViewState["ShowHeader"] = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        public enum TreeGridViewModes
        {
            Default,
            GridView,
            TreeView
        }

        [
        DefaultValue(TreeGridViewModes.Default)
        ]
        public virtual TreeGridViewModes Mode
        {
            get
            {
                object o = ViewState["Mode"];
                if (o != null)
                    return (TreeGridViewModes)o;
                return TreeGridViewModes.Default;
            }
            set
            {
                if (value != Mode)
                {
                    ViewState["Mode"] = value;
                }
            }
        }

        [DefaultValue(false)]
        public virtual bool AllowSorting
        {
            get
            {
                object o = ViewState["AllowSorting"];
                if (o != null)
                    return (bool)o;
                return false;
            }
            set
            {
                bool oldValue = AllowSorting;
                if (value != oldValue)
                {
                    ViewState["AllowSorting"] = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        [
        Browsable(false),
        DefaultValue(SortDirection.Ascending),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        PersistenceMode(PersistenceMode.InnerProperty)
        ]
        public virtual SortDirection SortDirection
        {
            get
            {
                return SortDirectionInternal;

            }
        }

        private SortDirection SortDirectionInternal
        {
            get
            {
                object o = ViewState["SortDirectionInternal"];
                if (o != null)
                    return (SortDirection)o;
                return _defaultSortDirection;
            }
            set
            {
                if (value < SortDirection.Ascending || value > SortDirection.Descending)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                SortDirection oldValue = SortDirectionInternal;
                if (oldValue != value)
                {
                    ViewState["SortDirectionInternal"] = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        }

        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)
        ]
        public virtual string SortExpression
        {
            get
            {
                return SortExpressionInternal;
            }
        }

        private string SortExpressionInternal
        {
            get
            {
                object o = ViewState["SortExpressionInternal"];
                if (o != null)
                    return (string)o;
                return _defaultSortExpression;
            }
            set
            {
                string oldValue = SortExpressionInternal;
                if (oldValue != value)
                {
                    ViewState["SortExpressionInternal"] = value;
                    if (Initialized)
                    {
                        RequiresDataBinding = true;
                    }
                }
            }
        } 

        public event EventHandler Sorted
        {
            add
            {
                Events.AddHandler(EventSorted, value);
            }
            remove
            {
                Events.RemoveHandler(EventSorted, value);
            }
        }

        public event GridViewSortEventHandler Sorting
        {
            add
            {
                Events.AddHandler(EventSorting, value);
            }
            remove
            {
                Events.RemoveHandler(EventSorting, value);
            }
        }

        public event GridViewCommandEventHandler RowCommand
        {
            add
            {
                Events.AddHandler(EventRowCommand, value);
            }
            remove
            {
                Events.RemoveHandler(EventRowCommand, value);
            }
        }

        public event GridViewRowEventHandler RowCreated
        {
            add
            {
                Events.AddHandler(EventRowCreated, value);
            }
            remove
            {
                Events.RemoveHandler(EventRowCreated, value);
            }
        }

        public event GridViewSelectEventHandler RowSelecting
        {
            add
            {
                Events.AddHandler(EventRowSelect, value);
            }
            remove
            {
                Events.RemoveHandler(EventRowSelect, value);
            }
        }

        public TreeGridView()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (Mode == TreeGridViewModes.GridView)
            {
                this.ShowLines = false;
                this.Style.Add(HtmlTextWriterStyle.MarginLeft, "-20px");
            }
            base.Render(writer);
        }

        protected override void LoadViewState(object savedState)
        {
            if (savedState != null)
            {
                object[] myState = (object[])savedState;

                base.LoadViewState(myState[0]);

                if (myState[1] != null)
                    ((IStateManager)Columns).LoadViewState(myState[1]);
            }
            else
            {
                base.LoadViewState(null);
            }
        }

        /// <internalonly/>
        /// <devdoc>
        /// <para>Saves the current state of the <see cref='System.Web.UI.WebControls.GridView'/>.</para> 
        /// </devdoc>
        protected override object SaveViewState()
        {
            object baseState = base.SaveViewState();
            object fieldState = (_fieldCollection != null) ? ((IStateManager)_fieldCollection).SaveViewState() : null;

            object[] myState = new object[2];
            myState[0] = baseState;
            myState[1] = fieldState;

            // note that we always have some state, atleast the RowCount
            return myState;
        }

        private void OnFieldsChanged(object sender, EventArgs e)
        {
            if (Initialized)
            {
                RequiresDataBinding = true;
            }
        }

        protected override void RaisePostBackEvent(string eventArgument)
        {
            base.RaisePostBackEvent(eventArgument);

            CommandEventArgs cea = GetCommandEventArgs(eventArgument);
            GridViewCommandEventArgs gvcea = new GridViewCommandEventArgs(null, this, cea);
            HandleEvent(gvcea, false, String.Empty);
        }

        private bool HandleEvent(EventArgs e, bool causesValidation, string validationGroup)
        {
            bool handled = false;

            if (causesValidation)
            {
                Page.Validate(validationGroup);
            }

            GridViewCommandEventArgs dce = e as GridViewCommandEventArgs;

            if (dce != null)
            {

                OnRowCommand(dce);
                handled = true;

                string command = dce.CommandName;

                if(String.Compare(command, DataControlCommands.SortCommandName,true) == 0)
                {
                    HandleSort((string)dce.CommandArgument);
                }

                if (String.Compare(command, DataControlCommands.SelectCommandName, true) == 0)
                {
                    HandleSelect((string)dce.CommandArgument);
                }
            }

            return handled;
        }

        private void HandleSelect(string selectedNodeValuePath)
        {
            if (String.IsNullOrEmpty(selectedNodeValuePath)) return;

            TreeNode selectedNode = this.FindNode(selectedNodeValuePath.Replace("\\","/"));

            int newSelect = 0;

            if (selectedNode != SelectedNode)
            {
                newSelect = 1;
            }

            GridViewSelectEventArgs e = new GridViewSelectEventArgs(newSelect);
            OnRowSelecting(e);

            if(e.Cancel)
            {
                return;
            }

            if (selectedNode != SelectedNode)
            {
                selectedNode.Select();
                OnSelectedNodeChanged(EventArgs.Empty);
            }
        }

        protected virtual void OnRowCommand(GridViewCommandEventArgs e)
        {
            GridViewCommandEventHandler handler = (GridViewCommandEventHandler)Events[EventRowCommand];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void HandleSort(string sortExpression)
        {
            if (!AllowSorting)
            {
                return;
            }

            SortDirection futureSortDirection = SortDirection.Ascending;

            if ((SortExpressionInternal == sortExpression) && (SortDirectionInternal == SortDirection.Ascending))
            {
                // switch direction 
                futureSortDirection = SortDirection.Descending;
            }

            HandleSort(sortExpression, futureSortDirection);
        }

        private void HandleSort(string sortExpression, SortDirection sortDirection)
        {
            GridViewSortEventArgs e = new GridViewSortEventArgs(sortExpression, sortDirection);
            OnSorting(e);

            if (e.Cancel)
            {
                return;
            }

            SortExpressionInternal = e.SortExpression;
            SortDirectionInternal = e.SortDirection;

            OnSorted(EventArgs.Empty);
            RequiresDataBinding = true;
        }

        protected virtual void OnSorting(GridViewSortEventArgs e)
        {
            GridViewSortEventHandler handler = (GridViewSortEventHandler)Events[EventSorting];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void OnSorted(EventArgs e)
        {
            EventHandler handler = (EventHandler)Events[EventSorted];
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public virtual void Sort(string sortExpression, SortDirection sortDirection)
        {
            HandleSort(sortExpression, sortDirection);
        }

        protected virtual void OnRowSelecting(GridViewSelectEventArgs e)
        {
            GridViewSelectEventHandler handler = (GridViewSelectEventHandler)Events[EventRowSelect];
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
