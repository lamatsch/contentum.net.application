using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
//using Contentum.eQuery.BusinessDocuments;
//using Contentum.eQuery;


/// <summary>
/// Local Utility NameSpace
/// </summary>
// &&
namespace Contentum.eMigration.Utility
{
	public class XmlFunction : Contentum.eMigration.BaseUtility.XmlFunction
	{
	}
	public class FileFunctions
	{
		public static byte[ ] GetFile ( String FilePath )
		{
			System.IO.FileStream fs = null;
			byte[ ] file_buffer = null;
			try
			{
				fs = new System.IO.FileStream ( FilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read );
			}
			catch ( System.IO.FileNotFoundException e )
			{
				throw e;
			}

			try
			{
				file_buffer = new byte[ fs.Length ];

				fs.Read ( file_buffer, 0, ( int )fs.Length );

				fs.Flush ( );
				fs.Close ( );
			}
			catch ( System.IO.FileLoadException e )
			{
				throw e;
			}

			return file_buffer;
		}

		public static string GetFileIconNameByFormatum ( string formatum )
		{
			if ( string.IsNullOrEmpty ( formatum ) )
			{
				return "default.gif";
			}

			switch ( formatum.ToLower ( ) )
			{
				case "xls":
				case "xlsx":
					return "xls.gif";
				case "gif":
				case "bmp":
					return "gif.gif";
				case "tiff":
				case "png":
				case "jpg":
					return "jpg.gif";
				case "pdf":
					return "pdf.gif";
				case "txt":
					return "txt.gif";
				case "doc":
				case "docx":
				case "docm":
				case "dot":
				case "dotx":
				case "dotm":
					return "doc.gif";
				case "ppt":
				case "pptx":
					return "ppt.gif";
				case "zip":
					return "zip.gif";
				default:
					return "default.gif";
			}
		}

	}


	public class Search : Contentum.eMigration.BaseUtility.Search
	{
		private static string GetSessionName_IktatoKonyvekSearchByGroup ( Page page )
		{
			return "SetIktatokonyvekSearch_" + FelhasznaloProfil.FelhasznaloSzerverzetId ( page );
		}

		///// <summary>
		///// A felhaszn�l� iktat�k�nyvekhez val� jogosults�ga, illetve beleiktat�si joga alapj�n gener�lja a s�v sz�r�st.
		///// Hiba eset�n null �rt�kkel t�r vissza.
		///// </summary>
		///// <param name="page"></param>
		///// <param name="errorPanel"></param>
		///// <returns></returns>
		//public static string GetIktatoKonyvekSearchWhere(Page page, Contentum.eUIControls.eErrorPanel errorPanel)
		//{
		//    return GetIktatoKonyvekSearchWhere(page, "", errorPanel);
		//}

		//private const string ImpossibleWhere = " 0=1 ";
		///// <summary>
		///// A felhaszn�l� iktat�k�nyvekhez val� jogosults�ga, illetve beleiktat�si joga alapj�n gener�lja a s�v sz�r�st.
		///// Ha a SavFilterben szerepel �rt�k, akkor csak az annak megfelel� s�vsz�r�st adja vissza, felt�ve, hogy a
		///// felhaszn�l�nak van joga a s�vhoz, ha nincs, lehetetlen felt�telt gener�l.
		///// Hiba eset�n lehetetlen felt�telt gener�l.
		///// </summary>
		///// <param name="page"></param>
		///// <param name="SavFilter">S�v (pontos) megad�sa, amennyiben egy konkr�t s�vra keres�nk. Ilyenkor s�vra vonatkoz� a sz�r�si felt�tel egyszer�s�dik.</param>
		///// <param name="errorPanel"></param>
		///// <returns></returns>
		//public static string GetIktatoKonyvekSearchWhere(Page page, string savFilter, Contentum.eUIControls.eErrorPanel errorPanel)
		//{
		//    string Where = null;

		//    string SessionNameByGroup = GetSessionName_IktatoKonyvekSearchByGroup(page);

		//    if (String.IsNullOrEmpty(savFilter) && page.Session[SessionNameByGroup] != null)
		//    {
		//        Where = page.Session[SessionNameByGroup].ToString();
		//    }
		//    else
		//    {
		//        EREC_IraIktatoKonyvekService iktatokonyvekService = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
		//        EREC_IraIktatoKonyvekSearch iktatokonyvekSearch = new EREC_IraIktatoKonyvekSearch();
		//        iktatokonyvekSearch.IktatoErkezteto.Value = "I";
		//        iktatokonyvekSearch.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
		//        ExecParam xpm = UI.SetExecParamDefault(page);

		//        if (!String.IsNullOrEmpty(savFilter))
		//        {
		//            iktatokonyvekSearch.Iktatohely.Value = String.Concat("*", savFilter);
		//            iktatokonyvekSearch.Iktatohely.Operator = Contentum.eQuery.Query.Operators.like;
		//        }

		//        Result iktatokonyvekResult = iktatokonyvekService.GetAllWithExtensionAndJogosultsag(xpm, iktatokonyvekSearch, true);
		//        if (iktatokonyvekResult.IsError)
		//        {
		//            Contentum.eUtility.ResultError.DisplayResultErrorOnErrorPanel(errorPanel, iktatokonyvekResult);
		//            return ImpossibleWhere;
		//        }

		//        Result iktatokonyvekResult_iktathat = iktatokonyvekService.GetAllWithIktathat(xpm, iktatokonyvekSearch);
		//        if (iktatokonyvekResult_iktathat.IsError)
		//        {
		//            Contentum.eUtility.ResultError.DisplayResultErrorOnErrorPanel(errorPanel, iktatokonyvekResult_iktathat);
		//            return ImpossibleWhere;
		//        }

		//        List<string> savList = new List<string>();
		//        if (iktatokonyvekResult.Ds.Tables[0].Rows.Count == 0 && iktatokonyvekResult_iktathat.Ds.Tables[0].Rows.Count == 0)
		//        {
		//            Where = ImpossibleWhere;
		//        }
		//        else
		//        {
		//            // a jogosultak �s az iktat�sra enged�lyezettek �sszef�s�l�se
		//            //List<string> savList = new List<string>();
		//            foreach (DataRow r in iktatokonyvekResult.Ds.Tables[0].Rows)
		//            {
		//                string sav = r["Iktatohely"].ToString().TrimStart("FPH".ToCharArray());
		//                if (string.IsNullOrEmpty(sav)) continue;

		//                if (sav.StartsWith("0") && sav.ToString().Length > 2)
		//                    sav = sav.Substring(1, 2);

		//                if (!String.IsNullOrEmpty(savFilter) && sav != savFilter) continue;

		//                if (!savList.Contains(sav))
		//                {
		//                    //Where += "PATINDEX('(" + sav + ")%',MIG_Sav.NAME) != 0 OR ";
		//                    savList.Add(sav);
		//                }
		//            }

		//            foreach (DataRow r in iktatokonyvekResult_iktathat.Ds.Tables[0].Rows)
		//            {
		//                string sav = r["Iktatohely"].ToString().TrimStart("FPH".ToCharArray());
		//                if (string.IsNullOrEmpty(sav)) continue;

		//                if (sav.StartsWith("0") && sav.ToString().Length > 2)
		//                    sav = sav.Substring(1, 2);

		//                if (!String.IsNullOrEmpty(savFilter) && sav != savFilter) continue;

		//                if (!savList.Contains(sav))
		//                {
		//                    //Where += "PATINDEX('(" + sav + ")%',MIG_Sav.NAME) != 0 OR ";
		//                    savList.Add(sav);
		//                }
		//            }
		//        }

		//        if (savList.Count > 0)
		//        //if (!String.IsNullOrEmpty(Where))
		//        {
		//            //Where = Where.TrimEnd(" OR ".ToCharArray());
		//            Where = String.Format("MIG_Foszam.EdokSav in ('{0}')", String.Join("','", savList.ToArray()));

		//            // ha nem volt speci�lis sz�r�s, akkor elmentj�k a sessionbe
		//            if (String.IsNullOrEmpty(savFilter))
		//            {
		//                page.Session[SessionNameByGroup] = Where;
		//            }
		//        }
		//        else if (!String.IsNullOrEmpty(savFilter))
		//        {
		//            Where = ImpossibleWhere;
		//        }
		//    }
		//    return Where;
		//}

		///// <summary>
		///// A felhaszn�l� iktat�k�nyvekhez val� jogosults�ga, illetve beleiktat�si joga alapj�n gener�lja a s�v sz�r�st.
		///// Ha a SavFilterben szerepel �rt�k, akkor csak az annak megfelel� s�vsz�r�st adja vissza, felt�ve, hogy a
		///// felhaszn�l�nak van joga a s�vhoz, ha nincs, lehetetlen felt�telt gener�l.
		///// Hiba eset�n lehetetlen felt�telt gener�l.
		///// </summary>
		///// <param name="page"></param>
		///// <param name="search">Az a keres�si objektum, melynek s�vra vonatkoz� sz�r�s�t be kell �ll�tani.</param>
		///// <param name="errorPanel"></param>
		///// <returns></returns>
		//public static void SetIktatokonyvekSearch(Page page, MIG_FoszamSearch search, Contentum.eUIControls.eErrorPanel errorPanel)
		//{
		//    string SessionNameByGroup = GetSessionName_IktatoKonyvekSearchByGroup(page);

		//    if (page.Session[SessionNameByGroup] == null)
		//    {
		//        GetIktatoKonyvekSearchWhere(page, errorPanel);
		//    }

		//    //search.ExtendedMIG_SavSearch.WhereByManual = (page.Session[SessionNameByGroup] == null ? ImpossibleWhere : page.Session[SessionNameByGroup].ToString());
		//    search.WhereByManual = (page.Session[SessionNameByGroup] == null ? ImpossibleWhere : page.Session[SessionNameByGroup].ToString());
		//}

		/// <summary>
		/// A felhaszn�l� iktat�k�nyvekhez val� jogosults�ga, illetve beleiktat�si joga alapj�n gener�lja a s�v sz�r�st.
		/// Hiba eset�n null �rt�kkel t�r vissza.
		/// </summary>
		/// <param name="page"></param>
		/// <param name="errorPanel"></param>
		/// <returns></returns>
		public static Field GetIktatoKonyvekSearchWhere ( Page page, Contentum.eUIControls.eErrorPanel errorPanel )
		{
			return GetIktatoKonyvekSearchWhere ( page, "", errorPanel );
		}

		private const string ImpossibleSavFieldValue = "---";
		private static Field SetImpossibleSavField ( Field SavField )
		{
			SavField.Value = ImpossibleSavFieldValue;
			SavField.Operator = Contentum.eQuery.Query.Operators.equals;

			return SavField;
		}

		/// <summary>
		/// A felhaszn�l� iktat�k�nyvekhez val� jogosults�ga, illetve beleiktat�si joga alapj�n gener�lja a s�v sz�r�st.
		/// Ha a SavFilterben szerepel �rt�k, akkor csak az annak megfelel� s�vsz�r�st adja vissza, felt�ve, hogy a
		/// felhaszn�l�nak van joga a s�vhoz, ha nincs, lehetetlen felt�telt gener�l.
		/// Hiba eset�n lehetetlen felt�telt gener�l.
		/// </summary>
		/// <param name="page"></param>
		/// <param name="SavFilter">S�v (pontos) megad�sa, amennyiben egy konkr�t s�vra keres�nk. Ilyenkor s�vra vonatkoz� a sz�r�si felt�tel egyszer�s�dik.</param>
		/// <param name="errorPanel"></param>
		/// <returns></returns>
		public static Field GetIktatoKonyvekSearchWhere ( Page page, string savFilter, Contentum.eUIControls.eErrorPanel errorPanel )
		{
			Field SavFieldCalculated = new Field ( );

			string SessionNameByGroup = GetSessionName_IktatoKonyvekSearchByGroup ( page );

			if ( String.IsNullOrEmpty ( savFilter ) && page.Session[ SessionNameByGroup ] != null )
			{
				SavFieldCalculated = ( Field )page.Session[ SessionNameByGroup ];
			}
			else
			{
				EREC_IraIktatoKonyvekService iktatokonyvekService = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService ( );
				EREC_IraIktatoKonyvekSearch iktatokonyvekSearch = new EREC_IraIktatoKonyvekSearch ( );
				iktatokonyvekSearch.IktatoErkezteto.Value = "I";
				iktatokonyvekSearch.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
				ExecParam xpm = UI.SetExecParamDefault ( page );

				if ( !String.IsNullOrEmpty ( savFilter ) )
				{
					iktatokonyvekSearch.Iktatohely.Value = String.Concat ( "*", savFilter );
					iktatokonyvekSearch.Iktatohely.Operator = Contentum.eQuery.Query.Operators.like;
				}

				Result iktatokonyvekResult = iktatokonyvekService.GetAllWithExtensionAndJogosultsag ( xpm, iktatokonyvekSearch, true );
				if ( iktatokonyvekResult.IsError )
				{
					Contentum.eUtility.ResultError.DisplayResultErrorOnErrorPanel ( errorPanel, iktatokonyvekResult );
					return SetImpossibleSavField ( SavFieldCalculated );
				}

				Result iktatokonyvekResult_iktathat = iktatokonyvekService.GetAllWithIktathat ( xpm, iktatokonyvekSearch );
				if ( iktatokonyvekResult_iktathat.IsError )
				{
					Contentum.eUtility.ResultError.DisplayResultErrorOnErrorPanel ( errorPanel, iktatokonyvekResult_iktathat );
					return SetImpossibleSavField ( SavFieldCalculated );
				}

				List<string> savList = new List<string> ( );
				if ( iktatokonyvekResult.Ds.Tables[ 0 ].Rows.Count == 0 && iktatokonyvekResult_iktathat.Ds.Tables[ 0 ].Rows.Count == 0 )
				{
					//Where = ImpossibleWhere;
					SetImpossibleSavField ( SavFieldCalculated );
				}
				else
				{
					// a jogosultak �s az iktat�sra enged�lyezettek �sszef�s�l�se
					//List<string> savList = new List<string>();
					foreach ( DataRow r in iktatokonyvekResult.Ds.Tables[ 0 ].Rows )
					{
                        string sav = TrimStart(r["Iktatohely"].ToString(), "FPH");
						if ( string.IsNullOrEmpty ( sav ) ) continue;

						if ( sav.StartsWith ( "0" ) && sav.ToString ( ).Length > 2 )
							sav = sav.Substring ( 1, 2 );

						if ( !String.IsNullOrEmpty ( savFilter ) && sav != savFilter ) continue;

						if ( !savList.Contains ( sav ) )
						{
							//Where += "PATINDEX('(" + sav + ")%',MIG_Sav.NAME) != 0 OR ";
							savList.Add ( sav );
						}
					}

					foreach ( DataRow r in iktatokonyvekResult_iktathat.Ds.Tables[ 0 ].Rows )
					{
                        string sav = TrimStart(r["Iktatohely"].ToString(), "FPH");
                        if ( string.IsNullOrEmpty ( sav ) ) continue;

						if ( sav.StartsWith ( "0" ) && sav.ToString ( ).Length > 2 )
							sav = sav.Substring ( 1, 2 );

						if ( !String.IsNullOrEmpty ( savFilter ) && sav != savFilter ) continue;

						if ( !savList.Contains ( sav ) )
						{
							//Where += "PATINDEX('(" + sav + ")%',MIG_Sav.NAME) != 0 OR ";
							savList.Add ( sav );
						}
					}
				}

				if ( savList.Count > 0 )
				//if (!String.IsNullOrEmpty(Where))
				{
					//Where = Where.TrimEnd(" OR ".ToCharArray());
					//Where = String.Format("MIG_Foszam.EdokSav in ('{0}')", String.Join("','", savList.ToArray()));
					SavFieldCalculated.Value = Search.GetSqlInnerString ( savList.ToArray ( ) );
					SavFieldCalculated.Operator = Contentum.eQuery.Query.Operators.inner;

					// ha nem volt speci�lis sz�r�s, akkor elmentj�k a sessionbe
					if ( String.IsNullOrEmpty ( savFilter ) )
					{
						page.Session[ SessionNameByGroup ] = SavFieldCalculated;
					}
				}
				else if ( !String.IsNullOrEmpty ( savFilter ) )
				{
					SetImpossibleSavField ( SavFieldCalculated );
				}
			}
			return SavFieldCalculated;
		}

		/// <summary>
		/// A felhaszn�l� iktat�k�nyvekhez val� jogosults�ga, illetve beleiktat�si joga alapj�n gener�lja a s�v sz�r�st.
		/// Ha a SavFilterben szerepel �rt�k, akkor csak az annak megfelel� s�vsz�r�st adja vissza, felt�ve, hogy a
		/// felhaszn�l�nak van joga a s�vhoz, ha nincs, lehetetlen felt�telt gener�l.
		/// Hiba eset�n lehetetlen felt�telt gener�l.
		/// </summary>
		/// <param name="page"></param>
		/// <param name="search">Az a keres�si objektum, melynek s�vra vonatkoz� sz�r�s�t be kell �ll�tani.</param>
		/// <param name="errorPanel"></param>
		/// <returns></returns>
		public static void SetIktatokonyvekSearch ( Page page, MIG_FoszamSearch search, Contentum.eUIControls.eErrorPanel errorPanel )
		{
			string SessionNameByGroup = GetSessionName_IktatoKonyvekSearchByGroup ( page );

			if ( page.Session[ SessionNameByGroup ] == null )
			{
				GetIktatoKonyvekSearchWhere ( page, errorPanel );
			}

			//search.ExtendedMIG_SavSearch.WhereByManual = (page.Session[SessionNameByGroup] == null ? ImpossibleWhere : page.Session[SessionNameByGroup].ToString());
			Field SavFieldCalculated = ( page.Session[ SessionNameByGroup ] == null ? SetImpossibleSavField ( new Field ( ) ) : ( Field )page.Session[ SessionNameByGroup ] );

			search.Manual_EdokSav.Value = SavFieldCalculated.Value;
			search.Manual_EdokSav.Operator = SavFieldCalculated.Operator;
		}

		/// <summary>
		/// A felhaszn�l� iktat�k�nyvekhez val� jogosults�ga, illetve beleiktat�si joga alapj�n gener�lja a s�v sz�r�st.
		/// Ha a SavFilterben szerepel �rt�k, akkor csak az annak megfelel� s�vsz�r�st adja vissza, felt�ve, hogy a
		/// felhaszn�l�nak van joga a s�vhoz, ha nincs, lehetetlen felt�telt gener�l.
		/// Hiba eset�n lehetetlen felt�telt gener�l.
		/// </summary>
		/// <param name="page"></param>
		/// <param name="search">Az a keres�si objektum, melynek s�vra vonatkoz� sz�r�s�t be kell �ll�tani.</param>
		/// <param name="SavFilter">S�v (pontos) megad�sa, amennyiben egy konkr�t s�vra keres�nk. Ilyenkor s�vra vonatkoz� a sz�r�si felt�tel egyszer�s�dik.</param>
		/// <param name="errorPanel"></param>
		/// <returns></returns>
		public static void SetIktatokonyvekSearch ( Page page, MIG_FoszamSearch search, string savFilter, Contentum.eUIControls.eErrorPanel errorPanel )
		{
			Field SavFieldCalculated = null;

			if ( String.IsNullOrEmpty ( savFilter ) )
			{
				string SessionNameByGroup = GetSessionName_IktatoKonyvekSearchByGroup ( page );

				if ( page.Session[ SessionNameByGroup ] == null )
				{
					GetIktatoKonyvekSearchWhere ( page, errorPanel );
				}

				//search.ExtendedMIG_SavSearch.WhereByManual = (page.Session[SessionNameByGroup] == null ? ImpossibleWhere : page.Session[SessionNameByGroup].ToString());

				SavFieldCalculated = ( page.Session[ SessionNameByGroup ] == null ? SetImpossibleSavField ( new Field ( ) ) : ( Field )page.Session[ SessionNameByGroup ] );
			}
			else
			{
				SavFieldCalculated = GetIktatoKonyvekSearchWhere ( page, savFilter, errorPanel );
			}
			search.Manual_EdokSav.Value = SavFieldCalculated.Value;
			search.Manual_EdokSav.Operator = SavFieldCalculated.Operator;
		}


        //    public static void SetIktatokonyvekSearch(Page page,MIG_FoszamSearch search, Contentum.eUIControls.eErrorPanel errorPanel)
        //    {
        //        if (page.Session["SetIktatokonyvekSearch"] == null)
        //        {
        //            EREC_IraIktatoKonyvekService iktatokonyvekService = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        //            EREC_IraIktatoKonyvekSearch iktatokonyvekSearch = new EREC_IraIktatoKonyvekSearch();
        //            iktatokonyvekSearch.IktatoErkezteto.Value = "I";
        //            iktatokonyvekSearch.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
        //            ExecParam xpm = UI.SetExecParamDefault(page);

        //            Result iktatokonyvekResult = iktatokonyvekService.GetAllWithExtensionAndJogosultsag(xpm, iktatokonyvekSearch, true);
        //            if (!string.IsNullOrEmpty(iktatokonyvekResult.ErrorCode))
        //            {
        //                Contentum.eUtility.ResultError.DisplayResultErrorOnErrorPanel(errorPanel, iktatokonyvekResult);
        //                return;
        //            }

        //            Result iktatokonyvekResult_iktathat = iktatokonyvekService.GetAllWithIktathat(xpm, iktatokonyvekSearch);
        //            if (!string.IsNullOrEmpty(iktatokonyvekResult_iktathat.ErrorCode))
        //            {
        //                Contentum.eUtility.ResultError.DisplayResultErrorOnErrorPanel(errorPanel, iktatokonyvekResult_iktathat);
        //                return;
        //            }

        //            search.ExtendedMIG_SavSearch.WhereByManual = "";

        //            if (iktatokonyvekResult.Ds.Tables[0].Rows.Count == 0 && iktatokonyvekResult_iktathat.Ds.Tables[0].Rows.Count == 0)
        //            {
        //                search.ExtendedMIG_SavSearch.WhereByManual = " 0=1 ";
        //            }

        //            // a jogosultak �s az iktat�sra enged�lyezettek �sszef�s�l�se
        //            List<string> savList = new List<string>();
        //            foreach (DataRow r in iktatokonyvekResult.Ds.Tables[0].Rows)
        //            {
        //                string sav = r["Iktatohely"].ToString().TrimStart("FPH".ToCharArray());
        //                if (string.IsNullOrEmpty(sav)) continue;

        //                if (sav.StartsWith("0") && sav.ToString().Length > 2)
        //                    sav = sav.Substring(1, 2);

        //                if (!savList.Contains(sav))
        //                {
        //                    search.ExtendedMIG_SavSearch.WhereByManual += " PATINDEX('(" + sav + ")%',MIG_Sav.NAME) != 0 OR ";
        //                    savList.Add(sav);
        //                }
        //            }

        //            foreach (DataRow r in iktatokonyvekResult_iktathat.Ds.Tables[0].Rows)
        //            {
        //                string sav = r["Iktatohely"].ToString().TrimStart("FPH".ToCharArray());
        //                if (string.IsNullOrEmpty(sav)) continue;

        //                if (sav.StartsWith("0") && sav.ToString().Length > 2)
        //                    sav = sav.Substring(1, 2);

        //                if (!savList.Contains(sav))
        //                {
        //                    search.ExtendedMIG_SavSearch.WhereByManual += " PATINDEX('(" + sav + ")%',MIG_Sav.NAME) != 0 OR ";
        //                    savList.Add(sav);
        //                }
        //            }

        //            page.Session["SetIktatokonyvekSearch"] = search.ExtendedMIG_SavSearch.WhereByManual.TrimEnd(" OR ".ToCharArray());
        //        }

        //        search.ExtendedMIG_SavSearch.WhereByManual = page.Session["SetIktatokonyvekSearch"].ToString();
        //    }

        static string TrimStart(string target, string trimString)
        {
            if (String.IsNullOrEmpty(trimString)) return target;

            string result = target;
            if (result.StartsWith(trimString))
            {
                result = result.Substring(trimString.Length);
            }

            return result;
        }

        public static MIG_FoszamSearch GetFelulvizsgalatDefaultSearch()
        {
            MIG_FoszamSearch search = new MIG_FoszamSearch();

            //szereletek ne j�jjenek alap�rtelmezetten
            search.Csatolva_Id.AndGroup("111");
            search.Csatolva_Id.IsNull();
            //--------------------------------------------------------
            search.Edok_Utoirat_Id.AndGroup("111");
            search.Edok_Utoirat_Id.IsNull();

            return search;
        }
    }

	public class eMigrationService : Contentum.eMigration.BaseUtility.eMigrationService
	{
	}

	public class UI : Contentum.eMigration.BaseUtility.UI
	{
        public static void SetGridViewColumnVisiblity(GridView gridView, string dataField, bool visible)
        {
            BoundField column = GetGridViewColumn(gridView, dataField);

            if (column != null)
            {
                column.Visible = visible;
            }
        }

        public static BoundField GetGridViewColumn(GridView gridView, string dataFiled)
        {
            for (int i = 0; i < gridView.Columns.Count; i++)
            {
                DataControlField field = gridView.Columns[i];

                BoundField boundfield = field as BoundField;

                if (boundfield != null && boundfield.DataField == dataFiled)
                {
                    return boundfield;
                }
            }

            return null;
        }
    }

	public class UIMezoObjektumErtekek : Contentum.eMigration.BaseUtility.UIMezoObjektumErtekek
	{
	}

	public class Log : Contentum.eMigration.BaseUtility.Log
	{
	}

	public class Authentication : Contentum.eMigration.BaseUtility.Authentication
	{
	}

	public class FelhasznaloProfil : Contentum.eMigration.BaseUtility.FelhasznaloProfil
	{
	}

	public class FunctionRights : Contentum.eMigration.BaseUtility.FunctionRights
	{
	}

	public class LockManager : Contentum.eMigration.BaseUtility.LockManager
	{
	}

	public class JavaScripts : Contentum.eMigration.BaseUtility.JavaScripts
	{
		public static string SetOnClientClickIFramePrintForm ( string url )
		{
			string javascript = "";
			javascript =
				 "if(document.getElementById(\"downloadDiv\") == null)" +
				 "{  var downloadDiv = document.createElement(\"div\");" +
				 "   document.getElementsByTagName(\"body\")[0].appendChild(downloadDiv);" +
				 "   downloadDiv.style.width = \"0px\";" +
				 "   downloadDiv.style.height = \"0px\";" +
				 "   downloadDiv.id = \"downloadDiv\"; }" +
				 "else var downloadDiv = document.getElementById(\"downloadDiv\");" +
				 "if(document.getElementById(\"downloadFrame\") == null)" +
				 "{  var downloadFrame = document.createElement(\"iframe\");" +
				 "   downloadFrame.id = \"downloadFrame\";" +
				 "   downloadFrame.src = '" + url + "';" +
				 "   downloadFrame.scrolling = \"no\";" +
				 "   downloadFrame.style.width = \"0px\";" +
				 "   downloadFrame.style.height = \"0px\";" +
				 "   downloadDiv.innerHTML = downloadFrame.outerHTML; }" +
				 "else { var downloadFrame = document.getElementById(\"downloadFrame\");" +
				 "       downloadFrame.src = '" + url + "'; }";
			return javascript;
		}
	}

	public class DefaultDates : Contentum.eMigration.BaseUtility.DefaultDates
	{
	}


    public class CommandName : Contentum.eMigration.BaseUtility.CommandName
    {
        public const string LomtarbaHelyezes = "LomtarbaHelyezes";
        public const string SzerelesVisszavonas = "SzerelesVisszavonas";
        // BLG_236
        public const string Irattarbarendezes = "Irattarbarendezes";

        //LZS_BLG_8097
        public const string AiAtadas = "AiAtadas";

        public const string Atadas = "Atadas";
    }

	public class EventArgumentConst : Contentum.eMigration.BaseUtility.EventArgumentConst
	{
	}

	public class Constants : Contentum.eMigration.BaseUtility.Constants
	{
		public static class Startup
		{
			public const string SearchForm = "SearchForm";
            public const string StartupName = "Startup";

            // Fel�lvizsg�lat
            public const string SelejtezesSearchForm = "SelejtezesSearchForm";
			public const string LeveltarbaAdasSearchForm = "LeveltarbaAdasSearchForm";
			public const string EgyebSzervezetnekAtadasSearchForm = "EgyebSzervezetnekAtadasSearchForm";

			public const string FromSelejtezes = "FromSelejtezes";
			public const string FromLeveltarbaAdas = "FromLeveltarbaAdas";
			public const string FromEgyebSzervezetnekAtadas = "FromEgyebSzervezetnekAtadas";
			public const string FromFelulvizsgalat = "FromFelulvizsgalat";
			public const string FromJegyzekTetel = "FromJegyzekTetel";
            public const string FromUgyirat = "FromUgyirat";

        }

		public static class CustomSearchObjectSessionNames
		{
			public const string MigFelulvizsgalatSearch = "MigFelulvizsgalatSearch";
		}

        public const string SelectedJegyzekTetelIds = "SelectedJegyzekTetelIds";
    }


	/// <summary>
	/// URL-ekben el�fordul� param�ternevek
	/// </summary>
	public class QueryStringVars : Contentum.eMigration.BaseUtility.QueryStringVars
	{
		public const string Ev = "Ev";
		public const string IsEvFilterDisabled = "IsEvFilterDisabled";
		public const string FoszamGridViewClientID = "FoszamGridViewClientID";
		public const string Azonosito = "Azonosito";
	}

	public class Defaults : Contentum.eMigration.BaseUtility.Defaults
	{
	}


	public class ResultError : Contentum.eMigration.BaseUtility.ResultError
	{
	}

	public class Notify : Contentum.eMigration.BaseUtility.Notify
	{
	}

	public class KodTarak : Contentum.eMigration.BaseUtility.KodTarak
	{
	}

	public class PageView : BaseUtility.PageView
	{
		public PageView ( Page ParentPage, StateBag ViewState )
			 : base ( ParentPage, ViewState )
		{
		}
	}

	public class IratHelye : BaseUtility.IratHelye
	{
		public static void SetGridViewRow ( GridViewRowEventArgs e, string labelIratHelyeId, Page page )
		{
			if ( e.Row.RowType == DataControlRowType.DataRow )
			{
				Label labelIratHelye = ( Label )e.Row.FindControl ( labelIratHelyeId );
				labelIratHelye.Text = GetTextFormKod ( labelIratHelye.Text, page );
				if ( labelIratHelye.Text == Selejtezett.Text )
				{
					DataRowView drv = ( DataRowView )e.Row.DataItem;
					if ( drv != null && drv[ "Selejtezes_Datuma" ] != null )
					{
						string selejtezesDatuma = drv[ "Selejtezes_Datuma" ].ToString ( );
						labelIratHelye.Text += " (" + selejtezesDatuma + ")";
					}
				}
			}
		}
		public static string GetTextFormKod ( string Kod, Page page )
		{
			string text = String.Empty;
			if ( String.IsNullOrEmpty ( Kod.Trim ( ) ) )
			{
				return text;
			}

			DropDownList list = new DropDownList ( );
			FillDropDownList ( list, page );
			list.SelectedValue = Kod;
			if ( list.SelectedIndex > -1 )
			{
				text = list.SelectedItem.Text;
			}

			return text;
		}
		public static void FillDropDownList ( DropDownList list, Page page )
		{
			ExecParam execParam = UI.SetExecParamDefault ( page, new ExecParam ( ) );
			list.Items.Clear ( );
			list.Items.Add ( Skontro );
			list.Items.Add ( Irattarban );

			// CR3316 Ugyhol statuszok �sszekavarodtak
			// FPH-ban nincs Irattarbakuldott st�tusz, helyette r�gt�n Irattarban statuszba kell rakni
			// Statuszok be�ll�t�sa orgf�gg�
			if ( FelhasznaloProfil.OrgKod ( page ) != Constants.OrgKod.FPH )
			{
				//    if (Rendszerparameterek.Get(execParam, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
				//{
				list.Items.Add ( Ugyintezesen );
				list.Items.Add ( IrattarbaKuldott );
			}
			else
			{
				list.Items.Add ( Osztalyon );
				list.Items.Add ( KoztHiv );
				list.Items.Add ( Birosag );
				list.Items.Add ( Ugyeszseg );
				list.Items.Add ( Vezetoseg );
				list.Items.Add ( Kozjegyzonel );
				list.Items.Add ( FopolgarmesteriHivatal );
			}
			list.Items.Add ( SelejtezesreVar );
			list.Items.Add ( Selejtezett );
			list.Items.Add ( IrattarbolElkert );
			list.Items.Add ( SkontrobolElkert );
			list.Items.Add ( Lomtar );

			list.Items.Add ( Leveltar );
			list.Items.Add ( Jegyzek );
			list.Items.Add ( LezartJegyzek );
			list.Items.Add ( EgyebSzervezetnekAtadott );
			// CR3226 Sztornozott iratokra val� sz�r�s migr�lt t�telekben
			list.Items.Add ( Sztornozott );
			list.Items.Add(Szerelt);
		}

		public static void FillDropDownListByStatusz ( Page page, DropDownList list, Foszamok.Statusz statusz, bool filterByFunkcio )
		{
			list.Items.Clear ( );

			if ( statusz == null ) return;

			FillDropDownList ( list, page );

			if ( list.Items.Contains ( IratHelye.Skontro ) )
			{
				if ( statusz.Ugyhol != Constants.MIG_IratHelye.Skontro && !Foszamok.SkontrobaHelyezheto ( statusz ) )
				{
					list.Items.Remove ( IratHelye.Skontro );
				}
			}

			if ( list.Items.Contains ( IratHelye.Irattarban ) )
			{
				if ( statusz.Ugyhol != Constants.MIG_IratHelye.Irattarban && !Foszamok.IrattarbaKuldheto ( statusz ) && !Foszamok.SelejtezesreKijelolesVisszavonhato ( statusz ) )
				{
					list.Items.Remove ( IratHelye.Irattarban );
				}
			}

			if ( list.Items.Contains ( IratHelye.Osztalyon ) )
			{
				if ( statusz.Ugyhol != Constants.MIG_IratHelye.Osztalyon && !Foszamok.KiadhatoOsztalyra ( statusz ) && !Foszamok.SkontrobolKiveheto ( statusz ) )
				{
					list.Items.Remove ( IratHelye.Osztalyon );
				}
			}

			if ( !Foszamok.Modosithato ( statusz ) )
			{
				//if (statusz.Ugyhol != Constants.MIG_IratHelye.KoztHiv)
				//{
				//    list.Items.Remove(IratHelye.KoztHiv);
				//}

				//if (statusz.Ugyhol != Constants.MIG_IratHelye.Birosag)
				//{
				//    list.Items.Remove(IratHelye.Birosag);
				//}

				//if (statusz.Ugyhol != Constants.MIG_IratHelye.Ugyeszseg)
				//{
				//    list.Items.Remove(IratHelye.Ugyeszseg);
				//}

				//if (statusz.Ugyhol != Constants.MIG_IratHelye.Vezetoseg)
				//{
				//    list.Items.Remove(IratHelye.Vezetoseg);
				//}

				//if (statusz.Ugyhol != Constants.MIG_IratHelye.Kozjegyzonel)
				//{
				//    list.Items.Remove(IratHelye.Kozjegyzonel);
				//}

				//if (statusz.Ugyhol != Constants.MIG_IratHelye.FopolgarmesteriHivatal)
				//{
				//    list.Items.Remove(IratHelye.FopolgarmesteriHivatal);
				//}
			}


			if ( list.Items.Contains ( IratHelye.SelejtezesreVar ) )
			{
				if ( statusz.Ugyhol != Constants.MIG_IratHelye.SelejtezesreVar && !Foszamok.SelejtezesreKijelolheto ( statusz ) )
				{
					list.Items.Remove ( SelejtezesreVar );
				}
			}

			if ( list.Items.Contains ( IratHelye.Selejtezett ) )
			{
				if ( statusz.Ugyhol != Constants.MIG_IratHelye.Selejtezett && !Foszamok.Selejtezheto ( statusz ) )
				{
					list.Items.Remove ( Selejtezett );
				}
			}

			if ( list.Items.Contains ( IratHelye.IrattarbolElkert ) )
			{
				if ( statusz.Ugyhol != Constants.MIG_IratHelye.IrattarbolElkert && statusz.Ugyhol != Constants.MIG_IratHelye.Irattarban )
				{
					list.Items.Remove ( IrattarbolElkert );
				}
			}

			if ( list.Items.Contains ( IratHelye.SkontrobolElkert ) )
			{
				if ( statusz.Ugyhol != Constants.MIG_IratHelye.SkontrobolElkert && !Foszamok.SkontrobolKiveheto ( statusz ) )
				{
					list.Items.Remove ( SkontrobolElkert );
				}
			}

			if ( list.Items.Contains ( IratHelye.Lomtar ) )
			{
				if ( statusz.Ugyhol != Constants.MIG_IratHelye.Lomtar && !Foszamok.LomtarbaHelyezheto ( statusz ) )
				{
					list.Items.Remove ( IratHelye.Lomtar );
				}
			}

			if ( filterByFunkcio )
			{
				FilterDropDownListByFunkcio ( page, list, statusz );
			}
		}

		public static bool GetFunkcioJog ( Page page, string iratHelye )
		{
			if ( String.IsNullOrEmpty ( iratHelye ) )
				return true;

			switch ( iratHelye )
			{
				case Constants.MIG_IratHelye.Irattarban:
					return FunctionRights.GetFunkcioJog ( page, "FoszamIrattarbaKuldes" );
				case Constants.MIG_IratHelye.SkontrobolElkert:
					return FunctionRights.GetFunkcioJog ( page, "FoszamSkontrobolKivetel" );
				case Constants.MIG_IratHelye.Osztalyon:
					return FunctionRights.GetFunkcioJog ( page, "FoszamKiadasOsztalyra" );
				case Constants.MIG_IratHelye.SelejtezesreVar:
				case Constants.MIG_IratHelye.Selejtezett:
					return FunctionRights.GetFunkcioJog ( page, "FoszamSelejtezes" );
				case Constants.MIG_IratHelye.Skontro:
					return FunctionRights.GetFunkcioJog ( page, "FoszamSkontrobaHelyezes" );
				case Constants.MIG_IratHelye.Lomtar:
					return FunctionRights.GetFunkcioJog ( page, "FoszamLomtarbaHelyezes" );
				default:
					return true;
			}
		}

		public static void FilterDropDownListByFunkcio ( Page page, DropDownList list, Foszamok.Statusz stausz )
		{
			for ( int i = list.Items.Count - 1; i >= 0; i-- )
			{
				ListItem item = list.Items[ i ];
				if ( stausz.Ugyhol != item.Value && !IratHelye.GetFunkcioJog ( page, item.Value ) )
				{
					list.Items.Remove ( item );
				}
			}
		}
	}

    public class UgyiratTipus
    {
        public static string GetRegiTipusFromUgyiratJelleg(string ugyirat_jelleg)
        {
            switch (ugyirat_jelleg)
            {
                case KodTarak.UGYIRAT_JELLEG.Papir: return "pap�r alap�";
                case KodTarak.UGYIRAT_JELLEG.Elektronikus: return "elektronikus";
                case KodTarak.UGYIRAT_JELLEG.Vegyes: return "vegyes";
                default: { return ""; };
            }
        }

        public static string GetUgyiratJellegFromRegiTipus(string regi_tipus)
        {
            switch (regi_tipus)
            {
                case "pap�r alap�": return KodTarak.UGYIRAT_JELLEG.Papir;
                case "elektronikus": return KodTarak.UGYIRAT_JELLEG.Elektronikus;
                case "vegyes": return KodTarak.UGYIRAT_JELLEG.Vegyes;
                default: { return ""; };
            }
        }
    }

	public enum ObjektumAzonositoTipus
	{
		Missing = 0,
		Id = 1,
		Azonosito = 2
	}

	public static class eRecord
	{
		private static string eRecordWebsiteUrl;

		public static string ERecordWebsiteUrl
		{
			get
			{
				if ( String.IsNullOrEmpty ( eRecordWebsiteUrl ) )
				{
					string url = UI.GetAppSetting ( "eRecordWebSiteUrl" );
					try
					{
						Uri uri = new Uri ( url );
						eRecordWebsiteUrl = uri.AbsolutePath;
					}
					catch ( UriFormatException )
					{
						eRecordWebsiteUrl = url;
					}
				}
				return eRecordWebsiteUrl;
			}
		}

		public static string ugyiratFormUrl = ERecordWebsiteUrl + "UgyUgyiratokForm.aspx";
	}

	public class Foszamok : BaseUtility.Foszamok
	{
	}

	public class Rendszerparameterek : Contentum.eUtility.Rendszerparameterek
	{
	}


}
