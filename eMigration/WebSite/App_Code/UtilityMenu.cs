﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Data;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for UtilityMenu
/// </summary>
namespace Contentum.eMigration.Utility
{
    public static class UtilityMenu
    {
        public static bool IsMenuVisible(Page page, string menuId)
        {
            if (String.IsNullOrEmpty(menuId))
            {
                return false;
            }

            menuId = menuId.ToUpper();
            if (menuId == "2660A783-B709-4BE7-9DD5-E3CC411CF62A") // Vezetoi panel
            {
                ExecParam xpm = UI.SetExecParamDefault(page);
                try
                {
                    var vezetettCsoportok = Contentum.eUtility.Csoportok.GetFelhasznaloVezetettCsoportjai(page, xpm.Felhasznalo_Id);
                    return vezetettCsoportok.Count > 0;
                }
                catch
                {
                    return false;
                }
            }

            //Felülvizsgálat
            if (menuId == "D821344F-7E97-439E-9E02-47DA6B40BE22")
            {
                return Rendszerparameterek.GetBoolean(page, Rendszerparameterek.EMIGRATIONBEN_SELEJTEZES_JEGYZEKRE_HELYEZESSEL, false);
            }

            return true;
        }
    }
}