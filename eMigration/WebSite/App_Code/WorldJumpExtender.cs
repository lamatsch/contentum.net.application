﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eUtility;

/// <summary>
/// Summary description for WorldJumpExtender
/// </summary>
namespace Contentum.eUIControls
{
    [TargetControlType(typeof(TextBox))]
    public class WorldJumpExtender: ExtenderControl
    {
        private string _autoCompleteExtenderId;

        public string AutoCompleteExtenderId
        {
            get { return (_autoCompleteExtenderId == null) ? String.Empty : _autoCompleteExtenderId; }
            set { _autoCompleteExtenderId = value; }
        }

        private bool _enabled = true;

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        private bool _freeType = false;

        public bool FreeType
        {
            get { return _freeType; }
            set { _freeType = value;}
        }

        protected override System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            if (!Enabled || !targetControl.Visible)
                return null;

            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("Utility.WorldJumpBehavior", targetControl.ClientID);
            
            if (String.IsNullOrEmpty(this.AutoCompleteExtenderId))
            {
                throw new InvalidOperationException("A WorldJumpExtender AutoCompleteExtenderId propertyje nem lehet üres.");
            }

            Control control = FindControl(this.AutoCompleteExtenderId);
            if (control == null)
            {
                throw new InvalidOperationException(String.Format("A WorldJumpExtender AutoCompleteExtenderId-ban megadott vezétlő nem található: {0}",this.AutoCompleteExtenderId));
            }

            descriptor.AddProperty("AutoCompleteExtenderId", control.ClientID);
            descriptor.AddProperty("Delimeter", Constants.AutoComplete.delimeter);

            if (FreeType)
            {
                descriptor.AddProperty("FreeType", FreeType);
            }

            return new ScriptDescriptor[] { descriptor };
        }

        protected override System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
        {
            if (!Enabled)
                return null;

            ScriptReference reference = new ScriptReference();
            reference.Path = "~/JavaScripts/WorldJumpBehavior.js";

            return new ScriptReference[] { reference };
        }
    }
}
