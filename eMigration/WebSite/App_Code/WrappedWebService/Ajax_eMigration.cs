﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for Ajax_eMigration
/// </summary>
[WebService(Namespace = "Contentum.eMigration.WebSite")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class Ajax_eMigration : System.Web.Services.WebService {

    public Ajax_eMigration () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool IsIrattariJelExists(string felhasznaloId, string ev, string irattariJel)
    {
        if (String.IsNullOrEmpty(irattariJel))
            return true;

        MIG_IrattariJelService service = eMigrationService.ServiceFactory.GetMIG_IrattariJelService();
        ExecParam xpm = new ExecParam();
        xpm.Felhasznalo_Id = felhasznaloId;
        MIG_IrattariJelSearch sch = new MIG_IrattariJelSearch();
        if (!String.IsNullOrEmpty(ev))
        {
            sch.EV.Value = ev;
            sch.EV.Operator = Query.Operators.equals;
        }
        sch.IRJEL.Value = irattariJel;
        sch.IRJEL.Operator = Query.Operators.equals;
        Result res = service.GetAll(xpm, sch);

        if (res.IsError)
        {
            throw new Exception(res.ErrorMessage);
        }

        if (res.Ds.Tables[0].Rows.Count > 0)
            return true;
        return false;
    }  
}
