using System;
using System.Web.UI;
using System.Collections.Generic;

/// <summary>
/// Summary description for Class1
/// </summary>
// &&
namespace Contentum.eAdmin.Utility
{
    public class XmlFunction : Contentum.eAdmin.BaseUtility.XmlFunction
    {
    }

    public class Search : Contentum.eAdmin.BaseUtility.Search
    {
    }

    public class eAdminService : Contentum.eAdmin.BaseUtility.eAdminService
    {
    }

    public class UI : BaseUtility.UI
    {
        public static void SetFocus(Control control)
        {
            if (control != null)
            {
                control.Page.ClientScript.RegisterStartupScript(control.Page.GetType(), "setfocus", "var focusedcontrol = document.getElementById('" + control.ClientID + "'); focusedcontrol.focus();", true);
            }
        }

        public static List<String> StringToListBySeparator(String StringList, Char Separator)
        {
            String[] stringlist = StringList.Split(Separator);
            List<string> _List = new List<string>();

            foreach (String item in stringlist)
            {
                if (!String.IsNullOrEmpty(item))
                {
                    _List.Add(item);
                }
            }
            return _List;
        }
    }


    public class UIMezoObjektumErtekek : Contentum.eAdmin.BaseUtility.UIMezoObjektumErtekek
    {
    }

    public class Log : Contentum.eAdmin.BaseUtility.Log
    {
        public Log(Page page)
            : base(page)
        {
        }

    }

    public class Authentication : Contentum.eAdmin.BaseUtility.Authentication
    {
    }

    public class FelhasznaloProfil : Contentum.eAdmin.BaseUtility.FelhasznaloProfil
    {
    }

    public class FunctionRights : Contentum.eAdmin.BaseUtility.FunctionRights
    {
    }

    public class LockManager : Contentum.eAdmin.BaseUtility.LockManager
    {
    }

    public class JavaScripts : BaseUtility.JavaScripts
    {

        public static string SetOnClientClickSendObjectsConfirm(string GridViewClientId)
        {
            string javascript = "";
            javascript =
                "var count = getSelectedCheckBoxesCount('" +
                GridViewClientId + "','check'); " +
                " if (count==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
                + "'); return false;} ";
            return javascript;
        }

        public static void SendBackResultIdsListToCallingWindow(Page parentPage, String RecordsId, String Message)
        {
            if (parentPage == null || RecordsId == null) return;

            string hiddenFieldId = parentPage.Request.QueryString.Get(QueryStringVars.HiddenFieldId);
            string MessageHiddenFieldId = parentPage.Request.QueryString.Get(QueryStringVars.MessageHiddenFieldId);
            if (!String.IsNullOrEmpty(hiddenFieldId))
            {

                String script = "";
                script += " if (window.dialogArguments) { ";
                script += " window.dialogArguments.parentWindow.document.getElementById('" + hiddenFieldId + "').value = '" + RecordsId + "'; \n";
                script += " window.dialogArguments.parentWindow.document.getElementById('" + MessageHiddenFieldId + "').value = '" + Message + "'; \n";
                script += " } else { "
                    + " window.opener.document.getElementById('" + hiddenFieldId + "').value = '" + RecordsId + "'; \n"
                    + " window.opener.document.getElementById('" + MessageHiddenFieldId + "').value = '" + Message + "'; \n"
                    + " } window.returnValue=true;";

                parentPage.ClientScript.RegisterStartupScript(parentPage.GetType(), "ReturnValuesToParentWindow"
                    , script, true);

            }
        }

    }

    public class DefaultDates : Contentum.eAdmin.BaseUtility.DefaultDates
    {
    }


    public class CommandName : Contentum.eAdmin.BaseUtility.CommandName
    {
    }

    public class EventArgumentConst : Contentum.eAdmin.BaseUtility.EventArgumentConst
    {
    }

    public class Constants : Contentum.eAdmin.BaseUtility.Constants
    {
    }

    /// <summary>
    /// URL-ekben el�fordul� param�ternevek
    /// </summary>
    public class QueryStringVars : Contentum.eAdmin.BaseUtility.QueryStringVars
    {
    }

    public class Defaults : Contentum.eAdmin.BaseUtility.Defaults
    {
    }


    public class ResultError : Contentum.eAdmin.BaseUtility.ResultError
    {
    }

    public class Notify : Contentum.eAdmin.BaseUtility.Notify
    {
    }

    public class KodTarak : Contentum.eAdmin.BaseUtility.KodTarak
    {
    }

    public interface ISelectableUserComponent : Contentum.eAdmin.BaseUtility.ISelectableUserComponent
    {
    }

    public class PageView : Contentum.eAdmin.BaseUtility.PageView
    {
        public PageView(Page ParentPage, StateBag ViewState) : base(ParentPage, ViewState)
        {
        }
    }

    public class Rendszerparameterek : Contentum.eAdmin.BaseUtility.Rendszerparameterek
    {
    }

}
