﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Component_AlkalmazasTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public RequiredFieldValidator Validator
    {
        get { return Validator1; }
    }


    public string Text
    {
        set { AlkalmazasNev.Text = value; }
        get { return AlkalmazasNev.Text; }
    }

    public Contentum.eUIControls.CustomTextBox TextBox
    {
        get { return AlkalmazasNev; }
    }


    public bool Enabled
    {
        set
        {
            this.TextBox.Enabled = value;
        }
        get { return this.TextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            this.TextBox.ReadOnly = value;

        }
        get { return this.TextBox.ReadOnly; }
    }


    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public string CssClass
    {
        get { return TextBox.CssClass; }
        set { TextBox.CssClass = value; }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
        }
    }

    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_AlkalmazasokService service = eAdminService.ServiceFactory.GetKRT_AlkalmazasokService();
            ExecParam execParam = UI.SetExecParamDefault(Page);
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Alkalmazasok _KRT_Alkalmazasok = (KRT_Alkalmazasok)result.Record;
                Text = _KRT_Alkalmazasok.Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
        }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

    #region ISelectableUserComponent Members

    private bool _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            if (!_ViewEnabled)
            {
                ReadOnly = !_ViewEnabled;
                this.TextBox.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    bool _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                this.TextBox.CssClass += " ViewDisabledWebControl";
            }
        }
    }

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(TextBox);

        // Le kell tiltani a ClientValidatort
        Validate = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}