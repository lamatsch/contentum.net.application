﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eUtility;

public partial class Component_BelepesiAdatok : System.Web.UI.UserControl
{
    public HiddenField HiddenFieldFelhasznaloId
    {
        get
        {
            return hf_FelhasznaloId;
        }
    }
    public HiddenField HiddenFieldHelyettesitesId
    {
        get
        {
            return hf_HelyettesitesId;
        }
    }
    public HiddenField HiddenFieldCsoporttagId
    {
        get
        {
            return hf_CsoporttagId;
        }
    }
    public HiddenField HiddenFieldSzervezetId
    {
        get
        {
            return hf_SzervezetId;
        }
    }

    public HiddenField HiddenFieldLoginType
    {
        get
        {
            return hf_LoginType;
        }
    }


    private Label _FelhasznaloNev;

    public Label FelhasznaloNev
    {
        get { return _FelhasznaloNev; }
        set { _FelhasznaloNev = value; }
    }


    private Label _LoginUserNev;

    public Label LoginUserNev
    {
        get { return _LoginUserNev; }
        set { _LoginUserNev = value; }
    }

    private Label _HelyettesitesMod;

    public Label HelyettesitesMod
    {
        get { return _HelyettesitesMod; }
        set { _HelyettesitesMod = value; }
    }

    private Label _FelettesSzervezet;

    public Label FelettesSzervezet
    {
        get { return _FelettesSzervezet; }
        set { _FelettesSzervezet = value; }
    }

    private Image _OrgLogo_Top;

    public Image OrgLogo_Top
    {
        get { return _OrgLogo_Top; }
        set { _OrgLogo_Top = value; }
    }

    private Image _OrgLogo_Bottom;

    public Image OrgLogo_Bottom
    {
        get { return _OrgLogo_Bottom; }
        set { _OrgLogo_Bottom = value; }
    }

    public void LoadBelepesiAdatok()
    {
        if (Page.Session[Constants.FelhasznaloProfil] != null)
        {
            Contentum.eUtility.FelhasznaloProfil f = (Contentum.eUtility.FelhasznaloProfil)Page.Session[Constants.FelhasznaloProfil];


            if (f.LoginUser.Id == f.Felhasznalo.Id)
            {
                if (FelhasznaloNev != null)
                    FelhasznaloNev.Visible = false;

                if (LoginUserNev != null)
                    LoginUserNev.Text = f.LoginUser.Nev;
            }
            else
            {
                if (FelhasznaloNev != null)
                {
                    FelhasznaloNev.Text = f.Felhasznalo.Nev + " <br/>";
                    FelhasznaloNev.Visible = true;
                }

                if (LoginUserNev != null)
                    LoginUserNev.Text = "(" + f.LoginUser.Nev + ")";

                if (HelyettesitesMod != null)
                {
                    if (f.Helyettesites.HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Megbizas)
                    {
                        HelyettesitesMod.Text = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar("HELYETTESITES_MOD", KodTarak.HELYETTESITES_MOD.Megbizas, Page);
                        HelyettesitesMod.Visible = true;
                    }
                    else
                    {
                        HelyettesitesMod.Text = "";
                        HelyettesitesMod.Visible = false;
                    }
                }
            }

            // FelhasznaloId (helyettesített felhasználó Id-ja) és SzervezetId lementése hiddenfieldbe
            // (ha lejár a session, és úgy jön egy postback, akkor a bejelentkező eljárásnál ezzel a helyettesítéssel/szervezettel próbáljuk meg automatikusan beléptetni)
            hf_FelhasznaloId.Value = f.Felhasznalo.Id;
            hf_HelyettesitesId.Value = f.Helyettesites.Id;
            hf_CsoporttagId.Value = f.FelhasznaloCsoportTagsag.Id;
            hf_SzervezetId.Value = f.FelhasznaloCsoportTagsag.Csoport_Id;
            hf_LoginType.Value = f.LoginType;

            //OrgNev.Text = f.Felhasznalo.Org.Nev;

            if (FelettesSzervezet != null)
                FelettesSzervezet.Text = f.FelhasznaloCsoportTagsag.Csoport_Nev;

            #region Logo
            String ImagePath = "images/hu/fejlec/";

            System.IO.DirectoryInfo directoryInfo = new System.IO.DirectoryInfo(Server.MapPath(ImagePath + f.Felhasznalo.Org.Kod));
            if (directoryInfo.Exists == true)
            {
                ImagePath += f.Felhasznalo.Org.Kod + "/";
            }

            bool isNMHH = FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.NMHH;
            if (isNMHH)
            {
                if (OrgLogo_Top != null)
                    OrgLogo_Top.ImageUrl = ImagePath + "nmhh_logo.png";
                if (OrgLogo_Bottom != null)
                    OrgLogo_Bottom.ImageUrl = ImagePath + "logo_bottom_middle.jpg";
            }
            #endregion Logo

            RefreshBelepesiAdatok();
        }
        else
        {
            UI.RedirectToErrorPageByErrorCode(Page, "UserProfileError");
        }
    }

    private void RefreshBelepesiAdatok()
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm != null)
        {
            if (sm.IsInAsyncPostBack)
            {
                bool refresh = false;
                List<HiddenField> hfBelepesiAdatok = new List<HiddenField>(5);
                hfBelepesiAdatok.Add(hf_FelhasznaloId);
                hfBelepesiAdatok.Add(hf_HelyettesitesId);
                hfBelepesiAdatok.Add(hf_CsoporttagId);
                hfBelepesiAdatok.Add(hf_SzervezetId);
                hfBelepesiAdatok.Add(hf_LoginType);

                foreach (HiddenField hf in hfBelepesiAdatok)
                {
                    string elozoErtek = Request.Params[hf.UniqueID] ?? String.Empty;
                    if (hf.Value != elozoErtek)
                    {
                        refresh = true;
                        break;
                    }
                }


                if (refresh)
                {
                    List<Label> labelBelepesiAdatok = new List<Label>(4);
                    if (FelhasznaloNev != null)
                        labelBelepesiAdatok.Add(FelhasznaloNev);
                    if (LoginUserNev != null)
                        labelBelepesiAdatok.Add(LoginUserNev);
                    if (HelyettesitesMod != null)
                        labelBelepesiAdatok.Add(HelyettesitesMod);
                    if (FelettesSzervezet != null)
                        labelBelepesiAdatok.Add(FelettesSzervezet);

                    List<Image> imgBelepesiAdatok = new List<Image>(2);
                    if (OrgLogo_Top != null)
                        imgBelepesiAdatok.Add(OrgLogo_Top);
                    if (OrgLogo_Bottom != null)
                        imgBelepesiAdatok.Add(OrgLogo_Bottom);

                    string js = String.Empty;
                    foreach (HiddenField hf in hfBelepesiAdatok)
                    {
                        js += "var hf = $get('" + hf.ClientID + "');";
                        js += "if(hf) hf.value = '" + hf.Value + "';";
                    }
                    foreach (Label label in labelBelepesiAdatok)
                    {
                        js += "var label = $get('" + label.ClientID + "');";
                        js += "if(label && label.firstChild) label.firstChild.nodeValue = '" + label.Text + "';";
                    }
                    foreach (Image img in imgBelepesiAdatok)
                    {
                        js += "var img = $get('" + img.ClientID + "');";
                        js += "if(img) img.src = '" + img.ImageUrl + "';";
                    }

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "RefreshBelepesiAdatok", js, true);
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
