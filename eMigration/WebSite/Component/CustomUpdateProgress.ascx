<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomUpdateProgress.ascx.cs" Inherits="Component_CustomUpdateProgress" %>

<asp:UpdateProgress id="UpdateProgress1" runat="server" DisplayAfter="30">
        <progresstemplate>
 <div class="updateProgress" id="UpdateProgressPanel" runat="server">
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
        <table>
        <tr><td>
        <img src="images/hu/egyeb/activity_indicator.gif" alt="" />
        </td>
        <td class="updateProgressText">
        <asp:Label ID="Label_progress" runat="server" Text="Feldolgozás folyamatban..."></asp:Label>
        </td></tr>
        </table>
        </eUI:eFormPanel> 
</div>
<ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server" TargetControlID="UpdateProgressPanel"
VerticalSide="Middle" HorizontalSide="Center">
 </ajaxToolkit:AlwaysVisibleControlExtender>
</progresstemplate>
    </asp:UpdateProgress>