using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;

public partial class Component_CustomUpdateProgress : System.Web.UI.UserControl
{
    public UpdateProgress UpdateProgress
    {
        get { return UpdateProgress1; }
    }


    public int DisplayAfter
    {
        get
        {
            return UpdateProgress1.DisplayAfter;
        }
        set
        {
            UpdateProgress1.DisplayAfter = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
