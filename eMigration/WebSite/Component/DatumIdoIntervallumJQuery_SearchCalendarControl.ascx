﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="DatumIdoIntervallumJQuery_SearchCalendarControl.ascx.cs"
    Inherits="Component_DatumIdoIntervallumJQuery_SearchCalendarControl" %>

<div class="DisableWrap">
    <table>
        <tr>
            <td>
                <asp:TextBox ID="DatumKezd" runat="server" class="dateTimePickerClass"
                    onchange="compareDates();" title="kezdő dátum" />
            </td>
            <td>
                <img src="../images/hu/egyeb/right-arrow.jpg" alt="Másol" title="Másol"
                    onclick="return copyDatumKezdToVege();return false;" style="cursor: pointer;" />
            </td>
            <td>
                <asp:TextBox ID="DatumVege" runat="server" class="dateTimePickerClass" title="Záró dátum"
                    onchange="compareDates();" />
            </td>
        </tr>
    </table>
</div>
<%-- STYLE --%>
<link href="../JavaScripts/jquery.datetimepicker.min.css" rel="stylesheet" />
<link href="../JavaScripts/jquery-ui.css" rel="stylesheet" />
<style>
    .error {
        padding: 3px;
        margin: 3px;
        background-color: #ffc;
        border: 1px solid #c00;
    }
</style>
<%--JAVASCRIPTS--%>
<script src="../JavaScripts/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="../JavaScripts/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<script src="../JavaScripts/jquery-ui.js" type="text/javascript"></script>
<script src="../JavaScripts/jquery.ui.datepicker-hu.js" type="text/javascript"></script>
<%--JAVASCRIPT CUSTOM--%>
<script type="text/javascript">
    /**
     * CONSTANTS
     */
    const DATE_KEZD = "DatumKezd";
    const DATE_ZARO = "DatumVege";
    const TITLE_ZARO = 'Záró dátum';
    const TITLE_ERROR_ZARO = 'A záró dátum korábbi mint a kezdő dátum !!!';

    /**
    * AUTORUN
    */
    initDateTimePickers();
    setDatesOnChange();
    /*-------------------*/

    /**
     * Átmásolja a kezdő dátumot a vég dátumba
     */
    function copyDatumKezdToVege() {
        var kezdControl = getTextboxControl(DATE_KEZD);
        var vegControl = getTextboxControl(DATE_ZARO);
        vegControl.value = kezdControl.value;
        compareDates();
    }
    /**
     * Beállítja a textbox kontrolokat datetimepicker típusúvá
     */
    function initDateTimePickers() {
        setAllDateTimePicker();
    }

    /**
     * Beállítja a textbox kontrolt datetinmepicker típusúvá
     */
    function setAllDateTimePicker() {
        $('.dateTimePickerClass')
            .datetimepicker({
                format: 'Y.m.d H:i',
                theme: 'dark',
            }
            );
    }
    /**
     * Összehasonlítja a kezdő és a záró dátumot
     */
    function compareDates() {
        var kezdControl = getTextboxControl(DATE_KEZD);
        var vegControl = getTextboxControl(DATE_ZARO);

        var kezdDate = new Date(kezdControl.value);
        var vegDate = new Date(vegControl.value)

        if (vegDate < kezdDate) {
            vegControl.className = 'error';
            vegControl.title = TITLE_ERROR_ZARO;
        }
        else {
            vegControl.classList.remove('error');
            vegControl.className = 'dateTimePickerClass';
            vegControl.title = TITLE_ZARO;
        }
    }
    /*HELPER FUNCTIONS*/
    /**
    * Visszaadja az asp-s kontrol kliens id-ját.
    * @param asp_net_id
    */
    function GetClientID(asp_net_control_id) {
        return $("[id$=" + asp_net_control_id + "]").attr("id");
    };
    /**
    * Megkeresi és visszaadja a asp.net id alapján a kontrolt
    * @param identity
    */
    function getTextboxControl(identity) {
        var cId = GetClientID(identity);
        var cControl = document.getElementById(cId);
        return cControl;
    }
</script>
