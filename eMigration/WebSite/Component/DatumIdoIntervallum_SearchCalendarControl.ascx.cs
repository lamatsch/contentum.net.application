﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class Component_DatumIdoIntervallum_SearchCalendarControl : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    protected void Page_Load(object sender, EventArgs e)
    {
        datumKezd_masol_ImageButton.OnClientClick = "JavaScript: document.getElementById('" + DatumVege.TextBox.ClientID + "').value = " +
                " document.getElementById('" + DatumKezd.TextBox.ClientID + "').value; " +
                " document.getElementById('" + DatumVege.GetHourTextBox.ClientID + "').value = document.getElementById('" + DatumKezd.GetHourTextBox.ClientID + "').value; " +
                " document.getElementById('" + DatumVege.GetMinuteTextBox.ClientID + "').value = document.getElementById('" + DatumKezd.GetMinuteTextBox.ClientID + "').value; " +
                "return false;";
    }


    public string DatumKezdValue
    {
        get
        {
            return this.DatumKezd.Text;
        }
        set
        {
            this.DatumKezd.Text = value;
        }
    }

    public string DatumVegeValue
    {
        get
        {
            return this.DatumVege.Text;
        }
        set
        {
            this.DatumVege.Text = value;
        }
    }

    #region ISelectableUserComponent method implementations

    Boolean _Enabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;
            DatumKezd.Enabled = _Enabled;
            DatumVege.Enabled = _Enabled;

            datumKezd_masol_ImageButton.Enabled = _Enabled;
        }
    }


    Boolean _Readonly = false;

    public Boolean Readonly
    {
        get { return _Readonly; }
        set
        {
            _Readonly = value;
            DatumKezd.ReadOnly = _Readonly;
            DatumVege.ReadOnly = _Readonly;

            datumKezd_masol_ImageButton.CssClass = "disabledCalendarImage";
        }
    }


    Boolean _ViewEnabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view enabled].
    /// </summary>
    /// <value><c>true</c> if [view enabled]; otherwise, <c>false</c>.</value>
    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Readonly = !_ViewEnabled;

            if (!_ViewEnabled)
            {
                DatumKezd.ViewEnabled = value;
                DatumVege.ViewEnabled = value;
            }
        }
    }

    Boolean _ViewVisible = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view visible].
    /// </summary>
    /// <value><c>true</c> if [view visible]; otherwise, <c>false</c>.</value>
    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;

            if (!_ViewVisible)
            {
                DatumKezd.ViewVisible = value;
                DatumVege.ViewVisible = value;

                datumKezd_masol_ImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }


    /// <summary>
    /// Gets the component list.
    /// </summary>
    /// <returns></returns>
    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.AddRange(DatumKezd.GetComponentList());
        componentList.AddRange(DatumVege.GetComponentList());

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    /// <summary>
    /// Datum kezdete és Datum vége beállítása a megadott mezőknek
    /// </summary>
    public void SetSearchObjectFields(Field SearchField)
    {
        DateTime dtStart = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0);
        DateTime dtEnd = new DateTime(DateTime.Now.Year, 12, 31, 23, 59, 59);

        if (!String.IsNullOrEmpty(DatumKezdValue) && !String.IsNullOrEmpty(DatumVegeValue))
        {
            if (DateTime.TryParse(DatumKezdValue, out dtStart))
            {
                if (DateTime.TryParse(DatumVegeValue, out dtEnd))
                {
                    SearchField.Value = dtStart.ToString();
                    SearchField.ValueTo = dtEnd.ToString();
                    SearchField.Operator = Query.Operators.between;
                }
            }
        }
        else if (!String.IsNullOrEmpty(DatumKezdValue))
        {
            if (DateTime.TryParse(DatumKezdValue, out dtStart))
            {
                SearchField.Value = dtStart.ToString();
                SearchField.Operator = Query.Operators.greaterorequal;
            }
        }
        else if (!String.IsNullOrEmpty(DatumVegeValue))
        {
            if (DateTime.TryParse(DatumVegeValue, out dtEnd))
            {
                SearchField.Value = dtStart.ToString();
                SearchField.ValueTo = dtEnd.ToString();
                SearchField.Operator = Query.Operators.between;
            }
        }
    }

    public string GetSearchText()
    {
        string text = String.Empty;
        Field field = new Field();
        this.SetSearchObjectFields(field);
        switch (field.Operator)
        {
            case Query.Operators.greaterorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.lessorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.between:
                text = field.Value + Query.OperatorNames[field.Operator] + field.ValueTo;
                break;
        }

        return text;
    }

    #endregion
}
