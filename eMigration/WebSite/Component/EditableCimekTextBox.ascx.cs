using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.Text;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class Component_EditableCimekTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, Contentum.eUtility.Test.ITestComponent
{
    private class Cim
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        private string delimeter;

        public string Delimeter
        {
            get { return delimeter; }
            set { delimeter = value; }
        }
        public Cim()
        {
        }
        public Cim(string text)
        {
            Text = text;
            Delimeter = ", ";
        }
        public Cim(string text, string delimeter)
        {
            Text = text;
            Delimeter = delimeter;
        }
    }

    private class CimCollection : IEnumerable
    {
        private List<Cim> items;
        public CimCollection()
        {
            items = new List<Cim>();
        }
        public void Add(string text, string delimeter)
        {
            items.Add(new Cim(text, delimeter));
        }
        public void Add(string text)
        {
            items.Add(new Cim(text));
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion
    }

    private string GetAppendedCim(KRT_Cimek krt_cimek)
    {
        CimCollection cim = new CimCollection();
        StringBuilder text = new StringBuilder("");
        string delimeter = ", ";
        string delimeterSpace = " ";


        try
        {
            switch (krt_cimek.Tipus)
            {
                case KodTarak.Cim_Tipus.Postai:
                    if (String.IsNullOrEmpty(krt_cimek.Hazszam) && !String.IsNullOrEmpty(krt_cimek.HRSZ))
                    {
                        cim.Add("HRSZ.", delimeterSpace);
                        cim.Add(krt_cimek.HRSZ, delimeterSpace);
                    }
                    else
                    {
                        cim.Add(krt_cimek.OrszagNev, delimeter);
                        cim.Add(krt_cimek.IRSZ, delimeter);
                        cim.Add(krt_cimek.TelepulesNev, delimeter);
                        cim.Add(krt_cimek.KozteruletNev, delimeterSpace);
                        cim.Add(krt_cimek.KozteruletTipusNev, delimeterSpace);
                        string hazszam = krt_cimek.Hazszam;
                        string hazszamIg = krt_cimek.Hazszamig;
                        string hazszamBetujel = krt_cimek.HazszamBetujel;
                        if (!String.IsNullOrEmpty(hazszamIg))
                            hazszam += "-" + hazszamIg;
                        if (!String.IsNullOrEmpty(hazszamBetujel))
                            hazszam += "/" + hazszamBetujel;
                        cim.Add(hazszam, delimeter);
                        string lepcsohaz = krt_cimek.Lepcsohaz;
                        if (!String.IsNullOrEmpty(lepcsohaz))
                            lepcsohaz += " l�pcs�h�z";
                        cim.Add(lepcsohaz, delimeter);
                        string szint = krt_cimek.Szint;
                        if (!String.IsNullOrEmpty(szint))
                            szint += ". emelet";
                        cim.Add(szint, delimeter);
                        string ajto = krt_cimek.Ajto;
                        if (!String.IsNullOrEmpty(ajto))
                        {
                            string ajtoBetujel = krt_cimek.AjtoBetujel;
                            if (!String.IsNullOrEmpty(ajtoBetujel))
                                ajto += "/" + ajtoBetujel;
                            ajto += " ajt�";
                        }
                        cim.Add(ajto, delimeter);
                    }
                    break;

                case KodTarak.Cim_Tipus.Egyeb:
                    string Cim = krt_cimek.CimTobbi;
                    cim.Add(Cim, delimeter);
                    break;

                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }

            string lastDelimeter = "";

            foreach (Cim item in cim)
            {
                if (!String.IsNullOrEmpty(item.Text))
                {
                    text.Append(item.Text);
                    text.Append(item.Delimeter);
                    lastDelimeter = item.Delimeter;
                }
            }

            if (text.Length >= lastDelimeter.Length)
                text = text.Remove(text.Length - lastDelimeter.Length, lastDelimeter.Length);
        }
        catch (Exception)
        {
            return "";
        }

        return text.ToString();
    }

    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            TextBox1.ValidationGroup = value;
        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }


    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Modify
    {
        set { ModifyImageButton.OnClientClick = value; }
        get { return ModifyImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }

    public ImageButton ImageButton_New
    {
        get { return NewImageButton; }
    }

    public ImageButton ImageButton_View
    {
        get { return ViewImageButton; }
    }

    public ImageButton ImageButton_Modify
    {
        get { return ModifyImageButton; }
    }

    public string Text
    {
        set { TextBox1.Text = value; }
        get
        {
            if (IsOnTestPage)
            {
                if (String.IsNullOrEmpty(Id_HiddenField))
                    return TextBox1.Text.Trim();
                else
                    return TextBox1.Text;
            }

            return TextBox1.Text;
        }
    }

    public TextBox TextBox
    {
        get { return TextBox1; }
    }

    public bool FreeTextEnabled
    {
        set
        {
            if (value == false)
            {
                TextBox1.Attributes.Add("readonly", "readonly");
                TextBox1.CssClass += " mrUrlapInputWaterMarked";
            }
            else
            {
                TextBox1.Attributes.Remove("readonly");
                TextBox1.CssClass = TextBox1.CssClass.Replace(" mrUrlapInputWaterMarked", "");
            }

        }
        get { return TextBox1.Attributes["readonly"] == "readonly" ? false : true; }
    }

    public bool ModifyVisible
    {
        get { return this.ModifyImageButton.Visible; }
        set { this.ModifyImageButton.Visible = value; }
    }

    public bool Enabled
    {
        set
        {
            TextBox1.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ModifyImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ModifyImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ModifyImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }
        }
        get { return TextBox1.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            TextBox1.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ModifyImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ModifyImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ModifyImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return TextBox1.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                TextBox1.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ModifyImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TextBox1.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ModifyImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
            if (value == true)
            {
                ModifyImageButton.Visible = !value;
            }
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ModifyImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetCimekTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Cimek krt_cimek = (KRT_Cimek)result.Record;
                if (krt_cimek == null)
                {
                    Text = ""; // ha nem talal cim recordot
                }
                else
                {
                    Text = GetAppendedCim(krt_cimek);
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
        }
    }

    public void SetCimekTextBoxByStringOrId(string cimId, string cimString, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        Id_HiddenField = cimId;
        if (!String.IsNullOrEmpty(cimString))
        {
            Text = cimString;
        }
        else
        {
            SetCimekTextBoxById(errorPanel);
        }
    }

    public void SetStringAndIdSearchFields(Field fieldId, Field fieldString)
    {
        fieldId.Value = Id_HiddenField;
        fieldString.Value = Text;
        if (String.IsNullOrEmpty(Id_HiddenField))
        {
            fieldId.Operator = String.Empty;
            fieldString.Operator = Search.GetOperatorByLikeCharater(Text);
        }
        else
        {
            fieldId.Operator = Query.Operators.equals;
            fieldString.Operator = String.Empty;
        }
    }

    public string CssClass
    {
        get
        {
            return TextBox.CssClass;
        }
        set
        {
            TextBox.CssClass = value;
        }
    }

    private bool _tryFireChangeEvent = false;

    public bool TryFireChangeEvent
    {
        get { return _tryFireChangeEvent; }
        set { _tryFireChangeEvent = value; }
    }

    public bool AutoPostBack
    {
        get
        {
            return TextBox.AutoPostBack;
        }
        set
        {
            TextBox.AutoPostBack = value;
        }
    }

    private event EventHandler _TextChanged;

    public event EventHandler TextChanged
    {
        add
        {
            this._TextChanged += value;
        }
        remove
        {
            this._TextChanged -= value;
        }
    }

    private void RaiseTextChanged()
    {
        if (this._TextChanged != null)
        {
            this._TextChanged(this.TextBox, EventArgs.Empty);
        }
    }

    private KRT_Orszagok _Orszag = null;

    public KRT_Orszagok Orszag
    {
        get
        {
            KRT_Orszagok emptyOrszag = new KRT_Orszagok();
            if (String.IsNullOrEmpty(Id_HiddenField))
            {
                return emptyOrszag;
            }

            KRT_CimekService svc = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam xpm = UI.SetExecParamDefault(Page);
            xpm.Record_Id = Id_HiddenField;

            Result res = svc.Get(xpm);

            if (res.IsError)
            {
                return emptyOrszag;
            }

            KRT_Cimek cim = (KRT_Cimek)res.Record;

            if (String.IsNullOrEmpty(cim.Orszag_Id))
            {
                return emptyOrszag;
            }

            KRT_OrszagokService svcOrszagok = eAdminService.ServiceFactory.GetKRT_OrszagokService();
            xpm.Record_Id = cim.Orszag_Id;

            res = svcOrszagok.Get(xpm);

            if (res.IsError)
            {
                return emptyOrszag;
            }

            return (KRT_Orszagok)res.Record;
        }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        // �tt�ve a Page_Loadba �s ott m�dos�tva
        ////OnClick = JavaScripts.SetOnClientClick("CsoportokLovList.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, CsoportMegnevezes.ClientID);   

        //OnClick_Lov = JavaScripts.SetOnClientClick("CimekLovList.aspx",
        //   QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
        //   + "&" + QueryStringVars.TextBoxId + "=" + TextBox1.ClientID
        //            , 900, 650, "", "", false);

        //OnClick_New = JavaScripts.SetOnClientClick("CimekForm.aspx"
        //    , CommandName.Command + "=" + CommandName.New
        //    + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
        //    + "&" + QueryStringVars.TextBoxId + "=" + TextBox1.ClientID
        //    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        //OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
        //        "CimekForm.aspx", "", HiddenField1.ClientID);

        //OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
        //    HiddenField1.ClientID + "').value = '';return false;";

        ////Manu�lis be�r�s eset�n az id t�rl�se
        //string jsClearHiddenField = "$get('" + HiddenField1.ClientID + "').value = '';";

        //TextBox.Attributes.Add("onchange", jsClearHiddenField);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        // CR3321 Partner r�gz�t�s csak keres�s ut�n
        //bopmh
        if (FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            NewImageButton.Visible = false;
        }

        //OnClick = JavaScripts.SetOnClientClick("CsoportokLovList.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, CsoportMegnevezes.ClientID);   

        string query = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + TextBox1.ClientID;

        if (_tryFireChangeEvent)
        {
            query += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }

        OnClick_Lov = JavaScripts.SetOnClientClick("CimekLovList.aspx", query
                    , 900, 650, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("CimekForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + query
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "CimekForm.aspx", "", HiddenField1.ClientID);

        OnClick_Modify = JavaScripts.SetOnClientClick_FormModifyByHiddenField(
                "CimekForm.aspx", query, HiddenField1.ClientID, Defaults.PopupWidth, Defaults.PopupHeight);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";

        //Manu�lis be�r�s eset�n az id t�rl�se
        string jsClearHiddenField = "var clear=true; if (typeof(event) != 'undefined' && event){if(event.lovlist && event.lovlist == 1) clear = false;}"
    + "if(clear) $get('" + HiddenField1.ClientID + @"').value = '';";

        TextBox.Attributes.Add("onchange", jsClearHiddenField);

        TextBox.TextChanged += new EventHandler(TextBox_TextChanged);


    }

    void TextBox_TextChanged(object sender, EventArgs e)
    {
        RaiseTextChanged();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ResetImageButton.Visible = !Validate && !ViewMode;

        if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
        {
            Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

            if (c != null)
            {
                c.Visible = Validate;
            }
        }
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(TextBox1);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ModifyImageButton);
        componentList.Add(ResetImageButton);

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ModifyImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    #region ITestComponent Members

    public bool IsOnTestPage
    {
        get
        {
            if (Page is Contentum.eUtility.Test.BaseTestPage)
                return true;
            return false;
        }
    }

    #endregion
}
