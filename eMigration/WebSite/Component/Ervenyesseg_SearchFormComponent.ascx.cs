using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class Component_Ervenyesseg_SearchFormComponent : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent
{
    public bool TartomanyVisible
    {
        set
        {
            trErvenyessegTartomany.Visible = value;
        }
        get
        {
            return trErvenyessegTartomany.Visible;
        }
    }

    public bool ValidateDateFormat
    {
        set { ErvenyessegCalendarControl1.ValidateDateFormat = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        if (!IsPostBack)
        {
            SetDefault();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    /// <summary>
    /// Keresési objektum ErvKezd és ErvVege mezőinek beállítása
    /// </summary>
    /// <param name="ErvKezd"></param>
    /// <param name="ErvVege"></param>
    public void SetSearchObjectFields(Field ervKezd, Field ervVege)
    {
        if (ervenyessegTartomany_CheckBox.Checked == false)
        {
            if (Ervenyes_RadioButton.Checked == true)
            {
                // érvényes
                ervKezd.Value = Query.SQLFunction.getdate;
                ervKezd.Operator = Query.Operators.lessorequal;
                ervKezd.Group = "100";
                ervKezd.GroupOperator = Query.Operators.and;

                ervVege.Value = Query.SQLFunction.getdate;
                ervVege.Operator = Query.Operators.greaterorequal;
                ervVege.Group = "100";
                ervVege.GroupOperator = Query.Operators.and;
            }
            else if (Ervenytelen_RadioButton.Checked == true)
            {
                // érvénytelen
                ervKezd.Value = Query.SQLFunction.getdate;
                ervKezd.Operator = Query.Operators.greaterorequal;
                ervKezd.Group = "100";
                ervKezd.GroupOperator = Query.Operators.or;

                ervVege.Value = Query.SQLFunction.getdate;
                ervVege.Operator = Query.Operators.lessorequal;
                ervVege.Group = "100";
                ervVege.GroupOperator = Query.Operators.or;
            }
            else
            {
                // összes
                ervKezd.Clear();

                ervVege.Clear();
            }
        }
        else
        {
            // érvényességtartomány van kiválasztva:
            // A feltétel: ervKezd <= ErvenyessegCalendarControl1.ErvVege && ervVege >= ErvenyessegCalendarControl1.ErvKezd            
            if (!String.IsNullOrEmpty(ErvenyessegCalendarControl1.ErvKezd))
            {
                ervKezd.Value = ErvenyessegCalendarControl1.ErvKezd;
                ervKezd.Operator = Query.Operators.lessorequal;
                ervKezd.Group = "100";
                ervKezd.GroupOperator = Query.Operators.and;
            }
            else
            {
                ervKezd.Clear();
            }

            ervVege.Value = ErvenyessegCalendarControl1.ErvVege;
            ervVege.Operator = Query.Operators.greaterorequal;
            ervVege.Group = "100";
            ervVege.GroupOperator = Query.Operators.and;
        }
    }

    public void SetDefault()
    {
        Ervenyes_RadioButton.Enabled = true;
        Ervenytelen_RadioButton.Enabled = true;
        ErvenyessegMindegy_RadioButton.Enabled = true;
        Ervenyes_RadioButton.Checked = true;
        Ervenytelen_RadioButton.Checked = false;
        ErvenyessegMindegy_RadioButton.Checked = false;

        ervenyessegTartomany_CheckBox.Checked = false;
        ErvenyessegCalendarControl1.SetDefault();
        ErvenyessegTartomany_Label.Enabled = false;
        Ervenyesseg_Label.Enabled = true;
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "initEnableOrDisable", ErvenyessegTartomany_CheckBox.Attributes["onclick"], true);
    }
    
    public void SetDefault(Field ervKezd, Field ervVege)
    {        
        SetDefault();

        if (ervKezd != null && ervVege != null)
        {
            if (ervKezd.Value == Query.SQLFunction.getdate && ervVege.Value == Query.SQLFunction.getdate)
            {
                // érvényes vagy érvénytelen állapot:
                if (ervVege.GroupOperator == Query.Operators.and)
                {
                    // érvényes
                    Ervenyes_RadioButton.Checked = true;
                }
                if (ervVege.GroupOperator == Query.Operators.or)
                {
                    // érvénytelen
                    Ervenytelen_RadioButton.Checked = true;
                    Ervenyes_RadioButton.Checked = false;
                    ErvenyessegMindegy_RadioButton.Checked = false;
                }
            }
            else if (String.IsNullOrEmpty(ervKezd.Value) && String.IsNullOrEmpty(ervVege.Value))
            {
                // mindegy
                ErvenyessegMindegy_RadioButton.Checked = true;
                Ervenyes_RadioButton.Checked = false;
                Ervenytelen_RadioButton.Checked = false;
            }
            else
            {
                // érvényességtartomány van megadva
                ErvenyessegCalendarControl1.ErvKezd = ervVege.Value;
                ErvenyessegCalendarControl1.ErvVege = ervKezd.Value;

                ervenyessegTartomany_CheckBox.Checked = true;

                //ErvenyessegCalendarControl1.ErvVege_ImageButton.Attributes["onload"] = ervenyessegTartomany_CheckBox.Attributes["onclick"];
                ////ErvenyessegCalendarControl1.SetDefault();
                //ErvenyessegCalendarControl1.ErvKezd_TextBox.Enabled = true;
                //ErvenyessegCalendarControl1.ErvKezd_ImageButton.Enabled = true;
                //ErvenyessegCalendarControl1.ErvVege_TextBox.Enabled = true;
                //ErvenyessegCalendarControl1.ErvVege_ImageButton.Enabled = true;
                ErvenyessegTartomany_Label.Enabled = true;
                
                //Ervenyesseg_Label.Enabled = true;

            }
        }
        //ErvenyessegCalendarControl1.ErvVege_ImageButton.Attributes["onload"] = ervenyessegTartomany_CheckBox.Attributes["onclick"];
    }

    //public void LoadFromSearchObject(Field ErvKezd, Field ErvVege)
    //{
    //    if (ErvKezd.Value == Query.SQLFunction.getdate)
    //    {
    //        ErvenyessegCalendarControl1.ErvKezd = DefaultDates.Today;
    //    }
    //    else
    //    {
    //        ErvenyessegCalendarControl1.ErvKezd = ErvKezd.Value;
    //    }

    //    if (ErvVege.Value == Query.SQLFunction.getdate)
    //    {
    //        ErvenyessegCalendarControl1.ErvVege = DefaultDates.Today;
    //    }
    //    else
    //    {
    //        ErvenyessegCalendarControl1.ErvVege = ErvVege.Value;
    //    }
    //}

    #region public properties

    public Boolean Enabled
    {
        set { Panel1.Enabled = value; }
        get { return Panel1.Enabled; }    
    }
    public string ErvKezd
    {
        set { ErvenyessegCalendarControl1.ErvKezd = value; }
        get { return ErvenyessegCalendarControl1.ErvKezd; }
    }

    public string ErvVege
    {
        set { ErvenyessegCalendarControl1.ErvVege = value; }
        get { return ErvenyessegCalendarControl1.ErvVege; }
    }

    public TextBox ErvKezd_TextBox
    {
        get { return ErvenyessegCalendarControl1.ErvKezd_TextBox; }
    }

    public TextBox ErvVege_TextBox
    {
        get { return ErvenyessegCalendarControl1.ErvVege_TextBox; }
    }

    public CheckBox ErvenyessegTartomany_CheckBox
    {
        get { return ervenyessegTartomany_CheckBox; }
    }

    public bool Validate
    {
        set
        {
            ErvenyessegCalendarControl1.Validate = value;
        }

    }

    #endregion



    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");


        ErvenyessegTartomany_CheckBox.Attributes["onclick"] =
            "enableOrDisableComponents('" + ervenyessegTartomany_CheckBox.ClientID
                + "',0,['"
                + ErvenyessegCalendarControl1.ErvKezd_TextBox.ClientID
                + "','"
                + ErvenyessegCalendarControl1.ErvVege_TextBox.ClientID
                + "','"
                + ErvenyessegCalendarControl1.ErvKezd_ImageButton.ClientID
                + "','"
                + ErvenyessegCalendarControl1.ErvVege_ImageButton.ClientID
                + "','"
                + ErvenyessegTartomany_Label.ClientID
                + "']); "
                + " enableOrDisableComponents('" + ervenyessegTartomany_CheckBox.ClientID
                + "',1,['"
                + Ervenyes_RadioButton.ClientID
                + "','"
                + Ervenytelen_RadioButton.ClientID
                + "','"
                + ErvenyessegMindegy_RadioButton.ClientID
                + "','"
                + Ervenyesseg_Label.ClientID
                + "']); "
                + " enableOrDisableImages('" + ervenyessegTartomany_CheckBox.ClientID
                + "',0,['"
                + ErvenyessegCalendarControl1.ErvKezd_ImageButton.ClientID
                + "','"
                + ErvenyessegCalendarControl1.ErvVege_ImageButton.ClientID
                + "']); ";

        
    }

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;

        if (ervenyessegTartomany_CheckBox.Checked)
        {
            text = Search.GetQueryText(ErvenyessegTartomany_Label.Text,ErvenyessegCalendarControl1.GetSearchText()).TrimEnd(Search.whereDelimeter.ToCharArray());
        }
        else
        {
            if (Ervenytelen_RadioButton.Checked)
            {
                text = Search.GetQueryText(Ervenyesseg_Label.Text, Ervenytelen_RadioButton.Text).TrimEnd(Search.whereDelimeter.ToCharArray());
            }
            if (ErvenyessegMindegy_RadioButton.Checked)
            {
                text = Search.GetQueryText(Ervenyesseg_Label.Text, ErvenyessegMindegy_RadioButton.Text).TrimEnd(Search.whereDelimeter.ToCharArray());
            }
        }

        return text;
    }

    #endregion
}
