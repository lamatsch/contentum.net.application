<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormHeader.ascx.cs" Inherits="Component_FormHeader" %>
<%@ Register Src="FormTemplateLoader.ascx" TagName="FormTemplateLoader" TagPrefix="uc1" %>
<%@ Register Src="HelpLink.ascx" TagName="HelpLink" TagPrefix="help" %> 
<%@ Register Src="~/Component/FunctionKeysManager.ascx" TagName="FunctionKeysManager" TagPrefix="fm"%>

<div style="display:inline">
 <table style="width: 100%; height: 70px; background-image: url('images/hu/fejlec/kepernyo_fill.jpg'); background-repeat: repeat-x;" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 85px; vertical-align: top;">
            <img id="Img1" runat="server" src="../images/hu/fejlec/kepernyo_ikon.jpg" alt=" " />
            </td>
            <td style="vertical-align: top; text-align: left;
                        font-weight: bold;	font-size: 14px;">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <tr>
                        <td style="height: 46px;text-align: left;vertical-align: middle;" >
                            <asp:Label ID="HeaderLabel" runat="server" CssClass="FormHeaderText"></asp:Label>&nbsp;
                            <asp:Label ID="SpacerLabel" runat="server" Text="-" CssClass="FormHeaderText"></asp:Label>&nbsp;&nbsp;
                            <asp:Label ID="ModeLabel" runat="server" CssClass="FormHeaderText"></asp:Label></td>  
                        <td align="right" style="height: 46px; vertical-align: bottom; position: static; text-align: right;">
                         <%-- Spacer--%>
                        </td>
                        <td style="vertical-align: middle; text-align:right; background-image: url('images/hu/fejlec/kepernyo_fill.jpg'); background-repeat:repeat-x; width: 128px; height: 46px;">
                        <asp:LinkButton ID="DesignViewLinkButton" runat="server" Text="" CssClass="DesignViewLinkButton" OnClick="DesignViewLinkButton_Click" Width="250px" CausesValidation="false" TabIndex = "-1"></asp:LinkButton>
                       </td>
                        <td align="right" valign="middle" style="padding-left:1px">
                            <asp:HyperLink runat="server" ID="ModifyHyperLink" style="display:none;" ToolTip="<%$Resources:Buttons,Modositas%>">
                                <asp:Image ID="ModifyImage" runat="server" CssClass="highlightit" ImageUrl="~/images/hu/ikon/modositas.gif"  AlternateText="<%$Resources:Buttons,Modositas%>" />
                            </asp:HyperLink>
                            <asp:ImageButton ID="FeladatImageButton" runat="server" Visible="false"/>
                            <help:HelpLink ID="linkHelp" runat="server" />
                        </td>                                                
                    </tr>
                    <tr>
                        <td style="height: 3px;" colspan="3">
                            <uc1:FormTemplateLoader id="FormTemplateLoader1" runat="server" Visible="false"></uc1:FormTemplateLoader>            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        <td colspan="4">
        <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false" Width="90%">
        </eUI:eErrorPanel>
        </td>
        </tr>
    </table>
<asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
</div>
