using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_FormHeader : System.Web.UI.UserControl
{
    string Command = "";

    public string HeaderTitle
    {
        set 
        { 
            HeaderLabel.Text = value;
        }
        get { return HeaderLabel.Text; }

    }

    private String fullManualHeaderTitle;

    public String FullManualHeaderTitle
    {
        get { return fullManualHeaderTitle; }
        set { fullManualHeaderTitle = value; }
    }

    private bool disableModeLabel = false;

    public bool DisableModeLabel
    {
        get { return disableModeLabel; }
        set { disableModeLabel = value; }
    }
    
    public string ModeText
    {
        set { ModeLabel.Text = value; }
        get { return ModeLabel.Text; }

    }

    private bool PageViewEnabled
    {
        get
        {
            return Rendszerparameterek.GetBoolean(this.Page, Rendszerparameterek.PAGE_VIEW_ENABLED, false);
        }
    }

    private bool designViewVisible = true;
    public bool DesignViewVisible
    {
        get
        {
            return designViewVisible && PageViewEnabled;
        }
        set
        {
            designViewVisible = value;
        }
    }

    public HyperLink ModifyLink
    {
        get
        {
            return ModifyHyperLink;
        }
        set
        {
            ModifyHyperLink = value;
        }
    }

    #region FormTemplateLoader

    public bool FormTemplateLoader1_Visibility
    {
        get { return FormTemplateLoader1.Visible; }
        set { FormTemplateLoader1.Visible = value; }
    }

    public object TemplateObject
    {
        get { return FormTemplateLoader1.SearchObject; }
        set { FormTemplateLoader1.SearchObject = value; }
    }

    public Type TemplateObjectType
    {
        get { return FormTemplateLoader1.SearchObjectType; }
        set { FormTemplateLoader1.SearchObjectType = value; }
    }

    public string CurrentTemplateName
    {
        get { return FormTemplateLoader1.CurrentTemplateName; }
        set { FormTemplateLoader1.CurrentTemplateName = value; }
    }

    public string CustomTemplateTipusNev
    {
        get { return FormTemplateLoader1.CustomTemplateTipusNev; }
        set { FormTemplateLoader1.CustomTemplateTipusNev = value; }
    }

    public string CurrentTemplateId
    {
        get { return FormTemplateLoader1.CurrentTemplateId; }
        set { FormTemplateLoader1.CurrentTemplateId = value; }
    }

    public void LoadTemplateObjectById(string templateId)
    {
        FormTemplateLoader1.LoadTemplateObjectById(templateId);
    }

    #endregion


    public Contentum.eUIControls.eErrorPanel ErrorPanel
    {
        get { return EErrorPanel1; }
    }

    public string Record_Ver
    {
        get { return Record_Ver_HiddenField.Value; }
        set { Record_Ver_HiddenField.Value = value; }
    }

    /// <summary>
    /// �j template l�trehoz�sa
    /// </summary>
    /// <param name="newSearchObject"></param>
    public void NewTemplate(object newSearchObject)
    {
        FormTemplateLoader1.NewTemplate(newSearchObject);
    }

    /// <summary>
    /// Aktu�lis template elment�se
    /// </summary>
    /// <param name="newSearchObject"></param>
    public void SaveCurrentTemplate(object newSearchObject)
    {
        FormTemplateLoader1.SaveCurrentTemplate(newSearchObject);
    }

    public void TemplateReset()
    {
        FormTemplateLoader1.TemplateReset();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        FormTemplateLoader1.ButtonsClick += new CommandEventHandler(FormTemplateLoader1_ButtonsClick);
        FormTemplateLoader1.ErrorPanel = ErrorPanel;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        Command = Request.QueryString.Get(QueryStringVars.Command);
        switch (Command)
        {
            case CommandName.New:
                ModeLabel.Text = Resources.Form.FormHeaderNew;
                break;
            case CommandName.View:
                ModeLabel.Text = Resources.Form.FormHeaderView;
                break;
            case CommandName.Modify:
                ModeLabel.Text = Resources.Form.FormHeaderModify;
                break;
            default:
                ModeLabel.Text = Resources.Form.FormHeaderView;
                break;
        }

        if (Command != CommandName.New && Command != CommandName.Modify)    // pl. hat�s�gi adatok kit�lt�se miatt sz�ks�ges a Modify-n�l is
        {
            FormTemplateLoader1.Visible = false;
        }

        if (DesignViewVisible && FunctionRights.GetFunkcioJog(Page, CommandName.DesignView))
        {
            if (Command == CommandName.DesignView)
            {
                DesignViewLinkButton.Text = Resources.Form.FormHeaderDesignViewLinkButtonTextBack;
            }
            else
            {
                DesignViewLinkButton.Text = Resources.Form.FormHeaderDesignViewLinkButtonText;
            }
        }
        else
        {
            DesignViewLinkButton.Enabled = false;
            DesignViewLinkButton.Visible = false;
        }

        FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);
        if (fm != null)
        {
            string pageName = String.Empty;
            if (Page.GetType().BaseType != null)
            {
                pageName = Page.GetType().BaseType.Name;
            }
            switch (pageName)
            {
                case "UgyiratElintezettForm":
                case "LezarasForm":
                case "SzerelesForm":
                case "SkontroInditasForm":
                    fm.ObjektumId = Request.QueryString.Get("UgyiratId") ?? String.Empty;
                    fm.ObjektumType = Constants.TableNames.EREC_UgyUgyiratok;
                    break;
                case "IrattarbaAdasForm":
                    fm.ObjektumId = Request.QueryString.Get(QueryStringVars.Id) ?? String.Empty;
                    fm.ObjektumType = Constants.TableNames.EREC_UgyUgyiratok;
                    break;
                case "SzignalasForm":
                    string KuldemenyId = Request.QueryString.Get("KuldemenyId");
                    if (!String.IsNullOrEmpty(KuldemenyId))
                    {
                        fm.ObjektumId = KuldemenyId;
                        fm.ObjektumType = Constants.TableNames.EREC_KuldKuldemenyek;
                    }
                    else
                    {
                        fm.ObjektumId = Request.QueryString.Get("UgyiratId") ?? String.Empty;
                        fm.ObjektumType = Constants.TableNames.EREC_UgyUgyiratok;
                    }
                    break;
                default:
                    fm.ObjektumId = Request.QueryString.Get(QueryStringVars.Id) ?? String.Empty;
                    int index = pageName.IndexOf("Form");
                    if (index > 0)
                    {
                        fm.ObjektumType = "EREC_" + pageName.Substring(0, index);
                    }
                    else
                    {
                        fm.ObjektumType = String.Empty;
                    }
                    break;
            }
        }
    }

    private string GetPageTitle()
    {
        string title = HeaderTitle;
        if (SpacerLabel.Visible && ModeLabel.Visible && !String.IsNullOrEmpty(ModeLabel.Text))
        {
            title += SpacerLabel.Text + ModeLabel.Text;
        }
        return title;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(fullManualHeaderTitle))
        {
            HeaderTitle = fullManualHeaderTitle;
            SpacerLabel.Visible = false;
            ModeLabel.Visible = false;
        }
        if (disableModeLabel == true)
        {
            SpacerLabel.Visible = false;
            ModeLabel.Visible = false;
        }

        Page.Title = this.GetPageTitle();
    }


    public event CommandEventHandler ButtonsClick;

    protected void FormTemplateLoader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (ButtonsClick != null)
        {
            ButtonsClick(this, e);
        }
    }

    protected void DesignViewLinkButton_Click(object sender, EventArgs e)
    {
        if (Command == CommandName.DesignView)
        {
            if (Session["PrevUrl"] != null)
            {
                String PrevUrl = Session["PrevUrl"].ToString();
                Session.Remove("PrevUrl");
                UI.RedirectPage(Page, PrevUrl);
            }
            else
            {
                UI.RedirectPage(Page, Page.Request.CurrentExecutionFilePath);
            }
        }
        else
        {
            Session["PrevUrl"] = Page.Request.Url;
            UI.RedirectPage(Page, Page.Request.CurrentExecutionFilePath + "?" + CommandName.Command + "=" + CommandName.DesignView);
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!IsPostBack)
        {
            if (Contentum.eUtility.HataridosFeladatok.IsFeladatJelzoIkonEnabledOnForm(Page))
            {
                FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);
                if (fm != null)
                {
                    if (!String.IsNullOrEmpty(fm.ObjektumType) && !String.IsNullOrEmpty(fm.ObjektumId) && fm.IsValidObjektumType())
                    {
                        UI.SetFeladatInfo(Page, FeladatImageButton, fm.ObjektumType, fm.ObjektumId);
                        FeladatImageButton.Style[HtmlTextWriterStyle.Width] = "25px";
                        FeladatImageButton.Style[HtmlTextWriterStyle.Height] = "25px";
                    }
                }
            }
        }

        base.Render(writer);
    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterGetActiveTabClientScript(Page);
    }
}
