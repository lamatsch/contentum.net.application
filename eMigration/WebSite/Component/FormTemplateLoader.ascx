<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormTemplateLoader.ascx.cs" Inherits="Component_FormTemplateLoader" %>
<%@ Register Src="../Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc15" %><%-- CR3194 - kisherceg megosztás --%>
<table border="0" cellpadding="0" cellspacing="0"><tr><td>
<div runat="server" id="Div_mainPanel" style="width: 0px; height: 0px; top: 800px; left: 0px; position:fixed; display: none;">

<eUI:eFormPanel ID="EFormPanel1" runat="server" Width="750px" Enabled="true" Visible="true">    
      <table>
        <tr>
            <td style="height: 26px; text-align: right;width:200px">
                <asp:Label ID="Label12" CssClass="formLoaderCaption" runat="server">Betöltött template:</asp:Label>
            </td>
            <td style="height: 26px;white-space:nowrap;padding-right:10px;text-align:left">
                <asp:Label ID="Label_TemplateName" runat="server" Text="_TemplateName_"></asp:Label>
                <asp:Label ID="labelDefault" runat="server" Text="(Alapértelmezett)"></asp:Label>
            </td>
            <td style="height: 26px;text-align:left;width:400px">
                <asp:Button ID="SaveTemplateButton" runat="server" Text="Mentés" CausesValidation="false" OnClick="Button_Click" />
                </td>
        </tr>
        <tr>
            <td style="height: 26px; text-align: right">
                <asp:Label ID="Label1" CssClass="formLoaderCaption" runat="server" Text="További template-ek:"></asp:Label></td>
            <td colspan="1" style="padding-right:10px;text-align:left">
                <asp:DropDownList ID="TemplateList" runat="server" CssClass="formLoaderComboBox">
                </asp:DropDownList></td>
            <td colspan="1" style="text-align:left">
                <asp:Button ID="LoadTemplateButton" runat="server" Text="Betöltés" CausesValidation="false" OnClick="Button_Click" />
                <asp:Button ID="InvalidateTemplateButton" runat="server" Text="Törlés" CausesValidation="false" OnClick="Button_Click" />
                <asp:Button ID="SetDefaultButton" runat="server" Text="Alap" CausesValidation="false" OnClick="Button_Click" />
                <asp:Button ID="RemoveDefaultButton" runat="server" Text="Nem Alap" CausesValidation="false" OnClick="Button_Click" />
            </td>
        </tr>
        <tr>
            <td style="height: 26px; text-align: right;">
                <asp:Label ID="Label2" CssClass="formLoaderCaption" runat="server" Text="Új template:"></asp:Label></td>
            <td colspan="1" style="padding-right:10px;text-align:left">
                <asp:TextBox ID="NewTemplateTextBox" runat="server" CssClass="formLoaderInput"></asp:TextBox></td>
            <td colspan="1" style="text-align:left">
                <asp:Button ID="NewTemplateButton" runat="server" Text="Létrehoz" CausesValidation="false" OnClick="Button_Click" />
                <asp:CheckBox ID="cbNewDefault" runat="server" Checked="false" Text="Alapértelmezett" ToolTip="Alapértelmezett"/>
                <asp:CheckBox ID="cbNewGlobalDefault" runat="server" Checked="false" Text="Globális alapértelmezett" ToolTip="Globális alapértelmezett" />
            </td>
        </tr>
        <tr><%-- CR3194 - kisherceg megosztás --%>
            <td style="height: 26px; text-align: right;">
                <asp:Label ID="Label3" CssClass="formLoaderCaption" runat="server" Text="A kiválasztott template megosztása"></asp:Label></td>
            <td colspan="2" style="text-align:left">
                    <uc15:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBox" runat="server" ReadOnly="false"
                                ViewMode="false" Validate="false" />
            </td>
        </tr>
        <tr>
            <td style="height: 26px; text-align: right;">
            </td>
             <td colspan="1" style="text-align:left">
             </td>
            <td colspan="1" style="text-align:left">
                <asp:CheckBox ID="CheckBoxPublic" runat="server" Checked="false" Text="Publikus?"
                              ToolTip="<%$Resources:Form,SablonPublikus%>" />
               <asp:Button ID="ShareButton" runat="server" Text="Megoszt" CausesValidation="false"
                    OnClick="Button_Click"/>
            </td>
        </tr>
    </table>
<asp:Button ID="Button1" runat="server" Text="Bezár" CausesValidation="false"/>
<asp:HiddenField ID="TemplateId_HiddenField" runat="server" />
    <asp:HiddenField ID="Ver_KRT_UIMezoObjektumErtekek_HiddenField" runat="server" />
    <asp:HiddenField ID="DefaultTemplateId_HiddenField" runat="server" />
</eUI:eFormPanel>
</div>
</td></tr>
<tr><td>
<asp:Panel runat="server" ID="TemplateLoaderPanel" >
<table border="0" cellpadding="0" cellspacing="0">
<tr><td style="text-align: left;padding-right: 10px; ">
<asp:ImageButton TabIndex = "-1" ID="TemplateImageButton" runat="server" 
    ImageUrl="~/images/hu/egyeb/formtemplate_ikon.png" onmouseover="swapByName(this.id,'formtemplate_ikon_keret.png')" onmouseout="swapByName(this.id,'formtemplate_ikon.png')" 
    AlternateText="Template kiválasztás/létrehozás" CausesValidation="false" />&nbsp;
    </td>
    <td>
    <asp:Panel ID="LinkButtons_Panel" runat="server">
    </asp:Panel>
    </td>
    </tr></table>
    </asp:Panel>
</td></tr></table>

<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" 
        PopupControlID="EFormPanel1" 
        TargetControlID="TemplateImageButton"  
        CancelControlID="Button1"          
        PopupDragHandleControlID="EFormPanel1" 
        BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>

