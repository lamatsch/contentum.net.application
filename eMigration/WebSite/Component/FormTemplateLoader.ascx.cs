using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using System.Data;
using Contentum.eQuery.BusinessDocuments;

public partial class Component_FormTemplateLoader : System.Web.UI.UserControl, IScriptControl
{
    #region Settings

    private const string defaultTemplateBackgroundColor = "yellow";
    private const string defaultTemplateStyle = "background:" + defaultTemplateBackgroundColor;
    private const string servicePath = @"WrappedWebService/Ajax.asmx";
    private const string updateAlapertelmezettMethod = "UpdateAlapertelmezettTemplate";

    #endregion

    private bool _hasGlobalDefaultSetupRight;

    protected void Page_Init(object sender, EventArgs e)
    {        
        SaveTemplateButton.CommandName = CommandName.SaveTemplate;
        LoadTemplateButton.CommandName = CommandName.LoadTemplate;
        NewTemplateButton.CommandName = CommandName.NewTemplate;
        InvalidateTemplateButton.CommandName = CommandName.InvalidateTemplate;

        #region CR3194 - Kisherceg megoszt�s
        
        ShareButton.CommandName = CommandName.Share;
        ShareButton.Attributes["onclick"] = "if (document.getElementById('" + FelhasznaloCsoportTextBox.HiddenField.ClientID + "').value=='' && document.getElementById('" + CheckBoxPublic.ClientID + "').checked==false) "
           + " { alert('" + Resources.Error.UINoSelectedUserOrNotPublic + "'); return false; }"+
           "var e = document.getElementById('" + TemplateList.ClientID + "');" +
           "if (typeof e.value == 'undefined' || e.value == '') "
           + " { alert('" + Resources.Error.UINoSelectedTemplate + "'); return false; }"
           
           ;
        #endregion CR3194 - Kisherceg megoszt�s

        TemplateImageButton.OnClientClick = "var divObj = document.getElementById('" + Div_mainPanel.ClientID + "'); "
            +" divObj.style.top='0px'; divObj.style.display='';";

        CurrentTemplateName = Resources.Form.UI_NoTemplateLoaded;

        NewTemplateButton.Attributes["onclick"] = "if (document.getElementById('" + NewTemplateTextBox.ClientID + "').value=='') "
            + " { alert('" + Resources.Error.UITextBoxIsEmpty + "'); return false; }";

        _hasGlobalDefaultSetupRight = FunctionRights.GetFunkcioJog(Page, "FormTemplate_GlobalDefaultSetup");
        cbNewGlobalDefault.Visible = _hasGlobalDefaultSetupRight;

        SetPublikusSablonLathatosag();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        fillTemplatesDictionary();
        createLinkButtons();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        LoadDefaultTemplateObject();

        // ha nincs bet�ltve template, Ment�s gombot letiltjuk
        if (String.IsNullOrEmpty(CurrentTemplateId))
        {
            //SaveTemplateButton.Enabled = false;

            SaveTemplateButton.Visible = false;
        }
        else
        {
            SaveTemplateButton.Visible = true;
        }

        fillTemplateDropDownList();

        // Ha �res a lista
        if (TemplateList.Items.Count == 0)
        {
            //LoadTemplateButton.Enabled = false;
            //InvalidateTemplateButton.Enabled = false;

            LoadTemplateButton.Visible = false;
            InvalidateTemplateButton.Visible = false;
            SetDefaultButton.Style.Add("display", "none");
            RemoveDefaultButton.Style.Add("display", "none");
        }
        else
        {
            LoadTemplateButton.Visible = true;
            InvalidateTemplateButton.Visible = true;
        }

        if (!String.IsNullOrEmpty(CurrentTemplateId) && CurrentTemplateId == DefaultTemplateId)
        {
            labelDefault.Style.Remove("display");
        }
        else
        {
            labelDefault.Style.Add("display", "none");
        }

        if (TemplateList.Items.Count > 0)
        {
            if (TemplateList.SelectedIndex > -1 && TemplateList.SelectedValue == DefaultTemplateId)
            {
                SetDefaultButton.Style.Add("display", "none");
                RemoveDefaultButton.Style.Remove("display");
            }
            else
            {
                SetDefaultButton.Style.Remove("display");
                RemoveDefaultButton.Style.Add("display", "none");
            }
        }
    }

    //private Dictionary<string, string> templatesDictionary;
    private KRT_UIMezoObjektumErtekek _defaultTemplateObject = null;

    private Dictionary<string, string> TemplateDictionary
    {
        get
        {
            if (!(ViewState["TemplateDictionary"] is Dictionary<string, string>))
                ViewState["TemplateDictionary"] = null;

            if (ViewState["TemplateDictionary"] == null)
                return null;

            return ViewState["TemplateDictionary"] as Dictionary<string, string>;
            
        }

        set
        {
            ViewState["TemplateDictionary"] = value;
        }
    }

    private void fillTemplatesDictionary()
    {
        if (TemplateDictionary == null)
        {
            Dictionary<string, string>  templatesDictionary = new Dictionary<string, string>();

            Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService _KRT_UIMezoObjektumErtekekService = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();
            Contentum.eQuery.BusinessDocuments.KRT_UIMezoObjektumErtekekSearch searchObj = new Contentum.eQuery.BusinessDocuments.KRT_UIMezoObjektumErtekekSearch();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            //searchObj.Felhasznalo_Id.Value = FelhasznaloProfil.FelhasznaloId(Page);
            //searchObj.Felhasznalo_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            string whereByManual = String.Format(" and (Felhasznalo_Id='{0}' or (Publikus = '1' and Szervezet_Id = '{1}'))", FelhasznaloProfil.FelhasznaloId(Page), FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
            searchObj.WhereByManual = whereByManual;

            if (searchObjectType == null)
            {
                return;
            }
            else
            {
                searchObj.TemplateTipusNev.Value = this.GetTemplateTipusNev();
                searchObj.TemplateTipusNev.Operator = Contentum.eQuery.Query.Operators.equals;
            }

            searchObj.OrderBy = "UtolsoHasznIdo desc";

            Contentum.eBusinessDocuments.Result result = _KRT_UIMezoObjektumErtekekService.GetAll(execParam, searchObj);

            if (result.Ds != null)
            {
                bool needLoadAlapertelmezett = false;
                string qsTemplateId = Request.QueryString.Get(Contentum.eUtility.QueryStringVars.TemplateId);
                if (String.IsNullOrEmpty(qsTemplateId) && !IsPostBack && Session[this.GetTemplateTipusNev()] == null)
                    needLoadAlapertelmezett = true;

                DataRow[] defaultRows = result.Ds.Tables[0].Select(string.Format("Alapertelmezett = '{0}' AND ISNULL(Publikus, '0') <> '{0}'", Constants.Database.Yes));
                if (defaultRows.Length > 0)
                    DefaultTemplateId = defaultRows[0]["Id"].ToString();
                else if (needLoadAlapertelmezett)
                    defaultRows = result.Ds.Tables[0].Select(string.Format("Alapertelmezett = '{0}' AND Publikus = '{0}'", Constants.Database.Yes));
                if (defaultRows.Length > 0 && needLoadAlapertelmezett)
                {
                    _defaultTemplateObject = new KRT_UIMezoObjektumErtekek();
                    _defaultTemplateObject.Id = defaultRows[0]["Id"].ToString();
                    _defaultTemplateObject.Nev = defaultRows[0]["Nev"].ToString();
                    _defaultTemplateObject.TemplateXML = defaultRows[0]["TemplateXml"].ToString();
                    _defaultTemplateObject.Alapertelmezett = defaultRows[0]["Alapertelmezett"].ToString();
                    _defaultTemplateObject.Base.Ver = defaultRows[0]["Ver"].ToString();
                    _defaultTemplateObject.Publikus = defaultRows[0]["Publikus"].ToString();
                    _defaultTemplateObject.Felhasznalo_Id = defaultRows[0]["Felhasznalo_Id"].ToString();
                }

                for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                {
                    string templateNev = result.Ds.Tables[0].Rows[i]["Nev"].ToString();
                    string templateId = result.Ds.Tables[0].Rows[i]["Id"].ToString();
                    string publikus = result.Ds.Tables[0].Rows[i]["Publikus"].ToString();
                    string felhasznaloId = result.Ds.Tables[0].Rows[i]["Felhasznalo_Id"].ToString();
                    string alapertelmezett = result.Ds.Tables[0].Rows[i]["Alapertelmezett"].ToString();

                    if (_hasGlobalDefaultSetupRight || !publikus.Equals(Constants.Database.Yes) || !alapertelmezett.Equals(Constants.Database.Yes) || felhasznaloId.Equals(FelhasznaloProfil.FelhasznaloId(Page)))
                        templatesDictionary.Add(templateId, templateNev);
                }

                TemplateDictionary = templatesDictionary;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Resources.Error.ErrorHeader_FillTemplateList);
            }
        }
    }

    /// <summary>
    /// dropdownlist felt�lt�se a templatesDictionary-b�l
    /// </summary> 
    private void fillTemplateDropDownList()
    {
        if (TemplateDictionary != null)
        {
            TemplateList.Items.Clear();
            foreach (KeyValuePair<string,string> kvp in TemplateDictionary)
            {
                string templateId = kvp.Key;
                string templateNev = kvp.Value;

                ListItem newItem = new ListItem(templateNev, templateId);

                if (DefaultTemplateId == templateId)
                    newItem.Attributes.Add("style", defaultTemplateStyle);

                TemplateList.Items.Add(newItem);
            }
        }
    }

    /// <summary>
    /// template-lista lek�r�se adatb�zisb�l, majd a dropdownlist �s a templatelinkek friss�t�se
    /// </summary>
    private void refreshAllTemplateItems()
    {
        fillTemplatesDictionary();
        createLinkButtons();
        fillTemplateDropDownList();
    }

    private object searchObject;

    public object SearchObject
    {
        get { return searchObject; }
        set { searchObject = value; }
    }

    private Type searchObjectType;

    public Type SearchObjectType
    {
        get { return searchObjectType; }
        set { searchObjectType = value; }
    }

    private String customTemplateTipusNev = "";
    /// <summary>
    /// Ha be van �ll�tva, ezt veszi figyelembe a template t�pus nev�n�l
    /// </summary>
    public String CustomTemplateTipusNev
    {
        get { return customTemplateTipusNev; }
        set { customTemplateTipusNev = value; }
    }

    //seg�d f�ggv�ny a t�pus nev�nek meghat�roz�s�ra
    private string GetTemplateTipusNev()
    {
        if (!String.IsNullOrEmpty(CustomTemplateTipusNev))
        {
            return CustomTemplateTipusNev;
        }
        else if (SearchObjectType != null)
        {
            return SearchObjectType.Name;
        }

        return String.Empty;
    }

    public string CurrentTemplateName
    {
        get { return Label_TemplateName.Text; }
        set { Label_TemplateName.Text = value; }
    }

    private string Publikus
    {
        get
        {
            return CheckBoxPublic.Checked ? "1" : "0";
        }
        set
        {
            CheckBoxPublic.Checked = ("1" == value ? true : false);
        }
    }

    //private string currentTemplateId;

    public string CurrentTemplateId
    {
        get { return TemplateId_HiddenField.Value; }
        set { TemplateId_HiddenField.Value = value; }
    }

    public string DefaultTemplateId
    {
        get { return DefaultTemplateId_HiddenField.Value; }
        set { DefaultTemplateId_HiddenField.Value = value; }
    }

    /// <summary>
    /// Az utolj�ra bet�lt�tt/mentett KRT_UIMezoObjektumErtekek rekord verzi�ja
    /// </summary>
    public int CurrentVer_KRT_UIMezoObjektumErtekek
    {
        get {
            try
            {
                return Int32.Parse(Ver_KRT_UIMezoObjektumErtekek_HiddenField.Value);
            }
            catch (FormatException)
            {
                return -1;
            }
        }
        set
        {
            Ver_KRT_UIMezoObjektumErtekek_HiddenField.Value = value.ToString();
        }
    }

    private Contentum.eUIControls.eErrorPanel errorPanel = null;

    public Contentum.eUIControls.eErrorPanel ErrorPanel
    {
        get { return errorPanel; }
        set { errorPanel = value; }
    }

    private void createLinkButtons()
    {
        if (TemplateDictionary != null)
        {
            LinkButtons_Panel.Controls.Clear();
            foreach (KeyValuePair<string,string> kvp in TemplateDictionary)
            {
                string templateId = kvp.Key;
                string templateNev = kvp.Value;

                LinkButton newLinkButton = new LinkButton();
                newLinkButton.Text = templateNev;
                newLinkButton.CssClass = "templateLinkButtons";
                newLinkButton.CommandName = "LoadTemplate";
                newLinkButton.CommandArgument = templateId;
                newLinkButton.ID = "templateLinkButton_" + templateId;
                newLinkButton.Command += new CommandEventHandler(LoadTemplateCommandEvent);
                newLinkButton.ToolTip = "'" + templateNev + "' " + Resources.Search.UI_LoadTemplate_TooltipText;
                newLinkButton.CausesValidation = false;

                Literal space = new Literal();
                space.Text = " ";

                if (DefaultTemplateId == templateId)
                {
                    newLinkButton.Style.Add("background",defaultTemplateBackgroundColor);
                }
                //a kiv�lasztott template kiemelve �s els� helyen
                if (CurrentTemplateId == templateId)
                {
                    newLinkButton.Font.Bold = true;
                    newLinkButton.Font.Underline = true;
                    LinkButtons_Panel.Controls.AddAt(0, space);
                    LinkButtons_Panel.Controls.AddAt(0, newLinkButton);
                }
                else
                {
                    LinkButtons_Panel.Controls.Add(newLinkButton);
                    LinkButtons_Panel.Controls.Add(space);
                }
            }
        }
    }

    public void LoadTemplateCommandEvent(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "LoadTemplate" && e.CommandArgument != null)
        {
            string templateId = e.CommandArgument.ToString();

            LoadSelectedTemplateObject(templateId);
            refreshAllTemplateItems();

            CommandEventArgs args = new CommandEventArgs(CommandName.LoadTemplate, CurrentTemplateId);

            ButtonsClick(this, args);            

        }
    }
        
    public void TemplateReset()
    {
        CurrentTemplateId = "";
        CurrentTemplateName = Resources.Form.UI_NoTemplateLoaded;
        cbNewDefault.Checked = false;
        cbNewGlobalDefault.Checked = false;
        refreshAllTemplateItems();
    }

    public void NewTemplate(object newSearchObject)
    {
        searchObject = newSearchObject;
        if (searchObject is BaseSearchObject)
        {
            (searchObject as BaseSearchObject).ReadableWhere = Search.GetReadableWhere(Page.Form);
        }
        string alapertelmezett = (cbNewDefault.Checked || cbNewGlobalDefault.Checked) ? Constants.Database.Yes : Constants.Database.No;
        string publikus = cbNewGlobalDefault.Checked ? Constants.Database.Yes : Constants.Database.No;
        string szerverzetId = cbNewGlobalDefault.Checked ? FelhasznaloProfil.FelhasznaloSzerverzetId(Page) : null;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        string newId = UIMezoObjektumErtekek.Insert(execParam, newSearchObject, this.GetTemplateTipusNev(), CurrentTemplateName, alapertelmezett, publikus, szerverzetId, ErrorPanel);
        if (String.IsNullOrEmpty(newId))
        {
            TemplateReset();
        }
        else
        {
            cbNewDefault.Checked = false;
            cbNewGlobalDefault.Checked = false;
            CurrentTemplateId = newId;
            if (alapertelmezett == Constants.Database.Yes)
                DefaultTemplateId = newId;
            CurrentVer_KRT_UIMezoObjektumErtekek = 1;
            TemplateDictionary.Add(CurrentTemplateId, CurrentTemplateName);
            refreshAllTemplateItems();
        }
    }

    public void SaveCurrentTemplate(object newSearchObject)
    {        
        searchObject = newSearchObject;
        if (searchObject is BaseSearchObject)
        {
            (searchObject as BaseSearchObject).ReadableWhere = Search.GetReadableWhere(Page.Form);
        }
        string Alapertelmezett = (CurrentTemplateId == DefaultTemplateId) ? Constants.Database.Yes : Constants.Database.No;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = CurrentTemplateId;

        #region CR3194 - Kisherceg megoszt�s
        KRT_UIMezoObjektumErtekek _KRT_UIMezoObjektumErtekek = new KRT_UIMezoObjektumErtekek();
        using (Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService _KRT_UIMezoObjektumErtekekService = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService())
        {
            Result res = _KRT_UIMezoObjektumErtekekService.Get(execParam);
            _KRT_UIMezoObjektumErtekek = (KRT_UIMezoObjektumErtekek)res.Record;
        }
        //KRT_UIMezoObjektumErtekek _KRT_UIMezoObjektumErtekek =
        //    UIMezoObjektumErtekek.Get(execParam, ErrorPanel);
        if (_KRT_UIMezoObjektumErtekek.Publikus == "1" && _KRT_UIMezoObjektumErtekek.Felhasznalo_Id != execParam.Felhasznalo_Id)
        {
            TemplateReset();
            errorPanel.Header = Resources.Error.ErrorHeader_ModifyTemplate;
            errorPanel.Body = Resources.Error.UIPublicTemplateNotModifyable;
            errorPanel.Visible = true;
        }
        else
        {

            Contentum.eUtility.ResultError resultError = UIMezoObjektumErtekek.Update(execParam,
                        newSearchObject, CurrentVer_KRT_UIMezoObjektumErtekek, Alapertelmezett);

            if (resultError.IsError)
            {
                TemplateReset();
                errorPanel.Header = Resources.Error.ErrorHeader_ModifyTemplate;
                errorPanel.Body = resultError.ErrorMessage;
                errorPanel.Visible = true;
            }
            else
            {
                CurrentVer_KRT_UIMezoObjektumErtekek += 1;
            }
        }
        #endregion
    }	

    public event CommandEventHandler ButtonsClick;

    protected void Button_Click(object sender, EventArgs e)
    {
        if (ButtonsClick != null)
        {
            string commandName = (sender as Button).CommandName;          

            CommandEventArgs args = null;
            if (commandName == CommandName.LoadTemplate)
            {               
                
                LoadSelectedTemplateObject();
                refreshAllTemplateItems();

                args = new CommandEventArgs(commandName, CurrentTemplateId);
            }
            else if (commandName == CommandName.NewTemplate)
            {
                string newTemplateName = NewTemplateTextBox.Text;
                NewTemplateTextBox.Text = "";
                CurrentTemplateName = newTemplateName;
                args = new CommandEventArgs(commandName, newTemplateName);
            }
            else if (commandName == CommandName.SaveTemplate)
            {
                args = new CommandEventArgs(commandName, CurrentTemplateId);
            }
            else if (commandName == CommandName.InvalidateTemplate)
            {
                //string invalidatedId = CurrentTemplateId;

                // A dropdown list�ban kiv�lasztott template-et t�r�lj�k
                string invalidatedId = TemplateList.SelectedValue;
                
                InvalidateSelectedTemplate();

                args = new CommandEventArgs(commandName, invalidatedId);
                
            }
            #region CR3194 - Kisherceg megoszt�s
            else if (commandName == CommandName.Share)
            {
                ShareSelectedTemplate();
                args = new CommandEventArgs(commandName, TemplateList.SelectedValue);
            }
            #endregion CR3194 - Kisherceg megoszt�s
            if(args != null)
                ButtonsClick(this, args);
        }
    }
    #region CR3194 - Kisherceg megoszt�s
    private void ShareSelectedTemplate()
    { 
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = TemplateList.SelectedValue;

        KRT_UIMezoObjektumErtekek _KRT_UIMezoObjektumErtekek =
            UIMezoObjektumErtekek.Get(execParam, ErrorPanel);

        _KRT_UIMezoObjektumErtekek.Org_Id = FelhasznaloProfil.OrgId(Page);
        _KRT_UIMezoObjektumErtekek.Szervezet_Id = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
        if (!CheckBoxPublic.Checked)
        {            
            _KRT_UIMezoObjektumErtekek.Publikus = "0";
            _KRT_UIMezoObjektumErtekek.Felhasznalo_Id = FelhasznaloCsoportTextBox.Id_HiddenField;
        }
        else 
        {
            _KRT_UIMezoObjektumErtekek.Publikus = "1";
        }
        Result result = UIMezoObjektumErtekek.Share(execParam, _KRT_UIMezoObjektumErtekek);
        if (result.IsError)
        {
            errorPanel.Header = Resources.Error.ErrorLabel;
            errorPanel.Body = Resources.Error.ErrorHeader_ModifyTemplate;
            errorPanel.Visible = true;
            
            //ResultError.DisplayErrorOnErrorPanel(ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.ErrorHeader_ModifyTemplate);
            //ErrorUpdatePanel1.Update();
        }
    }
    #endregion
    private void InvalidateSelectedTemplate()
    {
        string selectedTemplateId = TemplateList.SelectedValue;

        if (!String.IsNullOrEmpty(selectedTemplateId))
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = selectedTemplateId;

            #region CR3194 - Kisherceg megoszt�s
            KRT_UIMezoObjektumErtekek _KRT_UIMezoObjektumErtekek = new KRT_UIMezoObjektumErtekek();
            using (Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService _KRT_UIMezoObjektumErtekekService = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService())
            {
                Result res = _KRT_UIMezoObjektumErtekekService.Get(execParam);
                _KRT_UIMezoObjektumErtekek = (KRT_UIMezoObjektumErtekek)res.Record;
            }
            //KRT_UIMezoObjektumErtekek _KRT_UIMezoObjektumErtekek =
            //    UIMezoObjektumErtekek.Get(execParam, ErrorPanel);
            if (_KRT_UIMezoObjektumErtekek.Publikus == "1" && _KRT_UIMezoObjektumErtekek.Felhasznalo_Id != execParam.Felhasznalo_Id)
            {
                TemplateReset();
                errorPanel.Header = Resources.Error.ErrorHeader_InvalidateTemplate;
                errorPanel.Body = Resources.Error.UIPublicTemplateNotInvalidateable;
                errorPanel.Visible = true;
            }
            else
            {

                Contentum.eUtility.ResultError resultError = UIMezoObjektumErtekek.Invalidate(execParam);
                if (resultError.IsError)
                {
                    TemplateReset();
                    errorPanel.Header = Resources.Error.ErrorHeader_InvalidateTemplate;
                    errorPanel.Body = resultError.ErrorMessage;
                    errorPanel.Visible = true;
                }
                else
                {
                    TemplateDictionary.Remove(selectedTemplateId);
                    if (CurrentTemplateId == selectedTemplateId)
                    {
                        TemplateReset();
                    }
                    else
                    {
                        refreshAllTemplateItems();
                    }
                }
            }
            #endregion
        }
    }

    public void LoadTemplateObjectById(string templateId)
    {
        LoadSelectedTemplateObject(templateId);
    }

    private void LoadSelectedTemplateObject()
    {
        string id = "";
        string templateName = "";
        if (TemplateList.SelectedItem != null)
        {
            id = TemplateList.SelectedItem.Value;
            templateName = TemplateList.SelectedItem.Text;
        }
        LoadSelectedTemplateObject(id, templateName);
    }

    private void LoadSelectedTemplateObject(String templateId)
    {
        string templateName = "";
        if (TemplateList != null)
        {
            ListItem listItem = TemplateList.Items.FindByValue(templateId);
            if (listItem != null)
            {
                templateName = listItem.Text;
            }
        }
        LoadSelectedTemplateObject(templateId, templateName);
    }

    private void LoadSelectedTemplateObject(String templateId,String templateName)
    {
        string id = templateId;
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;
                
        KRT_UIMezoObjektumErtekek _KRT_UIMezoObjektumErtekek = 
            UIMezoObjektumErtekek.Get(execParam,ErrorPanel);

        LoadSelectedTemplateObject(_KRT_UIMezoObjektumErtekek);
    }

    private void LoadSelectedTemplateObject(KRT_UIMezoObjektumErtekek objektumErtekek)
    {
        if (objektumErtekek != null)
        {
            if (_hasGlobalDefaultSetupRight || !objektumErtekek.Publikus.Equals(Constants.Database.Yes) || !objektumErtekek.Alapertelmezett.Equals(Constants.Database.Yes) || objektumErtekek.Felhasznalo_Id.Equals(FelhasznaloProfil.FelhasznaloId(Page)))
            {
                CurrentTemplateId = objektumErtekek.Id;
                CurrentTemplateName = objektumErtekek.Nev;
                CurrentVer_KRT_UIMezoObjektumErtekek = Int32.Parse(objektumErtekek.Base.Ver);
                if (objektumErtekek.Alapertelmezett == Constants.Database.Yes)
                    DefaultTemplateId = CurrentTemplateId;
            }

            Publikus = objektumErtekek.Publikus;

            SearchObject = XmlFunction.XmlToObject(objektumErtekek.TemplateXML, searchObjectType);
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(ErrorPanel, Resources.Error.ErrorHeader_LoadTemplate, "");
        }
    }

    private void LoadDefaultTemplateObject()
    {
        if (_defaultTemplateObject != null)
        {
            #region utols� haszn�lat idej�nek m�dos�t�sa

            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            xpm.Record_Id = _defaultTemplateObject.Id;

            int currentVersion = Int32.Parse(_defaultTemplateObject.Base.Ver);

            Contentum.eUtility.ResultError re = Contentum.eUtility.UIMezoObjektumErtekek.UpdateUtolsoHasznalatIdo(xpm, currentVersion);

            if (re.IsError)
            {
                ResultError.DisplayErrorOnErrorPanel(errorPanel,re.ErrorHeader, re.ErrorMessage);
            }
            else
            {
                int newVer = currentVersion + 1; ;
                _defaultTemplateObject.Base.Ver = newVer.ToString();
            }

            #endregion

            LoadSelectedTemplateObject(_defaultTemplateObject);

            CommandEventArgs args = new CommandEventArgs(CommandName.LoadTemplate, _defaultTemplateObject.Id);
            ButtonsClick(this, args);

            _defaultTemplateObject = null;
        }
    }

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.TemplateLoaderBehavior", this.EFormPanel1.ClientID);
        descriptor.AddProperty("TemplateListId", this.TemplateList.ClientID);
        descriptor.AddProperty("SetDefaultButtonId", this.SetDefaultButton.ClientID);
        descriptor.AddProperty("RemoveDefaultButtonId", this.RemoveDefaultButton.ClientID);
        descriptor.AddProperty("DefaultTemplateBackgroundColor", defaultTemplateBackgroundColor);
        descriptor.AddProperty("UpdateAlapertelmezettMethod", updateAlapertelmezettMethod);
        descriptor.AddProperty("ServicePath", servicePath);
        descriptor.AddProperty("FelhasznaloId",FelhasznaloProfil.FelhasznaloId(Page));
        descriptor.AddProperty("DefaultTemplateHiddenFieldId", DefaultTemplateId_HiddenField.ClientID);
        descriptor.AddProperty("CurrentTemplateHiddenFieldId", TemplateId_HiddenField.ClientID);
        descriptor.AddProperty("CurrentVersionHiddenFieldId", Ver_KRT_UIMezoObjektumErtekek_HiddenField.ClientID);
        descriptor.AddProperty("LabelDefaultId",labelDefault.ClientID);
        descriptor.AddProperty("LinkButtonsPanelId",LinkButtons_Panel.ClientID);

        return new ScriptDescriptor[] { descriptor };
    }

    public IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/TemplateLoader.js";

        return new ScriptReference[] { reference };
    }

    #endregion

    /// <summary>
    /// SetPublikusSablonLathatosag
    /// </summary>
    private void SetPublikusSablonLathatosag()
    {
        if (Contentum.eUtility.FunctionRights.GetFunkcioJog(Page, "PublikusSablonNew"))
        {
            CheckBoxPublic.Enabled = true;
            CheckBoxPublic.ToolTip = Resources.Form.SablonPublikus;
        }
        else
        {
            CheckBoxPublic.Enabled = false;
            CheckBoxPublic.ToolTip = Resources.Form.SablonPublikusDisabled;
        }
    }
}
