﻿using Contentum.eAdmin.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_FuggoKodtarakDropDownList : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region cascading properties

    public string KodcsoportKod
    {
        get
        {
            return ViewState["KodcsoportKod"] as string;
        }

        set
        {
            ViewState["KodcsoportKod"] = value;
        }
    }

    private string _ParentControlId;

    public string ParentControlId
    {
        get
        {
            return _ParentControlId;
        }
        set
        {
            _ParentControlId = value;
        }
    }

    private string ContextKey
    {
        get
        {
            return Kodtarak_CascadingDropDown.ContextKey;
        }
        set
        {
            Kodtarak_CascadingDropDown.ContextKey = value;
        }
    }

    public DropDownList DropDownList
    {
        get
        {
            return Kodtarak_DropDownList;
        }
    }

    #endregion

    public string SelectedValue
    {
        get { return GetSelectedValue(); }
        set { SetSelectedValue(value); }
    }

    const string separator = ":::";

    string GetSelectedValue()
    {
        string value = Kodtarak_CascadingDropDown.SelectedValue;

        if (!String.IsNullOrEmpty(value))
        {
            return value.Split(new string[] { separator }, StringSplitOptions.None)[0];
        }

        return value;
    }

    void SetSelectedValue(string value)
    {
        Kodtarak_CascadingDropDown.SelectedValue = value;
    }

    string GetKodNev(string kod)
    {
        if (String.IsNullOrEmpty(this.KodcsoportKod))
        {
            return kod;
        }
        else
        {
            string nev = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(this.KodcsoportKod, kod, Page);

            return nev;
        }
    }

    public string Text
    {
        get
        {
            if (Kodtarak_DropDownList.SelectedItem != null)
            {
                return Kodtarak_DropDownList.SelectedItem.Text;
            }
            else
            {
                return "";
            }
        }
    }

    public bool ReadOnly
    {
        get { return Kodtarak_DropDownList.ReadOnly; }
        set { Kodtarak_DropDownList.ReadOnly = value; Kodtarak_CascadingDropDown.Enabled = !value; }
    }

    public bool Enabled
    {
        get { return Kodtarak_DropDownList.Enabled; }
        set { Kodtarak_DropDownList.Enabled = value; Kodtarak_CascadingDropDown.Enabled = value; }
    }

    public string CssClass
    {
        get { return Kodtarak_DropDownList.CssClass; }
        set { Kodtarak_DropDownList.CssClass = value; }
    }

    public bool Validate
    {
        get { return RequiredFieldValidator1.Enabled; }
        set { RequiredFieldValidator1.Enabled = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    void ResolveSelectedValue()
    {
        if (!String.IsNullOrEmpty(this.SelectedValue))
        {
            string kod = this.SelectedValue;
            string nev = GetKodNev(kod);

            ListItem selectedItem = new ListItem(nev, kod);
            selectedItem.Selected = true;
            Kodtarak_DropDownList.Items.Clear();
            Kodtarak_DropDownList.Items.Add(selectedItem);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        Kodtarak_CascadingDropDown.Category = this.KodcsoportKod;
        this.ResolveSelectedValue();
        if (!String.IsNullOrEmpty(this.ParentControlId))
        {
            Component_FuggoKodtarakDropDownList parentControl = this.FindControlHelper(this.ParentControlId) as Component_FuggoKodtarakDropDownList;

            if (parentControl != null)
            {
                this.ContextKey = parentControl.KodcsoportKod;
                Kodtarak_CascadingDropDown.ParentControlID = parentControl.UniqueID + "$" + parentControl.DropDownList.ID;
            }
        }
        base.OnPreRender(e);
    }

    protected Control FindControlHelper(string id)
    {
        Control c = base.FindControl(id);
        Control nc = NamingContainer;

        while ((null == c) && (null != nc))
        {
            c = nc.FindControl(id);
            nc = nc.NamingContainer;
        }

        return c;
    }

    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(Kodtarak_DropDownList);

        return componentList;
    }

    private bool _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            if (!_ViewEnabled)
            {
                ReadOnly = !_ViewEnabled;
                DropDownList.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    bool _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                DropDownList.CssClass += " ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}