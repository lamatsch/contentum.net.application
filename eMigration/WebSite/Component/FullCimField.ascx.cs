using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class Component_FullCimField : System.Web.UI.UserControl, IScriptControl, Contentum.eUtility.ISelectableUserComponent
{
    private string getFullCim()
    {
        string irsz = IranyitoszamTextBox1.Text.Trim();
        string telepules = TelepulesTextBox1.Text.Trim();
        string kozterulet = KozteruletTextBox1.Text.Trim();
        string kozteruletTipus = KozteruletTipusTextBox1.Text.Trim();
        string cimTobbi = TobbiTextBox.Text.Trim();

        string cim = irsz;

        if (!String.IsNullOrEmpty(telepules))
        {
            if (!String.IsNullOrEmpty(cim))
            {
                cim += " ";
            }

            cim += telepules;
        }

        if (!String.IsNullOrEmpty(kozterulet))
        {
            if (!String.IsNullOrEmpty(cim))
            {
                cim += " ";
            }

            cim += kozterulet;
        }

        if (!String.IsNullOrEmpty(kozteruletTipus))
        {
            if (!String.IsNullOrEmpty(cim))
            {
                cim += " ";
            }

            cim += kozteruletTipus;
        }

        if (!String.IsNullOrEmpty(cimTobbi))
        {
            if (!String.IsNullOrEmpty(cim))
            {
                cim += " ";
            }

            cim += cimTobbi;
        }

        return cim;

    }

    public string Text
    {
        get { return getFullCim(); }
    }

    #region cim

    public bool IsOrszagVisible
    {
        get
        {
            return OrszagTextBox1.Visible;
        }
        set
        {
            OrszagTextBox1.Visible = value;
        }
    }

    private event EventHandler _OrszagTextChanged;

    public event EventHandler OrszagTextChanged
    {
        add
        {
            this._OrszagTextChanged += value;
        }
        remove
        {
            this._OrszagTextChanged -= value;
        }
    }

    private void RaiseOrszagTextChanged()
    {
        if (this._OrszagTextChanged != null)
        {
            this._OrszagTextChanged(this.OrszagTextBox1.TextBox, EventArgs.Empty);
        }
    }

    private static void LoadBusinessDocumentFromDataRow(object BusinessDocument, DataRow row)
    {
        try
        {
            if (row != null)
            {
                System.Reflection.PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                foreach (System.Reflection.PropertyInfo P in Properties)
                {
                    if (row.Table.Columns.Contains(P.Name))
                    {
                        if (!(row.IsNull(P.Name)))
                        {
                            P.SetValue(BusinessDocument, row[P.Name].ToString(), null);
                        }
                    }
                }
                System.Reflection.FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                object BaseObject = BaseField.GetValue(BusinessDocument);

                System.Reflection.PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                foreach (System.Reflection.PropertyInfo P in BaseProperties)
                {
                    if (row.Table.Columns.Contains(P.Name))
                    {
                        if (!(row.IsNull(P.Name)))
                        {
                            P.SetValue(BaseObject, row[P.Name].ToString(), null);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public KRT_Orszagok Orszag
    {
        get
        {
            KRT_Orszagok orszag = new KRT_Orszagok();

            if (String.IsNullOrEmpty(OrszagTextBox1.Text))
            {
                orszag.Nev = String.Empty;
                return orszag;
            }

            KRT_OrszagokService svc = eAdminService.ServiceFactory.GetKRT_OrszagokService();
            ExecParam xpm = UI.SetExecParamDefault(Page);
            KRT_OrszagokSearch sch = new KRT_OrszagokSearch();
            sch.Nev.Value = OrszagTextBox1.Text;
            sch.Nev.Operator = Query.Operators.equals;

            Result res = svc.GetAll(xpm, sch);

            if (!res.IsError)
            {
                if (res.Ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = res.Ds.Tables[0].Rows[0];
                    LoadBusinessDocumentFromDataRow(orszag, row);
                    return orszag;
                    
                }
            }
            orszag.Nev = OrszagTextBox1.Text;
            return orszag;

        }
    }

    private TextBox cimTextBox = null;

    public TextBox CimTextBox
    {
        get { return cimTextBox; }
        set { cimTextBox = value; }
    }

    public string CimTextBoxClientId
    {
        get
        {
            if (cimTextBox != null)
                return cimTextBox.ClientID;
            else
                return String.Empty;
        }
    }

    private HiddenField cimHiddenField = null;

    public HiddenField CimHiddenField
    {
        get { return cimHiddenField; }
        set { cimHiddenField = value; }
    }

    public string CimHiddenFieldClientId
    {
        get
        {
            if (cimHiddenField != null)
                return cimHiddenField.ClientID;
            else
                return String.Empty;
        }
    }

    // BUG_4731
    private TextBox cimTextBoxClone = null;

    public TextBox CimTextBoxClone
    {
        get { return cimTextBoxClone; }
        set { cimTextBoxClone = value; }
    }

    public string CimTextBoxCloneClientId
    {
        get
        {
            if (cimTextBoxClone != null)
                return cimTextBoxClone.ClientID;
            else
                return String.Empty;
        }
    }

    private HiddenField cimHiddenFieldClone = null;

    public HiddenField CimHiddenFieldClone
    {
        get { return cimHiddenFieldClone; }
        set { cimHiddenFieldClone = value; }
    }

    public string CimHiddenFieldCloneClientId
    {
        get
        {
            if (cimHiddenFieldClone != null)
                return cimHiddenFieldClone.ClientID;
            else
                return String.Empty;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //Contentum.eUtility.JavaScripts.RegisterCimScript(Page, IranyitoszamTextBox1.TextBox.ClientID,IranyitoszamTextBox1.AutoCompleteExtenderClientID,  
        //    TelepulesTextBox1.TextBox.ClientID, TelepulesTextBox1.AutoCompleteExtenderClientID, 
        //    KozteruletTextBox1.TextBox.ClientID, KozteruletTipusTextBox1.TextBox.ClientID,TobbiTextBox.ClientID, CimTextBoxClientId, CimHiddenFieldClientId);

        OrszagTextBox1.TextBox.AutoPostBack = true;
        OrszagTextBox1.TextBox.TextChanged += new EventHandler(OrszagTextBox_TextChanged);
    }

    void OrszagTextBox_TextChanged(object sender, EventArgs e)
    {
        RaiseOrszagTextChanged();
    }

    #region IScriptControl Members

    private ScriptManager sm;
    private bool ajaxEnabled = true;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.CimManager", this.IranyitoszamTextBox1.TextBox.ClientID);
        descriptor.AddProperty("OrszagTextBoxClientId", this.OrszagTextBox1.TextBox.ClientID);
        descriptor.AddProperty("IranyitoszamTextBoxClientId", this.IranyitoszamTextBox1.TextBox.ClientID);
        descriptor.AddProperty("IranyitoszamAutoCompleteClientId", this.IranyitoszamTextBox1.AutoCompleteExtenderClientID);
        descriptor.AddProperty("TelepulesTextBoxClientId", this.TelepulesTextBox1.TextBox.ClientID);
        descriptor.AddProperty("TelepulesAutoCompleteClientId", this.TelepulesTextBox1.AutoCompleteExtenderClientID);
        descriptor.AddProperty("KozteruletTextBoxClientId", this.KozteruletTextBox1.TextBox.ClientID);
        descriptor.AddProperty("KozteruletTipusTextBoxClientId", this.KozteruletTipusTextBox1.TextBox.ClientID);
        descriptor.AddProperty("TobbiTextBoxClientId", this.TobbiTextBox.ClientID);
        descriptor.AddProperty("CimTextBoxClientId", this.CimTextBoxClientId);
        descriptor.AddProperty("CimHiddenFieldClientId", this.CimHiddenFieldClientId);
        // BUG_4731
        descriptor.AddProperty("CimTextBoxCloneClientId", this.CimTextBoxCloneClientId);
        descriptor.AddProperty("CimHiddenFieldCloneClientId", this.CimHiddenFieldCloneClientId);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/Cim.js";

        return new ScriptReference[] { reference };
    }

    #endregion

    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        ajaxEnabled = false;
        componentList.AddRange(IranyitoszamTextBox1.GetComponentList());
        componentList.AddRange(TelepulesTextBox1.GetComponentList());
        componentList.AddRange(KozteruletTextBox1.GetComponentList());
        componentList.AddRange(KozteruletTipusTextBox1.GetComponentList());
        componentList.Add(TobbiTextBox);

        return componentList;
    }

    private bool _ViewEnabled = true;
    public bool ViewEnabled
    {
        get
        {
            return _ViewEnabled;
        }
        set
        {
            _ViewEnabled = value;
            IranyitoszamTextBox1.ViewEnabled = value;
            TelepulesTextBox1.ViewEnabled = value;
            KozteruletTextBox1.ViewEnabled = value;
            KozteruletTipusTextBox1.ViewEnabled = value;
            TobbiTextBox.ReadOnly = !value;
        }
    }

    Boolean _ViewVisible = true;
    public bool ViewVisible
    {
        get
        {
            return _ViewVisible;
        }
        set
        {
            _ViewVisible = value;
            IranyitoszamTextBox1.ViewVisible = value;
            TelepulesTextBox1.ViewVisible = value;
            KozteruletTextBox1.ViewVisible = value;
            KozteruletTipusTextBox1.ViewVisible = value;
            TobbiTextBox.Enabled = value;
        }
    }

    #endregion
}
