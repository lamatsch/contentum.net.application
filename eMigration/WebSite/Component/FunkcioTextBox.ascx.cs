using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Component_FunkcioTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        get
        {
            return Validator1.ValidationGroup;
        }
        set
        {
            Validator1.ValidationGroup = value;
        }
    }

    public bool Enabled
    {
        set
        {
            FunkcioNev.Enabled = value;
            LovImageButton.Enabled = value;            
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            AutoCompleteExtenderFunkcio.Enabled = !value;
            if (value == false) 
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return FunkcioNev.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        {
            _readonly = value;
            ViewState["ReadOnly"] = value;

        }
        get 
        {
            object o = ViewState["ReadOnly"];
            if (o != null)
                return (bool)o;
            return false;
        }
    }

    private bool _readonly
    {
        set
        {
            FunkcioNev.ReadOnly = value;
            NewImageButton.Enabled = !value;
            LovImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            AutoCompleteExtenderFunkcio.Enabled = !value;
            //Validate = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
                Validate = false;
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }
        }
        get
        {
            return FunkcioNev.ReadOnly;
        }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public ImageButton ImageButton_Reset
    {
        get
        {
            return ResetImageButton;
        }
    }

    public string Text
    {
        set { FunkcioNev.Text = value; }
        get { return FunkcioNev.Text; }
    }

    public TextBox TextBox
    {
        get { return FunkcioNev; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;            
            if (!_ViewEnabled)
            {
                ReadOnly = !_ViewEnabled;
                FunkcioNev.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;           
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                FunkcioNev.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }

    private ScriptManager sm;
    private bool _AjaxEnabled = false;

    public bool AjaxEnabled
    {
        get 
        {
            if (ViewState["AjaxEnabled"] == null)
                return _AjaxEnabled;

            return (bool) ViewState["AjaxEnabled"];
        }
        set 
        { 
            ViewState["AjaxEnabled"] = value; 
        }
    }

    private bool _FeladatMode = false;

    public bool FeladatMode
    {
        get
        {
            if (ViewState["FeladatMode"] == null)
                return _FeladatMode;

            return (bool)ViewState["FeladatMode"];
        }
        set
        {
            ViewState["FeladatMode"] = value;
            // nem lehet egyidej�leg bekapcsolva
            if (value == true)
            {
                FeladatDefinicioMode = false;
            }
        }
    }

    private bool _FeladatDefinicioMode = false;

    public bool FeladatDefinicioMode
    {
        get
        {
            if (ViewState["FeladatDefinicioMode"] == null)
                return _FeladatDefinicioMode;

            return (bool)ViewState["FeladatDefinicioMode"];
        }
        set
        {
            ViewState["FeladatDefinicioMode"] = value;
            // nem lehet egyidej�leg bekapcsolva
            if (value == true)
            {
                FeladatMode = false;
            }
        }
    }

    public string BehaviorID
    {
        get { return this.TextBox.ClientID + "_FunkcioBehavior";}
    }

    public string ObjektumTipusFilter
    {
        get { return HiddenFieldObjektumTipusFilter.Value; }
        set { HiddenFieldObjektumTipusFilter.Value = value; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetFunkcioTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Funkciok krt_funkciok = (KRT_Funkciok)result.Record;
                Text = krt_funkciok.Nev;
                if (FeladatMode)
                {
                    SetObjektumTipus(krt_funkciok, errorPanel);
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
        }
    }

    private void SetObjektumTipus(KRT_Funkciok krt_funkciok, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(krt_funkciok.ObjTipus_Id_AdatElem))
        {
            KRT_ObjTipusokService objService = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = krt_funkciok.ObjTipus_Id_AdatElem;
            Result res = objService.Get(execParam);
            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, res);
            }
            else
            {
                KRT_ObjTipusok objTipus = (KRT_ObjTipusok)res.Record;
                HiddenFieldObjektumTipus.Value = objTipus.Kod;
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //OnClick = JavaScripts.SetOnClientClick("FunkciokLovList.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, FunkcioNev.ClientID);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string qs = String.Empty;
        if (AjaxEnabled || FeladatMode || FeladatDefinicioMode)
        {
            qs = "&" + QueryStringVars.ObjektumTipusKod + "=" + "' + $find('" + this.BehaviorID + "').get_ObjektumTipusKodFilter() + '";
        }
        else
        {
            qs = "&" + QueryStringVars.ObjektumTipusKod + "=" + ObjektumTipusFilter;
        }

        if (FeladatMode)
        {
            OnClick_Lov = JavaScripts.SetOnClientClick("FunkciokLovList.aspx",
            QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + FunkcioNev.ClientID
            + "&" + QueryStringVars.TipusHiddenFieldId + "=" + HiddenFieldObjektumTipus.ClientID
            + "&FeladatMode=1" + qs
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
        }
        else if (FeladatDefinicioMode)
        {
            OnClick_Lov = JavaScripts.SetOnClientClick("FunkciokLovList.aspx",
            QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + FunkcioNev.ClientID
            + "&" + QueryStringVars.TipusHiddenFieldId + "=" + HiddenFieldObjektumTipus.ClientID
            + "&FeladatDefinicioMode=1" + qs
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
        }
        else
        {
            OnClick_Lov = JavaScripts.SetOnClientClick("FunkciokLovList.aspx",
               QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
               + "&" + QueryStringVars.TextBoxId + "=" + FunkcioNev.ClientID
               + qs
             , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
        }

        OnClick_New = JavaScripts.SetOnClientClick("FunkciokForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + FunkcioNev.ClientID
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "FunkciokForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        ResetImageButton.Visible = (!Validate && !_readonly);
        //ASP.NET 2.0 bug work around
        if (!AjaxEnabled)
        {
            TextBox.Attributes.Add("readonly", "readonly");
            AutoCompleteExtenderFunkcio.Enabled = false;
        }
        else
        {
            String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);

            AutoCompleteExtenderFunkcio.ContextKey = felh_Id;

            if (FeladatMode)
            {
                AutoCompleteExtenderFunkcio.ContextKey += ";FeladatMode";
            }
            else if (FeladatDefinicioMode)
            {
                AutoCompleteExtenderFunkcio.ContextKey += ";FeladatDefinicioMode";
            }
            else
            {
                AutoCompleteExtenderFunkcio.ContextKey += ";NormalMode";
            }
            if (!String.IsNullOrEmpty(ObjektumTipusFilter))
            {
                AutoCompleteExtenderFunkcio.ContextKey += ";" + ObjektumTipusFilter;
            }
        }

        if (FeladatMode || FeladatDefinicioMode)
        {
            NewImageButton.Visible = false;
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }
    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(FunkcioNev);
        componentList.Add(LovImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ResetImageButton);

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";
        
        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    protected override void OnPreRender(EventArgs e)
    {
        if (AjaxEnabled || FeladatMode || FeladatDefinicioMode)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");

                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (AjaxEnabled || FeladatMode || FeladatDefinicioMode)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    #region IScriptControl Members

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.FunkciokBehavior", this.TextBox.ClientID);
        descriptor.AddProperty("id", this.BehaviorID);
        descriptor.AddProperty("AutoCompleteExtenderId", this.AutoCompleteExtenderFunkcio.ClientID);
        descriptor.AddProperty("HiddenFieldId", this.HiddenField1.ClientID);
        descriptor.AddProperty("TipusHiddenFieldId", this.HiddenFieldObjektumTipus.ClientID);
        descriptor.AddProperty("TipusHiddenFieldFilterId", this.HiddenFieldObjektumTipusFilter.ClientID);
        descriptor.AddProperty("FeladatMode", FeladatMode);
        descriptor.AddProperty("FeladatDefinicioMode", FeladatDefinicioMode);
        //descriptor.AddProperty("CustomValueEnabled", this.CustomValueEnabled);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/Funkciok.js";

        return new ScriptReference[] { reference };
    }

    #endregion
}
