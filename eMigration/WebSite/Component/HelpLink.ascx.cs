using System;

using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.IO;
using Contentum.eUtility;

public partial class Component_HelpLink : System.Web.UI.UserControl
{
    // Megjegyz�s:
    // a finomabb k�rnyezet�rz�kel�s �rdek�ben a k�vetkez� elnevez�si konvenci�t haszn�ljuk:
    // az aktu�lis oldal neve
    // + a k�vetkez� priorit�ssal az esetleges QueryStringVars:
    // Startup, Mode
    // valamint ha az alap f�jln�vvel sem tal�lhat� oldal, a f�jln�v konvenci�k szerinti v�gz�d�s�t is megvizsg�ljuk
    #region Settings

    const string helpDirectoryName = "Help";
    const string helpFileExtension = ".htm";
    const string helpFileNotFound = "Az oldalhoz nem tartozik s�g� f�jl.";
    // azon f�jln�v v�gz�d�sek, melyekhez �ltal�nos help tartozhat
    // (fontos, hogy az egym�st a v�g�k�n tartalmaz� nevek k�z�l a r�videbb h�tr�bb �lljon)
    const string helpGeneralNames = "Search;LovList;List;PrintForm;Form";

    #endregion

    #region Public Properties

    public string ImageSrc
    {
        get { return help.Src; }
        set { help.Src = value; }
    }

    public string SwapImageOutSrc
    {
        get { return SwapImageOutSrc_HiddenField.Value; }
        set
        {
            SwapImageOutSrc_HiddenField.Value = value;
            try
            {
                help.Attributes.Remove("onMouseOut");
            }
            finally
            {
                help.Attributes.Add("onMouseOut", "swapByName(this.id,'" + value + "')");
            }
        }
    }

    public string SwapImageOverSrc
    {
        get { return SwapImageOverSrc_HiddenField.Value; }
        set
        {
            SwapImageOverSrc_HiddenField.Value = value;
            try
            {
                help.Attributes.Remove("onMouseOver");
            }
            finally
            {
                help.Attributes.Add("onMouseOver", "swapByName(this.id,'" + value + "')");
            }
        }
    }

    #endregion Public Properties
    #region Utils


    /// <summary>
    /// A QueryString elemz�s�b�l el��ll�tja azon lehets�ges f�jln�v kieg�sz�t�seket,
    /// melyeket az adott oldal nev�hez f�zve esetleg pontosabban meghat�rozhatjuk a
    /// relev�ns help f�jlt. A legfinomabb megk�l�nb�ztet�st vizsg�ljuk legel�sz�r,
    /// a param�terek k�z�tt priorit�si sorrendet haszn�lunk (Startup, Mode).
    /// V�g�l az elnevez�si konvenci�k szerinti f�jln�v v�gz�d�s illeszt�s�vel, ha van tal�lat
    /// az esetleges �ltal�nos le�r�st tartalmaz� f�jlnevet is gener�lunkl.
    /// </summary>
    /// <returns></returns>
    private List<string> GetPossibleHelpFileNames(string helpBaseFileName)
    {
        List<string> extensions = new List<string>();
        List<string> filenames = new List<string>();
        string mode = Request.QueryString.Get(Contentum.eUtility.QueryStringVars.Mode);
        string startup = Request.QueryString.Get(Contentum.eUtility.QueryStringVars.Startup);
        string command = Request.QueryString.Get(Contentum.eUtility.QueryStringVars.Command);

        // a priorit�sok ford�tott sorrendj�ben kell a v�gz�d�seket gener�lni, h�tulr�l el�re �sszef�zve,
        // majd a v�g�n f�zz�k el� a f�jlnevet:
        // pl: s, c, m priorit�si sorrend v�gz�d�sei gener�lva:
        // _s_c_m
        // _s_c
        // _s_m
        // _s
        // _c_m
        // _c
        // _m
        // <"">

        extensions.Add(""); // ezzel az eredeti f�jlnevet kapjuk vissza

        if (!String.IsNullOrEmpty(mode))
        {
            List<string> helpLocalList = new List<string>();
            foreach (string ext in extensions)
            {
                helpLocalList.Add("_" + mode + ext);
            }
            extensions.InsertRange(0, helpLocalList);
        }

        if (!String.IsNullOrEmpty(command))
        {
            List<string> helpLocalList = new List<string>();
            foreach (string ext in extensions)
            {
                helpLocalList.Add("_" + command + ext);
            }
            extensions.InsertRange(0, helpLocalList);
        }

        if (!String.IsNullOrEmpty(startup))
        {
            List<string> helpLocalList = new List<string>();
            foreach (string ext in extensions)
            {
                helpLocalList.Add("_" + startup + ext);
            }
            extensions.InsertRange(0, helpLocalList);
        }

        foreach (string ext in extensions)
        {
            filenames.Add(helpBaseFileName + ext);
        }

        string[] generalNames = helpGeneralNames.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

        foreach (string generalName in generalNames)
        {
            if (helpBaseFileName.EndsWith(generalName, StringComparison.InvariantCultureIgnoreCase))
            {
                filenames.Add(generalName);
                break;
            }
        }

        return filenames;
    }
    #endregion Utils

    protected void Page_Init(object sender, EventArgs e)
    {        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Info: speci�lis (QueryStringben k�doland�) URL karakterek
        // RFC 3986 section 2.2 Reserved Characters (January 2005)
        // ! * ' ( ) ; : @ & = + $ , / ? % # [ ] 
        // RFC 3986 section 2.3 Unreserved Characters (January 2005)
        // A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 
        // a b c d e f g h i j k l m n o p q r s t u v w x y z 
        // 0 1 2 3 4 5 6 7 8 9 - _ . ~ 

        // Reserved characters after percent-encoding
        //  !   *   '   (   )   ;   :   @   &   =   +   $   ,   /   ?   %   #   [   ] 
        // %21 %2A %27 %28 %29 %3B %3A %40 %26 %3D %2B %24 %2C %2F %3F %25 %23 %5B %5D 


        //K�rnyezetf�gg� help: a s�g� gombhoz az oldalhoz tartoz� s�g� f�jl el�r�si �tvonal�nak hozz�rendel�s

        string pageUrl = Page.Request.Url.GetLeftPart(UriPartial.Path);
        //// illeg�lis karakter kiv�tele
        //pageUrl = pageUrl.Replace('|', '_');
        string helpDirectory = "/" + helpDirectoryName + "/";

        string helpPageBaseName = Path.GetFileNameWithoutExtension(pageUrl);

        bool bHelpPageFound = false;

        string helpPageName = "";
        string helpPageUrl = "";
        string helpPageServerPath = "";
        List<string> helpPageNameList = GetPossibleHelpFileNames(helpPageBaseName);
        foreach (string filename in helpPageNameList)
        {
            helpPageName = filename + helpFileExtension;
            helpPageUrl = "~" + VirtualPathUtility.Combine(helpDirectory, helpPageName);
            helpPageServerPath = Server.MapPath(helpPageUrl);
            FileInfo fiHelpPage = new FileInfo(helpPageServerPath);

            if (fiHelpPage.Exists)
            {
                bHelpPageFound = true;
                break;
            }
        }

        if (bHelpPageFound)
        {
            linkHelp.HRef = helpPageUrl;
            linkHelp.Target = "_blank";
        }
        else
        {
            linkHelp.HRef = "";
            linkHelp.Target = "";
            linkHelp.Attributes.Add("onclick", "alert('" + helpFileNotFound + "')");
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

}
