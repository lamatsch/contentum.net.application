using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_IranyitoszamTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    public bool WatermarkEnabled
    {
        get {return TextBoxWatermarkExtender1.Enabled; }
        set { TextBoxWatermarkExtender1.Enabled = value; }
    }

    public string Text
    {
        set { Iranyitoszam_TextBox.Text = value; }
        get { return Iranyitoszam_TextBox.Text; }
    }

    public bool Validate
    {
        set
        {
            RequiredFieldValidator1.Enabled = value;
            RequiredFieldValidator1.Visible = value;
        }
        get { return RequiredFieldValidator1.Enabled; }
    }

    public bool Number
    {
        get { return FilteredTextBoxExtender1.Enabled; }
        set { FilteredTextBoxExtender1.Enabled = value; }
    }

    //public string OnClick
    //{
    //    set { IranyitoszamImageButton.OnClientClick = value; }
    //    get { return IranyitoszamImageButton.OnClientClick; }
    //}

    public TextBox TextBox
    {
        get { return Iranyitoszam_TextBox; }
    }

    public bool Enabled
    {
        set
        {
            Iranyitoszam_TextBox.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            FilteredTextBoxExtender1.Enabled = value;
        }
        get { return Iranyitoszam_TextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            Iranyitoszam_TextBox.ReadOnly = value;
            AutoCompleteExtender1.Enabled = !value;
            FilteredTextBoxExtender1.Enabled = !value;
        }
        get { return Iranyitoszam_TextBox.ReadOnly; }
    }

    //public string Id_HiddenField
    //{
    //    set { HiddenField1.Value = value; }
    //    get { return HiddenField1.Value; }
    //}

    public bool AutoCompleteEnabled
    {
        set { AutoCompleteExtender1.Enabled = value; }
        get { return AutoCompleteExtender1.Enabled; }
    }

    public String AutoCompleteExtenderClientID
    {
        get { return AutoCompleteExtender1.ClientID; }
    }

    public string CssClass
    {
        get { return Iranyitoszam_TextBox.CssClass; }
        set 
        { 
            Iranyitoszam_TextBox.CssClass = value;
            TextBoxWatermarkExtender1.WatermarkCssClass = value + " " + TextBoxWatermarkExtender1.WatermarkCssClass;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                Iranyitoszam_TextBox.CssClass += " ViewReadOnlyWebControl";
                //IranyitoszamImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                Iranyitoszam_TextBox.CssClass += " ViewDisabledWebControl";
                //IranyitoszamImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }


    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //AutoCompleteExtender1.ServicePath = UI.GetAppSetting("eAdminBusinessServiceUrl") + "KRT_TelepulesekService.asmx";
        AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

        // �tt�ve a Page_Loadba, m�sk�pp �res marad a FelhasznaloId, ha lej�rt a session
        //String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        //AutoCompleteExtender1.ContextKey = felh_Id + ";";

        //OnClick = JavaScripts.SetOnClientClick("IranyitoszamokLovList.aspx",
        //   QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
        //   + "&" + QueryStringVars.TextBoxId + "=" + Iranyitoszam_TextBox.ClientID
        //            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // autentik�ci� ut�n, m�sk�pp �res marad a FelhasznaloId, ha lej�rt a session
        String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        AutoCompleteExtender1.ContextKey = felh_Id + ";";

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
        {
            Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

            if (c != null)
            {
                c.Visible = Validate;
            }
        }
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(Iranyitoszam_TextBox);
        //componentList.Add(IranyitoszamImageButton);

        // itt tartottam....

        //IranyitoszamImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        RequiredFieldValidator1.Enabled = false;
        FilteredTextBoxExtender1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;
        

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
