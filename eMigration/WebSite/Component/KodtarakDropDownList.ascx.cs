﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUIControls;
using System.Linq;

public partial class Component_KodtarakDropDownList : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    private readonly ListItem emptyListItem = new ListItem(Resources.Form.EmptyListItem, "");
    private string deletedValue = "[" + Resources.Form.UI_ToroltKodErtek + "]";
    //LZS - BUG_11346
    private string KodCsoport = string.Empty;

    public enum ListModeEnum
    {
        DropDownList,
        RadioButtonList
    }

    public ListModeEnum ListMode
    {
        get
        {
            object o = ViewState["ListMode"];
            if (o != null)
                return (ListModeEnum)o;

            return ListModeEnum.DropDownList;
        }
        set
        {
            switch (value)
            {
                case ListModeEnum.DropDownList:
                    Kodtarak_DropDownList.Visible = true;
                    Kodtarak_RadioButtonList.Visible = false;
                    break;
                case ListModeEnum.RadioButtonList:
                    Kodtarak_DropDownList.Visible = false;
                    Kodtarak_RadioButtonList.Visible = true;
                    break;
                default:
                    goto case ListModeEnum.DropDownList;
            }
            ViewState["ListMode"] = value;
        }
    }

    private ListControl _KodtarakList
    {
        get
        {
            switch (ListMode)
            {
                case ListModeEnum.DropDownList:
                    return Kodtarak_DropDownList;
                case ListModeEnum.RadioButtonList:
                    return Kodtarak_RadioButtonList;
                default:
                    goto case ListModeEnum.DropDownList;
            }
        }
    }

    public ListControl KodtarakList
    {
        get
        {
            return _KodtarakList;
        }
    }

    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _KodtarakList.SelectedIndexChanged += new EventHandler(_KodtarakList_SelectedIndexChanged);
    }

    public void Page_PreRender(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
        {
            Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

            if (c != null)
            {
                c.Visible = Validate;
            }
        }
    }

    void _KodtarakList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (_SelectedIndexChanged != null)
        {
            _SelectedIndexChanged(sender, e);
        }
    }

    private event EventHandler _SelectedIndexChanged;

    public event EventHandler SelectedIndexChanged
    {
        add
        {
            _SelectedIndexChanged += value;
        }
        remove
        {
            _SelectedIndexChanged -= value;
        }
    }

    public bool AutoPostBack
    {
        get
        {
            return _KodtarakList.AutoPostBack;
        }
        set
        {
            _KodtarakList.AutoPostBack = true;
        }
    }

    public CssStyleCollection Style
    {
        get
        {
            return _KodtarakList.Style;
        }
    }
    
    public eDropDownList DropDownList
    {
        get { return Kodtarak_DropDownList; }
    }

    //public RadioButtonList RadioButtonList
    //{
    //    get { return Kodtarak_RadioButtonList; }
    //}

    public String SelectedValue
    {
        get { return _KodtarakList.SelectedValue; }
        set { SetSelectedValue(value); }
    }

    public String Text
    {
        get
        {
            if (_KodtarakList.SelectedItem != null)
            {
                return _KodtarakList.SelectedItem.Text;
            }
            else
            {
                return "";
            }
        }
    }

    public bool Enabled
    {
        get { return _KodtarakList.Enabled; }
        set { _KodtarakList.Enabled = value; }
    }

    public bool ReadOnly
    {
        get
        {
            switch (ListMode)
            {
                case ListModeEnum.DropDownList:
                    return Kodtarak_DropDownList.ReadOnly;
                case ListModeEnum.RadioButtonList:
                    return !Kodtarak_RadioButtonList.Enabled;
                default:
                    goto case ListModeEnum.DropDownList;
            }
        }
        set
        {
            switch (ListMode)
            {
                case ListModeEnum.DropDownList:
                    Kodtarak_DropDownList.ReadOnly = value;
                    break;
                case ListModeEnum.RadioButtonList:
                    Kodtarak_RadioButtonList.Enabled = !value;
                    break;
                default:
                    goto case ListModeEnum.DropDownList;
            }
        }
    }

    public Unit Width
    {
        get { return _KodtarakList.Width; }
        set { _KodtarakList.Width = value; }    
    }

    public string CssClass
    {
        get { return _KodtarakList.CssClass; }
        set { _KodtarakList.CssClass = value; }
    }

    public string ValidationGroup
    {
        set
        {
            _KodtarakList.ValidationGroup = value;
        }
        get { return _KodtarakList.ValidationGroup; }
    }

    public bool Validate
    {
        set
        {
            RequiredFieldValidator1.Enabled = value;
        }
        get { return RequiredFieldValidator1.Enabled; }
    }

    private DateTime? _Ervenyesseg;
    public DateTime? Ervenyesseg
    {
        get { return _Ervenyesseg; }
        set { _Ervenyesseg = value; }
    }

    private string _TextFormat;

    public string TextFormat
    {
        get { return _TextFormat; }
        set { _TextFormat = value; }
    }

    private string GetKodtarText(Contentum.eUtility.KodTar_Cache.KodTarElem kodTarElem)
    {
        //default
        if (String.IsNullOrEmpty(TextFormat))
            return kodTarElem.Nev;

        string text = TextFormat;
        //név
        text = text.Replace("#Nev", kodTarElem.Nev);
        //kód
        text = text.Replace("#Kod", kodTarElem.Kod);
        //rövid név
        text = text.Replace("#RovidNev", kodTarElem.RovidNev);

        return text;
    }

    #region Nincs funkciójog filter
    /// <summary>
    /// Feltölti a DropDownList-et a megadott kódcsoporthoz tartozó (érvényes) kódtárértékekkel
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="errorPanel"></param>
    public void FillDropDownList(String kodcsoport, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList(kodcsoport, null, addEmptyItem, errorPanel);
    }

    public void FillDropDownList(String kodcsoport, List<string> filterList, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        //LZS - BUG_11346
        this.KodCsoport = kodcsoport;

        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakList = 
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoport, Page, this.Ervenyesseg);

        if (kodtarakList == null)
        {
            errorPanel.Header = Resources.Error.ErrorLabel;
            errorPanel.Body = Resources.Error.ErrorText_KodtarDropDownFill;
            errorPanel.Visible = true;
        }
        else
        {
            _KodtarakList.Items.Clear();

            foreach (Contentum.eUtility.KodTar_Cache.KodTarElem kte in kodtarakList)
            {
                String kodtarKod = kte.Kod; // Key a kódtár kód
                String kodtarNev = kte.Nev; // Value a kódtárérték neve
                string kodtarText = GetKodtarText(kte);

                if (!IsInGlobalFilter(kodcsoport,kodtarKod))
                {
                    // ha van megadva szûrés lista, és nincs benne, nem adjuk hozzá:
                    if (filterList != null && filterList.Contains(kodtarKod) == false)
                    {
                        // nem adjuk hozzá
                    }
                    else
                    {
                        _KodtarakList.Items.Add(new ListItem(kodtarText, kodtarKod));
                    }
                }
            }
            if (addEmptyItem)
            {
                _KodtarakList.Items.Insert(0, emptyListItem);
            }
        }

        //KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        //Result result = service.GetAllByKodcsoportKod(execParam, kodcsoport);
        //if (String.IsNullOrEmpty(result.ErrorCode))
        //{
        //    _KodtarakList.Items.Clear();

        //    foreach (DataRow row in result.Ds.Tables[0].Rows)
        //    {
        //        String kodtarNev = row["Nev"].ToString();
        //        String kodtarKod = row["Kod"].ToString();
        //        _KodtarakList.Items.Add(new ListItem(kodtarNev, kodtarKod));
        //    }
        //    if (addEmptyItem)
        //    {
        //        _KodtarakList.Items.Insert(0, emptyListItem);
        //    }
        //}
        //else
        //{
        //    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
        //}
    }

    private bool IsInGlobalFilter(string kodcsoport, string kodtarKod)
    {
        if(kodcsoport == "KEZELESI_FELJEGYZESEK_TIPUSA" && kodtarKod == KodTarak.KEZELESI_FELJEGYZESEK_TIPUSA.EloadoiMunkanaploMegjegyzes)
        {
            return true;
        }

        if (kodcsoport == KodTarak.FELADAT_ALTIPUS.kcsNev && kodtarKod == KodTarak.FELADAT_ALTIPUS.EloadoiMunkanaploMegjegyzes)
        {
            return true;
        }

        return false;
    }

    public void FillDropDownList(string kodcsoport, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList(kodcsoport, false, errorPanel);
    }

    public void FillWithOneValue(string kodcsoport, string kodtar_ertek, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        //LZS - BUG_11346
        this.KodCsoport = kodcsoport;

        _KodtarakList.Items.Clear();

        // Kódtárérték Cache-bõl:
        if (!String.IsNullOrEmpty(kodtar_ertek))
        {
            List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakList =
             Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoport, Page, this.Ervenyesseg);

            if (kodtarakList == null)
            {
                errorPanel.Header = Resources.Error.ErrorLabel;
                errorPanel.Body = Resources.Error.ErrorText_KodtarDropDownFill;
                errorPanel.Visible = true;
            }
            else
            {
                //_KodtarakList.Items.Clear();
                Contentum.eUtility.KodTar_Cache.KodTarElem kodtarElem = null;
                foreach (Contentum.eUtility.KodTar_Cache.KodTarElem kte in kodtarakList)
                {
                    if (kte.Kod == kodtar_ertek)
                    {
                        kodtarElem = kte;
                        break;
                    }
                }

                if (kodtarElem != null)
                {
                    _KodtarakList.Items.Add(new ListItem(GetKodtarText(kodtarElem), kodtar_ertek));
                }
                else
                {

                    //LZS - BUG_11346
                    DeletedKodTarErtek_Handling(this.KodCsoport, kodtar_ertek);
                    _KodtarakList.SelectedValue = _KodtarakList.Items[0].Value;
                }

            }


            //KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            //Result result = service.GetAllByKodcsoportKod(execParam, kodcsoport);

            //if (String.IsNullOrEmpty(result.ErrorCode))
            //{
            //    _KodtarakList.Items.Clear();
            //    if (result.Ds.Tables[0].Rows.Count > 0)
            //    {
            //        bool isEqualKodtar = false;
            //        foreach (DataRow row in result.Ds.Tables[0].Rows)
            //        {
            //            String kodtarKod = row["Kod"].ToString();
            //            if (kodtarKod == kodtar)
            //            {
            //                String kodtarNev = row["Nev"].ToString();
            //                _KodtarakList.Items.Add(new ListItem(kodtarNev, kodtarKod));
            //                isEqualKodtar = true;
            //                break;
            //            }
            //        }
            //        if (!isEqualKodtar)
            //        {
            //            _KodtarakList.Items.Insert(0, new ListItem(kodtar + " " + deletedValue, kodtar));
            //        }
            //    }
            //    else
            //    {
            //        _KodtarakList.Items.Insert(0, new ListItem(kodtar + " " + deletedValue, kodtar));
            //    }
            //}
            //else
            //{
            //    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            //}
        }
        else
        {
            _KodtarakList.Items.Insert(0, emptyListItem);
        }

    }

    /// <summary>
    /// Feltölti a listát, és beállítja az értékét a megadottra, ha az létezik. 
    /// Ha nem létezik, új elemként felvéve, és megjelölve, hogy ez egy nem létezõ kódtárérték
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="selectedValue"></param>
    /// <param name="addEmptyItem"></param>
    /// <param name="errorPanel"></param>
    /// 
    public void FillAndSetSelectedValue(String kodcsoport, String selectedValue, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillAndSetSelectedValue(kodcsoport, selectedValue, null, addEmptyItem, errorPanel);
    }

    public void FillAndSetSelectedValue(String kodcsoport, String selectedValue, List<string> filterList, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        //LZS - BUG_11346
        this.KodCsoport = kodcsoport;

        FillDropDownList(kodcsoport, filterList, addEmptyItem, errorPanel);
        ListItem selectedListItem = _KodtarakList.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            _KodtarakList.SelectedValue = selectedValue;
        }
        else
        {
            //LZS - BUG_11346
            DeletedKodTarErtek_Handling(kodcsoport, selectedValue);
        }
    }

    public void FillAndSetSelectedValue(String kodcsoport, String selectedValue, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillAndSetSelectedValue(kodcsoport, selectedValue, false, errorPanel);
    }

    public void FillAndSetEmptyValue(string kodcsoport, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillAndSetSelectedValue(kodcsoport, emptyListItem.Value, true, errorPanel);
    }

    #endregion Nincs funkciójog filter

    #region Funkciójog filter

    #region Utils
    private List<string> GetFunkcioKodokByKodCsoport(string kodcsoport)
    {
        if (String.IsNullOrEmpty(kodcsoport)) return null;

        //if (ViewState[String.Format("FunkcioKod_{0}", kodcsoport)] == null)
        //{
        KRT_FunkciokSearch search = new KRT_FunkciokSearch();
        search.Kod.Value = String.Format("KT_{0}_*_*abled", kodcsoport);  // "KT_<kódcsoport kód>_<kódtár kód>_Enabled", "KT_<kódcsoport kód>_<kódtár kód>_Disabled";
        search.Kod.Operator = Query.Operators.like;
        List<string> lstFunkcioKodok = FunctionRights.GetAllFunkcioKod(Page, search);

        //    ViewState[String.Format("FunkcioKod_{0}", kodcsoport)] = lstFunkcioKodok;
        //}

        //return ViewState[String.Format("FunkcioKod_{0}", kodcsoport)] as List<string>;
        return lstFunkcioKodok;
    }

    // Ha létezik egy adott kódtár értékhez definiált funkciójog, megengedõ vagy tiltó,
    // akkor amennyiben a felhasználónak nincs explicit megengedõjoga
    //  illetve van explicit tiltójoga, töröljük az elemet.
    // Ha nem létezik a  kódtár értékhez megadott névkonvenciót követõ funkciójog, akkor az elem látható marad.
    private void FilterListItemsByFunkcioJog(ListItemCollection listItems, string kodcsoport)
    {
        if (String.IsNullOrEmpty(kodcsoport)) return;
        if (listItems == null || listItems.Count == 0) return;

        List<string> lstFunkciok = GetFunkcioKodokByKodCsoport(kodcsoport);

        if (lstFunkciok != null)
        {
            for (int i = listItems.Count - 1; i >= 0; i--)
            {
                ListItem item = listItems[i];
                if (!String.IsNullOrEmpty(item.Value))
                {
                    string enabledFunkcio = String.Format("KT_{0}_{1}_Enabled", kodcsoport, item.Value);
                    string disabledFunkcio = String.Format("KT_{0}_{1}_Disabled", kodcsoport, item.Value);

                    if ((lstFunkciok.Contains(enabledFunkcio) && !FunctionRights.GetFunkcioJog(Page, enabledFunkcio))
                        || (lstFunkciok.Contains(disabledFunkcio) && FunctionRights.GetFunkcioJog(Page, disabledFunkcio)))
                    {
                        if (item.Selected == false)
                        {
                            listItems.Remove(item);
                        }
                        else
                        {
                            // ki van választva, csak megjelöljük
                            item.Text = String.Format("{0} {1}", item.Text, deletedValue);
                        }
                    }
                }
            }
        }
    }
    #endregion Utils

    /// <summary>
    /// Feltölti a DropDownList-et a megadott kódcsoporthoz tartozó (érvényes) kódtárértékekkel
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="errorPanel"></param>
    public void FillDropDownList(String kodcsoport, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillDropDownList(kodcsoport, null, addEmptyItem, errorPanel, bFilterByFunkcioJog);
    }

    public void FillDropDownList(String kodcsoport, List<string> filterList, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillDropDownList(kodcsoport, filterList, addEmptyItem, errorPanel);
        if (bFilterByFunkcioJog)
        {
            FilterListItemsByFunkcioJog(_KodtarakList.Items, kodcsoport);
        }
    }

    public void FillDropDownList(string kodcsoport, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillDropDownList(kodcsoport, false, errorPanel, bFilterByFunkcioJog);
    }

    public void FillWithOneValue(string kodcsoport, string kodtar_ertek, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillWithOneValue(kodcsoport, kodtar_ertek, errorPanel);
        if (bFilterByFunkcioJog)
        {
            FilterListItemsByFunkcioJog(this.DropDownList.Items, kodcsoport);
        }
    }

    /// <summary>
    /// Feltölti a listát, és beállítja az értékét a megadottra, ha az létezik. 
    /// Ha nem létezik, új elemként felvéve, és megjelölve, hogy ez egy nem létezõ kódtárérték
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="selectedValue"></param>
    /// <param name="addEmptyItem"></param>
    /// <param name="errorPanel"></param>
    /// 
    public void FillAndSetSelectedValue(String kodcsoport, String selectedValue, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillAndSetSelectedValue(kodcsoport, selectedValue, null, addEmptyItem, errorPanel, bFilterByFunkcioJog);
    }

    public void FillAndSetSelectedValue(String kodcsoport, String selectedValue, List<string> filterList, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillDropDownList(kodcsoport, filterList, addEmptyItem, errorPanel, bFilterByFunkcioJog);
        ListItem selectedListItem = _KodtarakList.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            _KodtarakList.SelectedValue = selectedValue;
        }
        else
        {
            //LZS - BUG_11346
            DeletedKodTarErtek_Handling(kodcsoport, selectedValue);
        }
    }

    public void FillAndSetSelectedValue(String kodcsoport, String selectedValue, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillAndSetSelectedValue(kodcsoport, selectedValue, false, errorPanel, bFilterByFunkcioJog);
    }

    public void FillAndSetEmptyValue(string kodcsoport, Contentum.eUIControls.eErrorPanel errorPanel, bool bFilterByFunkcioJog)
    {
        FillAndSetSelectedValue(kodcsoport, emptyListItem.Value, true, errorPanel, bFilterByFunkcioJog);
    }

    #endregion Funkciójog filter

    /// <summary>
    /// Beállítja a már feltöltött lista értékét a megadottra, ha az létezik. 
    /// Ha nem létezik, új elemként felvéve, és megjelölve, hogy ez egy nem létezõ kódtárérték
    /// </summary>
    /// <param name="selectedValue"></param>
    public void SetSelectedValue(String selectedValue)
    {
        ListItem selectedListItem = _KodtarakList.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            _KodtarakList.SelectedValue = selectedValue;
        }
        else
        {
            //LZS - BUG_11346
            DeletedKodTarErtek_Handling(this.KodCsoport, selectedValue);
            _KodtarakList.SelectedValue = _KodtarakList.Items[0].Value;
        }
    }

    private void DeletedKodTarErtek_Handling(string kodcsoport, string selectedValue)
    {
        //LZS - BUG_11346
        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakErvenytelenList =
       Contentum.eUtility.KodTar_Cache.GetErvenytelenKodtarakByKodCsoportList(kodcsoport, Page, this.Ervenyesseg);

        var querykodtarakErvenytelenList = (from item in kodtarakErvenytelenList
                                            where item.Kod == selectedValue
                                            orderby item.ErvVege descending
                                            select item);

        //LZS - BUG_11346 - Amennyiben nincs érvényes, de van érvénytelen, és csak egy ilyen van az adott kóddal, akkor az jelenjen meg:
        //LZS - BUG_11346 - Amennyiben több is van az adott kóddal, akkor az jelenjen meg, amelyiknél az érvényességi idő vége a legkésőbbi:
        if (querykodtarakErvenytelenList != null && querykodtarakErvenytelenList.Count() > 0)
        {
            var queryResult = querykodtarakErvenytelenList.FirstOrDefault();
            _KodtarakList.Items.Insert(0, new ListItem(queryResult.Nev, queryResult.Kod));
        }
        else//LZS - BUG_11346 - Amennyiben egyáltalán nincs az adott kódtár érték a kódcsoportban, akkor a kód érték + [Nem létező kódérték] szöveg jelenjen meg:
        {
            string itemText_Deleted = selectedValue + " " + deletedValue;
            // ha üres elem volt a selectedValue, akkor nem [Nem létező kódérték] szöveget írunk ki, hanem egy üres elemet adunk hozzá
            if (String.IsNullOrEmpty(selectedValue))
            {
                itemText_Deleted = String.Empty;
            }
            _KodtarakList.Items.Insert(0, new ListItem(itemText_Deleted, selectedValue));
        }
    }

    public void Clear()
    {
        _KodtarakList.Items.Clear();
    }

    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(DropDownList);

        return componentList;
    }

    private bool _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            if (!_ViewEnabled)
            {
                ReadOnly = !_ViewEnabled;
                DropDownList.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    bool _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                DropDownList.CssClass += " ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;

        if (_KodtarakList.SelectedIndex > -1 && _KodtarakList.SelectedItem.Value != emptyListItem.Value)
        {
            text = _KodtarakList.SelectedItem.Text;
        }

        return text;
    }

    #endregion
}
