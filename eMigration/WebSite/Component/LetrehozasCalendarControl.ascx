﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LetrehozasCalendarControl.ascx.cs" Inherits="Component_LetrehozasCalendarControl" %>

<asp:TextBox ID="letrehozasKezd_TextBox" runat="server" CssClass="mrUrlapCalendar_Erv"></asp:TextBox>
<asp:ImageButton TabIndex = "-1"
    ID="letrehozasKezd_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png"
    OnClientClick="return false;" />&nbsp;
<asp:TextBox ID="letrehozasVege_TextBox" runat="server" CssClass="mrUrlapCalendar_Erv"></asp:TextBox>
<asp:ImageButton TabIndex = "-1"
    ID="letrehozasVege_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png"
    OnClientClick="return false;" />
<ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="letrehozasKezd_ImageButton"
    TargetControlID="letrehozasKezd_TextBox">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="letrehozasVege_ImageButton"
    TargetControlID="letrehozasVege_TextBox">
</ajaxToolkit:CalendarExtender>
<%--<asp:RequiredFieldValidator ID="letrehozasKezd_RequiredFieldValidator" runat="server" SetFocusOnError="true"
    ControlToValidate="letrehozasKezd_TextBox" Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
--%><%--<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
    TargetControlID="letrehozasKezd_RequiredFieldValidator">
        <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
</ajaxToolkit:ValidatorCalloutExtender>--%>
<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="letrehozas_TextBox" SetFocusOnError="true"
    ControlToValidate="letrehozasVege_TextBox" Display="None" ErrorMessage="<%$Resources:Form,Erv_kezd_vege_CompareValidatorMessage%>"
    Operator="GreaterThanEqual" Type="Date" Visible="False"></asp:CompareValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server"
    TargetControlID="CompareValidator1">
        <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
        <asp:CustomValidator ID="CustomValidator_DateFormat_Kezd" runat="server" 
           ErrorMessage="<%$Resources:Form,DateTimeFormatValidationMessage%>" SetFocusOnError="true"
           ClientValidationFunction="Utility.Calendar.ValidateDateTimeFormat"  
           EnableClientScript="true" ControlToValidate="letrehozasKezd_TextBox" Display="None"
    />
        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_DateFormat_Kezd" runat="server"
        TargetControlID="CustomValidator_DateFormat_Kezd">
            <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
        <asp:CustomValidator ID="CustomValidator_DateFormat_Vege" runat="server" 
           ErrorMessage="<%$Resources:Form,DateTimeFormatValidationMessage%>" SetFocusOnError="true"
           ClientValidationFunction="Utility.Calendar.ValidateDateTimeFormat"  
           EnableClientScript="true" ControlToValidate="letrehozasVege_TextBox" Display="None"
    />
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_DateFormat_Vege" runat="server"
        TargetControlID="CustomValidator_DateFormat_Vege">
            <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
