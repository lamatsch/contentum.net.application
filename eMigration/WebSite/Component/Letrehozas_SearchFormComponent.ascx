﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Letrehozas_SearchFormComponent.ascx.cs" Inherits="Component_Letrehozas_SearchFormComponent" %>
<%@ Register Src="~/Component/LetrehozasCalendarControl.ascx" TagName="LetrehozasCalendarControl"
    TagPrefix="uc3" %>

<asp:Panel ID="Panel1" runat="server">
<table width="100%" cellpadding="0" cellspacing="0">
    <tr id="trLetrehozasTartomany" runat="server">
        <td class="mrUrlapCaption">
            <asp:Label ID="LetrehozasTartomany_Label" runat="server" Text="Létrehozás ekkor:"></asp:Label></td>
        <td class="mrUrlapMezo">
            <uc3:LetrehozasCalendarControl id="LetrehozasCalendarControl1" runat="server"  SearchMode="true" ValidateDateFormat="true" />
        </td>
    </tr>
</table>
</asp:Panel>
