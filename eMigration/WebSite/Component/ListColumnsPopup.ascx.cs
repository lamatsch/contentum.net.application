﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Text;
using Contentum.eUtility;
using System.Collections.Generic;

public partial class Component_ListColumnsPopup : System.Web.UI.UserControl
{
    private bool _isInitialized = false;

    public bool IsInitialized
    {
        get { return _isInitialized; }
    }

    private GridView _attachedGridView = null;

    public GridView AttachedGridView
    {
        get { return _attachedGridView; }
        set
        {
            _attachedGridView = value;
            _isInitialized = true;
        }
    }

    private Contentum.eUIControls.TreeGridView _attachedTreeGridView = null;

    public Contentum.eUIControls.TreeGridView AttachedTreeGridView
    {
        get { return _attachedTreeGridView; }
        set
        {
            _attachedTreeGridView = value;
            _isInitialized = true;
        }
    }

    private Type searchObjectType;
    public Type SearchObjectType
    {
        get { return searchObjectType; }
        set { searchObjectType = value; }
    }

    private String customSearchObjectSessionName = "";    
    public String CustomSearchObjectSessionName
    {
        get { return customSearchObjectSessionName; }
        set { customSearchObjectSessionName = value; }
    }

    
    public string TargetControlID
    {
        get
        {
            return mdeListColumnsPopup.TargetControlID;
        }
        set
        {
            mdeListColumnsPopup.TargetControlID = value;
        }
    }
        
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            RegisterJavascripts();
    }

    private void RegisterJavascripts()
    {
        string js = @"
                    function showListColumnsPopup()
                    {                
                        $find('" + mdeListColumnsPopup.ClientID+ @"').show();        
                            
                        return false;
                    }    
                    
                    function hideListColumnsPopup()  
                     {  
                       $find('" + mdeListColumnsPopup.ClientID + @"').hide();
                       return false;  
                     }
                     
                     function cancelListColumnsPopup()
                     {
                        var closeButton = $get('" + btnContinue.ClientID + @"'); 
                        if (closeButton) {closeButton.click();}
                     }
                     
                     function doPostBack_Refresh()
                     {
                        __doPostBack('" + btnRefresh.UniqueID + @"','');
                     }
                     
                     function showOrHideDetailsColumnsPanel()
                     {
                          var divDetailsPanel = $get('divDetailsColumnsPanel');
                          var text = '';  
                        
                          if (divDetailsPanel.style.display == 'none')
                          {  
                              divDetailsPanel.style.display = '';
                          }  
                          else  
                          {  
                              divDetailsPanel.style.display = 'none';
                          }         
                     }";

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ListColumnsPopupScript", js, true);
    }

    public void InitComponent()
    {
        if (AttachedGridView != null) InitComponent(AttachedGridView.Columns);
        if (AttachedTreeGridView != null) InitComponent(AttachedTreeGridView.Columns);
    }

    public void InitComponent(DataControlFieldCollection Columns)
    {
        if (Columns == null) {return;}

        CheckBoxList1.Items.Clear();

        for (int i = 0; i < Columns.Count; i++)
        {
            if (!string.IsNullOrEmpty(Columns[i].HeaderText))
            {
                ListItem listItem = new ListItem(Columns[i].HeaderText, i.ToString());
                listItem.Selected = Columns[i].Visible ? true : false;
                
                CheckBoxList1.Items.Add(listItem);
            }
        }
    }

    // Default oszlopláthatóságok beállítása
    // Csak ha már meg van adva a SearchObjectType
    public bool SetDefaultColumnsVisibilityIfExist()
    {
        bool existsDefault = false;
        GridView gridView = _attachedGridView;
        if (gridView == null)
        {
            if (_attachedTreeGridView != null)
            {
                gridView = new GridView();
                foreach (DataControlField column in _attachedTreeGridView.Columns)
                {
                    gridView.Columns.Add(column);
                }
                gridView.ID = _attachedTreeGridView.ID;
            }
        }
        if (gridView != null && searchObjectType != null)
        {
            existsDefault = UIMezoObjektumErtekek.SetDefaultColumnsVisibilityIfExist(Page, gridView, searchObjectType, customSearchObjectSessionName);
        }
        return existsDefault;
    }

    public event EventHandler GridViewBind;
    public event EventHandler DefaultColumnsVisibilitySaved;

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        if (AttachedGridView != null || AttachedTreeGridView != null)
        {
            DataControlFieldCollection Columns = null;
            if (AttachedGridView != null) Columns = AttachedGridView.Columns;
            if (AttachedTreeGridView != null) Columns = AttachedTreeGridView.Columns;

            int columnsCount = Columns.Count;

            // csinálnunk kell gridViewBind-ot, ha van olyan oszlop, aminek a Visible értékét false-ról true-ra állítjuk
            // (mert ilyenkor ezek az adatok nincsenek leküldve a ViewState-be)
            bool needGridViewBind = false;

            foreach (ListItem listItem in CheckBoxList1.Items)
            {
                int columnIndex;
                try
                {
                    Int32.TryParse(listItem.Value, out columnIndex);

                    if (columnIndex >= Columns.Count)
                    {
                        continue;
                    }

                    bool columnWasInVisible = !Columns[columnIndex].Visible;

                    if (listItem.Selected != Columns[columnIndex].Visible)
                    {
                        if (AttachedTreeGridView != null) needGridViewBind = true;
                        if (listItem.Selected)
                        {
                            Columns[columnIndex].Visible = true;
                            if (columnWasInVisible)
                            {
                                needGridViewBind = true;
                            }
                        }
                        else
                        {
                            Columns[columnIndex].Visible = false;
                        }
                    }
                }
                catch
                { 
                    // hiba, nem dobjuk tovább
                    Logger.Error("Hiba: ListColumnsPopup.btnRefresh_Click; GridView: " + AttachedGridView.ID + " listItem: " + listItem.Text);
                }
            }

            if (needGridViewBind)
            {
                if (GridViewBind != null)
                {
                    GridViewBind(sender, e);
                }
            }            
        }
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {        
        if (searchObjectType == null && string.IsNullOrEmpty(customSearchObjectSessionName))
        {
            return;
        }

        // Insert vagy Update kell
        // Volt-e már rekord erre a templateTípusra?

        string templateTipusNev = GetColumnsVisibilityTemplateTipusNev(searchObjectType, customSearchObjectSessionName);

        #region Keresés:
        Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

        KRT_UIMezoObjektumErtekekSearch search = new KRT_UIMezoObjektumErtekekSearch();

        search.Felhasznalo_Id.Value = Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(Page);
        search.Felhasznalo_Id.Operator = Query.Operators.equals;

        search.TemplateTipusNev.Value = templateTipusNev;
        search.TemplateTipusNev.Operator = Query.Operators.equals;

        search.Alapertelmezett.Value = Contentum.eUtility.Constants.Database.Yes;
        search.Alapertelmezett.Operator = Query.Operators.equals;

        ExecParam execParam = Contentum.eUtility.UI.SetExecParamDefault(Page);

        Result result_Search = service.GetAll(execParam, search);
        if (result_Search.IsError)
        {
            // hiba
            Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.GetAll", execParam, result_Search);
            return;
        }

        #endregion

        string templateId = String.Empty;
        string templateVer = String.Empty;

        if (result_Search.Ds.Tables[0].Rows.Count > 1)
        {
            // ha valamiért több van, akkor az elsőt beállítjuk update-re, a többit érvénytelenítjük:
            templateId = result_Search.Ds.Tables[0].Rows[0]["Id"].ToString();
            templateVer = result_Search.Ds.Tables[0].Rows[0]["Ver"].ToString();

            #region Invalidate
            ExecParam[] execParamArray = new ExecParam[result_Search.Ds.Tables[0].Rows.Count - 1];
            // a 0.-at nem töröljük, mert azt majd update-eljük
            for (int i = 1; i < result_Search.Ds.Tables[0].Rows.Count; i++)
            {
                execParamArray[i - 1] = Contentum.eUtility.UI.SetExecParamDefault(Page);
                execParamArray[i - 1].Record_Id = result_Search.Ds.Tables[0].Rows[i]["Id"].ToString();
            }

            Result result_invalidate = service.MultiInvalidate(execParamArray);
            if (result_invalidate.IsError)
            {
                // hiba
                Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.MultiInvalidate ", execParam, result_invalidate);
                return;
            }
            #endregion
        }
        else if (result_Search.Ds.Tables[0].Rows.Count == 1)
        {
            templateId = result_Search.Ds.Tables[0].Rows[0]["Id"].ToString();
            templateVer = result_Search.Ds.Tables[0].Rows[0]["Ver"].ToString();
        }

        KRT_UIMezoObjektumErtekek krt_UIMezoObjektumErtekek = CreateBusinessObject();

        if (!String.IsNullOrEmpty(templateId))
        {
            #region Update

            // elmentett verziót be kell állítani az Update-hez:
            krt_UIMezoObjektumErtekek.Base.Ver = templateVer;
            krt_UIMezoObjektumErtekek.Base.Updated.Ver = true;

            ExecParam execParam_update = Contentum.eUtility.UI.SetExecParamDefault(Page);
            execParam_update.Record_Id = templateId;

            Result result_update = service.Update(execParam_update, krt_UIMezoObjektumErtekek);
            if (result_update.IsError)
            {
                // hiba:
                Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.Update", execParam_update, result_update);
                return;
            }
            #endregion
        }
        else
        {
            #region Insert

            ExecParam execParam_Insert = Contentum.eUtility.UI.SetExecParamDefault(Page);

            Result result_Insert = service.Insert(execParam_Insert, krt_UIMezoObjektumErtekek);
            if (result_Insert.IsError)
            {
                // hiba:
                Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.Insert", execParam_Insert, result_Insert);
                return;
            }

            templateId = result_Insert.Uid;

            #endregion
        }

        #region Sessionben lévő Dictionary-ben update
        if (Page.Session[Constants.SessionNames.DefaultColumnsVisibilityDictionary] != null)
        {
            Dictionary<string, UIMezoObjektumErtekek.GridViewColumnsVisibility> columnsVisibilityDict = 
                (Dictionary<string, UIMezoObjektumErtekek.GridViewColumnsVisibility>)Page.Session[Constants.SessionNames.DefaultColumnsVisibilityDictionary];

            UIMezoObjektumErtekek.GridViewColumnsVisibility columnsVisibilityObj =
                new UIMezoObjektumErtekek.GridViewColumnsVisibility(templateId, krt_UIMezoObjektumErtekek.TemplateXML);

            columnsVisibilityDict[templateTipusNev] = columnsVisibilityObj;
        }
        #endregion

        // Mintha a frissítés gombot is megnyomtuk volna:
        btnRefresh_Click(sender, e);

        // esemény kiváltása:
        if (DefaultColumnsVisibilitySaved != null)
        {
            DefaultColumnsVisibilitySaved(sender, e);
        }
    }

    private string GetColumnsVisibilityTemplateTipusNev(Type searchObjectType, string customSearchObjectSessionName)
    {
        if (!string.IsNullOrEmpty(customSearchObjectSessionName))
        {
            return Contentum.eUtility.UIMezoObjektumErtekek.ColumnsVisibilityTag + customSearchObjectSessionName;
        }
        else
        {
            return Contentum.eUtility.UIMezoObjektumErtekek.ColumnsVisibilityTag + searchObjectType.Name;
        }
    }

    private KRT_UIMezoObjektumErtekek CreateBusinessObject()
    {
        KRT_UIMezoObjektumErtekek krt_UIMezoObjektumErtekek = new KRT_UIMezoObjektumErtekek();
        krt_UIMezoObjektumErtekek.Updated.SetValueAll(false);
        krt_UIMezoObjektumErtekek.Base.Updated.SetValueAll(false);

        krt_UIMezoObjektumErtekek.Felhasznalo_Id = Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(Page);
        krt_UIMezoObjektumErtekek.Updated.Felhasznalo_Id = true;

        krt_UIMezoObjektumErtekek.TemplateTipusNev = GetColumnsVisibilityTemplateTipusNev(searchObjectType, customSearchObjectSessionName);
        krt_UIMezoObjektumErtekek.Updated.TemplateTipusNev = true;

        // Névnek beállítjuk a templateTipusNev-et, úgyis csak egy lesz egy fajtából
        krt_UIMezoObjektumErtekek.Nev = krt_UIMezoObjektumErtekek.TemplateTipusNev;
        krt_UIMezoObjektumErtekek.Updated.Nev = true;

        if (AttachedGridView != null)
            krt_UIMezoObjektumErtekek.TemplateXML = GetXmlStringFromComponents(CheckBoxList1, AttachedGridView.Columns);
        else if(AttachedTreeGridView != null)
            krt_UIMezoObjektumErtekek.TemplateXML = GetXmlStringFromComponents(CheckBoxList1, AttachedTreeGridView.Columns);
        krt_UIMezoObjektumErtekek.Updated.TemplateXML = true;

        // Alapértelmezettnek kell beállítani:
        krt_UIMezoObjektumErtekek.Alapertelmezett = Contentum.eUtility.Constants.Database.Yes;
        krt_UIMezoObjektumErtekek.Updated.Alapertelmezett = true;

        krt_UIMezoObjektumErtekek.UtolsoHasznIdo = DateTime.Now.ToString();
        krt_UIMezoObjektumErtekek.Updated.UtolsoHasznIdo = true;

        return krt_UIMezoObjektumErtekek;
    }

    private string GetXmlStringFromComponents(CheckBoxList checkBoxList, DataControlFieldCollection Columns)
    {
        StringBuilder sb_xml = new StringBuilder(@"<GridView><Columns>");

        foreach (ListItem listItem in checkBoxList.Items)
        {
            string index = listItem.Value;
            string visible = listItem.Selected ? "true" : "false";
            string headerText = String.Empty;
            string sortExpression = String.Empty;

            try
            {
                int indexInt = Int32.Parse(index);

                headerText = Columns[indexInt].HeaderText;
                sortExpression = Columns[indexInt].SortExpression;
            }
            catch (Exception e)
            {
                Logger.Error("Hiba: ListColumnsPopup.GetXmlStringFromComponents", e);
            }

            sb_xml.Append(String.Format("<Column index=\"{0}\" visible=\"{1}\" headerText=\"{2}\" sortExpression=\"{3}\" />",
                index, visible, headerText, sortExpression));
        }

        sb_xml.Append("</Columns></GridView>");

        return sb_xml.ToString();
    }

    protected void DeleteButton_Click(object sender, EventArgs e)
    {
        if (searchObjectType == null && string.IsNullOrEmpty(customSearchObjectSessionName))
        {
            return;
        }

        // Volt-e már rekord erre a templateTípusra?
        string templateTipusNev = GetColumnsVisibilityTemplateTipusNev(searchObjectType, customSearchObjectSessionName);


        #region Keresés:
        Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();

        KRT_UIMezoObjektumErtekekSearch search = new KRT_UIMezoObjektumErtekekSearch();

        search.Felhasznalo_Id.Value = Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(Page);
        search.Felhasznalo_Id.Operator = Query.Operators.equals;

        search.TemplateTipusNev.Value = templateTipusNev;
        search.TemplateTipusNev.Operator = Query.Operators.equals;

        search.Alapertelmezett.Value = Contentum.eUtility.Constants.Database.Yes;
        search.Alapertelmezett.Operator = Query.Operators.equals;

        ExecParam execParam = Contentum.eUtility.UI.SetExecParamDefault(Page);

        Result result_Search = service.GetAll(execParam, search);
        if (result_Search.IsError)
        {
            // hiba
            Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.GetAll", execParam, result_Search);
            return;
        }

        #endregion

        if (result_Search.Ds.Tables[0].Rows.Count > 0)
        {
            #region Invalidate

            ExecParam[] execParamArray = new ExecParam[result_Search.Ds.Tables[0].Rows.Count];
            
            for (int i = 0; i < result_Search.Ds.Tables[0].Rows.Count; i++)
            {
                execParamArray[i] = Contentum.eUtility.UI.SetExecParamDefault(Page);
                execParamArray[i].Record_Id = result_Search.Ds.Tables[0].Rows[i]["Id"].ToString();
            }

            Result result_invalidate = service.MultiInvalidate(execParamArray);
            if (result_invalidate.IsError)
            {
                // hiba
                Logger.Error("Hiba: KRT_UIMezoObjektumErtekekService.MultiInvalidate ", execParam, result_invalidate);
                return;
            }

            #endregion
        }


        #region Sessionben lévő Dictionary-ből is törlés
        if (Page.Session[Constants.SessionNames.DefaultColumnsVisibilityDictionary] != null)
        {
            Dictionary<string, UIMezoObjektumErtekek.GridViewColumnsVisibility> columnsVisibilityDict =
                (Dictionary<string, UIMezoObjektumErtekek.GridViewColumnsVisibility>)Page.Session[Constants.SessionNames.DefaultColumnsVisibilityDictionary];

            if (columnsVisibilityDict.ContainsKey(templateTipusNev))
            {
                columnsVisibilityDict.Remove(templateTipusNev);
            }
        }
        #endregion

        Response.Redirect(Request.Url.OriginalString, true);
    }

}

