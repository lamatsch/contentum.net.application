using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Collections.Generic;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Data;
using System.Xml;
using System.Reflection;
using System.Text;

using Contentum.eUIControls;

public partial class Component_ListHeader : System.Web.UI.UserControl
{

    private const string funkcio_ListaOszlopLathatosagAllitas = "ListaOszlopLathatosagAllitas";

    private const string CssClass_item_default = "";

    #region SetImageButtonEnabledProperty
    private void SetImageButtonEnabledProperty(ImageButton imageButton, bool enabledValue)
    {
        imageButton.Enabled = enabledValue;
    }

    // lehet�s�g a funkci�gomb csoportok elt�r� kezel�s�re
    private void SetLeftFunctionButtonEnabledProperty(ImageButton imageButton, bool enabledValue)
    {
        SetImageButtonEnabledProperty(imageButton, enabledValue);
    }

    private void SetRightFunctionButtonEnabledProperty(ImageButton imageButton, bool enabledValue)
    {
        SetImageButtonEnabledProperty(imageButton, enabledValue);
    }

    private void SetSearchButtonEnabledProperty(ImageButton imageButton, bool enabledValue)
    {
        SetImageButtonEnabledProperty(imageButton, enabledValue);
    }

    #region IsDisabledButtonToSetHidden by RPM
    private bool IsDisabledButtonToSetHidden()
    {
        return Contentum.eUtility.Rendszerparameterek.GetBoolean(Page, Contentum.eUtility.Rendszerparameterek.UI_SETUP_HIDE_DISABLED_ICONS);;
    }
    #endregion IsDisabledButtonToSetHidden by RPM
    #endregion SetImageButtonEnabledProperty

    #region Paging

    //private static int _PageIndex = 0;
    //private static int _PageCount = 0;

    private string _PageName = "";

    public string PageName
    {
        get { return _PageName; }
        set
        {
            _PageName = value;

            // az esetleges &nbsp; karaktersorok lecser�l�se �res stringre, hogy a b�ng�sz� c�m�ben is norm�lisan jelenjen meg
            string pageTitle = value.Replace("&nbsp;", " ");
            Page.Title = pageTitle;
        }
    }

    public int PageIndex
    {
        get
        {
            try
            {
                if (string.IsNullOrEmpty(PageIndex_HiddenField.Value))
                {
                    return 0;
                }
                else
                {
                    return Int32.Parse(PageIndex_HiddenField.Value);
                }
            }
            catch (FormatException)
            {
                return 0;
            }
        }
        set
        {
            PageIndex_HiddenField.Value = value.ToString();
        }
    }
    public int PageCount
    {
        get
        {
            try
            {
                if (string.IsNullOrEmpty(PageCount_HiddenField.Value))
                {
                    return 1;
                }
                else
                {
                    return Int32.Parse(PageCount_HiddenField.Value);
                }
            }
            catch (FormatException)
            {
                return 1;
            }
        }
        set
        {
            PageCount_HiddenField.Value = value.ToString();
        }
    }

    public void RefreshPagerLabel()
    {
       // max. oldalsz�m:
        int pageSize = Int32.Parse(RowCount);
        if (pageSize == 0)
        {
            PageCount = 1;
        }
        else
        {
            PageCount = RecordNumber / pageSize;
            if (RecordNumber % pageSize > 0) PageCount++;
        }        

        PagerLabel = (PageIndex + 1).ToString() + "/" + PageCount;
    }


    #region SelectedRecord properties

    public string SelectedRecordId
    {
        get
        {
            if (String.IsNullOrEmpty(selectedRecordId.Value))
            {
                return String.Empty;
            }
            else
            {
                return selectedRecordId.Value;
            }
        }
        set
        {
            if (selectedRecordId.Value != value)
            {
                selectedRecordId.Value = value;
                RaiseSelectedRecordChanged();
            }
        }
    }

    public event EventHandler SelectedRecordChanged;

    private void RaiseSelectedRecordChanged()
    {
        if(SelectedRecordChanged != null)
        {
            SelectedRecordChanged(this,EventArgs.Empty);
        }
    }

    public void ForgetSelectedRecord()
    {
        SelectedRecordId = String.Empty;
    }

    private GridView attachedGridView = null;

    public GridView AttachedGridView
    {
        get { return attachedGridView; }
        set 
        {
            if (attachedGridView != null)
            {
                attachedGridView.SelectedIndexChanged -= attachedGridView_SelectedIndexChanged;
            }
            attachedGridView = value;
            attachedGridView.SelectedIndexChanged += new EventHandler(attachedGridView_SelectedIndexChanged);
            
            ListSortPopup1.AttachedGridView = value;
            ListColumnsPopup1.AttachedGridView = value;

            // Default oszlopl�that�s�gok be�ll�t�sa
            SetDefaultColumnsVisibilityIfExist();
            
            // BUG_8359
            SetDefaultOrderByIfExist();
        }
    }

    private Contentum.eUIControls.TreeGridView _attachedTreeGridView = null;

    public Contentum.eUIControls.TreeGridView AttachedTreeGridView
    {
        get { return _attachedTreeGridView; }
        set
        {
            _attachedTreeGridView = value;
            ListSortPopup1.AttachedTreeGridView = value;
            ListColumnsPopup1.AttachedTreeGridView = value;

            // Default oszlopl�that�s�gok be�ll�t�sa
            SetDefaultColumnsVisibilityIfExist();
        }
    }

    private void SetDefaultColumnsVisibilityIfExist()
    {
        if ((IsReload || !IsPostBack))
        {
            bool existsDefault = ListColumnsPopup1.SetDefaultColumnsVisibilityIfExist();
            if (existsDefault)
            {
                // ikon �t�ll�t�sa, hogy l�tsz�djon, van bet�lt�tt oszlop�ll�t�s template
                ChangeListColumnsImage(true);
            }
        }
    }

    private bool attachedGridView_RowDeselectFunctionEnabled = true;

    /// <summary>
    /// Sorkijel�l�s megsz�ntet�se funkci� enged�lyez�se/tilt�sa a GridView-n
    /// </summary>
    public bool AttachedGridView_RowDeselectFunctionEnabled
    {
        get { return attachedGridView_RowDeselectFunctionEnabled; }
        set { attachedGridView_RowDeselectFunctionEnabled = value; }
    }

    
    protected void attachedGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (attachedGridView.SelectedValue != null)
            SelectedRecordId = attachedGridView.SelectedValue.ToString();
        else
            SelectedRecordId = String.Empty;
    }

    public string AttachedGridViewId
    {
        get 
        {
            if (attachedGridView != null)
                return attachedGridView.ID;
            else
                return String.Empty;
        }
    }

    #endregion


    //public int PageIndex
    //{
    //    get { return _PageIndex; }
    //    set { _PageIndex = value; }
    //}
    //public int PageCount
    //{
    //    get { return _PageCount; }
    //    set { _PageCount = value; }
    //}

    public String PagerLabel
    {
        get { return Pager.Text; }
        set { Pager.Text = value; }
    }

    public bool Scrollable
    {
        get { return ScrollableCheckBox.Checked; }
        //set { _PageIndex = value; }
    }

    public string RowCount
    {
        get 
        {
            string strRowCount = RowCountTextBox.Text;
            int iRowCount;
            if (Int32.TryParse(strRowCount, out iRowCount))
            {
                if (iRowCount >= 0)
                {
                    return strRowCount;
                }
            }

            strRowCount = this.GetRowCountFromSession();
            if (!String.IsNullOrEmpty(strRowCount))
            {
                RowCountTextBox.Text = strRowCount;
                return strRowCount;
            }

            RowCountTextBox.Text = Constants.MasterRowCountNumber.ToString();
            return RowCountTextBox.Text; 
        }
        //set { _PageIndex = value; }
    }

    private string GetRowCountFromSession()
    {
        if (Session[Constants.MasterRowCount] != null)
        {
            string strRowCount = Session[Constants.MasterRowCount].ToString(); ;
            int iRowCount;
            if (Int32.TryParse(strRowCount, out iRowCount))
            {
                if (iRowCount >= 0)
                {
                    return strRowCount;
                }
            }

            Session[Constants.MasterRowCount] = Constants.MasterRowCountNumber;
            return Constants.MasterRowCountNumber.ToString();
        }
        else
        {
            return String.Empty;
        }
    }

    public int RecordNumber
    {
        get
        {
            string text = labelRecordNumber.Text;
            int number = 0;
            if(text.StartsWith("(") && text.EndsWith(")"))
            {
                Int32.TryParse(text.Substring(1,text.Length - 2),out number);
            }
            return number; 
        }
        set { labelRecordNumber.Text = String.Format("({0})", value); }
    }


    private bool enableListSortPopup = true;
    public bool EnableListSortPopup
    {
        get { return enableListSortPopup; }
        set 
        { 
            enableListSortPopup = value;
            ListSortPopup1.Visible = value;
            ImageSort.Visible = value;
            ListColumnsPopup1.Visible = value;
            ImageListColumns.Visible = value;
        }
    }

    private bool _isReload = false;

    public bool IsReload
    {
        get { return _isReload; }
        set { _isReload = value; }
    }

    public Component_ListSortPopup ListSortPopup
    {
        get
        {
            return ListSortPopup1;
        }
    }
   
    public bool PagerVisible
    {
        set
        {
            tablePager.Visible = value;
        }
        get
        {
            return tablePager.Visible;
        }
    }

    private void SetPagerButtonsEnabled(bool value)
    {
        ImageButton_FirstPage.Enabled = value;
        ImageButton_PrevPage.Enabled = value;
        ImageButton_NextPage.Enabled = value;
        ImageButton_LastPage.Enabled = value;
    }

    #endregion

    public String HeaderLabel
    {
        get { return Header.Text; }
        set
        {
            Header.Text = value;
            PageName = value;
            //CollapsiblePanelExtender2.CollapsedText = value;
        }
    }

    private Type searchObjectType;
    /// <summary>
    /// A list�ra vonatkoz� keres�si objektum t�pusa
    /// </summary>
    public Type SearchObjectType
    {
        get { return searchObjectType; }
        set 
        { 
            searchObjectType = value;
            ListColumnsPopup1.SearchObjectType = value;
            
            SetDefaultSearchTemplate();

            // Default oszlopl�that�s�gok be�ll�t�sa
            SetDefaultColumnsVisibilityIfExist();

            // BUG_8359
            ListSortPopup1.SearchObjectType = value;
            SetDefaultOrderByIfExist();
        }
    }

    // BUG_8359
    private void SetDefaultOrderByIfExist()
    {
        if ((IsReload || !IsPostBack))
        {
            bool existsDefault = ListSortPopup1.SetDefaultOrderByIfExist();
        }
    }

    private void ChangeListColumnsImage(bool existsDefaultTemplate)
    {
        string imageName = existsDefaultTemplate ? "column_on_active.gif" : "column_on.gif";
        string imageName2 = existsDefaultTemplate ? "column_on_active2.gif" : "column_on2.gif";

        ImageListColumns.ImageUrl = "../images/hu/egyeb/" + imageName;
        ImageListColumns.Attributes["onmouseover"] = "swapByName(this.id,'" + imageName2 + "')";
        ImageListColumns.Attributes["onmouseout"] = "swapByName(this.id,'" + imageName + "')";
    }

    protected void ListColumnsPopup_DefaultColumnsVisibilitySaved(object sender, EventArgs e)
    {
        // ikon �t�ll�t�sa, hogy l�tsz�djon, van bet�lt�tt oszlop�ll�t�s template
        ChangeListColumnsImage(true);
    }


    private String customSearchObjectSessionName = "";
    /// <summary>
    /// Ha be van �ll�tva, ezt veszi figyelembe a keres�si objektum vizsg�latakor, nem pedig a SearchObjectType -ot
    /// </summary>
    public String CustomSearchObjectSessionName
    {
        get { return customSearchObjectSessionName; }
        set
        {
            customSearchObjectSessionName = value;
            ListColumnsPopup1.CustomSearchObjectSessionName = value;
        }
    }

    private string SearchObjetSessionName
    {
        get
        {
            if (!String.IsNullOrEmpty(customSearchObjectSessionName))
                return customSearchObjectSessionName;

            if (searchObjectType != null)
                return searchObjectType.Name;

            return String.Empty;
        }
    }

    private bool SetDefaultSearchTemplate()
    {
        return this.SetDefaultSearchTemplate(false);
    }

    private bool SetDefaultSearchTemplate(bool sessionOverWrite)
    {
        if (searchObjectType == null)
            return false;

        string sessionName = String.Empty;
        bool IsSearchObjectInSession = false;
        bool IsCustomSession = false;

        if (!String.IsNullOrEmpty(customSearchObjectSessionName))
        {
            sessionName = customSearchObjectSessionName;
            IsSearchObjectInSession = Search.IsSearchObjectInSession_CustomSessionName(Page, sessionName);
            IsCustomSession = true;
        }
        else if (searchObjectType != null)
        {
            sessionName = searchObjectType.Name;
            IsSearchObjectInSession = Search.IsSearchObjectInSession(Page, searchObjectType);
            IsCustomSession = false;
        }


        if (!SetWhereByManual())
        {
            if (!String.IsNullOrEmpty(sessionName) && (!IsSearchObjectInSession || sessionOverWrite))
            {
                object defaultSearchObject = Contentum.eUtility.UIMezoObjektumErtekek.GetDeafultSearchObject(Page, sessionName, searchObjectType);

                if (defaultSearchObject != null)
                {
                    if (IsCustomSession)
                    {
                        Search.SetSearchObject_CustomSessionName(Page, defaultSearchObject, sessionName);
                    }
                    else
                    {
                        Search.SetSearchObject(Page, defaultSearchObject);
                    }

                    return true;
                }
            }
        }

        return false;
    }

    private bool SetWhereByManual()
    {
        if (!IsPostBack)
        {
            string whereByManual = Request.QueryString.Get(QueryStringVars.WhereByManual);
            if (!String.IsNullOrEmpty(whereByManual))
            {
                //wherebymanual dek�d�l�sa
                whereByManual = Server.UrlDecode(whereByManual);
                try
                {
                    whereByManual = Encoding.UTF8.GetString(Convert.FromBase64String(whereByManual));
                }
                catch(FormatException)
                {
                    return false;
                }
                if (!String.IsNullOrEmpty(SearchObjetSessionName))
                {
                    // Ki�r�tj�k az adott sessionv�ltoz�t:
                    Contentum.eUtility.Search.RemoveSearchObjectFromSession_CustomSessionName(Page, SearchObjetSessionName);
                }

                if (searchObjectType != null)
                {
                    object SearchObject = null;

                    try
                    {
                        // ha l�tezik olyan param�terezett konstruktor, amely bool bemen� param�tert fogad
                        // (konvenci�: createExtendedSearchObjects), akkor azt h�vjuk - ha nincs, akkor
                        // a hiba�gban a megh�vjuk default konstruktort
                        SearchObject = Activator.CreateInstance(searchObjectType, new object[] { true });
                    }
                    catch (MissingMethodException)
                    {
                        SearchObject = Activator.CreateInstance(searchObjectType);
                    }
                    FieldInfo fiWhereByManual = searchObjectType.GetField("WhereByManual");
                    if (fiWhereByManual != null)
                    {
                        fiWhereByManual.SetValue(SearchObject, whereByManual);
                    }

                    if (!String.IsNullOrEmpty(SearchObjetSessionName))
                    {
                        // Ki�r�tj�k az adott sessionv�ltoz�t:
                        Contentum.eUtility.Search.SetSearchObject_CustomSessionName(Page, SearchObject, SearchObjetSessionName);
                    }

                }

                return true;
            }
            else
            {
                //wherebymanual t�rl�se
                object SearchObject = null;
                if (!String.IsNullOrEmpty(SearchObjetSessionName))
                {
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, SearchObjetSessionName))
                    {
                        SearchObject = Search.GetSearchObject_CustomSessionName(Page, new Object(), SearchObjetSessionName);
                    }
                }

                if(SearchObject != null)
                {
                    FieldInfo fiWhereByManual = SearchObject.GetType().GetField("WhereByManual");
                    if (fiWhereByManual != null)
                    {
                        fiWhereByManual.SetValue(SearchObject, String.Empty);
                    }
                }
            }
        }

        return false;
    }
     
    private bool nincsFeltoltes = false;
    public bool NincsFeltoltes 
    {
        get
        {
            return nincsFeltoltes;
        }
        set
        {
            nincsFeltoltes = value;
        }
    }

    #region Buttons

    #region Search

    public ImageButton SearchButton
    {
        get
        {
            return ImageSearch;
        }
    }

    public bool SearchEnabled
    {
        get { return ImageSearch.Enabled; }
        set { SetSearchButtonEnabledProperty(ImageSearch, value); }
    }

    public bool SearchVisible
    {
        get { return ImageSearch.Visible; }
        set { ImageSearch.Visible = value; }
    }

    public String SearchOnClientClick
    {
        get { return ImageSearch.OnClientClick; }
        set { ImageSearch.OnClientClick = value; }
    }
    #endregion

    #region NumericSearch

    public ImageButton NumericSearchButton
    {
        get
        {
            return ImageNumericSearch;
        }
    }
    public bool NumericSearchEnabled
    {
        get { return ImageNumericSearch.Enabled; }
        set { SetSearchButtonEnabledProperty(ImageNumericSearch, value); }
    }

    public bool NumericSearchVisible
    {
        get { return ImageNumericSearch.Visible; }
        set { ImageNumericSearch.Visible = value; }
    }

    public String NumericSearchOnClientClick
    {
        get { return ImageNumericSearch.OnClientClick; }
        set { ImageNumericSearch.OnClientClick = value; }
    }
    #endregion

    #region FTSearch

    public ImageButton FTSearchButton
    {
        get
        {
            return ImageFTSearch;
        }
    }

    public bool FTSearchEnabled
    {
        get { return ImageFTSearch.Enabled; }
        set { SetSearchButtonEnabledProperty(ImageFTSearch, value); }
    }

    public bool FTSearchVisible
    {
        get { return ImageFTSearch.Visible; }
        set { ImageFTSearch.Visible = value; }
    }

    public String FTSearchOnClientClick
    {
        get { return ImageFTSearch.OnClientClick; }
        set { ImageFTSearch.OnClientClick = value; }
    }
    #endregion

    #region New

    public ImageButton NewButton
    {
        get
        {
            return ImageNew;
        }
    }

    public bool NewEnabled
    {
        get { return ImageNew.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageNew, value); }
    }

    public bool NewVisible
    {
        get { return ImageNew.Visible; }
        set { ImageNew.Visible = value; }
    }

    public String NewOnClientClick
    {
        get { return ImageNew.OnClientClick; }
        set { ImageNew.OnClientClick = value; }
    }
    
    public string NewToolTip
    {
        get { return ImageNew.ToolTip; }
        set { ImageNew.ToolTip = value; }
    }
    #endregion

    #region Add

    public ImageButton AddButton
    {
        get
        {
            return ImageAdd;
        }
    }

    public bool AddEnabled
    {
        get { return ImageAdd.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageAdd, value); }
    }

    public bool AddVisible
    {
        get { return ImageAdd.Visible; }
        set { ImageAdd.Visible = value; }
    }

    public String AddOnClientClick
    {
        get { return ImageAdd.OnClientClick; }
        set { ImageAdd.OnClientClick = value; }
    }

    public string AddToolTip
    {
        get { return ImageAdd.ToolTip; }
        set { ImageAdd.ToolTip = value; }
    }
    #endregion

    #region View

    public ImageButton ViewButton
    {
        get
        {
            return ImageView;
        }
    }

    public ImageButton ImageButtonView
    {
        get
        {
            return ImageView;
        }
    }
    public bool ViewEnabled
    {
        get { return ImageView.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageView, value); }
    }

    public bool ViewVisible
    {
        get { return ImageView.Visible; }
        set { ImageView.Visible = value; }
    }

    public String ViewOnClientClick
    {
        get { return ImageView.OnClientClick; }
        set { ImageView.OnClientClick = value; }
    }
    public string ViewToolTip
    {
        get { return ImageView.ToolTip; }
        set { ImageView.ToolTip = value; }
    }
    #endregion

    #region Modify

    public ImageButton ModifyButton
    {
        get
        {
            return ImageModify;
        }
    }

    public bool ModifyEnabled
    {
        get { return ImageModify.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageModify, value); }
    }

    public bool ModifyVisible
    {
        get { return ImageModify.Visible; }
        set { ImageModify.Visible = value; }
    }

    public String ModifyOnClientClick
    {
        get { return ImageModify.OnClientClick; }
        set { ImageModify.OnClientClick = value; }
    }
    public string ModifyToolTip
    {
        get { return ImageModify.ToolTip; }
        set { ImageModify.ToolTip = value; }
    }
    #endregion

    #region UgyiratTerkep

    public ImageButton UgyiratTerkepButton
    {
        get
        {
            return ImageUgyiratTerkep;
        }
    }

    public bool UgyiratTerkepEnabled
    {
        get { return ImageUgyiratTerkep.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageUgyiratTerkep, value); }
    }

    public bool UgyiratTerkepVisible
    {
        get { return ImageUgyiratTerkep.Visible; }
        set { ImageUgyiratTerkep.Visible = value; }
    }

    public String UgyiratTerkepOnClientClick
    {
        get { return ImageUgyiratTerkep.OnClientClick; }
        set { ImageUgyiratTerkep.OnClientClick = value; }
    }
    public string UgyiratTerkepToolTip
    {
        get { return ImageUgyiratTerkep.ToolTip; }
        set { ImageUgyiratTerkep.ToolTip = value; }
    }
    #endregion

    #region Invalidate

    public ImageButton InvalidateButton
    {
        get
        {
            return ImageInvalidate;
        }
    }

    public bool InvalidateEnabled
    {
        get { return ImageInvalidate.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageInvalidate, value); }
    }

    public bool InvalidateVisible
    {
        get { return ImageInvalidate.Visible; }
        set { ImageInvalidate.Visible = value; }
    }

    public String InvalidateOnClientClick
    {
        get { return ImageInvalidate.OnClientClick; }
        set { ImageInvalidate.OnClientClick = value; }
    }
    #endregion

    #region Revalidate

    public ImageButton RevalidateButton
    {
        get
        {
            return ImageRevalidate;
        }
    }

    public bool RevalidateEnabled
    {
        get { return ImageRevalidate.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageRevalidate, value); }
    }

    public bool RevalidateVisible
    {
        get { return ImageRevalidate.Visible; }
        set { ImageRevalidate.Visible = value; }
    }

    public String RevalidateOnClientClick
    {
        get { return ImageRevalidate.OnClientClick; }
        set { ImageRevalidate.OnClientClick = value; }
    }
    #endregion

    #region History

    public ImageButton HistoryButton
    {
        get
        {
            return ImageHistory;
        }
    }

    public bool HistoryEnabled
    {
        get { return ImageHistory.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageHistory, value); }
    }

    public bool HistoryVisible
    {
        get { return ImageHistory.Visible; }
        set { ImageHistory.Visible = value; }
    }

    public String HistoryOnClientClick
    {
        get { return ImageHistory.OnClientClick; }
        set { ImageHistory.OnClientClick = value; }
    }
    #endregion

    #region Refresh

    public ImageButton RefreshButton
    {
        get
        {
            return ImageRefresh;
        }
    }

    public bool RefreshEnabled
    {
        get { return ImageRefresh.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageRefresh, value); }

    }
    public bool RefreshVisible
    {
        get { return ImageRefresh.Visible; }
        set { ImageRefresh.Visible = value; }
    }

    public String RefreshOnClientClick
    {
        get { return ImageRefresh.OnClientClick; }
        set { ImageRefresh.OnClientClick = value; }
    }
    public ImageButton RefreshImageButton
    {
        get { return ImageRefresh; }
    }
    #endregion


    #region DefaultFilter

    public ImageButton DefaultFilterButton
    {
        get
        {
            return ImageDefaultFilter;
        }
    }

    public bool DefaultFilterEnabled
    {
        get { return ImageDefaultFilter.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageDefaultFilter, value); }
    }
    public bool DefaultFilterVisible
    {
        get { return ImageDefaultFilter.Visible; }
        set { ImageDefaultFilter.Visible = value; }
    }

    public String DefaultFilterOnClientClick
    {
        get { return ImageDefaultFilter.OnClientClick; }
        set { ImageDefaultFilter.OnClientClick = value; }
    }
    public ImageButton DefaultFilterImageButton
    {
        get { return ImageDefaultFilter; }
    }
    #endregion


    #region Lock
    public CustomFunctionImageButton LockButton
    {
        get
        {
            return ImageLock;
        }
    }
    public CustomFunctionImageButton Lock
    {
        get { return ImageLock; }
        set { ImageLock = value; }
    }
    public bool LockEnabled
    {
        get { return ImageLock.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageLock, value); }
    }
    public bool LockVisible
    {
        get { return ImageLock.Visible; }
        set { ImageLock.Visible = value; }
    }
    public String LockOnClientClick
    {
        get { return ImageLock.OnClientClick; }
        set { ImageLock.OnClientClick = value; }
    }
    #endregion

    #region Unlock
    public CustomFunctionImageButton UnLockButton
    {
        get
        {
            return ImageUnLock;
        }
    }
    public CustomFunctionImageButton Unlock
    {
        get { return ImageUnLock; }
        set { ImageUnLock = value; }
    }
    public bool UnlockEnabled
    {
        get { return ImageUnLock.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageUnLock, value); }
    }
    public bool UnlockVisible
    {
        get { return ImageUnLock.Visible; }
        set { ImageUnLock.Visible = value; }
    }
    public String UnlockOnClientClick
    {
        get { return ImageUnLock.OnClientClick; }
        set { ImageUnLock.OnClientClick = value; }
    }
    #endregion


    #region PrintHtml
    public bool PrintHtmlEnabled
    {
        get { return PrintHtml.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(PrintHtml.PrintHtmlImage, value); }
    }

    public bool PrintHtmlVisible
    {
        get { return PrintHtml.Visible; }
        set { PrintHtml.Visible = value; }
    }

    public String PrintHtmlOnClientClick
    {
        get { return PrintHtml.OnClientClick; }
        set { PrintHtml.OnClientClick = value; }
    }

    #endregion PrintHtml

    /// <summary>
    /// A baloldali gombok enged�lyez�se jogosults�g alapj�n
    /// </summary>
    /// <param name="FunctionCodePrefix">Funkci� k�d el�tagja</param>
    public void EnableLeftButtonsByFunctionRight(string FunctionCodePrefix)
    {
        NewEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.New);
        ViewEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.View);
        ModifyEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.Modify);
        UgyiratTerkepEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.View);
        InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.Invalidate);
        RevalidateEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.Invalidate);
        HistoryEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.ViewHistory);
        LockEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.Lock);
        UnlockEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.Lock);
        //bernat.laszlo added
        ExportEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.ExcelExport);
        #region CR3155 - CSV export
        CSVExportEnabled = FunctionRights.GetFunkcioJog(Page, FunctionCodePrefix + CommandName.CSVExport);
        #endregion
    }

    #region Bontas

    public bool BontasEnabled
    {
        get { return ImageBontas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageBontas, value); }
    }
    public bool BontasVisible
    {
        get { return ImageBontas.Visible; }
        set { ImageBontas.Visible = value; }
    }
    public String BontasOnClientClick
    {
        get { return ImageBontas.OnClientClick; }
        set { ImageBontas.OnClientClick = value; }
    }
    #endregion

    #region EloadoiIv

    public bool EloadoiIvEnabled
    {
        get { return ImageEloadoiIv.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageEloadoiIv, value); }
    }
    public bool EloadoiIvVisible
    {
        get { return ImageEloadoiIv.Visible; }
        set { ImageEloadoiIv.Visible = value; }
    }
    public String EloadoiIvOnClientClick
    {
        get { return ImageEloadoiIv.OnClientClick; }
        set { ImageEloadoiIv.OnClientClick = value; }
    }
    #endregion

    #region PeresEloadoiIv

    public bool PeresEloadoiIvEnabled
    {
        get { return ImagePeresEloadoiIv.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImagePeresEloadoiIv, value); }
    }
    public bool PeresEloadoiIvVisible
    {
        get { return ImagePeresEloadoiIv.Visible; }
        set { ImagePeresEloadoiIv.Visible = value; }
    }
    public String PeresEloadoiIvOnClientClick
    {
        get { return ImagePeresEloadoiIv.OnClientClick; }
        set { ImagePeresEloadoiIv.OnClientClick = value; }
    }
    #endregion

    #region Iktatas
    public bool IktatasEnabled
    {
        get { return ImageIktatas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageIktatas, value); }
    }
    public bool IktatasVisible
    {
        get { return ImageIktatas.Visible; }
        set { ImageIktatas.Visible = value; }
    }
    public String IktatasOnClientClick
    {
        get { return ImageIktatas.OnClientClick; }
        set { ImageIktatas.OnClientClick = value; }
    }
    #endregion

    #region IktatasMegtagadas

    public CustomFunctionImageButton IktatasMegtagadas
    {
        get { return ImageIktatasMegtagadas; }
        set { ImageIktatasMegtagadas = value; }
    }

    public bool IktatasMegtagadasEnabled
    {
        get { return ImageIktatasMegtagadas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageIktatasMegtagadas, value); }
    }
    public bool IktatasMegtagadasVisible
    {
        get { return ImageIktatasMegtagadas.Visible; }
        set { ImageIktatasMegtagadas.Visible = value; }
    }
    public String IktatasMegtagadasOnClientClick
    {
        get { return ImageIktatasMegtagadas.OnClientClick; }
        set { ImageIktatasMegtagadas.OnClientClick = value; }
    }
    #endregion IktatasMegtagadas

    #region BejovoIratIktatas

    public CustomFunctionImageButton BejovoIratIktatas
    {
        get { return ImageBejovoIratIktatas; }
        set { ImageBejovoIratIktatas = value; }
    }
    
    public bool BejovoIratIktatasEnabled
    {
        get { return ImageBejovoIratIktatas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageBejovoIratIktatas, value); }
    }
    public bool BejovoIratIktatasVisible
    {
        get { return ImageBejovoIratIktatas.Visible; }
        set { ImageBejovoIratIktatas.Visible = value; }
    }
    public String BejovoIratIktatasOnClientClick
    {
        get { return ImageBejovoIratIktatas.OnClientClick; }
        set { ImageBejovoIratIktatas.OnClientClick = value; }
    }
    #endregion

    #region BelsoIratIktatas
    public bool BelsoIratIktatasEnabled
    {
        get { return ImageBelsoIratIktatas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageBelsoIratIktatas, value); }
    }
    public bool BelsoIratIktatasVisible
    {
        get { return ImageBelsoIratIktatas.Visible; }
        set { ImageBelsoIratIktatas.Visible = value; }
    }
    public String BelsoIratIktatasOnClientClick
    {
        get { return ImageBelsoIratIktatas.OnClientClick; }
        set { ImageBelsoIratIktatas.OnClientClick = value; }
    }
    #endregion

    #region BejovoIratIktatasAlszamra

    public CustomFunctionImageButton BejovoIratIktatasAlszamra
    {
        get { return ImageBejovoIratIktatasAlszamra; }
        set { ImageBejovoIratIktatasAlszamra = value; }
    }

    public bool BejovoIratIktatasAlszamraEnabled
    {
        get { return ImageBejovoIratIktatasAlszamra.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageBejovoIratIktatasAlszamra, value); }
    }
    public bool BejovoIratIktatasAlszamraVisible
    {
        get { return ImageBejovoIratIktatasAlszamra.Visible; }
        set { ImageBejovoIratIktatasAlszamra.Visible = value; }
    }
    public String BejovoIratIktatasAlszamraOnClientClick
    {
        get { return ImageBejovoIratIktatasAlszamra.OnClientClick; }
        set { ImageBejovoIratIktatasAlszamra.OnClientClick = value; }
    }
    #endregion

    #region BelsoIratIktatasAlszamra
    public bool BelsoIratIktatasAlszamraEnabled
    {
        get { return ImageBelsoIratIktatasAlszamra.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageBelsoIratIktatasAlszamra, value); }
    }
    public bool BelsoIratIktatasAlszamraVisible
    {
        get { return ImageBelsoIratIktatasAlszamra.Visible; }
        set { ImageBelsoIratIktatasAlszamra.Visible = value; }
    }
    public String BelsoIratIktatasAlszamraOnClientClick
    {
        get { return ImageBelsoIratIktatasAlszamra.OnClientClick; }
        set { ImageBelsoIratIktatasAlszamra.OnClientClick = value; }
    }
    #endregion

    #region AtIktatas
    public bool AtIktatasEnabled
    {
        get { return ImageAtIktatas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageAtIktatas, value); }
    }
    public bool AtIktatasVisible
    {
        get { return ImageAtIktatas.Visible; }
        set { ImageAtIktatas.Visible = value; }
    }
    public String AtIktatasOnClientClick
    {
        get { return ImageAtIktatas.OnClientClick; }
        set { ImageAtIktatas.OnClientClick = value; }
    }
    #endregion

    #region MunkapeldanyIktatasa
    // CR 3110: Iktat�s Munkap�ld�nyra Rendszerparam�ter alapj�n (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapj�n
    public CustomFunctionImageButton MunkapeldanyIktatasa
    {
        get { return ImageMunkapeldanyIktatasa; }
        set { ImageMunkapeldanyIktatasa = value; }
    }

    public bool MunkapeldanyIktatasaEnabled
    {
        get { return ImageMunkapeldanyIktatasa.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageMunkapeldanyIktatasa, value); }
    }
    public bool MunkapeldanyIktatasaVisible
    {
        get { return ImageMunkapeldanyIktatasa.Visible; }
        set { ImageMunkapeldanyIktatasa.Visible = value; }
    }
    public String MunkapeldanyIktatasaOnClientClick
    {
        get { return ImageMunkapeldanyIktatasa.OnClientClick; }
        set { ImageMunkapeldanyIktatasa.OnClientClick = value; }
    }
    #endregion

    #region Expedialas
    public CustomFunctionImageButton Expedialas
    {
        get { return ImageExpedialas; }
        set { ImageExpedialas = value; }
    }

    public bool ExpedialasEnabled
    {
        get { return ImageExpedialas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageExpedialas, value); }
    }
    public bool ExpedialasVisible
    {
        get { return ImageExpedialas.Visible; }
        set { ImageExpedialas.Visible = value; }
    }
    public String ExpedialasOnClientClick
    {
        get { return ImageExpedialas.OnClientClick; }
        set { ImageExpedialas.OnClientClick = value; }
    }
    #endregion

    #region Postazas
    public CustomFunctionImageButton Postazas
    {
        get { return ImagePostazas; }
        set { ImagePostazas = value; }
    }

    public bool PostazasEnabled
    {
        get { return ImagePostazas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImagePostazas, value); }
    }
    public bool PostazasVisible
    {
        get { return ImagePostazas.Visible; }
        set { ImagePostazas.Visible = value; }
    }
    public String PostazasOnClientClick
    {
        get { return ImagePostazas.OnClientClick; }
        set { ImagePostazas.OnClientClick = value; }
    }
    #endregion

    #region Felszabaditas

    public CustomFunctionImageButton FelszabaditasButton
    {
        get { return ImageFelszabaditas; }
        set { ImageFelszabaditas = value; }
    }

    public bool FelszabaditasEnabled
    {
        get { return ImageFelszabaditas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageFelszabaditas, value); }
    }
    public bool FelszabaditasVisible
    {
        get { return ImageFelszabaditas.Visible; }
        set { ImageFelszabaditas.Visible = value; }
    }
    public string FelszabaditasOnClientClick
    {
        get { return ImageFelszabaditas.OnClientClick; }
        set { ImageFelszabaditas.OnClientClick = value; }
    }
    #endregion


    #region Sztorno
    public CustomFunctionImageButton SztornoButton
    {
        get { return ImageSztorno; }
    }
    public CustomFunctionImageButton Sztorno
    {
        get { return ImageSztorno; }
        set { ImageSztorno = value; }
    }

    public bool SztornoEnabled
    {
        get { return ImageSztorno.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageSztorno, value); }
    }
    public bool SztornoVisible
    {
        get { return ImageSztorno.Visible; }
        set { ImageSztorno.Visible = value; }
    }
    public String SztornoOnClientClick
    {
        get { return ImageSztorno.OnClientClick; }
        set { ImageSztorno.OnClientClick = value; }
    }

    private string _SztornoButtonToolTip;
    public string SztornoButtonToolTip
    {
        get
        {
            return ImageSztorno.ToolTip;
        }
        set
        {
            if (ImageSztorno != null)
            {
                ImageSztorno.ToolTip = value;
            }
            else
            {
                _SztornoButtonToolTip = value;
            }
        }
    }
    #endregion

    #region SendObjects
    public bool SendObjectsEnabled
    {
        get { return ImageSendObjects.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageSendObjects, value); }
    }
    public bool SendObjectsVisible
    {
        get { return ImageSendObjects.Visible; }
        set { ImageSendObjects.Visible = value; }
    }
    public String SendObjectsOnClientClick
    {
        get { return ImageSendObjects.OnClientClick; }
        set { ImageSendObjects.OnClientClick = value; }
    }
    #endregion

    #region Boritonyomtatas
    public bool BoritonyomtatasEnabled
    {
        get { return ImageBoritonyomtatas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageBoritonyomtatas, value); }
    }
    public bool BoritonyomtatasVisible
    {
        get { return ImageBoritonyomtatas.Visible; }
        set { ImageBoritonyomtatas.Visible = value; }
    }
    public String BoritonyomtatasOnClientClick
    {
        get { return ImageBoritonyomtatas.OnClientClick; }
        set { ImageBoritonyomtatas.OnClientClick = value; }
    }
    #endregion

    #region Ugyiratpotlonyomtatas
    public bool UgyiratpotlonyomtatasEnabled
    {
        get { return ImageUgyiratpotlonyomtatas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageUgyiratpotlonyomtatas, value); }
    }
    public bool UgyiratpotlonyomtatasVisible
    {
        get { return ImageUgyiratpotlonyomtatas.Visible; }
        set { ImageUgyiratpotlonyomtatas.Visible = value; }
    }
    public String UgyiratpotlonyomtatasOnClientClick
    {
        get { return ImageUgyiratpotlonyomtatas.OnClientClick; }
        set { ImageUgyiratpotlonyomtatas.OnClientClick = value; }
    }
    #endregion

    #region IktatokonyvLezaras
    public bool IktatokonyvLezarasEnabled
    {
        get { return ImageIktatokonyvLezaras.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageIktatokonyvLezaras, value); }
    }

    public bool IktatokonyvLezarasVisible
    {
        get { return ImageIktatokonyvLezaras.Visible; }
        set { ImageIktatokonyvLezaras.Visible = value; }
    }

    public String IktatokonyvLezarasOnClientClick
    {
        get { return ImageIktatokonyvLezaras.OnClientClick; }
        set { ImageIktatokonyvLezaras.OnClientClick = value; }
    }
    #endregion

    #region Atvetel
    public CustomFunctionImageButton AtvetelButton
    {
        get
        {
            return ImageAtvetel;
        }
    }
    public bool AtvetelEnabled
    {
        get { return ImageAtvetel.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageAtvetel, value); }
    }
    public bool AtvetelVisible
    {
        get { return ImageAtvetel.Visible; }
        set { ImageAtvetel.Visible = value; }
    }
    public String AtvetelOnClientClick
    {
        get { return ImageAtvetel.OnClientClick; }
        set { ImageAtvetel.OnClientClick = value; }
    }
    #endregion

    #region AtvetelListaval
    public bool AtvetelListavalEnabled
    {
        get { return ImageAtvetelListaval.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageAtvetelListaval, value); }
    }
    public bool AtvetelListavalVisible
    {
        get { return ImageAtvetelListaval.Visible; }
        set { ImageAtvetelListaval.Visible = value; }
    }
    public String AtvetelListavalOnClientClick
    {
        get { return ImageAtvetelListaval.OnClientClick; }
        set { ImageAtvetelListaval.OnClientClick = value; }
    }
    #endregion

    #region AtvetelUgyintezesre
    public bool AtvetelUgyintezesreEnabled
    {
        get { return ImageAtvetelUgyintezesre.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageAtvetelUgyintezesre, value); }
    }
    public bool AtvetelUgyintezesreVisible
    {
        get { return ImageAtvetelUgyintezesre.Visible; }
        set { ImageAtvetelUgyintezesre.Visible = value; }
    }
    public String AtvetelUgyintezesreOnClientClick
    {
        get { return ImageAtvetelUgyintezesre.OnClientClick; }
        set { ImageAtvetelUgyintezesre.OnClientClick = value; }
    }
    #endregion

    #region Visszakuldes
    public CustomFunctionImageButton VisszakuldesButton
    {
        get
        {
            return ImageVisszakuldes;
        }
    }
    public bool VisszakuldesEnabled
    {
        get { return ImageVisszakuldes.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageVisszakuldes, value); }
    }
    public bool VisszakuldesVisible
    {
        get { return ImageVisszakuldes.Visible; }
        set { ImageVisszakuldes.Visible = value; }
    }
    public String VisszakuldesOnClientClick
    {
        get { return ImageVisszakuldes.OnClientClick; }
        set { ImageVisszakuldes.OnClientClick = value; }
    }
    #endregion

    #region CR3128 -Iratp�ld�ny l�trehoz�s nyom�gomb az iratok list�j�ra
    public CustomFunctionImageButton IratpeldanyLetrehozasButton
    {
        get
        {
            return ImageIratpeldanyLetrehozas;
        }
    }
    public bool IratpeldanyLetrehozasEnabled
    {
        get { return ImageIratpeldanyLetrehozas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageIratpeldanyLetrehozas, value); }
    }
    public bool IratpeldanyLetrehozasVisible
    {
        get { return ImageIratpeldanyLetrehozas.Visible; }
        set { ImageIratpeldanyLetrehozas.Visible = value; }
    }
    public string IratpeldanyLetrehozasOnClientClick
    {
        get { return ImageIratpeldanyLetrehozas.OnClientClick; }
        set { ImageIratpeldanyLetrehozas.OnClientClick = value; }
    }
    #endregion

    #region CR3129 - Sz�ks�g lenne egy �j ikonra az al��r�shoz
    public CustomFunctionImageButton IratCsatolmanyAlairasButton
    {
        get
        {
            return ImageIratCsatolmanyAlairas;
        }
    }
    public bool IratCsatolmanyAlairasEnabled
    {
        get { return ImageIratCsatolmanyAlairas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageIratCsatolmanyAlairas, value); }
    }
    public bool IratCsatolmanyAlairasVisible
    {
        get { return ImageIratCsatolmanyAlairas.Visible; }
        set { ImageIratCsatolmanyAlairas.Visible = value; }
    }
    public string IratCsatolmanyAlairasOnClientClick
    {
        get { return ImageIratCsatolmanyAlairas.OnClientClick; }
        set { ImageIratCsatolmanyAlairas.OnClientClick = value; }
    }
    #endregion

    #region Athelyezes
    public CustomFunctionImageButton AthelyezesButton
    {
        get
        {
            return ImageAthelyezes;
        }
    }
    public bool AthelyezesEnabled
    {
        get { return ImageAthelyezes.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageAthelyezes, value); }
    }
    public bool AthelyezesVisible
    {
        get { return ImageAthelyezes.Visible; }
        set { ImageAthelyezes.Visible = value; }
    }
    public string AthelyezesOnClientClick
    {
        get { return ImageAthelyezes.OnClientClick; }
        set { ImageAthelyezes.OnClientClick = value; }
    }
    #endregion


    #region Szignalas
    public bool SzignalasEnabled
    {
        get { return ImageSzignalas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageSzignalas, value); }
    }
    public bool SzignalasVisible
    {
        get { return ImageSzignalas.Visible; }
        set { ImageSzignalas.Visible = value; }
    }
    public String SzignalasOnClientClick
    {
        get { return ImageSzignalas.OnClientClick; }
        set { ImageSzignalas.OnClientClick = value; }
    }
    #endregion

    #region Atadas
    public bool AtadasEnabled
    {
        get { return ImageAtadas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageAtadas, value); }
    }
    public bool AtadasVisible
    {
        get { return ImageAtadas.Visible; }
        set { ImageAtadas.Visible = value; }
    }
    public String AtadasOnClientClick
    {
        get { return ImageAtadas.OnClientClick; }
        set { ImageAtadas.OnClientClick = value; }
    }
    #endregion

    #region AtadasListaval
    public bool AtadasListavalEnabled
    {
        get { return ImageAtadasListaval.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageAtadasListaval, value); }
    }
    public bool AtadasListavalVisible
    {
        get { return ImageAtadasListaval.Visible; }
        set { ImageAtadasListaval.Visible = value; }
    }
    public String AtadasListavalOnClientClick
    {
        get { return ImageAtadasListaval.OnClientClick; }
        set { ImageAtadasListaval.OnClientClick = value; }
    }
    #endregion

    #region AtadasraKijelol
    public bool AtadasraKijelolEnabled
    {
        get { return ImageAtadasra.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageAtadasra, value); }
    }
    public bool AtadasraKijelolVisible
    {
        get { return ImageAtadasra.Visible; }
        set { ImageAtadasra.Visible = value; }
    }
    public String AtadasraKijelolOnClientClick
    {
        get { return ImageAtadasra.OnClientClick; }
        set { ImageAtadasra.OnClientClick = value; }
    }
    public CustomFunctionImageButton AtadasraKijelolImageButton
    {
        get { return ImageAtadasra; }        
    }


    #endregion

    #region Irattarozasra
    public bool IrattarozasraEnabled
    {
        get { return ImageIrattarozasra.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageIrattarozasra, value); }
    }
    public bool IrattarozasraVisible
    {
        get { return ImageIrattarozasra.Visible; }
        set { ImageIrattarozasra.Visible = value; }
    }
    public String IrattarozasraOnClientClick
    {
        get { return ImageIrattarozasra.OnClientClick; }
        set { ImageIrattarozasra.OnClientClick = value; }
    }

    public String IrattarozasraToolTip
    {
        get { return ImageIrattarozasra.ToolTip; }
        set { ImageIrattarozasra.ToolTip = value; }
    }
    #endregion

    #region KiadasOsztalyra
    public bool KiadasOsztalyraEnabled
    {
        get { return ImageKiadasOsztalyra.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageKiadasOsztalyra, value); }
    }
    public bool KiadasOsztalyraVisible
    {
        get { return ImageKiadasOsztalyra.Visible; }
        set { ImageKiadasOsztalyra.Visible = value; }
    }
    public String KiadasOsztalyraOnClientClick
    {
        get { return ImageKiadasOsztalyra.OnClientClick; }
        set { ImageKiadasOsztalyra.OnClientClick = value; }
    }
    #endregion

    #region Kolcsonzes
    public CustomFunctionImageButton Kolcsonzes
    {
        get { return ImageKolcsonzes; }
        set { ImageKolcsonzes = value; }
    }    
    public bool KolcsonzesEnabled
    {
        get { return ImageKolcsonzes.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageKolcsonzes, value); }
    }
    public bool KolcsonzesVisible
    {
        get { return ImageKolcsonzes.Visible; }
        set { ImageKolcsonzes.Visible = value; }
    }
    public String KolcsonzesOnClientClick
    {
        get { return ImageKolcsonzes.OnClientClick; }
        set { ImageKolcsonzes.OnClientClick = value; }
    }
    #endregion       
    
    #region KolcsonzesVissza
    public CustomFunctionImageButton KolcsonzesVissza
    {
        get { return ImageKolcsonzesVissza; }
        set { ImageKolcsonzesVissza = value; }
    }
    public bool KolcsonzesVisszaEnabled
    {
        get { return ImageKolcsonzesVissza.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageKolcsonzesVissza, value); }
    }
    public bool KolcsonzesVisszaVisible
    {
        get { return ImageKolcsonzesVissza.Visible; }
        set { ImageKolcsonzesVissza.Visible = value; }
    }
    public String KolcsonzesVisszaOnClientClick
    {
        get { return ImageKolcsonzesVissza.OnClientClick; }
        set { ImageKolcsonzesVissza.OnClientClick = value; }
    }
    #endregion

    #region ImageKolcsonzesJovahagyas
    public CustomFunctionImageButton KolcsonzesJovahagyas
    {
        get { return ImageKolcsonzesJovahagyas; }
        set { ImageKolcsonzesJovahagyas = value; }
    }
    public bool KolcsonzesJovahagyasEnabled
    {
        set { SetRightFunctionButtonEnabledProperty(ImageKolcsonzesJovahagyas, value); }
    }
    public bool KolcsonzesJovahagyasVisible
    {
        get { return ImageKolcsonzesJovahagyas.Visible; }
        set { ImageKolcsonzesJovahagyas.Visible = value; }
    }
    public String KolcsonzesJovahagyasOnClientClick
    {
        get { return ImageKolcsonzesJovahagyas.OnClientClick; }
        set { ImageKolcsonzesJovahagyas.OnClientClick = value; }
    }
    #endregion

    #region ImageKolcsonzesVisszautasitas
    public CustomFunctionImageButton KolcsonzesVisszautasitas
    {
        get { return ImageKolcsonzesVisszautasitas; }
        set { ImageKolcsonzesVisszautasitas = value; }
    }
    public bool KolcsonzesVisszautasitasEnabled
    {
        get { return ImageKolcsonzesVisszautasitas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageKolcsonzesVisszautasitas, value); }
    }
    public bool KolcsonzesVisszautasitasVisible
    {
        get { return ImageKolcsonzesVisszautasitas.Visible; }
        set { ImageKolcsonzesVisszautasitas.Visible = value; }
    }
    public String KolcsonzesVisszautasitasOnClientClick
    {
        get { return ImageKolcsonzesVisszautasitas.OnClientClick; }
        set { ImageKolcsonzesVisszautasitas.OnClientClick = value; }
    }
    #endregion

    #region KolcsonzesSztorno
    public CustomFunctionImageButton KolcsonzesSztorno
    {
        get { return ImageKolcsonzesSztorno; }
        set { ImageKolcsonzesSztorno = value; }
    }
    public bool KolcsonzesSztornoEnabled
    {
        get { return ImageKolcsonzesSztorno.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageKolcsonzesSztorno, value); }
    }
    public bool KolcsonzesSztornoVisible
    {
        get { return ImageKolcsonzesSztorno.Visible; }
        set { ImageKolcsonzesSztorno.Visible = value; }
    }
    public String KolcsonzesSztornoOnClientClick
    {
        get { return ImageKolcsonzesSztorno.OnClientClick; }
        set { ImageKolcsonzesSztorno.OnClientClick = value; }
    }
    #endregion

    #region KolcsonzesKiadas
    public CustomFunctionImageButton KolcsonzesKiadas
    {
        get { return ImageKolcsonzesKiadas; }
        set { ImageKolcsonzesKiadas = value; }
    }
    public bool KolcsonzesKiadasEnabled
    {
        get { return ImageKolcsonzesKiadas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageKolcsonzesKiadas, value); }
    }
    public bool KolcsonzesKiadasVisible
    {
        get { return ImageKolcsonzesKiadas.Visible; }
        set { ImageKolcsonzesKiadas.Visible = value; }
    }
    public String KolcsonzesKiadasOnClientClick
    {
        get { return ImageKolcsonzesKiadas.OnClientClick; }
        set { ImageKolcsonzesKiadas.OnClientClick = value; }
    }
    #endregion

    #region Jegyzekrehelyezes
    public CustomFunctionImageButton Jegyzekrehelyezes
    {
        get { return ImageJegyzekrehelyezes; }
        set { ImageJegyzekrehelyezes = value; }
    }
    public bool JegyzekrehelyezesEnabled
    {
        get { return ImageJegyzekrehelyezes.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageJegyzekrehelyezes, value); }
    }
    public bool JegyzekrehelyezesVisible
    {
        get { return ImageJegyzekrehelyezes.Visible; }
        set { ImageJegyzekrehelyezes.Visible = value; }
    }
    public String JegyzekrehelyezesOnClientClick
    {
        get { return ImageJegyzekrehelyezes.OnClientClick; }
        set { ImageJegyzekrehelyezes.OnClientClick = value; }
    }
    #endregion

    #region TovabbitasraKijelol
    public bool TovabbitasraKijelolEnabled
    {
        get { return ImageTovabbitasra.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageTovabbitasra, value); }
    }
    public bool TovabbitasraKijelolVisible
    {
        get { return ImageTovabbitasra.Visible; }
        set { ImageTovabbitasra.Visible = value; }
    }
    public String TovabbitasraKijelolOnClientClick
    {
        get { return ImageTovabbitasra.OnClientClick; }
        set { ImageTovabbitasra.OnClientClick = value; }
    }
    #endregion

    #region Lezaras
    public CustomFunctionImageButton LezarasButton
    {
        get { return ImageLezaras; }
    }
    public CustomFunctionImageButton Lezaras
    {
        get { return ImageLezaras; }
        set { ImageLezaras = value; }
    }

    public bool LezarasEnabled
    {
        get { return ImageLezaras.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageLezaras, value); }
    }
    public bool LezarasVisible
    {
        get { return ImageLezaras.Visible; }
        set { ImageLezaras.Visible = value; }
    }
    public String LezarasOnClientClick
    {
        get { return ImageLezaras.OnClientClick; }
        set { ImageLezaras.OnClientClick = value; }
    }
    #endregion

    #region LezarasVisszavonas
    public CustomFunctionImageButton LezarasVisszavonas
    {
        get { return ImageLezarasVisszavonas; }
        set { ImageLezarasVisszavonas = value; }
    }

    public bool LezarasVisszavonasEnabled
    {
        get { return ImageLezarasVisszavonas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageLezarasVisszavonas, value); }
    }
    public bool LezarasVisszavonasVisible
    {
        get { return ImageLezarasVisszavonas.Visible; }
        set { ImageLezarasVisszavonas.Visible = value; }
    }
    public String LezarasVisszavonasOnClientClick
    {
        get { return ImageLezarasVisszavonas.OnClientClick; }
        set { ImageLezarasVisszavonas.OnClientClick = value; }
    }
    #endregion

    #region ElintezetteNyilvanitas
    public CustomFunctionImageButton ElintezetteNyilvanitas
    {
        get { return ImageElintezetteNyilvanitas; }
        set { ImageElintezetteNyilvanitas = value; }
    }

    public bool ElintezetteNyilvanitasEnabled
    {
        get { return ImageElintezetteNyilvanitas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageElintezetteNyilvanitas, value); }
    }
    public bool ElintezetteNyilvanitasVisible
    {
        get { return ImageElintezetteNyilvanitas.Visible; }
        set { ImageElintezetteNyilvanitas.Visible = value; }
    }
    public String ElintezetteNyilvanitasOnClientClick
    {
        get { return ImageElintezetteNyilvanitas.OnClientClick; }
        set { ImageElintezetteNyilvanitas.OnClientClick = value; }
    }
    #endregion

    #region ElintezetteNyilvanitasVisszavon
    public CustomFunctionImageButton ElintezetteNyilvanitasVisszavon
    {
        get { return ImageElintezetteNyilvanitasVisszavon; }
        set { ImageElintezetteNyilvanitasVisszavon = value; }
    }

    public bool ElintezetteNyilvanitasVisszavonEnabled
    {
        get { return ImageElintezetteNyilvanitasVisszavon.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageElintezetteNyilvanitasVisszavon, value); }
    }
    public bool ElintezetteNyilvanitasVisszavonVisible
    {
        get { return ImageElintezetteNyilvanitasVisszavon.Visible; }
        set { ImageElintezetteNyilvanitasVisszavon.Visible = value; }
    }
    public String ElintezetteNyilvanitasVisszavonOnClientClick
    {
        get { return ImageElintezetteNyilvanitasVisszavon.OnClientClick; }
        set { ImageElintezetteNyilvanitasVisszavon.OnClientClick = value; }
    }
    #endregion

    #region UgyiratIrattarAtvetel
    public CustomFunctionImageButton UgyiratIrattarAtvetel
    {
        get { return ImageUgyiratIrattarAtvetel; }
        set { ImageUgyiratIrattarAtvetel = value; }
    }

    public bool UgyiratIrattarAtvetelEnabled
    {
        get { return ImageUgyiratIrattarAtvetel.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageUgyiratIrattarAtvetel, value); }
    }
    public bool UgyiratIrattarAtvetelVisible
    {
        get { return ImageUgyiratIrattarAtvetel.Visible; }
        set { ImageUgyiratIrattarAtvetel.Visible = value; }
    }
    public String UgyiratIrattarAtvetelOnClientClick
    {
        get { return ImageUgyiratIrattarAtvetel.OnClientClick; }
        set { ImageUgyiratIrattarAtvetel.OnClientClick = value; }
    }
    
    #endregion

    #region DossziebaHelyez

    public CustomFunctionImageButton DossziebaHelyez
    {
        get { return ImageDossziebaHelyez; }
        set { ImageDossziebaHelyez = value; }
    }

    public bool DossziebaHelyezEnabled
    {
        get { return ImageDossziebaHelyez.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageDossziebaHelyez, value); }
    }
    public bool DossziebaHelyezVisible
    {
        get { return ImageDossziebaHelyez.Visible; }
        set { ImageDossziebaHelyez.Visible = value; }
    }
    public String DossziebaHelyezOnClientClick
    {
        get { return ImageDossziebaHelyez.OnClientClick; }
        set { ImageDossziebaHelyez.OnClientClick = value; }
    }

    #endregion DossziebaHelyez

    #region DossziebolKivesz

    public CustomFunctionImageButton DossziebolKivesz
    {
        get { return ImageDossziebolKivesz; }
        set { ImageDossziebolKivesz = value; }
    }

    public bool DossziebolKiveszEnabled
    {
        get { return ImageDossziebolKivesz.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageDossziebolKivesz, value); }
    }
    public bool DossziebolKiveszVisible
    {
        get { return ImageDossziebolKivesz.Visible; }
        set { ImageDossziebolKivesz.Visible = value; }
    }
    public String DossziebolKiveszOnClientClick
    {
        get { return ImageDossziebolKivesz.OnClientClick; }
        set { ImageDossziebolKivesz.OnClientClick = value; }
    }

    #endregion DossziebolKivesz

    #region SkontrobaHelyez
    public bool SkontrobaHelyezEnabled
    {
        get { return ImageSkontrobaHelyez.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageSkontrobaHelyez, value); }
    }
    public bool SkontrobaHelyezVisible
    {
        get { return ImageSkontrobaHelyez.Visible; }
        set { ImageSkontrobaHelyez.Visible = value; }
    }
    public String SkontrobaHelyezOnClientClick
    {
        get { return ImageSkontrobaHelyez.OnClientClick; }
        set { ImageSkontrobaHelyez.OnClientClick = value; }
    }

    public String SkontrobaHelyezToolTip
    {
        get { return ImageSkontrobaHelyez.ToolTip; }
        set { ImageSkontrobaHelyez.ToolTip = value; }
    }

    #endregion

    #region SkontrobolKivetel
    public bool SkontrobolKivetelEnabled
    {
        get { return ImageSkontrobolKivetel.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageSkontrobolKivetel, value); }
    }
    public bool SkontrobolKivetelVisible
    {
        get { return ImageSkontrobolKivetel.Visible; }
        set { ImageSkontrobolKivetel.Visible = value; }
    }
    public String SkontrobolKivetelOnClientClick
    {
        get { return ImageSkontrobolKivetel.OnClientClick; }
        set { ImageSkontrobolKivetel.OnClientClick = value; }
    }
    #endregion

    #region Szereles
    public bool SzerelesEnabled
    {
        get { return ImageSzereles.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageSzereles, value); }
    }
    public bool SzerelesVisible
    {
        get { return ImageSzereles.Visible; }
        set { ImageSzereles.Visible = value; }
    }
    public String SzerelesOnClientClick
    {
        get { return ImageSzereles.OnClientClick; }
        set { ImageSzereles.OnClientClick = value; }
    }
    #endregion

    #region SzerelesVisszavon
    public bool SzerelesVisszavonEnabled
    {
        get { return ImageSzerelesVisszavon.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageSzerelesVisszavon, value); }
    }
    public bool SzerelesVisszavonVisible
    {
        get { return ImageSzerelesVisszavon.Visible; }
        set { ImageSzerelesVisszavon.Visible = value; }
    }
    public String SzerelesVisszavonOnClientClick
    {
        get { return ImageSzerelesVisszavon.OnClientClick; }
        set { ImageSzerelesVisszavon.OnClientClick = value; }
    }
    #endregion

    #region Csatolas
    public bool CsatolasEnabled
    {
        get { return ImageCsatolas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageCsatolas, value); }
    }
    public bool CsatolasVisible
    {
        get { return ImageCsatolas.Visible; }
        set { ImageCsatolas.Visible = value; }
    }
    public String CsatolasOnClientClick
    {
        get { return ImageCsatolas.OnClientClick; }
        set { ImageCsatolas.OnClientClick = value; }
    }
    #endregion

    #region Export

    public ImageButton ExportButton
    {
        get
        {
            return ImageExport;
        }
    }

    public bool ExportEnabled
    {
        get { return ImageExport.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageExport, value); }
    }
    public bool ExportVisible
    {
        get { return ImageExport.Visible; }
        set { ImageExport.Visible = value; }
    }
    public String ExportOnClientClick
    {
        get { return ImageExport.OnClientClick; }
        set { ImageExport.OnClientClick = value; }
    }
    public string ExportToolTip
    {
        get { return ImageExport.ToolTip; }
        set { ImageExport.ToolTip = value; }
    }

    #endregion

    #region CR3155 - CSV export

    public ImageButton CSVExportButton
    {
        get
        {
            return ImageCSVExport;
        }
    }

    public bool CSVExportEnabled
    {
        get { return ImageCSVExport.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageCSVExport, value); }
    }
    public bool CSVExportVisible
    {
        get { return ImageCSVExport.Visible; }
        set { ImageCSVExport.Visible = value; }
    }
    public String CSVExportOnClientClick
    {
        get { return ImageCSVExport.OnClientClick; }
        set { ImageCSVExport.OnClientClick = value; }
    }
    public string CSVExportToolTip
    {
        get { return ImageCSVExport.ToolTip; }
        set { ImageCSVExport.ToolTip = value; }
    }

    #endregion

    #region CR3206 - Iratt�ri strukt�ra - Irattarbarendezes
    public bool ImageIrattarbarendezesEnabled
    {
        get { return ImageIrattarbarendezes.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageIrattarbarendezes, value); }
    }
    public bool ImageIrattarbarendezesVisible
    {
        get { return ImageIrattarbarendezes.Visible; }
        set { ImageIrattarbarendezes.Visible = value; }
    }
    public String ImageIrattarbarendezesOnClientClick
    {
        get { return ImageIrattarbarendezes.OnClientClick; }
        set { ImageIrattarbarendezes.OnClientClick = value; }
    }

    public String ImageIrattarbarendezesToolTip
    {
        get { return ImageIrattarbarendezes.ToolTip; }
        set { ImageIrattarbarendezes.ToolTip = value; }
    }
    #endregion

    #region Print
    public bool PrintEnabled
    {
        get { return ImagePrint.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImagePrint, value); }
    }
    public bool PrintVisible
    {
        get { return ImagePrint.Visible; }
        set { ImagePrint.Visible = value; }
    }
    public String PrintOnClientClick
    {
        get { return ImagePrint.OnClientClick; }
        set { ImagePrint.OnClientClick = value; }
    }
    #endregion

    #region SSRSPrint
    public bool SSRSPrintEnabled
    {
        get { return ImageSSRSPrint.Enabled; }
        set { SetLeftFunctionButtonEnabledProperty(ImageSSRSPrint, value); }
    }
    public bool SSRSPrintVisible
    {
        get { return ImageSSRSPrint.Visible; }
        set { ImageSSRSPrint.Visible = value; }
    }
    public String SSRSPrintOnClientClick
    {
        get { return ImageSSRSPrint.OnClientClick; }
        set { ImageSSRSPrint.OnClientClick = value; }
    }
    #endregion

    #region Erkeztetes
    public bool ErkeztetesEnabled
    {
        get { return ImageErkeztetes.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageErkeztetes, value); }
    }
    public bool ErkeztetesVisible
    {
        get { return ImageErkeztetes.Visible; }
        set { ImageErkeztetes.Visible = value; }
    }
    public String ErkeztetesOnClientClick
    {
        get { return ImageErkeztetes.OnClientClick; }
        set { ImageErkeztetes.OnClientClick = value; }
    }
    #endregion

    #region Felulvizsgalat
    public CustomFunctionImageButton Felulvizsgalat
    {
        get { return ImageFelulvizsgalat; }
        set { ImageFelulvizsgalat = value; }
    }

    public bool FelulvizsgalatEnabled
    {
        get { return ImageFelulvizsgalat.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageFelulvizsgalat, value); }
    }
    public bool FelulvizsgalatVisible
    {
        get { return ImageFelulvizsgalat.Visible; }
        set { ImageFelulvizsgalat.Visible = value; }
    }
    public String FelulvizsgalatOnClientClick
    {
        get { return ImageFelulvizsgalat.OnClientClick; }
        set { ImageFelulvizsgalat.OnClientClick = value; }
    }
    #endregion

    #region JegyzekZarolas
    public CustomFunctionImageButton JegyzekZarolas
    {
        get { return ImageJegyzekZarolas; }
        set { ImageJegyzekZarolas = value; }
    }

    public bool JegyzekZarolasEnabled
    {
        get { return ImageJegyzekZarolas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageJegyzekZarolas, value); }
    }
    public bool JegyzekZarolasVisible
    {
        get { return ImageJegyzekZarolas.Visible; }
        set { ImageJegyzekZarolas.Visible = value; }
    }
    public String JegyzekZarolasOnClientClick
    {
        get { return ImageJegyzekZarolas.OnClientClick; }
        set { ImageJegyzekZarolas.OnClientClick = value; }
    }
    #endregion

    #region JegyzekVegrehajtas
    public CustomFunctionImageButton JegyzekVegrehajtas
    {
        get { return ImageJegyzekVegrehajtas; }
        set { ImageJegyzekVegrehajtas = value; }
    }

    public bool JegyzekVegrehajtasEnabled
    {
        get { return ImageJegyzekVegrehajtas.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageJegyzekVegrehajtas, value); }
    }
    public bool JegyzekVegrehajtasVisible
    {
        get { return ImageJegyzekVegrehajtas.Visible; }
        set { ImageJegyzekVegrehajtas.Visible = value; }
    }
    public String JegyzekVegrehajtasOnClientClick
    {
        get { return ImageJegyzekVegrehajtas.OnClientClick; }
        set { ImageJegyzekVegrehajtas.OnClientClick = value; }
    }
    #endregion

    #region JegyzekMegsemmisites
    public CustomFunctionImageButton JegyzekMegsemmisites
    {
        get { return ImageJegyzekMegsemmisites; }
        set { ImageJegyzekMegsemmisites = value; }
    }

    public bool JegyzekMegsemmisitesEnabled
    {
        get { return ImageJegyzekMegsemmisites.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageJegyzekMegsemmisites, value); }
    }
    public bool JegyzekMegsemmisitesVisible
    {
        get { return ImageJegyzekMegsemmisites.Visible; }
        set { ImageJegyzekMegsemmisites.Visible = value; }
    }
    public String JegyzekMegsemmisitesOnClientClick
    {
        get { return ImageJegyzekMegsemmisites.OnClientClick; }
        set { ImageJegyzekMegsemmisites.OnClientClick = value; }
    }
    #endregion

    #region TeljessegEllenorzes
    public CustomFunctionImageButton TeljessegEllenorzes
    {
        get { return ImageTeljessegEllenorzes; }
        set { ImageTeljessegEllenorzes = value; }
    }

    public bool TeljessegEllenorzesEnabled
    {
        get { return ImageTeljessegEllenorzes.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageTeljessegEllenorzes, value); }
    }
    public bool TeljessegEllenorzesVisible
    {
        get { return ImageTeljessegEllenorzes.Visible; }
        set { ImageTeljessegEllenorzes.Visible = value; }
    }
    public String TeljessegEllenorzesOnClientClick
    {
        get { return ImageTeljessegEllenorzes.OnClientClick; }
        set { ImageTeljessegEllenorzes.OnClientClick = value; }
    }
    #endregion

    #region SelejtezesreKijeloles
    public CustomFunctionImageButton SelejtezesreKijeloles
    {
        get { return ImageSelejtezesreKijeloles; }
        set { ImageSelejtezesreKijeloles = value; }
    }

    public bool SelejtezesreKijelolesEnabled
    {
        get { return ImageSelejtezesreKijeloles.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageSelejtezesreKijeloles, value); }
    }
    public bool SelejtezesreKijelolesVisible
    {
        get { return ImageSelejtezesreKijeloles.Visible; }
        set { ImageSelejtezesreKijeloles.Visible = value; }
    }
    public String SelejtezesreKijelolesOnClientClick
    {
        get { return ImageSelejtezesreKijeloles.OnClientClick; }
        set { ImageSelejtezesreKijeloles.OnClientClick = value; }
    }
    #endregion

    #region SelejtezesreKijelolesVisszavonasa
    public CustomFunctionImageButton SelejtezesreKijelolesVisszavonasa
    {
        get { return ImageSelejtezesreKijelolesVisszavonasa; }
        set { ImageSelejtezesreKijelolesVisszavonasa = value; }
    }

    public bool SelejtezesreKijelolesVisszavonasaEnabled
    {
        get { return ImageSelejtezesreKijelolesVisszavonasa.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageSelejtezesreKijelolesVisszavonasa, value); }
    }
    public bool SelejtezesreKijelolesVisszavonasaVisible
    {
        get { return ImageSelejtezesreKijelolesVisszavonasa.Visible; }
        set { ImageSelejtezesreKijelolesVisszavonasa.Visible = value; }
    }
    public String SelejtezesreKijelolesVisszavonasaOnClientClick
    {
        get { return ImageSelejtezesreKijelolesVisszavonasa.OnClientClick; }
        set { ImageSelejtezesreKijelolesVisszavonasa.OnClientClick = value; }
    }
    #endregion

    #region Selejtezes
    public CustomFunctionImageButton Selejtezes
    {
        get { return ImageSelejtezes; }
        set { ImageSelejtezes = value; }
    }

    public bool SelejtezesEnabled
    {
        get { return ImageSelejtezes.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageSelejtezes, value); }
    }
    public bool SelejtezesVisible
    {
        get { return ImageSelejtezes.Visible; }
        set { ImageSelejtezes.Visible = value; }
    }
    public String SelejtezesOnClientClick
    {
        get { return ImageSelejtezes.OnClientClick; }
        set { ImageSelejtezes.OnClientClick = value; }
    }
    #endregion

    #region LomtarbaHelyezes
    public CustomFunctionImageButton LomtarbaHelyezes
    {
        get { return ImageLomtarbaHelyezes; }
        set { ImageLomtarbaHelyezes = value; }
    }

    public bool LomtarbaHelyezesEnabled
    {
        get { return ImageLomtarbaHelyezes.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageLomtarbaHelyezes, value); }
    }
    public bool LomtarbaHelyezesVisible
    {
        get { return ImageLomtarbaHelyezes.Visible; }
        set { ImageLomtarbaHelyezes.Visible = value; }
    }
    public String LomtarbaHelyezesOnClientClick
    {
        get { return ImageLomtarbaHelyezes.OnClientClick; }
        set { ImageLomtarbaHelyezes.OnClientClick = value; }
    }
    #endregion

    #region CsoportHierarchia
    public CustomFunctionImageButton CsoportHierarchia
    {
        get { return ImageButtonCsoportHierarchia; }
        set { ImageButtonCsoportHierarchia = value; }
    }

    public bool CsoportHierarchiaEnabled
    {
        get { return ImageButtonCsoportHierarchia.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageButtonCsoportHierarchia, value); }
    }
    public bool CsoportHierarchiaVisible
    {
        get { return ImageButtonCsoportHierarchia.Visible; }
        set { ImageButtonCsoportHierarchia.Visible = value; }
    }
    public String CsoportHierarchiaOnClientClick
    {
        get { return ImageButtonCsoportHierarchia.OnClientClick; }
        set { ImageButtonCsoportHierarchia.OnClientClick = value; }
    }
    #endregion CsoportHierarchia

    #region Felf�ggeszt�s
    public CustomFunctionImageButton Felfuggesztes
    {
        get { return ImageButtonFelfuggeszt; }
        set { ImageButtonFelfuggeszt = value; }
    }
    public bool FelfuggesztesEnabled
    {
        get { return ImageButtonFelfuggeszt.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageButtonFelfuggeszt, value); }
    }
    public bool FelfuggesztesVisible
    {
        get { return ImageButtonFelfuggeszt.Visible; }
        set { ImageButtonFelfuggeszt.Visible = value; }
    }
    public String FelfuggesztesOnClientClick
    {
        get { return ImageButtonFelfuggeszt.OnClientClick; }
        set { ImageButtonFelfuggeszt.OnClientClick = value; }
    }
    #endregion

    #region ITSZ Modify

    public ImageButton ITSZModify
    {
        get
        {
            return ImageITSZModosit;
        }
    }

    public bool ITSZModifyEnabled
    {
        get { return ImageITSZModosit.Enabled; }
        set { SetRightFunctionButtonEnabledProperty(ImageITSZModosit, value); }
    }

    public bool ITSZModifyVisible
    {
        get { return ImageITSZModosit.Visible; }
        set { ImageITSZModosit.Visible = value; }
    }

    public String ITSZModifyOnClientClick
    {
        get { return ImageITSZModosit.OnClientClick; }
        set { ImageITSZModosit.OnClientClick = value; }
    }
    public string ITSZModifyToolTip
    {
        get { return ImageITSZModosit.ToolTip; }
        set { ImageITSZModosit.ToolTip = value; }
    }
    #endregion

    public void SetRightFunctionButtonsVisible(bool value)
    {
        ImageErkeztetes.Visible = value;
        ImageBejovoIratIktatas.Visible = value;
        ImageBejovoIratIktatasAlszamra.Visible = value;
        ImageSztorno.Visible = value;
        ImageIktatas.Visible = value;
        ImageIktatasMegtagadas.Visible = value;
        ImageBelsoIratIktatas.Visible = value;
        ImageBelsoIratIktatasAlszamra.Visible = value;
        ImageMunkapeldanyIktatasa.Visible = value;
        ImageAtIktatas.Visible = value;
        ImageAtadasra.Visible = value;
        ImageAtadas.Visible = value;
        ImageAtadasListaval.Visible = value;
        ImageAtadasVissza.Visible = value;
        ImageTovabbitasra.Visible = value;
        ImageSzignalas.Visible = value;
        ImageDossziebaHelyez.Visible = value;
        ImageDossziebolKivesz.Visible = value;
        ImageTovabbitasVissza.Visible = value;
        ImageLock.Visible = value;
        ImageUnLock.Visible = value;
        ImageLezaras.Visible = value;
        ImageLezarasVisszavonas.Visible = value;
        ImageElintezetteNyilvanitas.Visible = value;
        ImageElintezetteNyilvanitasVisszavon.Visible = value;
        ImageAtvetel.Visible = value;
        ImageAtvetelListaval.Visible = value;
        ImageAtvetelUgyintezesre.Visible = value;
        ImageVisszakuldes.Visible = value;
        ImageAthelyezes.Visible = value;
        ImageRendezes.Visible = value;
        ImageIrattarozasra.Visible = value;
        #region CR3206 - Iratt�ri strukt�ra
        ImageIrattarbarendezes.Visible = value;
        #endregion
        ImageKiadasOsztalyra.Visible = value;
        ImageUgyiratIrattarAtvetel.Visible = value;
        ImageBoritonyomtatas.Visible = value;
        ImageUgyiratpotlonyomtatas.Visible = value;
        ImageIktatokonyvLezaras.Visible = value;
        ImageBontas.Visible = value;
        ImageFelszabaditas.Visible = value;
        ImageEloadoiIv.Visible = value;
        ImagePeresEloadoiIv.Visible = value;
        ImageSkontrobaHelyez.Visible = value;
        ImageSkontrobolKivetel.Visible = value;
        ImageKolcsonzes.Visible = value;
        ImageKolcsonzesVissza.Visible = value;
        ImageKolcsonzesJovahagyas.Visible = value;
        ImageKolcsonzesVisszautasitas.Visible = value;
        ImageKolcsonzesSztorno.Visible = value;
        ImageKolcsonzesKiadas.Visible = value;
        ImageJegyzekrehelyezes.Visible = value;
        ImageSzereles.Visible = value;
        ImageSzerelesVisszavon.Visible = value;
        ImageCsatolas.Visible = value;

        ImageExpedialas.Visible = value;
        ImagePostazas.Visible = value;
        ImageFelulvizsgalat.Visible = value;
        ImageJegyzekZarolas.Visible = value;
        ImageJegyzekVegrehajtas.Visible = value;
        ImageJegyzekMegsemmisites.Visible = value;
        ImageTeljessegEllenorzes.Visible = value;

        ImageSelejtezesreKijeloles.Visible = value;
        ImageSelejtezesreKijelolesVisszavonasa.Visible = value;
        ImageSelejtezes.Visible = value;
        ImageLomtarbaHelyezes.Visible = value;

        PrintHtml.Visible = value;
        ImageButtonCsoportHierarchia.Visible = value;
        #region CR3128 -Iratp�ld�ny l�trehoz�s nyom�gomb az iratok list�j�ra
        ImageIratpeldanyLetrehozas.Visible = value;
        #endregion
        #region CR3129 - Sz�ks�g lenne egy �j ikonra az al��r�shoz
        ImageIratCsatolmanyAlairas.Visible = value;
        #endregion

        ImageButtonFelfuggeszt.Visible = value;
        ITSZModify.Visible = value;
    }

    #endregion

    public bool Collapsed
    {
        get
        {
            return CollapsiblePanelExtender2.Collapsed;
        }
        set
        {
            CollapsiblePanelExtender2.Collapsed = value;
        }
    }

    public bool DafaultPageLinkVisible
    {
        get
        {
            return tdLinkDefaultPage.Visible;
        }
        set
        {
            tdLinkDefaultPage.Visible = value;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(_SztornoButtonToolTip))
        {
            SztornoButtonToolTip = _SztornoButtonToolTip;
        }

        if (!IsPostBack)
        {
            if (Session[Constants.MasterRowCount] != null)
            {
                RowCountTextBox.Text = this.GetRowCountFromSession();
            }
            else
            {
                RowCountTextBox.Text = Constants.MasterRowCountNumber.ToString();
                Session[Constants.MasterRowCount] = RowCountTextBox.Text;
            }
        }

        // DefaultFilter ikon be�ll�t�sa:
        DefaultFilterOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(DefaultFilterImageButton.ClientID);

        bool isDisabledIconToSetHidden = IsDisabledButtonToSetHidden();
        ImageAdd.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageAtIktatas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageAtadas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageAtadasListaval.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageAtadasra.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageAthelyezes.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageAtvetel.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageAtvetelListaval.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageAtvetelUgyintezesre.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageVisszakuldes.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageBejovoIratIktatas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageBejovoIratIktatasAlszamra.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageBelsoIratIktatas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageBelsoIratIktatasAlszamra.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageBontas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageBoritonyomtatas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageButtonCsoportHierarchia.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageCsatolas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageDefaultFilter.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageDossziebaHelyez.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageDossziebolKivesz.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageElintezetteNyilvanitas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageElintezetteNyilvanitasVisszavon.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageEloadoiIv.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImagePeresEloadoiIv.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageErkeztetes.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageITSZModosit.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageExpedialas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageExport.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        #region CR3155 - CSV export
        ImageCSVExport.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        #endregion
        ImageFTSearch.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageFelszabaditas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageFelulvizsgalat.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageHistory.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageIktatas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageIktatasMegtagadas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageIktatokonyvLezaras.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageInvalidate.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageIrattarozasra.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        #region CR3206 - Iratt�ri strukt�ra
        ImageIrattarbarendezes.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        #endregion
        ImageJegyzekVegrehajtas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageJegyzekMegsemmisites.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageJegyzekZarolas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageJegyzekrehelyezes.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageKiadasOsztalyra.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageKolcsonzes.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageKolcsonzesJovahagyas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageKolcsonzesKiadas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageKolcsonzesSztorno.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageKolcsonzesVissza.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageKolcsonzesVisszautasitas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageLezaras.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageLezarasVisszavonas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageLock.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageModify.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageMunkapeldanyIktatasa.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageNew.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageNumericSearch.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImagePostazas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImagePrint.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSSRSPrint.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageRefresh.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageRevalidate.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSearch.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSelejtezes.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageLomtarbaHelyezes.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSelejtezesreKijeloles.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSelejtezesreKijelolesVisszavonasa.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSendObjects.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSkontrobaHelyez.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSkontrobolKivetel.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSzereles.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSzerelesVisszavon.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSzignalas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageSztorno.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageTeljessegEllenorzes.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageTovabbitasra.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageUgyiratIrattarAtvetel.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageUgyiratpotlonyomtatas.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageUgyiratTerkep.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageUnLock.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        ImageView.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
        PrintHtml.PrintHtmlImage.SetProperties(isDisabledIconToSetHidden, CssClass_item_default);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RaisePreLoad(this, EventArgs.Empty);
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");

        ImageSkontrobaHelyez.ToolTip = "Skontr�ba helyez�s";

        if (Session[Constants.MasterRowCount] == null)
        {
            Session[Constants.MasterRowCount] = RowCount;
        }
        
        bool refreshRowCountTextBox = false;

        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            if (eventArgument == EventArgumentConst.refreshMasterList)
            {
                refreshRowCountTextBox = true;

                // Default filter gomb lekezel�se:
                string eventTarget = Request.Params["__EVENTTARGET"].ToString();
                if (eventTarget == DefaultFilterImageButton.ClientID)
                {
                    // Default keres�si objektum be�ll�t�sa a sessionbe (vagy session ki�r�t�se)
                    SetDefaultFilter();
                }

            }
            else //rendez�s eset�n selectedRecordId t�rl�se
                if (eventArgument.Split('$')[0].ToLower() == "sort")
                {
                    if (Request.Params["__EVENTTARGET"] != null)
                    {
                        string eventTarget = Request.Params["__EVENTTARGET"].ToString();
                        string[] list = eventTarget.Split('$');
                        if (AttachedGridView != null && list[list.Length - 1] == AttachedGridViewId)
                        {
                            ForgetSelectedRecord();
                        }
                    }
                }
        }

        if (!IsPostBack)
        {
            if (Page.Session[Constants.MasterGridViewScrollable] != null)
                ScrollableCheckBox.Checked = (bool)Page.Session[Constants.MasterGridViewScrollable];
        }
        if (!IsPostBack || refreshRowCountTextBox == true)
        {
            if (Session[Constants.MasterRowCount] != null)
            {
                RowCountTextBox.Text = this.GetRowCountFromSession();
            }
        }

        if (AttachedGridView != null)
        {
            AttachedGridView.SelectedIndexChanging += new GridViewSelectEventHandler(AttachedGridView_SelectedIndexChanging);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {        
        // Ha nincs lista felt�lt�s: (pl. �gyirat list�n men�b�l ind�tott iktat�sn�l)
        if (nincsFeltoltes)
        {
            IsFilteredLabel.Text = "(" + Resources.List.UI_NincsFeltoltve + ")";
            SetPagerButtonsEnabled(false);
        }
        else
        {
            SetPagerButtonsEnabled(true);

            if (searchObjectType == null && String.IsNullOrEmpty(customSearchObjectSessionName))
            {
                IsFilteredLabel.Text = "(???)";
            }
            else
            {
                bool isFiltered = false;
                BaseSearchObject searchObject = null;
                if (!String.IsNullOrEmpty(customSearchObjectSessionName))
                {
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, customSearchObjectSessionName))
                    {
                        isFiltered = true;
                        try
                        {
                            searchObject = (BaseSearchObject)Search.GetSearchObject_CustomSessionName(Page, new Object(), customSearchObjectSessionName);
                        }
                        catch (InvalidCastException ex)
                        {
                            Logger.Error("Keres�si objektum kasztol�si hiba: " + ex.ToString());
                        }
                    }
                    else isFiltered = false;
                }
                else
                {
                    if (Search.IsSearchObjectInSession(Page, searchObjectType))
                    {
                        isFiltered = true;
                        try
                        {
                            searchObject = (BaseSearchObject)Search.GetSearchObject_CustomSessionName(Page, new Object(), searchObjectType.Name);
                        }
                        catch (InvalidCastException ex)
                        {
                            Logger.Error("Keres�si objektum kasztol�si hiba: " + ex.ToString());
                        }
                    }
                    else isFiltered = false;
                }

                if (isFiltered)
                {
                    if (searchObject != null && !String.IsNullOrEmpty(searchObject.ReadableWhere))
                    {
                        IsFilteredLabel.Text = "(" + Resources.List.UI_IsFiltered + ")" + GetReadableWhereFormattedStyle(searchObject.ReadableWhere);
                    }
                    else
                    {
                        IsFilteredLabel.Text = "(" + Resources.List.UI_IsFiltered + ")";
                    }
                }
                else
                {
                    IsFilteredLabel.Text = "";
                }
            }
        }

        #region ListSortPopup, ListColumnsPopup

        // Ha nincs AttachedGridView, nem jelen�tj�k meg:
        if (!ListSortPopup1.IsInitialized || enableListSortPopup == false)
        {
            ListSortPopup1.Visible = false;
            ImageSort.Visible = false;
        }
        else if (!IsPostBack || IsReload)
        {
            // Rendez�si felt�telek felt�lt�se a GridView-b�l:
            ListSortPopup1.FillSortExpressionDropDown();
        }

        // Ha nincs AttachedGridView, nem jelen�tj�k meg:
        if (!ListColumnsPopup1.IsInitialized || enableListSortPopup == false)
        {
            ListColumnsPopup1.Visible = false;
            ImageListColumns.Visible = false;
        }
        else if (!IsPostBack || IsReload)
        {
            // GridView oszlopainak bet�lt�se
            ListColumnsPopup1.InitComponent();
        }

        // Ha nincs funkci�joga, elt�ntetj�k az ikont:
        if (!FunctionRights.GetFunkcioJog(Page, funkcio_ListaOszlopLathatosagAllitas))
        {
            ImageListColumns.Visible = false;
            ListColumnsPopup1.Visible = false;
            td_ImageListColumns.Visible = false;
        }

        #endregion

        SetupFunctionKeyComponent();
    }

    private void SetupFunctionKeyComponent()
    {
        FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);

        if (fm != null && !fm.IsSet)
        {
            if (!String.IsNullOrEmpty(SelectedRecordId))
            {
                fm.ObjektumId = SelectedRecordId;
            }
            else
            {
                fm.ObjektumId = String.Empty;
            }
            if (searchObjectType != null)
            {
                string objektumType = String.Empty;
                string searchObjectName = searchObjectType.Name;
                int index = searchObjectName.IndexOf("Search");
                if (index > 0)
                {
                    objektumType = searchObjectName.Substring(0, index);
                }

                fm.ObjektumType = objektumType;
            }
            else
            {
                fm.ObjektumType = String.Empty;
            }
        }
}

    void AttachedGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        #region Sorkijel�l�s megsz�ntet�se funkci�

        if (attachedGridView_RowDeselectFunctionEnabled)
        {
            try
            {
                if (AttachedGridView != null && e.NewSelectedIndex == AttachedGridView.SelectedIndex
                    && e.NewSelectedIndex >= 0)
                {
                    AttachedGridView.SelectedIndex = -1;
                    e.NewSelectedIndex = -1;

                    // �sszes CheckBoxot unchecked-re �ll�tjuk:
                    foreach (GridViewRow row in AttachedGridView.Rows)
                    {
                        CheckBox ch = (CheckBox)row.FindControl("check");
                        if (ch != null)
                        {
                            ch.Checked = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Hiba: ListHeader.AttachedGridView_SelectedIndexChanging", ex);
            }
        }
        #endregion
    }

    private const int maxLength = 70;

    private string GetReadableWhereFormattedStyle(string text)
    {
        text = text.TrimEnd(Search.whereDelimeter.ToCharArray());
        int length = text.Length;
        string cutText = text;

        if (length > maxLength)
        {
            cutText = text.Substring(0, maxLength) + "...";
            string[] darabok = text.Split(new string[1]{Search.whereDelimeter} , StringSplitOptions.RemoveEmptyEntries);
            text = String.Empty;
            for(int i = 0; i<darabok.Length; i++)
            {
                if (i % 2 == 1)
                {
                    darabok[i] = "<span class=\"sp_AlternateText\">" + darabok[i] + "</span>";
                }

                text += darabok[i] + Search.whereDelimeter;
            }
            text = text.TrimEnd(Search.whereDelimeter.ToCharArray());
            spSearchPanel.SearchText = text;
            spSearchPanel.PopupControlExtender.TargetControlID = Header.ID;
            spSearchPanel.PopupControlExtender.OffsetY = 15;

            string jsShowPopup = "showSearchPanel();return false;";
            string jsClearShowPopup = "clearShowSearchPanel();return false";

            return "<br/><span onmouseover=\"" + jsShowPopup + "\" onmouseout=\"" + jsClearShowPopup + "\" class=\"searchLabel\">" + cutText + "</span>";
        }
        else
        {
            return "<br/><span class=\"searchLabel\">" + cutText + "</span>";
        }
    }

    // Default keres�si objektum be�ll�t�sa a sessionbe (vagy session ki�r�t�se)
    private void SetDefaultFilter()
    {
        //Default template-ek list�j�nak t�rl�se
        Session.Remove(Constants.SessionNames.DefaultTemplatesList);
        //Default Template bet�lt�se
        bool IsDefaultTemplate = SetDefaultSearchTemplate(true);
        // SearchObjectType �s CustomSearchObjectSessionName alapj�n el kell d�nteni, hogy milyen list�r�l is van sz�
        if (!IsDefaultTemplate)
        {
            if (!String.IsNullOrEmpty(CustomSearchObjectSessionName))
            {
                // Ki�r�tj�k az adott sessionv�ltoz�t:
                Contentum.eUtility.Search.RemoveSearchObjectFromSession_CustomSessionName(Page, CustomSearchObjectSessionName);
            }
            else if (SearchObjectType != null)
            {
                // Ki�r�tj�k az adott sessionv�ltoz�t:
                Contentum.eUtility.Search.RemoveSearchObjectFromSession(Page, SearchObjectType);
            }
        }
    }

    public event EventHandler PagingButtonsClick;

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //lapoz�s eset�n kiv�laszt�s t�rl�se
        switch ((sender as ImageButton).CommandName)
        {
            case "First":
                if (PageIndex != 0)
                {
                    PageIndex = 0;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "Prev":
                if (PageIndex - 1 >= 0)
                {
                    PageIndex = PageIndex - 1;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "Next":
                if (PageIndex + 1 < PageCount)
                {
                    PageIndex = PageIndex + 1;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "Last":
                if (PageIndex != PageCount - 1)
                {
                    PageIndex = PageCount - 1;
                    ForgetSelectedRecord();
                    goto case "IndexChanged";
                }
                break;
            case "IndexChanged":
                if (PagingButtonsClick != null)
                {
                    PagingButtonsClick(this, new EventArgs());
                }
                break;
        }



    }
    
    public event CommandEventHandler LeftFunkcionButtonsClick;

    public virtual void Left_FunkcionButtons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (LeftFunkcionButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            LeftFunkcionButtonsClick(this, args);
        }
    }

    public event CommandEventHandler RightFunkcionButtonsClick;

    public virtual void Right_FunkcionButtons_OnClick(object sender, ImageClickEventArgs e)
    {
        if (RightFunkcionButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            RightFunkcionButtonsClick(this, args);
        }
    }

    public event EventHandler ScrollableCheckedChanged;

    protected void ScrollableCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        Page.Session[Constants.MasterGridViewScrollable] = (sender as CheckBox).Checked;
    }

    public event EventHandler RowCount_Changed;

    protected void RowCountTextBox_TextChanged(object sender, EventArgs e)
    {
        Session[Constants.MasterRowCount] = RowCount;
        if (RowCount_Changed != null)
        {
            RowCount_Changed(sender, e);
        }
    }

    protected event EventHandler _preLoad;

    public event EventHandler PreLoad
    {
        add
        {
            _preLoad += value;
        }
        remove
        {
            _preLoad -= value;
        }
    }

    protected void RaisePreLoad(object seneder, EventArgs e)
    {
        if (_preLoad != null)
        {
            _preLoad(seneder, e);
        }
    }
}

