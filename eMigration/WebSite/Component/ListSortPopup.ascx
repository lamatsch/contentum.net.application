﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListSortPopup.ascx.cs" Inherits="Component_ListSortPopup" %>

<asp:Panel ID="pnlListSort" runat="server" CssClass="listSortPanel" Style="display: none;">
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
        <%--<div style="padding: 8px">--%>
        <h2 id="header" class="emp_HeaderWrapper" style="cursor: move;">
            <asp:Label ID="labelHeader" runat="server" Text="Rendezés megadása" CssClass="emp_Header"></asp:Label>
        </h2>
        <table>
            <tr>
                <th>
                </th>
                <th>
                    Oszlop
                </th>
                <th>
                    Irány
                </th>
            </tr>
            <tr>
                <td>
                    1.
                </td>
                <td>
                    <asp:DropDownList ID="SortExpression_DropDown_1" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                <asp:DropDownList ID="SortDirection_DropDown_1" runat="server">
                    <asp:ListItem Text="Növekvő" Value="ASC"></asp:ListItem>
                    <asp:ListItem Text="Csökkenő" Value="DESC"></asp:ListItem>
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    2.
                </td>
                <td>
                    <asp:DropDownList ID="SortExpression_DropDown_2" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                <asp:DropDownList ID="SortDirection_DropDown_2" runat="server">
                    <asp:ListItem Text="Növekvő" Value="ASC"></asp:ListItem>
                    <asp:ListItem Text="Csökkenő" Value="DESC"></asp:ListItem>
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    3.
                </td>
                <td>
                    <asp:DropDownList ID="SortExpression_DropDown_3" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                <asp:DropDownList ID="SortDirection_DropDown_3" runat="server">
                    <asp:ListItem Text="Növekvő" Value="ASC"></asp:ListItem>
                    <asp:ListItem Text="Csökkenő" Value="DESC"></asp:ListItem>
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    4.
                </td>
                <td>
                    <asp:DropDownList ID="SortExpression_DropDown_4" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                <asp:DropDownList ID="SortDirection_DropDown_4" runat="server">
                    <asp:ListItem Text="Növekvő" Value="ASC"></asp:ListItem>
                    <asp:ListItem Text="Csökkenő" Value="DESC"></asp:ListItem>
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    5.
                </td>
                <td>
                    <asp:DropDownList ID="SortExpression_DropDown_5" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                <asp:DropDownList ID="SortDirection_DropDown_5" runat="server">
                    <asp:ListItem Text="Növekvő" Value="ASC"></asp:ListItem>
                    <asp:ListItem Text="Csökkenő" Value="DESC"></asp:ListItem>
                </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />
         <%-- BUG_8359--%>
         <a id="lnkDetails" href="javascript:void(0)" style="text-decoration:underline" onclick="return showOrHideDetailsSortPanel()">
                    Mentés/visszaállítás...</a>
                <br /><br />
                <div id="divDetailsSortPanel" style="display: none">
                    <table style="width: 100%">
                        <tr style="text-align: center;">                                                   
                            <td>
                                <asp:Button ID="SaveTemplateButton" runat="server" Text="Rendezés mentése" OnClick="SaveButton_Click" />
                            </td>
                       <%-- </tr>
                        <tr style="text-align: center;">--%>
                            <td>
                            <asp:Button ID="DeleteTemplateButton" runat="server" Text="Eredeti rendezés visszaállítása" OnClick="DeleteButton_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
        
        <br />
        <asp:Button ID="btnSort" runat="server" CausesValidation="false" Text="Rendez" 
                OnClientClick="cancelListSortPopup(); doPostBack_Sort();" OnClick="btnSort_Click" />
         <asp:Button ID="btnContinue" runat="server" CausesValidation="false" Text="Bezár" />
        <%--</div>--%>
    </eUI:eFormPanel>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mdeListSortPopup" runat="server"
    PopupControlID="pnlListSort" OkControlID="btnContinue" BackgroundCssClass="emp_modalBackground" Y="220"
    OnOkScript="hideListSortPopup()" OnCancelScript="hideListSortPopup()" PopupDragHandleControlID="pnlListSort" />