<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LovListHeader.ascx.cs" Inherits="Component_LovListHeader" %>

<%@ Register Src="FormTemplateLoader.ascx" TagName="FormTemplateLoader" TagPrefix="uc1" %>    
<%@ Register Src="CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="HelpLink.ascx" TagName="HelpLink" TagPrefix="help" %>       
    <table style="width: 100%; height: 70px; background-image: url('images/hu/fejlec/kepernyo_fill.jpg'); background-repeat: repeat-x;" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 85px; vertical-align: top;">
            <img id="Img1" runat="server" src="../images/hu/fejlec/kepernyo_ikon.jpg" alt=" " />
            </td>
            <td style="vertical-align: top; text-align: left;
                        font-weight: bold;	font-size: 14px;">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <tr>
                        <td style="height: 46px;text-align: left;vertical-align: middle;" >
                            <asp:Label ID="HeaderLabel" runat="server" CssClass="FormHeaderText"></asp:Label>&nbsp;
                            <asp:Label ID="FilterLabel" runat="server"></asp:Label></td>
                        <td align="right" style="height: 46px; vertical-align: bottom; position: static; text-align: right;">
                         <%-- Spacer--%>
                        </td>
                        <td style="vertical-align: top; text-align:right; background-image: url('images/hu/fejlec/kepernyo_fill.jpg'); background-repeat:repeat-x; width: 128px; height: 46px;">
<%--                            <a href="../Default.aspx" onmouseover="swap('nyitooldal','images/hu/fejlec/nyitooldal2.jpg');"
                                onmouseout="swap('nyitooldal','images/hu/fejlec/nyitooldal.jpg');">
                                <img runat="server" src="../images/hu/fejlec/nyitooldal.jpg" id="nyitooldal" alt=" " style="vertical-align: top;" /></a>
--%>                        </td>
                        <td align="right" valign="middle" style="padding-left:1px">
                            <help:HelpLink ID="linkHelp" runat="server" />
                        </td>                                
                    </tr>
                    <tr>
                        <td style="height: 3px;" colspan="3">
                            <uc1:FormTemplateLoader id="FormTemplateLoader1" runat="server" Visible="false"></uc1:FormTemplateLoader>            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        <td colspan="2">
        <asp:UpdatePanel id="errorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
        <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false" Width="90%">
        </eUI:eErrorPanel>
        </contenttemplate>
        </asp:UpdatePanel>
        <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
        </td>
        </tr>
    </table>