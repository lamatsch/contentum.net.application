using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Component_MenuTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public RequiredFieldValidator Validator
    {
        get { return Validator1; }
    }


    public bool Enabled
    {
        set
        {
            MenuNev.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return MenuNev.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            MenuNev.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return MenuNev.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { MenuNev.Text = value; }
        get { return MenuNev.Text; }
    }

    public TextBox TextBox
    {
        get { return MenuNev; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }

    public ImageButton ImageButton_New
    {
        get { return NewImageButton; }
    }

    public ImageButton ImageButton_View
    {
        get { return ViewImageButton; }
    }

    public ImageButton ImageButton_Reset
    {
        get { return ResetImageButton; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    /// <returns>Az id-nak megfelel� KRT_Menuk objektum</returns>
    public KRT_Menuk SetMenuTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        KRT_Menuk krt_menuk = null;
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_MenukService service = eAdminService.ServiceFactory.GetKRT_MenukService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                krt_menuk = (KRT_Menuk)result.Record;
                Text = krt_menuk.Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);                
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
        }
        return krt_menuk;
    }

    /// <summary>
    /// A megadott komponens �rt�k�nek megfelel�en �ll�tja be az URL 'Filter' param�ter�nek �rt�k�t
    /// </summary>
    /// <param name="ComponentId">A komponens kliensoldali Id-ja</param>
    public void SetLovOnClickToFiltered(String ComponentClientId)
    {
        OnClick_Lov = JavaScripts.SetOnClientClick("MenukLovList.aspx",
            QueryStringVars.Filter + "='+document.getElementById('" + ComponentClientId + "').value+'"
           + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + MenuNev.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
    }

    /// <summary>
    /// Javascripttel tiltja le a gombok l�that�s�g�t
    /// </summary>
    public void SetButtonsToJSNonVisible()
    {
        UI.SetComponentToJSNonVisible(ImageButton_Lov);
        UI.SetComponentToJSNonVisible(ImageButton_New);
        UI.SetComponentToJSNonVisible(ImageButton_View);
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }

    #endregion
       

    protected void Page_Init(object sender, EventArgs e)
    {
        OnClick_Lov = JavaScripts.SetOnClientClick("MenukLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + MenuNev.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("MenukForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + MenuNev.ClientID
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "MenukForm.aspx", "", HiddenField1.ClientID);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        ResetImageButton.Visible = !Validate;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(TextBox);
        componentList.Add(ImageButton_Lov);
        componentList.Add(ImageButton_New);
        componentList.Add(ImageButton_View);

        ImageButton_Lov.OnClientClick = "";
        ImageButton_New.OnClientClick = "";
        ImageButton_View.OnClientClick = "";

        // Le kell tiltani a ClientValidatort
        Validate = false;

        return componentList;
    }

    private bool _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            if (!_ViewEnabled)
            {
                ReadOnly = !_ViewEnabled;
                TextBox.CssClass += " ViewReadOnlyWebControl";
                ImageButton_Lov.CssClass = "ViewReadOnlyWebControl";
                ImageButton_New.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    bool _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                TextBox.CssClass += " ViewDisabledWebControl";
                ImageButton_Lov.CssClass = "ViewDisabledWebControl";
                ImageButton_New.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
