﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eUIControls;

public partial class Component_NyomtatokDropdownList : System.Web.UI.UserControl
{
    private readonly ListItem emptyListItem = new ListItem(Resources.Form.EmptyListItem, "");

    protected void Page_Load(object sender, EventArgs e)
    {
        Nyomtatok_DropDownList.SelectedIndexChanged += new EventHandler(dropDownList1_SelectedIndexChanged);
    }
    public eDropDownList DropDownList
    {
        get { return Nyomtatok_DropDownList; }
    }
    /// <summary>
    /// Beállítja a már feltöltött lista értékét a megadottra, ha az létezik. 
    /// Ha nem létezik, új elemként felvéve, és megjelölve, hogy ez egy nem létező kódtárérték
    /// </summary>
    /// <param name="selectedValue"></param>
    public void SetSelectedValue(String selectedValue)
    {
        Nyomtatok_DropDownList.SelectedValue = selectedValue;
    }
    public String SelectedValue
    {
        get { return Nyomtatok_DropDownList.SelectedValue; }
        set { SetSelectedValue(value); }
    }

    public bool Enabled
    {
        get { return Nyomtatok_DropDownList.Enabled; }
        set { Nyomtatok_DropDownList.Enabled = value; }
    }

    public Unit Width
    {
        get { return Nyomtatok_DropDownList.Width; }
        set { Nyomtatok_DropDownList.Width = value; }
    }

    public string CssClass
    {
        get { return Nyomtatok_DropDownList.CssClass; }
        set { Nyomtatok_DropDownList.CssClass = value; }
    }

    public string ValidationGroup
    {
        set
        {
            Nyomtatok_DropDownList.ValidationGroup = value;
        }
        get { return Nyomtatok_DropDownList.ValidationGroup; }
    }

    /// <summary>
    /// Method executed with an item from the dropdownlist is selected.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelectedValue = Nyomtatok_DropDownList.SelectedItem.Value;
    }

    /// <summary>
    /// Feltölti a DropDownList-et a megadott felhasználóhozhoz tartozó (érvényes) nyomtatókkal
    /// </summary>
    /// <param name="errorPanel"></param>
    public void FillDropDownList(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        Nyomtatok_DropDownList.Items.Clear();

        if (Res.Ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < Res.Ds.Tables[0].Rows.Count; i++)
            {
                Nyomtatok_DropDownList.Items.Add(new ListItem(Res.Ds.Tables[0].Rows[i][DisplayColumnName].ToString(), Res.Ds.Tables[0].Rows[i][IdColumnName].ToString()));
            }

            if (addEmptyItem)
            {
                Nyomtatok_DropDownList.Items.Insert(0, emptyListItem);
            }
        }
    }

    public void FillDropDownList(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList(Res, IdColumnName, DisplayColumnName, false, errorPanel);
    }

}