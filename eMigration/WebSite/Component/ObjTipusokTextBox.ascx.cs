using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Component_ObjTipusokTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{

    #region public properties
    
    private string _Filter;    
    
    public string Filter
    {
        get { return _Filter; }
        set { _Filter = value; }
    }

    private bool _UgyiratHierarchia = false;

    public bool UgyiratHierarchia
    {
        get { return _UgyiratHierarchia; }
        set { _UgyiratHierarchia = value; }
    }

    private bool _RefreshCallingWindow = true;

    public bool RefreshCallingWindow
    {
        set { _RefreshCallingWindow = value; }
        get { return _RefreshCallingWindow; }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool Enabled
    {
        set
        {
            ObjTipusokMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return ObjTipusokMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            ObjTipusokMegnevezes.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return ObjTipusokMegnevezes.ReadOnly; }
    }

    public String CssClass
    {
        set { ObjTipusokMegnevezes.CssClass = value; }
        get { return ObjTipusokMegnevezes.CssClass; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { ObjTipusokMegnevezes.Text = value; }
        get { return ObjTipusokMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return ObjTipusokMegnevezes; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public string Obj_Id_Szulo
    {
        //set { HiddenField_Obj_Id_Szulo.Value = value; }
        get { return HiddenField_Obj_Id_Szulo.Value; }
    }

    public string KodCsoport
    {
        //set { HiddenField_KodCsoport.Value = value; }
        get { return HiddenField_KodCsoport.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                ObjTipusokMegnevezes.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                ObjTipusokMegnevezes.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }


    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }

    public ImageButton ImageButton_New
    {
        get { return NewImageButton; }
    }

    public ImageButton ImageButton_View
    {
        get { return ViewImageButton; }
    }

    public ImageButton ImageButton_Reset
    {
        get { return ResetImageButton; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public KRT_ObjTipusok SetObjTipusokTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        KRT_ObjTipusok krt_ObjTipusok = null;
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            KRT_ObjTipusokService service = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                krt_ObjTipusok = (KRT_ObjTipusok)result.Record;
                //Text = krt_ObjTipusok.Kod;
                //if (!String.IsNullOrEmpty(krt_ObjTipusok.Nev))
                //{
                //    Text += " (" + krt_ObjTipusok.Nev + ")";
                //}
                Text = krt_ObjTipusok.Nev;
 
                HiddenField_Obj_Id_Szulo.Value = krt_ObjTipusok.Obj_Id_Szulo;
                HiddenField_KodCsoport.Value = krt_ObjTipusok.KodCsoport_Id;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
        }

        return krt_ObjTipusok; // hiba eset�n �rt�ke null
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {        

        

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //String asdf = ObjTipusokMegnevezes.Text;

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode;

        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string query = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + 
            "&" + QueryStringVars.TextBoxId + "=" + ObjTipusokMegnevezes.ClientID
            // TODO: QueryStrinVars
            + "&" + "ObjIdSzuloHiddenField" + "=" + HiddenField_Obj_Id_Szulo.ClientID
            + "&" + "KodcsoportHiddenField" + "=" + HiddenField_KodCsoport.ClientID
            + (UgyiratHierarchia == true ? "&" + QueryStringVars.Mode + "=" + Constants.UgyiratHierarchia : "")
            + "&" + QueryStringVars.RefreshCallingWindow + "=" + (RefreshCallingWindow ? "1" : "0");
            
        if (!String.IsNullOrEmpty(Filter))
        {
            query += "&" + QueryStringVars.Filter + "=" + Filter;
            NewImageButton.Visible = false;
        }

        OnClick_Lov = JavaScripts.SetOnClientClick("ObjTipusokLovList.aspx",query, Defaults.PopupWidth, Defaults.PopupHeight, TextBox.ClientID, "", false);

        OnClick_New = JavaScripts.SetOnClientClick("ObjTipusokForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + ObjTipusokMegnevezes.ClientID
            // TODO: QueryStrinVars
            + "&" + "ObjIdSzuloHiddenField" + "=" + HiddenField_Obj_Id_Szulo.ClientID
            + "&" + "KodcsoportHiddenField" + "=" + HiddenField_KodCsoport.ClientID
            + "&" + QueryStringVars.RefreshCallingWindow + "=" + (RefreshCallingWindow ? "1" : "0")
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "ObjTipusokForm.aspx", "", HiddenField1.ClientID);


        OnClick_Reset = "var tbox = $get('" + TextBox.ClientID + "');$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';$get('" +
            HiddenField_Obj_Id_Szulo.ClientID + "').value = '';$get('" +
            HiddenField_KodCsoport.ClientID + "').value = '';" +
            "$common.tryFireEvent(tbox, 'change');" +
            "return false;";

    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(ObjTipusokMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
