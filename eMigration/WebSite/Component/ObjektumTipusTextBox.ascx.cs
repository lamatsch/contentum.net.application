using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class Component_ObjektumTipusTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool Enabled
    {
        set
        {
            ObjektumTipusNev.Enabled = value;
            LovImageButton.Enabled = value;            
            ResetImageButton.Enabled = value;
            if (value == false) 
            {
                LovImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return ObjektumTipusNev.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        {
            ObjektumTipusNev.ReadOnly = value;
            LovImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return ObjektumTipusNev.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { ObjektumTipusNev.Text = value; }
        get { return ObjektumTipusNev.Text; }
    }

    public TextBox TextBox
    {
        get { return ObjektumTipusNev; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;            
            if (!_ViewEnabled)
            {
                ReadOnly = !_ViewEnabled;
                ObjektumTipusNev.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;           
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                ObjektumTipusNev.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetFunkcioTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Funkciok krt_funkciok = (KRT_Funkciok)result.Record;
                Text = krt_funkciok.Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //OnClick = JavaScripts.SetOnClientClick("FunkciokLovList.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, FunkcioNev.ClientID);

        OnClick_Lov = JavaScripts.SetOnClientClick("ObjektumTipusokLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + ObjektumTipusNev.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        ResetImageButton.Visible = !Validate;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(ObjektumTipusNev);
        componentList.Add(LovImageButton);
        componentList.Add(ResetImageButton);

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        LovImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";
        
        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
