<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrszagTextBox.ascx.cs" Inherits="Component_OrszagTextBox" %>
<%@ Register Src="RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc1" %>
<div class="DisableWrap">
<asp:TextBox ID="OrszagNev_TextBox" runat="server" CssClass="mrUrlapInput"
    Enabled="true"></asp:TextBox>
<asp:ImageButton TabIndex = "-1" ID="OrszagImageButton" runat="server" ImageUrl="~/images/hu/lov/cimzett_cime2.jpg" CssClass="mrUrlapInputImageButton" AlternateText="Kiv�laszt" /><asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:RequiredFieldValidator
    ID="RequiredFieldValidator1" runat="server" ControlToValidate="OrszagNev_TextBox" SetFocusOnError="true"
    Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
    TargetControlID="RequiredFieldValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
<ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="false" MinimumPrefixLength="2" 
TargetControlID="OrszagNev_TextBox" CompletionSetCount="20" ContextKey="" UseContextKey="true" FirstRowSelected="true" 
 ServicePath="http://localhost:100/eAdminWebService/KRT_OrszagokService.asmx" ServiceMethod="GetOrszagokList"
></ajaxToolkit:AutoCompleteExtender>
<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="OrszagNev_TextBox" runat="server"
WatermarkText="Orsz�g" WatermarkCssClass="watermarked" Enabled="false"></ajaxtoolkit:TextBoxWatermarkExtender>
</div>