using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class Component_OrszagTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    public bool WatermarkEnabled
    {
        get { return TextBoxWatermarkExtender1.Enabled; }
        set { TextBoxWatermarkExtender1.Enabled = value; }
    }

    public bool ImageVisible
    {
        get { return OrszagImageButton.Visible; }
        set { OrszagImageButton.Visible = value; }
    }

    public string Text
    {
        set { OrszagNev_TextBox.Text = value; }
        get { return OrszagNev_TextBox.Text; }
    }

    public bool Validate
    {
        set
        {
            RequiredFieldValidator1.Enabled = value;
            RequiredFieldValidator1.Visible = value;
        }
        get { return RequiredFieldValidator1.Enabled; }
    }

    public string OnClick
    {
        set { OrszagImageButton.OnClientClick = value; }
        get { return OrszagImageButton.OnClientClick; }
    }

    public TextBox TextBox
    {
        get { return OrszagNev_TextBox; }
    }

    public bool Enabled
    {
        set
        {
            OrszagNev_TextBox.Enabled = value;
            OrszagImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (!value)
            {
                OrszagImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                OrszagImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return OrszagNev_TextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            OrszagNev_TextBox.ReadOnly = value;
            OrszagImageButton.Enabled = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (value)
            {
                OrszagImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                OrszagImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return OrszagNev_TextBox.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public bool AutoCompleteEnabled
    {
        set { AutoCompleteExtender1.Enabled = value; }
        get { return AutoCompleteExtender1.Enabled; }
    }

    public string CssClass
    {
        get { return OrszagNev_TextBox.CssClass; }
        set 
        { 
            OrszagNev_TextBox.CssClass = value;
            TextBoxWatermarkExtender1.WatermarkCssClass = value + " " + TextBoxWatermarkExtender1.WatermarkCssClass;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                OrszagNev_TextBox.CssClass += " ViewReadOnlyWebControl";
                OrszagImageButton.CssClass += " ViewReadOnlyWebControl";                                
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                OrszagNev_TextBox.CssClass += " ViewDisabledWebControl";
                OrszagImageButton.CssClass += " ViewDisabledWebControl";
            }
        }
    }

    public void SetDefault()
    {
        Text = Resources.Form.DefaultCountry;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //OnClick = JavaScripts.SetOnClientClick("OrszagokLovList.aspx","", Defaults.PopupWidth, Defaults.PopupHeight,OrszagImageButton.ClientID);

        //AutoCompleteExtender1.ServicePath = UI.GetAppSetting("eAdminBusinessServiceUrl") + "KRT_OrszagokService.asmx";
        AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";
        

        String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        AutoCompleteExtender1.ContextKey = felh_Id + ";";

        OnClick = JavaScripts.SetOnClientClick("OrszagokLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + OrszagNev_TextBox.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(OrszagNev_TextBox);
        componentList.Add(OrszagImageButton);

        OrszagImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        RequiredFieldValidator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
