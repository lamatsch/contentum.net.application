﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eAdmin.Utility;

public partial class Component_PasswordControl : System.Web.UI.UserControl
{
    public bool IsValid
    {
        get
        {
            return CompareValidatorPassword.IsValid;
        }
    }

    public string Password
    {
        get
        {
            return requiredTextBoxJelszo.Text;
        }
    }

    public bool DisplayErrorPanel(Contentum.eUIControls.eErrorPanel errorPanel, bool PasswordRequired)
    {
        if (!PasswordRequired)
            return false;

        if (!IsValid)
        {
            ResultError.DisplayErrorOnErrorPanel(errorPanel, "Jelszó megadási hiba", "Nem egyezik a két jelszó!");
        }

        return !IsValid;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        string js = "function " + this.ClientID + @"_Display(value){
                     if(value)
                     {
                         $('#" + trPasswordJelszo.ClientID + ",#" + trPasswordJelszoMegerosites.ClientID + @"').show();
                     }
                     else
                     {
                        $('#" + trPasswordJelszo.ClientID + ",#" + trPasswordJelszoMegerosites.ClientID + @"').hide();
                     }
                     var req1 = $get('" + requiredTextBoxJelszo.Validator.ClientID + @"');
                     var req2 = $get('" + requiredTextBoxJelszoMegerositese.Validator.ClientID + @"');
                     if(req1 && req2) req1.enabled = req2.enabled = value;
                     }";

        ScriptManager.RegisterStartupScript(this, this.GetType(), js,js, true);
    }
}
