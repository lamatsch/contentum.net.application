using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eUtility;
using Contentum.eUIControls;

public partial class Component_PrintHtml : System.Web.UI.UserControl
{
    #region PrintHtml
    public CustomFunctionImageButton PrintHtmlImage
    {
        get { return ImagePrintHtml; }
        set { ImagePrintHtml = value; }
    }

    public bool Enabled
    {
        get { return ImagePrintHtml.Enabled; }
        set
        {
            ImagePrintHtml.Enabled = value;
            //if (value == false)
            //{
            //    ImagePrintHtml.CssClass = "disableditem";
            //}
        }
    }

    public bool Visible
    {
        get { return ImagePrintHtml.Visible; }
        set { ImagePrintHtml.Visible = value; }
    }

    public String OnClientClick
    {
        get { return ImagePrintHtml.OnClientClick; }
        set { ImagePrintHtml.OnClientClick = value; }
    }

    public String ImageUrl
    {
        get { return ImagePrintHtml.ImageUrl; }
        set { ImagePrintHtml.ImageUrl = value; }
    }

    public String CssClass
    {
        get { return ImagePrintHtml.CssClass; }
        set { ImagePrintHtml.CssClass = value; }
    }

    public String onmouseover
    {
        get { return ImagePrintHtml.Attributes["onmouseover"]; }
        set
        {
            if (ImagePrintHtml.Attributes["onmouseover"] != null)
            {
                ImagePrintHtml.Attributes["onmouseover"] = value;
            }
            else
            {
                ImagePrintHtml.Attributes.Add("onmouseover", value);
            }
        }
    }
    
    public String onmouseout
    {
        get { return ImagePrintHtml.Attributes["onmouseout"]; }
        set
        {
            if (ImagePrintHtml.Attributes["onmouseout"] != null)
            {
                ImagePrintHtml.Attributes["onmouseout"] = value;
            }
            else
            {
                ImagePrintHtml.Attributes.Add("onmouseout", value);
            }
        }
    }

    public String AlternateText
    {
        get { return ImagePrintHtml.AlternateText; }
        set { ImagePrintHtml.AlternateText = value; }
    }


    public String ToolTip
    {
        get { return ImagePrintHtml.ToolTip; }
        set { ImagePrintHtml.ToolTip = value; }
    }

    #endregion PrintHtml

    public static void RegisterPrintHtmlScript(Page page)
    {
        ScriptManager sm = ScriptManager.GetCurrent(page);
        if (sm == null)
        {
            throw new Exception("ScriptManager not found.");
        }
        ScriptReference sr = new ScriptReference();
        sr.Path = "~/JavaScripts/PrinterFriendlyVersion.js";
        if (!sm.Scripts.Contains(sr))
            sm.Scripts.Add(sr);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RegisterPrintHtmlScript(Page);
            string cssPath = Request.ApplicationPath + "/App_Themes/" + Page.Theme + "/StyleSheet.css";
            //JavaScripts.registerOuterScriptFile(Page, "printHtmlScript", "Javascripts/PrinterFriendlyVersion.js");

            string imgPath = Request.ApplicationPath + "/images/hu/trapezgomb/";
            string imgNamePrint = "nyomtatas_trap.jpg";
            string imgNameClose = "megsem_trap.jpg";
            string cssClassImg = "highlightit";

            string userName = UI.RemoveEmailAddressFromCsoportNev(CsoportNevek_Cache.GetCsoportNevFromCache(FelhasznaloProfil.FelhasznaloId(Page), Page));
            userName += " - "  + CsoportNevek_Cache.GetCsoportNevFromCache(FelhasznaloProfil.FelhasznaloSzerverzetId(Page), Page);
            if (FelhasznaloProfil.FelhasznaloId(Page) != FelhasznaloProfil.LoginUserId(Page))
            {
                string loginUserName = UI.RemoveEmailAddressFromCsoportNev(CsoportNevek_Cache.GetCsoportNevFromCache(FelhasznaloProfil.LoginUserId(Page), Page));
                userName += " (" + loginUserName + ")";
            }

            ImagePrintHtml.OnClientClick = "createPrinterFriendlyVersion('" + userName + "','" + cssPath + "','" + imgPath + imgNamePrint + "','" + imgPath + imgNameClose + "','" + cssClassImg + "');return false;";

        }

    }
}
