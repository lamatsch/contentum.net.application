<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequiredNumberBox.ascx.cs" Inherits="Component_RequiredNumberBox" %>
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>

<asp:TextBox ID="TextBox1" runat="server" CssClass="mrUrlapInputNumber" Enabled="true"></asp:TextBox><asp:RequiredFieldValidator
    ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" Display="None" SetFocusOnError="true"
    ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator><ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
        <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());" />
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
    FilterType="Numbers"  
    TargetControlID="TextBox1">
</ajaxToolkit:FilteredTextBoxExtender>
<asp:RegularExpressionValidator ID="RegExpNumberValidator" runat="server" Enabled="false"
    ControlToValidate="TextBox1"  
    EnableClientScript="true"
    ValidationExpression="^-?[0-9]+(.[0-9]+)?$"
    Display="None"
    SetFocusOnError="true"
    ErrorMessage="<%$Resources:Form,RegularExpressionValidationMessage_Number%>" />
<ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RegExpNumberValidator">
        <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());" />
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
