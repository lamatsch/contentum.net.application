<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchFooter.ascx.cs" Inherits="Component_SearchFooter" %>
<%@ Register Src="FormTemplateLoader.ascx" TagName="FormTemplateLoader" TagPrefix="uc1" %>
        <table style="width: 60%;">
        <tr>
        <%--<td style="text-align: center;">
            <uc1:FormTemplateLoader id="FormTemplateLoader1" runat="server" Visible="true"></uc1:FormTemplateLoader>            
        </td>--%>
        <td style="text-align: center;">
            <asp:ImageButton ID="ImageSearch" TabIndex="1" runat="server" ImageUrl="~/images/hu/ovalgomb/kereses.jpg" 
                onmouseover="swapByName(this.id,'kereses2.jpg')" onmouseout="swapByName(this.id,'kereses.jpg')" 
                OnClick="ImageButton_Click" CommandName="Search" /></td>
        <td style="text-align: center;">
            <asp:ImageButton ID="ImageDefault" TabIndex="2" runat="server" ImageUrl="~/images/hu/ovalgomb/alapallapot.jpg" 
            onmouseover="swapByName(this.id,'alapallapot2.jpg')" onmouseout="swapByName(this.id,'alapallapot.jpg')" 
            OnClick="ImageButton_Click" CommandName="Default" /></td>
        <td style="text-align: center;">
            <asp:ImageButton ID="ImageCancel" TabIndex="3" runat="server" ImageUrl="~/images/hu/ovalgomb/megsem.jpg" 
            onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')" 
            OnClientClick="window.close(); return false;" CommandName="Cancel" OnClick="ImageButton_Click" /></td>
        <td style="text-align: center;">
            <asp:ImageButton ID="ImageFullTextSearch" TabIndex="4" runat="server" ImageUrl="~/images/hu/ovalgomb/fulltextsearch.gif" 
            onmouseover="swapByName(this.id,'fulltextsearch2.gif')" onmouseout="swapByName(this.id,'fulltextsearch.gif')" 
            CommandName="FullTextSearch" OnClick="ImageButton_Click" Visible="False" /></td>
        </tr>
        </table>
