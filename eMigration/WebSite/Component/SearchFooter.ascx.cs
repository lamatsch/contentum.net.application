using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_SearchFooter : System.Web.UI.UserControl
{
    public bool SearchEnabled
    {
        get { return ImageSearch.Enabled; }
        set { ImageSearch.Enabled = value; }
    }

    public ImageButton ImageButton_Search
    {
        get
        {
            return ImageSearch;
        }
    }

    public ImageButton ImageButton_Default
    {
        get
        {
            return ImageDefault;
        }
    }

    public ImageButton ImageButton_Cancel
    {
        get
        {
            return ImageCancel;
        }
    }

    public ImageButton ImageButton_FullTextSearch
    {
        get
        {
            return ImageFullTextSearch;
        }
    }

    public bool FullTextSearchVisible
    {
        get { return ImageFullTextSearch.Visible; }
        set { ImageFullTextSearch.Visible = value; }
    }

    public String FullTextSearchOnClientClick
    {
        get { return ImageFullTextSearch.OnClientClick; }
        set { ImageFullTextSearch.OnClientClick = value; }
    }

    //public object SearchObject
    //{
    //    get { return FormTemplateLoader1.SearchObject; }
    //    set { FormTemplateLoader1.SearchObject = value; }
    //}

    //public Type SearchObjectType
    //{
    //    get { return FormTemplateLoader1.SearchObjectType; }
    //    set { FormTemplateLoader1.SearchObjectType = value; }
    //}

    //public string CurrentTemplateName
    //{
    //    get { return FormTemplateLoader1.CurrentTemplateName; }
    //    set { FormTemplateLoader1.CurrentTemplateName = value; }
    //}

    //public string CurrentTemplateId
    //{
    //    get { return FormTemplateLoader1.CurrentTemplateId; }
    //    set { FormTemplateLoader1.CurrentTemplateId = value; }
    //}

    ///// <summary>
    ///// �j template l�trehoz�sa
    ///// </summary>
    ///// <param name="newSearchObject"></param>
    //public void NewTemplate(object newSearchObject)
    //{
    //    FormTemplateLoader1.NewTemplate(newSearchObject);
    //}

    ///// <summary>
    ///// Aktu�lis template elment�se
    ///// </summary>
    ///// <param name="newSearchObject"></param>
    //public void SaveCurrentTemplate(object newSearchObject)
    //{
    //    FormTemplateLoader1.SaveCurrentTemplate(newSearchObject);
    //}

    //public void TemplateReset()
    //{
    //    FormTemplateLoader1.TemplateReset();
    //}

    //public void RegisterErrorPanelToFormTemplateLoader(Contentum.eUIControls.eErrorPanel errorPanel)
    //{
    //    FormTemplateLoader1.ErrorPanel = errorPanel;
    //}


    protected void Page_Init(object sender, EventArgs e)
    {
        //FormTemplateLoader1.ButtonsClick += new CommandEventHandler(FormTemplateLoader1_ButtonsClick);
    }
    
    
    protected void Page_Load(object sender, EventArgs e)
    {
        // CR3156: Enter lenyom�s�ra a Tal�lati lista ment�se k�perny� j�tt be a keres� form-on. Ezut�n a Keres�s gomb lesz az alap�rtelmezett
        if (!IsPostBack)
        {
            Page.Form.DefaultButton = ImageSearch.UniqueID;
        }
        //UI.SwapImageToDisabled(ImageSearch);
    }

    public event CommandEventHandler ButtonsClick;

    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        if (ButtonsClick != null)
        {
            string commandName = "";
            if (sender.Equals(ImageDefault))
            {
                commandName = CommandName.Default;
            }
            else
            {
                commandName = (sender as ImageButton).CommandName;
            }

            CommandEventArgs args =
                new CommandEventArgs(commandName, "");
            ButtonsClick(this, args);
        }
    }

    //protected void FormTemplateLoader1_ButtonsClick(object sender, CommandEventArgs e)
    //{
    //    if (ButtonsClick != null)
    //    {
    //        //string commandName = e.CommandName;

    //        //CommandEventArgs args = e;
    //        //if (e.CommandName == CommandName.LoadTemplate)
    //        //{
    //        //    object SearchFormObject = e.CommandArgument;
    //        //    args = new CommandEventArgs(commandName, SearchFormObject);
    //        //}
    //        //else
    //        //{
    //        //    args = new CommandEventArgs(commandName, "");
    //        //}
    //        ButtonsClick(this, e);
    //    }        
    //}

    public void SetDefaultButton()
    {
        Page.Form.DefaultButton = ImageSearch.UniqueID;
    }
}
