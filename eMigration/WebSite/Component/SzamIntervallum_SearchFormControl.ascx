<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SzamIntervallum_SearchFormControl.ascx.cs" Inherits="Component_SzamIntervallum_SearchFormControl" %>

<div class="DisableWrap">
    <asp:TextBox ID="SzamTolTextBox" runat="server" CssClass="mrUrlapInputNumber"></asp:TextBox>
    <asp:ImageButton TabIndex = "-1" ID="SzamTol_masol_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/right-arrow.jpg"
        AlternateText="M�sol" />
    <asp:TextBox ID="SzamIgTextBox" runat="server" CssClass="mrUrlapInputNumber"></asp:TextBox>
 <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
    FilterType="Numbers"  
    TargetControlID="SzamTolTextBox">
</ajaxToolkit:FilteredTextBoxExtender>
<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
    FilterType="Numbers" 
    TargetControlID="SzamIgTextBox">
</ajaxToolkit:FilteredTextBoxExtender>