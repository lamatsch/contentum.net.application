<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TalalatokSzama_SearchFormComponent.ascx.cs" Inherits="Component_TalalatokSzama_SearchFormComponent" %>

<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<table cellspacing="0" cellpadding="0" width="100%">
    <tbody>
        <tr class="urlapSor">
            <td class="mrUrlapTalalatokSzamaCella">
                <asp:Label ID="Label6" runat="server" Text="<%$Resources:Search,TalalatokSzamaLabelText %>"></asp:Label>
                &nbsp;
                <uc4:requirednumberbox id="TalalatokSzama_RequiredNumberBox" runat="server" validate="false" />
            </td>
        </tr>
    </tbody>
</table>
