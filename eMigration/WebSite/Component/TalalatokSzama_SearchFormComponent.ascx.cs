using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_TalalatokSzama_SearchFormComponent : System.Web.UI.UserControl
{
    private const int talalatokSzamaDefault = 20;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDefault();
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
     
    }

    
    public bool Enabled
    {
        get { return TalalatokSzama_RequiredNumberBox.Enabled; }
        set { TalalatokSzama_RequiredNumberBox.Enabled = value; }
    }
    

    public int TalalatokSzama
    {
        get 
        {
            try
            {
                return int.Parse(TalalatokSzama_RequiredNumberBox.Text);
            }
            catch (System.FormatException)
            {
                return talalatokSzamaDefault;
            }
        }
        set 
        {
            TalalatokSzama_RequiredNumberBox.Text = value.ToString(); 
        }
    }

    public void SetDefault()
    {
        if (Session[Constants.MasterRowCount] != null)
        {
            TalalatokSzama_RequiredNumberBox.Text = Session[Constants.MasterRowCount].ToString();
        }
        else
        {
            TalalatokSzama_RequiredNumberBox.Text = talalatokSzamaDefault.ToString();
        }
    }

    public void SetMasterRowCountToSession()
    {
        try
        {
            int i = Int32.Parse(TalalatokSzama_RequiredNumberBox.Text);
            if (i > 0)
            {
                Session[Constants.MasterRowCount] = TalalatokSzama_RequiredNumberBox.Text;
            }
        }
        catch
        { 
            // do nothing
        }
    }

	
}
