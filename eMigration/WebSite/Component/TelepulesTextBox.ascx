<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TelepulesTextBox.ascx.cs" Inherits="Component_TelepulesTextBox" %>
<%@ Register Src="RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc1" %>
<div class="DisableWrap">
<asp:TextBox ID="TelepulesNev_TextBox" runat="server" CssClass="mrUrlapInput"
    Enabled="true"></asp:TextBox>
<asp:ImageButton TabIndex = "-1" ID="TelepulesImageButton" runat="server" ImageUrl="~/images/hu/lov/cimzett_cime2.jpg" CssClass="mrUrlapInputImageButton" AlternateText="Kiválaszt" /><asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:RequiredFieldValidator
    ID="RequiredFieldValidator1" runat="server" ControlToValidate="TelepulesNev_TextBox" SetFocusOnError="true"
    Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
    TargetControlID="RequiredFieldValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
<ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="false" MinimumPrefixLength="2" 
TargetControlID="TelepulesNev_TextBox" CompletionSetCount="20" ContextKey="" UseContextKey="true" FirstRowSelected="true" 
 ServicePath="http://localhost:100/eAdminWebService/KRT_TelepulesekService.asmx" ServiceMethod="GetTelepulesekList" CompletionInterval="2000"
></ajaxToolkit:AutoCompleteExtender>
<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="TelepulesNev_TextBox" runat="server"
WatermarkText="Település" WatermarkCssClass="watermarked" Enabled="false"></ajaxtoolkit:TextBoxWatermarkExtender>
</div>