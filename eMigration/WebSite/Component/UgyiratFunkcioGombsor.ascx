<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UgyiratFunkcioGombsor.ascx.cs"
    Inherits="Component_FunkcioGombsor" %>

<asp:UpdatePanel ID="_FunkcioGombsorUpdatePanel" runat="server">
    <ContentTemplate>
        <asp:Panel ID="_FunkcioGombsorPanel" runat="server">
                <asp:ImageButton ID="Uj_ugyirat" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/ugyirat_letrehozasa.jpg"
                    OnClick="ImageButton_Click" CommandName="Uj_ugyirat" AlternateText="<%$Resources:Buttons,Uj_ugyirat %>" />
                <asp:ImageButton ID="Modositas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/modositas.jpg"
                    OnClick="ImageButton_Click" CommandName="Modositas" AlternateText="<%$Resources:Buttons,Modositas %>" />
                <asp:ImageButton ID="Szignalas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/szignalas.jpg"
                    OnClick="ImageButton_Click" CommandName="Szignalas" AlternateText="<%$Resources:Buttons,Szignalas %>" />
                <asp:ImageButton ID="Szignalas_vissza" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/szignalas_vissza.jpg"
                    OnClick="ImageButton_Click" CommandName="Szignalas_vissza" AlternateText="<%$Resources:Buttons,Szignalas_vissza %>" />
                <asp:ImageButton ID="Szkontro_inditas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/szkontro_inditas.jpg"
                    OnClick="ImageButton_Click" CommandName="Szkontro_inditas" AlternateText="<%$Resources:Buttons,Szkontro_inditas %>" />
                <asp:ImageButton ID="Szkontrobol_vissza" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/szkontrobol_visszavetel.jpg"
                    OnClick="ImageButton_Click" CommandName="Szkontrobol_vissza" AlternateText="<%$Resources:Buttons,Szkontrobol_vissza %>" />
                <asp:ImageButton ID="Felfuggesztes" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/felfuggesztes.jpg"
                    OnClick="ImageButton_Click" CommandName="Felfuggesztes" AlternateText="<%$Resources:Buttons,Felfuggesztes %>" />
                <asp:ImageButton ID="Felfuggesztesbol_vissza" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/felfuggesztesbol_visszavetel.jpg" OnClick="ImageButton_Click"
                    CommandName="Felfuggesztesbol_vissza" AlternateText="<%$Resources:Buttons,Felfuggesztesbol_vissza %>" />
                <asp:ImageButton ID="Tovabbitas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/tovabbitas.jpg"
                    OnClick="ImageButton_Click" CommandName="Tovabbitas" AlternateText="<%$Resources:Buttons,Tovabbitas %>" />
                <asp:ImageButton ID="Sztornozas" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/sztornozas.jpg"
                    OnClick="ImageButton_Click" CommandName="Sztornozas" AlternateText="<%$Resources:Buttons,Sztornozas %>" />
                <asp:ImageButton ID="Ugyintezes_lezarasa" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/ugyintezes_lezarasa.jpg"
                    OnClick="ImageButton_Click" CommandName="Ugyintezes_lezarasa" AlternateText="<%$Resources:Buttons,Ugyintezes_lezarasa %>" />
                <asp:ImageButton ID="Ugyintezes_lezarasa_vissza" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/ugyintezes_lezarasa_vissza.jpg" OnClick="ImageButton_Click"
                    CommandName="Ugyintezes_lezarasa_vissza" AlternateText="<%$Resources:Buttons,Ugyintezes_lezarasa_vissza %>" />
                <asp:ImageButton ID="Ugyirat_lezarasa" CssClass="highlightit" runat="server" ImageUrl="../images/hu/muvgomb/ugyirat_lezarasa.jpg"
                    OnClick="ImageButton_Click" CommandName="Ugyirat_lezarasa" AlternateText="<%$Resources:Buttons,Ugyirat_lezarasa %>" />
                <asp:ImageButton ID="Ugyirat_lezarasa_vissza" CssClass="highlightit" runat="server"
                    ImageUrl="../images/hu/muvgomb/ugyirat_lezarasa_vissza.jpg" OnClick="ImageButton_Click"
                    CommandName="Ugyirat_lezarasa_vissza" AlternateText="<%$Resources:Buttons,Ugyirat_lezarasa_vissza %>" />
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
