﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="YesNoNotSetSearchFormControl.ascx.cs" Inherits="Component_YesNoNotSetSearchFormControl" %>

<asp:RadioButtonList ID="rbl" runat="server" RepeatColumns="3" RepeatDirection="Vertical" RepeatLayout="Table">
    <asp:ListItem Value="Yes" Text="Igen" />
    <asp:ListItem Value="No" Text="Nem" />
    <asp:ListItem Value="NotSet" Text="Összes" Selected="True" />
</asp:RadioButtonList>
