<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="CsoportokForm.aspx.cs" Inherits="CsoportokForm" Title="Untitled Page" %>


    <%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
    
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc7" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" />
   
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text="Csoport neve:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="Nev" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="T�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:KodtarakDropDownList ID="Tipus_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="�rtes�t�si e-mail c�m:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="ErtesitesEmail_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor" runat="server" id="System_tr">
                            <td class="mrUrlapCaption" style="height: 30px">
                            </td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <asp:CheckBox ID="System_CheckBox" runat="server" Text="Csak a rendszer kezelheti" Enabled="False" /></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="height: 30px">
                            </td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <asp:CheckBox ID="CheckBoxOroklesMod" runat="server" Text="Csoport dolgoz�i l�thatj�k egym�s t�teleit" Checked="true"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" >
                                <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                
                    <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>

