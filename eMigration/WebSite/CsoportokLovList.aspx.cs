using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class CsoportokLovList : Contentum.eUtility.UI.PageBase
{
    private bool disable_refreshLovList = false;
    private string filter = String.Empty;
    private bool hierarchiabanLefele = false;
    private bool kozvetlenulHozzarendelt = false;
    private string szervezetId = string.Empty;

    private Contentum.eUtility.Csoportok.CsoportFilterObject filterObject = new Contentum.eUtility.Csoportok.CsoportFilterObject();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "CsoportokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        // van-e sz�r�s a csoportt�pusra?
        filterObject.QsDeserialize(Request.QueryString);

        if (!String.IsNullOrEmpty(filterObject.SzignalasTipus))
        {
            // egyes szign�l�si m�dokn�l fel�l�rjuk a megadott csoportt�pust, ha volt!
            filterObject.SetCsoportTipusbySzignalasTipus(filterObject.SzignalasTipus);
        }

        switch (filterObject.Tipus)
        {
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Dolgozo:
                LovListHeader1.HeaderTitle = Resources.LovList.CsoportokLovListHeaderTitle_Filtered_Felhasznalo;
                break;
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Szervezet:
                LovListHeader1.HeaderTitle = Resources.LovList.CsoportokLovListHeaderTitle_Filtered_Szervezet;
                break;
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.SajatSzervezetDolgozoi:
                LovListHeader1.HeaderTitle = Resources.LovList.CsoportokLovListHeaderTitle_Filtered_SajatSzervezetDolgozoi;
                break;
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.SajatCsoportDolgozoi:
                LovListHeader1.HeaderTitle = Resources.LovList.CsoportokLovListHeaderTitle_Filtered_SajatCsoportDolgozoi;
                break;
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Bizottsag:
                LovListHeader1.HeaderTitle = Resources.LovList.CsoportokLovListHeaderTitle_Filtered_Bizottsag;
                break;
        }

        //sz�r�s
        if (filterObject.Tipus == Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Irattar)
        {
            LovListHeader1.HeaderTitle = Resources.LovList.CsoportokLovListHeaderTitle_Filtered_Irattar;
        }
        // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
        if (filterObject.Tipus == Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.AllAtmenetiIrattar)
        {
            LovListHeader1.HeaderTitle = Resources.LovList.CsoportokLovListHeaderTitle_Filtered_AllAtmenetiIrattar;
        }
        //csoport sz�r�s
        //hierarchiabanLefele = (Request.QueryString.Get(QueryStringVars.CsoportFilter.HierarchiabanLefele) == "true") ? true : false;
        //kozvetlenulHozzarendelt = (Request.QueryString.Get(QueryStringVars.CsoportFilter.KozvetlenulHozzarendelt) == "true") ? true : false;
        //szervezetId = string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.CsoportFilter.SzervezetId))
        //    ? UI.SetExecParamDefault(Page, new ExecParam()).FelhasznaloSzervezet_Id
        //    : Request.QueryString.Get(QueryStringVars.CsoportFilter.SzervezetId);

        if (!String.IsNullOrEmpty(filterObject.SzervezetId))
        {
            szervezetId = filterObject.SzervezetId;
            hierarchiabanLefele = true;
        }

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("CsoportokSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick =
             JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
            + JavaScripts.SetOnClientClick("CsoportokForm.aspx"
            , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'"
            , Defaults.PopupWidth, Defaults.PopupHeight, null);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
            ScriptManager1.SetFocus(TextBoxSearch);
        }
    }


    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        if (filterObject.Tipus == Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Disabled)
        {
            return;
        }

        KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();
        KRT_CsoportokSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (KRT_CsoportokSearch)Search.GetSearchObject(Page, new KRT_CsoportokSearch());
        }
        else
        {
            search = new KRT_CsoportokSearch();

            search.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
            search.Nev.Value = SearchKey;
        }

        search.OrderBy = "Nev";

        // ha kell, sz�r�nk a t�pusra, ak�r fel�lv�gva a r�szletes keres�sn�l megadott t�pust is
        switch (filterObject.Tipus)
        {
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Dolgozo:
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.OsszesDolgozo:
                search.Tipus.Value = KodTarak.CSOPORTTIPUS.Dolgozo;
                search.Tipus.Operator = Query.Operators.equals;
                break;
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Szervezet:
                search.Tipus.Value = "'" + KodTarak.CSOPORTTIPUS.Szervezet + "','" + KodTarak.CSOPORTTIPUS.Projekt
                    + "','" + KodTarak.CSOPORTTIPUS.HivatalVezetes + "'";
                search.Tipus.Operator = Query.Operators.inner;
                break;
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.SajatSzervezetDolgozoi:
                search.Tipus.Value = KodTarak.CSOPORTTIPUS.Dolgozo;
                search.Tipus.Operator = Query.Operators.equals;
                break;
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.SajatCsoportDolgozoi:
                search.Tipus.Value = KodTarak.CSOPORTTIPUS.Dolgozo;
                search.Tipus.Operator = Query.Operators.equals;
                break;
            case Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Bizottsag:
                search.Tipus.Value = KodTarak.CSOPORTTIPUS.Testulet_Bizottsag;
                search.Tipus.Operator = Query.Operators.equals;
                break;
        }

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result;

        if (!String.IsNullOrEmpty(filterObject.IktatokonyvId) && String.IsNullOrEmpty(szervezetId))
        {
            // CR3157 A csoport kiv�laszt� komponensben iktat�k�nyv v�laszt�sn�l az ide iktat�k �s a jogosultak unioja jelenjen meg
            // eRecord
            //RightsService serviceRights = eAdminService.ServiceFactory.getRightsService();      
            //result = serviceRights.GetAllRightedSubCsoportByJogtargy(ExecParam, filterObject.IktatokonyvId, search); ;
            result = service.GetAllByIktatokonyvIktathat(ExecParam, filterObject.IktatokonyvId, search);
        }
        else
        {
            if (filterObject.Tipus == Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Irattar)
            {
                result = service.GetAllIrattar(ExecParam);
            }
            // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
            else if (filterObject.Tipus == Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.AllAtmenetiIrattar)
            {
                result = service.GetAllAtmenetiIrattar(ExecParam);
            }
            else if (filterObject.Tipus == Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.SajatSzervezetDolgozoi)
            {
                string sajatSzervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
                result = service.GetAllSubCsoport(ExecParam, sajatSzervezetId, false, search);
            }
            else if (filterObject.Tipus == Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.SajatCsoportDolgozoi)
            {
                string sajatSzervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
                result = service.GetAllSubCsoport(ExecParam, sajatSzervezetId, true, search);
            }
            else
            {
                if (hierarchiabanLefele)
                {
                    result = service.GetAllSubCsoport(ExecParam, szervezetId, kozvetlenulHozzarendelt, search);
                }
                else
                    result = service.GetAll(ExecParam, search);
            }
        }

        UI.ListBoxFill(ListBoxSearchResult, result, "Nev", LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;
                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, false, tryFireChangeEvent);

                if (Request.QueryString.Get(QueryStringVars.RefreshCallingWindow) == "1")
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                }
                else
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                }
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult("", true);
                    }
                    break;
            }
        }

        string js = @"var key= null; if(window.event) key = window.event.keyCode; else key = event.keyCode;this.focus();
                    if(key && key == Sys.UI.Key.enter){" + Page.ClientScript.GetPostBackEventReference(ButtonSearch, "") + ";}";

        TextBoxSearch.Attributes.Add("onkeypress", js);

        string js2 = @"var key= null; if(window.event) key = window.event.keyCode; else key = event.keyCode;this.focus();
                    if(key && key == Sys.UI.Key.enter){" + Page.ClientScript.GetPostBackEventReference(LovListFooter1.ImageButton_Ok, "") + ";}";

        ListBoxSearchResult.Attributes.Add("onkeypress", js2);

    }

}
