using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class CsoportokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_CsoportokSearch);

    private const string kcsKod_CSOPORTTIPUS = "CSOPORTTIPUS";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.CsoportokSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            KRT_CsoportokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_CsoportokSearch)Search.GetSearchObject(Page, new KRT_CsoportokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_CsoportokSearch krt_CsoportokSearch = null;
        if (searchObject != null) krt_CsoportokSearch = (KRT_CsoportokSearch)searchObject;

        if (krt_CsoportokSearch != null)
        {
            Nev_TextBox.Text = krt_CsoportokSearch.Nev.Value;
            Tipus_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_CSOPORTTIPUS, krt_CsoportokSearch.Tipus.Value, true, SearchHeader1.ErrorPanel);
            ErtesitesEmail_TextBox.Text = krt_CsoportokSearch.ErtesitesEmail.Value;
            
            Ervenyesseg_SearchFormComponent1.SetDefault(
                krt_CsoportokSearch.ErvKezd, krt_CsoportokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_CsoportokSearch SetSearchObjectFromComponents()
    {
        KRT_CsoportokSearch krt_CsoportokSearch = (KRT_CsoportokSearch)SearchHeader1.TemplateObject;
        if (krt_CsoportokSearch == null)
        {
            krt_CsoportokSearch = new KRT_CsoportokSearch();
        }
               
        if (!String.IsNullOrEmpty(Nev_TextBox.Text))
        {
            krt_CsoportokSearch.Nev.Value = Nev_TextBox.Text;
            krt_CsoportokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Tipus_KodtarakDropDownList.SelectedValue))
        {
            krt_CsoportokSearch.Tipus.Value = Tipus_KodtarakDropDownList.SelectedValue;
            krt_CsoportokSearch.Tipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(ErtesitesEmail_TextBox.Text))
        {
            krt_CsoportokSearch.ErtesitesEmail.Value = ErtesitesEmail_TextBox.Text;
            krt_CsoportokSearch.ErtesitesEmail.Operator = Search.GetOperatorByLikeCharater(ErtesitesEmail_TextBox.Text);
        }
        
        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            krt_CsoportokSearch.ErvKezd, krt_CsoportokSearch.ErvVege);

        return krt_CsoportokSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_CsoportokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_CsoportokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_CsoportokSearch();
    }

}

