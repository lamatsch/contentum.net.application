using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Contentum.eAdmin.Utility;

public partial class _Default : Contentum.eUtility.UI.PageBase
{

    //Authentication auth = new Authentication();
    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Authentication.CheckLogin(Page);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Component_MenuControl menuControl =
            Component_MenuControl.AddMenuControl(Panel1);
    }

}
