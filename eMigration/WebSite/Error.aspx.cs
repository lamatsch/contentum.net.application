using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Error : Contentum.eUtility.UI.PageBase
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Response.CacheControl = "no-cache";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string errorCode = Page.Request.QueryString.Get("errorcode");
        string ErrorMsg = Page.Request.QueryString.Get("ErrorMsg");

        String mailtoAddress = String.Empty;

        try
        {
            mailtoAddress = Contentum.eUtility.Rendszerparameterek.Get(Page, Contentum.eUtility.Rendszerparameterek.HIBABEJELENTES_EMAIL_CIM, false) ?? String.Empty;
        }
        catch
        {
            // pl. m�g nincs bejelentkezve
        }

        mailtoLink.HRef = String.Format("mailto:{0}", mailtoAddress);
        mailtoLink.InnerText = mailtoAddress;

        ErrorPanel1.EmailButtonClick += new CommandEventHandler(errorPanel_EmailButtonClick);

        if (!String.IsNullOrEmpty(errorCode))
        {            
            try
            {
                ErrorPanel1.Header = Resources.Error.ResourceManager.GetString(errorCode);
                ErrorPanel1.Body = Resources.Error.ResourceManager.GetString(errorCode + "_Text");                
            }
            catch
            {
                ErrorPanel1.Header = Resources.Error.ErrorLabel + ": " + errorCode;
                ErrorPanel1.Body = Resources.Error.NotErrorCode;
            }
        }
        else if (!String.IsNullOrEmpty(ErrorMsg))
        {
            ErrorPanel1.Header = Resources.Error.ErrorLabel;
            ErrorPanel1.Body = ErrorMsg;
        }
        else
        {
            //ErrorPanel1.Header = "";
            //ErrorPanel1.Body = "";

            // Az �ltal�nos hibapanelt jelen�tj�k meg:
            ErrorPanel1.Visible = false;
            GenericErrorPanel.Visible = true;


            Label_CurrentTime.Text = DateTime.Now.ToString();


        }

        //try
        //{
        //    if (Page.PreviousPage == null || Page.PreviousPage.MasterPageFile.Contains("EmptyMasterPage.master"))
        //    {
        //        ImageVissza.Visible = false;
        //    }
        //    else
        //    {
        //        ImageVissza.Visible = true;
        //    }

        //}
        //catch 
        //{
        //    ImageVissza.Visible = false;
        //    ImageClose.Visible = true;
        //}

        ImageClose.Focus();
        
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string prevUrl = UI.GetPageFromHistory(Page, 2);

            if (prevUrl != null)
            {
                string js = "if(typeof(dialogArguments) == 'undefined' && history.length>0)";
                js += "{history.back();} else {";
                js += JavaScripts.GetRedirectPageScript(prevUrl);
                js += "};return false;";

                ImageVissza.OnClientClick = js;
            }
            else if (!Authentication.IsAUserLoggedIn(Page))
            {
                string js = "";
                js += JavaScripts.GetRedirectPageScript("Login.aspx");
                js += ";return false;";

                ImageVissza.OnClientClick = js;
            }
            else
            {
                ImageVissza.Visible = false;
            }
        }
    }

    protected void errorPanel_EmailButtonClick(object sender, CommandEventArgs e)
    {
        Contentum.eUIControls.eErrorPanel errorPanel = (Contentum.eUIControls.eErrorPanel)sender;
        string[] parameters = e.CommandArgument.ToString().Split(';');
        string ErrorCode = parameters[0];
        string ErrorMessage = parameters[1];
        string CurrentLogEntry = parameters[2];

        bool IsSuccess = false;

        try
        {
            IsSuccess = Notify.SendErrorInEmail(Page, ErrorCode, ErrorMessage, CurrentLogEntry);
        }
        catch
        {
            // pl. m�g nincs bejelentkezve
        }

        if (errorPanel != null)
        {
            JavaScripts.RegisterEmailFeedbackMessage(errorPanel, IsSuccess);
        }

    }


    protected void ImageVissza_Click(object sender, ImageClickEventArgs e)
    {    
    }
}
