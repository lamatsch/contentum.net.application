﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="ErtekLovList.aspx.cs" Inherits="ErtekLovList" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,ErtekLovListTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table style="width: 90%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr id="trMaxRowWarning" class="LovListWarningRow" runat="server" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr class="urlapSor">
                                                        <td class="mrUrlapCaption">
                                                            <asp:Label ID="labelNev" runat="server" Text="Megnevezés:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="txtNev" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor">
                                                        <td colspan="6">
                                                            <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"
                                                                AlternateText="Keresés"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="GridCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel1" runat="server">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align: top; text-align: left" class="GridViewHeaderBackGroundStyle">
                                                                <!-- Kliens oldali kiválasztás kezelése -->
                                                                <asp:HiddenField ID="GridViewSelectedIndex" runat="server"></asp:HiddenField>
                                                                <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                                    CellPadding="0" BorderWidth="1px" AllowPaging="false" PagerSettings-Visible="false"
                                                                    AutoGenerateColumns="False" AllowSorting="true" AlternatingRowStyle-CssClass="GridViewAlternateRowStyle" OnSorting="GridViewSearchResult_Sorting">
                                                                    <PagerSettings Visible="False"></PagerSettings>
                                                                    <Columns>
                                                                        <asp:BoundField HeaderText="Megnevezés">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="440px"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True"></RowStyle>
                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"></SelectedRowStyle>
                                                                    <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>

