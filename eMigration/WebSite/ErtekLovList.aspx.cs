﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.Utility;
using Contentum.eQuery;

public partial class ErtekLovList : System.Web.UI.Page
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;

    private string _column;

    protected void Page_Init(object sender, EventArgs e)
    {
        RegisterJavascripts();

        LovListFooter1.ButtonsClick += new CommandEventHandler(LovListFooter_ButtonsClick);

        _column = Request.QueryString.Get("column");
        var boundField = (BoundField)(GridViewSearchResult.Columns[0]);
        boundField.DataField = _column;
        boundField.SortExpression = _column;

        if (!IsPostBack)
        {
            txtNev.Focus();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        if (!IsPostBack)
        {
            FillGridViewSearchResult();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult();
                    }
                    break;
            }
        }

        // scrollozhatóság állítása
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridCPE, 15);
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, GridCPE);
        FillGridViewSearchResult();
    }

    protected void FillGridViewSearchResult()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("GridViewSearchResult", ViewState, _column);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("GridViewSearchResult", ViewState,SortDirection.Ascending);

        FillGridViewSearchResult(sortExpression, sortDirection);
    }

    protected void FillGridViewSearchResult(String SortExpression, SortDirection SortDirection)
    {
        GridViewSelectedIndex.Value = String.Empty;

        var service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
        var search = new MIG_FoszamSearch();

        var prefixText = txtNev.Text.Replace("*", "%"); // *-gal is működjön
        if (!prefixText.Contains("%"))
        {
            prefixText += '%';
        }
        search.WhereByManual = "DISTINCT:" + _column + " LIKE '" + prefixText.Replace("'", "''") + "' ";

        search.OrderBy = Search.GetOrderBy("GridViewSearchResult", ViewState, SortExpression, SortDirection);

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void GridViewSearchResult_Sorting(object sender, GridViewSortEventArgs e)
    {
        FillGridViewSearchResult(e.SortExpression, UI.GetSortToGridView("GridViewSearchResult", ViewState, e.SortExpression));
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 0);

                    JavaScripts.SendBackResultToCallingWindow(Page,String.Empty, selectedText);
                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                    GridViewSearchResult.Visible = false;
                    disable_refreshLovList = true;
                }
            }
            else
            {
                //GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    private void RegisterJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, new HiddenField());
            }
        }

        base.Render(writer);
    }
}
