<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="FelulvizsgalatForm.aspx.cs" Inherits="FelulvizsgalatForm" Title="Untitled Page" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc10" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc11" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>
<%@ Register Src="eMigrationComponent/JegyzekTextBox.ascx" TagName="JegyzekTextBox" TagPrefix="uc12" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="eMigrationComponent/IrattariJelTextBox.ascx" TagName="IrattariJelTextBox" TagPrefix="uc" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc" %>
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc"%>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc20" %>
<%@ Register Src="~/Component/FuggoKodtarakDropDownList.ascx" TagPrefix="uc13" TagName="FuggoKodtarakDropDownList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <uc:CustomUpdateProgress ID="UpdateProgress" runat="server" />
    
    <uc1:FormHeader ID="FormHeader1" runat="server" />

    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="upFelulvizsgalat" runat="server">
    <ContentTemplate>
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <asp:HiddenField ID="hfSelectedFoszamok" runat="server" />

                    <eUI:eFormPanel ID="EFormPanelSzerelesError" runat="server" Visible="false">
                    <asp:HiddenField ID="hfBreakAbleSzereltFoszamok" runat="server" />
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="label3" runat="server" Text="A k�vetkez� szerelt �gyiratokon nem hajthat� v�gre a m�velet!"></asp:Label>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="labelBadSzereltFoszamok" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="label6" runat="server" Text="K�v�nja felbontani ezeket a szerel�seket?"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>

                    <eUI:eFormPanel ID="EFormPanelSearch" runat="server" style="margin-bottom: 5px;" Visible="false">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                                <asp:Label ID="Label2" runat="server" Text="�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:RequiredNumberBox ID="UI_YEAR_TextBox" runat="server" Validate="false"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label4" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="UI_SAV_TextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label16" runat="server" Text="F�sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                               <uc:SzamIntervallum_SearchFormControl id="UI_NUM_TextBox"
                                 runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="Iratt�ri Jel:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:IrattariJelTextBox ID="IRJ2000_TextBox" runat="server" SearchMode="true" EvTextBoxControlID="UI_YEAR_TextBox"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelIratHelye" runat="server" Text="Irat helye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="ddlistIratHelye" runat="server" CssClass="mrUrlapInputComboBox">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>

                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUserNev" runat="server" Text="Fel�lvizsg�l�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc10:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBoxFelulvizsgalo" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_FelulvizsgalatIdeje" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelNev" runat="server" Text="Fel�lvizsg�lat ideje:"></asp:Label>
                             </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="CalendarControlFelulvizsgalatIdeje" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_UjVizsgalat" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label150" runat="server" Text="�j �rz�si id�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc20:RequiredNumberBox ID="RequierdNumberBox_UjorzesIdo" runat="server"/>
                                <%--BLG_2156--%>
                                <uc13:FuggoKodtarakDropDownList runat="server" ID="FuggoKodtarakDropDownList_IdoegysegFelhasznalas" KodcsoportKod="IDOEGYSEG_FELHASZNALAS"   CssClass="hiddenitem" />
                                <uc13:FuggoKodtarakDropDownList runat="server" ID="FuggoKodtarakDropDownList_Idoegyseg" KodcsoportKod="IDOEGYSEG"  ParentControlId="FuggoKodtarakDropDownList_IdoegysegFelhasznalas" Validate="True" />
                           </td>
                            
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelFelulvizsgalatEredmenye" runat="server" Text="Fel�lvizsg�lat eredm�nye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="ddlistFelulvizsgalatEredmenye" CausesValidation="false" AutoPostBack="true" runat="server" CssClass="mrUrlapInputComboBox" />
                            </td>
                        </tr>
                        <tr class="urlapSor"  id="tr_Jegyzek" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label1" runat="server" Text="Jegyz�k:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc12:JegyzekTextBox runat="server" ID="JegyzekekTextBox1" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMegyjegyzes" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="TextBoxMegyjegyzes" runat="server" Height="60px" TextMode="MultiLine" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                                    
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

