using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eMigration.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eQuery;

public partial class FelulvizsgalatForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    private string StartUp = String.Empty;

    Type _type = typeof(MIG_FoszamSearch);
    private const string customSessionName = Contentum.eMigration.Utility.Constants.CustomSearchObjectSessionNames.MigFelulvizsgalatSearch;

    public static class Felulvizsgalat
    {
        public static readonly ListItem Selejtezes = new ListItem("Selejtez�s", "S");
        public static readonly ListItem LeveltarbaAdas = new ListItem("Lev�lt�rba ad�s", "L");
        public static readonly ListItem EgyebSzervezetnekAtadas = new ListItem("Egy�b szervezetnek �tad�s", "E");
        public static readonly ListItem Meghosszabitas = new ListItem("�rz�si id� meghosszab�t�sa", "M");

        public static void FillDropDownList(DropDownList list, string startup)
        {
            list.Items.Clear();
            if (!String.IsNullOrEmpty(startup))
            {
                if (startup == Constants.Startup.FromSelejtezes)
                {
                    list.Items.Add(Selejtezes);
                }

                if (startup == Constants.Startup.FromLeveltarbaAdas)
                {
                    list.Items.Add(LeveltarbaAdas);
                }

                if (startup == Constants.Startup.FromEgyebSzervezetnekAtadas)
                {
                    list.Items.Add(EgyebSzervezetnekAtadas);
                }

                list.Items.Add(Meghosszabitas);
            }
        }
    }

    private string ParentGridViewClientID
    {
        get
        {
            return Request.QueryString.Get(QueryStringVars.FoszamGridViewClientID);
        }
    }

    private string SelectedFoszamokQueryString
    {
        get
        {
            return Request.QueryString.Get(QueryStringVars.ObjektumId);
        }
    }

    private string[] SelectedFoszamok
    {
        get
        {
            // TODO: ObjektumId helyett FoszamId
            string ids = SelectedFoszamokQueryString;

            if (String.IsNullOrEmpty(ids))
            {
                ids = hfSelectedFoszamok.Value;
            }

            if (String.IsNullOrEmpty(ids))
            {
                return null;
            }

            string[] foszamIds = ids.Split(',');
            return foszamIds;
        }
    }

    private bool IsSearchPanelVisible
    {
        get
        {
            return String.IsNullOrEmpty(SelectedFoszamokQueryString) && String.IsNullOrEmpty(ParentGridViewClientID);
        }
    }

    private void SetViewControls()
    {

    }

    private void SetModifyControls()
    {

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        StartUp = Request.QueryString.Get(QueryStringVars.Startup);
        JegyzekekTextBox1.StartUp = StartUp;

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Felulvizsgalat" + Command);
                break;
        }

        if (!IsPostBack)
        {
            MIG_JegyzekTetelekService svc = eMigrationService.ServiceFactory.GetMIG_JegyzekTetelekService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            if (StartUp == Constants.Startup.FromEgyebSzervezetnekAtadas)
            {

            }
            else
            {
                // TODO:

                ////szerel�sek ellen�rz�se
                //res = svc.CheckSzerelesek(xpm, GetSelectedFoszamok());

                //if (res.Ds.Tables[0].Rows.Count > 0)
                //{
                //    EFormPanelSzerelesError.Visible = true;
                //    EFormPanel1.Visible = false;
                //    int i = 0;
                //    string[] ugyiratok = new string[res.Ds.Tables[0].Rows.Count];
                //    foreach (DataRow row in res.Ds.Tables[0].Rows)
                //    {
                //        if (i == 0)
                //        {
                //            labelBadSzereltFoszamok.Text = "<ul style=\"list-style-type:decimal;\">";
                //        }
                //        else // if (i != 0)
                //        {

                //            hfBreakAbleSzereltFoszamok.Value += ";";
                //        }



                //        string onclick = JavaScripts.SetOnCLientClick_NoPostBack("FoszamForm.aspx"
                //        , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + row["Id"].ToString()
                //        + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromFelulvizsgalat
                //        + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                //        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);
                //        labelBadSzereltFoszamok.Text += "<li style=\"padding-top:5px;\"><a href='' class=\"badUgyiratok\" onclick=\"" + onclick + "\">" + row["Foszam_Merge"].ToString() + "</a></<li>";
                //        hfBreakAbleSzereltFoszamok.Value += row["Id"].ToString();
                //        i++;
                //    }
                //    labelBadSzereltFoszamok.Text += "</ul>";


                //}
            }
            
        }
        else
        {
            EFormPanel1.Visible = true;
        }

        //if (Command == CommandName.View || Command == CommandName.Modify)
        //{
        //    String id = Request.QueryString.Get(QueryStringVars.Id);
        //    if (String.IsNullOrEmpty(id))
        //    {
        //        // nincs Id megadva:
        //        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        //    }
        //    else
        //    {
        //        //KRT_FelulvizsgalatService service = eAdminService.ServiceFactory.GetKRT_FelulvizsgalatService();
        //        //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        //        //execParam.Record_Id = id;

        //        //Result result = service.Get(execParam);
        //        //if (String.IsNullOrEmpty(result.ErrorCode))
        //        //{
        //        //    KRT_Felulvizsgalat krt_Felulvizsgalat = (KRT_Felulvizsgalat)result.Record;
        //        //    LoadComponentsFromBusinessObject(krt_Felulvizsgalat);
        //        //}
        //        //else
        //        //{
        //        //    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        //        //}
        //    }

        //    //cbAutogeneratePartner.Checked = false;
        //    //tr_AutoGeneratePartner_CheckBox.Visible = false;
        //    //tr_Jelszo_lejar_ido.Visible = false;
        //    //tr_Jelszo.Visible = false;
        //    //tr_Tipus_dropdown.Visible = false;
        //}
        //else if (Command == CommandName.New)
        //{
        //    string partnerID = Request.QueryString.Get(QueryStringVars.PartnerId);
        //    if (!String.IsNullOrEmpty(partnerID))
        //    {
        //        //PartnerTextBox1.Id_HiddenField = partnerID;
        //        //PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
        //        //PartnerTextBox1.Enabled = false;
        //        //tr_AutoGeneratePartner_CheckBox.Visible = false;
        //    }

        //}
        //if (Command == CommandName.View)
        //{
        //    SetViewControls();
        //}
        //if (Command == CommandName.Modify)
        //{
        //    SetModifyControls();
        //}

        //PartnerTextBox1.Type = Constants.FilterType.Partnerek.BelsoSzemely;

        if (!IsPostBack)
        {
            FillDropDownListFelulvizsgalat();
            FelhasznaloCsoportTextBoxFelulvizsgalo.Id_HiddenField = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
            FelhasznaloCsoportTextBoxFelulvizsgalo.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            CalendarControlFelulvizsgalatIdeje.Text = DateTime.Now.ToString();
        }

        //Kijel�lt f�sz�mok felv�tele a sz�l� oldalr�l
        if (!String.IsNullOrEmpty(ParentGridViewClientID) && !IsPostBack)
        {
            string js = @"var parentWindow = Utility.Popup.getParentWindow();
                          var hfSelectedFoszamok = $get('" + hfSelectedFoszamok.ClientID + @"');
                         if(parentWindow && hfSelectedFoszamok){ 
                         hfSelectedFoszamok.value = parentWindow.getIdsBySelectedCheckboxes('" + ParentGridViewClientID + "','check') };";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "GetSelectedFoszamok", js, true);
        }

        FuggoKodtarakDropDownList_IdoegysegFelhasznalas.SelectedValue = KodTarak.IdoegysegFelhasznalas.Selejtezes;
    }

    private void FillDropDownListFelulvizsgalat()
    {
        Felulvizsgalat.FillDropDownList(ddlistFelulvizsgalatEredmenye, StartUp);
        if (ddlistFelulvizsgalatEredmenye.Items.Count == 1) ddlistFelulvizsgalatEredmenye.ReadOnly = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        //FormHeader1.HeaderTitle = Resources.Form.FelulvizsgalatFormHeaderTitle;

        // Munkahely kiv�laszt�sn�l sz�r�s a szervezetre:
        ////PartnerTextBoxMunkahely.SetFilterType_Szervezet();

        ////calendarJelszoLejar.Text = DefaultDates.EndDate;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        FormFooter1.Command = CommandName.New;

        if (EErrorPanel1.Visible)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel1.Update();
        }

        if (!IsPostBack)
        {
            MIG_FoszamSearch searchObject = null;
            if (Search.IsSearchObjectInSession_CustomSessionName(Page, customSessionName))
            {
                searchObject = (MIG_FoszamSearch)Search.GetSearchObject_CustomSessionName(Page, new MIG_FoszamSearch(), customSessionName);
            }
            else
            {
                searchObject = new MIG_FoszamSearch();
            }

            IratHelye.FillDropDownList(ddlistIratHelye, Page);
            // �res �rt�k hozz�ad�sa a list�hoz
            ddlistIratHelye.Items.Insert(0, new ListItem("[Nincs megadva elem]", ""));
            LoadComponentsFromSearchObject(searchObject);

            LoadComponentsFromSearchObject(searchObject);
        }

    }

    // business object --> form
    //private void LoadComponentsFromBusinessObject(KRT_Felulvizsgalat krt_Felulvizsgalat)
    //{
    //    requiredTextBoxUserNev.Text = krt_Felulvizsgalat.UserNev;
    //    requiredTextBoxNev.Text = krt_Felulvizsgalat.Nev;

    //    PartnerTextBox1.Id_HiddenField = krt_Felulvizsgalat.Partner_id;
    //    PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

    //    PartnerTextBoxMunkahely.Id_HiddenField = krt_Felulvizsgalat.Partner_Id_Munkahely;
    //    PartnerTextBoxMunkahely.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

    //    textEmail.Text = krt_Felulvizsgalat.EMail;

    //    try
    //    {
    //        ddownAuthType.SelectedValue = krt_Felulvizsgalat.Tipus;
    //    }
    //    catch (System.ArgumentOutOfRangeException)
    //    {
    //        ddownAuthType.ClearSelection();
    //    }

    //    calendarJelszoLejar.Visible = false;
    //    requiredTextBoxJelszo.Visible = false;

    //    this.SetRadioList(krt_Felulvizsgalat.Engedelyezett);

    //    //cbSystem.Checked = (krt_Felulvizsgalat.System == "1") ? true : false;

    //    ErvenyessegCalendarControl1.ErvKezd = krt_Felulvizsgalat.ErvKezd;
    //    ErvenyessegCalendarControl1.ErvVege = krt_Felulvizsgalat.ErvVege;

    //    FormHeader1.Record_Ver = krt_Felulvizsgalat.Base.Ver;

    //    FormPart_CreatedModified1.SetComponentValues(krt_Felulvizsgalat.Base);

    //}

    // form --> business object
    //private KRT_Felulvizsgalat GetBusinessObjectFromComponents()
    //{
    //    KRT_Felulvizsgalat krt_Felulvizsgalat = new KRT_Felulvizsgalat();
    //    krt_Felulvizsgalat.Updated.SetValueAll(false);
    //    krt_Felulvizsgalat.Base.Updated.SetValueAll(false);

    //    krt_Felulvizsgalat.UserNev = requiredTextBoxUserNev.Text;
    //    krt_Felulvizsgalat.Updated.UserNev = pageView.GetUpdatedByView(requiredTextBoxUserNev);

    //    krt_Felulvizsgalat.Nev = requiredTextBoxNev.Text;
    //    krt_Felulvizsgalat.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);

    //    if (cbAutogeneratePartner.Checked == false)
    //    {
    //        krt_Felulvizsgalat.Partner_id = PartnerTextBox1.Id_HiddenField;
    //        krt_Felulvizsgalat.Updated.Partner_id = pageView.GetUpdatedByView(PartnerTextBox1);
    //    }

    //    krt_Felulvizsgalat.Partner_Id_Munkahely = PartnerTextBoxMunkahely.Id_HiddenField;
    //    krt_Felulvizsgalat.Updated.Partner_Id_Munkahely = pageView.GetUpdatedByView(PartnerTextBoxMunkahely);

    //    krt_Felulvizsgalat.EMail = textEmail.Text;
    //    krt_Felulvizsgalat.Updated.EMail = pageView.GetUpdatedByView(textEmail);

    //    krt_Felulvizsgalat.Tipus = ddownAuthType.SelectedValue;
    //    krt_Felulvizsgalat.Updated.Tipus = pageView.GetUpdatedByView(ddownAuthType);

    //    if (!String.IsNullOrEmpty(calendarJelszoLejar.Text))
    //    {
    //        krt_Felulvizsgalat.JelszoLejaratIdo = calendarJelszoLejar.Text;
    //        krt_Felulvizsgalat.Updated.JelszoLejaratIdo = pageView.GetUpdatedByView(calendarJelszoLejar);
    //    }

    //    krt_Felulvizsgalat.Jelszo = requiredTextBoxJelszo.Text;
    //    krt_Felulvizsgalat.Updated.Jelszo = pageView.GetUpdatedByView(requiredTextBoxJelszo);

    //    if (rbListEngedelyzett.SelectedIndex > -1)
    //    {
    //        krt_Felulvizsgalat.Engedelyezett = rbListEngedelyzett.SelectedValue;
    //        krt_Felulvizsgalat.Updated.Engedelyezett = pageView.GetUpdatedByView(rbListEngedelyzett);
    //    }

    //    //krt_Felulvizsgalat.System = cbSystem.Checked ? "1" : "0";
    //    //krt_Felulvizsgalat.Updated.System = pageView.GetUpdatedByView(cbSystem);

    //    krt_Felulvizsgalat.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
    //    krt_Felulvizsgalat.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

    //    krt_Felulvizsgalat.ErvVege = ErvenyessegCalendarControl1.ErvVege;
    //    krt_Felulvizsgalat.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

    //    krt_Felulvizsgalat.Base.Ver = FormHeader1.Record_Ver;
    //    krt_Felulvizsgalat.Base.Updated.Ver = true;

    //    return krt_Felulvizsgalat;
    //}

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            //compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);
            //compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            //compSelector.Add_ComponentOnClick(PartnerTextBox1);
            //compSelector.Add_ComponentOnClick(cbAutogeneratePartner);
            //compSelector.Add_ComponentOnClick(PartnerTextBoxMunkahely);
            //compSelector.Add_ComponentOnClick(ddownMaxMinosites);
            //compSelector.Add_ComponentOnClick(textEmail);
            //compSelector.Add_ComponentOnClick(ddownAuthType);
            //compSelector.Add_ComponentOnClick(calendarJelszoLejar);
            //compSelector.Add_ComponentOnClick(requiredTextBoxJelszo);
            //compSelector.Add_ComponentOnClick(rbListEngedelyzett);
            //compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            //compSelector.Add_ComponentOnClick(cbSystem);

            FormFooter1.SaveEnabled = false;

            //requiredTextBoxJelszo.Enabled = true;
            //AutoGeneratePartner_CheckBox.Attributes["onclick"] = "";
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //C�m be�ll�t�sa
        FormHeader1.DisableModeLabel = true;
        FormHeader1.HeaderTitle = Resources.Form.FelulvizsgalatFormHeaderTitle;
        if (StartUp == Constants.Startup.FromSelejtezes)
        {
            FormHeader1.HeaderTitle = Resources.Form.SelejtezesFelulvizsgalatFormHeaderTitle;
        }
        else if (StartUp == Constants.Startup.FromLeveltarbaAdas)
        {
            FormHeader1.HeaderTitle = Resources.Form.LeveltarbaAdasFelulvizsgalatFormHeaderTitle;   
        }
        else if (StartUp == Constants.Startup.FromEgyebSzervezetnekAtadas)
        {
            FormHeader1.HeaderTitle = Resources.Form.EgyebSzervezetnekAtadasFelulvizsgalatFormHeaderTitle;
        }

        if (ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.LeveltarbaAdas.Value || ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Selejtezes.Value
            || ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.EgyebSzervezetnekAtadas.Value)
        {
            tr_FelulvizsgalatIdeje.Visible = true;
            tr_UjVizsgalat.Visible = false;
            tr_Jegyzek.Visible = true;
        }
        else if (ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Meghosszabitas.Value)
        {
            tr_FelulvizsgalatIdeje.Visible = false;
            tr_UjVizsgalat.Visible = true;
            tr_Jegyzek.Visible = false;
        }

        if (SelectedFoszamok == null)
        {
            EFormPanelSearch.Visible = IsSearchPanelVisible;
        }
    }

    private void LoadComponentsFromSearchObject(object searchObject)
    {
        MIG_FoszamSearch MIG_FoszamSearch = null;
        if (searchObject != null) MIG_FoszamSearch = (MIG_FoszamSearch)searchObject;

        if (MIG_FoszamSearch != null)
        {
            UI_SAV_TextBox.Text = MIG_FoszamSearch.EdokSav.Value;
            UI_YEAR_TextBox.Text = MIG_FoszamSearch.UI_YEAR.Value;
            UI_NUM_TextBox.SetComponentFromSearchObjectFields(MIG_FoszamSearch.UI_NUM);
            IRJ2000_TextBox.Text = MIG_FoszamSearch.IRJ2000.Value;
            ddlistIratHelye.SelectedValue = MIG_FoszamSearch.UGYHOL.Value;
        }
    }

    private MIG_FoszamSearch SetSearchObjectFromComponents()
    {
        MIG_FoszamSearch MIG_FoszamSearch = new MIG_FoszamSearch();

        if (SelectedFoszamok != null)
        {
            MIG_FoszamSearch.Id.In(SelectedFoszamok);
        }
        else
        {
            MIG_FoszamSearch.EdokSav.LikeWhenNeeded(UI_SAV_TextBox.Text);
            MIG_FoszamSearch.UI_YEAR.LikeWhenNeeded(UI_YEAR_TextBox.Text);
            
            UI_NUM_TextBox.SetSearchObjectFields(MIG_FoszamSearch.UI_NUM);

            if (!String.IsNullOrEmpty(IRJ2000_TextBox.Text))
            {
                MIG_FoszamSearch.IRJ2000.Value = IRJ2000_TextBox.Text;
                MIG_FoszamSearch.IRJ2000.Operator = Search.GetOperatorByLikeCharater(IRJ2000_TextBox.Text);
                MIG_FoszamSearch.IRJ2000.OrGroup("01");
                MIG_FoszamSearch.UI_IRJ.Value = IRJ2000_TextBox.Text;
                MIG_FoszamSearch.UI_IRJ.Operator = Search.GetOperatorByLikeCharater(IRJ2000_TextBox.Text);
                MIG_FoszamSearch.UI_IRJ.OrGroup("01");
            }

            if (!String.IsNullOrEmpty(ddlistIratHelye.SelectedValue))
            {
                MIG_FoszamSearch.UGYHOL.Filter(ddlistIratHelye.SelectedValue);
            }
        }

        return MIG_FoszamSearch;
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Felulvizsgalat" + Command))
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                try
                {
                    ValidateFields();
                    //string[] foszamIds = GetSelectedFoszamok();
                    MIG_FoszamSearch _MIG_FoszamSearch = SetSearchObjectFromComponents();
                    Result result = null;
                    if (ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Meghosszabitas.Value)
                    {
                        MIG_FoszamService svcFoszamok = eMigrationService.ServiceFactory.GetMIG_FoszamService();

                        result = svcFoszamok.Meghosszabitas(execParam, SelectedFoszamok, RequierdNumberBox_UjorzesIdo.Text, FuggoKodtarakDropDownList_Idoegyseg.SelectedValue, CalendarControlFelulvizsgalatIdeje.SelectedDate, TextBoxMegyjegyzes.Text);
                    }
                    else
                    {
                        MIG_JegyzekTetelekService svcJegyzekTetelek = eMigrationService.ServiceFactory.GetMIG_JegyzekTetelekService();

                        result = svcJegyzekTetelek.JegyzekreHelyezes(execParam, _MIG_FoszamSearch, JegyzekekTextBox1.Id_HiddenField, TextBoxMegyjegyzes.Text);
                    }

                    if (!result.IsError)
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                        JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                        ErrorUpdatePanel1.Update();
                    }
                }
                catch (FormatException fe)
                {
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFormatErrorHeader, fe.Message);
                    ErrorUpdatePanel1.Update();
                }
            }

            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    private void ValidateFields()
    {
    }

    private string[] GetSelectedFoszamok()
    {
        // TODO: ObjektumId helyett FoszamId
        string ids = Request.QueryString.Get(QueryStringVars.ObjektumId);

        if (String.IsNullOrEmpty(ids.Trim()))
        {
            return new string[] { };
        }

        string[] foszamIds = ids.Split(',');
        return foszamIds;
    }

    private void registerJavascripts()
    {

        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }
}
