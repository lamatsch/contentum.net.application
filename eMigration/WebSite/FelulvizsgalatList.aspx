<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="FelulvizsgalatList.aspx.cs" Inherits="FelulvizsgalatList" Title="Untitled Page" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="FelulvizsgalhatoCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <ajaxToolkit:CollapsiblePanelExtender ID="FelulvizsgalhatoCPE" runat="server" TargetControlID="PanelFelulvizsgalhato"
                    CollapsedSize="20" Collapsed="False" ExpandControlID="FelulvizsgalhatoCPEButton"
                    CollapseControlID="FelulvizsgalhatoCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                    AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                    ImageControlID="FelulvizsgalhatoCPEButton" ExpandedSize="0" ExpandedText="Fel�lvizsg�lhat� f�sz�mok list�ja"
                    CollapsedText="Fel�lvizsg�lhat� f�sz�mok list�ja">
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="PanelFelulvizsgalhato" runat="server">
                    <ajaxToolkit:TabContainer ID="TabContainerMaster" runat="server" OnActiveTabChanged="TabContainerMaster_ActiveTabChanged"
                        AutoPostBack="false" OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                        <!--Selejtezheto-->
                        <ajaxToolkit:TabPanel ID="TabPanelSelejtezheto" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:Label ID="labelSelejtezheto" runat="server" Text="Selejtezhet� migr�lt t�telek"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelSelejtezheto" runat="server" OnLoad="updatePanelSelejtezheto_Load">
                                    <ContentTemplate>
                                        <!-- scrollozhat�s�g miatt -->
                                        <ajaxToolkit:CollapsiblePanelExtender ID="SelejtezhetoCPE" runat="server" TargetControlID="panelSelejtezheto"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                            AutoExpand="false" ExpandedSize="0">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="panelSelejtezheto" runat="server">
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <asp:GridView ID="gridViewSelejtezheto" runat="server" OnRowCommand="gridViewSelejtezheto_RowCommand"
                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                            OnPreRender="gridViewSelejtezheto_PreRender" AutoGenerateColumns="False" DataKeyNames="Id"
                                                            OnSorting="gridViewSelejtezheto_Sorting" OnRowDataBound="gridViewSelejtezheto_RowDataBound">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <div class="DisableWrap">
                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                            &nbsp;
                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                        </div>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText"
                                                                        Enabled='<%# IsNemSzerelt(Eval("Csatolva_Id") as Guid?, Eval("Edok_Utoirat_Id") as Guid?) %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                    <HeaderStyle Width="25px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:CommandField>
                                                                <asp:TemplateField SortExpression="MIG_Foszam.IRJ2000 {0}, MIG_Foszam.UI_IRJ" HeaderText="Iratt&#225;ri Jel">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="70px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelIRJ" runat="server" Text='<%# GetIrattariJel(Eval("UI_YEAR"),Eval("UI_IRJ"),Eval("IRJ2000")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m" SortExpression="MIG_Foszam.EdokSav {0}, MIG_Foszam.UI_NUM {0}, MIG_Foszam.UI_YEAR">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="UI_NAME" SortExpression="MIG_Foszam.UI_NAME" HeaderText="&#220;gyf&#233;l">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="200px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MEMO" SortExpression="MIG_Foszam.MEMO" HeaderText="T&#225;rgy">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="230px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="UI_OT_ID" SortExpression="MIG_Foszam.UI_OT_ID" HeaderText="Azonos�t�">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Eloado_Nev" SortExpression="Eloado_Nev" HeaderText="El�ad�">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="El�irat" SortExpression="Eloirat_Azon_Id">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <ItemTemplate>
                                                                        <%-- Az Eloirat_Azon_Id mez�b�l jelen�tj�k meg az azonos�t�t --%>
                                                                        <asp:Label ID="labelEloiratNev" runat="server" Text='<%# Format_Eloirat_Azon_Id(Eval("Eloirat_Azon_Id").ToString()) %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Ut�irat" SortExpression="Edok_Utoirat_Azon">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <ItemTemplate>
                                                                        <%--Vagy az Edok_Utoirat_Azon mez�t, vagy a MergeFoszam_Csatolt mez�t jelen�tj�k meg--%>
                                                                        <asp:Label ID="labelUtoiratNev" runat="server" Text='<%#                                        
                                                string.Concat("",                                  
                                                  !string.IsNullOrEmpty(Eval("Csatolva_Id").ToString()) ? "<a href=\"Foszamlist.aspx?Id="+Eval("Csatolva_Id") as string +"\" style=\"text-decoration:underline\">"
                                                    + Eval("MergeFoszam_Csatolt") as string +"</a>" : ""
                                                    , !string.IsNullOrEmpty(Eval("Edok_Utoirat_Azon").ToString()) ? Eval("Edok_Utoirat_Azon").ToString() : "") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Irat helye" SortExpression="MIG_Foszam.UGYHOL" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                    HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelIratHelye" runat="server" Text='<%#Eval("UGYHOL") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Iratt�rba" SortExpression="MIG_Foszam.IRATTARBA" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                    HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="120px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelIrattarbaDate" runat="server" Text='<%#Eval("IRATTARBA") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Meg�rz�si id�" SortExpression="MIG_Foszam.MegorzesiIdo"
										                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
										                            <ItemTemplate>
											                            <asp:Label ID="labelMegorzesiIdo" runat="server" Text='<%#Eval("MegorzesiIdo") %>' />
										                            </ItemTemplate>
									                            </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Alsz�m" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                    HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="50px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelALNO" runat="server" Text='<%#Eval("ALNO") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jelleg" SortExpression="MIG_Foszam.Ugyirat_tipus"
										                            ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
										                            <ItemTemplate>
											                            <asp:Label ID="labelUgyiratTipus" runat="server" Text='<%#Eval("Ugyirat_tipus") %>' />
										                            </ItemTemplate>
									                            </asp:TemplateField>
                                                                <%-- Checkboxok jelzik, hogy egyes m�veletek enged�lyezettek-e --%>
									                            <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
										                            <HeaderTemplate>
											                            <asp:Label ID="headerModosithatosag" runat="server" Text="Nem m�dos�that�" />
										                            </HeaderTemplate>
										                            <ItemTemplate>
											                            <asp:CheckBox ID="cbModosithato" runat="server" />
										                            </ItemTemplate>
									                            </asp:TemplateField>
                                                            </Columns>
                                                            <PagerSettings Visible="False" />
                                                            <EmptyDataTemplate>
                                                                <b style="padding-left: 10px">A lista tartalma �res!</b>
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- �tadhat� -->
                        <ajaxToolkit:TabPanel ID="TabPanelAtadhato" runat="server" TabIndex="1">
                            <HeaderTemplate>
                                <asp:Label ID="labelAtadhato" runat="server" Text="Lev�lt�rba adhat� migr�lt t�telek"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelAtadhato" runat="server" OnLoad="updatePanelAtadhato_Load">
                                    <ContentTemplate>
                                        <!-- scrollozhat�s�g miatt -->
                                        <ajaxToolkit:CollapsiblePanelExtender ID="AtadhatoCPE" runat="server" TargetControlID="panelAtadhato"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                            AutoExpand="false" ExpandedSize="0">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="panelAtadhato" runat="server">
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <asp:GridView ID="gridViewAtadhato" runat="server" OnRowCommand="gridViewAtadhato_RowCommand"
                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                            OnPreRender="gridViewAtadhato_PreRender" AutoGenerateColumns="False" DataKeyNames="Id"
                                                            OnSorting="gridViewAtadhato_Sorting" OnRowDataBound="gridViewAtadhato_RowDataBound">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <div class="DisableWrap">
                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                            &nbsp;
                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                        </div>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText"
                                                                        Enabled='<%# IsNemSzerelt(Eval("Csatolva_Id") as Guid?, Eval("Edok_Utoirat_Id") as Guid?) %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                    <HeaderStyle Width="25px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:CommandField>
                                                                <asp:TemplateField SortExpression="MIG_Foszam.IRJ2000 {0}, MIG_Foszam.UI_IRJ" HeaderText="Iratt&#225;ri Jel">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="70px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelIRJ" runat="server" Text='<%# GetIrattariJel(Eval("UI_YEAR"),Eval("UI_IRJ"),Eval("IRJ2000")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m" SortExpression="MIG_Foszam.EdokSav {0}, MIG_Foszam.UI_NUM {0}, MIG_Foszam.UI_YEAR">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="UI_NAME" SortExpression="MIG_Foszam.UI_NAME" HeaderText="&#220;gyf&#233;l">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="200px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MEMO" SortExpression="MIG_Foszam.MEMO" HeaderText="T&#225;rgy">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="230px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="UI_OT_ID" SortExpression="MIG_Foszam.UI_OT_ID" HeaderText="Azonos�t�">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Eloado_Nev" SortExpression="Eloado_Nev" HeaderText="El�ad�">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="El�irat" SortExpression="Eloirat_Azon_Id">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <ItemTemplate>
                                                                        <%-- Az Eloirat_Azon_Id mez�b�l jelen�tj�k meg az azonos�t�t --%>
                                                                        <asp:Label ID="labelEloiratNev" runat="server" Text='<%# Format_Eloirat_Azon_Id(Eval("Eloirat_Azon_Id").ToString()) %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Ut�irat" SortExpression="Edok_Utoirat_Azon">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <ItemTemplate>
                                                                        <%--Vagy az Edok_Utoirat_Azon mez�t, vagy a MergeFoszam_Csatolt mez�t jelen�tj�k meg--%>
                                                                        <asp:Label ID="labelUtoiratNev" runat="server" Text='<%#                                        
                                                string.Concat("",                                  
                                                  !string.IsNullOrEmpty(Eval("Csatolva_Id").ToString()) ? "<a href=\"Foszamlist.aspx?Id="+Eval("Csatolva_Id") as string +"\" style=\"text-decoration:underline\">"
                                                    + Eval("MergeFoszam_Csatolt") as string +"</a>" : ""
                                                    , !string.IsNullOrEmpty(Eval("Edok_Utoirat_Azon").ToString()) ? Eval("Edok_Utoirat_Azon").ToString() : "") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Irat helye" SortExpression="MIG_Foszam.UGYHOL" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                    HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelIratHelye" runat="server" Text='<%#Eval("UGYHOL") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Iratt�rba" SortExpression="MIG_Foszam.IRATTARBA" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                    HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="120px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelIrattarbaDate" runat="server" Text='<%#Eval("IRATTARBA") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Meg�rz�si id�" SortExpression="MIG_Foszam.MegorzesiIdo"
										                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
										                            <ItemTemplate>
											                            <asp:Label ID="labelMegorzesiIdo" runat="server" Text='<%#Eval("MegorzesiIdo") %>' />
										                            </ItemTemplate>
									                            </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Alsz�m" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                    HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="50px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelALNO" runat="server" Text='<%#Eval("ALNO") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jelleg" SortExpression="MIG_Foszam.Ugyirat_tipus"
										                            ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
										                            <ItemTemplate>
											                            <asp:Label ID="labelUgyiratTipus" runat="server" Text='<%#Eval("Ugyirat_tipus") %>' />
										                            </ItemTemplate>
									                            </asp:TemplateField>
                                                                <%-- Checkboxok jelzik, hogy egyes m�veletek enged�lyezettek-e --%>
									                            <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
										                            <HeaderTemplate>
											                            <asp:Label ID="headerModosithatosag" runat="server" Text="Nem m�dos�that�" />
										                            </HeaderTemplate>
										                            <ItemTemplate>
											                            <asp:CheckBox ID="cbModosithato" runat="server" />
										                            </ItemTemplate>
									                            </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <b style="padding-left: 10px">A lista tartalma �res!</b>
                                                            </EmptyDataTemplate>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- Egy�b szervezetnek �tad�s -->
                        <ajaxToolkit:TabPanel ID="TabPanelEgyebSzervezetnekAtadhato" runat="server" TabIndex="2">
                            <HeaderTemplate>
                                <asp:Label ID="labelEgyebSzervezetnekAtadhato" runat="server" Text="Egy�b szervezetnek �tadhat� migr�lt t�telek"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelEgyebSzervezetnekAtadhato" runat="server" OnLoad="updatePanelEgyebSzervezetnekAtadhato_Load">
                                    <ContentTemplate>
                                        <!-- scrollozhat�s�g miatt -->
                                        <ajaxToolkit:CollapsiblePanelExtender ID="EgyebSzervezetnekAtadhatoCPE" runat="server"
                                            TargetControlID="panelEgyebSzervezetnekAtadhato" CollapsedSize="20" Collapsed="False"
                                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" ExpandedSize="0">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="panelEgyebSzervezetnekAtadhato" runat="server">
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <asp:GridView ID="gridViewEgyebSzervezetnekAtadhato" runat="server" OnRowCommand="gridViewEgyebSzervezetnekAtadhato_RowCommand"
                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                            OnPreRender="gridViewEgyebSzervezetnekAtadhato_PreRender" AutoGenerateColumns="False"
                                                            DataKeyNames="Id" OnSorting="gridViewEgyebSzervezetnekAtadhato_Sorting" OnRowDataBound="gridViewEgyebSzervezetnekAtadhato_RowDataBound">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <div class="DisableWrap">
                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                            &nbsp;
                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                        </div>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText"
                                                                        Enabled='<%# IsNemSzerelt(Eval("Csatolva_Id") as Guid?, Eval("Edok_Utoirat_Id") as Guid?) %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                    <HeaderStyle Width="25px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:CommandField>
                                                                <asp:TemplateField SortExpression="MIG_Foszam.IRJ2000 {0}, MIG_Foszam.UI_IRJ" HeaderText="Iratt&#225;ri Jel">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="70px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelIRJ" runat="server" Text='<%# GetIrattariJel(Eval("UI_YEAR"),Eval("UI_IRJ"),Eval("IRJ2000")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m" SortExpression="MIG_Foszam.EdokSav {0}, MIG_Foszam.UI_NUM {0}, MIG_Foszam.UI_YEAR">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="UI_NAME" SortExpression="MIG_Foszam.UI_NAME" HeaderText="&#220;gyf&#233;l">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="200px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MEMO" SortExpression="MIG_Foszam.MEMO" HeaderText="T&#225;rgy">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="230px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="UI_OT_ID" SortExpression="MIG_Foszam.UI_OT_ID" HeaderText="Azonos�t�">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Eloado_Nev" SortExpression="Eloado_Nev" HeaderText="El�ad�">
                                                                    <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                    <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                    </HeaderStyle>
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="El�irat" SortExpression="Eloirat_Azon_Id">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <ItemTemplate>
                                                                        <%-- Az Eloirat_Azon_Id mez�b�l jelen�tj�k meg az azonos�t�t --%>
                                                                        <asp:Label ID="labelEloiratNev" runat="server" Text='<%# Format_Eloirat_Azon_Id(Eval("Eloirat_Azon_Id").ToString()) %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Ut�irat" SortExpression="Edok_Utoirat_Azon">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <ItemTemplate>
                                                                        <%--Vagy az Edok_Utoirat_Azon mez�t, vagy a MergeFoszam_Csatolt mez�t jelen�tj�k meg--%>
                                                                        <asp:Label ID="labelUtoiratNev" runat="server" Text='<%#                                        
                                                string.Concat("",                                  
                                                  !string.IsNullOrEmpty(Eval("Csatolva_Id").ToString()) ? "<a href=\"Foszamlist.aspx?Id="+Eval("Csatolva_Id") as string +"\" style=\"text-decoration:underline\">"
                                                    + Eval("MergeFoszam_Csatolt") as string +"</a>" : ""
                                                    , !string.IsNullOrEmpty(Eval("Edok_Utoirat_Azon").ToString()) ? Eval("Edok_Utoirat_Azon").ToString() : "") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Irat helye" SortExpression="MIG_Foszam.UGYHOL" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                    HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelIratHelye" runat="server" Text='<%#Eval("UGYHOL") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Iratt�rba" SortExpression="MIG_Foszam.IRATTARBA" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                    HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="120px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelIrattarbaDate" runat="server" Text='<%#Eval("IRATTARBA") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Meg�rz�si id�" SortExpression="MIG_Foszam.MegorzesiIdo"
										                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
										                            <ItemTemplate>
											                            <asp:Label ID="labelMegorzesiIdo" runat="server" Text='<%#Eval("MegorzesiIdo") %>' />
										                            </ItemTemplate>
									                            </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Alsz�m" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                    HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="50px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelALNO" runat="server" Text='<%#Eval("ALNO") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Jelleg" SortExpression="MIG_Foszam.Ugyirat_tipus"
										                            ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
										                            <ItemTemplate>
											                            <asp:Label ID="labelUgyiratTipus" runat="server" Text='<%#Eval("Ugyirat_tipus") %>' />
										                            </ItemTemplate>
									                            </asp:TemplateField>
                                                                <%-- Checkboxok jelzik, hogy egyes m�veletek enged�lyezettek-e --%>
									                            <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
										                            <HeaderTemplate>
											                            <asp:Label ID="headerModosithatosag" runat="server" Text="Nem m�dos�that�" />
										                            </HeaderTemplate>
										                            <ItemTemplate>
											                            <asp:CheckBox ID="cbModosithato" runat="server" />
										                            </ItemTemplate>
									                            </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <b style="padding-left: 10px">A lista tartalma �res!</b>
                                                            </EmptyDataTemplate>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
