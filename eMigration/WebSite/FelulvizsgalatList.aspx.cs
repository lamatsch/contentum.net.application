using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eQuery;
using Contentum.eMigration.Query.BusinessDocuments;

public partial class FelulvizsgalatList : System.Web.UI.Page
{
    UI ui = new UI();

    private const string SessionName_FelulvizsgalatListStartup = "FelulvizsgalatListStartup";

    public const int tabIndexSelejtezheto = 0;
    public const int tabIndexAtadhato = 1;
    public const int tabIndexEgyebSzervezetnekAtadhato = 2;

    string defaultSortExpression = "MIG_Foszam.EdokSav ASC, MIG_Foszam.UI_YEAR DESC, MIG_Foszam.UI_NUM";
    private const string customSessionName = Contentum.eMigration.Utility.Constants.CustomSearchObjectSessionNames.MigFelulvizsgalatSearch;

    private string GetSelectedMasterRecordID()
    {
        if (TabContainerMaster.ActiveTabIndex == tabIndexSelejtezheto)
        {
            return UI.GetGridViewSelectedRecordId(gridViewSelejtezheto);
        }
        else if (TabContainerMaster.ActiveTabIndex == tabIndexAtadhato)
        {
            return UI.GetGridViewSelectedRecordId(gridViewAtadhato);
        }
        else if (TabContainerMaster.ActiveTabIndex == tabIndexEgyebSzervezetnekAtadhato)
        {
            return UI.GetGridViewSelectedRecordId(gridViewEgyebSzervezetnekAtadhato);
        }
        else
        {
            return UI.GetGridViewSelectedRecordId(gridViewSelejtezheto);
        }
    }

    protected string GetIrattariJel(object oYear, object oIrj, object oIrj2000)
    {
        if (oYear == null)
            return String.Empty;

        try
        {
            int year = (int)oYear;
            if (year < 2000)
                return (oIrj ?? String.Empty).ToString();
            else
                return (oIrj2000 ?? String.Empty).ToString();
        }
        catch (Exception e)
        {
            return String.Empty;
        }
    }

    // Segedfv.

    private const string Eloirat_Azon_Id_Delimitter = "***";
    private const string Eloirat_Azon_Id_RecordSeparator = "|||";

    protected string Format_Eloirat_Azon_Id(string Eloirat_Azon_Id)
    {
        if (!string.IsNullOrEmpty(Eloirat_Azon_Id))
        {
            string[] strParts = Eloirat_Azon_Id.Split(new string[] { Eloirat_Azon_Id_RecordSeparator }, StringSplitOptions.RemoveEmptyEntries);
            string res = "";
            foreach (string s in strParts)
            {
                if (!String.IsNullOrEmpty(s))
                {
                    if (res.Length > 0) { res += "<br/>"; }
                    res += "<a href=\"Foszamlist.aspx?Id=" + this.GetIdFrom_Eloirat_Azon_Id(s) + "\" style=\"text-decoration:underline\">"
                        + this.GetAzonositoFrom_Eloirat_Azon_Id(s) + "</a>";
                }
            }
            return res;
        }
        else
        {
            return "";
        }
    }

    protected string GetAzonositoFrom_Eloirat_Azon_Id(string Eloirat_Azon_Id)
    {
        if (!string.IsNullOrEmpty(Eloirat_Azon_Id))
        {
            string[] strParts = Eloirat_Azon_Id.Split(new string[1] { Eloirat_Azon_Id_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            return strParts[0];
        }
        else
        {
            return "";
        }
    }

    protected string GetIdFrom_Eloirat_Azon_Id(string Eloirat_Azon_Id)
    {
        if (!string.IsNullOrEmpty(Eloirat_Azon_Id))
        {
            string[] strParts = Eloirat_Azon_Id.Split(new string[1] { Eloirat_Azon_Id_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            if (strParts.Length > 1)
            {
                return strParts[1];
            }
            else
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }

    private string Startup = String.Empty;

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelulvizsgalatList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        if (Session[SessionName_FelulvizsgalatListStartup] == null)
        {
            switch (Startup)
            {
                case Constants.Startup.SelejtezesSearchForm:
                case Constants.Startup.LeveltarbaAdasSearchForm:
                case Constants.Startup.EgyebSzervezetnekAtadasSearchForm:
                    Session[SessionName_FelulvizsgalatListStartup] = Startup;
                    Response.Clear();
                    Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
                    break;
            }
        }

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        #region WorkAround: az els� l�that� tab legyen akt�v, a t�bbit elt�vol�tjuk
        // WorkAround:
        // automatikusan az els� tab akt�v, de ez nem biztos, hogy l�that�, viszont nem el�g �t�ll�tani az ActiveTabot
        // az els� l�that�ra, mert az eg�sz kont�ner elt�nik... ez�rt tr�kk�z�nk, be�ll�tjuk az els� l�that� Tabot akt�vra,
        // a t�bbit kivessz�k (nem csak elt�ntetj�k!)
        AjaxControlToolkit.TabPanel tabPanelFirstVisible = null;
        foreach (AjaxControlToolkit.TabPanel tp in TabContainerMaster.Tabs)
        {
            if (tp.Visible)
            {
                tabPanelFirstVisible = tp;
                break;
            }
        }

        if (tabPanelFirstVisible != null)
        {
            while (TabContainerMaster.Tabs[0] != tabPanelFirstVisible)
            {
                TabContainerMaster.Tabs.RemoveAt(0);
            }

            TabContainerMaster.ActiveTab = tabPanelFirstVisible;
        }
        #endregion WorkAround: az els� l�that� tab legyen akt�v, a t�bbit elt�vol�tjuk

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = "Fel�lvizsg�lhat� migr�lt t�telek list�ja";
        //Sorrend fontos!!! 1. Custom Session Name 2. SearchObject Type (ez a template miatt kell)
        ListHeader1.CustomSearchObjectSessionName = customSessionName;
        ListHeader1.SearchObjectType = typeof(MIG_FoszamSearch);
        ListHeader1.ModifyVisible = false;
        ListHeader1.UgyiratTerkepVisible = true;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.SendObjectsVisible = false;
        //ListHeader1.SearchVisible = false;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        //TabContainer
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainerMaster);
        //Master TabContainer
        ScriptManager1.RegisterAsyncPostBackControl(TabContainerMaster);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();


        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(TabContainerMaster.ClientID);

        ListHeader1.FelulvizsgalatVisible = true;
        ListHeader1.FelulvizsgalatOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.ITSZModifyVisible = true;

        #region Tab kiv�laszt�s

        if (Session[SessionName_FelulvizsgalatListStartup] != null)
        {
            int selectedTabIndex = -1;
            switch (Session[SessionName_FelulvizsgalatListStartup].ToString())
            {
                case Constants.Startup.SelejtezesSearchForm:
                    selectedTabIndex = tabIndexSelejtezheto;
                    break;
                case Constants.Startup.LeveltarbaAdasSearchForm:
                    selectedTabIndex = tabIndexAtadhato;
                    break;
                case Constants.Startup.EgyebSzervezetnekAtadasSearchForm:
                    selectedTabIndex = tabIndexEgyebSzervezetnekAtadhato;
                    break;
            }

            if (selectedTabIndex > -1)
            {
                foreach (AjaxControlToolkit.TabPanel tab in TabContainerMaster.Tabs)
                {
                    if (tab.TabIndex == selectedTabIndex)
                    {
                        if (tab.Visible)
                        {
                            tab.Enabled = true;
                            TabContainerMaster.ActiveTab = tab;
                        }
                        else
                        {
                            tab.Enabled = false;
                        }
                    }
                }
            }
        }
        #endregion TabKivalasztas

        //selectedRecordId kezel�se
        if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
        {
            ListHeader1.AttachedGridView = gridViewSelejtezheto;

            //T�meges fel�lvizsg�lat
            ListHeader1.FelulvizsgalatOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + gridViewSelejtezheto.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            ListHeader1.FelulvizsgalatOnClientClick += JavaScripts.SetOnClientClick("FelulvizsgalatForm.aspx",
                // TODO: ObjektumId helyett FoszamId
                QueryStringVars.ObjektumId + "=" + "'+getIdsBySelectedCheckboxes('" + gridViewSelejtezheto.ClientID + "','check')+'" +
                "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromSelejtezes
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelSelejtezheto.ClientID, EventArgumentConst.refreshMasterList);

            ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("FelulvizsgalatSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelSelejtezheto.ClientID, EventArgumentConst.refreshMasterList);
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
        {
            ListHeader1.AttachedGridView = gridViewEgyebSzervezetnekAtadhato;

            //T�meges fel�lvizsg�lat
            ListHeader1.FelulvizsgalatOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + gridViewEgyebSzervezetnekAtadhato.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            ListHeader1.FelulvizsgalatOnClientClick += JavaScripts.SetOnClientClick("FelulvizsgalatForm.aspx",
                // TODO: ObjektumId helyett FoszamId
                QueryStringVars.ObjektumId + "=" + "'+getIdsBySelectedCheckboxes('" + gridViewEgyebSzervezetnekAtadhato.ClientID + "','check')+'" +
                "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromEgyebSzervezetnekAtadas
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEgyebSzervezetnekAtadhato.ClientID, EventArgumentConst.refreshMasterList);


            ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("FelulvizsgalatSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEgyebSzervezetnekAtadhato.ClientID, EventArgumentConst.refreshMasterList);
        }
        else // ezt meghagytuk default �gnak
        {
            ListHeader1.AttachedGridView = gridViewAtadhato;

            //T�meges fel�lvizsg�lat
            ListHeader1.FelulvizsgalatOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + gridViewAtadhato.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            ListHeader1.FelulvizsgalatOnClientClick += JavaScripts.SetOnClientClick("FelulvizsgalatForm.aspx",
                // TODO: ObjektumId helyett FoszamId
                QueryStringVars.ObjektumId + "=" + "'+getIdsBySelectedCheckboxes('" + gridViewAtadhato.ClientID + "','check')+'" +
                "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromLeveltarbaAdas
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelAtadhato.ClientID, EventArgumentConst.refreshMasterList);

            ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("FelulvizsgalatSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelAtadhato.ClientID, EventArgumentConst.refreshMasterList);
        }

        /* Breaked Records, email eset�n */
        Search.SetIdsToSearchObject(Page, "Id", new MIG_FoszamSearch(), customSessionName);

        if (Session[SessionName_FelulvizsgalatListStartup] != null)
        {
            switch (Session[SessionName_FelulvizsgalatListStartup].ToString())
            {
                case Constants.Startup.SelejtezesSearchForm:
                case Constants.Startup.LeveltarbaAdasSearchForm:
                case Constants.Startup.EgyebSzervezetnekAtadasSearchForm:
                    ListHeader1.NincsFeltoltes = true;
                    break;
            }
        }
        else if (!IsPostBack)
        {
            ActiveTabRefreshMaster(TabContainerMaster);
        }

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = false;
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "FelulvizsgalatList");
        ListHeader1.ModifyEnabled = false;
        ListHeader1.UgyiratTerkepEnabled = FunctionRights.GetFunkcioJog(Page, "FelulvizsgalatList");
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Foszam" + CommandName.ViewHistory);
        ListHeader1.FelulvizsgalatEnabled = FunctionRights.GetFunkcioJog(Page, "Felulvizsgalat");

        ListHeader1.LockEnabled = false;
        ListHeader1.UnlockEnabled = false;

        UpdatePanel updatePanel = null;

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = "";
            if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
            {
                MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewSelejtezheto);
                RefreshOnClientClicksByMasterListSelectedRow_gridViewSelejtezheto(MasterListSelectedRowId);

                updatePanel = updatePanelSelejtezheto;
            }
            else if (TabContainerMaster.ActiveTab.Equals(TabPanelAtadhato))
            {
                MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewAtadhato);
                RefreshOnClientClicksByMasterListSelectedRow_gridViewAtadhato(MasterListSelectedRowId);

                updatePanel = updatePanelAtadhato;
            }
            else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
            {
                MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewEgyebSzervezetnekAtadhato);
                RefreshOnClientClicksByMasterListSelectedRow_gridViewEgyebSzervezetnekAtadhato(MasterListSelectedRowId);

                updatePanel = updatePanelEgyebSzervezetnekAtadhato;
            }
        }

        if (Session[SessionName_FelulvizsgalatListStartup] != null)
        {
            switch (Session[SessionName_FelulvizsgalatListStartup].ToString())
            {
                case Constants.Startup.SelejtezesSearchForm:
                case Constants.Startup.LeveltarbaAdasSearchForm:
                case Constants.Startup.EgyebSzervezetnekAtadasSearchForm:
                    string script = "OpenNewWindow(); function OpenNewWindow() { ";
                    script +=
                        JavaScripts.SetOnClientClick_DisplayUpdateProgress("FelulvizsgalatSearch.aspx", ""
                            , Defaults.PopupWidth, Defaults.PopupHeight, updatePanel.ClientID, EventArgumentConst.refreshMasterList
                            , CustomUpdateProgress1.UpdateProgress.ClientID);
                    script += "}";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "FelulvizsgalatSearch", script, true);
                    Session.Remove(SessionName_FelulvizsgalatListStartup);
                    break;
            }
        }
    }

    #endregion

    #region Master Tab

    protected void TabContainerMaster_ActiveTabChanged(object sender, EventArgs e)
    {
        ListHeader1.PageIndex = 0;
        ActiveTabRefreshMaster(sender as AjaxControlToolkit.TabContainer);

    }

    private void ActiveTabRefreshMaster(AjaxControlToolkit.TabContainer sender)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case tabIndexSelejtezheto:
                gridViewSelejtezhetoBind();
                break;
            case tabIndexAtadhato:
                gridViewAtadhatoBind();
                break;
            case tabIndexEgyebSzervezetnekAtadhato:
                gridViewEgyebSzervezetnekAtadhatoBind();
                break;
        }
    }

    private GridView GetActiveGridView()
    {
        switch (TabContainerMaster.ActiveTab.TabIndex)
        {
            case tabIndexSelejtezheto:
                return gridViewSelejtezheto;
            case tabIndexAtadhato:
                return gridViewAtadhato;
            case tabIndexEgyebSzervezetnekAtadhato:
                return gridViewEgyebSzervezetnekAtadhato;
            default:
                return gridViewSelejtezheto;
        }
    }

    private UpdatePanel GetActiveUpdatePanel()
    {
        switch (TabContainerMaster.ActiveTab.TabIndex)
        {
            case tabIndexSelejtezheto:
                return updatePanelSelejtezheto;
            case tabIndexAtadhato:
                return updatePanelAtadhato;
            case tabIndexEgyebSzervezetnekAtadhato:
                return updatePanelEgyebSzervezetnekAtadhato;
            default:
                return updatePanelSelejtezheto;
        }
    }

    void GridViewRow_SetModosithatoCheckBox(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        { 
            DataRowView drv = (DataRowView)e.Row.DataItem;
            if (drv != null)
            {
                ExecParam execParam = UI.SetExecParamDefault(Page);
                Foszamok.Statusz statusz = Foszamok.GetAllapotByDataRowView(drv);

                CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");
                if (cbModosithato != null)
                {
                    Boolean javithato = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.EMIGRATION_ADATOK_MODOSITHATOSAGA_ENABLED, false);
                    cbModosithato.Checked = javithato || Foszamok.Modosithato(statusz);

                    ExecParam xpm = UI.SetExecParamDefault(Page);
                    if (statusz.Ugyhol == Constants.MIG_IratHelye.Irattarban && xpm.FelhasznaloSzervezet_Id == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(xpm).Obj_Id)
                    {
                        cbModosithato.Checked = true;

                    }
                }
            }
        }
    }

    #endregion

    #region Master List

    protected bool IsNemSzerelt(Guid? csatolva_Id, Guid? edok_Utoirat_Id)
    {
        return csatolva_Id == null && edok_Utoirat_Id == null;
    }

    #region Selejtezheto

    protected void gridViewSelejtezhetoBind()
    {
        string defaultSortExpression = "MIG_Foszam.IRJ2000 {0}, MIG_Foszam.UI_IRJ";
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewSelejtezheto", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewSelejtezheto", ViewState);

        gridViewSelejtezhetoBind(sortExpression, sortDirection);
    }

    protected void gridViewSelejtezhetoBind(String SortExpression, SortDirection SortDirection)
    {
        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        MIG_FoszamSearch search = (MIG_FoszamSearch)Search.GetSearchObject_CustomSessionName(Page, Search.GetFelulvizsgalatDefaultSearch(), customSessionName);

        search.OrderBy = Search.GetOrderBy("gridViewSelejtezheto", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllSelejtezheto(ExecParam, search);

        UI.GridViewFill(gridViewSelejtezheto, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);


    }

    protected void gridViewSelejtezheto_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        IratHelye.SetGridViewRow(e, "labelIratHelye",Page);
        GridViewRow_SetModosithatoCheckBox(e);
    }

    protected void gridViewSelejtezheto_PreRender(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
        {
            //int prev_PageIndex = gridViewSelejtezheto.PageIndex;

            //gridViewSelejtezheto.PageIndex = ListHeader1.PageIndex;
            //ListHeader1.PageCount = gridViewSelejtezheto.PageCount;

            //if (prev_PageIndex != gridViewSelejtezheto.PageIndex)
            //{
            //    //scroll �llapot�nak t�rl�se
            //    JavaScripts.ResetScroll(Page, SelejtezhetoCPE);
            //    gridViewSelejtezhetoBind();
            //}
            //else
            //{
            UI.GridViewSetScrollable(ListHeader1.Scrollable, SelejtezhetoCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, AtadhatoCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, EgyebSzervezetnekAtadhatoCPE);
            //}


            //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(gridViewSelejtezheto);

            ListHeader1.RefreshPagerLabel();

            ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewSelejtezheto);
        }

    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
        {
            gridViewSelejtezhetoBind();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelAtadhato))
        {
            gridViewAtadhatoBind();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
        {
            gridViewEgyebSzervezetnekAtadhatoBind();
        }
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
        {
            JavaScripts.ResetScroll(Page, SelejtezhetoCPE);
            gridViewSelejtezhetoBind();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelAtadhato))
        {
            JavaScripts.ResetScroll(Page, AtadhatoCPE);
            gridViewAtadhatoBind();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
        {
            JavaScripts.ResetScroll(Page, EgyebSzervezetnekAtadhatoCPE);
            gridViewEgyebSzervezetnekAtadhatoBind();
        }
    }

    protected void gridViewSelejtezheto_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewSelejtezheto, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            gridViewSelejtezheto_SelectRowCommand(id);
        }
    }

    private void gridViewSelejtezheto_SelectRowCommand(string cimId)
    {
    }

    private void RefreshOnClientClicksByMasterListSelectedRow_gridViewSelejtezheto(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromFelulvizsgalat
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelSelejtezheto.ClientID);

            // �gyiratt�rk�p View m�dban
            ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromFelulvizsgalat
                + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelSelejtezheto.ClientID);


            string tableName = "MIG_Foszam";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelSelejtezheto.ClientID);
        }
    }

    protected void updatePanelSelejtezheto_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
                    {
                        gridViewSelejtezhetoBind();
                        gridViewSelejtezheto_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewSelejtezheto));
                    }
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.ITSZModosit:
                {
                    List<string> chked = ui.GetGridViewSelectedRows(GetActiveGridView(), EErrorPanel1, ErrorUpdatePanel);
                    List<string> selectedItems = ui.GetGridViewSelectedAndExecutableRows(GetActiveGridView(), "cbModosithato", EErrorPanel1, ErrorUpdatePanel);   // elvileg nem biztos, hogy a kiv�lasztott sor egyben a bepip�lt sor is
                    if (selectedItems.Count > 0 && chked.Count == selectedItems.Count)
                    {
                        Session["SelectedFoszamIds"] = String.Join(",", selectedItems.ToArray());
                        string js = JavaScripts.SetOnClientClickWithTimeout("ITSZModositasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, GetActiveUpdatePanel().ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIdsModosit", js, true);
                    }
                    else if (chked.Count != selectedItems.Count)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIdsModositError", string.Format("alert('{0}');", Resources.Error.UIUgyiratNemModosithato), true);
                    }
                }
                break;
        }
    }

    protected void gridViewSelejtezheto_Sorting(object sender, GridViewSortEventArgs e)
    {
        gridViewSelejtezhetoBind(e.SortExpression, UI.GetSortToGridView("gridViewSelejtezheto", ViewState, e.SortExpression));
    }

    #endregion

    #region Atadhato

    protected void gridViewAtadhatoBind()
    {
        string defaultSortExpression = "MIG_Foszam.EdokSav ASC, MIG_Foszam.UI_YEAR DESC, MIG_Foszam.UI_NUM"; //"EREC_IraIrattariTetelek.IrattariTetelszam ASC, EREC_IraIrattariTetelek.LetrehozasIdo";
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewAtadhato", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewAtadhato", ViewState);

        gridViewAtadhatoBind(sortExpression, sortDirection);
    }

    protected void gridViewAtadhatoBind(String SortExpression, SortDirection SortDirection)
    {
        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
        

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        MIG_FoszamSearch search = (MIG_FoszamSearch)Search.GetSearchObject_CustomSessionName(Page, Search.GetFelulvizsgalatDefaultSearch(), customSessionName);
        
        search.OrderBy = Search.GetOrderBy("gridViewAtadhato", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllLeveltarbaAdhato(ExecParam, search);

        UI.GridViewFill(gridViewAtadhato, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void gridViewAtadhato_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        IratHelye.SetGridViewRow(e, "labelIratHelye",Page);
        GridViewRow_SetModosithatoCheckBox(e);
    }

    protected void gridViewAtadhato_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewAtadhato, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            gridViewAtadhato_SelectRowCommand(id);
        }
    }

    private void gridViewAtadhato_SelectRowCommand(string cimId)
    {
    }

    private void RefreshOnClientClicksByMasterListSelectedRow_gridViewAtadhato(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                 , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromFelulvizsgalat
                 , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelSelejtezheto.ClientID);

            // �gyiratt�rk�p View m�dban
            ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromFelulvizsgalat
                + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelSelejtezheto.ClientID);

            string tableName = "MIG_Foszam";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelSelejtezheto.ClientID);

        }
    }

    protected void gridViewAtadhato_PreRender(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelAtadhato))
        {
            //int prev_PageIndex = gridViewAtadhato.PageIndex;

            //gridViewAtadhato.PageIndex = ListHeader1.PageIndex;
            //ListHeader1.PageCount = gridViewAtadhato.PageCount;

            //if (prev_PageIndex != gridViewAtadhato.PageIndex)
            //{
            //    //scroll �llapot�nak t�rl�se
            //    JavaScripts.ResetScroll(Page, AtadhatoCPE);
            //    gridViewAtadhatoBind();
            //}
            //else
            //{
            UI.GridViewSetScrollable(ListHeader1.Scrollable, AtadhatoCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, SelejtezhetoCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, EgyebSzervezetnekAtadhatoCPE);
            //}

            //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(gridViewAtadhato);

            ListHeader1.RefreshPagerLabel();

            ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewAtadhato);
        }

    }


    protected void updatePanelAtadhato_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    if (TabContainerMaster.ActiveTab.Equals(TabPanelAtadhato))
                    {
                        gridViewAtadhatoBind();
                        gridViewAtadhato_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewAtadhato));
                    }
                    break;
            }
        }
    }

    protected void gridViewAtadhato_Sorting(object sender, GridViewSortEventArgs e)
    {
        gridViewAtadhatoBind(e.SortExpression, UI.GetSortToGridView("gridViewAtadhato", ViewState, e.SortExpression));
    }

    #endregion

    #region EgyebSzervezetnekAtadhato

    protected void gridViewEgyebSzervezetnekAtadhatoBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewEgyebSzervezetnekAtadhato", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewEgyebSzervezetnekAtadhato", ViewState);

        gridViewEgyebSzervezetnekAtadhatoBind(sortExpression, sortDirection);
    }

    protected void gridViewEgyebSzervezetnekAtadhatoBind(String SortExpression, SortDirection SortDirection)
    {
        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();


        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        MIG_FoszamSearch search = (MIG_FoszamSearch)Search.GetSearchObject_CustomSessionName(Page, Search.GetFelulvizsgalatDefaultSearch(), customSessionName);

        search.OrderBy = Search.GetOrderBy("gridViewEgyebSzervezetnekAtadhato", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllEgyebSzervezetnekAtadhato(ExecParam, search);

        UI.GridViewFill(gridViewEgyebSzervezetnekAtadhato, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void gridViewEgyebSzervezetnekAtadhato_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        IratHelye.SetGridViewRow(e, "labelIratHelye",Page);
        GridViewRow_SetModosithatoCheckBox(e);
    }

    protected void gridViewEgyebSzervezetnekAtadhato_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewEgyebSzervezetnekAtadhato, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            gridViewEgyebSzervezetnekAtadhato_SelectRowCommand(id);
        }
    }

    private void gridViewEgyebSzervezetnekAtadhato_SelectRowCommand(string cimId)
    {
    }

    private void RefreshOnClientClicksByMasterListSelectedRow_gridViewEgyebSzervezetnekAtadhato(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                 , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromFelulvizsgalat
                 , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelEgyebSzervezetnekAtadhato.ClientID);

            // �gyiratt�rk�p View m�dban
            ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromFelulvizsgalat
                + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelEgyebSzervezetnekAtadhato.ClientID);

            string tableName = "MIG_Foszam";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelEgyebSzervezetnekAtadhato.ClientID);

        }
    }

    protected void gridViewEgyebSzervezetnekAtadhato_PreRender(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
        {
            //int prev_PageIndex = gridViewEgyebSzervezetnekAtadhato.PageIndex;

            //gridViewEgyebSzervezetnekAtadhato.PageIndex = ListHeader1.PageIndex;
            //ListHeader1.PageCount = gridViewEgyebSzervezetnekAtadhato.PageCount;

            //if (prev_PageIndex != gridViewEgyebSzervezetnekAtadhato.PageIndex)
            //{
            //    //scroll �llapot�nak t�rl�se
            //    JavaScripts.ResetScroll(Page, EgyebSzervezetnekAtadhatoCPE);
            //    gridViewEgyebSzervezetnekAtadhatoBind();
            //}
            //else
            //{
            UI.GridViewSetScrollable(ListHeader1.Scrollable, SelejtezhetoCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, AtadhatoCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, EgyebSzervezetnekAtadhatoCPE);
            //}

            //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(gridViewEgyebSzervezetnekAtadhato);

            ListHeader1.RefreshPagerLabel();

            ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEgyebSzervezetnekAtadhato);
        }

    }


    protected void updatePanelEgyebSzervezetnekAtadhato_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
                    {
                        gridViewEgyebSzervezetnekAtadhatoBind();
                        gridViewEgyebSzervezetnekAtadhato_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewEgyebSzervezetnekAtadhato));
                    }
                    break;
            }
        }
    }

    protected void gridViewEgyebSzervezetnekAtadhato_Sorting(object sender, GridViewSortEventArgs e)
    {
        gridViewEgyebSzervezetnekAtadhatoBind(e.SortExpression, UI.GetSortToGridView("gridViewEgyebSzervezetnekAtadhato", ViewState, e.SortExpression));
    }

    #endregion

    #endregion
}
