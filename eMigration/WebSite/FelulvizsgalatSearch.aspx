<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FelulvizsgalatSearch.aspx.cs" Inherits="FelulvizsgalatSearch" Title="Untitled Page" %>

<%@ Register Src="eMigrationComponent/IrattariJelTextBox.ascx" TagName="IrattariJelTextBox" TagPrefix="uc" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc14" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                                <asp:Label ID="Label3" runat="server" Text="�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:RequiredNumberBox ID="UI_YEAR_TextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label1" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="UI_SAV_TextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label16" runat="server" Text="F�sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:SzamIntervallum_SearchFormControl id="UI_NUM_TextBox"
                                 runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Iratt�ri Jel:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:IrattariJelTextBox ID="IRJ2000_TextBox" runat="server" SearchMode="true" EvTextBoxControlID="UI_YEAR_TextBox"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelIratHelye" runat="server" Text="Irat helye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="ddlistIratHelye" runat="server" CssClass="mrUrlapInputComboBox">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="CheckBox_SzereltFoszamok" runat="server" Text="Szerelt f�sz�mok megjelen�t�se" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label8" runat="server" Text="Meg�rz�si id�:">
                                </asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_MegorzesiIdo"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelIRATTARBA" runat="server" Text="Iratt�rba helyez�s d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:DatumIntervallum_SearchCalendarControl ID="Irattarba_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                   <uc2:SearchFooter ID="SearchFooter1" runat="server" />                   
                </td>
            </tr>
        </table>
</asp:Content>

