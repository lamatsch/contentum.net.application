using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eMigration.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Query.BusinessDocuments;

public partial class FelulvizsgalatSearch : System.Web.UI.Page
{
    Type _type = typeof(MIG_FoszamSearch);
    private const string customSessionName = Contentum.eMigration.Utility.Constants.CustomSearchObjectSessionNames.MigFelulvizsgalatSearch;

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.FelulvizsgalatSearchHeaderTitle;
        SearchHeader1.CustomTemplateTipusNev = customSessionName;
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick += new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {

            MIG_FoszamSearch searchObject = null;
            if (Search.IsSearchObjectInSession_CustomSessionName(Page, customSessionName))
            {
                searchObject = (MIG_FoszamSearch)Search.GetSearchObject_CustomSessionName(Page, new MIG_FoszamSearch(),customSessionName);
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }

            IratHelye.FillDropDownList(ddlistIratHelye, Page);
            // �res �rt�k hozz�ad�sa a list�hoz
            ddlistIratHelye.Items.Insert(0, new ListItem("[Nincs megadva elem]", ""));
            LoadComponentsFromSearchObject(searchObject);

        }
    }

    private void LoadComponentsFromSearchObject(object searchObject)
    {
        MIG_FoszamSearch MIG_FoszamSearch = null;
        if (searchObject != null) MIG_FoszamSearch = (MIG_FoszamSearch)searchObject;

        if (MIG_FoszamSearch != null)
        {
            UI_SAV_TextBox.Text = MIG_FoszamSearch.EdokSav.Value;
            UI_YEAR_TextBox.Text = MIG_FoszamSearch.UI_YEAR.Value;
            UI_NUM_TextBox.SetComponentFromSearchObjectFields(MIG_FoszamSearch.UI_NUM);
            IRJ2000_TextBox.Text = MIG_FoszamSearch.IRJ2000.Value;
            //UI_NAME_TextBox.Text = MIG_FoszamSearch.UI_NAME.Value;
            //MEMO_TextBox.Text = MIG_FoszamSearch.MEMO.Value;
            //UI_OT_ID_TextBox.Text = MIG_FoszamSearch.UI_OT_ID.Value;

            //textEloado.Text = MIG_FoszamSearch.Edok_Ugyintezo_Nev.Value;

            ddlistIratHelye.SelectedValue = MIG_FoszamSearch.UGYHOL.Value;
            Irattarba_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(MIG_FoszamSearch.IRATTARBA);
            //Skontro_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(MIG_FoszamSearch.SCONTRO);
            //Selejtezes_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(MIG_FoszamSearch.Selejtezes_Datuma);
            DatumIntervallum_MegorzesiIdo.SetComponentFromSearchObjectFields(MIG_FoszamSearch.MegorzesiIdo);

            if (MIG_FoszamSearch.Csatolva_Id.Operator == Query.Operators.isnull && MIG_FoszamSearch.Edok_Utoirat_Id.Operator == Query.Operators.isnull)
            {
                CheckBox_SzereltFoszamok.Checked = false;
            }
            else
            {
                CheckBox_SzereltFoszamok.Checked = true;
            }

            #region ExtendedMIG_AlszamSearch
            //IDNUMTextBox.Text = MIG_FoszamSearch.ExtendedMIG_AlszamSearch.IDNUM.Value;
            //BKNEVTextBox.Text = MIG_FoszamSearch.ExtendedMIG_AlszamSearch.BKNEV.Value;
            //MJTextBox.Text = MIG_FoszamSearch.ExtendedMIG_AlszamSearch.MJ.Value;
            #endregion ExtendedMIG_AlszamSearch

            #region ExtendedMIG_EloadoSearch
            //NAMETextBox.Text = MIG_FoszamSearch.ExtendedMIG_EloadoSearch.NAME.Value;
            #endregion ExtendedMIG_EloadoSearch
        }


    }

    private MIG_FoszamSearch SetSearchObjectFromComponents()
    {
        MIG_FoszamSearch MIG_FoszamSearch = (MIG_FoszamSearch)SearchHeader1.TemplateObject;
        if (MIG_FoszamSearch == null)
        {
            MIG_FoszamSearch = new MIG_FoszamSearch();
        }

        if (!String.IsNullOrEmpty(UI_SAV_TextBox.Text))
        {
            MIG_FoszamSearch.EdokSav.Value = UI_SAV_TextBox.Text;
            MIG_FoszamSearch.EdokSav.Operator = Search.GetOperatorByLikeCharater(UI_SAV_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(UI_YEAR_TextBox.Text))
        {
            MIG_FoszamSearch.UI_YEAR.Value = UI_YEAR_TextBox.Text;
            MIG_FoszamSearch.UI_YEAR.Operator = Search.GetOperatorByLikeCharater(UI_YEAR_TextBox.Text);
        }

        UI_NUM_TextBox.SetSearchObjectFields(MIG_FoszamSearch.UI_NUM);

        if (!String.IsNullOrEmpty(IRJ2000_TextBox.Text))
        {
            MIG_FoszamSearch.IRJ2000.Value = IRJ2000_TextBox.Text;
            MIG_FoszamSearch.IRJ2000.Operator = Search.GetOperatorByLikeCharater(IRJ2000_TextBox.Text);
            MIG_FoszamSearch.IRJ2000.GroupOperator = Query.Operators.or;
            MIG_FoszamSearch.IRJ2000.Group = "01";
            MIG_FoszamSearch.UI_IRJ.Value = IRJ2000_TextBox.Text;
            MIG_FoszamSearch.UI_IRJ.Operator = Search.GetOperatorByLikeCharater(IRJ2000_TextBox.Text);
            MIG_FoszamSearch.UI_IRJ.GroupOperator = Query.Operators.or;
            MIG_FoszamSearch.UI_IRJ.Group = "01";
        }

        //if (!String.IsNullOrEmpty(UI_NAME_TextBox.Text))
        //{
        //    MIG_FoszamSearch.UI_NAME.Value = UI_NAME_TextBox.Text;
        //    MIG_FoszamSearch.UI_NAME.Operator = Search.GetOperatorByLikeCharater(UI_NAME_TextBox.Text);
        //}

        //if (!String.IsNullOrEmpty(MEMO_TextBox.Text))
        //{
        //    MIG_FoszamSearch.MEMO.Value = MEMO_TextBox.Text;
        //    MIG_FoszamSearch.MEMO.Operator = Search.GetOperatorByLikeCharater(MEMO_TextBox.Text);
        //}
        //if (!String.IsNullOrEmpty(UI_OT_ID_TextBox.Text))
        //{
        //    MIG_FoszamSearch.UI_OT_ID.Value = UI_OT_ID_TextBox.Text;
        //    MIG_FoszamSearch.UI_OT_ID.Operator = Search.GetOperatorByLikeCharater(UI_OT_ID_TextBox.Text);
        //}
        //if (!String.IsNullOrEmpty(FTSearch.Text))
        //{
        //    MIG_FoszamSearch.Conc.Value = FTSearch.Text;
        //    MIG_FoszamSearch.Conc.Operator = Contentum.eQuery.Query.Operators.contains;
        //}

        if (!String.IsNullOrEmpty(ddlistIratHelye.SelectedValue))
        {
            MIG_FoszamSearch.UGYHOL.Value = ddlistIratHelye.SelectedValue;
            MIG_FoszamSearch.UGYHOL.Operator = Query.Operators.equals;
        }

        if (CheckBox_SzereltFoszamok.Checked)
        {
            MIG_FoszamSearch.Csatolva_Id.Clear();
            //--------------------------------------------------------
            MIG_FoszamSearch.Edok_Utoirat_Id.Clear();
        }
        else
        {
            MIG_FoszamSearch.Csatolva_Id.AndGroup("111");
            MIG_FoszamSearch.Csatolva_Id.IsNull();
            //--------------------------------------------------------
            MIG_FoszamSearch.Edok_Utoirat_Id.AndGroup("111");
            MIG_FoszamSearch.Edok_Utoirat_Id.IsNull();
        }

        //// Foszam eloado: ket helyen is keresni kell
        //if (!String.IsNullOrEmpty(textEloado.Text))
        //{
        //    MIG_FoszamSearch.Edok_Ugyintezo_Nev.Group = "444";
        //    MIG_FoszamSearch.Edok_Ugyintezo_Nev.GroupOperator = Query.Operators.or;
        //    MIG_FoszamSearch.Edok_Ugyintezo_Nev.Value = textEloado.Text;
        //    MIG_FoszamSearch.Edok_Ugyintezo_Nev.Operator = Search.GetOperatorByLikeCharater(textEloado.Text);

        //    MIG_FoszamSearch.Manual_MIG_Eloado_NAME.Group = "444";
        //    MIG_FoszamSearch.Manual_MIG_Eloado_NAME.GroupOperator = Query.Operators.or;
        //    MIG_FoszamSearch.Manual_MIG_Eloado_NAME.Value = textEloado.Text;
        //    MIG_FoszamSearch.Manual_MIG_Eloado_NAME.Operator = Search.GetOperatorByLikeCharater(textEloado.Text);

        //}

        Irattarba_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(MIG_FoszamSearch.IRATTARBA);
        //Skontro_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(MIG_FoszamSearch.SCONTRO);
        //Selejtezes_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(MIG_FoszamSearch.Selejtezes_Datuma);
        DatumIntervallum_MegorzesiIdo.SetSearchObjectFields(MIG_FoszamSearch.MegorzesiIdo);

        #region ExtendedMIG_AlszamSearch
        //if (!String.IsNullOrEmpty(IDNUMTextBox.Text))
        //{
        //    MIG_FoszamSearch.ExtendedMIG_AlszamSearch.IDNUM.Value = IDNUMTextBox.Text;
        //    MIG_FoszamSearch.ExtendedMIG_AlszamSearch.IDNUM.Operator = Search.GetOperatorByLikeCharater(IDNUMTextBox.Text);
        //}

        //if (!String.IsNullOrEmpty(BKNEVTextBox.Text))
        //{
        //    MIG_FoszamSearch.ExtendedMIG_AlszamSearch.BKNEV.Value = BKNEVTextBox.Text;
        //    MIG_FoszamSearch.ExtendedMIG_AlszamSearch.BKNEV.Operator = Search.GetOperatorByLikeCharater(BKNEVTextBox.Text);
        //}

        //if (!String.IsNullOrEmpty(MJTextBox.Text))
        //{
        //    MIG_FoszamSearch.ExtendedMIG_AlszamSearch.MJ.Value = MJTextBox.Text;
        //    MIG_FoszamSearch.ExtendedMIG_AlszamSearch.MJ.Operator = Search.GetOperatorByLikeCharater(MJTextBox.Text);
        //}
        #endregion ExtendedMIG_AlszamSearch

        #region ExtendedMIG_EloadoSearch
        //if (!String.IsNullOrEmpty(NAMETextBox.Text))
        //{
        //    MIG_FoszamSearch.ExtendedMIG_EloadoSearch.NAME.Value = NAMETextBox.Text;
        //    MIG_FoszamSearch.ExtendedMIG_EloadoSearch.NAME.Operator = Search.GetOperatorByLikeCharater(NAMETextBox.Text);
        //}
        #endregion ExtendedMIG_EloadoSearch

        return MIG_FoszamSearch;
    }

    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            MIG_FoszamSearch searchObject = SetSearchObjectFromComponents();

            MIG_FoszamSearch defaultSearchObject = GetDefaultSearchObject();

            if (Search.IsIdentical(searchObject, defaultSearchObject)
                && Search.IsIdentical(searchObject.ExtendedMIG_AlszamSearch, defaultSearchObject.ExtendedMIG_AlszamSearch)
                && Search.IsIdentical(searchObject.ExtendedMIG_EloadoSearch, defaultSearchObject.ExtendedMIG_EloadoSearch)
                )
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession_CustomSessionName(Page, customSessionName);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject_CustomSessionName(Page, searchObject, customSessionName);
            }

            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }

    }

    private MIG_FoszamSearch GetDefaultSearchObject()
    {
        return Search.GetFelulvizsgalatDefaultSearch();
    }
}
