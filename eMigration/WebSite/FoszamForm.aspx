<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="FoszamForm.aspx.cs" Inherits="FoszamForm" Title="R�gi adatok (f�sz�m) karbantart�sa" EnableEventValidation="false" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="eMigrationComponent/FoszamFormTab.ascx" TagName="FoszamFormTab"
    TagPrefix="tp1" %>
<%@ Register Src="eMigrationComponent/FoszamAlszamTerkepTab.ascx" TagName="FoszamAlszamTerkepTab"
    TagPrefix="tp11" %>
<%--BLG_632--%>
<%@ Register Src="eMigrationComponent/FoszamAlszamEgyebAdatTab.ascx" TagName="FoszamAlszamEgyebAdatTab"
    TagPrefix="tp12" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,MIG_FoszamFormHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <asp:UpdatePanel ID="updatePanelFoszamHiddenFiled" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hiddebFoszamId" runat="server" />
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="FoszamFormPanel" runat="server">
        <ajaxToolkit:TabContainer ID="FoszamTabContainer" runat="server" Width="100%"
            OnActiveTabChanged="FoszamTabContainer_ActiveTabChanged" OnClientActiveTabChanged="ActiveTabChanged"
            ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="TabFoszamPanel" runat="server" TabIndex="0">
                <HeaderTemplate>
                    <asp:Label ID="TabFoszamPanelHeader" runat="server" Text="F�sz�m" />
                </HeaderTemplate>
                <ContentTemplate>
                    <%-- �gy --%>
                    <tp1:FoszamFormTab ID="FoszamFormTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="TabFoszamAlszamTerkepPanel" runat="server" TabIndex="1">
                <HeaderTemplate>
                    <asp:Label ID="Label1" runat="server" Text="F�sz�m-alsz�m t�rk�p" />
                </HeaderTemplate>
                <ContentTemplate>
                    <tp11:FoszamAlszamTerkepTab ID="FoszamAlszamTerkepTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <%--BLG_632--%>
             <ajaxToolkit:TabPanel ID="TabEgyebAdatokPanel" runat="server" TabIndex="2">
                <HeaderTemplate>
                    <asp:Label ID="Label2" runat="server" Text="Egy�b adatok" />
                </HeaderTemplate>
                <ContentTemplate>
                    <tp12:FoszamAlszamEgyebAdatTab ID="FoszamAlszamEgyebAdatTab1" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
    </asp:Panel>
    <uc2:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>
