using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Service;

public partial class FoszamForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";

    private bool FoszamAlszamTerkepMode = false;

    private MIG_Foszam obj_MIG_Foszam = null;

    private string Azonosito
    {
        get
        {
            string azonsito = Request.QueryString.Get(QueryStringVars.RegiAdatAzonosito);
            if (String.IsNullOrEmpty(azonsito))
                return String.Empty;
            return Server.UrlDecode(azonsito);
        }
    }
    private string Id
    {
        get
        {
            string id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
                return String.Empty;
            return id;
        }
    }

    public ObjektumAzonositoTipus ObjektumAzonosito
    {
        get
        {
            if (!String.IsNullOrEmpty(Azonosito))
                return ObjektumAzonositoTipus.Azonosito;
            if (!String.IsNullOrEmpty(Id))
                return ObjektumAzonositoTipus.Id;
            return ObjektumAzonositoTipus.Missing;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        JavaScripts.RegisterActiveTabChangedClientScript(Page, FoszamTabContainer);
        ScriptManager1.RegisterAsyncPostBackControl(FoszamTabContainer);
               
        Command = Request.QueryString.Get(CommandName.Command);

        string mode = Request.QueryString.Get(QueryStringVars.Mode);
        if (mode == Constants.UgyiratTerkep)
        {
            FoszamAlszamTerkepMode = true;
        }

        // FormHeader be�ll�t�sa az FoszamFormTab1 -nak:
        FoszamFormTab1.FormHeader = FormHeader1;

        if (Command != CommandName.View)
        {
            FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
        }

        // New tiltva, csak iktat�ssal j�het l�tre �j �gyirat, ez meg nem az...
        if (Command == CommandName.New)
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

        if (Command == CommandName.Modify)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FoszamModify");
        }

        SetEnabledAllTabsByFunctionRights();

        FoszamFormTab1.hiddenFoszamID = hiddebFoszamId;
        FoszamAlszamTerkepTab1.hiddenFoszamID = hiddebFoszamId;

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            if (ObjektumAzonosito == ObjektumAzonositoTipus.Missing)
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                FoszamFormPanel.Visible = false;
            }
            else
            {
                // Rekord lek�r�se jogosults�gellen�rz�ssel egy�tt

                if (!IsPostBack)
                {
                    obj_MIG_Foszam = CheckRights();        
                }

                #region Felesleges tabf�lek elt�ntet�se

                if (FoszamAlszamTerkepMode == true)
                {
                    // Csak az �gyiratt�rk�p-panelt jelen�tj�k meg?
                    SetAllTabsInvisible();

                    FoszamAlszamTerkepTab1.Visible = true;
                    TabFoszamAlszamTerkepPanel.Enabled = true;
                }

                #endregion

            }
        }

        FoszamFormTab1.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);
        FoszamAlszamTerkepTab1.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);

        FoszamFormTab1.Active = true;
        FoszamFormTab1.ParentForm = Constants.ParentForms.Foszam;

        FoszamAlszamTerkepTab1.Active = false;
        FoszamAlszamTerkepTab1.ParentForm = Constants.ParentForms.Foszam;

        // BLG_632
        FoszamAlszamEgyebAdatTab1.Active = false;
        FoszamAlszamEgyebAdatTab1.ParentForm = Constants.ParentForms.Foszam;

        if (FoszamAlszamTerkepMode == true)
        {
            FoszamFormTab1.Active = false;

            FoszamAlszamTerkepTab1.Active = true;
        }
    }

   
    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        if (!IsPostBack)
        {
            string selectedTab = Request.QueryString.Get(QueryStringVars.SelectedTab);
            // Default panel beallitasa:
            if (FoszamAlszamTerkepMode == true || selectedTab == Constants.Tabs.UgyiratTerkepTab)
            {
                selectedTab = "FoszamAlszamTerkep";
            }
            if (!String.IsNullOrEmpty(selectedTab))
            {
                bool selectedTabFound = false;
                foreach(AjaxControlToolkit.TabPanel tab in FoszamTabContainer.Tabs)
                {
                    if (tab.ID.ToLower().Contains(selectedTab.ToLower()))
                    {
                        selectedTabFound = true;
                        FoszamTabContainer.ActiveTab = tab;
                        break;
                    }
                }

                if (!selectedTabFound)
                {
                    FoszamTabContainer.ActiveTab = TabFoszamPanel;
                }
            }
            else
            {
                FoszamTabContainer.ActiveTab = TabFoszamPanel;
            }

            ActiveTabRefresh(FoszamTabContainer);
        }

        // Atverjuk a formfooter-t, mert ugy kell mukodni-e, mint ha nezet lenne, csak a Bezar gombra van szukseg
        FormFooter1.Command = CommandName.View;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }


    /// <summary>
    /// Ha v�ltozott valamilyen tulajdons�ga az �gyiratnak, jogosults�gellen�rz�s
    /// </summary>    
    void CallBack_OnChangedObjectProperties(object sender, EventArgs e)
    {
        CheckRights();
    }


    private MIG_Foszam CheckRights()
    {
        // TODO: jogosults�gellen�rz�s?
        MIG_Foszam mig_Foszam = null;
        Result result = new Result();

        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        if (ObjektumAzonosito == ObjektumAzonositoTipus.Id)
        {
            execParam.Record_Id = Id;
            result = service.Get(execParam);
        }
        else
        {
            result = service.GetByAzonosito(execParam, Azonosito);
        }
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            FoszamFormPanel.Visible = false;
            return null;
        }
        else
        {
            mig_Foszam = (MIG_Foszam)result.Record;
            if (mig_Foszam == null)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, ResultError.CreateNewResultWithErrorCode(50101));
                FoszamFormPanel.Visible = false;
                return null;
            }
        }

        if (mig_Foszam != null)
        {
            //Id elmentese a ViewState-be
            if (ObjektumAzonosito == ObjektumAzonositoTipus.Azonosito)
            {
                hiddebFoszamId.Value = mig_Foszam.Id;
            }
        }

        return mig_Foszam;
    }



    #region Forms Tab
                   
    protected void FoszamTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
        FoszamFormTab1.Active = false;
        FoszamAlszamTerkepTab1.Active = false;

        if (FoszamAlszamTerkepMode == true)
        {
            // Csak az �gyiratt�rk�p l�tsz�dik:
            FoszamAlszamTerkepTab1.Active = true;
            FoszamAlszamTerkepTab1.ReLoadTab();
            return;
        }
        else
        {

            if (FoszamTabContainer.ActiveTab.Equals(TabFoszamPanel))
            {
                FoszamFormTab1.Active = true;

                // a m�r esetleg lek�rt �gyirat objektum �tad�sa
                FoszamFormTab1.obj_MIG_Foszam = this.obj_MIG_Foszam;

                FoszamFormTab1.ReLoadTab();
            }
            else if (FoszamTabContainer.ActiveTab.Equals(TabFoszamAlszamTerkepPanel))
            {
                FoszamAlszamTerkepTab1.Active = true;
                FoszamAlszamTerkepTab1.ReLoadTab();
            }
            // BLG_632
            else if (FoszamTabContainer.ActiveTab.Equals(TabEgyebAdatokPanel))
            {
                FoszamAlszamEgyebAdatTab1.Active = true;
                FoszamAlszamEgyebAdatTab1.ReLoadTab();
            }

        }
    }

    #endregion


    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    //// (Itt nem kell)
    //private void SetEnabledAllTabs(Boolean value)
    //{

    //}

    private void SetAllTabsInvisible()
    {
        FoszamFormTab1.Visible = false;
        TabFoszamPanel.Enabled = false;

        FoszamAlszamTerkepTab1.Visible = false;
        TabFoszamAlszamTerkepPanel.Enabled = false;

        // BLG_632
        FoszamAlszamEgyebAdatTab1.Visible = false;
        TabEgyebAdatokPanel.Enabled = false;

    }


    private void SetEnabledAllTabsByFunctionRights()
    {
        // a default panelt nem kell ellenorizni, azt majd ott helyben
        TabFoszamPanel.Enabled = true;

        // TODO: Funkci�jogosults�gok!!
        TabFoszamAlszamTerkepPanel.Enabled = true;

        // BLG_632
        TabEgyebAdatokPanel.Enabled = true;

    }

}
