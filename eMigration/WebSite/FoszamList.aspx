<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="FoszamList.aspx.cs" Inherits="MIG_FoszamList" Title="Migr�ci� F�sz�m" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<%@ Register Src="~/eMigrationComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="MIG_FoszamCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="MIG_FoszamUpdatePanel" runat="server" OnLoad="MIG_FoszamUpdatePanel_Load">
                    <ContentTemplate>
                        <asp:HiddenField ID="HiddenField_SkontroVege" runat="server" />
                        <ajaxToolkit:CollapsiblePanelExtender ID="MIG_FoszamCPE" runat="server" TargetControlID="Panel1"
                            ExpandedText="F�sz�mok list�ja" CollapsedText="F�sz�mok list�ja" ImageControlID="MIG_FoszamCPEButton"
                            ExpandedImage="images/hu/Grid/minus.gif" CollapsedImage="images/hu/Grid/plus.gif"
                            AutoExpand="false" AutoCollapse="false" ExpandDirection="Vertical" CollapseControlID="MIG_FoszamCPEButton"
                            ExpandControlID="MIG_FoszamCPEButton" Collapsed="False" CollapsedSize="20" ExpandedSize="0">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                            <asp:HiddenField ID="GridViewSelectedId" runat="server" />
                            <asp:GridView ID="MIG_FoszamGridView" runat="server" OnSelectedIndexChanged="MIG_FoszamGridView_SelectedIndexChanged"
                                OnRowCommand="MIG_FoszamGridView_RowCommand" CellPadding="0" BorderWidth="1px"
                                AllowPaging="True" OnRowDataBound="MIG_FoszamGridView_RowDataBound" OnPreRender="MIG_FoszamGridView_PreRender"
                                OnSorting="MIG_FoszamGridView_Sorting" PagerSettings-Visible="false" AllowSorting="True"
                                AutoGenerateColumns="False" DataKeyNames="Id, Csatolmany_Count">
                                <PagerSettings Visible="False"></PagerSettings>
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <!-- OnCommand="GridViewSelectAllCheckBox" -->
                                            <span style="white-space: nowrap">
                                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                    AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                &nbsp;&nbsp;
                                            <!-- OnCommand="GridViewDeselectAllCheckBox" -->
                                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                    AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                            </span>
                                        </HeaderTemplate>
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle"></ItemStyle>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField SelectImageUrl="~/images/hu/Grid/3Drafts.gif" ButtonType="Image"
                                        ShowSelectButton="True">
                                        <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage"></HeaderStyle>
                                        <ItemStyle CssClass="GridViewSelectRowImage" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:CommandField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                        HeaderStyle-Width="25px" HeaderText="<%$Forditas:BoundField_Csatolmany|Csny.%>">
                                        <ItemTemplate>
                                            <asp:Image ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Id" Visible="False" SortExpression="Id">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="150px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UI_SAV" SortExpression="MIG_Foszam.EdokSav" HeaderText="<%$Forditas:BoundField_EdokSav|Iktat�k�nyv%>">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="50px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UI_YEAR" SortExpression="MIG_Foszam.UI_YEAR" HeaderText="<%$Forditas:BoundField_UI_YEAR|�v%>">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="50px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UI_NUM" SortExpression="MIG_Foszam.UI_NUM" HeaderText="<%$Forditas:BoundField_UI_NUM|F�sz�m%>">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="65px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField SortExpression="MIG_Foszam.IRJ2000 {0}, MIG_Foszam.UI_IRJ" HeaderText="<%$Forditas:BoundField_UI_IRJ|Iratt�ri jel%>">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="70px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="labelIRJ" runat="server" Text='<%# GetIrattariJel(Eval("UI_YEAR"),Eval("UI_IRJ"),Eval("IRJ2000")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UI_NAME" SortExpression="MIG_Foszam.UI_NAME" HeaderText="<%$Forditas:BoundField_UI_NAME|�gyf�l%>">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="200px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MEMO" SortExpression="MIG_Foszam.MEMO" HeaderText="<%$Forditas:BoundField_MEMO|T�rgy%>">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="230px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UI_OT_ID" SortExpression="MIG_Foszam.UI_OT_ID" HeaderText="<%$Forditas:BoundField_UI_OT_ID|Azonos�t�%>">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField Visible="false" HeaderText="Azonos�t�" SortExpression="MIG_Foszam.Foszam_Merge"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelFoszam_Merge" runat="server" Text='<%#Eval("Foszam_Merge") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Eloado_Nev" SortExpression="Eloado_Nev" HeaderText="<%$Forditas:BoundField_Eloado_Nev|�gyint�z�%>">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_Eloirat|El�irat%>" SortExpression="Eloirat_Azon_Id">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <ItemTemplate>
                                            <%-- Az Eloirat_Azon_Id mez�b�l jelen�tj�k meg az azonos�t�t --%>
                                            <asp:Label ID="labelEloiratNev" runat="server" Text='<%# Format_Eloirat_Azon_Id(Eval("Eloirat_Azon_Id").ToString()) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_Utoirat|Ut�irat%>" SortExpression="Edok_Utoirat_Azon">
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <ItemTemplate>
                                            <%--Vagy az Edok_Utoirat_Azon mez�t, vagy a MergeFoszam_Csatolt mez�t jelen�tj�k meg--%>
                                            <asp:Label ID="labelUtoiratNev" runat="server" Text='<%#GetUtoiratLink(Eval("Csatolva_Id").ToString(),  Eval("MergeFoszam_Csatolt").ToString(),Eval("Edok_Utoirat_Id").ToString(),Eval("Edok_Utoirat_Azon").ToString()) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="Edok_Utoirat_Azon" SortExpression="Edok_Utoirat_Azon" HeaderText="Ut�irat">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>--%>
                                    <%-- /�j megjelen�tett mez� --%>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_UGYHOL|Irat helye%>" SortExpression="MIG_Foszam.UGYHOL"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelIratHelye" runat="server" Text='<%#Eval("UGYHOL") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_Ugyirat_tipus|Irat t�pusa%>" SortExpression="MIG_Foszam.Ugyirat_tipus"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelUgyiratTipus" runat="server" Text='<%#Eval("Ugyirat_tipus") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_Hatarido|Hat�rid�%>" SortExpression="MIG_Foszam.Hatarido"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelHataridoDate" runat="server" Text='<%#Eval("HATARIDO") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_IRATTARBA|Iratt�rba%>" SortExpression="MIG_Foszam.IRATTARBA"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelIrattarbaDate" runat="server" Text='<%#Eval("IRATTARBA") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_SCONTRO|Skontr� v�ge%>" SortExpression="MIG_Foszam.SCONTRO"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelSkontroVegeDate" runat="server" Text='<%#Eval("SCONTRO") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_Feladat|Feladat%>" SortExpression="MIG_Foszam.Feladat"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelFeladat" runat="server" Text='<%#Eval("Feladat") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_Szervezet|Szervezet%>" SortExpression="MIG_Foszam.Szervezet"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelSzervezet" runat="server" Text='<%#Eval("Szervezet") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_Kikero|Kik�r�%>" SortExpression="MIG_Foszam.IrattarbolKikeroNev"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelIrattarbolKikeroNev" runat="server" Text='<%#Eval("IrattarbolKikeroNev") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_IrattarbolKikeres_Datuma|Kik�r�s d�tuma%>" SortExpression="MIG_Foszam.IrattarbolKikeres_Datuma"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelIrattarbolKikeres_Datuma" runat="server" Text='<%#Eval("IrattarbolKikeres_Datuma") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_MegorzesiIdo|Meg�rz�si id�%>" SortExpression="MIG_Foszam.MegorzesiIdo"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelMegorzesiIdo" runat="server" Text='<%#Eval("MegorzesiIdo") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Forditas:BoundField_ALNO|Alsz�m%>"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelALNO" runat="server" Text='<%#Eval("ALNO") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--BLG_236--%>
                                    <asp:BoundField DataField="IrattariHely" HeaderText="<%$Forditas:BoundField_IrattariHely|Iratt�ri hely%>" SortExpression="IrattariHely">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <%-- Checkboxok jelzik, hogy egyes m�veletek enged�lyezettek-e --%>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
                                        <HeaderTemplate>
                                            <asp:Label ID="headerModosithatosag" runat="server" Text="Nem m�dos�that�" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbSkontrobaHelyezheto" runat="server" />
                                            <asp:CheckBox ID="cbSkontrobolKiveheto" runat="server" />
                                            <asp:CheckBox ID="cbIrattarbaKuldheto" runat="server" />
                                            <asp:CheckBox ID="cbIrattarbaVeheto" runat="server" />
                                            <asp:CheckBox ID="cbIrattarbolKikerheto" runat="server" />
                                            <asp:CheckBox ID="cbKiadhatoOsztalyra" runat="server" />
                                            <asp:CheckBox ID="cbModosithato" runat="server" />
                                            <asp:CheckBox ID="cbSelejtezesreKijelolheto" runat="server" />
                                            <asp:CheckBox ID="cbSelejtezesreKijelolesVisszavonhato" runat="server" />
                                            <asp:CheckBox ID="cbSelejtezheto" runat="server" />
                                            <asp:CheckBox ID="cbLomtarbaHelyezheto" runat="server" />
                                            <asp:CheckBox ID="cbSzereleheto" runat="server" />
                                            <asp:CheckBox ID="cbSzerelesVisszavonhato" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- /Checkboxok jelzik, hogy egyes m�veletek enged�lyezettek-e --%>
                                </Columns>
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True"></RowStyle>
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle CssClass="GridViewHeaderStyle"></HeaderStyle>
                            </asp:GridView>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2"></td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel8" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                                    <ajaxToolkit:TabPanel ID="AlszamokTabPanel" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelHozzarendeltAlszamaok" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text="Hozz�rendelt alsz�mok"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="AlszamokUpdatePanel" runat="server" OnLoad="AlszamokUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="AlszamokPanel" runat="server" Width="100%" Visible="false">
                                                        <uc1:SubListHeader ID="AlszamokSubListHeader" runat="server"></uc1:SubListHeader>
                                                        <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="vertical-align: top; text-align: left" class="GridViewHeaderBackGroundStyle">
                                                                        <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponent" />
                                                                        <ajaxToolkit:CollapsiblePanelExtender ID="AlszamokCPE" runat="server" TargetControlID="Panel3"
                                                                            AutoExpand="false" AutoCollapse="false" ExpandDirection="Vertical" Collapsed="False"
                                                                            CollapsedSize="20" ExpandedSize="0">
                                                                        </ajaxToolkit:CollapsiblePanelExtender>
                                                                        <asp:Panel ID="Panel3" runat="server">
                                                                            <asp:GridView ID="AlszamokGridView" runat="server" OnRowCommand="AlszamokGridView_RowCommand"
                                                                                CellPadding="0" BorderWidth="1px" AllowPaging="True" OnPreRender="AlszamokGridView_PreRender"
                                                                                OnSorting="AlszamokGridView_Sorting" PagerSettings-Visible="false" AllowSorting="True"
                                                                                OnRowDataBound="AlszamokGridView_RowDataBound" AutoGenerateColumns="False" DataKeyNames="Id, Csatolmany_Count, MIG_Dokumentum_Id">
                                                                                <PagerSettings Visible="False"></PagerSettings>
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <HeaderTemplate>
                                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                            &nbsp;&nbsp;
                                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle"></ItemStyle>
                                                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"></HeaderStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:CommandField SelectImageUrl="~/images/hu/Grid/3Drafts.gif" SelectText="<%$ Resources:List,AlternateText_RowSelectButton %>"
                                                                                        ButtonType="Image" ShowSelectButton="True">
                                                                                        <HeaderStyle Width="25px"></HeaderStyle>
                                                                                        <ItemStyle BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                    </asp:CommandField>
                                                                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                                        HeaderStyle-Width="25px" HeaderText="<%$Forditas:BoundField_Csatolmany|Csny.%>">
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                                                                runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="ALNO" SortExpression="ALNO" HeaderText="<%$Forditas:BoundField_ALNO|Alsz�m%>">
                                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                                        <HeaderStyle Width="75px" HorizontalAlign="Left"></HeaderStyle>
                                                                                    </asp:BoundField>
                                                                                    <%-- �j megjelen�tett mez� --%>
                                                                                    <asp:BoundField DataField="IDNUM" SortExpression="IDNUM" HeaderText="<%$Forditas:BoundField_IDNUM|Hiv.sz�m%>">
                                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                                        <HeaderStyle Width="150px" HorizontalAlign="Left"></HeaderStyle>
                                                                                    </asp:BoundField>
                                                                                    <%-- /�j megjelen�tett mez� --%>
                                                                                    <asp:BoundField DataField="BKNEV" SortExpression="BKNEV" HeaderText="<%$Forditas:BoundField_BKNEV|Bek�ld�%>">
                                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                                        <HeaderStyle Width="280px" HorizontalAlign="Left"></HeaderStyle>
                                                                                    </asp:BoundField>
                                                                                    <%-- �j megjelen�tett mez� --%>
                                                                                    <asp:BoundField DataField="EloadoNev" SortExpression="EloadoNev" HeaderText="<%$Forditas:BoundField_EloadoNev|El�ad�%>">
                                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                                        <HeaderStyle Width="150px" HorizontalAlign="Left"></HeaderStyle>
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="MJ" SortExpression="MJ" HeaderText="<%$Forditas:BoundField_MJ|Megjegyz�s%>">
                                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                                        <HeaderStyle Width="150px" HorizontalAlign="Left"></HeaderStyle>
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="UGYHOL" SortExpression="UGYHOL" HeaderText="<%$Forditas:BoundField_UGYHOL|Irat helye%>">
                                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                                        <HeaderStyle Width="100px" HorizontalAlign="Left"></HeaderStyle>
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="IRATTARBA" DataFormatString="{0:d}" SortExpression="IRATTARBA" HeaderText="<%$Forditas:BoundField_IRATTARBA|Iratt�rba ad�s%>">
                                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                                        <HeaderStyle Width="100px" HorizontalAlign="Left"></HeaderStyle>
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="Adathordozo_tipusa" SortExpression="Adathordozo_tipusa" HeaderText="<%$Forditas:BoundField_Adathordozo_tipusa|Adath. t�pus%>">
                                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                                        <HeaderStyle Width="100px" HorizontalAlign="Left"></HeaderStyle>
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="Irattipus" SortExpression="Irattipus" HeaderText="<%$Forditas:BoundField_Irattipus|Iratt�pus%>">
                                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                                        <HeaderStyle Width="100px" HorizontalAlign="Left"></HeaderStyle>
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="Feladat" SortExpression="Feladat" HeaderText="<%$Forditas:BoundField_Feladat|Csatolt irat%>">
                                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                                        <HeaderStyle Width="100px" HorizontalAlign="Left"></HeaderStyle>
                                                                                    </asp:BoundField>
                                                                                </Columns>
                                                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True"></RowStyle>
                                                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle"></SelectedRowStyle>
                                                                                <HeaderStyle CssClass="GridViewHeaderStyle"></HeaderStyle>
                                                                            </asp:GridView>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="TobbesTabPanel" runat="server" TabIndex="1">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelTobbes" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="T�bbes"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="TobbesUpdatePanel" runat="server" OnLoad="TobbesUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="TobbesPanel" runat="server" Visible="false" Width="100%">
                                                        <uc1:SubListHeader ID="TobbesSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="TobbesCPE" runat="server" TargetControlID="panel13"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="panel13" runat="server">
                                                                        <asp:GridView ID="TobbesGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                            AllowSorting="True" AutoGenerateColumns="false" OnSorting="TobbesGridView_Sorting"
                                                                            OnPreRender="TobbesGridView_PreRender" OnRowCommand="TobbesGridView_RowCommand"
                                                                            DataKeyNames="Id">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="NEV" HeaderText="N�v" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="NEV" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="IRSZ" HeaderText="Ir�ny�t�sz�m" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="IRSZ" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="VAROSNEV" HeaderText="V�ros" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="VAROSNEV" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="UTCA" HeaderText="Utca" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="UTCA" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" />
                                                                                <asp:BoundField DataField="HSZ" HeaderText="H�zsz�m" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="HSZ" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <uc3:LovListFooter ID="LovListFooter1" runat="server" />
            </td>
        </tr>
    </table>
    <br />

    <script language="javascript" type="text/javascript">
        function openUgyiratPotloLap(ids) {
            console.log(ids);
            var frame = document.getElementById('iframeUgyiratPotlo');
            frame.src = "Mig_UgyiratPotloSSRS.aspx?UgyiratId=" + ids.toString();
        }
    </script>

    <iframe id="iframeUgyiratPotlo" style="width: 0px; height: 0px;"></iframe>
</asp:Content>
