﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Web.Script.Serialization;
using System.Linq;

public partial class MIG_FoszamList : Contentum.eUtility.UI.PageBase //System.Web.UI.Page
{
    UI ui = new UI();
    //private bool disable_refreshLovList = false;

    readonly string jsNemModosithato = "alert('" + Resources.List.UI_NotModifiable + "');return false;";

    protected bool getModosithatosag(GridView gridView)
    {
        return getModosithatosag(gridView, -1);
    }

    protected bool getModosithatosag(GridView gridView, int rowIndex)
    {
        int index;
        if (rowIndex == -1)
        {
            index = gridView.SelectedIndex;
        }
        else
        {
            index = rowIndex;
        }
        GridViewRow selectedRow = gridView.Rows[index];
        return getModosithatosag(selectedRow);
    }

    protected bool getModosithatosag(GridViewRow selectedRow)
    {
        bool modosithato = false;
        if (selectedRow != null)
        {
            CheckBox cbModosithato = (CheckBox)selectedRow.FindControl("cbModosithato");
            if (cbModosithato != null)
            {
                modosithato = cbModosithato.Checked;
            }
        }
        return modosithato;
    }

    private string Startup = String.Empty;

    public bool IsNMHHColumnsVisible
    {
        get { return "NMHH".Equals(FelhasznaloProfil.OrgKod(Page), StringComparison.InvariantCultureIgnoreCase); }
    }

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        // TODO: jogok kialakítása az iktatókönyvekhez (listázás)
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FoszamList");
        Authentication.CheckLogin(Page);

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        if (Session["FoszamListStartup"] == null)
        {
            if (Startup == Constants.Startup.SearchForm)
            {
                Session["FoszamListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
            }
        }

        registerJavascripts();

        LovListFooter1.ButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
        LovListFooter1.Visible = false;

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        AlszamokSubListHeader.RowCount_Changed += new EventHandler(AlszamokSubListHeader_RowCount_Changed);
        AlszamokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(AlszamokSubListHeader_ErvenyessegFilter_Changed);
        TobbesSubListHeader.RowCount_Changed += new EventHandler(TobbesSubListHeader_RowCount_Changed);
        TobbesSubListHeader.ErvenyessegFilter_Changed += new EventHandler(TobbesSubListHeader_ErvenyessegFilter_Changed);


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(MIG_FoszamGridView.ClientID);

        ListHeader1.HeaderLabel = Resources.List.MIG_FoszamListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(MIG_FoszamSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;
        ListHeader1.UgyiratTerkepVisible = true;

        ListHeader1.SkontrobaHelyezVisible = true;
        ListHeader1.SkontrobolKivetelVisible = true;
        ListHeader1.IrattarozasraVisible = true;
        ListHeader1.KiadasOsztalyraVisible = true;
        ListHeader1.SelejtezesreKijelolesVisible = true;
        ListHeader1.SelejtezesreKijelolesVisszavonasaVisible = true;
        ListHeader1.SelejtezesVisible = true;
        ListHeader1.LomtarbaHelyezesVisible = true;
        ListHeader1.UgyiratIrattarAtvetel.Visible = true;
        ListHeader1.Kolcsonzes.Visible = true;
        ListHeader1.SzerelesVisible = true;
        ListHeader1.ITSZModifyVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        #region IRAT POT NYOMTATAS
        //if (FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa"))
        ListHeader1.UgyiratpotlonyomtatasVisible = true;

        // ListHeader1.UgyiratpotlonyomtatasOnClientClick = "var count = getSelectedCheckBoxesCount('"
        //		+ MIG_FoszamGridView.ClientID + "','check'); "
        //		+ " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} ";

        ListHeader1.UgyiratpotlonyomtatasOnClientClick = "javascript:window.open('Mig_UgyiratPotloSSRS.aspx?UgyiratId=" + "'+getIdsBySelectedCheckboxes('" + MIG_FoszamGridView.ClientID + "','check')+'" + "')";
        #endregion

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("FoszamSearch.aspx", ""
             , Defaults.PopupWidth, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.SzerelesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(MIG_FoszamGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(MIG_FoszamGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(MIG_FoszamGridView.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("FoszamListPrintForm.aspx");

        string column_v = "";
        for (int i = 0; i < MIG_FoszamGridView.Columns.Count; i++)
        {
            if (MIG_FoszamGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('FoszamSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(MIG_FoszamGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick(eRecord.ERecordWebsiteUrl + "FelhasznalokMultiSelect.aspx",
             QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
          , Defaults.PopupWidth, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, "", true);

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = MIG_FoszamGridView;

        AlszamokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        AlszamokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        //LZS - BLG_8097
        AlszamokSubListHeader.AiAtadasEnabled = AlszamokSubListHeader.AiAtadasVisible = false;


        //// ellenõrzés módosíthatóságra
        //AlszamokSubListHeader.IrattarozasraOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
        //    AlszamokGridView.ClientID
        //    , "check"
        //    , "cbIrattarbaTesz"
        //    , false
        //    , Resources.Question.UIConfirmHeader_IrattarbaTeszTomeges
        //    , Resources.Question.UI_IrattarbaTeszTomeges_Question);
        //AlszamokSubListHeader.KiadasOsztalyraOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
        //    AlszamokGridView.ClientID
        //    , "check"
        //    , "cbKiadasOsztalyra"
        //    , false
        //    , Resources.Question.UIConfirmHeader_KiadasOsztalyraTomeges
        //    , Resources.Question.UI_KiadasOsztalyraTomeges_Question);
        //AlszamokSubListHeader.SkontrobaHelyezOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
        //    AlszamokGridView.ClientID
        //    , "check"
        //    , "cbSkontrobaHelyez"
        //    , false
        //    , Resources.Question.UIConfirmHeader_SkontrobaHelyezTomeges
        //    , Resources.Question.UI_SkontrobaHelyezTomeges_Question);
        //AlszamokSubListHeader.SkontrobolKivetelOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
        //    AlszamokGridView.ClientID
        //    , "check"
        //    , "cbSkontrobolKivesz"
        //    , false
        //    , Resources.Question.UIConfirmHeader_SkontrobolKiveszTomeges
        //    , Resources.Question.UI_SkontrobolKiveszTomeges_Question);

        // ellenõrzés módosíthatóságra (Foszam)
        ListHeader1.IrattarozasraOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
             MIG_FoszamGridView.ClientID
             , "check"
             , "cbIrattarbaKuldheto"
             , true
             , Resources.Question.UIConfirmHeader_IrattarbaTeszTomeges
             , Resources.Question.UI_IrattarbaTeszTomeges_Question);
        ListHeader1.KiadasOsztalyraOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
             MIG_FoszamGridView.ClientID
             , "check"
             , "cbKiadhatoOsztalyra"
             , true
             , Resources.Question.UIConfirmHeader_KiadasOsztalyraTomeges
             , Resources.Question.UI_KiadasOsztalyraTomeges_Question);
        ListHeader1.SkontrobaHelyezOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
             MIG_FoszamGridView.ClientID
             , "check"
             , "cbSkontrobaHelyezheto"
             , true
             , Resources.Question.UIConfirmHeader_SkontrobaHelyezTomeges
             , Resources.Question.UI_SkontrobaHelyezTomeges_Question);
        ListHeader1.SkontrobaHelyezOnClientClick += JavaScripts.SetOnClientClick("SkontroVegeForm.aspx"
             , "Command=" + CommandName.Modify + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField_SkontroVege.ClientID
             , Defaults.PopupWidth, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refresh);
        ListHeader1.SkontrobolKivetelOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
             MIG_FoszamGridView.ClientID
             , "check"
             , "cbSkontrobolKiveheto"
             , true
             , Resources.Question.UIConfirmHeader_SkontrobolKiveszTomeges
             , Resources.Question.UI_SkontrobolKiveszTomeges_Question);

        ListHeader1.SelejtezesreKijelolesOnClientClick = "var count = getSelectedCheckBoxesCount('" + MIG_FoszamGridView.ClientID + "','check'); if(count == 0){" +
        JavaScripts.SetOnClientClick("SelejtezesForm.aspx"
             , QueryStringVars.Command + "=" + CommandName.SelejtezesreKijeloles
             , Defaults.PopupWidth, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
             + "}else{" +
             JavaScripts.SetOnClientClickOperationAllowedConfirm(
             MIG_FoszamGridView.ClientID
             , "check"
             , "cbSelejtezesreKijelolheto"
             , true
             , Resources.Question.UIConfirmHeader_SelejtezesreKijelolesTomeges
             , Resources.Question.UI_SelejtezesreKijelolesTomeges_Question)
             + "}";

        ListHeader1.SelejtezesreKijelolesVisszavonasaOnClientClick = "var count = getSelectedCheckBoxesCount('" + MIG_FoszamGridView.ClientID + "','check'); if(count == 0){" +
             JavaScripts.SetOnClientClick("SelejtezesForm.aspx"
             , QueryStringVars.Command + "=" + CommandName.SelejtezesreKijelolesVisszavonasa
             , Defaults.PopupWidth, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
             + "}else{" +
             JavaScripts.SetOnClientClickOperationAllowedConfirm(
             MIG_FoszamGridView.ClientID
             , "check"
             , "cbSelejtezesreKijelolesVisszavonhato"
             , true
             , Resources.Question.UIConfirmHeader_SelejtezesreKijelolesVisszavonasaTomeges
             , Resources.Question.UI_SelejtezesreKijelolesVisszavonasaTomeges_Question)
             + "}";

        ListHeader1.SelejtezesOnClientClick = "var count = getSelectedCheckBoxesCount('" + MIG_FoszamGridView.ClientID + "','check'); if(count == 0){" +
             JavaScripts.SetOnClientClick("SelejtezesForm.aspx"
             , QueryStringVars.Command + "=" + CommandName.Selejtezes
             , Defaults.PopupWidth, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
             + "}else{" +
             JavaScripts.SetOnClientClickOperationAllowedConfirm(
             MIG_FoszamGridView.ClientID
             , "check"
             , "cbSelejtezheto"
             , true
             , Resources.Question.UIConfirmHeader_SelejtezesTomeges
             , Resources.Question.UI_SelejtezesTomeges_Question)
             + "}";

        ListHeader1.LomtarbaHelyezesOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
             MIG_FoszamGridView.ClientID
             , "check"
             , "cbLomtarbaHelyezheto"
             , true
             , Resources.Question.UIConfirmHeader_LomtarbaHelyezesTomeges
             , Resources.Question.UI_LomtarbaHelyezesTomeges_Question);

        ListHeader1.UgyiratIrattarAtvetelOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
             MIG_FoszamGridView.ClientID
             , "check"
             , "cbIrattarbaVeheto"
             , true
             , Resources.Question.UIConfirmHeader_IrattarbaTeszTomeges
             , Resources.Question.UI_IrattarbaTeszTomeges_Question);

        ListHeader1.KolcsonzesOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
             MIG_FoszamGridView.ClientID
             , "check"
             , "cbIrattarbolKikerheto"
             , true
             , "Irattárból kikérés"
             , "Biztosan kikéri a kijelölt tételeket?");


        //BLG_236
        ListHeader1.ImageIrattarbarendezesVisible = true;
        ListHeader1.ImageIrattarbarendezesOnClientClick = "var count = getSelectedCheckBoxesCount('"
                  + MIG_FoszamGridView.ClientID + "','check'); "
                  + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        //LZS BLG_8532
        ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("FoszamQuickSearch.aspx", ""
             , Defaults.PopupWidth, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        AlszamokSubListHeader.ButtonsClick += new CommandEventHandler(AlszamokSubListHeader_ButtonsClick);

        //selectedRecordId kezelése
        AlszamokSubListHeader.AttachedGridView = AlszamokGridView;

        TobbesSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        TobbesSubListHeader.ButtonsClick += new CommandEventHandler(TobbesSubListHeader_ButtonsClick);

        //selectedRecordId kezelése
        TobbesSubListHeader.AttachedGridView = TobbesGridView;

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new MIG_FoszamSearch());

        if (Session["FoszamListStartup"] != null &&
             Session["FoszamListStartup"].ToString() == Constants.Startup.SearchForm
             )
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            MIG_FoszamGridViewBind();
        }

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //LZS - BUG_9434
        if (FelhasznaloProfil.OrgKod(this.Page) == Contentum.eUtility.Constants.OrgKod.NMHH ||
            FelhasznaloProfil.OrgKod(this.Page) == Contentum.eUtility.Constants.OrgKod.NMHH_TUK)
        {
            //LZS BLG_8532
            ListHeader1.NewEnabled =
            ListHeader1.NewVisible = false;

            //LZS BLG_8532
            ListHeader1.ModifyEnabled =
            ListHeader1.ModifyVisible = false; //FunctionRights.GetFunkcioJog ( Page, "FoszamModify" );
        }
        else
        {
            ListHeader1.NewEnabled =
            ListHeader1.NewVisible = true;

            ListHeader1.ModifyEnabled =
            ListHeader1.ModifyVisible = FunctionRights.GetFunkcioJog(Page, "FoszamModify");
        }

        //LZS BLG_8532
        ListHeader1.NumericSearchEnabled =
        ListHeader1.NumericSearchVisible = true;

        ListHeader1.ViewEnabled = true;

        ListHeader1.UgyiratTerkepEnabled = true;
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = false; // nincs history

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        ListHeader1.SkontrobaHelyezEnabled = FunctionRights.GetFunkcioJog(Page, "FoszamSkontrobaHelyezes");
        ListHeader1.SkontrobolKivetelEnabled = FunctionRights.GetFunkcioJog(Page, "FoszamSkontrobolKivetel");
        ListHeader1.IrattarozasraEnabled = FunctionRights.GetFunkcioJog(Page, "FoszamIrattarbaKuldes");
        ListHeader1.KiadasOsztalyraEnabled = FunctionRights.GetFunkcioJog(Page, "FoszamKiadasOsztalyra");
        ListHeader1.SelejtezesreKijelolesEnabled = ListHeader1.SelejtezesreKijelolesVisszavonasaEnabled
             = ListHeader1.SelejtezesEnabled = FunctionRights.GetFunkcioJog(Page, "FoszamSelejtezes");
        ListHeader1.LomtarbaHelyezesEnabled = FunctionRights.GetFunkcioJog(Page, "FoszamLomtarbaHelyezes");
        ListHeader1.UgyiratIrattarAtvetelEnabled = FunctionRights.GetFunkcioJog(Page, "FoszamIrattarbaVetel");
        ListHeader1.KolcsonzesEnabled = FunctionRights.GetFunkcioJog(Page, "FoszamIrattarbolKikeres");

        //BLG_236
        ListHeader1.ImageIrattarbarendezesEnabled = FunctionRights.GetFunkcioJog(Page, "IrattarRendezes" + CommandName.Modify);

        AlszamokSubListHeader.NewEnabled = false;
        AlszamokSubListHeader.NewVisible = false;
        AlszamokSubListHeader.ViewEnabled = true;
        AlszamokSubListHeader.ModifyVisible = false;
        AlszamokSubListHeader.ModifyEnabled = false;
        AlszamokSubListHeader.InvalidateEnabled = false;
        AlszamokSubListHeader.InvalidateVisible = false;
        AlszamokSubListHeader.ValidFilterVisible = false;
        //AlszamokSubListHeader.IrattarozasraVisible = true;
        //AlszamokSubListHeader.KiadasOsztalyraVisible = true;
        //AlszamokSubListHeader.SkontrobaHelyezVisible = true;
        ////AlszamokSubListHeader.SkontrobolKivetelVisible = false;
        //AlszamokSubListHeader.SkontrobolKivetelVisible = true;

        TobbesSubListHeader.NewEnabled = false;
        TobbesSubListHeader.ViewEnabled = false;
        TobbesSubListHeader.ModifyEnabled = false;
        TobbesSubListHeader.InvalidateEnabled = false;
        TobbesSubListHeader.ValidFilterVisible = false;

        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Foszam" + CommandName.ViewHistory);

        //ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Lock);
        //ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Lock);
        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(MIG_FoszamGridView);
            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                //PartnerekGridView_RefreshOnClientClicks(MasterListSelectedRowId);
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(MIG_FoszamGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(AlszamokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(TobbesGridView);

        string column_v = "";
        for (int i = 0; i < MIG_FoszamGridView.Columns.Count; i++)
        {
            if (MIG_FoszamGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('FoszamSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";

        if (Session["FoszamListStartup"] != null)
        {
            if (Session["FoszamListStartup"].ToString() == Constants.Startup.SearchForm)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";
                script +=
                     JavaScripts.SetOnClientClick_DisplayUpdateProgress("FoszamSearch.aspx", ""
                          , Defaults.PopupWidth, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                          , CustomUpdateProgress1.UpdateProgress.ClientID);
                script += "}";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "FoszamSearch", script, true);
                Session.Remove("FoszamListStartup");
            }
        }

        UI.SetGridViewColumnVisiblity(AlszamokGridView, "UGYHOL", IsNMHHColumnsVisible);
        UI.SetGridViewColumnVisiblity(AlszamokGridView, "IRATTARBA", IsNMHHColumnsVisible);
        UI.SetGridViewColumnVisiblity(AlszamokGridView, "Adathordozo_tipusa", IsNMHHColumnsVisible);
        UI.SetGridViewColumnVisiblity(AlszamokGridView, "Irattipus", IsNMHHColumnsVisible);
        UI.SetGridViewColumnVisiblity(AlszamokGridView, "Feladat", IsNMHHColumnsVisible);
    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        //return;
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (MIG_FoszamGridView.SelectedIndex > -1)
            {
                MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
                //Contentum.eMigration.Service.MIG_FoszamService service = Contentum.eMigration.Utility.eMigrationService.ServiceFactory.GetMIG_FoszamService();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = MIG_FoszamGridView.SelectedValue.ToString();

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    MIG_Foszam MIG_Foszam = (MIG_Foszam)result.Record;

                    //Page.ClientScript.RegisterStartupScript(Page.GetType(),"","alert('"+MIG_Foszam.MEMO+"')");
                    //string hiddenFieldId = Page.Request.QueryString.Get(QueryStringVars.HiddenFieldId);
                    //bool returnv = JavaScripts.SendBackResultToCallingWindow(Page, MIG_Foszam.Id, MIG_Foszam.MEMO);
                    //String ParentSite = Page.Parent.Site.Name;

                    //JavaScripts.SendBackResultToCallingWindow(Page, MIG_Foszam.Id, MIG_Foszam.MEMO);
                    //JavaScripts.RegisterCloseWindowClientScript(Page, false);
                    //disable_refreshLovList = true;
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                }

            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1
                     , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                ErrorUpdatePanel.Update();
            }

        }
        /*if (e.CommandName.ToString() == CommandName.Ok)
		{
			 if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
			 {
				  int selectedIndex = -1;
				  Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

				  //string selectedText = ListBoxSearchResult.SelectedItem.Text;
				  if (selectedIndex > -1)
				  {
						GridViewSearchResult.SelectedIndex = selectedIndex;

						string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
						//string selectedText = ListBoxSearchResult.SelectedItem.Text;
						string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

						bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
						if (refreshCallingWindow == true)
						{
							 string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
							 if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
							 else { refreshCallingWindow = false; }
						}

						JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
						disable_refreshLovList = true;
				  }
			 }
			 else
			 {
				  GridViewSelectedId.Value = String.Empty;
				  GridViewSelectedIndex.Value = String.Empty;

				  ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
						, Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
				  LovListHeader1.ErrorUpdatePanel.Update();
			 }
		}*/
    }


    // Segedfv.

    private const string Eloirat_Azon_Id_Delimitter = "***";
    private const string Eloirat_Azon_Id_RecordSeparator = "|||";

    protected string Format_Eloirat_Azon_Id(string Eloirat_Azon_Id)
    {
        if (!string.IsNullOrEmpty(Eloirat_Azon_Id))
        {
            string[] strParts = Eloirat_Azon_Id.Split(new string[] { Eloirat_Azon_Id_RecordSeparator }, StringSplitOptions.RemoveEmptyEntries);
            string res = "";
            foreach (string s in strParts)
            {
                if (!String.IsNullOrEmpty(s))
                {
                    if (res.Length > 0) { res += "<br/>"; }
                    res += "<a href=\"Foszamlist.aspx?Id=" + this.GetIdFrom_Eloirat_Azon_Id(s) + "\" style=\"text-decoration:underline\">"
                        + this.GetAzonositoFrom_Eloirat_Azon_Id(s) + "</a>";
                }
            }
            return res;
        }
        else
        {
            return "";
        }
    }

    protected string GetAzonositoFrom_Eloirat_Azon_Id(string Eloirat_Azon_Id)
    {
        if (!string.IsNullOrEmpty(Eloirat_Azon_Id))
        {
            string[] strParts = Eloirat_Azon_Id.Split(new string[1] { Eloirat_Azon_Id_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            return strParts[0];
        }
        else
        {
            return "";
        }
    }

    protected string GetIdFrom_Eloirat_Azon_Id(string Eloirat_Azon_Id)
    {
        if (!string.IsNullOrEmpty(Eloirat_Azon_Id))
        {
            string[] strParts = Eloirat_Azon_Id.Split(new string[1] { Eloirat_Azon_Id_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            if (strParts.Length > 1)
            {
                return strParts[1];
            }
            else
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }

    #endregion


    #region Master List

    protected void MIG_FoszamGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("MIG_FoszamGridView", ViewState, ""); // "Id" helyett UI_SAV, UI_YEAR DESC, UI_NUM DESC
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("MIG_FoszamGridView", ViewState);

        MIG_FoszamGridViewBind(sortExpression, sortDirection);
    }

    protected void MIG_FoszamGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(MIG_FoszamGridView);

        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        MIG_FoszamSearch search = (MIG_FoszamSearch)Search.GetSearchObject(Page, new MIG_FoszamSearch());
        search.OrderBy = Search.GetOrderBy("MIG_FoszamGridView", ViewState, SortExpression, SortDirection);

        //search.Csatolva_Rendszer.Value = "";
        //search.Csatolva_Rendszer.Operator = Contentum.eQuery.Query.Operators.isnull;
        search.Selejtezve.Value = "";
        search.Selejtezve.Operator = Contentum.eQuery.Query.Operators.isnull;
        //search.UI_IRSZ.Value = "";
        //search.UI_IRSZ.Operator = Contentum.eQuery.Query.Operators.notnull;

        search.TopRow = 2000;

        Search.SetIktatokonyvekSearch(Page, search, EErrorPanel1);
        //if (!string.IsNullOrEmpty(search.UI_SAV.Value))
        //{
        //    search.WhereByManual += " MIG_SAV " + search.UI_SAV.Operator + search.UI_SAV.Value;
        //    search.UI_SAV.Value = "";
        //}

        // Lapozás beállítása:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(MIG_FoszamGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        MIG_FoszamGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, MIG_FoszamCPE);
        MIG_FoszamGridViewBind();
    }

    //proba
    protected void MIG_FoszamGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(MIG_FoszamGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
            ActiveTabRefresh(TabContainer1, id);
            //MIG_FoszamGridView_SelectRowCommand(id);
        }
    }

    //private void MIG_FoszamGridView_RefreshOnClientClicks(string partnerCim_Id)
    //{
    //    string id = partnerCim_Id;
    //    if (!String.IsNullOrEmpty(id))
    //    {
    //        AlszamokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("AlszamForm.aspx"
    //            , "Command=" + CommandName.New + "&" + QueryStringVars.foszamId + "=" + id
    //            , Defaults.PopupWidth, Defaults.PopupHeight, AlszamokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
    //        /*MIG_FoszamSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("PartnerCimekForm.aspx"
    //            , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
    //            , Defaults.PopupWidth, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);*/
    //    }
    //}

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page);
        string felhnev = Contentum.eUtility.FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(execParam.Felhasznalo_Id, Page);
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, MIG_FoszamUpdatePanel.ClientID);

            bool modosithato = getModosithatosag(MIG_FoszamGridView);
            if (!modosithato)
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_UgyiratMegtekintes
                     + "')) {" + JavaScripts.SetOnClientClick("FoszamForm.aspx"
                     , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                     , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                     + "} else return false;";

                // ügyirattérkép View módban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, MIG_FoszamUpdatePanel.ClientID);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                     , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                     , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);


                // ügyirattérkép Modify módban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, MIG_FoszamUpdatePanel.ClientID);

            }
            ErrorDetails error;
            ExecParam ep = UI.SetExecParamDefault(Page);
            Foszamok.Statusz statusz = Foszamok.GetAllapotById(id, ep, EErrorPanel1);
            if (statusz == null)
            {
                ErrorUpdatePanel.Update();
                return;
            }

            if (Foszamok.SzerelhetoBeleIrat(statusz, ep, Page, felhnev, out error))
            {
                ListHeader1.SzerelesVisible = true;
                ListHeader1.SzerelesOnClientClick = JavaScripts.SetOnClientClick("SzerelesForm.aspx",
                     QueryStringVars.Id + "=" + id
                     , Defaults.PopupWidth_Max, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            }
            else
            {
                // Nem szerelhetõ bele másik ügyirat:    
                ListHeader1.SzerelesOnClientClick = "alert('" + error.Message + "'); return false;";

            }

            ErrorDetails err_visszavon;
            if (statusz.IsSzerelt)
            {
                ListHeader1.SzerelesVisszavonVisible = true;
                ListHeader1.SzerelesVisszavonEnabled = true;

                if (Foszamok.SzerelesVisszavonhato(statusz, ep, Page, felhnev, out err_visszavon))
                {
                    ListHeader1.SzerelesVisszavonOnClientClick = " if (confirm('" + Resources.Question.UIConfirmHeader_ElokeszitettSzerelesFelbontasa
              + "')==false) { return false; } ";
                }
                else
                {
                    ListHeader1.SzerelesVisszavonOnClientClick = "alert('" + err_visszavon.Message + "'); return false;";
                }
            }

            string tableName = "MIG_Foszam";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                 , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                 , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, MIG_FoszamUpdatePanel.ClientID);

            //AlszamokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("AlszamForm.aspx"
            //    , "Command=" + CommandName.New + "&" + QueryStringVars.Id + "=" + id
            //    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, AlszamokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            //AlszamokSubListHeader.SkontrobaHelyezOnClientClick += JavaScripts.SetOnClientClick("AlszamSkontroVegeForm.aspx"
            //    , "Command=" + CommandName.Modify + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField_SkontroVege.ClientID
            //    , Defaults.PopupWidth, Defaults.PopupHeight, AlszamokUpdatePanel.ClientID, EventArgumentConst.refresh);



            //LZS - BLG_8097
            AlszamokSubListHeader.AiAtadasEnabled =
                AlszamokSubListHeader.AiAtadasVisible = FunctionRights.GetFunkcioJog(Page, "ORTT_atmeneti_irattarozas");

        }
    }

    protected void MIG_FoszamGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page
        //    , "cb_UtolsoHasznalatSikeres", "UtolsoHasznalatSiker");
        IratHelye.SetGridViewRow(e, "labelIratHelye", Page);
        UI.SetGridViewDateTime(e, "labelIrattarbaDate");
        UI.SetGridViewDateTime(e, "labelSkontroVegeDate");
        UI.SetGridViewDateTime(e, "labelHataridoDate");

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ErrorDetails error;
            DataRowView drv = (DataRowView)e.Row.DataItem;
            if (drv != null)
            {
                ExecParam execParam = UI.SetExecParamDefault(Page);
                string felhnev = Contentum.eUtility.FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(execParam.Felhasznalo_Id, Page);
                Foszamok.Statusz statusz = Foszamok.GetAllapotByDataRowView(drv);
                CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");
                if (cbModosithato != null)
                {
                    // BUG_11710
                    //cbModosithato.Checked = Foszamok.Modosithato(statusz);

                    Boolean javithato = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.EMIGRATION_ADATOK_MODOSITHATOSAGA_ENABLED, false);
                    cbModosithato.Checked = javithato || Foszamok.Modosithato(statusz);

                    ExecParam xpm = UI.SetExecParamDefault(Page);
                    if (statusz.Ugyhol == Constants.MIG_IratHelye.Irattarban && xpm.FelhasznaloSzervezet_Id == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(xpm).Obj_Id)
                    {
                        cbModosithato.Checked = true;
                    }

                }
                CheckBox cbSkontrobaHelyezheto = (CheckBox)e.Row.FindControl("cbSkontrobaHelyezheto");
                if (cbSkontrobaHelyezheto != null)
                {
                    cbSkontrobaHelyezheto.Checked = Foszamok.SkontrobaHelyezheto(statusz);
                }
                CheckBox cbSkontrobolKiveheto = (CheckBox)e.Row.FindControl("cbSkontrobolKiveheto");
                if (cbSkontrobolKiveheto != null)
                {
                    cbSkontrobolKiveheto.Checked = Foszamok.SkontrobolKiveheto(statusz);
                }
                CheckBox cbIrattarbaKuldheto = (CheckBox)e.Row.FindControl("cbIrattarbaKuldheto");
                if (cbIrattarbaKuldheto != null)
                {
                    cbIrattarbaKuldheto.Checked = Foszamok.IrattarbaKuldheto(statusz);
                }
                CheckBox cbIrattarbaVeheto = (CheckBox)e.Row.FindControl("cbIrattarbaVeheto");
                if (cbIrattarbaVeheto != null)
                {
                    cbIrattarbaVeheto.Checked = Foszamok.IrattarbaVeheto(statusz);
                }
                CheckBox cbIrattarbolKikerheto = (CheckBox)e.Row.FindControl("cbIrattarbolKikerheto");
                if (cbIrattarbolKikerheto != null)
                {
                    cbIrattarbolKikerheto.Checked = Foszamok.IrattarbolKikerheto(statusz);
                }
                CheckBox cbKiadhatoOsztalyra = (CheckBox)e.Row.FindControl("cbKiadhatoOsztalyra");
                if (cbKiadhatoOsztalyra != null)
                {
                    cbKiadhatoOsztalyra.Checked = Foszamok.KiadhatoOsztalyra(statusz);
                }
                CheckBox cbSelejtezesreKijelolheto = (CheckBox)e.Row.FindControl("cbSelejtezesreKijelolheto");
                if (cbSelejtezesreKijelolheto != null)
                {
                    cbSelejtezesreKijelolheto.Checked = Foszamok.SelejtezesreKijelolheto(statusz);
                }
                CheckBox cbSelejtezesreKijelolesVisszavonhato = (CheckBox)e.Row.FindControl("cbSelejtezesreKijelolesVisszavonhato");
                if (cbSelejtezesreKijelolesVisszavonhato != null)
                {
                    cbSelejtezesreKijelolesVisszavonhato.Checked = Foszamok.SelejtezesreKijelolesVisszavonhato(statusz);
                }
                CheckBox cbSelejtezheto = (CheckBox)e.Row.FindControl("cbSelejtezheto");
                if (cbSelejtezheto != null)
                {
                    cbSelejtezheto.Checked = Foszamok.Selejtezheto(statusz);
                }
                CheckBox cbLomtarbaHelyezheto = (CheckBox)e.Row.FindControl("cbLomtarbaHelyezheto");
                if (cbLomtarbaHelyezheto != null)
                {
                    cbLomtarbaHelyezheto.Checked = Foszamok.LomtarbaHelyezheto(statusz);
                }
                CheckBox cbSzerelheto = (CheckBox)e.Row.FindControl("cbSzerelheto");
                if (cbSzerelheto != null)
                {
                    cbSzerelheto.Checked = Foszamok.SzerelhetoBeleIrat(statusz, UI.SetExecParamDefault(Page), Page, felhnev, out error);
                }
                CheckBox cbSzerelesVisszavonhato = (CheckBox)e.Row.FindControl("cbSzerelesVisszavonhato");
                if (cbSzerelesVisszavonhato != null)
                {
                    cbSzerelesVisszavonhato.Checked = Foszamok.SzerelesVisszavonhato(statusz, execParam, Page, felhnev, out error);
                }

                #region Lakk Zsolt: BLG - 1588 - Csatolmány ikon megjelenítése

                string csatolmanyCount = String.Empty;

                csatolmanyCount = MIG_FoszamGridView.DataKeys[e.Row.RowIndex].Values[1].ToString();

                //Lakk Zsolt: Megjelenítjük a csatolmány ikont az utolsó oszlopban, amennyiben a lekérdezésben szereblõ csatolmányok száma értéke > 0 (labelCsatolmany)
                Image csatolmanyImage = (Image)e.Row.FindControl("CsatolmanyImage");
                System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;

                //Label labelCsatolmany = ( Label )e.Row.FindControl( "labelCsatolmany" );

                //if( labelCsatolmany != null && csatolmanyImage != null )
                //{
                //	csatolmanyImage.Visible = Convert.ToInt32( labelCsatolmany.Text ) > 0 ? true : false;
                //}

                if (!String.IsNullOrEmpty(csatolmanyCount))
                {
                    Image CsatolmanyImage = (Image)e.Row.FindControl("CsatolmanyImage");
                    if (CsatolmanyImage != null)
                    {
                        //CsatolmanyImage.OnClientClick = "";
                        int count = 0;
                        Int32.TryParse(csatolmanyCount, out count);
                        if (count > 0)
                        {
                            CsatolmanyImage.Visible = true;
                            CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
                            CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

                        }
                        else
                        {
                            CsatolmanyImage.Visible = false;
                        }
                    }

                }
                else
                {
                    Image CsatolmanyImage = (Image)e.Row.FindControl("CsatolmanyImage");
                    if (CsatolmanyImage != null)
                    {
                        //CsatolmanyImage.OnClientClick = "";
                        CsatolmanyImage.Visible = false;
                    }

                }
                #endregion
            }
        }
    }

    protected void MIG_FoszamGridView_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void MIG_FoszamGridView_PreRender(object sender, EventArgs e)
    {
        //int prev_PageIndex = MIG_FoszamGridView.PageIndex;

        //MIG_FoszamGridView.PageIndex = ListHeader1.PageIndex;
        //string detailRowCount = "";
        //if (Session[Constants.DetailRowCount] != null)
        //{
        //    detailRowCount = Session[Constants.DetailRowCount].ToString();
        //}

        //if (prev_PageIndex != MIG_FoszamGridView.PageIndex)
        //{
        //    MIG_FoszamGridViewBind();
        //}
        //else
        //{
        UI.GridViewSetScrollable(ListHeader1.Scrollable, MIG_FoszamCPE);
        //}
        //ListHeader1.PageCount = MIG_FoszamGridView.PageCount;
        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(MIG_FoszamGridView);

        ListHeader1.RefreshPagerLabel();

    }

    void MIG_FoszamSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(AlszamokSubListHeader.RowCount);
        AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
    }

    protected void MIG_FoszamGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        MIG_FoszamGridViewBind(e.SortExpression, UI.GetSortToGridView("MIG_FoszamGridView", ViewState, e.SortExpression));
    }

    protected void MIG_FoszamUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    MIG_FoszamGridViewBind();
                    //MIG_FoszamGridView_SelectRowCommand(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
                    break;
                case EventArgumentConst.refresh:
                    if (!String.IsNullOrEmpty(HiddenField_SkontroVege.Value))
                    {
                        CommandEventArgs commandeventargs = new CommandEventArgs(CommandName.SkontrobaHelyez, null);
                        ListHeaderRightFunkcionButtonsClick(ListHeader1, commandeventargs);
                        HiddenField_SkontroVege.Value = "";
                    }
                    break;
            }
        }
    }

    private void MIG_FoszamGridView_SelectRowCommand(string MIG_FoszamId)
    {
        string id = MIG_FoszamId;
        if (!String.IsNullOrEmpty(id))
        {


        }

    }

    //private void PartnerekGridView_RefreshOnClientClicks(string MIG_Foszam_Id)
    //{
    //    string id = MIG_Foszam_Id;
    //    if (!String.IsNullOrEmpty(id))
    //    {
    //        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
    //            , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
    //            , Defaults.PopupWidth, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
    //    }
    //}

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                break;
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        string js = string.Empty;
        switch (e.CommandName)
        {
            case CommandName.Irattarozasra:
                // CR3316 Ugyhol statuszok összekavarodtak
                // FPH-ban nincs Irattarbakuldott státusz, helyette rögtön Irattarban statuszba kell rakni
                if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.FPH)
                {
                    IrattarbaVetelSelectedFoszam();
                }
                else
                {
                    IrattarbaKuldSelectedFoszam();
                }
                MIG_FoszamGridViewBind();
                break;
            case CommandName.KiadasOsztalyra:
                KiadasOsztalyraSelectedFoszam();
                MIG_FoszamGridViewBind();
                break;
            case CommandName.SkontrobaHelyez:
                SkontrobaSelectedFoszam();
                MIG_FoszamGridViewBind();
                break;
            case CommandName.SkontrobolKivesz:
                SkontrobolKiSelectedFoszam();
                MIG_FoszamGridViewBind();
                break;
            case CommandName.SelejtezesreKijeloles:
                SelejtezesreKijelolesSelectedFoszam();
                MIG_FoszamGridViewBind();
                break;
            case CommandName.SelejtezesreKijelolesVisszavonasa:
                SelejtezesreKijelolesVisszavonasaSelectedFoszam();
                MIG_FoszamGridViewBind();
                break;
            case CommandName.Selejtezes:
                SelejtezesSelectedFoszam();
                MIG_FoszamGridViewBind();
                break;
            case CommandName.LomtarbaHelyezes:
                LomtarbaHelyezesSelectedFoszam();
                MIG_FoszamGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedFoszamok();
                break;
            case CommandName.UgyiratIrattarAtvetel:
                IrattarbaVetelSelectedFoszam();
                MIG_FoszamGridViewBind();
                break;
            case "Kolcsonzes":
                IrattarbolKikeresSelectedFoszam();
                MIG_FoszamGridViewBind();
                break;
            case CommandName.SzerelesVisszavonas:
                SzerelesVisszavonasSelectedFoszam();
                MIG_FoszamGridViewBind();
                break;
            case CommandName.UgyiratpotloNyomtatasa:
                UgyiratPotloNyomtatasa();
                //IraKezbesitesiTetelekGridViewBind();
                break;
            case CommandName.ITSZModosit:
                {
                    string selectedId = UI.GetGridViewSelectedRecordId(MIG_FoszamGridView);
                    List<string> chked = ui.GetGridViewSelectedRows(MIG_FoszamGridView, EErrorPanel1, MIG_FoszamUpdatePanel);
                    List<string> selectedItems = ui.GetGridViewSelectedAndExecutableRows(MIG_FoszamGridView, "cbModosithato", EErrorPanel1, ErrorUpdatePanel);   // elvileg nem biztos, hogy a kiválasztott sor egyben a bepipált sor is
                    if (selectedItems.Count > 0 && chked.Count == selectedItems.Count)
                    {
                        Session["SelectedFoszamIds"] = String.Join(",", selectedItems.ToArray());
                        js = JavaScripts.SetOnClientClickWithTimeout("ITSZModositasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIdsModosit", js, true);
                    }
                    else if (chked.Count != selectedItems.Count)
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIdsModositError", string.Format("alert('{0}');", Resources.Error.UIUgyiratNemModosithato), true);
                    }

                    MIG_FoszamGridViewBind();
                }
                break;
            // BLG_236
            case CommandName.Irattarbarendezes:
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(MIG_FoszamGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session["SelectedUgyiratIds"] = sb.ToString();

                Session["SelectedBarcodeIds"] = null;


                js = JavaScripts.SetOnClientClickWithTimeout("IrattarRendezesForm.aspx", CommandName.Irattarbarendezes + "&" + QueryStringVars.Startup + "=" + Startup, Defaults.PopupWidth_Max, Defaults.PopupHeight, MIG_FoszamUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIrattarbarendezes", js, true);

                break;
        }
    }
    private void SzerelesVisszavonasSelectedFoszam()
    {
        List<string> selectedMIGFoszamIds = ui.GetGridViewSelectedAndExecutableRows(MIG_FoszamGridView, "cbSzerelesVisszavonhato", EErrorPanel1, ErrorUpdatePanel);
        if (selectedMIGFoszamIds.Count == 0) { return; }

        EREC_UgyUgyiratokService ugyiratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        MIG_FoszamService migFoszamService = eMigrationService.ServiceFactory.GetMIG_FoszamService();

        List<string> utoiratNevek = UI.GetGridViewColumnValuesOfSelectedRows(MIG_FoszamGridView, "labelUtoiratNev");
        List<string> selectedMIGFoszamMerge = UI.GetGridViewColumnValuesOfSelectedRows(MIG_FoszamGridView, "labelFoszam_Merge");

        for (int i = 0; i < selectedMIGFoszamIds.Count; i++)
        {
            string migFoszamID = selectedMIGFoszamIds[i];
            string migFoszamMerge = selectedMIGFoszamMerge[i];
            string utoIratNev = utoiratNevek[i];
            utoIratNev = utoIratNev.Substring(utoIratNev.IndexOf(">") + 1, ((utoIratNev.IndexOf("</")-1) - utoIratNev.IndexOf(">")));
            Logger.Debug("SzerelesVisszavonasSelectedFoszam: migFoszamID" + migFoszamID);
            Logger.Debug("SzerelesVisszavonasSelectedFoszam: utoIratNev" + utoIratNev);

            var ugyiratokSearch = new EREC_UgyUgyiratokSearch();
            ugyiratokSearch.Azonosito.Filter(utoIratNev);
            var ugyiratSearchResult = ugyiratokService.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), ugyiratokSearch);

            if (ugyiratSearchResult.IsError)
            {
                Logger.Debug("SzerelesVisszavonasSelectedFoszam: ugyiratSearchResult.Error" + ugyiratSearchResult.ErrorMessage);
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, ugyiratSearchResult);
                continue;
            }
            if (!ugyiratSearchResult.HasData())
            {
                Logger.Debug("SzerelesVisszavonasSelectedFoszam: ugyiratSearchResult.No data");
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba a szétszerelés során", "Nem található az előirat az azonosító alapján.");
                continue;
            }
            var ugyirat = ugyiratSearchResult.GetData<EREC_UgyUgyiratok>().First();            

            string szuloUgyiratId = ugyirat.Id;
            string visszaVonandoUgyiratAzonosito = migFoszamMerge;

            var regiAdatSzerelesVisszavonasaResult = ugyiratokService.RegiAdatSzerelesVisszavonasa(UI.SetExecParamDefault(Page, new ExecParam()), szuloUgyiratId, visszaVonandoUgyiratAzonosito);

            if (regiAdatSzerelesVisszavonasaResult.IsError)
            {
                Logger.Debug("SzerelesVisszavonasSelectedFoszam: regiAdatSzerelesVisszavonasaResult.Error" + ugyiratSearchResult.ErrorMessage);
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, regiAdatSzerelesVisszavonasaResult);
                continue;
            }

            var foszamSzerelesMigResult = migFoszamService.FoszamSzerelesMig(UI.SetExecParamDefault(Page, new ExecParam()), migFoszamID, migFoszamID, true);
            if (!String.IsNullOrEmpty(foszamSzerelesMigResult.ErrorCode))
            {
                Logger.Debug("SzerelesVisszavonasSelectedFoszam: regiAdatSzerelesVisszavonasaResult.Error" + foszamSzerelesMigResult.ErrorMessage);
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, foszamSzerelesMigResult);
                continue;
            }
        }
    }

    private void SelejtezesreKijelolesSelectedFoszam()
    {
        List<string> selectedItems = ui.GetGridViewSelectedAndExecutableRows(MIG_FoszamGridView, "cbSelejtezesreKijelolheto", EErrorPanel1, ErrorUpdatePanel);

        if (selectedItems.Count > 0)
        {
            MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            MIG_FoszamSearch search = new MIG_FoszamSearch();
            search.Id.Value = Search.GetSqlInnerString(selectedItems.ToArray());
            search.Id.Operator = Query.Operators.inner;
            Result res = service.SelejtezesreKijeloles(execParam, search);
            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }
    }

    private void SelejtezesreKijelolesVisszavonasaSelectedFoszam()
    {
        List<string> selectedItems = ui.GetGridViewSelectedAndExecutableRows(MIG_FoszamGridView, "cbSelejtezesreKijelolesVisszavonhato", EErrorPanel1, ErrorUpdatePanel);

        if (selectedItems.Count > 0)
        {
            MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            MIG_FoszamSearch search = new MIG_FoszamSearch();
            search.Id.Value = Search.GetSqlInnerString(selectedItems.ToArray());
            search.Id.Operator = Query.Operators.inner;
            Result res = service.SelejtezesreKijelolesVisszavonasa(execParam, search);
            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }
    }

    private void SelejtezesSelectedFoszam()
    {
        List<string> selectedItems = ui.GetGridViewSelectedAndExecutableRows(MIG_FoszamGridView, "cbSelejtezheto", EErrorPanel1, ErrorUpdatePanel);

        if (selectedItems.Count > 0)
        {
            MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            MIG_FoszamSearch search = new MIG_FoszamSearch();
            search.Id.Value = Search.GetSqlInnerString(selectedItems.ToArray());
            search.Id.Operator = Query.Operators.inner;
            Result res = service.Selejtezes(execParam, search);
            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }
    }

    private void LomtarbaHelyezesSelectedFoszam()
    {
        ExecParam[] execParams = GetSelectedAndExecutableRecordsFromGridView(MIG_FoszamGridView, "cbLomtarbaHelyezheto");

        if (execParams != null && execParams.Length > 0)
        {
            MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            Result res = svc.LomtarbaHelyezesTomeges(execParams);
            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }
    }

    /// <summary>
    /// Elkuldi emailben a MIG_FoszamGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedFoszamok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(MIG_FoszamGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "MIG_Foszam");
        }
    }

    protected string GetIrattariJel(object oYear, object oIrj, object oIrj2000)
    {
        if (oYear == null)
            return String.Empty;

        try
        {
            int year = (int)oYear;
            if (year < 2000)
                return (oIrj ?? String.Empty).ToString();
            else
                return (oIrj2000 ?? String.Empty).ToString();
        }
        catch (Exception)
        {
            return String.Empty;
        }
    }
    #endregion

    #region Detail Tab
    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (MIG_FoszamGridView.SelectedIndex == -1)
        {
            return;
        }
        string foszamId = UI.GetGridViewSelectedRecordId(MIG_FoszamGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, foszamId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string foszamId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                AlszamokGridViewBind(foszamId);
                AlszamokPanel.Visible = true;
                break;
            case 1:
                TobbesGridViewBind(foszamId);
                TobbesPanel.Visible = true;
                break;

        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(AlszamokGridView);
                break;
            case 1:
                ui.GridViewClear(TobbesGridView);
                break;

        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        AlszamokSubListHeader.RowCount = RowCount;
        TobbesSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(AlszamokTabPanel))
        {
            AlszamokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(AlszamokGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(TobbesTabPanel))
        {
            TobbesGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(TobbesGridView));
        }
    }

    #endregion

    #region Alszamok Detail

    private void AlszamokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedAlszamok();
            AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
        }

        if (e.CommandName == CommandName.AiAtadas)
        {
            AiAtadas(UI.GetGridViewSelectedRecordId(AlszamokGridView));
        }


        //switch (e.CommandName)
        //{
        //    case CommandName.Irattarozasra:
        //        IrattarbaSelectedAlszam();
        //        AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
        //        break;
        //    case CommandName.KiadasOsztalyra:
        //        KiadasOsztalyraSelectedAlszam();
        //        AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
        //        break;
        //    case CommandName.SkontrobaHelyez:
        //        SkontrobaSelectedAlszam();
        //        AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
        //        break;
        //    case CommandName.SkontrobolKivesz:
        //        SkontrobolKiSelectedAlszam();
        //        AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
        //        break;
        //}
    }

    private ExecParam[] GetSelectedRecordsFromGridView(GridView gridView)
    {
        List<string> itemsList = ui.GetGridViewSelectedRows(gridView, EErrorPanel1, ErrorUpdatePanel);
        ExecParam[] execParams = new ExecParam[itemsList.Count];
        for (int i = 0; i < itemsList.Count; i++)
        {
            execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
            execParams[i].Record_Id = itemsList[i];
        }

        return execParams;
    }

    private ExecParam[] GetSelectedAndExecutableRecordsFromGridView(GridView gridView, string executableCheckBoxID)
    {
        List<string> itemsList = ui.GetGridViewSelectedAndExecutableRows(gridView, executableCheckBoxID, EErrorPanel1, ErrorUpdatePanel);
        ExecParam[] execParams = new ExecParam[itemsList.Count];
        for (int i = 0; i < itemsList.Count; i++)
        {
            execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
            execParams[i].Record_Id = itemsList[i];
        }

        return execParams;
    }

    private void SkontrobolKiSelectedFoszam()
    {
        //if (MIG_FoszamGridView.SelectedIndex > -1)
        //{
        //    string id = UI.GetGridViewSelectedRecordId(MIG_FoszamGridView);
        //    MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
        //    ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        //    xpm.Record_Id = id;
        //    Result res = svc.SkontrobolKivesz(xpm);
        //    if (!String.IsNullOrEmpty(res.ErrorCode))
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
        //    }
        //}

        ExecParam[] execParams = GetSelectedAndExecutableRecordsFromGridView(MIG_FoszamGridView, "cbSkontrobolKiveheto");
        if (execParams != null && execParams.Length > 0)
        {
            MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            Result res = svc.SkontrobolKiveszTomeges(execParams);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }
    }

    private void SkontrobaSelectedFoszam()
    {
        //if (MIG_FoszamGridView.SelectedIndex > -1)
        //{
        //    string id = UI.GetGridViewSelectedRecordId(MIG_FoszamGridView);
        //    MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
        //    ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        //    xpm.Record_Id = id;
        //    Result res = svc.SkontrobaHelyez(xpm);
        //    if (!String.IsNullOrEmpty(res.ErrorCode))
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
        //    }
        //}

        if (!String.IsNullOrEmpty(HiddenField_SkontroVege.Value))
        {
            ExecParam[] execParams = GetSelectedAndExecutableRecordsFromGridView(MIG_FoszamGridView, "cbSkontrobaHelyezheto");
            if (execParams != null && execParams.Length > 0)
            {
                MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
                Result res = svc.SkontrobaHelyezTomeges(execParams, HiddenField_SkontroVege.Value);// TODO: dátum bekérés
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                }
            }
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_56008);
        }
    }

    private void IrattarbaVetelSelectedFoszam()
    {
        //if (MIG_FoszamGridView.SelectedIndex > -1)
        //{
        //    string id = UI.GetGridViewSelectedRecordId(MIG_FoszamGridView);
        //    MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
        //    ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        //    xpm.Record_Id = id;
        //    Result res = svc.IrattarbaTesz(xpm);
        //    if (!String.IsNullOrEmpty(res.ErrorCode))
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
        //    }
        //}

        // CR3316 Ugyhol statuszok összekavarodtak
        // FPH-ban nincs Irattarbakuldott státusz, helyette rögtön Irattarban statuszba kell rakni
        ExecParam[] execParams;
        if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.FPH)
        {
            execParams = GetSelectedAndExecutableRecordsFromGridView(MIG_FoszamGridView, "cbIrattarbaKuldheto");
        }
        else
            execParams = GetSelectedAndExecutableRecordsFromGridView(MIG_FoszamGridView, "cbIrattarbaVeheto");

        if (execParams != null && execParams.Length > 0)
        {
            MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            Result res = svc.IrattarbaTeszTomeges(execParams);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }
    }

    private void IrattarbaKuldSelectedFoszam()
    {

        ExecParam[] execParams = GetSelectedAndExecutableRecordsFromGridView(MIG_FoszamGridView, "cbIrattarbaKuldheto");
        if (execParams != null && execParams.Length > 0)
        {
            MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            Result res = svc.IrattarbaKuldTomeges(execParams);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }
    }

    private void IrattarbolKikeresSelectedFoszam()
    {
        string irattarbolKikeroNev = FelhasznaloProfil.GetCurrent(Page).Felhasznalo.Nev;
        ExecParam[] execParams = GetSelectedAndExecutableRecordsFromGridView(MIG_FoszamGridView, "cbIrattarbolKikerheto");
        if (execParams != null && execParams.Length > 0)
        {
            MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            Result res = svc.IrattarbolKikerTomeges(execParams);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }
    }

    private void KiadasOsztalyraSelectedFoszam()
    {
        //if (MIG_FoszamGridView.SelectedIndex > -1)
        //{
        //    string id = UI.GetGridViewSelectedRecordId(MIG_FoszamGridView);
        //    MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
        //    ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        //    xpm.Record_Id = id;
        //    Result res = svc.KiadasOsztalyra(xpm);
        //    if (!String.IsNullOrEmpty(res.ErrorCode))
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
        //    }
        //}

        ExecParam[] execParams = GetSelectedAndExecutableRecordsFromGridView(MIG_FoszamGridView, "cbKiadhatoOsztalyra");
        if (execParams != null && execParams.Length > 0)
        {
            MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            Result res = svc.KiadasOsztalyraTomeges(execParams);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }
    }

    protected void AlszamokGridViewBind(string foszamId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("AlszamokGridView", ViewState, "ALNO");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("AlszamokGridView", ViewState);

        DokumentumVizualizerComponent.ClearDokumentElements();

        AlszamokGridViewBind(foszamId, sortExpression, sortDirection);
    }

    protected void AlszamokGridViewBind(string foszamId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(foszamId))
        {
            MIG_AlszamService service = eMigrationService.ServiceFactory.GetMIG_AlszamService();

            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            MIG_AlszamSearch search = (MIG_AlszamSearch)Search.GetSearchObject(Page, new MIG_AlszamSearch());
            search.OrderBy = Search.GetOrderBy("AlszamokGridView", ViewState, SortExpression, SortDirection);

            search.MIG_Foszam_Id.Value = foszamId;
            search.MIG_Foszam_Id.Operator = Contentum.eQuery.Query.Operators.equals;


            Result res = service.GetAll(ExecParam, search);
            UI.GridViewFill(AlszamokGridView, res, AlszamokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Label1);

            int sorszam = AlszamokGridView.Rows.Count;
            ui.SetClientScriptToGridViewSelectDeSelectButton(AlszamokGridView);

        }
        else
        {
            ui.GridViewClear(AlszamokGridView);
        }
    }

    void AlszamokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
    }

    protected void AlszamokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(AlszamokTabPanel))
                    //{
                    //    AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
                    //}
                    ActiveTabRefreshDetailList(AlszamokTabPanel);
                    break;
                    //case EventArgumentConst.refresh:
                    //    if (!String.IsNullOrEmpty(HiddenField_SkontroVege.Value))
                    //    {
                    //        CommandEventArgs commandeventargs = new CommandEventArgs(CommandName.SkontrobaHelyez, null);
                    //        AlszamokSubListHeader_ButtonsClick(AlszamokSubListHeader, commandeventargs);
                    //        HiddenField_SkontroVege.Value = "";
                    //    }
                    //    break;
            }
        }
    }

    /// <summary>
    /// Törli a CsoporttagokGridView -ban kijelölt elemeket
    /// </summary>
    private void deleteSelectedAlszamok()
    {
        //if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_AlszamInvalidate"))
        //{
        //    List<string> deletableItemsList = ui.GetGridViewSelectedRows(AlszamokGridView, EErrorPanel1, ErrorUpdatePanel);
        //    KRT_Felhasznalo_AlszamService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_AlszamService();
        //    ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
        //    for (int i = 0; i < deletableItemsList.Count; i++)
        //    {
        //        execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
        //        execParams[i].Record_Id = deletableItemsList[i];
        //    }
        //    Result result = service.MultiInvalidate(execParams);
        //    if (!String.IsNullOrEmpty(result.ErrorCode))
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        //        ErrorUpdatePanel.Update();
        //    }
        //}
        //else
        //{
        //    UI.RedirectToAccessDeniedErrorPage(Page);
        //}
    }

    //LZS - BLG_8097
    private void AiAtadas(string AlszamId)
    {
        if (!string.IsNullOrEmpty(AlszamId))
        {
            MIG_AlszamService AlszamService = eMigrationService.ServiceFactory.GetMIG_AlszamService();
            ExecParam AlszamExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            AlszamExecParam.Record_Id = AlszamId;

            Result resAlszam = AlszamService.Get(AlszamExecParam);
            MIG_Alszam AlszamRecord = resAlszam.Record as MIG_Alszam;

            MIG_FoszamService FoszamService = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam FoszamExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            FoszamExecParam.Record_Id = AlszamRecord.MIG_Foszam_Id;

            Result resFoszam = FoszamService.Get(FoszamExecParam);
            MIG_Foszam FoszamRecord = resFoszam.Record as MIG_Foszam;

            if (string.IsNullOrEmpty(FoszamRecord.UGYHOL) || FoszamRecord.UGYHOL == "3")
            {
                //Alszám módosítás
                AlszamRecord.UGYHOL = "ÁI";
                AlszamRecord.Updated.UGYHOL = true;

                AlszamRecord.IRATTARBA = DateTime.Today.ToString("yyyy-MM-dd");
                AlszamRecord.Updated.IRATTARBA = true;

                Result UpdateAlszamRecord = AlszamService.Update(AlszamExecParam, AlszamRecord);

                //Főszám módosítás
                FoszamRecord.UGYHOL = "2";
                FoszamRecord.Updated.UGYHOL = true;

                FoszamRecord.IRATTARBA = DateTime.Today.ToString("yyyy-MM-dd");
                FoszamRecord.Updated.IRATTARBA = true;

                FoszamRecord.IrattariHely = "Átmeneti irattár";
                FoszamRecord.Updated.IrattariHely = true;

                FoszamRecord.Updated.Selejtezve = false;

                Result FoszamResult = FoszamService.Update(FoszamExecParam, FoszamRecord);
            }
            else
            {
                string js = "alert('A kijelölt főszámú ügyirat nem helyezhető átmeneti irattárba!')";
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "foszamAlert", js, true);
            }

        }
        else
        {
            string js1 = "alert('Nincs kijelölt alszám!')";
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "alszamAlert", js1, true);
        }
    }

    protected void AlszamokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(AlszamokGridView, selectedRowNumber, "check");
        }
    }

    private void AlszamokGridView_RefreshOnClientClicks(string AlszamId)
    {
        string id = AlszamId;
        if (!String.IsNullOrEmpty(id))
        {
            AlszamokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("AlszamForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, AlszamokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            //bool modosithato = getModosithatosag(MIG_FoszamGridView);   // a szülõ fõszámot kell nézni, hogy indítható-e Modify parancs (szereltekre nem indítható)
            //AlszamokSubListHeader.ModifyEnabled = modosithato && FunctionRights.GetFunkcioJog(Page, "UgyiratModify");
            //if (AlszamokSubListHeader.ModifyEnabled)
            //{
            //    AlszamokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("AlszamForm.aspx"
            //        , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, AlszamokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            //}
            //AlszamokSubListHeader.IrattarozasraOnClientClick = String.Empty;
            //AlszamokSubListHeader.KiadasOsztalyraOnClientClick = String.Empty;

            // tömeges tranzakciók miatt kivéve, mindkét gomb látszik
            //if (IsAlszamInSkontro(id))
            //{
            //    AlszamokSubListHeader.SkontrobaHelyezVisible = false;
            //    AlszamokSubListHeader.SkontrobolKivetelVisible = true;
            //    AlszamokSubListHeader.SkontrobolKivetelOnClientClick = String.Empty;
            //}
            //else
            //{
            //    AlszamokSubListHeader.SkontrobaHelyezVisible = true;
            //    AlszamokSubListHeader.SkontrobaHelyezOnClientClick = String.Empty;
            //    AlszamokSubListHeader.SkontrobolKivetelVisible = false;
            //}
        }
    }

    //private bool IsAlszamInSkontro(string AlszamId)
    //{
    //    bool ret = false;
    //    GridViewRow row = AlszamokGridView.SelectedRow;
    //    if (row != null)
    //    {
    //        Label labelIratHelye = (Label)row.FindControl("labelIratHelye");
    //        Label labelSkontroVegeDate = (Label)row.FindControl("labelSkontroVegeDate");
    //        if (labelIratHelye != null && labelSkontroVegeDate != null)
    //        {
    //            if (labelIratHelye.Text == IratHelye.Skontro.Text && !String.IsNullOrEmpty(labelSkontroVegeDate.Text))
    //            {
    //                ret = true;
    //            }
    //        }
    //    }

    //    return ret;
    //}

    protected void AlszamokGridView_PreRender(object sender, EventArgs e)
    {
        //proba


        //proba vége
        int prev_PageIndex = AlszamokGridView.PageIndex;

        AlszamokGridView.PageIndex = AlszamokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != AlszamokGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, AlszamokCPE);
            AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
        }
        else
        {
            UI.GridViewSetScrollable(AlszamokSubListHeader.Scrollable, AlszamokCPE);
        }
        AlszamokSubListHeader.PageCount = AlszamokGridView.PageCount;
        AlszamokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(AlszamokGridView);



    }


    void AlszamokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(AlszamokSubListHeader.RowCount);
        AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
    }

    protected void AlszamokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        AlszamokGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView)
             , e.SortExpression, UI.GetSortToGridView("AlszamokGridView", ViewState, e.SortExpression));
    }

    protected void AlszamokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //IratHelye.SetGridViewRow(e, "labelIratHelye");
        //UI.SetGridViewDateTime(e, "labelIrattarbaDate");
        //UI.SetGridViewDateTime(e, "labelSkontroVegeDate");

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            #region Lakk Zsolt: BLG - 1588 - Csatolmány ikon megjelenítése

            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;

            string csatolmanyCount = String.Empty;
            string MIG_Dokumentum_Id = String.Empty;
            //string Alszam_Count = String.Empty;

            if (AlszamokGridView.DataKeys[e.Row.RowIndex].Values.Contains("Csatolmany_Count"))
            {
                csatolmanyCount = AlszamokGridView.DataKeys[e.Row.RowIndex].Values[1].ToString();
            }

            if (AlszamokGridView.DataKeys[e.Row.RowIndex].Values.Contains("MIG_Dokumentum_Id"))
            {
                MIG_Dokumentum_Id = AlszamokGridView.DataKeys[e.Row.RowIndex].Values[2].ToString();
            }

            //if ( AlszamokGridView.DataKeys[ e.Row.RowIndex ].Values.Contains ( "Alszam_Count" ) )
            //{
            //	Alszam_Count = AlszamokGridView.DataKeys[ e.Row.RowIndex ].Values[ 3 ].ToString ( );
            //}


            //Lakk Zsolt: Megjelenítjük a csatolmány ikont az utolsó oszlopban, amennyiben a lekérdezésben szereblõ csatolmányok száma értéke > 0 (labelCsatolmany)
            Image csatolmanyImage = (Image)e.Row.FindControl("CsatolmanyImage");

            //Label labelCsatolmany = ( Label )e.Row.FindControl( "labelCsatolmany" );

            //if( labelCsatolmany != null && csatolmanyImage != null )
            //{
            //	csatolmanyImage.Visible = Convert.ToInt32( labelCsatolmany.Text ) > 0 ? true : false;
            //}

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    int count = 0;

                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        CsatolmanyImage.Visible = true;
                        CsatolmanyImage.OnClientClick = "alert('" + Resources.Form.UI_NavigationError_UndefinedObject + "');";
                        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);


                        if (count == 1 && !String.IsNullOrEmpty(MIG_Dokumentum_Id))
                        {
                            CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
                            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", MIG_Dokumentum_Id);
                            DokumentumVizualizerComponent.AddDokumentElement(CsatolmanyImage.ClientID, MIG_Dokumentum_Id);
                        }
                        else
                        {
                            CsatolmanyImage.ToolTip = String.Format("Az irathoz több csatolmány ( {0} db ) tartozik, megnyitása a csatolmányok fülön lehetséges. ", csatolmanyCount);

                            // set "link"
                            string id = "";

                            if (drw["Id"] != null)
                            {
                                id = drw["Id"].ToString();
                            }

                            //string Azonosito = ""; // iktatószám, a ReadableWhere fogja használni a dokumentumok listáján
                            //if ( drw.Row.Table.Columns.Contains ( "Foszam_Merge" ) && drw[ "Foszam_Merge" ] != null )
                            //{
                            //	Azonosito = drw[ "Foszam_Merge" ].ToString ( );
                            //}

                            if (!String.IsNullOrEmpty(id))
                            {
                                string onclick = JavaScripts.SetOnClientClick("AlszamForm.aspx"
                                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                                + "&" + QueryStringVars.SelectedTab + "=" + "TabAlszamCsatolmanyokPanel"
                                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, AlszamokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

                                CsatolmanyImage.OnClientClick = onclick;
                            }
                        }
                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }

            }
            else
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    CsatolmanyImage.Visible = false;
                }

            }
            #endregion
        }
    }

    #endregion

    #region Tobbes Detail

    private void TobbesSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedTobbes();
            TobbesGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
        }
    }

    protected void TobbesGridViewBind(string foszamId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("TobbesGridView", ViewState, "UI_SAV");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("TobbesGridView", ViewState);

        TobbesGridViewBind(foszamId, sortExpression, sortDirection);
    }

    protected void TobbesGridViewBind(string foszamId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(foszamId))
        {
            MIG_TobbesService service = eMigrationService.ServiceFactory.GetMIG_TobbesService();

            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            MIG_TobbesSearch search = (MIG_TobbesSearch)Search.GetSearchObject(Page, new MIG_TobbesSearch());
            search.OrderBy = Search.GetOrderBy("TobbesGridView", ViewState, SortExpression, SortDirection);

            search.MIG_Foszam_Id.Value = foszamId;
            search.MIG_Foszam_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            //search.MIG_Varos_Id.Value = "9B321666-CCD9-4747-96F8-5AC2A0216196";
            //search.MIG_Varos_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result res = service.GetAll(ExecParam, search);
            UI.GridViewFill(TobbesGridView, res, TobbesSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Label2);

            int sorszam = TobbesGridView.Rows.Count;
            ui.SetClientScriptToGridViewSelectDeSelectButton(TobbesGridView);

        }
        else
        {
            ui.GridViewClear(TobbesGridView);
        }
    }

    void TobbesSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        TobbesGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
    }

    protected void TobbesUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    if (TabContainer1.ActiveTab.Equals(TobbesTabPanel))
                    {
                        TobbesGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// Törli a CsoporttagokGridView -ban kijelölt elemeket
    /// </summary>
    private void deleteSelectedTobbes()
    {
        //if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo_SzerepkorInvalidate"))
        //{
        //    List<string> deletableItemsList = ui.GetGridViewSelectedRows(TobbesGridView, EErrorPanel1, ErrorUpdatePanel);
        //    KRT_Felhasznalo_SzerepkorService service = eAdminService.ServiceFactory.GetKRT_Felhasznalo_SzerepkorService();
        //    ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
        //    for (int i = 0; i < deletableItemsList.Count; i++)
        //    {
        //        execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
        //        execParams[i].Record_Id = deletableItemsList[i];
        //    }
        //    Result result = service.MultiInvalidate(execParams);
        //    if (!String.IsNullOrEmpty(result.ErrorCode))
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        //        ErrorUpdatePanel.Update();
        //    }
        //    else
        //    {
        //        /** 
        //          * <FIGYELEM!!!> 
        //          * Ez nem másolandó át a többi formra, ez csak itt kell!!!
        //          * (Ezzel elõidézünk egy funkció- és szerepkörlista-frissítést
        //          *  az összes felhasználó Session-jeiben)
        //          */
        //        FunctionRights.RefreshFunkciokLastUpdatedTimeInCache(Page);
        //        /**
        //         * </FIGYELEM>
        //         */
        //    }
        //}
        //else
        //{
        //    UI.RedirectToAccessDeniedErrorPage(Page);
        //}
    }

    protected void TobbesGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(TobbesGridView, selectedRowNumber, "check");
        }
    }

    private void TobbesGridView_RefreshOnClientClicks(string szerepkorId)
    {
        string id = szerepkorId;
        if (!String.IsNullOrEmpty(id))
        {
            TobbesSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("TobbesForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, TobbesUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            TobbesSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("TobbesForm.aspx"
                 , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, TobbesUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void TobbesGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = TobbesGridView.PageIndex;

        TobbesGridView.PageIndex = TobbesSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != TobbesGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, TobbesCPE);
            TobbesGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
        }
        else
        {
            UI.GridViewSetScrollable(TobbesSubListHeader.Scrollable, TobbesCPE);
        }
        TobbesSubListHeader.PageCount = TobbesGridView.PageCount;
        TobbesSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(TobbesGridView);
    }

    void TobbesSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(TobbesSubListHeader.RowCount);
        TobbesGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView));
    }

    protected void TobbesGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        TobbesGridViewBind(UI.GetGridViewSelectedRecordId(MIG_FoszamGridView)
             , e.SortExpression, UI.GetSortToGridView("TobbesGridView", ViewState, e.SortExpression));
    }

    #endregion

    private void UgyiratPotloNyomtatasa()
    {
        List<string> selectedItemsList = ui.GetGridViewSelectedRows(MIG_FoszamGridView, EErrorPanel1, ErrorUpdatePanel);

        string js1 = "";
        if (selectedItemsList == null || selectedItemsList.Count < 1)
        {
            js1 = "alert('Nincs a kijelölt tételek között kikölcsönzött!')";
        }
        else
        {
            string json = new JavaScriptSerializer().Serialize(selectedItemsList);
            js1 = "openUgyiratPotloLap(" + json + ");";
        }
        ScriptManager.RegisterStartupScript(Page, this.GetType(), "UgyiratPotlo", js1, true);
    }

    protected string GetUtoiratLink(string csatolva_Id, string mergeFoszam_Csatolt, string edok_Utoirat_Id, string edok_Utoirat_Azon)
    {
        string link = String.Empty;

        if (!String.IsNullOrEmpty(csatolva_Id))
        {
            link = String.Format("<a href =\"Foszamlist.aspx?Id={0}\" style=\"text-decoration:underline\">{1}</a>", csatolva_Id, mergeFoszam_Csatolt);
        }

        if (!String.IsNullOrEmpty(edok_Utoirat_Id))
        {
            string url = String.Format("{0}UgyUgyiratokForm.aspx?Command=View&Id={1}", eRecord.ERecordWebsiteUrl, edok_Utoirat_Id);
            string onclick = JavaScripts.SetOnCLientClick_NoPostBack(url, String.Empty, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);
            link += String.Format("<a onclick=\"{0}\" style=\"text-decoration:underline;cursor:pointer\">{1}</a>", onclick, edok_Utoirat_Azon); ;
        }

        return link;
    }
}
