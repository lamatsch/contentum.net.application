﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;

public partial class FoszamListPrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("MIG_FoszamGridView", ViewState, ""); // "Id" helyett UI_YEAR DESC, UI_NUM DESC, UI_SAV DESC
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("MIG_FoszamGridView", ViewState);

        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        MIG_FoszamSearch search = (MIG_FoszamSearch)Search.GetSearchObject(Page, new MIG_FoszamSearch());
        search.OrderBy = Search.GetOrderBy("MIG_FoszamGridView", ViewState, sortExpression, sortDirection);

        //search.Csatolva_Rendszer.Value = "";
        //search.Csatolva_Rendszer.Operator = Contentum.eQuery.Query.Operators.isnull;
        search.Selejtezve.Value = "";
        search.Selejtezve.Operator = Contentum.eQuery.Query.Operators.isnull;
        //search.UI_IRSZ.Value = "";
        //search.UI_IRSZ.Operator = Contentum.eQuery.Query.Operators.notnull;

        search.TopRow = 2000;

        //search.ExtendedMIG_SavSearch.WhereByManual = Search.GetIktatoKonyvekSearchWhere(Page, EErrorPanel1);
        //search.WhereByManual += Search.GetIktatoKonyvekSearchWhere(Page, EErrorPanel1);
        Search.SetIktatokonyvekSearch(Page, search, null);

        //EREC_IraIktatoKonyvekService iktatokonyvekService = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        //EREC_IraIktatoKonyvekSearch iktatokonyvekSearch = new EREC_IraIktatoKonyvekSearch();
        //iktatokonyvekSearch.IktatoErkezteto.Value = "I";
        //iktatokonyvekSearch.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;

        //Result iktatokonyvekResult = iktatokonyvekService.GetAllWithExtensionAndJogosultsag(ExecParam.Clone(), iktatokonyvekSearch, true);
        //if (!string.IsNullOrEmpty(iktatokonyvekResult.ErrorCode))
        //{
        //    //Contentum.eUtility.ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, iktatokonyvekResult);
        //    return;
        //}

        //search.ExtendedMIG_SavSearch.WhereByManual = "";

        //if (iktatokonyvekResult.Ds.Tables[0].Rows.Count == 0)
        //{
        //    search.ExtendedMIG_SavSearch.WhereByManual = " 0=1 ";
        //}

        //foreach (DataRow r in iktatokonyvekResult.Ds.Tables[0].Rows)
        //{
        //    string sav = r["Iktatohely"].ToString().TrimStart("FPH".ToCharArray());
        //    if (string.IsNullOrEmpty(sav)) continue;

        //    if (sav.StartsWith("0") && sav.ToString().Length > 2)
        //        sav = sav.Substring(1, 2);

        //    search.ExtendedMIG_SavSearch.WhereByManual += " PATINDEX('(" + sav + ")%',MIG_Sav.NAME) != 0 OR ";
        //}

        ExecParam.Paging.PageNumber = 0;
        ExecParam.Paging.PageSize = 500;

        //search.ExtendedMIG_SavSearch.WhereByManual = search.ExtendedMIG_SavSearch.WhereByManual.TrimEnd(" OR ".ToCharArray());
        //if (!string.IsNullOrEmpty(search.UI_SAV.Value))
        //{
        //    search.WhereByManual += " MIG_SAV " + search.UI_SAV.Operator + search.UI_SAV.Value;
        //    search.UI_SAV.Value = "";
        //}

        // Lapozás beállítása:
        //UI.SetPaging(ExecParam, ListHeader1);

        Result result = service.GetAll(ExecParam, search);

        if (result.IsError)
        {
            string js = String.Format("alert('Hiba a nyomtatásnál: {0}');", ResultError.GetErrorMessageFromResultObject(result));
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "error", js, true);
            return;
        }

        DataTable table = new DataTable("ParentTable");
        DataColumn column;
        DataRow row;
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Where";
        column.AutoIncrement = false;
        column.Caption = "Where";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        DataColumn[] PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;
        result.Ds.Tables.Add(table);

        row = table.NewRow();
        row["id"] = 0;
        row["Where"] = search.ReadableWhere;
        table.Rows.Add(row);

        result.Ds.Tables.Remove("Table1");

        string xml = result.Ds.GetXml();
        string xsd = result.Ds.GetXmlSchema();

        string templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns0=\"migfsz\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>Kiss Gergely</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>3</o:Revision><o:TotalTime>107</o:TotalTime><o:Created>2008-07-17T09:58:00Z</o:Created><o:LastSaved>2008-10-29T09:52:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>21</o:Words><o:Characters>145</o:Characters><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>165</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Tahoma\"><w:panose-1 w:val=\"020B0604030504040204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"61002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000101FF\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"26E95FF6\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"4C9C4A66\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"0\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Normál\"/><w:rsid w:val=\"004A681A\"/><w:pPr><w:spacing w:after=\"200\" w:line=\"276\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezdés alapbetűtípusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Normál táblázat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"Rácsos táblázat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"009E0533\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"lfej\"><w:name w:val=\"header\"/><wx:uiName wx:val=\"Élőfej\"/><w:basedOn w:val=\"Norml\"/><w:link w:val=\"lfejChar\"/><w:rsid w:val=\"009E0533\"/><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"4536\"/><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"lfejChar\"><w:name w:val=\"Élőfej Char\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:link w:val=\"lfej\"/><w:rsid w:val=\"009E0533\"/></w:style><w:style w:type=\"paragraph\" w:styleId=\"llb\"><w:name w:val=\"footer\"/><wx:uiName wx:val=\"Élőláb\"/><w:basedOn w:val=\"Norml\"/><w:link w:val=\"llbChar\"/><w:rsid w:val=\"009E0533\"/><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"4536\"/><w:tab w:val=\"right\" w:pos=\"9072\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"llbChar\"><w:name w:val=\"Élőláb Char\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:link w:val=\"llb\"/><w:rsid w:val=\"009E0533\"/></w:style><w:style w:type=\"paragraph\" w:styleId=\"Buborkszveg\"><w:name w:val=\"Balloon Text\"/><wx:uiName wx:val=\"Buborékszöveg\"/><w:basedOn w:val=\"Norml\"/><w:link w:val=\"BuborkszvegChar\"/><w:rsid w:val=\"009E0533\"/><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Tahoma\" w:h-ansi=\"Tahoma\" w:cs=\"Tahoma\"/><wx:font wx:val=\"Tahoma\"/><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"BuborkszvegChar\"><w:name w:val=\"Buborékszöveg Char\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:link w:val=\"Buborkszveg\"/><w:rsid w:val=\"009E0533\"/><w:rPr><w:rFonts w:ascii=\"Tahoma\" w:h-ansi=\"Tahoma\" w:cs=\"Tahoma\"/><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Listaszerbekezds\"><w:name w:val=\"List Paragraph\"/><wx:uiName wx:val=\"Listaszerű bekezdés\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"009E0533\"/><w:pPr><w:ind w:left=\"720\"/><w:contextualSpacing/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Stlus1\"><w:name w:val=\"Stílus1\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"008F3F1C\"/><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"18\"/></w:rPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"4098\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:footnotePr><w:footnote w:type=\"separator\"><w:p wsp:rsidR=\"001B6E3E\" wsp:rsidRDefault=\"001B6E3E\" wsp:rsidP=\"009E0533\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:separator/></w:r></w:p></w:footnote><w:footnote w:type=\"continuation-separator\"><w:p wsp:rsidR=\"001B6E3E\" wsp:rsidRDefault=\"001B6E3E\" wsp:rsidP=\"009E0533\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:continuationSeparator/></w:r></w:p></w:footnote></w:footnotePr><w:endnotePr><w:endnote w:type=\"separator\"><w:p wsp:rsidR=\"001B6E3E\" wsp:rsidRDefault=\"001B6E3E\" wsp:rsidP=\"009E0533\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:separator/></w:r></w:p></w:endnote><w:endnote w:type=\"continuation-separator\"><w:p wsp:rsidR=\"001B6E3E\" wsp:rsidRDefault=\"001B6E3E\" wsp:rsidP=\"009E0533\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r><w:continuationSeparator/></w:r></w:p></w:endnote></w:endnotePr><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"009E0533\"/><wsp:rsid wsp:val=\"00060FFF\"/><wsp:rsid wsp:val=\"001B6E3E\"/><wsp:rsid wsp:val=\"003D54D9\"/><wsp:rsid wsp:val=\"003F1417\"/><wsp:rsid wsp:val=\"004A681A\"/><wsp:rsid wsp:val=\"005A2412\"/><wsp:rsid wsp:val=\"005B7167\"/><wsp:rsid wsp:val=\"008F3F1C\"/><wsp:rsid wsp:val=\"00915386\"/><wsp:rsid wsp:val=\"009E0533\"/><wsp:rsid wsp:val=\"00B11478\"/></wsp:rsids></w:docPr><w:body><ns0:NewDataSet><w:p wsp:rsidR=\"009E0533\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"009E0533\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>(</w:t></w:r><ParentTable><Where/></ParentTable><w:r><w:t>)</w:t></w:r></w:p><w:p wsp:rsidR=\"009E0533\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"009E0533\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w=\"0\" w:type=\"auto\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"392\"/><w:gridCol w:w=\"522\"/><w:gridCol w:w=\"612\"/><w:gridCol w:w=\"888\"/><w:gridCol w:w=\"671\"/><w:gridCol w:w=\"851\"/><w:gridCol w:w=\"1134\"/><w:gridCol w:w=\"850\"/><w:gridCol w:w=\"797\"/><w:gridCol w:w=\"854\"/><w:gridCol w:w=\"759\"/><w:gridCol w:w=\"908\"/><w:gridCol w:w=\"916\"/><w:gridCol w:w=\"548\"/></w:tblGrid><w:tr wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidTr=\"005B7167\"><w:tc><w:tcPr><w:tcW w:w=\"392\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Ssz</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"522\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Sáv</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"612\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Év</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"888\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Főszám</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"671\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Irattári jel</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"851\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Ügyfél</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1134\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Tárgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"850\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Azonosító</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"797\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Előirat</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"854\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Utóirat</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"759\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Irat helye</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"908\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Irattárba</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"916\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Skontró vége</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"548\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005B7167\"><w:rPr><w:b/></w:rPr><w:t>Alszám</w:t></w:r></w:p></w:tc></w:tr><Table ns0:repeater=\"true\"><w:tr wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidTr=\"005B7167\"><w:tc><w:tcPr><w:tcW w:w=\"392\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc><UI_SAV><w:tc><w:tcPr><w:tcW w:w=\"522\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></UI_SAV><UI_YEAR><w:tc><w:tcPr><w:tcW w:w=\"612\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></UI_YEAR><UI_NUM><w:tc><w:tcPr><w:tcW w:w=\"888\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></UI_NUM><IRJ2000><w:tc><w:tcPr><w:tcW w:w=\"671\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></IRJ2000><UI_NAME><w:tc><w:tcPr><w:tcW w:w=\"851\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></UI_NAME><MEMO><w:tc><w:tcPr><w:tcW w:w=\"1134\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></MEMO><UI_OT_ID><w:tc><w:tcPr><w:tcW w:w=\"850\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></UI_OT_ID><Eloirat_Azon><w:tc><w:tcPr><w:tcW w:w=\"797\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"00B11478\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></Eloirat_Azon><Utoirat_Azon><w:tc><w:tcPr><w:tcW w:w=\"854\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"00060FFF\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></Utoirat_Azon><UGYHOL><w:tc><w:tcPr><w:tcW w:w=\"759\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></UGYHOL><IRATTARBA><w:tc><w:tcPr><w:tcW w:w=\"908\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></IRATTARBA><SCONTRO><w:tc><w:tcPr><w:tcW w:w=\"916\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></SCONTRO><ALNO><w:tc><w:tcPr><w:tcW w:w=\"548\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"005B7167\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"005B7167\"><w:pPr><w:pStyle w:val=\"Stlus1\"/></w:pPr></w:p></w:tc></ALNO></w:tr></Table></w:tbl><w:p wsp:rsidR=\"004A681A\" wsp:rsidRDefault=\"005A2412\"/></ns0:NewDataSet><w:sectPr wsp:rsidR=\"004A681A\" wsp:rsidSect=\"009E0533\"><w:hdr w:type=\"odd\"><w:p wsp:rsidR=\"009E0533\" wsp:rsidRPr=\"009E0533\" wsp:rsidRDefault=\"009E0533\" wsp:rsidP=\"009E0533\"><w:pPr><w:pStyle w:val=\"lfej\"/><w:jc w:val=\"center\"/><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009E0533\"><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>Migráció - főszám</w:t></w:r></w:p></w:hdr><w:ftr w:type=\"odd\"><w:p wsp:rsidR=\"009E0533\" wsp:rsidRDefault=\"005A2412\"><w:pPr><w:pStyle w:val=\"llb\"/><w:jc w:val=\"right\"/></w:pPr><w:r><w:rPr><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:fldChar w:fldCharType=\"begin\"/></w:r><w:r wsp:rsidR=\"009E0533\"><w:rPr><w:b/></w:rPr><w:instrText>PAGE</w:instrText></w:r><w:r><w:rPr><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:fldChar w:fldCharType=\"separate\"/></w:r><w:r wsp:rsidR=\"00B11478\"><w:rPr><w:b/><w:noProof/></w:rPr><w:t>1</w:t></w:r><w:r><w:rPr><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:fldChar w:fldCharType=\"end\"/></w:r><w:r wsp:rsidR=\"009E0533\"><w:t> / </w:t></w:r><w:r><w:rPr><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:fldChar w:fldCharType=\"begin\"/></w:r><w:r wsp:rsidR=\"009E0533\"><w:rPr><w:b/></w:rPr><w:instrText>NUMPAGES</w:instrText></w:r><w:r><w:rPr><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:fldChar w:fldCharType=\"separate\"/></w:r><w:r wsp:rsidR=\"00B11478\"><w:rPr><w:b/><w:noProof/></w:rPr><w:t>1</w:t></w:r><w:r><w:rPr><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:fldChar w:fldCharType=\"end\"/></w:r></w:p><w:p wsp:rsidR=\"009E0533\" wsp:rsidRDefault=\"009E0533\"><w:pPr><w:pStyle w:val=\"llb\"/></w:pPr></w:p></w:ftr><w:pgSz w:w=\"11906\" w:h=\"16838\"/><w:pgMar w:top=\"720\" w:right=\"720\" w:bottom=\"720\" w:left=\"720\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";

        Contentum.eTemplateManager.Service.TemplateManagerService tms = Contentum.eUtility.eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        result = tms.GetWordDocument_DataSet(templateText, result, false);

        if (!result.IsError)
        {
            byte[] res = (byte[])result.Record;

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/msword";
            Response.OutputStream.Write(res, 0, res.Length);
            Response.OutputStream.Flush();
            Response.End();
        }
        else
        {
            if (result.ErrorCode == "99999" //"TimeoutExpired"
                || result.ErrorCode == "99998" //"NoFreThread"
            )
            {
                string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
                return;
            }
            else
            {
                string js = String.Format("alert('Hiba a nyomtatásnál: {0}');", ResultError.GetErrorMessageFromResultObject(result));
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "error", js, true);
                return;

            }

        }
    }
}
