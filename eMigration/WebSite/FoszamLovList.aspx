<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FoszamLovList.aspx.cs" Inherits="FoszamLovList" Title="Untitled Page" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/OrszagTextBox.ascx" TagName="OrszagTextBox" TagPrefix="uc3" %> 
<%@ Register Src="Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc4" %>
<%@ Register Src="~/Component/IranyitoszamTextBox.ascx" TagName="IranyitoszamTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/KozteruletTextBox.ascx" TagName="KozteruletTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/KozteruletTipusTextBox.ascx" TagName="KozteruletTipusTextBox" TagPrefix="uc7" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc8" %>
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc1:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,MIG_FoszamLovListHeaderTitle%>" /> 
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                          <br />
                            <span class="foszamLovList_SearchText">
                                <asp:Label ID="labelSearchTextPrefix" runat="server" Text="Keres�si param�terek: " style="font-weight:bold" Visible="false"></asp:Label>
                                <asp:Label ID="labelSearchText" runat="server" Text="" Visible="false"></asp:Label>
                            </span>
                            <br />
                            <table style="width: 90%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr id="trMaxRowWarning" class="LovListWarningRow" runat="server" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <table style="width: 95%" cellspacing="0" cellpadding="0" border="0">
                                                <tr class="urlapSor">
                                                    <td class="mrUrlapCaption_nowidth" style="width:0px">
                                                        <asp:Label ID="labelEv" runat="server" Text="�v: " Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapInputRovid">
                                                        <asp:TextBox ID="textEv" runat="server" Width="100px"></asp:TextBox>
                                                    </td>
                                                    <td class="mrUrlapCaption_nowidth">
                                                        <asp:Label ID="labelSav" runat="server" Text="Iktat�k�nyv: " Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapInputRovid">
                                                        <asp:TextBox ID="textSav" runat="server" Width="100px"></asp:TextBox>
                                                    </td>
                                                    <td class="mrUrlapCaption_nowidth">
                                                        <asp:Label ID="labelFoszam" runat="server" Text="F�sz�m: " Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapInputRovid">
                                                        <asp:TextBox ID="textFoszam" runat="server" Width="100px"></asp:TextBox>
                                                    </td>
                                                    <td style="position:relative;top:3px;text-align:right;">
                                                        <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"
                                                            AlternateText="Keres�s"></asp:ImageButton>
                                                        <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"
                                                            AlternateText="R�szletes keres�s"></asp:ImageButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Panel Style="padding-top: 3px" ID="panelCimSearch" runat="server" Width="97.5%">
                                                <div style="display: inline; margin-left: -9px; position: absolute">
                                                    <img style="cursor: pointer" id="btnCpeCimSearch" src="images/hu/Grid/minus.gif"
                                                        runat="server" />
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="LovListCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel1" runat="server" Width="95%">
                                                <table style="width: 102%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align: top; text-align: left" id="headerBackGround" class="GridViewHeaderBackGroundStyle"
                                                                runat="server">
                                                                <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                                <asp:HiddenField ID="GridViewSelectedIndex" runat="server"></asp:HiddenField>
                                                                <asp:HiddenField ID="GridViewSelectedId" runat="server"></asp:HiddenField>
                                                                <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                                    OnPreRender="GridViewSearchResult_PreRender" OnSelectedIndexChanging="GridViewSearchResult_SelectedIndexChanging"
                                                                    CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                                    PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                                    DataKeyNames="Id" PageSize="5000" OnRowDataBound="GridViewSearchResult_RowDataBound">
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                    <SelectedRowStyle CssClass="GridViewLovListSelectedRowStyle" />
                                                                    <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="UI_YEAR" HeaderText="&#201;v" SortExpression="UI_YEAR">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <HeaderStyle Width="9%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="UI_SAV" HeaderText="Iktat�k�nyv" SortExpression="UI_SAV">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <HeaderStyle Width="9%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="UI_NUM" HeaderText="F�sz&#225;m" SortExpression="UI_NUM">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <HeaderStyle Width="10%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="IRJ2000" HeaderText="Iratt&#225;ri Jel" SortExpression="IRJ2000">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <HeaderStyle Width="12%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="UI_NAME" HeaderText="&#220;gyf&#233;l" SortExpression="UI_NAME">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <HeaderStyle Width="20%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="MEMO" HeaderText="T&#225;rgy" SortExpression="MEMO">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <HeaderStyle Width="15%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="UI_OT_ID" SortExpression="UI_OT_ID" HeaderText="Azonos�t�">
                                                                            <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                            <HeaderStyle Width="15%" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                            </HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ALNO" SortExpression="ALNO" HeaderText="Alsz�mok">
                                                                            <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                            <HeaderStyle Width="10%" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                            </HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Eloado_Nev" SortExpression="Eloado_Nev" HeaderText="�gyint�z�">
                                                                            <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                                                            <HeaderStyle Width="10%" CssClass="GridViewBorderHeader" HorizontalAlign="Left">
                                                                            </HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader"
                                                                            HeaderStyle-Width="100px">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="headerIratHelye" runat="server" Text="Irat helye" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="labelIratHelye" runat="server" Text='<%#Eval("UGYHOL") %>' />
                                                                                <asp:Label ID="labelSelejtezve" runat="server" CssClass="GridViewLovListInvisibleCoulumnStyle"
                                                                                    Text='<%# !string.IsNullOrEmpty(Eval("UGYHOL").ToString()) && Eval("UGYHOL").ToString() == "S" ? "S" : "" %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="SCONTRO" SortExpression="SCONTRO" HeaderText="Skontr� v�ge">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="10%"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="IRATTARBA" SortExpression="IRATTARBA" HeaderText="Iratt�rba">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="10%"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Conc" SortExpression="Conc" HeaderText="Konkaten�lt t�rgy">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="IrattariTetelszam" SortExpression="IrattariTetelszam"
                                                                            HeaderText="Iratt�ri t�telsz�m">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="MegkulJelzes" SortExpression="MegkulJelzes" HeaderText="Megk�l. jelz�s">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField HeaderText="Ut�irat">
                                                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="labelUtoiratNev" runat="server" Text='<%#                                        
                                                                        string.Concat("",                                  
                                                                          !string.IsNullOrEmpty(Eval("Csatolva_Id").ToString()) ? "<a href=\"javascript:void(0)\" onclick=\"__doPostBack("+ this.apostrof + Panel1.ClientID + this.apostrof + "," + this.apostrof + "SelectId_" + Eval("Csatolva_Id") as string + this.apostrof + ")\" style=\"text-decoration:underline\">"
                                                                            + Eval("MergeFoszam_Csatolt") as string +"<a />" : ""
                                                                            , !string.IsNullOrEmpty(Eval("Edok_Utoirat_Azon").ToString()) ? Eval("Edok_Utoirat_Azon").ToString() : "") %>' />
                                                                                <asp:Label ID="labelCsatolva_Id" runat="server" CssClass="GridViewLovListInvisibleCoulumnStyle"
                                                                                    Text='<%# Eval("Csatolva_Id") %>' />
                                                                                <asp:Label ID="labelEdok_Utoirat_Id" runat="server" CssClass="GridViewLovListInvisibleCoulumnStyle"
                                                                                    Text='<%# Eval("Edok_Utoirat_Id") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="UGYHOL" SortExpression="UGYHOL" HeaderText="Irat helye">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <PagerSettings Visible="False" />
                                                                    <EmptyDataTemplate>
                                                                        <asp:Label runat="server" Text="<%$Resources:Search,NoSearchResult%>"></asp:Label>
                                                                    </EmptyDataTemplate>
                                                                    <AlternatingRowStyle CssClass="GridViewAlternateRowStyle" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <br />
                                            <asp:ImageButton ID="ImageButtonReszletesAdatok" onmouseover="swapByName(this.id,'reszletesadatok2.png')"
                                                onmouseout="swapByName(this.id,'reszletesadatok.png')" runat="server" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                                AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc2:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>

