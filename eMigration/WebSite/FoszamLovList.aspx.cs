using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
//using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUIControls;
using System.Data;
using System.Text;


using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;

using Contentum.eRecord.Service;

public partial class FoszamLovList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    private bool disable_refreshLovList = false;
    private string filterType = "All"; // lehets�ges �rt�kek: "All" (==""), "Szervezet", "Szemely"

    private const int tabIndexPostai = 0;
    private const int tabIndexEgyeb = 1;
    
    private GridViewRow prevouseRow = null;
    private bool alternateRow = false;
    private const string alternateRowStyle = "GridViewAlternateRowStyle";
    private const string rowStyle = "GridViewRowStyle";
    private const string selectedRowStyle = "GridViewLovListSelectedRowStyle";

    private string selectedId = "";
    // az .aspx oldalon kellett
    protected string apostrof = "'";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FoszamList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Logger.Info("FoszamLovList/Page_Init kezdete.");
        registerJavascripts();
        filterType = Request.QueryString.Get(QueryStringVars.Filter);
        if (filterType == null) filterType = "";

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
        Logger.Info("FoszamLovList/Page_Init v�ge.");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Logger.Info("FoszamLovList/Page_Load kezdete.");

        LovListHeader1.UpdateProgress.DisplayAfter = 100;

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("FoszamForm.aspx", "", GridViewSelectedId.ClientID, 
            Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("FoszamSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        //if (!IsPostBack)
        //{
        //    FillGridViewSearchResult(TextBoxSearch.Text, false);
        //}

        LovListHeader1.ErrorPanel.Visible = false;
        LovListHeader1.ErrorUpdatePanel.Update();

        ScriptManager1.SetFocus(ButtonSearch);

        if (!IsPostBack)
        {
            InitComponentsFromQueryString();
        }

        Logger.Info("FoszamLovList/Page_Load v�ge.");
    }

    private void InitComponentsFromQueryString()
    {
        string ev = Request.QueryString.Get(QueryStringVars.ElozmenySearchParameters.Ev);

        if (!String.IsNullOrEmpty(ev))
        {
            textEv.Text = ev;
        }

        string iktatokonyvValue = Request.QueryString.Get(QueryStringVars.ElozmenySearchParameters.IktatokonyvValue);

        if (!String.IsNullOrEmpty(iktatokonyvValue))
        {
            string sav = Contentum.eRecord.BaseUtility.IktatoKonyvek.GetSavFromIktatokonyvValue(iktatokonyvValue);
            textSav.Text = sav;
        }

        string foszam = Request.QueryString.Get(QueryStringVars.ElozmenySearchParameters.Foszam);

        if (!String.IsNullOrEmpty(foszam))
        {
            textFoszam.Text = foszam;
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Logger.Info("Keres�s gomb Click.");
        JavaScripts.ResetScroll(Page, LovListCPE);
        FillGridViewSearchResult(false);
    }

    private KRT_CimekSearch GetCimekFromComponents()
    {
        KRT_CimekSearch search = new KRT_CimekSearch();


        return search;
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        Logger.Info("Gridview felt�lt�se keres�si eredm�nnyel!");
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
        MIG_FoszamSearch search = null;

        if (searchObjectFromSession == true)
        {
            Logger.Info("Search object session -b�l j�n.");
            search = (MIG_FoszamSearch)Contentum.eMigration.Utility.Search.GetSearchObject(Page, new MIG_FoszamSearch());
            labelSearchText.Visible = true;
            labelSearchTextPrefix.Visible = true;
            labelSearchText.Text = search.ReadableWhere.TrimEnd(Search.whereDelimeter.ToCharArray());
        }
        else
        {
            Logger.Info("Search object nem session -b�l j�n.");
            search = new MIG_FoszamSearch();

            labelSearchText.Visible = false;
            labelSearchTextPrefix.Visible = false;            
            if (!String.IsNullOrEmpty(textEv.Text))
            {
                search.UI_YEAR.Operator = Search.GetOperatorByLikeCharater(textEv.Text);
                search.UI_YEAR.Value = textEv.Text;
            }
            if (!String.IsNullOrEmpty(textSav.Text))
            {
                //    search.UI_SAV.Name = "SUBSTRING(MIG_Sav.NAME,2, CHARINDEX(')', MIG_Sav.NAME) - 2 )";
                //    search.UI_SAV.Type = "String";
                //    search.UI_SAV.Value = textSav.Text;
                //    search.UI_SAV.Operator = Search.GetOperatorByLikeCharater(textSav.Text);

                search.EdokSav.Value = textSav.Text;
                search.EdokSav.Operator = Search.GetOperatorByLikeCharater(textSav.Text);
            }
            if (!String.IsNullOrEmpty(textFoszam.Text))
            {
                search.UI_NUM.Operator = Search.GetOperatorByLikeCharater(textFoszam.Text);
                search.UI_NUM.Value = textFoszam.Text;
            }

        }

        // Ha r�kattintott egy ut�iratra, csak azt hozzuk:
        if (!String.IsNullOrEmpty(selectedId))
        {
            search = new MIG_FoszamSearch();
            search.Id.Value = selectedId;
            search.Id.Operator = Query.Operators.equals;
        }

        //search.Csatolva_Rendszer.Value = "";
        //search.Csatolva_Rendszer.Operator = Contentum.eQuery.Query.Operators.isnull;
        search.Selejtezve.Value = "";
        search.Selejtezve.Operator = Contentum.eQuery.Query.Operators.isnull;


    /*    if (filterType == Constants.FilterType.Ugyiratok.Szereles)
        {
            // hozzuk a szerelteket is, csak kisz�rk�tve
            //search.Edok_Utoirat_Id.Operator = Query.Operators.isnull;            
            search.WhereByManual = "UI_YEAR < '"+Request.QueryString.Get(QueryStringVars.ElozmenySearchParameters.Ev)+"'";
        }*/

        // search.OrderBy = "UI_NUM";

        ExecParam ExecParam = Contentum.eMigration.Utility.UI.SetExecParamDefault(Page, new ExecParam());

        //search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);
        search.TopRow = 250;

        //// CR#2021 (EB 2009.04.08): a k�nnyebb karbantart�s �s egys�ges kezel�s miatt �temelve a Utilitybe (Search)
        //EREC_IraIktatoKonyvekService iktatokonyvekService = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        //EREC_IraIktatoKonyvekSearch iktatokonyvekSearch = new EREC_IraIktatoKonyvekSearch();
        //iktatokonyvekSearch.IktatoErkezteto.Value = "I";
        //iktatokonyvekSearch.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;

        //Result iktatokonyvekResult = iktatokonyvekService.GetAllWithExtensionAndJogosultsag(ExecParam.Clone(), iktatokonyvekSearch, true);
        //if (!string.IsNullOrEmpty(iktatokonyvekResult.ErrorCode))
        //{
        //    Contentum.eUtility.ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, iktatokonyvekResult);
        //    return;
        //}

        //search.ExtendedMIG_SavSearch.WhereByManual = "";

        //if (iktatokonyvekResult.Ds.Tables[0].Rows.Count == 0)
        //{
        //    search.ExtendedMIG_SavSearch.WhereByManual = " 0=1 ";
        //}
        //else
        //{
        // ha ki van t�ltve a S�v egy konkr�t �rt�kkel, akkor csak ahhoz f�zz�k hozz� a "PATINDEX" vizsg�latot
        // tov�bb� minden s�v minta csak egyszer szerepel
        //string extended_sav_where = "";
        //List<string> savList = new List<string>();
        string savFilter = search.UI_SAV.Value.Trim();
        if (search.UI_SAV.Operator != Contentum.eQuery.Query.Operators.equals)
        {
            savFilter = "";
        }

        //    // neh�z lenne k�zvetlen�l lek�r�skor sz�rni, mert lehet FPHxxx, FPH0xx is, ez�rt v�gigmegy�nk a list�n
        //    foreach (DataRow r in iktatokonyvekResult.Ds.Tables[0].Rows)
        //    {
        //        string sav = r["Iktatohely"].ToString().TrimStart("FPH".ToCharArray());
        //        if (string.IsNullOrEmpty(sav)) continue;

        //        if (sav.StartsWith("0") && sav.ToString().Length > 2)
        //            sav = sav.Substring(1, 2);

        //        if (!String.IsNullOrEmpty(savFilter) && sav != savFilter) continue;

        //        if (!savList.Contains(sav))
        //        {
        //            extended_sav_where += " PATINDEX('(" + sav + ")%',MIG_Sav.NAME) != 0 OR ";
        //            savList.Add(sav);
        //        }
        //    }

        //if (!String.IsNullOrEmpty(savFilter) && sav != savFilter) continue;
        //extended_sav_where = Search.GetIktatoKonyvekSearchWhere(Page, LovListHeader1.ErrorPanel);

        //if (!String.IsNullOrEmpty(savFilter) && String.IsNullOrEmpty(extended_sav_where))
        //{
        //    // ha volt sz�r�s a s�vra, de nem szerepelt a visszaadott iktat�k�nyv list�n, akkor ellentmond�s (nincs joga)
        //    search.ExtendedMIG_SavSearch.WhereByManual = " 0=1 ";
        //}
        //else
        //{
        //    search.ExtendedMIG_SavSearch.WhereByManual = extended_sav_where.TrimEnd(" OR ".ToCharArray());
        //}
        //}

        //search.ExtendedMIG_SavSearch.WhereByManual = Search.GetIktatoKonyvekSearchWhere(Page, savFilter, LovListHeader1.ErrorPanel);
        Search.SetIktatokonyvekSearch(Page, search, savFilter, LovListHeader1.ErrorPanel);
        Result result = service.GetAll(ExecParam, search);

        GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
        Logger.Info("Gridview felt�lt�s v�get �rt!");
    }

    //r�szletes keres�s ut�n az lovlist keres�si mez�inek alap�llapotba �ll�t�sa
    private void SetDefaultState()
    {
        textEv.Text = String.Empty;
        textSav.Text = String.Empty;
        textFoszam.Text = String.Empty;
    }

    private void GridViewFill(GridView gridView, Result result, eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
    {
        Logger.Info("GridView felt�lt�s kezdete");
        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            // Hiba kezeles
            Logger.Error("Hiba a felt�lt�s sor�n:" + result.ErrorMessage);
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
            if (parentErrorUpdatePanel != null)
            {
                parentErrorUpdatePanel.Update();
            }
        }

        //DataSet res = new DataSet();
        //DataTable table = new DataTable();
        //table.Columns.Add("Id", typeof(Guid));
        //table.Columns.Add("UI_SAV");
        //table.Columns.Add("UI_YEAR");
        //table.Columns.Add("UI_NUM");
        //table.Columns.Add("IRJ2000");
        //table.Columns.Add("UI_NAME");
        //table.Columns.Add("MEMO");
        //res.Tables.Add(table);


        if (result.Ds != null)
        {
            Logger.Info("Dataset felt�lt�s kezdete");
            try
            {
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    //foreach (DataRow row in result.Ds.Tables[0].Rows)
                    //{
                    //    string UI_SAV = row["UI_SAV"].ToString();
                    //    string UI_YEAR = row["UI_YEAR"].ToString();
                    //    string UI_NUM = row["UI_NUM"].ToString();
                    //    string IRJ2000 = row["IRJ2000"].ToString();
                    //    string UI_NAME = row["UI_NAME"].ToString();
                    //    string MEMO = row["MEMO"].ToString();
                    //    DataRow newRow = res.Tables[0].NewRow();
                    //    newRow["Id"] = row["Id"];
                    //    newRow["UI_SAV"] = UI_SAV;
                    //    newRow["UI_YEAR"] = UI_YEAR;
                    //    newRow["UI_NUM"] = UI_NUM;
                    //    newRow["IRJ2000"] = IRJ2000;
                    //    newRow["UI_NAME"] = UI_NAME;
                    //    newRow["MEMO"] = MEMO;
                    //    res.Tables[0].Rows.Add(newRow);
                    //}
                }
                else
                {
                    HtmlTableCell td = (HtmlTableCell)Panel1.FindControl("headerBackGround");
                    Panel1.Width = Unit.Percentage(96.6);
                    gridView.BorderWidth = Unit.Pixel(0);
                }
            }
            catch (Exception e)
            {
                Logger.Error("Hiba a dataset felt�lt�s�ben:" + e.Message);
                ResultError.DisplayErrorOnErrorPanel(errorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"), e.Message);
            }
            Logger.Info("Dataset felt�lt�s befejez�d�tt!");
        }
        else
        {
            Logger.Warn("Kapott result �res!");
            HtmlTableCell td = (HtmlTableCell)Panel1.FindControl("headerBackGround");
            Panel1.Width = Unit.Percentage(96.6);
            gridView.BorderWidth = Unit.Pixel(0);
        }

        gridView.DataSource = result.Ds;
        gridView.DataBind();
        Logger.Info("GridView felt�lt�s befejez�d�tt!");
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        Logger.Info("LovListFooter_ButtonsClick: " + e.CommandName.ToString());
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;
                    Logger.Info("Visszat�r�si �rt�k �ssze�ll�t�sa: t�rgy (s�v-f�sz�m/�v) -form�tumban");
                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string ev = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);
                    string sav = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 2);
                    string foszam = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 3);
                    string targy = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 6);

                    string skontrovege = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 11);
                    string irattarba = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 12);
                    string ugyhol = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 17);

                    // ne legyen �res �rt�k, mert az irat hely�t nem adjuk �t
                    if (ugyhol == Constants.MIG_IratHelye.Irattarban && String.IsNullOrEmpty(irattarba))
                    {
                        irattarba = " ";
                    }
                    else if (ugyhol == Constants.MIG_IratHelye.Skontro && String.IsNullOrEmpty(skontrovege))
                    {
                        skontrovege = " ";
                    }

                    string conc = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 13);
                    string irattaritetelszam = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 14);
                    string megkuljelzes = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 15);
                    string irj2000 = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 4);
                    // BUG_13049
                    // t�rgy
                    string memo = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 6); 

                    //string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 4);
                    //string selectedText = targy;// +" (" + sav + "-" + foszam + "/" + ev + ")";
                    string azonositoText = Contentum.eUtility.RegiAdatAzonosito.GetAzonosito(sav, ev, foszam);
                    string selectedText = azonositoText;                    
                    JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);                    
                    // azonos�t�t, iratt�ri t�telsz�mot �s iktat�k�nyv megk�l�nb�ztet� jelz�st k�l�n kell visszak�ldeni:
                    Dictionary<string, string> azonositoDictionary = new Dictionary<string, string>();
                    if (!string.IsNullOrEmpty(Page.Request.QueryString.Get(QueryStringVars.AzonositoTextBox)))
                    {
                        azonositoDictionary.Add(Page.Request.QueryString.Get(QueryStringVars.AzonositoTextBox), azonositoText);
                    }

                    if (!string.IsNullOrEmpty(Page.Request.QueryString.Get(QueryStringVars.IrattariTetelTextBox)))
                    {
                        azonositoDictionary.Add(Page.Request.QueryString.Get(QueryStringVars.IrattariTetelTextBox), irattaritetelszam);
                    }

                    if (!string.IsNullOrEmpty(Page.Request.QueryString.Get(QueryStringVars.MegkulJelzesTextBox)))
                    {
                        azonositoDictionary.Add(Page.Request.QueryString.Get(QueryStringVars.MegkulJelzesTextBox), megkuljelzes);
                    }

                    if (!string.IsNullOrEmpty(Page.Request.QueryString.Get(QueryStringVars.SkontroVegeTextBox)))
                    {
                        azonositoDictionary.Add(Page.Request.QueryString.Get(QueryStringVars.SkontroVegeTextBox), skontrovege);
                    }

                    if (!string.IsNullOrEmpty(Page.Request.QueryString.Get(QueryStringVars.IrattarbaTextBox)))
                    {
                        azonositoDictionary.Add(Page.Request.QueryString.Get(QueryStringVars.IrattarbaTextBox), irattarba);
                    }

                    if (!string.IsNullOrEmpty(Page.Request.QueryString.Get("Irj")))
                    {
                        azonositoDictionary.Add(Page.Request.QueryString.Get("Irj"), irj2000);
                    }

                    // BUG_13049
                    if (!string.IsNullOrEmpty(Page.Request.QueryString.Get("UgyiratTargy")))
                    {
                        azonositoDictionary.Add(Page.Request.QueryString.Get("UgyiratTargy"), memo);
                    }
                    if (!string.IsNullOrEmpty(Page.Request.QueryString.Get("IratTargy")))
                    {
                        azonositoDictionary.Add(Page.Request.QueryString.Get("IratTargy"), memo);
                    }


                    if (azonositoDictionary.Count > 0)
                    {
                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, azonositoDictionary, "regiAzonosito");

                        // Nem kell megjelen�teni a Textboxot, mert egy k�l�n sorban jelen�tj�k majd meg az azonos�t�t (__doPostBack ut�n)
                        //                        string js = @"
                        //                        if (window.dialogArguments.parentWindow.document.getElementById('" + Page.Request.QueryString.Get(QueryStringVars.AzonositoTextBox) + @"').parentNode.parentNode != null)
                        //                        {
                        //                            window.dialogArguments.parentWindow.document.getElementById('" + Page.Request.QueryString.Get(QueryStringVars.AzonositoTextBox) + @"').parentNode.parentNode.style.display = 'block';
                        //                        }
                        //                        ";
                        //                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "valami", js, true);
                    }

                    string jsCallbackFunctionName = Request.QueryString.Get(QueryStringVars.ParentWindowCallbackFunction);
                    if (!String.IsNullOrEmpty(jsCallbackFunctionName))
                    {
                        JavaScripts.RegisterParentWindowCallbackFunction(Page, jsCallbackFunctionName);
                    }

                    GridViewSearchResult.Visible = false;
                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                    Logger.Info("Eredm�ny visszaad�sa: " + selectedText);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;
                Logger.Warn("Nincs kiv�lasztott sor!");
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        Logger.Info("FoszamLovList/Page_PreRender kezdete.");
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            
            if (!string.IsNullOrEmpty(eventArgument) && eventArgument.StartsWith("SelectId_"))
            {
                // kinyerj�k az id-t:
                selectedId = eventArgument.Replace("SelectId_", "");
                if (disable_refreshLovList != true)
                {
                    FillGridViewSearchResult(false);
                }
            }
            else
            {
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshLovListByDetailSearch:
                        if (disable_refreshLovList != true)
                        {
                            JavaScripts.ResetScroll(Page, LovListCPE);
                            FillGridViewSearchResult(true);
                        }
                        break;
                }
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, LovListCPE, 12);
        if (!LovListCPE.ScrollContents)
        {
            Panel1.Width = Unit.Percentage(96.6);
        }
        else
        {
            Panel1.Width = Unit.Percentage(95);
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {

                if (filterType == Constants.FilterType.Ugyiratok.Szereles)
                {
                    // Ahol Csatolva_Id vagy Edok_Utoirat_Id ki van t�ltve, kisz�rk�tj�k:

                    List<string> filterLabelIdList = new List<string>();
                    filterLabelIdList.Add("labelCsatolva_Id");
                    filterLabelIdList.Add("labelEdok_Utoirat_Id");                    
                    // Tov�bb� ha selejtezve van, akkor is
                    filterLabelIdList.Add("labelSelejtezve");
                    
                    
                    JavaScripts.SetLovListGridViewRowScripts_disableIfOneIsNotEmpty(row, GridViewSelectedIndex, GridViewSelectedId, filterLabelIdList);                    
                }
                else
                {
                    JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
                }
            }

        }

        base.Render(writer);

    }


    private void registerJavascripts()
    {
        Logger.Info("Java scriptek regisztr�l�sa.");
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void GridViewSearchResult_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridViewSearchResult.Rows[e.NewSelectedIndex].ControlStyle.CssClass = selectedRowStyle;
    }

    //sorok sz�nez�se
    protected void GridViewSearchResult_PreRender(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                if (prevouseRow != null)
                {

                    if (prevouseRow.Cells[0].Text == row.Cells[0].Text)
                    {
                        if (String.IsNullOrEmpty(HttpUtility.HtmlDecode(prevouseRow.Cells[2].Text).Trim()))
                        {
                            prevouseRow.Visible = false;

                        }
                    }
                    else
                    {
                        alternateRow = !alternateRow;
                    }
                }

                if (row.RowState == DataControlRowState.Normal || row.RowState == DataControlRowState.Alternate)
                {
                    if (alternateRow)
                    {
                        row.RowState = DataControlRowState.Alternate;
                        row.ControlStyle.CssClass = alternateRowStyle;
                    }
                    else
                    {
                        row.RowState = DataControlRowState.Normal;
                        row.ControlStyle.CssClass = rowStyle;
                    }
                }

                prevouseRow = row;
            }
        }
    }

    protected void GridViewSearchResult_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        IratHelye.SetGridViewRow(e, "labelIratHelye",Page);
    }

}
