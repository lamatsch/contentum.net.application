<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="FoszamQuickSearch.aspx.cs" Inherits="FoszamSearch" Title="<%$Resources:Search,MIG_FoszamSearchHeaderTitle%>" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc13" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="~/eMigrationComponent/IrattariJelTextBox.ascx" TagName="IrattariJelTextBox" TagPrefix="uc"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="FoszamAdatai_eFormPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                                <asp:Label ID="Label3" runat="server" Text="�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:RequiredNumberBox ID="UI_YEAR_TextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label1" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="UI_SAV_TextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label16" runat="server" Text="F�sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="UI_NUM_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <!-- lara -->
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;</td>
                            <td class="mrUrlapMezo">
                                &nbsp;</td>
                        </tr>
						<!-- lara -->	                
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
        <tr class="tabExtendableListFejlec">
            <td style="text-align: left;">
              <asp:Panel runat="server" ID="AlszamAdataiHeader">
                <asp:ImageButton runat="server" ID="AlszamAdataiCPEButton" ImageUrl="images/hu/Grid/plus.gif" OnClientClick="return false;" />
                &nbsp;
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <eUI:eFormPanel ID="TalalatokSzamaPanel" runat="server">
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                runat="server" />
                        </td>
                    </tr>
                </table>
                </eUI:eFormPanel>
                &nbsp;&nbsp;
                <uc2:SearchFooter ID="SearchFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
