using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eMigration.Utility;
using Contentum.eMigration.Query.BusinessDocuments;

using Contentum.eQuery;

public partial class FoszamSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(MIG_FoszamSearch);

  
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.MIG_FoszamSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            MIG_FoszamSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (MIG_FoszamSearch)Search.GetSearchObject(Page, new MIG_FoszamSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }

           
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
       
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        MIG_FoszamSearch MIG_FoszamSearch = null;
        if (searchObject != null) MIG_FoszamSearch = (MIG_FoszamSearch)searchObject;

        if (MIG_FoszamSearch != null)
        {
          
            //UI_SAV_TextBox.Text = MIG_FoszamSearch.UI_SAV.Value;
            UI_SAV_TextBox.Text = MIG_FoszamSearch.EdokSav.Value;
            UI_YEAR_TextBox.Text = MIG_FoszamSearch.UI_YEAR.Value;
            UI_NUM_TextBox.Text = MIG_FoszamSearch.UI_NUM.Value;
          
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private MIG_FoszamSearch SetSearchObjectFromComponents()
    {
        MIG_FoszamSearch MIG_FoszamSearch = (MIG_FoszamSearch)SearchHeader1.TemplateObject;
        if (MIG_FoszamSearch == null)
        {
            MIG_FoszamSearch = new MIG_FoszamSearch();
        }

        if (!String.IsNullOrEmpty(UI_SAV_TextBox.Text))
        {
            //    MIG_FoszamSearch.UI_SAV.Name = "SUBSTRING(MIG_Sav.NAME,2, CHARINDEX(')', MIG_Sav.NAME) - 2 )";
            //    MIG_FoszamSearch.UI_SAV.Type = "String";
            //    //MIG_FoszamSearch.WhereByManual += " MIG_SAV " + Search.GetOperatorByLikeCharater(UI_SAV_TextBox.Text) + UI_SAV_TextBox.Text;
            //    MIG_FoszamSearch.UI_SAV.Value = UI_SAV_TextBox.Text;
            //    MIG_FoszamSearch.UI_SAV.Operator = Search.GetOperatorByLikeCharater(UI_SAV_TextBox.Text);

            MIG_FoszamSearch.EdokSav.Value = UI_SAV_TextBox.Text;
            MIG_FoszamSearch.EdokSav.Operator = Search.GetOperatorByLikeCharater(UI_SAV_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(UI_YEAR_TextBox.Text))
        {
            MIG_FoszamSearch.UI_YEAR.Value = UI_YEAR_TextBox.Text;
            MIG_FoszamSearch.UI_YEAR.Operator = Search.GetOperatorByLikeCharater(UI_YEAR_TextBox.Text);
        }
       

        if (!String.IsNullOrEmpty(UI_NUM_TextBox.Text))
        {
            MIG_FoszamSearch.UI_NUM.Value = UI_NUM_TextBox.Text;
            MIG_FoszamSearch.UI_NUM.Operator = Search.GetOperatorByLikeCharater(UI_NUM_TextBox.Text);
        }

        return MIG_FoszamSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            MIG_FoszamSearch searchObject = SetSearchObjectFromComponents();
            MIG_FoszamSearch defaultSearchObject = GetDefaultSearchObject();
            if (Search.IsIdentical(searchObject, defaultSearchObject, searchObject.WhereByManual, "")
                && Search.IsIdentical(searchObject.ExtendedMIG_AlszamSearch, defaultSearchObject.ExtendedMIG_AlszamSearch
                ,searchObject.ExtendedMIG_AlszamSearch.WhereByManual, defaultSearchObject.ExtendedMIG_AlszamSearch.WhereByManual)
                && Search.IsIdentical(searchObject.ExtendedMIG_EloadoSearch, defaultSearchObject.ExtendedMIG_EloadoSearch
                , searchObject.ExtendedMIG_EloadoSearch.WhereByManual, defaultSearchObject.ExtendedMIG_EloadoSearch.WhereByManual))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);

            
        }
    }

    private MIG_FoszamSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new MIG_FoszamSearch();
    }

}
