﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eMigration.Utility;
using Contentum.eQuery;
using Contentum.eMigration.Service;
using Contentum.eMigration.Query.BusinessDocuments;

public class ContentumReportViewerCredentials : IReportServerCredentials
{
    [DllImport("advapi32.dll", SetLastError = true)]
    public extern static bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public extern static bool CloseHandle(IntPtr handle);

    [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public extern static bool DuplicateToken(IntPtr ExistingTokenHandle,
        int SECURITY_IMPERSONATION_LEVEL, ref IntPtr DuplicateTokenHandle);

    public ContentumReportViewerCredentials()
    {
    }

    public ContentumReportViewerCredentials(string username)
    {
        this.Username = username;
    }


    public ContentumReportViewerCredentials(string username, string password)
    {
        this.Username = username;
        this.Password = password;
    }


    public ContentumReportViewerCredentials(string username, string password, string domain)
    {
        this.Username = username;
        this.Password = password;
        this.Domain = domain;
    }


    public string Username
    {
        get
        {
            return this.username;
        }
        set
        {
            string username = value;
            if (username.Contains("\\"))
            {
                this.domain = username.Substring(0, username.IndexOf("\\"));
                this.username = username.Substring(username.IndexOf("\\") + 1);
            }
            else
            {
                this.username = username;
            }
        }
    }
    private string username;



    public string Password
    {
        get
        {
            return this.password;
        }
        set
        {
            this.password = value;
        }
    }
    private string password;


    public string Domain
    {
        get
        {
            return this.domain;
        }
        set
        {
            this.domain = value;
        }
    }
    private string domain;




    #region IReportServerCredentials Members

    public bool GetBasicCredentials(out string basicUser, out string basicPassword, out string basicDomain)
    {
        basicUser = username;
        basicPassword = password;
        basicDomain = domain;
        return username != null && password != null && domain != null;
    }

    public bool GetFormsCredentials(out string formsUser, out string formsPassword, out string formsAuthority)
    {
        formsUser = username;
        formsPassword = password;
        formsAuthority = domain;
        return username != null && password != null && domain != null;

    }

    public bool GetFormsCredentials(out Cookie authCookie,
  out string user, out string password, out string authority)
    {
        authCookie = null;
        user = password = authority = null;
        return false;  // Not implemented
    }


    public WindowsIdentity ImpersonationUser
    {
        get
        {

            string[] args = new string[3] { this.Domain.ToString(), this.Username.ToString(), this.Password.ToString() };
            IntPtr tokenHandle = new IntPtr(0);
            IntPtr dupeTokenHandle = new IntPtr(0);

            const int LOGON32_PROVIDER_DEFAULT = 0;
            //This parameter causes LogonUser to create a primary token.
            const int LOGON32_LOGON_INTERACTIVE = 2;
            const int SecurityImpersonation = 2;

            tokenHandle = IntPtr.Zero;
            dupeTokenHandle = IntPtr.Zero;
            try
            {
                // Call LogonUser to obtain an handle to an access token.
                bool returnValue = LogonUser(args[1], args[0], args[2],
                    LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT,
                    ref tokenHandle);

                if (false == returnValue)
                {
                    Console.WriteLine("LogonUser failed with error code : {0}",
                        Marshal.GetLastWin32Error());
                    return null;
                }

                // Check the identity.
                System.Diagnostics.Trace.WriteLine("Before impersonation: "
                    + WindowsIdentity.GetCurrent().Name);


                bool retVal = DuplicateToken(tokenHandle, SecurityImpersonation, ref dupeTokenHandle);
                if (false == retVal)
                {
                    CloseHandle(tokenHandle);
                    Console.WriteLine("Exception in token duplication.");
                    return null;
                }


                // The token that is passed to the following constructor must
                // be a primary token to impersonate.
                WindowsIdentity newId = new WindowsIdentity(dupeTokenHandle);
                WindowsImpersonationContext impersonatedUser = newId.Impersonate();


                // Free the tokens.
                if (tokenHandle != IntPtr.Zero)
                    CloseHandle(tokenHandle);
                if (dupeTokenHandle != IntPtr.Zero)
                    CloseHandle(dupeTokenHandle);

                // Check the identity.
                System.Diagnostics.Trace.WriteLine("After impersonation: "
                    + WindowsIdentity.GetCurrent().Name);

                return newId;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred. " + ex.Message);
            }

            return null;
        }
    }

    public ICredentials NetworkCredentials
    {
        get
        {
            return null;  // Not using NetworkCredentials to authenticate.
        }
    }


    #endregion
}

public partial class FoszamSSRS : Contentum.eUtility.UI.PageBase
{
    private string vis = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        vis = Request.QueryString.Get(QueryStringVars.Filter);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ContentumReportViewerCredentials rvc = new ContentumReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;

            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                ExecParam.Fake = true;

                MIG_FoszamSearch search = (MIG_FoszamSearch)Search.GetSearchObject(Page, new MIG_FoszamSearch());

                search.Selejtezve.Value = "";
                search.Selejtezve.Operator = Contentum.eQuery.Query.Operators.isnull;
                search.TopRow = 0;

                Search.SetIktatokonyvekSearch(Page, search, EErrorPanel1);

                Result result = service.GetAll(ExecParam, search);

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add("order by EdokSav, UI_YEAR DESC, UI_NUM DESC");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        //case "ExtendedMIG_SavWhere":
                        //    ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExtendedMIG_SavWhere"));
                        //    break;
                        case "ExtendedMIG_AlszamWhere":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExtendedMIG_AlszamWhere"));
                            break;
                        case "ExtendedMIG_EloadoWhere":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExtendedMIG_EloadoWhere"));
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("20000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                //ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
			case "CsnyVisibility":
                            if (vis.Substring(2, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "SavVisibility":
                            if (vis.Substring(4, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "EvVisibility":
                            if (vis.Substring(5, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "FoszamVisibility":
                            if (vis.Substring(6, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "IrattariJelVisibility":
                            if (vis.Substring(7, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "UgyfelVisibility":
                            if (vis.Substring(8, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "TargyVisibility":
                            if (vis.Substring(9, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "AzonositoVisibility":
                            if (vis.Substring(10, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "EloadoVisibility":
                            if (vis.Substring(11, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "EloiratVisibility":
                            if (vis.Substring(12, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "UtoiratVisibility":
                            if (vis.Substring(13, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
							
							
                        case "IratHelyeVisibility":
                            if (vis.Substring(14, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
						case "IratTipusaVisibility":
                            if (vis.Substring(15, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
						case "HataridoVisibility":
                            if (vis.Substring(16, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
							
                        case "IrattarbaVisibility":
                            if (vis.Substring(17, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "ScontroVegeVisibility":
                            if (vis.Substring(18, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
			            case "FeladatVisibility":
                            if (vis.Substring(19, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "SzervezetVisibility":
                            if (vis.Substring(20, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                            case "KikeroVisibility":
                            if (vis.Substring(21, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        
                        case "KikeresDatumaVisibility":
                            if (vis.Substring(22, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "MegorzesDatumaVisibility":
                            if (vis.Substring(23, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                            case "AlszamVisibility":
                            if (vis.Substring(24, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                            case "IrattariHelyVisibility":
                            if (vis.Substring(25, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
