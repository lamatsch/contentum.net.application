<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="FoszamSearch.aspx.cs" Inherits="FoszamSearch" Title="<%$Resources:Search,MIG_FoszamSearchHeaderTitle%>" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc13" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="~/eMigrationComponent/IrattariJelTextBox.ascx" TagName="IrattariJelTextBox" TagPrefix="uc"%>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="kod" %>
<%@ Register Src="~/eMigrationComponent/LovTextBox.ascx" TagName="LovTextBox" TagPrefix="ltt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:RequiredFieldValidator ID="FTS_RequiredFieldValidator" runat="server" SetFocusOnError="true"
        ControlToValidate="FTSearch" Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"
        Enabled="False"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
        TargetControlID="FTS_RequiredFieldValidator">
        <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="$addHandler(this.get_target(),'mouseover',OnValidatorOver);"/>
            </Sequence>    
            </OnShow></Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScripts/jquery.js" />
        </Scripts>
    </asp:ScriptManager>

    <style>
        .AutoCompleteExtenderCompletionList {
            max-width: 250px;
            max-height: 250px;
            overflow: hidden;
        }
    </style>

    <script type="text/javascript">
        Sys.Application.add_load(function () {
            $(window).scrollTop(0);
        });
    </script>
    
    <ajaxToolkit:CollapsiblePanelExtender ID="AlszamAdataiCPE" runat="server" TargetControlID="Alszam_eFormPanel"
        CollapsedSize="0" Collapsed="true" 
        AutoCollapse="false" AutoExpand="false"    
        ScrollContents="false"
        ExpandedText="<%$Resources:Search,UI_AlszamAdatai_ExpandedText%>" CollapsedText="<%$Resources:Search,UI_AlszamAdatai_CollapsedText%>" TextLabelID="labelAlszamAdataiShowHide"
        ExpandControlID="AlszamAdataiHeader" CollapseControlID="AlszamAdataiHeader"
        ExpandedImage="images/hu/Grid/minus.gif" CollapsedImage="images/hu/Grid/plus.gif" ImageControlID="AlszamAdataiCPEButton"
        >
    </ajaxToolkit:CollapsiblePanelExtender>
    
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="FoszamAdatai_eFormPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                                <asp:Label ID="Label3" runat="server" Text="�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:RequiredNumberBox ID="UI_YEAR_TextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label1" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="UI_SAV_TextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label16" runat="server" Text="F�sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="UI_NUM_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Iratt�ri Jel:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <%--<uc:IrattariJelTextBox ID="IRJ2000_TextBox" runat="server" SearchMode="true" EvTextBoxControlID="UI_YEAR_TextBox" IsCustomValueEnabled="true"/>--%>
                                <ltt:LovTextBox ID="IRJ2000_TextBox" runat="server" CssClass="mrUrlapInput" Column="IRJ2000" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" Text="�gyf�l:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <ltt:LovTextBox ID="UI_NAME_TextBox" runat="server" CssClass="mrUrlapInput" Column="UI_NAME" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="T�rgy:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="MEMO_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="Azonos�t�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="UI_OT_ID_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="Teljes sz�veges keres�s (�gyf�l, ir.sz�m, utca, h�zsz�m, t�rgy):"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="FTSearch" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                         
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelEloado" runat="server" Text="El�ad�:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <ltt:LovTextBox ID="textEloado" runat="server" CssClass="mrUrlapInput" Column="Edok_Ugyintezo_Nev" />
                            </td>
                        </tr> 
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelIratHelye" runat="server" Text="Irat Helye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:HiddenField ID="HiddenField_IratEredetiHelye" runat="server" />
                                <asp:DropDownList ID="ddlistIratHelye" runat="server" CssClass="mrUrlapInputComboBox">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUgyirat_tipus" runat="server" Text="Irat t�pusa:"></asp:Label>&nbsp;</td>
                            <td class="mrUrlapMezo">
                                <kod:KodtarakDropDownList ID="Ugy_Jelleg_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelHatarido" runat="server" Text="Hat�rid�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Hatarido_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelIRATTARBA" runat="server" Text="Iratt�rba helyez�s d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Irattarba_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelSkontro" runat="server" Text="Skontr� v�ge:">
                                </asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Skontro_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelSelejtezesDatuma" runat="server" Text="Selejtez�s d�tuma:">
                                </asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="Selejtezes_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
		                <tr class="urlapSor">
			                  <td class="mrUrlapCaption">
			                  </td>
			                  <td class="mrUrlapMezo">
			                      <asp:RadioButtonList ID="rblSzerelt" runat="server" >
			                      		<asp:ListItem Value="0" Selected="false" Text="Nem szerelt" />
				                        <asp:ListItem  Value="1" Selected="false" Text="Szerelt" />
				                        <asp:ListItem Value="NotSet" Selected="true" Text="�sszes" />
				                    </asp:RadioButtonList>
			                  </td>
		                </tr>
                        <!-- lara -->
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelFeladat" runat="server" Text="Feladat:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textFeladat" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelSzervezet" runat="server" Text="Szervezet:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <ltt:LovTextBox ID="textSzervezet" runat="server" CssClass="mrUrlapInput" Column="Szervezet" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKikero" runat="server" Text="Kik�r�:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textKikero" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelIrattarbolKikeres_Datuma" runat="server" Text="Kik�r�s d�tuma:">
                                </asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_IrattarbolKikeres_Datuma"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr> 
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label8" runat="server" Text="Meg�rz�si id�:">
                                </asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_MegorzesiIdo"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr> 
                        <%--BLG_236--%>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelIrattariHely" runat="server" Text="Iratt�ri hely:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <ltt:LovTextBox ID="textIrattariHely" runat="server" CssClass="mrUrlapInput" Column="IrattariHely" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
            </td>
        </tr>
        <tr class="tabExtendableListFejlec">
            <td style="text-align: left;">
              <asp:Panel runat="server" ID="AlszamAdataiHeader">
                <asp:ImageButton runat="server" ID="AlszamAdataiCPEButton" ImageUrl="images/hu/Grid/plus.gif" OnClientClick="return false;" />
                &nbsp;
                <asp:Label ID="labelAlszamAdataiShowHide" runat="server" Text="<%$Resources:Search,UI_AlszamAdatai_CollapsedText%>" />
              </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <eUI:eFormPanel ID="Alszam_eFormPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                                <asp:Label ID="labelIDNUM" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="IDNUMTextBox" runat="server"  CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                                <asp:Label ID="labelBKNEV" runat="server" Text="Bek�ld�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="BKNEVTextBox" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                                <asp:Label ID="labelNAME" runat="server" Text="El�ad� n�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="NAMETextBox" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                                <asp:Label ID="labelMJ" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="MJTextBox" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>                        
                    </table>
                </eUI:eFormPanel>
                <eUI:eFormPanel ID="TalalatokSzamaPanel" runat="server">
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                runat="server" />
                        </td>
                    </tr>
                </table>
                </eUI:eFormPanel>
                &nbsp;&nbsp;
                <uc2:SearchFooter ID="SearchFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
