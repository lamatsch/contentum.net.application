using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eMigration.Utility;
using Contentum.eMigration.Query.BusinessDocuments;

using Contentum.eQuery;

public partial class FoszamSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(MIG_FoszamSearch);

    #region ControlCheckUtils

    private bool IsThereAFilledControl_AlszamAdatai()
    {
        bool bIsThereFilledControl = !String.IsNullOrEmpty(IDNUMTextBox.Text)
            || !String.IsNullOrEmpty(BKNEVTextBox.Text)
            || !String.IsNullOrEmpty(NAMETextBox.Text)
            || !String.IsNullOrEmpty(MJTextBox.Text);

        return bIsThereFilledControl;
    }

    #endregion ControlCheckUtils

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.MIG_FoszamSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            MIG_FoszamSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (MIG_FoszamSearch)Search.GetSearchObject(Page, new MIG_FoszamSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }

            IratHelye.FillDropDownList(ddlistIratHelye, Page);
//            UgyiratTipus.FillDropDownList(ddlistUgyirat_Tipus, Page);

            // �gyirat t�pusa
            Ugy_Jelleg_KodtarakDropDownList.FillAndSetSelectedValue("UGYIRAT_JELLEG",
                UgyiratTipus.GetUgyiratJellegFromRegiTipus(searchObject.Ugyirat_tipus.Value), true, SearchHeader1.ErrorPanel);

            // �res �rt�k hozz�ad�sa a list�hoz
            ddlistIratHelye.Items.Insert(0, new ListItem("[Nincs megadva elem]", ""));
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        #region �sszecsukhat� panelek l�that�s�ga tartalmuk alapj�n
        // ha van adat, kinyitjuk a panelt
        #region alsz�m adatai mez�k
        bool bAlszamAdataiPanelCollapsed = true   // alap�rtelmez�sben �sszecsukva
            && !IsThereAFilledControl_AlszamAdatai();

        AlszamAdataiCPE.Collapsed = bAlszamAdataiPanelCollapsed;
        #endregion alsz�m adatai mez�k

        #endregion �sszecsukhat� panelek l�that�s�ga

    }

    /// <summary>
    /// Keres�si objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        MIG_FoszamSearch MIG_FoszamSearch = null;
        if (searchObject != null) MIG_FoszamSearch = (MIG_FoszamSearch)searchObject;

        if (MIG_FoszamSearch != null)
        {
            //if (!String.IsNullOrEmpty(MIG_FoszamSearch.WhereByManual))
            //{
            //    FTSearch.Text = MIG_FoszamSearch.WhereByManual;
            //}
            FTSearch.Text = MIG_FoszamSearch.Conc.Value;
            //UI_SAV_TextBox.Text = MIG_FoszamSearch.UI_SAV.Value;
            UI_SAV_TextBox.Text = MIG_FoszamSearch.EdokSav.Value;
            UI_YEAR_TextBox.Text = MIG_FoszamSearch.UI_YEAR.Value;
            UI_NUM_TextBox.Text = MIG_FoszamSearch.UI_NUM.Value;
            IRJ2000_TextBox.Text = MIG_FoszamSearch.IRJ2000.Value;
            UI_NAME_TextBox.Text = MIG_FoszamSearch.UI_NAME.Value;
            MEMO_TextBox.Text = MIG_FoszamSearch.MEMO.Value;
            UI_OT_ID_TextBox.Text = MIG_FoszamSearch.UI_OT_ID.Value;

            textEloado.Text = MIG_FoszamSearch.Edok_Ugyintezo_Nev.Value;

            ddlistIratHelye.SelectedValue = MIG_FoszamSearch.UGYHOL.Value;
            
            Hatarido_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(MIG_FoszamSearch.Hatarido);
            Irattarba_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(MIG_FoszamSearch.IRATTARBA);
            Skontro_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(MIG_FoszamSearch.SCONTRO);
            Selejtezes_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(MIG_FoszamSearch.Selejtezes_Datuma);

            if (MIG_FoszamSearch.Csatolva_Id.Operator == Query.Operators.isnull && MIG_FoszamSearch.Edok_Utoirat_Id.Operator == Query.Operators.isnull)
            {
                rblSzerelt.SelectedValue = "0";
            }
            else if (MIG_FoszamSearch.Csatolva_Id.Operator == Query.Operators.notnull || MIG_FoszamSearch.Edok_Utoirat_Id.Operator == Query.Operators.notnull)
            {
                rblSzerelt.SelectedValue = "1";
            }
            else
            {
                rblSzerelt.SelectedValue = "NotSet";
            }

            // CR 3090 : Feladat mez� szerinti keres�s
            textFeladat.Text = MIG_FoszamSearch.Feladat.Value;
            textSzervezet.Text = MIG_FoszamSearch.Szervezet.Value;
            textKikero.Text = MIG_FoszamSearch.IrattarbolKikeroNev.Value;

            Ugy_Jelleg_KodtarakDropDownList.SelectedValue = UgyiratTipus.GetUgyiratJellegFromRegiTipus(MIG_FoszamSearch.Ugyirat_tipus.Value);

            DatumIntervallum_IrattarbolKikeres_Datuma.SetComponentFromSearchObjectFields(MIG_FoszamSearch.IrattarbolKikeres_Datuma);
            DatumIntervallum_MegorzesiIdo.SetComponentFromSearchObjectFields(MIG_FoszamSearch.MegorzesiIdo);

            // BLG_236
            textIrattariHely.Text = MIG_FoszamSearch.IrattariHely.Value;

            #region ExtendedMIG_AlszamSearch
            IDNUMTextBox.Text = MIG_FoszamSearch.ExtendedMIG_AlszamSearch.IDNUM.Value;
            BKNEVTextBox.Text = MIG_FoszamSearch.ExtendedMIG_AlszamSearch.BKNEV.Value;
            MJTextBox.Text = MIG_FoszamSearch.ExtendedMIG_AlszamSearch.MJ.Value;
            #endregion ExtendedMIG_AlszamSearch

            #region ExtendedMIG_EloadoSearch
            NAMETextBox.Text = MIG_FoszamSearch.ExtendedMIG_EloadoSearch.NAME.Value;
            #endregion ExtendedMIG_EloadoSearch
        }
    }

    /// <summary>
    /// Form --> Keres�si objektum
    /// </summary>
    private MIG_FoszamSearch SetSearchObjectFromComponents()
    {
        MIG_FoszamSearch MIG_FoszamSearch = (MIG_FoszamSearch)SearchHeader1.TemplateObject;
        if (MIG_FoszamSearch == null)
        {
            MIG_FoszamSearch = new MIG_FoszamSearch();
        }

        MIG_FoszamSearch.EdokSav.LikeWhenNeededIfNotEmpty(UI_SAV_TextBox.Text);

        MIG_FoszamSearch.UI_YEAR.LikeWhenNeededIfNotEmpty(UI_YEAR_TextBox.Text);

        MIG_FoszamSearch.UI_NUM.LikeWhenNeededIfNotEmpty(UI_NUM_TextBox.Text);

        if (!String.IsNullOrEmpty(IRJ2000_TextBox.Text))
        {
            MIG_FoszamSearch.IRJ2000.Value = IRJ2000_TextBox.Text;
            MIG_FoszamSearch.IRJ2000.Operator = Search.GetOperatorByLikeCharater(IRJ2000_TextBox.Text);
            MIG_FoszamSearch.IRJ2000.GroupOperator = Query.Operators.or;
            MIG_FoszamSearch.IRJ2000.Group = "01";
            MIG_FoszamSearch.UI_IRJ.Value = IRJ2000_TextBox.Text;
            MIG_FoszamSearch.UI_IRJ.Operator = Search.GetOperatorByLikeCharater(IRJ2000_TextBox.Text);
            MIG_FoszamSearch.UI_IRJ.GroupOperator = Query.Operators.or;
            MIG_FoszamSearch.UI_IRJ.Group = "01";
        }

        MIG_FoszamSearch.UI_NAME.LikeWhenNeededIfNotEmpty(UI_NAME_TextBox.Text);

        MIG_FoszamSearch.MEMO.LikeWhenNeededIfNotEmpty(MEMO_TextBox.Text);

        MIG_FoszamSearch.UI_OT_ID.LikeWhenNeededIfNotEmpty(UI_OT_ID_TextBox.Text);

        if (!String.IsNullOrEmpty(FTSearch.Text))
        {
            ////MIG_FoszamSearch.WhereByManual = " contains(*,'" + FTSearch.Text + "')";
            //MIG_FoszamSearch.WhereByManual = FTSearch.Text;

            // BUG_296
            //MIG_FoszamSearch.Conc.Value = FTSearch.Text;
            //MIG_FoszamSearch.Conc.Operator = Query.Operators.contains;
            MIG_FoszamSearch.Conc.Value = "%"+ FTSearch.Text +"%";
            MIG_FoszamSearch.Conc.Operator = Query.Operators.like;

        }

        MIG_FoszamSearch.UGYHOL.FilterIfNotEmpty(ddlistIratHelye.SelectedValue);
 
        switch (rblSzerelt.SelectedValue)
        {
            case "0":
                MIG_FoszamSearch.Csatolva_Id.AndGroup("111");
                MIG_FoszamSearch.Csatolva_Id.IsNull();
                //--------------------------------------------------------
                MIG_FoszamSearch.Edok_Utoirat_Id.AndGroup("111");
                MIG_FoszamSearch.Edok_Utoirat_Id.IsNull();
                break;
            case "1":
                MIG_FoszamSearch.Csatolva_Id.OrGroup("111");
                MIG_FoszamSearch.Csatolva_Id.NotNull();
                //--------------------------------------------------------
                MIG_FoszamSearch.Edok_Utoirat_Id.OrGroup("111");
                MIG_FoszamSearch.Edok_Utoirat_Id.NotNull();
                break;
            default:
                MIG_FoszamSearch.Csatolva_Id.Clear();
                //--------------------------------------------------------
                MIG_FoszamSearch.Edok_Utoirat_Id.Clear();
                break;
        }

        // Foszam eloado: ket helyen is keresni kell
        if (!String.IsNullOrEmpty(textEloado.Text))
        {
            MIG_FoszamSearch.Edok_Ugyintezo_Nev.OrGroup("444");
            MIG_FoszamSearch.Edok_Ugyintezo_Nev.LikeWhenNeeded(textEloado.Text);

            MIG_FoszamSearch.Manual_MIG_Eloado_NAME.OrGroup("444");
            MIG_FoszamSearch.Manual_MIG_Eloado_NAME.LikeWhenNeeded(textEloado.Text);
        }

        Irattarba_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(MIG_FoszamSearch.IRATTARBA);
        Skontro_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(MIG_FoszamSearch.SCONTRO);
        Selejtezes_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(MIG_FoszamSearch.Selejtezes_Datuma);
        Hatarido_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(MIG_FoszamSearch.Hatarido);

        // CR 3090 : Feladat mez� alapj�n keres�s
        MIG_FoszamSearch.Feladat.LikeWhenNeededIfNotEmpty(textFeladat.Text);

        MIG_FoszamSearch.Szervezet.LikeWhenNeededIfNotEmpty(textSzervezet.Text);

        MIG_FoszamSearch.IrattarbolKikeroNev.LikeWhenNeededIfNotEmpty(textKikero.Text);

        var ugyiratTipus = UgyiratTipus.GetRegiTipusFromUgyiratJelleg(Ugy_Jelleg_KodtarakDropDownList.SelectedValue);
        MIG_FoszamSearch.Ugyirat_tipus.FilterIfNotEmpty(ugyiratTipus);

        DatumIntervallum_IrattarbolKikeres_Datuma.SetSearchObjectFields(MIG_FoszamSearch.IrattarbolKikeres_Datuma);
        DatumIntervallum_MegorzesiIdo.SetSearchObjectFields(MIG_FoszamSearch.MegorzesiIdo);

        //BLG_236
        MIG_FoszamSearch.IrattariHely.LikeWhenNeededIfNotEmpty(textIrattariHely.Text);

        #region ExtendedMIG_AlszamSearch
        MIG_FoszamSearch.ExtendedMIG_AlszamSearch.IDNUM.LikeWhenNeededIfNotEmpty(IDNUMTextBox.Text);

        MIG_FoszamSearch.ExtendedMIG_AlszamSearch.BKNEV.LikeWhenNeededIfNotEmpty(BKNEVTextBox.Text);

        MIG_FoszamSearch.ExtendedMIG_AlszamSearch.MJ.LikeWhenNeededIfNotEmpty(MJTextBox.Text);
        #endregion ExtendedMIG_AlszamSearch

        #region ExtendedMIG_EloadoSearch
        MIG_FoszamSearch.ExtendedMIG_EloadoSearch.NAME.LikeWhenNeededIfNotEmpty(NAMETextBox.Text);
        #endregion ExtendedMIG_EloadoSearch

        return MIG_FoszamSearch;
    }



    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            MIG_FoszamSearch searchObject = SetSearchObjectFromComponents();
            MIG_FoszamSearch defaultSearchObject = GetDefaultSearchObject();
            if (Search.IsIdentical(searchObject, defaultSearchObject, searchObject.WhereByManual, "")
                && Search.IsIdentical(searchObject.ExtendedMIG_AlszamSearch, defaultSearchObject.ExtendedMIG_AlszamSearch
                ,searchObject.ExtendedMIG_AlszamSearch.WhereByManual, defaultSearchObject.ExtendedMIG_AlszamSearch.WhereByManual)
                && Search.IsIdentical(searchObject.ExtendedMIG_EloadoSearch, defaultSearchObject.ExtendedMIG_EloadoSearch
                , searchObject.ExtendedMIG_EloadoSearch.WhereByManual, defaultSearchObject.ExtendedMIG_EloadoSearch.WhereByManual))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);

            #region panelek elt�ntet�se, hogy ne k�ldj�nk vissza feleslegesen adatokat
            Alszam_eFormPanel.Visible = false;
            FoszamAdatai_eFormPanel.Visible = false;
            #endregion

        }
    }

    private MIG_FoszamSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new MIG_FoszamSearch();
    }

}
