﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Net;
using System.IO;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using Contentum.eMigration.Utility;

public partial class GetDocumentContent : System.Web.UI.Page
{
    ContentDownloader contentDownloader = new ContentDownloader();
    protected void Page_PreRender(object sender, EventArgs e)
    {
        string externalLink = string.Empty;
        string filename = string.Empty;
        string guid = string.Empty;
        string currentVersion = String.Empty;
        string externalSource = string.Empty;
        
        try
        {

            #region GUID alapján externalLink lekérése

            MIG_DokumentumService dokService = eMigrationService.ServiceFactory.GetMIG_DokumentumService();
            ExecParam ep = Contentum.eMigration.Utility.UI.SetExecParamDefault(Page);

            if (!string.IsNullOrEmpty(Request.QueryString.Get("id")))
            {
                ep.Record_Id = Request.QueryString.Get("id");

                Result dokGetResult = dokService.Get(ep);
                if (dokGetResult.IsError)
                {
                    throw new ResultException(dokGetResult);
                }

                externalLink = (dokGetResult.Record as MIG_Dokumentum).External_Link;
                filename = (dokGetResult.Record as MIG_Dokumentum).FajlNev;
                externalSource = (dokGetResult.Record as MIG_Dokumentum).External_Source;
                guid = ep.Record_Id;
            }

            #endregion GUID alapján externalLink letöltése

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();

            // BUG_12361
            //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
            {
                filename = EncodeFileName(filename);
            }

            if (externalSource.Contains("SharePoint"))
                GetFileBySharePoint(externalLink, filename);
            else if (externalSource.Contains("FileShare"))
                GetFileByFileShare(externalLink, filename);
            else if (externalSource.Contains("UCM"))
                GetFileByUCM(externalLink, filename);
            else if (externalSource.Contains(HAIRCsatolmanyFeltoltesParameters.Constants.HAIR_FILE_SOURCE))
                contentDownloader.GetFileByHAIR(Page, externalLink, filename, string.Empty);
            else
                throw new NotImplementedException(string.Format("Hibás external_source mező:{0}", externalSource));            
        }
        catch (ResultException resEx)
        {
            Response.ClearHeaders();          
            Response.Write(Contentum.eMigration.Utility.ResultError.GetErrorMessageFromResultObject(resEx.GetResult()));
        }
        catch (Exception exp)
        {
            Response.ClearHeaders();        
            Response.Write(Resources.Error.ErrorCode_52795); // A kért dokumentum nem érhető el! Kérem, próbálja meg később.
            Logger.Warn("GetDocumentumContent hiba: " + exp.Message);
        }
        finally
        {
            Response.End();
        }
    }

    private string EncodeFileName(string fileName)
    {
        if (String.IsNullOrEmpty(fileName))
            return String.Empty;

        fileName = HttpUtility.UrlEncode(fileName);
        fileName = fileName.Replace("+", "%20");

        return fileName;
    }
     /// <summary>
    /// Filetartalom letőltése és továbbítása a kliensnek SharePoint esetén
    /// </summary>
    /// <param name="externalLink">a file ürlje</param>
    /// <param name="filename">a fájl neve és kiterjesztése kliens oldalon</param>
    private void GetFileBySharePoint(string externalLink, string filename)
    {
               #region Header bejegyzések beállítása

                WebRequest request = WebRequest.Create(externalLink);

                request.Credentials = new NetworkCredential(Contentum.eUtility.UI.GetAppSetting("SharePointUserName"), Contentum.eUtility.UI.GetAppSetting("SharePointPassword"), Contentum.eUtility.UI.GetAppSetting("SharePointUserDomain"));
                WebResponse response = request.GetResponse();

                // BUG_12361
                //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
                {
                    filename = EncodeFileName(filename);
                }

                // Content-Type és fájl nevének beállítása
                Response.ContentType = response.ContentType;
                Response.CacheControl = "public";
                Response.HeaderEncoding = System.Text.Encoding.UTF8;
                Response.Charset = "utf-8";
                Response.AddHeader("Content-Disposition", "inline;filename=\"" + filename + "\";");

                #endregion Header bejegyzések beállítása

                #region Fájltartalom letöltése és továbbítása a kliensnek

                System.IO.Stream s = response.GetResponseStream();
                byte[] buf = new byte[20480];

                while (true)
                {
                    int readBytes = s.Read(buf, 0, buf.Length);

                    if (readBytes == 0)
                        break;

                    Response.OutputStream.Write(buf, 0, readBytes);
                }

                s.Close();                
                #endregion Fájltartalom letöltése és továbbítása a kliensnek

    }
    /// <summary>
    /// Filetartalom letőltése és továbbítása a kliensnek FileShare esetén
    /// </summary>
    /// <param name="externalLink">a file ürlje</param>
    /// <param name="filename">a fájl neve és kiterjesztése kliens oldalon</param>
    private void GetFileByFileShare(string externalLink,string filename)
    {
        #region Header bejegyzések beállítása        

        // BUG_12361
        //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
        {
            filename = EncodeFileName(filename);
        }

        // Content-Type és fájl nevének beállítása
        Response.ContentType = "application/octet-stream";
        Response.CacheControl = "public";
        Response.HeaderEncoding = System.Text.Encoding.UTF8;
        Response.Charset = "utf-8";
        Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filename + "\";");

        #endregion Header bejegyzések beállítása
        using (var file = File.OpenRead(externalLink))
        {
            byte[] buf = new byte[20480];

            while (true)
            {
                int readBytes = file.Read(buf, 0, buf.Length);

                if (readBytes == 0)
                    break;

                Response.OutputStream.Write(buf, 0, readBytes);
            }            
        }
    }

    private void GetFileByUCM(string externalLink, string filename)
    {
        if (!String.IsNullOrEmpty(externalLink))
        {
            #region Header bejegyzések beállítása

            WebRequest request = WebRequest.Create(externalLink);

            request.UseDefaultCredentials = true;
            WebResponse response = request.GetResponse();

            // BUG_12361
            //if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("MSIE"))
            {
                filename = EncodeFileName(filename);
            }

            // Content-Type és fájl nevének beállítása
            Response.ContentType = response.ContentType;
            Response.CacheControl = "public";
            Response.HeaderEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "utf-8";
            Response.AddHeader("Content-Disposition", "inline;filename=\"" + filename + "\";");

            #endregion Header bejegyzések beállítása

            #region Fájltartalom letöltése és továbbítása a kliensnek

            System.IO.Stream s = response.GetResponseStream();
            byte[] buf = new byte[20480];

            while (true)
            {
                int readBytes = s.Read(buf, 0, buf.Length);

                if (readBytes == 0)
                    break;

                Response.OutputStream.Write(buf, 0, readBytes);
            }

            s.Close();
            #endregion Fájltartalom letöltése és továbbítása a kliensnek
        }
    }

}
