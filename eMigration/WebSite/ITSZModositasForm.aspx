﻿<%@ Page Language="C#"  MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="ITSZModositasForm.aspx.cs" Inherits="ITSZModositasForm" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>

<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>
<%@ Register Src="~/eMigrationComponent/IrattariJelTextBox.ascx" TagName="IrattariJelTextBox" TagPrefix="uc"%>

<%@ Register src="eMigrationComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%-- CR3246 Irattári Helyek kezelésének módosítása--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
        
    <!--Frissítés jelzése-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
              
    
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
             <uc1:FormHeader ID="FormHeader1" runat="server" />
<%--                    <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="MainPanel" runat="server">
    
        <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
    
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                    <asp:Panel ID="FoszamListPanel" runat="server" Visible="false">
                         <br />
                        <asp:Label ID="Label_Ugyiratok" runat="server" Text="Ügyiratok:" Visible="false"  CssClass="GridViewTitle"></asp:Label>          
                        <br />
                        <asp:GridView ID="FoszamGridView" runat="server" CellPadding="0" CellSpacing="0"
                            BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                            OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                            AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id">
                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                            <HeaderStyle CssClass="GridViewHeaderStyle"/>                      
                            <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                                                           
                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                            <ItemTemplate>                                    
                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                                    Visible="false" OnClientClick="return false;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>  
                                    <%-- * Adattagok * --%>
                                     <asp:BoundField DataField="Id" Visible="False" SortExpression="Id">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="150px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UI_SAV" SortExpression="MIG_Foszam.EdokSav" HeaderText="Iktatókönyv">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="50px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UI_YEAR" SortExpression="MIG_Foszam.UI_YEAR" HeaderText="&#201;v">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="50px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UI_NUM" SortExpression="MIG_Foszam.UI_NUM" HeaderText="Fősz&#225;m">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="65px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField SortExpression="MIG_Foszam.IRJ2000 {0}, MIG_Foszam.UI_IRJ" HeaderText="Iratt&#225;ri Jel">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="70px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="labelIRJ" runat="server" Text='<%# GetIrattariJel(Eval("UI_YEAR"),Eval("UI_IRJ"),Eval("IRJ2000")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UI_NAME" SortExpression="MIG_Foszam.UI_NAME" HeaderText="&#220;gyf&#233;l">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="200px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MEMO" SortExpression="MIG_Foszam.MEMO" HeaderText="T&#225;rgy">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="230px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UI_OT_ID" SortExpression="MIG_Foszam.UI_OT_ID" HeaderText="Azonosító">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Eloado_Nev" SortExpression="Eloado_Nev" HeaderText="Ügyintézo">
                                        <ItemStyle CssClass="GridViewBorder"></ItemStyle>
                                        <HeaderStyle Width="100px" CssClass="GridViewBorderHeader" HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>                                   
                                    <asp:TemplateField HeaderText="Irat helye" SortExpression="MIG_Foszam.UGYHOL"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelIratHelye" runat="server" Text='<%#Eval("UGYHOL") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Irat típusa" SortExpression="MIG_Foszam.Ugyirat_tipus"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelUgyiratTipus" runat="server" Text='<%#Eval("Ugyirat_tipus") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Határidő" SortExpression="MIG_Foszam.Hatarido"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelHataridoDate" runat="server" Text='<%#Eval("HATARIDO") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Irattárba" SortExpression="MIG_Foszam.IRATTARBA"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelIrattarbaDate" runat="server" Text='<%#Eval("IRATTARBA") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Skontró vége" SortExpression="MIG_Foszam.SCONTRO"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelSkontroVegeDate" runat="server" Text='<%#Eval("SCONTRO") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Feladat" SortExpression="MIG_Foszam.Feladat"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelFeladat" runat="server" Text='<%#Eval("Feladat") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Szervezet" SortExpression="MIG_Foszam.Szervezet"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelSzervezet" runat="server" Text='<%#Eval("Szervezet") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Kikérő" SortExpression="MIG_Foszam.IrattarbolKikeroNev"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelIrattarbolKikeroNev" runat="server" Text='<%#Eval("IrattarbolKikeroNev") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Kikérés dátuma" SortExpression="MIG_Foszam.IrattarbolKikeres_Datuma"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelIrattarbolKikeres_Datuma" runat="server" Text='<%#Eval("IrattarbolKikeres_Datuma") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Megőrzési idő" SortExpression="MIG_Foszam.MegorzesiIdo"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelMegorzesiIdo" runat="server" Text='<%#Eval("MegorzesiIdo") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Alszám"
                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="labelALNO" runat="server" Text='<%#Eval("ALNO") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>                    
                    
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Label ID="labelTetelekSzama" Text="Kijelölt tételek száma: " runat="server" />
                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                </td>
            </tr>
            <tr>
            <td>
            <br />
                <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false">                
                <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                </asp:Panel>
                <br />
                <asp:Panel ID="Panel_Warning_NemAzonositottVonalkodok" runat="server" Visible="false" HorizontalAlign="Center">                    
                    <br />
                    <asp:Label ID="labelNotIdentifiedHeader" runat="server" Text="<%$Resources:Form,UI_NemAzonositottVonalkodok%>" 
                        Font-Bold="true" Font-Size="Larger" CssClass="warningHeader"></asp:Label>
                    <span style="position:relative;top:-10px; left:38%;">
                        <asp:Label ID="Label_Warning_NemAzonositottVonalkodok" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                    </span>
                </asp:Panel>
                
            </td></tr>
            <tr>
                <td>
                    
                 <eUI:eFormPanel ID="Panel_ITSZ" runat="server">
                <table cellpadding="0" cellspacing="0" style="width: 100%;" >
                    <tr class="urlapSor" runat="server"> 
                        <td  style="text-align: left; vertical-align: top;"  colspan="2">
                            <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                           <asp:Label ID="labelIrattariJel" class= "GridViewTitle" runat="server" Text="<%$Forditas:labelIrattariJel|Irattári tételszám:%>"></asp:Label>
                            <br />                            
                        </td >
                        <td style="text-align:left;vertical-align:top" colspan="2"><uc:IrattariJelTextBox ID="IRJ2000_TextBox" runat="server" SearchMode="true" EvTextBoxControlID="UI_YEAR_TextBox" IsCustomValueEnabled="true"/></td>
                    </tr>
                    <tr>                        
                        <td class="mrUrlapMezo">
                            <%-- CR3246 Irattári Helyek kezelésének módosítása--%>
                             <asp:UpdatePanel runat="server" ID="ITSZ_UpdatePanel"  UpdateMode="Conditional" >
                                                             
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi" runat="server">
                        <td></td>                  
                    </tr>
                </table>
                         </eUI:eFormPanel>
           
                    <uc2:FormFooter ID="FormFooter1" runat="server" />                 
                </td>
            </tr>            
        </table>
     </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upResult">
     <ContentTemplate>
      <asp:Panel ID="ResultPanel" runat="server" Visible="false" width="90%">
            <eUI:eFormPanel ID="EFormPanel2" runat="server" CssClass="mrResultPanel">
                <div class="mrResultPanelText">A kijelölt tételek modosítása sikeresen végrehajtódott.</div>
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                               CommandName="Close" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>      
      </asp:Panel>
      </ContentTemplate>
     </asp:UpdatePanel>
</asp:Content>