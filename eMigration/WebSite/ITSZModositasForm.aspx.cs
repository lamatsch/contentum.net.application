﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.BaseUtility;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Query.BusinessDocuments;

public partial class ITSZModositasForm : System.Web.UI.Page
{
    
    private string Command = "";
    private string StartUp = "";
    private static string Refirj;
    
    private UI ui = new UI();

    private String FoszamokStr = "";

    private String[] FoszamokArray;

    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const String FunkcioKod_FoszamIRJModosit = "FoszamModify";

    public string maxTetelszam = "0";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        StartUp = Request.QueryString.Get(QueryStringVars.Startup);

        FormFooter1.Command = CommandName.Modify;
        
        if (Session["SelectedFoszamIds"] != null)
        {
            FoszamokStr = Session["SelectedFoszamIds"].ToString();
            FoszamokArray = FoszamokStr.Split(',');
        }
        
        // Jogosultságellenőrzés:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_FoszamIRJModosit);
                break;
        }

        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.IrattarRendezesHeaderTitle;
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
               + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
               + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(FoszamGridView, FormHeader1.ErrorPanel, null).Count).ToString();

        ImageClose.OnClientClick = "window.returnValue=true; window.close(); return false;";

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
        
        int cntKijeloltFoszamok = ui.GetGridViewSelectedRows(FoszamGridView, FormHeader1.ErrorPanel, null).Count;

        int cntKijelolt = cntKijeloltFoszamok;

        labelTetelekSzamaDb.Text = cntKijelolt.ToString();
        if (FoszamGridView.Rows.Count==0)
            FormFooter1.ImageButton_Save.Enabled = false;

        if (IsPostBack)
        {
            ClearErrorPanel();
            if (FormHeader1.ErrorPanel.Visible)
                FormHeader1.ErrorPanel.Visible = false;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {
    }

    private void LoadFormComponents()
    {
        if (FoszamokArray != null && FoszamokArray.Length > 0)
        {
            FoszamListPanel.Visible = true;
            Label_Ugyiratok.Visible = true;
            FillFoszamokGridView();
        }
        
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());        
        List<string> irattarIds = new List<string>();
        if (irattarIds.Count > 0) { 
            MIG_FoszamService svcFoszamok = Contentum.eMigration.BaseUtility.eMigrationService.ServiceFactory.GetMIG_FoszamService();
            MIG_FoszamSearch schFoszamok = new MIG_FoszamSearch();

            schFoszamok.Id.In(irattarIds);

            Result result = svcFoszamok.GetAll(ExecParam,schFoszamok);
            if (result.IsError)
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", result.ErrorMessage);        
        }
    }

    private void FillFoszamokGridView()
    {
        DataSet ds = FoszamokGridViewBind();

        // ellenőrzés:
        if (ds != null && ds.Tables[0].Rows.Count != FoszamokArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            // CR3246 Irattári Helyek kezelésének módosítása        
        }

        // Van-e olyan rekord, ami nem adható át? 
        // CheckBoxok vizsgálatával:
        int count_Irattarban = UI.GetGridViewSelectedCheckBoxesCount(FoszamGridView, "check");

        int count_NincsIrattarban = FoszamokArray.Length - count_Irattarban;

        if (count_NincsIrattarban > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_FoszamITSZModosit, count_NincsIrattarban);
            Label_Warning_Ugyirat.Visible = true;
            Panel_Warning_Ugyirat.Visible = true;
            if (count_NincsIrattarban == FoszamGridView.Rows.Count)
                FormFooter1.ImageButton_Save.Enabled = false;
        }
    }

    protected DataSet FoszamokGridViewBind()
    {
        if (FoszamokArray != null && FoszamokArray.Length > 0)
        {
            MIG_FoszamService service = Contentum.eMigration.BaseUtility.eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            MIG_FoszamSearch search = new MIG_FoszamSearch();
            search.Id.In(FoszamokArray);

            Result res = service.GetAll(ExecParam, search);
            if (res.IsError)
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", res.ErrorMessage);
            string _ev = res.Ds.Tables[0].Rows[0]["ui_year"].ToString();
            string _id = res.Ds.Tables[0].Rows[0]["id"].ToString();
            string _irj = res.Ds.Tables[0].Rows[0]["ui_irj"].ToString();
            string _irj2000 = res.Ds.Tables[0].Rows[0]["irj2000"].ToString();
            Refirj = GetIrattariJel(_ev, _irj, _irj2000);
            ui.GridViewFill(FoszamGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }
    protected static string GetIrattariJel(object oYear, object oIrj, object oIrj2000)
    {
        if (oYear == null)
            return String.Empty;

        try
        {
            int year = int.Parse(oYear.ToString());
            if (year < 2000)
                return (oIrj ?? String.Empty).ToString();
            else
                return (oIrj2000 ?? String.Empty).ToString();
        }
        catch (Exception e)
        {
            Logger.Error("GetIrattariJel", e);
            return String.Empty;
        }
    }

    public static void FoszamokGridView_RowDataBound_CheckModosithato(GridViewRowEventArgs e, Page page)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var drv = (DataRowView)e.Row.DataItem;

            // Módosítható?
            ErrorDetails errorDetail;

            bool modosithato = Refirj.Equals(GetIrattariJel(nvl(drv.Row["ui_year"].ToString(),""), nvl(drv.Row["ui_irj"].ToString(),""), nvl(drv.Row["irj2000"].ToString(),"")));

            // CheckBox lekérése:
            CheckBox checkBox = (CheckBox)e.Row.FindControl("check");

            if (!modosithato)
            {
                // Ez így nem szép, de mükődik. 
                errorDetail = new ErrorDetails("Nem modosítható, mert különbözik az irattári jel. ","string",drv.Row["Id"].ToString(),"string",nvl(drv.Row["ui_irj"].ToString(),""));

                e.Row.BackColor = System.Drawing.Color.Coral;
                if (checkBox != null)
                {
                    checkBox.Checked = false;
                    checkBox.Enabled = false;
                }

                // Info Image állítása, hogy miért nem lehet átvenni
                ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                if (infoImageButton != null && errorDetail != null)
                {
                    infoImageButton.Visible = true;
                    string infoMessage = ResultError.GetErrorDetailInfoMsg(errorDetail, page);
                    infoImageButton.ToolTip = infoMessage;
                    infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                        + Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok + "','" + drv.Row["Id"].ToString() + "','"
                        + errorDetail.ObjectType + "','" + errorDetail.ObjectId + "'); return false;";
                }
            }
            else
            {
                checkBox.Checked = true;
                checkBox.Enabled = true;
            }
        }
    }

    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        FoszamokGridView_RowDataBound_CheckModosithato(e, Page);
              
        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        if (cb != null)
            cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                "; return true;");
    }

    private static string nvl(string a, string b)
    {
        if (string.IsNullOrEmpty(a))
            return b;
        else
            return a;
    }

    private void Load_ComponentSelectModul()
    {
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_FoszamIRJModosit))
            {
                int selectedIds = ui.GetGridViewSelectedRows(FoszamGridView, FormHeader1.ErrorPanel, null).Count;

                if (selectedIds > int.Parse(maxTetelszam))
                {
                    // ha a JavaScript végén "return false;" van, nem jelenik meg az üzenet...
                    string javaS = "alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + selectedIds.ToString() + "');"; // return false; } ";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "maxItemNumberExceed", javaS, true);
                    return;
                }
                                
                using (MIG_FoszamService service = Contentum.eMigration.BaseUtility.eMigrationService.ServiceFactory.GetMIG_FoszamService())
                {
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    
                    List<string> selectedItemsList_ugyirat = ui.GetGridViewSelectedRows(FoszamGridView, FormHeader1.ErrorPanel, null);

                    if (selectedItemsList_ugyirat.Count == 0)
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                        return;
                    }
                    string strUgyiratIds = String.Join(",", selectedItemsList_ugyirat.ToArray());
                    this.FormHeader1.HeaderTitle = IRJ2000_TextBox.GetSearchText();
                    Result res = service.ModifyITSZTomeges(selectedItemsList_ugyirat.ToArray(), IRJ2000_TextBox.Text, execParam.Clone());
                    if (res.IsError)
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", res.ErrorMessage);                    
                    else
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }
        }
    }
    
    private void ClearErrorPanel()
    {
        if (FormHeader1.ErrorPanel.Visible)
        {
            FormHeader1.ErrorPanel.Visible = false;
            FormHeader1.ErrorPanel.IsWarning = false;
            ErrorUpdatePanel1.Update();
        }
    }
}