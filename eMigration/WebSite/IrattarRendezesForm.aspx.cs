﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eMigration.Utility;
using System.Data;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Query.BusinessDocuments;

//using Contentum.eRecord.BaseUtility;

public partial class IrattarRendezesForm : System.Web.UI.Page
{
    private string Command = "";
    private string StartUp = "";

    private UI ui = new UI();

    private String Vonalkodok = ""; // vonalkódok vesszővel elválasztva
    private String UgyiratokStr = "";

    private String[] UgyiratokArray;
    private String[] VonalkodokArray;

    private string nemAzonositottVonalkodok = "";


    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const String FunkcioKod_IrattarRendezes = "IrattarRendezesModify";
    private Result result_IrattarRendezes = null;

    public string maxTetelszam = "0";

    protected string GetIrattariJel(object oYear, object oIrj, object oIrj2000)
    {
        if (oYear == null)
            return String.Empty;

        try
        {
            int year = (int)oYear;
            if (year < 2000)
                return (oIrj ?? String.Empty).ToString();
            else
                return (oIrj2000 ?? String.Empty).ToString();
        }
        catch (Exception)
        {
            return String.Empty;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        maxTetelszam = Contentum.eAdmin.Utility.Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        StartUp = Request.QueryString.Get(QueryStringVars.Startup);

        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        if (Session["SelectedBarcodeIds"] != null)
        {
            Vonalkodok = Session["SelectedBarcodeIds"].ToString();
            VonalkodokArray = Vonalkodok.Split(',');
        }

        if (Session["SelectedUgyiratIds"] != null)
        {
            UgyiratokStr = Session["SelectedUgyiratIds"].ToString();
            UgyiratokArray = UgyiratokStr.Split(',');
        }
        
        // Jogosultságellenőrzés:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_IrattarRendezes);
                break;
        }

        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        FormHeader1.HeaderTitle = Resources.Form.IrattarRendezesHeaderTitle;
        
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
               + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
               + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";
        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count).ToString();

        ImageClose.OnClientClick = "window.returnValue=true; window.close(); return false;";

        IrattariHely_VonalkodTextBox.Validate = false;
        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }


        
        int cntKijeloltUgyiratok = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count;

        int cntKijelolt = cntKijeloltUgyiratok;

        labelTetelekSzamaDb.Text = cntKijelolt.ToString();
        if (UgyUgyiratokGridView.Rows.Count==0)
            FormFooter1.ImageButton_Save.Enabled = false;

        // CR3246 Irattári Helyek kezelésének módosítása
        IrattariHely_VonalkodTextBox.TextBox.TextChanged += new EventHandler(IrattariHely_VonalkodTextBox_TextChanged);

        JavaScripts.SetFocus(IrattariHely_VonalkodTextBox);
        
        if (IsPostBack)
        {
            ClearErrorPanel();
            if (FormHeader1.ErrorPanel.Visible == true)
                FormHeader1.ErrorPanel.Visible = false;
           
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        
    }

    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {
        string javaS =
                "document.getElementById('" + IrattariHely_VonalkodTextBox.TextBox.ClientID + "').onblur = function () {if(document.getElementById('" + IrattariHely_VonalkodTextBox.TextBox.ClientID + "').value!='')__doPostBack('" + IrattariHely_VonalkodTextBox.TextBox.ClientID + "', '');};";
        ScriptManager.RegisterStartupScript(IrattariHely_VonalkodTextBox.TextBox, this.GetType(), "sendPostBack", javaS, true);
		// CR3246 Irattári Helyek kezelésének módosítása
        //if (!String.IsNullOrEmpty(IrattariHely_VonalkodTextBox.Text) && IrattariHelyLevelekDropDown.DropDownList.Items.FindByValue(IrattariHely_VonalkodTextBox.Text) != null)
        //    IrattariHelyLevelekDropDown.SelectedValue = IrattariHely_VonalkodTextBox.Text;
        //else if (!String.IsNullOrEmpty(IrattariHely_VonalkodTextBox.Text))
        //{
        //    IrattariHely_VonalkodTextBox.Text = "";
        //    Result resError = Contentum.eUtility.ResultException.GetResultFromException(new Exception());
        //    resError.ErrorMessage = Resources.Error.ErrorCode_52481;

        //    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
        //    if (ErrorUpdatePanel1 != null)
        //    {
        //        ErrorUpdatePanel1.Update();
        //    }
        //    return;
        //}
    }

    private void LoadFormComponents()
    {
        // Vonalkódok alapján ügyirat tömb feltöltése:
        if(VonalkodokArray!=null && VonalkodokArray.Count()>0)
            VonalkodElemzes();
       
        if (!String.IsNullOrEmpty(nemAzonositottVonalkodok))
        {
            Label_Warning_NemAzonositottVonalkodok.Text = nemAzonositottVonalkodok;
            Panel_Warning_NemAzonositottVonalkodok.Visible = true;
        }
        
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            UgyiratokListPanel.Visible = true;
            Label_Ugyiratok.Visible = true;
        
            FillUgyiratokGridView();
        }
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        // CR3246 Irattári Helyek kezelésének módosítása
        List<string> irattarIds = new List<string>();
        if (UgyUgyiratokGridView.Rows.Count > 0)
        {
            //Label labelIrattarTipus = (Label)UgyUgyiratokGridView.Rows[0].FindControl("Label_IrattarTipus");
            //if (labelIrattarTipus.Text == "Központi")
            //{
            //    string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(UI.SetExecParamDefault(Page)).Obj_Id;
            //    irattarIds.Add(kozpontiIrattarId);
            //}
            //else
            //{
                //Contentum.eAdmin.Service.KRT_CsoportokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                //Result result = service.GetAllIrattar(ExecParam);

                //foreach (DataRow row in result.Ds.Tables[0].Rows)
                //{
                //    string Id = row["Id"].ToString();
                //    if (!String.IsNullOrEmpty(Id) && !irattarIds.Contains(Id))
                //    {
                //        irattarIds.Add(Id);
                //    }
                //}
            //}
        }
            //EREC_IrattariHelyekService serviceIrattariHelyek = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();
            EREC_IrattariHelyekSearch IrattariHelyekSearch = new EREC_IrattariHelyekSearch();

            //IrattariHelyekSearch.Felelos_Csoport_Id.Value = Contentum.eRecord.BaseUtility.Search.GetSqlInnerString(irattarIds.ToArray());
            //IrattariHelyekSearch.Felelos_Csoport_Id.Operator = Query.Operators.inner;
            IrattarTerkep1.IrattariHelyekSearch = IrattariHelyekSearch;
            IrattarTerkep1.EErrorPanel1 = FormHeader1.ErrorPanel; // EErrorPanel1;

        Panel_IrattariStruktura.Visible = true;
        //ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        //using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        //{            
        //    string Kod = null;
        //    if (StartUp == Constants.Startup.FromKozpontiIrattar)
        //    {
        //        Kod = KodTarak.IrattarTipus.KozpontiIrattar;
        //    }
        //    else if (StartUp == Constants.Startup.FromAtmenetiIrattar)
        //    {
        //        Kod = KodTarak.IrattarTipus.AtmenetiIrattar;
        //    }

        //    var res = service.GetAllLeafs(ExecParam);
        //    IrattariHelyLevelekDropDown.FillDropDownList(res, "Vonalkod", "Ertek",Kod, FormHeader1.ErrorPanel);
        //    if (res != null && res.Ds != null && res.Ds.Tables != null && res.Ds.Tables[0].Rows.Count < 1)
        //        IrattariHelyLevelekDropDown.Enabled = false;
        //}

    }


    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellenőrzés:
        if (ds != null && ds.Tables[0].Rows.Count != UgyiratokArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            // CR3246 Irattári Helyek kezelésének módosítása
            Panel_IrattariStruktura.Visible = false;
        }

        // Van-e olyan rekord, ami nem adható át? 
        // CheckBoxok vizsgálatával:
        int count_Irattarban = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        int count_NincsIrattarban = UgyiratokArray.Length - count_Irattarban;

        if (count_NincsIrattarban > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UIUgyiratNincsIrattarban, count_NincsIrattarban);
            Label_Warning_Ugyirat.Visible = true;
            Panel_Warning_Ugyirat.Visible = true;
            if (count_NincsIrattarban == UgyUgyiratokGridView.Rows.Count)
                FormFooter1.ImageButton_Save.Enabled = false;
        }
    }


    /// <summary>
    /// Vonalkódok alapján objektumok beazonosítása
    /// </summary>
    private void VonalkodElemzes()
    {
        nemAzonositottVonalkodok = "";

        if (VonalkodokArray != null && VonalkodokArray.Length > 0)
        {
            KRT_BarkodokService service = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetKRT_BarkodokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            KRT_BarkodokSearch search = new KRT_BarkodokSearch();
            // keresési objektum beállítása: szűrés az Id-kra:
            search.Kod.Value = Search.GetSqlInnerString(VonalkodokArray);
            search.Kod.Operator = Query.Operators.inner;

            Result res = service.GetAll(execParam, search);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
            }
            else
            {
                if (res.Ds == null) { return; }

                List<String> ugyiratokList = new List<string>();

                List<String> hasznaltVonalkodok = new List<string>();

                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    string Kod = row["Kod"].ToString();
                    string Obj_Id = row["Obj_Id"].ToString();
                    string Obj_type = row["Obj_type"].ToString();

                    switch (Obj_type)
                    {
                        case Constants.TableNames.EREC_UgyUgyiratok:
                            ugyiratokList.Add(Obj_Id);
                            hasznaltVonalkodok.Add(Kod);
                            break;
                    }
                }

                // A nem azonositott vonalkodok kigyűjtése
                foreach (string vonalkod in VonalkodokArray)
                {
                    if (!hasznaltVonalkodok.Contains(vonalkod))
                    {
                        nemAzonositottVonalkodok += "<li class=\"notIdentifiedText\">" + vonalkod + "</li>";
                    }
                }

                if (!String.IsNullOrEmpty(nemAzonositottVonalkodok))
                {
                    nemAzonositottVonalkodok = "<ul>" + nemAzonositottVonalkodok + "</ul>";
                }

                // Listákból tömbök feltöltése:
                UgyiratokArray = ugyiratokList.ToArray();
            }
        }
    }

    protected DataSet UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            UI.ClearGridViewRowSelection(UgyUgyiratokGridView);

        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        MIG_FoszamSearch search = (MIG_FoszamSearch)Search.GetSearchObject(Page, new MIG_FoszamSearch());
            //   search.OrderBy = Search.GetOrderBy("MIG_FoszamGridView", ViewState, SortExpression, SortDirection);

        search.Id.Value = Search.GetSqlInnerString(UgyiratokArray);
        search.Id.Operator = Query.Operators.inner;

        Result res = service.GetAll(ExecParam, search);

        ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel,null);
        DataSet ds = res.Ds;

        return ds;
    }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }

//////
        //if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        //{
        //    EREC_UgyUgyiratokService service = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        //    ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        //    EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
        //    search.Id.Value = Search.GetSqlInnerString(UgyiratokArray);
        //    search.Id.Operator = Query.Operators.inner;

        //    Result res = service.GetAllWithExtension(ExecParam, search);

        //    ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

        //    DataSet ds = res.Ds;

        //    return ds;
        //}
        //else
        //{
        //    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        //    return null;
        //}
    }


    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        UgyiratokGridView_RowDataBound_CheckIrattarbanKijeloles(e, Page);

        //#region Irattár típus mező feltöltés:

        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

        //    string Csoport_Id_Felelos = String.Empty;
        //    if (drv["Csoport_Id_Felelos"] != null)
        //    {
        //        Csoport_Id_Felelos = drv["Csoport_Id_Felelos"].ToString();
        //    }

        //    // Központi vagy átmeneti irattár?
        //    bool kozpontiIrattar = false;
        //    string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(UI.SetExecParamDefault(Page)).Obj_Id;

        //    if (Csoport_Id_Felelos.ToLower() == kozpontiIrattarId.ToLower())
        //    {
        //        kozpontiIrattar = true;
        //    }

        //    // Label lekérése:
        //    Label labelIrattarTipus = (Label)e.Row.FindControl("Label_IrattarTipus");
        //    if (labelIrattarTipus != null)
        //    {
        //        if (kozpontiIrattar)
        //        {
        //            labelIrattarTipus.Text = "Központi";
        //        }
        //        else
        //        {
        //            labelIrattarTipus.Text = "Átmeneti";
        //        }
        //    }
        //}
        //#endregion
        //UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page
        //    , "cb_UtolsoHasznalatSikeres", "UtolsoHasznalatSiker");
        IratHelye.SetGridViewRow(e, "labelIratHelye", Page);
        UI.SetGridViewDateTime(e, "labelIrattarbaDate");
        UI.SetGridViewDateTime(e, "labelSkontroVegeDate");
        UI.SetGridViewDateTime(e, "labelHataridoDate");

        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //   // ErrorDetails error;
        //    DataRowView drv = (DataRowView)e.Row.DataItem;
        //    if (drv != null)
        //    {
        //        ExecParam execParam = UI.SetExecParamDefault(Page);
        //        string felhnev = Contentum.eUtility.FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(execParam.Felhasznalo_Id, Page);
        //        Foszamok.Statusz statusz = Foszamok.GetAllapotByDataRowView(drv);
        //        CheckBox cbModosithato = (CheckBox)e.Row.FindControl("cbModosithato");
        //        if (cbModosithato != null)
        //        {
        //            cbModosithato.Checked = Foszamok.Modosithato(statusz);
        //            ExecParam xpm = UI.SetExecParamDefault(Page);
        //            if (statusz.Ugyhol == Constants.MIG_IratHelye.Irattarban)
        //                //&& xpm.FelhasznaloSzervezet_Id == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(xpm).Obj_Id)
        //            {
        //                cbModosithato.Checked = true;
        //            }

        //        }
                //CheckBox cbSkontrobaHelyezheto = (CheckBox)e.Row.FindControl("cbSkontrobaHelyezheto");
                //if (cbSkontrobaHelyezheto != null)
                //{
                //    cbSkontrobaHelyezheto.Checked = Foszamok.SkontrobaHelyezheto(statusz);
                //}
                //CheckBox cbSkontrobolKiveheto = (CheckBox)e.Row.FindControl("cbSkontrobolKiveheto");
                //if (cbSkontrobolKiveheto != null)
                //{
                //    cbSkontrobolKiveheto.Checked = Foszamok.SkontrobolKiveheto(statusz);
                //}
                //CheckBox cbIrattarbaKuldheto = (CheckBox)e.Row.FindControl("cbIrattarbaKuldheto");
                //if (cbIrattarbaKuldheto != null)
                //{
                //    cbIrattarbaKuldheto.Checked = Foszamok.IrattarbaKuldheto(statusz);
                //}
                //CheckBox cbIrattarbaVeheto = (CheckBox)e.Row.FindControl("cbIrattarbaVeheto");
                //if (cbIrattarbaVeheto != null)
                //{
                //    cbIrattarbaVeheto.Checked = Foszamok.IrattarbaVeheto(statusz);
                //}
                //CheckBox cbIrattarbolKikerheto = (CheckBox)e.Row.FindControl("cbIrattarbolKikerheto");
                //if (cbIrattarbolKikerheto != null)
                //{
                //    cbIrattarbolKikerheto.Checked = Foszamok.IrattarbolKikerheto(statusz);
                //}
                //CheckBox cbKiadhatoOsztalyra = (CheckBox)e.Row.FindControl("cbKiadhatoOsztalyra");
                //if (cbKiadhatoOsztalyra != null)
                //{
                //    cbKiadhatoOsztalyra.Checked = Foszamok.KiadhatoOsztalyra(statusz);
                //}
                //CheckBox cbSelejtezesreKijelolheto = (CheckBox)e.Row.FindControl("cbSelejtezesreKijelolheto");
                //if (cbSelejtezesreKijelolheto != null)
                //{
                //    cbSelejtezesreKijelolheto.Checked = Foszamok.SelejtezesreKijelolheto(statusz);
                //}
                //CheckBox cbSelejtezesreKijelolesVisszavonhato = (CheckBox)e.Row.FindControl("cbSelejtezesreKijelolesVisszavonhato");
                //if (cbSelejtezesreKijelolesVisszavonhato != null)
                //{
                //    cbSelejtezesreKijelolesVisszavonhato.Checked = Foszamok.SelejtezesreKijelolesVisszavonhato(statusz);
                //}
                //CheckBox cbSelejtezheto = (CheckBox)e.Row.FindControl("cbSelejtezheto");
                //if (cbSelejtezheto != null)
                //{
                //    cbSelejtezheto.Checked = Foszamok.Selejtezheto(statusz);
                //}
                //CheckBox cbLomtarbaHelyezheto = (CheckBox)e.Row.FindControl("cbLomtarbaHelyezheto");
                //if (cbLomtarbaHelyezheto != null)
                //{
                //    cbLomtarbaHelyezheto.Checked = Foszamok.LomtarbaHelyezheto(statusz);
                //}
                //CheckBox cbSzerelheto = (CheckBox)e.Row.FindControl("cbSzerelheto");
                //if (cbSzerelheto != null)
                //{
                //    cbSzerelheto.Checked = Foszamok.SzerelhetoBeleIrat(statusz, UI.SetExecParamDefault(Page), Page, felhnev, out error);
                //}
                //CheckBox cbSzerelesVisszavonhato = (CheckBox)e.Row.FindControl("cbSzerelesVisszavonhato");
                //if (cbSzerelesVisszavonhato != null)
                //{
                //    cbSzerelesVisszavonhato.Checked = Foszamok.SzerelesVisszavonhato(statusz, execParam, Page, felhnev, out error);
                //}
          //  }
      //  }
    }

    public static void UgyiratokGridView_RowDataBound_CheckIrattarbanKijeloles(GridViewRowEventArgs e, Page page)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            // Irattárban van ellenőrzés:

            Contentum.eMigration.BaseUtility.Foszamok.Statusz statusz = Foszamok.GetAllapotByDataRowView(drv);

            //bool atadasraKijelolheto = false;
            ErrorDetails errorDetail;

            bool Irattarban = Foszamok.CheckIrattarban(page, statusz, out errorDetail);

            // CheckBox lekérése:
            CheckBox checkBox = (CheckBox)e.Row.FindControl("check");

            if (Irattarban == false)
            {
                e.Row.BackColor = System.Drawing.Color.Coral;
                if (checkBox != null)
                {
                    checkBox.Checked = false;
                    checkBox.Enabled = false;
                }

                // Info Image állítása, miért hibás
                ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                if (infoImageButton != null && errorDetail != null)
                {
                    infoImageButton.Visible = true;
                    string infoMessage = ResultError.GetErrorDetailInfoMsg(errorDetail, page);
                    infoImageButton.ToolTip = infoMessage;
                    infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                        + Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok + "','" + statusz.Id + "','"
                        + errorDetail.ObjectType + "','" + errorDetail.ObjectId + "'); return false;";
                }
            }
            else
            {
                checkBox.Checked = true;
                checkBox.Enabled = true;
            }
        }
    }


    private void Load_ComponentSelectModul()
    {
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_IrattarRendezes))
            {
                int selectedIds = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count;

                if (selectedIds > int.Parse(maxTetelszam))
                {
                    // ha a JavaScript végén "return false;" van, nem jelenik meg az üzenet...
                    string javaS = "alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + selectedIds.ToString() + "');"; // return false; } ";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "maxItemNumberExceed", javaS, true);
                    return;
                }

                // irattárba rendezés

                using (EREC_IrattariHelyekService service = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
                {
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    List<string> selectedItemsList_ugyirat = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                    if (selectedItemsList_ugyirat.Count == 0)
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                        return;
                    }
                    // CR3246 Irattári Helyek kezelésének módosítása
                    //String[] UgyiratIds = selectedItemsList_ugyirat.ToArray();
                    string strUgyiratIds = String.Join(",", selectedItemsList_ugyirat.ToArray());

                    if (!String.IsNullOrEmpty(IrattarTerkep1.SelectedValue))
                    {
                        if ((IrattarTerkep1.TreeView.SelectedNode.ChildNodes.Count > 0) || (IrattarTerkep1.TreeView.SelectedNode.Parent == null))
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, ResultError.GetErrorMessageByErrorCode(52355));
                            return;
                        }
                    }
                    // CR3246 Irattári Helyek kezelésének módosítása
                    //Result Res_IrattariHely = new Result();
                    //if (String.IsNullOrEmpty(IrattariHely_VonalkodTextBox.Text) && String.IsNullOrEmpty(IrattariHelyLevelekDropDown.SelectedValue))
                    //{
                    //    Result resError = Contentum.eUtility.ResultException.GetResultFromException(new Exception());
                    //    resError.ErrorMessage = Resources.Error.UINincsMegadvaMindenAdat;

                    //    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
                    //    if (ErrorUpdatePanel1 != null)
                    //    {
                    //        ErrorUpdatePanel1.Update();
                    //    }
                    //}
                    //else
                    //{
                    //    string Vonalkod = (String.IsNullOrEmpty(IrattariHely_VonalkodTextBox.Text)?IrattariHelyLevelekDropDown.SelectedValue:IrattariHely_VonalkodTextBox.Text);
                    //    Res_IrattariHely = service.GetByVonalkod(execParam, Vonalkod);
                    //}
                    //if (!String.IsNullOrEmpty(Res_IrattariHely.ErrorCode))
                    //{
                    //    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, Res_IrattariHely);
                    //}

                    //result_IrattarRendezes = service.IrattarRendezes(execParam, String.Join(",", UgyiratIds), ((EREC_IrattariHelyek)Res_IrattariHely.Record).Id, ((EREC_IrattariHelyek)Res_IrattariHely.Record).Ertek);

                    //if (String.IsNullOrEmpty(result_IrattarRendezes.ErrorCode))
                    //{
                    //    //JavaScripts.RegisterCloseWindowClientScript(Page);
                    //    MainPanel.Visible = false;
                    //    ResultPanel.Visible = true;
                    //}
                    //else
                    //{
                    //    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_IrattarRendezes);
                    //    UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result_IrattarRendezes);
                    //}

                    //if (!string.IsNullOrEmpty(IrattariHelyLevelekDropDown.SelectedValue))
                    using (MIG_IrattariHelyekService IrattarService = Contentum.eMigration.BaseUtility.eMigrationService.ServiceFactory.GetMIG_IrattariHelyekService())
                    {
                        Result Res_IrattariHely = IrattarService.IrattarRendezes(execParam.Clone(), strUgyiratIds, IrattarTerkep1.SelectedValue, IrattarTerkep1.SelectedText);
                        if (String.IsNullOrEmpty(Res_IrattariHely.ErrorCode))
                        {
                            //JavaScripts.RegisterCloseWindowClientScript(Page);
                            MainPanel.Visible = false;
                            ResultPanel.Visible = true;
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, Res_IrattariHely);
                            UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, Res_IrattariHely);
                        }
                    }
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }

        }
    }
    // CR3246 Irattári Helyek kezelésének módosítása
    //protected void IrattarLevelekDropdownList_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (EErrorPanel1.Visible == true)
    //    {
    //        EErrorPanel1.Visible = false;
    //        ErrorUpdatePanel1.Update();
    //    }

    //    IrattariHely_VonalkodTextBox.Text = ((DropDownList)sender).SelectedValueUpdatePanel1
    //}

    // CR3246 Irattári Helyek kezelésének módosítása
    protected void IrattariHely_VonalkodTextBox_TextChanged(object sender, EventArgs e)
    {
        ClearErrorPanel();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IrattariHelyekService serviceIrattariHelyek = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();
        if (!String.IsNullOrEmpty(IrattariHely_VonalkodTextBox.Text))
        {
            Result result = serviceIrattariHelyek.GetByVonalkod(ExecParam, IrattariHely_VonalkodTextBox.Text);
            EREC_IrattariHelyek irattariHely = result.Record as EREC_IrattariHelyek;
            if (irattariHely == null)
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, ResultError.GetErrorMessageByErrorCode(52356)); //EErrorPanel1

                IrattarTerkep1.TreeView.Nodes[0].Selected = true;
            } else
            {
                string selectedId = irattariHely.Id;
                TreeNode node = IrattarTerkep1.FindByValue(selectedId);
                if (node == null)
                {
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, ResultError.GetErrorMessageByErrorCode(52356));
                   
                    IrattarTerkep1.TreeView.Nodes[0].Selected = true;

                }
                else
                {
                    node.Selected = true;
                    IrattarTerkep1.ExpandAllParents(node);
                    //    IrattarTerkep1.TreeView.SelectedNode.Value = selectedId;

                }
            }
            TreeViewUpdatePanel.Update();
            ErrorUpdatePanel1.Update();
        }
    }

    protected void IrattarTerkep_SelectedNodeChanged(object sender, EventArgs e)
    {
        ClearErrorPanel();

        IrattariHely_VonalkodTextBox.Text = "";
        Vonalkod_UpdatePanel.Update();
    }

    private void ClearErrorPanel()
    {

        if (FormHeader1.ErrorPanel.Visible == true)
        {
            FormHeader1.ErrorPanel.Visible = false;
            FormHeader1.ErrorPanel.IsWarning = false;
            ErrorUpdatePanel1.Update();
        }
    }
}