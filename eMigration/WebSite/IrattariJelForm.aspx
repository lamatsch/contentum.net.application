﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="IrattariJelForm.aspx.cs" Inherits="IrattariJelForm" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc:FormHeader id="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel id="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelEv" runat="server" Text="Év:" /></td>
                            <td class="mrUrlapMezo">
                                <uc:RequiredNumberBox ID="txtEv" runat="server" Validate="false"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelIrattariJelReq" runat="server" Text="*" CssClass="ReqStar"/>
                                <asp:Label ID="labelIrattariJel" runat="server" Text="Irattári jel:" /></td>
                            <td class="mrUrlapMezo">
                                <uc:RequiredTextBox ID="txtIrattariJel" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelNev" runat="server" Text="Megnevezés:" /></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="txtNev" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                 </table>
                 </eUI:eFormPanel>
                <uc:formfooter id="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>

