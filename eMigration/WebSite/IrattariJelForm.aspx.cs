﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eBusinessDocuments;


public partial class IrattariJelForm : System.Web.UI.Page
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private string InitialEv
    {
        get
        {
            return Request.QueryString.Get("Ev");
        }
    }

    private string InitialIrattariJel
    {
        get
        {
            return Request.QueryString.Get("IrattariJel");
        }
    }

    private void SetNewControls()
    {
        txtEv.Text = InitialEv;
        txtIrattariJel.Text = InitialIrattariJel;
    }

    private void SetViewControls()
    {
        txtEv.ReadOnly = true;
        txtIrattariJel.ReadOnly = true;
        txtNev.ReadOnly = true;
    }

    private void SetModifyControls()
    {
        txtEv.ReadOnly = true;
        txtIrattariJel.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FoszamIrattariJelSzovegesModositas");
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String s = Request.QueryString.Get(QueryStringVars.Id);
            int id;
            if (String.IsNullOrEmpty(s) || !Int32.TryParse(s, out id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                MIG_IrattariJelService service = eMigrationService.ServiceFactory.GetMIG_IrattariJelService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.Get(execParam, id);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    MIG_IrattariJel MIG_IrattariJel = (MIG_IrattariJel)result.Record;
                    LoadComponentsFromBusinessObject(MIG_IrattariJel);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }

        if (Command == CommandName.New)
        {
            this.SetNewControls();
        }

        if (Command == CommandName.Modify)
        {
            this.SetModifyControls();
        }

        if (Command == CommandName.View)
        {
            this.SetViewControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = "Irattári jel";

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private void LoadComponentsFromBusinessObject(MIG_IrattariJel mig_IrattariJel)
    {
        txtEv.Text = mig_IrattariJel.EV;
        txtIrattariJel.Text = mig_IrattariJel.IRJEL;
        txtNev.Text = mig_IrattariJel.NAME;
        FormHeader1.Record_Ver = mig_IrattariJel.Base.Ver;
    }

    private MIG_IrattariJel GetBusinessObjectFromComponents()
    {
        MIG_IrattariJel mig_IrattariJel = new MIG_IrattariJel();

        mig_IrattariJel.Updated.SetValueAll(false);
        mig_IrattariJel.Base.Updated.SetValueAll(false);

        mig_IrattariJel.EV = txtEv.Text;
        mig_IrattariJel.Updated.EV = pageView.GetUpdatedByView(txtEv);

        mig_IrattariJel.IRJEL = txtIrattariJel.Text;
        mig_IrattariJel.Updated.IRJEL = pageView.GetUpdatedByView(txtIrattariJel);

        mig_IrattariJel.NAME = txtNev.Text;
        mig_IrattariJel.Updated.NAME = pageView.GetUpdatedByView(txtNev);

        mig_IrattariJel.Base.Ver = FormHeader1.Record_Ver;
        mig_IrattariJel.Base.Updated.Ver = true;

        return mig_IrattariJel;
    }

    private void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(txtEv);
            compSelector.Add_ComponentOnClick(txtIrattariJel);
            compSelector.Add_ComponentOnClick(txtNev);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "FoszamIrattariJelSzovegesModositas"))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            MIG_IrattariJelService service = eMigrationService.ServiceFactory.GetMIG_IrattariJelService();
                            MIG_IrattariJel erec_Jegyzekek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_Jegyzekek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy másik formról hívták meg, adatok visszaküldése:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, txtIrattariJel.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String s = Request.QueryString.Get(QueryStringVars.Id);
                            int recordId;
                            if (String.IsNullOrEmpty(s) || !Int32.TryParse(s, out recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                MIG_IrattariJelService service = eMigrationService.ServiceFactory.GetMIG_IrattariJelService();
                                MIG_IrattariJel erec_Jegyzekek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                Result result = service.Update(execParam, recordId, erec_Jegyzekek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}