﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="IrattariJelLovList.aspx.cs" Inherits="IrattariJelLovList" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,MIG_IrattariJelekLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table style="width: 90%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr id="trMaxRowWarning" class="LovListWarningRow" runat="server" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr class="urlapSor">
                                                        <td class="mrUrlapCaption_nowidth">
                                                            <asp:Label ID="labelEv" runat="server" Text="Év:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc4:RequiredNumberBox ID="txtEv" runat="server" Validate="false" />
                                                        </td>
                                                        <td class="mrUrlapCaption">
                                                            <asp:Label ID="labelIrattariJel" runat="server" Text="Irattári jel:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="txtIrattariJel" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td class="mrUrlapCaption">
                                                            <asp:Label ID="labelNev" runat="server" Text="Megnevezés:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="txtNev" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor">
                                                        <td colspan="6">
                                                            <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"
                                                                AlternateText="Keresés"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="IrattariJelekCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel1" runat="server">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align: top; text-align: left" class="GridViewHeaderBackGroundStyle">
                                                                <!-- Kliens oldali kiválasztás kezelése -->
                                                                <asp:HiddenField ID="GridViewSelectedIndex" runat="server"></asp:HiddenField>
                                                                <asp:HiddenField ID="GridViewSelectedId" runat="server"></asp:HiddenField>
                                                                <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                                    CellPadding="0" BorderWidth="1px" AllowPaging="True" PagerSettings-Visible="false"
                                                                    AutoGenerateColumns="False" AllowSorting="true" AlternatingRowStyle-CssClass="GridViewAlternateRowStyle" OnSorting="GridViewSearchResult_Sorting">
                                                                    <PagerSettings Visible="False"></PagerSettings>
                                                                    <Columns>
                                                                        <asp:BoundField DataField="EV" SortExpression="CTXX_DBF.EV"
                                                                            HeaderText="Év">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="50px"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="IRJEL" SortExpression="CTXX_DBF.IRJEL"
                                                                            HeaderText="Irattári jel">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="100px"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="NAME" SortExpression="CTXX_DBF.Name" HeaderText="Megnevezés">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="440px"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True"></RowStyle>
                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"></SelectedRowStyle>
                                                                    <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>

