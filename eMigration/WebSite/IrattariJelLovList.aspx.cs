﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.Utility;
using Contentum.eQuery;

public partial class IrattariJelLovList : System.Web.UI.Page
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        RegisterJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        if (!IsPostBack)
        {
            string ev = Request.QueryString.Get(QueryStringVars.Ev);
            if (!String.IsNullOrEmpty(ev))
            {
                txtEv.Text = ev;
            }
            string isEvReadOnly = Request.QueryString.Get(QueryStringVars.IsEvFilterDisabled);
            if (!String.IsNullOrEmpty(isEvReadOnly) && isEvReadOnly == "1")
            {
                txtEv.ReadOnly = true;
                UI.AddCssClass(txtEv.TextBox, "ReadOnlyWebControl");
            }

            txtIrattariJel.Focus();
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);


        if (!IsPostBack)
        {
            FillGridViewSearchResult();
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult();
                    }
                    break;
            }
        }

        // scrollozhatóság állítása
        UI.SetLovListGridViewScrollable(GridViewSearchResult, IrattariJelekCPE, 15);
    }


    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, IrattariJelekCPE);
        FillGridViewSearchResult();
    }

    protected void FillGridViewSearchResult()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("GridViewSearchResult", ViewState, "CTXX_DBF.EV ASC, CTXX_DBF.NAME");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("GridViewSearchResult", ViewState,SortDirection.Ascending);

        FillGridViewSearchResult(sortExpression, sortDirection);
    }

    protected void FillGridViewSearchResult(String SortExpression, SortDirection SortDirection)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        MIG_IrattariJelService service = eMigrationService.ServiceFactory.GetMIG_IrattariJelService();
        MIG_IrattariJelSearch search = null;

        search = new MIG_IrattariJelSearch();

        if (!String.IsNullOrEmpty(txtEv.Text))
        {
            search.EV.Value = txtEv.Text;
            search.EV.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(txtIrattariJel.Text))
        {
            search.IRJEL.Value = txtIrattariJel.Text;
            search.IRJEL.Operator = Search.GetOperatorByLikeCharater(txtIrattariJel.Text);
        }

        if (!String.IsNullOrEmpty(txtNev.Text))
        {
            search.NAME.Value = txtNev.Text;
            search.NAME.Operator = Search.GetOperatorByLikeCharater(txtNev.Text);
        }

        search.OrderBy = Search.GetOrderBy("GridViewSearchResult", ViewState, SortExpression, SortDirection); //"IrattariTetelszam";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);

    }

    protected void GridViewSearchResult_Sorting(object sender, GridViewSortEventArgs e)
    {
        FillGridViewSearchResult(e.SortExpression, UI.GetSortToGridView("GridViewSearchResult", ViewState, e.SortExpression));
    }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    JavaScripts.SendBackResultToCallingWindow(Page,String.Empty, selectedText);
                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                    GridViewSearchResult.Visible = false;
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    private void RegisterJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }
}
