﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.BankszamlaszamError = {};
Utility.BankszamlaszamError.badVektorFormat = 'Az ellenőrző vektor formátuma nem megfelelő!\n(Csak számokat tartalmazhat)';
Utility.BankszamlaszamError.badBankszamlaszamInputFormat = 'A bankszámlaszám formátuma nem megfelelő!\n(3×8 számjegyet kell tartalmaznia)';
Utility.BankszamlaszamError.badBankszamlaszamInputFormatSixteenCharsFormatEnabled = 'A bankszámlaszám formátuma nem megfelelő!\n(2×8 vagy 3×8 számjegyet kell tartalmaznia)';
Utility.BankszamlaszamError.badBankszamlaszamFormat = 'A bankszámlaszám formátuma nem megfelelő!\n(Csak számokat tartalmazhat)';
Utility.BankszamlaszamError.badBankszamlaszamLength = 'A bankszámlaszám hossza ({0}) nem megfelelő!\n(A kívánt hossz: 24)';
Utility.BankszamlaszamError.badBankszamlaszamLengthSixteenCharsFormatEnabled = 'A bankszámlaszám hossza ({0}) nem megfelelő!\n(A kívánt hossz: 16 vagy 24)';
Utility.BankszamlaszamError.chekSumError = 'A bankszámlaszám ellenőrző összege hibás!';

Utility.BankszamlaszamBehavior = function(element) {
    Utility.BankszamlaszamBehavior.initializeBase(this, [element]);

    //Properties
    this.autoCompleteExtenderClientId = '';
    this.hiddenFieldClientId = null;
    this.partnerBehaviorId = null;
    this.checkVektor = null;
    this.checksumValidatorId = null;
    this.checksumValidatorCalloutId = null;
    this.checkBankszamlaszam = true;
    this.userId = null;
    this.loginId = null;
    this.isSixteenCharsFormatEnabled = true; // bankszámlaszám csak 16 karakterrel engedélyezett-e
    this.customTextEnabled = false;
    this.textBoxPart1ClientId = null;
    this.textBoxPart2ClientId = null;
    this.textBoxPart3ClientId = null;
    //Variables
    this._partnerBehavior = null;
    this._hiddenField = null;
    this._checksumValidator = null;
    this._checksumValidatorCallout = null;
    this._textBoxPart2 = null;
    this._textBoxPart3 = null;
    this._keyUpHandler = null;
    this._keyDownHandler = null;
    this.itemSelectedHandler = null;
    this.populatingHandler = null;
    this.showingHandler = null;
    this.blurHandler = null;
    this._textChangedHandler = null;
    this._checksumValidationHandler = null;
    this._partnerChangedHandler = null;

    this.applicationLoadHandler = null;
    this.alternateRowStyle = 'GridViewAlternateRowStyle';

    this._bankszamlaszamlength_part1 = 8;
    this._bankszamlaszamlength_part2_short = 8;
    this._bankszamlaszamlength_part2_long = 16;
}

Utility.BankszamlaszamBehavior.prototype =
{

    initialize: function() {
        Utility.BankszamlaszamBehavior.callBaseMethod(this, 'initialize');

        //        this.itemSelectedHandler = Function.createDelegate(this, this.OnItemSelected);
        //        this.populatingHandler = Function.createDelegate(this, this.OnPopulating);
        //        this.showingHandler = Function.createDelegate(this, this.OnShowing);
        //        this.blurHandler = Function.createDelegate(this, this.OnBlur);


        this._applicationLoadHandler = Function.createDelegate(this, this._OnApplicationLoad);
        Sys.Application.add_load(this._applicationLoadHandler);

        if (this.checkBankszamlaszam) {

            //            this._textChangedHandler = Function.createDelegate(this, this._OnTextChanged);
            // 
            //            if(element)
            //            {
            //                $addHandler(element, "change", this._textChangedHandler);
            //            }

        }

        if (this.checksumValidatorId) {
            this._checksumValidator = $get(this.checksumValidatorId);

            if (this._checksumValidator) {
                this._checksumValidationHandler = Function.createDelegate(this, this._ChecksumValidate);
                this._checksumValidator.evaluationfunction = this._checksumValidationHandler;
            }
        }

    },

    _OnApplicationLoad: function() {
        this.itemSelectedHandler = Function.createDelegate(this, this.OnItemSelected);
        this.populatingHandler = Function.createDelegate(this, this.OnPopulating);
        this.showingHandler = Function.createDelegate(this, this.OnShowing);
        this.blurHandler = Function.createDelegate(this, this.OnBlur);

        this._keyUpHandler = Function.createDelegate(this, this._OnKeyUp);
        this._keyDownHandler = Function.createDelegate(this, this._OnKeyDown);
        //this._partnerChangedHandler = Function.createDelegate(this, this._OnPartnerChanged);
        //this._textChangedHandler = Function.createDelegate(this, this._OnTextChanged);

        this._partnerChangedHandler = Function.createDelegate(this, this._OnPartnerChanged);

        if (this.partnerBehaviorId) {
            this._partnerBehavior = $find(this.partnerBehaviorId);
        }
        if (this._partnerBehavior) {
            this._partnerBehavior.add_hiddenfieldValueChanged(this._partnerChangedHandler);
        }

        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);

        if (autoCompleteExtender != null) {
            autoCompleteExtender.add_itemSelected(this.itemSelectedHandler);
            autoCompleteExtender.add_populating(this.populatingHandler);
            autoCompleteExtender.add_showing(this.showingHandler);
        }

        var element = this.get_element();
        if (element) {
            $addHandler(element, 'blur', this.blurHandler);
            $addHandler(element, "keyup", this._keyUpHandler);
            $addHandler(element, "keydown", this._keyDownHandler);
            //$addHandler(element, "change", this._textChangedHandler);
        }

        if (this.textBoxPart2ClientId) {
            this._textBoxPart2 = $get(this.textBoxPart2ClientId);
        }
        if (this.textBoxPart3ClientId) {
            this._textBoxPart3 = $get(this.textBoxPart3ClientId);
        }

        if (this._textBoxPart2) {
            $addHandler(this._textBoxPart2, 'blur', this.blurHandler);
            $addHandler(this._textBoxPart2, "keyup", this._keyUpHandler);
            $addHandler(this._textBoxPart2, "keydown", this._keyDownHandler);
        }

        if (this._textBoxPart3) {
            $addHandler(this._textBoxPart3, 'blur', this.blurHandler);
            $addHandler(this._textBoxPart3, "keyup", this._keyUpHandler);
            $addHandler(this._textBoxPart3, "keydown", this._keyDownHandler);
        }
    },

    dispose: function() {

        var element = this.get_element();

        if (element) {
            if (this._keyUpHandler) {
                $removeHandler(element, "keyup", this._keyUpHandler);
            }
            //this._keyUpHandler = null;
            if (this._textChangedHandler) {
                $removeHandler(element, "change", this._textChangedHandler);
            }
            this._textChangedHandler = null;
            if (this.blurHandler) {
                $removeHandler(element, 'blur', this.blurHandler);
            }
            //this.blurHandler = null;

            if (this._keyDownHandler) {
                $removeHandler(element, "keydown", this._keyDownHandler);
            }
            //this._keyDownHandler = null;

            if (this._checksumValidator) {
                this._checksumValidator.evaluationfunction = null;
                this._checksumValidator.errormessage = null;
                this._checksumValidator = null;
            }

            this._checksumValidationHandler = null;

            this._checksumValidatorCallout = null;

            if (this._partnerChangedHandler && this._partnerBehavior) {
                this._partnerBehavior.remove_hiddenfieldValueChanged(this._partnerChangedHandler);
            }

            this._partnerBehavior = null;
            this._partnerChangedHandler = null;
        }

        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);

        if (this.itemSelectedHandler != null) {

            if (autoCompleteExtender != null) {
                autoCompleteExtender.remove_itemSelected(this.itemSelectedHandler);
            }

            this.itemSelectedHandler = null;
        }

        if (this.populatingHandler != null) {

            if (autoCompleteExtender != null) {
                autoCompleteExtender.remove_populating(this.populatingHandler);
            }

            this.populatingHandler = null;
        }

        if (this.showingHandler != null) {
            if (autoCompleteExtender != null) {
                autoCompleteExtender.remove_showing(this.showingHandler);
            }

            this.showingHandler = null;
        }

        if (this.applicationLoadHandler != null) {
            Sys.Application.remove_load(this.applicationLoadHandler);
            this.applicationLoadHandler = null;
        }

        if (this._textBoxPart2) {
            if (this.blurHandler) {
                $removeHandler(this._textBoxPart2, 'blur', this.blurHandler);
            }
            if (this._keyUpHandler) {
                $removeHandler(this._textBoxPart2, "keyup", this._keyUpHandler);
            }
            if (this._keyDownHandler) {
                $removeHandler(this._textBoxPart2, "keydown", this._keyDownHandler);
            }
        }

        if (this._textBoxPart3) {
            if (this.blurHandler) {
                $removeHandler(this._textBoxPart3, 'blur', this.blurHandler);
            }
            if (this._keyUpHandler) {
                $removeHandler(this._textBoxPart3, "keyup", this._keyUpHandler);
            }
            if (this._keyDownHandler) {
                $removeHandler(this._textBoxPart3, "keydown", this._keyDownHandler);
            }
        }
        this.blurHandler = null;
        this._keyUpHandler = null;
        this._keyDownHandler = null;

        Utility.BankszamlaszamBehavior.callBaseMethod(this, 'dispose');
    },

    OnItemSelected: function(sender, args) {
        var hiddenField = $get(this.hiddenFieldClientId);

        if (hiddenField != null) {
            var value = args.get_value();

            if (value != null) {
                hiddenField.value = value.trim();
            }
        }

        var text = args.get_text().replace(/-/g, '');
        this.set_Value(text);

    },

    OnPopulating: function(sender, args) {
    },

    OnShowing: function(sender, args) {
        this.SetColor(sender);
    },

    SetColor: function(autoCompleteExtender) {
        var list = autoCompleteExtender.get_completionList();
        var prevId = null;
        var alternateRow = true;

        if (list != null && list.childNodes != null && list.childNodes.length > 0) {

            // lista szélesség meghatározása
            if (this._textBoxPart3 && autoCompleteExtender.get_element()) {
                var tb3Bounds = Sys.UI.DomElement.getBounds(this._textBoxPart3);
                var tb1Location = Sys.UI.DomElement.getLocation(autoCompleteExtender.get_element());
                list.style.width = (tb3Bounds.x + tb3Bounds.width - tb1Location.x).toString() + 'px';
            }

            for (var i = 0; i < list.childNodes.length; i++) {
                var listElement = list.childNodes[i];
                if (listElement != null) {
                    var id = listElement._value.split(';')[0].trim();

                    if (id != null && prevId != null) {
                        if (id != prevId) {
                            alternateRow = !alternateRow;
                        }
                    }
                    else {
                        alternateRow = !alternateRow;
                    }

                    if (id != null)
                        prevId = id;

                    if (alternateRow) {
                        if (autoCompleteExtender._completionListItemCssClass != null) {
                            Sys.UI.DomElement.addCssClass(listElement, autoCompleteExtender._completionListItemCssClass);
                        }
                        Sys.UI.DomElement.addCssClass(listElement, this.alternateRowStyle);
                    }

                    this.SetListElementStyle(autoCompleteExtender, list, listElement);
                }
            }
        }
    },

    SetListElementStyle: function(autoCompleteExtender, list, listElement) {
        if (list.style.display == 'none') {
            autoCompleteExtender._popupBehavior.show();
        }

        listElement.style.whiteSpace = 'nowrap';
    },


    OnBlur: function(args) {
        if (!this.customTextEnabled) {
            var hiddenField = $get(this.hiddenFieldClientId);

            if (hiddenField != null) {
                if (hiddenField.value == '') {
                    var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);

                    if (autoCompleteExtender != null) {
                        autoCompleteExtender.get_element().value = '';
                        autoCompleteExtender._currentPrefix = '';
                        if (this._textBoxPart2) { this._textBoxPart2.value = ''; }
                        if (this._textBoxPart3) { this._textBoxPart3.value = ''; }
                    }
                }
            }
        }
    },


    _OnKeyUp: function(ev) {
        var text = this.get_Value();
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
        if (k && k > 88 && k < 193) {
            var index = text.length - 1;
            switch (k) {
                case 192: //Ö
                    this.set_Value(text.substring(0, index) + '0');
                    break;
                case 89: //Y
                    this.set_Value(text.substring(0, index));
                    if (ev.shiftKey) this.set_Value(this.get_Value() + 'Z');
                    else this.set_Value(this.get_Value() + 'z');
                    break;
                case 90: //Z
                    this.set_Value(text.substring(0, index));
                    if (ev.shiftKey) this.set_Value(this.get_Value() + 'Y');
                    else this.set_Value(this.get_Value() + 'y');
                    break;
                case 191: //Ü
                    this.set_Value(text.substring(0, index) + '-');
                    break;
            }
        }

        if (ev.target) {
            if (ev.target == this.get_element()) {
                if (this._textBoxPart2 && ev.target.value.length == this._bankszamlaszamlength_part1) { this._textBoxPart2.focus(); }
            }
            else if (ev.target == this._textBoxPart2) {
                if (this._textBoxPart3 && ev.target.value.length == this._bankszamlaszamlength_part2_short) { this._textBoxPart3.focus(); }
            }
        }



        this.raiseKeyUp(ev);
    },

    _OnKeyDown: function(ev) {
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
        if (k == Sys.UI.Key.enter) {
            return false;
        }
    },

    _OnTextChanged: function(ev) {
        if (this.checkBankszamlaszam) {
            this.AlertBankszamlaszamCheckError();
        }
    },

    _OnPartnerChanged: function(sender, args) {

        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);

        if (autoCompleteExtender != null) {
            var partnerId = args;
            var arrayContextKey = autoCompleteExtender._contextKey.split(';');
            if (arrayContextKey && arrayContextKey.length > 2) {
                arrayContextKey[2] = partnerId;

                autoCompleteExtender._cache = {};
                autoCompleteExtender.set_contextKey(arrayContextKey.join(';'));
            }
        }
    },

    AlertBankszamlaszamCheckError: function() {
        var check = this.CheckBankszamlaszam(this.get_Value);
        if (check != 1) {
            alert(check);
            this.set_Value('');
            return 0;
        }
        return 1;
    },

    _ChecksumValidate: function(sender, args) {
        var bankszamlaszam = this.get_Value();
        var check = this.CheckBankszamlaszam(bankszamlaszam);

        if (check == 1) {
            return true;
        }
        else {
            this._SetValidatorErrorMessage(this._checksumValidator, check);
            return false;
        }
    },

    _SetValidatorErrorMessage: function(validator, message) {
        if (!validator && !message)
            return;

        validator.errormessage = message;
        if (validator.firstChild)
            validator.firstChild.nodeValue = message;

        this._FindValidatorCallout();

        if (this._checksumValidatorCallout) {
            if (this._checksumValidatorCallout._errorMessageCell) {
                this._checksumValidatorCallout._errorMessageCell.innerHTML = message.replace(/\n/g, '<br/>');
            }
        }
    },

    _FindValidatorCallout: function() {
        if (!this._checksumValidatorCallout) {
            if (this.checksumValidatorCalloutId) {
                this._checksumValidatorCallout = $find(this.checksumValidatorCalloutId);
            }
        }
    },

    CheckBankszamlaszam: function(bankszamlaszam) {

        if (bankszamlaszam.trim() == '') return 1;

        if (this.isSixteenCharsFormatEnabled) {
            if ((bankszamlaszam.length != this._bankszamlaszamlength_part1 + this._bankszamlaszamlength_part2_short)
            && (bankszamlaszam.length != this._bankszamlaszamlength_part1 + this._bankszamlaszamlength_part2_long)) {
                //hibas bankszamlaszam hossz
                return String.format(Utility.BankszamlaszamError.badBankszamlaszamLengthSixteenCharsFormatEnabled, bankszamlaszam.length);

            }
        }
        else {
            if (bankszamlaszam.length != this._bankszamlaszamlength_part1 + this._bankszamlaszamlength_part2_long) {

                return String.format(Utility.BankszamlaszamError.badBankszamlaszamLength, bankszamlaszam.length);
            }
        }
        var bankszamlaszam1 = bankszamlaszam.substring(0, this._bankszamlaszamlength_part1);
        var bankszamlaszam2 = bankszamlaszam.substring(this._bankszamlaszamlength_part1);

        var vektor1 = this.checkVektor[0];

        if (vektor1 != null) {
            if (isNaN(parseInt(vektor1))) {
                //rossz vektor
                return Utility.BankszamlaszamError.badVektorFormat;
            }

            if (isNaN(parseInt(bankszamlaszam1))) {
                //rossz bankszamlaszam
                return Utility.BankszamlaszamError.badBankszamlaszamFormat;
            }

            var length = vektor1.length;

            if (length != this._bankszamlaszamlength_part1 - 1) {
                //rossz vektor
                return Utility.BankszamlaszamError.badVektorFormat;
            }

            var checkSum = parseInt(bankszamlaszam1.charAt(length));

            if (isNaN(checkSum)) {
                return Utility.BankszamlaszamError.badBankszamlaszamFormat;
            }

            var c = 0;

            for (var i = 0; i < length; i++) {
                var a = parseInt(bankszamlaszam.charAt(i));
                if (isNaN(a)) return Utility.BankszamlaszamError.badBankszamlaszamFormat;
                var b = parseInt(vektor1.charAt(i));
                if (isNaN(b)) return Utility.BankszamlaszamError.badVektorFormat;
                c = c + a * b;
            }

            //Bankszámlaszám ellenőrző szám (8. jegy) képzése: jegy(8) = 10 - MOD10 (SUM (jegy(i)*vekt(i))) 
            // (i=1 - 7)  vekt=9731973
            c = (10 - (c % 10)) % 10;

            if (checkSum != c) {
                return Utility.BankszamlaszamError.chekSumError;
            }
        }

        if (this.checkVektor.length < 2) {
            //rossz vektor
            return Utility.BankszamlaszamError.badVektorFormat;
        }

        var vektor2 = this.checkVektor[1];

        if (vektor2 != null) {
            if (isNaN(parseInt(vektor2))) {
                //rossz vektor
                return Utility.BankszamlaszamError.badVektorFormat;
            }

            if (isNaN(parseInt(bankszamlaszam2))) {
                //rossz bankszamlaszam
                return Utility.BankszamlaszamError.badBankszamlaszamFormat;
            }

            var length2 = bankszamlaszam2.length - 1;

            if (bankszamlaszam2.length > vektor2.length + 1) {
                //rossz vektor
                return Utility.BankszamlaszamError.badVektorFormat;
            }
            else if (this.isSixteenCharsFormatEnabled) {
                if ((length2 != this._bankszamlaszamlength_part2_short - 1) && (length2 != this._bankszamlaszamlength_part2_long - 1)) {
                    //rossz vektor
                    return Utility.BankszamlaszamError.badVektorFormat;
                }
            }
            else {
                if (length2 != this._bankszamlaszamlength_part2_long - 1) {
                    //rossz vektor
                    return Utility.BankszamlaszamError.badVektorFormat;
                }
            }

            var checkSum2 = parseInt(bankszamlaszam2.charAt(length2));

            if (isNaN(checkSum2)) {
                return Utility.BankszamlaszamError.badBankszamlaszamFormat;
            }

            var c2 = 0;

            for (var i = 0; i < length2; i++) {
                var a2 = parseInt(bankszamlaszam2.charAt(i));
                if (isNaN(a2)) return Utility.BankszamlaszamError.badBankszamlaszamFormat;
                var b2 = parseInt(vektor2.charAt(i));
                if (isNaN(b2)) return Utility.BankszamlaszamError.badVektorFormat;
                c2 = c2 + a2 * b2;
            }

            //Bankszámlaszám ellenőrző szám (8. v. 16. jegy) képzése: jegy(8) = 10 - MOD10 (SUM (jegy(i)*vekt(i))) v. jegy(16) = 10 - MOD10 (SUM (jegy(i)*vekt(i)))
            // (i=1 - 7)  vekt=9731973 v. (i=1 - 15)  vekt=973197319731973
            c2 = (10 - (c2 % 10)) % 10;

            if (checkSum2 != c2) {
                return Utility.BankszamlaszamError.chekSumError;
            }
        }

        return 1;
    },

    padRight: function(str, len, pad) {
        if (!str) { return str; }
        if (!len) { var len = 0; }
        if (!pad) { var pad = ' '; }

        if (len + 1 >= str.length) {
            str = str + Array(len + 1 - str.length).join(pad);
        }

        return str;

    },

    padLeft: function(str, len, pad) {
        if (!str) { return str; }
        if (!len) { var len = 0; }
        if (!pad) { var pad = ' '; }

        if (len + 1 >= str.length) {
            str = Array(len + 1 - str.length).join(pad) + str;
        }

        return str;

    },

    pad: function(str, len, pad) {
        if (!str) { return str; }
        if (!len) { var len = 0; }
        if (!pad) { var pad = ' '; }

        if (len + 1 >= str.length) {
            var right = Math.ceil((padlen = len - str.length) / 2);
            var left = padlen - right;
            str = Array(left + 1).join(pad) + str + Array(right + 1).join(pad);
        }
        return str;

    },


    //getter, setter methods
    get_AutoCompleteExtenderClientId: function() {
        return this.autoCompleteExtenderClientId;
    },

    set_AutoCompleteExtenderClientId: function(value) {
        if (this.autoCompleteExtenderClientId != value) {
            this.autoCompleteExtenderClientId = value;
        }
    },

    get_HiddenFieldClientId: function() {
        return this.hiddenFieldClientId;
    },

    set_HiddenFieldClientId: function(value) {
        if (this.hiddenFieldClientId != value) {
            this.hiddenFieldClientId = value;
        }
    },

    get_TextBoxPart2ClientId: function() {
        return this.textBoxPart2ClientId;
    },

    set_TextBoxPart2ClientId: function(value) {
        if (this.textBoxPart2ClientId != value) {
            this.textBoxPart2ClientId = value;
        }
    },

    get_TextBoxPart3ClientId: function() {
        return this.textBoxPart3ClientId;
    },

    set_TextBoxPart3ClientId: function(value) {
        if (this.textBoxPart3ClientId != value) {
            this.textBoxPart3ClientId = value;
        }
    },

    //
    // Behavior properties
    //
    get_PartnerBehaviorId: function() {
        return this.partnerBehaviorId;
    },

    set_PartnerBehaviorId: function(value) {
        if (this.partnerBehaviorId !== value) {
            this.partnerBehaviorId = value;
            this.raisePropertyChanged('PartnerBehaviorId');
        }
    },

    get_CheckVektor: function() {
        return this.checkVektor;
    },

    set_CheckVektor: function(value) {
        if (this.checkVektor !== value) {
            this.checkVektor = value;
            this.raisePropertyChanged('CheckVektor');
        }
    },

    get_CheckBankszamlaszam: function() {
        return this.checkBankszamlaszam;
    },

    set_CheckBankszamlaszam: function(value) {
        if (this.checkBankszamlaszam !== value) {
            this.checkBankszamlaszam = value;
            this.raisePropertyChanged('CheckBankszamlaszam');
        }
    },

    get_ChecksumValidatorId: function() {
        return this.checksumValidatorId;
    },

    set_ChecksumValidatorId: function(value) {
        if (this.checksumValidatorId !== value) {
            this.checksumValidatorId = value;
            this.raisePropertyChanged('ChecksumValidatorId');
        }
    },

    get_ChecksumValidatorCalloutId: function() {
        return this.checksumValidatorCalloutId;
    },

    set_ChecksumValidatorCalloutId: function(value) {
        if (this.checksumValidatorCalloutId !== value) {
            this.checksumValidatorCalloutId = value;
            this.raisePropertyChanged('ChecksumValidatorCalloutId');
        }
    },


    get_UserId: function() {
        return this.userId;
    },

    set_UserId: function(value) {
        if (this.userId !== value) {
            this.userId = value;
            this.raisePropertyChanged('UserId');
        }
    },

    get_LoginId: function() {
        return this.loginId;
    },

    set_LoginId: function(value) {
        if (this.loginId !== value) {
            this.loginId = value;
            this.raisePropertyChanged('LoginId');
        }
    },

    get_IsSixteenCharsFormatEnabled: function() {
        return this.isSixteenCharsFormatEnabled;
    },

    set_IsSixteenCharsFormatEnabled: function(value) {
        if (this.isSixteenCharsFormatEnabled !== value) {
            this.isSixteenCharsFormatEnabled = value;
            this.raisePropertyChanged('IsSixteenCharsFormatEnabled');
        }
    },

    get_CustomTextEnabled: function() {
        return this.customTextEnabled;
    },

    set_CustomTextEnabled: function(value) {
        if (this.customTextEnabled != value) {
            this.customTextEnabled = value;
        }
    },

    get_Value: function() {
        var paddingChar = ' ';
        var tb1 = this.get_element();
        var sb = new Sys.StringBuilder();
        if (tb1) {
            if (this._textBoxPart2 && this._textBoxPart3) {
                if (this._textBoxPart3.value != '') {
                    sb.append(this.padRight(tb1.value, 8, paddingChar));
                    sb.append(this.padRight(this._textBoxPart2.value, 8, paddingChar));
                    sb.append(this._textBoxPart3.value);
                }
                else if (this._textBoxPart2.value != '') {
                    sb.append(this.padRight(tb1.value, 8, paddingChar));
                    sb.append(this._textBoxPart2.value);
                }
                else {
                    sb.append(tb1.value);
                }
            }
            else {
                sb.append(tb1.value);
            }
        }
        return sb.toString();
    },

    set_Value: function(value) {
        if (!value) { value = ''; }
        var tb1 = this.get_element();
        if (tb1) {
            if (this._textBoxPart2) {
                var txt = value;
                tb1.value = txt.substr(0, Math.min(txt.length, 8)).trim();
                txt = txt.substr(Math.min(txt.length, 8));
                this._textBoxPart2.value = txt.substr(0, Math.min(txt.length, 8)).trim();
                txt = txt.substr(Math.min(txt.length, 8));
                if (this._textBoxPart3) {
                    this._textBoxPart3.value = txt.substr(0, Math.min(txt.length, 8)).trim();
                    txt = txt.substr(Math.min(txt.length, 8));
                }
            }
        }
    },

    //
    // Events
    //

    add_keyUp: function(handler) {

        this.get_events().addHandler('keyUp', handler);
    },

    remove_keyUp: function(handler) {

        this.get_events().removeHandler('keyUp', handler);
    },

    raiseKeyUp: function(eventArgs) {

        var handler = this.get_events().getHandler('keyUp');
        if (handler) {
            handler(this, eventArgs);
        }
    }
}

Utility.BankszamlaszamBehavior.registerClass('Utility.BankszamlaszamBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();