﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.BarcodeError = {};
Utility.BarcodeError.badVektorFormat = 'Az ellenőrző vektor formátuma nem megfelelő!\n(Csak számokat tartalmazhat)';
Utility.BarcodeError.badBarCodeFormat = 'A vonalkód formátuma nem megfelelő!\n(Csak számokat tartalmazhat)';
Utility.BarcodeError.badBarCodeLength = 'A vonalkód hossza ({0}) nem megfelelő!\n(A kívánt hossz: {1})';
Utility.BarcodeError.chekSumError = 'A vonalkód ellenőrző összege hibás!';
// BLG_1940
Utility.BarcodeError.badPrefixError = 'A vonalkód prefix nem megfelelő!';

Utility.BarCodeBehavior = function(element)
{
    Utility.BarCodeBehavior.initializeBase(this, [element]);
    
    //Properties
    // BLG_1940
    this.checkPrefix = '';

    this.checkVektor = null;
    this.checksumValidatorId = null;
    this.checksumValidatorCalloutId = null;
    this.bgSoundId = null;
    this.checkVonalkod = true;
    this.playSoundOnError = false;
    this.playSoundOnAccept = false;
    this.soundOnError = '';
    this.soundOnAccept = '';
    this.replaceHungarianCharacters = false;
    //Variables
    this.barcodeLength = -1;
    this._checksumValidator = null;
    this._checksumValidatorCallout = null;
    this._bgSound = null;
    this._keyUpHandler = null;
    this._keyDownHandler = null;
    this._textChangedHandler = null;
    this._checksumValidationHandler = null;
    this._keyPressHandler = null;
}

Utility.BarCodeBehavior.prototype = 
{
    
    initialize : function() {
        Utility.BarCodeBehavior.callBaseMethod(this, 'initialize');
        
        this._keyUpHandler = Function.createDelegate(this, this._OnKeyUp);
        this._keyDownHandler = Function.createDelegate(this, this._OnKeyDown);
        this._keyPressHandler = Function.createDelegate(this, this._OnKeyPress);
        
        var element = this.get_element();
        if (element) 
        {
            $addHandler(element, "keyup", this._keyUpHandler);
            $addHandler(element, "keydown", this._keyDownHandler);
            $addHandler(element, "keypress", this._keyPressHandler);
        }
        
        if(this.checkVonalkod)
        {
        
//            this._textChangedHandler = Function.createDelegate(this, this._OnTextChanged);
// 
//            if(element)
//            {
//                $addHandler(element, "change", this._textChangedHandler);
//            }
        
        }
        
        if(this.checksumValidatorId)
        {
            this._checksumValidator = $get(this.checksumValidatorId);
            
            if(this._checksumValidator)
            {
               this._checksumValidationHandler = Function.createDelegate(this, this._ChecksumValidate);
               this._checksumValidator.evaluationfunction = this._checksumValidationHandler;
            }
        }
        
        if(this.bgSoundId)
        {
            this._bgSound = $get(this.bgSoundId);
        }        
        
        if(this.checkVektor)
        {
            // BUG_2107
            //this.barcodeLength = this.checkVektor.length + 1;
            if (this.checkPrefix !== null && this.checkVektor !== null)
                this.barcodeLength = this.checkVektor.length + this.checkPrefix.length +1;

        }
       
        
    },
    
    dispose : function() {
        
        var element = this.get_element();
        
        if(element)
        {
            if(this._keyUpHandler)
            {
                $removeHandler(element, "keyup", this._keyUpHandler);
            }
            this._keyUpHandler = null;
            if(this._textChangedHandler)
            {
                $removeHandler(element, "change", this._textChangedHandler);
            }
            this._textChangedHandler = null;
            if(this._keyDownHandler)
            {
                $removeHandler(element, "keydown", this._keyDownHandler);
            }
            this._keyDownHandler = null;
            if (this._keyPressHandler) {
                $removeHandler(element, "keypress", this._keyPressHandler);
            }
            this._keyPressHandler = null;
            
            if(this._checksumValidator)
            {
                this._checksumValidator.evaluationfunction = null;
                this._checksumValidator.errormessage = null;
                this._checksumValidator = null;
            }
            
            this._checksumValidationHandler = null;
            
            this._checksumValidatorCallout = null;    
        }
        
        Utility.BarCodeBehavior.callBaseMethod(this, 'dispose');
    },
    
     
    _OnKeyUp: function(ev)
    {
        if (!this.replaceHungarianCharacters) {
            console.log("replaceHungarianCharacters = false");
            return;
        } else {
            console.log("replaceHungarianCharacters = true");
        }

        var element = this.get_element();
        var text = element.value;
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
        if(k && k > 88 && k < 193)
        { 
            var index = text.length -1;
                switch(k)
                {
                   case 192: //Ö
                      element.value = text.substring(0,index) + '0'; 
                      break;              
                   case 89: //Y
                      element.value = text.substring(0,index); 
                      if(ev.shiftKey) element.value += 'Z';
                      else element.value += 'z';
                      break; 
                   case 90: //Z
                      element.value = text.substring(0,index); 
                      if(ev.shiftKey) element.value += 'Y';
                      else element.value += 'y';
                      break;
                   case 191: //Ü
                      element.value = text.substring(0,index) + '-'; 
                      break;   
                }    
        }
      
        this.raiseKeyUp(ev);
        // BUG_8925
        if (this.PostKeyUpEventJs) {
            
            var args = 'barCodeBehaviorParam';
            var f = new Function(args, this.PostKeyUpEventJs);
            f(this);
            
        }
    },
    
    _OnKeyDown: function(ev)
    {
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
        if(k === Sys.UI.Key.enter)
         {
            return false;
         }
    },

    _OnKeyPress: function (ev) {
        var k = event.which || event.keyCode;
        if (k == Sys.UI.Key.enter) {
            ev.preventDefault();
        }
    },
    
    _OnTextChanged: function(ev)
    { 
       if(this.checkVonalkod)
       {
            this.AlertVonalkodCheckError();
       }
    },
    
    AlertVonalkodCheckError: function()
    {
        var element = this.get_element();
        var check = this.CheckVonalkod(element.value);
        if(check !== 1)
        {
            alert(check);
            element.value = '';
            if (this.playSoundOnError === true)
            {
                this.PlaySound(this.soundOnError);
            }
            return 0;
        }
        
        if (this.playSoundOnAccept === true)
        {
            this.PlaySound(this.soundOnAccept);
        }
        
        return 1;
    },
    
    _ChecksumValidate: function(sender, args)
    {
        var element = this.get_element();
        var vonalkod = element.value;
        var check = this.CheckVonalkod(vonalkod);
        
        if(check === 1)
        {
            return true;
        }
        else
        {
            this._SetValidatorErrorMessage(this._checksumValidator,check);
            return false;
        }
    },
    
    _SetValidatorErrorMessage: function(validator, message)
    {
        if(! validator && !message)
            return;
        
        validator.errormessage = message;
        if(validator.firstChild)
            validator.firstChild.nodeValue = message;
               
       this._FindValidatorCallout();
        
        if(this._checksumValidatorCallout)
        {
            if(this._checksumValidatorCallout._errorMessageCell)
            {
                this._checksumValidatorCallout._errorMessageCell.innerHTML = message.replace(/\n/g,'<br/>');
            }
        } 
    },
    
    _FindValidatorCallout : function()
    {
        if(!this._checksumValidatorCallout)
        {
            if(this.checksumValidatorCalloutId)
            {
                this._checksumValidatorCallout = $find(this.checksumValidatorCalloutId);
            }
        }
    },
    
    CheckVonalkod: function(vonalkod)
    {
        if(vonalkod.trim() === '') return 1;
        var vektor = this.checkVektor;
        if(vektor !== null)
        {
            if(isNaN(parseInt(vektor)))
            {
                //rossz vektor
                return Utility.BarcodeError.badVektorFormat;
            }
            // BLG_1940
            //  BUG_2107
          //  var fullength = this.barcodeLength + this.checkPrefix.length;
          //  if (vonalkod.length != fullength)
              // return String.format(Utility.BarcodeError.badBarCodeLength, vonalkod.length, fullength);
            if (vonalkod.length !== this.barcodeLength)
                return String.format(Utility.BarcodeError.badBarCodeLength, vonalkod.length, this.barcodeLength);
            if (vonalkod.substr(0, this.checkPrefix.length) !== this.checkPrefix)
                return Utility.BarcodeError.badPrefixError;
           
            //var vonalkodNum = vonalkod.substr(vonalkod.length - this.barcodeLength);
            var vonalkodNum = vonalkod.substr(vonalkod.length - (this.barcodeLength - this.checkPrefix.length));

            if(isNaN(parseInt(vonalkodNum)))
            {
                //rossz vonalkod
                return Utility.BarcodeError.badBarCodeFormat;
            }
            
            var length = vektor.length;
            //if(vonalkodNum.length != this.barcodeLength)
            if (vonalkodNum.length !== this.barcodeLength - this.checkPrefix.length)

            {
                //hibas vonalkod hossz
                return String.format(Utility.BarcodeError.badBarCodeLength,vonalkod.length, this.barcodeLength);
                //return String.format(Utility.BarcodeError.badBarCodeLength, vonalkod.length, fullength);

            }
            
            var checkSum = parseInt(vonalkodNum.charAt(length));
            
            if(isNaN(checkSum))
            {
                return Utility.BarcodeError.badBarCodeFormat;
            }
            
            var c = 0;
            
            for(var i = 0;i<length; i++)
            {
                var a = parseInt(vonalkodNum.charAt(i));
                if(isNaN(a)) return Utility.BarcodeError.badBarCodeFormat;
                var b = parseInt(vektor.charAt(i));
                if(isNaN(b)) return Utility.BarcodeError.badVektorFormat;
                c = c + a*b;
            }
            c = 10 - (c % 10);
            if(c === 10) c=0;
            
            if(checkSum === c)
            {
                return 1;
            }
            else
            {
                return Utility.BarcodeError.chekSumError;
            }
        }
        
        return 1;
    },
    
    PlaySound: function(sound)
    {
        if (sound)
        {
            if (this._bgSound)
            {
                this._bgSound.src = sound;
            }
        }
    
    },
    
    //
    // Behavior properties
    //
    
    get_CheckVektor : function() {
        return this.checkVektor;
    },

    set_CheckVektor : function(value) {
        if (this.checkVektor !== value) {
            this.checkVektor = value;
            this.raisePropertyChanged('CheckVektor');
        }
    },
    
    get_CheckVonalkod : function() {
        return this.checkVonalkod;
    },

    set_CheckVonalkod : function(value) {
        if (this.checkVonalkod !== value) {
            this.checkVonalkod = value;
            this.raisePropertyChanged('CheckVonalkod');
        }
    },
    
    get_ChecksumValidatorId : function() {
        return this.checksumValidatorId;
    },

    set_ChecksumValidatorId : function(value) {
        if (this.checksumValidatorId !== value) {
            this.checksumValidatorId = value;
            this.raisePropertyChanged('ChecksumValidatorId');
        }
    },
    
    get_ChecksumValidatorCalloutId : function() {
        return this.checksumValidatorCalloutId;
    },

    set_ChecksumValidatorCalloutId : function(value) {
        if (this.checksumValidatorCalloutId !== value) {
            this.checksumValidatorCalloutId = value;
            this.raisePropertyChanged('ChecksumValidatorCalloutId');
        }
    },

    // BLG_1940
    get_CheckPrefix: function () {
        return this.checkPrefix;
    },

    set_CheckPrefix: function (value) {
        if (this.checkPrefix !== value) {
            this.checkPrefix = value;
            this.raisePropertyChanged('CheckPrefix');
        }
    },
    get_BgSoundId : function() {
        return this.bgSoundId;
    },

    set_BgSoundId : function(value) {
        if (this.bgSoundId !== value) {
            this.bgSoundId = value;
            this.raisePropertyChanged('BgSoundId');
        }
    },
    
    get_PlaySoundOnError : function() {
        return this.playSoundOnError;
    },

    set_PlaySoundOnError : function(value) {
        if (this.playSoundOnError !== value) {
            this.playSoundOnError = value;
            this.raisePropertyChanged('PlaySoundOnError');
        }
    },
    
    get_PlaySoundOnAccept : function() {
        return this.playSoundOnAccept;
    },

    set_PlaySoundOnAccept : function(value) {
        if (this.playSoundOnAccept !== value) {
            this.playSoundOnAccept = value;
            this.raisePropertyChanged('PlaySoundOnAccept');
        }
    },
    
    get_SoundOnError : function() {
        return this.soundOnError;
    },

    set_SoundOnError : function(value) {
        if (this.soundOnError !== value) {
            this.soundOnError = value;
            this.raisePropertyChanged('SoundOnError');
        }
    },
    
    get_SoundOnAccept : function() {
        return this.soundOnAccept;
    },

    set_SoundOnAccept : function(value) {
        if (this.soundOnAccept !== value) {
            this.soundOnAccept = value;
            this.raisePropertyChanged('SoundOnAccept');
        }
    },       
    // BUG_8925
    get_PostKeyUpEventJs: function () {
        return this.PostKeyUpEventJs;
    },

    set_PostKeyUpEventJs: function (value) {
        if (this.PostKeyUpEventJs !== value) {
            this.PostKeyUpEventJs = value;
            this.raisePropertyChanged('PostKeyUpEventJs');
        }
    },
    get_ReplaceHungarianCharacters: function () {
        return this.ReplaceHungarianCharacters;
    },
    set_ReplaceHungarianCharacters: function (value) {
        if (this.ReplaceHungarianCharacters !== value) {
            this.ReplaceHungarianCharacters = value;
            this.raisePropertyChanged('ReplaceHungarianCharacters');
        }
    },
    //
    // Events
    //
    
    add_keyUp : function(handler) {

        this.get_events().addHandler('keyUp', handler);
    },
    
    remove_keyUp : function(handler) {

        this.get_events().removeHandler('keyUp', handler);
    },
    
    raiseKeyUp : function(eventArgs) {

        var handler = this.get_events().getHandler('keyUp');
        if (handler)
        {
            handler(this, eventArgs);
        }
    }
}

Utility.BarCodeBehavior.registerClass('Utility.BarCodeBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();