// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.BarcodeRagszamMode = function() {
    throw Error.invalidOperation();
}

Utility.BarcodeRagszamMode.prototype = {
    Barcode: 1,
    BarcodeRagszam: 2,
    BarcodeRagszamTertivevenyVonalkod: 3
}

Utility.BarcodeRagszamMode.registerEnum("Utility.BarcodeRagszamMode", false);

Utility.BarcodeRagszamListaBehavior = function(element) {
    Utility.BarcodeRagszamListaBehavior.initializeBase(this, [element]);

    // Constants
    this.backGroundColorOnError = '#FF0000';

    //Properties
    this.barCodeBehaviorId = null;
    this.ragszamTextBoxClientId = null;
    this.ragszamBehaviorId = null;
    this.tertivevenyVonalkodBehaviorId = null;
    this.imageAddId = null;
    this.imageDeleteId = null;
    this.hiddenFieldClientStateId = null;
    this.maxElementsNumber = -1;
    this.listIsFullMessage = 'A lista tele!';
    this.cbSoundEffectId = null;

    this.checkBoxClientIds = null; // t�pust meghat�roz� checkboxok
    //Variables
    this.barCodeBehavior = null;
    this.barCodeTextBox = null;
    this.ragszamTextBox = null;
    this.tertivevenyVonalkodTextBox = null;
    this.ragszamBehavior = null;
    this.tertivevenyVonalkodBehavior = null;
    this.imageAdd = null;
    this.imageDelete = null;
    this.hiddenFieldClientState = null;

    this.tipus = Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod;

    this._applicationLoadHandler = null;
    this._barcodeBehaviorKeyUpHandler = null;
    this._ragszamBehaviorKeyUpHandler = null;
    this._ragszamBehaviorPropertyChangedHandler = null;
    this._tertivevenyVonalkodBehaviorKeyUpHandler = null;
    this._imageAddClickHandler = null;
    this._imageDeleteClickHandler = null;
    this._listBoxKeyDownHandler = null;
    this._cbSoundEffect = null;
    this._cbSoundEffectClickHandler = null;
}

Utility.BarcodeRagszamListaBehavior.prototype =
{

    initialize: function() {
        Utility.BarcodeRagszamListaBehavior.callBaseMethod(this, 'initialize');
        this._applicationLoadHandler = Function.createDelegate(this, this._OnApplicationLoad);
        Sys.Application.add_load(this._applicationLoadHandler);

        this._imageAddClickHandler = Function.createDelegate(this, this._OnImageAddClick);
        this._imageDeleteClickHandler = Function.createDelegate(this, this._OnImageDeleteClick);
        this._barcodeBehaviorKeyUpHandler = Function.createDelegate(this, this._OnBarcodeBehaviorKeyUp);
        this._ragszamBehaviorKeyUpHandler = Function.createDelegate(this, this._OnRagszamBehaviorKeyUp);
        //this._ragszamBehaviorPropertyChangedHandler = Function.createDelegate(this, this._OnRagszamBehaviorPropertyChanged);
        this._tertivevenyVonalkodBehaviorKeyUpHandler = Function.createDelegate(this, this._OnTertivevenyVonalkodBehaviorKeyUp);
        this._listBoxKeyDownHandler = Function.createDelegate(this, this._OnBarcodeRagszamListaKeyDown);
        this._cbSoundEffectClickHandler = Function.createDelegate(this, this._OnCbSoundEffectClick);

        if (this.imageAddId) {
            this.imageAdd = $get(this.imageAddId);
        }

        if (this.imageDeleteId) {
            this.imageDelete = $get(this.imageDeleteId);
        }

        if (this.hiddenFieldClientStateId) {
            this.hiddenFieldClientState = $get(this.hiddenFieldClientStateId);
        }

        if (this.cbSoundEffectId) {
            this._cbSoundEffect = $get(this.cbSoundEffectId);
        }

        if (this.imageAdd) {
            $addHandler(this.imageAdd, 'click', this._imageAddClickHandler);
        }

        if (this.imageDelete) {
            $addHandler(this.imageDelete, 'click', this._imageDeleteClickHandler);
        }

        if (this._cbSoundEffect) {
            $addHandler(this._cbSoundEffect, 'click', this._cbSoundEffectClickHandler);
        }

        element = this.get_element();

        if (element) {
            $addHandler(element, 'keydown', this._listBoxKeyDownHandler);
        }


        this._InitBarcodeRagszamListaClientState();
    },

    _OnApplicationLoad: function() {
        if (this.barCodeBehaviorId) {
            this.barCodeBehavior = $find(this.barCodeBehaviorId);
        }
        if (this.barCodeBehavior) {
            this.barCodeTextBox = this.barCodeBehavior.get_element();
            this.barCodeBehavior.add_keyUp(this._barcodeBehaviorKeyUpHandler);
            this.barCodeBehavior.set_CheckVonalkod(false);
        }

        //        if (this.ragszamTextBoxClientId) {
        //            this.ragszamTextBox = $get(this.ragszamTextBoxClientId);
        //            if (this.ragszamTextBox) {
        //                $addHandler(this.ragszamTextBox, "keyup", this._ragszamBehaviorKeyUpHandler);
        //            }
        //        }

        if (this.ragszamBehaviorId) {
            this.ragszamBehavior = $find(this.ragszamBehaviorId);
        }
        if (this.ragszamBehavior) {
            this.ragszamTextBox = this.ragszamBehavior.get_element();
            this.ragszamBehavior.add_keyUp(this._ragszamBehaviorKeyUpHandler);
            //            this.ragszamBehavior.add_propertyChanged(this._ragszamBehaviorPropertyChangedHandler);
            this._CheckRagszamInRagszamLista();
            this.ragszamBehavior.set_CheckRagszam(false);
        }

        if (this.tertivevenyVonalkodBehaviorId) {
            this.tertivevenyVonalkodBehavior = $find(this.tertivevenyVonalkodBehaviorId);
        }
        if (this.tertivevenyVonalkodBehavior) {
            this.tertivevenyVonalkodTextBox = this.tertivevenyVonalkodBehavior.get_element();
            this.tertivevenyVonalkodBehavior.add_keyUp(this._tertivevenyVonalkodBehaviorKeyUpHandler);
            this.tertivevenyVonalkodBehavior.set_CheckVonalkod(false);
        }

    },

    dispose: function() {

        if (this._applicationLoadHandler != null) {
            Sys.Application.remove_load(this._applicationLoadHandler);
            this._applicationLoadHandler = null;
        }

        if (this._barcodeBehaviorKeyUpHandler && this.barCodeBehavior) {
            this.barCodeBehavior.remove_keyUp(this._barcodeBehaviorKeyUpHandler);
        }

        this.barCodeBehavior = null;
        this.barCodeTextBox = null;
        this._barcodeBehaviorKeyUpHandler = null;

        //        if (this._ragszamBehaviorKeyUpHandler && this.ragszamtextBox) {
        //            $removeHandler(this.ragszamtextBox, "keyup", this._ragszamBehaviorKeyUpHandler);
        //        }
        if (this.ragszamBehavior) {
            if (this._ragszamBehaviorKeyUpHandler) {
                this.ragszamBehavior.remove_keyUp(this._ragszamBehaviorKeyUpHandler);
            }
            if (this._ragszamBehaviorPropertyChangedHandler) {
                this.ragszamBehavior.remove_propertyChanged(this._ragszamBehaviorPropertyChangedHandler);
            }
        }
        this.ragszamBehavior = null;
        this.ragszamTextBox = null;
        this._ragszamBehaviorKeyUpHandler = null;
        this._ragszamBehaviorPropertyChangedHandler = null;

        if (this._tertivevenyVonalkodBehaviorKeyUpHandler && this.tertivevenyVonalkodBehavior) {
            this.tertivevenyVonalkodBehavior.remove_keyUp(this._tertivevenyVonalkodBehaviorKeyUpHandler);
        }

        this.tertivevenyVonalkodBehavior = null;
        this.tertivevenyVonalkodTextBox = null;
        this._tertivevenyVonalkodBehaviorKeyUpHandler = null;

        if (this.imageAdd && this._imageAddClickHandler) {
            $removeHandler(this.imageAdd, 'click', this._imageAddClickHandler);
        }

        this.imageAdd = null;
        this._imageAddClickHandler = null;

        if (this.imageDelete && this._imageDeleteClickHandler) {
            $removeHandler(this.imageDelete, 'click', this._imageDeleteClickHandler);
        }

        this.imageDelete = null;
        this._imageDeleteClickHandler = null;

        if (this._cbSoundEffect && this._cbSoundEffectClickHandler) {
            $removeHandler(this._cbSoundEffect, 'click', this._cbSoundEffectClickHandler);
        }

        this._cbSoundEffect = null;
        this._cbSoundEffectClickHandler = null;

        if (this._listBoxKeyDownHandler) {
            $removeHandler(element, 'keydown', this._listBoxKeyDownHandler);
        }

        this._listBoxKeyDownHandler = null;

        this.hiddenFieldClientState = null;

        Utility.BarcodeRagszamListaBehavior.callBaseMethod(this, 'dispose');
    },


    _InitBarcodeRagszamListaClientState: function() {
        if (this.hiddenFieldClientState && this.hiddenFieldClientState.value.trim() != '') {
            var gridView = this.get_element();
            if (gridView && gridView.rows && gridView.rows.length <= 2) {
                if (gridView.rows.length == 2) {
                    if (gridView.rows[1].cells && gridView.rows[1].cells[0]) {
                        if (!gridView.rows[1].cells[0].style || gridView.rows[1].cells[0].style.display != 'none')
                            return;
                    }
                }
            }

            var clientState = this.hiddenFieldClientState.value.trim();
            var barcodeArray = clientState.split(';');
            for (var i = 0; i < barcodeArray.length; i++) {
                var item = barcodeArray[i].trim();
                if (item != '') {
                    var elements = item.split(',');
                    var barcode = elements[0];
                    var ragszam = '';
                    var tertivevenyVonalkod = '';
                    if ((this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszam || this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod) && elements.length > 1) {
                        ragszam = elements[1];

                        if (this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod && elements.length > 2) {
                            tertivevenyVonalkod = elements[2];
                        }
                    }
                    this.AddElementToBarcodeRagszamLista(gridView, barcode, ragszam, tertivevenyVonalkod);
                }
            }
        }
    },

    _OnBarcodeBehaviorKeyUp: function(sender, ev) {
        if (this.barCodeBehavior && this.barCodeTextBox) {
            var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
            if (k == Sys.UI.Key.enter || this.barCodeBehavior.barcodeLength == this.barCodeTextBox.value.length) {
                if ((this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszam || this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod) && this.ragszamTextBox) {
                    this.ragszamTextBox.focus();
                }
                else {
                    var element = this.get_element();
                    if (this.get_Tipus() == Utility.BarcodeRagszamMode.Barcode) {
                        if (this.AddElementToBarcodeRagszamLista(element, this.barCodeTextBox.value, '', '')) {
                            this.barCodeTextBox.value = '';
                            this.barCodeTextBox.focus();
                        }
                    }
                }

            }
        }
    },


    _OnRagszamBehaviorKeyUp: function(sender, ev) {
        if (this.ragszamBehavior && this.ragszamTextBox) {
            var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
            if (k == Sys.UI.Key.enter || this.ragszamBehavior.IsDataCompleted()) {
                if ((this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod) && this.tertivevenyVonalkodTextBox) {
                    this.tertivevenyVonalkodTextBox.focus();
                }
                else {
                    var element = this.get_element();
                    if (this.AddElementToBarcodeRagszamLista(element, this.barCodeTextBox.value, this.ragszamTextBox.value, '')) {
                        this.barCodeTextBox.value = '';
                        this.ragszamTextBox.value = '';
                        this.barCodeTextBox.focus();
                    }
                }
            }

        }
    },

    _OnTertivevenyVonalkodBehaviorKeyUp: function(sender, ev) {
        if (this.tertivevenyVonalkodBehavior && this.tertivevenyVonalkodTextBox) {
            var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
            if (k == Sys.UI.Key.enter || this.tertivevenyVonalkodBehavior.barcodeLength == this.tertivevenyVonalkodTextBox.value.length) {
                var element = this.get_element();
                if (this.AddElementToBarcodeRagszamLista(element, this.barCodeTextBox.value, this.ragszamTextBox.value, this.tertivevenyVonalkodTextBox.value)) {
                    this.barCodeTextBox.value = '';
                    this.ragszamTextBox.value = '';
                    this.tertivevenyVonalkodTextBox.value = '';
                    this.barCodeTextBox.focus();
                }
            }

        }
    },



    _OnBarcodeRagszamListaKeyDown: function(ev) {
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
        if (k == Sys.UI.Key.del) {
            var gridView = this.get_element();
            this.RemoveSelectedElementFromBarcodeRagszamLista(gridView);
        }
    },

    _OnImageAddClick: function(ev) {
        if (this.barCodeTextBox && this.barCodeTextBox.value.trim() != '') {
            var gridView = this.get_element();
            if (gridView) {
                var barcode = '';
                var ragszam = '';
                var tertivevenyVonalkod = '';
                if (this.barCodeTextBox) {
                    barcode = this.barCodeTextBox.value;
                }
                if (this.ragszamTextBox) {
                    ragszam = this.ragszamTextBox.value;
                }
                if (this.tertivevenyVonalkodtextBox) {
                    tertivevenyVonalkod = this.tertivevenyRTextBox.value;
                }
                if (this.AddElementToBarcodeRagszamLista(gridView, barcode, ragszam, tertivevenyVonalkod)) {
                    this.barCodeTextBox.value = '';
                    if (this.ragszamTextBox) {
                        this.ragszamTextBox.value = '';
                    }
                    if (this.tertivevenyVonalkodtextBox) {
                        this.tertivevenyVonalkodTextBox.value = '';
                    }
                    if (this.barCodeTextBox) {
                        this.barCodeTextBox.focus();
                    }
                }
            }
        }
    },

    _OnImageDeleteClick: function(ev) {
        var gridView = this.get_element();
        this.RemoveSelectedElementFromBarcodeRagszamLista(gridView);
    },

    _OnCbSoundEffectClick: function(ev) {
        if (this.barCodeBehavior && this._cbSoundEffect) {
            this.barCodeBehavior.set_PlaySoundOnAccept(this._cbSoundEffect.checked);
        }
    },

    //    _OnRagszamBehaviorPropertyChanged: function(sender, args) {
    //        if (args.get_propertyName() == 'IsKulfold') {
    //            // TODO: check ragsz�mok a gridviewban
    //            this._CheckRagszamInRagszamLista();
    //        }
    //    },

    _CheckRagszamInRagszamLista: function() {
        var gridView = this.get_element();

        if (gridView && gridView.rows) {
            var rows = gridView.rows;
            var rowsLength = rows.length;
            if (rowsLength >= 2) {
                var tipus = this.get_Tipus();

                if (tipus == Utility.BarcodeRagszamMode.BarcodeRagszam || tipus == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod) {

                    // ignore header row
                    var msg = '';
                    for (var i = 1; i < rowsLength; i++) {
                        if (!rows[i].style || rows[i].style.display != 'none') {
                            if (rows[i].cells.length > 2) {
                                var ragszam = rows[i].cells[2].innerText;
                                var checkresult = this.ragszamBehavior.CheckRagszam(ragszam);
                                if (checkresult != 1) {
                                    //alert(checkresult); // debug
                                    msg += ragszam + ': ' + checkresult + '\n';
                                    rows[i].cells[2].style.backgroundColor = this.backGroundColorOnError;
                                }
                                else {
                                    rows[i].cells[2].style.backgroundColor = '';
                                }
                            }
                        }
                    }

                    if (msg != '') {
                        alert(msg);
                    }

                }
            }
        }

    },

    _AddRowToBarcodeRagszamLista: function(gridView, barcode, ragszam, tertivevenyVonalkod) {
        if (!gridView) return false;
        barcode = barcode.trim();
        if (barcode && barcode != '') {
            if (gridView && gridView.rows) {
                if (gridView.rows.length >= 2) {
                    if (gridView.rows[1].style && gridView.rows[1].style.display == 'none') {
                        var tr = gridView.rows[1].cloneNode(true);

                        tr.style.display = '';
                        if (tr.children && tr.children.length > 0) {
                            if (tr.children[0] && tr.children[0].lastChild) {
                                //tr.children[0].firstChild.value = barcode;
                                tr.children[0].lastChild.innerHTML = barcode;
                            }

                            if (tr.children && tr.children.length > 1) {
                                tr.children[1].innerHTML = barcode;
                            }

                            if (this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszam || this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod) {
                                if (tr.children && tr.children.length > 2) {
                                    tr.children[2].innerHTML = ragszam;
                                    tr.children[2].style.backgroundColor = '';

                                    if (this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod) {
                                        if (tr.children.length > 3) {
                                            tr.children[3].innerHTML = tertivevenyVonalkod;
                                        }
                                    }
                                }
                            }
                            gridView.firstChild.appendChild(tr);
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    },

    AddElementToBarcodeRagszamLista: function(gridView, barcode, ragszam, tertivevenyVonalkod) {
        if (this.barCodeBehavior) {
            var check = this.barCodeBehavior.AlertVonalkodCheckError();
            if (check != 1) {
                return 0;
            }
        }

        if (this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszam && this.ragszamBehavior) {
            var check = this.ragszamBehavior.AlertRagszamCheckError();
            if (check != 1) {
                return 0;
            }
        }

        else if (this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod && this.tertivevenyVonalkodBehavior) {
            var check = this.tertivevenyVonalkodBehavior.AlertVonalkodCheckError();
            if (check != 1) {
                return 0;
            }
        }

        if (!gridView || !gridView.rows || !barcode)
            return 0;

        var rows = gridView.rows;
        var rowsLength = rows.length;
        var isElementInList = 0;

        for (var i = 0; i < rowsLength; i++) {
            if (rows[i].cells.length > 0 && rows[i].cells[0].lastChild.innerHTML == barcode) {
                isElementInList = 1;

                if ((this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszam || this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod) && rows[i].cells.length > 2) {
                    rows[i].cells[2].innerHTML = ragszam;
                    rows[i].cells[2].style.backgroundColor = '';
                    this.SaveVonalkodState(gridView, this.hiddenFieldClientState);

                    if (this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod && rows[i].cells.length > 3) {
                        rows[i].cells[3].innerHTML = tertivevenyVonalkod;
                        this.SaveVonalkodState(gridView, this.hiddenFieldClientState);
                    }
                }

                return 1;
            }
        }

        if (isElementInList == 0) {
            if (this.maxElementsNumber > -1 && this.maxElementsNumber < rowsLength) {
                alert(this.listIsFullMessage);
                return 0;
            }

            var IsAdded = this._AddRowToBarcodeRagszamLista(gridView, barcode, ragszam, tertivevenyVonalkod)

            if (IsAdded) {
                this.SaveVonalkodState(gridView, this.hiddenFieldClientState);
            }
            return 1;

        }
        else { };

        return 0;
    },

    RemoveSelectedElementFromBarcodeRagszamLista: function(gridView) {
        if (!gridView || !gridView.rows)
            return 0;

        var rows = gridView.rows;
        var rowsLength = rows.length;

        if (rowsLength > 0) {
            for (var i = rowsLength - 1; i > 0; i--) {
                if (!rows[i].style || rows[i].style.display != 'none') {
                    var cb = rows[i].cells[0].firstChild;
                    if (cb && cb.checked) {
                        rows[i].removeNode(true);
                    }
                }
            }
            this.SaveVonalkodState(gridView, this.hiddenFieldClientState);
            return 1;
        }

        return 0;
    },

    GetBarcodeRagszamListaElementsString: function(gridView) {
        if (!gridView || !gridView.rows)
            return '';

        var rows = gridView.rows;
        var rowsLength = rows.length;
        var sb = new Sys.StringBuilder();

        var isFirst = true;
        var bGetRagszam = ((this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszam || this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod) ? true : false);
        var bGetTertivevenyVonalkod = (this.get_Tipus() == Utility.BarcodeRagszamMode.BarcodeRagszamTertivevenyVonalkod ? true : false);

        // ignore header row
        for (var i = 1; i < rowsLength; i++) {
            if (!rows[i].style || rows[i].style.display != 'none') {
                if (!isFirst) {
                    sb.append(';');
                }

                sb.append(rows[i].cells[0].lastChild.innerHTML);
                if (bGetRagszam) {
                    sb.append(',');
                    if (rows[i].cells.length > 2) {

                        sb.append(rows[i].cells[2].innerText);
                    }
                }

                if (bGetTertivevenyVonalkod) {
                    sb.append(',');
                    if (rows[i].cells.length > 3) {

                        sb.append(rows[i].cells[3].innerText);
                    }
                }
                isFirst = false;
            }
        }

        return sb.toString();
    },

    SaveVonalkodState: function(gridView, clientStateField) {
        if (!gridView || !clientStateField)
            return 0;

        clientStateField.value = this.GetBarcodeRagszamListaElementsString(gridView);
        return 1;
    },

    //
    // Behavior properties
    //

    get_BarCodeBehaviorId: function() {
        return this.barCodeBehaviorId;
    },

    set_BarCodeBehaviorId: function(value) {
        if (this.barCodeBehaviorId !== value) {
            this.barCodeBehaviorId = value;
            this.raisePropertyChanged('BarCodeBehaviorId');
        }
    },

    get_RagszamBehaviorId: function() {
        return this.ragszamBehaviorId;
    },

    set_RagszamBehaviorId: function(value) {
        if (this.ragszamBehaviorId !== value) {
            this.ragszamBehaviorId = value;
            this.raisePropertyChanged('RagszamBehaviorId');
        }
    },

    get_TertivevenyVonalkodBehaviorId: function() {
        return this.tertivevenyVonalkodBehaviorId;
    },

    set_TertivevenyVonalkodBehaviorId: function(value) {
        if (this.tertivevenyVonalkodBehaviorId !== value) {
            this.tertivevenyVonalkodBehaviorId = value;
            this.raisePropertyChanged('TertivevenyVonalkodBehaviorId');
        }
    },

    get_RagszamTextBoxClientId: function() {
        return this.ragszamTextBoxClientId;
    },

    set_RagszamTextBoxClientId: function(value) {
        if (this.ragszamTextBoxClientId !== value) {
            this.ragszamTextBoxClientId = value;
            this.raisePropertyChanged('RagszamTextBoxClientId');
        }
    },

    get_ImageAddId: function() {
        return this.imageAddId;
    },

    set_ImageAddId: function(value) {
        if (this.imageAddId !== value) {
            this.imageAddId = value;
            this.raisePropertyChanged('ImageAddId');
        }
    },

    get_CheckBoxClientIds: function() {
        return this.checkBoxClientIds;
    },

    set_CheckBoxClientIds: function(value) {
        if (this.checkBoxClientIds !== value) {
            this.checkBoxClientIds = value;
            this.raisePropertyChanged('CheckBoxClientIds');
        }
    },

    get_ImageDeleteId: function() {
        return this.imageDeleteId;
    },

    set_ImageDeleteId: function(value) {
        if (this.imageDeleteId !== value) {
            this.imageDeleteId = value;
            this.raisePropertyChanged('ImageDeleteId');
        }
    },

    get_CbSoundEffectId: function() {
        return this.cbSoundEffectId;
    },

    set_CbSoundEffectId: function(value) {
        if (this.cbSoundEffectId !== value) {
            this.cbSoundEffectId = value;
            this.raisePropertyChanged('CbSoundEffectId');
        }
    },

    get_HiddenFieldClientStateId: function() {
        return this.hiddenFieldClientStateId;
    },

    set_HiddenFieldClientStateId: function(value) {
        if (this.hiddenFieldClientStateId !== value) {
            this.hiddenFieldClientStateId = value;
            this.raisePropertyChanged('HiddenFieldClientStateId');
        }
    },

    get_MaxElementsNumber: function() {
        return this.maxElementsNumber;
    },

    set_MaxElementsNumber: function(value) {
        if (this.maxElementsNumber !== value) {
            this.maxElementsNumber = value;
            this.raisePropertyChanged('MaxElementsNumber');
        }
    },

    get_ListIsFullMessage: function() {
        return this.listIsFullMessage;
    },

    set_ListIsFullMessage: function(value) {
        if (this.listIsFullMessage !== value) {
            this.listIsFullMessage = value;
            this.raisePropertyChanged('ListIsFullMessage');
        }
    },


    //    setByTipus: function(ischecked) {
    //        var tipus = ischecked ? Utility.BarcodeRagszamMode.BarcodeRagszam : Utility.BarcodeRagszamMode.Barcode;
    //        if (tipus != this.get_Tipus()) {
    //            this.set_Tipus(tipus);
    //            var gv = this.get_element();
    //            var display = (this.tipus == Utility.BarcodeRagszamMode.BarcodeRagszam ? '' : 'none');
    //            if (this.ragszamTextBox) {
    //                this.ragszamTextBox.style.display = display;
    //                this.ragszamTextBox.parentNode.style.display = display;
    //            }

    //            if (gv && gv.rows && gv.rows.length > 0 && gv.rows[0].cells.length > 2) {
    //                for (var r = 0; r < gv.rows.length; r++) {
    //                    if (!gv.rows[r].style || gv.rows[r].style.display != 'none') {
    //                        gv.rows[r].cells[2].style.display = display;
    //                    }
    //                }
    //            }
    //            this.SaveVonalkodState(gv, this.hiddenFieldClientState);
    //        }
    //    },

    get_Tipus: function() {
        return this.tipus;
        //        if (this.barCodeTextBox) {
        //            if (!this.ragszamTextBox) {
        //                return Utility.BarcodeRagszamMode.Barcode;
        //            }
        //            else {
        //                return Utility.BarcodeRagszamMode.BarcodeRagszam;
        //            }
        //        }
        //        else if (this.ragszamTextBox) {
        //            return Utility.BarcodeRagszamMode.Ragszam;
        //        }

        //        return '';
    },

    set_Tipus: function(value) {
        if (this.tipus !== value) {
            this.tipus = value;
            this.raisePropertyChanged('Tipus');
        }
    }
}

Utility.BarcodeRagszamListaBehavior.registerClass('Utility.BarcodeRagszamListaBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
