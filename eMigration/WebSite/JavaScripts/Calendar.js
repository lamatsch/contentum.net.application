﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.Calendar = {};

Utility.Calendar.ValidateDateTimeFormat = function(source, arguments)
{
    var inputValue = arguments.Value;
    if(inputValue != '')
    {
        var inputDate = Date.parseLocale(inputValue);
        if(inputDate == null)
        {
            arguments.IsValid = false;
        }
    }
}

Utility.Calendar.ValidateDateTimeCompare = function(source, arguments) {
    var inputValue = arguments.Value;
    var compareValue = source.valuetocompare;
    var operator = source.operator;
    if (inputValue != '' && compareValue != '' && operator != '') {
        var inputDate = Date.parseLocale(inputValue);
        var compareDate = Date.parseLocale(compareValue);
        if (inputDate != null && compareDate != null) {
            var now = new Date();
            if (source.HourControlToValidate) {
                var hourControl = $get(source.HourControlToValidate);
                if (hourControl) {
                    Utility.Calendar.SetValidatorCalloutPosition(source);
                }
                if (hourControl && hourControl.value != '') {
                    var hour = parseInt(hourControl.value, 10);
                    if (!isNaN(hour)) {
                        inputDate.setHours(hour);
                        compareDate.setHours(now.getHours());
                        compareDate.setMinutes(now.getMinutes());
                    }
                }
            }
            if (source.MinuteControlToValidate) {
                var minuteControl = $get(source.MinuteControlToValidate);
                if (minuteControl) {
                    Utility.Calendar.SetValidatorCalloutPosition(source);
                }
                if (minuteControl && minuteControl.value != '') {
                    var minute = parseInt(minuteControl.value, 10);
                    if (!isNaN(minute)) {
                        inputDate.setMinutes(minute);
                        compareDate.setMinutes(now.getMinutes());
                        compareDate.setHours(now.getHours());
                    }
                }

            }
            arguments.IsValid = Utility.Calendar.CompareValues(inputDate, compareDate, operator);
        }
    }
}

Utility.Calendar.SetValidatorCalloutPosition = function(validator) {
    if (!validator.positioned) {
        var behavior = validator._behaviors[0];
        if (behavior && behavior.get_name() == "ValidatorCalloutBehavior") {
            var popupBehavior = behavior._popupBehavior;
            if (popupBehavior) {
                popupBehavior.add_showing(Utility.Calendar.ValidatorCalloutOnShow);
                validator.positioned = true;
            }
        }
    }
}

Utility.Calendar.ValidatorCalloutOnShow = function(sender, args) {
    sender.set_x(sender.get_x() + 80);
}

Utility.Calendar.CompareValues =function(op1, op2, operator)
{
     switch (operator) 
     {
        case "NotEqual":
        return (op1 != op2);
        case "GreaterThan":
        return (op1 > op2);
        case "GreaterThanEqual":
        return (op1 >= op2);
        case "LessThan":
        return (op1 < op2);
        case "LessThanEqual":
        return (op1 <= op2);
        default:
        return (op1 == op2);
     }
}

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();