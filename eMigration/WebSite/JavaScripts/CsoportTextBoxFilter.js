﻿///<reference name="MicrosoftAjax.js" />

Type.registerNamespace("Utility");

Utility.CsoportTipus = function() {
    throw Error.invalidOperation();
}

Utility.CsoportTipus.prototype = {
    Disabled : -1,
    Empty : 0,
    Szervezet : 1,
    Dolgozo : 2,
    Irattar : 3,
    SajatSzervezetDolgozoi : 4,
    SajatCsoportDolgozoi : 5,
    OsszesDolgozo: 6  
}

Utility.CsoportTipus.registerEnum("Utility.CsoportTipus", false);

Utility.CsoportFilterObject = function() {
    this.FelhasznaloId = '';
    this.SzervezetId = '';
    this.IktatokonyvId = '';
    this.Tipus = '';
    // a LovListtel való kommunikációhoz (mivel a Tipus nem string, ezért JavaScript nem adható át)
    this.SzignalasTipus = '';
}

Utility.CsoportFilterObject.prototype = {
    deserialize: function(value) {
        try {
            var obj = Sys.Serialization.JavaScriptSerializer.deserialize(value);
            if (obj.CsoportFilterObject) {
                this.FelhasznaloId = obj.CsoportFilterObject.FelhasznaloId;
                this.IktatokonyvId = obj.CsoportFilterObject.IktatokonyvId;
                this.Tipus = obj.CsoportFilterObject.Tipus;
                this.SzervezetId = obj.CsoportFilterObject.SzervezetId;
                this.SzignalasTipus = obj.CsoportFilterObject.SzignalasTipus;
            }
        }
        catch (ex) {
        }
    },

    serialize: function() {
        var obj = new Object();
        obj.CsoportFilterObject = this;
        return Sys.Serialization.JavaScriptSerializer.serialize(obj);
    }

}


Utility.CsoportFilterObject.registerClass('Utility.CsoportFilterObject');


Utility.CsoportTextBoxFilterBehavior = function(element) {
    Utility.CsoportTextBoxFilterBehavior.initializeBase(this, [element]);

    //Properties
    this._hiddenFieldClientId = null;
    this._erkeztetoDropDownListClientId = null;
    this._iktatoHiddenfieldClientId = null;
    this._szignalasDropDownListClientId = null;
    this._checkBoxAllDolgozoClientId = null;
    this._szervezetTextBoxClientId = null;
    this._szervezetHiddenFieldClientId = null;
    this._szervezetTextBoxContainerClientId = null;
    // ha definiáltan nincs kiválasztható szervezet:
    // eltűnjön-e a befoglaló objektum (táblázat sora) (true)
    // vagy csak letiltódjanak az elemek (false)
    this._hideSzervezetTextBoxContainerIfDisabled = false;

    //Variables
    this._hiddenField = null;
    this._erkeztetoDropDownList = null;
    this._iktatoHiddenfield = null;
    this._szignalasDropDownList = null;
    this._checkBoxAllDolgozo = null;
    this._szervezetTextBox = null;
    this._szervezetHiddenField = null;
    this._szervezetTextBoxContainer = null;
    this._focusHandler = null;
    this._autoCompleteBehavior = null;
    this._contextKey = null;
    this._applicationLoadHandler = null;
    this._checkBoxAllDolgozoChangeHandler = null;
    this._erkezetoDropDownListChangeHandler = null;
    this._szignalasDropDownListChangeHandler = null;
    this._szervezetTextBoxChangehandler = null;
    this._lastSzervezetId = '';
    this._szervezetAutoCompleteBehavior = null;
    this._szervezetAutoCompleteItemSelectedHandler = null;
    this._filterByErkezteto = false;
    this._filterBySzignalas = false;
    this._prevText = '';
    this._prevValue = '';
    this._cleared = false;
}

Utility.CsoportTextBoxFilterBehavior.prototype = {
    initialize: function() {
        Utility.CsoportTextBoxFilterBehavior.callBaseMethod(this, 'initialize');
        var e = this.get_element();

        this._focusHandler = Function.createDelegate(this, this._OnFocus);
        $addHandler(e, 'focus', this._focusHandler);

        if (this._hiddenFieldClientId) {
            this._hiddenField = $get(this._hiddenFieldClientId);
        }

        if (this._erkeztetoDropDownListClientId) {
            this._erkeztetoDropDownList = $get(this._erkeztetoDropDownListClientId);
            if (this._erkeztetoDropDownList) {
                this._erkezetoDropDownListChangeHandler = Function.createDelegate(this, this._OnErkezetoDropDownListChanged);
                $addHandler(this._erkeztetoDropDownList, 'change', this._erkezetoDropDownListChangeHandler);
            }
        }

        if (this._iktatoHiddenfieldClientId) {
            this._iktatoHiddenfield = $get(this._iktatoHiddenfieldClientId);
        }

        if (this._checkBoxAllDolgozoClientId) {
            this._checkBoxAllDolgozo = $get(this._checkBoxAllDolgozoClientId);
            if (this._checkBoxAllDolgozo) {
                this._checkBoxAllDolgozoChangeHandler = Function.createDelegate(this, this._OnCheckBoxAllDolgozoChange);
                $addHandler(this._checkBoxAllDolgozo, 'click', this._checkBoxAllDolgozoChangeHandler);
            }
        }

        if (this._szervezetTextBoxClientId) {
            this._szervezetTextBox = $get(this._szervezetTextBoxClientId);
            if (this._szervezetTextBox) {
                this._szervezetTextBoxChangehandler = Function.createDelegate(this, this._OnSzervezetTextBoxChanged);
                $addHandler(this._szervezetTextBox, 'change', this._szervezetTextBoxChangehandler);
            }
        }

        if (this._szervezetHiddenFieldClientId) {
            this._szervezetHiddenField = $get(this._szervezetHiddenFieldClientId);
            if (this._szervezetHiddenField) {
                this._lastSzervezetId = this._szervezetHiddenField.value;
            }
        }

        if (this._szervezetTextBoxContainerClientId) {
            this._szervezetTextBoxContainer = $get(this._szervezetTextBoxContainerClientId);
        }

        // a szervezetTextBoxContainer meghatározása után kell állnia, másképp nem tudja eltüntetni a sort
        if (this._szignalasDropDownListClientId) {
            this._szignalasDropDownList = $get(this._szignalasDropDownListClientId);
            if (this._szignalasDropDownList) {
                this._szignalasDropDownListChangeHandler = Function.createDelegate(this, this._OnSzignalasDropDownListChanged);
                $addHandler(this._szignalasDropDownList, 'change', this._szignalasDropDownListChangeHandler);
                $common.tryFireEvent(this._szignalasDropDownList, 'change');
            }
        }

        this._applicationLoadHandler = Function.createDelegate(this, this._OnApplicationLoad);
        Sys.Application.add_load(this._applicationLoadHandler);
    },

    _OnApplicationLoad: function() {
        this._autoCompleteBehavior = this._tryFindBehavior(this.get_element(), AjaxControlToolkit.AutoCompleteBehavior);
        if (this._szervezetTextBox) {
            this._szervezetAutoCompleteBehavior = this._tryFindBehavior(this._szervezetTextBox, AjaxControlToolkit.AutoCompleteBehavior);
            if (this._szervezetAutoCompleteBehavior) {
                this._szervezetAutoCompleteItemSelectedHandler = Function.createDelegate(this, this._OnSzervezetAutoCompleteItemSelected);
                this._szervezetAutoCompleteBehavior.add_itemSelected(this._szervezetAutoCompleteItemSelectedHandler);
            }
        }
    },

    dispose: function() {

        var e = this.get_element();

        if (this._focusHandler) {
            $removeHandler(e, 'focus', this._focusHandler);
            this._focusHandler = null;
        }

        if (this._applicationLoadHandler) {
            Sys.Application.remove_load(this._applicationLoadHandler);
            this._applicationLoadHandler = null;
        }

        if (this._checkBoxAllDolgozoChangeHandler) {
            if (this._checkBoxAllDolgozoChangeHandler)
                $removeHandler(this._checkBoxAllDolgozo, 'click', this._checkBoxAllDolgozoChangeHandler);
            this._checkBoxAllDolgozoChangeHandler = null;
        }

        if (this._erkezetoDropDownListChangeHandler) {
            if (this._erkezetoDropDownListChangeHandler)
                $removeHandler(this._erkeztetoDropDownList, 'change', this._erkezetoDropDownListChangeHandler);
            this._erkezetoDropDownListChangeHandler = null;
        }

        if (this._szignalasDropDownListChangeHandler) {
            if (this._szignalasDropDownListChangeHandler)
                $removeHandler(this._szignalasDropDownList, 'change', this._szignalasDropDownListChangeHandler);
            this._szignalasDropDownListChangeHandler = null;
        }

        if (this._szervezetTextBoxChangehandler) {
            if (this._szervezetTextBox)
                $removeHandler(this._szervezetTextBox, 'change', this._szervezetTextBoxChangehandler);
            this._szervezetTextBoxChangehandler = null;
        }


        if (this._szervezetAutoCompleteItemSelectedHandler) {
            if (this._szervezetAutoCompleteBehavior)
                this._szervezetAutoCompleteBehavior.remove_itemSelected(this._szervezetAutoCompleteItemSelectedHandler);
            this._szervezetAutoCompleteItemSelectedHandler = null;
        }

        this._erkeztetoDropDownList = null;
        this._szignalasDropDownList = null;
        this._autoCompleteBehavior = null;
        this._checkBoxAllDolgozo = null;
        this._szervezetTextBox = null;
        this._szervezetHiddenField = null;
        this._szervezetTextBoxContainer = null;
        this._szervezetAutoCompleteBehavior = null;

        Utility.CsoportTextBoxFilterBehavior.callBaseMethod(this, 'dispose');
    },

    _OnFocus: function(sender, e) {
        this._SetContextKey(this.get_iktatokonyvId() + this.get_tipus() + this.get_szervezetId() + this.get_szignalasTipus());
    },

    _SetContextKey: function(value) {
        if (this._autoCompleteBehavior) {
            if (this._IsContextKeyChanged(value)) {
                this._contextKey = value;
                autoCompleteExtender = this._autoCompleteBehavior;
                this._ClearAutoCompleteCache();
                var context = autoCompleteExtender.get_contextKey();
                var filterObject = new Utility.CsoportFilterObject();
                filterObject.deserialize(context);
                filterObject.IktatokonyvId = this.get_iktatokonyvId();
                if (this.get_tipus() != '')
                    filterObject.Tipus = this.get_tipus();
                if (this._szervezetHiddenField) {
                    filterObject.SzervezetId = this.get_szervezetId();
                    this._lastSzervezetId = this.get_szervezetId();
                }
                filterObject.SzignalasTipus = this.get_szignalasTipus();
                autoCompleteExtender.set_contextKey(filterObject.serialize());
            }
        }
    },

    _IsContextKeyChanged: function(value) {
        if (!this._contextKey) {
            return true;
        }

        if (this._contextKey != value) {
            return true;
        }

        return false;
    },

    _ClearAutoCompleteCache: function() {
        if (this._autoCompleteBehavior) {
            this._autoCompleteBehavior._cache = {};
        }
    },

    _tryFindBehavior: function(element, behaviorType) {
        var behavior = null;
        if (element) {
            if (behaviorType) {
                var behaviors = Sys.UI.Behavior.getBehaviorsByType(element, behaviorType);
                if (behaviors.length > 0) {
                    behavior = behaviors[0];
                }
            }
        }
        return behavior;
    },


    _OnCheckBoxAllDolgozoChange: function(sender, e) {
        if (this.get_tipus() != Utility.CsoportTipus.OsszesDolgozo) {
            this._SaveAutoCompleteState();
            this._cleared = false;
            this._CheckAutoCompleteValue();
        }
        else {
            var element = this.get_element();
            if (element) {
                if (element.value == '' && this._cleared) {
                    this._LoadAutoCompleteState();
                }
            }
        }
    },

    _OnErkezetoDropDownListChanged: function(sender, e) {
        if (this._erkeztetoDropDownList) {
            if (this._erkeztetoDropDownList.selectedIndex > -1) {
                var selectedValue = this._erkeztetoDropDownList.options[this._erkeztetoDropDownList.selectedIndex].value;
                if (selectedValue == '') {
                    return;
                }
            }
            else {
                return;
            }
        }

        this._filterByErkezteto = true;
        this._ClearIfNeed();
        this._filterByErkezteto = false;
    },

    _OnSzignalasDropDownListChanged: function(sender, e) {
        if (this._szignalasDropDownList) {
            if (this._szignalasDropDownList.selectedIndex > -1) {
                var selectedValue = this._szignalasDropDownList.options[this._szignalasDropDownList.selectedIndex].value;
            }
            else {
                return;
            }
        }

        if (this._szervezetTextBoxContainer) {
            if (this.get_tipus() == Utility.CsoportTipus.Disabled) {
                if (this._hideSzervezetTextBoxContainerIfDisabled == true) {
                    this._szervezetTextBoxContainer.style.display = 'none';
                }
                else {
                    var elementArray = this._szervezetTextBoxContainer.getElementsByTagName('*');
                    for (var i = 0; i < elementArray.length; i++) {
                        //var tagName = elementArray.item(i).nodeName;
                        var tagObj = elementArray.item(i);
                        tagObj.disabled = true;
                    }
                }
            }
            else {
                if (this._hideSzervezetTextBoxContainerIfDisabled == true) {
                    this._szervezetTextBoxContainer.style.display = '';
                }
                else {
                    var elementArray = this._szervezetTextBoxContainer.getElementsByTagName('*');
                    for (var i = 0; i < elementArray.length; i++) {
                        //var tagName = elementArray.item(i).nodeName;
                        var tagObj = elementArray.item(i);
                        tagObj.disabled = false;
                    }
                }
            }
        }

        this._filterBySzignalas = true;
        this._ClearIfNeed();
        this._filterBySzignalas = false;
    },

    _OnSzervezetTextBoxChanged: function(sender, args) {
        if (this._szervezetTextBox) {
            var szervezet = this._szervezetTextBox.value;
            //Szervezet üres nem kell törlés
            if (szervezet == '') {
                return;
            }
        }

        if (this._szervezetHiddenField) {
            //üres
            if (this._szervezetHiddenField.value == '') {
                return;
            }
            //Id nem változott nem kell törlés
            if (this._szervezetHiddenField.value == this._lastSzervezetId) {
                return;
            }
            else {
                this._lastSzervezetId = this._szervezetHiddenField.value;
            }
        }

        this._ClearIfNeed();
    },

    _OnSzervezetAutoCompleteItemSelected: function(sender, args) {
        var value = args.get_value();
        if (value == '') {
            return;
        }
        if (this._lastSzervezetId == value) {
            return;
        }
        else {
            this._lastSzervezetId = value;
        }

        this._ClearIfNeed();
    },

    _ClearIfNeed: function() {
        var element = this.get_element();
        if (element) {
            //üres nem kell ellenőrizni
            if (element.value == '') {
                return;
            }
            else {
                this._CheckAutoCompleteValue();
            }
        }

    },

    _CheckAutoCompleteValue: function() {
        if (this._autoCompleteBehavior) {
            var extender = this._autoCompleteBehavior;
            var servicePath = extender.get_servicePath();
            var serviceMethod = extender.get_serviceMethod();
            this._SetContextKey(this.get_iktatokonyvId() + this.get_tipus() + this.get_szervezetId() + this.get_szignalasTipus());
            var contextKey = extender.get_contextKey();
            var prefix = this.get_element().value;
            var params = { prefixText: prefix, count: 1, contextKey: contextKey };

            // Invoke the web service
            Sys.Net.WebServiceProxy.invoke(servicePath, serviceMethod, false, params,
                                    Function.createDelegate(this, this._OnMethodComplete),
                                    Function.createDelegate(this, this._OnMethodFailed), extender);
        }
    },

    _OnMethodComplete: function(result, context) {
        //Van találat nem kell törölni
        if (result && result.length && result.length > 0) {
            return;
        }
        else {
            this._Clear();
        }
    },

    _OnMethodFailed: function(err, response, context) {
    },

    _Clear: function() {
        var element = this.get_element();
        if (element) {
            element.value = '';
        }
        if (this._hiddenField) {
            this._hiddenField.value = '';
        }

        this._cleared = true;
    },

    _SaveAutoCompleteState: function() {
        var element = this.get_element();
        if (element) {
            this._prevText = element.value;
        }
        if (this._hiddenField) {
            this._prevValue = this._hiddenField.value;
        }
    },

    _LoadAutoCompleteState: function() {
        var element = this.get_element();
        if (element) {
            element.value = this._prevText;
        }
        if (this._hiddenField) {
            this._hiddenField.value = this._prevValue;
        }
    },

    //publikus methods
    get_iktatokonyvId: function() {
        if (this._erkeztetoDropDownList && this._erkeztetoDropDownList.selectedIndex > -1) {
            return this._erkeztetoDropDownList.options[this._erkeztetoDropDownList.selectedIndex].value;
        }
        else if (this._iktatoHiddenfield) {
            return this._iktatoHiddenfield.value;
        }
        else {
            return '';
        }
    },

    get_tipus: function() {
        if (this._checkBoxAllDolgozo) {
            if (this._checkBoxAllDolgozo.checked) {
                return Utility.CsoportTipus.OsszesDolgozo;
            }
            else {
                return Utility.CsoportTipus.SajatSzervezetDolgozoi;
            }
        }
        else if (this._szignalasDropDownList) {
            var selectedValue = '';
            if (this._szignalasDropDownList.selectedIndex > -1) {
                selectedValue = this._szignalasDropDownList.options[this._szignalasDropDownList.selectedIndex].value;
            }

            switch (selectedValue) {
                case "1":
                case "2":
                case "3":
                    return Utility.CsoportTipus.Disabled;
                    break;
                case "4":
                    return Utility.CsoportTipus.Szervezet;
                case "5":
                    return Utility.CsoportTipus.Dolgozo;
                case "6":
                default:
                    return Utility.CsoportTipus.Disabled;
            }
        }

        return '';
    },

    get_szignalasTipus: function() {
        var selectedValue = '';
        if (this._szignalasDropDownList) {

            if (this._szignalasDropDownList.selectedIndex > -1) {
                selectedValue = this._szignalasDropDownList.options[this._szignalasDropDownList.selectedIndex].value;
            }
        }
        return selectedValue;
    },

    get_szervezetId: function() {
        if (this._filterByErkezteto) {
            return '';
        }
        if (this._szervezetHiddenField) {
            return this._szervezetHiddenField.value;
        }
        else {
            return '';
        }
    },

    //getter, setter methods
    get_HiddenFieldClientId: function() {
        return this._hiddenFieldClientId;
    },

    set_HiddenFieldClientId: function(value) {
        if (this._hiddenFieldClientId != value) {
            this._hiddenFieldClientId = value;
        }
    },

    get_ErkeztetoDropDownListClientId: function() {
        return this._erkeztetoDropDownListClientId;
    },

    set_ErkeztetoDropDownListClientId: function(value) {
        if (this._erkeztetoDropDownListClientId != value) {
            this._erkeztetoDropDownListClientId = value;
        }
    },

    get_SzignalasDropDownListClientId: function() {
        return this._szignalasDropDownListClientId;
    },

    set_SzignalasDropDownListClientId: function(value) {
        if (this._szignalasDropDownListClientId != value) {
            this._szignalasDropDownListClientId = value;
        }
    },

    get_IktatoHiddenfieldClientId: function() {
        return this._iktatoHiddenfieldClientId;
    },

    set_IktatoHiddenfieldClientId: function(value) {
        if (this._iktatoHiddenfieldClientId != value) {
            this._iktatoHiddenfieldClientId = value;
        }
    },

    get_CheckBoxAllDolgozoClientId: function(value) {
        return this._checkBoxAllDolgozoClientId;
    },

    set_CheckBoxAllDolgozoClientId: function(value) {
        if (this._checkBoxAllDolgozoClientId != value) {
            this._checkBoxAllDolgozoClientId = value;
        }
    },

    get_SzervezetTextBoxClientId: function() {
        return this._szervezetTextBoxClientId;
    },

    set_SzervezetTextBoxClientId: function(value) {
        if (this._szervezetTextBoxClientId != value) {
            this._szervezetTextBoxClientId = value;
        }
    },

    get_SzervezetHiddenFieldClientId: function() {
        return this._szervezetHiddenFieldClientId;
    },

    set_SzervezetHiddenFieldClientId: function(value) {
        if (this._szervezetHiddenFieldClientId != value) {
            this._szervezetHiddenFieldClientId = value;
        }
    },

    get_SzervezetTextBoxContainerClientId: function() {
        return this._szervezetTextBoxContainerClientId;
    },

    set_SzervezetTextBoxContainerClientId: function(value) {
        if (this._szervezetTextBoxContainerClientId != value) {
            this._szervezetTextBoxContainerClientId = value;
        }
    },

    get_HideSzervezetTextBoxContainerIfDisabled: function() {
        return this._hideSzervezetTextBoxContainerIfDisabled;
    },

    set_HideSzervezetTextBoxContainerIfDisabled: function(value) {
        if (this._hideSzervezetTextBoxContainerIfDisabled != value) {
            this._hideSzervezetTextBoxContainerIfDisabled = value;
        }
    }

}

Utility.CsoportTextBoxFilterBehavior.registerClass('Utility.CsoportTextBoxFilterBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();