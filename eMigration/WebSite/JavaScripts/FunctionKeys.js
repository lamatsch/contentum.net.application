﻿/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.FunctionKeysBehavior = function(element) {
    Utility.FunctionKeysBehavior.initializeBase(this, [element]);
    //Properties
    this._hfObjektumIdClientID = null;
    this._hfObjektumTypeClientID = null;
    this._validObjektumTypes = null;
    this._isFeladatFunctionKeyEnabled = true;
    //Variables
    this._hfObjektumId = null;
    this._hfObjektumType = null;
    this._documentKeyDownHandler = null;

}

Utility.FunctionKeysBehavior.prototype = {
    initialize: function() {
        Utility.FunctionKeysBehavior.callBaseMethod(this, 'initialize');

        if (this._hfObjektumIdClientID) {
            this._hfObjektumId = $get(this._hfObjektumIdClientID);
        }

        if (this._hfObjektumTypeClientID) {
            this._hfObjektumType = $get(this._hfObjektumTypeClientID);
        }

        this._documentKeyDownHandler = Function.createDelegate(this, this._OnDocumentKeyDown);
        $addHandler(document, 'keydown', this._documentKeyDownHandler);
    },
    dispose: function() {
        if (this._documentKeyDownHandler) {
            $removeHandler(document, 'keydown', this._documentKeyDownHandler);
            this._documentKeyDownHandler = null;
        }

        this._hfObjektumId = null;
        this._hfObjektumType = null;

        Utility.FunctionKeysBehavior.callBaseMethod(this, 'dispose');
    },

    _OnDocumentKeyDown: function(ev) {
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;

        if (this._isFeladatFunctionKeyEnabled) {
            if (k == 117) //F6
            {
                var handled = this._HandleFeladatKeyDown();
                if (handled) {
                    ev.preventDefault();
                    ev.stopPropagation();
                }
            }
        }
    },

    _HandleFeladatKeyDown: function() {
        var handled = false;
        if (this._IsFeladatParametersValid()) {
            handled = true;
            var objektumId = this._hfObjektumId.value;
            var objektumType = this._hfObjektumType.value;
            var qs = '&ObjektumId=' + objektumId;
            qs += '&ObjektumType=' + objektumType;
            qs += '&Starup=FunctionKey';
            popup('Feladatok.aspx?Command=Modify' + qs, 1024, 768);
        }

        return handled;
    },

    _IsFeladatParametersValid: function() {
        if (this._IsParametersValid()) {
            var objektumId = this._hfObjektumId.value;
            var objektumType = this._hfObjektumType.value;

            if (objektumId == '' || objektumType == '') {
                return false;
            }

            if (this._validObjektumTypes == null || Array.indexOf(this._validObjektumTypes, objektumType) > -1) {
                return true;
            }


        }
        return false;
    },

    _IsParametersValid: function() {
        if (this._hfObjektumId && this._hfObjektumType) {
            return true;
        }
        return false;
    },


    //getter, setter methods
    get_HfObjektumIdClientID: function() {
        return this._hfObjektumIdClientID;
    },

    set_HfObjektumIdClientID: function(value) {
        if (this._hfObjektumIdClientID != value) {
            this._hfObjektumIdClientID = value;
        }
    },
    get_HfObjektumTypeClientID: function() {
        return this._hfObjektumTypeClientID;
    },

    set_HfObjektumTypeClientID: function(value) {
        if (this._hfObjektumTypeClientID != value) {
            this._hfObjektumTypeClientID = value;
        }
    },

    get_ValidObjektumTypesArray: function() {
        return this._validObjektumTypes;
    },

    set_ValidObjektumTypesArray: function(value) {
        if (this._validObjektumTypes != value) {
            this._validObjektumTypes = value;
        }
    },

    get_IsFeladatFunctionKeyEnabled: function() {
        return this._isFeladatFunctionKeyEnabled;
    },

    set_IsFeladatFunctionKeyEnabled: function(value) {
        if (this._isFeladatFunctionKeyEnabled != value) {
            this._isFeladatFunctionKeyEnabled = value;
        }
    }
}


Utility.FunctionKeysBehavior.registerClass('Utility.FunctionKeysBehavior', Sys.UI.Behavior); 


if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();