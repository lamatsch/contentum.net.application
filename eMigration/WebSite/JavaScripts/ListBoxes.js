// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.ListBoxBehavior = function(element)
{
    Utility.ListBoxBehavior.initializeBase(this, [element]);
    
    //Properties
    this.barCodeBehaviorId = null;
    this.imageAddId = null;
    this.imageDeleteId = null;
    this.hiddenFieldClientStateId = null;
    this.maxElementsNumber = -1;
    this.listIsFullMessage = 'A lista tele!';
    this.cbSoundEffectId = null;
    //Variables
    this.barCodeBehavior = null;
    this.barCodeTextBox = null;
    this.imageAdd = null;
    this.imageDelete = null;
    this.hiddenFieldClientState = null;
    this._applicationLoadHandler = null;
    this._barcodeBehaviorKeyUpHandler = null;
    this._imageAddClickHandler = null;
    this._imageDeleteClickHandler = null;
    this._listBoxKeyDownHandler = null;
    this._cbSoundEffect = null;
    this._cbSoundEffectClickHandler = null;
}

Utility.ListBoxBehavior.prototype = 
{
    
    initialize : function() {
        Utility.ListBoxBehavior.callBaseMethod(this, 'initialize');
        
        this._applicationLoadHandler = Function.createDelegate(this, this._OnApplicationLoad);
        Sys.Application.add_load(this._applicationLoadHandler);
        
        this._imageAddClickHandler = Function.createDelegate(this, this._OnImageAddClick);
        this._imageDeleteClickHandler = Function.createDelegate(this, this._OnImageDeleteClick);
        this._barcodeBehaviorKeyUpHandler = Function.createDelegate(this,this._OnBarcodeBehaviorKeyUp);
        this._listBoxKeyDownHandler = Function.createDelegate(this, this._OnListBoxKeyDown);
        this._cbSoundEffectClickHandler = Function.createDelegate(this, this._OnCbSoundEffectClick);
        
        if(this.imageAddId)
        {
           this.imageAdd = $get(this.imageAddId); 
        }
        
        if(this.imageDeleteId)
        {
           this.imageDelete = $get(this.imageDeleteId); 
        }
        
        if(this.hiddenFieldClientStateId)
        {
           this.hiddenFieldClientState = $get(this.hiddenFieldClientStateId); 
        }
        
        if(this.cbSoundEffectId)
        {
           this._cbSoundEffect = $get(this.cbSoundEffectId); 
        }
        
        if(this.imageAdd)
        {
            $addHandler(this.imageAdd, 'click', this._imageAddClickHandler);
        }
        
        if(this.imageDelete)
        {
            $addHandler(this.imageDelete, 'click', this._imageDeleteClickHandler);
        }
        
        if(this._cbSoundEffect)
        {
            $addHandler(this._cbSoundEffect, 'click', this._cbSoundEffectClickHandler);
        }  
        
        element = this.get_element();
        
        if(element)
        {
            $addHandler(element, 'keydown', this._listBoxKeyDownHandler); 
        }
        
        
        this._InitListBoxClientState();
        
    },
    
    _OnApplicationLoad: function()
    {
        if(this.barCodeBehaviorId)
        {
            this.barCodeBehavior = $find(this.barCodeBehaviorId);
        }
        if(this.barCodeBehavior)
        {
            this.barCodeTextBox = this.barCodeBehavior.get_element();
            this.barCodeBehavior.add_keyUp(this._barcodeBehaviorKeyUpHandler);
            this.barCodeBehavior.set_CheckVonalkod(false);
        }
    },
    
    dispose : function() {
        
        if(this._applicationLoadHandler != null)
        {
            Sys.Application.remove_load(this._applicationLoadHandler);
            this._applicationLoadHandler = null;
        }
        
        if(this._barcodeBehaviorKeyUpHandler && this.barCodeBehavior)
        {
            this.barCodeBehavior.remove_keyUp(this._barcodeBehaviorKeyUpHandler);
        }
        
        this.barCodeBehavior = null;
        this.barCodeTextBox = null;
        this._barcodeBehaviorKeyUpHandler = null;
        
        if(this.imageAdd && this._imageAddClickHandler)
        {
            $removeHandler(this.imageAdd, 'click', this._imageAddClickHandler);
        }
        
        this.imageAdd = null;
        this._imageAddClickHandler = null;
        
        if(this.imageDelete && this._imageDeleteClickHandler)
        {
            $removeHandler(this.imageDelete, 'click', this._imageDeleteClickHandler);
        }
        
        this.imageDelete = null;
        this._imageDeleteClickHandler = null;
        
        if(this._cbSoundEffect && this._cbSoundEffectClickHandler)
        {
            $removeHandler(this._cbSoundEffect, 'click', this._cbSoundEffectClickHandler);
        }
        
        this._cbSoundEffect = null;
        this._cbSoundEffectClickHandler = null;        
        
        if(this._listBoxKeyDownHandler)
        {
            $removeHandler(element, 'keydown', this._listBoxKeyDownHandler); 
        }
        
        this._listBoxKeyDownHandler = null;
        
        this.hiddenFieldClientState = null;
        
        Utility.ListBoxBehavior.callBaseMethod(this, 'dispose');
    },
    
    
    _InitListBoxClientState: function()
    {
        if(this.hiddenFieldClientState && this.hiddenFieldClientState.value.trim() != '')
        {
            var element = this.get_element();
            if(element && element.options && element.options.length == 0)
            {
                var clientState = this.hiddenFieldClientState.value.trim();
                var barcodeArray = clientState.split(',');
                for(var i = 0; i< barcodeArray.length; i++)
                {
                    var barcode = barcodeArray[i];
                    barcode = barcode.trim();
                    if(barcode != '')
                    {
                        var opt = new Option(barcode, barcode);
                        Utility.DomElement.Select.Add(element, opt);
                    }
                }
            }
        }
    },
     
    _OnBarcodeBehaviorKeyUp: function(sender, ev)
    {
       if(this.barCodeBehavior && this.barCodeTextBox)
       {
            var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
            if(k == Sys.UI.Key.enter || this.barCodeBehavior.barcodeLength == this.barCodeTextBox.value.length)
            {
                var element = this.get_element();
                this.AddElementToListBox(element,this.barCodeTextBox.value);
                this.barCodeTextBox.value = '';
            }
       }
    },
    
    _OnListBoxKeyDown: function(ev)
    {
          var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
          if(k == Sys.UI.Key.del)
          {
              var element = this.get_element();
              this.RemoveSelectedElementFromListBox(element);
          }
    },
    
    _OnImageAddClick: function(ev)
    {
        if(this.barCodeTextBox && this.barCodeTextBox.value.trim() != '')
        {
            var element = this.get_element();
            this.AddElementToListBox(element,this.barCodeTextBox.value);
            this.barCodeTextBox.value = '';
        }
    },
    
    _OnImageDeleteClick: function(ev)
    {
        var element = this.get_element();
        this.RemoveSelectedElementFromListBox(element);
    },
    
    _OnCbSoundEffectClick: function(ev)
    {
        if(this.barCodeBehavior && this._cbSoundEffect)
        {
            this.barCodeBehavior.set_PlaySoundOnAccept(this._cbSoundEffect.checked);
        }
    },
    
    AddElementToListBox: function(listBox, textElement)
    {
        if(this.barCodeBehavior)
        {
            var check = this.barCodeBehavior.AlertVonalkodCheckError();
            if(check != 1)
            {
                return 0;
            }
        }
        
        if(!listBox || !listBox.options || !textElement)
           return 0;
        
        var options = listBox.options;
        var optionsLength = options.length;     
        var isElementInList = 0;
        
        for(var i=0; i< optionsLength; i++)
        {
          if(options[i].value == textElement) 
          { 
             isElementInList = 1; 
          }
        }
        
        if(isElementInList == 0) 
        {
            if(this.maxElementsNumber > -1 && this.maxElementsNumber < optionsLength)
            {
                alert(this.listIsFullMessage);
                return 0;
            }
            
            var opt = new Option(textElement,textElement);
            
            var IsAdded = Utility.DomElement.Select.Add(listBox, opt, true);
            
            if(IsAdded)
            {
                this.SaveVonalkodState(listBox,this.hiddenFieldClientState);
            }
            
        }
        else {};
        
        return 0;
    },
    
    RemoveSelectedElementFromListBox: function(listBox)
    {
        if(!listBox || !listBox.options)
           return 0;
           
        var options = listBox.options;
        var optionsLength = options.length;  
           
        if(optionsLength > 0)
        {
          var idx = listBox.selectedIndex;
          if(idx > -1)
          {
             listBox.remove(idx);
             optionsLength--;
             //t�bbes kijel�l�s eset�n
             if(listBox.multiple)
             {
                    var i = idx;
                    while(i < optionsLength)
                    {
                       if(!options[i]) break;
                       
                       if(options[i].selected)
                       {
                          listBox.remove(i);
                          optionsLength--;
                       }
                       else
                       {
                           i++;
                       }
                    }
             }
             this.SaveVonalkodState(listBox,this.hiddenFieldClientState);
             return 1;
          } 
        }
        
        return 0;
    },
    
    GetListBoxElementsString: function(listBox)
    {
        if(!listBox || !listBox.options)
           return '';
        
        var options = listBox.options;
        var optionsLength = options.length;   
        var sb = new Sys.StringBuilder();
        
	    for (var i=0; i<optionsLength; i++)
	    {
		    if(i > 0)
		    {
		        sb.append(',');
		    }
		    
		    sb.append(options[i].value);
	    }
	    
	    return sb.toString();
    },

    SaveVonalkodState: function(listBox, clientStateField)
    {
        if(!listBox || !clientStateField)
           return 0;
           
        clientStateField.value = this.GetListBoxElementsString(listBox);
        return 1;
    },

    //
    // Behavior properties
    //
    
    get_BarCodeBehaviorId : function() {
        return this.barCodeBehaviorId;
    },

    set_BarCodeBehaviorId : function(value) {
        if (this.barCodeBehaviorId !== value) {
            this.barCodeBehaviorId = value;
            this.raisePropertyChanged('BarCodeBehaviorId');
        }
    },
    
    get_ImageAddId : function() {
        return this.imageAddId;
    },

    set_ImageAddId : function(value) {
        if (this.imageAddId !== value) {
            this.imageAddId = value;
            this.raisePropertyChanged('ImageAddId');
        }
    },
    
    get_ImageDeleteId : function() {
        return this.imageDeleteId;
    },

    set_ImageDeleteId : function(value) {
        if (this.imageDeleteId !== value) {
            this.imageDeleteId = value;
            this.raisePropertyChanged('ImageDeleteId');
        }
    },

    get_CbSoundEffectId : function() {
        return this.cbSoundEffectId;
    },

    set_CbSoundEffectId : function(value) {
        if (this.cbSoundEffectId !== value) {
            this.cbSoundEffectId = value;
            this.raisePropertyChanged('CbSoundEffectId');
        }
    },    
    
    get_HiddenFieldClientStateId : function() {
        return this.hiddenFieldClientStateId;
    },

    set_HiddenFieldClientStateId : function(value) {
        if (this.hiddenFieldClientStateId !== value) {
            this.hiddenFieldClientStateId = value;
            this.raisePropertyChanged('HiddenFieldClientStateId');
        }
    },
    
    get_MaxElementsNumber : function() {
        return this.maxElementsNumber;
    },

    set_MaxElementsNumber : function(value) {
        if (this.maxElementsNumber !== value) {
            this.maxElementsNumber = value;
            this.raisePropertyChanged('MaxElementsNumber');
        }
    },
    
    get_ListIsFullMessage : function() {
        return this.listIsFullMessage;
    },

    set_ListIsFullMessage : function(value) {
        if (this.listIsFullMessage !== value) {
            this.listIsFullMessage = value;
            this.raisePropertyChanged('ListIsFullMessage');
        }
    }
}

Utility.ListBoxBehavior.registerClass('Utility.ListBoxBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
