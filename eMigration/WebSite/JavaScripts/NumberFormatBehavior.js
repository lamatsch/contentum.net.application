﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.NumberFormatBehavior = function (element) {
    Utility.NumberFormatBehavior.initializeBase(this, [element]);

    //Properties
    this.numberMaxLength = null;
    //Variables
    this.keyUpHandler = null;
    this.pasteHandler = null;
    this.applicationLoadHandler = null;
    this.enabled = true;
}

Utility.NumberFormatBehavior.prototype =
{
    initialize: function () {
        Utility.NumberFormatBehavior.callBaseMethod(this, 'initialize');

        var element = this.get_element();

        if (element) {
            this.enabled = !element.readOnly && !element.disabled;
        }

        if (this.enabled) {
            this.keyUpHandler = Function.createDelegate(this, this._OnKeyUp);
            this.pasteHandler = Function.createDelegate(this, this._OnPaste);

            if (element) {
                $addHandler(element, "keyup", this.keyUpHandler);
                $addHandler(element, "paste", this.pasteHandler);
            }
        }

        this.applicationLoadHandler = Function.createDelegate(this, this._OnApplicationLoad);
        Sys.Application.add_load(this.applicationLoadHandler);

        this._FormatInput(false)
    },

    dispose: function () {

        var element = this.get_element();

        if (element) {
            if (this.keyUpHandler) {
                $removeHandler(element, "keyup", this.keyUpHandler);
                this.keyUpHandler = null;
            }

            if (this.pasteHandler) {
                $removeHandler(element, "paste", this.pasteHandler);
                this.pasteHandler = null;
            }
        }

        if (this.applicationLoadHandler) {
            Sys.Application.remove_load(this.applicationLoadHandler);
            this.applicationLoadHandler = null;
        }

        Utility.NumberFormatBehavior.callBaseMethod(this, 'dispose');
    },

    _OnApplicationLoad: function () {
    },

    _OnKeyUp: function (ev) {
        this._FormatInput(true);
    },

    _OnPaste: function (ev) {
        var $this = this;
        window.setTimeout(function () { $this._FormatInput(true) }, 1);
    },

    _FormatInput: function (cut) {
        var element = this.get_element();
        var text = element.value;
        var originalText = text;

        var originalLength = text.length;
        var cursorPos = this._GetCaretPosition(element);

        if (cut) {
            text = this._GetMaxLengthText(text);
        }

        var number = Number.parseLocale(text);

        if (!isNaN(number)) {
            text = number.localeFormat("N0");

            if (text != originalText) {
                element.value = text;

                if (cut) {
                    var newLength = text.length
                    var newCursorPos = Math.max(0, cursorPos + newLength - originalLength);

                    Utility.Selection.textboxSelect(element, newCursorPos, newCursorPos);
                }
            }
        }
    },

    _GetMaxLengthText: function (text) {
        if (text) {
            var number = Number.parseLocale(text);

            if (!isNaN(number)) {
                if (number.toString().length > this.numberMaxLength) {
                    var output = number.toString().substring(0, this.numberMaxLength);
                    return output;
                }
            }
        }

        return text;
    },

    _GetCaretPosition: function (oField) {

        // Initialize
        var iCaretPos = 0;

        // IE Support
        if (document.selection) {
            // To get cursor position, get empty selection range
            var oSel = document.selection.createRange();

            // Move selection start to 0 position
            oSel.moveStart('character', -oField.value.length);

            // The caret position is selection length
            iCaretPos = oSel.text.length;
        }

        // Firefox support
        else if (oField.selectionStart || oField.selectionStart == '0')
            iCaretPos = oField.selectionStart;

        // Return results
        return (iCaretPos);
    },

    //
    // Behavior properties
    //
    get_NumberMaxLength: function () {
        return this.numberMaxLength;
    },

    set_NumberMaxLength: function (value) {
        if (this.numberMaxLength !== value) {
            this.numberMaxLength = value;
            this.raisePropertyChanged('NumberMaxLength');
        }
    }
}

Utility.NumberFormatBehavior.registerClass('Utility.NumberFormatBehavior', Sys.UI.Behavior);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();