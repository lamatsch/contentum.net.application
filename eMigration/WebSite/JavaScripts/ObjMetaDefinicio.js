///<reference name="MicrosoftAjax.js" />

Type.registerNamespace("Utility");

Utility.ObjMetaDefinicioBehavior = function(element) {
    Utility.ObjMetaDefinicioBehavior.initializeBase(this, [element]);
    
    //Properties
    this._autoCompleteExtenderId = null;
    this._hiddenFieldId = null;
    this._definiciotipusHiddenFieldId = null;
    this._spsszinkronizaltHiddenFieldId = null;
    this._customValueEnabled = true;
    //Variables
    this._autoCompleteExtender = null;
    this._hiddenField = null;
    this._definiciotipusHiddenField = null;
    this._spsszinkronizaltHiddenField = null;
    this._itemSelectedHandler = null;
    this._textChangedHandler = null;
}

Utility.ObjMetaDefinicioBehavior.prototype = {
    initialize : function() {
        Utility.ObjMetaDefinicioBehavior.callBaseMethod(this, 'initialize');
       
        if(this._hiddenFieldId)
        {
            this._hiddenField = $get(this._hiddenFieldId);
        }
        
        if(this._definiciotipusHiddenFieldId)
        {
            this._definiciotipusHiddenField = $get(this._definiciotipusHiddenFieldId);
        }
        
        if(this._spsszinkronizaltHiddenFieldId)
        {
            this._spsszinkronizaltHiddenField = $get(this._spsszinkronizaltHiddenFieldId);
        }   
        
        this._itemSelectedHandler = Function.createDelegate(this,this._OnItemSelected);
        
        if(!this._customValueEnabled)
        {
            this._textChangedHandler = Function.createDelegate(this,this._OnTextChanged);
        }    
        
        this._applicationLoadHandler = Function.createDelegate(this,this._OnApplicationLoad);
        Sys.Application.add_load(this._applicationLoadHandler);
    },
    
    _OnApplicationLoad : function()
    {
        if(this._autoCompleteExtenderId)
        {
            this._autoCompleteExtender = $find(this._autoCompleteExtenderId);
        }
        
        if(this._autoCompleteExtender)
        {
            this._autoCompleteExtender.add_itemSelected(this._itemSelectedHandler);
            if(!this._customValueEnabled)
            {
               $addHandler(this._autoCompleteExtender.get_element(),'blur',this._textChangedHandler);
            }   
        }
    },
    
    dispose : function() {
     
        if(this._applicationLoadHandler)
        {
            Sys.Application.remove_load(this._applicationLoadHandler);
            this._applicationLoadHandler = null;
        }
        
        if(this._itemSelectedHandler)
        {
            if(this._autoCompleteExtender)
            {
                this._autoCompleteExtender.remove_itemSelected(this._itemSelectedHandler);
            }
            
            this._itemSelectedHandler = null;
        }
        
        if(!this._customValueEnabled)
        {
            if(this._textChangedHandler)
            {
                if(this._autoCompleteExtender && this._autoCompleteExtender.get_element())
                {
                    $removeHandler(this._autoCompleteExtender.get_element(),'blur',this._textChangedHandler);
                }
                
                this._textChangedHandler = null;    
            }
        }
        
        this._autoCompleteExtender = null;
        this._hiddenField = null;
        this._definiciotipusHiddenField = null;
        this._spsszinkronizaltHiddenField = null;

        Utility.ObjMetaDefinicioBehavior.callBaseMethod(this, 'dispose');
    },
    
    _OnItemSelected: function(sender, args)
    {         
        if(this._hiddenField != null)
        {
            var value = args.get_value();
            
            if(value != null)
            {
                 var values = value.split(';');
                 
                 if(values.length > 0)
                    this._hiddenField.value = values[0].trim();
                    
                 if(values.length > 1 && this._definiciotipusHiddenField)
                    this._definiciotipusHiddenField.value = values[1].trim();
                    
                 if(values.length > 2 && this._spsszinkronizaltHiddenField)
                    this._spsszinkronizaltHiddenField.value = values[2].trim();

            }
        }
        
    },
    
    _OnTextChanged: function(args)
    {        
        if(this._hiddenField != null)
        {
            if(this._hiddenField.value == '')
            {     
                 if(this._autoCompleteExtender != null)
                 {
                     this._autoCompleteExtender.get_element().value = '';
                 }   
            }
       }
    },

    //getter, setter methods
    get_AutoCompleteExtenderId : function()
    {
        return this._autoCompleteExtenderId;
    },
    
    set_AutoCompleteExtenderId : function(value)
    {
       if(this._autoCompleteExtenderId != value)
       {
          this._autoCompleteExtenderId = value;
       }
    },

    get_HiddenFieldId : function()
    {
        return this._hiddenFieldId;
    },
    
    set_HiddenFieldId : function(value)
    {
       if(this._hiddenFieldId != value)
       {
          this._hiddenFieldId = value;
       }
    },
    
    get_DefinicioTipusHiddenFieldId : function()
    {
        return this._definiciotipusHiddenFieldId;
    },
    
    set_DefinicioTipusHiddenFieldId : function(value)
    {
       if(this._definiciotipusHiddenFieldId != value)
       {
          this._definiciotipusHiddenFieldId= value;
       }
    },
    
    get_SPSSzinkronizaltHiddenFieldId : function()
    {
        return this._spsszinkronizaltHiddenFieldId;
    },
    
    set_SPSSzinkronizaltHiddenFieldId : function(value)
    {
       if(this._spsszinkronizaltHiddenFieldId != value)
       {
          this._spsszinkronizaltHiddenFieldId= value;
       }
    },
    
    get_CustomValueEnabled : function()
    {
        return this._customValueEnabled;
    },
    
    set_CustomValueEnabled : function(value)
    {
       if(this._customValueEnabled != value)
       {
          this._customValueEnabled = value;
       }
    }     
}

Utility.ObjMetaDefinicioBehavior.registerClass('Utility.ObjMetaDefinicioBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();