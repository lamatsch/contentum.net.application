﻿/* -------------------------------------------------------------------------------------------------
Nyomtatobarat verzio generalas
-------------------------------
nyomtatando szekciok: printable = "true" - kivéve ezek azon subnode-jai, ahol a printable = "false" vagy tagName = "SCRIPT"
------------------------------------------------------------------------------------------------- */

function createPrinterFriendlyVersion(userName, stylesheets, printButtonImgPath, closeButtonImgPath, cssClassImg)
{
  
    var WindowObject = window.open('', 'PrinterFriendlyVersion','width=800,height=600,top=100,left=150,location=0,locationbar=0,directories=no,menubar=0,toolbar=0,scrollbars=1,status=0,resizable=1');
    
    var content = '';

    var domtree = document.body.cloneNode(false);
    domtree = getPrintableNodes(document.body, domtree);

    var controlpanel = getControlPanelNode(WindowObject.document, printButtonImgPath, closeButtonImgPath, cssClassImg);
    var footer = getFooterNode(WindowObject.document, userName);

    content = controlpanel.innerHTML + domtree.innerHTML + footer.innerHTML;
        
   
    if (content)
    {
        WindowObject.document.writeln('<html><head>');
        WindowObject.document.writeln(getStyleSheets(stylesheets));
        WindowObject.document.writeln('<style id="styles" type="text/css"> @media print { .printerfriendly_noprint { display:none; } } @media screen { .printerfriendly_noprint { display:block; } }</style>');
        WindowObject.document.writeln('</head><body>');
        WindowObject.document.writeln(content);
        WindowObject.document.writeln('</body></html>');
        WindowObject.document.close();
        WindowObject.focus();
        
    }
}

function getPrintableNodes(parentNode, contentNode)
{
    for (var n = 0; n < parentNode.childNodes.length; n++)
    {
        var child = parentNode.childNodes[n];
        if (child.printable == "true")
        {
            var newNode = child.cloneNode(true);
            newNode = removeNonPrintableChildren(newNode, newNode);
            contentNode.appendChild(newNode);
        }
        else if ((child.tagName != "SCRIPT" && (!child.style || (child.style.display != 'none' && child.style.visibility != 'hidden'))) && child.hasChildNodes())
        {
            contentNode = getPrintableNodes(child, contentNode);
        }
    }
    return contentNode;
}

function removeNonPrintableChildren(parentNode, currentChildNode)
{
    for (var n = currentChildNode.childNodes.length - 1; n >= 0; n--)
    {
        var child = currentChildNode.childNodes[n];
        if (child.printable == "false" || child.tagname == "SCRIPT")
        {
            currentChildNode.removeChild(child);
        }
        else if (child.hasChildNodes())
        {
            removeNonPrintableChildren(parentNode, child);
        }
    }
    return parentNode;
}

function getStyleSheets(stylesheets)
{
    var stylesheetsArray = stylesheets.split(";");
    var stylesheetsLinks = "";
    for(var i=0; i < stylesheetsArray.length;i++){
        stylesheetsLinks = stylesheetsLinks + '<link href="' + stylesheetsArray[i] + '" type="text/css" rel="stylesheet"/>' ;
    }
    return stylesheetsLinks;
}

function getControlPanelNode(document, printButtonImgPath, closeButtonImgPath, cssClassImg)
{
    var controlPanelContainerNode = document.createElement('div');
    
    var controlPanelNode = document.createElement('div');
    controlPanelNode.style.textAlign = "right";
    controlPanelNode.style.paddingBottom = "20px";
    controlPanelNode.className= "printerfriendly_noprint";
    var printButton = document.createElement('input');
    printButton.type = "image";
    printButton.src = printButtonImgPath;
    printButton.className = cssClassImg;
    printButton.value = "Nyomtat";
    printButton.onClick="javascript:window.print();window.close();";
    var closeButton = document.createElement('input');
    closeButton.type = 'image';
    closeButton.src = closeButtonImgPath;
    closeButton.className = cssClassImg;
    closeButton.value = 'Bezár';
    closeButton.onClick="javascript:window.close();";
    
    controlPanelNode.appendChild(printButton);
    controlPanelNode.appendChild(closeButton);
    
    controlPanelContainerNode.appendChild(controlPanelNode);
    return controlPanelContainerNode;
}

function getFooterNode(document, userName)
{
    var footerContainerNode = document.createElement('div');
    
    var footerNode = document.createElement('div');
    footerNode.style.textAlign = "left";
    footerNode.style.display = "block";
    footerNode.style.paddingTop = "20px";
    var userNameLabel = document.createElement('b');
    userNameLabel.innerText = "Készítette: ";
    var userName = document.createTextNode(userName);
    var spacer = document.createElement('br');   
    var dateLabel = document.createElement('b');
    dateLabel.innerText = "Dátum: ";
    var date = document.createTextNode(new Date().toLocaleString());
      
    footerNode.appendChild(userNameLabel);
    footerNode.appendChild(userName);
    footerNode.appendChild(spacer);
    footerNode.appendChild(dateLabel);
    footerNode.appendChild(date);    
    
    footerContainerNode.appendChild(footerNode);
    return footerContainerNode;
}




