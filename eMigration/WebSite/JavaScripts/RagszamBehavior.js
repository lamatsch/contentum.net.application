﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.RagszamError = {};
Utility.RagszamError.badRagszamLengthKulfold = "A nemzetközi küldeményazonosító (ragszám) hossza nem megfelelő! Várt hossz: {0} - Tényleges hossz: {1}";
Utility.RagszamError.badRagszamFormatKulfold = "A nemzetközi küldeményazonosító formátuma hibás!";
Utility.RagszamError.checkSumErrorKulfold = "A nemzetközi küldeményazonosító ellenőrző összege hibás! Ellenőrző jegy: {0} - Számított: {1}";
Utility.RagszamError.badRagszamLengthBelfold = "A belföldi küldeményazonosító (ragszám) hossza nem megfelelő! Várt hossz: {0} - Tényleges hossz: {1}";
Utility.RagszamError.badRagszamFormatBelfold = "A belföldi küldeményazonosító formátuma hibás!";
Utility.RagszamError.checkSumErrorBelfold = "A belföldi küldeményazonosító ellenőrző összege hibás! Ellenőrző jegy: {0} - Számított: {1}"


Utility.RagszamBehavior = function(element) {
    Utility.RagszamBehavior.initializeBase(this, [element]);

    //Properties
    this.checkVektor = null;
    this.checksumValidatorId = null;
    this.checksumValidatorCalloutId = null;
    this.bgSoundId = null;
    this.checkRagszam = true;
    this.playSoundOnError = false;
    this.playSoundOnAccept = false;
    this.soundOnError = '';
    this.soundOnAccept = '';
    this.isKulfold = false;
    this.automaticFormatDetection = true; // külföldi-e, formátum alapján felismerés engedélyezés
    //Variables
    this.ragszamLengthBelfold = 16;
    this.ragszamLengthKulfold = 13;
    this.ragszamPrefixListBelfold = ["RL", "EL", "KR", "RB"];
    this.ragszamPrefixListKulfold = ["RR", "VV"];
    this.ragszamPatternBelfold = "^((" + this.ragszamPrefixListBelfold.join('|') + ")[0-9]{14})$";
    this.ragszamPatternKulfold = "^((" + this.ragszamPrefixListKulfold.join('|') + ")[0-9]{9}HU)$";
    this._checksumValidator = null;
    this._checksumValidatorCallout = null;
    this._bgSound = null;
    this._keyUpHandler = null;
    this._keyDownHandler = null;
    this._textChangedHandler = null;
    this._checksumValidationHandler = null;
}

Utility.RagszamBehavior.prototype =
{

    initialize: function() {
        Utility.RagszamBehavior.callBaseMethod(this, 'initialize');

        this._keyUpHandler = Function.createDelegate(this, this._OnKeyUp);
        this._keyDownHandler = Function.createDelegate(this, this._OnKeyDown);

        var element = this.get_element();
        if (element) {
            $addHandler(element, "keyup", this._keyUpHandler);
            $addHandler(element, "keydown", this._keyDownHandler);
        }

        if (this.checkRagszam) {

            //            this._textChangedHandler = Function.createDelegate(this, this._OnTextChanged);
            // 
            //            if(element)
            //            {
            //                $addHandler(element, "change", this._textChangedHandler);
            //            }

        }

        if (this.checksumValidatorId) {
            this._checksumValidator = $get(this.checksumValidatorId);

            if (this._checksumValidator) {
                this._checksumValidationHandler = Function.createDelegate(this, this._ChecksumValidate);
                this._checksumValidator.evaluationfunction = this._checksumValidationHandler;
            }
        }

        if (this.bgSoundId) {
            this._bgSound = $get(this.bgSoundId);
        }
        //        
        //        if(this.checkVektor)
        //        {
        //            this.ragszamLength = this.checkVektor.length + 1;
        //        }


    },

    dispose: function() {

        var element = this.get_element();

        if (element) {
            if (this._keyUpHandler) {
                $removeHandler(element, "keyup", this._keyUpHandler);
            }
            this._keyUpHandler = null;
            if (this._textChangedHandler) {
                $removeHandler(element, "change", this._textChangedHandler);
            }
            this._textChangedHandler = null;
            if (this._keyDownHandler) {
                $removeHandler(element, "keydown", this._keyDownHandler);
            }
            this._keyDownHandler = null;

            if (this._checksumValidator) {
                this._checksumValidator.evaluationfunction = null;
                this._checksumValidator.errormessage = null;
                this._checksumValidator = null;
            }

            this._checksumValidationHandler = null;

            this._checksumValidatorCallout = null;
        }

        Utility.RagszamBehavior.callBaseMethod(this, 'dispose');
    },


    _OnKeyUp: function(ev) {
        var element = this.get_element();
        var text = element.value;
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
        if (k && k > 88 && k < 193) {
            var index = text.length - 1;
            switch (k) {
                case 192: //Ö
                    element.value = text.substring(0, index) + '0';
                    break;
                case 89: //Y
                    element.value = text.substring(0, index);
                    if (ev.shiftKey) element.value += 'Z';
                    else element.value += 'z';
                    break;
                case 90: //Z
                    element.value = text.substring(0, index);
                    if (ev.shiftKey) element.value += 'Y';
                    else element.value += 'y';
                    break;
                case 191: //Ü
                    element.value = text.substring(0, index) + '-';
                    break;
            }
        }

        this.raiseKeyUp(ev);
    },

    _OnKeyDown: function(ev) {
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
        if (k == Sys.UI.Key.enter) {
            return false;
        }
    },

    _OnTextChanged: function(ev) {
        if (this.checkRagszam) {
            this.AlertRagszamCheckError();
        }
    },

    AlertRagszamCheckError: function() {
        var element = this.get_element();
        var check = this.CheckRagszam(element.value);
        if (check != 1) {
            alert(check);
            //element.value = '';
            element.focus();
            element.select();
            if (this.playSoundOnError == true) {
                this.PlaySound(this.soundOnError);
            }
            return 0;
        }

        if (this.playSoundOnAccept == true) {
            this.PlaySound(this.soundOnAccept);
        }

        return 1;
    },

    _ChecksumValidate: function(sender, args) {
        var element = this.get_element()
        var rag_szam = element.value;
        var check = this.CheckRagszam(rag_szam);

        if (check == 1) {
            return true;
        }
        else {
            this._SetValidatorErrorMessage(this._checksumValidator, check);
            return false;
        }
    },

    _SetValidatorErrorMessage: function(validator, message) {
        if (!validator && !message)
            return;

        validator.errormessage = message;
        if (validator.firstChild)
            validator.firstChild.nodeValue = message;

        this._FindValidatorCallout();

        if (this._checksumValidatorCallout) {
            if (this._checksumValidatorCallout._errorMessageCell) {
                this._checksumValidatorCallout._errorMessageCell.innerHTML = message.replace(/\n/g, '<br/>');
            }
        }
    },

    _FindValidatorCallout: function() {
        if (!this._checksumValidatorCallout) {
            if (this.checksumValidatorCalloutId) {
                this._checksumValidatorCallout = $find(this.checksumValidatorCalloutId);
            }
        }
    },

    IsKulfoldByFormat: function(ragszam) {
        var checkRegexp = new RegExp(this.ragszamPatternKulfold, 'g');
        if (ragszam.length == this.ragszamLengthKulfold && checkRegexp.test(ragszam)) {
            return true;
        }
        else {
            var prefixRegexpKulfold = new RegExp("^(" + this.ragszamPrefixListKulfold.join('|') + ")");
            if (prefixRegexpKulfold.test(ragszam)) {
                return true;
            }
        }
        // default: belföld
        return false;
    },

    IsKulfoldiRagszam: function(ragszam) {
        if (this.automaticFormatDetection) {
            return this.IsKulfoldByFormat(ragszam);
        }
        else {
            return this.isKulfold;
        }
    },

    IsDataCompleted: function() {
        var element = this.get_element();

        if (element) {
            var ragszam = element.value;
            if (this.IsKulfoldiRagszam(ragszam)) {
                return (ragszam.length == this.ragszamLengthKulfold);
            }
            else {
                return (ragszam.length == this.ragszamLengthBelfold);
            }
        }

        return false;
    },

    CheckRagszam: function(ragszam) {
        if (ragszam.trim() == '') return 1;

        var ragszamSpecLength = (this.IsKulfoldiRagszam(ragszam) ? this.ragszamLengthKulfold : this.ragszamLengthBelfold);
        var ragszamPattern = (this.IsKulfoldiRagszam(ragszam) ? this.ragszamPatternKulfold : this.ragszamPatternBelfold);
        var formatError = (this.IsKulfoldiRagszam(ragszam) ? Utility.RagszamError.badRagszamFormatKulfold : Utility.RagszamError.badRagszamFormatBelfold);
        var lengthError = (this.IsKulfoldiRagszam(ragszam) ? Utility.RagszamError.badRagszamLengthKulfold : Utility.RagszamError.badRagszamLengthBelfold);
        // hossz és formátum ellenőrzés
        if (ragszam.length != ragszamSpecLength) {
            //"A nemzetközi/belföldi küldeményazonosító (ragszám) hossza nem megfelelő! Várt hossz: {0} - Tényleges hossz: {1}"
            return String.format(lengthError, ragszamSpecLength, ragszam.length);
        }
        else {
            var checkRegexp = new RegExp(ragszamPattern, 'g');
            if (!checkRegexp.test(ragszam)) {
                //"A nemzetközi/belföldi küldeményazonosító formátuma hibás!"
                return formatError;
            }
        }

        // 0 based
        var startPos = (this.IsKulfoldiRagszam(ragszam) ? 2 : 6);
        var checkSumPos = (this.IsKulfoldiRagszam(ragszam) ? 10 : 15);
        var checksumError = (this.IsKulfoldiRagszam(ragszam) ? Utility.RagszamError.checkSumErrorKulfold : Utility.RagszamError.checkSumErrorBelfold);
        var vector = (this.IsKulfoldiRagszam(ragszam) ? [8, 6, 4, 2, 3, 5, 9, 7] : [3, 1, 3, 1, 3, 1, 3, 1, 3]);

        // checksum
        var checkSum = parseInt(ragszam.charAt(checkSumPos));
        var sum = 0;
        for (var i = startPos; i < checkSumPos; i++) {
            sum += parseInt(ragszam.charAt(i)) * vector[i - startPos];
        }

        if (this.isKulfold) {
            var modresult = sum % 11;
            var calculatedcheck = -1;
            if (modresult == 0) {
                calculatedcheck = 5;
            }
            else if (modresult == 1) {
                calculatedcheck = 0;
            }
            else {
                calculatedcheck = 11 - modresult;
            }
        }
        else {
            var calculatedcheck = sum % 10;
        }

        if (calculatedcheck != checkSum) {
            //"A nemzetközi/belföldi küldeményazonosító ellenőrző összege hibás! Ellenőrző jegy: {0} - Számított: {1}"
            return String.format(checksumError, checkSum, calculatedcheck);
        }

        return 1;
    },

    PlaySound: function(sound) {
        if (sound) {
            if (this._bgSound) {
                this._bgSound.src = sound;
            }
        }

    },

    //
    // Behavior properties
    //

    get_IsKulfold: function() {
        return this.isKulfold;
    },

    set_IsKulfold: function(value) {
        if (this.isKulfold !== value) {
            this.isKulfold = value;
            this.raisePropertyChanged('IsKulfold');
        }
    },

    get_AutomaticFormatDetection: function() {
        return this.automaticFormatDetection;
    },

    set_AutomaticFormatDetection: function(value) {
        if (this.automaticFormatDetection !== value) {
            this.automaticFormatDetection = value;
            this.raisePropertyChanged('AutomaticFormatDetection');
        }
    },

    get_CheckRagszam: function() {
        return this.checkRagszam;
    },

    set_CheckRagszam: function(value) {
        if (this.checkRagszam !== value) {
            this.checkRagszam = value;
            this.raisePropertyChanged('CheckRagszam');
        }
    },

    get_ChecksumValidatorId: function() {
        return this.checksumValidatorId;
    },

    set_ChecksumValidatorId: function(value) {
        if (this.checksumValidatorId !== value) {
            this.checksumValidatorId = value;
            this.raisePropertyChanged('ChecksumValidatorId');
        }
    },

    get_ChecksumValidatorCalloutId: function() {
        return this.checksumValidatorCalloutId;
    },

    set_ChecksumValidatorCalloutId: function(value) {
        if (this.checksumValidatorCalloutId !== value) {
            this.checksumValidatorCalloutId = value;
            this.raisePropertyChanged('ChecksumValidatorCalloutId');
        }
    },

    get_BgSoundId: function() {
        return this.bgSoundId;
    },

    set_BgSoundId: function(value) {
        if (this.bgSoundId !== value) {
            this.bgSoundId = value;
            this.raisePropertyChanged('BgSoundId');
        }
    },

    get_PlaySoundOnError: function() {
        return this.playSoundOnError;
    },

    set_PlaySoundOnError: function(value) {
        if (this.playSoundOnError !== value) {
            this.playSoundOnError = value;
            this.raisePropertyChanged('PlaySoundOnError');
        }
    },

    get_PlaySoundOnAccept: function() {
        return this.playSoundOnAccept;
    },

    set_PlaySoundOnAccept: function(value) {
        if (this.playSoundOnAccept !== value) {
            this.playSoundOnAccept = value;
            this.raisePropertyChanged('PlaySoundOnAccept');
        }
    },

    get_SoundOnError: function() {
        return this.soundOnError;
    },

    set_SoundOnError: function(value) {
        if (this.soundOnError !== value) {
            this.soundOnError = value;
            this.raisePropertyChanged('SoundOnError');
        }
    },

    get_SoundOnAccept: function() {
        return this.soundOnAccept;
    },

    set_SoundOnAccept: function(value) {
        if (this.soundOnAccept !== value) {
            this.soundOnAccept = value;
            this.raisePropertyChanged('SoundOnAccept');
        }
    },

    //
    // Events
    //

    add_keyUp: function(handler) {

        this.get_events().addHandler('keyUp', handler);
    },

    remove_keyUp: function(handler) {

        this.get_events().removeHandler('keyUp', handler);
    },

    raiseKeyUp: function(eventArgs) {

        var handler = this.get_events().getHandler('keyUp');
        if (handler) {
            handler(this, eventArgs);
        }
    }
}

Utility.RagszamBehavior.registerClass('Utility.RagszamBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();