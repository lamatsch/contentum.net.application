﻿///<reference name="MicrosoftAjax.js" />
///<reference path="AjaxLogging.js" />
///<reference assembly="AjaxControlToolkit" name="AjaxControlToolkit.Common.Common.js" />

Type.registerNamespace("Utility");
Utility.TemplateLoaderBehavior = function(element) {
    Utility.TemplateLoaderBehavior.initializeBase(this, [element]);
    
    //Properties
    this._templateListId = null;
    this._setDefaultButtonId = null;
    this._removeDefaultButtonId = null;
    this._defaultTemplateBackgroundColor = null;
    this._servicePath = null;
    this._updateAlapertelmezettMethod = null;
    this._felhasznaloId = null;
    this._defaultTemplateHiddenFieldId = null;
    this._currentTemplateHiddenFieldId = null;
    this._currentVersionHiddenFieldId = null;
    this._labelDefaultId = null;
    this._linkButtonsPanelId = null;
    //Variables
    this._templateList = null;
    this._setDefaultButton = null;
    this._removeDefaultButton = null;
    this._applicationLoadHandler = null;
    this._listChangedHandler = null;
    this._defaultButtonClickHandler = null;
    this._defaultTemplateHiddenField = null;
    this._currentTemplateHiddenField = null;
    this._currentVersionHiddenField = null;
    this._labelDefault = null;
    this._linkButtonsPanel = null;
}

Utility.TemplateLoaderBehavior.prototype = {
    initialize : function() {
        Utility.TemplateLoaderBehavior.callBaseMethod(this, 'initialize');
       
        if(this._templateListId)
        {
            this._templateList = $get(this._templateListId);
        }
        
        if(this._setDefaultButtonId)
        {
            this._setDefaultButton = $get(this._setDefaultButtonId);
        }
        
        if(this._removeDefaultButtonId)
        {
            this._removeDefaultButton = $get(this._removeDefaultButtonId);
        }
        
        if(this._defaultTemplateHiddenFieldId)
        {
            this._defaultTemplateHiddenField = $get(this._defaultTemplateHiddenFieldId);
        }
        
        if(this._currentTemplateHiddenFieldId)
        {
            this._currentTemplateHiddenField = $get(this._currentTemplateHiddenFieldId);
        }  
        
        if(this._currentVersionHiddenFieldId)
        {
            this._currentVersionHiddenField = $get(this._currentVersionHiddenFieldId);
        }
        
        if(this._labelDefaultId)
        {
            this._labelDefault = $get(this._labelDefaultId);
        }
        
        if(this._linkButtonsPanelId)
        {
            this._linkButtonsPanel = $get(this._linkButtonsPanelId);
        }
        
        this._listChangedHandler = Function.createDelegate(this,this._OnListItemChanged);
        this._defaultButtonClickHandler = Function.createDelegate(this, this._OnDefaultButtonClick);
        
        if(this._templateList)
        {
            $addHandler(this._templateList,"change",this._listChangedHandler);   
        }
        
        if(this._setDefaultButton)
        {
            $addHandler(this._setDefaultButton,"click",this._defaultButtonClickHandler);
        }
        
        if(this._removeDefaultButton)
        {
            $addHandler(this._removeDefaultButton,"click",this._defaultButtonClickHandler);
        }
        
        this._applicationLoadHandler = Function.createDelegate(this,this._OnApplicationLoad);
        Sys.Application.add_load(this._applicationLoadHandler);
    },
    
    _OnApplicationLoad : function()
    {
    },
    
    _OnListItemChanged: function(sender, args)
    {         
        if(this._templateList && this._setDefaultButton && this._removeDefaultButton && this._defaultTemplateBackgroundColor)
        {
            if(this._templateList.selectedIndex > -1)
            {
               var selectedItem = this._templateList.options[this._templateList.selectedIndex];
               if(selectedItem)
               {
                    if(this._IsListItemAlapertelmezett(selectedItem))
                    {
                        this._setDefaultButton.style.display = 'none';
                        this._removeDefaultButton.style.display = '';
                    }
                    else
                    {
                        this._setDefaultButton.style.display = '';
                        this._removeDefaultButton.style.display = 'none';
                    }
               }
            }
        }
    },
    
    _OnDefaultButtonClick: function(sender, args)
    {
        if(this._templateList && this._servicePath && this._updateAlapertelmezettMethod && this._felhasznaloId)
        {
            if(this._templateList.selectedIndex > -1)
            {
                var selectedItem = this._templateList.options[this._templateList.selectedIndex];
                var newAlapertelmezettValue = 0;
                var templateId = selectedItem.value;
                if(this._IsListItemAlapertelmezett(selectedItem))
                {
                    newAlapertelmezettValue = 0;
                }
                else
                {
                    newAlapertelmezettValue = 1;
                }
                
                var params = {"felhasznaloId":this._felhasznaloId,"templateId": templateId,"Alapertelmezett":newAlapertelmezettValue };
                
                this._UpdatingStart();
                Sys.Net.WebServiceProxy.invoke(this._servicePath,this._updateAlapertelmezettMethod,
                false, params, Function.createDelegate(this,this._OnUpdateAlapertelmezettSuccess),
                Function.createDelegate(this,this._OnUpdateAlapertelmezettError),newAlapertelmezettValue); 
            }
        }
        
        return false;
    },
    
    _OnUpdateAlapertelmezettSuccess: function(result, context)
    {
        this._SetNewAlapertelmezett(context);
        this._UpdatingEnd();
        
        if(this._templateList)
        {
            $common.tryFireEvent(this._templateList,"change");
        }
    },
    
    _SetNewAlapertelmezett: function(newAlapertelmezettValue)
    {
        var selectedItem = null;
        if(this._templateList && this._templateList.selectedIndex > -1)
        {
                var selectedItem = this._templateList.options[this._templateList.selectedIndex];
        }
        //dropdown lista és default hiddenfield beallitasa
        if(newAlapertelmezettValue == 1)
        {
            if(selectedItem)
            {
                var selectedId = selectedItem.value;
                
                for(var i=0; i<this._templateList.options.length; i++)
                {
                    var item = this._templateList.options[i];
                    if(item.value == selectedId)
                    {
                        item.style.background = this._defaultTemplateBackgroundColor;
                    }
                    else
                    {
                        item.style.background = '';
                    }
                }
            }
            
            if(this._defaultTemplateHiddenField)
            {
                this._defaultTemplateHiddenField.value = selectedId;
            }
            
        }
        else
        {
            if(this._templateList && this._templateList.selectedIndex > -1)
            {
                selectedItem.style.background = '';
            } 
            
            if(this._defaultTemplateHiddenField)
            {
                this._defaultTemplateHiddenField.value = '';
            }   
        }
        
        //verzio es jelzo label beallitasa
        if(this._currentTemplateHiddenField && this._currentVersionHiddenField)
        {
             if(selectedItem)
             {
                if(this._currentTemplateHiddenField.value == selectedItem.value)
                {
                    var version = parseInt(this._currentVersionHiddenField.value);
                    if(!isNaN(version))
                    {
                        this._currentVersionHiddenField.value = version + 1;
                    }
                    
                    if(newAlapertelmezettValue == 1)
                    {
                        if(this._labelDefault)
                        {
                            this._labelDefault.style.display = '';
                        }
                    }
                    else
                    {
                        if(this._labelDefault)
                        {
                            this._labelDefault.style.display = 'none';
                        }
                    }
                }
                else
                {
                        if(this._labelDefault)
                        {
                            this._labelDefault.style.display = 'none';
                        }
                }
            }
        }
        
        //Alapértelmezett link beallitasa
        if(this._linkButtonsPanel && this._linkButtonsPanel.childNodes && this._linkButtonsPanel.childNodes.length > 0 && selectedItem)
        {
            for(var i=0; i< this._linkButtonsPanel.childNodes.length; i++)
            {
               var currentLink = this._linkButtonsPanel.childNodes[i];
               if(currentLink && currentLink.id)
               {
                   if(currentLink.id.endsWith('_'+ selectedItem.value))
                   {
                      if(newAlapertelmezettValue == 1)
                      {
                        currentLink.style.background = this._defaultTemplateBackgroundColor;
                      }
                      else
                      {
                        currentLink.style.background = '';
                        break;
                      }
                   }
                   else
                   {
                      currentLink.style.background = '';
                   }
               }
            }
        }
    },
    
    _OnUpdateAlapertelmezettError : function(err,response, context)
    {
       this._UpdatingEnd();
       var errorMessage = err.get_message();
       alert("Hiba lépett fel a módosítás során: " + errorMessage);
    },
    
    _UpdatingStart: function()
    {
        this._templateList.disabled = true;
    },
    
    _UpdatingEnd: function()
    {
       this._templateList.disabled = false;
    },
    
    _IsListItemAlapertelmezett : function(item)
    {
        if(item.style.background == this._defaultTemplateBackgroundColor)
        {
            return true;
        }
        else
        {
            return false;
        }
    },

    dispose : function() {
     
        if(this._applicationLoadHandler)
        {
            Sys.Application.remove_load(this._applicationLoadHandler);
            this._applicationLoadHandler = null;
        }
        
        if(this._listChangedHandler)
        {
            if(this._templateList)
            {
                $removeHandler(this._templateList,"change",this._listChangedHandler);
            }
            
            this._listChangedHandler = null;
        }
        
        if(this._defaultButtonClickHandler)
        {
            if(this._setDefaultButton)
            {
                $addHandler(this._setDefaultButton,"click",this._defaultButtonClickHandler);
            }
        
            if(this._removeDefaultButton)
            {
                $addHandler(this._removeDefaultButton,"click",this._defaultButtonClickHandler);
            }
            
            this._defaultButtonClickHandler = null;
        }
        

        Utility.TemplateLoaderBehavior.callBaseMethod(this, 'dispose');
    },
        
    //getter, setter methods
    get_TemplateListId : function()
    {
        return this._templateListId;
    },
    
    set_TemplateListId : function(value)
    {
       if(this._templateListId != value)
       {
          this._templateListId = value;
       }
    },

    get_SetDefaultButtonId : function()
    {
        return this._setDefaultButtonId;
    },
    
    set_SetDefaultButtonId : function(value)
    {
       if(this._setDefaultButtonId != value)
       {
          this._setDefaultButtonId = value;
       }
    },
    
    get_RemoveDefaultButtonId: function()
    {
        return this._removeDefaultButtonId;
    },
    
    set_RemoveDefaultButtonId : function(value)
    {
       if(this._removeDefaultButtonId != value)
       {
          this._removeDefaultButtonId= value;
       }
    },
    
    get_DefaultTemplateBackgroundColor: function()
    {
        return this._defaultTemplateBackgroundColor;
    },
    
    set_DefaultTemplateBackgroundColor : function(value)
    {
       if(this._defaultTemplateBackgroundColor != value)
       {
          this._defaultTemplateBackgroundColor= value;
       }
    },
    
    get_ServicePath: function()
    {
        return this._servicePath;
    },
    
    set_ServicePath : function(value)
    {
       if(this._servicePath != value)
       {
          this._servicePath = value;
       }
    },
    
    get_UpdateAlapertelmezettMethod: function()
    {
        return this._updateAlapertelmezettMethod;
    },
    
    set_UpdateAlapertelmezettMethod : function(value)
    {
       if(this._updateAlapertelmezettMethod != value)
       {
          this._updateAlapertelmezettMethod = value;
       }
    },
    
    get_FelhasznaloId: function()
    {
        return this._felhasznaloId;
    },
    
    set_FelhasznaloId : function(value)
    {
       if(this._felhasznaloId != value)
       {
          this._felhasznaloId = value;
       }
    },
    
    get_DefaultTemplateHiddenFieldId: function()
    {
        return this._defaultTemplateHiddenFieldId;
    },
    
    set_DefaultTemplateHiddenFieldId : function(value)
    {
       if(this._defaultTemplateHiddenFieldId != value)
       {
          this._defaultTemplateHiddenFieldId = value;
       }
    },
    
    get_CurrentTemplateHiddenFieldId: function()
    {
        return this._currentTemplateHiddenFieldId;
    },
    
    set_CurrentTemplateHiddenFieldId : function(value)
    {
       if(this._currentTemplateHiddenFieldId != value)
       {
          this._currentTemplateHiddenFieldId = value;
       }
    },
    
    get_CurrentVersionHiddenFieldId: function()
    {
        return this._currentVersionHiddenFieldId;
    },
    
    set_CurrentVersionHiddenFieldId : function(value)
    {
       if(this._currentVersionHiddenFieldId != value)
       {
          this._currentVersionHiddenFieldId = value;
       }
    },
    
    get_LabelDefaultId: function()
    {
        return this._labelDefaultId;
    },
    
    set_LabelDefaultId : function(value)
    {
       if(this._labelDefaultId != value)
       {
          this._labelDefaultId = value;
       }
    },
    
    get_LinkButtonsPanelId: function()
    {
        return this._linkButtonsPanelId;
    },
    
    set_LinkButtonsPanelId : function(value)
    {
       if(this._linkButtonsPanelId != value)
       {
          this._linkButtonsPanelId = value;
       }
    }
}

Utility.TemplateLoaderBehavior.registerClass('Utility.TemplateLoaderBehavior', Sys.UI.Behavior);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();