using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
//using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;

// TELJES K�D KIDOLGOZAND�!

public partial class JegyzekTetelekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;


    private void SetNewControls()
    {
    }

    private void SetViewControls()
    {
        JegyzekTextBox1.ReadOnly = true;
        FoszamTextBox1.ReadOnly = true;
        txtMegjegyzes.ReadOnly = true;
        CalendarControl_Sztornozasdatuma.ReadOnly = true;
        txtAtvevoIktatoszama.ReadOnly = true;
    }

    private void SetModifyControls()
    {
        JegyzekTextBox1.ReadOnly = true;
        FoszamTextBox1.ReadOnly = true;
        txtMegjegyzes.ReadOnly = false;
        CalendarControl_Sztornozasdatuma.ReadOnly = true;
        txtAtvevoIktatoszama.ReadOnly = false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "JegyzekTetel" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                MIG_JegyzekTetelekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekTetelekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    MIG_JegyzekTetelek erec_JegyzekTetelek = (MIG_JegyzekTetelek)result.Record;
                    LoadComponentsFromBusinessObject(erec_JegyzekTetelek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            this.SetNewControls();
        }

        if (Command == CommandName.Modify)
        {
            this.SetModifyControls();
        }

        if (Command == CommandName.View)
        {
            this.SetViewControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.IraJegyzekTetelekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if ((!String.IsNullOrEmpty(JegyzekTextBox1.Id_HiddenField)
                && JegyzekTextBox1.JegyzekTipus != UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value)
            || !String.IsNullOrEmpty(CalendarControl_Sztornozasdatuma.Text))
        {
            txtAtvevoIktatoszama.ReadOnly = true;
        }

        // sztorn�zott nem m�dos�that� (sztorn�z�s: itt �rv�nytelen�t�s)
        if (!String.IsNullOrEmpty(CalendarControl_Sztornozasdatuma.Text))
        {
            SetViewControls();
            FormFooter1.SaveEnabled = false;
        }
    }

    private void LoadComponentsFromBusinessObject(MIG_JegyzekTetelek mig_JegyzekTetelek)
    {
        JegyzekTextBox1.Id_HiddenField = mig_JegyzekTetelek.Jegyzek_Id;
        JegyzekTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);

        FoszamTextBox1.Id_HiddenField = mig_JegyzekTetelek.MIG_Foszam_Id;
        FoszamTextBox1.SetTextBoxById(FormHeader1.ErrorPanel, null);

        txtMegjegyzes.Text = mig_JegyzekTetelek.Megjegyzes;

        //CalendarControl_Sztornozasdatuma.Text = rec_JegyzekTetelek.SztornozasDat;
        DateTime dt;
        if (DateTime.TryParse(mig_JegyzekTetelek.SztornozasDat, out dt) && DateTime.Compare(dt, DateTime.Now) < 0)
        {
            CalendarControl_Sztornozasdatuma.Text = mig_JegyzekTetelek.SztornozasDat;
        }
        else
        {
            CalendarControl_Sztornozasdatuma.Text = String.Empty;
        }

        txtAtvevoIktatoszama.Text = mig_JegyzekTetelek.AtvevoIktatoszama;

        FormHeader1.Record_Ver = mig_JegyzekTetelek.Base.Ver;
        FormPart_CreatedModified1.SetComponentValues(mig_JegyzekTetelek.Base);
    }

    private MIG_JegyzekTetelek GetBusinessObjectFromComponents()
    {
        MIG_JegyzekTetelek mig_JegyzekTetelek = new MIG_JegyzekTetelek();

        mig_JegyzekTetelek.Updated.SetValueAll(false);
        mig_JegyzekTetelek.Base.Updated.SetValueAll(false);

        mig_JegyzekTetelek.Jegyzek_Id = JegyzekTextBox1.Id_HiddenField;
        mig_JegyzekTetelek.Updated.Jegyzek_Id = pageView.GetUpdatedByView(JegyzekTextBox1);

        mig_JegyzekTetelek.MIG_Foszam_Id = FoszamTextBox1.Id_HiddenField;
        mig_JegyzekTetelek.Updated.MIG_Foszam_Id = pageView.GetUpdatedByView(FoszamTextBox1);

        mig_JegyzekTetelek.Megjegyzes = txtMegjegyzes.Text;
        mig_JegyzekTetelek.Updated.Megjegyzes = pageView.GetUpdatedByView(txtMegjegyzes);

        //erec_JegyzekTetelek.SztornozasDat = CalendarControl_Sztornozasdatuma.Text;
        //erec_JegyzekTetelek.Updated.SztornozasDat = pageView.GetUpdatedByView(CalendarControl_Sztornozasdatuma);

        mig_JegyzekTetelek.AtvevoIktatoszama = txtAtvevoIktatoszama.Text;
        mig_JegyzekTetelek.Updated.AtvevoIktatoszama = pageView.GetUpdatedByView(txtAtvevoIktatoszama);

        mig_JegyzekTetelek.Base.Ver = FormHeader1.Record_Ver;
        mig_JegyzekTetelek.Base.Updated.Ver = true;

        return mig_JegyzekTetelek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(JegyzekTextBox1);
            compSelector.Add_ComponentOnClick(FoszamTextBox1);
            compSelector.Add_ComponentOnClick(txtMegjegyzes);
            compSelector.Add_ComponentOnClick(txtAtvevoIktatoszama);
            compSelector.Add_ComponentOnClick(CalendarControl_Sztornozasdatuma);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "JegyzekTetel" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            MIG_JegyzekTetelekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekTetelekService();
                            MIG_JegyzekTetelek erec_JegyzekTetelek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_JegyzekTetelek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, txtAtvevoIktatoszama.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                MIG_JegyzekTetelekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekTetelekService();
                                MIG_JegyzekTetelek erec_JegyzekTetelek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_JegyzekTetelek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
