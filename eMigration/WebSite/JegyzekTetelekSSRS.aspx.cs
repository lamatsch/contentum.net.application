﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.Utility;
using Contentum.eQuery;
using Contentum.eUtility;
using GridViewColumns = Contentum.eRecord.BaseUtility.GridViewColumns;
using GridViewColumnVisibilityState = Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState;

public partial class JegyzekTetelekSSRS : Contentum.eUtility.UI.PageBase
{
    private string JegyzekId = null;
    private string OrderBy = null;

    GridViewColumnVisibilityState visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();
    GridViewColumnVisibilityState tetelekVisibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();

    protected void Page_Init(object sender, EventArgs e)
    {
        JegyzekId = Request.QueryString.Get(Contentum.eUtility.QueryStringVars.Id);
        OrderBy = Request.QueryString.Get("OrderBy");//(Contentum.eUtility.QueryStringVars.OrderBy);

        visibilityState.LoadFromCustomString((string)Page.Session[GridViewColumnVisibilityState.Constants.SessionVariables.JegyzekTetelekSSRS]);
        tetelekVisibilityState.LoadFromCustomString((string)Page.Session[GridViewColumnVisibilityState.Constants.SessionVariables.JegyzekTetelekSSRSTetelek]);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       if(!IsPostBack)
     { 
        ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
        ReportViewer1.ServerReport.ReportServerCredentials = rvc;
		
		System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
        ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
        ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;
		
        ReportViewer1.ShowRefreshButton = false;
        ReportViewer1.ShowParameterPrompts = false;

        ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

        if (rpis.Count > 0)
        {
            ReportParameter[] ReportParameters = GetReportParameters(rpis);

            ReportViewer1.ServerReport.SetParameters(ReportParameters);
        }

        ReportViewer1.ServerReport.Refresh();
    }
  }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                #region Jegyzekek Where
                MIG_JegyzekekSearch search_jegyzek = new MIG_JegyzekekSearch();
                search_jegyzek.Id.Value = JegyzekId;
                search_jegyzek.Id.Operator = Query.Operators.equals;

                search_jegyzek.TopRow = 0;
                search_jegyzek.OrderBy = " order by MIG_Jegyzekek.LetrehozasIdo";

                Query query_jegyzek = new Query();
                query_jegyzek.BuildFromBusinessDocument(search_jegyzek);

                string WhereJegyzek = query_jegyzek.Where;
                #endregion Jegyzekek Where

                #region Jegyzek tetelek Where
                MIG_JegyzekTetelekSearch search_jegyzektetelek = new MIG_JegyzekTetelekSearch();
                search_jegyzektetelek.Jegyzek_Id.Value = JegyzekId;
                search_jegyzektetelek.Jegyzek_Id.Operator = Query.Operators.equals;

                search_jegyzektetelek.SztornozasDat.Value = "";
                search_jegyzektetelek.SztornozasDat.Operator = Query.Operators.isnull;

                search_jegyzektetelek.TopRow = 0;
                search_jegyzektetelek.OrderBy = String.IsNullOrEmpty(OrderBy) ? " order by MIG_JegyzekTetelek.Azonosito" : String.Format(" order by {0}", OrderBy);

                Query query_jegyzektetelek = new Query();
                query_jegyzektetelek.BuildFromBusinessDocument(search_jegyzektetelek);

                string WhereJegyzekTetelek = query_jegyzektetelek.Where;
                #endregion Jegyzek tetelek Where

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "WhereJegyzek":
                            ReportParameters[i].Values.Add(WhereJegyzek);
                            break;
                        case "OrderByJegyzek":
                            ReportParameters[i].Values.Add(search_jegyzek.OrderBy);
                            break;
                        case "TopRowJegyzek":
                            ReportParameters[i].Values.Add(Convert.ToString(search_jegyzek.TopRow));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(Page));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(Contentum.eUtility.FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(null);
                            break;
                        case "WhereJegyzekTetelek":
                            ReportParameters[i].Values.Add(WhereJegyzekTetelek);
                            break;
                        case "OrderByJegyzekTetelek":
                            ReportParameters[i].Values.Add(search_jegyzektetelek.OrderBy);
                            break;
                        case "TopRowJegyzekTetelek":
                            ReportParameters[i].Values.Add(Convert.ToString(search_jegyzektetelek.TopRow));
                            break;
                        case "TipusVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.JegyzekekColumnNames.Tipus));
                            break;
                        case "JegyzekVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.JegyzekekColumnNames.Nev));
                            break;
                        case "KezeloVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.JegyzekekColumnNames.Felelos_Nev));
                            break;
                        case "VegrehajtoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.JegyzekekColumnNames.Vegrehajto_Nev));
                            break;
                        case "AtvevoVisibility"://[Partner_Id_LeveltariAtvevo_Nev]
                            ReportParameters[i].Values.Add(Boolean.TrueString);
                            break;
                        case "LetrehozasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.JegyzekekColumnNames.LetrehozasIdo));
                            break;
                        case "LezarasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.JegyzekekColumnNames.LezarasDatuma));
                            break;
                        case "MegsemmisitesVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.JegyzekekColumnNames.MegsemmisitesDatuma));
                            break;
                        case "StornoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.JegyzekekColumnNames.SztornozasDat));
                            break;
                        case "AtvevoIktatoszamVisibility":
                            ReportParameters[i].Values.Add(tetelekVisibilityState.GetIsVisibleValue(GridViewColumns.JegyzekTetelekColumnName.AtvevoIktatoszama));
                            break;
                        case "AtadasDatumaVisibility":
                            ReportParameters[i].Values.Add(tetelekVisibilityState.GetIsVisibleValue(GridViewColumns.JegyzekTetelekColumnName.AtadasDatuma));
                            break;

                    }
                }
            }
        }
        return ReportParameters;
    }
}
