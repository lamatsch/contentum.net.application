using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
//using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;

// TELJES K�D KIDOLGOZAND�!

public partial class JegyzekekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;


    private void SetNewControls()
    {
        CsoportTextBoxFelelos.Id_HiddenField = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
        CsoportTextBoxFelelos.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        trVegrehajto.Visible = false;
        trAtvevo.Visible = false;
        trLezaras.Visible = false;
        trMegsemmesites.Visible = false;
        trSztornozas.Visible = false;
    }

    private void SetViewControls()
    {
        Nev_TextBox.ReadOnly = true;
        TipusDropDownList.Enabled = false;
        CsoportTextBoxFelelos.ReadOnly = true;
        CsoportTextBoxVegrehajto.ReadOnly = true;
        textAtvevoEllenor.ReadOnly = true;
        CalendarControlMegsemmisites.ReadOnly = true;

    }

    private void SetModifyControls()
    {
        TipusDropDownList.Enabled = false;
        CsoportTextBoxFelelos.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Jegyzek" + Command);
                break;
        }

        if (!IsPostBack)
        {
            UI.JegyzekTipus.FillDropDownList(TipusDropDownList);

            string startup = Request.QueryString.Get(QueryStringVars.Startup);

            if (startup == Constants.Startup.FromSelejtezes)
            {
                TipusDropDownList.SelectedValue = UI.JegyzekTipus.SelejtezesiJegyzek.Value;
                TipusDropDownList.ReadOnly = true;
            }
            else
            if (startup == Constants.Startup.FromLeveltarbaAdas)
            {
                TipusDropDownList.SelectedValue = UI.JegyzekTipus.LeveltariAtadasJegyzek.Value;
                TipusDropDownList.ReadOnly = true;
            }
            else
            if (startup == Constants.Startup.FromEgyebSzervezetnekAtadas)
            {
                TipusDropDownList.SelectedValue = UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value;
                TipusDropDownList.ReadOnly = true;
            }
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                MIG_JegyzekekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    MIG_Jegyzekek MIG_Jegyzekek = (MIG_Jegyzekek)result.Record;
                    LoadComponentsFromBusinessObject(MIG_Jegyzekek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            this.SetNewControls();
        }

        if (Command == CommandName.Modify)
        {
            this.SetModifyControls();
        }

        if (Command == CommandName.View)
        {
            this.SetViewControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.IraJegyzekekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private void LoadComponentsFromBusinessObject(MIG_Jegyzekek mig_Jegyzekek)
    {
        Nev_TextBox.Text = mig_Jegyzekek.Nev;

        TipusDropDownList.SelectedValue = mig_Jegyzekek.Tipus;

        CsoportTextBoxFelelos.Id_HiddenField = mig_Jegyzekek.Csoport_Id_Felelos;
        CsoportTextBoxFelelos.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        CalendarControlLezaras.Text = mig_Jegyzekek.LezarasDatuma;

        CsoportTextBoxVegrehajto.Id_HiddenField = mig_Jegyzekek.FelhasznaloCsoport_Id_Vegrehaj;
        CsoportTextBoxVegrehajto.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        //PartnerTextBoxAtvevo.Id_HiddenField = erec_Jegyzekek.Partner_Id_LeveltariAtvevo;
        //PartnerTextBoxAtvevo.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
        textAtvevoEllenor.Text = mig_Jegyzekek.Atvevo_Nev;
        if (mig_Jegyzekek.Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value)
        {
            labelAtvevoEllenor.Text = "Ellen�r";
        }

        CalendarControlMegsemmisites.Text = mig_Jegyzekek.VegrehajtasDatuma;

        CalendarControlStorno.Text = mig_Jegyzekek.SztornozasDat;

        FormHeader1.Record_Ver = mig_Jegyzekek.Base.Ver;
    }

    private MIG_Jegyzekek GetBusinessObjectFromComponents()
    {
        MIG_Jegyzekek mig_Jegyzekek = new MIG_Jegyzekek();

        mig_Jegyzekek.Updated.SetValueAll(false);
        mig_Jegyzekek.Base.Updated.SetValueAll(false);

        mig_Jegyzekek.Nev = Nev_TextBox.Text;
        mig_Jegyzekek.Updated.Nev = pageView.GetUpdatedByView(Nev_TextBox);

        mig_Jegyzekek.Tipus = TipusDropDownList.SelectedValue;
        mig_Jegyzekek.Updated.Tipus = pageView.GetUpdatedByView(TipusDropDownList);

        mig_Jegyzekek.Csoport_Id_Felelos = CsoportTextBoxFelelos.Id_HiddenField;
        mig_Jegyzekek.Updated.Csoport_Id_Felelos = pageView.GetUpdatedByView(CsoportTextBoxFelelos);

        mig_Jegyzekek.Felelos_Nev = CsoportTextBoxFelelos.Text;
        mig_Jegyzekek.Updated.Felelos_Nev = pageView.GetUpdatedByView(CsoportTextBoxFelelos);

        mig_Jegyzekek.LezarasDatuma = CalendarControlLezaras.Text;
        mig_Jegyzekek.Updated.LezarasDatuma = pageView.GetUpdatedByView(CalendarControlLezaras);

        mig_Jegyzekek.FelhasznaloCsoport_Id_Vegrehaj = CsoportTextBoxVegrehajto.Id_HiddenField;
        mig_Jegyzekek.Updated.FelhasznaloCsoport_Id_Vegrehaj = pageView.GetUpdatedByView(CsoportTextBoxVegrehajto);

        mig_Jegyzekek.Vegrehajto_Nev = CsoportTextBoxVegrehajto.Text;
        mig_Jegyzekek.Updated.Vegrehajto_Nev = pageView.GetUpdatedByView(CsoportTextBoxVegrehajto);

        //mig_Jegyzekek.Partner_Id_LeveltariAtvevo = PartnerTextBoxAtvevo.Id_HiddenField;
        //mig_Jegyzekek.Updated.Partner_Id_LeveltariAtvevo = pageView.GetUpdatedByView(PartnerTextBoxAtvevo);
        mig_Jegyzekek.Atvevo_Nev = textAtvevoEllenor.Text;
        mig_Jegyzekek.Updated.Atvevo_Nev = pageView.GetUpdatedByView(textAtvevoEllenor);

        mig_Jegyzekek.VegrehajtasDatuma = CalendarControlMegsemmisites.Text;
        mig_Jegyzekek.Updated.VegrehajtasDatuma = pageView.GetUpdatedByView(CalendarControlMegsemmisites);

        mig_Jegyzekek.SztornozasDat = CalendarControlStorno.Text;
        mig_Jegyzekek.Updated.SztornozasDat = pageView.GetUpdatedByView(CalendarControlStorno);


        mig_Jegyzekek.Base.Ver = FormHeader1.Record_Ver;
        mig_Jegyzekek.Base.Updated.Ver = true;

        return mig_Jegyzekek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Nev_TextBox);
            compSelector.Add_ComponentOnClick(TipusDropDownList);
            compSelector.Add_ComponentOnClick(CsoportTextBoxFelelos);
            compSelector.Add_ComponentOnClick(CalendarControlLezaras);
            compSelector.Add_ComponentOnClick(CsoportTextBoxVegrehajto);
            compSelector.Add_ComponentOnClick(textAtvevoEllenor);
            compSelector.Add_ComponentOnClick(CalendarControlMegsemmisites);
            compSelector.Add_ComponentOnClick(CalendarControlStorno);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Jegyzek" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            MIG_JegyzekekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
                            MIG_Jegyzekek erec_Jegyzekek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_Jegyzekek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev_TextBox.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                MIG_JegyzekekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
                                MIG_Jegyzekek erec_Jegyzekek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_Jegyzekek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
