﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
CodeFile="JegyzekekList.aspx.cs" Inherits="JegyzekekList" Title="Untitled Page" EnableEventValidation="false"%>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
<%--HiddenFields--%> 

   
<%--HiddenFields--%> 
<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="JegyzekekCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="JegyzekekUpdatePanel" runat="server" OnLoad="JegyzekekUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="JegyzekekCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="JegyzekekCPEButton"
                            CollapseControlID="JegyzekekCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="JegyzekekCPEButton" ExpandedSize="0" ExpandedText="Jegyzékek listája"
                            CollapsedText="Jegyzékek listája">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="JegyzekekGridView" runat="server" OnRowCommand="JegyzekekGridView_RowCommand"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" GridLines="None" AllowPaging="True"
                                            PagerSettings-Visible="false" AllowSorting="True" OnPreRender="JegyzekekGridView_PreRender"
                                            AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="JegyzekekGridView_Sorting"
                                            OnRowDataBound="JegyzekekGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <div class="DisableWrap">
                                                        <asp:ImageButton ID="SelectingRowsImageButton"
                                                            runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                            runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                 <asp:TemplateField HeaderText="Típus" AccessibleHeaderText="Tipus" SortExpression="Tipus"
                                                    ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="170px">                                                                                            
                                                <ItemTemplate>
                                                    <asp:Label ID="labelJegyzekTipus" runat="server" Text='<%#Eval("Tipus") %>'/>
                                                </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:BoundField DataField="Nev" AccessibleHeaderText="Nev" HeaderText="Jegyzék megnevezés" SortExpression="Nev" >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Felelos_Nev" AccessibleHeaderText="Felelos_Nev" HeaderText="Kezelő" SortExpression="Felelos_Nev" >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Vegrehajto_Nev" AccessibleHeaderText="Vegrehajto_Nev" HeaderText="Végrehajtó" SortExpression="Vegrehajto_Nev" >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField> 
                                                <asp:BoundField DataField="Atvevo_Nev" AccessibleHeaderText="Atvevo_Nev" HeaderText="Átvevő/Ellenőr" SortExpression="Atvevo_Nev" >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                 <asp:TemplateField HeaderText="Létrehozás" AccessibleHeaderText="LetrehozasIdo" SortExpression="LetrehozasIdo"
                                                    ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="150px">                                                                                            
                                                <ItemTemplate>
                                                    <asp:Label ID="labelLetrehozasDate" runat="server" Text='<%#Eval("LetrehozasIdo") %>'/>
                                                </ItemTemplate>
                                                </asp:TemplateField>    
                                                 <asp:TemplateField HeaderText="Lezárás" AccessibleHeaderText="LezarasDatuma" SortExpression="LezarasDatuma"
                                                     ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="150px">                                                                                            
                                                <ItemTemplate>
                                                    <asp:Label ID="labelLezarasDate" runat="server" Text='<%#Eval("LezarasDatuma") %>'/>
                                                </ItemTemplate>
                                                </asp:TemplateField>    
                                                 <asp:TemplateField HeaderText="Selejtezés\ Levéltárba átadás időpontja" AccessibleHeaderText="VegrehajtasDatuma" SortExpression="VegrehajtasDatuma"
                                                    ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="150px">                                                                                           
                                                <ItemTemplate>
                                                    <asp:Label ID="labelVegrehajtasDate" runat="server" Text='<%#Eval("VegrehajtasDatuma") %>'/>
                                                </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sztornó" AccessibleHeaderText="SztornozasDat" SortExpression="SztornozasDat"
                                                         ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="150px">                                                                                            
                                                <ItemTemplate>
                                                    <asp:Label ID="labelSztornoDate" runat="server" Text='<%#Eval("SztornozasDat") %>'/>
                                                </ItemTemplate>
                                                </asp:TemplateField>        
                                                <asp:TemplateField HeaderText="Megsemmisítés időpontja" AccessibleHeaderText="MegsemmisitesDatuma" SortExpression="MegsemmisitesDatuma" 
                                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelMegsemmisitesDatuma" runat="server" Text='<%#Eval("MegsemmisitesDatuma") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Állapot" AccessibleHeaderText="Allapot_Nev" SortExpression="Allapot_Nev" 
                                                        ItemStyle-CssClass="GridViewBoundFieldItemStyle" HeaderStyle-CssClass="GridViewBorderHeader" HeaderStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelAllapotNev" runat="server" Text='<%#Eval("Allapot_Nev") %>' />
                                                        <asp:Label ID="labelAllapot" runat="server" Text='<%#Eval("Allapot") %>' Style="display: none;" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
    <td style="text-align: left; height: 8px;" colspan="2">
    </td>
    </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px; height: 575px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%; height: 575px;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                            TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                            CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                            AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel8" runat="server">
                                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server"  Width="100%">
                                                <ajaxToolkit:TabPanel ID="JegyzekTetelekTabPanel" runat="server" TabIndex="0">
                                                    <HeaderTemplate>
                                                        <asp:UpdatePanel ID="LabelUpdatePanelJegyzekTetelek" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="Header1" runat="server" Text="Jegyzék tételek"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </HeaderTemplate>
                                                    <ContentTemplate>
                                                        <asp:UpdatePanel ID="JegyzekTetelekUpdatePanel" runat="server" OnLoad="JegyzekTetelekUpdatePanel_Load">
                                                            <ContentTemplate>
                                                                <asp:Panel ID="JegyzekTetelekPanel" runat="server" Visible="false" Width="100%">
                                                                    <uc1:SubListHeader ID="JegyzekTetelekSubListHeader" runat="server" />
                                                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                                <ajaxToolkit:CollapsiblePanelExtender ID="JegyzekTetelekCPE" runat="server" TargetControlID="Panel2"
                                                                                    CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                                    AutoExpand="false" ExpandedSize="0">
                                                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                                                <asp:Panel ID="Panel2" runat="server">
                                                                                    <asp:GridView ID="JegyzekTetelekGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                                        BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                                        AllowSorting="True" AutoGenerateColumns="false" OnSorting="JegyzekTetelekGridView_Sorting"
                                                                                        OnPreRender="JegyzekTetelekGridView_PreRender" OnRowCommand="JegyzekTetelekGridView_RowCommand"
                                                                                        DataKeyNames="Id">
                                                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField>
                                                                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                                <HeaderTemplate>
                                                                                                    <div class="DisableWrap">
                                                                                                    <asp:ImageButton ID="SelectingRowsImageButton"
                                                                                                        runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                                    &nbsp;&nbsp;
                                                                                                    <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                                                        runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                                    </div>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                                <HeaderStyle Width="25px" />
                                                                                                <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                            </asp:CommandField>
                                                                                            <asp:BoundField DataField="Azonosito" AccessibleHeaderText="Azonosito" HeaderText="Azonosító" SortExpression="Azonosito" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            HeaderStyle-Width="150px" />
                                                                                            <asp:BoundField DataField="Megjegyzes" AccessibleHeaderText="Megjegyzes" HeaderText="Megjegyzés" SortExpression="Megjegyzes" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            HeaderStyle-Width="200px" />
                                                                                            <asp:BoundField DataField="LetrehozasIdo" AccessibleHeaderText="LetrehozasIdo" HeaderText="Jegyzékre helyezés" SortExpression="LetrehozasIdo" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            HeaderStyle-Width="120px" />
                                                                                            <asp:BoundField DataField="AtvevoIktatoszama" AccessibleHeaderText="AtvevoIktatoszama" HeaderText="Átvevő iktatószáma" SortExpression="AtvevoIktatoszama" >
                                                                                                <HeaderStyle Width="150px" />
                                                                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                            </asp:BoundField>
                                                                                             <asp:BoundField DataField="AtadasDatuma" AccessibleHeaderText="AtadasDatuma" HeaderText="Átadás dátuma" SortExpression="AtadasDatuma" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                                 HeaderStyle-Width="120px" />
                                                                                            <asp:TemplateField>
                                                                                                <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                                <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="labelObjId" runat="server" Text='<%#Eval("MIG_Foszam_Id")%>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />

</asp:Content>

