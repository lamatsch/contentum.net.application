﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eAdmin.Service;
using Contentum.eMigration.Utility;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.BusinessDocuments;

public partial class JegyzekekList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    private bool IsSztornoEnabled = false;

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "JegyzekekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        JegyzekTetelekSubListHeader.RowCount_Changed += new EventHandler(JegyzekTetelekSubListHeader_RowCount_Changed);

        JegyzekTetelekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(JegyzekTetelekSubListHeader_ErvenyessegFilter_Changed);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.IraJegyzekekListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(MIG_JegyzekekSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.AtadasVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("JegyzekekSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, JegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("JegyzekekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, JegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(JegyzekekGridView.ClientID);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(JegyzekekGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(JegyzekekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(JegyzekekGridView.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("JegyzekekListajaPrintForm.aspx");

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(JegyzekekGridView.ClientID);
        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, JegyzekekUpdatePanel.ClientID, "", true);

        ListHeader1.AtadasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();


        ListHeader1.JegyzekMegsemmisitesVisible = true;
        ListHeader1.JegyzekZarolasVisible = true;
        ListHeader1.JegyzekVegrehajtasVisible = true;
        ListHeader1.SztornoVisible = true;

        ListHeader1.JegyzekZarolasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.JegyzekVegrehajtasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.SztornoOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.JegyzekMegsemmisitesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = JegyzekekGridView;

        JegyzekTetelekSubListHeader.NewVisible = false;
        JegyzekTetelekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        JegyzekTetelekSubListHeader.ModifyVisible = true;
        JegyzekTetelekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        JegyzekTetelekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickHierarchicalDeleteConfirm(JegyzekTetelekGridView.ClientID);
        JegyzekTetelekSubListHeader.ButtonsClick += new CommandEventHandler(JegyzekTetelekSubListHeader_ButtonsClick);

        //selectedRecordId kezelése
        JegyzekTetelekSubListHeader.AttachedGridView = JegyzekTetelekGridView;

       

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new MIG_JegyzekekSearch());


        if (!IsPostBack) JegyzekekGridViewBind();

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.ViewHistory);

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.Lock);

        ListHeader1.AtadasEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekTetelAtadas");

        ListHeader1.JegyzekZarolasEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekZarolas");
        ListHeader1.JegyzekVegrehajtasEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekVegrehajtas");

        ListHeader1.JegyzekMegsemmisitesEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekMegsemmisites");

        JegyzekTetelekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "JegyzekTetelekList");

        // a Modify jogot ellenőrizzük a New-ra, mivel itt az új kapcsolatnál az IktatoKonyv.DefaultIrattariTetelszam 
        // módosítása történik
        JegyzekTetelekSubListHeader.NewEnabled = false;
        JegyzekTetelekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekTetel" + CommandName.View);
        JegyzekTetelekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekTetel" + CommandName.Modify); ;
        JegyzekTetelekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekTetel" + CommandName.Invalidate);

        //if (IsPostBack)
        //{
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(JegyzekekGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        //}

        if (String.IsNullOrEmpty(MasterListSelectedRowId))
        {
            Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.JegyzekTetelekSSRS] = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(JegyzekekGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.JegyzekekColumnNames().GetType()).ToCustomString();

            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('JegyzekekSSRS.aspx')";
        }
        else
        {
            if (FunctionRights.GetFunkcioJog(Page, "JegyzekMegsemmisites"))
            {
                try
                {
                    MIG_JegyzekekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();

                    ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                    ExecParam.Record_Id = MasterListSelectedRowId;
                    Result result = service.Get(ExecParam);
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        MIG_Jegyzekek jegyzek = (MIG_Jegyzekek)result.Record;
                        if (jegyzek != null)
                        {
                            if(jegyzek.Tipus != UI.JegyzekTipus.SelejtezesiJegyzek.Value)
                                ListHeader1.JegyzekMegsemmisitesOnClientClick = "alert('Csak selejtezési jegyzék semmisíthető meg!'); return false";
                            else if (jegyzek.Allapot != KodTarak.JEGYZEK_ALLAPOT.Vegrahajtott)
                                ListHeader1.JegyzekMegsemmisitesOnClientClick = "alert('Csak végrehajtott állapotú jegyzék semmisíthető meg!'); return false";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("JegyzekekList.Page_PreRender error:" + ex.Message);
                }
            }
        }
    }

    #endregion


    #region Master List

    protected void JegyzekekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("JegyzekekGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("JegyzekekGridView", ViewState);

        JegyzekekGridViewBind(sortExpression, sortDirection);
    }

    protected void JegyzekekGridViewBind(String SortExpression, SortDirection SortDirection)
    {

        MIG_JegyzekekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        MIG_JegyzekekSearch search = (MIG_JegyzekekSearch)Search.GetSearchObject(Page, new MIG_JegyzekekSearch());
        search.OrderBy = Search.GetOrderBy("JegyzekekGridView", ViewState, SortExpression, SortDirection);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(JegyzekekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void JegyzekekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.JegyzekTipus.SetGridViewRow(e, "labelJegyzekTipus");
        UI.SetGridViewDateTime(e, "labelLetrehozasDate");
        UI.SetGridViewDateTime(e, "labelLezarasDate");
        UI.SetGridViewDateTime(e, "labelVegrehajtasDate");
        UI.SetGridViewDateTime(e, "labelSztornoDate");
        UI.SetGridViewDateTime(e, "labelMegsemmisitesDatuma");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void JegyzekekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = JegyzekekGridView.PageIndex;

        JegyzekekGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = JegyzekekGridView.PageCount;

        if (prev_PageIndex != JegyzekekGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, JegyzekekCPE);
            JegyzekekGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, JegyzekekCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(JegyzekekGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(JegyzekekGridView);
    }

    private void GridView_RowDataBound_DisableSzereltCheckbox(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            bool isSzerelt = false;

            if (drw.Row.Table.Columns.Contains("Csatolva_Id"))
            {
                isSzerelt |= !String.IsNullOrEmpty(drw["Csatolva_Id"].ToString());
            }

            if (drw.Row.Table.Columns.Contains("Edok_Utoirat_Id"))
            {
                isSzerelt |= !String.IsNullOrEmpty(drw["Edok_Utoirat_Id"].ToString());
            }

            CheckBox check = (CheckBox)e.Row.FindControl("check");

            if (check != null)
            {
                if (isSzerelt)
                {
                    check.Enabled = false;
                }
            }
        }
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        JegyzekekGridViewBind();
    }

    protected void JegyzekekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(JegyzekekGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);

            //JegyzekekGridView_SelectRowCommand(id);
        }
    }

    //private void JegyzekekGridView_SelectRowCommand(string felhasznaloId)
    //{
    //    //ActiveTabRefresh(TabContainer1, felhasznaloId);
    //}

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("JegyzekekForm.aspx"
             , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
             , Defaults.PopupWidth, Defaults.PopupHeight, JegyzekekUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("JegyzekekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, JegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "MIG_Jegyzekek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, JegyzekekUpdatePanel.ClientID);

            //JegyzekTetelekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("Jegyzekek_IktatoKonyvek_Form.aspx"
            //    , "Command=" + CommandName.New + "&" + QueryStringVars.IrattariTetelId + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, JegyzekTetelekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            string column_v = "";
            for (int i = 0; i < JegyzekekGridView.Columns.Count; i++)
            {
                if (JegyzekekGridView.Columns[i].Visible)
                {
                    column_v = column_v + "1";
                }
                else
                {
                    column_v = column_v + "0";
                }
            }

            Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.JegyzekTetelekSSRS] = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(JegyzekekGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.JegyzekekColumnNames().GetType()).ToCustomString();

            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('JegyzekTetelekSSRS.aspx?" + QueryStringVars.Id + "=" + id +
                //"&" + QueryStringVars.OrderBy + "=" + GetOrderBy(JegyzekTetelekGridView.ID)
                "&" + "OrderBy" + "=" + GetOrderBy(JegyzekTetelekGridView.ID)
                + "')";

            ListHeader1.SztornoOnClientClick = "if(!confirm('Biztosan sztornózni kívánja a jegyzéket?')) return false;";
            IsSztornoEnabled = true;

            SetMegSemmisitesButtonCommand(id);

            GridViewRow row = JegyzekekGridView.SelectedRow;
            if (row != null)
            {
                bool isTetelInvalidable = true;

                Label labelSztornoDate = (Label)row.FindControl("labelSztornoDate");
                if (labelSztornoDate != null)
                {
                    if (!String.IsNullOrEmpty(labelSztornoDate.Text))
                    {
                        isTetelInvalidable = false;
                        ListHeader1.JegyzekZarolasOnClientClick = "alert('A jegyzék nem zárható le, mert sztornózott!'); return false;";
                        ListHeader1.JegyzekVegrehajtasOnClientClick = "alert('A jegyzék nem hajtható végre, mert sztornózott!'); return false;";
                        ListHeader1.SztornoOnClientClick = "alert('A jegyzék már sztornózva van.');return false;";
                        ListHeader1.ModifyOnClientClick = "alert('A jegyzék nem módosítható, mert sztornózott!'); return false;";
                        IsSztornoEnabled = false;
                        return;
                    }
                }
                bool isZarolva = false;
                Label labelLezarasDate = (Label)row.FindControl("labelLezarasDate");
                if (labelLezarasDate != null)
                {
                    string date = Server.HtmlDecode(labelLezarasDate.Text).Trim();
                    if (String.IsNullOrEmpty(date))
                    {
                        ListHeader1.JegyzekZarolasOnClientClick = "if(confirm('Biztosan zárolja a jegyzéket?') == true) return true; else return false;";
                        ListHeader1.JegyzekVegrehajtasOnClientClick = "alert('A jegyzék nem hajtható végre, mert nincs zárolva!'); return false;";
                        ListHeader1.AtadasOnClientClick = "alert('A jegyzék nem adható át, mert nincs zárolva!'); return false;";
                        //ListHeader1.AtadasEnabled = false;
                    }
                    else
                    {
                        ListHeader1.AtadasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                 + JegyzekTetelekGridView.ClientID + "','check'); "
                 + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
                        ListHeader1.JegyzekZarolasOnClientClick = "alert('A jegyzék már zárolva van.'); return false;";
                        isZarolva = true;
                        ListHeader1.AtadasEnabled = true;

                    }
                }

                if (isZarolva)
                {
                    isTetelInvalidable = false;
                    Label labelAllapot = row.FindControl("labelAllapot") as Label;

                    if (labelAllapot != null
                        && labelAllapot.Text == KodTarak.JEGYZEK_ALLAPOT.VegrehajtasFolyamatban)
                    {
                        ListHeader1.JegyzekVegrehajtasOnClientClick = "alert('A jegyzék végrehajtása folyamatban van.'); return false;";
                        ListHeader1.SztornoOnClientClick = "alert('A jegyzék nem sztornózható, mert a végrehajtása folyamatban van!');return false;";
                        IsSztornoEnabled = false;
                    }
                    else
                    {


                        Label labelVegrehajtasDate = (Label)row.FindControl("labelVegrehajtasDate");

                        if (labelVegrehajtasDate != null)
                        {
                            string date = Server.HtmlDecode(labelVegrehajtasDate.Text).Trim();
                            if (String.IsNullOrEmpty(date))
                            {
                                ListHeader1.JegyzekVegrehajtasOnClientClick = JavaScripts.SetOnClientClick("JegyzekekVegrehajtasForm.aspx", QueryStringVars.Id + "=" + id
                               , Defaults.PopupWidth, Defaults.PopupHeight, JegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                            }
                            else
                            {
                                ListHeader1.JegyzekVegrehajtasOnClientClick = "alert('A jegyzék már végre van hajtva.'); return false;";
                                ListHeader1.SztornoOnClientClick = "alert('A jegyzék nem sztornózható, mert már végre van hajtva!');return false;";
                                IsSztornoEnabled = false;
                            }
                        }
                    }
                }

                JegyzekTetelekSubListHeader.InvalidateEnabled = isTetelInvalidable;

                Label labelMegsemmisitesDatuma = (Label)row.FindControl("labelMegsemmisitesDatuma");

                if (!String.IsNullOrEmpty(labelMegsemmisitesDatuma.Text))
                {
                    ListHeader1.JegyzekMegsemmisitesOnClientClick = "alert('A jegyzék már megsemmisítésre került!'); return false;";
                }
            }
        }
    }

    // jegyzék tételek rendezési sorrendjének visszaolvasása a viewstate-ből (Search.GetOrderBy "visszaforgatása")
    private string GetOrderBy(String GridViewName)
    {
        string _ret = "";
        if (!String.IsNullOrEmpty(GridViewName))
        {
            String SortExpression = ViewState[GridViewName + "SortExpression"] as String;
            SortDirection SortDirection = (ViewState[GridViewName + "SortDirection"] != null) ? (SortDirection)ViewState[GridViewName + "SortDirection"] : SortDirection.Ascending;

            if (SortExpression == null) return _ret;

            if (SortExpression != "")
            {
                _ret = SortExpression;

                // ha [FullSortExpression] stringgel kezdődik a kifejezés, akkor ez már tartalmazza a rendezési irányokat is,
                // nem kell azt hozzácsapni (mivel lehet hogy több oszlopot is megadtunk, mindegyikhez külön rendezési iránnyal)
                if (_ret.StartsWith(Search.FullSortExpression))
                {
                    // [FullSortExpression] string levágása:
                    _ret = _ret.Remove(0, Search.FullSortExpression.Length);
                }
                else
                {
                    switch (SortDirection)
                    {
                        case SortDirection.Ascending:
                            _ret = String.Format(_ret, "ASC");  // ha több mezőből áll a rendezés, megadható {0} is az egyes mezőkhöz
                            _ret += " ASC";
                            break;
                        case SortDirection.Descending:
                            _ret = String.Format(_ret, "DESC");  // ha több mezőből áll a rendezés, megadható {0} is az egyes mezőkhöz
                            _ret += " DESC";
                            break;
                    }
                }
            }
        }
        return _ret;
    }

    protected void JegyzekekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    JegyzekekGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(JegyzekekGridView));
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedJegyzekek();
            JegyzekekGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedJegyzekekRecords();
                JegyzekekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedJegyzekekRecords();
                JegyzekekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedJegyzekek();
                break;
            case CommandName.JegyzekZarolas:
                ZarolSelectedJegyzek();
                JegyzekekGridViewBind();
                break;
            case CommandName.JegyzekVegrehajtas:
                VegrehajtSelectedJegyzek();
                JegyzekekGridViewBind();
                break;
            case CommandName.Sztorno:
                SztornozSelectedJegyzek();
                JegyzekekGridViewBind();
                break;
            case CommandName.Atadas:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(JegyzekTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (lstGridViewSelectedRows.Count > 0)
                    {
                        Session[Constants.SelectedJegyzekTetelIds] = string.Join(",", lstGridViewSelectedRows.ToArray());

                        string js = JavaScripts.SetOnClientClickWithTimeout("TomegesAtadas.aspx", String.Empty, Defaults.PopupWidth_Max, Defaults.PopupHeight, JegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "TomegesAtadasScript", js, true);
                    }
                }
                break;
        }
    }


    private void ZarolSelectedJegyzek()
    {
        string id = UI.GetGridViewSelectedRecordId(JegyzekekGridView);

        if (!String.IsNullOrEmpty(id))
        {
            MIG_JegyzekekService svc = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            xpm.Record_Id = id;

            Result res = svc.JegyzekLezarasa(xpm);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                ErrorUpdatePanel.Update();
                EErrorPanel1.Visible = true;
            }
        }
        
    }

    private void VegrehajtSelectedJegyzek()
    {
        string id = UI.GetGridViewSelectedRecordId(JegyzekekGridView);
        string leveltariAtvevo = String.Empty;

        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            leveltariAtvevo =  Request.Params["__EVENTARGUMENT"].ToString();
        }

        if (!String.IsNullOrEmpty(id))
        {
            MIG_JegyzekekService svc = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            xpm.Record_Id = id;

            Result res = svc.JegyzekVegrehajtasa(xpm, leveltariAtvevo, false);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                ErrorUpdatePanel.Update();
                EErrorPanel1.Visible = true;
            }
        }
    }

    private void SztornozSelectedJegyzek()
    {
        string id = UI.GetGridViewSelectedRecordId(JegyzekekGridView);

        if (!String.IsNullOrEmpty(id))
        {
            MIG_JegyzekekService svc = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            xpm.Record_Id = id;

            Result res = svc.JegyzekSztornozas(xpm);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                ErrorUpdatePanel.Update();
                EErrorPanel1.Visible = true;
            }
        }

    }

    private void LockSelectedJegyzekekRecords()
    {
        LockManager.LockSelectedGridViewRecords(JegyzekekGridView, "MIG_Jegyzekek"
            , "JegyzekLock", "JegyzekForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedJegyzekekRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(JegyzekekGridView, "MIG_Jegyzekek"
            , "JegyzekLock", "JegyzekForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Törli a JegyzekekGridView -ban kijelölt elemeket
    /// </summary>
    private void deleteSelectedJegyzekek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "JegyzekInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(JegyzekekGridView, EErrorPanel1, ErrorUpdatePanel);
            MIG_JegyzekekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
        }
    }

    /// <summary>
    /// Elkuldi emailben a IraErkeztetoKonyvekGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedJegyzekek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(JegyzekekGridView, EErrorPanel1, ErrorUpdatePanel)
                , UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "MIG_Jegyzekek");
        }
    }

    protected void JegyzekekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        JegyzekekGridViewBind(e.SortExpression, UI.GetSortToGridView("JegyzekekGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    private void SetMegSemmisitesButtonCommand(string id)
    {
        //ListHeader1.MegsJegyzNyomtatasOnClientClick = 
        ListHeader1.JegyzekMegsemmisitesOnClientClick =
        JavaScripts.SetOnClientClick("JegyzekekMegsemmisites.aspx"
            , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            , Defaults.PopupWidth, Defaults.PopupHeight, JegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
    }

    #endregion


    #region Detail Tab

    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(JegyzekekGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (JegyzekekGridView.SelectedIndex == -1)
        {
            return;
        }
        string irattariTetelId = UI.GetGridViewSelectedRecordId(JegyzekekGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, irattariTetelId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string irattariTetelId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                JegyzekTetelekGridViewBind(irattariTetelId);
                JegyzekTetelekPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(JegyzekTetelekGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        JegyzekTetelekSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(JegyzekTetelekTabPanel))
        {
            JegyzekTetelekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(JegyzekTetelekGridView));
        }
    }

    #endregion


    #region JegyzekTetelek Detail

    private void JegyzekTetelekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedJegyzekTetelek();
            JegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(JegyzekekGridView));
        }
    }

    protected void JegyzekTetelekGridViewBind(string irattariTetelId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("JegyzekTetelekGridView", ViewState, "Azonosito");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("JegyzekTetelekGridView", ViewState);

        JegyzekTetelekGridViewBind(irattariTetelId, sortExpression, sortDirection);
    }

    protected void JegyzekTetelekGridViewBind(string irattariTetelId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(irattariTetelId))
        {
            MIG_JegyzekTetelekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekTetelekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            MIG_JegyzekTetelekSearch search = new MIG_JegyzekTetelekSearch();

            search.Jegyzek_Id.Value = irattariTetelId;
            search.Jegyzek_Id.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("JegyzekTetelekGridView", ViewState, SortExpression, SortDirection);

            //JegyzekTetelekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);
            if (JegyzekTetelekSubListHeader.ValidFilterSetting == Component_SubListHeader.ValidityType.Valid)
            {
                search.SztornozasDat.Value = "";
                search.SztornozasDat.Operator = Query.Operators.isnull;
            }
            else if (JegyzekTetelekSubListHeader.ValidFilterSetting == Component_SubListHeader.ValidityType.Invalid)
            {
                search.SztornozasDat.Value = "";
                search.SztornozasDat.Operator = Query.Operators.notnull;
            }

            Result res = service.GetAllWithExtension(ExecParam, search);
            UI.GridViewFill(JegyzekTetelekGridView, res, JegyzekTetelekSubListHeader, EErrorPanel1, ErrorUpdatePanel);

            // Típustól függőn elrejtünk oszlopokat
            MIG_JegyzekekService jegyzekekService = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
            ExecParam jegyzekExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            jegyzekExecParam.Record_Id = irattariTetelId;
            Result iraJegyzekekResult = jegyzekekService.Get(jegyzekExecParam);
            if (iraJegyzekekResult.Record != null && (iraJegyzekekResult.Record as MIG_Jegyzekek).Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value) // "S"
            {
                UI.SetGridViewColumnVisiblity(JegyzekTetelekGridView, "AtvevoIktatoszama", false);
                UI.SetGridViewColumnVisiblity(JegyzekTetelekGridView, "AtadasDatuma", false);
            }
            else
            {
                UI.SetGridViewColumnVisiblity(JegyzekTetelekGridView, "AtvevoIktatoszama", true);
                UI.SetGridViewColumnVisiblity(JegyzekTetelekGridView, "AtadasDatuma", true);
            }

            Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.JegyzekTetelekSSRSTetelek] = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(JegyzekTetelekGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.JegyzekTetelekColumnName().GetType()).ToCustomString();

            UI.SetTabHeaderRowCountText(res, Header1);
        }
        else
        {
            ui.GridViewClear(JegyzekTetelekGridView);
        }
    }


    void JegyzekTetelekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        JegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(JegyzekekGridView));
    }

    protected void JegyzekTetelekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(JegyzekTetelekTabPanel))
                    //{
                    //    JegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(JegyzekTetelekGridView));
                    //}
                    ActiveTabRefreshDetailList(JegyzekTetelekTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// Törli a kijelölt hozzárendeléseket, vagyis beállítja az MIG_JegyzekTetelek.SztornozasDat mezőt (update művelet valójában)
    /// </summary>
    private void deleteSelectedJegyzekTetelek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "JegyzekTetelInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(JegyzekTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
            MIG_JegyzekTetelekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekTetelekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];

            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }

            Result result = service.JegyzekTetelSztornozasTomeges(execParams);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void JegyzekTetelekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(JegyzekTetelekGridView, selectedRowNumber, "check");
        }
    }

    private void JegyzekTetelekGridView_RefreshOnClientClicks(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            // ha nem ellenőrizzük, a SelectedRow értéke kívül eshet a tartományon (exception!)
            if (JegyzekTetelekGridView.Rows.Count > 0 && JegyzekTetelekGridView.SelectedIndex < JegyzekTetelekGridView.Rows.Count)
            {
                if (JegyzekTetelekGridView.SelectedRow != null)
                {
                    Label labelObjId = (Label)JegyzekTetelekGridView.SelectedRow.FindControl("labelObjId");
                    if (labelObjId != null)
                    {
                        JegyzekTetelekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                            , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + labelObjId.Text + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromJegyzekTetel
                            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, JegyzekTetelekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

                    }
                }
            }
            // módosítás: csak a megjegyzés és esetleg az átvevő iktatószáma (Note mező)
            JegyzekTetelekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("JegyzekTetelekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, JegyzekTetelekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }

        int jegyzekTetelekCount = JegyzekTetelekSubListHeader.RecordNumber;
        if (jegyzekTetelekCount > 0 && IsSztornoEnabled)
        {
            ListHeader1.SztornoOnClientClick = "alert('A jegyzék nem sztornózható, mert a jegyzékhez tartoznak tételek!');return false;";
        }
    }

    protected void JegyzekTetelekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = JegyzekTetelekGridView.PageIndex;

        JegyzekTetelekGridView.PageIndex = JegyzekTetelekSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != JegyzekTetelekGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, JegyzekTetelekCPE);
            JegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(JegyzekekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(JegyzekTetelekSubListHeader.Scrollable, JegyzekTetelekCPE);
        }
        JegyzekTetelekSubListHeader.PageCount = JegyzekTetelekGridView.PageCount;
        JegyzekTetelekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(JegyzekTetelekGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(JegyzekTetelekGridView);
    }


    void JegyzekTetelekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(JegyzekTetelekSubListHeader.RowCount);
        JegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(JegyzekekGridView));
    }

    protected void JegyzekTetelekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        JegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(JegyzekekGridView)
            , e.SortExpression, UI.GetSortToGridView("JegyzekTetelekGridView", ViewState, e.SortExpression));
    }

    #endregion
}
