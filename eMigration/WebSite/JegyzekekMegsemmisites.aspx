<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="JegyzekekMegsemmisites.aspx.cs" Inherits="JegyzekekMegsemmisites" Title="Untitled Page" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc23" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %> 
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBoxTextBox" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Jegyz�k megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Nev_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true">
                                </asp:TextBox>
                                <asp:HiddenField ID="hfVegrehajtasDatuma" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label3" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label4" runat="server" Text="Megsemmis�t�s id�pontja:"></asp:Label>
                                </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="MegsemmisitesDatuma" runat="server" Validate="true" />
                            </td>
                        </tr>    
                                                
                    </table>
                    </eUI:eFormPanel>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
        
</asp:Content>

