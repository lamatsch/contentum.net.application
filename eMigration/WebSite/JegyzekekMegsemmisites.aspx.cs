using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Query.BusinessDocuments;

// TELJES K�D KIDOLGOZAND�!

public partial class JegyzekekMegsemmisites : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;


    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "JegyzekMegsemmisites");
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                MIG_JegyzekekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    MIG_Jegyzekek jegyzek = (MIG_Jegyzekek)result.Record;
                    LoadComponentsFromBusinessObject(jegyzek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.FullManualHeaderTitle = "Jegyz�k t�telek megsemmis�t�se";

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private void LoadComponentsFromBusinessObject(MIG_Jegyzekek jegyzek)
    {
        Nev_TextBox.Text = jegyzek.Nev;
        MegsemmisitesDatuma.Text = DateTime.Now.ToString();
        hfVegrehajtasDatuma.Value = jegyzek.VegrehajtasDatuma;

        FormHeader1.Record_Ver = jegyzek.Base.Ver;
    }

    private MIG_Jegyzekek GetBusinessObjectFromComponents()
    {
        MIG_Jegyzekek jegyzek = new MIG_Jegyzekek();

        jegyzek.Updated.SetValueAll(false);
        jegyzek.Base.Updated.SetValueAll(false);

        jegyzek.MegsemmisitesDatuma = MegsemmisitesDatuma.Text;
        jegyzek.Updated.MegsemmisitesDatuma = pageView.GetUpdatedByView(MegsemmisitesDatuma);

        jegyzek.Base.Ver = FormHeader1.Record_Ver;
        jegyzek.Base.Updated.Ver = true;

        return jegyzek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Nev_TextBox);

            FormFooter1.SaveEnabled = false;
        }
    }

    #region BLG 2954 seg�df�ggv�nyek

    Result MegsemmisitFoszamok(MIG_Foszam foszam, String MegsemmisitveId, int currentVersion)
    {
        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

        foszam.Updated.SetValueAll(false);
        foszam.Base.Updated.SetValueAll(false);

        //foszam.Csoport_Id_Felelos = MegsemmisitveId;
        //foszam.Updated.Csoport_Id_Felelos = true;

        //foszam.FelhasznaloCsoport_Id_Orzo = MegsemmisitveId;
        //foszam.Updated.FelhasznaloCsoport_Id_Orzo = true;

        ExecParam epUpdate = UI.SetExecParamDefault(Page, new ExecParam());
        epUpdate.Record_Id = foszam.Id;

        foszam.Base.Ver = currentVersion.ToString();
        foszam.Base.Updated.Ver = true;

        return service.Update(epUpdate, foszam);
    }

    private bool MegsemmisitesBejegyzese(String recordId)
    {
        try
        {
            String MegsemmisitveId = GetMegsemmisitveId();
            if (String.IsNullOrEmpty(MegsemmisitveId))
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "'Megsemmis�tve' Csoport nem tal�lhat�");
                return false;
            }

            MIG_JegyzekTetelekService jegyzekTetelekService = eMigrationService.ServiceFactory.GetMIG_JegyzekTetelekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            MIG_JegyzekTetelekSearch jegyzekTetelekSearch = new MIG_JegyzekTetelekSearch();

            jegyzekTetelekSearch.Jegyzek_Id.Value = recordId;
            jegyzekTetelekSearch.Jegyzek_Id.Operator = Query.Operators.equals;

            Result jegyzekTetelekRes = jegyzekTetelekService.GetAllWithExtension(ExecParam, jegyzekTetelekSearch);

            if (String.IsNullOrEmpty(jegyzekTetelekRes.ErrorCode))
            {
                if (jegyzekTetelekRes.Ds != null && jegyzekTetelekRes.Ds.Tables.Count > 0)
                {
                    Dictionary<String, Boolean> IratIdsDictionary = new Dictionary<String, Boolean>();
                    foreach (System.Data.DataRow row in jegyzekTetelekRes.Ds.Tables[0].Rows)
                    {
                        String foszamId = row["MIG_Foszam_Id"].ToString();
                        if (!String.IsNullOrEmpty(foszamId))
                        {
                            ExecParam execParamSelect = UI.SetExecParamDefault(Page, new ExecParam());
                            execParamSelect.Record_Id = foszamId;


                            MIG_FoszamService foszamService = eMigrationService.ServiceFactory.GetMIG_FoszamService();

                            Result resFoszam = foszamService.Get(execParamSelect);

                            if (String.IsNullOrEmpty(resFoszam.ErrorCode))
                            {
                                MIG_Foszam foszam = (MIG_Foszam)resFoszam.Record;
                                int currentVersion = Int32.Parse(foszam.Base.Ver);
                                Result updateRes = MegsemmisitFoszamok(foszam, MegsemmisitveId, currentVersion);
                                if (!String.IsNullOrEmpty(updateRes.ErrorCode))
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, updateRes);
                                    return false;
                                }

                            }
                        }
                    }

                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, jegyzekTetelekRes);
                return false;
            }
        }
        catch (Exception ex)
        {
            Logger.Error("IraJegyzekekMegsemmisites.FormFooter1ButtonsClick error:" + ex.Message);
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "Hiba a megsemmis�t�s bejegyz�se sor�n:" + ex.Message);
            return false;
        }

        return true;
    }

    private String GetMegsemmisitveId()
    {
        try
        {
            KRT_CsoportokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
            KRT_CsoportokSearch search_csoportok = new KRT_CsoportokSearch();

            search_csoportok.Nev.Value = "Megsemmis�tve";
            search_csoportok.Nev.Operator = Query.Operators.equals;

            Result res = service.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), search_csoportok);
            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                if (res.Ds != null && res.Ds.Tables.Count > 0)
                {
                    System.Data.DataRow row = res.Ds.Tables[0].Rows[0];
                    return row["Id"].ToString();
                }
            }

        }
        catch(Exception ex)
        {
            Logger.Error("IraJegyzekekMegsemmisites.GetMegsemmisitveId error:" + ex.Message);
        }
        return String.Empty;
    }
    #endregion


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "JegyzekMegsemmisites"))
            {
                switch (Command)
                {
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                //if (MegsemmisitesBejegyzese(recordId))
                                //{
                                    MIG_JegyzekekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
                                    MIG_Jegyzekek jegyzekek = GetBusinessObjectFromComponents();

                                    if (!CheckDatum(jegyzekek))
                                        return;

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    Result result = service.JegyzekMegsemmisitese(execParam, jegyzekek);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }
                                //}
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    bool CheckDatum(MIG_Jegyzekek jegyzek)
    {
        string vegrehajtasDatuma = hfVegrehajtasDatuma.Value;

        if (!String.IsNullOrEmpty(vegrehajtasDatuma))
        {
            DateTime dtVegrehajtasDatuma;

            if (DateTime.TryParse(vegrehajtasDatuma, out dtVegrehajtasDatuma))
            {
                if (!jegyzek.Typed.MegsemmisitesDatuma.IsNull)
                {
                    if (jegyzek.Typed.MegsemmisitesDatuma.Value.Date < dtVegrehajtasDatuma.Date)
                    {
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", "A megsemmis�t�s id�pontja nem lehet kor�bbi, mint a selejtez�s id�pontja!");
                        return false;
                    }
                }
            }
        }

        return true;
    }

}
