using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eMigration.Query.BusinessDocuments;

// a teljes kód kidolgozandó!!!

public partial class JegyzekekSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(MIG_JegyzekekSearch);
    //private const string kcsKod_IRATTARI_JEL = "IRATTARI_JEL";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.IraJegyzekekSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            MIG_JegyzekekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (MIG_JegyzekekSearch)Search.GetSearchObject(Page, new MIG_JegyzekekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        MIG_JegyzekekSearch mig_JegyzekekSearch = null;
        if (searchObject != null) mig_JegyzekekSearch = (MIG_JegyzekekSearch)searchObject;

        if (mig_JegyzekekSearch != null)
        {

            UI.JegyzekTipus.FillDropDownListWithEmptyValue(TipusDropDownList);
            //TipusDropDownList.Items.Add(new ListItem("[Nincs kiválasztva]", ""));
            TipusDropDownList.SelectedValue = mig_JegyzekekSearch.Tipus.Value;

            JegyzekMegnevezes_TextBox.Text = mig_JegyzekekSearch.Nev.Value;

            Felelos_CsoportTextBox.Id_HiddenField = mig_JegyzekekSearch.Csoport_Id_Felelos.Value;
            Felelos_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            Vegrehajto_CsoportTextBox.Id_HiddenField = mig_JegyzekekSearch.FelhasznaloCsoport_Id_Vegrehaj.Value;
            Vegrehajto_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            //Atvevo_PartnerTextBox.Id_HiddenField = erec_JegyzekekSearch.Partner_Id_LeveltariAtvevo.Value;
            //Atvevo_PartnerTextBox.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);
            textAtvevoellenor.Text = mig_JegyzekekSearch.Atvevo_Nev.Value;

            rbListAllapot.SelectedValue = "Osszes";
            if (mig_JegyzekekSearch.LezarasDatuma.Operator == Query.Operators.notnull)
            {
                rbListAllapot.SelectedValue = "Lezart";
            }

            if (mig_JegyzekekSearch.VegrehajtasDatuma.Operator == Query.Operators.notnull)
            {
                rbListAllapot.SelectedValue = "Vegrehajtott";
            }

            if (mig_JegyzekekSearch.SztornozasDat.Operator == Query.Operators.notnull)
            {
                rbListAllapot.SelectedValue = "Sztornozott";
            }

            if (mig_JegyzekekSearch.LezarasDatuma.Operator == Query.Operators.isnull &&
               mig_JegyzekekSearch.VegrehajtasDatuma.Operator == Query.Operators.isnull &&
               mig_JegyzekekSearch.SztornozasDat.Operator == Query.Operators.isnull)
            {
                rbListAllapot.SelectedValue = "Nyitott";
            }

            if (mig_JegyzekekSearch.Allapot.Value == KodTarak.JEGYZEK_ALLAPOT.VegrehajtasFolyamatban)
            {
                rbListAllapot.SelectedValue = "VegrehajtasFolyamatban";
            }

            DatumIntervallum_SearchCalendarControl2.SetComponentFromSearchObjectFields(mig_JegyzekekSearch.LezarasDatuma);

            DatumIntervallum_SearchCalendarControl1.SetComponentFromSearchObjectFields(mig_JegyzekekSearch.VegrehajtasDatuma);

            DatumIntervallum_SearchCalendarControlSztornozas.SetComponentFromSearchObjectFields(mig_JegyzekekSearch.SztornozasDat);

            CheckBox_MegsemmisitesreVar.Checked = mig_JegyzekekSearch.MegsemmisitesDatuma.Operator == Query.Operators.isnull;

            DatumIntervallum_SearchCalendarControl_Megsemmisites.SetComponentFromSearchObjectFields(mig_JegyzekekSearch.MegsemmisitesDatuma);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private MIG_JegyzekekSearch SetSearchObjectFromComponents()
    {
        Contentum.eMigration.Query.BusinessDocuments.MIG_JegyzekekSearch mig_JegyzekekSearch = (MIG_JegyzekekSearch)SearchHeader1.TemplateObject;
        if (mig_JegyzekekSearch == null)
        {
            mig_JegyzekekSearch = new MIG_JegyzekekSearch();
        }

        if (!String.IsNullOrEmpty(TipusDropDownList.SelectedValue))
        {
            mig_JegyzekekSearch.Tipus.Value = TipusDropDownList.SelectedValue;
            mig_JegyzekekSearch.Tipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(JegyzekMegnevezes_TextBox.Text))
        {
            mig_JegyzekekSearch.Nev.Value = JegyzekMegnevezes_TextBox.Text;
            mig_JegyzekekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(JegyzekMegnevezes_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Felelos_CsoportTextBox.Id_HiddenField))
        {
            mig_JegyzekekSearch.Csoport_Id_Felelos.Value = Felelos_CsoportTextBox.Id_HiddenField;
            mig_JegyzekekSearch.Csoport_Id_Felelos.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(Vegrehajto_CsoportTextBox.Id_HiddenField))
        {
            mig_JegyzekekSearch.FelhasznaloCsoport_Id_Vegrehaj.Value = Vegrehajto_CsoportTextBox.Id_HiddenField;
            mig_JegyzekekSearch.FelhasznaloCsoport_Id_Vegrehaj.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(textAtvevoellenor.Text))
        {
            mig_JegyzekekSearch.Atvevo_Nev.Value = textAtvevoellenor.Text;
            mig_JegyzekekSearch.Atvevo_Nev.Operator = Search.GetOperatorByLikeCharater(textAtvevoellenor.Text);
        }


        switch (rbListAllapot.SelectedValue)
        {
            case "Osszes":
                DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(mig_JegyzekekSearch.VegrehajtasDatuma);
                DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(mig_JegyzekekSearch.LezarasDatuma);
                DatumIntervallum_SearchCalendarControlSztornozas.SetSearchObjectFields(mig_JegyzekekSearch.SztornozasDat);
                break;
            case "Nyitott":
                mig_JegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.Nyitott;
                mig_JegyzekekSearch.Allapot.Operator = Query.Operators.equals;
                mig_JegyzekekSearch.LezarasDatuma.Operator = Query.Operators.isnull;
                mig_JegyzekekSearch.VegrehajtasDatuma.Operator = Query.Operators.isnull;
                mig_JegyzekekSearch.SztornozasDat.Operator = Query.Operators.isnull;
                DatumIntervallum_SearchCalendarControl1.Clear();
                DatumIntervallum_SearchCalendarControl2.Clear();
                DatumIntervallum_SearchCalendarControlSztornozas.Clear();
                break;
            case "Lezart":
                mig_JegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.Lezart;
                mig_JegyzekekSearch.Allapot.Operator = Query.Operators.equals;
                mig_JegyzekekSearch.LezarasDatuma.Operator = Query.Operators.notnull;
                DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(mig_JegyzekekSearch.VegrehajtasDatuma);
                DatumIntervallum_SearchCalendarControl2.Clear();
                DatumIntervallum_SearchCalendarControlSztornozas.SetSearchObjectFields(mig_JegyzekekSearch.SztornozasDat);
                break;
            case "Vegrehajtott":
                mig_JegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.Vegrahajtott;
                mig_JegyzekekSearch.Allapot.Operator = Query.Operators.equals;
                mig_JegyzekekSearch.VegrehajtasDatuma.Operator = Query.Operators.notnull;
                DatumIntervallum_SearchCalendarControl1.Clear();
                DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(mig_JegyzekekSearch.LezarasDatuma);
                DatumIntervallum_SearchCalendarControlSztornozas.Clear();
                break;
            case "Sztornozott":
                mig_JegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.Sztornozott;
                mig_JegyzekekSearch.Allapot.Operator = Query.Operators.equals;
                mig_JegyzekekSearch.SztornozasDat.Operator = Query.Operators.notnull;
                DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(mig_JegyzekekSearch.LezarasDatuma);
                DatumIntervallum_SearchCalendarControlSztornozas.Clear();
                DatumIntervallum_SearchCalendarControl2.Clear();
                break;
            case "VegrehajtasFolyamatban":
                mig_JegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.VegrehajtasFolyamatban;
                mig_JegyzekekSearch.Allapot.Operator = Query.Operators.equals;
                DatumIntervallum_SearchCalendarControl1.Clear();
                DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(mig_JegyzekekSearch.LezarasDatuma);
                DatumIntervallum_SearchCalendarControlSztornozas.Clear();
                break;
        }

        if (CheckBox_MegsemmisitesreVar.Checked)
        {
            mig_JegyzekekSearch.MegsemmisitesDatuma.Operator = Query.Operators.isnull;
            DatumIntervallum_SearchCalendarControl_Megsemmisites.Clear();
        }
        else
        {
            DatumIntervallum_SearchCalendarControl_Megsemmisites.SetSearchObjectFields(mig_JegyzekekSearch.MegsemmisitesDatuma);
        }

        return mig_JegyzekekSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            MIG_JegyzekekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                //default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private MIG_JegyzekekSearch GetDefaultSearchObject()
    {
        return new MIG_JegyzekekSearch();
    }

}
