﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Contentum.eMigration.Utility;
using Contentum.eMigration.Service;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query;

public partial class JegyzekekVegrehajtasForm : System.Web.UI.Page
{
    private string Id = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        Id = Request.QueryString.Get(QueryStringVars.Id);
        if (String.IsNullOrEmpty(Id))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
        else
        {

            MIG_JegyzekekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                MIG_Jegyzekek jegyzek = (MIG_Jegyzekek)result.Record;
                LoadComponentsFromBusinessObject(jegyzek);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }

        FormFooter1.Command = CommandName.Modify;
    }

    private void LoadComponentsFromBusinessObject(MIG_Jegyzekek jegyzek)
    {
        Nev_TextBox.Text = jegyzek.Nev;

        if (jegyzek.Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value)
        {
            LeveltariAtvevoLabel.Text = "Ellenőr:";
        }
        else if (jegyzek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value)
        {
            LeveltariAtvevoLabel.Text = "Átvevő:";
        }

        hfTipus.Value = jegyzek.Tipus;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.FullManualHeaderTitle = "Jegyzék végrehajtása";

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            Result resultVegrahajtas = JegyzekVegrehajtas();

            if (resultVegrahajtas.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultVegrahajtas);
            }
            else
            {
                JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
            }
        }
    }

    private Result JegyzekVegrehajtas()
    {
        MIG_JegyzekekService svc = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
        ExecParam xpm = UI.SetExecParamDefault(Page);
        xpm.Record_Id = Id;

        Result res = svc.JegyzekVegrehajtasa(xpm, GetAtvevo(), cbAszinkron.Checked);

        return res;
    }

    string GetAtvevo()
    {
        return LeveltariAtvevo.Text;
    }
}