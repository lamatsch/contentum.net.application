﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
//using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;
using Contentum.eUtility;

public partial class MigrUgyiratpotloLapPrintForm : Contentum.eUtility.UI.PageBase
{
   // private string fejId = String.Empty;
    private string ugyiratIds = String.Empty;
   // private string proba = "";

    private string executor = String.Empty;
    private string szervezet = String.Empty;


    protected void Page_Init(object sender, EventArgs e)
    {
      //  fejId = Request.QueryString.Get("fejId");
        ugyiratIds = Request.QueryString.Get("UgyiratId");

       
    }




    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

          ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;






        if (rpis != null)
        {
            if (rpis.Count > 0)

            {
                //EREC_UgyUgyiratokService ugyservice = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                //EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

                //erec_UgyUgyiratokSearch.Id.Value = ugyiratIds;
                //erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;
                //erec_UgyUgyiratokSearch.WhereByManual = " and EREC_IrattariKikero.Allapot = '04' ";

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                executor = execParam.LoginUser_Id;
                szervezet = execParam.Org_Id;


                execParam.Fake = true;

                //Result result = ugyservice.GetAllWithExtensionForUgyiratpotlo(execParam, erec_UgyUgyiratokSearch);

                ReportParameters = new ReportParameter[rpis.Count];

               // if (result.Ds.Tables[0].Rows.Count > 0)
               // {
                    

                    for (int i = 0; i < rpis.Count; i++)
                    {


                        ReportParameters[i] = new ReportParameter(rpis[i].Name);

                        switch (rpis[i].Name)
                        {
                            case "OrderBy":

                                //  ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Foszam_Merge"));

                                break;

                            case "Where":
                                if (!string.IsNullOrEmpty(ugyiratIds))
                                {
                                   // ReportParameters[i].Values.Add(" ( Mig_Foszam.id in ( '" + ugyiratIds + "') ) ");
                                }
                                break;


                            case "TopRow":
                                //  ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                                break;

                            case "ExecutorUserId":
                                if (!string.IsNullOrEmpty(executor))
                                {
                                    ReportParameters[i].Values.Add(executor);
                                }
                                break;


                            case "FelhasznaloSzervezet_Id":
                                if (!string.IsNullOrEmpty(szervezet))
                                {
                                    ReportParameters[i].Values.Add(szervezet);
                                }
                                break;

                        case "UgyiratIds":
                            if (!string.IsNullOrEmpty(ugyiratIds))
                            {
                                ReportParameters[i].Values.AddRange(ugyiratIds.Split(','));
                            }
                            break;





                    }
                    }

               // }

            //    else
             //   {
                    // Response.Write("ooo");

            //    }


            }
        }
        return ReportParameters;
    }


}