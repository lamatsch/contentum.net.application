﻿using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class PrintFormMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {

        Response.CacheControl = "no-cache";

        if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
        {
            Response.Redirect("Login.aspx", true);
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
