using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Data;

public partial class RecordHistory : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();

    string GetFunkcioKod()
    {
        string tableName = Request.QueryString.Get(QueryStringVars.TableName);

        if (tableName == "MIG_Jegyzekek")
            return "JegyzekViewHistory";

        KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        string MuveletKod_ViewHistory = "ViewHistory";

        Result result = service.GetByTableAndOperation(execParam, tableName, MuveletKod_ViewHistory);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);
            UI.RedirectToErrorPageByErrorMsg(Page, resultError.ErrorMessage);
        }
        else
        {
            KRT_Funkciok funkcio = (KRT_Funkciok)result.Record;
            String funkcioKod = funkcio.Kod;
            return funkcioKod;
        }

        return null;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        /// Jogosultságellenőrzés:

        string funkcioKod = GetFunkcioKod();

        if(!String.IsNullOrEmpty(funkcioKod))
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcioKod);
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        //HistoryListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("HistorySearch.aspx", ""
        //    , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HistoryListHeader1.HeaderLabel = Resources.List.RecordHistoryHeaderTitle;

        HistoryListHeader1.SearchEnabled = false;
        HistoryListHeader1.ViewEnabled = false;
        HistoryListHeader1.ExportEnabled = false;
        HistoryListHeader1.PrintEnabled = false;

        HistoryGridView.RowDataBound += new GridViewRowEventHandler(HistoryGridView_RowDataBound);

        if (!IsPostBack) HistoryGridViewBind();
                
    }

    void HistoryGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            if (drv != null)
            {
                if (drv["ColumnName"] != null)
                {
                    string colName = drv["ColumnName"].ToString();
                    if (colName == "UGYHOL")
                    {
                        string oldValue = (drv["OldValue"] ?? String.Empty).ToString();
                        oldValue = Contentum.eMigration.Utility.IratHelye.GetTextFormKod(oldValue,Page);
                        string newValue = (drv["NewValue"] ?? String.Empty).ToString();
                        newValue = Contentum.eMigration.Utility.IratHelye.GetTextFormKod(newValue,Page);
                        TableCell cellOldValue = GetGridViewCell(e.Row, "OldValue");
                        if (cellOldValue != null)
                        {
                            cellOldValue.Text = oldValue;
                        }
                        TableCell cellNewValue = GetGridViewCell(e.Row, "NewValue");
                        if (cellNewValue != null)
                        {
                            cellNewValue.Text = newValue;
                        }
                    }
                }

                FormatDateTime(drv, e.Row, "OldValue");
                FormatDateTime(drv, e.Row, "NewValue");
            }
        }
    }

    private void FormatDateTime(DataRowView drv, GridViewRow gridViewRow, string DataField)
    {
        if (drv == null || drv[DataField] == null)
            return;

        string value = drv[DataField].ToString();

        if (String.IsNullOrEmpty(value))
            return;

        DateTime dt;

        if(DateTime.TryParse(value, out dt))
        {
            value = dt.ToString();
            TableCell cell = GetGridViewCell(gridViewRow, DataField);
            if (cell != null)
            {
                cell.Text = value;
            }
        }
    }

    TableCell GetGridViewCell(GridViewRow row, string DataField)
    {
        foreach (TableCell cell in row.Cells)
        {
            DataControlFieldCell fieldCell = cell as DataControlFieldCell;
            BoundField boundField = fieldCell.ContainingField as BoundField;
            if (boundField != null)
            {
                if (boundField.DataField == DataField)
                {
                    return cell;
                }
            }
        }

        return null;
    }

    protected void HistoryGridViewBind()
    {
        string recordId = Request.QueryString.Get(QueryStringVars.Id);
        string tableName = Request.QueryString.Get(QueryStringVars.TableName);

        if (!String.IsNullOrEmpty(recordId) && !String.IsNullOrEmpty(tableName))
        {
            Contentum.eMigration.Service.RecordHistoryService service = Contentum.eMigration.Utility.eMigrationService.ServiceFactory.GetRecordHistoryService();

            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            ExecParam.Record_Id = recordId;
            Result res = service.GetAllByRecord(ExecParam, tableName);

            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                return;
            }

            if (res.Ds != null)
            {
                ui.GridViewFill(HistoryGridView, res, EErrorPanel1, ErrorUpdatePanel);
            }
        }
        else
        {
            ResultError.DisplayNoIdParamError(EErrorPanel1);
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        
    }
    protected void HistoryGridView_PreRender(object sender, EventArgs e)
    {        
        int prev_PageIndex = HistoryGridView.PageIndex;

        HistoryGridView.PageIndex = HistoryListHeader1.PageIndex;
        HistoryListHeader1.PageCount = HistoryGridView.PageCount;

        if (prev_PageIndex != HistoryGridView.PageIndex)
        {
            HistoryGridViewBind();
            UI.ClearGridViewRowSelection(HistoryGridView);
            //ActiveTabClear();
        }

        HistoryListHeader1.PagerLabel = (HistoryGridView.PageIndex + 1).ToString() + "/" + HistoryGridView.PageCount.ToString();
    }

    protected void HistoryGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
}
