﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="SelejtezesForm.aspx.cs" Inherits="SelejtezesForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="~/eMigrationComponent/IrattariJelTextBox.ascx" TagName="IrattariJelTextBox" TagPrefix="uc"%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true"> 
    </asp:ScriptManager>
    <div>
    <asp:UpdatePanel runat="server" ID="updatePanelFormHeader">
    <ContentTemplate>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="" />
    </ContentTemplate>    
    </asp:UpdatePanel>
    </div>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                <asp:UpdatePanel runat="server" ID="updatePanelMain">
                <ContentTemplate>
                    <eUI:eFormPanel ID="panelSelejtezes" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="labelEv" runat="server" Text="Év:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc4:RequiredNumberBox ID="textEv" runat="server" Validate="true" CssClass="mrUrlapInput"/>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="label2" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="labelIrattariJel" runat="server" Text="Irattári jel:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc:IrattariJelTextBox ID="textIrattariJel" runat="server" Validate="true" EvTextBoxControlID="textEv"/>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelIratHelye" runat="server" Text="Irat helye:" CssClass="mrUrlapInputWaterMarked"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textIratHelye" runat="server" ReadOnly="true" CssClass="mrUrlapInput ReadOnlyWebControl"/>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                         <asp:Label ID="labelNemSzerelt" runat="server" Text="Nem szerelt" style="font-weight:bold" CssClass="mrUrlapInputWaterMarked"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <eUI:eFormPanel ID="panelResult" runat="server" Visible="false" CssClass="mrResultPanel">
                        <div class="mrResultPanelText">A <asp:Label CssClass="mrResultPanelText" ID="labelMuvelet" runat="server" Text="művelet"/> sikeresen végrehajtódott</div>                  
				        <table cellspacing="0" cellpadding="0" width="100%" class="mrResultTable">
			             <tr class="urlapSorBigSize">
	    		            <td style="text-align:center;padding-bottom:10px;" colspan="2">
	    		                <asp:Label runat="server" ID="labelRecordNumberTitle" Text="A tételek száma:" style="font-size:14pt;padding-right:4px;"></asp:Label>
	    		                <asp:Label runat="server" ID="labelRecordNumber" style="font-size:14pt;color:firebrick;"></asp:Label>
                             </td>
			             </tr>
			             <tr class="urlapSorBigSize">
				            <td colspan="2">
				                <asp:Label ID="labelFilterTitle" runat="server" style="font-size:13pt;padding-right:4px;font-weight:bold">Szűrési feltétel:</asp:Label>
				                <asp:Label ID="labelFilter" runat="server" style="font-size:13pt;"></asp:Label>
				            </td>
				        </tr>
			            </table>
				    </eUI:eFormPanel>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImageSaveAndNew" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_uj.png"
                                    onmouseover="swapByName(this.id,'rendben_es_uj2.png')" onmouseout="swapByName(this.id,'rendben_es_uj.png')"
                                    CommandName="SaveAndNew" HAlign="right" Visible="false"
                                    CausesValidation="false" onclick="ImageSaveAndNew_Click" />
                            </td>
                            <td>
                                <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
                            </td>
                        </tr>
                    </table>    
                </ContentTemplate>    
                </asp:UpdatePanel>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>