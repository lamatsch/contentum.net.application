﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eMigration.Utility;
using Contentum.eMigration.Service;
using Contentum.eBusinessDocuments;

public partial class SelejtezesForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FoszamSelejtezes");
        Command = Request.QueryString.Get(CommandName.Command);
        FormHeader1.DesignViewVisible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        switch (Command)
        {
            case CommandName.SelejtezesreKijeloles:
                FormHeader1.FullManualHeaderTitle = "Selejtezésre kijelölés";
                textIratHelye.Text = IratHelye.Irattarban.Text;
                break;
            case CommandName.SelejtezesreKijelolesVisszavonasa:
                FormHeader1.FullManualHeaderTitle = "Selejtezésre kijelölés visszavonása";
                textIratHelye.Text = IratHelye.SelejtezesreVar.Text;
                break;
            case CommandName.Selejtezes:
                FormHeader1.FullManualHeaderTitle = "Selejtezés";
                textIratHelye.Text = IratHelye.SelejtezesreVar.Text;
                break;
            default:
                FormHeader1.FullManualHeaderTitle = String.Empty;
                textIratHelye.Text = String.Empty;
                break;
        }

        FormFooter1.Command = CommandName.New;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        ScriptManager1.SetFocus(textEv.TextBox);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ViewState["RefreshList"] != null)
        {
            if (ViewState["RefreshList"].ToString() == "1")
            {
                FormFooter1.ImageButton_Cancel.OnClientClick = "window.returnValue=true; window.close(); return false;";
            }
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private MIG_FoszamSearch SetSearchObjectFromComponents()
    {
        MIG_FoszamSearch mig_foszamSearch = new MIG_FoszamSearch();

        mig_foszamSearch.UI_YEAR.Value = textEv.Text;
        mig_foszamSearch.UI_YEAR.Operator = Query.Operators.equals;

        string sYear = textEv.Text;
        int year;
        if (!String.IsNullOrEmpty(sYear) && Int32.TryParse(sYear, out year))
        {
            if (year < 2000)
            {
                mig_foszamSearch.UI_IRJ.Value = textIrattariJel.Text;
                mig_foszamSearch.UI_IRJ.Operator = Query.Operators.equals;
            }
            else
            {
                mig_foszamSearch.IRJ2000.Value = textIrattariJel.Text;
                mig_foszamSearch.IRJ2000.Operator = Query.Operators.equals;
            }
        }
        else
        {
            mig_foszamSearch.IRJ2000.Value = textIrattariJel.Text;
            mig_foszamSearch.IRJ2000.Operator = Query.Operators.equals;
        }

        mig_foszamSearch.Csatolva_Id.Operator = Query.Operators.isnull;

        mig_foszamSearch.Edok_Utoirat_Id.Operator = Query.Operators.isnull;

        switch (Command)
        {
            case CommandName.SelejtezesreKijeloles:
                mig_foszamSearch.UGYHOL.Value = Constants.MIG_IratHelye.Irattarban;
                mig_foszamSearch.UGYHOL.Operator = Query.Operators.equals;
                break;
            case CommandName.SelejtezesreKijelolesVisszavonasa:
            case CommandName.Selejtezes:
                mig_foszamSearch.UGYHOL.Value = Constants.MIG_IratHelye.SelejtezesreVar;
                mig_foszamSearch.UGYHOL.Operator = Query.Operators.equals;
                break;
            default:
                break;
        }

        Search.SetIktatokonyvekSearch(Page, mig_foszamSearch, FormHeader1.ErrorPanel);


        return mig_foszamSearch;
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            switch (Command)
            {
                case CommandName.SelejtezesreKijeloles:
                    {
                        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
                        MIG_FoszamSearch search = SetSearchObjectFromComponents();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        Result result = service.SelejtezesreKijeloles(execParam, search);

                        if (!result.IsError)
                        {
                            SetResultPanel(result);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        }
                        break;
                    }
                case CommandName.SelejtezesreKijelolesVisszavonasa:
                    {
                        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
                        MIG_FoszamSearch search = SetSearchObjectFromComponents();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        Result result = service.SelejtezesreKijelolesVisszavonasa(execParam, search);

                        if (!result.IsError)
                        {
                            SetResultPanel(result);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        }
                        break;
                    }
                    case CommandName.Selejtezes:
                    {
                        MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
                        MIG_FoszamSearch search = SetSearchObjectFromComponents();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        Result result = service.Selejtezes(execParam, search);

                        if (!result.IsError)
                        {
                            SetResultPanel(result);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        }
                        break;
                    }
            }
        }
    }

    private void SetResultPanel(Result result)
    {
        //Panel-ek beállítása
        panelSelejtezes.Visible = false;
        panelResult.Visible = true;
        FormFooter1.ImageButton_Save.Visible = false;
        FormFooter1.ImageButton_Cancel.Visible = false;
        FormFooter1.ImageButton_Close.Visible = true;
        FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue=true; window.close(); return false;";
        ImageSaveAndNew.Visible = true;
        labelFilter.Text = String.Format("Év: {0}, Irattári jel: {1}", textEv.Text, textIrattariJel.Text);

        switch (Command)
        {
            case CommandName.SelejtezesreKijeloles:
                labelMuvelet.Text = "selejtezésre kijelölés";
                labelRecordNumberTitle.Text = "A selejtezésre kijelölt tételek száma:";
                break;
            case CommandName.SelejtezesreKijelolesVisszavonasa:
                labelMuvelet.Text = "selejtezésre kijelölés visszavonása";
                labelRecordNumberTitle.Text = "A selejtezésre kijelölésről visszavont tételek száma:";
                break;
            case CommandName.Selejtezes:
                labelMuvelet.Text = "selejtezés";
                labelRecordNumberTitle.Text = "A selejtezett tételek száma:";
                break;
            default:
                break;
        }

        if (result.Ds != null && result.Ds.Tables.Count > 0)
        {
            string recordNumber = result.Ds.Tables[0].Rows[0]["RecordNumber"].ToString();
            labelRecordNumber.Text = recordNumber;
            if (recordNumber != "0")
            {
                ViewState["RefreshList"] = "1";
            }

        }
        else
        {
            labelRecordNumberTitle.Visible = false;
            labelRecordNumber.Visible = false;
        }
    }

    protected void ImageSaveAndNew_Click(object sender, ImageClickEventArgs e)
    {
        panelSelejtezes.Visible = true;
        panelResult.Visible = false;
        ImageSaveAndNew.Visible = false;
        textEv.Text = String.Empty;
        textIrattariJel.Text = String.Empty;
    }
}
