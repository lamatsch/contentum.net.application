<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="SkontroVegeForm.aspx.cs" Inherits="MIG_SkontroVegeForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc11" %>
<%@ Register Src="~/Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,SkontroVegeFormHeaderTitle %>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                            </tbody>
                        </table>
                        <asp:Panel ID="PostaiCim_Panel" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    </tr>

                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelSkontro" runat="server" Text="Skontr� v�ge:">
                                            </asp:Label>
                                            </td>
                                        <td class="mrUrlapMezo">
                                            <uc3:CalendarControl ID="CalendarSkontro" runat="server" Validate="true" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        &nbsp;<!-- Egy�b c�mek -->
                       
            </eUI:eFormPanel>
             <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
             </td>
           </tr>
        </tbody>
     </table>
</asp:Content>

