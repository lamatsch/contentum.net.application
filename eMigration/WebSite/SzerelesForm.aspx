<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="SzerelesForm.aspx.cs" Inherits="SzerelesForm" Title="Szerel�s" %>

<%@ Register Src="eMigrationComponent/FoszamTextBox.ascx" TagName="UgyiratTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc11" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,SzerelesFormHeaderTitle %>" />
    
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <asp:RadioButtonList ID="RadioButtonList_SzerelesTipus" runat="server" Visible="false"
                    RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList_SzerelesTipus_SelectedIndexChanged">                
                    <asp:ListItem Text="Szerel�s felbont�sa" Value="SzerelesFelbontasa"></asp:ListItem>
                    <asp:ListItem Text="�j szerel�s" Value="UjSzereles" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    
    <asp:Panel ID="MainPanel" runat="server">
        <table cellpadding="0" cellspacing="0" width="90%">            
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="label7" runat="server" Text="Szerelend� �gyirat:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:UgyiratTextBox ID="Szerelendo_Ugyirat_UgyiratTextBox" runat="server" Filter="Szereles" MigralasVisible="True"/>
                            </td>
                        </tr>                                     
                    </table>
                    </eUI:eFormPanel>                    
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>            
        </table>
      </asp:Panel>  
</asp:Content>

