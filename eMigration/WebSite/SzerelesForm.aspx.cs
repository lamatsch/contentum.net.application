using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;
using Contentum.eMigration.Utility;
using Contentum.eMigration.Service;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;

public partial class SzerelesForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private string Id = "";
    private PageView pageView = null;
    private const string const_ElokeszitettSzereles = "ElokeszitettSzereles";
    private const string const_ElokeszitettSzerelesFelbontasa = "SzerelesFelbontasa";
    private const string const_UjSzereles = "UjSzereles";


    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);
        Command = Request.QueryString.Get(CommandName.Command);
        FormFooter1.Command = CommandName.Modify;
        Id = Request.QueryString.Get(QueryStringVars.Id);


        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
        FormFooter1.ImageButton_Save.OnClientClick = String.Empty;        

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private void LoadFormComponents()
    {
        LoadFormComponents(false);
    }

    private void LoadFormComponents(bool setComponentsForElokeszitettSzerelesFelbontas)
    {
        if (String.IsNullOrEmpty(Id))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
        }
        else
        {
            
            #region �gyirat Get

            MIG_FoszamService service_foszam = eMigrationService.ServiceFactory.GetMIG_FoszamService();

            ExecParam execParam_Get = UI.SetExecParamDefault(Page);
            execParam_Get.Record_Id = Id;

            Result result_ugyiratGet = service_foszam.Get(execParam_Get);
            if (result_ugyiratGet.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ugyiratGet);
                MainPanel.Visible = false;
                return;
            }

            MIG_Foszam MIG_FoszamObj = (MIG_Foszam)result_ugyiratGet.Record;            
            CheckSzerelhetoBeleIrat(MIG_FoszamObj);

            #endregion            
            /// �gyirat elemz�se, van-e r� szerel�s

            MIG_FoszamSearch search_eloiratok = new MIG_FoszamSearch();

            search_eloiratok.Csatolva_Id.Value = Id;
            search_eloiratok.Csatolva_Id.Operator = Query.Operators.equals;
            search_eloiratok.Csatolva_Rendszer.Value = "0";
            search_eloiratok.Csatolva_Rendszer.Operator = Query.Operators.equals;


            Result result_eloiratokGetAll = service_foszam.GetAll(UI.SetExecParamDefault(Page), search_eloiratok);
            if (result_eloiratokGetAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_eloiratokGetAll);
                MainPanel.Visible = false;
                return;
            }

            foreach (DataRow row in result_eloiratokGetAll.Ds.Tables[0].Rows)
            {
                string row_Csatolva_Id = row["Csatolva_Id"].ToString();
                string row_Csatolva_Rendszer = row["Csatolva_Rendszer"].ToString();
                

                if (row_Csatolva_Id == Id && row_Csatolva_Rendszer == "0")
                {
                    string row_Id = row["Id"].ToString();
                    string row_Targy = row["MEMO"].ToString();                    

                    SetComponentsForElokeszitettSzereles(MIG_FoszamObj, row_Id, row_Targy);

                    if (setComponentsForElokeszitettSzerelesFelbontas)
                    {
                        CheckSzerelesFelbonthato(Foszamok.GetAllapotByDataRow(row)); 
                    }
                    else
                    {
                        CheckSzerelhetoBeleIrat(MIG_FoszamObj);
                    }

                    break;
                }
                else
                {
                CheckSzerelhetoBeleIrat(MIG_FoszamObj);
                ResetFormComponentsForUjUgyirat();
                }
             }
       }
    }
    private bool CheckSzerelesFelbonthato(Foszamok.Statusz szerelendo)
    {
        ErrorDetails error;
        ExecParam ep = UI.SetExecParamDefault(Page);
        string felhnev = Contentum.eUtility.FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(ep.Felhasznalo_Id, Page);
        if (!Foszamok.SzerelesVisszavonhato(szerelendo, ep, Page,felhnev, out error))
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", error.Message);
            return false;
        }
        return true;
    }
    private bool CheckSzerelhetoBeleIrat(MIG_Foszam MIG_FoszamObj)
    {
        ErrorDetails error;
        ExecParam ep = UI.SetExecParamDefault(Page);
        string felhnev = Contentum.eUtility.FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(ep.Felhasznalo_Id, Page);
        Foszamok.Statusz s = Foszamok.GetAllapotByBusinessDocument(MIG_FoszamObj);
        if (!Foszamok.SzerelhetoBeleIrat(s, ep, Page, felhnev,out error))
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", error.Message);
            return false;
        }
        return true;        
    }

    private bool CheckSzerelhetoIrat(string Id, string Szerelendo_Id)
    {
        ErrorDetails error;
        ExecParam ep = UI.SetExecParamDefault(Page);
        string felhnev = Contentum.eUtility.FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(ep.Felhasznalo_Id, Page);
        Foszamok.Statusz szerelendo_statusz = Foszamok.GetAllapotById(Szerelendo_Id,ep,FormHeader1.ErrorPanel);
        Foszamok.Statusz cel_statusz = Foszamok.GetAllapotById(Id, ep, FormHeader1.ErrorPanel);
        if (!Foszamok.SzerelhetoIrat(szerelendo_statusz, cel_statusz, ep, Page,felhnev,out error))
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", error.Message);
            return false;
        }
        return true;
    }
    
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {

            if (String.IsNullOrEmpty(Id))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                return;
            }
            
            // Szerel�s

            MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam execParam = UI.SetExecParamDefault(Page);               
            string szerelendo_ugyiratId = Szerelendo_Ugyirat_UgyiratTextBox.Id_HiddenField;
            string szerelendo_ugyiratAzon = Szerelendo_Ugyirat_UgyiratTextBox.Text;
            string cel_ugyiratId = Id;

            ExecParam ep = execParam.Clone();
            ep.Record_Id = szerelendo_ugyiratId;



            // el�k�sz�tett szerel�s v�gleges�t�s, vagy sima szerel�s:
                
                 
            if (RadioButtonList_SzerelesTipus.SelectedValue == const_UjSzereles)
            {

                Result result = new Result();
                if (!CheckSzerelhetoIrat(cel_ugyiratId,szerelendo_ugyiratId))
                {                    
                    return;
                }
                //result = service.FoszamSzerelesMig(execParam, Id,szerelendo_ugyiratId, cel_ugyiratId, false);     
                result = service.FoszamSzerelesMig(execParam,szerelendo_ugyiratId,cel_ugyiratId,false);
                if (!result.IsError)
                {
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, Id);
                    JavaScripts.RegisterCloseWindowClientScript(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
            if (RadioButtonList_SzerelesTipus.SelectedValue == const_ElokeszitettSzerelesFelbontasa)
            {

                Result result = new Result();
                //result = service.FoszamSzerelesMig(execParam, Id,szerelendo_ugyiratId, cel_ugyiratId, false);     
                if (!CheckSzerelesFelbonthato(Foszamok.GetAllapotById(szerelendo_ugyiratId,ep.Clone(),FormHeader1.ErrorPanel)))
                {
                    return;
                }
                result = service.FoszamSzerelesMig(execParam, szerelendo_ugyiratId, cel_ugyiratId, true);
                if (!result.IsError)
                {
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, Id);
                    JavaScripts.RegisterCloseWindowClientScript(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }            

        }
    }   

    protected void RadioButtonList_SzerelesTipus_SelectedIndexChanged(object sender, EventArgs e)
    {
        // hibapanel elt�ntet�se
        if (FormHeader1.ErrorPanel.Visible == true)
        {
            FormHeader1.ErrorPanel.Visible = false;
        }
        // ha a f�panel el lett esetleg t�ntetve:
        MainPanel.Visible = true;

        if (RadioButtonList_SzerelesTipus.SelectedValue == const_UjSzereles)
        {
            ResetFormComponentsForUjUgyirat();

            FormHeader1.HeaderTitle = Resources.Form.SzerelesFormHeaderTitle;
        }
        else if (RadioButtonList_SzerelesTipus.SelectedValue == const_ElokeszitettSzereles)
        {
            // ha l�tezik el�k�sz�tett szerel�s, arra fognak be�llni a komponensek
            LoadFormComponents();

            FormHeader1.HeaderTitle = Resources.Form.SzerelesFormHeaderTitle;
        }
        else if (RadioButtonList_SzerelesTipus.SelectedValue == const_ElokeszitettSzerelesFelbontasa)
        {
            /// ugyanaz l�tsz�dik, mint az el�k�sz�tett szerel�sn�l
            /// formheader c�m v�ltozik, illetve Rendben gombra meger�s�t� (confirm) js k�d

            LoadFormComponents(true);

            FormHeader1.HeaderTitle = Resources.Form.ElokeszitettSzerelesFelbontasaFormHeader;

            FormFooter1.ImageButton_Save.OnClientClick = " if (confirm('" + Resources.Question.UIConfirmHeader_ElokeszitettSzerelesFelbontasa
                + "')==false) { return false; } " + FormFooter1.ImageButton_Save.OnClientClick;
        }
    }

    private void SetComponentsForElokeszitettSzereles(MIG_Foszam obj, string szerelendoUgyiratId, string szerelendoUgyiratIktatoSzam)
    {
        
        Szerelendo_Ugyirat_UgyiratTextBox.Id_HiddenField = szerelendoUgyiratId;
        Szerelendo_Ugyirat_UgyiratTextBox.Text = szerelendoUgyiratIktatoSzam;        
        RadioButtonList_SzerelesTipus.Visible = true;
        Szerelendo_Ugyirat_UgyiratTextBox.ReadOnly = true;
        
        
    }

    private void ResetFormComponentsForUjUgyirat()
    {

        Szerelendo_Ugyirat_UgyiratTextBox.Id_HiddenField = String.Empty;
        Szerelendo_Ugyirat_UgyiratTextBox.Text = String.Empty;        
        Szerelendo_Ugyirat_UgyiratTextBox.ReadOnly = false;
        Szerelendo_Ugyirat_UgyiratTextBox.ImageButton_Lov.CssClass = "mrUrlapInputImageButton";
        

    }
    private void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack && FormHeader1.ErrorPanel.Visible)
        {
            FormHeader1.ErrorPanel.Visible = false;
        }

    }
}