<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="TobbesForm.aspx.cs" Inherits="MIG_TobbesForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,MIG_TobbesFormHeaderTitle %>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <asp:Panel ID="PostaiCim_Panel" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelALNO" runat="server" Text="Alsz�m:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textALNO" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelNEV" runat="server" Text="N�v:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textNEV" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIRSZ" runat="server" Text="Ir�ny�t�sz�m:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textIRSZ" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelVAROSNEV" runat="server" Text="V�ros:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textVAROSNEV" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUTCA" runat="server" Text="Utca:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUTCA" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelHSZ" runat="server" Text="H�zsz�m:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textHSZ" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        &nbsp;<!-- Egy�b c�mek -->
                       
            </eUI:eFormPanel>
             <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
             </td>
           </tr>
        </tbody>
     </table>
</asp:Content>

