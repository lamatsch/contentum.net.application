using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;

public partial class MIG_TobbesForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    //private const string kodcsoportCim = "CIM_TIPUS";
    //private readonly string jsModifyAlert = "alert('" + Resources.Form.MIG_TobbesModifyWarning + "');";

    public static class BoolString
    {
        public static readonly ListItem Yes = new ListItem("Igen", "1");
        public static readonly ListItem No = new ListItem("Nem", "0");
        public static void FillDropDownList(DropDownList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(Yes);
            list.Items.Add(No);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillDropDownList(DropDownList list)
        {
            FillDropDownList(list, No.Value);
        }
        public static void SetSelectedValue(DropDownList list, string selectedValue)
        {
            if (selectedValue == Yes.Value || selectedValue == No.Value)
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                list.SelectedValue = No.Value;
            }
        }
    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        textALNO.ReadOnly = true;
        textNEV.ReadOnly = true;
        textVAROSNEV.ReadOnly = true;
        textIRSZ.ReadOnly = true;
        textUTCA.ReadOnly = true;
        textHSZ.ReadOnly = true;

        labelALNO.CssClass = "mrUrlapInputWaterMarked";
        labelNEV.CssClass = "mrUrlapInputWaterMarked";
        labelIRSZ.CssClass = "mrUrlapInputWaterMarked";
        labelVAROSNEV.CssClass = "mrUrlapInputWaterMarked";
        labelUTCA.CssClass = "mrUrlapInputWaterMarked";
        labelHSZ.CssClass = "mrUrlapInputWaterMarked";

    }

    /// <summary>
    /// A leg�rd�l� list�ban kiv�lasztott �rt�knek megfelel� panel aktiv�l�sa
    /// </summary>
    private void SetActivePanel()
    {

    }


    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Cim" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                MIG_TobbesService service = eMigrationService.ServiceFactory.GetMIG_TobbesService();
                //Contentum.eMigration.Service.MIG_TobbesService service = Contentum.eMigration.Utility.eMigrationService.ServiceFactory.GetMIG_TobbesService();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    MIG_Tobbes MIG_Tobbes = (MIG_Tobbes)result.Record;
                    LoadComponentsFromBusinessObject(MIG_Tobbes);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }

        registerJavascripts();

    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            //pageView.SetViewOnPage();
        }

    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(MIG_Tobbes mig_Tobbes)
    {
        if (String.IsNullOrEmpty(mig_Tobbes.MIG_Varos_Id))
        {
            textVAROSNEV.Text = "";
        }
        else
        {
            MIG_VarosService service = eMigrationService.ServiceFactory.GetMIG_VarosService();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = mig_Tobbes.MIG_Varos_Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                MIG_Varos MIG_Varos = (MIG_Varos)result.Record;
                textVAROSNEV.Text = MIG_Varos.NAME;
            }
            else
            {
                textVAROSNEV.Text = "";
            }
        }
        
        textALNO.Text = mig_Tobbes.ALNO;
        textNEV.Text = mig_Tobbes.NEV;
        textIRSZ.Text = mig_Tobbes.IRSZ;
        textUTCA.Text = mig_Tobbes.UTCA;
        textHSZ.Text = mig_Tobbes.HSZ;

        //aktu�lis verzi� ekt�rol�sa
        //FormHeader1.Record_Ver = mig_Tobbes.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        //FormPart_CreatedModified1.SetComponentValues(mig_Tobbes.Base);

    }

    // form --> business object
    // form --> business object
    /*private KRT_MIG_Tobbes GetBusinessObjectFromComponents()
    {
        
    }*/

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(textALNO); 
            compSelector.Add_ComponentOnClick(textNEV);
            compSelector.Add_ComponentOnClick(textIRSZ);
            compSelector.Add_ComponentOnClick(textVAROSNEV);
            compSelector.Add_ComponentOnClick(textUTCA);
            compSelector.Add_ComponentOnClick(textHSZ);
            

            FormFooter1.SaveEnabled = false;
        }

    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {

    }


    protected void registerJavascripts()
    {
        //AutoComplete-ek k�zti f�gg�s�gek be�ll�t�sa, postback eset�n contextkey �jra be�ll�t�sa
        //JavaScripts.SetAutoCompleteContextKey(Page, OrszagTextBox1.TextBox, TelepulesTextBox1.AutoCompleteExtenderClientID, true);
        //JavaScripts.SetAutoCompleteContextKey(Page, TelepulesTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID, true);

        //az els� ir�ny�t�sz�m automatikus be�r�sa
        //JavaScripts.SetTextWithFirstResult(TelepulesTextBox1.TextBox, IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);

        //A popup list�k zIndex-�nek be�ll�t�sa 1000-re
        //JavaScripts.SetCompletionListszIndex(Page);
    }

}
