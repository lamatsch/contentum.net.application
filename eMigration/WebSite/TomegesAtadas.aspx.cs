﻿using AjaxControlToolkit;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Contentum.eMigration.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eQuery;
using System.Linq;

public partial class TomegesAtadas : System.Web.UI.Page
{

    private string[] SelectedIds
    {
        get
        {
            return Session[Constants.SelectedJegyzekTetelIds].ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }
    }

    private IEnumerable<string> CheckedIds
    {
        get
        {
            foreach (GridViewRow row in tomegesAtadasGridView.Rows)
            {
                if (row.RowType != DataControlRowType.DataRow)
                    continue;
                CheckBox check = row.FindControl("check") as CheckBox;
                if (check.Checked)
                    yield return tomegesAtadasGridView.DataKeys[row.DataItemIndex].Value.ToString();
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "JegyzekTetelAtadas");
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        //FormFooter1.ImageButton_Save.OnClientClick = string.Format("{0} alert('{1}');", JavaScripts.SetOnClientClickIsSelectedRow(tomegesAtadasGridView.ClientID), Resources.List.UI_Atadas_AtadasFizikailag);
        FormHeader1.HeaderTitle = Resources.Form.TomegesAtadasHeaderTitle;

        CalendarControlAtadas.Validate = true;
        CalendarControlAtadas.Validator.ErrorMessage = "A mező kitöltése kötelező";

        if (IsPostBack)
        {
            // errorPanel eltüntetése, ha ki volt rakva:
            FormHeader1.ErrorPanel.Visible = false;
        }
        else
        {
            CalendarControlAtadas.SetTodayAndTime();
            TomegesAtadasGridViewBind();
        }
    }

    private void TomegesAtadasGridViewBind()
    {
        string sortExpression;

        SortDirection sortDirection = Search.GetSortDirectionFromViewState(tomegesAtadasGridView.ID, ViewState, SortDirection.Descending);

        sortExpression = Search.GetSortExpressionFromViewState(tomegesAtadasGridView.ID, ViewState, "Azonosito");
        TomegesAtadasGridViewBind(sortExpression, sortDirection);
    }


    private void TomegesAtadasGridViewBind(string sortExpression, SortDirection sortDirection)
    {
        Result result;
        result = GetJegyzekTetelekData(sortExpression, sortDirection);

        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillGridView"));
            ErrorUpdatePanel.Update();
        }
        else
        {
            SetEnabled(result);
            tomegesAtadasGridView.DataSource = result.Ds;
            tomegesAtadasGridView.DataBind();
        }
    }

    private Result GetJegyzekTetelekData(string sortExpression, SortDirection sortDirection)
    {
        MIG_JegyzekTetelekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekTetelekService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        MIG_JegyzekTetelekSearch search = new MIG_JegyzekTetelekSearch();
        search.Id.Value = Search.GetSqlInnerString(SelectedIds);
        search.Id.Operator = Query.Operators.inner;
        Result res = service.GetAllWithExtension(ExecParam, search);
        return res;
    }

    private void SetEnabled(Result result)
    {
        result.Ds.Tables[0].Columns.Add("Atadhato", typeof(bool));
        result.Ds.Tables[0].Columns.Add("ErrorDetail", typeof(string));
        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            String errorDetail = "";

            bool atadhato = true;


            string foszamId = row["MIG_Foszam_Id"].ToString();

            MIG_FoszamService iratPledanyService = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam foszamExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            foszamExecParam.Record_Id = foszamId;
            Result foszamResult = iratPledanyService.Get(foszamExecParam);

            if (!foszamResult.IsError)
            {
                MIG_Foszam foszam = (MIG_Foszam)foszamResult.Record;
                if (foszam.Ugyirat_tipus == "elektronikus")
                {
                    atadhato = false;
                    errorDetail = "Elektronikus főszám nem adható át. ";
                }
            }


            if (!String.IsNullOrEmpty(row["AtadasDatuma"].ToString()))
            {
                atadhato = false;
                errorDetail += "A tétel már át van adva. ";
            }

            string jegyzekId = row["Jegyzek_Id"].ToString();

            MIG_JegyzekekService jegyzekService = eMigrationService.ServiceFactory.GetMIG_JegyzekekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = jegyzekId;
            Result res = jegyzekService.Get(execParam);

            if (!res.IsError)
            {
                MIG_Jegyzekek jegyzek = (MIG_Jegyzekek)res.Record;

                if (String.IsNullOrEmpty(jegyzek.LezarasDatuma))
                {
                    atadhato = false;
                    errorDetail += "A tétel jegyzéke nem zárolt. ";
                }
            }

            row["Atadhato"] = atadhato;

            if (!atadhato)
                row["ErrorDetail"] = errorDetail;

        }
    }


    protected void TomegesAtadasGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dataRowView = e.Row.DataItem as DataRowView;
            if (!(bool)dataRowView["Atadhato"])
                e.Row.CssClass += " ViewDisabledWebControl";
        }
    }

    protected void TomegesAtadasGridView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HoverMenuExtender hmeErrorDetail = e.Row.FindControl("hmeErrorDetail") as HoverMenuExtender;
            e.Row.ID = "TomegesAtadas_" + e.Row.RowIndex.ToString();
            hmeErrorDetail.TargetControlID = e.Row.ID;
        }
    }

    protected void TomegesAtadasGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        TomegesAtadasGridViewBind(e.SortExpression, UI.GetSortToGridView(tomegesAtadasGridView.ID, ViewState, e.SortExpression));
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (CheckedIds.Count() == 0)
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UINoSelectedRow);
                return;
            }


            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            MIG_JegyzekTetelekService service = eMigrationService.ServiceFactory.GetMIG_JegyzekTetelekService();;
            Result result = service.Atadas(execParam, CheckedIds.ToArray(), CalendarControlAtadas.Text);

            if (!result.IsError)
            {
                JavaScripts.RegisterCloseWindowClientScript(Page);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }
    }

}