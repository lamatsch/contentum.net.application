﻿using Contentum.eBusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
//using Contentum.eRecord.Utility;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web.UI;

public partial class UgyiratpotloLapPrintForm : Contentum.eUtility.UI.PageBase
{
    private string FoszamIds = null;
    protected void Page_Init(object sender, EventArgs e)
    {
        var temp = Request.QueryString.Get("FoszamIds");
        if (string.IsNullOrEmpty(temp))
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "UgyiratPotloHiba", "alert('Nincs input tétel !')", true);
            return;
        }
        SetFilterString(temp);
    }

    private void SetFilterString(string temp)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(temp);
        sb.Insert(0, "'");
        sb.Append("'");
        sb.Replace(",", "','");
        FoszamIds = sb.ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        PotloLap();
    }

    private void PotloLap()
    {
        if (String.IsNullOrEmpty(FoszamIds))
            return;

        FoszamIds = FoszamIds.Replace("?", "'");
        ExecParam execParam = Contentum.eMigration.Utility.UI.SetExecParamDefault(Page, new ExecParam());
        Result result = GetFoszamByIdList(execParam, FoszamIds);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            Contentum.eMigration.BaseUtility.ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            return;
        }

        #region TEMPLATE TEXT
        string templateText = "";
        string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
        string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
        string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
        string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
        WebRequest wr = WebRequest.Create(SP_TM_site_url + "Ugyiratpotlo lap.V2.xml");
        wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
        StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
        templateText = template.ReadToEnd();
        template.Close();
        #endregion

        #region MyRegion
        //templateText = File.ReadAllText(@"D:\SHARED\Ugyiratpotlo lap.V2.xml");
        #endregion

        result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

        bool pdf;
        string filename = GetFileName(out pdf);

        #region PRIORITY
        int priority = 1;
        bool prior = false;

        Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = result.Uid;
        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

        if (string.IsNullOrEmpty(csop_result.ErrorCode))
        {
            foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
            {
                if (_row["Tipus"].ToString().Equals("3"))
                {
                    prior = true;
                }
            }
        }

        if (prior)
        {
            priority++;
        }
        #endregion

        for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
        {
            if (result.Ds.Tables[0].Columns.Contains("Kapta_Id") && !string.IsNullOrEmpty(result.Ds.Tables[0].Rows[i]["Kapta_Id"].ToString()))
            {
                result.Ds.Tables[0].Rows[i]["Kapta_IdNev"] =
                    Contentum.eUtility.FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(result.Ds.Tables[0].Rows[i]["Kapta_Id"].ToString(), Page);
            }
            if (result.Ds.Tables[0].Columns.Contains("Irattar_Id") && !string.IsNullOrEmpty(result.Ds.Tables[0].Rows[i]["Irattar_Id"].ToString()))
            {
                result.Ds.Tables[0].Rows[i]["Irattar_neve"] =
                    Contentum.eMigration.Utility.IratHelye.GetTextFormKod(result.Ds.Tables[0].Rows[i]["Irattar_Id"].ToString(), Page);
            }
        }

        result = GenerateDocumentFromTemplate(result, templateText, pdf, filename, priority);
        byte[] res = (byte[])result.Record;

        SetResult(result, pdf, res, filename);
    }

    private static Result GenerateDocumentFromTemplate(Result result, string templateText, bool pdf, string filename, int priority)
    {
       #region TEMPLATE
        Contentum.eTemplateManager.Service.TemplateManagerService tms = Contentum.eRecord.BaseUtility.eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        result = tms.GetWordDocument_DataSet_Thread(templateText, result, pdf, priority, filename, 25);
        #endregion
        return result;
    }

    private void SetResult(Result result, bool pdf, byte[] res, string fileName)
    {
        if (string.IsNullOrEmpty(result.ErrorCode))
        {
            #region RESPONSE
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.AddHeader("content-disposition", @"attachment;filename=" + fileName);
            if (pdf)
            {
                Response.ContentType = "application/pdf";
            }
            else
            {
                Response.ContentType = "application/msword";
            }

            Response.OutputStream.Write(res, 0, res.Length);
            Response.OutputStream.Flush();
            Response.End();
            #endregion
        }
        else
        {
            if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
            {
                string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
            }
        }
    }

    /// <summary>
    /// Executes foszam service query 
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ids"></param>
    /// <returns></returns>
    private Result GetFoszamByIdList(ExecParam execParam, string ids)
    {
        MIG_FoszamService service = Contentum.eMigration.Utility.eMigrationService.ServiceFactory.GetMIG_FoszamService();
        MIG_FoszamSearch search = new MIG_FoszamSearch();
        search.Id.Value = ids;
        search.Id.Operator = Query.Operators.inner;

        Result result = service.GetAllWithExtensionForUgyiratpotlo(execParam, search);
        return result;
    }
    /// <summary>
    /// Return file name by parameters
    /// </summary>
    /// <param name="isPdf"></param>
    /// <returns></returns>
    private string GetFileName(out bool isPdf)
    {
        isPdf = false;
        string fileName = null;
        if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
        {
            isPdf = true;
        }
        if (isPdf)
        {
            fileName = "Ugyiratpotlo_lap_" +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
        }
        else
        {
            fileName = "Ugyiratpotlo_lap_" +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
        }
        return fileName;
    }
}
