﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlszamCsatolmanyokTab.ascx.cs" Inherits="eMigrationComponent_AlszamCsatolmanyokTab" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc3" %>
<%@ Register Src="../Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc11" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc5" %>



<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="CsatolmanyokUpdatePanel" runat="server" OnLoad="CsatolmanyokUpdatePanel_Load"  UpdateMode="Conditional">
    <ContentTemplate>
    <asp:Panel ID="Gombok" runat="server" style="text-align:left">
            <asp:ImageButton  runat="server" style="display:block;padding-bottom:5px;" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"  ID="ImageView" OnClick="FunctionButtons_Click" CommandName="View"
                                                     AlternateText="Megtekintés" onmouseover="swapByName(this.id,'megtekintes_trap2.jpg')" onmouseout="swapByName(this.id,'megtekintes_trap.jpg')" />
        </asp:Panel>
    <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
    <asp:HiddenField ID="GridViewSelectedId" runat="server" />
    <asp:GridView ID="MIG_DokumentumGridView" runat="server" CellPadding="0" CellSpacing="0"
            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
            AllowSorting="True" DataKeyNames="Id" AutoGenerateColumns="False" 
            Width="98%" Visible="true" 
            onrowdatabound="MIG_DokumentumGridView_RowDataBound" 
            onrowcommand="MIG_DokumentumGridView_RowCommand" 
            onsorting="MIG_DokumentumGridView_Sorting">                          
            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
            <HeaderStyle CssClass="GridViewHeaderStyle" />
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                    <HeaderTemplate>
                        <span  style="white-space:nowrap">
                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                            &nbsp;&nbsp;
                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                        </span>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                            CssClass="HideCheckBoxText" />                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                </asp:CommandField>
                <asp:BoundField DataField="FajlNev" HeaderText="Állomány" SortExpression="FajlNev">
                    <HeaderStyle CssClass="GridViewBorderHeader" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="" SortExpression="MIG_Dokumentum.Tipus">
                    <HeaderStyle CssClass="GridViewBorderHeader" Width="0px" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                    <ItemTemplate>
                        <asp:ImageButton ID="fileIcon_ImageButton" runat="server" Enabled="false" OnClientClick="return false;"
                            ImageUrl="/Contentum/eRecord/images/hu/fileicons/default.gif" ToolTip="Megnyitás" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Leiras" HeaderText="Megjegyzés" SortExpression="Leiras" Visible = "false">
                    <HeaderStyle CssClass="GridViewBorderHeader" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:BoundField DataField="Formatum" HeaderText="Formátum" SortExpression="Formatum">
                    <HeaderStyle CssClass="GridViewBorderHeader" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                </asp:BoundField>
                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage" Visible = "false">
                    <ItemTemplate>
                        <asp:Image ID="MellekletImage" AlternateText="Melléklet" Height="20px" Width="25px"
                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewInfoImage" />
                    <ItemStyle CssClass="GridViewInfoImage" />
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage" Visible = "false">
                    <HeaderTemplate>
                        <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                    </ItemTemplate>
                    <HeaderStyle CssClass="GridViewLockedImage" />
                    <ItemStyle CssClass="GridViewLockedImage" />
                </asp:TemplateField>                
            </Columns>
            <PagerSettings Visible="False" />
        </asp:GridView>
        <br />        
        </ContentTemplate>
        </asp:UpdatePanel>