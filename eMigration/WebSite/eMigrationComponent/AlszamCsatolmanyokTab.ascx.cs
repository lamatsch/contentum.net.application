﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eMigration.Utility;
using Contentum.eMigration.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;


public partial class eMigrationComponent_AlszamCsatolmanyokTab : System.Web.UI.UserControl
{
    #region OnChangeObjectPropertiesEvent
    public event EventHandler OnChangedObjectProperties;
    public UI ui = new UI();

    private void RaiseEvent_OnChangedObjectProperties()
    {
        if (OnChangedObjectProperties != null)
        {
            OnChangedObjectProperties(this, new EventArgs());
        }
    }
    #endregion
    
    private string ParentId
    {
        get
        {
            string id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
                return String.Empty;
            return id;
        }
    }

    private string Command = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(QueryStringVars.Command);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }
        
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        
        SetUIIcons();
        MIG_DokumentumGridViewBind_WithReselectRow();        
        JavaScripts.RegisterScrollManagerScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, this);
        JavaScripts.RegisterPopupWindowClientScript(Page);
        string MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(MIG_DokumentumGridView);
        RefreshOnClientClicksByGridViewSelectedRow(MasterListSelectedRowId);                    
        ui.SetClientScriptToGridViewSelectDeSelectButton(MIG_DokumentumGridView);
            
    }
    public void SetUIIcons()
    {

        ImageView.Visible = true;      
        
        
    }    
    protected void MIG_DokumentumGridViewBind()
    {
        
        String sortExpression = Search.GetSortExpressionFromViewState("MIG_DokumentumGridView", ViewState, ""); // "Id" helyett UI_SAV, UI_YEAR DESC, UI_NUM DESC
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("MIG_DokumentumGridView", ViewState);

        MIG_DokumentumGridViewBind(sortExpression, sortDirection);
    }

    protected void MIG_DokumentumGridViewBind(String SortExpression, SortDirection SortDirection)
    {
     ///TODO: TRY Catch
        UI.ClearGridViewRowSelection(MIG_DokumentumGridView);
        MIG_DokumentumService service = eMigrationService.ServiceFactory.GetMIG_DokumentumService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        MIG_DokumentumSearch search = (MIG_DokumentumSearch)Search.GetSearchObject(Page, new MIG_DokumentumSearch());        
        search.MIG_Alszam_Id.Value = ParentId;
        search.MIG_Alszam_Id.Operator = Query.Operators.equals;
        if (string.IsNullOrEmpty(SortExpression))
            search.OrderBy = "Id DESC";
        else
            search.OrderBy = Search.GetOrderBy("MIG_DokumentumGridView", ViewState, SortExpression, SortDirection);
        ExecParam.Paging.PageNumber = -1;              
        // Lapozás beállítása:
        //UI.SetPaging(ExecParam, CsatolmanyokListHeader);
        
        
        Result res = service.GetAll(ExecParam,search);


        UI.GridViewFill(MIG_DokumentumGridView, res,this, EErrorPanel1, ErrorUpdatePanel1);
        
    }
    /// <summary>
    /// Formatum mező alapján beállítja a fájlikonokat
    /// </summary>
    public void SetFileIcons_GridViewRowDataBound(GridViewRowEventArgs e, Page page, string imageButtonId)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            string formatum = String.Empty;
            string externalLink = String.Empty;

            if (drv != null && drv["Formatum"] != null)
            {
                formatum = drv["Formatum"].ToString();
            }

            if (drv != null && drv["External_Link"] != null)
            {
                //externalLink = drv["External_Link"].ToString();
                externalLink = "GetDocumentContent.aspx/" + drv["FajlNev"].ToString() + "?id=" + drv["Id"];
            }

            if (!string.IsNullOrEmpty(formatum))
            {
                ImageButton fileIcon = (ImageButton)e.Row.FindControl(imageButtonId);

                if (fileIcon != null)
                {
                    fileIcon.ImageUrl = "~/images/hu/fileicons/" + FileFunctions.GetFileIconNameByFormatum(formatum);

                    if (!String.IsNullOrEmpty(externalLink))
                    {
                        fileIcon.OnClientClick = "window.open('" + externalLink + "'); return false;";
                        fileIcon.Enabled = true;
                    }
                    
                }
            }
        }
    }
   
    protected void MIG_DokumentumGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        SetFileIcons_GridViewRowDataBound(e, Page, "fileIcon_ImageButton");        
    }
    protected void FunctionButtons_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ib  = (sender as ImageButton);
        switch (ib.CommandName)
        {
            case CommandName.View: {                
                var checklist = ui.GetGridViewSelectedRows(MIG_DokumentumGridView, EErrorPanel1, ErrorUpdatePanel1);
                if (checklist.Count < 2)
                    return; 
                string js = string.Empty;
                var excludeid = MIG_DokumentumGridView.SelectedValue.ToString();
                checklist.Remove(excludeid);
                foreach (string id in checklist)
                {
                    js += string.Format("window.open('GetDocumentContent.aspx?id={0}');", id);

                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OnClickA", js, true);
                break;
                 }
            default: {break;}
        }
    }
    protected void MIG_DokumentumGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());            
            UI.SetGridViewCheckBoxesToSelectedRow(MIG_DokumentumGridView, selectedRowNumber, "check");            
        }
    }
    /// <summary>
    /// A sorkiválasztás megtartásával fríssiti az adatokat.
    /// </summary>
    private void MIG_DokumentumGridViewBind_WithReselectRow()
    {
        #region kiválaszott sor megjegyzése
        int selectedIndex = MIG_DokumentumGridView.SelectedIndex;
        string dataKey = String.Empty;
        if (selectedIndex > -1 && MIG_DokumentumGridView.DataKeys.Count > selectedIndex)
        {
            dataKey = MIG_DokumentumGridView.DataKeys[selectedIndex].Value.ToString();
        }
        MIG_DokumentumGridViewBind();
        if (MIG_DokumentumGridView.Rows.Count > selectedIndex)
        {
            if (selectedIndex > -1 && MIG_DokumentumGridView.DataKeys.Count > selectedIndex)
            {
                if (dataKey == MIG_DokumentumGridView.DataKeys[selectedIndex].Value.ToString())
                {
                    MIG_DokumentumGridView.SelectedIndex = selectedIndex;
                    UI.SetGridViewCheckBoxesToSelectedRow(MIG_DokumentumGridView, selectedIndex, "check");
                }
            }
        }
        #endregion sor újra kiválasztása
    }
    
    protected void MIG_DokumentumGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
       MIG_DokumentumGridViewBind(e.SortExpression, UI.GetSortToGridView("MIG_FoszamGridView", ViewState, e.SortExpression));
    }
    protected void CsatolmanyokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            if (!string.IsNullOrEmpty(eventArgument))
            MIG_DokumentumGridViewBind_WithReselectRow();            
        }
    }
    /// <summary>
    /// Frsísiti a Megtekintes gomb OnClientClick eseményeit. ///
    /// </summary>    
    private void RefreshOnClientClicksByGridViewSelectedRow(string id)
    {
        var CheckList = ui.GetGridViewSelectedRows(MIG_DokumentumGridView, EErrorPanel1, ErrorUpdatePanel1);       
        if (String.IsNullOrEmpty(id))
        {
            ImageView.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();            
        }
        else
        {
            ImageView.OnClientClick = string.Format("window.open('GetDocumentContent.aspx?id={0}')", id);                
        }        
        
    }
    
}
