<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlszamFormTab.ascx.cs"
    Inherits="eMigrationComponent_AlszamTab" %>

<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc3" %>
<%@ Register Src="../Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc11" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc5" %>

<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="AlszamUpdatePanel" runat="server" OnLoad="AlszamUpdatePanel_Load">
    <ContentTemplate>
        <asp:Panel ID="MainPanel" runat="server" Visible="true">
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server"></asp:HiddenField>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr>
                        <td valign="top">
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                            </tbody>
                        </table>
                        <asp:Panel ID="PostaiCim_Panel" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    </tr>
                                    <%--BLG_8481--%>
                                    <%--<tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUI_SAV" runat="server" Text="Iktat�k�nyv:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUI_SAV" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUI_YEAR" runat="server" Text="�v:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUI_YEAR" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUI_NUM" runat="server" Text="F�sz�m:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUI_NUM" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUI_YEAR" runat="server" Text="�v:"></asp:Label>&nbsp;</td>
                                      
                                        <td >
                                            <table>
                                                <tbody>
                                                    <tr>

                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="textUI_YEAR" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                                        </td>

                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelUI_SAV" runat="server" Text="Iktat�k�nyv:"></asp:Label></td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="textUI_SAV" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelUI_NUM" runat="server" Text="F�sz�m:"></asp:Label></td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="textUI_NUM" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelALNO" runat="server" Text="Alsz�m:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textALNO" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIDNUM" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textIDNUM" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                        </td>
                                    </tr>                                    
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelBKNEV" runat="server" Text="Bek�ld�:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textBKNEV" runat="server" CssClass="mrUrlapInput" Width="95%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelELINT" runat="server" Text="Elint�z�s d�tuma:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textELINT" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelERK" runat="server" Text="�rkez�s:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textERK" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIKTAT" runat="server" Text="Iktat�s:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textIKTAT" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelELOADONEV" runat="server" Text="El�ad�:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textELOADONEV" runat="server" CssClass="mrUrlapInput" Width="95%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelROGZIT" runat="server" Text="R�gz�t�:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textROGZIT" runat="server" CssClass="mrUrlapInput"  Width="95%"></asp:TextBox></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelMJ" runat="server" Text="Megjegyz�s:" CssClass="mrUrlapInputWaterMarked"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textMJ" runat="server" CssClass="mrUrlapInput" ReadOnly="true"  Width="95%" TextMode="MultiLine"></asp:TextBox></td>
                                    </tr>
                                    <%if (IsNMHHColumnsVisible)
                                        { %>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUGYHOL" runat="server" Text="Irat helye:" CssClass="mrUrlapInputWaterMarked"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUGYHOL" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIRATTARBA" runat="server" Text="Iratt�rba ad�s:" CssClass="mrUrlapInputWaterMarked"></asp:Label>
                                            </td>
                                        <td class="mrUrlapMezo">
                                            <uc3:Calendarcontrol ID="CalendarIrattarba" runat="server" Validate="false" ReadOnly="true"/>
                                        </td>
                                    </tr>
                                     <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAdathordozo_tipusa" runat="server" Text="Adath. t�pus:" CssClass="mrUrlapInputWaterMarked"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textAdathordozo_tipusa" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIrattipus" runat="server" Text="Iratt�pus:" CssClass="mrUrlapInputWaterMarked"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textIrattipus" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelFeladat" runat="server" Text="Csatolt irat:" CssClass="mrUrlapInputWaterMarked"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textFeladat" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox></td>
                                    </tr>
                                    <% } %>
                                </tbody>
                            </table>
                        </asp:Panel>
                        &nbsp;<!-- Egy�b c�mek -->
                       
            </eUI:eFormPanel>
            <uc5:TabFooter ID="TabFooter1" runat="server"></uc5:TabFooter>
        </tbody>
    </table></asp:Panel> 
</ContentTemplate>
</asp:UpdatePanel>
