using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eMigration.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Service;

public partial class eMigrationComponent_AlszamTab : System.Web.UI.UserControl
{

    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    //private const string kodcsoportCim = "CIM_TIPUS";
    //private readonly string jsModifyAlert = "alert('" + Resources.Form.MIG_AlszamModifyWarning + "');";

    private String ParentId = "";

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    // A Parent formr�l kell be�ll�tani, hogy innen tudjuk �ll�tani a form c�met
    public Component_FormHeader FormHeader = null;


    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    // A sz�ban forg� �gyirat objektum (megtekint�s, m�dos�t�s eset�n haszn�latos)
    public MIG_Alszam obj_MIG_Alszam = null;


    // Az objektumban t�rt�n� v�ltoz�sokat jelz� esem�ny, ami pl. kiv�lthat egy jogosults�gellen�rz�st
    public event EventHandler OnChangedObjectProperties;

    private void RaiseEvent_OnChangedObjectProperties()
    {
        if (OnChangedObjectProperties != null)
        {
            OnChangedObjectProperties(this, new EventArgs());
        }
    }

    //be van-e t�ltve az �gyirat
    public bool IsMIGAlszamLoaded
    {
        get
        {
            if (ViewState["IsMIGAlszamLoaded"] != null)
            {
                bool loaded;
                if (Boolean.TryParse(ViewState["IsMIGAlszamLoaded"].ToString(), out loaded))
                {
                    return loaded;
                }
            }

            return false;
        }
        set
        {
            ViewState["IsMIGAlszamLoaded"] = value;
        }
    }

    //public static class BoolString
    //{
    //    public static readonly ListItem Yes = new ListItem("Igen", "1");
    //    public static readonly ListItem No = new ListItem("Nem", "0");
    //    public static void FillDropDownList(DropDownList list, string selectedValue)
    //    {
    //        list.Items.Clear();
    //        list.Items.Add(Yes);
    //        list.Items.Add(No);
    //        SetSelectedValue(list, selectedValue);
    //    }
    //    public static void FillDropDownList(DropDownList list)
    //    {
    //        FillDropDownList(list, No.Value);
    //    }
    //    public static void SetSelectedValue(DropDownList list, string selectedValue)
    //    {
    //        if (selectedValue == Yes.Value || selectedValue == No.Value)
    //        {
    //            list.SelectedValue = selectedValue;
    //        }
    //        else
    //        {
    //            list.SelectedValue = No.Value;
    //        }
    //    }
    //}

    private string RegisterCheckSkontroVegeJavaScript(DropDownList ddlIratHelye, HiddenField hfIratEredetiHelye, Component_CalendarControl ccSkontroVege)
    {
        string js = "";
        if (ddlIratHelye != null && ccSkontroVege != null)
        {
            js = "var text = $get('" + ddlIratHelye.ClientID + @"');
                  var cc = $get('" + ccSkontroVege.TextBox.ClientID + @"');
                  var hf = $get('" + hfIratEredetiHelye.ClientID + @"');

                   if (text && hf && text.value == hf.value)
                   {
                          alert('" + Resources.Error.ErrorCode_56006 + @"');
                          return false;
                   }
                   else if(text && text.value == '1')
                   {
                       if (hf && hf.value == '2')
                       {
                          alert('" + Resources.Error.ErrorCode_56007 + @"');
                          return false;
                       }                    
                       else if (!cc || cc.value == '')
                       {
                          alert('" + Resources.Error.ErrorCode_56008 + @"');
                          return false;
                       }
                       else
                       {
                         var year   = parseInt(cc.value.substring(0,4));
                         var month  = parseInt(cc.value.substring(5,7));
                         var day   = parseInt(cc.value.substring(8,10));
                         var ccDate = new Date(year, month-1, day);
                         var today = new Date;
                         var tomorrow = new Date(today.getYear(), today.getMonth(), today.getDate() + 1); 

                         if (ccDate < tomorrow)
                         {
                           alert('" + Resources.Error.ErrorCode_56009 + @"');
                           return false;
                         }
                         else {return true;}
                      }
                   }
                   else {return true;}";
        }

        return js;

    }


    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        textUI_SAV.ReadOnly = true;
        textUI_YEAR.ReadOnly = true;
        textUI_NUM.ReadOnly = true;
        textALNO.ReadOnly = true;
        textIDNUM.ReadOnly = true;
        textBKNEV.ReadOnly = true;
        textELINT.ReadOnly = true;
        textERK.ReadOnly = true;
        textIKTAT.ReadOnly = true;
        textELOADONEV.ReadOnly = true;
        textROGZIT.ReadOnly = true;
        textMJ.ReadOnly = true;
        //ddlistIratHelye.Enabled = false;
        //CalendarIrattarba.ReadOnly = true;
        //CalendarSkontro.ReadOnly = true;

        labelUI_SAV.CssClass = "mrUrlapInputWaterMarked";
        labelUI_YEAR.CssClass = "mrUrlapInputWaterMarked";
        labelUI_NUM.CssClass = "mrUrlapInputWaterMarked";
        labelALNO.CssClass = "mrUrlapInputWaterMarked";
        labelIDNUM.CssClass = "mrUrlapInputWaterMarked";
        labelBKNEV.CssClass = "mrUrlapInputWaterMarked";
        labelELINT.CssClass = "mrUrlapInputWaterMarked";
        //labelIRATTARBA.CssClass = "mrUrlapInputWaterMarked";
        labelERK.CssClass = "mrUrlapInputWaterMarked";
        labelIKTAT.CssClass = "mrUrlapInputWaterMarked";
        labelELOADONEV.CssClass = "mrUrlapInputWaterMarked";
        labelROGZIT.CssClass = "mrUrlapInputWaterMarked";
        labelMJ.CssClass = "mrUrlapInputWaterMarked";
        //labelIratHelye.CssClass = "mrUrlapInputWaterMarked";
        //labelSkontro.CssClass = "mrUrlapInputWaterMarked";

    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        textUI_SAV.ReadOnly = true;
        textUI_YEAR.ReadOnly = true;
        textUI_NUM.ReadOnly = true;
        textALNO.ReadOnly = true;
        textIDNUM.ReadOnly = true;
        textBKNEV.ReadOnly = true;
        textELINT.ReadOnly = true;
        textERK.ReadOnly = true;
        textIKTAT.ReadOnly = true;
        textELOADONEV.ReadOnly = true;
        textROGZIT.ReadOnly = true;
        textMJ.ReadOnly = true;
        //CalendarIrattarba.ReadOnly = true;
        //CalendarSkontro.ReadOnly = true;

        labelUI_SAV.CssClass = "mrUrlapInputWaterMarked";
        labelUI_YEAR.CssClass = "mrUrlapInputWaterMarked";
        labelUI_NUM.CssClass = "mrUrlapInputWaterMarked";
        labelALNO.CssClass = "mrUrlapInputWaterMarked";
        labelIDNUM.CssClass = "mrUrlapInputWaterMarked";
        labelBKNEV.CssClass = "mrUrlapInputWaterMarked";
        labelELINT.CssClass = "mrUrlapInputWaterMarked";
        //labelIRATTARBA.CssClass = "mrUrlapInputWaterMarked";
        labelERK.CssClass = "mrUrlapInputWaterMarked";
        labelIKTAT.CssClass = "mrUrlapInputWaterMarked";
        labelELOADONEV.CssClass = "mrUrlapInputWaterMarked";
        labelROGZIT.CssClass = "mrUrlapInputWaterMarked";
        labelMJ.CssClass = "mrUrlapInputWaterMarked";
        //labelSkontro.CssClass = "mrUrlapInputWaterMarked";

    }

    public bool IsNMHHColumnsVisible
    {
        get { return "NMHH".Equals(FelhasznaloProfil.OrgKod(Page), StringComparison.InvariantCultureIgnoreCase); }
    }


    // funkci�jogok ellen�rz�se a f� formon

    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(QueryStringVars.Command);

        pageView = new PageView(Page, ViewState);

        ParentId = Request.QueryString.Get(QueryStringVars.Id);

        switch (Command)
        {
            case CommandName.DesignView:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                TabFooter1.Controls.Add(compSelector);
                break;
            default:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MIGAlszam" + Command);
                break;
        }

        //if (!IsPostBack)
        //{
        //    IratHelye.FillDropDownList(ddlistIratHelye);
        //}

        if (Command == CommandName.View)
        {
            SetViewControls();
        }

        if (Command == CommandName.Modify)
        {
            SetModifyControls();

            //TabFooter1.ImageButton_Save.OnClientClick += RegisterCheckSkontroVegeJavaScript(ddlistIratHelye, HiddenField_IratEredetiHelye, CalendarSkontro);
            //TabFooter1.ImageButton_SaveAndClose.OnClientClick += RegisterCheckSkontroVegeJavaScript(ddlistIratHelye, HiddenField_IratEredetiHelye, CalendarSkontro);

        }

        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }

    }

    protected void AlszamUpdatePanel_Load(object sender, EventArgs e)
    {
        // ha nem ez az akt�v tab, nem fut le
        if (!_Active) return;

        if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshUgyiratokPanel:
                        ReLoadTab(true);
                        RaiseEvent_OnChangedObjectProperties();
                        break;
                }
            }
        }
    }

    public void ReLoadTab()
    {
        if (!IsPostBack || !IsMIGAlszamLoaded)
        {
            ReLoadTab(true);
        }
        else
        {
            ReLoadTab(false);
        }
    }

    private void ReLoadTab(bool loadRecord)
    {               

        // nem lehet New
        if (Command == CommandName.New) 
        { 
            return; 
        }

        if (_ParentForm == Constants.ParentForms.Alszam)
        {
            if (!MainPanel.Visible) return;

            if (Command == CommandName.View || Command == CommandName.Modify)
            {

                if (String.IsNullOrEmpty(ParentId))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel1.Update();
                }
                else
                {
                    if (loadRecord)
                    {
                        // rekord bet�lt�se

                        if (obj_MIG_Alszam == null)
                        {
                            MIG_AlszamService service = eMigrationService.ServiceFactory.GetMIG_AlszamService();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam.Record_Id = ParentId;

                            Result result = service.Get(execParam);
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                obj_MIG_Alszam = (MIG_Alszam)result.Record;
                                if (obj_MIG_Alszam == null)
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1,
                                        ResultError.CreateNewResultWithErrorCode(50101));
                                    ErrorUpdatePanel1.Update();
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();                                
                            }
                        }

                        if (obj_MIG_Alszam != null)
                        {
                            LoadComponentsFromBusinessObject(obj_MIG_Alszam);
                        }
                    }
                }
            }

            // Ez az egyes tab fulek belso commandjat hatarozza meg! pl: Kuldemeny modositasakor meg kell kulonboztetni gridview felvetel/modositast!!!
            TabFooter1.CommandArgument = Command;


            if (Command == CommandName.DesignView)
            {
                Load_ComponentSelectModul();
            }
            else
            {
                pageView.SetViewOnPage(Command);
            }


            // Komponensek l�that�s�g�nak be�ll�t�sa:
            SetComponentsVisibility(Command);
        }
        else
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }

    }

    


    #region private methods


    /// <summary>
    /// Komponensek l�that�s�g�nak be�ll�t�sa
    /// </summary>
    /// <param name="_ugyiratStatusz">lehet null is, csak olyankor �jra le kell k�rni</param>
    private void SetComponentsVisibility(String _command)
    {
        #region TabFooter gombok �ll�t�sa

        if (_command == CommandName.Modify)
        {
            TabFooter1.ImageButton_Save.Visible = true;
            TabFooter1.ImageButton_SaveAndClose.Visible = true;           
            TabFooter1.ImageButton_SaveAndNew.Visible = false;            
        }

        // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
        TabFooter1.ImageButton_Cancel.Visible = false;

        #endregion

    }


    // business object --> form
    private void LoadComponentsFromBusinessObject(MIG_Alszam mig_Alszam)
    {
        if (String.IsNullOrEmpty(mig_Alszam.MIG_Eloado_Id))
        {
            textELOADONEV.Text = "";
        }
        else
        {
            MIG_EloadoService service = eMigrationService.ServiceFactory.GetMIG_EloadoService();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = mig_Alszam.MIG_Eloado_Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                MIG_Eloado MIG_Eloado = (MIG_Eloado)result.Record;
                textELOADONEV.Text = MIG_Eloado.NAME;
            }
            else
            {
                textELOADONEV.Text = "";
            }
        }

        // BUG_7342 - MIG_Foszam.EdokSav megjelen�t�se
        string edokSav = null;
        if (!String.IsNullOrEmpty(mig_Alszam.MIG_Foszam_Id))
        {
            MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = mig_Alszam.MIG_Foszam_Id;

            Result result = service.Get(execParam);
            if (!result.IsError)
            {
                MIG_Foszam foszam = (MIG_Foszam)result.Record;
                edokSav = foszam.EdokSav;
            }
        }

        textUI_SAV.Text = edokSav ?? mig_Alszam.UI_SAV;
        textUI_YEAR.Text = mig_Alszam.UI_YEAR;
        textUI_NUM.Text = mig_Alszam.UI_NUM;
        textALNO.Text = mig_Alszam.ALNO;
        textIDNUM.Text = mig_Alszam.IDNUM;
        textBKNEV.Text = mig_Alszam.BKNEV;
        if (mig_Alszam.ELINT.Contains("0:"))
        {
            textELINT.Text = mig_Alszam.ELINT.Remove(mig_Alszam.ELINT.IndexOf("0:"));
        }
        else
        {
            textELINT.Text = mig_Alszam.ELINT;
        }
        //if (mig_Alszam.IRATTARBA.Contains("0:"))
        //{
        //    CalendarIrattarba.Text = mig_Alszam.IRATTARBA.Remove(mig_Alszam.IRATTARBA.IndexOf("0:"));
        //}
        //else
        //{
        //    CalendarIrattarba.Text = mig_Alszam.IRATTARBA;
        //}
        if (mig_Alszam.ERK.Contains("0:"))
        {
            textERK.Text = mig_Alszam.ERK.Remove(mig_Alszam.ERK.IndexOf("0:"));
        }
        else
        {
            textERK.Text = mig_Alszam.ERK;
        }
        if (mig_Alszam.IKTAT.Contains("0:"))
        {
            textIKTAT.Text = mig_Alszam.IKTAT.Remove(mig_Alszam.IKTAT.IndexOf("0:"));
        }
        else
        {
            textIKTAT.Text = mig_Alszam.IKTAT;
        }
        textROGZIT.Text = mig_Alszam.ROGZIT;
        textMJ.Text = mig_Alszam.MJ;

        //ddlistIratHelye.SelectedValue = mig_Alszam.UGYHOL;
        //HiddenField_IratEredetiHelye.Value = mig_Alszam.UGYHOL; // megjegyezz�k, hogy �ssze lehessen hasonl�tani
        //CalendarSkontro.Text = mig_Alszam.SCONTRO;

        textUGYHOL.Text = mig_Alszam.UGYHOL;
        CalendarIrattarba.Text = mig_Alszam.IRATTARBA;
        textAdathordozo_tipusa.Text = mig_Alszam.Adathordozo_tipusa;
        textIrattipus.Text = mig_Alszam.Irattipus;
        textFeladat.Text = mig_Alszam.Feladat;

    }

    private void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(textUI_SAV);
            compSelector.Add_ComponentOnClick(textUI_YEAR);
            compSelector.Add_ComponentOnClick(textUI_NUM);
            compSelector.Add_ComponentOnClick(textALNO);
            compSelector.Add_ComponentOnClick(textIDNUM);
            compSelector.Add_ComponentOnClick(textBKNEV);
            compSelector.Add_ComponentOnClick(textELINT);
            //compSelector.Add_ComponentOnClick(CalendarIrattarba);
            compSelector.Add_ComponentOnClick(textERK);
            compSelector.Add_ComponentOnClick(textIKTAT);
            compSelector.Add_ComponentOnClick(textELOADONEV);
            compSelector.Add_ComponentOnClick(textROGZIT);
            compSelector.Add_ComponentOnClick(textMJ);
            //compSelector.Add_ComponentOnClick(CalendarSkontro);
            //compSelector.Add_ComponentOnClick(ddlistIratHelye);

            TabFooter1.SaveEnabled = false;
        }

    }

    // // form --> business object
    //private MIG_Alszam GetBusinessObjectFromComponents()
    //{
        //MIG_Alszam alszam = new MIG_Alszam();
        //alszam.Updated.SetValueAll(false);

        //alszam.UGYHOL = ddlistIratHelye.SelectedValue;
        //alszam.Updated.UGYHOL = pageView.GetUpdatedByView(ddlistIratHelye);

        //if (alszam.UGYHOL == IratHelye.Skontro.Value)
        //{
        //    alszam.SCONTRO = CalendarSkontro.Text;
        //    alszam.Updated.SCONTRO = true;
        //}

        //return alszam;
    //}


    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Save ||e.CommandName == CommandName.SaveAndClose)
        //{
        //    switch (Command)
        //    {
        //        case CommandName.Modify:
        //            {
        //                String recordId = Request.QueryString.Get(QueryStringVars.Id);
        //                if (String.IsNullOrEmpty(recordId))
        //                {
        //                    // nincs Id megadva:
        //                    ResultError.DisplayNoIdParamError(EErrorPanel1);
        //                }
        //                else
        //                {
        //                    MIG_AlszamService service = eMigrationService.ServiceFactory.GetMIG_AlszamService();
        //                    MIG_Alszam alszam = GetBusinessObjectFromComponents();

        //                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        //                    execParam.Record_Id = recordId;

        //                    Result result = service.Update(execParam, alszam);

        //                    if (String.IsNullOrEmpty(result.ErrorCode))
        //                    {
        //                        JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);

        //                        if (e.CommandName == CommandName.SaveAndClose)
        //                        {
        //                            JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
        //                        }
        //                        else
        //                        {
        //                            ReLoadTab(true);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        //                    }
        //                }
        //                break;
        //            }
        //    }
        //}
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Command == CommandName.Modify)
        {
            
        }
    }

    #endregion




}
