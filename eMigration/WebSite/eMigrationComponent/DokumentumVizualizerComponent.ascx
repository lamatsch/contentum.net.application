﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DokumentumVizualizerComponent.ascx.cs" Inherits="eMigrationComponent_DokumentumVizualizerComponent" %>

<asp:Panel ID="panelVizualizer" runat="server" CssClass="basic_overlay" style="display:none;">
    <div class="basic_overlay_mid">
        <div class="body_wrapper">
            <asp:Panel id="panelHeader" runat="server" class="header">
                <asp:Panel ID=panelClose runat="server" class="closeWrapper">
                    <asp:Label id="labelClose" runat="server" class="closeText" Text="Bezár"/>
                    <asp:Image Id="imgClose" runat="server" ImageUrl="~/images/hu/wordvizualizer/close_btn.png"/>
                </asp:Panel>
            </asp:Panel>
            <iframe id="frameBody" runat="server" class="body" style="background-color: White;" scrolling="yes" width="100%"
                height="100%" frameborder="0" marginheight="0" marginwidth="0">
            </iframe>
            <asp:Panel ID="panelTitle" runat="server" class="title">
                <asp:Label ID="labelTitle" runat="server" class="titleText" />
            </asp:Panel>
        </div>
    </div>
</asp:Panel>
<ajaxToolkit:DragPanelExtender ID="dragPanelExtenderVizualizer" runat="server" DragHandleID="panelHeader" 
TargetControlID="panelVizualizer">
</ajaxToolkit:DragPanelExtender>