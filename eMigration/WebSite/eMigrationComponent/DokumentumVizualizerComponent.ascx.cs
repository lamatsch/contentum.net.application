﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Contentum.eMigration.BaseUtility;

public partial class eMigrationComponent_DokumentumVizualizerComponent : System.Web.UI.UserControl, IScriptControl
{
	[Serializable]
	private class DokumentumElement : IEquatable<DokumentumElement>
	{
		private string clientID;

		public string ClientID
		{
			get { return clientID; }
			set { clientID = value; }
		}

		private string dokumentumId;

		public string DokumentumId
		{
			get { return dokumentumId; }
			set { dokumentumId = value; }
		}

		private string fileName;

		public string FileName
		{
			get { return fileName; }
			set { fileName = value; }
		}

		private string version;

		public string Version
		{
			get { return version; }
			set { version = value; }
		}

		private string externalLink;

		public string ExternalLink
		{
			get { return externalLink; }
			set { externalLink = value; }
		}

		public bool IsDocx
		{
			get
			{
				if ( !String.IsNullOrEmpty ( this.fileName ) )
				{
					return System.IO.Path.GetExtension ( this.fileName ).ToLower ( ) == ".docx";
				}

				return true;
			}
		}

		public DokumentumElement ( string ClientID, string DokumentumId, string FileName, string Version, string ExternalLink )
		{
			this.clientID = ClientID;
			this.dokumentumId = DokumentumId;
			this.fileName = FileName;
			this.version = Version;
			this.externalLink = ExternalLink;
		}

		#region IEquatable<DokumentumElement> Members

		public bool Equals ( DokumentumElement other )
		{
			return this.clientID.Equals ( other.clientID );
		}

		#endregion
	}

	private List<DokumentumElement> _elementsList
	{
		get
		{
			object o = ViewState[ "_elementsList" ];

			if ( o == null )
			{
				o = ViewState[ "_elementsList" ] = new List<DokumentumElement> ( );
			}

			return o as List<DokumentumElement>;
		}
	}

	private Dictionary<string, List<string>> _unFilledElements = new Dictionary<string, List<string>> ( );

	public void ClearDokumentElements ( )
	{
		_elementsList.Clear ( );
		_unFilledElements.Clear ( );
	}

	public void AddDokumentElement ( string ClientID, string DokumentumId, string FileName, string Version, string ExternalLink )
	{
		DokumentumElement dokument = new DokumentumElement ( ClientID, DokumentumId, FileName, Version, ExternalLink );
		if ( dokument.IsDocx )
		{
			if ( !_elementsList.Contains ( dokument ) )
			{
				_elementsList.Add ( dokument );
			}
		}
	}

	public void AddDokumentElement ( string ClientID, string DokumentumId )
	{
		if ( !_unFilledElements.ContainsKey ( DokumentumId ) )
		{
			_unFilledElements.Add ( DokumentumId, new List<string> ( ) );
		}
		_unFilledElements[ DokumentumId ].Add ( ClientID );
	}

	public void RemoveDokumentumElement ( string ClientID )
	{
		DokumentumElement d = new DokumentumElement ( ClientID, String.Empty, String.Empty, String.Empty, String.Empty );
		if ( _elementsList.Contains ( d ) )
		{
			_elementsList.Remove ( d );
		}
	}

	private void FillDokumentumElements ( )
	{
		if ( _unFilledElements.Count > 0 )
		{
			Contentum.eBusinessDocuments.ExecParam xpm = Contentum.eUtility.UI.SetExecParamDefault ( Page );

			Contentum.eMigration.Service.MIG_DokumentumService svc = eMigrationService.ServiceFactory.GetMIG_DokumentumService ( );
			Contentum.eQuery.BusinessDocuments.MIG_DokumentumSearch sch = new Contentum.eQuery.BusinessDocuments.MIG_DokumentumSearch ( );
			sch.Id.Value = Contentum.eUtility.Search.GetSqlInnerString ( _unFilledElements.Keys.ToArray ( ) );
			sch.Id.Operator = Contentum.eQuery.Query.Operators.inner;
			sch.OrderBy = "Id DESC";
			Contentum.eBusinessDocuments.Result res = svc.GetAll ( xpm, sch );

			if ( !res.IsError )
			{
				foreach ( DataRow row in res.Ds.Tables[ 0 ].Rows )
				{
					string dokumentumId = row[ "Id" ].ToString ( );
					string fileName = row[ "FajlNev" ].ToString ( );
					string version = row[ "Ver" ].ToString ( );
					string externalLink = row[ "External_Link" ].ToString ( );

					foreach ( string clientId in _unFilledElements[ dokumentumId ] )
					{
						AddDokumentElement ( clientId, dokumentumId, fileName, version, externalLink );

					}
				}
			}

			_unFilledElements.Clear ( );

		}
	}

	public string BehaviorID
	{
		get
		{
			return this.ClientID + "_Behavior";
		}
	}

	private int _displayAfter = 1000;

	public int DisplayAfter
	{
		get { return _displayAfter; }
		set { _displayAfter = value; }
	}

	protected void Page_Load ( object sender, EventArgs e )
	{

	}

	#region IScriptControl Members

	private ScriptManager sm;

	protected override void OnPreRender ( EventArgs e )
	{
		if ( !this.DesignMode )
		{
			// Test for ScriptManager and register if it exists
			sm = ScriptManager.GetCurrent ( Page );

			if ( sm == null )
				throw new HttpException ( "A ScriptManager control must exist on the current page." );


			sm.RegisterScriptControl ( this );
		}

		base.OnPreRender ( e );

		FillDokumentumElements ( );
	}

	protected override void Render ( HtmlTextWriter writer )
	{
		if ( !this.DesignMode )
		{
			sm.RegisterScriptDescriptors ( this );
		}

		base.Render ( writer );
	}

	public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors ( )
	{
		ScriptControlDescriptor descriptor = new ScriptControlDescriptor ( "Utility.DokumentumVizualizerBehavior", this.panelVizualizer.ClientID );
		descriptor.AddProperty ( "id", this.BehaviorID );
		descriptor.AddProperty ( "PanelVizualizerClientID", this.panelVizualizer.ClientID );
		descriptor.AddProperty ( "FrameBodyClientID", this.frameBody.ClientID );
		descriptor.AddProperty ( "ElementsList", this._elementsList );
		descriptor.AddProperty ( "PanelCloseClientID", this.panelClose.ClientID );
		descriptor.AddProperty ( "LabelTitleClientID", this.labelTitle.ClientID );
		descriptor.AddProperty ( "DisplayAfter", this._displayAfter );

		return new ScriptDescriptor[ ] { descriptor };
	}

	public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences ( )
	{
		ScriptReference reference = new ScriptReference ( );
		reference.Path = "~/JavaScripts/DokumentumVizualizerBehavior.js";

		ScriptReference referenceJQuery = new ScriptReference ( );
		referenceJQuery.Path = "~/JavaScripts/jquery.js";
		referenceJQuery.NotifyScriptLoaded = true;


		return new ScriptReference[ ] { reference, referenceJQuery };
	}

	#endregion
}
