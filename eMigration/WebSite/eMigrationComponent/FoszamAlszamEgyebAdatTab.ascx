<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FoszamAlszamEgyebAdatTab.ascx.cs"
    Inherits="eMigrationComponent_FoszamAlszamEgyebAdatTab" %>

<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc5" %>
<%@ Register Src="../Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
<Services>
    <asp:ServiceReference Path="~/WrappedWebService/Ajax_eMigration.asmx" />
</Services>
</asp:ScriptManagerProxy>
<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="FoszamAlszamEgyebAdatUpdatePanel" runat="server"  OnLoad="FoszamAlszamEgyebAdatUpdatePanel_Load">
    <ContentTemplate>
  <uc1:SubListHeader ID="ListHeader1" runat="server" />
  <asp:Panel ID="MainPanel" runat="server" Visible="true">
    <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
    <asp:HiddenField ID="GridViewSelectedId" runat="server" />
    <asp:GridView ID="MIG_MetaAdatGridView" runat="server" CellPadding="0" CellSpacing="0"
            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
            AllowSorting="True" DataKeyNames="Id" AutoGenerateColumns="False" 
            Width="98%" Visible="true" 
         OnPreRender="MIG_MetaAdatGridView_PreRender"
            onrowcommand="MIG_MetaAdatGridView_RowCommand" 
            onsorting="MIG_MetaAdatGridView_Sorting">                          
            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
            <HeaderStyle CssClass="GridViewHeaderStyle" />
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                    <HeaderTemplate>
                        <span  style="white-space:nowrap">
                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                            &nbsp;&nbsp;
                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                        </span>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                            CssClass="HideCheckBoxText" />                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                </asp:CommandField>
                <asp:BoundField DataField="Cimke" HeaderText="C�mke" SortExpression="Cimke">
                    <HeaderStyle CssClass="GridViewBorderHeader" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Ertek" HeaderText="�rt�k" SortExpression="Ertek">
                    <HeaderStyle CssClass="GridViewBorderHeader" />
                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Left" />
                </asp:BoundField>                
            </Columns>
            <PagerSettings Visible="False" />
        </asp:GridView>
        <br />        
       </asp:Panel> 
        </ContentTemplate>
        </asp:UpdatePanel>
     