using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eMigration.Utility;
using Contentum.eMigration.Service;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.Query;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class eMigrationComponent_FoszamAlszamEgyebAdatTab : System.Web.UI.UserControl
{
    #region OnChangeObjectPropertiesEvent
    public event EventHandler OnChangedObjectProperties;
    public UI ui = new UI();

    private void RaiseEvent_OnChangedObjectProperties()
    {
        if (OnChangedObjectProperties != null)
        {
            OnChangedObjectProperties(this, new EventArgs());
        }
    }
    #endregion

    private string ParentId
    {
        get
        {
            string id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
                return String.Empty;
            return id;
        }
    }

    private ObjektumAzonositoTipus ObjektumAzonosito
    {
        get
        {
            if (!String.IsNullOrEmpty(ParentId))
                return ObjektumAzonositoTipus.Azonosito;
            if (!String.IsNullOrEmpty(ParentId))
                return ObjektumAzonositoTipus.Id;
            return ObjektumAzonositoTipus.Missing;
        }
    }

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }


    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    private string Command = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        //Command = Request.QueryString.Get(QueryStringVars.Command);

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //F� lista gombjainak l�that�s�g�nak be�ll�t�sa
        ListHeader1.NewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.ValidFilterVisible = false;

      

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = MIG_MetaAdatGridView;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(MIG_MetaAdatGridView);

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }

    }
 
    //UpdatePanel Load esem�nykezel�je
    protected void FoszamAlszamEgyebAdatUpdatePanel_Load(object sender, EventArgs e)
    {
        if (!MainPanel.Visible) return;

        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refresh:
                    MIG_MetaAdatGridViewBind();
                    break;
            }
        }
    }
    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
       MIG_MetaAdatGridViewBind();
    }

    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        MIG_MetaAdatGridViewBind();

    }

    protected void MIG_MetaAdatGridView_PreRender(object sender, EventArgs e)
    {
        //UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeEsemenyek);
        ListHeader1.RefreshPagerLabel();
    }

    protected void MIG_MetaAdatGridViewBind()
    {

        String sortExpression = Search.GetSortExpressionFromViewState("MIG_EgyebAdatGridView", ViewState, ""); // "Id" helyett UI_SAV, UI_YEAR DESC, UI_NUM DESC
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("MIG_EgyebAdatGridView", ViewState);

        MIG_MetaAdatGridViewBind(sortExpression, sortDirection);
    }

    protected void MIG_MetaAdatGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        ///TODO: TRY Catch
        UI.ClearGridViewRowSelection(MIG_MetaAdatGridView);
        MIG_META_ADATService service = eMigrationService.ServiceFactory.GetMIG_META_ADATService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        MIG_META_ADATSearch search = (MIG_META_ADATSearch)Search.GetSearchObject(Page, new MIG_META_ADATSearch());
        search.OBJ_ID.Value = ParentId;
        search.OBJ_ID.Operator = Query.Operators.equals;

        if (string.IsNullOrEmpty(SortExpression))
            search.OrderBy = "Id DESC";
        else
            search.OrderBy = Search.GetOrderBy("MIG_MetaAdatGridView", ViewState, SortExpression, SortDirection);

        KRT_ForditasokSearch forditasokSearch = (KRT_ForditasokSearch)Search.GetSearchObject(Page, new KRT_ForditasokSearch());

        forditasokSearch.NyelvKod.Value = System.Globalization.CultureInfo.CurrentCulture.Name;
        forditasokSearch.NyelvKod.Operator = Query.Operators.equals;
        forditasokSearch.OrgKod.Value = Contentum.eUtility.FelhasznaloProfil.OrgKod(HttpContext.Current.Session);
        forditasokSearch.OrgKod.Operator = Query.Operators.equals;
        forditasokSearch.Modul.Value = System.Configuration.ConfigurationManager.AppSettings["ApplicationName"];
        forditasokSearch.Modul.Operator = Query.Operators.equals;
        forditasokSearch.Komponens.Value = this.AppRelativeVirtualPath.Substring(2);
        forditasokSearch.Komponens.Operator = Query.Operators.equals;

          // ExecParam.Paging.PageNumber = -1;
        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);
        Result res = service.GetAllWithExtension(ExecParam, search,forditasokSearch);


        UI.GridViewFill(MIG_MetaAdatGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel1);
      
    }


    protected void MIG_MetaAdatGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(MIG_MetaAdatGridView, selectedRowNumber, "check");
        }
    }
   

    protected void MIG_MetaAdatGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        MIG_MetaAdatGridViewBind(e.SortExpression, UI.GetSortToGridView("MIG_MetaAdatGridView", ViewState, e.SortExpression));
    }

 
    public void ReLoadTab()
    {
        
        if (_ParentForm == Contentum.eMigration.Utility.Constants.ParentForms.Foszam)
        {
            if (!MainPanel.Visible) return;

            //if (Command == CommandName.View || Command == CommandName.Modify)
            //{

            if (ObjektumAzonosito == ObjektumAzonositoTipus.Missing)
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                ErrorUpdatePanel1.Update();
            }
            else
            {
                MIG_MetaAdatGridViewBind();
            }
        }
        else
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }
    }
}

