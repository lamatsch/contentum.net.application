<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FoszamAlszamTerkepTab.ascx.cs" Inherits="eMigrationComponent_FoszamAlszamTerkepTab" %>
<%@ Register Src="../Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
    
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
<%--    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />--%>
                    <asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel" UpdateMode="Always" OnLoad="TreeViewUpdatePanel_Load">
                        <ContentTemplate>    
     
     <asp:Panel ID="MainPanel" runat="server" Visible="true">
     
    <ajaxToolkit:CollapsiblePanelExtender ID="FoszamAlszamCPE" runat="server" TargetControlID="Panel1"
        CollapsedSize="20" Collapsed="False" 
         AutoCollapse="false" AutoExpand="false"    
        ExpandedSize="450"
        ScrollContents="true">
    </ajaxToolkit:CollapsiblePanelExtender>   
         
         <table style="width: 100%;">
             <tr>
                <td style="width: 100%">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 100%; text-align: left; border: solid 1px Silver; ">
                                <asp:Panel ID="Panel1" runat="server" Visible="true">                                        
                                    <asp:TreeView ID="TreeView1" runat="server" EnableClientScript="false" 
                                        OnSelectedNodeChanged="TreeView1_SelectedNodeChanged" OnTreeNodePopulate="TreeView1_TreeNodePopulate" BackColor="White" ShowLines="True" PopulateNodesFromClient="True" CollapseImageToolTip="" ExpandImageToolTip="" SkipLinkText="" NodeWrap="True">
                                        <SelectedNodeStyle Font-Bold="False" BackColor="#E0E0E0" />
                                    </asp:TreeView>
                                </asp:Panel>     
                            </td>                                   
                       </tr>
                       <tr>
                            <td style="width: 100%; height: 50px;">                               
                                <asp:Panel ID="PanelGombok" runat="server" Visible="false">
                                    <asp:HiddenField ID="HiddenField_SkontroVege" runat="server" />
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td style="width: 100%; text-align: center;">
                                                <asp:ImageButton  runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"  ID="ImageView" OnClick="FunctionButtons_Click" CommandName="View"
                                                     AlternateText="Megtekint�s" onmouseover="swapByName(this.id,'megtekintes_trap2.jpg')" onmouseout="swapByName(this.id,'megtekintes_trap.jpg')" />
                                                <asp:ImageButton  runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg" ID="ImageModify" OnClick="FunctionButtons_Click" CommandName="Modify"
                                                     AlternateText="M�dos�t�s" onmouseover="swapByName(this.id,'modositas_trap2.jpg')" onmouseout="swapByName(this.id,'modositas_trap.jpg')" />                                             
                                                <asp:ImageButton  runat="server" ImageUrl="~/images/hu/trapezgomb/skontorba_tesz_trap.jpg" ID="ImageSkontrobaHelyez" OnClick="FunctionButtons_Click" CommandName="SkontrobaHelyez"
                                                     AlternateText="Skontr�ba helyez�s" onmouseover="swapByName(this.id,'skontorba_tesz_trap2.jpg')" onmouseout="swapByName(this.id,'skontorba_tesz_trap.jpg')" />
                                                <asp:ImageButton  runat="server" ImageUrl="~/images/hu/trapezgomb/skontorbol_kivesz_trap.jpg" ID="ImageSkontrobolKivetel" OnClick="FunctionButtons_Click" CommandName="SkontrobolKivesz"
                                                     AlternateText="Skontr�b�l kiv�tel" onmouseover="swapByName(this.id,'skontorbol_kivesz_trap2.jpg')" onmouseout="swapByName(this.id,'skontorbol_kivesz_trap.jpg')" />
                                                <asp:ImageButton  runat="server" ImageUrl="~/images/hu/trapezgomb/irattar_trap.jpg" ID="ImageIrattarozasra" OnClick="FunctionButtons_Click" CommandName="Irattarozasra"
                                                     AlternateText="Iratt�rba k�ld�s" onmouseover="swapByName(this.id,'irattar_trap2.jpg')" onmouseout="swapByName(this.id,'irattar_trap.jpg')" />
                                                <asp:ImageButton  runat="server" ImageUrl="~/images/hu/trapezgomb/kiadas_osztalyra_trap.jpg" ID="ImageKiadasOsztalyra" OnClick="FunctionButtons_Click" CommandName="KiadasOsztalyra"
                                                     AlternateText="Kiad�s oszt�lyra" onmouseover="swapByName(this.id,'kiadas_osztalyra_trap2.jpg')" onmouseout="swapByName(this.id,'kiadas_osztalyra_trap.jpg')" />
                                                <asp:ImageButton  runat="server" ImageUrl="~/images/hu/trapezgomb/szereles_visszavonas_trap.gif" ID="ImageSzerelesVisszavonas" OnClick="FunctionButtons_Click"  CommandName="SzerelesVisszavonas"
                                                     AlternateText="Szerel�s visszavon�sa" onmouseover="swapByName(this.id,'szereles_visszavonas_trap2.gif')" onmouseout="swapByName(this.id,'szereles_visszavonas_trap.gif')" />
                                            </td>                                                    
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
              </tr>
           </table>
        </asp:Panel>
    </ContentTemplate>
 </asp:UpdatePanel>
