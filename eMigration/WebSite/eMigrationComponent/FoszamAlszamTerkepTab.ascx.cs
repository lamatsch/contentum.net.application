//http://localhost:100/eRecord/TreeView.aspx?Id=BE154850-F8E0-4F21-BDBD-12696D400A11
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Utility;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eQuery;

public partial class eMigrationComponent_FoszamAlszamTerkepTab : System.Web.UI.UserControl
{
    private String _SelectedId = "";

    public String SelectedId
    {
      get { return _SelectedId; }
      set { _SelectedId = value; }
    }
    private String _SelectedNode = "";

    public String SelectedNode
    {
      get { return _SelectedNode; }
      set { _SelectedNode = value; }
    }

    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            //MainPanel.Visible = value;
            PanelGombok.Visible = value;
            if (!value)
            {
                TreeView1.Nodes.Clear();
            }
        }
    }

    public string Command = "";
    UI ui = new UI();    
    private PageView pageView = null;

    public HiddenField hiddenFoszamID = null;

    private string ParentAzonosito
    {
        get
        {
            string azonsito = Request.QueryString.Get(QueryStringVars.RegiAdatAzonosito);
            if (String.IsNullOrEmpty(azonsito))
                return String.Empty;
            return Server.UrlDecode(azonsito);
        }
    }

    public String ParentId
    {
        get
        {
            if (!String.IsNullOrEmpty(ParentAzonosito))
            {
                if (hiddenFoszamID != null && !String.IsNullOrEmpty(hiddenFoszamID.Value))
                {
                    return hiddenFoszamID.Value;
                }
                else
                {
                    //id lekerese ha veletlenul meg nincs meg
                    MIG_Foszam mig_Foszam = null;
                    Result result = new Result();

                    MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    result = service.GetByAzonosito(execParam, ParentAzonosito);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        mig_Foszam = (MIG_Foszam)result.Record;
                        if (mig_Foszam != null)
                        {
                            if (hiddenFoszamID != null)
                            {
                                hiddenFoszamID.Value = mig_Foszam.Id;
                            }
                            return mig_Foszam.Id;
                        }
                    }

                    return String.Empty;
                }
            }
            else
            {
                string id = Request.QueryString.Get(QueryStringVars.Id);
                return id;
            }
        }
    }
    
    private string FoUgyirat_Id = "";


    // Az objektumban t�rt�n� v�ltoz�sokat jelz� esem�ny, ami pl. kiv�lthat egy jogosults�gellen�rz�st
    public event EventHandler OnChangedObjectProperties;

    private void RaiseEvent_OnChangedObjectProperties()
    {
        if (OnChangedObjectProperties != null)
        {
            OnChangedObjectProperties(this, new EventArgs());
        }
    }

    #region dictionary item oszt�lyok

    [Serializable]
    private class FoszamItem
    {
        private string _Id;
        private string _UI_SAV;
        private string _UI_YEAR;
        private string _UI_NUM;
        private string _UI_NAME;
        private string _UGYHOL;
        private bool _isTopLevel; // a m�dos�that�s�g (mozgat�s) vizsg�lat�hoz sz�ks�ges

        public FoszamItem()
        {
            _Id = "";
            _UI_SAV = "";
            _UI_YEAR = "";
            _UI_NUM = "";
            _UI_NAME = "";
            _UGYHOL = "";
            
            _isTopLevel = false;
        }

        public FoszamItem(string Id, string Sav, string Year, string Num, string Name, string Ugyhol, bool isTopLevel)
        {
            _Id = Id;
            _UI_SAV = Sav;
            _UI_YEAR = Year;
            _UI_NUM = Num;
            _UI_NAME = Name;
            _UGYHOL = Ugyhol;
            _isTopLevel = isTopLevel;
        }

        #region properties
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string Sav
        {
            get { return _UI_SAV; }
            set { _UI_SAV = value; }
        }

        public string Name
        {
            get { return _UI_NAME; }
            set { _UI_NAME = value; }
        }

        public string Year
        {
            get { return _UI_YEAR; }
            set { _UI_YEAR = value; }
        }

        public string Num
        {
            get { return _UI_NUM; }
            set { _UI_NUM = value; }
        }

        public string Ugyhol
        {
            get { return _UGYHOL; }
            set { _UGYHOL = value; }
        }

        public bool isTopLevel
        {
            get { return _isTopLevel; }
            set { _isTopLevel = value; }
        }
        #endregion properties

    }

    [Serializable]
    private class AlszamItem
    {
        private string _Id;
        private string _ALNO;
        //private string _UGYHOL;
        private string _MIG_Foszam_Id;

        public AlszamItem()
        {
            _Id = "";
            _ALNO = "";
            //_UGYHOL = "";
            _MIG_Foszam_Id = "";
        }

        //public AlszamItem(string Id, string Alno, string Ugyhol, string MIG_Foszam_Id)
        public AlszamItem(string Id, string Alno, string MIG_Foszam_Id)
        {
            _Id = Id;
            _ALNO = Alno;
            //_UGYHOL = Ugyhol;
            _MIG_Foszam_Id = MIG_Foszam_Id;
        }

        #region properties
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string Alno
        {
            get { return _ALNO; }
            set { _ALNO = value; }
        }

        //public string Ugyhol
        //{
        //    get { return _UGYHOL; }
        //    set { _UGYHOL = value; }
        //}

        public string MIG_Foszam_Id
        {
            get { return _MIG_Foszam_Id; }
            set { _MIG_Foszam_Id = value; }
        }
        #endregion properties

    }
    #endregion dictionary item oszt�lyok

    #region Base Tree Dictionary

    System.Collections.Generic.Dictionary<String, FoszamItem> FoszamDictionary = new System.Collections.Generic.Dictionary<string, FoszamItem>();
    System.Collections.Generic.Dictionary<String, AlszamItem> AlszamDictionary = new System.Collections.Generic.Dictionary<string, AlszamItem>();

    #endregion

    #region public Properties
    
    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    #endregion

    #region ButtonsProperty
    Boolean ViewEnabled
    {
        get { return ImageView.Enabled; }
        set
        {
            ImageView.Enabled = value;
            if (!value)
            {
                ImageView.CssClass = "disableditem";
            }
            else
            {
                ImageView.CssClass = "";
            }
        }
    }

    Boolean ModifyEnabled
    {
        get { return ImageModify.Enabled; }
        set
        {
            ImageModify.Enabled = value;
            if (!value)
            {
                ImageModify.CssClass = "disableditem";
            }
            else
            {
                ImageModify.CssClass = "";
            }
        }
    }

    Boolean SkontrobaHelyezEnabled
    {
        get { return ImageSkontrobaHelyez.Enabled; }
        set
        {
            ImageSkontrobaHelyez.Enabled = value;
            if (!value)
            {
                ImageSkontrobaHelyez.CssClass = "disableditem";
            }
            else
            {
                ImageSkontrobaHelyez.CssClass = "";
            }
        }
    }

    Boolean SkontrobolKiveszEnabled
    {
        get { return ImageSkontrobolKivetel.Enabled; }
        set
        {
            ImageSkontrobolKivetel.Enabled = value;
            if (!value)
            {
                ImageSkontrobolKivetel.CssClass = "disableditem";
            }
            else
            {
                ImageSkontrobolKivetel.CssClass = "";
            }
        }
    }
    Boolean SzerelesVisszavonasEnabled
    {
        get { return ImageSzerelesVisszavonas.Enabled; }
        set
        {
            ImageSzerelesVisszavonas.Enabled = value;
            if (!value)
            {
                ImageSzerelesVisszavonas.CssClass = "disableditem";
            }
            else
            {
                ImageSzerelesVisszavonas.CssClass = "";
            }
        }
    }

    Boolean IrattarozasraEnabled
    {
        get { return ImageIrattarozasra.Enabled; }
        set
        {
            ImageIrattarozasra.Enabled = value;
            if (!value)
            {
                ImageIrattarozasra.CssClass = "disableditem";
            }
            else
            {
                ImageIrattarozasra.CssClass = "";
            }
        }
    }

    Boolean KiadasOsztalyraEnabled
    {
        get { return ImageKiadasOsztalyra.Enabled; }
        set
        {
            ImageKiadasOsztalyra.Enabled = value;
            if (!value)
            {
                ImageKiadasOsztalyra.CssClass = "disableditem";
            }
            else
            {
                ImageKiadasOsztalyra.CssClass = "";
            }
        }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {        
        //ParentId = Request.QueryString.Get("Id");

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        // TODO: testbol!
        //MainPanel.Visible = true;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }

             //ViewState -bol visszatolti az elmentett statuszokat
            try
            {
                if (ViewState["FoszamDictionary"] != null)
                {
                    FoszamDictionary = (System.Collections.Generic.Dictionary<String, FoszamItem>)ViewState["FoszamDictionary"];
                }
                if (ViewState["AlszamDictionary"] != null)
                {
                    AlszamDictionary = (System.Collections.Generic.Dictionary<String, AlszamItem>)ViewState["AlszamDictionary"];
                }
            }
            catch
            { }
        }
        else
        {
            //ReLoadTab();        
        }
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
    }


    protected void TreeViewUpdatePanel_Load(object sender, EventArgs e)
    {
        //if (Command == CommandName.Modify)
        //{
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refresh:
                        if (!String.IsNullOrEmpty(HiddenField_SkontroVege.Value))
                        {
                            FunctionButtons_Click(ImageSkontrobaHelyez, null);
                            HiddenField_SkontroVege.Value = "";
                        }
                        RefreshSelectedNode();
                        RaiseEvent_OnChangedObjectProperties();
                        break;
                }
            }
        //}
    }


    public void ReLoadTab()
    {
        if (ParentForm != Constants.ParentForms.Foszam
            && ParentForm != Constants.ParentForms.Alszam)
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            Active = false;
        }
        else
        {
            if (!Active) return;
            
            ViewState["Loaded"] = false;
            LoadTreeView(ParentForm, ParentId);
            ViewState["Loaded"] = true;
            pageView.SetViewOnPage(Command);
        }    
    }

    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    {
        TreeView tv = (TreeView)sender;

        SelectedId = tv.SelectedValue;

        if (String.IsNullOrEmpty(SelectedId)) return;
        if (tv.SelectedNode == null) return;

        switch (tv.SelectedNode.Depth)
        {
            case 0:
                SelectedNode = Constants.ParentForms.Foszam;
                SetButtonsByFoszam();
                break;
            case 1:
                SelectedNode = Constants.ParentForms.Alszam;
                SetButtonsByAlszam();
                break;
        }

    }

    #region Buttons

    private void SetButtonsByFoszam()
    {
        // -------------- Engedelyek Beallitasa -------------------
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();
      
        FoszamItem foszamItem = null;
        FoszamDictionary.TryGetValue(SelectedId, out foszamItem);
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        if (foszamItem == null)
            return;


        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            ViewEnabled = true;

            ModifyEnabled = false;

            if (foszamItem.Id == UgyiratTipus.UtoIratMigralt.ToString())
            {
                ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratModify") && foszamItem.isTopLevel;

                if (ViewEnabled)
                {
                    ImageView.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack(eRecord.ugyiratFormUrl,
                      QueryStringVars.Command + "=" + CommandName.View + "&" +
                      QueryStringVars.Id + "=" + SelectedId,
                      Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);
                }

                if (ModifyEnabled)
                {
                    ImageModify.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack(eRecord.ugyiratFormUrl,
                      QueryStringVars.Command + "=" + CommandName.Modify + "&" +
                      QueryStringVars.Id + "=" + SelectedId,
                      Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

                    //SkontrobaHelyezEnabled = (foszamItem.Ugyhol != "1" && foszamItem.Ugyhol != "2"); // nincs skontr�ban, sem iratt�rban
                    //SkontrobolKiveszEnabled = (foszamItem.Ugyhol == "1");   // skontr�ban van
                    //IrattarozasraEnabled = (foszamItem.Ugyhol != "2");  // nincs iratt�rban
                    //KiadasOsztalyraEnabled = (foszamItem.Ugyhol != "3"); // nincs oszt�lyon
                }

                //if (SkontrobaHelyezEnabled)
                //{
                //    ImageSkontrobaHelyez.OnClientClick += JavaScripts.SetOnClientClick("SkontroVegeForm.aspx"
                //    , "Command=" + CommandName.Modify + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField_SkontroVege.ClientID
                //    , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refresh);
                //}
                //else
                //{
                //    ImageSkontrobaHelyez.OnClientClick = "";
                //}

                return;
            }
            

            if (ViewEnabled)
            {
                ImageView.OnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx"
                           , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + SelectedId
                           , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }

        }

        #endregion

        #region Csak Command==Modify eset�n enged�lyezett gombok:
        //// nem lehet m�dos�tani...
        //// Alszam csak View m�dban h�vhat�, de f�sz�mon kereszt�l �thelyezhet�,
        //// ez�rt ott View m�dban is megengedj�k a Foszam m�dos�t�s�t
        //if (Command == CommandName.Modify || ParentForm == Constants.ParentForms.Alszam)
        string felhnev = Contentum.eUtility.FelhasznaloNevek_Cache.GetFelhasznaloNevFromCache(execParam.Felhasznalo_Id, Page);
        if (Command == CommandName.Modify)
        {
            ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratModify") && foszamItem.isTopLevel;

            if (ModifyEnabled)
            {
                ImageModify.OnClientClick = JavaScripts.SetOnClientClick("FoszamForm.aspx",
                  QueryStringVars.Command + "=" + CommandName.Modify + "&" +
                  QueryStringVars.Id + "=" + SelectedId,
                  Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refresh);

                SkontrobaHelyezEnabled = (foszamItem.Ugyhol != "1" && foszamItem.Ugyhol != "2"); // nincs skontr�ban, sem iratt�rban
                SkontrobolKiveszEnabled = (foszamItem.Ugyhol == "1");   // skontr�ban van
                IrattarozasraEnabled = (foszamItem.Ugyhol != "2");  // nincs iratt�rban
                KiadasOsztalyraEnabled = (foszamItem.Ugyhol != "3"); // nincs oszt�lyon                 
            }
            ErrorDetails error;
            ExecParam ep = UI.SetExecParamDefault(Page);
            Foszamok.Statusz st = Foszamok.GetAllapotById(SelectedId, ep, EErrorPanel1);
            SzerelesVisszavonasEnabled = Foszamok.SzerelesVisszavonhato(st,ep,Page,felhnev,out error);
            ImageSzerelesVisszavonas.OnClientClick = " if (confirm('" + Resources.Question.UIConfirmHeader_ElokeszitettSzerelesFelbontasa
            + "')==false) { return false; } ";
            if (SkontrobaHelyezEnabled)
            {
                ImageSkontrobaHelyez.OnClientClick += JavaScripts.SetOnClientClick("SkontroVegeForm.aspx"
                , "Command=" + CommandName.Modify + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField_SkontroVege.ClientID
                , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refresh);
            }
            else
            {
                ImageSkontrobaHelyez.OnClientClick = "";
            }

        }

        #endregion

               
    }

    private void SetButtonsByAlszam()
    {
        SetAllButtonsEnabled(false);
        ResetAllButtonsOnClientClick();

        AlszamItem alszamItem = null;
        AlszamDictionary.TryGetValue(SelectedId, out alszamItem);
        if (alszamItem == null)
            return;

        FoszamItem foszamItem = null;
        FoszamDictionary.TryGetValue(alszamItem.MIG_Foszam_Id, out foszamItem);
        if (foszamItem == null)
            return;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        #region Modify �S View m�dban is enged�lyezett gombok:

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            SkontrobaHelyezEnabled = false;
            SkontrobolKiveszEnabled = false;
            IrattarozasraEnabled = false;
            KiadasOsztalyraEnabled = false;

            ViewEnabled = true;

            if (ViewEnabled)
            {
                ImageView.OnClientClick = JavaScripts.SetOnClientClick("AlszamForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + SelectedId
                   , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID);
            }

            if (ParentForm == Constants.ParentForms.Foszam || Command == CommandName.Modify)
            {
                //ModifyEnabled = true;

                //SkontrobaHelyezEnabled = (alszamItem.Ugyhol != "1" && alszamItem.Ugyhol != "2"); // nincs skontr�ban, sem iratt�rban
                //SkontrobolKiveszEnabled = (alszamItem.Ugyhol == "1");   // skontr�ban van
                //IrattarozasraEnabled = (alszamItem.Ugyhol != "2");  // nincs iratt�rban
                //KiadasOsztalyraEnabled = (alszamItem.Ugyhol != "3"); // nincs oszt�lyon
            }
            else
            {
                ModifyEnabled = false;

                //SkontrobaHelyezEnabled = false;
                //SkontrobolKiveszEnabled = false;
                //IrattarozasraEnabled = false;
                //KiadasOsztalyraEnabled = false;
            }

            if (ModifyEnabled)
            {
                ImageModify.OnClientClick = JavaScripts.SetOnClientClick("AlszamForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + SelectedId
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, TreeViewUpdatePanel.ClientID, EventArgumentConst.refresh);
            }

            //if (SkontrobaHelyezEnabled)
            //{
            //    ImageSkontrobaHelyez.OnClientClick += JavaScripts.SetOnClientClick("AlszamSkontroVegeForm.aspx"
            //    , "Command=" + CommandName.Modify + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField_SkontroVege.ClientID
            //    , Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refresh);
            //}
            //else
            //{
            //    ImageSkontrobaHelyez.OnClientClick = "";
            //}
        }

        #endregion
        
        #region Csak Command==Modify eset�n enged�lyezett gombok:

        if (Command == CommandName.Modify)
        {

        }

        #endregion

    }

  
    private void SetAllButtonsEnabled(Boolean value)
    {
        ViewEnabled = value;
        ModifyEnabled = value;
        SkontrobaHelyezEnabled = value;
        SkontrobolKiveszEnabled = value;
        IrattarozasraEnabled = value;
        KiadasOsztalyraEnabled = value;
        SzerelesVisszavonasEnabled = value;
    }

    private void ResetAllButtonsOnClientClick()
    {
        ImageView.OnClientClick = "";
        ImageModify.OnClientClick = "";
        ImageSkontrobaHelyez.OnClientClick = "";
        ImageSkontrobolKivetel.OnClientClick = "";
        ImageIrattarozasra.OnClientClick = "";
        ImageKiadasOsztalyra.OnClientClick = "";
        ImageSzerelesVisszavonas.OnClientClick = "";
    }

    #endregion Buttons

    private void LoadTreeView(String NodeName, String NodeId)
    {
        // st�tusz Dictionary-k t�rl�se:
        FoszamDictionary.Clear();
        AlszamDictionary.Clear();

        MIG_FoszamService service_foszam = eMigrationService.ServiceFactory.GetMIG_FoszamService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        int foszamAlszamTerkepNodeType = 0;
        
        switch (NodeName)
        {
            case Constants.ParentForms.Alszam:
                foszamAlszamTerkepNodeType = Constants.FoszamAlszamTerkepNodeTypes.Alszam;
                break;
            case Constants.ParentForms.Foszam:
                foszamAlszamTerkepNodeType = Constants.FoszamAlszamTerkepNodeTypes.Foszam;
                break;
        }

        Result result = service_foszam.GetAllFoszamAlszamTerkepDataSets(execParam, foszamAlszamTerkepNodeType, NodeId);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
            return;
        }

        DataTable table_foszam = result.Ds.Tables[Constants.TableNames.MIG_Foszam];
        DataTable table_alszam = result.Ds.Tables[Constants.TableNames.MIG_Alszam];

        switch (NodeName)
        {
            case Constants.ParentForms.Alszam:
                {
                    MIG_Alszam mig_Alszam = (MIG_Alszam)result.Record;
                    
                    // F�sz�m szint felt�lt�se:
                    TreeNode foszamNode = AddFoszamNode(table_foszam, mig_Alszam.MIG_Foszam_Id);

                    // Alsz�mok:
                    AddAlszamNode(foszamNode, table_alszam);
                    foszamNode.Expanded = true;

                    // Alsz�m Node Select
                    TreeNode selectedAlszamNode = TreeView1.FindNode(foszamNode.ValuePath + "/" + mig_Alszam.Id);
                    if (selectedAlszamNode != null)
                    {
                        selectedAlszamNode.Selected = true;
                        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                    }                                        
                }
                break;
            case Constants.ParentForms.Foszam:
                {
                    string foszamId = NodeId;

                    // F�sz�m szint felt�lt�se:
                    TreeNode foszamNode = AddFoszamNode(table_foszam, foszamId);

                    // Alsz�mok:
                    AddAlszamNode(foszamNode, table_alszam);
                    foszamNode.Expanded = true;

                    // F�sz�m Node Select
                    foszamNode.Selected = true;
                    TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
                }
                break;
        }

    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        switch (e.Node.Depth)
        {
            case 0:
                AddAlszamNode(e.Node);
                break;
            default:
                break;
        }
    }

    enum UgyiratTipus
    {
        Normal,
        EloIrat,
        UtoIrat,
        UtoIratMigralt
    }

    /// <summary>
    /// F�sz�m szint felt�lt�se
    /// </summary>
    /// <param name="FoszamId"></param>
    /// <returns></returns>
    private TreeNode AddFoszamNode(string FoszamId)
    {
        if (String.IsNullOrEmpty(FoszamId))
        {
            return null;
        }

        // itt el�g lenne a Get, de esetleges k�s�bbi ig�nyek �s kompatibilit�si okok miatt GetAll-t haszn�lunk
        #region Init
        MIG_FoszamService service_foszam = eMigrationService.ServiceFactory.GetMIG_FoszamService();
        MIG_FoszamSearch search_foszam = new MIG_FoszamSearch();
        ExecParam execParam_foszam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result_foszamGetAll = new Result();
        #endregion

        #region Search be�ll�t�s
        search_foszam.Id.Value = FoszamId;
        search_foszam.Id.Operator = Contentum.eQuery.Query.Operators.equals;
        search_foszam.Id.Group = "468";
        search_foszam.Id.GroupOperator = Query.Operators.or;

        search_foszam.Csatolva_Id.Value = FoszamId;
        search_foszam.Csatolva_Id.Operator = Query.Operators.equals;
        search_foszam.Csatolva_Id.Group = "468";
        search_foszam.Csatolva_Id.GroupOperator = Query.Operators.or;

        result_foszamGetAll = service_foszam.GetAll(execParam_foszam, search_foszam);
        if (!String.IsNullOrEmpty(result_foszamGetAll.ErrorCode))
        {
            return new TreeNode();
        }
        #endregion

        return AddFoszamNode(result_foszamGetAll.Ds.Tables[0], FoszamId);      
    }


    // F�sz�mt szint felt�lt�se
    private TreeNode AddFoszamNode(DataTable dataTable_foszam, string FoszamId)
    {
        DataRow foszamRow = null;
        DataRow szuloFoszamRow = null;
        List<DataRow> EloIratokList = new List<DataRow>();

        foreach (DataRow row in dataTable_foszam.Rows)
        {
            string row_foszamId = row["Id"].ToString();
            string row_szuloFoszamId = row["Csatolva_Id"].ToString();

            if (row_foszamId == FoszamId)
            {
                // ez a sz�banforg� f�sz�m rekord:
                if (foszamRow != null)
                {
                    // hiba, m�r volt ez a sor!
                    return null;
                }
                else
                {
                    foszamRow = row;
                }
            }
            else if (row_szuloFoszamId == FoszamId)
            {
                EloIratokList.Add(row);
            }
            else if (szuloFoszamRow == null)
            {
                // elvileg ez a sz�l� �gyirat:
                szuloFoszamRow = row;
            }
            else
            {
                // hiba, ilyen sort nem k�rt�nk!!
                return null;
            }
        }

        // Sz�l� �gyirat ellen�rz�se:
        if (szuloFoszamRow != null && foszamRow != null)
        {
            if (foszamRow["Csatolva_Id"].ToString()
                != szuloFoszamRow["Id"].ToString())
            {
                // hiba:
                return null;
            }
        }


        // EL�SZ�R A F� �GYIRATOT ADJUK HOZZ� (==TreeView1.Nodes[0]):

        if (foszamRow == null)
        {
            // hiba:
            return null;
        }

        TreeNode FoszamTreeNode = AddFoszamNode(foszamRow, UgyiratTipus.Normal);

        // el�iratok hozz�ad�sa:
        foreach (DataRow row in EloIratokList)
        {
            AddFoszamNode(row, UgyiratTipus.EloIrat);
        }

        // Ut�irat hozz�ad�sa, ha van:
        if (szuloFoszamRow != null)
        {
            AddFoszamNode(szuloFoszamRow, UgyiratTipus.UtoIrat);
        }

        string Edok_Utoirat_Id = foszamRow["Edok_Utoirat_Id"].ToString();

        if (!String.IsNullOrEmpty(Edok_Utoirat_Id))
        {
            AddFoszamNode(foszamRow, UgyiratTipus.UtoIratMigralt);
        }

        return FoszamTreeNode;
    }


    private TreeNode AddFoszamNode(DataRow dataRow_foszam, UgyiratTipus ugyiratTipus)
    {
        string FoszamId = "";
        

        FoszamId = dataRow_foszam["Id"].ToString();
        bool isTopLevel = (String.IsNullOrEmpty(dataRow_foszam["Csatolva_Id"].ToString()) && String.IsNullOrEmpty(dataRow_foszam["Csatolva_Id"].ToString()));

        if (ugyiratTipus == UgyiratTipus.Normal)
        {
            // Page_PreRender -ben kell (???)
            FoUgyirat_Id = FoszamId;
        }

        if (ugyiratTipus == UgyiratTipus.UtoIratMigralt)
        {
            FoszamId = dataRow_foszam["Edok_Utoirat_Id"].ToString();
        }

        #region TreeNode hozz�ad�sa
        // load to Dictionary 
        if (FoszamDictionary.ContainsKey(FoszamId) == false)
        {
            if (ugyiratTipus == UgyiratTipus.UtoIratMigralt)
            {
                FoszamDictionary.Add(FoszamId,
                   new FoszamItem(
                     UgyiratTipus.UtoIratMigralt.ToString()
                   , String.Empty
                   , String.Empty
                   , String.Empty
                   , String.Empty
                   , String.Empty
                   , isTopLevel));
            }
            else
            {
                FoszamDictionary.Add(FoszamId,
                   new FoszamItem(
                     FoszamId
                   , dataRow_foszam["UI_SAV"].ToString()
                   , dataRow_foszam["UI_YEAR"].ToString()
                   , dataRow_foszam["UI_NUM"].ToString()
                   , dataRow_foszam["UI_NAME"].ToString()
                   , dataRow_foszam["UGYHOL"].ToString()
                   , isTopLevel));
            }
        }

        string FoszamText = GetTreeNodeText_Foszam(dataRow_foszam, ugyiratTipus);

        TreeNode FoszamTreeNode = new TreeNode(FoszamText, FoszamId);
        if (ugyiratTipus == UgyiratTipus.UtoIratMigralt)
        {
            FoszamTreeNode.PopulateOnDemand = false;
            FoszamTreeNode.Expanded = true;
        }
        else
        {
            FoszamTreeNode.PopulateOnDemand = true;
            FoszamTreeNode.Expanded = false;
        }

        TreeView1.Nodes.Add(FoszamTreeNode);
        ViewState["FoszamDictionary"] = FoszamDictionary;

        return FoszamTreeNode;

        #endregion


    }

    private void AddAlszamNode(TreeNode FoszamTreeNode)
    {
        #region Init
        MIG_AlszamService service_alszam = eMigrationService.ServiceFactory.GetMIG_AlszamService();
        MIG_AlszamSearch search_alszam = new MIG_AlszamSearch();
        ExecParam execParam_alszam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result_alszamGetAll = new Result();
        #endregion

        #region Search be�ll�t�s
        search_alszam.MIG_Foszam_Id.Operator = Contentum.eQuery.Query.Operators.equals;
        search_alszam.MIG_Foszam_Id.Value = FoszamTreeNode.Value;
        result_alszamGetAll = service_alszam.GetAll(execParam_alszam, search_alszam);
        if (!String.IsNullOrEmpty(result_alszamGetAll.ErrorCode))
        {
            return;
        }
        #endregion

        #region DS lek�r�s

        AddAlszamNode(FoszamTreeNode, result_alszamGetAll.Ds.Tables[0]);
        
        #endregion
    }

    private void AddAlszamNode(TreeNode FoszamTreeNode, DataTable dataTable_alszam)
    {
        foreach (DataRow row in dataTable_alszam.Rows)
        {
            AddAlszamNode(row, FoszamTreeNode);
        }

    }


    private void AddAlszamNode(DataRow dataRow_alszam, TreeNode FoszamTreeNode)
    {
        string foszamId = FoszamTreeNode.Value;
        string alszamId = dataRow_alszam["Id"].ToString();

        String AlszamText = GetTreeNodeText_Alszam(dataRow_alszam);
        

        TreeNode alszamTreeNode = new TreeNode(AlszamText, alszamId);
        alszamTreeNode.PopulateOnDemand = true;
        alszamTreeNode.Expanded = false;
        FoszamTreeNode.ChildNodes.Add(alszamTreeNode);

        // load to Dictionary 
        if (AlszamDictionary.ContainsKey(alszamId) == false)
        {
            AlszamDictionary.Add(alszamId,
                new AlszamItem (
                    alszamId
                    , dataRow_alszam["ALNO"].ToString()
                    //, dataRow_alszam["UGYHOL"].ToString()
                    , dataRow_alszam["MIG_Foszam_Id"].ToString()
                    ));
        }

        ViewState["AlszamDictionary"] = AlszamDictionary;
    }

    private string GetTreeNodeText_Alszam(DataRow datarow_alszam)
    {
        String AlszamText = "";

        String foszamId = datarow_alszam["MIG_Foszam_Id"].ToString();
        String alszamId = datarow_alszam["Id"].ToString();

        AlszamText += "<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD' style='min-width: 20px;'>"
                + SetAlszamUrl("<img src='images/hu/egyeb/treeview_irat.gif' alt='Alsz�m' />", foszamId, alszamId) + "&nbsp;</td>";

        AlszamText += "<td style='width: 100px; min-width: 100px; padding-right: 10px;'>";
        AlszamText += SetAlszamUrl("<b> Alsz�m: </b>" + datarow_alszam["ALNO"].ToString(), foszamId, alszamId);
        AlszamText += "</td><td style='width: 150px; min-width: 100px; padding-right: 10px;'>";
        AlszamText += SetAlszamUrl("<b> Hiv.sz.: </b>" + datarow_alszam["IDNUM"].ToString(), foszamId, alszamId);
        AlszamText += "</td><td style='width: 200px; min-width: 100px; padding-right: 10px;'>";
        AlszamText += SetAlszamUrl("<b> Bek�ld�: </b>" + datarow_alszam["BKNEV"].ToString(), foszamId, alszamId);
        AlszamText += "</td><td style='width: 220px; min-width: 100px; padding-right: 10px;'>";
        AlszamText += SetAlszamUrl("<b> El�ad�: </b>" + datarow_alszam["EloadoNev"].ToString(), foszamId, alszamId);
        //AlszamText += "</td><td style='width: 100px; min-width: 100px; padding-right: 10px;'>";
        //AlszamText += SetAlszamUrl("<b> Irat helye: </b>" + IratHelye.GetTextFormKod(datarow_alszam["UGYHOL"].ToString()), foszamId, alszamId);
        //AlszamText += "</td><td style='width: 100px; min-width: 100px; padding-right: 10px;'>";
        //AlszamText += SetAlszamUrl("<b> Iratt�rba: </b>" + (datarow_alszam["IRATTARBA"].ToString().Length > 11 ? datarow_alszam["IRATTARBA"].ToString().Substring(0, 11) : datarow_alszam["IRATTARBA"].ToString()), foszamId, alszamId);
        //AlszamText += "</td><td style='width: 100px; min-width: 100px; padding-right: 10px;'>";
        //AlszamText += SetAlszamUrl("<b> Skontr�ba: </b>" + (datarow_alszam["SCONTRO"].ToString().Length > 11 ? datarow_alszam["SCONTRO"].ToString().Substring(0, 11) : datarow_alszam["SCONTRO"].ToString()), foszamId, alszamId);
        AlszamText += "</td><td style='width: auto; min-width: 100px;'>";
        AlszamText += SetAlszamUrl("<b> Megj.: </b>" + datarow_alszam["MJ"].ToString(), foszamId, alszamId);
        AlszamText += "</td></tr></table>";

        return AlszamText;
    }

    private string GetTreeNodeText_Foszam(DataRow datarow_foszam, UgyiratTipus ugyiratTipus)
    {
        string elotag = "";
        if (ugyiratTipus == UgyiratTipus.EloIrat)
        {
            elotag = "[El�irat]<br />";
        }
        else if (ugyiratTipus == UgyiratTipus.UtoIrat)
        {
            elotag = "[Ut�irat]<br />";
        }

        String FoszamText = "";

        String foszamId = datarow_foszam["Id"].ToString();

        if (ugyiratTipus == UgyiratTipus.UtoIratMigralt)
        {
            foszamId = datarow_foszam["Edok_Utoirat_Id"].ToString();
            string FoszamAzonosito = String.Empty;
            FoszamAzonosito = datarow_foszam["Edok_Utoirat_Azon"].ToString();

            if (String.IsNullOrEmpty(FoszamAzonosito))
            {
                FoszamAzonosito = "Nincs megadva azonos�t�";
            }

            elotag = "[Ut�irat, 2007 ut�ni]&nbsp;";

            FoszamText = "<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'>"
            + SetFoszamUrl("<img src='images/hu/egyeb/treeview_ugyirat.gif' alt='F�sz�m' />", foszamId) + "&nbsp;</td>";

            FoszamText += "<td style='width: 300px;'>";
            FoszamText += SetFoszamUrl(elotag + "<b>" + FoszamAzonosito + "</b>", foszamId);
            FoszamText += "</td></tr></table>";

            return FoszamText;

        }

        FoszamText = "<table border='1' cellspacing='0' cellpadding='0' class='TreeViewRowTable'><tr><td rowspan='2' class='TreeViewRowTD'><div style='height:100%;vertical-align:middle;'>"
            + SetFoszamUrl("<img src='images/hu/egyeb/treeview_ugyirat.gif' alt='F�sz�m' />", foszamId) + "&nbsp;</div></td>";
        
        FoszamText += "<td style='width: 110px;'>";
        FoszamText += SetFoszamUrl(elotag + "<b>Iktat�k�nyv: </b>" + datarow_foszam["UI_SAV"].ToString() + "", foszamId);
        FoszamText += "</td><td style='width: 75px;'>";
        FoszamText += SetFoszamUrl("<b> �v: </b>" + datarow_foszam["UI_YEAR"].ToString() + "&nbsp;&nbsp;", foszamId);
        FoszamText += "</td><td style='width: 100px;'>";
        FoszamText += SetFoszamUrl("<b> F�sz�m: </b>" + datarow_foszam["UI_NUM"].ToString(), foszamId);
        FoszamText += "</td><td style='width: 150px;'>";
        FoszamText += SetFoszamUrl("<b> Iratt�ri jel: </b>" + datarow_foszam["IRJ2000"].ToString(), foszamId);
        FoszamText += "</td><td style='width: 200px;'>";
        FoszamText += SetFoszamUrl("<b> �gyf�l: </b>" + datarow_foszam["UI_NAME"].ToString(), foszamId);
        FoszamText += "</td><td style='width: 200px;'>";
        FoszamText += SetFoszamUrl("<b> T�rgy: </b>" + datarow_foszam["MEMO"].ToString(), foszamId);
        FoszamText += "</td><td style='width: 100px;'>";
        FoszamText += SetFoszamUrl("<b> Azonos�t�: </b>" + datarow_foszam["UI_OT_ID"].ToString(), foszamId);
        FoszamText += "</td><td style='width: 100px;'>";
        FoszamText += SetFoszamUrl("<b> Irat helye: </b>" + IratHelye.GetTextFormKod(datarow_foszam["UGYHOL"].ToString(),Page), foszamId);
        FoszamText += "</td><td style='width: 100px;'>";
        FoszamText += SetFoszamUrl("<b> Iratt�rba: </b>" + (datarow_foszam["IRATTARBA"].ToString().Length > 11 ? datarow_foszam["IRATTARBA"].ToString().Substring(0, 11) : datarow_foszam["IRATTARBA"].ToString()), foszamId);
        FoszamText += "</td><td style='width: 100px;'>";
        FoszamText += SetFoszamUrl("<b> Skontr� v�ge: </b>" + (datarow_foszam["SCONTRO"].ToString().Length > 11 ? datarow_foszam["SCONTRO"].ToString().Substring(0, 11) : datarow_foszam["SCONTRO"].ToString()), foszamId);

        FoszamText += "</td></tr></table>";
        
        return FoszamText;
    }


    // TreeView friss�t�se a kijel�lt sor szintj�ig
    private void RefreshSelectedNode()
    {
        TreeNode selectedTreeNode = TreeView1.SelectedNode;

        if (selectedTreeNode == null || String.IsNullOrEmpty(selectedTreeNode.Value))
        {
            return;
        }
        
        // Node m�lys�g d�nti el a t�pust 
        switch (selectedTreeNode.Depth)
        {
            case 0:
                #region F�sz�mok:
                // A f� �gyiratra friss�t�nk
                string foszamId = TreeView1.Nodes[0].Value;

                TreeView1.Nodes.Clear();

                LoadTreeView(Constants.ParentForms.Foszam, foszamId);
                #endregion
                break;
            case 1:
                #region Alsz�mok:

                //string alszamId = selectedTreeNode.Value;
                ////TreeView1.Nodes.Clear();
                ////LoadTreeView(Constants.ParentForms.Alszam, alszamId);

                //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                //Result result = null;

                //MIG_AlszamService service = eMigrationService.ServiceFactory.GetMIG_AlszamService();
                //MIG_AlszamSearch search = new MIG_AlszamSearch();
                //search.Id.Value = alszamId;
                //search.Id.Operator = Query.Operators.equals;
                //// GetAll, hogy datarowt kapjunk
                //result = service.GetAll(execParam, search);

                //if (String.IsNullOrEmpty(result.ErrorCode))
                //{
                //    if (result.Ds.Tables[0].Rows.Count > 0)
                //    {
                //        DataRow dataRow = result.Ds.Tables[0].Rows[0];
                //        UpdateAlszamNode(dataRow, selectedTreeNode);
                //    }
                //}

                #endregion
                break;
        }
    }

    private void UpdateFoszamNode(DataRow dataRow_foszam, TreeNode FoszamNode)
    {
        if (FoszamNode == null || dataRow_foszam == null)
            return;

        String foszamId = dataRow_foszam["Id"].ToString();
        bool isTopLevel = (String.IsNullOrEmpty(dataRow_foszam["Csatolva_Id"].ToString()) && String.IsNullOrEmpty(dataRow_foszam["Edok_Utoirat_Id"].ToString()));
        
        // update in Dictionary 
        if (FoszamDictionary.ContainsKey(foszamId) == true)
        {
            FoszamItem foszamItem = null;
            FoszamDictionary.TryGetValue(foszamId, out foszamItem);

            // csak UGYHOL, SCONTRO �s IRATTARBA v�ltozhat
            foszamItem.Ugyhol = dataRow_foszam["UGYHOL"].ToString();

            FoszamDictionary.Remove(foszamId);
            FoszamDictionary.Add(foszamId, foszamItem);

        }
        else
        {
            // ide nem szabad bej�nnie...
            FoszamItem foszamItem = new FoszamItem(
                 foszamId
               , dataRow_foszam["UI_SAV"].ToString()
               , dataRow_foszam["UI_YEAR"].ToString()
               , dataRow_foszam["UI_NUM"].ToString()
               , dataRow_foszam["UI_NAME"].ToString()
               , dataRow_foszam["UGYHOL"].ToString()
               , isTopLevel);
            FoszamDictionary.Add(foszamId, foszamItem);
        }

        ViewState["FoszamDictionary"] = FoszamDictionary;

        //ugyirat t�pus meghat�roz�sa: csak norm�l �gyiratot lehet m�dos�tani, vagyis ami nem szerelt
        if (isTopLevel)
        {
            FoszamNode.Text = GetTreeNodeText_Foszam(dataRow_foszam, UgyiratTipus.Normal);
        }

        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
    }

    private void UpdateAlszamNode(DataRow dataRow_alszam, TreeNode AlszamNode)
    {
        if (AlszamNode == null || dataRow_alszam == null)
            return;

        String alszamId = dataRow_alszam["Id"].ToString();

        // update in Dictionary 
        if (AlszamDictionary.ContainsKey(alszamId) == true)
        {
            AlszamItem alszamItem = null;
            AlszamDictionary.TryGetValue(alszamId, out alszamItem);

            //// csak UGYHOL, SCONTRO �s IRATTARBA v�ltozhat
            //alszamItem.Ugyhol = dataRow_alszam["UGYHOL"].ToString();

            AlszamDictionary.Remove(alszamId);
            AlszamDictionary.Add(alszamId, alszamItem);

        }
        else
        {
            // ide nem szabad bej�nnie...
            AlszamItem alszamItem = new AlszamItem(
                    alszamId
                    , dataRow_alszam["ALNO"].ToString()
                    //, dataRow_alszam["UGYHOL"].ToString()
                    , dataRow_alszam["MIG_Foszam_Id"].ToString()
                    );
            AlszamDictionary.Add(alszamId, alszamItem);
        }

        ViewState["AlszamDictionary"] = AlszamDictionary;

        AlszamNode.Text = GetTreeNodeText_Alszam(dataRow_alszam);

        TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());

    }

    private String SetFoszamUrl(String szoveg, String Foszam_Id)
    {
        return "<a href=\"javascript:__doPostBack('" + TreeView1.UniqueID + "','s" + Foszam_Id + "');\">" + szoveg + "</a>";
    }
    private String SetAlszamUrl(String szoveg, String Foszam_Id, String Alszam_Id)
    {
        return "<a href=\"javascript:__doPostBack('" + TreeView1.UniqueID + "','s" + Foszam_Id + "\\\\" + Alszam_Id + "');\">" + szoveg + "</a>";
    }
 
    // Funkci�gombok megnyom�s�nak lekezel�se
    protected void FunctionButtons_Click(object sender, ImageClickEventArgs e)
    {        
        TreeNode selectedTreeNode = TreeView1.SelectedNode;        
        if (selectedTreeNode == null)
        {
            return;
        }

        SelectedId = selectedTreeNode.Value;

        if (selectedTreeNode.Depth == 0)    // Foszam node
        {
            switch ((sender as ImageButton).CommandName)
            {
                case CommandName.Irattarozasra:
                    IrattarbaSelectedFoszam(SelectedId);
                    RefreshSelectedNode();
                    RaiseEvent_OnChangedObjectProperties();
                    break;
                case CommandName.KiadasOsztalyra:
                    KiadasOsztalyraSelectedFoszam(SelectedId);
                    RefreshSelectedNode();
                    RaiseEvent_OnChangedObjectProperties();
                    break;
                case CommandName.SkontrobaHelyez:
                    SkontrobaSelectedFoszam(SelectedId);
                    //RefreshSelectedNode();
                    //RaiseEvent_OnChangedObjectProperties();
                    break;
                case CommandName.SkontrobolKivesz:
                    SkontrobolKiSelectedFoszam(SelectedId);
                    RefreshSelectedNode();
                    RaiseEvent_OnChangedObjectProperties();
                    break;
                case CommandName.SzerelesVisszavonas:
                    SzerelesVisszavonasSelectedFoszam(SelectedId);
                    RefreshSelectedNode();

                    break;

            }
        }

    }

    private void SkontrobolKiSelectedFoszam(String Id)
    {
        if (!String.IsNullOrEmpty(SelectedId))
        {
            MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            xpm.Record_Id = Id;
            Result res = svc.SkontrobolKivesz(xpm);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }

    }
    private void SzerelesVisszavonasSelectedFoszam(String Id)
    {
        if (!String.IsNullOrEmpty(Id))
        {
            // BUG_15419 El�iratain�l Szerel�s visszavon�s
            using (EREC_UgyUgyiratokService svc = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService())
            {
                // Meg�llap�tjuk, hogy EDOK-al szerelt vagy r�gi migr�lt adattal
                ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
                String szulo_Ugyirat_Id = "";

                using (MIG_FoszamService mIG_FoszamService = eMigrationService.ServiceFactory.GetMIG_FoszamService())
                {
                    ExecParam utoirat_Azon_ExecParam = xpm.Clone();
                    utoirat_Azon_ExecParam.Record_Id = Id;
                    Result utoirat_Azon_Result = mIG_FoszamService.Get(utoirat_Azon_ExecParam);
                    if (String.IsNullOrEmpty(utoirat_Azon_Result.ErrorCode))
                    {
                        szulo_Ugyirat_Id = (utoirat_Azon_Result.Record as MIG_Foszam).Edok_Utoirat_Id;
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, utoirat_Azon_Result);
                        return;
                    }
                }

                // Ha szerel�s �j (EDOK) �gyirattal t�rt�nt
                if (!String.IsNullOrEmpty(szulo_Ugyirat_Id))
                {
                    // Lek�rj�k az �gyiratot a kapcsolatok t�bl�b�l, kivessz�k bel�le a (r�gi/migr�lt) szerelt �gyirat azonos�t�j�t, amellyel elv�gezz�k a szerel�s megsz�ntet�s�t, mintha az eRecord fel�letr�l ind�tottuk volna
                    using (Contentum.eRecord.Service.EREC_UgyiratObjKapcsolatokService ugyiratobjkapcsolatokservice = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyiratObjKapcsolatokService())
                    {
                        ExecParam ugyiratobjkapcsolatokservice_ExecParam = xpm.Clone();
                        EREC_UgyiratObjKapcsolatokSearch erec_ugyiratobjkapcsolatoksearch = new EREC_UgyiratObjKapcsolatokSearch();
                        erec_ugyiratobjkapcsolatoksearch.Obj_Id_Kapcsolt.Value = szulo_Ugyirat_Id;
                        erec_ugyiratobjkapcsolatoksearch.Obj_Id_Kapcsolt.Operator = Contentum.eQuery.Query.Operators.equals;

                        erec_ugyiratobjkapcsolatoksearch.KapcsolatTipus.Value = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MigraltSzereles;
                        erec_ugyiratobjkapcsolatoksearch.KapcsolatTipus.Operator = Contentum.eQuery.Query.Operators.equals;

                        Result ugyiratobjkapcsolatokservice_Result = ugyiratobjkapcsolatokservice.GetAll(ugyiratobjkapcsolatokservice_ExecParam, erec_ugyiratobjkapcsolatoksearch);
                        
                        if (!String.IsNullOrEmpty(ugyiratobjkapcsolatokservice_Result.ErrorCode))
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, ugyiratobjkapcsolatokservice_Result);
                            return;
                        }

                        // A kapcsolatb�l kider�tj�k a migr�lt szerelt �gyirat azonos�t�j�t
                        if (ugyiratobjkapcsolatokservice_Result.Ds.Tables.Count > 0 && ugyiratobjkapcsolatokservice_Result.Ds.Tables[0].Rows.Count > 0)
                        {
                            Result res = svc.RegiAdatSzerelesVisszavonasa(xpm, szulo_Ugyirat_Id, ugyiratobjkapcsolatokservice_Result.Ds.Tables[0].Rows[0]["Leiras"].ToString());
                            if (!String.IsNullOrEmpty(res.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                            }
                        }
                    }
                }
                else // Ha szerel�s r�gi migr�lt �gyirattal t�rt�nt
                {
                    using (MIG_FoszamService mig_FoszamService = eMigrationService.ServiceFactory.GetMIG_FoszamService())
                    {
                        ExecParam mig_FoszamService_ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                        Result res = mig_FoszamService.FoszamSzerelesMig(mig_FoszamService_ExecParam, Id, ParentId, true);
                        if (!String.IsNullOrEmpty(res.ErrorCode))
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                        }
                    }
                }
            }
        }
    }

    private void SkontrobaSelectedFoszam(String Id)
    {
        if (!String.IsNullOrEmpty(HiddenField_SkontroVege.Value))
        {
            if (!String.IsNullOrEmpty(SelectedId))
            {
                MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
                ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
                xpm.Record_Id = Id;
                Result res = svc.SkontrobaHelyez(xpm, HiddenField_SkontroVege.Value);
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                }
            }
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_56008);
        }

    }

    private void IrattarbaSelectedFoszam(String Id)
    {
        if (!String.IsNullOrEmpty(SelectedId))
        {
            MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            xpm.Record_Id = Id;
            Result res = svc.IrattarbaTesz(xpm);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }
    }

    private void KiadasOsztalyraSelectedFoszam(String Id)
    {
        if (!String.IsNullOrEmpty(SelectedId))
        {
            MIG_FoszamService svc = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            xpm.Record_Id = Id;
            Result res = svc.KiadasOsztalyra(xpm);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
            }
        }

    }

    

}
