<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FoszamFormTab.ascx.cs"
    Inherits="eMigrationComponent_FoszamTab" %>

<%@ Register Src="../Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="fh" %>
<%@ Register Src="../Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc3" %>
<%@ Register Src="../Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc11" %>
<%@ Register Src="TabFooter.ascx" TagName="TabFooter" TagPrefix="uc5" %>
<%@ Register Src="~/eMigrationComponent/IrattariJelTextBox.ascx" TagName="IrattariJelTextBox" TagPrefix="uc"%>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
<Services>
    <asp:ServiceReference Path="~/WrappedWebService/Ajax_eMigration.asmx" />
</Services>
</asp:ScriptManagerProxy>
<%--Hiba megjelenites--%>
    
    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
<asp:UpdatePanel ID="FoszamUpdatePanel" runat="server" OnLoad="FoszamUpdatePanel_Load">
    <ContentTemplate>
        <script type="text/javascript">
            var isIrattariJelChecked = false;
            function IsIrattariJelChanged() {
                var oldIrj = $get('<%=HiddenField_IRJ2000Eredeti.ClientID %>').value;
                var irj = $get('<%=textIRJ2000.TextBox.ClientID %>').value;
                return (irj != '' && oldIrj != irj);
            }
            function CheckIrattariJel(buttonClientId) {
                if (!isIrattariJelChecked && IsIrattariJelChanged()) {
                    var felhId = '<%=GetFelhasznaloId() %>';
                    var ev = $get('<%=textUI_YEAR.ClientID %>').value;
                    var irj = $get('<%=textIRJ2000.TextBox.ClientID %>').value;
                    var context = buttonClientId;
                    Ajax_eMigration.IsIrattariJelExists(felhId, ev, irj, OnCheckIrattariJelCompleted, OnCheckIrattariJelError, context);
                    return true;
                }

                return false;
            }
            function OnCheckIrattariJelCompleted(result, context) {
                isIrattariJelChecked = true;
                if (!result) {
                    var insert = confirm('A megadott iratt�ri jel nem szerepel a rendszerben. Kiv�nja felvenni?');
                    if (insert) {
                        var ev = $get('<%=textUI_YEAR.ClientID %>').value;
                        var irj = $get('<%=textIRJ2000.TextBox.ClientID %>').value;
                        var url = 'IrattariJelForm.aspx?Command=New&IrattariJel=' + irj + '&Ev=' + ev;
                        popup(url, 800, 600);
                    }
                }
                var buttonClientId = context;
                $get(buttonClientId).click();
                isIrattariJelChecked = false;
            }
            function OnCheckIrattariJelError(result, context) {
                isIrattariJelChecked = true;
                alert("Hiba l�pett fel az iratt�ri jel ellen�rz�se sor�n!\n" + result.get_message());
                var buttonClientId = context;
                $get(buttonClientId).click();
            }
        </script>
        <asp:Panel ID="MainPanel" runat="server" Visible="true">
            <asp:HiddenField ID="Record_Ver_HiddenField" runat="server"></asp:HiddenField>
            <table cellspacing="0" cellpadding="0" Width="100%">
                <tbody>
                    <tr>
                        <td valign="top">
                   <eUI:eFormPanel ID="EFormPanel1" runat="server" >
                       <%--BLG_8481--%>
                       <%-- <table cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                            </tbody>
                        </table>--%>
                        <asp:Panel ID="PostaiCim_Panel" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapNyitoSor">
                                        <td class="mrUrlapCaption">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                        <td class="mrUrlapMezo">
                                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    </tr>
                                    <%--BLG_8481--%>
                                    <tr class="urlapSor">
                                       
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUI_YEAR" runat="server" Text="�v:"></asp:Label>&nbsp;</td>
                                      
                                        <td colspan="3">
                                            <table>
                                                <tbody>
                                                    <tr>

                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="textUI_YEAR" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                                        </td>

                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelUI_SAV" runat="server" Text="Iktat�k�nyv:"></asp:Label></td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="textUI_SAV" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelUI_NUM" runat="server" Text="F�sz�m:"></asp:Label></td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="textUI_NUM" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--<tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUI_NUM" runat="server" Text="F�sz�m:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUI_NUM" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelIRJ2000" runat="server" Text="Iratt�ri jel:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo" colspan ="3">
                                            <uc:IrattariJelTextBox ID="textIRJ2000" runat="server" EvTextBoxControlID="textUI_YEAR" IsEvFilterDisabled="true" CssClass="mrUrlapInput"/>
                                            <asp:HiddenField ID="HiddenField_IRJ2000Eredeti" runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelUI_NAME" runat="server" Text="�gyf�l:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo" colspan ="3">
                                            <asp:TextBox ID="textUI_NAME" runat="server" CssClass="mrUrlapInput" Width="95%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelUI_IRSZ" runat="server" Text="Ir�ny�t�sz�m:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUI_IRSZ" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelVAROSNEV" runat="server" Text="V�ros:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo" >
                                            <asp:TextBox ID="textVAROSNEV" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%--<tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelVAROSNEV" runat="server" Text="V�ros:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textVAROSNEV" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelUI_UTCA" runat="server" Text="Utca:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUI_UTCA" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelUI_HSZ" runat="server" Text="H�zsz�m:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUI_HSZ" runat="server" CssClass="mrUrlapInputRovid"></asp:TextBox>
                                        </td>
                                       
                                    </tr>
                                    <%--<tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUI_HSZ" runat="server" Text="H�zsz�m:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUI_HSZ" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUI_HRSZ" runat="server" Text="Helyrajzi sz�m:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUI_HRSZ" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelMEMO" runat="server" Text="T�rgy:"></asp:Label></td>
                                        <td class="mrUrlapMezo" colspan="3">
                                            <asp:TextBox ID="textMEMO" runat="server" CssClass="mrUrlapInput" Width="95%" TextMode="MultiLine"></asp:TextBox></td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelEloado" runat="server" Text="El�ad�:" />
                                        </td>
                                        <td class="mrUrlapMezo" colspan="3">
                                            <asp:TextBox ID="textEloado" runat="server" CssClass="mrUrlapInput" />
                                            <asp:ImageButton TabIndex = "-1" ID="LovImageButtonEloado" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')" 
                                                CssClass="mrUrlapInputImageButton" AlternateText="Kiv�laszt" />
                                            <asp:ImageButton TabIndex = "-1" ID="ResetImageButtonEloado" runat="server" ImageUrl="~/images/hu/egyeb/reset_icon.png" onmouseover="swapByName(this.id,'reset_icon_keret.png')" onmouseout="swapByName(this.id,'reset_icon.png')"
                                                CssClass="mrUrlapInputImageButton" AlternateText="Alap�llapot"/>
                                            <asp:HiddenField ID="HiddenField_EdokUgyintezoCsoportId" runat="server" />
                                            <asp:HiddenField ID="HiddenField_EloadoNevEredeti" runat="server" />
                                        </td>
                                    </tr>                                    
                                    
                                   <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelIratHelye" runat="server" Text="Irat helye:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:HiddenField ID="HiddenField_IratEredetiHelye" runat="server" />
                                            <asp:DropDownList ID="ddlistIratHelye" runat="server" CssClass="mrUrlapInputComboBox" ></asp:DropDownList>
                                        </td>
                                       <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelUgyirat_tipus" runat="server" Text="Irat t�pusa:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUgyirat_tipus" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%--<tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUgyirat_tipus" runat="server" Text="Irat t�pusa:"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textUgyirat_tipus" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelHatarido" runat="server" Text="Hat�rid�"></asp:Label>
                                            </td>
                                        <td class="mrUrlapMezo">
                                            <asp:HiddenField ID="HiddenField_Hatarido" runat="server" />
                                            <uc3:Calendarcontrol ID="CalendarHatarido" runat="server" Validate="false" />
                                        </td>
                                        
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelSkontro" runat="server" Text="Skontr� v�ge:">
                                            </asp:Label>
                                            </td>
                                        <td class="mrUrlapMezo" >
                                            <asp:HiddenField ID="HiddenField_SkontroEredeti" runat="server" />
                                            <uc3:CalendarControl ID="CalendarSkontro" runat="server" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelIRATTARBA" runat="server" Text="Iratt�rba helyez�s d�tuma:"></asp:Label>
                                            </td>
                                        <td class="mrUrlapMezo" >
                                            <asp:HiddenField ID="HiddenField_IrattarbaEredeti" runat="server" />
                                            <uc3:Calendarcontrol ID="CalendarIrattarba" runat="server" Validate="false" />
                                        </td>
                                          <%--BLG_236--%>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelIrattariHely" runat="server" Text="Iratt�ri hely:" CssClass="mrUrlapInputWaterMarked"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="TextIrattariHely" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                       
                                    </tr>
                                    <%--<tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelSkontro" runat="server" Text="Skontr� v�ge:">
                                            </asp:Label>
                                            </td>
                                        <td class="mrUrlapMezo">
                                            <asp:HiddenField ID="HiddenField_SkontroEredeti" runat="server" />
                                            <uc3:CalendarControl ID="CalendarSkontro" runat="server" Validate="false" />
                                        </td>
                                    </tr>--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelFeladat" runat="server" Text="Feladat:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" colspan="3">
                                            <asp:TextBox ID="textFeladat" runat="server" CssClass="mrUrlapInput" Width="95%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelSzervezet" runat="server" Text="Szervezet:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" colspan="3">
                                            <asp:TextBox ID="textSzervezet" runat="server" CssClass="mrUrlapInput"  Width="95%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelKikero" runat="server" Text="Kik�r�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="textKikero" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                         <td class="mrUrlapCaption_short">
                                            <asp:Label ID="labelIrattarbolKikeres_Datuma" runat="server" Text="Kik�r�s d�tuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc3:CalendarControl ID="CalendarIrattarbolKikeres_Datuma" runat="server"></uc3:CalendarControl>
                                        </td>
                                    </tr>
                                    <%--<tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIrattarbolKikeres_Datuma" runat="server" Text="Kik�r�s d�tuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc3:CalendarControl ID="CalendarIrattarbolKikeres_Datuma" runat="server"></uc3:CalendarControl>
                                        </td>
                                    </tr>--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelMegorzesiIdo" runat="server" Text="Meg�rz�si id�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" colspan="3">
                                            <uc3:CalendarControl ID="CalendarMegorzesiIdo" runat="server" Validate="false"></uc3:CalendarControl>
                                        </td>
                                    </tr>
                                    <%--BLG_236--%>
                                       <%--<tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIrattariHely" runat="server" Text="Iratt�ri hely:" CssClass="mrUrlapInputWaterMarked"></asp:Label>&nbsp;</td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="TextIrattariHely" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                        </td>
                                    </tr>--%>
                                </tbody>
                            </table>
                        </asp:Panel>
                        &nbsp;<!-- Egy�b c�mek -->
                       
            </eUI:eFormPanel>
            <uc5:TabFooter ID="TabFooter1" runat="server"></uc5:TabFooter>
        </tbody>
    </table></asp:Panel> 
</ContentTemplate>
</asp:UpdatePanel>
