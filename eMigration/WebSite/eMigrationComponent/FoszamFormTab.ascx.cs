using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eMigration.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using System.Text;
using Contentum.eRecord.Service;

public partial class eMigrationComponent_FoszamTab : System.Web.UI.UserControl
{

    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    //private const string kodcsoportCim = "CIM_TIPUS";
    //private readonly string jsModifyAlert = "alert('" + Resources.Form.MIG_FoszamModifyWarning + "');";

    public HiddenField hiddenFoszamID = null;

    public void SaveFoszamId(string id)
    {
        if (hiddenFoszamID != null)
            hiddenFoszamID.Value = id;
    }

    private string ParentAzonosito
    {
        get
        {
            string azonsito = Request.QueryString.Get(QueryStringVars.RegiAdatAzonosito);
            if (String.IsNullOrEmpty(azonsito))
                return String.Empty;
            return Server.UrlDecode(azonsito);
        }
    }
    private string ParentId
    {
        get
        {
            string id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
                return String.Empty;
            return id;
        }
    }

    private ObjektumAzonositoTipus ObjektumAzonosito
    {
        get
        {
            if (!String.IsNullOrEmpty(ParentAzonosito))
                return ObjektumAzonositoTipus.Azonosito;
            if (!String.IsNullOrEmpty(ParentId))
                return ObjektumAzonositoTipus.Id;
            return ObjektumAzonositoTipus.Missing;
        }
    }

    private String _ParentForm = "";

    public String ParentForm
    {
        get { return _ParentForm; }
        set { _ParentForm = value; }
    }

    // A Parent formr�l kell be�ll�tani, hogy innen tudjuk �ll�tani a form c�met
    public Component_FormHeader FormHeader = null;


    public Boolean _Active = false;

    public Boolean Active
    {
        get { return _Active; }
        set
        {
            _Active = value;
            MainPanel.Visible = value;
        }
    }

    // A sz�ban forg� �gyirat objektum (megtekint�s, m�dos�t�s eset�n haszn�latos)
    public MIG_Foszam obj_MIG_Foszam = null;


    // Az objektumban t�rt�n� v�ltoz�sokat jelz� esem�ny, ami pl. kiv�lthat egy jogosults�gellen�rz�st
    public event EventHandler OnChangedObjectProperties;

    private void RaiseEvent_OnChangedObjectProperties()
    {
        if (OnChangedObjectProperties != null)
        {
            OnChangedObjectProperties(this, new EventArgs());
        }
    }

    //be van-e t�ltve az �gyirat
    public bool IsMIGFoszamLoaded
    {
        get
        {
            if (ViewState["IsMIGFoszamLoaded"] != null)
            {
                bool loaded;
                if (Boolean.TryParse(ViewState["IsMIGFoszamLoaded"].ToString(), out loaded))
                {
                    return loaded;
                }
            }

            return false;
        }
        set
        {
            ViewState["IsMIGFoszamLoaded"] = value;
        }
    }

    //public static class BoolString
    //{
    //    public static readonly ListItem Yes = new ListItem("Igen", "1");
    //    public static readonly ListItem No = new ListItem("Nem", "0");
    //    public static void FillDropDownList(DropDownList list, string selectedValue)
    //    {
    //        list.Items.Clear();
    //        list.Items.Add(Yes);
    //        list.Items.Add(No);
    //        SetSelectedValue(list, selectedValue);
    //    }
    //    public static void FillDropDownList(DropDownList list)
    //    {
    //        FillDropDownList(list, No.Value);
    //    }
    //    public static void SetSelectedValue(DropDownList list, string selectedValue)
    //    {
    //        if (selectedValue == Yes.Value || selectedValue == No.Value)
    //        {
    //            list.SelectedValue = selectedValue;
    //        }
    //        else
    //        {
    //            list.SelectedValue = No.Value;
    //        }
    //    }
    //}

    private string RegisterCheckSkontroVegeJavaScript(DropDownList ddlIratHelye, HiddenField hfIratEredetiHelye, Component_CalendarControl ccSkontroVege)
    {
        string js = "";
        if (ddlIratHelye != null && ccSkontroVege != null)
        {
            js = "var text = $get('" + ddlIratHelye.ClientID + @"');
                  var cc = $get('" + ccSkontroVege.TextBox.ClientID + @"');
                  var hf = $get('" + hfIratEredetiHelye.ClientID + @"');

                   if(text && text.value == '1')
                   {
                       if (hf && hf.value == '2')
                       {
                          alert('" + Resources.Error.ErrorCode_56007 + @"');
                          return false;
                       }                    
                       else if (!cc || cc.value == '')
                       {
                          alert('" + Resources.Error.ErrorCode_56008 + @"');
                          return false;
                       }
                       else
                       {
                         var year   = parseInt(cc.value.substring(0,4), 10);
                         var month  = parseInt(cc.value.substring(5,7), 10);
                         var day   = parseInt(cc.value.substring(8,10), 10);
                         var ccDate = new Date(year, month-1, day);
                         var today = new Date;
                         var tomorrow = new Date(today.getYear(), today.getMonth(), today.getDate() + 1);

                         if (ccDate < tomorrow)
                         {
                           alert('" + Resources.Error.ErrorCode_56009 + @"');
                           return false;
                         }
                         else {return true;}
                      }
                   }
                   else {return true;}";
        }

        return js;

    }

    private string GetIratHelyeChangedJavaScript()
    {
        string js = "";
        js = @"var text = $get('" + ddlistIratHelye.ClientID + @"');
var hf = $get('" + HiddenField_IratEredetiHelye.ClientID + @"');
var ccSkontroVege = $get('" + CalendarSkontro.TextBox.ClientID + @"');
var ccIrattarba = $get('" + CalendarIrattarba.TextBox.ClientID + @"');
var hfSkontroVege = $get('" + HiddenField_SkontroEredeti.ClientID + @"');
var hfIrattarba = $get('" + HiddenField_IrattarbaEredeti.ClientID + @"');

if (text)
{
    if (hf && text.value == '1' && hf.value == '2')
    {
        text.value = '2';
        if (ccIrattarba && hfIrattarba)
        {
            ccIrattarba.value = hfIrattarba.value;
        }
        alert('" + Resources.Error.ErrorCode_56007 + @"');
    }
    else if (text.value == '1' && ccIrattarba)
    {
        ccIrattarba.value = '';
        if (ccSkontroVege && hfSkontroVege)
        {
            ccSkontroVege.value = hfSkontroVege.value;
        }
    }
    else if(text.value == '2' && ccSkontroVege)
    {
        ccSkontroVege.value = '';
        if (ccIrattarba && hfIrattarba)
        {
            ccIrattarba.value = hfIrattarba.value;
        }
    }
    else if (ccIrattarba && ccSkontroVege)
    {
        ccIrattarba.value = '';
        ccSkontroVege.value = '';
    };
}
return false;";
        return js;

    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        textUI_SAV.ReadOnly = true;
        textUI_YEAR.ReadOnly = true;
        textUI_NUM.ReadOnly = true;
        textIRJ2000.ReadOnly = true;
        textUI_NAME.ReadOnly = true;
        textUI_IRSZ.ReadOnly = true;
        textVAROSNEV.ReadOnly = true;
        textUI_UTCA.ReadOnly = true;
        textUI_HSZ.ReadOnly = true;
        textUI_HRSZ.ReadOnly = true;
        textMEMO.ReadOnly = true;
        ddlistIratHelye.ReadOnly = true;
        CalendarIrattarba.ReadOnly = true;
        CalendarSkontro.ReadOnly = true;
        CalendarHatarido.ReadOnly = true;
        textEloado.ReadOnly = true;
        LovImageButtonEloado.Visible = false;
        ResetImageButtonEloado.Visible = false;
        textFeladat.ReadOnly = true;
        textSzervezet.ReadOnly = true;
        textKikero.ReadOnly = true;
        textUgyirat_tipus.ReadOnly = true;
        CalendarIrattarbolKikeres_Datuma.ReadOnly = true;
        CalendarMegorzesiIdo.ReadOnly = true;
        // BLG_236
        TextIrattariHely.ReadOnly = true;

        labelUI_SAV.CssClass = "mrUrlapInputWaterMarked";
        labelUI_YEAR.CssClass = "mrUrlapInputWaterMarked";
        labelUI_NUM.CssClass = "mrUrlapInputWaterMarked";
        labelIRJ2000.CssClass = "mrUrlapInputWaterMarked";
        labelUI_NAME.CssClass = "mrUrlapInputWaterMarked";
        labelUI_IRSZ.CssClass = "mrUrlapInputWaterMarked";
        labelVAROSNEV.CssClass = "mrUrlapInputWaterMarked";
        labelUI_UTCA.CssClass = "mrUrlapInputWaterMarked";
        labelUgyirat_tipus.CssClass = "mrUrlapInputWaterMarked";
        labelUI_HSZ.CssClass = "mrUrlapInputWaterMarked";
        labelUI_HRSZ.CssClass = "mrUrlapInputWaterMarked";
        labelMEMO.CssClass = "mrUrlapInputWaterMarked";
        labelIratHelye.CssClass = "mrUrlapInputWaterMarked";
        labelHatarido.CssClass = "mrUrlapInputWaterMarked";
        labelIRATTARBA.CssClass = "mrUrlapInputWaterMarked";
        labelSkontro.CssClass = "mrUrlapInputWaterMarked";
        labelEloado.CssClass = "mrUrlapInputWaterMarked";
        labelFeladat.CssClass = "mrUrlapInputWaterMarked";
        labelSzervezet.CssClass = "mrUrlapInputWaterMarked";
        labelKikero.CssClass = "mrUrlapInputWaterMarked";
        labelIrattarbolKikeres_Datuma.CssClass = "mrUrlapInputWaterMarked";
        labelMegorzesiIdo.CssClass = "mrUrlapInputWaterMarked";

    }

    /// <summary>
    /// Modify m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        // BUG_11710
        Boolean javithato = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.EMIGRATION_ADATOK_MODOSITHATOSAGA_ENABLED, false);
        String recordId = Request.QueryString.Get(QueryStringVars.Id);
        Foszamok.Statusz statusz = Foszamok.GetAllapotById(recordId, Page, EErrorPanel1);
        Boolean modosithato = Foszamok.Modosithato(statusz);
        ExecParam xpm = UI.SetExecParamDefault(Page);
        if (statusz.Ugyhol == Constants.MIG_IratHelye.Irattarban && xpm.FelhasznaloSzervezet_Id == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(xpm).Obj_Id)
        {
            modosithato = true;
        }
            // jelenleg csak az �thelyez�s (iratt�r, skontr�, oszt�ly) enged�lyezett
        textUI_SAV.ReadOnly = true;
        textUI_YEAR.ReadOnly = true;
        textUI_NUM.ReadOnly = true;
        
        // BUG_11710
        //textUI_NAME.ReadOnly = true;
        textUI_NAME.ReadOnly = !javithato;
        //textMEMO.ReadOnly = true;
        textMEMO.ReadOnly = !javithato;
        //textEloado.ReadOnly = true; // ha ReadOnly, akkor nem k�ldi le a szerverre a v�ltoz�st, ez�rt ink�bb JavaScriptb�l �ll�tjuk

        textIRJ2000.ReadOnly = !modosithato;
        ddlistIratHelye.ReadOnly = !modosithato;
        textUgyirat_tipus.ReadOnly = !modosithato;
        CalendarHatarido.ReadOnly = !modosithato;
        CalendarMegorzesiIdo.ReadOnly = !modosithato;

        textUI_IRSZ.ReadOnly = true;
        textVAROSNEV.ReadOnly = true;
        textUI_UTCA.ReadOnly = true;
        textUI_HSZ.ReadOnly = true;
        textUI_HRSZ.ReadOnly = true;
        
        //textUgyirat_tipus.ReadOnly = true;
        //ddlistIratHelye.ReadOnly = true;
        CalendarIrattarba.ReadOnly = true;
        //CalendarHatarido.ReadOnly = true;
        //CalendarSkontro.ReadOnly = true;
        textFeladat.ReadOnly = true;
        textSzervezet.ReadOnly = true;
        textKikero.ReadOnly = true;
        CalendarIrattarbolKikeres_Datuma.ReadOnly = true;
        // BLG_236
        TextIrattariHely.ReadOnly = true;

        labelUI_SAV.CssClass = "mrUrlapInputWaterMarked";
        labelUI_YEAR.CssClass = "mrUrlapInputWaterMarked";
        labelUI_NUM.CssClass = "mrUrlapInputWaterMarked";
        //labelIRJ2000.CssClass = "mrUrlapInputWaterMarked";
        // BUG_11710
        if (!javithato)
        {
            labelUI_NAME.CssClass = "mrUrlapInputWaterMarked";
            labelMEMO.CssClass = "mrUrlapInputWaterMarked";
        }
        if (!modosithato)
        {
            labelIRJ2000.CssClass = "mrUrlapInputWaterMarked";
            labelIratHelye.CssClass = "mrUrlapInputWaterMarked";
            labelUgyirat_tipus.CssClass = "mrUrlapInputWaterMarked";
            labelHatarido.CssClass = "mrUrlapInputWaterMarked";
            labelMegorzesiIdo.CssClass = "mrUrlapInputWaterMarked";
        }
            labelUI_IRSZ.CssClass = "mrUrlapInputWaterMarked";
        labelVAROSNEV.CssClass = "mrUrlapInputWaterMarked";
        labelUI_UTCA.CssClass = "mrUrlapInputWaterMarked";
        labelUI_HSZ.CssClass = "mrUrlapInputWaterMarked";
        labelUI_HRSZ.CssClass = "mrUrlapInputWaterMarked";
        //labelIratHelye.CssClass = "mrUrlapInputWaterMarked";
        labelIRATTARBA.CssClass = "mrUrlapInputWaterMarked";
        //labelHatarido.CssClass = "mrUrlapInputWaterMarked";
        //labelSkontro.CssClass = "mrUrlapInputWaterMarked";
        labelFeladat.CssClass = "mrUrlapInputWaterMarked";
        labelSzervezet.CssClass = "mrUrlapInputWaterMarked";
        labelKikero.CssClass = "mrUrlapInputWaterMarked";
        //labelUgyirat_tipus.CssClass = "mrUrlapInputWaterMarked";
        labelIrattarbolKikeres_Datuma.CssClass = "mrUrlapInputWaterMarked";
    }

    // funkci�jogok ellen�rz�se a f� formon

    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(QueryStringVars.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                TabFooter1.Controls.Add(compSelector);
                break;
            default:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MIGFoszam" + Command);
                break;
        }

        //if (!IsPostBack)
        //{
        //    IratHelye.FillDropDownList(ddlistIratHelye);
        //}

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        else if (Command == CommandName.Modify)
        {
            SetModifyControls();

            TabFooter1.ImageButton_Save.OnClientClick += RegisterCheckIrattariJelScript(TabFooter1.ImageButton_Save);
            TabFooter1.ImageButton_SaveAndClose.OnClientClick += RegisterCheckIrattariJelScript(TabFooter1.ImageButton_SaveAndClose);

            TabFooter1.ImageButton_Save.OnClientClick += RegisterCheckSkontroVegeJavaScript(ddlistIratHelye, HiddenField_IratEredetiHelye, CalendarSkontro);
            TabFooter1.ImageButton_SaveAndClose.OnClientClick += RegisterCheckSkontroVegeJavaScript(ddlistIratHelye, HiddenField_IratEredetiHelye, CalendarSkontro);

            ddlistIratHelye.Attributes.Add("onchange", GetIratHelyeChangedJavaScript());

            //ASP.NET 2.0 bug work around
            // BUG_11710
            Boolean javithato = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.EMIGRATION_ADATOK_MODOSITHATOSAGA_ENABLED, false);
            if (!javithato) textEloado.Attributes.Add("readonly", "readonly");
        }


        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }

    }

    protected void FoszamUpdatePanel_Load(object sender, EventArgs e)
    {
        // ha nem ez az akt�v tab, nem fut le
        if (!_Active) return;

        if (Command == CommandName.Modify)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshUgyiratokPanel:
                        ReLoadTab(true);
                        RaiseEvent_OnChangedObjectProperties();
                        break;
                }
            }
        }
    }

    public void ReLoadTab()
    {
        if (!IsPostBack || !IsMIGFoszamLoaded)
        {
            ReLoadTab(true);
        }
        else
        {
            ReLoadTab(false);
        }
    }

    private void ReLoadTab(bool loadRecord)
    {

        // nem lehet New
        if (Command == CommandName.New)
        {
            return;
        }

        if (_ParentForm == Contentum.eMigration.Utility.Constants.ParentForms.Foszam)
        {
            if (!MainPanel.Visible) return;

            if (Command == CommandName.View || Command == CommandName.Modify)
            {

                if (ObjektumAzonosito == ObjektumAzonositoTipus.Missing)
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(EErrorPanel1);
                    ErrorUpdatePanel1.Update();
                }
                else
                {
                    if (loadRecord)
                    {
                        // rekord bet�lt�se

                        if (obj_MIG_Foszam == null)
                        {
                            Result result = new Result();
                            MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            if (ObjektumAzonosito == ObjektumAzonositoTipus.Id)
                            {
                                execParam.Record_Id = ParentId;
                                result = service.Get(execParam);
                            }
                            else
                            {
                                result = service.GetByAzonosito(execParam, ParentAzonosito);
                            }
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                obj_MIG_Foszam = (MIG_Foszam)result.Record;
                                if (obj_MIG_Foszam == null)
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1,
                                        ResultError.CreateNewResultWithErrorCode(50101));
                                    ErrorUpdatePanel1.Update();
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }

                        }

                        if (obj_MIG_Foszam != null)
                        {
                            //Id elmentese a ViewState-be
                            if (ObjektumAzonosito == ObjektumAzonositoTipus.Azonosito)
                            {
                                this.SaveFoszamId(obj_MIG_Foszam.Id);
                            }

                            // lista sz�r�se st�tusz szerint �s funkci� jogosults�g szerint
                            IratHelye.FillDropDownListByStatusz(Page, ddlistIratHelye, Foszamok.GetAllapotByBusinessDocument(obj_MIG_Foszam), true);

                            LoadComponentsFromBusinessObject(obj_MIG_Foszam);

                            //#region MIG_Alszam adatainak lek�r�se MIG_FoszamGetAll haszn�lat�val

                            //Result result = new Result();
                            //MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();

                            //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            //MIG_FoszamSearch search = new MIG_FoszamSearch();

                            //search.Id.Value = obj_MIG_Foszam.Id;
                            //search.Id.Operator = Query.Operators.equals;

                            //result = service.GetAll(execParam, search);
                            //if (String.IsNullOrEmpty(result.ErrorCode))
                            //{
                            //    if (result.Ds.Tables[0].Rows.Count == 1)
                            //    {
                            //        DataRow dataRow_MIG_FoszamGetAll = result.Ds.Tables[0].Rows[0];
                            //        LoadComponentsFromDataRow(dataRow_MIG_FoszamGetAll);
                            //    }
                            //}
                            //else
                            //{
                            //    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            //    ErrorUpdatePanel1.Update();
                            //}

                            //#endregion MIG_Alszam adatainak lek�r�se MIG_FoszamGetAll haszn�lat�val
                        }

                    }
                }
            }

            // Ez az egyes tab fulek belso commandjat hatarozza meg! pl: Kuldemeny modositasakor meg kell kulonboztetni gridview felvetel/modositast!!!
            TabFooter1.CommandArgument = Command;


            if (Command == CommandName.DesignView)
            {
                Load_ComponentSelectModul();
            }
            else
            {
                pageView.SetViewOnPage(Command);
            }


            // Komponensek l�that�s�g�nak be�ll�t�sa:
            SetComponentsVisibility(Command);
        }
        else
        {
            // ha egy�b oldalra lenne bet�ve a tabpanel
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel,
                Resources.Error.UITabPanelIsNotEnabledHere + " (" + Page.ToString() + ")");
            ErrorUpdatePanel1.Update();
            MainPanel.Visible = false;
        }

    }

    #region private methods


    /// <summary>
    /// Komponensek l�that�s�g�nak be�ll�t�sa
    /// </summary>
    /// <param name="_ugyiratStatusz">lehet null is, csak olyankor �jra le kell k�rni</param>
    private void SetComponentsVisibility(String _command)
    {
        #region TabFooter gombok �ll�t�sa

        if (_command == CommandName.Modify)
        {
            TabFooter1.ImageButton_Save.Visible = true;
            TabFooter1.ImageButton_SaveAndClose.Visible = true;
            TabFooter1.ImageButton_SaveAndNew.Visible = false;
        }

        // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
        TabFooter1.ImageButton_Cancel.Visible = false;

        #endregion

        #region LovList, Reset gombok �ll�t�sa

        if (_command == CommandName.Modify)
        {
            #region LovList
            LovImageButtonEloado.Visible = true;
            Contentum.eUtility.Csoportok.CsoportFilterObject filterObject = new Contentum.eUtility.Csoportok.CsoportFilterObject();

            filterObject.Tipus = Contentum.eUtility.Csoportok.CsoportFilterObject.CsoprtTipus.Dolgozo;

            string queryString_Lov = QueryStringVars.HiddenFieldId + "=" + HiddenField_EdokUgyintezoCsoportId.ClientID
                + "&" + QueryStringVars.TextBoxId + "=" + textEloado.ClientID
                + "&" + filterObject.QsSerialize(); // sz�r�s dolgoz�kra (nem lehet szervezet/csoport)

            LovImageButtonEloado.OnClientClick = JavaScripts.SetOnClientClick(eRecord.ERecordWebsiteUrl + "CsoportokLovList.aspx"
                        , queryString_Lov
                        , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

            #endregion LovList

            #region Reset
            ResetImageButtonEloado.Visible = true;
            string js_Reset = @"var hfEdokUgyintezoCsoportId = $get('" + HiddenField_EdokUgyintezoCsoportId.ClientID + @"');
var textEloado = $get('" + textEloado.ClientID + @"');
var hfMIGEloadoNev = $get('" + HiddenField_EloadoNevEredeti.ClientID + @"');
if (hfEdokUgyintezoCsoportId)
{
    hfEdokUgyintezoCsoportId.value = '';
}
if (textEloado && hfMIGEloadoNev)
{
    textEloado.value = hfMIGEloadoNev.value;
}
return false;
";
            ResetImageButtonEloado.OnClientClick = js_Reset;

            #endregion Reset
        }
        #endregion

    }


    // business object --> form
    private void LoadComponentsFromBusinessObject(MIG_Foszam mig_Foszam)
    {
        if (String.IsNullOrEmpty(mig_Foszam.MIG_Varos_Id))
        {
            textVAROSNEV.Text = "";
        }
        else
        {
            MIG_VarosService service = eMigrationService.ServiceFactory.GetMIG_VarosService();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = mig_Foszam.MIG_Varos_Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                MIG_Varos MIG_Varos = (MIG_Varos)result.Record;
                textVAROSNEV.Text = MIG_Varos.NAME;
            }
            else
            {
                textVAROSNEV.Text = "";
            }
        }
        textUI_SAV.Text = mig_Foszam.UI_SAV;
        textUI_YEAR.Text = mig_Foszam.UI_YEAR;
        textUI_NUM.Text = mig_Foszam.UI_NUM;
        if (!mig_Foszam.Typed.UI_YEAR.IsNull)
        {
            if (mig_Foszam.Typed.UI_YEAR.Value < 2000)
                textIRJ2000.Text = mig_Foszam.UI_IRJ;
            else
                textIRJ2000.Text = mig_Foszam.IRJ2000;

            HiddenField_IRJ2000Eredeti.Value = textIRJ2000.Text; // megjegyezz�k, hogy �ssze lehessen hasonl�tani
        }

        textUI_NAME.Text = mig_Foszam.UI_NAME;
        textUI_IRSZ.Text = mig_Foszam.UI_IRSZ;
        textUI_UTCA.Text = mig_Foszam.UI_UTCA;
        textUI_HSZ.Text = mig_Foszam.UI_HSZ;
        textUI_HRSZ.Text = mig_Foszam.UI_HRSZ;
        textMEMO.Text = mig_Foszam.MEMO;

        ddlistIratHelye.SelectedValue = mig_Foszam.UGYHOL;
        HiddenField_IratEredetiHelye.Value = mig_Foszam.UGYHOL; // megjegyezz�k, hogy �ssze lehessen hasonl�tani
        if (mig_Foszam.Hatarido.Contains("0:"))
        {
            CalendarHatarido.Text = mig_Foszam.Hatarido.Remove(mig_Foszam.Hatarido.IndexOf("0:"));
        }
        else
        {
            CalendarHatarido.Text = mig_Foszam.Hatarido;
        }
        HiddenField_Hatarido.Value = CalendarHatarido.TextBox.Text; // megjegyezz�k (form�zva), hogy vissza lehessen �rni        
        if (mig_Foszam.IRATTARBA.Contains("0:"))
        {
            CalendarIrattarba.Text = mig_Foszam.IRATTARBA.Remove(mig_Foszam.IRATTARBA.IndexOf("0:"));
        }
        else
        {
            CalendarIrattarba.Text = mig_Foszam.IRATTARBA;
        }
        HiddenField_IrattarbaEredeti.Value = CalendarIrattarba.TextBox.Text; // megjegyezz�k (form�zva), hogy vissza lehessen �rni

        // BUG_6566
        if (Command == CommandName.Modify && mig_Foszam.UGYHOL != IratHelye.Skontro.Value)
        {
            labelSkontro.CssClass = "mrUrlapInputWaterMarked";
            CalendarSkontro.ReadOnly = true;
            CalendarSkontro.Text = "";
            HiddenField_SkontroEredeti.Value = "";
        }
        else //
        {
            if (mig_Foszam.SCONTRO.Contains("0:"))
            {
                CalendarSkontro.Text = mig_Foszam.SCONTRO.Remove(mig_Foszam.SCONTRO.IndexOf("0:"));
            }
            else
            {
                CalendarSkontro.Text = mig_Foszam.SCONTRO;
            }
            HiddenField_SkontroEredeti.Value = CalendarSkontro.TextBox.Text; // megjegyezz�k (form�zva), hogy vissza lehessen �rni
        }


        #region El�ad�/�gyint�z�
        textEloado.Text = "";
        if (String.IsNullOrEmpty(mig_Foszam.Edok_Ugyintezo_Csoport_Id))
        {
            if (!String.IsNullOrEmpty(mig_Foszam.MIG_Eloado_Id))
            {
                MIG_EloadoService service = eMigrationService.ServiceFactory.GetMIG_EloadoService();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = mig_Foszam.MIG_Eloado_Id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    MIG_Eloado MIG_Eloado = (MIG_Eloado)result.Record;
                    textEloado.Text = MIG_Eloado.NAME;
                    HiddenField_EloadoNevEredeti.Value = MIG_Eloado.NAME;
                }
            }
            HiddenField_EdokUgyintezoCsoportId.Value = "";
        }
        else
        {
            HiddenField_EdokUgyintezoCsoportId.Value = ""; // nem kell bele�rni semmit, csak ha v�ltoztattuk
            textEloado.Text = mig_Foszam.Edok_Ugyintezo_Nev;
            HiddenField_EloadoNevEredeti.Value = mig_Foszam.Edok_Ugyintezo_Nev;
        }
        #endregion El�ad�/�gyint�z�

        textFeladat.Text = mig_Foszam.Feladat;
        textSzervezet.Text = mig_Foszam.Szervezet;
        textKikero.Text = mig_Foszam.IrattarbolKikeroNev;
        textUgyirat_tipus.Text = mig_Foszam.Ugyirat_tipus;
        CalendarIrattarbolKikeres_Datuma.Text = mig_Foszam.IrattarbolKikeres_Datuma;
        CalendarMegorzesiIdo.Text = mig_Foszam.MegorzesiIdo;

        // BLG_236
        if (String.IsNullOrEmpty(mig_Foszam.IrattarId))
        {
            TextIrattariHely.Text = mig_Foszam.IrattariHely;
        }
        else
        {
            EREC_IrattariHelyekService serviceIrattariHelyek = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = mig_Foszam.IrattarId;

            Result result = serviceIrattariHelyek.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_IrattariHelyek erec_IrattariHely = (EREC_IrattariHelyek)result.Record;
                TextIrattariHely.Text = erec_IrattariHely.Ertek;
            }
            else
            {
                textVAROSNEV.Text = "";
            }
        }

        //aktu�lis verzi� elt�rol�sa
        //FormHeader1.Record_Ver = mig_Foszam.Base.Ver;
        //A m�dos�t�s,l�trehoz�s form be�ll�t�sa
        //FormPart_CreatedModified1.SetComponentValues(mig_Foszam.Base);

    }

    //// business object datarow --> form
    //// MIG_Alszam adatainak kit�lt�se a MIG_FoszamGetAllb�l �tvett adatokkal
    //private void LoadComponentsFromDataRow(DataRow dataRow_MIG_FoszamGetAll)
    //{
    //    if (dataRow_MIG_FoszamGetAll == null) return;

    //    String IRATTARBA = dataRow_MIG_FoszamGetAll["IRATTARBA"].ToString();
    //    String UGYHOL = dataRow_MIG_FoszamGetAll["UGYHOL"].ToString();
    //    String SCONTRO = dataRow_MIG_FoszamGetAll["SCONTRO"].ToString();

    //    if (!String.IsNullOrEmpty(IRATTARBA))
    //    {
    //        if (IRATTARBA.Contains("0:"))
    //        {
    //            CalendarIrattarba.Text = IRATTARBA.Remove(IRATTARBA.IndexOf("0:"));
    //        }
    //        else
    //        {
    //            CalendarIrattarba.Text = IRATTARBA;
    //        }
    //    }

    //    if (!String.IsNullOrEmpty(UGYHOL))
    //    {
    //        ddlistIratHelye.SelectedValue = UGYHOL;
    //    }

    //    if (!String.IsNullOrEmpty(SCONTRO))
    //    {
    //        CalendarSkontro.Text = SCONTRO;
    //    }

    //}

    private void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(textUI_SAV);
            compSelector.Add_ComponentOnClick(textUI_YEAR);
            compSelector.Add_ComponentOnClick(textUI_NUM);
            compSelector.Add_ComponentOnClick(textIRJ2000);
            compSelector.Add_ComponentOnClick(textUI_NAME);
            compSelector.Add_ComponentOnClick(textUI_IRSZ);
            compSelector.Add_ComponentOnClick(textVAROSNEV);
            compSelector.Add_ComponentOnClick(textUI_UTCA);
            compSelector.Add_ComponentOnClick(textUI_HSZ);
            compSelector.Add_ComponentOnClick(textUI_HRSZ);
            compSelector.Add_ComponentOnClick(textMEMO);
            compSelector.Add_ComponentOnClick(textEloado);
            compSelector.Add_ComponentOnClick(textUgyirat_tipus);

            compSelector.Add_ComponentOnClick(ddlistIratHelye);
            compSelector.Add_ComponentOnClick(CalendarIrattarba);
            compSelector.Add_ComponentOnClick(CalendarSkontro);
            compSelector.Add_ComponentOnClick(CalendarHatarido);

            TabFooter1.SaveEnabled = false;
        }

    }

    // // form --> business object
    //private MIG_Foszam GetBusinessObjectFromComponents()
    //{
    //    // TODO: 
    //    // Csak azokat a mez�ket �ll�tjuk be, amik m�dos�that�k
    //    MIG_Foszam mig_Foszam = new MIG_Foszam();
    //    // TODO: Feltoltes a form alapjan adattal
    //    // ...
    //    return mig_Foszam;

    //}

    private bool IsIrattariJelChanged
    {
        get
        {
            return HiddenField_IRJ2000Eredeti.Value != textIRJ2000.Text;
        }
    }

    private bool IsIratHelyeChanged()
    {
        return (ddlistIratHelye.SelectedValue != HiddenField_IratEredetiHelye.Value
            || HiddenField_SkontroEredeti.Value != CalendarSkontro.TextBox.Text
            || HiddenField_IrattarbaEredeti.Value != CalendarIrattarba.TextBox.Text
            || IsIrattariJelChanged);
    }

    private bool IsEloadoChanged()
    {
        return (!String.IsNullOrEmpty(HiddenField_EdokUgyintezoCsoportId.Value));
    }

    private MIG_Foszam GetBusinessObjectFromComponents()
    {
        MIG_Foszam mig_Foszam = new MIG_Foszam();
        mig_Foszam.Updated.SetValueAll(false);

        mig_Foszam.UGYHOL = ddlistIratHelye.SelectedValue;
        mig_Foszam.Updated.UGYHOL = pageView.GetUpdatedByView(ddlistIratHelye);

        string sYear = textUI_YEAR.Text;
        int year;
        if (!String.IsNullOrEmpty(sYear) && Int32.TryParse(sYear, out year))
        {
            if (year < 2000)
            {
                mig_Foszam.UI_IRJ = textIRJ2000.Text;
                mig_Foszam.Updated.UI_IRJ = pageView.GetUpdatedByView(textIRJ2000);
            }
            else
            {
                mig_Foszam.IRJ2000 = textIRJ2000.Text;
                mig_Foszam.Updated.IRJ2000 = pageView.GetUpdatedByView(textIRJ2000);
            }
        }

        if (mig_Foszam.UGYHOL == IratHelye.Skontro.Value)
        {
            mig_Foszam.SCONTRO = CalendarSkontro.Text;
            mig_Foszam.Updated.SCONTRO = true;

            mig_Foszam.IRATTARBA = "";
            mig_Foszam.Updated.IRATTARBA = true;
        }
        else
        {
            mig_Foszam.SCONTRO = "";
            mig_Foszam.Updated.SCONTRO = true;

            mig_Foszam.IRATTARBA = "";
            mig_Foszam.Updated.IRATTARBA = true;
        }
        if (!string.IsNullOrEmpty(CalendarHatarido.Text))
        {
            mig_Foszam.Hatarido = CalendarHatarido.Text;
            mig_Foszam.Updated.Hatarido = true;

        }
        mig_Foszam.Ugyirat_tipus = textUgyirat_tipus.Text;
        mig_Foszam.Updated.Ugyirat_tipus = true;

        #region El�ad�/�gyint�z�
        if (!String.IsNullOrEmpty(HiddenField_EdokUgyintezoCsoportId.Value))
        {
            // EDOK �gyint�z� ment�se
            mig_Foszam.Edok_Ugyintezo_Csoport_Id = HiddenField_EdokUgyintezoCsoportId.Value;
            mig_Foszam.Updated.Edok_Ugyintezo_Csoport_Id = true;

            mig_Foszam.Edok_Ugyintezo_Nev = textEloado.Text;
            mig_Foszam.Updated.Edok_Ugyintezo_Nev = pageView.GetUpdatedByView(textEloado);

            // r�gi MIG_Eloado - MIG_Foszam k�t�s t�rl�se
            mig_Foszam.MIG_Eloado_Id = "";
            mig_Foszam.Updated.MIG_Eloado_Id = true;
        }
        #endregion El�ad�/�gyint�z�

        mig_Foszam.MegorzesiIdo = CalendarMegorzesiIdo.Text;
        mig_Foszam.Updated.MegorzesiIdo = true;

        // BUG_11710
        mig_Foszam.UI_NAME = textUI_NAME.Text;
        mig_Foszam.Updated.UI_NAME = pageView.GetUpdatedByView(textUI_NAME);

        mig_Foszam.MEMO =  textMEMO.Text;
        mig_Foszam.Updated.MEMO = pageView.GetUpdatedByView(textUI_NAME);

        return mig_Foszam;
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            switch (Command)
            {
                case CommandName.Modify:
                    {
                        String recordId = Request.QueryString.Get(QueryStringVars.Id);
                        if (String.IsNullOrEmpty(recordId))
                        {
                            // nincs Id megadva:
                            ResultError.DisplayNoIdParamError(EErrorPanel1);
                        }
                        else
                        {
                            MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
                            MIG_Foszam foszam = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam.Record_Id = recordId;

                            Result result = new Result();

                            bool bEloadoChanged = IsEloadoChanged();
                            bool bIratHelyeChanged = IsIratHelyeChanged();
                            bool bError = false;

                            // 1. ha az irat helye v�ltozott, a teljes szerel�si l�nc egy�tt mozog, ha a param�ter engedi
                            if (bIratHelyeChanged)
                            {
                                result = service.FoszamAthelyzes(execParam, foszam, Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.MIGRACIOS_FOSZAM_HELY_VALTOZAS_TELJES_LANC, true));
                                if (!String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    bError = true;
                                }
                            }

                            // 2. ha az el�ad� megv�ltozott, Update
                            if (!bError && bEloadoChanged)
                            {
                                result = service.Update(execParam, foszam);
                                if (!String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    bError = true;
                                }
                            }
                            // Hat�rid� �s Irat t�pusa megv�ltozott
                            if (!bError)
                            {
                                result = service.Update(execParam, foszam);
                                if (!String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    bError = true;
                                }
                            }

                            if (!bError)
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);

                                if (e.CommandName == CommandName.SaveAndClose)
                                {
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                }
                                else
                                {
                                    ReLoadTab(true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            }
                        }
                    }
                    break;
            }
        }
    }

    private bool IsIrattariJelSzovegesModositas
    {
        get
        {
            return FunctionRights.HasFunctionRight(UI.SetExecParamDefault(Page), "FoszamIrattariJelSzovegesModositas");
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Ha van funkci�joga, akkor sz�veges is megadhat� az iratt�ri jel
        textIRJ2000.IsCustomValueEnabled = IsIrattariJelSzovegesModositas;
    }

    protected string GetFelhasznaloId()
    {
        ExecParam xpm = UI.SetExecParamDefault(Page);
        return xpm.Felhasznalo_Id;
    }

    private string RegisterCheckIrattariJelScript(ImageButton button)
    {
        if (IsIrattariJelSzovegesModositas)
        {
            return "if(CheckIrattariJel('" + button.ClientID + "')) {return false;};";
        }

        return String.Empty;
    }

    #endregion




}
