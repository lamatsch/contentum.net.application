using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eMigration.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Service;
using Contentum.eMigration.BusinessDocuments;

public partial class eMigrationComponent_FoszamTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent, UI.ILovListTextBox
{
    #region public properties

    private string filter;

    public string Filter
    {
        get { return filter; }
        set { filter = value; }
    }

    private string filterByObjektumKapcsolat = String.Empty;

    public string FilterByObjektumKapcsolat
    {
        get { return filterByObjektumKapcsolat; }
        set { filterByObjektumKapcsolat = value; }
    }

    private string objektumId = String.Empty;

    public string ObjektumId
    {
        get { return objektumId; }
        set { objektumId = value; }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        get
        {
            return Validator1.ValidationGroup;
        }
        set
        {
            Validator1.ValidationGroup = value;
        }
    }

    public bool Enabled
    {
        set
        {
            txtFoszam.Enabled = value;
            LovImageButton.Enabled = value;
            //NewImageButton.Enabled = value;
            if (value == false)
            {
                UI.SwapImageToDisabled(LovImageButton);
                //UI.SwapImageToDisabled(NewImageButton);
            }
        }
        get { return txtFoszam.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            txtFoszam.ReadOnly = value;
            LovImageButton.Enabled = !value;
            //NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                //NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
        }
        get { return txtFoszam.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    //public string OnClick_New
    //{
    //    set { NewImageButton.OnClientClick = value; }
    //    get { return NewImageButton.OnClientClick; }
    //}
    
    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { txtFoszam.Text = value; }
        get { return txtFoszam.Text; }
    }

    public TextBox TextBox
    {
        get { return txtFoszam; }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }


    public Unit Width
    {
        get { return TextBox.Width; }
        set { TextBox.Width = value; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                txtFoszam.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                //NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                txtFoszam.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                //NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            //NewImageButton.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekintés gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                //NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    public RequiredFieldValidator Validator
    {
        get
        {
            return Validator1;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {

            MIG_FoszamService service = eMigrationService.ServiceFactory.GetMIG_FoszamService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                MIG_Foszam erec_Ugyiratok = (MIG_Foszam)result.Record;

                SetTextBoxByBusinessObject(erec_Ugyiratok, errorPanel, errorUpdatePanel);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                if (errorUpdatePanel != null)
                {
                    errorUpdatePanel.Update();
                }
            }
        }
        else
        {
            Text = "";
        }
    }


    public void SetTextBoxByBusinessObject(MIG_Foszam mig_Foszam, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        Id_HiddenField = mig_Foszam.Id;

        Text = String.Format("({0}) /{1} /{2}", mig_Foszam.EdokSav, mig_Foszam.UI_YEAR, mig_Foszam.UI_NUM); // Azonosito;
    }



    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        
        ResetImageButton.Visible = !Validate && !ViewMode;
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string filterQuery = String.Empty;
        if (!String.IsNullOrEmpty(filter))
        {
            filterQuery = "&" + QueryStringVars.Filter + "=" + filter;
        }

        OnClick_Lov = JavaScripts.SetOnClientClick("FoszamLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + txtFoszam.ClientID+filterQuery
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, "", "", false);



        //OnClick_New = JavaScripts.SetOnClientClick("FoszamForm.aspx"
        //    , CommandName.Command + "=" + CommandName.New
        //    + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
        //    + "&" + QueryStringVars.TextBoxId + "=" + txtFoszam.ClientID
        //    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "FoszamForm.aspx", "", HiddenField1.ClientID, "", Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";

        //if (MigralasVisible)
        //{
        //    ImageButton_MigraltKereses.Visible = true;
        //    ViewImageButtonMigralt.Visible = true;

        //    string jsViewVisiblity = "function SetUgyiratView(){var imgView = $get('" + ViewImageButton.ClientID + "'); if(imgView) imgView.style.display = '';";
        //    jsViewVisiblity += "var imgViewMig = $get('" + ViewImageButtonMigralt.ClientID + "'); if(imgViewMig) imgViewMig.style.display = 'none';";
        //    jsViewVisiblity += "var hfType = $get('" + hfUgyiratType.ClientID + "'); if(hfType) hfType.value = 'edok';";
        //    jsViewVisiblity += "return false;}";

        //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SetUgyiratView", jsViewVisiblity, true);


        //    OnClick_Lov = JavaScripts.SetOnClientClick("UgyUgyiratokLovList.aspx",
        //    QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
        //    + "&" + QueryStringVars.TextBoxId + "=" + txtFoszam.ClientID
        //    + "&" + QueryStringVars.ParentWindowCallbackFunction + "=SetUgyiratView" + filterQuery
        //            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, "", "", false);

        //    string jsViewVisiblityMig = "function SetMigraltUgyiratView(){var imgView = $get('" + ViewImageButton.ClientID + "'); if(imgView) imgView.style.display = 'none';";
        //    jsViewVisiblityMig += "var imgViewMig = $get('" + ViewImageButtonMigralt.ClientID + "'); if(imgViewMig) imgViewMig.style.display = '';";
        //    jsViewVisiblityMig += "var hfType = $get('" + hfUgyiratType.ClientID + "'); if(hfType) hfType.value = 'migralt';";
        //    jsViewVisiblityMig += "return false;}";

        //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SetMigraltUgyiratView", jsViewVisiblityMig, true);

        //    ImageButton_MigraltKereses.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack(eMigration.migralasLovListUrl,
        //    QueryStringVars.HiddenFieldId + "=" + HiddenField.ClientID + "&" +
        //    QueryStringVars.AzonositoTextBox + "=" + TextBox.ClientID + "&" +
        //    QueryStringVars.Filter + "=" + Constants.FilterType.Ugyiratok.Szereles + "&" +
        //    QueryStringVars.ParentWindowCallbackFunction + "=SetMigraltUgyiratView",
        //    Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        //    string jsMigraltView = "var txtBox = $get('" + TextBox.ClientID + "');if(!txtBox || txtBox.value == ''){alert('" + Resources.Error.UINoSelectedItem + "'); return false;}";
        //    jsMigraltView += "var azonosito = txtBox.value;azonosito = azonosito.replace(/\\//g,'%2f');";

        //    ViewImageButtonMigralt.OnClientClick = jsMigraltView +  JavaScripts.SetOnCLientClick_NoPostBack(eMigration.migralasFormUrl,
        //              QueryStringVars.Command + "=" + CommandName.View + "&" +
        //              QueryStringVars.RegiAdatAzonosito + "=' + escape(azonosito) + '",
        //              Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);
        //}
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(txtFoszam);
        componentList.Add(LovImageButton);
        //componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);

        LovImageButton.OnClientClick = "";
        //NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
