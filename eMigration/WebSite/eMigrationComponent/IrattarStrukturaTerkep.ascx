﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IrattarStrukturaTerkep.ascx.cs" Inherits="eMigrationComponent_IrattarStrukturaTerkep" %>

    <asp:HiddenField ID="SelectedId_HiddenField" runat="server" />
    <asp:HiddenField ID="SelectedNode_HiddenField" runat="server" />
    <asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel" UpdateMode="Conditional"
        >
        <ContentTemplate>    
            <asp:Panel ID="Panel1" runat="server" Visible="true" CssClass="ajax__tab_body">
                <asp:TreeView ID="TreeView1" runat="server" 
                    onselectednodechanged="TreeView1_SelectedNodeChanged"
                    OnTreeNodeExpanded="TreeView1_Node_Expand">
					<%--CR3246 Irattári Helyek kezelésének módosítása--%>
                    <SelectedNodeStyle CssClass="TreeView_SelectedNode" Font-Bold="False" BackColor="#CCCCCC" />
                </asp:TreeView>
            </asp:Panel>     
        </ContentTemplate>
    </asp:UpdatePanel>
