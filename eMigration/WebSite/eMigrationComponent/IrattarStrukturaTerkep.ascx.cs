﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eMigration.Utility;
using Contentum.eQuery;
using Contentum.eUIControls;

public partial class eMigrationComponent_IrattarStrukturaTerkep : System.Web.UI.UserControl
{
    public eErrorPanel EErrorPanel1
    {
        get;
        set;
    }
    //public string SelectedId 
    //{ 
    //    get { return SelectedId_HiddenField.Value; }
    //    set { SelectedId_HiddenField.Value = value; }
    //}
    //public TreeNode SelectedNode 
    //{ 
    //    get { return TreeView1.SelectedNode; } 
    //}
    public HiddenField SelectedHiddenField 
    { 
        get { return SelectedId_HiddenField; } 
    }
    public TreeView TreeView
    {
        get { return TreeView1; }
        set { TreeView1 = value; }
    }

    // CR3246 Irattári Helyek kezelésének módosítása
    private EREC_IrattariHelyekSearch _IrattariHelyekSearch;
    public EREC_IrattariHelyekSearch IrattariHelyekSearch
    {
        get
        {
            if (_IrattariHelyekSearch == null)
            {
                _IrattariHelyekSearch = new EREC_IrattariHelyekSearch();
            }
            return _IrattariHelyekSearch;
        }
        set
        {
            _IrattariHelyekSearch = value;
        }
    }

    public event EventHandler SelectedNodeChanged;

    public bool AddEmptyItem { get; set; }

    TreeNode EmptyItem = new TreeNode("Nincs megadva", Guid.Empty.ToString());

    public string SelectedValue
    {
        get
        {
            if (TreeView1.SelectedValue == EmptyItem.Value)
            {
                return null;
            }
            else
            {
                return TreeView1.SelectedValue;
            }
        }
    }

    public string SelectedText
    {
        get
        {
            if (TreeView1.SelectedValue == EmptyItem.Value)
            {
                return null;
            }
            else
            {
                return TreeView1.SelectedNode.Text;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TreeView1.Nodes.Add(new TreeNode("Irattári struktúra",""));
            TreeView1.Nodes[0].Selected = true;
            TreeView1.ExpandDepth = 0;
            LoadTreeViewRoots();
            if (AddEmptyItem)
            {
                TreeView1.Nodes.Add(EmptyItem);
            }
            SelectedNodeChanged(TreeView1, e);
        }

        

        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    if (Session["ModifyIrattariHelyId"] != null && !String.IsNullOrEmpty(Session["ModifyIrattariHelyId"].ToString()))
                    {
                        if (TreeView1.SelectedNode.Parent.Value == "")
                        {
                            TreeView1.SelectedNode.Parent.Selected = true;
                            LoadTreeViewRoots();
                        }
                        else
                        {
                            TreeView1.SelectedNode.Parent.Selected = true;
                            LoadTreeViewChildren(TreeView1.SelectedNode);
                        }
                        TreeView1.SelectedNode.Expanded = true;
                        foreach (TreeNode tn in TreeView1.SelectedNode.ChildNodes)
                        {
                            if (tn.Value.Equals(Session["ModifyIrattariHelyId"].ToString()))
                                tn.Selected = true;
                        }
                       
                        SelectedNodeChanged(TreeView1, e);
                        TreeView1.SelectedNode.Expanded = true;
                        TreeViewUpdatePanel.Update();
                        Session["ModifyIrattariHelyId"] = "";
                    }
                    else
                    {
                        TreeView1.SelectedNode.ChildNodes.Clear();
                        if (!String.IsNullOrEmpty(TreeView1.SelectedNode.Value))
                            LoadTreeViewChildren(TreeView1.SelectedNode);
                        else
                            LoadTreeViewRoots();
                        SelectedNodeChanged(TreeView1, e);
                        TreeView1.SelectedNode.Expanded = true;
                        TreeViewUpdatePanel.Update();
                    }
                    break;
            }
        }
        
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        
    }
    protected void TreeView1_Node_Expand(Object sender, TreeNodeEventArgs e)
    {
        if (e.Node == null) return;
        if (String.IsNullOrEmpty(e.Node.Value) || e.Node.ChildNodes.Count > 0 )
        {
            e.Node.Expanded = true;
        }
        else 
        {
            LoadTreeViewChildren(e.Node);
            e.Node.Expanded = true;
        }
    }
    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    {
        TreeView tv = (TreeView)sender;
        
        if (tv.SelectedNode == null) return;

        //SelectedId_HiddenField.Value = tv.SelectedNode.Value;
        //SelectedNode_HiddenField.Value = tv.SelectedNode.Text;
        SelectedNodeChanged(sender, e);
    }

    public void RefreshTreeView(object sender, EventArgs e)
    {
        LoadTreeViewRoots();
        SelectedNodeChanged(sender, e);
        TreeViewUpdatePanel.Update();
    }

    public void DeleteSelectedNode()
    {
        if (TreeView1.SelectedNode != null)
        {
            TreeNode ParentNode = TreeView1.SelectedNode.Parent;
            if (ParentNode != null)
            {
                ParentNode.ChildNodes.Remove(TreeView1.SelectedNode);
                ParentNode.Select();
                TreeView1_SelectedNodeChanged(TreeView1, new EventArgs());
            }
        }
        TreeViewUpdatePanel.Update();
    }
    protected void LoadTreeViewRoots()
    {
        ViewState["Loaded"] = false;
        Session["isNotExistChild"] = "";
        TreeView1.Nodes[0].ChildNodes.Clear();
        using (Contentum.eRecord.Service.EREC_IrattariHelyekService service = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            ExecParam execParam = UI.SetExecParamDefault(Page);
            // CR3246 Irattári Helyek kezelésének módosítása
            //var t = service.GetAllRoots(execParam, new EREC_IrattariHelyekSearch());
            var t = service.GetAll(execParam, IrattariHelyekSearch);

            if (t != null)
            {
                if (!String.IsNullOrEmpty(t.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, t);
                }                
            }
            if (t.Ds != null)
            {
                foreach (DataRow item in t.Ds.Tables[0].Rows)
                {
                    RecursiveChild(TreeView1.Nodes, item);
                    //if (item["isExistChild"].Equals("0"))
                    //    Session["isNotExistChild"] += item["Id"].ToString().ToLower();
                }
            }
        }
        ViewState["Loaded"] = true;
        TreeView1.Nodes[0].Selected = true;
        TreeView1.Nodes[0].Expanded = true;
        foreach (TreeNode tn in TreeView1.Nodes[0].ChildNodes)
        {
            //   if (!Session["isNotExistChild"].ToString().Contains(tn.Value))
            if (tn.ChildNodes.Count>0) tn.PopulateOnDemand = true;
        }
    }
    protected void LoadTreeViewChildren(TreeNode tn)
    {
        ViewState["Loaded"] = false;
        tn.ChildNodes.Clear();
        using (Contentum.eRecord.Service.EREC_IrattariHelyekService service = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            ExecParam execParam = UI.SetExecParamDefault(Page);
            // CR3246 Irattári Helyek kezelésének módosítása
            if (!String.IsNullOrEmpty(tn.Value))
            {
                var c = service.IrattariHelyStrukturaGetByRoots(execParam, tn.Value);
                if (c != null)
                {
                    if (!String.IsNullOrEmpty(c.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, c);
                    }
                }
                if (c.Ds != null)
                {
                    foreach (DataRow child in c.Ds.Tables[0].Rows)
                    {
                        if (tn.Parent != null)
                            RecursiveChild(tn.Parent.ChildNodes, child);
                    }
                }
            }
        }
        ViewState["Loaded"] = true;
    }
    public void RecursiveChild(TreeNodeCollection tn, DataRow child)
    {
        for (int i = 0; i < tn.Count; ++i)
        {
            if (tn[i].Value == child["SzuloId"].ToString())
            {
                tn[i].ChildNodes.Add(new TreeNode(child["Ertek"].ToString(), child["Id"].ToString()));
                tn[i].Expanded = false;
            }
            RecursiveChild(tn[i].ChildNodes, child);
        }  
    }
    public void ExpandSelectedNodeAllLevels()
    {
        if (TreeView1.SelectedNode != null)
        {
            TreeView1.SelectedNode.ExpandAll();
            Session["NewIrattariHelyId"] = "";
            TreeViewUpdatePanel.Update();
        }
    }

    // CR3246 Irattári Helyek kezelésének módosítása
    public TreeNode FindByValue(string searchstring)
    {
        try
        {
            for (int i = 0; i < TreeView1.Nodes.Count; i++)
            {
                TreeNode trNode = TreeView1.Nodes[i];
                if (trNode.Value == searchstring)
                    return trNode;
                else
                {
                   
                    TreeNode trAnswerNode = FindChildNodes(trNode, searchstring);
                    if (trAnswerNode != null)
                    {
                        return trAnswerNode;
                    }
                }
            }
            return null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private TreeNode FindChildNodes(TreeNode trNode, string searchstring)
    {
        try
        {
            for (int i = 0; i < trNode.ChildNodes.Count; i++)
            {
                TreeNode trChildNode = trNode.ChildNodes[i];
                if (trChildNode.Value == searchstring)
                    return trChildNode;
                else
                {
                    TreeNode trAnswerNode = FindChildNodes(trChildNode, searchstring);
                    if (trAnswerNode != null)
                    {
                        return trAnswerNode;
                    }
                }
            }
            return null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void ExpandAllParents(TreeNode trNode)
    {
        try
        {
            trNode.Expanded = true;
            if (trNode.Parent == null)
            {
                return;
            } else
            {
                trNode = trNode.Parent;
                ExpandAllParents(trNode);
            }
               
          
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

}