﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IrattariHelyLevelekDropDown.ascx.cs" Inherits="eRecordComponent_IrattariHelyLevelekDropDown" %>

<asp:DropDownList ID="IrattarLevelekDropdownList" EnableViewState="true" AutoPostBack="true" runat="server" CssClass="mrUrlapInputComboBox" OnSelectedIndexChanged="dropDownList1_SelectedIndexChanged" >
</asp:DropDownList>
 <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"  InitialValue="" ErrorMessage="<%$Resources:Form,IrattariHelyNotSelected%>" Display="None" ControlToValidate="IrattarLevelekDropdownList" Enabled="true"></asp:RequiredFieldValidator>
 <ajaxToolkit:ValidatorCalloutExtender id="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator1"></ajaxToolkit:ValidatorCalloutExtender>