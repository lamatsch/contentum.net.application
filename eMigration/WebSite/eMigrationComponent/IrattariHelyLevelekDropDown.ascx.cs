﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eUIControls;

public partial class eRecordComponent_IrattariHelyLevelekDropDown : System.Web.UI.UserControl
{
    private readonly ListItem emptyListItem = new ListItem(Resources.Form.EmptyListItem, "");

    protected void Page_Load(object sender, EventArgs e)
    {
        IrattarLevelekDropdownList.SelectedIndexChanged += new EventHandler(dropDownList1_SelectedIndexChanged);          
    }
    public eDropDownList DropDownList
    {
        get { return IrattarLevelekDropdownList; }
    }
    /// <summary>
    /// Beállítja a már feltöltött lista értékét a megadottra, ha az létezik. 
    /// Ha nem létezik, új elemként felvéve, és megjelölve, hogy ez egy nem létező kódtárérték
    /// </summary>
    /// <param name="selectedValue"></param>
    public void SetSelectedValue(String selectedValue)
    {
        IrattarLevelekDropdownList.SelectedValue = selectedValue;
    }
    public String SelectedValue
    {
        get { return IrattarLevelekDropdownList.SelectedValue; }
        set { SetSelectedValue(value); }
    }

    public bool Validate
    {
        get { return RequiredFieldValidator1.Enabled; }
        set { RequiredFieldValidator1.Enabled = value; }
    }

    public bool Enabled
    {
        get { return IrattarLevelekDropdownList.Enabled; }
        set { IrattarLevelekDropdownList.Enabled = value; }
    }

    public Unit Width
    {
        get { return IrattarLevelekDropdownList.Width; }
        set { IrattarLevelekDropdownList.Width = value; }
    }

    public string CssClass
    {
        get { return IrattarLevelekDropdownList.CssClass; }
        set { IrattarLevelekDropdownList.CssClass = value; }
    }

    public string ValidationGroup
    {
        set
        {
            IrattarLevelekDropdownList.ValidationGroup = value;
        }
        get { return IrattarLevelekDropdownList.ValidationGroup; }
    }
    public event EventHandler SelectedIndexChanged;
    /// <summary>
    /// Method executed with an item from the dropdownlist is selected.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelectedValue = IrattarLevelekDropdownList.SelectedItem.Value;
        if (SelectedIndexChanged!=null)
            SelectedIndexChanged(sender, e);
    }

    /// <summary>
    /// Feltölti a DropDownList-et
    /// </summary>
    /// <param name="errorPanel"></param>
    public void FillDropDownList(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, String IrattarTipus, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        IrattarLevelekDropdownList.Items.Clear();

        if (!Res.IsError && Res.Ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < Res.Ds.Tables[0].Rows.Count; i++)
            {
                if (Res.Ds.Tables[0].Columns.Contains("IrattarTipus") && !string.IsNullOrEmpty(Res.Ds.Tables[0].Rows[i]["IrattarTipus"].ToString()) && !String.IsNullOrEmpty(IrattarTipus))
                {
                    if (Res.Ds.Tables[0].Rows[i]["IrattarTipus"].ToString() != IrattarTipus)
                    {
                        continue;
                    }
                }
                IrattarLevelekDropdownList.Items.Add(new ListItem(Res.Ds.Tables[0].Rows[i][DisplayColumnName].ToString(), Res.Ds.Tables[0].Rows[i][IdColumnName].ToString()));
            }

            if (addEmptyItem)
            {
                IrattarLevelekDropdownList.Items.Insert(0, emptyListItem);
            }
        }
    }

    public void FillDropDownList(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName,String IrattarTipus, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList(Res, IdColumnName, DisplayColumnName,IrattarTipus ,false, errorPanel);
    }

    /// <summary>
    /// Feltölti a DropDownList-et
    /// </summary>
    /// <param name="errorPanel"></param>
    public void FillDropDownList(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList(Res, IdColumnName, DisplayColumnName, null, addEmptyItem, errorPanel);
    }

    public void FillDropDownList(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList(Res, IdColumnName, DisplayColumnName, false, errorPanel);
    }

}