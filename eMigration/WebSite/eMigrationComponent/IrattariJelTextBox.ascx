﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IrattariJelTextBox.ascx.cs" Inherits="eMigrationComponent_IrattariJelTextBox" %>

<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc" %>

<div class="DisableWrap">
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:TextBox ID="txtIrattariJel" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
    <asp:ImageButton TabIndex = "-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')" CssClass="mrUrlapInputImageButton" AlternateText="Kiválaszt" />
    <asp:ImageButton TabIndex = "-1" ID="ResetImageButton" runat="server" Visible="false" ImageUrl="~/images/hu/egyeb/reset_icon.png" onmouseover="swapByName(this.id,'reset_icon_keret.png')" onmouseout="swapByName(this.id,'reset_icon.png')" AlternateText="Alapállapot"/>
    <asp:RequiredFieldValidator ID="reqValidator" runat="server" ControlToValidate="txtIrattariJel" SetFocusOnError="true"
    Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>" Enabled="false"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="reqValidator">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
     <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="true" MinimumPrefixLength="2" 
      TargetControlID="txtIrattariJel" CompletionSetCount="20" ContextKey="" UseContextKey="true" FirstRowSelected="true" 
      ServicePath="~/WrappedWebService/Ajax.asmx" ServiceMethod="GetIrattariJelList"
      CompletionListItemCssClass="GridViewRowStyle" CompletionListHighlightedItemCssClass="GridViewLovListSelectedRowStyle">
    </ajaxToolkit:AutoCompleteExtender>
</div>