﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eMigration.Utility;

public partial class eMigrationComponent_IrattariJelTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string Text
    {
        set { txtIrattariJel.Text = value; }
        get { return txtIrattariJel.Text; }
    }

    private bool _ReadOnly = false;
    public bool ReadOnly
    {
        set
        {
            _ReadOnly = value;
            LovImageButton.Visible = !value;
            ResetImageButton.Visible = !value;
            // BUG_8481 
            // Bug kapcsán javítva
            txtIrattariJel.ReadOnly = value;
        }
        get 
        { 
            return _ReadOnly; 
        }
    }

    public bool Enabled
    {
        set
        {
            txtIrattariJel.Enabled = value;
            LovImageButton.Visible = value;
            ResetImageButton.Visible = value;

        }
        get { return txtIrattariJel.Enabled; }
    }

    private bool _SearchMode = false;

    public bool SearchMode
    {
        get
        {
            return _SearchMode;
        }
        set
        {
            _SearchMode = value;
            ResetImageButton.Visible = value;
        }
    }

    public bool Validate
    {
        get
        {
            return reqValidator.Enabled;
        }
        set
        {
            reqValidator.Enabled = value;
        }
    }

    private string _evTextBoxControlID;

    public string EvTextBoxControlID
    {
        get { return _evTextBoxControlID; }
        set { _evTextBoxControlID = value; }
    }

    private bool _isEvFilterDisabled = false;

    public bool IsEvFilterDisabled
    {
        get { return _isEvFilterDisabled; }
        set { _isEvFilterDisabled = value; }
    }

    private bool _IsCustomValueEnabled = false;

    public bool IsCustomValueEnabled
    {
        get { return _IsCustomValueEnabled; }
        set { _IsCustomValueEnabled = value; }
    }

    public TextBox TextBox
    {
        get
        {
            return txtIrattariJel;
        }
    }

    #endregion

    private TextBox GetEvTextBox()
    {
        TextBox evTextBox = null;

        if (!String.IsNullOrEmpty(EvTextBoxControlID))
        {
            Control c = NamingContainer.FindControl(EvTextBoxControlID);

            if (c != null)
            {
                if (c is TextBox)
                {
                    evTextBox = c as TextBox;
                }

                if (c is Component_RequiredNumberBox)
                {
                    evTextBox = (c as Component_RequiredNumberBox).TextBox;
                }


            }
        }

        return evTextBox;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        AutoCompleteExtender1.ContextKey = felh_Id;

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsCustomValueEnabled)
        {
            txtIrattariJel.Attributes.Add("readonly", "readonly");
        }

        string qs = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
          + "&" +  QueryStringVars.TextBoxId + "=" + txtIrattariJel.ClientID;

        TextBox evTextBox = GetEvTextBox();

        if(evTextBox != null)
        {
            qs += "&" + QueryStringVars.Ev + "=" + "' + $get('" + evTextBox.ClientID + "').value + '";
        }

        if(IsEvFilterDisabled)
        {
            qs += "&" + QueryStringVars.IsEvFilterDisabled + "=1";
        }
        
        OnClick_Lov = JavaScripts.SetOnClientClick("IrattariJelLovList.aspx",
          qs
          , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        ResetImageButton.OnClientClick = "$get('"+ txtIrattariJel.ClientID + "').value = '';return false";
    }

    #region ISelectableUserComponent method implementations

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                UI.AddCssClass(txtIrattariJel,"ViewReadOnlyWebControl");
                UI.AddCssClass(LovImageButton,"ViewReadOnlyWebControl");
                UI.AddCssClass(ResetImageButton,"ViewReadOnlyWebControl");
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                UI.AddCssClass(txtIrattariJel,"ViewDisabledWebControl");
                UI.AddCssClass(LovImageButton,"ViewDisabledWebControl");
                UI.AddCssClass(ResetImageButton,"ViewDisabledWebControl");
            }
        }
    }

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(txtIrattariJel);
        componentList.Add(LovImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validate = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
