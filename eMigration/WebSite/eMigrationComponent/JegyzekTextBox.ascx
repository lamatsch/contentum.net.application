<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JegyzekTextBox.ascx.cs" Inherits="eMigrationComponent_JegyzekTextBox" %>
<div class="DisableWrap">
<asp:TextBox ID="txtJegyzekNev" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
<asp:ImageButton TabIndex = "-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')"
 CssClass="mrUrlapInputImageButton" AlternateText="Kiv�laszt" />
 <asp:ImageButton TabIndex = "-1" ID="NewImageButton" runat="server" CssClass="mrUrlapInputImageButton"
    ImageUrl="~/images/hu/lov/hozzaad.gif" onmouseover="swapByName(this.id,'hozzaad_keret.gif')" onmouseout="swapByName(this.id,'hozzaad.gif')" AlternateText="�j" />
    <asp:ImageButton TabIndex = "-1" ID="ViewImageButton" runat="server" CssClass="mrUrlapInputImageButton"
    ImageUrl="~/images/hu/egyeb/nagyito.gif" onmouseover="swapByName(this.id,'nagyito_keret.gif')" onmouseout="swapByName(this.id,'nagyito.gif')" AlternateText="Megtekint" />
   <asp:ImageButton TabIndex = "-1" ID="ResetImageButton" runat="server" Visible="false" ImageUrl="~/images/hu/egyeb/reset_icon.png" onmouseover="swapByName(this.id,'reset_icon_keret.png')" onmouseout="swapByName(this.id,'reset_icon.png')" AlternateText="Alap�llapot"/>
    <asp:HiddenField ID="HiddenField1" runat="server" />
<asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="txtJegyzekNev" SetFocusOnError="true"
    Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
</div>