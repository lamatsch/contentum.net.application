﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eMigration.Utility;

public partial class eMigrationComponent_LovTextBox : System.Web.UI.UserControl, Contentum.eAdmin.Utility.ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string Column { get; set; }

    public string Text
    {
        set { txtErtek.Text = value; }
        get { return txtErtek.Text; }
    }

    private bool _ReadOnly = false;
    public bool ReadOnly
    {
        set
        {
            _ReadOnly = value;
            LovImageButton.Visible = !value;
            ResetImageButton.Visible = !value;
            txtErtek.ReadOnly = value;
        }
        get 
        { 
            return _ReadOnly; 
        }
    }

    public bool Enabled
    {
        set
        {
            txtErtek.Enabled = value;
            LovImageButton.Visible = value;
            ResetImageButton.Visible = value;
        }
        get { return txtErtek.Enabled; }
    }

    private bool _SearchMode = false;

    public bool SearchMode
    {
        get
        {
            return _SearchMode;
        }
        set
        {
            _SearchMode = value;
            ResetImageButton.Visible = value;
        }
    }

    public bool Validate
    {
        get
        {
            return reqValidator.Enabled;
        }
        set
        {
            reqValidator.Enabled = value;
        }
    }

    private bool _IsCustomValueEnabled = true; // required for autocomplete

    public bool IsCustomValueEnabled
    {
        get { return _IsCustomValueEnabled; }
        set { _IsCustomValueEnabled = value; }
    }

    public TextBox TextBox
    {
        get
        {
            return txtErtek;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        AutoCompleteExtender1.ContextKey = Column; // felh_Id;

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsCustomValueEnabled)
        {
            txtErtek.Attributes.Add("readonly", "readonly");
        }

        string qs = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
          + "&" +  QueryStringVars.TextBoxId + "=" + txtErtek.ClientID + "&column=" + Column;

        OnClick_Lov = JavaScripts.SetOnClientClick("ErtekLovList.aspx", qs, Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        ResetImageButton.OnClientClick = "$get('"+ txtErtek.ClientID + "').value = '';return false";
    }

    #region ISelectableUserComponent method implementations

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                UI.AddCssClass(txtErtek,"ViewReadOnlyWebControl");
                UI.AddCssClass(LovImageButton,"ViewReadOnlyWebControl");
                UI.AddCssClass(ResetImageButton,"ViewReadOnlyWebControl");
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                UI.AddCssClass(txtErtek, "ViewDisabledWebControl");
                UI.AddCssClass(LovImageButton,"ViewDisabledWebControl");
                UI.AddCssClass(ResetImageButton,"ViewDisabledWebControl");
            }
        }
    }

    public List<WebControl> GetComponentList()
    {
        var componentList = new List<WebControl>();

        componentList.Add(txtErtek);
        componentList.Add(LovImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validate = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
