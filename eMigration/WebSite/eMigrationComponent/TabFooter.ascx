<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TabFooter.ascx.cs" Inherits="eMigrationComponent_TabFooter" %>

<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <asp:ImageButton ID="ImageSave" TabIndex="1" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')"
                OnClick="ImageButton_Click" CommandName="Save" Visible="False" CausesValidation="true" />
        </td>
        <td>
            <asp:ImageButton ID="ImageClose" TabIndex="2"  runat="server" Visible="false" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                OnClientClick="window.close(); return true;" OnClick="ImageButton_Click" CommandName="Close" />
        </td>
        <td>
            <asp:ImageButton ID="ImageSaveAndClose" TabIndex="3"  runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_bezar.png"
                onmouseover="swapByName(this.id,'rendben_es_bezar2.png')" onmouseout="swapByName(this.id,'rendben_es_bezar.png')"
                CommandName="SaveAndClose" OnClick="ImageButton_Click" Visible="False" CausesValidation="true" />
        </td>
        <td>
            <asp:ImageButton ID="ImageSaveAndNew" TabIndex="4"  runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_uj.png"
                onmouseover="swapByName(this.id,'rendben_es_uj2.png')" onmouseout="swapByName(this.id,'rendben_es_uj.png')"
                CommandName="SaveAndNew" HAlign="right" OnClick="ImageButton_Click" Visible="False"
                CausesValidation="true" />
        </td>
        <td>
            <asp:HyperLink ID="linkSaveAndNew" TabIndex="5"  runat="server" Visible = "false">
                <asp:Image ID="imgSaveAndNew" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben_es_uj.png"
                    onmouseover="swapByName(this.id,'rendben_es_uj2.png')" onmouseout="swapByName(this.id,'rendben_es_uj.png')"
                    />
            </asp:HyperLink>    
        </td>
        <td>
            <asp:ImageButton ID="ImageCancel" TabIndex="13"  runat="server" ImageUrl="~/images/hu/ovalgomb/megsem.jpg"
                onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')"
                CommandName="Cancel" OnClick="ImageButton_Click"
                Visible="False" CausesValidation="false"/></td>
    </tr>
</table>

     

