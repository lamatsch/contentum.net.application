using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using System.Linq;

public partial class eMigrationComponent_VonalKodTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{
    private const string bgSoundHtmlId = "SoundContainer";
    private string Command = "";
    #region public properties

    private bool validate = true;

    public bool Validate
    {
        set
        {
            SetValidate(value);
        }
        get { return validate; }
    }

    private bool _setValidateInPageInit = false;
    private void SetValidate(bool value)
    {
        if (pageInitialized)
        {
            _setValidateInPageInit = false;
            if (!Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
            {
                Validator1.Enabled = value;
                CustomValidatorBarcodeCheckSum.Enabled = value;
                validate = value;
            }
            else
            {
                Validator1.Enabled = false;
                CustomValidatorBarcodeCheckSum.Enabled = false;
                validate = false;
            }
        }
        else
        {
            _setValidateInPageInit = true;
            validate = value;
        }
    }

    public bool RequiredValidate
    {
        get { return Validator1.Enabled; }
        set { Validator1.Enabled = value; }
    }

    public bool FormatValidate
    {
        get { return CustomValidatorBarcodeCheckSum.Enabled; }
        set { CustomValidatorBarcodeCheckSum.Enabled = value; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            TextBox.ValidationGroup = value;

        }
        get { return Validator1.ValidationGroup; }
    }

    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    // ClientID t�mb pl. javascriptb�l val� letilt�shoz
    public string[] ValidatorClientIDs
    {
        get
        {
            string[] result = new string[2];
            result[0] = Validator1.ClientID;
            result[1] = CustomValidatorBarcodeCheckSum.ClientID;
            return result;
        }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }

    public bool Enabled
    {
        set
        {
            TextBox.Enabled = value;

        }
        get { return TextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            TextBox.ReadOnly = value;
            if (value == true)
            {
                Validate = false;
            }
        }
        get { return TextBox.ReadOnly; }
    }

    public Unit Width
    {
        get { return TextBox.Width; }
        set { TextBox.Width = value; }
    }

    public string Text
    {
        set { TextBox.Text = value; }
        get { return TextBox.Text; }
    }

    public TextBox TextBox
    {
        get { return vonalkodTextBox; }
    }


    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
        }
    }


    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
            }
        }
    }

    public string CssClass
    {
        get { return TextBox.CssClass; }
        set { TextBox.CssClass = value; }
    }

    private string postKeyUpEventJs = "";
    public string PostKeyUpEventJs
    {
        get { return postKeyUpEventJs; }
        set { postKeyUpEventJs = value; }
    }

    public string BehaviorID
    {
        get
        {
            return this.TextBox.ClientID + "_Behavior";
        }
    }

    // JavaScript sound effects
    private bool _playSoundOnError = false;
    public bool PlaySoundOnError
    {
        get { return _playSoundOnError; }
        set { _playSoundOnError = value; }
    }

    private bool _playSoundOnAccept = false;
    public bool PlaySoundOnAccept
    {
        get { return _playSoundOnAccept; }
        set { _playSoundOnAccept = value; }
    }

    private string _soundOnError = "sounds/barcodeOnError.wav";
    public string SoundOnError
    {
        get { return _soundOnError; }
        set { _soundOnError = value; }
    }

    private string _soundOnAccept = "sounds/barcodeOnAccept.wav";
    public string SoundOnAccept
    {
        get { return _soundOnAccept; }
        set { _soundOnAccept = value; }
    }

    #endregion    
    private bool _forceBarcodeCheck = false;

    /// <summary>
    /// BOPMH eset�n nincs vonalk�d ellen�rz�s ha az �rt�ke true kik�nyszer�ti az ellen�rz�st.
    /// </summary>
    public bool ForceBarcodeCheck
    {
        get { return _forceBarcodeCheck; }
        set { _forceBarcodeCheck = value; }
    }

    private bool pageInitialized = false;
    protected void Page_Init(object sender, EventArgs e)
    {
        pageInitialized = true;

        if (_setValidateInPageInit)
        {
            SetValidate(validate);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlGenericControl bgsound = (HtmlGenericControl)Page.Header.FindControl(bgSoundHtmlId);
        if (bgsound == null)
        {
            bgsound = new HtmlGenericControl("bgsound");
            bgsound.Attributes.Add("id", bgSoundHtmlId);
            bgsound.Attributes.Add("src", "");
            bgsound.Attributes.Add("loop", "1");
            bgsound.ID = bgSoundHtmlId;
            Page.Header.Controls.Add(bgsound);
        }
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        string js = String.Empty;
        if (postKeyUpEventJs.Length > 0)
        {
            js += postKeyUpEventJs;
        }
        TextBox.Attributes.Add("onkeyup", js);
        Command = Request.QueryString.Get(CommandName.Command);

        //CR3058
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO") && !ForceBarcodeCheck)
        {
            Validator1.Enabled = false;
            CustomValidatorBarcodeCheckSum.Enabled = false;
            //vonalkodTextBox.ReadOnly = true;
        }
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(vonalkodTextBox);

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);

            if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
            {
                Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

                if (c != null)
                {
                    c.Visible = Validate;
                }
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        if (this.TextBox.Visible)
        {
            ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.BarCodeBehavior", this.TextBox.ClientID);
            string CheckVektor = Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_ELLENORZO_VEKTOR);
            #region CR3225 - A vonalk�dos iratmozgat�sok nem m�k�dnek a Buda�rsi k�rnyzet alatt.
            if (!String.IsNullOrEmpty(CheckVektor) && (!Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO")) || ForceBarcodeCheck)
            {
                descriptor.AddProperty("CheckVektor", CheckVektor);
            }
            #endregion
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.VONALKOD_MAGYAR_KARAKTEREK_CSEREJE))
            {
                descriptor.AddProperty("ReplaceHungarianCharacters", true);
            }
            descriptor.AddProperty("id", this.BehaviorID);
            descriptor.AddProperty("ChecksumValidatorId", CustomValidatorBarcodeCheckSum.ClientID);
            descriptor.AddProperty("ChecksumValidatorCalloutId", ValidatorCalloutExtenderBarcodeCheckSum.ClientID);

            HtmlGenericControl bgsound = (HtmlGenericControl)Page.Header.FindControl(bgSoundHtmlId);
            if (bgsound != null)
            {
                descriptor.AddProperty("BgSoundId", bgsound.ClientID);
                //descriptor.AddProperty("PlaySoundOnError", false);
                //descriptor.AddProperty("PlaySoundOnAccept", false);
                descriptor.AddProperty("SoundOnError", SoundOnError);
                descriptor.AddProperty("SoundOnAccept", SoundOnAccept);
            }

            return new ScriptDescriptor[] { descriptor };
        }

        return null;
    }

    private static string GenerateVersionString(int length)
    {
        Random random = new Random();
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return new string(Enumerable.Repeat(chars, length)
          .Select(s => s[random.Next(s.Length)]).ToArray());
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        //BUG 2886 - vegye �szre, ha v�ltozott a javascript
        reference.Path = "~/JavaScripts/BarCodeBehavior.js?v=" + GenerateVersionString(12);

        return new ScriptReference[] { reference };
    }

    #endregion
}
