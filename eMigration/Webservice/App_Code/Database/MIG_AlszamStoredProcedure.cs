using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.EnterpriseServices;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for MIG_AlszamStoredProcedure
/// </summary>
public partial class MIG_AlszamStoredProcedure
{
    //private SqlConnection Connection;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_MIG_AlszamInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_MIG_AlszamUpdateParameters = null;

    public MIG_AlszamStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        /*try
        {
            Connection = Database.Connect(App);
        }
        catch (Exception e)
        {
            throw e;
        }*/
    }

    public Result Get(ExecParam ExecParam)
    {
        Logger.Info("Alsz�m Get Start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_AlszamGet"); 
        
        Result _ret = new Result();
        try
        {
            Logger.Info("sp_MIG_AlszamGet h�v�sa");
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_AlszamGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            /*SqlDataReader dr = SqlComm.ExecuteReader();

            if (dr.HasRows)
            {
                MIG_Alszam _MIG_Alszam = new MIG_Alszam();
                Utility.LoadBusinessDocumentFromSqlDataReader(_MIG_Alszam, dr);
                _ret.Record = _MIG_Alszam;
            }

            dr.Close();*/
            MIG_Alszam _MIG_Alszam = new MIG_Alszam();
            Utility.LoadBusinessDocumentFromDataAdapter(_MIG_Alszam, SqlComm);
            _ret.Record = _MIG_Alszam;

        }
        catch (SqlException e)
        {
            Logger.Error("Alsz�m Get error: " + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        Logger.Info("Alsz�m Get End");
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, MIG_AlszamSearch _MIG_AlszamSearch)
    {
        Logger.Info("Alsz�m GetAll Start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_AlszamGetAll");

        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_AlszamSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_AlszamSearch.WhereByManual;
            Logger.Info("sp_MIG_AlszamGetAll h�v�sa");
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_AlszamGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_AlszamSearch.OrderBy, _MIG_AlszamSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            Logger.Error("Alsz�m GetAll error: " + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        Logger.Info("Alsz�m GetAll end.");
        return _ret;

    }

    #region Depricated
    public Result Insert(String Method, ExecParam ExecParam, MIG_Alszam Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, MIG_Alszam Record, DateTime ExecutionTime)
    {
        Result _ret = new Result();

        System.Data.SqlClient.SqlCommand SqlComm = null;

        if (Method == "") Method = "Insert";
        switch (Method)
        {
            case Constants.Insert:
                Record.Base.Updated.LetrehozasIdo = true;
                Record.Base.Updated.Letrehozo_id = true;
                Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_AlszamInsert]");
                if (sp_MIG_AlszamInsertParameters == null)
                {
                    sp_MIG_AlszamInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_AlszamInsert]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_AlszamInsertParameters);

                Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

                break;
            case Constants.Update:
                Record.Base.Updated.ModositasIdo = true;
                Record.Base.Updated.Modosito_id = true;
                Record.Base.ModositasIdo = ExecutionTime.ToString();
                Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_AlszamUpdate]");
                if (sp_MIG_AlszamUpdateParameters == null)
                {
                    sp_MIG_AlszamUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_AlszamUpdate]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_AlszamUpdateParameters);

                Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);
               

                break;
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
    

        return _ret;
    }

    #endregion

    ///// <summary>
    ///// ExecParam.RecordId-j� Record UGYHOL mez�j�nek m�dos�t�sa a Record.UGYHOL-ban megadott �rt�kre
    ///// </summary>
    ///// <param name="ExecParam"></param>
    ///// <param name="Record"></param>
    ///// <returns></returns>
    //public Result AlszamAthelyezes(ExecParam ExecParam, MIG_Alszam Record)
    //{
    //    Result _ret = new Result();
    //    try
    //    {
    //        Logger.Info("sp_MIG_AlszamAthelyezes h�v�sa");
    //        SqlCommand SqlComm = new SqlCommand("[sp_MIG_AlszamAthelyezes]");
    //        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
    //        SqlComm.Connection = dataContext.Connection;
    //        SqlComm.Transaction = dataContext.Transaction;

    //        SqlComm.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Value = SqlGuid.Parse(ExecParam.Record_Id);
    //        SqlComm.Parameters.Add("@Ugyhova", SqlDbType.NVarChar).Value = Record.UGYHOL;

    //        if (Record.Updated.SCONTRO == true)
    //        {
    //            SqlComm.Parameters.Add("@SkontroVege", SqlDbType.DateTime).Value = SqlDateTime.Parse(Record.SCONTRO);
    //        }

    //        SqlComm.ExecuteNonQuery();


    //    }
    //    catch (SqlException e)
    //    {
    //        Logger.Error("Alsz�m �thelyez�s error: " + e.Message);
    //        _ret.ErrorCode = e.ErrorCode.ToString();
    //        _ret.ErrorMessage = e.Message;
    //    }

    //    Logger.Info("Alsz�m �thelyez�s End");
    //    return _ret;
    //}

}
