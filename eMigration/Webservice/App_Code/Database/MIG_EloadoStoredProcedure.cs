using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.EnterpriseServices;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for MIG_EloadoStoredProcedure
/// </summary>
public partial class MIG_EloadoStoredProcedure
{
    //private SqlConnection Connection;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_MIG_EloadoInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_MIG_EloadoUpdateParameters = null;

    public MIG_EloadoStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        /*try
        {
            Connection = Database.Connect(App);
        }
        catch (Exception e)
        {
            throw e;
        }*/
    }

    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_EloadoGet");

        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_EloadoGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            /*SqlDataReader dr = SqlComm.ExecuteReader();

            if (dr.HasRows)
            {
                MIG_Eloado _MIG_Eloado = new MIG_Eloado();
                Utility.LoadBusinessDocumentFromSqlDataReader(_MIG_Eloado, dr);
                _ret.Record = _MIG_Eloado;
            }

            dr.Close();*/
            MIG_Eloado _MIG_Eloado = new MIG_Eloado();
            Utility.LoadBusinessDocumentFromDataAdapter(_MIG_Eloado, SqlComm);
            _ret.Record = _MIG_Eloado;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, MIG_EloadoSearch _MIG_EloadoSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_EloadoGetAll");

        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_EloadoSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_EloadoSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_EloadoGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_EloadoSearch.OrderBy, _MIG_EloadoSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}
