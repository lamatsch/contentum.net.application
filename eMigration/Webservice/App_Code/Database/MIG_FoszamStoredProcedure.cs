using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.EnterpriseServices;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for MIG_FoszamStoredProcedure
/// </summary>
public partial class MIG_FoszamStoredProcedure
{
    //private SqlConnection Connection;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_MIG_FoszamInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_MIG_FoszamUpdateParameters = null;

    public MIG_FoszamStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        /*try
        {
            Connection = Database.Connect(App);
        }
        catch (Exception e)
        {
            throw e;
        }*/
    }

    public Result Insert(String Method, ExecParam ExecParam, MIG_Foszam Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Csatolas(ExecParam ExecParam, MIG_Foszam Record)
    {
        Logger.Info("F�sz�m Csatol�s start");
        String Muvelet = "C";

        Result _ret = new Result();
        Logger.Info("F�sz�m csatol�s end");
        return _ret;
    }
    public Result Insert(String Method, ExecParam ExecParam, MIG_Foszam Record, DateTime ExecutionTime)
    {
        Logger.Info("Foszam Insert start");
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
        if (Method == "") Method = "Update";
        switch (Method)
        {
            case Constants.Update:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamUpdate");
                Record.Base.Updated.ModositasIdo = true;
                Record.Base.Updated.Modosito_id = true;
                Record.Base.ModositasIdo = ExecutionTime.ToString();
                Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                Logger.Info("sp_MIG_FoszamUpdate h�v�sa");
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_FoszamUpdate]");
                if (sp_MIG_FoszamUpdateParameters == null)
                {
                    //sp_MIG_FoszamUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_MIG_FoszamUpdate]");
                    //sp_MIG_FoszamUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_MIG_FoszamUpdate]");
                    sp_MIG_FoszamUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_FoszamUpdate]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_FoszamUpdateParameters);

                Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                //LZS 
                if (SqlComm.Parameters.Contains("@UI_SAV"))
                {
                    SqlComm.Parameters["@UI_SAV"].Value = Record.OLD_UI_SAV;
                }
                else
                {
                    SqlComm.Parameters.Add("@UI_SAV", SqlDbType.Int).Value = Record.OLD_UI_SAV;
                }

                break;
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            }
        }
        catch (SqlException e)
        {
            Logger.Error("Foszam Insert error: " + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);
        Logger.Info("Foszam Insert end.");
        return _ret;
    }

    public Result Get(ExecParam ExecParam)
    {
        Logger.Info("Foszam Get Start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamGet");

        Result _ret = new Result();
        try
        {
            Logger.Info("sp_MIG_FoszamGet h�v�sa");
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            /*SqlDataReader dr = SqlComm.ExecuteReader();

            if (dr.HasRows)
            {
                MIG_Foszam _MIG_Foszam = new MIG_Foszam();
                Utility.LoadBusinessDocumentFromSqlDataReader(_MIG_Foszam, dr);
                _ret.Record = _MIG_Foszam;
            }

            dr.Close();*/
            MIG_Foszam _MIG_Foszam = new MIG_Foszam();
            Utility.LoadBusinessDocumentFromDataAdapter(_MIG_Foszam, SqlComm);
            _ret.Record = _MIG_Foszam;

        }
        catch (SqlException e)
        {
            Logger.Error("F�sz�m Get error:" + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        Logger.Info("F�sz�m Get End");
        return _ret;

    }

    public Result GetAllDistinct(ExecParam ExecParam, string column, string where)
    {
        Logger.Info("F�sz�m GetAllDistinct Start");
        var log = Log.SpStart(ExecParam, "sp_MIG_FoszamGetAllDistinct");

        Result _ret = new Result();
        try
        {
            Logger.Info("sp_MIG_FoszamGetAllDistinct h�v�sa");
            var cmd = new SqlCommand("[sp_MIG_FoszamGetAllDistinct]");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Column", column);
            cmd.Parameters.AddWithValue("@Where", where);
            //cmd.Connection = Connection;
            cmd.Connection = dataContext.Connection;
            cmd.Transaction = dataContext.Transaction;

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(cmd);
            }
        }
        catch (SqlException e)
        {
            Logger.Error("F�sz�m GetAllDistinct error: " + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        Logger.Info("F�sz�m GetAllDistinct end");
        return _ret;
    }

    public Result GetAll(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Logger.Info("F�sz�m GetAll Start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamGetAll");

        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_FoszamSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (!String.IsNullOrEmpty(_MIG_FoszamSearch.WhereByManual))
            {
                //string normalizedStr = new Contentum.eQuery.FullTextSearch.SQLContainsCondition(_MIG_FoszamSearch.WhereByManual).Normalized;
                //normalizedStr = "contains(MIG_Foszam.*, '" + normalizedStr + "')";
                if (String.IsNullOrEmpty(query.Where))
                {
                    query.Where += _MIG_FoszamSearch.WhereByManual;
                    //query.Where += normalizedStr;
                }
                else
                {
                    query.Where += " and " + _MIG_FoszamSearch.WhereByManual;
                    //query.Where += " and " + normalizedStr;
                }
            }

            Logger.Info("sp_MIG_FoszamGetAll h�v�sa");
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_FoszamSearch.OrderBy, _MIG_FoszamSearch.TopRow);

            //if (!Search.IsIdentical(_MIG_FoszamSearch.ExtendedMIG_SavSearch, new MIG_SavSearch(),
            //    _MIG_FoszamSearch.ExtendedMIG_SavSearch.WhereByManual, new MIG_SavSearch().WhereByManual))
            //{
            //    Query savSearchQuery = new Query();
            //    savSearchQuery.BuildFromBusinessDocument(_MIG_FoszamSearch.ExtendedMIG_SavSearch);

            //    if (!string.IsNullOrEmpty(_MIG_FoszamSearch.ExtendedMIG_SavSearch.WhereByManual))
            //    {
            //        // EB 2010.02.15: egyel�re kikapcsolva, mert �t kell el�bb n�zni hozz� minden h�v�t
            //        //if (!String.IsNullOrEmpty(savSearchQuery.Where))
            //        //{
            //        //    _MIG_FoszamSearch.ExtendedMIG_SavSearch.WhereByManual += " and ";
            //        //}

            //        // EB 2011.05.06: and hozz�f�z�s a WhereByManualhoz, nem a Where-hez, m�sk�pp hib�s gener�l�s
            //        if (String.IsNullOrEmpty(savSearchQuery.Where))
            //        {
            //            savSearchQuery.Where += _MIG_FoszamSearch.ExtendedMIG_SavSearch.WhereByManual;
            //        }
            //        else
            //        {
            //            savSearchQuery.Where += " and " + _MIG_FoszamSearch.ExtendedMIG_SavSearch.WhereByManual;
            //        }
            //    }

            //    SqlComm.Parameters.Add("@ExtendedMIG_SavWhere", SqlDbType.NVarChar);
            //    //SqlComm.Parameters["@ExtendedMIG_SavWhere"].Size = 4000;
            //    SqlComm.Parameters["@ExtendedMIG_SavWhere"].Value = savSearchQuery.Where;
            //}

            /// Plusz Where felt�teleket k�l�n param�terben �tadva a t�rolt elj�r�snak:
            if (!Search.IsIdentical(_MIG_FoszamSearch.ExtendedMIG_AlszamSearch, new MIG_AlszamSearch(),
                _MIG_FoszamSearch.ExtendedMIG_AlszamSearch.WhereByManual, new MIG_AlszamSearch().WhereByManual))
            {
                Query query_MIG_Alszam = new Query();
                query_MIG_Alszam.BuildFromBusinessDocument(_MIG_FoszamSearch.ExtendedMIG_AlszamSearch);
                query_MIG_Alszam.Where += _MIG_FoszamSearch.ExtendedMIG_AlszamSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExtendedMIG_AlszamWhere", System.Data.SqlDbType.NVarChar));
                //SqlComm.Parameters["@ExtendedMIG_AlszamWhere"].Size = 4000;
                SqlComm.Parameters["@ExtendedMIG_AlszamWhere"].Value = query_MIG_Alszam.Where;
            }

            if (!Search.IsIdentical(_MIG_FoszamSearch.ExtendedMIG_EloadoSearch, new MIG_EloadoSearch(),
                _MIG_FoszamSearch.ExtendedMIG_EloadoSearch.WhereByManual, new MIG_EloadoSearch().WhereByManual))
            {
                Query query_MIG_Eloado = new Query();
                query_MIG_Eloado.BuildFromBusinessDocument(_MIG_FoszamSearch.ExtendedMIG_EloadoSearch);
                query_MIG_Eloado.Where += _MIG_FoszamSearch.ExtendedMIG_EloadoSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExtendedMIG_EloadoWhere", System.Data.SqlDbType.NVarChar));
                //SqlComm.Parameters["@ExtendedMIG_EloadoWhere"].Size = 4000;
                SqlComm.Parameters["@ExtendedMIG_EloadoWhere"].Value = query_MIG_Eloado.Where;
            }

            if (!String.IsNullOrEmpty(_MIG_FoszamSearch.FilterFelulvizsgalat))
            {
                SqlComm.Parameters.Add("@FilterFelulvizsgalat", SqlDbType.NVarChar).Value = _MIG_FoszamSearch.FilterFelulvizsgalat;
            }

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }
        }
        catch (SqlException e)
        {
            Logger.Error("F�sz�m GetAll error: " + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        Logger.Info("F�sz�m GetAll end");
        return _ret;

    }

    public Result ForXml(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamForXml");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_FoszamSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (!String.IsNullOrEmpty(_MIG_FoszamSearch.WhereByManual))
            {
                if (String.IsNullOrEmpty(query.Where))
                {
                    query.Where += _MIG_FoszamSearch.WhereByManual;
                }
                else
                {
                    query.Where += " and " + _MIG_FoszamSearch.WhereByManual;
                }
            }

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamForXml]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_FoszamSearch.OrderBy, _MIG_FoszamSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result ForXmlNincsUgyintezo(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamForXmlNincsUgyintezo");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_FoszamSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (!String.IsNullOrEmpty(_MIG_FoszamSearch.WhereByManual))
            {
                if (String.IsNullOrEmpty(query.Where))
                {
                    query.Where += _MIG_FoszamSearch.WhereByManual;
                }
                else
                {
                    query.Where += " and " + _MIG_FoszamSearch.WhereByManual;
                }
            }

            // t�blanevek cser�je az XML form�tumhoz haszn�lt nevek miatt
            query.Where = System.Text.RegularExpressions.Regex.Replace(query.Where, @"(?<c>[^.])MIG_Eloado", "${c}KRT_Felhasznalok");
            query.Where = System.Text.RegularExpressions.Regex.Replace(query.Where, @"(?<c>[^.])MIG_Foszam", "${c}EREC_UgyUgyiratok");

            _MIG_FoszamSearch.OrderBy = System.Text.RegularExpressions.Regex.Replace(_MIG_FoszamSearch.OrderBy, @"(?<c>[^.])MIG_Eloado", "${c}KRT_Felhasznalok");
            _MIG_FoszamSearch.OrderBy = System.Text.RegularExpressions.Regex.Replace(_MIG_FoszamSearch.OrderBy, @"(?<c>[^.])MIG_Foszam", "${c}EREC_UgyUgyiratok");

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamForXmlNincsUgyintezo]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_FoszamSearch.OrderBy, _MIG_FoszamSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result ForElszamoltatasiJk(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch, string UgyintezoId, string UgyintezoNev)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamForElszamoltatasiJk");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_FoszamSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_FoszamSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamForElszamoltatasiJk]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_FoszamSearch.OrderBy, _MIG_FoszamSearch.TopRow);

            if (!String.IsNullOrEmpty(UgyintezoId))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyintezoId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@UgyintezoId"].Value = SqlGuid.Parse(UgyintezoId);
            }

            if (!String.IsNullOrEmpty(UgyintezoNev))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyintezoNev", System.Data.SqlDbType.NVarChar, 100));
                SqlComm.Parameters["@UgyintezoNev"].Value = UgyintezoNev;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    //LZS - BUG_4747
    public Result ForElszamoltatasiJkSSRS(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch, string UgyintezoId, string UgyintezoNev)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokForElszamoltatasiJkSSRS");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_FoszamSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_FoszamSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokForElszamoltatasiJkSSRS]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_FoszamSearch.OrderBy, _MIG_FoszamSearch.TopRow);

            if (!String.IsNullOrEmpty(UgyintezoId))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyintezoId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@UgyintezoId"].Value = SqlGuid.Parse(UgyintezoId);
            }

            if (!String.IsNullOrEmpty(UgyintezoNev))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyintezoNev", System.Data.SqlDbType.NVarChar, 100));
                SqlComm.Parameters["@UgyintezoNev"].Value = UgyintezoNev;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllWithExtensionAndSzereltek(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamGetAllWithExtensionAndSzereltek");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_FoszamSearch, true);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _MIG_FoszamSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_MIG_FoszamSearch.WhereByManual))
            {
                query.Where += " and " + _MIG_FoszamSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamGetAllWithExtensionAndSzereltek]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_FoszamSearch.OrderBy, _MIG_FoszamSearch.TopRow);

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //        sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    public Result FoszamSzereles(ExecParam ExecParam, string sav, string ev, string foszam, string edok_ugyirat_id, string edok_ugyirat_azonosito, bool isVisszavonas)
    {
        Logger.DebugStart();
        Logger.Debug("FoszamSzereles sp start");
        Logger.Debug("Felhasznalo Id: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("Sav: " + sav);
        Logger.Debug("Ev: " + ev);
        Logger.Debug("Foszam: " + foszam);
        Logger.Debug("Edok_ugyirat_id: " + edok_ugyirat_id);
        Logger.Debug("Edok_ugyirat_azonosito: " + edok_ugyirat_azonosito);
        Logger.Debug("IsVisszavonas: " + isVisszavonas.ToString());
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamSzereles");

        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamSzereles]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Logger.Debug("Parameterek atadasa a tarolt eljarasnak start");
            SqlComm.Parameters.Add("@Sav", SqlDbType.NVarChar).Value = sav;
            SqlComm.Parameters.Add("@Year", SqlDbType.Int).Value = SqlInt32.Parse(ev);
            SqlComm.Parameters.Add("@Num", SqlDbType.Int).Value = SqlInt32.Parse(foszam);
            if (isVisszavonas)
            {
                SqlComm.Parameters.Add("@Visszavonas", SqlDbType.Int).Value = 1;
            }
            else
            {
                SqlComm.Parameters.Add("@Edok_Utoirat_Id", SqlDbType.UniqueIdentifier).Value = SqlGuid.Parse(edok_ugyirat_id);
                SqlComm.Parameters.Add("@Edok_Utoirat_Azon", SqlDbType.NVarChar).Value = edok_ugyirat_azonosito;
            }

            Utility.AddExecuteDefaultParameter(SqlComm, ExecParam, DateTime.Now);

            Logger.Debug("Parameterek atadasa a tarolt eljarasnak end");

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            Logger.Error("FoszamSzereles sp hiba:" + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        Logger.Debug("FoszamSzereles sp end");
        Logger.DebugEnd();
        return _ret;

    }
    public Result FoszamSzerelesMig(ExecParam ExecParam, string szerelendo_id, string csatolva_id, bool isVisszavonas)
    {
        Logger.DebugStart();
        Logger.Debug("FoszamSzerelesMig sp start");
        Logger.Debug("Felhasznalo Id: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("szerelendo_id :" + szerelendo_id);
        Logger.Debug("csatolva_id: " + csatolva_id);
        Logger.Debug("IsVisszavonas: " + isVisszavonas.ToString());
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamSzerelesMig");

        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamSzerelesMig]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Logger.Debug("Parameterek atadasa a tarolt eljarasnak start");
            SqlComm.Parameters.Add("@Szerelendo_Id", SqlDbType.UniqueIdentifier).Value = SqlGuid.Parse(szerelendo_id);
            if (isVisszavonas)
            {
                SqlComm.Parameters.Add("@Visszavonas", SqlDbType.Int).Value = 1;
            }
            else
            {
                SqlComm.Parameters.Add("@Csatolva_Id", SqlDbType.UniqueIdentifier).Value = SqlGuid.Parse(csatolva_id);
                SqlComm.Parameters.Add("@Csatolva_Rendszer", SqlDbType.Int).Value = 0;
            }

            Utility.AddExecuteDefaultParameter(SqlComm, ExecParam, DateTime.Now);

            Logger.Debug("Parameterek atadasa a tarolt eljarasnak end");

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            Logger.Error("FoszamSzerelesMig sp hiba:" + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        Logger.Debug("FoszamSzerelesMig sp end");
        Logger.DebugEnd();
        return _ret;

    }

    /// <summary>
    /// ExecParam.RecorId-ban megadott sz�l� �gyirat-hiearchi�j�nak lek�rdez�se lek�rdez�se
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    public Result GetSzuloHiearchy(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamGetSzuloHiearchy");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamGetSzuloHiearchy]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    /// <summary>
    /// A ParentIdArray-ban megadott f�sz�mokhoz szerelt f�sz�mok lek�r�se
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="ParentIdArray"></param>
    /// <returns></returns>
    public Result GetAllSzereltByParent(ExecParam ExecParam, string[] ParentIdArray)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamGetAllSzereltByParent");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamGetAllSzereltByParent]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;


            string ParentIdList = Contentum.eUtility.Utils.GetStringFromArray(ParentIdArray, ",");
            SqlComm.Parameters.Add(new SqlParameter("@ParentIdList", SqlDbType.NVarChar));
            SqlComm.Parameters["@ParentIdList"].Value = ParentIdList;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    /// <summary>
    /// ExecParam.RecordId-j� F�sz�m UGYHOL mez�j�nek m�dos�t�sa a Record.UGYHOL-ban megadott �rt�kre
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <param name="bMoveHierarchy">Ha true, a teljes szerel�si l�nc mozog, ha false, csak a konkr�t rekord.</param>
    /// <returns></returns>
    public Result FoszamAthelyezes(ExecParam ExecParam, MIG_Foszam Record, bool bMoveHierarchy)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamAthelyezes");

        Result _ret = new Result();

        try
        {
            Logger.Info("sp_MIG_FoszamAthelyezes h�v�sa");
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamAthelyezes]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@Id", SqlDbType.UniqueIdentifier).Value = SqlGuid.Parse(ExecParam.Record_Id);
            SqlComm.Parameters.Add("@Ugyhova", SqlDbType.NVarChar).Value = Record.UGYHOL;
            SqlComm.Parameters.Add("@MoveHierarchy", SqlDbType.Char).Value = (bMoveHierarchy ? "1" : "0");

            if (Record.Updated.SCONTRO == true && !String.IsNullOrEmpty(Record.SCONTRO))
            {
                SqlComm.Parameters.Add("@SkontroVege", SqlDbType.DateTime).Value = SqlDateTime.Parse(Record.SCONTRO);
            }

            if (Record.Updated.IRJ2000)
            {
                SqlComm.Parameters.Add("@IRJ2000", SqlDbType.NVarChar).Value = Record.IRJ2000;
            }

            if (Record.Updated.UI_IRJ)
            {
                SqlComm.Parameters.Add("@UI_IRJ", SqlDbType.NVarChar).Value = Record.UI_IRJ;
            }

            Utility.AddExecuteDefaultParameter(SqlComm, ExecParam, DateTime.Now);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            Logger.Error("sp_MIG_FoszamAthelyezes error: " + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    /// <summary>
    /// AzonositoArray-ban megadott azonos�t�j� F�sz�mok 
    /// utols� alsz�m�nak UGYHOL mez�j�nek m�dos�t�sa a Record.UGYHOL-ban megadott �rt�kre
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="AzonositoArray"></param>
    /// <param name="Record"></param>
    /// <param name="bMoveHierarchy">Ha true, a teljes szerel�si l�nc mozog, ha false, csak a konkr�t rekord.</param>
    /// <returns></returns>
    public Result FoszamAthelyezesTomeges(ExecParam ExecParam, string[] FoszamIdArray, MIG_Foszam Record, bool bMoveHierarchy)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamAthelyezesTomeges");

        Result _ret = new Result();

        try
        {
            Logger.Info("sp_MIG_FoszamAthelyezesTomeges h�v�sa");
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamAthelyezesTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            string FoszamIdList = Contentum.eUtility.Utils.GetStringFromArray(FoszamIdArray, ",");

            Logger.Debug("FoszamIdList: " + FoszamIdList);

            SqlComm.Parameters.Add("@FoszamIdList", SqlDbType.NVarChar).Value = FoszamIdList;
            SqlComm.Parameters.Add("@Ugyhova", SqlDbType.NVarChar).Value = Record.UGYHOL;
            SqlComm.Parameters.Add("@MoveHierarchy", SqlDbType.Char).Value = (bMoveHierarchy ? "1" : "0");

            if (Record.Updated.SCONTRO == true && !String.IsNullOrEmpty(Record.SCONTRO))
            {
                Logger.Debug("SCONTRO: " + Record.SCONTRO);
                SqlComm.Parameters.Add("@SkontroVege", SqlDbType.DateTime).Value = SqlDateTime.Parse(Record.SCONTRO);
            }

            if (Record.Updated.IrattarbolKikeroNev == true && !String.IsNullOrEmpty(Record.IrattarbolKikeroNev))
            {
                Logger.Debug("IrattarbolKikeroNev: " + Record.IrattarbolKikeroNev);
                SqlComm.Parameters.Add("@IrattarbolKikeroNev", SqlDbType.NVarChar).Value = Record.IrattarbolKikeroNev;
            }

            Utility.AddExecuteDefaultParameter(SqlComm, ExecParam, DateTime.Now);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            Logger.Error("sp_MIG_FoszamAthelyezesTomeges error: " + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result UpdateTomeges(ExecParam execParam, MIG_FoszamSearch _MIG_FoszamSearch, MIG_Foszam Record, bool UpdateHierarchy)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_MIG_FoszamUpdateTomeges");

        Result _ret = new Result();

        try
        {
            Logger.Info("sp_MIG_FoszamUpdateTomeges h�v�sa");
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamUpdateTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_FoszamSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (!String.IsNullOrEmpty(_MIG_FoszamSearch.WhereByManual))
            {
                //string normalizedStr = new Contentum.eQuery.FullTextSearch.SQLContainsCondition(_MIG_FoszamSearch.WhereByManual).Normalized;
                //normalizedStr = "contains(MIG_Foszam.*, '" + normalizedStr + "')";
                if (String.IsNullOrEmpty(query.Where))
                {
                    //query.Where += normalizedStr;
                    query.Where += _MIG_FoszamSearch.WhereByManual;
                }
                else
                {
                    //query.Where += " and " + normalizedStr;
                    query.Where += " and " + _MIG_FoszamSearch.WhereByManual;
                }
            }

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where"].Value = query.Where;

            //if (!Search.IsIdentical(_MIG_FoszamSearch.ExtendedMIG_SavSearch, new MIG_SavSearch(),
            //    _MIG_FoszamSearch.ExtendedMIG_SavSearch.WhereByManual, new MIG_SavSearch().WhereByManual))
            //{
            //    Query savSearchQuery = new Query();
            //    savSearchQuery.BuildFromBusinessDocument(_MIG_FoszamSearch.ExtendedMIG_SavSearch);

            //    if (!string.IsNullOrEmpty(_MIG_FoszamSearch.ExtendedMIG_SavSearch.WhereByManual))
            //    {
            //        savSearchQuery.Where += _MIG_FoszamSearch.ExtendedMIG_SavSearch.WhereByManual;
            //    }

            //    SqlComm.Parameters.Add("@ExtendedMIG_SavWhere", SqlDbType.NVarChar);
            //    SqlComm.Parameters["@ExtendedMIG_SavWhere"].Value = savSearchQuery.Where;
            //}

            /// Plusz Where felt�teleket k�l�n param�terben �tadva a t�rolt elj�r�snak:
            if (!Search.IsIdentical(_MIG_FoszamSearch.ExtendedMIG_AlszamSearch, new MIG_AlszamSearch(),
                _MIG_FoszamSearch.ExtendedMIG_AlszamSearch.WhereByManual, new MIG_AlszamSearch().WhereByManual))
            {
                Query query_MIG_Alszam = new Query();
                query_MIG_Alszam.BuildFromBusinessDocument(_MIG_FoszamSearch.ExtendedMIG_AlszamSearch);
                query_MIG_Alszam.Where += _MIG_FoszamSearch.ExtendedMIG_AlszamSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExtendedMIG_AlszamWhere", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@ExtendedMIG_AlszamWhere"].Value = query_MIG_Alszam.Where;
            }

            if (!Search.IsIdentical(_MIG_FoszamSearch.ExtendedMIG_EloadoSearch, new MIG_EloadoSearch(),
                _MIG_FoszamSearch.ExtendedMIG_EloadoSearch.WhereByManual, new MIG_EloadoSearch().WhereByManual))
            {
                Query query_MIG_Eloado = new Query();
                query_MIG_Eloado.BuildFromBusinessDocument(_MIG_FoszamSearch.ExtendedMIG_EloadoSearch);
                query_MIG_Eloado.Where += _MIG_FoszamSearch.ExtendedMIG_EloadoSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExtendedMIG_EloadoWhere", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@ExtendedMIG_EloadoWhere"].Value = query_MIG_Eloado.Where;
            }

            if (_MIG_FoszamSearch.ExtendedMIG_JegyzekTetelekSearch != null)
            {
                if (!Search.IsIdentical(_MIG_FoszamSearch.ExtendedMIG_JegyzekTetelekSearch, new MIG_JegyzekTetelekSearch(),
                _MIG_FoszamSearch.ExtendedMIG_JegyzekTetelekSearch.WhereByManual, new MIG_JegyzekTetelekSearch().WhereByManual))
                {
                    Query query_MIG_JegyzekTetelek = new Query();
                    query_MIG_JegyzekTetelek.BuildFromBusinessDocument(_MIG_FoszamSearch.ExtendedMIG_JegyzekTetelekSearch);
                    query_MIG_JegyzekTetelek.Where += _MIG_FoszamSearch.ExtendedMIG_JegyzekTetelekSearch.WhereByManual;

                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExtendedMIG_JegyzekTetelekWhere", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@ExtendedMIG_JegyzekTetelekWhere"].Value = query_MIG_JegyzekTetelek.Where;
                }
            }

            if (sp_MIG_FoszamUpdateParameters == null)
            {
                sp_MIG_FoszamUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_FoszamUpdateTomeges]");
            }

            Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_FoszamUpdateParameters);

            if (UpdateHierarchy)
            {
                SqlComm.Parameters.Add("@UpdateHierarchy", SqlDbType.Char).Value = Contentum.eUtility.Constants.Database.Yes;
            }

            Utility.AddExecuteDefaultParameter(SqlComm, execParam, DateTime.Now);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            Logger.Error("sp_MIG_FoszamUpdateTomeges error: " + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }
    /// <summary>
    /// GetAllWithExtensionForUgyiratpotlo
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_MIG_FoszamSearch"></param>
    /// <returns></returns>
    public Result GetAllWithExtensionForUgyiratpotlo(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_FoszamGetAllWithExtensionForUgyiratpotlo");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_FoszamSearch, true);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _MIG_FoszamSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_MIG_FoszamSearch.WhereByManual))
            {
                query.Where += " and " + _MIG_FoszamSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_FoszamGetAllWithExtensionForUgyiratpotlo]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_FoszamSearch.OrderBy, _MIG_FoszamSearch.TopRow);

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //        sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

}
