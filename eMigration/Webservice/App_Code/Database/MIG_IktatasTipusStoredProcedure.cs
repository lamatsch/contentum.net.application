using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.EnterpriseServices;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for MIG_IktatasTipusStoredProcedure
/// </summary>
public partial class MIG_IktatasTipusStoredProcedure
{
    //private SqlConnection Connection;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_MIG_IktatasTipusInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_MIG_IktatasTipusUpdateParameters = null;

    public MIG_IktatasTipusStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        /*try
        {
            Connection = Database.Connect(App);
        }
        catch (Exception e)
        {
            throw e;
        }*/
    }

    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_IktatasTipusGet");

        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_IktatasTipusGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            /*SqlDataReader dr = SqlComm.ExecuteReader();

            if (dr.HasRows)
            {
                MIG_IktatasTipus _MIG_IktatasTipus = new MIG_IktatasTipus();
                Utility.LoadBusinessDocumentFromSqlDataReader(_MIG_IktatasTipus, dr);
                _ret.Record = _MIG_IktatasTipus;
            }

            dr.Close();*/
            MIG_IktatasTipus _MIG_IktatasTipus = new MIG_IktatasTipus();
            Utility.LoadBusinessDocumentFromDataAdapter(_MIG_IktatasTipus, SqlComm);
            _ret.Record = _MIG_IktatasTipus;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, MIG_IktatasTipusSearch _MIG_IktatasTipusSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_IktatasTipusGetAll");

        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_IktatasTipusSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_IktatasTipusSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_IktatasTipusGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_IktatasTipusSearch.OrderBy, _MIG_IktatasTipusSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}
