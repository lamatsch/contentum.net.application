﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data.SqlClient;
using System.Data;
using Contentum.eUtility;

/// <summary>
/// Summary description for MIG_IrattariHelyekStoredProcedure
/// </summary>
public partial class MIG_IrattariHelyekStoredProcedure
{

    private DataContext dataContext;

    public MIG_IrattariHelyekStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
        //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
        //throw e;
        //}
    }

    //public Result GetAllRoots(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
    //{ 

    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariHelyekGetAllRoots");

    //    Result result = new Result();

    //    try
    //    {
    //        Query query = new Query();
    //        query.BuildFromBusinessDocument(_EREC_IrattariHelyekSearch);

    //        //az egyedi szuresi feltel hozzaadasa!!!
    //        query.Where += _EREC_IrattariHelyekSearch.WhereByManual;

    //        SqlCommand SqlComm = new SqlCommand("[sp_EREC_IrattariHelyekGetAllRoots]");
    //        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

    //        SqlComm.Connection = dataContext.Connection;
    //        SqlComm.Transaction = dataContext.Transaction;

    //        Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IrattariHelyekSearch.OrderBy, _EREC_IrattariHelyekSearch.TopRow);

    //        DataSet ds = new DataSet();
    //        SqlDataAdapter adapter = new SqlDataAdapter();
    //        adapter.SelectCommand = SqlComm;
    //        adapter.Fill(ds);

    //        result.Ds = ds;

    //    }
    //    catch (SqlException e)
    //    {
    //        result.ErrorCode = e.ErrorCode.ToString();
    //        result.ErrorMessage = e.Message;
    //    }

    //    log.SpEnd(ExecParam, result);
    //    return result;
    //}
    //public Result GetReadOnlyIds(ExecParam ExecParam)
    //{

    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariHelyekGetReadOnlyId");

    //    Result result = new Result();

    //    try
    //    {

    //        SqlCommand SqlComm = new SqlCommand("[sp_EREC_IrattariHelyekGetReadOnlyId]");
    //        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

    //        SqlComm.Connection = dataContext.Connection;
    //        SqlComm.Transaction = dataContext.Transaction;

    //        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
    //        SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

    //        DataSet ds = new DataSet();
    //        SqlDataAdapter adapter = new SqlDataAdapter();
    //        adapter.SelectCommand = SqlComm;
    //        adapter.Fill(ds);

    //        result.Ds = ds;

    //    }
    //    catch (SqlException e)
    //    {
    //        result.ErrorCode = e.ErrorCode.ToString();
    //        result.ErrorMessage = e.Message;
    //    }

    //    log.SpEnd(ExecParam, result);
    //    return result;
    //}
    //public Result GetAllLeafs(ExecParam ExecParam)
    //{
    //    Result result = new Result();
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_VIEW_IrattariHelyLevelekGetAll");
    //    try
    //    {

    //        SqlCommand SqlComm = new SqlCommand("[sp_VIEW_IrattariHelyLevelekGetAll]");
    //        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

    //        SqlComm.Connection = dataContext.Connection;
    //        SqlComm.Transaction = dataContext.Transaction;

    //        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
    //        SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

    //        DataSet ds = new DataSet();
    //        SqlDataAdapter adapter = new SqlDataAdapter();
    //        adapter.SelectCommand = SqlComm;
    //        adapter.Fill(ds);

    //        result.Ds = ds;

    //    }
    //    catch (SqlException e)
    //    {
    //        result.ErrorCode = e.ErrorCode.ToString();
    //        result.ErrorMessage = e.Message;
    //    }

    //    log.SpEnd(ExecParam, result);
    //    return result;
    //}

    //public Result IrattariHelyStrukturaGetByRoots(ExecParam ExecParam, string rootId)
    //{

    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariHelyStrukturaGetByRoots");

    //    Result result = new Result();

    //    try
    //    {


    //        SqlCommand SqlComm = new SqlCommand("[sp_EREC_IrattariHelyStrukturaGetByRoots]");
    //        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

    //        SqlComm.Connection = dataContext.Connection;
    //        SqlComm.Transaction = dataContext.Transaction;

    //        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RootId", System.Data.SqlDbType.UniqueIdentifier));
    //        SqlComm.Parameters["@RootId"].Value = System.Data.SqlTypes.SqlGuid.Parse(rootId);

    //        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
    //        SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

    //        DataSet ds = new DataSet();
    //        SqlDataAdapter adapter = new SqlDataAdapter();
    //        adapter.SelectCommand = SqlComm;
    //        adapter.Fill(ds);

    //        result.Ds = ds;

    //    }
    //    catch (SqlException e)
    //    {
    //        result.ErrorCode = e.ErrorCode.ToString();
    //        result.ErrorMessage = e.Message;
    //    }

    //    log.SpEnd(ExecParam, result);
    //    return result;
    //}
    public Result IrattarRendezes(ExecParam ExecParam, DateTime ExecutionTime, string UgyiratIds, string IrattarId, string Ertek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_IrattarRendezes");

        Result result = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_IrattarRendezes]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratIds", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@UgyiratIds"].Value = UgyiratIds;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IrattariHelyErtek", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@IrattariHelyErtek"].Value = String.IsNullOrEmpty(Ertek) ? System.Data.SqlTypes.SqlString.Null : Ertek;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IrattarId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IrattarId"].Value = String.IsNullOrEmpty(IrattarId) ? System.Data.SqlTypes.SqlGuid.Null : System.Data.SqlTypes.SqlGuid.Parse(IrattarId);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutionTime", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@ExecutionTime"].Value = ExecutionTime.ToString();

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            result.ErrorCode = e.ErrorCode.ToString();
            result.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, result);
        return result;
    }
    //public Result GetByVonalkod(ExecParam ExecParam, string Vonalkod)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_VIEW_IrattariHelyekGetByVonalkod");

    //    Result _ret = new Result();

    //    try
    //    {
    //        SqlCommand SqlComm = new SqlCommand("[sp_VIEW_IrattariHelyekGetByVonalkod]");
    //        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
    //        SqlComm.Connection = dataContext.Connection;
    //        SqlComm.Transaction = dataContext.Transaction;

    //        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Vonalkod", System.Data.SqlDbType.NVarChar));
    //        SqlComm.Parameters["@Vonalkod"].Value = Vonalkod;

    //        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
    //        SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

    //        EREC_IrattariHelyek _EREC_IrattariHelyek = new EREC_IrattariHelyek();
    //        LoadEREC_IRattariHelyekFromView(_EREC_IrattariHelyek, SqlComm);
    //        _ret.Record = _EREC_IrattariHelyek;

    //    }
    //    catch (SqlException e)
    //    {
    //        _ret.ErrorCode = e.ErrorCode.ToString();
    //        _ret.ErrorMessage = e.Message;
    //    }

    //    log.SpEnd(ExecParam, _ret);
    //    return _ret;

    //}

    //private static void LoadEREC_IRattariHelyekFromView(EREC_IrattariHelyek BusinessDocument, System.Data.SqlClient.SqlCommand sqlcommand)
    //{
    //    DataSet ds = new DataSet();
    //    SqlDataAdapter da = new SqlDataAdapter();
    //    da.SelectCommand = sqlcommand;
    //    da.Fill(ds);

    //    try
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            System.Reflection.PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
    //            foreach (System.Reflection.PropertyInfo P in Properties)
    //            {
    //                //dt.Rows[0][GetOrdinal(P.Name)];
    //                if (ds.Tables[0].Columns.Contains(P.Name) && !(ds.Tables[0].Rows[0].IsNull(P.Name)))
    //                {
    //                    P.SetValue(BusinessDocument, ds.Tables[0].Rows[0][P.Name].ToString(), null);
    //                }
    //            }
    //            System.Reflection.FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
    //            object BaseObject = BaseField.GetValue(BusinessDocument);

    //            System.Reflection.PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

    //            foreach (System.Reflection.PropertyInfo P in BaseProperties)
    //            {
    //                if (ds.Tables[0].Columns.Contains(P.Name) && !(ds.Tables[0].Rows[0].IsNull(P.Name)))
    //                {
    //                    P.SetValue(BaseObject, ds.Tables[0].Rows[0][P.Name].ToString(), null);
    //                }
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        ds.Dispose();
    //        da.Dispose();
    //    }
    //}
}