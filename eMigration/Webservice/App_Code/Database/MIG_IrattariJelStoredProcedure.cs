﻿using System;
using System.Collections.Generic;
using System.Web;
using Contentum.eBusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eUtility;
using System.Data;
using System.Data.SqlClient;
using Contentum.eQuery;
using Contentum.eMigration.BusinessDocuments;

/// <summary>
/// Summary description for MIG_IrattariJelStoredProcedure
/// </summary>
public partial class MIG_IrattariJelStoredProcedure
{
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_MIG_IrattariJelInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_MIG_IrattariJelUpdateParameters = null;

    public MIG_IrattariJelStoredProcedure(DataContext _dataContext)
	{
        this.dataContext = _dataContext;
	}

    public Result Insert(String Method, ExecParam ExecParam, MIG_IrattariJel Record, int RecordId)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now, RecordId);
    }

    public Result Insert(String Method, ExecParam ExecParam, MIG_IrattariJel Record, DateTime ExecutionTime, int RecordId)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
        if (Method == "") Method = "Insert";
        switch (Method)
        {
            case Constants.Insert:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_IrattariJelInsert");
                Record.Base.Updated.LetrehozasIdo = true;
                Record.Base.Updated.Letrehozo_id = true;
                Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_IrattariJelInsert]");
                if (sp_MIG_IrattariJelInsertParameters == null)
                {
                    sp_MIG_IrattariJelInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_IrattariJelInsert]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_IrattariJelInsertParameters);

                //Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.Int));
                SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;

                break;
            case Constants.Update:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_IrattariJelUpdate");
                Record.Base.Updated.ModositasIdo = true;
                Record.Base.Updated.Modosito_id = true;
                Record.Base.ModositasIdo = ExecutionTime.ToString();
                Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_IrattariJelUpdate]");
                if (sp_MIG_IrattariJelUpdateParameters == null)
                {
                    sp_MIG_IrattariJelUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_IrattariJelUpdate]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_IrattariJelUpdateParameters);

                //Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);
                Utility.AddExecuteDefaultParameter(SqlComm, ExecParam, ExecutionTime);
                if (SqlComm.Parameters.Contains("@id"))
                {
                    SqlComm.Parameters["@id"].Value = RecordId;
                }
                else
                {
                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.Int));
                    SqlComm.Parameters["@id"].Value = RecordId;
                }

                break;
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }      

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }


    public Result Get(ExecParam ExecParam, int RecordId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_IrattariJelGet");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_IrattariJelGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            //Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.Int));
            SqlComm.Parameters["@id"].Value = RecordId;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            MIG_IrattariJel _MIG_IrattariJel = new MIG_IrattariJel();
            Utility.LoadBusinessDocumentFromDataAdapter(_MIG_IrattariJel, SqlComm);
            _ret.Record = _MIG_IrattariJel;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, MIG_IrattariJelSearch _MIG_IrattariJelSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_IrattariJelGetAll");

        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_IrattariJelSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where = Search.ConcatWhereExpressions(query.Where, _MIG_IrattariJelSearch.WhereByManual);
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_IrattariJelGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_IrattariJelSearch.OrderBy, _MIG_IrattariJelSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}
