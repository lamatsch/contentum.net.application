using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.EnterpriseServices;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for MIG_JegyzekTetelekStoredProcedure
/// </summary>
public partial class MIG_JegyzekTetelekStoredProcedure
{
    //private SqlConnection Connection;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_MIG_JegyzekTetelekInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_MIG_JegyzekTetelekUpdateParameters = null;

    public MIG_JegyzekTetelekStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        /*try
        {
            Connection = Database.Connect(App);
        }
        catch (Exception e)
        {
            throw e;
        }*/
    }

    #region Generated

    public Result Insert(String Method, ExecParam ExecParam, MIG_JegyzekTetelek Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, MIG_JegyzekTetelek Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
        if (Method == "") Method = "Insert";
        switch (Method)
        {
            case Constants.Insert:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekTetelekInsert");
                Record.Base.Updated.LetrehozasIdo = true;
                Record.Base.Updated.Letrehozo_id = true;
                Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_JegyzekTetelekInsert]");
                if (sp_MIG_JegyzekTetelekInsertParameters == null)
                {
                    //sp_MIG_JegyzekTetelekInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_MIG_JegyzekTetelekInsert]");
                    //sp_MIG_JegyzekTetelekInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_MIG_JegyzekTetelekInsert]");
                    sp_MIG_JegyzekTetelekInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_JegyzekTetelekInsert]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_JegyzekTetelekInsertParameters);

                Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

                break;
            case Constants.Update:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekTetelekUpdate");
                Record.Base.Updated.ModositasIdo = true;
                Record.Base.Updated.Modosito_id = true;
                Record.Base.ModositasIdo = ExecutionTime.ToString();
                Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_JegyzekTetelekUpdate]");
                if (sp_MIG_JegyzekTetelekUpdateParameters == null)
                {
                    //sp_MIG_JegyzekTetelekUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_MIG_JegyzekTetelekUpdate]");
                    //sp_MIG_JegyzekTetelekUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_MIG_JegyzekTetelekUpdate]");
                    sp_MIG_JegyzekTetelekUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_JegyzekTetelekUpdate]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_JegyzekTetelekUpdateParameters);

                Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                break;
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekTetelekGet");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_JegyzekTetelekGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            MIG_JegyzekTetelek _MIG_JegyzekTetelek = new MIG_JegyzekTetelek();
            Utility.LoadBusinessDocumentFromDataAdapter(_MIG_JegyzekTetelek, SqlComm);
            _ret.Record = _MIG_JegyzekTetelek;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, MIG_JegyzekTetelekSearch _MIG_JegyzekTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekTetelekGetAll");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_JegyzekTetelekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_JegyzekTetelekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_JegyzekTetelekGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_JegyzekTetelekSearch.OrderBy, _MIG_JegyzekTetelekSearch.TopRow);

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekTetelekDelete");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        SqlCommand SqlComm = new SqlCommand("[sp_MIG_JegyzekTetelekDelete]");

        try
        {
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.ExecuteNonQuery();

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekTetelekInvalidate");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_JegyzekTetelekInvalidate]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    #endregion generated

    public Result GetAllWithExtension(ExecParam ExecParam, MIG_JegyzekTetelekSearch _MIG_JegyzekTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekTetelekGetAllWithExtension");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_JegyzekTetelekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_JegyzekTetelekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_JegyzekTetelekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_JegyzekTetelekSearch.OrderBy, _MIG_JegyzekTetelekSearch.TopRow);

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result InsertTomeges(ExecParam ExecParam, MIG_JegyzekTetelek Record, MIG_FoszamSearch _MIG_FoszamSearch, bool InsertFoszamHierarchy)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekTetelekInsertTomeges");

        Result _ret = new Result();

        DateTime ExecutionTime = DateTime.Now;

        Record.Base.Updated.LetrehozasIdo = true;
        Record.Base.Updated.Letrehozo_id = true;
        Record.Base.LetrehozasIdo = ExecutionTime.ToString();
        Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
        System.Data.SqlClient.SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_JegyzekTetelekInsertTomeges]");
        if (sp_MIG_JegyzekTetelekInsertParameters == null)
        {
            sp_MIG_JegyzekTetelekInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_JegyzekTetelekInsert]");
        }

        Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_JegyzekTetelekInsertParameters);

        //Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);


        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        Query query = new Query();
        query.BuildFromBusinessDocument(_MIG_FoszamSearch);

        //az egyedi szuresi feltel hozzaadasa!!!
        if (!String.IsNullOrEmpty(_MIG_FoszamSearch.WhereByManual))
        {
            //string normalizedStr = new Contentum.eQuery.FullTextSearch.SQLContainsCondition(_MIG_FoszamSearch.WhereByManual).Normalized;
            //normalizedStr = "contains(MIG_Foszam.*, '" + normalizedStr + "')";
            if (String.IsNullOrEmpty(query.Where))
            {
                //query.Where += normalizedStr;
                query.Where += _MIG_FoszamSearch.WhereByManual;
            }
            else
            {
                //query.Where += " and " + normalizedStr;
                query.Where += " and " + _MIG_FoszamSearch.WhereByManual;
            }
        }

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MIG_FoszamWhere", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@MIG_FoszamWhere"].Value = query.Where;

        /// Plusz Where feltételeket külön paraméterben átadva a tárolt eljárásnak:
        if (!Search.IsIdentical(_MIG_FoszamSearch.ExtendedMIG_AlszamSearch, new MIG_AlszamSearch(),
            _MIG_FoszamSearch.ExtendedMIG_AlszamSearch.WhereByManual, new MIG_AlszamSearch().WhereByManual))
        {
            Query query_MIG_Alszam = new Query();
            query_MIG_Alszam.BuildFromBusinessDocument(_MIG_FoszamSearch.ExtendedMIG_AlszamSearch);
            query_MIG_Alszam.Where += _MIG_FoszamSearch.ExtendedMIG_AlszamSearch.WhereByManual;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExtendedMIG_AlszamWhere", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@ExtendedMIG_AlszamWhere"].Value = query_MIG_Alszam.Where;
        }

        if (!Search.IsIdentical(_MIG_FoszamSearch.ExtendedMIG_EloadoSearch, new MIG_EloadoSearch(),
            _MIG_FoszamSearch.ExtendedMIG_EloadoSearch.WhereByManual, new MIG_EloadoSearch().WhereByManual))
        {
            Query query_MIG_Eloado = new Query();
            query_MIG_Eloado.BuildFromBusinessDocument(_MIG_FoszamSearch.ExtendedMIG_EloadoSearch);
            query_MIG_Eloado.Where += _MIG_FoszamSearch.ExtendedMIG_EloadoSearch.WhereByManual;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExtendedMIG_EloadoWhere", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@ExtendedMIG_EloadoWhere"].Value = query_MIG_Eloado.Where;
        }

        if (InsertFoszamHierarchy)
        {
            SqlComm.Parameters.Add("@InsertFoszamHierarchy", SqlDbType.Char).Value = Contentum.eUtility.Constants.Database.Yes;
        }

        try
        {
            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result UpdateTomeges(ExecParam ExecParam, MIG_JegyzekTetelek Record, MIG_JegyzekTetelekSearch _MIG_JegyzekTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekTetelekUpdateTomeges");

        Result _ret = new Result();
        DateTime ExecutionTime = DateTime.Now;


        Record.Base.Updated.ModositasIdo = true;
        Record.Base.Updated.Modosito_id = true;
        Record.Base.ModositasIdo = ExecutionTime.ToString();
        Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
        System.Data.SqlClient.SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_JegyzekTetelekUpdateTomeges]");
        if (sp_MIG_JegyzekTetelekUpdateParameters == null)
        {
            sp_MIG_JegyzekTetelekUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_JegyzekTetelekUpdate]");
        }

        Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_JegyzekTetelekUpdateParameters);

        Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        //az egyedi szuresi feltel hozzaadasa!!!
        Query query = new Query();
        query.BuildFromBusinessDocument(_MIG_JegyzekTetelekSearch);

        //az egyedi szuresi feltel hozzaadasa!!!
        query.Where += _MIG_JegyzekTetelekSearch.WhereByManual;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@Where"].Value = query.Where;

        try
        {
            SqlComm.ExecuteNonQuery();

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }
}
