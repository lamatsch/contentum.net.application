using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.EnterpriseServices;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for MIG_JegyzekekStoredProcedure
/// </summary>
public partial class MIG_JegyzekekStoredProcedure
{
    //private SqlConnection Connection;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_MIG_JegyzekekInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_MIG_JegyzekekUpdateParameters = null;

    public MIG_JegyzekekStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        /*try
        {
            Connection = Database.Connect(App);
        }
        catch (Exception e)
        {
            throw e;
        }*/
    }

    public Result Insert(String Method, ExecParam ExecParam, MIG_Jegyzekek Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, MIG_Jegyzekek Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
        if (Method == "") Method = "Insert";
        switch (Method)
        {
            case Constants.Insert:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekekInsert");
                Record.Base.Updated.LetrehozasIdo = true;
                Record.Base.Updated.Letrehozo_id = true;
                Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_JegyzekekInsert]");
                if (sp_MIG_JegyzekekInsertParameters == null)
                {
                    //sp_MIG_JegyzekekInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_MIG_JegyzekekInsert]");
                    //sp_MIG_JegyzekekInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_MIG_JegyzekekInsert]");
                    sp_MIG_JegyzekekInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_JegyzekekInsert]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_JegyzekekInsertParameters);

                Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

                break;
            case Constants.Update:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekekUpdate");
                Record.Base.Updated.ModositasIdo = true;
                Record.Base.Updated.Modosito_id = true;
                Record.Base.ModositasIdo = ExecutionTime.ToString();
                Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_MIG_JegyzekekUpdate]");
                if (sp_MIG_JegyzekekUpdateParameters == null)
                {
                    //sp_MIG_JegyzekekUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_MIG_JegyzekekUpdate]");
                    //sp_MIG_JegyzekekUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_MIG_JegyzekekUpdate]");
                    sp_MIG_JegyzekekUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_MIG_JegyzekekUpdate]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_MIG_JegyzekekUpdateParameters);

                Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                break;
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekekGet");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_JegyzekekGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            MIG_Jegyzekek _MIG_Jegyzekek = new MIG_Jegyzekek();
            Utility.LoadBusinessDocumentFromDataAdapter(_MIG_Jegyzekek, SqlComm);
            _ret.Record = _MIG_Jegyzekek;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, MIG_JegyzekekSearch _MIG_JegyzekekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekekGetAll");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_JegyzekekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_JegyzekekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_JegyzekekGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_JegyzekekSearch.OrderBy, _MIG_JegyzekekSearch.TopRow);

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekekDelete");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        SqlCommand SqlComm = new SqlCommand("[sp_MIG_JegyzekekDelete]");

        try
        {
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.ExecuteNonQuery();

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_JegyzekekInvalidate");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_JegyzekekInvalidate]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

}
