using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.EnterpriseServices;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for MIG_MigralasiHibaStoredProcedure
/// </summary>
public partial class MIG_MigralasiHibaStoredProcedure
{
    //private SqlConnection Connection;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_MIG_MigralasiHibaInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_MIG_MigralasiHibaUpdateParameters = null;

    public MIG_MigralasiHibaStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        /*try
        {
            Connection = Database.Connect(App);
        }
        catch (Exception e)
        {
            throw e;
        }*/
    }

    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_MigralasiHibaGet");

        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_MigralasiHibaGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            /*SqlDataReader dr = SqlComm.ExecuteReader();

            if (dr.HasRows)
            {
                MIG_MigralasiHiba _MIG_MigralasiHiba = new MIG_MigralasiHiba();
                Utility.LoadBusinessDocumentFromSqlDataReader(_MIG_MigralasiHiba, dr);
                _ret.Record = _MIG_MigralasiHiba;
            }

            dr.Close();*/
            MIG_MigralasiHiba _MIG_MigralasiHiba = new MIG_MigralasiHiba();
            Utility.LoadBusinessDocumentFromDataAdapter(_MIG_MigralasiHiba, SqlComm);
            _ret.Record = _MIG_MigralasiHiba;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, MIG_MigralasiHibaSearch _MIG_MigralasiHibaSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_MigralasiHibaGetAll");

        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_MigralasiHibaSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_MigralasiHibaSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_MigralasiHibaGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_MigralasiHibaSearch.OrderBy, _MIG_MigralasiHibaSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}
