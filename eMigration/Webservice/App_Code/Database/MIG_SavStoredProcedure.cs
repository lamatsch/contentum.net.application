using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.EnterpriseServices;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for MIG_SavStoredProcedure
/// </summary>
public partial class MIG_SavStoredProcedure
{
    //private SqlConnection Connection;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_MIG_SavInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_MIG_SavUpdateParameters = null;

    public MIG_SavStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        /*try
        {
            Connection = Database.Connect(App);
        }
        catch (Exception e)
        {
            throw e;
        }*/
    }

    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_SavGet");

        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_SavGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            /*SqlDataReader dr = SqlComm.ExecuteReader();

            if (dr.HasRows)
            {
                MIG_Sav _MIG_Sav = new MIG_Sav();
                Utility.LoadBusinessDocumentFromSqlDataReader(_MIG_Sav, dr);
                _ret.Record = _MIG_Sav;
            }

            dr.Close();*/
            MIG_Sav _MIG_Sav = new MIG_Sav();
            Utility.LoadBusinessDocumentFromDataAdapter(_MIG_Sav, SqlComm);
            _ret.Record = _MIG_Sav;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, MIG_SavSearch _MIG_SavSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_SavGetAll");

        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_SavSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_SavSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_MIG_SavGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_SavSearch.OrderBy, _MIG_SavSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}
