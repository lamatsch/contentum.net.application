using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.EnterpriseServices;
using Contentum.eMigration.Query;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for MIG_VarosStoredProcedure
/// </summary>
public partial class MIG_VarosStoredProcedure
{
    //private SqlConnection Connection;

    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_MIG_VarosInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_MIG_VarosUpdateParameters = null;

    public MIG_VarosStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        /*try
        {
            Connection = Database.Connect(App);
        }
        catch (Exception e)
        {
            throw e;
        }*/
    }

    public Result Get(ExecParam ExecParam)
    {
        Logger.Info("V�ros Get Start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_VarosGet");

        Result _ret = new Result();
        try
        {
            Logger.Info("sp_MIG_VarosGet h�v�sa");
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_VarosGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            /*SqlDataReader dr = SqlComm.ExecuteReader();

            if (dr.HasRows)
            {
                MIG_Varos _MIG_Varos = new MIG_Varos();
                Utility.LoadBusinessDocumentFromSqlDataReader(_MIG_Varos, dr);
                _ret.Record = _MIG_Varos;
            }

            dr.Close();*/
            MIG_Varos _MIG_Varos = new MIG_Varos();
            Utility.LoadBusinessDocumentFromDataAdapter(_MIG_Varos, SqlComm);
            _ret.Record = _MIG_Varos;

        }
        catch (SqlException e)
        {
            Logger.Error("V�ros Get error: " + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        Logger.Info("V�ros Get End");
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, MIG_VarosSearch _MIG_VarosSearch)
    {
        Logger.Info("V�ros GetAll start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_MIG_VarosGetAll");

        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_MIG_VarosSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _MIG_VarosSearch.WhereByManual;
            Logger.Info("sp_MIG_VarosGetAll h�v�sa");
            SqlCommand SqlComm = new SqlCommand("[sp_MIG_VarosGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _MIG_VarosSearch.OrderBy, _MIG_VarosSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            Logger.Error("V�ros GetAll Error: " + e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        Logger.Info("V�ros GetAll end");
        return _ret;

    }

}
