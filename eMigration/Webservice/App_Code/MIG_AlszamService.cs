using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eMigration.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class MIG_AlszamService : System.Web.Services.WebService
{
    private MIG_AlszamStoredProcedure sp = null;

    private DataContext dataContext;

    public MIG_AlszamService()
    {
        dataContext = new DataContext(this.Application);

        sp = new MIG_AlszamStoredProcedure(dataContext);
    }

    public MIG_AlszamService(DataContext _dataContext)
    {
         this.dataContext = _dataContext;
         sp = new MIG_AlszamStoredProcedure(dataContext);
    }  

    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Alszam))]
    public Result Get(ExecParam ExecParam)
    {
        //return sp.Get(ExecParam);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Get(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_AlszamSearch))]
    public Result GetAll(ExecParam ExecParam, MIG_AlszamSearch _MIG_AlszamSearch)
    {
        //return sp.GetAll(ExecParam, _MIG_AlszamSearch);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _MIG_AlszamSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region Depricated
    /// <summary>
    /// Speci�lis update, csak az UGYHOL mez� m�dos�that�
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    [WebMethod(Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Alszam))]
    public Result Update(ExecParam ExecParam, MIG_Alszam Record)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //result = sp.AlszamAthelyezes(ExecParam, Record);
            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }
    #endregion Depricated

    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_AlszamSearch))]
    public Result Test()
    {
        ExecParam ExecParam = new ExecParam();
        ExecParam.Record_Id = "CC11BA0E-A2A1-4D06-AA8A-0000205F01D8";
        return sp.Get(ExecParam);
    }

    ///// <summary>
    ///// ExecParam.RecorId skontr�ba tesz
    ///// </summary>
    ///// <param name="ExecParam"></param>
    ///// <returns></returns>
    //[WebMethod(Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak skontr�ba t�tele.")]
    //public Result SkontrobaHelyez(ExecParam ExecParam, string SkontroVege)
    //{
    //    Result result = new Result();

    //    if (String.IsNullOrEmpty(ExecParam.Record_Id))
    //    {
    //        //Nics megadva RecodId
    //        result.ErrorCode = result.ErrorMessage = "[50405]";
    //        return result;
    //    }

    //    if (String.IsNullOrEmpty(SkontroVege))
    //    {
    //        //Nics megadva SkontroVege
    //        result.ErrorCode = result.ErrorMessage = "[56008]";
    //        return result;
    //    }

    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

    //        MIG_Alszam alszam = new MIG_Alszam();
    //        alszam.Updated.SetValueAll(false);

    //        alszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Skontro.Value;
    //        alszam.Updated.UGYHOL = true;

    //        alszam.SCONTRO = SkontroVege;
    //        alszam.Updated.SCONTRO = true;

    //        ExecParam xpm = ExecParam.Clone();

    //        result = this.Update(xpm, alszam);

    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            throw new ResultException(result);
    //        }
    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    return result;
    //}

    ///// <summary>
    ///// SkontrobaHelyezTomeges(ExecParam[] ExecParams)
    ///// Az adott t�bl�ban t�bb rekord skontr�ba helyez�se. A m�dos�tand� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. 
    ///// </summary>
    ///// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    ///// <param name="SkontroVege">Skontr� v�ge bemen� param�ter. D�tumot kell tartalmaznia.</param>
    ///// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    //[WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord skontr�ba helyez�se. A m�dos�tand� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalmaz�s, h�v� HTML oldal).")]
    ////[AutoComplete(false)]
    //[System.Xml.Serialization.XmlInclude(typeof(MIG_Alszam))]
    //public Result SkontrobaHelyezTomeges(ExecParam[] ExecParams, string SkontroVege)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

    //    Result result = new Result();

    //    if (String.IsNullOrEmpty(SkontroVege))
    //    {
    //        //Nics megadva SkontroVege
    //        result.ErrorCode = result.ErrorMessage = "[56008]";
    //        return result;
    //    }

    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

    //        for (int i = 0; i < ExecParams.Length; i++)
    //        {
    //            Result result_skontro = SkontrobaHelyez(ExecParams[i], SkontroVege);
    //            if (!String.IsNullOrEmpty(result_skontro.ErrorCode))
    //            {
    //                throw new ResultException(result_skontro);
    //            }
    //        }
    //        ////Commit:
    //        //ContextUtil.SetComplete();

    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    log.WsEnd(ExecParams, result);
    //    return result;
    //}

    //public static string GetSkontroVegeDatum()
    //{
    //    DateTime now = DateTime.Now;
    //    DateTime end;
    //    end = now.AddMonths(1);
    //    if (now.Day >= 15)
    //    {
    //        end = end.AddDays(15 - end.Day);
    //    }
    //    else
    //    {
    //        end = end.AddDays(-end.Day);
    //    }

    //    return end.ToString();
    //}

    ///// <summary>
    ///// ExecParam.RecorId skontr�bol kiveszi
    ///// </summary>
    ///// <param name="ExecParam"></param>
    ///// <returns></returns>
    //[WebMethod(Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak skontr�b�l kiv�tele.")]
    //public Result SkontrobolKivesz(ExecParam ExecParam)
    //{
    //    Result result = new Result();

    //    if (String.IsNullOrEmpty(ExecParam.Record_Id))
    //    {
    //        //Nics megadva RecodId
    //        result.ErrorCode = result.ErrorMessage = "[50405]";
    //        return result;
    //    }

    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

    //        MIG_Alszam alszam = new MIG_Alszam();
    //        alszam.Updated.SetValueAll(false);

    //        alszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Osztalyon.Value;
    //        alszam.Updated.UGYHOL = true;

    //        ExecParam xpm = ExecParam.Clone();

    //        result = this.Update(xpm, alszam);

    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            throw new ResultException(result);
    //        }
    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    return result;
    //}

    ///// <summary>
    ///// SkontrobolKiveszTomeges(ExecParam[] ExecParams)
    ///// Az adott t�bl�ban t�bb rekord skontr�b�l kiv�tele. A m�dos�tand� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. 
    ///// </summary>
    ///// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    ///// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    //[WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord skontr�b�l kiv�tele. A m�dos�tand� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalmaz�s, h�v� HTML oldal).")]
    ////[AutoComplete(false)]
    //[System.Xml.Serialization.XmlInclude(typeof(MIG_Alszam))]
    //public Result SkontrobolKiveszTomeges(ExecParam[] ExecParams)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

    //    Result result = new Result();
    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

    //        for (int i = 0; i < ExecParams.Length; i++)
    //        {
    //            Result result_skontro = SkontrobolKivesz(ExecParams[i]);
    //            if (!String.IsNullOrEmpty(result_skontro.ErrorCode))
    //            {
    //                throw new ResultException(result_skontro);
    //            }
    //        }
    //        ////Commit:
    //        //ContextUtil.SetComplete();

    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    log.WsEnd(ExecParams, result);
    //    return result;
    //}

    ///// <summary>
    ///// ExecParam.RecorId iratt�rba t�tel
    ///// </summary>
    ///// <param name="ExecParam"></param>
    ///// <returns></returns>
    //[WebMethod(Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak iratt�rba t�tele.")]
    //public Result IrattarbaTesz(ExecParam ExecParam)
    //{
    //    Result result = new Result();

    //    if (String.IsNullOrEmpty(ExecParam.Record_Id))
    //    {
    //        //Nics megadva RecodId
    //        result.ErrorCode = result.ErrorMessage = "[50405]";
    //        return result;
    //    }

    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

    //        MIG_Alszam alszam = new MIG_Alszam();
    //        alszam.Updated.SetValueAll(false);

    //        alszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Irattarban.Value;
    //        alszam.Updated.UGYHOL = true;

    //        ExecParam xpm = ExecParam.Clone();

    //        result = this.Update(xpm, alszam);

    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            throw new ResultException(result);
    //        }
    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    return result;
    //}

    ///// <summary>
    ///// IrattarbaTeszTomeges(ExecParam[] ExecParams)
    ///// Az adott t�bl�ban t�bb rekord iratt�rba t�tele. A m�dos�tand� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. 
    ///// </summary>
    ///// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    ///// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    //[WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord iratt�rba t�tele. A m�dos�tand� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalmaz�s, h�v� HTML oldal).")]
    ////[AutoComplete(false)]
    //[System.Xml.Serialization.XmlInclude(typeof(MIG_Alszam))]
    //public Result IrattarbaTeszTomeges(ExecParam[] ExecParams)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

    //    Result result = new Result();
    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

    //        for (int i = 0; i < ExecParams.Length; i++)
    //        {
    //            Result result_irattarbatesz = IrattarbaTesz(ExecParams[i]);
    //            if (!String.IsNullOrEmpty(result_irattarbatesz.ErrorCode))
    //            {
    //                throw new ResultException(result_irattarbatesz);
    //            }
    //        }
    //        ////Commit:
    //        //ContextUtil.SetComplete();

    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    log.WsEnd(ExecParams, result);
    //    return result;
    //}

    ///// <summary>
    ///// ExecParam.RecorId iratt�rba t�tel
    ///// </summary>
    ///// <param name="ExecParam"></param>
    ///// <returns></returns>
    //[WebMethod(Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak kiad�sa oszt�lyra t�tele.")]
    //public Result KiadasOsztalyra(ExecParam ExecParam)
    //{
    //    Result result = new Result();

    //    if (String.IsNullOrEmpty(ExecParam.Record_Id))
    //    {
    //        //Nics megadva RecodId
    //        result.ErrorCode = result.ErrorMessage = "[50405]";
    //        return result;
    //    }

    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

    //        MIG_Alszam alszam = new MIG_Alszam();
    //        alszam.Updated.SetValueAll(false);

    //        alszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Osztalyon.Value;
    //        alszam.Updated.UGYHOL = true;

    //        ExecParam xpm = ExecParam.Clone();

    //        result = this.Update(xpm, alszam);

    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            throw new ResultException(result);
    //        }
    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    return result;
    //}

    ///// <summary>
    ///// KiadasOsztalyraTomeges(ExecParam[] ExecParams)
    ///// Az adott t�bl�ban t�bb rekord kiad�sa oszt�lyra. A m�dos�tand� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. 
    ///// </summary>
    ///// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    ///// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    //[WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord kiad�sa oszt�lyra. A m�dos�tand� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalmaz�s, h�v� HTML oldal).")]
    ////[AutoComplete(false)]
    //[System.Xml.Serialization.XmlInclude(typeof(MIG_Alszam))]
    //public Result KiadasOsztalyraTomeges(ExecParam[] ExecParams)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

    //    Result result = new Result();
    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

    //        for (int i = 0; i < ExecParams.Length; i++)
    //        {
    //            Result result_kiadasosztalyra = KiadasOsztalyra(ExecParams[i]);
    //            if (!String.IsNullOrEmpty(result_kiadasosztalyra.ErrorCode))
    //            {
    //                throw new ResultException(result_kiadasosztalyra);
    //            }
    //        }
    //        ////Commit:
    //        //ContextUtil.SetComplete();

    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    log.WsEnd(ExecParams, result);
    //    return result;
    //}

}