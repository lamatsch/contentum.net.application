using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Collections;
using System.Collections.Generic;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using Contentum.eDocument.Service;
using System.Text.RegularExpressions;
using System.IO;

/// <summary>
///    A(z) MIG_Dokumentum t�bl�hoz tartoz� Web szolg�ltat�sok.
/// </summary>

//[Transaction(Isolation = TransactionIsolationLevel.ReadCommitted)]
[WebService(Namespace = "Contentum.eMigration.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class MIG_DokumentumService : System.Web.Services.WebService
{
    private MIG_DokumentumStoredProcedure sp = null;

    private DataContext dataContext;

    public MIG_DokumentumService()
    {
        dataContext = new DataContext(this.Application);

        //sp = new MIG_DokumentumStoredProcedure(this.Application);
        sp = new MIG_DokumentumStoredProcedure(dataContext);
    }

    public MIG_DokumentumService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new MIG_DokumentumStoredProcedure(dataContext);
    }
    /// <summary>
    /// Get(ExecParam ExecParam)  
    /// A MIG_Dokumentum rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Dokumentum))]
    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Get(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// GetAll(ExecParam ExecParam, MIG_DokumentumSearch _MIG_DokumentumSearch)
    /// A(z) MIG_Dokumentum t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam.Record_Id nem haszn�lt, tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).</param>
    /// <param name="_MIG_DokumentumSearch">Bemen� param�ter, a keres�si felt�teleket tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_DokumentumSearch))]
    public Result GetAll(ExecParam ExecParam, MIG_DokumentumSearch _MIG_DokumentumSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _MIG_DokumentumSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Insert(ExecParam ExecParam, MIG_Dokumentum Record)
    /// Egy rekord felv�tele a MIG_Dokumentum t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Dokumentum))]
    public Result Insert(ExecParam ExecParam, MIG_Dokumentum Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Update(ExecParam ExecParam, MIG_Dokumentum Record)
    /// Az MIG_Dokumentum t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Dokumentum))]
    public Result Update(ExecParam ExecParam, MIG_Dokumentum Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az MIG_Dokumentum t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Dokumentum))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// MultiInvalidate(ExecParam ExecParams)
    /// A MIG_Dokumentum t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Dokumentum))]
    public Result MultiInvalidate(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_invalidate = Invalidate(ExecParams[i]);
                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                {
                    throw new ResultException(result_invalidate);
                }
            }
            ////Commit:
            //ContextUtil.SetComplete();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }
    /// <summary>
    /// Delete(ExecParam ExecParam)
    /// A(z) MIG_Dokumentum t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak fizikai t�rl�se.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>     
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak fizikai t�rl�se. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Dokumentum))]
    public Result Delete(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Delete(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod]
    public Result RefreshDocumentsFromUCM(ExecParam execParam, string alszamId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = false;
            isTransactionBeginHere = false;

            MIG_Alszam alszamObj = GetAlszam(execParam, alszamId);
            string foszamId = alszamObj.MIG_Foszam_Id;
            MIG_Foszam foszamObj = GetFoszam(execParam, foszamId);

            string iktatokonyv = foszamObj.EdokSav;
            string foszam = foszamObj.UI_NUM;
            string ev = foszamObj.UI_YEAR;

            string ugyszam = String.Format("{0}-{1}-{2}", iktatokonyv, foszam, ev);
            string alszam = alszamObj.ALNO;

            UCMDocumentService ucmDocService = Contentum.eUtility.eDocumentService.ServiceFactory.GetUCMDocumentService();
            Result ucmDocResult = ucmDocService.GetDocuments(execParam, ugyszam, alszam, true);

            if (ucmDocResult.IsError)
            {
                throw new ResultException(ucmDocResult);
            }

            KRT_DokumentumokList dokumentumok = ucmDocResult.Record as KRT_DokumentumokList;

            if (dokumentumok.Dokumentumok.Count > 0)
            {
                //jelenleg dokumentumok lek�r�se
                MIG_DokumentumSearch search = new MIG_DokumentumSearch();
                search.MIG_Alszam_Id.Value = alszamId;
                search.MIG_Alszam_Id.Operator = Query.Operators.equals;
                search.OrderBy = "Id";
                Result dokResult = this.GetAll(execParam, search);

                if (dokResult.IsError)
                {
                    throw new ResultException(dokResult);
                }

                List<string> fajlNevList = new List<string>();

                foreach (DataRow row in dokResult.Ds.Tables[0].Rows)
                {
                    string fajlNev = row["FajlNev"].ToString();
                    fajlNevList.Add(fajlNev);
                }

                foreach (KRT_Dokumentumok krt_dokumentum in dokumentumok.Dokumentumok)
                {
                    if (!fajlNevList.Contains(krt_dokumentum.FajlNev))
                    {
                        MIG_Dokumentum migDokumentum = new MIG_Dokumentum();
                        migDokumentum.MIG_Alszam_Id = alszamId;

                        migDokumentum.Id = Guid.NewGuid().ToString();
                        migDokumentum.FajlNev = krt_dokumentum.FajlNev;
                        migDokumentum.Tipus = krt_dokumentum.Tipus;
                        migDokumentum.Formatum = krt_dokumentum.Formatum;
                        migDokumentum.Leiras = krt_dokumentum.Leiras;
                        migDokumentum.External_Source = krt_dokumentum.External_Source;
                        migDokumentum.External_Link = krt_dokumentum.External_Link;
                        migDokumentum.BarCode = krt_dokumentum.BarCode;
                        migDokumentum.Allapot = krt_dokumentum.Allapot;
                        migDokumentum.ElektronikusAlairas = krt_dokumentum.ElektronikusAlairas;
                        migDokumentum.AlairasFelulvizsgalat = krt_dokumentum.AlairasFelulvizsgalat;

                        Result insertResult = this.Insert(execParam, migDokumentum);

                        if (insertResult.IsError)
                        {
                            throw new ResultException(insertResult);
                        }
                    }
                }
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception ex)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(ex);
            Logger.Error("MIG_DokumentumService.RefreshDocumentsFromUCM hiba:", ex);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    MIG_Alszam GetAlszam(ExecParam execParam, string alszamId)
    {
        MIG_AlszamService alszamService = new MIG_AlszamService();
        ExecParam alszamGet = execParam.Clone();
        alszamGet.Record_Id = alszamId;
        Result alszamResult = alszamService.Get(alszamGet);

        if (alszamResult.IsError)
        {
            throw new ResultException(alszamResult);
        }

        MIG_Alszam alszamObj = alszamResult.Record as MIG_Alszam;
        return alszamObj;
    }

    MIG_Foszam GetFoszam(ExecParam execParam, string foszamId)
    {
        MIG_FoszamService foszamService = new MIG_FoszamService();
        ExecParam foszamGet = execParam.Clone();
        foszamGet.Record_Id = foszamId;
        Result foszamResult = foszamService.Get(foszamGet);

        if (foszamResult.IsError)
        {
            throw new ResultException(foszamResult);
        }

        MIG_Foszam foszamObj = foszamResult.Record as MIG_Foszam;
        return foszamObj;
    }

    #region DeleteDokumentum

    public void DeleteDokumentum(ExecParam execParam, MIG_Dokumentum dokumentum)
    {
        //TODO: t�rl�st kidolgozni
        switch (dokumentum.External_Source)
        {
            case Contentum.eUtility.Constants.DocumentStoreType.FileShare:
                DeleteFileFromFileShare(dokumentum.FajlNev, dokumentum.External_Link);
                break;
            case Contentum.eUtility.Constants.DocumentStoreType.SharePoint:
                DeleteFileFromFileShare(dokumentum.FajlNev, dokumentum.External_Link);
                break;
            case Contentum.eUtility.Constants.DocumentStoreType.UCM:
                DeleteFileFromUCM(dokumentum.FajlNev, dokumentum.External_Link);
                break;
            default:
                break;
        }

        ExecParam execParamInvalidate = execParam.Clone();
        execParamInvalidate.Record_Id = dokumentum.Id;
        Result result = this.Invalidate(execParamInvalidate);

        result.CheckError();
    }

    void DeleteFileFromFileShare(string fileName, string filePath)
    {
        Logger.Debug(String.Format("DeleteFileFromFileShare: {0}, {1}", fileName, filePath));

        try
        {
            if (!File.Exists(filePath))
                throw new Exception(String.Format("A f�jl nem l�tezik: {0}!", filePath));

            File.Delete(filePath);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("F�jl t�rl�se sikertelen: {0}!", filePath), ex);
        }
    }

    void DeleteFileFromSharePoint(string fileName, string externalLink)
    {
        throw new NotImplementedException();
    }

    void DeleteFileFromUCM(string fileName, string externalLink)
    {
        throw new NotImplementedException();
    }
    #endregion
}