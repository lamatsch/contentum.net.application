﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Collections;
using System.Collections.Generic;

[WebService(Namespace = "Contentum.eMigration.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class MIG_FoszamService : System.Web.Services.WebService
{
    private MIG_FoszamStoredProcedure sp = null;

    private DataContext dataContext;

    public MIG_FoszamService()
    {
        dataContext = new DataContext(this.Application);

        sp = new MIG_FoszamStoredProcedure(dataContext);
    }

    public MIG_FoszamService(DataContext _dataContext)
    {
         this.dataContext = _dataContext;
         sp = new MIG_FoszamStoredProcedure(dataContext);
    }  

    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a tételazonosító alapján (ExecParam.Record_Id parameter). Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result Get(ExecParam ExecParam)
    {
        //return sp.Get(ExecParam);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Get(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Tömeges ITSZ modosítás")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result ModifyITSZTomeges(string[] UgyiratIds, string IRJ_2000, ExecParam ExecParam)
    {


        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            MIG_FoszamSearch schFoszam = new MIG_FoszamSearch();
            schFoszam.Id.Value = Search.GetSqlInnerString(UgyiratIds);
            schFoszam.Id.Operator = Query.Operators.inner;

            MIG_Foszam record = new MIG_Foszam();
            record.Base.Updated.SetValueAll(false);
            record.Updated.SetValueAll(false);
            record.IRJ2000 = IRJ_2000.ToString();
            record.Updated.IRJ2000 = true;
            record.UI_IRJ = IRJ_2000.ToString();
            record.Updated.UI_IRJ = true;

            result = this.UpdateTomeges(ExecParam.Clone(), schFoszam, record, false);
            if (result.IsError)
            {
                throw new ResultException(result);
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a tételazonosító alapján (ExecParam.Record_Id parameter). Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result GetAll(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        //return sp.GetAll(ExecParam, _MIG_FoszamSearch);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            if (_MIG_FoszamSearch.WhereByManual != null && _MIG_FoszamSearch.WhereByManual.StartsWith("DISTINCT:"))
            {
                var where = _MIG_FoszamSearch.WhereByManual.Replace("DISTINCT:", "");

                var p = where.IndexOf(" ");
                if (p >= 0)
                {
                    var column = where.Substring(0, p);
                    result = sp.GetAllDistinct(ExecParam, column, where);
                }
            }
            else
            {
                result = sp.GetAll(ExecParam, _MIG_FoszamSearch);
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a tételazonosító alapján (ExecParam.Record_Id parameter). Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result ForXml(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        //return sp.GetAll(ExecParam, _MIG_FoszamSearch);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ForXml(ExecParam, _MIG_FoszamSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a tételazonosító alapján (ExecParam.Record_Id parameter). Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result ForXmlNincsUgyintezo(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        //return sp.GetAll(ExecParam, _MIG_FoszamSearch);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ForXmlNincsUgyintezo(ExecParam, _MIG_FoszamSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a tételazonosító alapján (ExecParam.Record_Id parameter). Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result ForElszamoltatasiJk(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch, string UgyintezoId, string UgyintezoNev)
    {
        //return sp.GetAll(ExecParam, _MIG_FoszamSearch);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ForElszamoltatasiJk(ExecParam, _MIG_FoszamSearch, UgyintezoId, UgyintezoNev);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    //LZS - BUG_4747
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a tételazonosító alapján (ExecParam.Record_Id parameter). Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result ForElszamoltatasiJkSSRS(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch, string UgyintezoId, string UgyintezoNev)
    {
        //return sp.GetAll(ExecParam, _MIG_FoszamSearch);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ForElszamoltatasiJkSSRS(ExecParam, _MIG_FoszamSearch, UgyintezoId, UgyintezoNev);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Főszám (ügyirat) lista lekérése SSRS riporthoz, a szerelési láncokkal együtt.
    /// </summary>
    /// <param name="ExecParam">Felhasználói információk naplózáshoz, jogellenőrzéshez, stb.</param>
    /// <param name="_EREC_UgyUgyiratokSearch">Keresési feltételek</param>
    /// <returns></returns>
    [WebMethod(Description = "Főszám (ügyirat) lista lekérése SSRS riporthoz, a szerelési láncokkal együtt.")]
    public Result GetAllWithExtensionAndSzereltek(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtensionAndSzereltek(ExecParam, _MIG_FoszamSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Insert(ExecParam ExecParam, MIG_Foszam Record)
    /// Egy rekord felvétele a MIG_Foszam táblába. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result Insert(ExecParam ExecParam, MIG_Foszam Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Update(ExecParam ExecParam, MIG_Foszam Record)
    /// Az MIG_Foszam tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param>
    /// <param name="Record">A módosított adatokat a Record paraméter tartalmazza. </param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result Update(ExecParam ExecParam, MIG_Foszam Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Alszam))]
    public Result GetAllFoszamAlszamTerkepDataSets(ExecParam execParam, int nodeType, string nodeId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Logger.Info("Főszám-alszám térkép adatainak lekérése", execParam);

        // Paraméterek ellenőrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(nodeId)
            || (nodeType != Contentum.eMigration.BaseUtility.Constants.FoszamAlszamTerkepNodeTypes.Foszam
                && nodeType != Contentum.eMigration.BaseUtility.Constants.FoszamAlszamTerkepNodeTypes.Alszam))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52630);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            string foszamId = "";
            string alszamId = "";

            #region OrderBy
            const string foszamOrderBy = "";
            const string alszamOrderBy = "MIG_Alszam.ALNO ASC";

            #endregion

            #region Hierarchia lekérése

            MIG_AlszamService service_alszam = new MIG_AlszamService(this.dataContext);

            switch (nodeType)
            {
                case Contentum.eMigration.BaseUtility.Constants.FoszamAlszamTerkepNodeTypes.Alszam:
                    {
                        ExecParam execParam_alszam = execParam.Clone();
                        execParam_alszam.Record_Id = nodeId;

                        Result result_alszamGet = service_alszam.Get(execParam_alszam);
                        if (!String.IsNullOrEmpty(result_alszamGet.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_alszamGet);
                        }
                        MIG_Alszam mig_Alszam = (MIG_Alszam)result_alszamGet.Record;

                        alszamId = mig_Alszam.Id;
                        foszamId = mig_Alszam.MIG_Foszam_Id;

                        result.Record = mig_Alszam;

                        break;
                    }
                case Contentum.eMigration.BaseUtility.Constants.FoszamAlszamTerkepNodeTypes.Foszam:
                    {
                        foszamId = nodeId;

                        break;
                    }
            }

            #endregion

            result.Ds = new DataSet();

            #region Ügyirat és szerelt ügyiratok lekérése

            MIG_FoszamSearch search_foszamok = new MIG_FoszamSearch();

            // hozni kell azokat is, ahol ő a szülőügyirat (előiratok) (ebbe az ügyiratba szerelt ügyiratok)
            // illetve azt, amibe őt beleszerelték

            search_foszamok.Id.Value = foszamId;
            search_foszamok.Id.Operator = Contentum.eQuery.Query.Operators.equals;
            search_foszamok.Id.Group = "468";
            search_foszamok.Id.GroupOperator = Query.Operators.or;

            search_foszamok.Csatolva_Id.Value = foszamId;
            search_foszamok.Csatolva_Id.Operator = Query.Operators.equals;
            search_foszamok.Csatolva_Id.Group = "468";
            search_foszamok.Csatolva_Id.GroupOperator = Query.Operators.or;

            ExecParam execParam_foszamokGetAll = execParam.Clone();

            Result result_foszamGetAll = this.GetAll(execParam_foszamokGetAll, search_foszamok);
            if (!String.IsNullOrEmpty(result_foszamGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_foszamGetAll);
            }

            //result.Ds.Tables.Add(result_foszamGetAll.Ds.Tables[0]);
            DataTable table_foszam = result_foszamGetAll.Ds.Tables[0].Copy();
            table_foszam.TableName = Contentum.eUtility.Constants.TableNames.MIG_Foszam;

            result.Ds.Tables.Add(table_foszam);

            #region Utóirat lekérése (ha van meg van adva a fő ügyiratnál a Csatolva_Id)

            foreach (DataRow row in table_foszam.Rows)
            {
                string id = row["Id"].ToString();
                string Csatolva_Id = row["Csatolva_Id"].ToString();

                if (id == foszamId
                    && !String.IsNullOrEmpty(Csatolva_Id))
                {
                    // Szülő ügyirat lekérése:
                    MIG_FoszamSearch search_szuloFoszam = new MIG_FoszamSearch();

                    search_szuloFoszam.Id.Value = Csatolva_Id;
                    search_szuloFoszam.Id.Operator = Query.Operators.equals;

                    ExecParam execParam_szuloFoszamGetAll = execParam.Clone();

                    Result result_szuloFoszamGetAll = this.GetAll(execParam_szuloFoszamGetAll, search_szuloFoszam);
                    if (!String.IsNullOrEmpty(result_szuloFoszamGetAll.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_szuloFoszamGetAll);
                    }

                    // találat hozzáadása a többi ügyirathoz:
                    if (result_szuloFoszamGetAll.Ds.Tables[0].Rows.Count == 1)
                    {
                        // másik táblához hozzáadni a sort nem egyszerű:
                        // össze kell szedni az adatokat oszloponként
                        DataColumnCollection columns = result_szuloFoszamGetAll.Ds.Tables[0].Columns;
                        object[] rowVals = new object[columns.Count];
                        for (int i = 0; i < columns.Count; i++)
                        {
                            rowVals[i] = result_szuloFoszamGetAll.Ds.Tables[0].Rows[0][columns[i]];
                        }

                        // új sor hozzáadása a többi ügyirathoz:
                        table_foszam.Rows.Add(rowVals);
                    }

                    break;
                }
            }

            #endregion

            #endregion

            #region Alszámok (iratok) lekérése

            MIG_AlszamSearch search_alszamGetAll = new MIG_AlszamSearch();

            search_alszamGetAll.MIG_Foszam_Id.Value = foszamId;
            search_alszamGetAll.MIG_Foszam_Id.Operator = Query.Operators.equals;

            search_alszamGetAll.OrderBy = alszamOrderBy;

            ExecParam execParam_alszamGetAll = execParam.Clone();

            Result result_alszamGetAll = service_alszam.GetAll(execParam_alszamGetAll, search_alszamGetAll);
            if (!String.IsNullOrEmpty(result_alszamGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_alszamGetAll);
            }

            DataTable table_alszam = result_alszamGetAll.Ds.Tables[0].Copy();
            table_alszam.TableName = Contentum.eUtility.Constants.TableNames.MIG_Alszam;
            result.Ds.Tables.Add(table_alszam);

            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a szöveges azonosító alapján (Azonosito parameter). Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    public Result GetByAzonosito(ExecParam ExecParam, string Azonosito)
    {
        //return sp.GetAll(ExecParam, _MIG_FoszamSearch);
        Logger.DebugStart();
        Logger.Debug("Mig_FoszamService GetByAzonosito metodus start");
        Logger.Debug("Felhasznalo id: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("Azonosito: " + Azonosito);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            MIG_FoszamSearch search = new MIG_FoszamSearch();
            RegiAdatAzonosito.SetSerchObject(Azonosito, search);

            Result resGetAll = this.GetAll(ExecParam, search);

            if (!String.IsNullOrEmpty(resGetAll.ErrorCode))
            {
                throw new ResultException(resGetAll);
            }

            if (resGetAll.Ds == null || resGetAll.Ds.Tables[0] == null || resGetAll.Ds.Tables[0].Rows.Count == 0)
            {
                //rekord lekérése sikertelen
                //result.Record = null
                Logger.Error(String.Format("A következő azonositoju {0} rekord lekerese sikertelen.", Azonosito));
            }
            else
            {
                DataRow rowRecord = resGetAll.Ds.Tables[0].Rows[0];
                MIG_Foszam record = new MIG_Foszam();
                Utility.LoadBusinessDocumentFromDataRow(record, rowRecord);
                result.Record = record;
            }

            //result = sp.GetAll(ExecParam, _MIG_FoszamSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("Mig_FoszamService GetByAzonosito metodus hiba: {0},{1}",result.ErrorCode,result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("Mig_FoszamService GetByAzonosito metodus end");
        Logger.DebugEnd();
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott tábla szöveges azonosítóval kijelölt rekordjába az új ügyirat(amibe szereljük) id-jának és szöveges azonosítójának beírása. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    public Result FoszamSzereles(ExecParam ExecParam, string Azonosito, string edok_ugyirat_id, string edok_ugyirat_azonosito, bool isVisszavonas)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService FoszamSzereles metodus start.");
        Logger.Debug("Felhasznalo Id: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("Azonosito: " + Azonosito);
        Logger.Debug("Edok_ugyirat_id: " + edok_ugyirat_id);
        Logger.Debug("Edok_ugyirat_azonosito: " + edok_ugyirat_azonosito);
        Logger.Debug("IsVisszavonas: " + isVisszavonas.ToString());
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Debug("Bemeneti parameterek ellenorzese start");
            Arguments args = new Arguments();
            args.Add(new Argument("Azonosito", Azonosito, ArgumentTypes.String, false, null));
            if (!isVisszavonas)
            {
                args.Add(new Argument("Edok_ugyirat_id", edok_ugyirat_id, ArgumentTypes.Guid, false, null));
                args.Add(new Argument("Edok_ugyirat_azonosito", edok_ugyirat_azonosito, ArgumentTypes.String, false, null));
            }
            args.ValidateArguments();
            Logger.Debug("Bementei parameterek ellenorzese end");

            Logger.Debug("Azonosito felbontasa: ");
            string sav = RegiAdatAzonosito.GetSav(Azonosito);
            Logger.Debug("Sav: " + sav);
            string ev = RegiAdatAzonosito.GetEv(Azonosito);
            Logger.Debug("Ev: " + ev);
            string foszam = RegiAdatAzonosito.GetFoszam(Azonosito);
            Logger.Debug("Foszam: " + foszam);

            result = sp.FoszamSzereles(ExecParam, sav, ev, foszam, edok_ugyirat_id, edok_ugyirat_azonosito, isVisszavonas);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService FoszamSzereles metodus hiba: {0},{1}",result.ErrorCode,result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("MIG_FoszamService FoszamSzereles metodus end.");
        Logger.DebugEnd();
        return result;
    }
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott tábla szöveges azonosítóval kijelölt rekordjába az új ügyirat(amibe szereljük) id-jának és szöveges azonosítójának beírása. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    public Result FoszamSzerelesMig(ExecParam ExecParam, string szerelendo_id, string csatolva_id, bool isVisszavonas)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService FoszamSzerelesMig metodus start.");
        Logger.Debug("Felhasznalo Id: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("szerelendo_id: " + szerelendo_id);
        Logger.Debug("Edok_ugyirat_id: " + csatolva_id);        
        Logger.Debug("IsVisszavonas: " + isVisszavonas.ToString());
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Debug("Bementei parameterek ellenorzese start");
            Arguments args = new Arguments();
            args.Add(new Argument("szerelendo_id", szerelendo_id, ArgumentTypes.String, false, null));
            if (!isVisszavonas)
            {
                args.Add(new Argument("Csatolva_Id", csatolva_id, ArgumentTypes.Guid, false, null));                
            }
            args.ValidateArguments();
            Logger.Debug("Bementei parameterek ellenorzese end");

                        

            result = sp.FoszamSzerelesMig(ExecParam,szerelendo_id, csatolva_id, isVisszavonas);
            

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService FoszamSzerelesMig metodus hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("MIG_FoszamService FoszamSzerelesMig metodus end.");
        Logger.DebugEnd();
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a természetes azonositók alapján (Ev, Iktatokonyv, Foszam). A szerelt ügyirat szülő hiearchiáját is visszaadja.")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result GetFoszamWithSzulokByAzonosito(ExecParam execParam, MIG_FoszamSearch search)
    {
        Logger.DebugStart();
        Logger.Debug("FoszamService GetFoszamWithSzulokByAzonosito start");
        Logger.Debug("FelhasznaloId: " + execParam.Record_Id);
        Logger.Debug("Ev: " + search.UI_YEAR.Value);
        Logger.Debug("Sav: " + search.UI_SAV.Value);
        Logger.Debug("Foszam: " + search.UI_NUM.Value);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            Logger.Debug("Foszam kereses start");
            RegiAdatAzonosito.SetSearchObjectSav(search);
            Result res = this.GetAll(execParam, search); ;
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                Logger.Error(String.Format("FoszamService GetAll hiba: {0},{1}", res.ErrorCode, res.ErrorMessage));
                throw new ResultException(res);
            }

            Logger.Debug("Foszam kereses end");

            if (res.Ds.Tables[0].Rows.Count == 0)
            {
                Logger.Debug("A kereses eredmenytelen");
                throw new ResultException(ErrorCodes.UgyiratNemTalalhato);
            }

            if (res.Ds.Tables[0].Rows.Count > 1)
            {
                Logger.Error("A kerese eredmenye tobb record: " + res.Ds.Tables[0].Rows.Count);
                throw new ResultException(ErrorCodes.TalalatNemEgyertelmu);
            }

            DataRow foszamRow = res.Ds.Tables[0].Rows[0];
            string foszamId = foszamRow["Id"].ToString();
            Logger.Debug("Megtalalt foszam id-ja: " + foszamId);
            Logger.Debug("Megtalalt foszam iktatószáma: " + foszamRow["Foszam_Merge"]);
            string csatolvaId = foszamRow["Csatolva_Id"].ToString();
            Logger.Debug("Megtalalt foszam Csatolva_Id-ja: " + csatolvaId);
            if (IsFoszamSzerelve(foszamRow))
            {
                Logger.Debug("Foszam szerelt");
                Logger.Debug("SzuloHiearchia lekerdezese start");
                string ugyiratSzuloId = csatolvaId;
                ExecParam xpmSzulo = execParam.Clone();
                xpmSzulo.Record_Id = foszamId;
                Result resSzulo = this.GetSzuloHiearchy(xpmSzulo);
                if (!String.IsNullOrEmpty(resSzulo.ErrorCode))
                {
                    Logger.Error(String.Format("FoszamService GetSzuloHiearchy hiba: {0},{1}", resSzulo.ErrorCode, resSzulo.ErrorMessage));
                    throw new ResultException(resSzulo);
                }

                Logger.Debug("Eredmeny szama: " + resSzulo.Ds.Tables[0].Rows.Count);

                if (resSzulo.Ds.Tables[0].Rows.Count < 2)
                {
                    Logger.Error("A szulo hiearchia lekerdezese sikertelen");
                    throw new ResultException(ErrorCodes.SzuloHiearchiaLekerdezeseSikertelen);
                }

                Logger.Debug("SzuloHiearchia lekerdezese end");
                result = resSzulo;
            }
            else
            {
                result = res;
            }

            Logger.Debug("Eredmeny utolso soranak betoltese a Result.Record-ba start");
            DataRow nemSzereltFoszamRow = result.Ds.Tables[0].Rows[result.Ds.Tables[0].Rows.Count - 1];
            MIG_Foszam nemSzereltFoszam = new MIG_Foszam();
            Utility.LoadBusinessDocumentFromDataRow(nemSzereltFoszam, nemSzereltFoszamRow);

            if (String.IsNullOrEmpty(nemSzereltFoszam.Id))
            {
                Logger.Error("Nem szerelt foszam Id ures");
                throw new ResultException(ErrorCodes.UzletiObjekumFeltolteseSikertelen);
            }

            Logger.Debug("Nem szerelt foszam Id: " + nemSzereltFoszam.Id);

            result.Record = nemSzereltFoszam;

            Logger.Debug("Eredmeny utolso soranak betoltese a Result.Record-ba end");

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("FoszamService GetFoszamWithSzulokByAzonosito hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("FoszamService GetFoszamWithSzulokByAzonosito end");
        return result;
    }

    private bool IsFoszamSzerelve(DataRow foszamRow)
    {
        string csatolvaId = foszamRow["Csatolva_Id"].ToString();
        if (String.IsNullOrEmpty(csatolvaId))
        {
            return false;
        }

        return true;
    }

    ///<summary>
    ///ExecParam.RecordId-ban megadott főszámokhoz lekéri a szerelt főszámokat
    ///</summary>
    ///<param name="execParam"></param>
    ///<returns></returns>
    public Result GetAllSzereltByParent(ExecParam execParam)
    {
        if (String.IsNullOrEmpty(execParam.Record_Id))
            return new Result();

        return GetAllSzereltByParent(execParam, new string[1] { execParam.Record_Id });
    }

    /// <summary>
    /// A ParentIdArray-ban megadott főszámokhoz szerelt főszámok lekérése
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="ParentIdArray"></param>
    /// <returns></returns>
    public Result GetAllSzereltByParent(ExecParam execParam, string[] ParentIdList)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        result = sp.GetAllSzereltByParent(execParam, ParentIdList);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            throw new ResultException(result);
        }

        log.WsEnd(execParam, result);

        return result;
    }

    public static class ErrorCodes
    {
        public static int UgyiratNemTalalhato = 57000;
        public static int TalalatNemEgyertelmu = 57001;
        public static int NincsSzuloSzereltUgyiratnak = 57002;
        public static int SzuloHiearchiaLekerdezeseSikertelen = 57003;
        public static int UzletiObjekumFeltolteseSikertelen = 50700;
        public static int UgyiratNemSelejtezesreKijelolheto = 56400;
        public static int UgyiratNemSelejtezesreKijelolesVisszavonhato = 56401;
        public static int UgyiratNemSelejtezheto = 56402;
    }

    public Result GetSzuloHiearchy(ExecParam execParam)
    {
        Logger.Debug("FoszamService GetSzuloHiearchy start");
        Logger.Debug("RecordId: " + execParam.Record_Id);
        Logger.Debug("FelhasznaloId " + execParam.Felhasznalo_Id);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        Arguments args = new Arguments();
        args.Add(new Argument("RecordId", execParam.Record_Id, ArgumentTypes.Guid));
        args.ValidateArguments();

        result = sp.GetSzuloHiearchy(execParam);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Debug(String.Format("FoszamService GetSzuloHiearchy hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
            throw new ResultException(result);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("FoszamService GetSzuloHiearchy eredmeny szamossaga: " + result.Ds.Tables[0].Rows.Count);
        Logger.Debug("FoszamService GetSzuloHiearchy end");
        return result;
    }

    /// <summary>
    /// ExecParam.RecordId-jú Főszám UGYHOL mezőjének módosítása a Record.UGYHOL-ban megadott értékre
    /// </summary>
    /// <param name="execParam">ExecParam</param>
    /// <param name="Record">MIG_Foszam Record</param>
    /// <returns>Result</returns>
    [WebMethod(Description = "ExecParam.RecordId-jú Főszám UGYHOL mezőjének módosítása a Record.UGYHOL-ban megadott értékre")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result FoszamAthelyzes(ExecParam execParam, MIG_Foszam Record, bool moveHierarchy)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService FoszamAthelyzes start");
        Logger.Debug("FelhasznaloId: " + execParam.Felhasznalo_Id);
        Logger.Debug("RecordId: " + execParam.Record_Id);
        Logger.Debug("Ugyhol: " + Record.UGYHOL);
        Logger.Debug(String.Format("Hierarchia mozgatas: {0}", moveHierarchy));
        Logger.Debug(String.Format("Skontro: {0}", Record.SCONTRO ?? "NULL"));
        Logger.Debug(String.Format("Irattárba: {0}", Record.IRATTARBA ?? "NULL"));
        Logger.Debug(String.Format("IRJ2000: {0}",Record.IRJ2000 ?? "NULL"));
        Logger.Debug(String.Format("UI_IRJ: {0}", Record.UI_IRJ ?? "NULL"));
                                              
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.FoszamAthelyezes(execParam, Record, moveHierarchy);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error(String.Format("MIG_FoszamService FoszamAthelyzes hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
                throw new ResultException(result);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService FoszamAthelyzes hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("MIG_FoszamService FoszamAthelyzes end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// AzonositoArray-ban megadott azonosítójú Főszámok 
    /// utolsó alszámának UGYHOL mezőjének módosítása a Record.UGYHOL-ban megadott értékre
    /// </summary>
    /// <param name="execParam">ExecParam</param>
    /// <param name="FoszamIdArray">Azonosító tömb</param>
    /// <param name="Record">MIG_Foszam Record</param>
    /// <returns>Result</returns>
    [WebMethod(Description = "AzonositoArray-ban megadott azonosítójú Főszámok UGYHOL mezőjének módosítása a Record.UGYHOL-ban megadott értékre")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result FoszamAthelyzesTomeges(ExecParam execParam, string[] FoszamIdArray, MIG_Foszam Record, bool moveHierarchy)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService FoszamAthelyzesTomeges start");
        Logger.Debug("FelhasznaloId: " + execParam.Felhasznalo_Id);
        Logger.Debug(String.Format("FoszamIdArray Length: {0}", FoszamIdArray.Length));
        Logger.Debug("Ugyhol: " + Record.UGYHOL);
        Logger.Debug(String.Format("Hierarchia mozgatas: {0}", moveHierarchy));
        Logger.Debug(String.Format("Skontro: {0}", Record.SCONTRO ?? "NULL"));
        Logger.Debug(String.Format("Irattárba: {0}", Record.IRATTARBA ?? "NULL"));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();

        if (FoszamIdArray.Length == 0)
        {
            log.WsEnd(execParam, result);
            Logger.Debug("MIG_FoszamService FoszamAthelyzesTomeges end: FoszamIdArray hossza 0");
            Logger.DebugEnd();
            return result;
        }
                        

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.FoszamAthelyezesTomeges(execParam,FoszamIdArray, Record, moveHierarchy);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error(String.Format("MIG_FoszamService FoszamAthelyzesTomeges hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
                throw new ResultException(result);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService FoszamAthelyzesTomeges hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("MIG_FoszamService FoszamAthelyzesTomeges end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// ExecParam.RecorId skontróba tesz
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    [WebMethod(Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának skontróba tétele.")]
    public Result SkontrobaHelyez(ExecParam ExecParam, string SkontroVege)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService SkontrobaHelyez start");
        Logger.Debug("FelhasznaloId: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("RecordId: " + ExecParam.Record_Id);
        Logger.Debug(String.Format("Skontro: {0}", SkontroVege ?? "NULL"));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                //Nics megadva RecodId
                throw new ResultException(50405);
            }

            if (String.IsNullOrEmpty(SkontroVege))
            {
                //Nics megadva SkontroVege
                throw new ResultException(56008);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Skontro.Value;
            foszam.Updated.UGYHOL = true;

            foszam.SCONTRO = SkontroVege;
            foszam.Updated.SCONTRO = true;

            result = this.FoszamAthelyzes(ExecParam, foszam, true);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService SkontrobaHelyez hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("MIG_FoszamService SkontrobaHelyez end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// SkontrobaHelyezTomeges(ExecParam[] ExecParams)
    /// Az adott táblában több rekord skontróba helyezése. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <param name="SkontroVege">Skontró vége bemenő paraméter. Dátumot kell tartalmaznia.</param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott táblában több rekord skontróba helyezése. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result SkontrobaHelyezTomeges(ExecParam[] ExecParams, string SkontroVege)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService SkontrobaHelyezTomeges start");
        Logger.Debug(String.Format("Rekordok száma: {0}", ExecParams.Length));
        Logger.Debug("FelhasznaloId: " + (ExecParams[0] == null ? "missing" : ExecParams[0].Felhasznalo_Id));
        Logger.Debug(String.Format("Skontro: {0}", SkontroVege ?? "NULL"));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());
        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (String.IsNullOrEmpty(SkontroVege))
            {
                //Nics megadva SkontroVege
               throw new ResultException(56008);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string[] FoszamIdArray = new string[ExecParams.Length];

            for (int i = 0; i < ExecParams.Length; i++)
            {
                FoszamIdArray[i] = ExecParams[i].Record_Id;
            }

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Skontro.Value;
            foszam.Updated.UGYHOL = true;

            foszam.SCONTRO = SkontroVege;
            foszam.Updated.SCONTRO = true;

            result = this.FoszamAthelyzesTomeges(ExecParams[0], FoszamIdArray, foszam, true);

            ////Commit:
            //ContextUtil.SetComplete();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService SkontrobaHelyezTomeges hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        Logger.Debug("MIG_FoszamService SkontrobaHelyezTomeges end");
        Logger.DebugEnd();
        return result;
    }

    public static string GetSkontroVegeDatum()
    {
        DateTime now = DateTime.Now;
        DateTime end;
        end = now.AddMonths(1);
        if (now.Day >= 15)
        {
            end = end.AddDays(15 - end.Day);
        }
        else
        {
            end = end.AddDays(-end.Day);
        }

        return end.ToString();
    }

    /// <summary>
    /// ExecParam.RecorId skontróbol kiveszi
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    [WebMethod(Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának skontróból kivétele.")]
    public Result SkontrobolKivesz(ExecParam ExecParam)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService SkontrobolKivesz start");
        Logger.Debug("FelhasznaloId: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("RecordId: " + ExecParam.Record_Id);

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {

            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                //Nics megadva RecodId
                throw new ResultException(50405);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Osztalyon.Value;
            foszam.Updated.UGYHOL = true;

            result = this.FoszamAthelyzes(ExecParam, foszam, true);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService SkontrobolKivesz hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("MIG_FoszamService SkontrobolKivesz end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// SkontrobolKiveszTomeges(ExecParam[] ExecParams)
    /// Az adott táblában több rekord skontróból kivétele. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott táblában több rekord skontróból kivétele. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result SkontrobolKiveszTomeges(ExecParam[] ExecParams)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService SkontrobolKivesz start");
        Logger.Debug(String.Format("Rekordok száma: {0}", ExecParams.Length));;
        Logger.Debug("FelhasznaloId: " + (ExecParams[0] == null ? "missing" : ExecParams[0].Felhasznalo_Id));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string[] FoszamIdArray = new string[ExecParams.Length];

            for (int i = 0; i < ExecParams.Length; i++)
            {
                FoszamIdArray[i] = ExecParams[i].Record_Id;
            }

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Osztalyon.Value;
            foszam.Updated.UGYHOL = true;

            result = this.FoszamAthelyzesTomeges(ExecParams[0], FoszamIdArray, foszam, true);
            ////Commit:
            //ContextUtil.SetComplete();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService SkontrobolKiveszTomeges hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));

        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        Logger.Debug("MIG_FoszamService SkontrobolKiveszTomeges end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// ExecParam.RecorId irattárba tétel
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    [WebMethod(Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának irattárba tétele.")]
    public Result IrattarbaTesz(ExecParam ExecParam)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService IrattarbaTesz start");
        Logger.Debug("FelhasznaloId: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("RecordId: " + ExecParam.Record_Id);

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                //Nics megadva RecodId
                throw new ResultException(50405);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Irattarban.Value;
            foszam.Updated.UGYHOL = true;

            result = this.FoszamAthelyzes(ExecParam, foszam, true);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService IrattarbaTesz hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("MIG_FoszamService IrattarbaTesz end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// IrattarbaTeszTomeges(ExecParam[] ExecParams)
    /// Az adott táblában több rekord irattárba tétele. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott táblában több rekord irattárba tétele. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result IrattarbaTeszTomeges(ExecParam[] ExecParams)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService IrattarbaTeszTomeges start");
        Logger.Debug(String.Format("Rekordok száma: {0}", ExecParams.Length));;
        Logger.Debug("FelhasznaloId: " + (ExecParams[0] == null ? "missing" : ExecParams[0].Felhasznalo_Id));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string[] FoszamIdArray = new string[ExecParams.Length];

            for (int i = 0; i < ExecParams.Length; i++)
            {
                FoszamIdArray[i] = ExecParams[i].Record_Id;
            }

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Irattarban.Value;
            foszam.Updated.UGYHOL = true;

            result = this.FoszamAthelyzesTomeges(ExecParams[0], FoszamIdArray, foszam, true);
            ////Commit:
            //ContextUtil.SetComplete();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService IrattarbaTeszTomeges hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        Logger.Debug("MIG_FoszamService IrattarbaTeszTomeges end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// IrattarbaTeszTomeges(ExecParam[] ExecParams)
    /// Az adott táblában több rekord irattárba tétele. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott táblában több rekord irattárba tétele. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result IrattarbaKuldTomeges(ExecParam[] ExecParams)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService IrattarbaKuldTomeges start");
        Logger.Debug(String.Format("Rekordok száma: {0}", ExecParams.Length)); ;
        Logger.Debug("FelhasznaloId: " + (ExecParams[0] == null ? "missing" : ExecParams[0].Felhasznalo_Id));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string[] FoszamIdArray = new string[ExecParams.Length];

            for (int i = 0; i < ExecParams.Length; i++)
            {
                FoszamIdArray[i] = ExecParams[i].Record_Id;
            }

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.IrattarbaKuldott.Value;
            foszam.Updated.UGYHOL = true;

            result = this.FoszamAthelyzesTomeges(ExecParams[0], FoszamIdArray, foszam, true);
            ////Commit:
            //ContextUtil.SetComplete();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService IrattarbaKuldTomeges hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        Logger.Debug("MIG_FoszamService IrattarbaKuldTomeges end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// IrattarbaTeszTomeges(ExecParam[] ExecParams)
    /// Az adott táblában több rekord irattárba tétele. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott táblában több rekord irattárba tétele. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result IrattarbolKikerTomeges(ExecParam[] ExecParams)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService IrattarbolKikerTomeges start");
        Logger.Debug(String.Format("Rekordok száma: {0}", ExecParams.Length)); ;
        Logger.Debug("FelhasznaloId: " + (ExecParams[0] == null ? "missing" : ExecParams[0].Felhasznalo_Id));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        //irattárból kikérő nevének lekérése
        ExecParam xpmFelhasznalo = ExecParams[0].Clone();
        xpmFelhasznalo.Record_Id = ExecParams[0].Felhasznalo_Id;

        Contentum.eAdmin.Service.KRT_FelhasznalokService svcFelhasznalo = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        Result resFelhasznalo = svcFelhasznalo.Get(xpmFelhasznalo);

        if (resFelhasznalo.IsError)
        {
            return resFelhasznalo;
        }

        KRT_Felhasznalok felhasznalo = resFelhasznalo.Record as KRT_Felhasznalok;

        string irattarbolKikeroNev = felhasznalo.Nev;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string[] FoszamIdArray = new string[ExecParams.Length];

            for (int i = 0; i < ExecParams.Length; i++)
            {
                FoszamIdArray[i] = ExecParams[i].Record_Id;
            }

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.IrattarbolElkert.Value;
            foszam.Updated.UGYHOL = true;

            foszam.IrattarbolKikeroNev = irattarbolKikeroNev;
            foszam.Updated.IrattarbolKikeroNev = true;

            result = this.FoszamAthelyzesTomeges(ExecParams[0], FoszamIdArray, foszam, true);
            ////Commit:
            //ContextUtil.SetComplete();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService IrattarbolKikerTomeges hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        Logger.Debug("MIG_FoszamService IrattarbolKikerTomeges end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// ExecParam.RecorId irattárba tétel
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    [WebMethod(Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának kiadása osztályra tétele.")]
    public Result KiadasOsztalyra(ExecParam ExecParam)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService KiadasOsztalyra start");
        Logger.Debug("FelhasznaloId: " + ExecParam.Felhasznalo_Id);
        Logger.Debug("RecordId: " + ExecParam.Record_Id);

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                //Nics megadva RecodId
                throw new ResultException(50405);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Osztalyon.Value;
            foszam.Updated.UGYHOL = true;

            result = this.FoszamAthelyzes(ExecParam, foszam, true);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService KiadasOsztalyra hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("MIG_FoszamService KiadasOsztalyra end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// KiadasOsztalyraTomeges(ExecParam[] ExecParams)
    /// Az adott táblában több rekord kiadása osztályra. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott táblában több rekord kiadása osztályra. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result KiadasOsztalyraTomeges(ExecParam[] ExecParams)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService KiadasOsztalyraTomeges start");
        Logger.Debug(String.Format("Rekordok száma: {0}", ExecParams.Length));;
        Logger.Debug("FelhasznaloId: " + (ExecParams[0] == null ? "missing" : ExecParams[0].Felhasznalo_Id));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string[] FoszamIdArray = new string[ExecParams.Length];

            for (int i = 0; i < ExecParams.Length; i++)
            {
                FoszamIdArray[i] = ExecParams[i].Record_Id;
            }

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Osztalyon.Value;
            foszam.Updated.UGYHOL = true;

            result = this.FoszamAthelyzesTomeges(ExecParams[0], FoszamIdArray, foszam, true);
            ////Commit:
            //ContextUtil.SetComplete();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService KiadasOsztalyraTomeges hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        Logger.Debug("MIG_FoszamService KiadasOsztalyraTomeges end");
        Logger.DebugEnd();
        return result;
    }

    #region Selejtezes
    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result SelejtezesreKijeloles(ExecParam execParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService SelejtezesreKijeloles start");
        Logger.Debug("FelhasznaloId: " + (execParam == null ? "missing" : execParam.Felhasznalo_Id));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            #region Ellenorzes
            if(!String.IsNullOrEmpty(_MIG_FoszamSearch.Id.Value) && !String.IsNullOrEmpty(_MIG_FoszamSearch.Id.Operator))
            {
                Logger.Debug("Id alapu szures");

                result = this.GetAll(execParam.Clone(), _MIG_FoszamSearch);
                if(result.IsError)
                {
                    throw new ResultException(result);
                }

                Hashtable htStatusz = Contentum.eMigration.BaseUtility.Foszamok.GetAllapotByDataSet(result.Ds);

                foreach(DataRow row in result.Ds.Tables[0].Rows)
                {
                    string id = row["Id"].ToString();
                    Contentum.eMigration.BaseUtility.Foszamok.Statusz statusz =(Contentum.eMigration.BaseUtility.Foszamok.Statusz) htStatusz[id];
                    if(!Contentum.eMigration.BaseUtility.Foszamok.SelejtezesreKijelolheto(statusz))
                    {
                        throw new ResultException(ErrorCodes.UgyiratNemSelejtezesreKijelolheto);
                    }
                }

            }
            else
            {
                if (!Contentum.eMigration.BaseUtility.Foszamok.SelejtezesreKijelolheto(_MIG_FoszamSearch))
                {
                    throw new ResultException(ErrorCodes.UgyiratNemSelejtezesreKijelolheto);
                }
            }
            #endregion

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);
            foszam.Base.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eUtility.Constants.MIG_IratHelye.SelejtezesreVar;
            foszam.Updated.UGYHOL = true;

            result = this.UpdateTomeges(execParam, _MIG_FoszamSearch, foszam, true);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService SelejtezesreKijeloles hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("MIG_FoszamService SelejtezesreKijeloles end");
        Logger.DebugEnd();
        return result;
    }

    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result SelejtezesreKijelolesVisszavonasa(ExecParam execParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService SelejtezesreKijelolesVisszavonasa start");
        Logger.Debug("FelhasznaloId: " + (execParam == null ? "missing" : execParam.Felhasznalo_Id));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Ellenorzes
            if (!String.IsNullOrEmpty(_MIG_FoszamSearch.Id.Value) && !String.IsNullOrEmpty(_MIG_FoszamSearch.Id.Operator))
            {
                Logger.Debug("Id alapu szures");

                result = this.GetAll(execParam.Clone(), _MIG_FoszamSearch);
                if (result.IsError)
                {
                    throw new ResultException(result);
                }

                Hashtable htStatusz = Contentum.eMigration.BaseUtility.Foszamok.GetAllapotByDataSet(result.Ds);

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    string id = row["Id"].ToString();
                    Contentum.eMigration.BaseUtility.Foszamok.Statusz statusz = (Contentum.eMigration.BaseUtility.Foszamok.Statusz)htStatusz[id];
                    if (!Contentum.eMigration.BaseUtility.Foszamok.SelejtezesreKijelolesVisszavonhato(statusz))
                    {
                        throw new ResultException(ErrorCodes.UgyiratNemSelejtezesreKijelolesVisszavonhato);
                    }
                }

            }
            else
            {
                if (!Contentum.eMigration.BaseUtility.Foszamok.SelejtezesreKijelolesVisszavonhato(_MIG_FoszamSearch))
                {
                    throw new ResultException(ErrorCodes.UgyiratNemSelejtezesreKijelolesVisszavonhato);
                }
            }
            #endregion

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);
            foszam.Base.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eUtility.Constants.MIG_IratHelye.Irattarban;
            foszam.Updated.UGYHOL = true;

            result = this.UpdateTomeges(execParam, _MIG_FoszamSearch, foszam, true);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService SelejtezesreKijelolesVisszavonasa hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("MIG_FoszamService SelejtezesreKijelolesVisszavonasa end");
        Logger.DebugEnd();
        return result;
    }

    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result Selejtezes(ExecParam execParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService Selejtezes start");
        Logger.Debug("FelhasznaloId: " + (execParam == null ? "missing" : execParam.Felhasznalo_Id));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Ellenorzes
            if (!String.IsNullOrEmpty(_MIG_FoszamSearch.Id.Value) && !String.IsNullOrEmpty(_MIG_FoszamSearch.Id.Operator))
            {
                Logger.Debug("Id alapu szures");

                result = this.GetAll(execParam.Clone(), _MIG_FoszamSearch);
                if (result.IsError)
                {
                    throw new ResultException(result);
                }

                Hashtable htStatusz = Contentum.eMigration.BaseUtility.Foszamok.GetAllapotByDataSet(result.Ds);

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    string id = row["Id"].ToString();
                    Contentum.eMigration.BaseUtility.Foszamok.Statusz statusz = (Contentum.eMigration.BaseUtility.Foszamok.Statusz)htStatusz[id];
                    if (!Contentum.eMigration.BaseUtility.Foszamok.Selejtezheto(statusz))
                    {
                        throw new ResultException(ErrorCodes.UgyiratNemSelejtezheto);
                    }
                }

            }
            else
            {
                if (!Contentum.eMigration.BaseUtility.Foszamok.Selejtezheto(_MIG_FoszamSearch))
                {
                    throw new ResultException(ErrorCodes.UgyiratNemSelejtezheto);
                }
            }
            #endregion

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);
            foszam.Base.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eUtility.Constants.MIG_IratHelye.Selejtezett;
            foszam.Updated.UGYHOL = true;

            foszam.Selejtezes_Datuma = DateTime.Now.ToString();
            foszam.Updated.Selejtezes_Datuma = true;

            result = this.UpdateTomeges(execParam, _MIG_FoszamSearch, foszam, true);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService Selejtezes hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("MIG_FoszamService Selejtezes end");
        Logger.DebugEnd();
        return result;
    }
    #endregion

    #region Felülvizsgálat

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result GetAllSelejtezheto(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            List<string> notinnerAllapotok = new List<string>(new string[] {
                Contentum.eUtility.Constants.MIG_IratHelye.Selejtezett
                , Contentum.eUtility.Constants.MIG_IratHelye.Leveltar
                , Contentum.eUtility.Constants.MIG_IratHelye.EgyebSzervezetnekAtadott
                , Contentum.eUtility.Constants.MIG_IratHelye.Jegyzek
                , Contentum.eUtility.Constants.MIG_IratHelye.LezartJegyzek
                , Contentum.eUtility.Constants.MIG_IratHelye.Sztornozott
                });

            _MIG_FoszamSearch.Manual_UGYHOL.Value = Search.GetSqlInnerString(notinnerAllapotok.ToArray());
            _MIG_FoszamSearch.Manual_UGYHOL.Operator = Query.Operators.notinner;

            _MIG_FoszamSearch.FilterFelulvizsgalat = UI.JegyzekTipus.SelejtezesiJegyzek.Value;

            result = this.GetAll(ExecParam, _MIG_FoszamSearch);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result GetAllLeveltarbaAdhato(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            List<string> notinnerAllapotok = new List<string>(new string[] {
                Contentum.eUtility.Constants.MIG_IratHelye.Selejtezett
                , Contentum.eUtility.Constants.MIG_IratHelye.Leveltar
                , Contentum.eUtility.Constants.MIG_IratHelye.EgyebSzervezetnekAtadott
                , Contentum.eUtility.Constants.MIG_IratHelye.Jegyzek
                , Contentum.eUtility.Constants.MIG_IratHelye.LezartJegyzek
                , Contentum.eUtility.Constants.MIG_IratHelye.Sztornozott
                });

            _MIG_FoszamSearch.Manual_UGYHOL.Value = Search.GetSqlInnerString(notinnerAllapotok.ToArray());
            _MIG_FoszamSearch.Manual_UGYHOL.Operator = Query.Operators.notinner;

            _MIG_FoszamSearch.FilterFelulvizsgalat = UI.JegyzekTipus.LeveltariAtadasJegyzek.Value;
            _MIG_FoszamSearch.WhereByManual = @"(((CASE WHEN MIG_Foszam.UI_YEAR < 2000
                                                  THEN MIG_Foszam.UI_IRJ
                                                  ELSE
                                                  MIG_Foszam.IRJ2000 END) like '%0')
                                                OR
                                                ((CASE WHEN MIG_Foszam.UI_YEAR < 2000
                                                  THEN MIG_Foszam.UI_IRJ
                                                  ELSE
                                                  MIG_Foszam.IRJ2000 END) like '%NS'))";

            result = this.GetAll(ExecParam, _MIG_FoszamSearch);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result GetAllEgyebSzervezetnekAtadhato(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            // egyelőre nincs feltétel az egyéb szervezetnek átadhatóságra,
            // kivéve, hogy ne legyen mozgásban, de ezeken kívül minden ügyiratot hozunk,
            // ami megfelel a kapott keresési objektumnak

            List<string> notinnerAllapotok = new List<string>(new string[] {
                Contentum.eUtility.Constants.MIG_IratHelye.Selejtezett
                , Contentum.eUtility.Constants.MIG_IratHelye.Leveltar
                , Contentum.eUtility.Constants.MIG_IratHelye.EgyebSzervezetnekAtadott
                , Contentum.eUtility.Constants.MIG_IratHelye.Jegyzek
                , Contentum.eUtility.Constants.MIG_IratHelye.LezartJegyzek
                , Contentum.eUtility.Constants.MIG_IratHelye.Sztornozott
                });

            _MIG_FoszamSearch.Manual_UGYHOL.Value = Search.GetSqlInnerString(notinnerAllapotok.ToArray());
            _MIG_FoszamSearch.Manual_UGYHOL.Operator = Query.Operators.notinner;

            // egyelőre inkább letiltjuk a checkboxot a felületen
            //// ne legyen szerelt
            //_MIG_FoszamSearch.Csatolva_Id.Value = "";
            //_MIG_FoszamSearch.Csatolva_Id.Operator = Query.Operators.isnull;

            //_MIG_FoszamSearch.Edok_Utoirat_Id.Value = "";
            //_MIG_FoszamSearch.Edok_Utoirat_Id.Operator = Query.Operators.isnull;

            result = this.GetAll(ExecParam, _MIG_FoszamSearch);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result Meghosszabitas(ExecParam execParam, String[] foszam_Id_Array
    , String megorzesiIdo, String idoegyseg, DateTime felulvizsgDat, string megjegyzes)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (foszam_Id_Array == null || foszam_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52700);
            }
            else
            {
                Result result_update = new Result();
                MIG_Foszam foszam = new MIG_Foszam();

                for (int i = 0; i < foszam_Id_Array.Length; i++)
                {
                    String foszamId = foszam_Id_Array[i];

                    // főszám lekérése:
                    ExecParam execParam_Get = execParam.Clone();
                    execParam_Get.Record_Id = foszamId;

                    Result result_Get = this.Get(execParam_Get);
                    if (!String.IsNullOrEmpty(result_Get.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_Get);
                    }
                    else
                    {
                        if (result_Get.Record == null)
                        {
                            // hiba
                            throw new ResultException(52701);
                        }

                        foszam = (MIG_Foszam)result_Get.Record;

                        if (String.IsNullOrEmpty(foszam.MegorzesiIdo))
                            continue;

                        foszam.Updated.SetValueAll(false);
                        foszam.Base.Updated.SetValueAll(false);
                        foszam.Base.Updated.Ver = true;

                        foszam.Typed.MegorzesiIdo = AddMegorzesiIdo(Convert.ToInt32(megorzesiIdo), idoegyseg, foszam.Typed.MegorzesiIdo.Value);
                        foszam.Updated.MegorzesiIdo = true;

                        //foszam.UjOrzesiIdo = megorzesiIdo;
                        //foszam.Updated.UjOrzesiIdo = true;
                        //foszam.UjOrzesiIdoIdoegyseg = idoegyseg;
                        //foszam.Updated.UjOrzesiIdoIdoegyseg = true;
                        //foszam.FelulvizsgalatDat = felulvizsgDat.ToString();
                        //foszam.Updated.FelulvizsgalatDat = true;
                        //foszam.FelhCsoport_Id_Felulvizsgalo = execParam.Felhasznalo_Id;
                        //foszam.Updated.FelhCsoport_Id_Felulvizsgalo = true;

                        result_update = this.Update(execParam_Get, foszam);

                        // ha hiba volt az egyiknél, nem megyünk tovább:
                        if (!String.IsNullOrEmpty(result_update.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_update);
                        }

                        //kezelési feljegyzés létrehozása, ha van megjegyzés
                        //var erec_HataridosFeladatok = CreateTaskNote(megjegyzes);
                        //if (erec_HataridosFeladatok != null)
                        //{
                        //    EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
                        //    ExecParam execParam_kezFeljInsert = execParam.Clone();

                        //    // Ügyirathoz kötés
                        //    erec_HataridosFeladatok.Obj_Id = foszam.Id;
                        //    erec_HataridosFeladatok.Updated.Obj_Id = true;

                        //    erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                        //    erec_HataridosFeladatok.Updated.Obj_type = true;

                        //    if (!String.IsNullOrEmpty(foszam.Azonosito))
                        //    {
                        //        erec_HataridosFeladatok.Azonosito = foszam.Azonosito;
                        //        erec_HataridosFeladatok.Updated.Azonosito = true;
                        //    }

                        //    Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
                        //        execParam_kezFeljInsert, erec_HataridosFeladatok);

                        //    if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
                        //    {
                        //        throw new ResultException(result_kezFeljInsert);
                        //    }
                        //}
                    }
                }

                result = result_update;
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    public static DateTime AddMegorzesiIdo(int megorzesiIdo, string idoegyseg, DateTime megorzesiIdoVege)
    {
        switch (idoegyseg)
        {
            case KodTarak.IDOEGYSEG.Nap:
                megorzesiIdoVege = megorzesiIdoVege.AddDays(Convert.ToInt32(megorzesiIdo));
                break;
            case KodTarak.IDOEGYSEG.Het:
                megorzesiIdoVege = megorzesiIdoVege.AddDays(Convert.ToInt32(megorzesiIdo) * 7);
                break;
            case KodTarak.IDOEGYSEG.Honap:
                megorzesiIdoVege = megorzesiIdoVege.AddMonths(Convert.ToInt32(megorzesiIdo));
                break;
            case KodTarak.IDOEGYSEG.Ev:
                megorzesiIdoVege = megorzesiIdoVege.AddYears(Convert.ToInt32(megorzesiIdo));
                break;
        }

        return megorzesiIdoVege;
    }

    #endregion Felülvizsgálat

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result UpdateTomeges(ExecParam execParam, MIG_FoszamSearch _MIG_FoszamSearch, MIG_Foszam Record, bool UpdateHierarchy)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.UpdateTomeges(execParam, _MIG_FoszamSearch, Record, UpdateHierarchy);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// LomtarbaHelyezesTomeges(ExecParam[] ExecParams)
    /// Az adott táblában több rekord lomtárba helyezése. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(Description = " Az adott táblában több rekord lomtárba helyezése. A módosítandó tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Foszam))]
    public Result LomtarbaHelyezesTomeges(ExecParam[] ExecParams)
    {
        Logger.DebugStart();
        Logger.Debug("MIG_FoszamService LomtarbaHelyezesTomeges start");
        Logger.Debug(String.Format("Rekordok száma: {0}", ExecParams.Length)); ;
        Logger.Debug("FelhasznaloId: " + ((ExecParams != null && ExecParams.Length >0 && ExecParams[0] != null) ? ExecParams[0].Felhasznalo_Id : "missing" ));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string[] FoszamIdArray = new string[ExecParams.Length];

            for (int i = 0; i < ExecParams.Length; i++)
            {
                FoszamIdArray[i] = ExecParams[i].Record_Id;
            }

            MIG_Foszam foszam = new MIG_Foszam();
            foszam.Updated.SetValueAll(false);

            foszam.UGYHOL = Contentum.eMigration.BaseUtility.IratHelye.Lomtar.Value;
            foszam.Updated.UGYHOL = true;

            result = this.FoszamAthelyzesTomeges(ExecParams[0], FoszamIdArray, foszam, true);
            ////Commit:
            //ContextUtil.SetComplete();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("MIG_FoszamService LomtarbaHelyezesTomeges hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        Logger.Debug("MIG_FoszamService LomtarbaHelyezesTomeges end");
        Logger.DebugEnd();
        return result;
    }
    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a tételazonosító alapján (ExecParam.Record_Id parameter). Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result Test()
    {
        ExecParam ExecParam = new ExecParam();
        ExecParam.Record_Id = "CC11BA0E-A2A1-4D06-AA8A-0000205F01D8";
        return sp.Get(ExecParam);
    }

    #region GetAllWithExtensionForUgyiratpotlo
    [WebMethod(Description = "Főszám (ügyirat) lista lekérése ügyiratlap pótláshoz.")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_FoszamSearch))]
    public Result GetAllWithExtensionForUgyiratpotlo(ExecParam ExecParam, MIG_FoszamSearch _MIG_FoszamSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtensionForUgyiratpotlo(ExecParam, _MIG_FoszamSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion
}