﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using System.Web.Services;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;

/// <summary>
/// Summary description for MIG_IrattariHelyekService
/// </summary>
/// 
[WebService(Namespace = "Contentum.eMigration.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyek))]
public partial class MIG_IrattariHelyekService : System.Web.Services.WebService
{
    private MIG_IrattariHelyekStoredProcedure sp = null;

    private DataContext dataContext;

    public MIG_IrattariHelyekService()
    {
        dataContext = new DataContext(this.Application);

        //sp = new EREC_IrattariHelyekStoredProcedure(this.Application);
        sp = new MIG_IrattariHelyekStoredProcedure(dataContext);
    }

    public MIG_IrattariHelyekService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new MIG_IrattariHelyekStoredProcedure(dataContext);
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyek))]
    public Result IrattarRendezes(ExecParam ExecParam, string UgyiratIds, string IrattarId, string Ertek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.IrattarRendezes(ExecParam, DateTime.Now, UgyiratIds, IrattarId, Ertek);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}