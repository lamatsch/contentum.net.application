using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Collections;
using System.Collections.Generic;

[WebService(Namespace = "Contentum.eMigration.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class MIG_JegyzekTetelekService : System.Web.Services.WebService
{
    private MIG_JegyzekTetelekStoredProcedure sp = null;

    private DataContext dataContext;

    public MIG_JegyzekTetelekService()
    {
        dataContext = new DataContext(this.Application);

        sp = new MIG_JegyzekTetelekStoredProcedure(dataContext);
    }

    public MIG_JegyzekTetelekService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new MIG_JegyzekTetelekStoredProcedure(dataContext);
    }

    #region Generated
    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_JegyzekTetelek))]
    public Result Get(ExecParam ExecParam)
    {
        //return sp.Get(ExecParam);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Get(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_JegyzekTetelekSearch))]
    public Result GetAll(ExecParam ExecParam, MIG_JegyzekTetelekSearch _MIG_JegyzekTetelekSearch)
    {
        //return sp.GetAll(ExecParam, _MIG_JegyzekTetelekSearch);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _MIG_JegyzekTetelekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Insert(ExecParam ExecParam, MIG_JegyzekTetelek Record)
    /// Egy rekord felv�tele a MIG_JegyzekTetelek t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_JegyzekTetelek))]
    public Result Insert(ExecParam ExecParam, MIG_JegyzekTetelek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Update(ExecParam ExecParam, MIG_JegyzekTetelek Record)
    /// Az MIG_JegyzekTetelek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_JegyzekTetelek))]
    public Result Update(ExecParam ExecParam, MIG_JegyzekTetelek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az MIG_JegyzekTetelek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_JegyzekTetelek))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// MultiInvalidate(ExecParam ExecParams)
    /// A MIG_JegyzekTetelek t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_JegyzekTetelek))]
    public Result MultiInvalidate(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_invalidate = Invalidate(ExecParams[i]);
                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                {
                    throw new ResultException(result_invalidate);
                }
            }
            ////Commit:
            //ContextUtil.SetComplete();


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }
    /// <summary>
    /// Delete(ExecParam ExecParam)
    /// A(z) MIG_JegyzekTetelek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak fizikai t�rl�se.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>     
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak fizikai t�rl�se. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_JegyzekTetelek))]
    public Result Delete(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Delete(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion Generated

    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_JegyzekTetelekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, MIG_JegyzekTetelekSearch _MIG_JegyzekTetelekSearch)
    {
        //return sp.GetAll(ExecParam, _MIG_JegyzekTetelekSearch);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _MIG_JegyzekTetelekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    public Result InsertTomeges(ExecParam ExecParam, MIG_JegyzekTetelek Record, MIG_FoszamSearch _MIG_FoszamSearch, bool InsertFoszamHierarchy)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.InsertTomeges(ExecParam, Record, _MIG_FoszamSearch, InsertFoszamHierarchy);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    public Result UpdateTomeges(ExecParam ExecParam, MIG_JegyzekTetelek Record, MIG_JegyzekTetelekSearch _MIG_JegyzekTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.UpdateTomeges(ExecParam, Record, _MIG_JegyzekTetelekSearch);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region fel�lvizsg�lat

    [WebMethod(Description = "F�sz�m (�gyirat) �s szerelt t�teleinek jegyz�kre helyez�se")]
    public Result JegyzekreHelyezes(ExecParam execParam, MIG_FoszamSearch _MIG_FoszamSearch, String jegyzekId, string megjegyzes)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            List<string> notinnerAllapotok = new List<string>(new string[] {
                Contentum.eUtility.Constants.MIG_IratHelye.Selejtezett
                , Contentum.eUtility.Constants.MIG_IratHelye.Leveltar
                , Contentum.eUtility.Constants.MIG_IratHelye.Jegyzek
                , Contentum.eUtility.Constants.MIG_IratHelye.LezartJegyzek
                , Contentum.eUtility.Constants.MIG_IratHelye.EgyebSzervezetnekAtadott
                , Contentum.eUtility.Constants.MIG_IratHelye.Sztornozott
                });

            _MIG_FoszamSearch.Manual_UGYHOL.Value = Search.GetSqlInnerString(notinnerAllapotok.ToArray());
            _MIG_FoszamSearch.Manual_UGYHOL.Operator = Query.Operators.notinner;

            // ne legyen szerelt
            _MIG_FoszamSearch.Csatolva_Id.IsNull();
            _MIG_FoszamSearch.Edok_Utoirat_Id.IsNull();

            //jegyz�kt�telek insert�l�sa
            MIG_JegyzekTetelek jegyzekTetel = new MIG_JegyzekTetelek();
            jegyzekTetel.Updated.SetValueAll(false);
            jegyzekTetel.Base.Updated.SetValueAll(false);
            jegyzekTetel.Jegyzek_Id = jegyzekId;
            jegyzekTetel.Updated.Jegyzek_Id = true;
            jegyzekTetel.Megjegyzes = megjegyzes;
            jegyzekTetel.Updated.Megjegyzes = true;

            Result result_insert = this.InsertTomeges(execParam, jegyzekTetel, _MIG_FoszamSearch, true);

            if (result_insert.IsError)
            {
                throw new ResultException(result_insert);
            }

            //f�sz�m update
            MIG_FoszamService service_foszamok = new MIG_FoszamService(this.dataContext);

            MIG_Foszam mig_Foszam = new MIG_Foszam();
            mig_Foszam.Updated.SetValueAll(false);
            mig_Foszam.Base.Updated.SetValueAll(false);
            mig_Foszam.Base.Updated.Ver = true;
            mig_Foszam.UGYHOL = Contentum.eUtility.Constants.MIG_IratHelye.Jegyzek;
            mig_Foszam.Updated.UGYHOL = true;

            Result result_update = service_foszamok.UpdateTomeges(execParam, _MIG_FoszamSearch, mig_Foszam, true);

            if (result_update.IsError)
            {
                // hiba:
                throw new ResultException(result_update);
            }


            #region Esem�nynapl�z�s
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            //    string ids = Search.GetSqlInnerString(jegyzektetelIds.ToArray());

            //    Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ids, "JegyzekTetelNew");
            //    if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
            //        Logger.Debug("[ERROR]MIG_JegyzekTetelek::JegyzekTetelNew: ", execParam, eventLogResult);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    private string[] AddSzereltFoszamokToArray(ExecParam execParam, string[] mig_Foszam_Id_Array)
    {
        Result res = new Result();
        MIG_FoszamService svc = new MIG_FoszamService(this.dataContext);
        ExecParam xpm = execParam.Clone();
        List<string> szereltIds = new List<string>();
        foreach (string id in mig_Foszam_Id_Array)
        {
            xpm.Record_Id = id;
            res = svc.GetAllSzereltByParent(xpm);

            if (res.IsError)
            {
                throw new ResultException(res);
            }

            foreach (DataRow rowSzereltFoszam in res.Ds.Tables[0].Rows)
            {
                string foszamId = rowSzereltFoszam["Id"].ToString();
                szereltIds.Add(foszamId);
            }
        }

        if (szereltIds.Count > 0)
        {
            szereltIds.AddRange(mig_Foszam_Id_Array);

            mig_Foszam_Id_Array = szereltIds.ToArray();
        }

        return mig_Foszam_Id_Array;
    }

    /// <summary>
    /// Jegyz�k t�telek t�meges sztorn�z�sa
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod(Description = "Jegyz�k t�telek t�meges sztorn�z�sa (F�sz�m (�gyirat) �s szerelt t�teleinek lev�tele a jegyz�kr�l)")]
    public Result JegyzekTetelSztornozasTomeges(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_sztornozas = JegyzekTetelSztornozas(ExecParams[i]);
                if (!String.IsNullOrEmpty(result_sztornozas.ErrorCode))
                {
                    throw new ResultException(result_sztornozas);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }

    /// <summary>
    /// Jegyz�k t�tel sztorn�z�sa
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod(Description = "Jegyz�k t�tel sztorn�z�sa (F�sz�m (�gyirat) �s szerelt t�teleinek lev�tele a jegyz�kr�l)")]
    public Result JegyzekTetelSztornozas(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // k�s�bb a teljes hierarchi�t is lek�rj�k, de itt kell a f�sz�m Id
            Result resGet = this.Get(execParam);

            if (resGet.IsError)
            {
                throw new ResultException(resGet);
            }

            MIG_JegyzekTetelek mig_JegyzekTetelek_selected = (MIG_JegyzekTetelek)resGet.Record;

            #region Ellen�rz�s
            // TODO:
            // pl. most felt�telezz�k, hogy a szerel�si hierarchia tetej�n �ll� f�sz�m
            // t�tel�t sztorn�zza...
            if (!String.IsNullOrEmpty(mig_JegyzekTetelek_selected.SztornozasDat))
            {
                throw new ResultException(50603); //M�r �rv�nytelen�tve van a rekord!
            }
            #endregion Ellen�rz�s

            #region Szerelt �gyiratok jegyz�kt�teleinek lek�r�se
            string[] mig_Foszam_Array = new string[] { mig_JegyzekTetelek_selected.MIG_Foszam_Id };
            mig_Foszam_Array = AddSzereltFoszamokToArray(execParam.Clone(), mig_Foszam_Array);

            MIG_JegyzekTetelekSearch search = new MIG_JegyzekTetelekSearch();
            search.Jegyzek_Id.Value = mig_JegyzekTetelek_selected.Jegyzek_Id;
            search.Jegyzek_Id.Operator = Query.Operators.equals;

            search.MIG_Foszam_Id.Value = Search.GetSqlInnerString(mig_Foszam_Array);
            search.MIG_Foszam_Id.Operator = Query.Operators.inner;

            Result result_jegyzektetelekGetAll = this.GetAll(execParam.Clone(), search);
            if (result_jegyzektetelekGetAll.IsError)
            {
                throw new ResultException(result_jegyzektetelekGetAll);
            }

            #endregion Szerelt �gyiratok jegyz�kt�teleinek lek�r�se

            #region F�sz�m lek�r�se, UGYHOL vissza�ll�t�sa
            MIG_FoszamService service_foszam = new MIG_FoszamService(this.dataContext);

            ExecParam execParam_foszam = execParam.Clone();
            execParam_foszam.Record_Id = mig_JegyzekTetelek_selected.MIG_Foszam_Id;

            Result result_foszamGet = service_foszam.Get(execParam_foszam);
            if (result_foszamGet.IsError)
            {
                throw new ResultException(result_foszamGet);
            }

            MIG_Foszam mig_Foszam = (MIG_Foszam)result_foszamGet.Record;

            #region Ellen�rz�s
            // TODO:
            // pl. most felt�telezz�k, hogy a szerel�si hierarchia tetej�n �ll� f�sz�m
            // t�tel�t sztorn�zza...
            if (!String.IsNullOrEmpty(mig_Foszam.Csatolva_Id))
            {
                // Hiba a jegyz�k t�tel sztorn�z�sa sor�n: szerelt �gyirat csak a sz�l�j�n kereszt�l t�r�lhet� a jegyz�kr�l!
                throw new ResultException(55202);
            }
            #endregion Ellen�rz�s

            mig_Foszam.Base.Updated.SetValueAll(false);
            mig_Foszam.Updated.SetValueAll(false);
            mig_Foszam.Base.Updated.Ver = true;

            mig_Foszam.UGYHOL = mig_JegyzekTetelek_selected.UGYHOL;
            mig_Foszam.Updated.UGYHOL = true;

            Result result_foszamAthelyezes = service_foszam.FoszamAthelyzes(execParam_foszam, mig_Foszam, true);
            if (result_foszamAthelyezes.IsError)
            {
                throw new ResultException(result_foszamAthelyezes);
            }

            //Result result_update = service_foszam.Update(execParam_foszam, mig_Foszam);
            //if (result_update.IsError)
            //{
            //    throw new ResultException(result_update);
            //}
            #endregion F�sz�m lek�r�se, UGYHOL vissza�ll�t�sa

            MIG_JegyzekTetelek mig_JegyzekTetelek = null;
            ExecParam execParam_jegyzektetelekUpdate = execParam.Clone();

            foreach (DataRow row in result_jegyzektetelekGetAll.Ds.Tables[0].Rows)
            {
                mig_JegyzekTetelek = new MIG_JegyzekTetelek();
                Utility.LoadBusinessDocumentFromDataRow(mig_JegyzekTetelek, row);

                execParam_jegyzektetelekUpdate.Record_Id = mig_JegyzekTetelek.Id;

                mig_JegyzekTetelek.Base.Updated.SetValueAll(false);
                mig_JegyzekTetelek.Updated.SetValueAll(false);
                mig_JegyzekTetelek.Base.Updated.Ver = true;

                mig_JegyzekTetelek.SztornozasDat = DateTime.Now.ToString();
                mig_JegyzekTetelek.Updated.SztornozasDat = true;

                Result result_update = this.Update(execParam_jegyzektetelekUpdate, mig_JegyzekTetelek);

                if (result_update.IsError)
                {
                    throw new ResultException(result_update);
                }

                if (mig_JegyzekTetelek.Id == execParam.Record_Id)
                {
                    result = result_update;
                }
            }

            #region Esem�nynapl�z�s
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "MIG_Jegyzekek", "JegyzekSztornozas").Record;
            //    Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            //}
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    public Result Atadas(ExecParam execParam, string[] tetelIds, string atadasDatuma)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            foreach (string id in tetelIds)
            {
                execParam.Record_Id = id;

                result = this.Get(execParam);
                result.CheckError();

                MIG_JegyzekTetelek tetel = result.Record as MIG_JegyzekTetelek;
                tetel.Updated.SetValueAll(false);
                tetel.Base.Updated.SetValueAll(false);
                tetel.Base.Updated.Ver = true;

                tetel.AtadasDatuma = atadasDatuma;
                tetel.Updated.AtadasDatuma = true;

                result = this.Update(execParam, tetel);

                result.CheckError();
            }


            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    #endregion fel�lvizsg�lat
}