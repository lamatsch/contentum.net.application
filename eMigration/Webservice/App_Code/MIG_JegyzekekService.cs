using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eMigration.BusinessDocuments;
using Contentum.eMigration.Query.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Collections;
using System.Collections.Generic;
using Contentum.eQuery.BusinessDocuments;

[WebService(Namespace = "Contentum.eMigration.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class MIG_JegyzekekService : System.Web.Services.WebService
{
    private MIG_JegyzekekStoredProcedure sp = null;

    private DataContext dataContext;

    public MIG_JegyzekekService()
    {
        dataContext = new DataContext(this.Application);

        sp = new MIG_JegyzekekStoredProcedure(dataContext);
    }

    public MIG_JegyzekekService(DataContext _dataContext)
    {
         this.dataContext = _dataContext;
         sp = new MIG_JegyzekekStoredProcedure(dataContext);
    }

    void SetJegyzekAllapot(ExecParam execParam, MIG_Jegyzekek jegyzek, string allapot)
    {
        jegyzek.Allapot = allapot;
        jegyzek.Updated.Allapot = true;

        jegyzek.Allapot_Nev = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar("JEGYZEK_ALLAPOT", allapot, execParam.Clone(), Context.Cache);
        jegyzek.Updated.Allapot_Nev = true;
    }

    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Jegyzekek))]
    public Result Get(ExecParam ExecParam)
    {
        //return sp.Get(ExecParam);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Get(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    //[WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_JegyzekekSearch))]
    public Result GetAll(ExecParam ExecParam, MIG_JegyzekekSearch _MIG_JegyzekekSearch)
    {
        //return sp.GetAll(ExecParam, _MIG_JegyzekekSearch);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam, _MIG_JegyzekekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Insert(ExecParam ExecParam, MIG_Jegyzekek Record)
    /// Egy rekord felv�tele a MIG_Jegyzekek t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Jegyzekek))]
    public Result Insert(ExecParam ExecParam, MIG_Jegyzekek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            SetJegyzekAllapot(ExecParam, Record, KodTarak.JEGYZEK_ALLAPOT.Nyitott);

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Update(ExecParam ExecParam, MIG_Jegyzekek Record)
    /// Az MIG_Jegyzekek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Jegyzekek))]
    public Result Update(ExecParam ExecParam, MIG_Jegyzekek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az MIG_Jegyzekek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Jegyzekek))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// MultiInvalidate(ExecParam ExecParams)
    /// A MIG_Jegyzekek t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bl�ban t�bb rekord logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). A t�rlend� t�telek azonos�t�it (Record_Id) a ExecParams t�mb tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Jegyzekek))]
    public Result MultiInvalidate(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_invalidate = Invalidate(ExecParams[i]);
                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                {
                    throw new ResultException(result_invalidate);
                }
            }
            ////Commit:
            //ContextUtil.SetComplete();


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }
    /// <summary>
    /// Delete(ExecParam ExecParam)
    /// A(z) MIG_Jegyzekek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak fizikai t�rl�se.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>     
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak fizikai t�rl�se. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Jegyzekek))]
    public Result Delete(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Delete(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    #region fel�lvizsg�lat
    /// <summary>
    /// Jegyz�k lez�r�sa v�grehajt�s (�tad�s) el�tt
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result JegyzekLezarasa(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            Result result_get = this.Get(execParam);

            MIG_Jegyzekek jegyzek = (MIG_Jegyzekek)result_get.Record;


            jegyzek.Updated.SetValueAll(false);
            jegyzek.Base.Updated.SetValueAll(false);
            jegyzek.Base.Updated.Ver = true;

            //MIG_JegyzekTetelekService service_jegyzektetelek = new MIG_JegyzekTetelekService(this.dataContext);
            //MIG_JegyzekTetelekSearch search_jegyzektetelek = new MIG_JegyzekTetelekSearch();
            //ExecParam execParam_jegyzektetelek = execParam.Clone();
            //search_jegyzektetelek.Jegyzek_Id.Value = jegyzek.Id;
            //search_jegyzektetelek.Jegyzek_Id.Operator = Query.Operators.equals;

            //search_jegyzektetelek.SztornozasDat.Value = "";
            //search_jegyzektetelek.SztornozasDat.Operator = Query.Operators.isnull;

            //Result result_jegyzektetelekGetAll = service_jegyzektetelek.GetAll(execParam_jegyzektetelek, search_jegyzektetelek);

            //if (result_jegyzektetelekGetAll.IsError)
            //{
            //    throw new ResultException(result_jegyzektetelekGetAll);
            //}

            // esem�nynapl�z�shoz �s t�meges lek�r�shez
            //List<string> selectedFoszamIds = new List<string>(result_jegyzektetelekGetAll.Ds.Tables[0].Rows.Count);
            //foreach (DataRow row in result_jegyzektetelekGetAll.Ds.Tables[0].Rows)
            //{
            //    string foszamId = row["MIG_Foszam_Id"].ToString();

            //    //if (!selectedFoszamIds.Contains(foszamId))
            //    //{
            //    selectedFoszamIds.Add(foszamId);
            //    //}
            //}

            //foreach (DataRow row in res1.Ds.Tables[0].Rows)
            //if (selectedFoszamIds.Count > 0)
            //{
            ExecParam execParam_foszam = execParam.Clone();
            //xpm2.Record_Id = row["Obj_Id"].ToString();
            MIG_FoszamSearch search_foszam = new MIG_FoszamSearch();
            //search_foszam.Id.Value = Search.GetSqlInnerString(selectedFoszamIds.ToArray());
            //search_foszam.Id.Operator = Query.Operators.inner;
            search_foszam.ExtendedMIG_JegyzekTetelekSearch = new MIG_JegyzekTetelekSearch();
            search_foszam.ExtendedMIG_JegyzekTetelekSearch.Jegyzek_Id.Value = jegyzek.Id;
            search_foszam.ExtendedMIG_JegyzekTetelekSearch.Jegyzek_Id.Operator = Query.Operators.equals;
            search_foszam.ExtendedMIG_JegyzekTetelekSearch.SztornozasDat.Value = "";
            search_foszam.ExtendedMIG_JegyzekTetelekSearch.SztornozasDat.Operator = Query.Operators.isnull;

            MIG_FoszamService service_foszam = new MIG_FoszamService(this.dataContext);

            //Result result_foszamGetAll = service_foszam.GetAll(execParam_foszam, search_foszam); // t�meges lek�r�s

            //if (result_foszamGetAll.IsError)
            //{
            //    throw new ResultException(result_foszamGetAll);
            //}

            //if (result_foszamGetAll.Ds.Tables[0].Rows.Count < selectedFoszamIds.Count)
            //{
            //    // "Hiba a jegyz�k lez�r�sa sor�n: Nem tal�lhat� meg minden jegyz�kre vett �gyirat!"
            //    throw new ResultException(55101);
            //}

            //foreach (DataRow row in result_foszamGetAll.Ds.Tables[0].Rows)
            //{
            //    MIG_Foszam mig_Foszam = new MIG_Foszam();
            //    Utility.LoadBusinessDocumentFromDataRow(mig_Foszam, row);

            //    mig_Foszam.Updated.SetValueAll(false);
            //    mig_Foszam.Base.Updated.SetValueAll(false);
            //    mig_Foszam.Base.Updated.Ver = true;

            //    //�llapot be�ll�t�s
            //    mig_Foszam.UGYHOL = Contentum.eUtility.Constants.MIG_IratHelye.LezartJegyzek; //"Z"; //KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo;
            //    mig_Foszam.Updated.UGYHOL = true;

            //    execParam_foszam.Record_Id = mig_Foszam.Id;

            //    Result result_foszamUpdate = service_foszam.Update(execParam_foszam, mig_Foszam);

            //    if (result_foszamUpdate.IsError)
            //    {
            //        throw new ResultException(result_foszamUpdate);
            //    }

            //}

            //f�sz�m update
            MIG_Foszam mig_Foszam = new MIG_Foszam();
            mig_Foszam.Updated.SetValueAll(false);
            mig_Foszam.Base.Updated.SetValueAll(false);

            //�llapot be�ll�t�s
            mig_Foszam.UGYHOL = Contentum.eUtility.Constants.MIG_IratHelye.LezartJegyzek; //"Z"; //KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo;
            mig_Foszam.Updated.UGYHOL = true;

            Result result_foszamUpdate = service_foszam.UpdateTomeges(execParam_foszam, search_foszam, mig_Foszam, false);

            if (result_foszamUpdate.IsError)
            {
                throw new ResultException(result_foszamUpdate);
            }

            //}

            jegyzek.LezarasDatuma = DateTime.Now.ToString();
            jegyzek.Updated.LezarasDatuma = true;

            SetJegyzekAllapot(execParam, jegyzek, KodTarak.JEGYZEK_ALLAPOT.Lezart);

            Result result_update = this.Update(execParam, jegyzek);
            result = result_update;


            #region Esem�nynapl�z�s
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "MIG_Jegyzekek", "JegyzekLezaras").Record;
            //    Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            //}
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// Jegyz�k v�grehajt�sa (�tad�s)
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result JegyzekVegrehajtasa(ExecParam execParam, string atvevo, bool csatolmanyokTorleseAszinkron)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            Result result_get = this.Get(execParam);

            MIG_Jegyzekek jegyzek = (MIG_Jegyzekek)result_get.Record;

            FoszamokVegrehajtasa(execParam, jegyzek, atvevo, csatolmanyokTorleseAszinkron);

            jegyzek.Updated.SetValueAll(false);
            jegyzek.Base.Updated.SetValueAll(false);
            jegyzek.Base.Updated.Ver = true;

            if (!String.IsNullOrEmpty(atvevo))
            {
                jegyzek.Atvevo_Nev = atvevo;
                jegyzek.Updated.Atvevo_Nev = true;
            }
            jegyzek.FelhasznaloCsoport_Id_Vegrehaj = execParam.Felhasznalo_Id;
            jegyzek.Updated.FelhasznaloCsoport_Id_Vegrehaj = true;

            jegyzek.Vegrehajto_Nev = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(execParam.Felhasznalo_Id, execParam.Clone(), Context.Cache);
            jegyzek.Updated.Vegrehajto_Nev = true;

            jegyzek.VegrehajtasDatuma = DateTime.Now.ToString();
            jegyzek.Updated.VegrehajtasDatuma = true;

            if(csatolmanyokTorleseAszinkron)
                SetJegyzekAllapot(execParam, jegyzek, KodTarak.JEGYZEK_ALLAPOT.VegrehajtasFolyamatban);
            else
                SetJegyzekAllapot(execParam, jegyzek, KodTarak.JEGYZEK_ALLAPOT.Vegrahajtott);

            if (csatolmanyokTorleseAszinkron)
            {
                jegyzek.VegrehajtasKezdoDatuma = DateTime.Now.ToString();
                jegyzek.Updated.VegrehajtasKezdoDatuma = true;
            }

            Result result_update = this.Update(execParam, jegyzek);
            result = result_update;

            // Kiv�telesen kett�s napl�z�s: nem csak a jegyz�khez, hanem az �gyiratokhoz is k�tj�k az esem�nybejegyz�st!
            #region Esem�nynapl�z�s
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "MIG_Jegyzekek", "JegyzekVegrehajtas").Record;
            //    Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            //    //}

            //    //if (isTransactionBeginHere)
            //    //{
            //    //KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            //    string ids = Search.GetSqlInnerString(selectedUgyiratIds.ToArray());

            //    //Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ids, "UgyiratJegyzekVegrehajtas");
            //    eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ids, "UgyiratJegyzekVegrehajtas");
            //    if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
            //        Logger.Debug("[ERROR]MIG_Foszam::UgyiratJegyzekVegrehajtas: ", execParam, eventLogResult);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }


    /// <summary>
    /// Jegyz�k sztorn�z�sa
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result JegyzekSztornozas(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result resGet = this.Get(execParam);

            if (resGet.IsError)
            {
                throw new ResultException(resGet);
            }

            #region Ellen�rz�s - nincs rajta t�tel
            MIG_JegyzekTetelekService service_jegyzektetelek = new MIG_JegyzekTetelekService(this.dataContext);
            MIG_JegyzekTetelekSearch search_jegyzektetelek = new MIG_JegyzekTetelekSearch();

            ExecParam execParam_jegyzektetelek = execParam.Clone();

            search_jegyzektetelek.Jegyzek_Id.Value = execParam.Record_Id;
            search_jegyzektetelek.Jegyzek_Id.Operator = Query.Operators.equals;

            search_jegyzektetelek.SztornozasDat.Value = "";
            search_jegyzektetelek.SztornozasDat.Operator = Query.Operators.isnull;

            search_jegyzektetelek.TopRow = 1; // csak l�tez�st vizsg�lunk

            Result result_jegyzektetelekGetAll = service_jegyzektetelek.GetAll(execParam_jegyzektetelek, search_jegyzektetelek);
            if (result_jegyzektetelekGetAll.IsError)
            {
                throw new ResultException(result_jegyzektetelekGetAll);
            }

            if (result_jegyzektetelekGetAll.Ds.Tables[0].Rows.Count > 0)
            {
                //"Hiba a jegyz�k sztorn�z�sa sor�n: A jegyz�k nem sztorn�zhat�, mert a jegyz�khez tartoznak t�telek!"
                throw new ResultException(55102);
            }
            #endregion Ellen�rz�s - nincs rajta t�tel

            MIG_Jegyzekek jegyzek = (MIG_Jegyzekek)resGet.Record;
            jegyzek.Base.Updated.SetValueAll(false);
            jegyzek.Updated.SetValueAll(false);
            jegyzek.Base.Updated.Ver = true;

            jegyzek.SztornozasDat = DateTime.Now.ToString();
            jegyzek.Updated.SztornozasDat = true;

            SetJegyzekAllapot(execParam, jegyzek, KodTarak.JEGYZEK_ALLAPOT.Sztornozott);

            result = this.Update(execParam, jegyzek);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "MIG_Jegyzekek", "JegyzekSztornozas").Record;
            //    Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            //}
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(MIG_Jegyzekek))]
    public Result JegyzekMegsemmisitese(ExecParam execParam, MIG_Jegyzekek jegyzek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = this.Update(execParam, jegyzek);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_IraJegyzekek", "JegyzekMegsemmisites").Record;

            //    Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #endregion

    #region jegyz�k v�grehajt�sa

    void FoszamokVegrehajtasa(ExecParam execParam, MIG_Jegyzekek jegyzek, string atvevo, bool csatolmanyokTorleseAszinkron)
    {
        MIG_Foszam mig_Foszam = new MIG_Foszam();
        mig_Foszam.Updated.SetValueAll(false);
        mig_Foszam.Base.Updated.SetValueAll(false);

        string UGYHOL_Jegyzek = null;

        //�llapot be�ll�t�s
        if (jegyzek.Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value) //"S"
        {
            UGYHOL_Jegyzek = Contentum.eUtility.Constants.MIG_IratHelye.Selejtezett; //"S";//KodTarak.UGYIRAT_ALLAPOT.Selejtezett;
            mig_Foszam.Selejtezes_Datuma = DateTime.Now.ToString();
            mig_Foszam.Updated.Selejtezes_Datuma = true;
        }
        else if (jegyzek.Tipus == UI.JegyzekTipus.LeveltariAtadasJegyzek.Value) //"L"
        {
            UGYHOL_Jegyzek = Contentum.eUtility.Constants.MIG_IratHelye.Leveltar; //"T";// KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott;
        }
        else if (jegyzek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value) //"E"
        {
            UGYHOL_Jegyzek = Contentum.eUtility.Constants.MIG_IratHelye.EgyebSzervezetnekAtadott; // "A"; //KodTarak.UGYIRAT_ALLAPOT.EgyebSzervezetnekAtadott;
        }

        mig_Foszam.UGYHOL = UGYHOL_Jegyzek;
        mig_Foszam.Updated.UGYHOL = true;

        ExecParam execParam_foszam = execParam.Clone();

        MIG_FoszamSearch search_foszam = new MIG_FoszamSearch();
        search_foszam.ExtendedMIG_JegyzekTetelekSearch = new MIG_JegyzekTetelekSearch();
        search_foszam.ExtendedMIG_JegyzekTetelekSearch.Jegyzek_Id.Value = jegyzek.Id;
        search_foszam.ExtendedMIG_JegyzekTetelekSearch.Jegyzek_Id.Operator = Query.Operators.equals;
        search_foszam.ExtendedMIG_JegyzekTetelekSearch.SztornozasDat.Value = "";
        search_foszam.ExtendedMIG_JegyzekTetelekSearch.SztornozasDat.Operator = Query.Operators.isnull;

        MIG_FoszamService service_foszam = new MIG_FoszamService(this.dataContext);

        Result result_foszamAthelyezes = service_foszam.UpdateTomeges(execParam_foszam, search_foszam, mig_Foszam, false);

        if (result_foszamAthelyezes.IsError)
        {
            throw new ResultException(result_foszamAthelyezes);
        }

        if (!csatolmanyokTorleseAszinkron)
        {
            DeleteCsatolmanyok(execParam.Clone(), jegyzek.Id);
        }
    }

    void DeleteCsatolmanyok(ExecParam execParam, string jegyzekId)
    {
        MIG_DokumentumService dokumentumService = new MIG_DokumentumService(this.dataContext);
        MIG_DokumentumSearch dokumentumSearch = new MIG_DokumentumSearch();

        dokumentumSearch.MIG_Alszam_Id.Value = String.Format("SELECT Id FROM MIG_Alszam WHERE MIG_Foszam_Id in (SELECT MIG_Foszam_Id FROM MIG_JegyzekTetelek WHERE Jegyzek_Id = '{0}' AND SztornozasDat IS NULL)", jegyzekId);
        dokumentumSearch.MIG_Alszam_Id.Operator = Query.Operators.inner;

        Result result = dokumentumService.GetAll(execParam, dokumentumSearch);
        result.CheckError();

        foreach(DataRow row in result.Ds.Tables[0].Rows)
        {
            MIG_Dokumentum dokumentum = new MIG_Dokumentum();
            Utility.LoadBusinessDocumentFromDataRow(dokumentum, row);

            dokumentumService.DeleteDokumentum(execParam, dokumentum);
        }
    }

    [WebMethod()]
    public Result FolyamatbanLevoJegyzekekVegrehajtasa()
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309"; //Admin
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            MIG_JegyzekekSearch jegyzekekSearch = new MIG_JegyzekekSearch();
            jegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.VegrehajtasFolyamatban;
            jegyzekekSearch.Allapot.Operator = Query.Operators.equals;

            Logger.Debug("MIG_JegyzekekService.GetAll kezdete");
            Result jegyzekekResult = this.GetAll(execParam, jegyzekekSearch);
            Logger.Debug("MIG_JegyzekekService.GetAll vege");

            if (jegyzekekResult.IsError)
            {
                Logger.Error("MIG_JegyzekekService.GetAll hiba", execParam, jegyzekekResult);
                throw new ResultException(jegyzekekResult);
            }

            DataTable jegyzekekTable = jegyzekekResult.Ds.Tables[0];
            Logger.Debug(String.Format("Jegyz�kek sz�ma: {0}", jegyzekekTable.Rows.Count));

            foreach (DataRow row in jegyzekekTable.Rows)
            {
                MIG_Jegyzekek jegyzek = new MIG_Jegyzekek();
                Utility.LoadBusinessDocumentFromDataRow(jegyzek, row);
                try
                {
                    FolyamatbanLevoJegyzekVegrehajtasa(jegyzek);
                }
                catch (Exception ex)
                {
                    Result error = ResultException.GetResultFromException(ex);
                    Logger.Error(String.Format("FolyamatbanLevoJegyzekVegrehajtasa hiba: {0}", jegyzek.Id), execParam, error);
                }
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    void FolyamatbanLevoJegyzekVegrehajtasa(MIG_Jegyzekek jegyzek)
    {
        Logger.Info(String.Format("FolyamatbanLevoJegyzekVegrehajtasa kezdete: {0}", jegyzek.Id));

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = jegyzek.FelhasznaloCsoport_Id_Vegrehaj;
        execParam.Record_Id = jegyzek.Id;

        Logger.Debug("DeleteCsatolmanyok kezdete");
        DeleteCsatolmanyok(execParam, jegyzek.Id);
        Logger.Debug("DeleteCsatolmanyok vege");

        jegyzek.Updated.SetValueAll(false);
        jegyzek.Base.Updated.SetValueAll(false);
        jegyzek.Base.Updated.Ver = true;

        SetJegyzekAllapot(execParam, jegyzek, KodTarak.JEGYZEK_ALLAPOT.Vegrahajtott);

        jegyzek.VegrehajtasDatuma = DateTime.Now.ToString();
        jegyzek.Updated.VegrehajtasDatuma = true;

        Logger.Debug("MIG_JegyzekekService.Update kezdete");
        Result jegyzekResult = this.Update(execParam, jegyzek);
        Logger.Debug("MIG_JegyzekekService.Update vege");

        if (jegyzekResult.IsError)
        {
            Logger.Error("MIG_JegyzekekService.Update hiba", execParam, jegyzekResult);
            throw new ResultException(jegyzekResult);
        }


        Logger.Info("FolyamatbanLevoJegyzekVegrehajtasa vege");
    }

    #endregion
}