using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eUtility;
using System.Data;
using System.Collections.Generic;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;


/// <summary>
/// Summary description for RecordHistoryService
/// </summary>
[WebService(Namespace = "Contentum.eMigration.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class RecordHistoryService : System.Web.Services.WebService 
{
    private RecordHistoryServiceStoredProcedure sp = null;

    private DataContext dataContext;

    public RecordHistoryService () 
    {
        dataContext = new DataContext(this.Application);

        //sp = new RecordHistoryServiceStoredProcedure(this.Application);
        sp = new RecordHistoryServiceStoredProcedure(this.dataContext);
    }

    public RecordHistoryService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new RecordHistoryServiceStoredProcedure(this.dataContext);
    }


    [WebMethod()]
    public Result GetAllByRecord(ExecParam ExecParam, string TableName, System.Data.SqlTypes.SqlGuid Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByRecord(ExecParam, TableName);

            ResolveForeignKeys(ExecParam, result);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private void ResolveForeignKeys(ExecParam ExecParam, Result result)
    {
        if (result.IsError)
        {
            throw new ResultException(result);
        }

        if (result.Ds != null && result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
        {
            Dictionary<string, string> felhasznaloNev = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            DataTable tableResult = result.Ds.Tables[0];
            foreach (DataRow row in tableResult.Rows)
            {
                string executor = row["Executor"].ToString();
                if (!String.IsNullOrEmpty(executor)) continue;

                string executorId = row["ExecutorId"].ToString();
                if (!String.IsNullOrEmpty(executorId))
                {
                    if (!felhasznaloNev.ContainsKey(executorId))
                    {
                        felhasznaloNev.Add(executorId, executorId);
                    }
                }
            }

            if (felhasznaloNev.Count == 0) return;

            KRT_FelhasznalokService svcFelhasznalok = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            ExecParam xpm = ExecParam.Clone();
            KRT_FelhasznalokSearch schFelhasznalok = new KRT_FelhasznalokSearch();
            string[] felhasznaloIds = new string[felhasznaloNev.Count];
            felhasznaloNev.Values.CopyTo(felhasznaloIds, 0);
            schFelhasznalok.Id.Value = Search.GetSqlInnerString(felhasznaloIds);
            schFelhasznalok.Id.Operator = Query.Operators.inner;

            Result resFelhasznalok = svcFelhasznalok.GetAll(xpm, schFelhasznalok);

            if (resFelhasznalok.IsError)
                throw new ResultException(resFelhasznalok);

            foreach (DataRow row in resFelhasznalok.Ds.Tables[0].Rows)
            {
                string id = row["Id"].ToString();
                string nev = row["Nev"].ToString();

                if (felhasznaloNev.ContainsKey(id))
                {
                    felhasznaloNev[id] = nev;
                }
            }

            foreach (DataRow row in tableResult.Rows)
            {
                string executor = row["Executor"].ToString();
                if (!String.IsNullOrEmpty(executor)) continue;

                string executorId = row["ExecutorId"].ToString();
                if (felhasznaloNev.ContainsKey(executorId))
                {
                    row["Executor"] = felhasznaloNev[executorId];
                }
            }
        }
    }  
    
}

