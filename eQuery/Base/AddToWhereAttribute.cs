﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.Base
{
    [AttributeUsage(AttributeTargets.Field|AttributeTargets.Property,AllowMultiple=true,Inherited=true)]
    public class AddToWhereAttribute: Attribute
    {
        private bool _addToWhere;

        public bool AddToWhere
        {
            get
            {
                return this._addToWhere;
            }
        }

        public AddToWhereAttribute()
        {
            this._addToWhere = true;
        }

        public AddToWhereAttribute(bool AddToWhereValue)
        {
            this._addToWhere = AddToWhereValue;
        }
    }
}
