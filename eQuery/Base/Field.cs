using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlTypes;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// Field oszt�ly
    /// Egy t�bla egy mez�j�t reprezent�l� objektum
    /// A keres�si objektumokban haszn�lt
    /// </summary>  
    [Serializable]
    public class Field
    {
        public Field(String FieldName, String FieldType)
        {
            _Type = FieldType;
            _Name = FieldName;
        }

        public Field(String FieldName)
        {
            _Type = typeof(String).Name;
            _Name = FieldName;
        }
        public Field()
        {
        }

        private String _Name = "";
        /// <summary>
        /// A mez� neve t�blan�v hivatkoz�ssal egy�tt, pl. EREC_UgyUgyiratok.Allapot
        /// </summary>
        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private String _Operator = "";
        /// <summary>
        /// Az adott mez�re vonatkoz� m�velet pl. '='
        /// </summary>
        public String Operator
        {
            get { return _Operator; }
            set { _Operator = value; }
        }

        private String _Value = "";
        /// <summary>
        /// Az adott mez� �rt�ke a where felt�telben
        /// </summary>
        public String Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        private String _ValueTo = "";
        /// <summary>
        /// between oper�torn�l haszn�latos, ilyenkor a gener�lt sz�r�si felt�tel: [t�bla oszlop] between [Value] and [ValueTo]
        /// </summary>
        public String ValueTo
        {
            get { return _ValueTo; }
            set { _ValueTo = value; }
        }

        private String _Type = "";
        /// <summary>
        /// A mez� t�pusa, pl. "Int32", "Guid", "String"
        /// </summary>
        public String Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private String _Group = "0";

        public String Group
        {
            get { return _Group; }
            set { _Group = (String.IsNullOrEmpty(value) ? "0" : value); }
        }
        private String _GroupOperator = Query.Operators.and;

        public String GroupOperator
        {
            get { return _GroupOperator; }
            set { _GroupOperator = (String.IsNullOrEmpty(value) ? Query.Operators.and : value); }
        }

        public void Clear()
        {
            this._Value = String.Empty;
            this._ValueTo = String.Empty;
            this._Operator = String.Empty;
            this._Group = "0";
            this._GroupOperator = Query.Operators.and;
        }

        #region Filterek
        public void NotEquals(string value)
        {
            Filter(value, Query.Operators.notequals);
        }

        public void Greater(string value)
        {
            Filter(value, Query.Operators.greater);
        }

        public void GreaterOrEqual(string value)
        {
            Filter(value, Query.Operators.greaterorequal);
        }

        public void Less(string value)
        {
            Filter(value, Query.Operators.less);
        }

        public void LessOrEqual(string value)
        {
            Filter(value, Query.Operators.lessorequal);
        }

        public void IsNull()
        {
            Filter("", Query.Operators.isnull);
        }

        public void IsNullOrEquals(string value)
        {
            Filter(value, Query.Operators.isnullorequals);
        }

        public void IsNullOrNotEquals(string value)
        {
            Filter(value, Query.Operators.isnullornotequals);
        }

        public void NotNull()
        {
            Filter("", Query.Operators.notnull);
        }

        public void In(IEnumerable<string> values)
        {
            FilterIn(values, Query.Operators.inner);
        }

        public void IsNullOrIn(IEnumerable<string> values)
        {
            FilterIn(values, Query.Operators.isnullorinner);
        }

        public void IsNullOrNotIn(IEnumerable<string> values)
        {
            FilterIn(values, Query.Operators.isnullornotinner);
        }

        public void NotIn(IEnumerable<string> values)
        {
            FilterIn(values, Query.Operators.notinner);
        }

        private void FilterIn(IEnumerable<string> values, string inOperator)
        {
            if (values != null)
            {
                Filter(ToSqlInner(values), inOperator);
            }
        }

        public void Like(string value)
        {
            Filter(value, Query.Operators.like);
        }

        public void LikeFull(string value)
        {
            Filter(value, Query.Operators.likefull);
        }

        public void LikeStart(string value)
        {
            Filter(value, Query.Operators.likestart);
        }

        // based on Search.GetOperatorByLikeCharater (not accessible from here)
        public void LikeWhenNeeded(string value)
        {
            Value = value;

            if (value == null) Operator = "";
            else if (value.Trim() != "")
            {
                Operator = value.Contains("*") ? Query.Operators.like : Query.Operators.equals;
            }
            else Operator = "";
        }

        public void LikeWhenNeededIfNotEmpty(string value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                LikeWhenNeeded(value);
            }
        }

        public void Filter(string value)
        {
            Filter(value, Query.Operators.equals);
        }

        public void FilterIfNotEmpty(string value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                Filter(value, Query.Operators.equals);
            }
        }

        public void Filter(string value, string valueOperator)
        {
            Value = value;
            Operator = valueOperator;
        }

        public void FilterIfNotEmpty(string value, string valueOperator)
        {
            if (!String.IsNullOrEmpty(value))
            {
                Value = value;
                Operator = valueOperator;
            }
        }

        private void SetGroup(string group, string groupOperator)
        {
            Group = group;
            GroupOperator = groupOperator;
        }

        public void OrGroup(string group)
        {
            SetGroup(group, Query.Operators.or);
        }

        public void AndGroup(string group)
        {
            SetGroup(group, Query.Operators.and);
        }

        public void Between(string from, string to)
        {
            Value = from;
            ValueTo = to;
            Operator = Query.Operators.between;
        }

        private static string ToSqlInner(IEnumerable<string> values)
        {
            return values == null ? "" : String.Format("'{0}'", String.Join("','", values.ToArray()));
        }
        #endregion
    }
}
