using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

//using System.Runtime.InteropServices; // union imit�l�s�hoz

namespace Contentum.eQuery.FullTextSearch
{
    public class FullTextSearchException : ApplicationException
    {
        private string _ErrorCode = "";
        private string _ErrorMessage = "";

        public const string DefaultErrorCode = "[FTS-01]";

        public string ErrorCode
        {
            get { return _ErrorCode;}
        }

        public string ErrorMessage
        {
            get { return _ErrorMessage;}
        }


        public FullTextSearchException()
        {
        }

        public FullTextSearchException(string message)
        {
            _ErrorMessage = message;
            if (String.IsNullOrEmpty(_ErrorCode))
            {
                _ErrorCode = DefaultErrorCode;
            }
        }

        public FullTextSearchException(string errorcode, string message)
        {
            _ErrorMessage = message;
            _ErrorCode = errorcode;
        }



    }

    [Serializable]
    public class FullTextSearchTree
    {

        public enum Operator { None, And, Or, AndNot, Union, Intersect, Except };

        public enum NodeType { Unknown, Leaf, Operator };

        public Operator DefaultOperator;
        public static char FieldValueSeparator = '|';

        private OperatorGroup _root;
        private OperatorGroup _currentNode;

        private List<FieldValuePair> _leafList;
        public List<FieldValuePair> LeafList
        {
            get { return _leafList; }
            set { _leafList = value; }
        }

        // serializ�ci� ut�n ebb�l �jra el��ll�that�, ill. a felhaszn�l�i fel�leten a mez�k kit�lthet�k (FormTemplateLoader
        private List<NodeData> _inorderNodeList;
        public List<NodeData> InorderNodeList
        {
            get { return _inorderNodeList; }
            set { _inorderNodeList = value; }
        }
        /// <summary>
        /// Megmutatja, hogy a soros�tott t�rol�shoz el� van-e k�sz�tve a csom�pontok list�ja
        /// </summary>
        private bool _bIsInorderNodeListSet = false;
        public bool bIsInorderNodeListSet
        {
            get { return _bIsInorderNodeListSet; }
            set { _bIsInorderNodeListSet = value; }
        }
        
        /// <summary>
        /// A lek�rdezend� t�bla neve (pl. "EREC_ObjektumTargySzavai")
        /// </summary>
        private string _TableName = "";
        public string TableName
        {
            get { return _TableName; }
        }

        /// <summary>
        /// halmazm�veletek est�n azon mez�k, melyeket a SELECT-be fel kell venni
        /// pl. "Id, Ertek" -> SELECT Id, Ertek FROM <TableName>
        /// </summary>
        private string _SelectFields = "*";
        public string SelectFields
        {
            get { return _SelectFields; }
            set { _SelectFields = value; }
        }

        public int LeafCount
        {
            get { return (_leafList == null ? 0 : _leafList.Count); }
        }

        private bool isSetOperator(Operator op)
        {
            switch (op)
            {
                case Operator.Union:
                case Operator.Intersect:
                case Operator.Except:
                    return true;
                default:
                    return false;
            }
        }

        private bool isSetOperatorNode(OperatorGroup node)
        {
            if (node == null || node.data == null || node.data.type != NodeType.Operator)
            {
                return false;
            }

            return isSetOperator(node.data.op);
        }

        public List<string> GetLeafList()
        {
            if (_leafList == null || _leafList.Count == 0)
                return null;

            List<string> leafList = new List<string>();

            //foreach (FieldValuePair item in _leafList)
            //{
            //    leafList.Add(item.ToString());
            //}

            leafList = _leafList.ConvertAll<string>(FieldValuePair.ToString);

            return leafList;
        }

        public FullTextSearchTree()
        {
            _leafList = new List<FieldValuePair>();
            _root = new OperatorGroup();
            _currentNode = _root;
            DefaultOperator = Operator.And;
            _inorderNodeList = new List<NodeData>();
        }

        public FullTextSearchTree(string TableName) : this()
        {
            _TableName = TableName;
        }

        public class FieldValuePair
        {
            public string FieldName;
            public string Value;
            public bool bQuoteValue;    // ha true, az �tvett Value eg�sze id�z�jelek k�z�tt �rtend�

            public override string ToString()
            {
                return this.FieldName + FieldValueSeparator + this.Value;
            }

            public static string ToString(FieldValuePair fieldvaluepair)
            {
                if (fieldvaluepair == null)
                {
                    return "";
                }
                else
                {
                    return fieldvaluepair.ToString();
                }
            }
        }

        private class OperatorGroup
        {
            public OperatorGroup nodeLeft;
            public OperatorGroup nodeRight;
            public OperatorGroup nodeParent;
            public NodeData data;

            public OperatorGroup()
            {
                nodeLeft = null;
                nodeRight = null;
                nodeParent = null;
                data = new NodeData();
            }
        }

        //[StructLayout(LayoutKind.Explicit)]
        public class NodeData
        {
            //[FieldOffset(0)]
            public FieldValuePair field;

            //[FieldOffset(0)]
            public Operator op;

            public NodeType type;

            public NodeData()
            {
                type = NodeType.Unknown;
                op = Operator.None;
            }
        }

        /// <summary>
        /// Returns logical blocks of leaves from the inorder representation (logical blocks are operand sequences that separated by set operators).
        /// The block items are strings consisting of fieldname and value pairs, separated by pipe ('|') character
        /// </summary>
        /// <returns></returns>
        public List<List<string>> GetLogicallySeparatedLeafList()
        {
            if (_leafList == null || _leafList.Count == 0)
                return null;
            if (_inorderNodeList == null || _bIsInorderNodeListSet == false || _inorderNodeList.Count == 0)
                return null;

            List<List<string>> leafList = new List<List<string>>();

            foreach (NodeData node in _inorderNodeList)
            {
                if (node.type == NodeType.Operator)
                {
                    if (isSetOperator(node.op))
                    {
                        List<string> leafBlock = new List<string>();
                        leafList.Add(leafBlock);
                    }
                }
                else if (node.type == NodeType.Leaf)
                {
                    // add leaf
                    if (leafList.Count == 0)
                    {
                        List<string> leafBlock = new List<string>();
                        leafList.Add(leafBlock);
                    }

                    leafList[leafList.Count - 1].Add(node.field.ToString());
                }
                else if (node.type == NodeType.Unknown)
                {
                    // do nothing
                }

            }

            return leafList;
        }

        public void SetInorderNodeList()
        {
            // ki�r�tj�k
            if (_inorderNodeList == null)
            {
                _inorderNodeList = new List<NodeData>();
            }
            else
            {
                _inorderNodeList.Clear();
            }
            inorderReaderNodeData(_inorderNodeList, this._root);
            _bIsInorderNodeListSet = true;
        }

         /// <summary>
        /// R�szfa hozz�ad�sa a f�hoz az aktu�lis illeszt�si poz�ci�n.
        /// </summary>
        /// <param name="FullTextSearchTree">A hozz�adand� r�szfa</param>
        public static FullTextSearchTree MergeFTSTrees(FullTextSearchTree firstTree, FullTextSearchTree secondTree)
        {
            return MergeFTSTrees(firstTree, secondTree, Operator.Intersect);
        }

        /// <summary>
        /// R�szfa hozz�ad�sa a f�hoz az aktu�lis illeszt�si poz�ci�n.
        /// </summary>
        /// <param name="FullTextSearchTree">A hozz�adand� r�szfa</param>
        public static FullTextSearchTree MergeFTSTrees(FullTextSearchTree firstTree, FullTextSearchTree secondTree, Operator op)
        {
            if (firstTree == null && secondTree == null) return null;
            if (firstTree == null) return secondTree;
            if (secondTree == null) return firstTree;

            if (firstTree.TableName != secondTree.TableName)
            {
                throw new FullTextSearchException("[FTS-04]", "TableNames must be the same when merging fts trees!");
            }

            if (firstTree.SelectFields != secondTree.SelectFields)
            {
                throw new FullTextSearchException("[FTS-04]", "SelectFields must be the same when merging fts trees!");
            }

            FullTextSearchTree FTSTreeMerged = new FullTextSearchTree(firstTree.TableName);
            FTSTreeMerged.SelectFields = firstTree.SelectFields;

            if (firstTree.LeafCount > 0)
            {
                inorderSubTreeAppender(FTSTreeMerged, firstTree, firstTree._root);
                if (secondTree.LeafCount > 0)
                {
                    FTSTreeMerged.AddOperator(op);
                }
            }

            if (secondTree.LeafCount > 0)
            {
                inorderSubTreeAppender(FTSTreeMerged, secondTree, secondTree._root);
            }

            return FTSTreeMerged;

        }

        private static void inorderSubTreeAppender(FullTextSearchTree firstTree, FullTextSearchTree secondTree, OperatorGroup subtreeCurrentNode)
        {
            if (secondTree == null) return;
            if (subtreeCurrentNode == null) return;

            if (firstTree == null)
            {
                firstTree = new FullTextSearchTree();
            }

            inorderSubTreeAppender(firstTree, secondTree, subtreeCurrentNode.nodeLeft);

            if (subtreeCurrentNode.data.type == NodeType.Operator)
            {
                firstTree.AddOperator(subtreeCurrentNode.data.op);
            }
            else if (subtreeCurrentNode.data.type == NodeType.Leaf)
            {
                firstTree.AddFieldValuePair(subtreeCurrentNode.data.field.FieldName, subtreeCurrentNode.data.field.Value, subtreeCurrentNode.data.field.bQuoteValue);
            }
            else if (subtreeCurrentNode.data.type == NodeType.Unknown)
            {
                // do nothing
            }

            inorderSubTreeAppender(firstTree, secondTree, subtreeCurrentNode.nodeRight);

        }

        /// <summary>
        /// �j keres�felt�tel hozz�ad�sa a f�hoz.
        /// </summary>
        /// <param name="FieldName">Keres�mez� neve</param>
        /// <param name="Value">Keres�mez� �rt�ke</param>
        /// <param name="bQuoteValue">Az �tvett �rt�k eg�sze id�z�jelek k�z�tt �rtend�</param>
        public void AddFieldValuePair(string FieldName, string Value, bool bQuoteValue)
        {
            if (String.IsNullOrEmpty(FieldName))
            {
                throw new FullTextSearchException("[FTS-02]", "FieldName cannot be empty.");
            }

            if (String.IsNullOrEmpty(Value))
            {
                throw new FullTextSearchException("[FTS-02]", "Value cannot be empty.");
            }

            FieldValuePair fieldvaluepair = new FieldValuePair();
            fieldvaluepair.FieldName = FieldName;
            fieldvaluepair.Value = Value;
            fieldvaluepair.bQuoteValue = bQuoteValue;

            _leafList.Add(fieldvaluepair);

            if (_currentNode.data.type == NodeType.Unknown)  // csak gy�k�r lehet
            {
                _currentNode.data.field = fieldvaluepair;
                _currentNode.data.type = NodeType.Leaf;
            }
            else if (_currentNode.data.type == NodeType.Leaf)
            {
                // ha levelet lev�l k�vet, gener�lunk f�l�j�k egy default oper�tor csoportot
                // bal gyereke a jelenlegi lev�l, jobb gyerek egy �j lev�l, current node az �j oper�tor csoport
                // gy�k�r az �j oper�tor csoport
                AddOperator(DefaultOperator);

                OperatorGroup nodeRight = new OperatorGroup();
                nodeRight.data.type = NodeType.Leaf;
                nodeRight.data.field = fieldvaluepair;
                nodeRight.nodeParent = _currentNode;

                _currentNode.nodeRight = nodeRight;

            }
            else if (_currentNode.data.type == NodeType.Operator)
            {
                if (_currentNode.nodeLeft == null)
                {
                    OperatorGroup nodeLeft = new OperatorGroup();
                    nodeLeft.data.type = NodeType.Leaf;
                    nodeLeft.data.field = fieldvaluepair;
                    nodeLeft.nodeParent = _currentNode;
                    _currentNode.nodeLeft = nodeLeft;
                }
                else if (_currentNode.nodeRight == null)
                {
                    OperatorGroup nodeRight = new OperatorGroup();
                    nodeRight.data.type = NodeType.Leaf;
                    nodeRight.data.field = fieldvaluepair;
                    nodeRight.nodeParent = _currentNode;
                    _currentNode.nodeRight = nodeRight;
                }
                else
                {
                    // a jelenlegi oper�tornak m�r mindk�t operandusa adott, gener�lunk f�l� egy �j oper�tor csoportot,
                    // ez lesz a gy�k�r �s a current node is
                    AddOperator(DefaultOperator);

                    OperatorGroup nodeRight = new OperatorGroup();
                    nodeRight.data.type = NodeType.Leaf;
                    nodeRight.data.field = fieldvaluepair;
                    nodeRight.nodeParent = _currentNode;

                    _currentNode.nodeRight = nodeRight;
                }
            }

            // ha m�dosul a fa, t�r�lj�k a list�t!
            _inorderNodeList.Clear();
            _bIsInorderNodeListSet = false;
        }

        public void AddFieldValuePair(string FieldName, string Value)
        {
            AddFieldValuePair(FieldName, Value, false);
        }

        public void AddOperator(Operator op)
        {
            if (_currentNode.data.type == NodeType.Unknown)  // csak gy�k�r lehet
            {
                _currentNode.data.op = op;
                _currentNode.data.type = NodeType.Operator;
            }
            else if (_currentNode.data.type == NodeType.Leaf)   // csak gy�k�r lehet, egy�bk�nt mindig az oper�torra mutatunk
            {
                // ha levelet oper�tor k�vet, gener�lunk f�l�j�k egy oper�tor csoportot
                // bal gyereke a jelenlegi lev�l, jobb gyerek null, current node az �j oper�tor csoport
                // gy�k�r az �j oper�tor csoport
                OperatorGroup newOperatorGroup = new OperatorGroup();
                newOperatorGroup.data.type = NodeType.Operator;
                newOperatorGroup.data.op = op;
                newOperatorGroup.nodeLeft = _currentNode;

                _currentNode.nodeParent = newOperatorGroup;

                newOperatorGroup.nodeRight = null;
                _currentNode = newOperatorGroup;
                _root = newOperatorGroup;
            }
            else if (_currentNode.data.type == NodeType.Operator)
            {
                // ha oper�tort oper�tor k�vet, de nem halmazoper�tor
                // ha a bal gyerek nem l�tezik, ez lesz a bal gyerek, �s az lesz a current node
                // TODO:??? bal gyereknek nem lenne szabad �resnek lennie!!
                if (!isSetOperator(op))
                {
                    if (_currentNode.nodeLeft == null)
                    {
                        throw new FullTextSearchException("[FTS-03]", "Left operand of current operator missing. Cannot add new operator.");
                    }
                    else if (_currentNode.nodeRight == null)
                    {
                        // ha a bal gyerek l�tezik, a jobb gyerek null, akkor ez lesz az �j jobb gyerek �s a current node
                        OperatorGroup nodeRight = new OperatorGroup();
                        nodeRight.data.type = NodeType.Operator;
                        nodeRight.data.op = op;
                        nodeRight.nodeParent = _currentNode;
                        _currentNode.nodeRight = nodeRight;
                        _currentNode = _currentNode.nodeRight;
                    }
                    else
                    {
                        // a jelenlegi oper�tornak m�r mindk�t operandusa adott,
                        // ha az aktu�lis node nem halmazoper�tor
                        // akkor vagy megtal�lunk egy sz�l�t,
                        // akinek m�g nincs jobb gyereke, vagy
                        // gener�lunk f�l� egy �j oper�tor csoportot,
                        // ez lesz a gy�k�r �s a current node is, bal gyereke az eddigi oper�tor

                        while (_currentNode.nodeParent != null && _currentNode.nodeRight != null)
                        {
                            _currentNode = _currentNode.nodeParent;
                        }

                        if (_currentNode.nodeRight == null)
                        {
                            // ha a bal gyerek l�tezik, a jobb gyerek null, akkor ez lesz az �j jobb gyerek �s a current node
                            OperatorGroup nodeRight = new OperatorGroup();
                            nodeRight.data.type = NodeType.Operator;
                            nodeRight.data.op = op;
                            nodeRight.nodeParent = _currentNode;
                            _currentNode.nodeRight = nodeRight;
                            _currentNode = _currentNode.nodeRight;
                        }
                        else
                        {
                            OperatorGroup newOperatorGroup = new OperatorGroup();
                            newOperatorGroup.data.type = NodeType.Operator;
                            newOperatorGroup.data.op = op;
                            _currentNode.nodeParent = newOperatorGroup;
                            newOperatorGroup.nodeLeft = _currentNode;

                            newOperatorGroup.nodeRight = null;
                            _currentNode = newOperatorGroup;
                            _root = newOperatorGroup;
                        }
                    }

                }
                else
                {
                    // halmazoper�tort mindig gy�k�rbe tesz�nk
                    // ha az aktu�lis node is halmazoper�tor �s valamelyik gyerek �res, akkor hiba
                    if (isSetOperatorNode(_currentNode) && (_currentNode.nodeRight == null || _currentNode.nodeLeft == null))
                    {
                        throw new FullTextSearchException("[FTS-03]", "Operand of current set operator missing. Cannot add new set operator.");
                    }
                    else
                    {
                        if (_currentNode == _root)
                        {
                            OperatorGroup newOperatorGroup = new OperatorGroup();
                            newOperatorGroup.data.type = NodeType.Operator;
                            newOperatorGroup.data.op = op;
                            _currentNode.nodeParent = newOperatorGroup;
                            newOperatorGroup.nodeLeft = _currentNode;

                            newOperatorGroup.nodeRight = null;
                            _currentNode = newOperatorGroup;
                            _root = newOperatorGroup;
                        }
                        else
                        {
                            _currentNode = _root;
                            AddOperator(op);
                        }
                    }
                }
            }

            // ha m�dosul a fa, t�r�lj�k a list�t!
            _inorderNodeList.Clear();
            _bIsInorderNodeListSet = false;
        }

        private class NodeTransformer
        {
            public static string GetSelectCondition(string TableName, string SelectFields)
            {
                return "SELECT " + SelectFields + " FROM " + TableName + " WHERE ";
            }

            public static string GetContainsCondition(OperatorGroup node, string TableName)
            {
                string nodetext = "";
                if (node.data.type == NodeType.Leaf)
                {
                    string value;
                    if (node.data.field.bQuoteValue == true)
                    {
                        value = "\"" + node.data.field.Value + "\"";
                    }
                    else
                    {
                        value = new SQLContainsCondition(node.data.field.Value).Normalized;
                    }

                    nodetext = "contains( " + (String.IsNullOrEmpty(TableName) ? "" : (TableName + ".")) + node.data.field.FieldName + ", '" + value + "')";
                }
                else if (node.data.type == NodeType.Operator)
                {
                    switch (node.data.op)
                    {
                        case Operator.And:
                            nodetext = " AND ";
                            break;
                        case Operator.Or:
                            nodetext = " OR ";
                            break;
                        case Operator.AndNot:
                            nodetext = " AND NOT ";
                            break;
                        case Operator.Union:
                            nodetext = " UNION ";
                            break;
                        case Operator.Intersect:
                            nodetext = " INTERSECT ";
                            break;
                        case Operator.Except:
                            nodetext = " EXCEPT ";
                            break;
                        case Operator.None:
                        default:
                            // TODO: Exception
                            nodetext = " ";
                            break;
                    }
                }
                return nodetext;
            }
        }


        private void inorderReaderNodeData(List<NodeData> listNodeData, OperatorGroup rootNode)
        {
            if (listNodeData == null || rootNode == null) return;

            inorderReaderNodeData(listNodeData, rootNode.nodeLeft);

            listNodeData.Add(rootNode.data);

            inorderReaderNodeData(listNodeData, rootNode.nodeRight);
        }

        private void inorderReader(OperatorGroup rootNode, ref string condition)
        {
            string conditionLeft = "";
            string conditionRight = "";
            string conditionThis = "";

            if (rootNode.nodeLeft != null)
                inorderReader(rootNode.nodeLeft, ref conditionLeft);

            conditionThis = NodeTransformer.GetContainsCondition(rootNode, TableName);

            if (isSetOperatorNode(rootNode))
            {
                conditionThis += NodeTransformer.GetSelectCondition(TableName, SelectFields);
            }

            if (rootNode.nodeRight != null)
                inorderReader(rootNode.nodeRight, ref conditionRight);

            condition += conditionLeft + conditionThis + conditionRight;
        }

        private bool CheckTree(OperatorGroup rootNode)
        {
            if (rootNode.data.type == NodeType.Unknown)
                return false;

            if (rootNode.data.type == NodeType.Leaf)
                return true;

            if (rootNode.data.type == NodeType.Operator)
            {
                if (rootNode.nodeLeft == null ||rootNode.nodeRight == null )
                {
                    return false; 
                }

                if (CheckTree(rootNode.nodeLeft) == false)
                    return false;

                if (CheckTree(rootNode.nodeRight) == false)
                    return false;
            }

            return true;
        }

        public bool CheckTree()
        {
            return (CheckTree(_root));

        }

        public string TransformToFTSContainsConditions()
        {
            string condition = "";
            inorderReader(_root, ref condition);
            if (!String.IsNullOrEmpty(condition))
            {
                condition = NodeTransformer.GetSelectCondition(TableName, SelectFields) + condition;
            }
            return condition;
        }

    }

    public sealed class SQLContainsCondition
    {

        private string _strCondition;
        private string _strNormalized;
        private List<string> _lstSearchTerms;

        private SQLContainsCondition() { }

        public SQLContainsCondition(string condition)
        {

            _strCondition = condition;

            ConditionParser parser = new ConditionParser(condition);

            _strNormalized = parser.RootExpression.ToString();

            _lstSearchTerms = new List<string>();

            foreach (ConditionExpression exp in parser.RootExpression)
            {
                if (exp.IsTerm && exp.Term.Length != 0)
                {
                    _lstSearchTerms.Add(exp.Term);
                }
            }

        }

        public string Condition
        {
            get { return _strCondition; }
        }

        public string Normalized
        {
            get { return _strNormalized; }
        }

        public string[] SearchTerms
        {
            get { return _lstSearchTerms.ToArray(); }
        }

        private class Token {
            private StringBuilder _sbToken;

            public Token() {
                _sbToken = new StringBuilder();
            }

            public Token(string token)
            {
                _sbToken = new StringBuilder(token);
            }

            public Token(char c): this(c.ToString()) { }

            public string Value {
                get { return _sbToken.ToString(); }
                set { _sbToken = new StringBuilder(value); }
            }

            public void Reset() {
                _sbToken = new StringBuilder();
            }

            public void Append(char c) {
                _sbToken.Append(c);
            }

            public override string ToString()
            {
                return _sbToken.ToString();
            }

        }

        private sealed class ConditionParser
        {
            private Token _token;
            private ConditionOperator _copLastOp;
            private bool _isQuoted;

            private ConditionExpression _cexpRootExpression;
            private ConditionExpression _cexpCurrentExpression;

            private ConditionParser() { }

            public ConditionParser(String condition)
            {

                ConditionStream conditionStream = new ConditionStream(condition);

                _cexpRootExpression = new ConditionExpression();
                _cexpCurrentExpression = _cexpRootExpression;

                Reset();

                while (conditionStream.Read())
                {
                    if (ConditionOperator.IsSymbol(conditionStream.CurrentChar))
                    {
                        PutToken();
                        _token.Value = conditionStream.CurrentChar.ToString();
                        PutToken();
                        continue;
                    }
                    switch (conditionStream.CurrentChar)
                    {
                        case ' ': PutToken(); continue;
                        case '(': PushExpression(); continue;
                        case ')': PopExpression(); continue;
                        case '"':
                            PutToken();
                            _isQuoted = true;
                            _token.Value = conditionStream.ReadQuoted();
                            PutToken();
                            _isQuoted = false;
                            continue;
                    }

                    _token.Append(conditionStream.CurrentChar);
                }
                PutToken();

            }

            public ConditionExpression RootExpression
            {
                get { return _cexpRootExpression; }
            }

            private void Reset()
            {
                if (_token == null)
                {
                    _token = new Token();
                }
                else
                {
                    _token.Reset();
                }
                _copLastOp = ConditionOperator.And;
            }

            private void PushExpression()
            {
                PutToken();
                _cexpCurrentExpression = _cexpCurrentExpression.AddSubexpression(_copLastOp);
            }

            private void PopExpression()
            {
                PutToken();
                if (!_cexpCurrentExpression.IsRoot)
                {
                    _cexpCurrentExpression = _cexpCurrentExpression.Parent;
                }
                Reset();
            }

            private void PutToken()
            {
                if (!_isQuoted && ConditionOperator.TryParse(_token.Value, ref _copLastOp))
                {
                    // operator
                    _token.Reset();
                }
                else
                {
                    // term
                    string term = _token.Value;
                    if (_isQuoted)
                    {
                        term = Regex.Replace(term.Trim(), @"[ ]{2,}", " ");
                    }

                    if ((term.Length == 0) && !_isQuoted) return;

                    _cexpCurrentExpression.AddTerm(_copLastOp, term);

                    Reset();
                }

            }

        }

        private sealed class ConditionStream
        {
            private string _strCondition;
            private int _iIndex;

            private ConditionStream() { }

            public ConditionStream(string condition)
            {
                _strCondition = condition;
                _iIndex = -1;
            }

            public char CurrentChar
            {
                get { return (Eos() || Bos()) ? (char)0 : _strCondition[_iIndex]; }
            }

            public bool Read()
            {
                _iIndex++;
                if (Eos()) return false;
                return true;
            }

            public string ReadQuoted()
            {
                StringBuilder sb = new StringBuilder();
                while (Read())
                {
                    if (CurrentChar.Equals('"'))
                    {
                        if ((_iIndex + 1) == _strCondition.Length)
                        {
                            _iIndex = _strCondition.Length;
                            return sb.ToString();
                        }
                        char nextChar = _strCondition[_iIndex + 1];
                        if ((nextChar == ' ') || (nextChar == ')') || (nextChar == '(') || (ConditionOperator.IsSymbol(nextChar)))
                        {
                            return sb.ToString();
                        }
                        if (nextChar == '"')
                        {
                            _iIndex++;
                        }
                    }
                    sb.Append(CurrentChar);
                }
                return sb.ToString();
            }

            private bool Bos()
            {
                return (_iIndex < 0);
            }

            private bool Eos()
            {
                return (_iIndex >= _strCondition.Length);
            }

        }

        private sealed class ConditionExpression : IEnumerable<ConditionExpression>
        {
            private int _iIndex;
            private ConditionExpression _cexpParent;
            private ConditionOperator _copOperator;
            private string _strTerm;
            private List<ConditionExpression> _lstSubexpressions;
            private bool _isTerm;
            private bool _isPhrase;
            private bool _isPrefix;

            public ConditionExpression()
            {
                _strTerm = String.Empty;
                _lstSubexpressions = new List<ConditionExpression>();
            }

            private ConditionExpression(ConditionExpression parent, ConditionOperator op)
                : this()
            {
                _iIndex = parent._lstSubexpressions.Count;
                _cexpParent = parent;
                _copOperator = op;
            }

            private ConditionExpression(ConditionExpression parent, ConditionOperator op, string term)
                : this(parent, op)
            {

                _strTerm = term;

                _isTerm = true;

                _isPhrase = (term.IndexOf(' ') != -1);
                int prefixIndex = term.IndexOf('*');
                _isPrefix = (prefixIndex != -1);

                if (!_isPrefix) return;

                if (!_isPhrase)
                {
                    if (prefixIndex == (term.Length - 1)) return;
                    _strTerm = (prefixIndex == 0) ? "" : term.Remove(prefixIndex + 1);
                    return;
                }

                term = Regex.Replace(term, @"(\*[^ ]+)|(\*)", "");
                term = Regex.Replace(term.Trim(), @"[ ]{2,}", " ");
                _strTerm = term + "*";

            }

            public ConditionExpression Parent
            {
                get { return _cexpParent; }
            }

            public bool IsRoot
            {
                get { return (_cexpParent == null); }
            }

            public bool IsLastSubexpression
            {
                get { return (IsRoot || (!IsRoot && (_iIndex == (_cexpParent._lstSubexpressions.Count - 1)))); }
            }

            public ConditionOperator Operator
            {
                get { return _copOperator; }
            }

            public bool IsTerm
            {
                get { return _isTerm; }
            }

            public bool HasSubexpressions
            {
                get { return _lstSubexpressions.Count > 0; }
            }

            public ConditionExpression LastSubexpression
            {
                get { return (HasSubexpressions) ? _lstSubexpressions[_lstSubexpressions.Count - 1] : null; }
            }

            public ConditionExpression AddSubexpression(ConditionOperator op)
            {

                ConditionOperator newOp = op;
                if (op == ConditionOperator.Near)
                {
                    newOp = ConditionOperator.And;
                }

                ConditionExpression exp = new ConditionExpression(this, newOp);

                _lstSubexpressions.Add(exp);

                return exp;

            }

            public void AddTerm(ConditionOperator op, string term)
            {
                ConditionOperator newOp = op;
                if (!HasSubexpressions)
                {
                    newOp = ConditionOperator.And;
                }
                else
                {
                    if (op == ConditionOperator.Near)
                    {
                        if (LastSubexpression.HasSubexpressions)
                        {
                            newOp = ConditionOperator.And;
                        }
                    }
                }

                ConditionExpression exp = new ConditionExpression(this, newOp, term);

                _lstSubexpressions.Add(exp);

            }

            public string Term
            {
                get { return _strTerm; }
            }

            public override string ToString()
            {

                StringBuilder sb = new StringBuilder();

                if (IsTerm)
                {
                    sb.Append(String.Format("\"{0}\"", _strTerm.Replace("\"", "\"\"").Replace("'","''")));
                }
                else
                {

                    if (!IsRoot) sb.Append("(");

                    if (!HasSubexpressions)
                    {
                        sb.Append("\"\"");  // to avoid 'Null or empty full-text predicate' exception.
                    }
                    else
                    {
                        for (int i = 0; i < _lstSubexpressions.Count; i++)
                        {
                            ConditionExpression exp = _lstSubexpressions[i];
                            if (i > 0)
                            {
                                sb.Append(String.Format(" {0} ", exp._copOperator.ToString()));
                            }
                            sb.Append(exp.ToString());
                        }
                    }

                    if (!IsRoot) sb.Append(")");

                }

                return sb.ToString();

            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public IEnumerator<ConditionExpression> GetEnumerator()
            {
                foreach (ConditionExpression subexp in _lstSubexpressions)
                {
                    yield return subexp;
                    if (subexp.HasSubexpressions)
                    {
                        foreach (ConditionExpression subsubexp in subexp)
                        {
                            yield return subsubexp;
                        }
                    }
                }
            }

        }

        private struct ConditionOperator
        {
            private const char symbolAnd1 = '&';
            private const char symbolAnd2 = '+';
            private const char symbolAnd3 = ',';
            private const char symbolAnd4 = ';';
            private const char symbolAndNot1 = '\\';
            private const char symbolAndNot2 = '!';
            private const char symbolOr1 = '|';
            private const char symbolNear1 = '~';

            private const int opAnd = 0;
            private const int opAndNot = 1;
            private const int opOr = 2;
            private const int opNear = 3;

            public static ConditionOperator And = new ConditionOperator(opAnd);
            public static ConditionOperator AndNot = new ConditionOperator(opAndNot);
            public static ConditionOperator Or = new ConditionOperator(opOr);
            public static ConditionOperator Near = new ConditionOperator(opNear);

            private int _iValue;

            private ConditionOperator(int value)
            {
                _iValue = value;
            }

            public override string ToString()
            {
                switch (_iValue)
                {
                    case opAndNot: return "and not";
                    case opOr: return "or";
                    case opNear: return "near";
                    default:
                        return "and";
                }
            }

            public static bool IsSymbol(char c)
            {
                switch (c)
                {
                    case symbolAnd1: case symbolAnd2: case symbolAnd3: case symbolAnd4:
                        return true;
                    case symbolAndNot1: case symbolAndNot2:
                        return true;
                    case symbolOr1:
                        return true;
                    case symbolNear1:
                        return true;
                    default:
                        return false;
                }
                
            }

            public static bool TryParse(string s, ref ConditionOperator op)
            {

                if (s.Length == 1)
                {
                    switch (s[0])
                    {
                        case symbolAnd1: case symbolAnd2: case symbolAnd3: case symbolAnd4:
                            op = ConditionOperator.And;
                            return true;
                        case symbolAndNot1:
                            op = ConditionOperator.AndNot;
                            return true;
                        case symbolAndNot2:
                            if (op != ConditionOperator.And) return false;
                            op = ConditionOperator.AndNot;
                            return true;
                        case symbolOr1:
                            op = ConditionOperator.Or;
                            return true;
                        case symbolNear1:
                            op = ConditionOperator.Near;
                            return true;
                        default:
                            return false;
                    }
                    
                }

                if (s.Equals(ConditionOperator.And.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    op = ConditionOperator.And;
                    return true;
                }
                if (s.Equals("not", StringComparison.OrdinalIgnoreCase) && (op == ConditionOperator.And))
                {
                    op = ConditionOperator.AndNot;
                    return true;
                }
                if (s.Equals(ConditionOperator.Or.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    op = ConditionOperator.Or;
                    return true;
                }
                if (s.Equals(ConditionOperator.Near.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    op = ConditionOperator.Near;
                    return true;
                }

                return false;

            }
            // operator overloads
            public static bool operator ==(ConditionOperator cop1, ConditionOperator cop2)
            {
                return cop1.Equals(cop2);
            }
            public static bool operator !=(ConditionOperator cop1, ConditionOperator cop2)
            {
                return !cop1.Equals(cop2);
            }
            public override bool Equals(object obj)
            {
                return (obj is ConditionOperator) && (Equals((ConditionOperator)obj));
            }
            private bool Equals(ConditionOperator cop)
            {
                return (_iValue == cop._iValue);
            }
            public override int GetHashCode()
            {
                return _iValue.GetHashCode();
            }

        }

    }

}
