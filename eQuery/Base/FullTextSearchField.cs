using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlTypes;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// Full text indexes keres�shez haszn�latos oszt�ly
    /// </summary>
    [Serializable]
    public class FullTextSearchField
    {
        public FullTextSearchField()
        {

        }

        private string filter = "";
        /// <summary>
        /// A sz�r�si felt�tel megad�s�ra
        /// </summary>
        public string Filter
        {
            get { return filter; }
            set { filter = value; }
        }
	
    }
}
