using System;

using log4net;
using log4net.Config;

// A n�vt�r �tk�z�sek elker�l�se miatt egy egy�rtelm� n�vt�r
// A m�shol haszn�ltakhoz k�pest m�dos�tott, egyszer�s�tett Logger oszt�ly,
// mivel itt az �ltal�nosan haszn�lt Business Object ill. eUtility oszt�lyok (k�rk�r�s hivatkoz�s lenne!) nem el�rhet�ek
namespace Contentum.eQuery.Utility
{
	public sealed class Logger
	{
        public static string LOG_START = ">>>";
        public static string LOG_END = "<<<";
		private static Logger instance = new Logger();
		private static readonly ILog log = LogManager.GetLogger(typeof(Logger));
		
		public static Logger Instance {
			get { return instance; }
		}
		
		private Logger() { 
			log4net.Config.XmlConfigurator.Configure();
		}
		
		public static ILog GetLogger
		{
			get { return log; }
        }

        private static string Append(string p_msg, string p_errorcode,
                                     System.Exception p_ex)
        {
            string errorCode = (p_errorcode == null) ? "NULL" : p_errorcode;
            string errorMsg = (p_msg == null) ? "NULL" : p_msg;
            string exMsg     = (p_ex == null)        ? "NULL" : p_ex.Message;

            return "(ErrorCode:'" + errorCode + "'; ErrorMessage:'" + errorMsg + "') " + exMsg;
        }

        #region Debug

            public static bool IsDebugEnabled()
            {
                return Logger.log.IsDebugEnabled;
            }

            public static void Debug(string p_msg)
		    {
                Logger.Debug(p_msg, null, null);
		    }

		    public static void Debug(string p_msg, 
                                     System.Exception p_ex)
		    {
                Logger.Debug(p_msg, null, p_ex);
		    }

            public static void Debug(string p_msg, string p_errorcode,
                                     System.Exception p_ex)
            {
                if (Logger.IsDebugEnabled())
                {
                    Logger.log.Debug(Append(p_msg, p_errorcode, p_ex));
                }
            }

            public static void DebugStart()
            {
                Logger.DebugStart("");
            }

            public static void DebugStart(string p_msg)
            {
                Logger.log.Debug(Logger.LOG_START+p_msg);
            }

            public static void DebugEnd()
            {
                Logger.DebugEnd("");
            }

            public static void DebugEnd(string p_msg)
            {
                Logger.log.Debug(Logger.LOG_END + p_msg);
            }

        #endregion

        #region Info

            public static bool IsInfoEnabled()
            {
                return Logger.log.IsInfoEnabled;
            }

            public static void Info(string p_msg)
            {
                Logger.Info(p_msg, null, null);
            }

            public static void Info(string p_msg,
                                     System.Exception p_ex)
            {
                Logger.Info(p_msg, null, p_ex);
            }

            public static void Info(string p_msg, string p_errorcode,
                                     System.Exception p_ex)
            {
                if (Logger.IsInfoEnabled())
                {
                    Logger.log.Info(Append(p_msg, p_errorcode, p_ex));
                }
            }

            public static void InfoStart()
            {
                Logger.InfoStart("");
            }

            public static void InfoStart(string p_msg)
            {
                Logger.log.Info(Logger.LOG_START + p_msg);
            }

            public static void InfoEnd()
            {
                Logger.InfoEnd("");
            }

            public static void InfoEnd(string p_msg)
            {
                Logger.log.Info(Logger.LOG_END + p_msg);
            }

        #endregion

        #region Warn

            public static bool IsWarnEnabled()
            {
                return Logger.log.IsWarnEnabled;
            }

            public static void Warn(string p_msg)
            {
                Logger.Warn(p_msg, null, null);
            }

            public static void Warn(string p_msg,
                                     System.Exception p_ex)
            {
                Logger.Warn(p_msg, null, p_ex);
            }

            public static void Warn(string p_msg, string p_errorcode,
                                     System.Exception p_ex)
            {
                if (Logger.IsWarnEnabled())
                {
                    Logger.log.Warn(Append(p_msg, p_errorcode, p_ex));
                }
            }

            public static void WarnStart()
            {
                Logger.WarnStart("");
            }

            public static void WarnStart(string p_msg)
            {
                Logger.log.Warn(Logger.LOG_START + p_msg);
            }

            public static void WarnEnd()
            {
                Logger.WarnEnd("");
            }

            public static void WarnEnd(string p_msg)
            {
                Logger.log.Warn(Logger.LOG_END + p_msg);
            }

        #endregion

        #region Error

            public static bool IsErrorEnabled()
            {
                return Logger.log.IsErrorEnabled;
            }

            public static void Error(string p_msg)
            {
                Logger.Error(p_msg, null, null);
            }

            public static void Error(string p_msg,
                                     System.Exception p_ex)
            {
                Logger.Error(p_msg, null, p_ex);
            }

            public static void Error(string p_msg, string p_errorcode,
                                     System.Exception p_ex)
            {
                if (Logger.IsErrorEnabled())
                {
                    Logger.log.Error(Append(p_msg, p_errorcode, p_ex));
                }
            }

            public static void ErrorStart()
            {
                Logger.ErrorStart("");
            }

            public static void ErrorStart(string p_msg)
            {
                Logger.log.Error(Logger.LOG_START + p_msg);
            }

            public static void ErrorEnd()
            {
                Logger.ErrorEnd("");
            }

            public static void ErrorEnd(string p_msg)
            {
                Logger.log.Error(Logger.LOG_END + p_msg);
            }

        #endregion

        #region Fatal

            public static bool IsFatalEnabled()
            {
                return Logger.log.IsFatalEnabled;
            }

            public static void Fatal(string p_msg)
            {
                Logger.Fatal(p_msg, null, null);
            }

            public static void Fatal(string p_msg,
                                     System.Exception p_ex)
            {
                Logger.Fatal(p_msg, null, p_ex);
            }

            public static void Fatal(string p_msg, string p_errorcode,
                                     System.Exception p_ex)
            {
                if (Logger.IsFatalEnabled())
                {
                    Logger.log.Fatal(Append(p_msg, p_errorcode, p_ex));
                }
            }

            public static void FatalStart()
            {
                Logger.FatalStart("");
            }

            public static void FatalStart(string p_msg)
            {
                Logger.log.Fatal(Logger.LOG_START + p_msg);
            }

            public static void FatalEnd()
            {
                Logger.FatalEnd("");
            }

            public static void FatalEnd(string p_msg)
            {
                Logger.log.Fatal(Logger.LOG_END + p_msg);
            }

        #endregion
    }
}
