using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_CsatolmanyokSearch
    {
        private void Init_ManualFields()
        {
            _Manual_Ugyiratok_Id.Name = "EREC_UgyUgyiratok.Id";
            _Manual_Ugyiratok_Id.Type = "Guid";

            _Manual_Dokumentum_Nev.Name = "KRT_Dokumentumok.FajlNev";
            _Manual_Dokumentum_Nev.Type = "String";

            _Manual_Sablon_Azonosito.Name = "KRT_Dokumentumok.SablonAzonosito";
            _Manual_Sablon_Azonosito.Type = "String";

            _Manual_Dokumentum_BarCode.Name = "KRT_Dokumentumok.BarCode";
            _Manual_Dokumentum_BarCode.Type = "String";

            _Manual_Dokumentum_TartalomHash.Name = "KRT_Dokumentumok.TartalomHash";
            _Manual_Dokumentum_TartalomHash.Type = "String";

            _Manual_Dokumentum_KivonatHash.Name = "KRT_Dokumentumok.KivonatHash";
            _Manual_Dokumentum_KivonatHash.Type = "String";
        }

        private Field _Manual_Ugyiratok_Id = new Field();
        public Field Manual_Ugyiratok_Id
        {
            get { return _Manual_Ugyiratok_Id; }
            set { _Manual_Ugyiratok_Id = value; }
        }

        private Field _Manual_Dokumentum_Nev = new Field();
        public Field Manual_Dokumentum_Nev
        {
            get { return _Manual_Dokumentum_Nev; }
            set { _Manual_Dokumentum_Nev = value; }
        }

        private Field _Manual_Sablon_Azonosito = new Field();
        public Field Manual_Sablon_Azonosito
        {
            get { return _Manual_Sablon_Azonosito; }
            set { _Manual_Sablon_Azonosito = value; }
        }

        private Field _Manual_Dokumentum_BarCode = new Field();
        public Field Manual_Dokumentum_BarCode
        {
            get { return _Manual_Dokumentum_BarCode; }
            set { _Manual_Dokumentum_BarCode = value; }
        }

        private Field _Manual_Dokumentum_TartalomHash = new Field();
        public Field Manual_Dokumentum_TartalomHash
        {
            get { return _Manual_Dokumentum_TartalomHash; }
            set { _Manual_Dokumentum_TartalomHash = value; }
        }

        private Field _Manual_Dokumentum_KivonatHash = new Field();
        public Field Manual_Dokumentum_KivonatHash
        {
            get { return _Manual_Dokumentum_KivonatHash; }
            set { _Manual_Dokumentum_KivonatHash = value; }
        }
    }
}
