﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_HataridosFeladatokSearch
    {
        #region Egyéb, manuális mezők


        private void Init_ManualFields()
        {
            // BUG_6451
            _Letrehozo_id.Name = "EREC_HataridosFeladatok.Letrehozo_Id";
            _Letrehozo_id.Type = "Guid";

            _LetrehozasIdo.Name = "EREC_HataridosFeladatok.LetrehozasIdo";
            _LetrehozasIdo.Type = "DateTime";

            _Note.Name = "EREC_HataridosFeladatok.Note";
            _Note.Type = "String";

            _Manual_Lezartak.Name = "EREC_HataridosFeladatok.Allapot";
            _Manual_Lezartak.Type = "String";

        }

        // BUG_4651
        private Field _Letrehozo_id = new Field();
        public Field Letrehozo_id
        {
            get { return _Letrehozo_id; }
            set { _Letrehozo_id = value; }
        }

        private Field _LetrehozasIdo = new Field();

        public Field LetrehozasIdo
        {
            get { return _LetrehozasIdo; }
            set { _LetrehozasIdo = value; }
        }

        private Field _Note = new Field();

        public Field Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        private Field _Manual_Lezartak = new Field();

        public Field Manual_Lezartak
        {
            get { return _Manual_Lezartak; }
            set { _Manual_Lezartak = value; }
        }


        #endregion

        private bool _UseUgyiratHierarchy = false;

        public bool UseUgyiratHierarchy
        {
            get { return _UseUgyiratHierarchy; }
            set { _UseUgyiratHierarchy = value; }
        }

        private bool _UseUgyiratHierarchyWithParent = false;

        public bool UseUgyiratHierarchyWithParent
        {
            get { return _UseUgyiratHierarchyWithParent; }
            set { _UseUgyiratHierarchyWithParent = value; }
        }

        private bool _DisplayChildsIfParentNotVisible = false;

        public bool DisplayChildsIfParentNotVisible
        {
            get { return _DisplayChildsIfParentNotVisible; }
            set { _DisplayChildsIfParentNotVisible = value; }
        }

        private bool _Jogosultak = false;

        public bool Jogosultak
        {
            get { return _Jogosultak; }
            set { _Jogosultak = value; }
        }
    }
}
