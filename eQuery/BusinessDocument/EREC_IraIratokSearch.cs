using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_IraIratokSearch
    {
        /// <summary>
        /// Ha createExtendedSearchObjects == true, l�trehoz egy bels� 
        /// EREC_UgyUgyiratdarabokSearch objektumot
        /// </summary>
        /// <param name="createExtendedSearchObjects"></param>
        public EREC_IraIratokSearch(bool createExtendedSearchObjects)
            : this()
        {
            /// l�trehozzuk a bels�, m�s t�bl�ra vonatkoz� keres�si objektumokat, ha 
            /// createExtendedSearchObjects==true, egy�bk�nt null �rt�ken hagyjuk            
            if (createExtendedSearchObjects)
            {
                Extended_EREC_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();

                Extended_EREC_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();

                Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();

                Extended_EREC_IratAlairokSearch = new EREC_IratAlairokSearch();

                Extended_EREC_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();

                Extended_EREC_IraOnkormAdatokSearch = new EREC_IraOnkormAdatokSearch();

                /// a kuldemenyekn�l alapb�l nem szabad sz�rni az �rv�nyess�gre itt,
                /// mivel akkor az iratok lista nem hozn� azokat az iratokat, ahol
                /// a k�ldem�nyt m�r �rv�nytelen�tett�k

                Extended_EREC_KuldKuldemenyekSearch.ErvKezd.Clear();
                Extended_EREC_KuldKuldemenyekSearch.ErvVege.Clear();

                Extended_EREC_UgyUgyiratdarabokSearch.ErvKezd.Clear();
                Extended_EREC_UgyUgyiratdarabokSearch.ErvVege.Clear();

                Extended_EREC_IraIktatoKonyvekSearch.ErvKezd.Clear();
                Extended_EREC_IraIktatoKonyvekSearch.ErvVege.Clear();

                Extended_EREC_IratAlairokSearch.ErvKezd.Clear();
                Extended_EREC_IratAlairokSearch.ErvVege.Clear();

                Extended_EREC_PldIratPeldanyokSearch.ErvKezd.Clear();
                Extended_EREC_PldIratPeldanyokSearch.ErvVege.Clear();

                Extended_EREC_IraOnkormAdatokSearch.ErvKezd.Clear();
                Extended_EREC_IraOnkormAdatokSearch.ErvVege.Clear();
            }
            //SetDefaultAktivUIFilter();
        }

        public EREC_UgyUgyiratdarabokSearch Extended_EREC_UgyUgyiratdarabokSearch;

        public EREC_KuldKuldemenyekSearch Extended_EREC_KuldKuldemenyekSearch;

        public EREC_IratAlairokSearch Extended_EREC_IratAlairokSearch;

        public EREC_PldIratPeldanyokSearch Extended_EREC_PldIratPeldanyokSearch;

        public EREC_IraOnkormAdatokSearch Extended_EREC_IraOnkormAdatokSearch;

        [Contentum.eQuery.Base.AddToWhere]
        public EREC_IraIktatoKonyvekSearch Extended_EREC_IraIktatoKonyvekSearch;


        #region Egy�b, manu�lis mez�k


        private void Init_ManualFields()
        {
            // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
            // m�sodlagos sz�r�shez, pl. Irat.FilterObject ExcludeAllapot felt�telhez
            _Manual_Allapot_Filter.Name = "EREC_IraIratok.Allapot";
            _Manual_Allapot_Filter.Type = "String";

            _Manual_LetrehozasIdo.Name = "EREC_IraIratok.LetrehozasIdo";
            _Manual_LetrehozasIdo.Type = "DateTime";

            _Manual_Csatolhato.Name = "EREC_IraIratok.Allapot";
            _Manual_Csatolhato.Type = "String";

            _Manual_ModositasIdo.Name = "EREC_IraIratok.ModositasIdo";
            _Manual_ModositasIdo.Type = "DateTime";

            _Manual_Alszam_MunkaanyagFilter.Name = "EREC_IraIratok.Alszam";
            _Manual_Alszam_MunkaanyagFilter.Type = "Int32";

            _Manual_UgyiratFoszam_MunkaanyagFilter.Name = "EREC_UgyUgyiratok.Foszam";
            _Manual_UgyiratFoszam_MunkaanyagFilter.Type = "Int32";

            _Manual_Alszam_Sztornozott.Name = "EREC_IraIratok.Allapot";
            _Manual_Alszam_Sztornozott.Type = "String";
        }

        // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
        private Field _Manual_Allapot_Filter = new Field();

        public Field Manual_Allapot_Filter
        {
            get { return _Manual_Allapot_Filter; }
            set { _Manual_Allapot_Filter = value; }
        }

        private Field _Manual_LetrehozasIdo = new Field();

        public Field Manual_LetrehozasIdo
        {
            get { return _Manual_LetrehozasIdo; }
            set { _Manual_LetrehozasIdo = value; }
        }

        private Field _Manual_ModositasIdo = new Field();

        public Field Manual_ModositasIdo
        {
            get { return _Manual_ModositasIdo; }
            set { _Manual_ModositasIdo = value; }
        }

        private Field _Manual_Csatolhato = new Field();

        public Field Manual_Csatolhato
        {
            get { return _Manual_Csatolhato; }
            set { _Manual_Csatolhato = value; }
        }


        private Field _Manual_Alszam_MunkaanyagFilter = new Field();

        public Field Manual_Alszam_MunkaanyagFilter
        {
            get { return _Manual_Alszam_MunkaanyagFilter; }
            set { _Manual_Alszam_MunkaanyagFilter = value; }
        }

        private Field _Manual_UgyiratFoszam_MunkaanyagFilter = new Field();

        public Field Manual_UgyiratFoszam_MunkaanyagFilter
        {
            get { return _Manual_UgyiratFoszam_MunkaanyagFilter; }
            set { _Manual_UgyiratFoszam_MunkaanyagFilter = value; }
        }


        private Field _Manual_IktatasDatuma_Munkanap = new Field("EREC_IraIratok.IktatasDatuma", "Munkanap");

        public Field Manual_IktatasDatuma_Munkanap
        {
            get { return _Manual_IktatasDatuma_Munkanap; }
            set { _Manual_IktatasDatuma_Munkanap = value; }
        }


        private Field _Manual_Alszam_Sztornozott = new Field();

        public Field Manual_Alszam_Sztornozott
        {
            get { return _Manual_Alszam_Sztornozott; }
            set { _Manual_Alszam_Sztornozott = value; }
        }

        #endregion

        public FullTextSearchField fts_targyszavak;

        public FullTextSearchField fts_ertek;

        public FullTextSearchField fts_note;

        // CR#2035: feleslegess� v�lt, mert az EREC_IraIratok.Targy FTSString t�pus�
        //public FullTextSearchField fts_targy;

        public Contentum.eQuery.FullTextSearch.FullTextSearchTree fts_tree_ugyirat_auto = null;

        public Contentum.eQuery.FullTextSearch.FullTextSearchTree fts_tree_irat_auto = null;

        public Contentum.eQuery.FullTextSearch.FullTextSearchTree fts_tree_irat_egyeb = null;

        // sz�r�felt�tel az objektumok t�rgyszavaiban l�v� Obj_Id-ekre
        // pl. SELECT Obj_Id FROM EREC_ObjektumTargyszavai WHERE ...
        public string ObjektumTargyszavai_ObjIdFilter = "";

        private string _AlSzervezetId;

        public string AlSzervezetId
        {
            get { return _AlSzervezetId; }
            set { _AlSzervezetId = value; }
        }

        private bool _IsUgyiratTerkepQuery = false;

        /// <summary>
        /// Annak a jelz�s�re szolg�l, ha az �gyiratt�rk�phez k�rdezz�k le az iratok adatait
        /// sp_EREC_IraIratokGetAllWithExtension t�rolt elj�r�s megkapja param�terk�nt
        /// </summary>
        public bool IsUgyiratTerkepQuery
        {
            get { return _IsUgyiratTerkepQuery; }
            set { _IsUgyiratTerkepQuery = value; }
        }

        #region gyorsitas

        bool _Csoporttagokkal = true;

        public bool Csoporttagokkal
        {
            get
            {
                return _Csoporttagokkal;
            }
            set
            {
                _Csoporttagokkal = value;
            }
        }

        bool _CsakAktivIrat = false;

        public bool CsakAktivIrat
        {
            get
            {
                return _CsakAktivIrat;
            }
            set
            {
                _CsakAktivIrat = value;
            }
        }

        /// <summary>
        /// 0 (default) � nincs sz�r�s
        /// 1 � csak a csatolm�ny n�lk�liek
        /// 2 � csak a csatolm�nnyal rendelkez�k jelenjenek meg
        /// </summary>
        private Field _CsatolmanySzures = new Field();

        public Field CsatolmanySzures
        {
            get
            {
                if (string.IsNullOrEmpty(_CsatolmanySzures.Value))
                {
                    _CsatolmanySzures.Value = "0";
                }

                return _CsatolmanySzures;
            }
            set { _CsatolmanySzures = value; }
        }

        public void SetDefaultUIFilter()
        {
            this.Csoporttagokkal = false;
            this.CsakAktivIrat = true;
            this.CsatolmanySzures.Value = "0";
        }
        public void SetDefaultAktivUIFilter()
        {
            this.CsakAktivIrat = true;
        }
        #endregion
    }

}
