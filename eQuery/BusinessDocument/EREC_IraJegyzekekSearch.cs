﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_IraJegyzekekSearch
    {
        private void Init_ManualFields()
        {
            _Note.Name = "EREC_IraJegyzekek.Note";
            _Note.Type = "String";
        }

        private Field _Note = new Field();

        public Field Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
    }
}
