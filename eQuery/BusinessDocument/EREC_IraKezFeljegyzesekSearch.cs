using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_IraKezFeljegyzesekSearch
    {
        private void Init_ManualFields()
        {
            _Manual_Ugyiratok_Id.Name = "EREC_UgyUgyiratok.Id";
            _Manual_Ugyiratok_Id.Type = "Guid";

            _Manual_IraIratok_Id.Name = "EREC_IraIratok.Id";
            _Manual_IraIratok_Id.Type = "Guid";
        }

        private Field _Manual_Ugyiratok_Id = new Field();

        public Field Manual_Ugyiratok_Id
        {
            get { return _Manual_Ugyiratok_Id; }
            set { _Manual_Ugyiratok_Id = value; }
        }

        // IraIrat_Id �tir�ny�tva az iratp�ld�nyra, ez�rt ezzel tudjuk lek�rni az irathoz tartoz�kat
        private Field _Manual_IraIratok_Id = new Field();

        public Field Manual_IraIratok_Id
        {
            get { return _Manual_IraIratok_Id; }
            set { _Manual_IraIratok_Id = value; }
        }
    }
}
