﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_IraKezbesitesiFejekSearch
    {
        private void Init_ManualFields()
        {
            _Manual_Letrehozo_id.Name = "EREC_IraKezbesitesiFejek.Letrehozo_id";
            _Manual_Letrehozo_id.Type = "Guid";

            _Manual_LetrehozasIdo.Name = "EREC_IraKezbesitesiFejek.LetrehozasIdo";
            _Manual_LetrehozasIdo.Type = "DateTime";
        }

        private Field _Manual_Letrehozo_id = new Field();
        public Field Manual_Letrehozo_id
        {
            get { return _Manual_Letrehozo_id; }
            set { _Manual_Letrehozo_id = value; }
        }


        private Field _Manual_LetrehozasIdo = new Field();
        public Field Manual_LetrehozasIdo
        {
            get { return _Manual_LetrehozasIdo; }
            set { _Manual_LetrehozasIdo = value; }
        }


    }
}
