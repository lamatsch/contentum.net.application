using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_IraKezbesitesiTetelekSearch
    {
        public EREC_IraKezbesitesiTetelekSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_EREC_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
                Extended_EREC_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();
                Extended_EREC_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch(true);

                /// alapb�l kivessz�k az �rv�nyess�gre val� sz�r�st, hogy ha m�s sz�r�si felt�tel nincs megadva ezekn�l,
                /// ne gener�ljon semmit, hogy feleslegesen ne lass�tsa a lek�rdez�st
                /// 

                Extended_EREC_KuldKuldemenyekSearch.ErvKezd.Clear();
                Extended_EREC_KuldKuldemenyekSearch.ErvVege.Clear();

                Extended_EREC_UgyUgyiratokSearch.ErvKezd.Clear();
                Extended_EREC_UgyUgyiratokSearch.ErvVege.Clear();

                Extended_EREC_PldIratPeldanyokSearch.ErvKezd.Clear();
                Extended_EREC_PldIratPeldanyokSearch.ErvVege.Clear();

                Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.ErvKezd.Clear();
                Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.ErvVege.Clear();

                Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.ErvKezd.Clear();
                Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.ErvVege.Clear();

                Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.ErvKezd.Clear();
                Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.ErvVege.Clear();
            }
        }

        public EREC_KuldKuldemenyekSearch Extended_EREC_KuldKuldemenyekSearch;
        public EREC_UgyUgyiratokSearch Extended_EREC_UgyUgyiratokSearch;
        public EREC_PldIratPeldanyokSearch Extended_EREC_PldIratPeldanyokSearch;

        private void Init_ManualFields()
        {
            _Manual_LetrehozasIdo.Name = "EREC_IraKezbesitesiTetelek.LetrehozasIdo";
            _Manual_LetrehozasIdo.Type = "DateTime";

            _Manual_Atado_Sajat.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER";
            _Manual_Atado_Sajat.Type = "Guid";

            _Manual_Atado_Sajat_Szervezet.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER";
            _Manual_Atado_Sajat_Szervezet.Type = "Guid";

            _Manual_Atado_Sajat_Szervezet_Osszesen.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER";
            _Manual_Atado_Sajat_Szervezet_Osszesen.Type = "Guid";

            _Manual_Cimzett_Sajat.Name = "EREC_IraKezbesitesiTetelek.Csoport_Id_Cel";
            _Manual_Cimzett_Sajat.Type = "Guid";

            _Manual_Cimzett_Sajat_Szervezet.Name = "EREC_IraKezbesitesiTetelek.Csoport_Id_Cel";
            _Manual_Cimzett_Sajat_Szervezet.Type = "Guid";

            _Manual_Cimzett_Sajat_Szervezet_Osszesen.Name = "EREC_IraKezbesitesiTetelek.Csoport_Id_Cel";
            _Manual_Cimzett_Sajat_Szervezet_Osszesen.Type = "Guid";

            _Manual_Atvevo_Sajat.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser";
            _Manual_Atvevo_Sajat.Type = "Guid";

            _Manual_Atvevo_Sajat_Szervezet.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser";
            _Manual_Atvevo_Sajat_Szervezet.Type = "Guid";

            _Manual_Atvevo_Sajat_Szervezet_Osszesen.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser";
            _Manual_Atvevo_Sajat_Szervezet_Osszesen.Type = "Guid";

            _Manual_AtNemVett.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser";
            _Manual_AtNemVett.Type = "Guid";

            _Manual_Obj_type_Kuldemeny.Name = "EREC_IraKezbesitesiTetelek.Obj_type";
            _Manual_Obj_type_Kuldemeny.Type = "String";

            _Manual_Obj_type_Ugyirat.Name = "EREC_IraKezbesitesiTetelek.Obj_type";
            _Manual_Obj_type_Ugyirat.Type = "String";

            _Manual_Obj_type_IratPeldany.Name = "EREC_IraKezbesitesiTetelek.Obj_type";
            _Manual_Obj_type_IratPeldany.Type = "String";

            _Manual_Obj_type_Dosszie.Name = "EREC_IraKezbesitesiTetelek.Obj_type";
            _Manual_Obj_type_Dosszie.Type = "String";

            //_Manual_Kuld_ErkeztetoKonyvId.Name = "EREC_KuldKuldemenyek_Kuld.IraIktatokonyv_Id";
            //_Manual_Kuld_ErkeztetoKonyvId.Type = "Guid";

            //_Manual_Kuld_ErkeztetoSzam.Name = "EREC_KuldKuldemenyek_Kuld.Erkezteto_Szam";
            //_Manual_Kuld_ErkeztetoSzam.Type = "Int32";

            //_Manual_Ugyirat_IktatoKonyvId.Name = "EREC_UgyUgyiratok_Ugyirat.IraIktatokonyv_Id";
            //_Manual_Ugyirat_IktatoKonyvId.Type = "Guid";

            //_Manual_Ugyirat_Foszam.Name = "EREC_UgyUgyiratok_Ugyirat.Foszam";
            //_Manual_Ugyirat_Foszam.Type = "Int32";

            //_Manual_Pld_IktatoKonyvId.Name = "EREC_IraIktatoKonyvek_IratPld.Id";
            //_Manual_Pld_IktatoKonyvId.Type = "Guid";

            //_Manual_Pld_Foszam.Name = "EREC_UgyUgyiratdarabok_IratPld.Foszam";
            //_Manual_Pld_Foszam.Type = "Int32";

            //_Manual_Pld_Alszam.Name = "EREC_IraIratok_IratPld.Alszam";
            //_Manual_Pld_Alszam.Type = "Int32";

            //_Manual_Pld_Sorszam.Name = "EREC_PldIratPeldanyok_IratPld.Sorszam";
            //_Manual_Pld_Sorszam.Type = "Int32";
        }


        private Field _Manual_LetrehozasIdo = new Field();
        public Field Manual_LetrehozasIdo
        {
            get { return _Manual_LetrehozasIdo; }
            set { _Manual_LetrehozasIdo = value; }
        }

        private Field _Manual_Atado_Sajat = new Field();
        public Field Manual_Atado_Sajat
        {
            get { return _Manual_Atado_Sajat; }
            set { _Manual_Atado_Sajat = value; }
        }

        private Field _Manual_Atado_Sajat_Szervezet = new Field();
        public Field Manual_Atado_Sajat_Szervezet
        {
            get { return _Manual_Atado_Sajat_Szervezet; }
            set { _Manual_Atado_Sajat_Szervezet = value; }
        }

        private Field _Manual_Atado_Sajat_Szervezet_Osszesen = new Field();
        public Field Manual_Atado_Sajat_Szervezet_Osszesen
        {
            get { return _Manual_Atado_Sajat_Szervezet_Osszesen; }
            set { _Manual_Atado_Sajat_Szervezet_Osszesen = value; }
        }

        private Field _Manual_Cimzett_Sajat = new Field();
        public Field Manual_Cimzett_Sajat
        {
            get { return _Manual_Cimzett_Sajat; }
            set { _Manual_Cimzett_Sajat = value; }
        }

        private Field _Manual_Cimzett_Sajat_Szervezet = new Field();
        public Field Manual_Cimzett_Sajat_Szervezet
        {
            get { return _Manual_Cimzett_Sajat_Szervezet; }
            set { _Manual_Cimzett_Sajat_Szervezet = value; }
        }

        private Field _Manual_Cimzett_Sajat_Szervezet_Osszesen = new Field();
        public Field Manual_Cimzett_Sajat_Szervezet_Osszesen
        {
            get { return _Manual_Cimzett_Sajat_Szervezet_Osszesen; }
            set { _Manual_Cimzett_Sajat_Szervezet_Osszesen = value; }
        }

        private Field _Manual_Atvevo_Sajat = new Field();
        public Field Manual_Atvevo_Sajat
        {
            get { return _Manual_Atvevo_Sajat; }
            set { _Manual_Atvevo_Sajat = value; }
        }

        private Field _Manual_Atvevo_Sajat_Szervezet = new Field();
        public Field Manual_Atvevo_Sajat_Szervezet
        {
            get { return _Manual_Atvevo_Sajat_Szervezet; }
            set { _Manual_Atvevo_Sajat_Szervezet = value; }
        }

        private Field _Manual_Atvevo_Sajat_Szervezet_Osszesen = new Field();
        public Field Manual_Atvevo_Sajat_Szervezet_Osszesen
        {
            get { return _Manual_Atvevo_Sajat_Szervezet_Osszesen; }
            set { _Manual_Atvevo_Sajat_Szervezet_Osszesen = value; }
        }

        private Field _Manual_AtNemVett = new Field();
        public Field Manual_AtNemVett
        {
            get { return _Manual_AtNemVett; }
            set { _Manual_AtNemVett = value; }
        }

        private Field _Manual_Obj_type_Kuldemeny = new Field();
        public Field Manual_Obj_type_Kuldemeny
        {
            get { return _Manual_Obj_type_Kuldemeny; }
            set { _Manual_Obj_type_Kuldemeny = value; }
        }

        private Field _Manual_Obj_type_Ugyirat = new Field();
        public Field Manual_Obj_type_Ugyirat
        {
            get { return _Manual_Obj_type_Ugyirat; }
            set { _Manual_Obj_type_Ugyirat = value; }
        }

        private Field _Manual_Obj_type_IratPeldany = new Field();
        public Field Manual_Obj_type_IratPeldany
        {
            get { return _Manual_Obj_type_IratPeldany; }
            set { _Manual_Obj_type_IratPeldany = value; }
        }

        private Field _Manual_Obj_type_Dosszie = new Field();
        public Field Manual_Obj_type_Dosszie
        {
            get { return _Manual_Obj_type_Dosszie; }
            set { _Manual_Obj_type_Dosszie = value; }
        }

        //private Field _Manual_Kuld_ErkeztetoKonyvId = new Field();
        //public Field Manual_Kuld_ErkeztetoKonyvId
        //{
        //    get { return _Manual_Kuld_ErkeztetoKonyvId; }
        //    set { _Manual_Kuld_ErkeztetoKonyvId = value; }
        //}

        //private Field _Manual_Kuld_ErkeztetoSzam = new Field();
        //public Field Manual_Kuld_ErkeztetoSzam
        //{
        //    get { return _Manual_Kuld_ErkeztetoSzam; }
        //    set { _Manual_Kuld_ErkeztetoSzam = value; }
        //}

        //private Field _Manual_Ugyirat_IktatoKonyvId = new Field();
        //public Field Manual_Ugyirat_IktatoKonyvId
        //{
        //    get { return _Manual_Ugyirat_IktatoKonyvId; }
        //    set { _Manual_Ugyirat_IktatoKonyvId = value; }
        //}

        //private Field _Manual_Ugyirat_Foszam = new Field();
        //public Field Manual_Ugyirat_Foszam
        //{
        //    get { return _Manual_Ugyirat_Foszam; }
        //    set { _Manual_Ugyirat_Foszam = value; }
        //}


        //private Field _Manual_Pld_IktatoKonyvId = new Field();
        //public Field Manual_Pld_IktatoKonyvId
        //{
        //    get { return _Manual_Pld_IktatoKonyvId; }
        //    set { _Manual_Pld_IktatoKonyvId = value; }
        //}

        //private Field _Manual_Pld_Foszam = new Field();
        //public Field Manual_Pld_Foszam
        //{
        //    get { return _Manual_Pld_Foszam; }
        //    set { _Manual_Pld_Foszam = value; }
        //}

        //private Field _Manual_Pld_Alszam = new Field();
        //public Field Manual_Pld_Alszam
        //{
        //    get { return _Manual_Pld_Alszam; }
        //    set { _Manual_Pld_Alszam = value; }
        //}

        //private Field _Manual_Pld_Sorszam = new Field();
        //public Field Manual_Pld_Sorszam
        //{
        //    get { return _Manual_Pld_Sorszam; }
        //    set { _Manual_Pld_Sorszam = value; }
        //}

        // CR#2035: feleslegess� v�lt, mert a kiterjesztett keres�objektumok megfelel� mez�i FTSString t�pus�ak
        //// az �gyirat �s irat t�rgy�ban, valamint a bek�ld� nev�ben val� k�z�s keres�si kifejez�s,
        //// ami sz�ri a k�zbes�t�si t�teleket, illetve az objektumokat a bejel�lt objektum t�pusok szerint
        //public FullTextSearchField fts_altalanos;

        #region ObjectMode
        /// <summary>
        /// CR#1890: �ltal�nos keres�s: k�zbes�t�si t�tel vagy objektum m�d
        /// isObjectMode == false: K�zbes�t�si t�telek fel�l keres�nk, �s az objektumokra vonatkoz� korl�toz� felt�tel szerint (ha van) sz�k�t�nk.
        /// isObjectMode == true: Objektumok fel�l keres�nk, �s a k�zbes�t�si t�telekre vonatkoz� korl�toz� felt�tel szerint (ha van) sz�k�t�nk.
        /// </summary>
        public bool isObjectMode = false;

        /// <summary>
        /// Kieg�sz�t� sz�r�felt�telek a k�zbes�t�si t�telek objektum orient�lt keres�s�hez (isObjectMode == true)
        /// </summary>
        public ExtendedFilterForObjectModeClass ExtendedFilterForObjectMode = new ExtendedFilterForObjectModeClass();

        public class ExtendedFilterForObjectModeClass
        {
            /// <summary>
            /// ObjectMode-ban, amennyiben nincs a defaultt�l elt�r� sz�r�s a k�zbes�t�si t�telekre
            /// (IsDefaultWhere=true vagy a gener�lt Where-felt�tel �res), ezzel lehet meghat�rozni, hogy az objektumokhoz
            /// mely k�zbes�t�si t�telek jelenjenek meg
            /// LastValidIfExists: az objektumhoz kapcsol�d� utols� �rv�nyes k�zbes�t�si t�tel, ha l�tezik (alap�rtelmez�s)
            /// AllValidIfExists: az objektum a hozz�juk kapcsol�d� minden �rv�nyes k�zbes�t�si t�tellel, ha van olyan
            /// None: csak az objektumok, a k�zbes�t�si t�teleket nem adjuk vissza
            /// ObjectHasNoValidItem: csak olyan objektumok, melyekhez nem tartozik �rv�nyes k�zbes�t�si t�tel
            /// </summary>
            public enum KezbesitesiTetelekSearchModeType { LastValidIfExists, AllValidIfExists, None, ObjectHasNoValidItem }
            // tov�bbi lehets�ges �rt�kek: { ..., AllIfExists, AllInvalidIfExists, LastValid, AllValid, All, AllInvalid }
            // AllIfExists: az objektum a hozz�juk kapcsol�d� minden �rv�nyes �s �rv�nytelen k�zbes�t�si t�tellel, ha van olyan
            // AllInvalidIfExists: az objektum a hozz�juk kapcsol�d� minden �rv�nytelen k�zbes�t�si t�tellel, ha van olyan
            // LastValid: az objektumhoz kapcsol�d� utols� �rv�nyes k�zbes�t�si t�tel, �rv�nyes k�zbes�t�si t�tel n�lk�li objektumot nem adunk vissza
            // AllValid: az objektum a hozz�juk kapcsol�d� minden �rv�nyes k�zbes�t�si t�tellel, �rv�nyes k�zbes�t�si t�tel n�lk�li objektumot nem adunk vissza
            // All: az objektum a hozz�juk kapcsol�d� minden �rv�nyes �s �rv�nytelen k�zbes�t�si t�tellel, �rv�nyes k�zbes�t�si t�tel n�lk�li objektumot nem adunk vissza


            /// <summary>
            /// ObjectMode-ban, ha csak az alap�rtelmez�s szerinti felt�telek ker�lnek be a k�zbes�t�si t�telekre vonatkoz�
            /// keres�si felt�telbe (�rv�nyess�g, objektum t�pus), akkor nem vessz�k figyelembe a sz�r�st
            /// A where felt�tel "default" jelleg�t a fel�leten kell vizsg�lni (van-e a felhaszn�l� �ltal kit�lt�tt mez�),
            /// �s explicit �tadni
            /// </summary>
            private bool _isDefaultWhere = false;
            public bool IsDefaultWhere
            {
                get { return _isDefaultWhere; }
                set { _isDefaultWhere = value; }
            }

            private KezbesitesiTetelekSearchModeType _kezbesitesiTetelekSearchMode = KezbesitesiTetelekSearchModeType.LastValidIfExists;
            public KezbesitesiTetelekSearchModeType KezbesitesiTetelekSearchMode
            {
                get { return _kezbesitesiTetelekSearchMode; }
                set { _kezbesitesiTetelekSearchMode = value; }
            }
        }
        #endregion ObjectMode
    }
}
