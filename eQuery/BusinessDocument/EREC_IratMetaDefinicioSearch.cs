using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_IratMetaDefinicioSearch
    {
        private void Init_ManualFields()
        {
            _Manual_Csoport_Id_Felelos.Name = "EREC_SzignalasiJegyzekek.Csoport_Id_Felelos";
            _Manual_Csoport_Id_Felelos.Type = "Guid";

            _Manual_SzignalasTipusa.Name = "EREC_SzignalasiJegyzekek.SzignalasTipusa";
            _Manual_SzignalasTipusa.Type = "String";

            _Manual_Ugytipus_isNull.Name = "EREC_IratMetaDefinicio.Ugytipus";
            _Manual_Ugytipus_isNull.Type = "String";
        }

        private Field _Manual_Csoport_Id_Felelos = new Field();

        public Field Manual_Csoport_Id_Felelos
        {
            get { return _Manual_Csoport_Id_Felelos; }
            set { _Manual_Csoport_Id_Felelos = value; }
        }


        private Field _Manual_SzignalasTipusa = new Field();

        public Field Manual_SzignalasTipusa
        {
            get { return _Manual_SzignalasTipusa; }
            set { _Manual_SzignalasTipusa = value; }
        }

        private Field _Manual_Ugytipus_isNull = new Field();

        public Field Manual_Ugytipus_isNull
        {
            get { return _Manual_Ugytipus_isNull; }
            set { _Manual_Ugytipus_isNull = value; }
        }

    }
}
