using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_KuldKezFeljegyzesekSearch
    {
        private void Init_ManualFields()
        {
            _Manual_Ugyiratok_Id.Name = "EREC_UgyUgyiratok.Id";
            _Manual_Ugyiratok_Id.Type = "Guid";
        }

        private Field _Manual_Ugyiratok_Id = new Field();

        public Field Manual_Ugyiratok_Id
        {
            get { return _Manual_Ugyiratok_Id; }
            set { _Manual_Ugyiratok_Id = value; }
        }
    }
}
