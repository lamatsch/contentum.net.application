using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_KuldKuldemenyekSearch
    {
        public EREC_KuldKuldemenyekSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_EREC_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();
                Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
				Extended_KRT_MappakSearch = new KRT_MappakSearch();
                Extended_EREC_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
                Extended_EREC_SzamlakSearch = new EREC_SzamlakSearch();
                Extended_EREC_IraIratokSearch = new EREC_IraIratokSearch();
                Extended_EREC_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();
                

                /// alapb�l kivessz�k az �rv�nyess�gre val� sz�r�st, hogy ha m�s sz�r�si felt�tel nincs megadva ezekn�l,
                /// ne gener�ljon semmit, hogy feleslegesen ne lass�tsa a lek�rdez�st
                /// 
                Extended_EREC_PldIratPeldanyokSearch.ErvKezd.Clear();
                Extended_EREC_PldIratPeldanyokSearch.ErvVege.Clear();

                Extended_EREC_IraIktatoKonyvekSearch.ErvKezd.Clear();
                Extended_EREC_IraIktatoKonyvekSearch.ErvVege.Clear();

                Extended_KRT_MappakSearch.ErvKezd.Clear();
                Extended_KRT_MappakSearch.ErvVege.Clear();

                Extended_EREC_KuldKuldemenyekSearch.ErvKezd.Clear();
                Extended_EREC_KuldKuldemenyekSearch.ErvVege.Clear();

                Extended_EREC_SzamlakSearch.ErvKezd.Clear();
                Extended_EREC_SzamlakSearch.ErvVege.Clear();

                Extended_EREC_UgyUgyiratdarabokSearch.ErvKezd.Clear();
                Extended_EREC_UgyUgyiratdarabokSearch.ErvVege.Clear();

                Extended_EREC_IraIratokSearch.ErvKezd.Clear();
                Extended_EREC_IraIratokSearch.ErvVege.Clear();
                
            }
        }

        public EREC_PldIratPeldanyokSearch Extended_EREC_PldIratPeldanyokSearch;
        [Contentum.eQuery.Base.AddToWhere]
        public EREC_IraIratokSearch Extended_EREC_IraIratokSearch;
        [Contentum.eQuery.Base.AddToWhere]
        public EREC_UgyUgyiratdarabokSearch Extended_EREC_UgyUgyiratdarabokSearch;

        [Contentum.eQuery.Base.AddToWhere]
        public EREC_IraIktatoKonyvekSearch Extended_EREC_IraIktatoKonyvekSearch;

		public KRT_MappakSearch Extended_KRT_MappakSearch;

        // kapcsolatok keres�s�hez
        public EREC_KuldKuldemenyekSearch Extended_EREC_KuldKuldemenyekSearch;

        public EREC_SzamlakSearch Extended_EREC_SzamlakSearch;
		
        private void Init_ManualFields()
        {
            _Manual_Sajat.Name = "EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo";
            _Manual_Sajat.Type = "Guid";

            _Manual_Iktathatok_Allapot.Name = "EREC_KuldKuldemenyek.Allapot";
            _Manual_Iktathatok_Allapot.Type = "String";

            _Manual_Iktathatok_IktatniKell.Name = "EREC_KuldKuldemenyek.IktatniKell";
            _Manual_Iktathatok_IktatniKell.Type = "String";

            _Manual_Sztornozhatok.Name = "EREC_KuldKuldemenyek.Allapot";
            _Manual_Sztornozhatok.Type = "String";

            _Manual_Iktatottak.Name = "EREC_KuldKuldemenyek.Allapot";
            _Manual_Iktatottak.Type = "String";

            _Manual_Lezartak.Name = "EREC_KuldKuldemenyek.Allapot";
            _Manual_Lezartak.Type = "String";

            _Manual_Sajat_Allapot.Name = "EREC_KuldKuldemenyek.Allapot";
            _Manual_Sajat_Allapot.Type = "String";

            _Manual_Sajat_TovabbitasAlattAllapot.Name = "EREC_KuldKuldemenyek.TovabbitasAlattAllapot";
            _Manual_Sajat_TovabbitasAlattAllapot.Type = "String";

            _Manual_Feljegyzes_Altipus.Name = "EREC_HataridosFeladatok.Altipus";
            _Manual_Feljegyzes_Altipus.Type = "String";

            _Manual_Feljegyzes_Leiras.Name = "EREC_HataridosFeladatok.Leiras";
            _Manual_Feljegyzes_Leiras.Type = "String";

            //_Manual_KuldKezFelj_KezelesTipus.Name = "EREC_KuldKezFeljegyzesek.KezelesTipus";
            //_Manual_KuldKezFelj_KezelesTipus.Type = "String";

            //_Manual_KuldKezFelj_Leiras.Name = "EREC_KuldKezFeljegyzesek.Leiras";
            //_Manual_KuldKezFelj_Leiras.Type = "String";

            _Manual_PostazasIranya_DefaultFilter.Name = "EREC_KuldKuldemenyek.PostazasIranya";
            _Manual_PostazasIranya_DefaultFilter.Type = "String";

            _Note.Name = "EREC_KuldKuldemenyek.Note";
            _Note.Type = "String";

            _Manual_ModositasIdo.Name = "EREC_KuldKuldemenyek.ModositasIdo";
            _Manual_ModositasIdo.Type = "DateTime";

            _Manual_Erkezteto_Id.Name = "EREC_KuldKuldemenyek.Letrehozo_id";
            _Manual_Erkezteto_Id.Type = "Guid";

            _Manual_Csatolhato.Name = "EREC_KuldKuldemenyek.Allapot";
            _Manual_Csatolhato.Type = "String";

            _Manual_ErkeztetesIdeje.Name = "EREC_KuldKuldemenyek.LetrehozasIdo";
            _Manual_ErkeztetesIdeje.Type = "DateTime";
        }

        private Field _Manual_ModositasIdo = new Field();

        public Field Manual_ModositasIdo
        {
            get { return _Manual_ModositasIdo; }
            set { _Manual_ModositasIdo = value; }
        }


        private Field _Manual_Sajat = new Field();

        public Field Manual_Sajat
        {
            get { return _Manual_Sajat; }
            set { _Manual_Sajat = value; }
        } 


        private Field _Manual_Iktathatok_Allapot = new Field();

        public Field Manual_Iktathatok_Allapot
        {
            get { return _Manual_Iktathatok_Allapot; }
            set { _Manual_Iktathatok_Allapot = value; }
        }

        private Field _Manual_Iktathatok_IktatniKell = new Field();

        public Field Manual_Iktathatok_IktatniKell
        {
            get { return _Manual_Iktathatok_IktatniKell; }
            set { _Manual_Iktathatok_IktatniKell = value; }
        }


        private Field _Manual_Sztornozhatok = new Field();

        public Field Manual_Sztornozhatok
        {
            get { return _Manual_Sztornozhatok; }
            set { _Manual_Sztornozhatok = value; }
        }


        private Field _Manual_Iktatottak = new Field();

        public Field Manual_Iktatottak
        {
            get { return _Manual_Iktatottak; }
            set { _Manual_Iktatottak = value; }
        }


        private Field _Manual_Lezartak = new Field();

        public Field Manual_Lezartak
        {
            get { return _Manual_Lezartak; }
            set { _Manual_Lezartak = value; }
        }

        private Field _Manual_Sajat_Allapot = new Field();

        public Field Manual_Sajat_Allapot
        {
            get { return _Manual_Sajat_Allapot; }
            set { _Manual_Sajat_Allapot = value; }
        }

        private Field _Manual_Sajat_TovabbitasAlattAllapot = new Field();

        public Field Manual_Sajat_TovabbitasAlattAllapot
        {
            get { return _Manual_Sajat_TovabbitasAlattAllapot; }
            set { _Manual_Sajat_TovabbitasAlattAllapot = value; }
        }

        //private Field _Manual_KuldKezFelj_KezelesTipus = new Field();

        //public Field Manual_KuldKezFelj_KezelesTipus
        //{
        //    get { return _Manual_KuldKezFelj_KezelesTipus; }
        //    set { _Manual_KuldKezFelj_KezelesTipus = value; }
        //}


        //private Field _Manual_KuldKezFelj_Leiras = new Field();

        //public Field Manual_KuldKezFelj_Leiras
        //{
        //    get { return _Manual_KuldKezFelj_Leiras; }
        //    set { _Manual_KuldKezFelj_Leiras = value; }
        //}

        private Field _Manual_Feljegyzes_Altipus = new Field();

        public Field Manual_Feljegyzes_Altipus
        {
            get { return _Manual_Feljegyzes_Altipus; }
            set { _Manual_Feljegyzes_Altipus = value; }
        }


        private Field _Manual_Feljegyzes_Leiras = new Field();

        public Field Manual_Feljegyzes_Leiras
        {
            get { return _Manual_Feljegyzes_Leiras; }
            set { _Manual_Feljegyzes_Leiras = value; }
        }

        
        private Field _Manual_PostazasIranya_DefaultFilter = new Field();

        public Field Manual_PostazasIranya_DefaultFilter
        {
            get { return _Manual_PostazasIranya_DefaultFilter; }
            set { _Manual_PostazasIranya_DefaultFilter = value; }
        }

        private Field _Note = new Field();

        public Field Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
    
        private Field _Manual_Erkezteto_Id = new Field();

        public Field Manual_Erkezteto_Id
        {
            get { return _Manual_Erkezteto_Id; }
            set { _Manual_Erkezteto_Id = value; }
        }

        private Field _Manual_Csatolhato = new Field();

        public Field Manual_Csatolhato
        {
            get { return _Manual_Csatolhato; }
            set { _Manual_Csatolhato = value; }
        }

        private string _AlSzervezetId;

        public string AlSzervezetId
        {
            get { return _AlSzervezetId; }
            set { _AlSzervezetId = value; }
        }

        private Field _Manual_ErkeztetesIdeje = new Field();

        public Field Manual_ErkeztetesIdeje
        {
            get { return _Manual_ErkeztetesIdeje; }
            set { _Manual_ErkeztetesIdeje = value; }
        }

        //public Contentum.eQuery.FullTextSearch.FullTextSearchTree fts_tree_objektumtargyszavai = null;

        //// sz�r�felt�tel az objektumok t�rgyszavaiban l�v� Obj_Id-ekre
        //// pl. SELECT Obj_Id FROM EREC_ObjektumTargyszavai WHERE ...
        //public string ObjektumTargyszavai_ObjIdFilter = "";
    }
}
