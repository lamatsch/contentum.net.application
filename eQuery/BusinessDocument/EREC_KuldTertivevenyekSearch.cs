﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_KuldTertivevenyekSearch
    {
        public EREC_KuldTertivevenyekSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_EREC_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
                
                /// alapból kivesszük az érvényességre való szűrést, hogy ha más szűrési feltétel nincs megadva ezeknél,
                /// ne generáljon semmit, hogy feleslegesen ne lassítsa a lekérdezést
                /// 
                //Extended_EREC_KuldKuldemenyekSearch.ErvKezd.Value = "";
                //Extended_EREC_KuldKuldemenyekSearch.ErvKezd.Operator = "";
                //Extended_EREC_KuldKuldemenyekSearch.ErvKezd.GroupOperator = "";

                //Extended_EREC_KuldKuldemenyekSearch.ErvVege.Value = "";
                //Extended_EREC_KuldKuldemenyekSearch.ErvVege.Operator = "";
                //Extended_EREC_KuldKuldemenyekSearch.ErvVege.GroupOperator = "";
            }
        }

        public EREC_KuldKuldemenyekSearch Extended_EREC_KuldKuldemenyekSearch;

        private void Init_ManualFields()
        {
            // A Note mezőben jelenleg az átvétel jogcímét tároljuk
            _Manual_Note.Name = "EREC_KuldTertivevenyek.Note";
            _Manual_Note.Type = "String";
        }

        // A Note mezőben jelenleg az átvétel jogcímét tároljuk
        private Field _Manual_Note = new Field();

        public Field Manual_Note
        {
            get { return _Manual_Note; }
            set { _Manual_Note = value; }
        } 

    }
}
