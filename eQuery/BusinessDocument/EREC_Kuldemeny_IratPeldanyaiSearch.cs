﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_Kuldemeny_IratPeldanyaiSearch
    {
        public EREC_Kuldemeny_IratPeldanyaiSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_EREC_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch(false);
                Extended_EREC_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch(false);

                Extended_EREC_KuldKuldemenyekSearch.ErvKezd.Clear();
                Extended_EREC_KuldKuldemenyekSearch.ErvVege.Clear();

                Extended_EREC_PldIratPeldanyokSearch.ErvKezd.Clear();
                Extended_EREC_PldIratPeldanyokSearch.ErvVege.Clear();
            }
        }

        public EREC_KuldKuldemenyekSearch Extended_EREC_KuldKuldemenyekSearch;

        public EREC_PldIratPeldanyokSearch Extended_EREC_PldIratPeldanyokSearch;
    }
}
