using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_PldIratPeldanyokSearch
    {
        /// <summary>
        /// Ha createExtendedSearchObjects == true, l�trehoz egy bels� 
        /// EREC_IraIratokSearch �s egy EREC_UgyUgyiratokSearch objektumot
        /// </summary>
        /// <param name="createExtendedSearchObjects"></param>
        public EREC_PldIratPeldanyokSearch(bool createExtendedSearchObjects):this()
        {
            /// l�trehozzuk a bels�, m�s t�bl�ra vonatkoz� keres�si objektumokat, ha 
            /// createExtendedSearchObjects==true, egy�bk�nt null �rt�ken hagyjuk            
            if (createExtendedSearchObjects)
            {
                Extended_EREC_IraIratokSearch = new EREC_IraIratokSearch();
                Extended_EREC_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();
                Extended_EREC_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();
                Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
				Extended_KRT_MappakSearch = new KRT_MappakSearch();


                Extended_EREC_IraIktatoKonyvekSearch.ErvKezd.Clear();
                Extended_EREC_IraIktatoKonyvekSearch.ErvVege.Clear();

                Extended_EREC_UgyUgyiratokSearch.ErvKezd.Clear();
                Extended_EREC_UgyUgyiratokSearch.ErvVege.Clear();

                Extended_EREC_UgyUgyiratdarabokSearch.ErvKezd.Clear();
                Extended_EREC_UgyUgyiratdarabokSearch.ErvVege.Clear();

                Extended_EREC_IraIratokSearch.ErvKezd.Clear();
                Extended_EREC_IraIratokSearch.ErvVege.Clear();

                Extended_KRT_MappakSearch.ErvKezd.Clear();
                Extended_KRT_MappakSearch.ErvVege.Clear();	
            }
            //SetDefaultAktivUIFilter();
        }
                
        public EREC_IraIratokSearch Extended_EREC_IraIratokSearch;
        public EREC_UgyUgyiratokSearch Extended_EREC_UgyUgyiratokSearch;
        public EREC_UgyUgyiratdarabokSearch Extended_EREC_UgyUgyiratdarabokSearch;
        public EREC_HataridosFeladatokSearch Extended_EREC_HataridosFeladatokSearch;
		public KRT_MappakSearch Extended_KRT_MappakSearch;

		
        [Contentum.eQuery.Base.AddToWhere]
        public EREC_IraIktatoKonyvekSearch Extended_EREC_IraIktatoKonyvekSearch;

        #region Egy�b, manu�lis mez�k


        private void Init_ManualFields()
        {
            _Manual_Sajat.Name = "EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo";
            _Manual_Sajat.Type = "Guid";

            _Manual_Sajat_Allapot.Name = "EREC_PldIratPeldanyok.Allapot";
            _Manual_Sajat_Allapot.Type = "String";

            _Manual_Sajat_TovabbitasAlattAllapot.Name = "EREC_PldIratPeldanyok.TovabbitasAlattAllapot";
            _Manual_Sajat_TovabbitasAlattAllapot.Type = "String";
            
            _Manual_Jovahagyandok.Name = "EREC_PldIratPeldanyok.Allapot";
            _Manual_Jovahagyandok.Type = "String";            

            _Manual_KuldKuldemenyek_NevSTR_Bekuldo.Name = "EREC_KuldKuldemenyek.NevSTR_Bekuldo";
            _Manual_KuldKuldemenyek_NevSTR_Bekuldo.Type = "String";

            _Manual_KuldKuldemenyek_Partner_Id_Bekuldo.Name = "EREC_KuldKuldemenyek.Partner_Id_Bekuldo";
            _Manual_KuldKuldemenyek_Partner_Id_Bekuldo.Type = "Guid";

            //_Manual_IraKezFeljegyzesek_KezelesTipus.Name = "EREC_IraKezFeljegyzesek.KezelesTipus";
            //_Manual_IraKezFeljegyzesek_KezelesTipus.Type = "String";

            //_Manual_IraKezFeljegyzesek_Note.Name = "EREC_IraKezFeljegyzesek.Note";
            //_Manual_IraKezFeljegyzesek_Note.Type = "String";

            _Manual_Kezelhetok.Name = "EREC_PldIratPeldanyok.Allapot";
            _Manual_Kezelhetok.Type = "String";

            _Manual_Elkuldottek.Name = "EREC_PldIratPeldanyok.Allapot";
            _Manual_Elkuldottek.Type = "String";

            _Manual_Lezart_iratok.Name = "EREC_UgyUgyiratdarabok.Allapot";
            _Manual_Lezart_iratok.Type = "String";

            _Manual_Sztornozottak.Name = "EREC_PldIratPeldanyok.Allapot";
            _Manual_Sztornozottak.Type = "String";

            _Manual_LetrehozasIdo.Name = "EREC_PldIratPeldanyok.LetrehozasIdo";
            _Manual_LetrehozasIdo.Type = "DateTime";

            _Manual_ModositasIdo.Name = "EREC_PldIratPeldanyok.ModositasIdo";
            _Manual_ModositasIdo.Type = "DateTime";

            _Manual_Csatolhato.Name = "EREC_PldIratPeldanyok.Allapot";
            _Manual_Csatolhato.Type = "String";

            _Manual_Ugyintezo.Name = "ISNULL(EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez, EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez)";
            _Manual_Ugyintezo.Type = "Guid";

            _Manual_TovabbitasAlattiakNelkul.Name = "EREC_PldIratPeldanyok.Allapot";
            _Manual_TovabbitasAlattiakNelkul.Type = "String";

            #region BUG 6143
            // BUG#2921: EREC_KuldKuldemenyek --> EREC_KuldKuldemenyek_Kimeno
            _Kuldemeny_Ragszam.Name = "EREC_KuldKuldemenyek_Kimeno.Ragszam";
            _Kuldemeny_Ragszam.Type = "String";
            #endregion

        }

        private Field _Manual_Sajat = new Field();

        public Field Manual_Sajat
        {
            get { return _Manual_Sajat; }
            set { _Manual_Sajat = value; }
        }

        private Field _Manual_Sajat_Allapot = new Field();

        public Field Manual_Sajat_Allapot
        {
            get { return _Manual_Sajat_Allapot; }
            set { _Manual_Sajat_Allapot = value; }
        }

        private Field _Manual_Sajat_TovabbitasAlattAllapot = new Field();

        public Field Manual_Sajat_TovabbitasAlattAllapot
        {
            get { return _Manual_Sajat_TovabbitasAlattAllapot; }
            set { _Manual_Sajat_TovabbitasAlattAllapot = value; }
        }

        private Field _Manual_ModositasIdo = new Field();

        public Field Manual_ModositasIdo
        {
            get { return _Manual_ModositasIdo; }
            set { _Manual_ModositasIdo = value; }
        }

        
        private Field _Manual_Jovahagyandok = new Field();

        public Field Manual_Jovahagyandok
        {
            get { return _Manual_Jovahagyandok; }
            set { _Manual_Jovahagyandok = value; }
        }        

        private Field _Manual_LetrehozasIdo = new Field();

        public Field Manual_LetrehozasIdo
        {
            get { return _Manual_LetrehozasIdo; }
            set { _Manual_LetrehozasIdo = value; }
        }


        private Field _Manual_KuldKuldemenyek_Partner_Id_Bekuldo = new Field();

        public Field Manual_KuldKuldemenyek_Partner_Id_Bekuldo
        {
            get { return _Manual_KuldKuldemenyek_Partner_Id_Bekuldo; }
            set { _Manual_KuldKuldemenyek_Partner_Id_Bekuldo = value; }
        }

        private Field _Manual_KuldKuldemenyek_NevSTR_Bekuldo = new Field();

        public Field Manual_KuldKuldemenyek_NevSTR_Bekuldo
        {
            get { return _Manual_KuldKuldemenyek_NevSTR_Bekuldo; }
            set { _Manual_KuldKuldemenyek_NevSTR_Bekuldo = value; }
        }


        private Field _Manual_Kezelhetok = new Field();

        public Field Manual_Kezelhetok
        {
            get { return _Manual_Kezelhetok; }
            set { _Manual_Kezelhetok = value; }
        }


        private Field _Manual_Elkuldottek = new Field();

        public Field Manual_Elkuldottek
        {
            get { return _Manual_Elkuldottek; }
            set { _Manual_Elkuldottek = value; }
        }


        private Field _Manual_Lezart_iratok = new Field();

        public Field Manual_Lezart_iratok
        {
            get { return _Manual_Lezart_iratok; }
            set { _Manual_Lezart_iratok = value; }
        }


        private Field _Manual_Sztornozottak = new Field();

        public Field Manual_Sztornozottak
        {
            get { return _Manual_Sztornozottak; }
            set { _Manual_Sztornozottak = value; }
        }

        private Field _Manual_Csatolhato = new Field();

        public Field Manual_Csatolhato
        {
            get { return _Manual_Csatolhato; }
            set { _Manual_Csatolhato = value; }
        }

        private Field _Manual_Ugyintezo = new Field();

        public Field Manual_Ugyintezo
        {
            get { return _Manual_Ugyintezo; }
            set { _Manual_Ugyintezo = value; }
        }

        private Field _Manual_TovabbitasAlattiakNelkul = new Field();

        public Field Manual_TovabbitasAlattiakNelkul
        {
            get { return _Manual_TovabbitasAlattiakNelkul; }
            set { _Manual_TovabbitasAlattiakNelkul = value; }
        }

        #region BUG 6143
        private Field _Kuldemeny_Ragszam = new Field();

        public Field Kuldemeny_Ragszam
        {
            get { return _Kuldemeny_Ragszam; }
            set { _Kuldemeny_Ragszam = value; }
        }
        #endregion

        // CR#1729: K�l�n sz�r�s a tov�bb�t�s alatt l�v� iratp�ld�nyok �llapot�ra
        // (alap�rtelmez�sben ezek is j�nnek a list�kon, de ezzel a flaggel kikapcsolhat�)
        // Nem ker�l be a Where felt�telbe, csak a be�ll�t�s megjegyz�s�t �s oldalak k�z�tti �tvitel�t szolg�lja
        // Felhaszn�l�sa: ha �rt�ke true, a megadott �llapot az Allapot �s TovabbitasAlattAllapot mez�kben is keresend� (OR)
        // egy�bk�nt csak az Allapot mez�ben (PldIratPeldanyokSearch.aspx haszn�lja)
        private bool isTovabbitasAlattAllapotIncluded = true;

        public bool IsTovabbitasAlattAllapotIncluded
        {
            get { return isTovabbitasAlattAllapotIncluded; }
            set
            {
                if (isTovabbitasAlattAllapotIncluded != value)
                {
                    isTovabbitasAlattAllapotIncluded = value;
                    if (!value)
                    {
                        Manual_TovabbitasAlattiakNelkul.Value = "50"; //Tov�bb�t�s alatt �llapot
                        Manual_TovabbitasAlattiakNelkul.Operator = Query.Operators.notequals;
                    }
                    else
                    {
                       // Manual_TovabbitasAlattiakNelkul.Clear();
                        Manual_TovabbitasAlattiakNelkul.Value = "50"; //Tov�bb�t�s alatt �llapot
                        Manual_TovabbitasAlattiakNelkul.Operator = Query.Operators.equals;

                    }
                }
            }
        }

        #endregion

        #region gyorsitas

        bool _Csoporttagokkal = true;

        public bool Csoporttagokkal
        {
            get
            {
                return _Csoporttagokkal;
            }
            set
            {
                _Csoporttagokkal = value;
            }
        }

        bool _CsakAktivIrat = false;

        public bool CsakAktivIrat
        {
            get
            {
                return _CsakAktivIrat;
            }
            set
            {
                _CsakAktivIrat = value;
            }
        }

        public void SetDefaultUIFilter()
        {
            this.Csoporttagokkal = false;
            this.CsakAktivIrat = true;
        }

        public void SetDefaultAktivUIFilter()
        {
            this.CsakAktivIrat = true;
        }
        #endregion
    }
}
