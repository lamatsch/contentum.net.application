using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_SzamlakSearch
    {

        public EREC_SzamlakSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_EREC_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
                Extended_KRT_DokumentumokSearch = new KRT_DokumentumokSearch();


                /// alapb�l kivessz�k az �rv�nyess�gre val� sz�r�st, hogy ha m�s sz�r�si felt�tel nincs megadva ezekn�l,
                /// ne gener�ljon semmit, hogy feleslegesen ne lass�tsa a lek�rdez�st
                /// 

                Extended_EREC_KuldKuldemenyekSearch.ErvKezd.Clear();
                Extended_EREC_KuldKuldemenyekSearch.ErvVege.Clear();

                Extended_KRT_DokumentumokSearch.ErvKezd.Clear();
                Extended_KRT_DokumentumokSearch.ErvVege.Clear();
            }
        }

        public EREC_KuldKuldemenyekSearch Extended_EREC_KuldKuldemenyekSearch;

        public KRT_DokumentumokSearch Extended_KRT_DokumentumokSearch;

        #region Egy�b, manu�lis mez�k


        private void Init_ManualFields()
        {
            _Manual_Note.Name = "EREC_Szamlak.Note";
            _Manual_Note.Type = "String";            
        }

        private Field _Manual_Note = new Field();

        public Field Manual_Note
        {
            get { return _Manual_Note; }
            set { _Manual_Note = value; }
        }
        #endregion
 

    }
}
