using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_UgyUgyiratdarabokSearch
    {
        public EREC_UgyUgyiratdarabokSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();

                /// alapb�l kivessz�k az �rv�nyess�gre val� sz�r�st, hogy ha m�s sz�r�si felt�tel nincs megadva ezekn�l,
                /// ne gener�ljon semmit, hogy feleslegesen ne lass�tsa a lek�rdez�st
                /// 
                Extended_EREC_IraIktatoKonyvekSearch.ErvKezd.Clear();
                Extended_EREC_IraIktatoKonyvekSearch.ErvVege.Clear();
            }
        }

        public EREC_IraIktatoKonyvekSearch Extended_EREC_IraIktatoKonyvekSearch;

        #region Egy�b, manu�lis mez�k


        private void Init_ManualFields()
        {
            // TODO: manual fields


        }

        #endregion


    }
}
