﻿using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public class EREC_UgyUgyiratokCalendarViewSearch: EREC_UgyUgyiratokSearch
    {
        private DateTime _DatumTol;

        public DateTime DatumTol
        {
            get
            {
                return _DatumTol;
            }
            set
            {
                _DatumTol = value;
            }
        }

        private DateTime _DatumIg;

        public DateTime DatumIg
        {
            get
            {
                return _DatumIg;
            }
            set
            {
                _DatumIg = value;
            }
        }

        private Guid _UgyintezoId;

        public Guid UgyintezoId
        {
            get
            {
                return _UgyintezoId;
            }
            set
            {
                _UgyintezoId = value;
            }
        }
    }
}
