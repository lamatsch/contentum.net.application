using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class EREC_UgyUgyiratokSearch
    {
        public EREC_UgyUgyiratokSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_EREC_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
                Extended_EREC_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();
                Extended_EREC_IraIratokSearch = new EREC_IraIratokSearch();
                Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
                Extended_KRT_MappakSearch = new KRT_MappakSearch();
                Extended_EREC_IrattariKikeroSearch = new EREC_IrattariKikeroSearch();
                Extended_EREC_IratMetaDefinicioSearch = new EREC_IratMetaDefinicioSearch();

                /// alapb�l kivessz�k az �rv�nyess�gre val� sz�r�st, hogy ha m�s sz�r�si felt�tel nincs megadva ezekn�l,
                /// ne gener�ljon semmit, hogy feleslegesen ne lass�tsa a lek�rdez�st
                /// 

                Extended_EREC_KuldKuldemenyekSearch.ErvKezd.Clear();
                Extended_EREC_KuldKuldemenyekSearch.ErvVege.Clear();

                Extended_EREC_UgyUgyiratdarabokSearch.ErvKezd.Clear();
                Extended_EREC_UgyUgyiratdarabokSearch.ErvVege.Clear();

                Extended_EREC_IraIratokSearch.ErvKezd.Clear();
                Extended_EREC_IraIratokSearch.ErvVege.Clear();

                Extended_EREC_IraIktatoKonyvekSearch.ErvKezd.Clear();
                Extended_EREC_IraIktatoKonyvekSearch.ErvVege.Clear();

                Extended_KRT_MappakSearch.ErvKezd.Clear();
                Extended_KRT_MappakSearch.ErvVege.Clear();

                Extended_EREC_IrattariKikeroSearch.ErvKezd.Clear();
                Extended_EREC_IrattariKikeroSearch.ErvVege.Clear();

                Extended_EREC_IratMetaDefinicioSearch.ErvKezd.Clear();
                Extended_EREC_IratMetaDefinicioSearch.ErvVege.Clear();
            }
            //SetDefaultAktivUIFilter();
        }

        public EREC_KuldKuldemenyekSearch Extended_EREC_KuldKuldemenyekSearch;

        public EREC_UgyUgyiratdarabokSearch Extended_EREC_UgyUgyiratdarabokSearch;

        public EREC_IraIratokSearch Extended_EREC_IraIratokSearch;

        public EREC_IraIktatoKonyvekSearch Extended_EREC_IraIktatoKonyvekSearch;

        public KRT_MappakSearch Extended_KRT_MappakSearch;

        [Contentum.eQuery.Base.AddToWhere]
        public EREC_IrattariKikeroSearch Extended_EREC_IrattariKikeroSearch;

        public EREC_IratMetaDefinicioSearch Extended_EREC_IratMetaDefinicioSearch;

        #region Egy�b, manu�lis mez�k


        private void Init_ManualFields()
        {
            _Manual_LetrehozasIdo.Name = "EREC_UgyUgyiratok.LetrehozasIdo";
            _Manual_LetrehozasIdo.Type = "DateTime";

            _Manual_ModositasIdo.Name = "EREC_UgyUgyiratok.ModositasIdo";
            _Manual_ModositasIdo.Type = "DateTime";

            _Manual_Sajat.Name = "EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo";
            _Manual_Sajat.Type = "Guid";

            _Manual_Sajat_Allapot.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Sajat_Allapot.Type = "String";

            _Manual_Sajat_TovabbitasAlattAllapot.Name = "EREC_UgyUgyiratok.TovabbitasAlattAllapot";
            _Manual_Sajat_TovabbitasAlattAllapot.Type = "String";

            _Manual_Jovahagyandok.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Jovahagyandok.Type = "String";

            _Manual_Jovahagyo.Name = "EREC_UgyUgyiratok.Kovetkezo_Felelos_Id";
            _Manual_Jovahagyo.Type = "Guid";

            _Manual_Elo_Ugyiratok_Allapot.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Elo_Ugyiratok_Allapot.Type = "String";

            _Manual_Elo_Ugyiratok_TovabbitasAlattAllapot.Name = "EREC_UgyUgyiratok.TovabbitasAlattAllapot";
            _Manual_Elo_Ugyiratok_TovabbitasAlattAllapot.Type = "String";

            _Manual_Lezartak.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Lezartak.Type = "String";

            _Manual_Hataridoben.Name = "EREC_UgyUgyiratok.Hatarido";
            _Manual_Hataridoben.Type = "DateTime";

            _Manual_Skontroban.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Skontroban.Type = "String";

            _Manual_Sztornozottak.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Sztornozottak.Type = "String";

            _Manual_Hataridon_tul.Name = "EREC_UgyUgyiratok.Hatarido";
            _Manual_Hataridon_tul.Type = "DateTime";

            _Manual_Elintezett.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Elintezett.Type = "String";

            _Manual_Szereltek.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Szereltek.Type = "String";

            _Manual_SzereltekNelkul.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_SzereltekNelkul.Type = "String";

            _Manual_SzerelesreElokeszitettek.Name = "EREC_UgyUgyiratok_ElokeszitettSzerelesCount.ElokeszitettSzerelesCount";
            _Manual_SzerelesreElokeszitettek.Type = "String";

            _Manual_Szerelendok.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Szerelendok.Type = "String";

            _Manual_HataridoElottXNappal.Name = "EREC_UgyUgyiratok.Hatarido";
            _Manual_HataridoElottXNappal.Type = "DateTime";

            _Manual_AlszamraIktathatoak.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_AlszamraIktathatoak.Type = "String";

            _Manual_Allapot_DefaultFilter.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Allapot_DefaultFilter.Type = "String";

            // m�sodlagos sz�r�shez, pl. Ugyirat.FilterObject ExcludeAllapot felt�telhez
            _Manual_Allapot_Filter.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Allapot_Filter.Type = "String";

            _Manual_IrattariJel.Name = "EREC_IraIrattariTetelek.IrattariJel";
            _Manual_IrattariJel.Type = "String";

            _Manual_Szereles_Alatt.Name = "EREC_UgyUgyiratok.TovabbitasAlattAllapot";
            _Manual_Szereles_Alatt.Type = "String";

            _Manual_Csatolhato.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_Csatolhato.Type = "String";

            _Manual_Foszam_MunkaanyagFilter.Name = "EREC_UgyUgyiratok.Foszam";
            _Manual_Foszam_MunkaanyagFilter.Type = "Int32";

            _Manual_TovabbitasAlattiakNelkul.Name = "EREC_UgyUgyiratok.Allapot";
            _Manual_TovabbitasAlattiakNelkul.Type = "String";

        }

        private Field _Manual_LetrehozasIdo = new Field();

        public Field Manual_LetrehozasIdo
        {
            get { return _Manual_LetrehozasIdo; }
            set { _Manual_LetrehozasIdo = value; }
        }

        private Field _Manual_ModositasIdo = new Field();

        public Field Manual_ModositasIdo
        {
            get { return _Manual_ModositasIdo; }
            set { _Manual_ModositasIdo = value; }
        }

        private Field _Manual_Sajat = new Field();

        public Field Manual_Sajat
        {
            get { return _Manual_Sajat; }
            set { _Manual_Sajat = value; }
        }

        private Field _Manual_Sajat_Allapot = new Field();

        public Field Manual_Sajat_Allapot
        {
            get { return _Manual_Sajat_Allapot; }
            set { _Manual_Sajat_Allapot = value; }
        }

        private Field _Manual_Sajat_TovabbitasAlattAllapot = new Field();

        public Field Manual_Sajat_TovabbitasAlattAllapot
        {
            get { return _Manual_Sajat_TovabbitasAlattAllapot; }
            set { _Manual_Sajat_TovabbitasAlattAllapot = value; }
        }

        private Field _Manual_Jovahagyandok = new Field();

        public Field Manual_Jovahagyandok
        {
            get { return _Manual_Jovahagyandok; }
            set { _Manual_Jovahagyandok = value; }
        }

        private Field _Manual_Jovahagyo = new Field();

        public Field Manual_Jovahagyo
        {
            get { return _Manual_Jovahagyo; }
            set { _Manual_Jovahagyo = value; }
        }

        private Field _Manual_Elo_Ugyiratok_Allapot = new Field();

        public Field Manual_Elo_Ugyiratok_Allapot
        {
            get { return _Manual_Elo_Ugyiratok_Allapot; }
            set { _Manual_Elo_Ugyiratok_Allapot = value; }
        }

        private Field _Manual_Elo_Ugyiratok_TovabbitasAlattAllapot = new Field();

        public Field Manual_Elo_Ugyiratok_TovabbitasAlattAllapot
        {
            get { return _Manual_Elo_Ugyiratok_TovabbitasAlattAllapot; }
            set { _Manual_Elo_Ugyiratok_TovabbitasAlattAllapot = value; }
        }

        private Field _Manual_Lezartak = new Field();

        public Field Manual_Lezartak
        {
            get { return _Manual_Lezartak; }
            set { _Manual_Lezartak = value; }
        }

        private Field _Manual_Hataridoben = new Field();

        public Field Manual_Hataridoben
        {
            get { return _Manual_Hataridoben; }
            set { _Manual_Hataridoben = value; }
        }

        private Field _Manual_Skontroban = new Field();

        public Field Manual_Skontroban
        {
            get { return _Manual_Skontroban; }
            set { _Manual_Skontroban = value; }
        }

        private Field _Manual_Sztornozottak = new Field();

        public Field Manual_Sztornozottak
        {
            get { return _Manual_Sztornozottak; }
            set { _Manual_Sztornozottak = value; }
        }

        private Field _Manual_Hataridon_tul = new Field();

        public Field Manual_Hataridon_tul
        {
            get { return _Manual_Hataridon_tul; }
            set { _Manual_Hataridon_tul = value; }
        }

        private Field _Manual_Elintezett = new Field();

        public Field Manual_Elintezett
        {
            get { return _Manual_Elintezett; }
            set { _Manual_Elintezett = value; }
        }

        private Field _Manual_Szereltek = new Field();

        public Field Manual_Szereltek
        {
            get { return _Manual_Szereltek; }
            set { _Manual_Szereltek = value; }
        }

        private Field _Manual_SzereltekNelkul = new Field();

        public Field Manual_SzereltekNelkul
        {
            get { return _Manual_SzereltekNelkul; }
            set { _Manual_SzereltekNelkul = value; }
        }

        private Field _Manual_SzerelesreElokeszitettek = new Field();

        public Field Manual_SzerelesreElokeszitettek
        {
            get { return _Manual_SzerelesreElokeszitettek; }
            set { _Manual_SzerelesreElokeszitettek = value; }
        }

        private Field _Manual_Szerelendok = new Field();

        public Field Manual_Szerelendok
        {
            get { return _Manual_Szerelendok; }
            set { _Manual_Szerelendok = value; }
        }

        private Field _Manual_HataridoElottXNappal = new Field();

        public Field Manual_HataridoElottXNappal
        {
            get { return _Manual_HataridoElottXNappal; }
            set { _Manual_HataridoElottXNappal = value; }
        }


        private Field _Manual_AlszamraIktathatoak = new Field();

        public Field Manual_AlszamraIktathatoak
        {
            get { return _Manual_AlszamraIktathatoak; }
            set { _Manual_AlszamraIktathatoak = value; }
        }



        private Field _Manual_Allapot_DefaultFilter = new Field();

        public Field Manual_Allapot_DefaultFilter
        {
            get { return _Manual_Allapot_DefaultFilter; }
            set { _Manual_Allapot_DefaultFilter = value; }
        }

        private Field _Manual_Allapot_Filter = new Field();

        public Field Manual_Allapot_Filter
        {
            get { return _Manual_Allapot_Filter; }
            set { _Manual_Allapot_Filter = value; }
        }

        private Field _Manual_IrattariJel = new Field();

        public Field Manual_IrattariJel
        {
            get { return _Manual_IrattariJel; }
            set { _Manual_IrattariJel = value; }
        }
        private Field _Manual_Szereles_Alatt = new Field();

        public Field Manual_Szereles_Alatt
        {
            get { return _Manual_Szereles_Alatt; }
            set { _Manual_Szereles_Alatt = value; }
        }

        private Field _Manual_Csatolhato = new Field();

        public Field Manual_Csatolhato
        {
            get { return _Manual_Csatolhato; }
            set { _Manual_Csatolhato = value; }
        }


        private Field _Manual_Foszam_MunkaanyagFilter = new Field();

        public Field Manual_Foszam_MunkaanyagFilter
        {
            get { return _Manual_Foszam_MunkaanyagFilter; }
            set { _Manual_Foszam_MunkaanyagFilter = value; }
        }

        private Field _Manual_TovabbitasAlattiakNelkul = new Field();

        public Field Manual_TovabbitasAlattiakNelkul
        {
            get { return _Manual_TovabbitasAlattiakNelkul; }
            set { _Manual_TovabbitasAlattiakNelkul = value; }
        }



        #endregion

        public FullTextSearchField fts_targyszavak;

        public FullTextSearchField fts_ertek;

        public FullTextSearchField fts_note;

        // CR#2035: feleslegess� v�lt, mert az EREC_UgyUgyiratok.Targy FTSString t�pus�
        //public FullTextSearchField fts_targy;

        public Contentum.eQuery.FullTextSearch.FullTextSearchTree fts_tree_ugyirat_auto = null;

        public Contentum.eQuery.FullTextSearch.FullTextSearchTree fts_tree_ugyirat_egyeb = null;

        // sz�r�felt�tel az objektumok t�rgyszavaiban l�v� Obj_Id-ekre
        // pl. SELECT Obj_Id FROM EREC_ObjektumTargyszavai WHERE ...
        public string ObjektumTargyszavai_ObjIdFilter = "";

        // az �gyirat �s irat t�rgy�ban, valamint a bek�ld� nev�ben val� k�z�s keres�si
        // kifejez�s, ami VAGY kapcsolattal �rtend�en sz�ri az �gyiratokat
        public FullTextSearchField fts_altalanos;

        //public FullTextSearchField FtsTargySzavak
        //{
        //    get { return fts_targyszavak; }
        //    set { fts_targyszavak = value; }
        //}

        // CR#1548: K�l�n sz�r�s a tov�bb�t�s alatt l�v� �gyiratok �llapot�ra
        // (alap�rtelmez�sben ezek is j�nnek a list�kon, de ezzel a flaggel kikapcsolhat�)
        // Nem ker�l be a Where felt�telbe, csak a be�ll�t�s megjegyz�s�t �s oldalak k�z�tti �tvitel�t szolg�lja
        // Felhaszn�l�sa: ha �rt�ke true, a megadott �llapot az Allapot �s TovabbitasAlattAllapot mez�kben is keresend� (OR)
        // egy�bk�nt csak az Allapot mez�ben (UgyUgyiratokSearch.aspx haszn�lja)
        private bool isTovabbitasAlattAllapotIncluded = true;

        public bool IsTovabbitasAlattAllapotIncluded
        {
            get { return isTovabbitasAlattAllapotIncluded; }
            set
            {
                if (isTovabbitasAlattAllapotIncluded != value)
                {
                    isTovabbitasAlattAllapotIncluded = value;
                    if (!value)
                    {
                        Manual_TovabbitasAlattiakNelkul.Value = "50"; //Tov�bb�t�s alatt �llapot
                        Manual_TovabbitasAlattiakNelkul.Operator = Query.Operators.notequals;
                    }
                    else
                    {
                        Manual_TovabbitasAlattiakNelkul.Clear();
                    }
                }
            }
        }

        private bool isSzereltekExcluded = false;

        public bool IsSzereltekExcluded
        {
            get { return isSzereltekExcluded; }
            set
            {
                if (isSzereltekExcluded != value)
                {
                    isSzereltekExcluded = value;
                    if (value)
                    {
                        Manual_SzereltekNelkul.Value = "60"; //Szerelt �llapot
                        Manual_SzereltekNelkul.Operator = Query.Operators.notequals;
                    }
                    else
                    {
                        Manual_SzereltekNelkul.Clear();
                    }
                }
            }
        }

        private string _AlSzervezetId;

        public string AlSzervezetId
        {
            get { return _AlSzervezetId; }
            set { _AlSzervezetId = value; }
        }

        bool _Csoporttagokkal = true;

        public bool Csoporttagokkal
        {
            get
            {
                return _Csoporttagokkal;
            }
            set
            {
                _Csoporttagokkal = value;
            }
        }

        bool _CsakAktivIrat = false;

        public bool CsakAktivIrat
        {
            get
            {
                return _CsakAktivIrat;
            }
            set
            {
                _CsakAktivIrat = value;
            }
        }

        public void SetDefaultUIFilter()
        {
            this.Csoporttagokkal = false;
            this.CsakAktivIrat = true;
        }

        private int _NaponBelulNincsUjAlszam = -1;

        public int NaponBelulNincsUjAlszam
        {
            get
            {
                return _NaponBelulNincsUjAlszam;
            }
            set
            {
                _NaponBelulNincsUjAlszam = value;
            }
        }

        public void SetDefaultAktivUIFilter()
        {
            this.CsakAktivIrat = true;
        }

        public string UgyUgyiratSearchUgyintezoGuid { get; set; }
    }
}
