
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
        public partial class EREC_eMailBoritekokSearch    {

        #region Egy�b, manu�lis mez�k

        private void Init_ManualFields()
        {
            // CR#2152: hat�konys�gn�vel�s miatt OR helyet k�t JOIN, ez�rt speci�lis "mez�n�v"
            _Manual_IraIrat_Id.Name = "coalesce(EREC_IraIratok_Irat.Id,EREC_IraIratok_Kuld.Id)"; // "EREC_IraIratok.Id";
            _Manual_IraIrat_Id.Type = "Guid";
        }

        /// <summary>
        /// Iktatott email eset�n vagy az IraIrat_Id van kit�ltve, vagy a k�ldem�nyen kereszt�l lehet csak eljutni az iratig
        /// </summary>
        private Field _Manual_IraIrat_Id = new Field();

        public Field Manual_IraIrat_Id
        {
            get { return _Manual_IraIrat_Id; }
            set { _Manual_IraIrat_Id = value; }
        }

        }
        #endregion
}