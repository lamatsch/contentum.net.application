
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// Fact_Feladatok eQuery.BusinessDocument Class </summary>
    public partial class Fact_FeladatokSearch
    {
        public Fact_FeladatokSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_Dim_LejaratKategoriaSearch = new Dim_LejaratKategoriaSearch();
                Extended_Dim_PrioritasSearch = new Dim_PrioritasSearch();
            }

        }

        Dim_LejaratKategoriaSearch Extended_Dim_LejaratKategoriaSearch;
        Dim_PrioritasSearch Extended_Dim_PrioritasSearch;

        private void Init_ManualFields()
        {
        // do nothing at the moment
        }
    }

}