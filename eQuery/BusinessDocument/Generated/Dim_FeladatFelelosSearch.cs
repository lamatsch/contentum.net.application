
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// Dim_FeladatFelelos eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class Dim_FeladatFelelosSearch: BaseSearchObject    {
      public Dim_FeladatFelelosSearch()
      {            
                     _Id.Name = "Dim_FeladatFelelos.Id";
               _Id.Type = "Int32";            
               _FelelosTipusKod.Name = "Dim_FeladatFelelos.FelelosTipusKod";
               _FelelosTipusKod.Type = "Char";            
               _FelelosTipusNev.Name = "Dim_FeladatFelelos.FelelosTipusNev";
               _FelelosTipusNev.Type = "String";            
               _Szervezet_Id.Name = "Dim_FeladatFelelos.Szervezet_Id";
               _Szervezet_Id.Type = "Guid";            
               _SzervezetNev.Name = "Dim_FeladatFelelos.SzervezetNev";
               _SzervezetNev.Type = "String";            
               _Felhasznalo_Id.Name = "Dim_FeladatFelelos.Felhasznalo_Id";
               _Felhasznalo_Id.Type = "Guid";            
               _FelhasznaloNev.Name = "Dim_FeladatFelelos.FelhasznaloNev";
               _FelhasznaloNev.Type = "String";            
               _FrissitesIdo.Name = "Dim_FeladatFelelos.FrissitesIdo";
               _FrissitesIdo.Type = "DateTime";            
               _Modositas.Name = "Dim_FeladatFelelos.Modositas";
               _Modositas.Type = "DateTime";            
               _ETL_Load_Id.Name = "Dim_FeladatFelelos.ETL_Load_Id";
               _ETL_Load_Id.Type = "Int32";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FelelosTipusKod = new Field();

        public Field FelelosTipusKod
        {
            get { return _FelelosTipusKod; }
            set { _FelelosTipusKod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FelelosTipusNev = new Field();

        public Field FelelosTipusNev
        {
            get { return _FelelosTipusNev; }
            set { _FelelosTipusNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Szervezet_Id = new Field();

        public Field Szervezet_Id
        {
            get { return _Szervezet_Id; }
            set { _Szervezet_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SzervezetNev = new Field();

        public Field SzervezetNev
        {
            get { return _SzervezetNev; }
            set { _SzervezetNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id = new Field();

        public Field Felhasznalo_Id
        {
            get { return _Felhasznalo_Id; }
            set { _Felhasznalo_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FelhasznaloNev = new Field();

        public Field FelhasznaloNev
        {
            get { return _FelhasznaloNev; }
            set { _FelhasznaloNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FrissitesIdo = new Field();

        public Field FrissitesIdo
        {
            get { return _FrissitesIdo; }
            set { _FrissitesIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Modositas = new Field();

        public Field Modositas
        {
            get { return _Modositas; }
            set { _Modositas = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ETL_Load_Id = new Field();

        public Field ETL_Load_Id
        {
            get { return _ETL_Load_Id; }
            set { _ETL_Load_Id = value; }
        }
                              }

}