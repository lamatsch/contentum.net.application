
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// Dim_LejaratKategoria eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class Dim_LejaratKategoriaSearch: BaseSearchObject    {
      public Dim_LejaratKategoriaSearch()
      {            
                     _Id.Name = "Dim_LejaratKategoria.Id";
               _Id.Type = "Int32";            
               _Sorrend.Name = "Dim_LejaratKategoria.Sorrend";
               _Sorrend.Type = "Int32";            
               _TetelJel.Name = "Dim_LejaratKategoria.TetelJel";
               _TetelJel.Type = "Int32";            
               _LejaratKod.Name = "Dim_LejaratKategoria.LejaratKod";
               _LejaratKod.Type = "Char";            
               _LejaratNev.Name = "Dim_LejaratKategoria.LejaratNev";
               _LejaratNev.Type = "String";            
               _ElteresErtek.Name = "Dim_LejaratKategoria.ElteresErtek";
               _ElteresErtek.Type = "Int32";            
               _ElteresNev.Name = "Dim_LejaratKategoria.ElteresNev";
               _ElteresNev.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Sorrend = new Field();

        public Field Sorrend
        {
            get { return _Sorrend; }
            set { _Sorrend = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _TetelJel = new Field();

        public Field TetelJel
        {
            get { return _TetelJel; }
            set { _TetelJel = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LejaratKod = new Field();

        public Field LejaratKod
        {
            get { return _LejaratKod; }
            set { _LejaratKod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LejaratNev = new Field();

        public Field LejaratNev
        {
            get { return _LejaratNev; }
            set { _LejaratNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ElteresErtek = new Field();

        public Field ElteresErtek
        {
            get { return _ElteresErtek; }
            set { _ElteresErtek = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ElteresNev = new Field();

        public Field ElteresNev
        {
            get { return _ElteresNev; }
            set { _ElteresNev = value; }
        }
                              }

}