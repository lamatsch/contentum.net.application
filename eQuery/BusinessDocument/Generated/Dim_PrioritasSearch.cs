
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// Dim_Prioritas eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class Dim_PrioritasSearch: BaseSearchObject    {
      public Dim_PrioritasSearch()
      {            
                     _Id.Name = "Dim_Prioritas.Id";
               _Id.Type = "Int32";            
               _PrioritasSzint.Name = "Dim_Prioritas.PrioritasSzint";
               _PrioritasSzint.Type = "Int32";            
               _PrioritasSzintNev.Name = "Dim_Prioritas.PrioritasSzintNev";
               _PrioritasSzintNev.Type = "String";            
               _Prioritas.Name = "Dim_Prioritas.Prioritas";
               _Prioritas.Type = "Int32";
               _PrioritasNev.Name = "Dim_Prioritas.PrioritasNev";
               _PrioritasNev.Type = "String";      
               _ETL_Load_Id.Name = "Dim_Prioritas.ETL_Load_Id";
               _ETL_Load_Id.Type = "Int32";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _PrioritasSzint = new Field();

        public Field PrioritasSzint
        {
            get { return _PrioritasSzint; }
            set { _PrioritasSzint = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _PrioritasSzintNev = new Field();

        public Field PrioritasSzintNev
        {
            get { return _PrioritasSzintNev; }
            set { _PrioritasSzintNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Prioritas = new Field();

        public Field Prioritas
        {
            get { return _Prioritas; }
            set { _Prioritas = value; }
        }

        /// <summary>
        /// Id property </summary>
        private Field _PrioritasNev = new Field();

        public Field PrioritasNev
        {
            get { return _PrioritasNev; }
            set { _PrioritasNev = value; }
        }

        /// <summary>
        /// Id property </summary>
        private Field _ETL_Load_Id = new Field();

        public Field ETL_Load_Id
        {
            get { return _ETL_Load_Id; }
            set { _ETL_Load_Id = value; }
        }
                              }

}