
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EDOC_FelhasznaloSpecialisMappa eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class EDOC_FelhasznaloSpecialisMappaSearch
    {
      public EDOC_FelhasznaloSpecialisMappaSearch()
      {               _Id.Name = "EDOC_FelhasznaloSpecialisMappa.Id";
               _Id.Type = "Guid";            
               _Felhasznalo_Id.Name = "EDOC_FelhasznaloSpecialisMappa.Felhasznalo_Id";
               _Felhasznalo_Id.Type = "Guid";            
               _Tipus.Name = "EDOC_FelhasznaloSpecialisMappa.Tipus";
               _Tipus.Type = "Char";            
               _Tipus_Id.Name = "EDOC_FelhasznaloSpecialisMappa.Tipus_Id";
               _Tipus_Id.Type = "Guid";            
               _Mappa_Id.Name = "EDOC_FelhasznaloSpecialisMappa.Mappa_Id";
               _Mappa_Id.Type = "Guid";            
               _Mapp_Azonosito.Name = "EDOC_FelhasznaloSpecialisMappa.Mapp_Azonosito";
               _Mapp_Azonosito.Type = "String";            
               _Leiras.Name = "EDOC_FelhasznaloSpecialisMappa.Leiras";
               _Leiras.Type = "String";            
               _ErvKezd.Name = "EDOC_FelhasznaloSpecialisMappa.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EDOC_FelhasznaloSpecialisMappa.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id = new Field();

        public Field Felhasznalo_Id
        {
            get { return _Felhasznalo_Id; }
            set { _Felhasznalo_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus_Id = new Field();

        public Field Tipus_Id
        {
            get { return _Tipus_Id; }
            set { _Tipus_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Mappa_Id = new Field();

        public Field Mappa_Id
        {
            get { return _Mappa_Id; }
            set { _Mappa_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Mapp_Azonosito = new Field();

        public Field Mapp_Azonosito
        {
            get { return _Mapp_Azonosito; }
            set { _Mapp_Azonosito = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}