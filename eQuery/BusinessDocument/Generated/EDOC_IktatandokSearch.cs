
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EDOC_Iktatandok eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class EDOC_IktatandokSearch
    {
      public EDOC_IktatandokSearch()
      {               _Id.Name = "EDOC_Iktatandok.Id";
               _Id.Type = "Guid";            
               _Csoport_Id_Felelos.Name = "EDOC_Iktatandok.Csoport_Id_Felelos";
               _Csoport_Id_Felelos.Type = "Guid";            
               _Leiras.Name = "EDOC_Iktatandok.Leiras";
               _Leiras.Type = "String";            
               _Felhasznalo_Id_IktatastKero.Name = "EDOC_Iktatandok.Felhasznalo_Id_IktatastKero";
               _Felhasznalo_Id_IktatastKero.Type = "Guid";            
               _IraIratok_Id.Name = "EDOC_Iktatandok.IraIratok_Id";
               _IraIratok_Id.Type = "Guid";            
               _ErvKezd.Name = "EDOC_Iktatandok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EDOC_Iktatandok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Felelos = new Field();

        public Field Csoport_Id_Felelos
        {
            get { return _Csoport_Id_Felelos; }
            set { _Csoport_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id_IktatastKero = new Field();

        public Field Felhasznalo_Id_IktatastKero
        {
            get { return _Felhasznalo_Id_IktatastKero; }
            set { _Felhasznalo_Id_IktatastKero = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IraIratok_Id = new Field();

        public Field IraIratok_Id
        {
            get { return _IraIratok_Id; }
            set { _IraIratok_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}