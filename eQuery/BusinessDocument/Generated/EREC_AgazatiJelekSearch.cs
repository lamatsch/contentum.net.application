
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_AgazatiJelek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_AgazatiJelekSearch: BaseSearchObject    {
      public EREC_AgazatiJelekSearch()
      {            
                     _Id.Name = "EREC_AgazatiJelek.Id";
               _Id.Type = "Guid";            
               _Org.Name = "EREC_AgazatiJelek.Org";
               _Org.Type = "Guid";            
               _Kod.Name = "EREC_AgazatiJelek.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "EREC_AgazatiJelek.Nev";
               _Nev.Type = "String";            
               _AgazatiJel_Id.Name = "EREC_AgazatiJelek.AgazatiJel_Id";
               _AgazatiJel_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_AgazatiJelek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_AgazatiJelek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Kod property
        /// �gazat k�d
        /// </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// Megnevez�s
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// AgazatiJel_Id property
        /// AgazatiJel_Id fk
        /// </summary>
        private Field _AgazatiJel_Id = new Field();

        public Field AgazatiJel_Id
        {
            get { return _AgazatiJel_Id; }
            set { _AgazatiJel_Id = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}