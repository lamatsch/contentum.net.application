
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Alairas_Folyamat_Tetelek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_Alairas_Folyamat_TetelekSearch: BaseSearchObject    {
      public EREC_Alairas_Folyamat_TetelekSearch()
      {            
                     _Id.Name = "EREC_Alairas_Folyamat_Tetelek.Id";
               _Id.Type = "Guid";            
               _AlairasFolyamat_Id.Name = "EREC_Alairas_Folyamat_Tetelek.AlairasFolyamat_Id";
               _AlairasFolyamat_Id.Type = "Guid";            
               _IraIrat_Id.Name = "EREC_Alairas_Folyamat_Tetelek.IraIrat_Id";
               _IraIrat_Id.Type = "Guid";            
               _Csatolmany_Id.Name = "EREC_Alairas_Folyamat_Tetelek.Csatolmany_Id";
               _Csatolmany_Id.Type = "Guid";            
               _AlairasStatus.Name = "EREC_Alairas_Folyamat_Tetelek.AlairasStatus";
               _AlairasStatus.Type = "String";            
               _Hiba.Name = "EREC_Alairas_Folyamat_Tetelek.Hiba";
               _Hiba.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// AlairasFolyamat_Id property
        /// 
        /// </summary>
        private Field _AlairasFolyamat_Id = new Field();

        public Field AlairasFolyamat_Id
        {
            get { return _AlairasFolyamat_Id; }
            set { _AlairasFolyamat_Id = value; }
        }
                          
           
        /// <summary>
        /// IraIrat_Id property
        /// 
        /// </summary>
        private Field _IraIrat_Id = new Field();

        public Field IraIrat_Id
        {
            get { return _IraIrat_Id; }
            set { _IraIrat_Id = value; }
        }
                          
           
        /// <summary>
        /// Csatolmany_Id property
        /// 
        /// </summary>
        private Field _Csatolmany_Id = new Field();

        public Field Csatolmany_Id
        {
            get { return _Csatolmany_Id; }
            set { _Csatolmany_Id = value; }
        }
                          
           
        /// <summary>
        /// AlairasStatus property
        /// 
        /// </summary>
        private Field _AlairasStatus = new Field();

        public Field AlairasStatus
        {
            get { return _AlairasStatus; }
            set { _AlairasStatus = value; }
        }
                          
           
        /// <summary>
        /// Hiba property
        /// 
        /// </summary>
        private Field _Hiba = new Field();

        public Field Hiba
        {
            get { return _Hiba; }
            set { _Hiba = value; }
        }
                              }

}