
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Alairas_Folyamatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_Alairas_FolyamatokSearch: BaseSearchObject    {
      public EREC_Alairas_FolyamatokSearch()
      {            
                     _Id.Name = "EREC_Alairas_Folyamatok.Id";
               _Id.Type = "Guid";            
               _AlairasStarted.Name = "EREC_Alairas_Folyamatok.AlairasStarted";
               _AlairasStarted.Type = "DateTime";            
               _AlairasStopped.Name = "EREC_Alairas_Folyamatok.AlairasStopped";
               _AlairasStopped.Type = "DateTime";            
               _AlairasStatus.Name = "EREC_Alairas_Folyamatok.AlairasStatus";
               _AlairasStatus.Type = "String";            
               _ErvKezd.Name = "EREC_Alairas_Folyamatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Alairas_Folyamatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// AlairasStarted property
        /// 
        /// </summary>
        private Field _AlairasStarted = new Field();

        public Field AlairasStarted
        {
            get { return _AlairasStarted; }
            set { _AlairasStarted = value; }
        }
                          
           
        /// <summary>
        /// AlairasStopped property
        /// 
        /// </summary>
        private Field _AlairasStopped = new Field();

        public Field AlairasStopped
        {
            get { return _AlairasStopped; }
            set { _AlairasStopped = value; }
        }
                          
           
        /// <summary>
        /// AlairasStatus property
        /// 
        /// </summary>
        private Field _AlairasStatus = new Field();

        public Field AlairasStatus
        {
            get { return _AlairasStatus; }
            set { _AlairasStatus = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}