
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Csatolmanyok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_CsatolmanyokSearch: BaseSearchObject    {
      public EREC_CsatolmanyokSearch()
      {
          Init_ManualFields();

                     _Id.Name = "EREC_Csatolmanyok.Id";
               _Id.Type = "Guid";            
               _KuldKuldemeny_Id.Name = "EREC_Csatolmanyok.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _IraIrat_Id.Name = "EREC_Csatolmanyok.IraIrat_Id";
               _IraIrat_Id.Type = "Guid";            
               _Dokumentum_Id.Name = "EREC_Csatolmanyok.Dokumentum_Id";
               _Dokumentum_Id.Type = "Guid";            
               _Leiras.Name = "EREC_Csatolmanyok.Leiras";
               _Leiras.Type = "String";            
               _Lapszam.Name = "EREC_Csatolmanyok.Lapszam";
               _Lapszam.Type = "Int32";            
               _KapcsolatJelleg.Name = "EREC_Csatolmanyok.KapcsolatJelleg";
               _KapcsolatJelleg.Type = "String";            
               _DokumentumSzerep.Name = "EREC_Csatolmanyok.DokumentumSzerep";
               _DokumentumSzerep.Type = "String";            
               _IratAlairoJel.Name = "EREC_Csatolmanyok.IratAlairoJel";
               _IratAlairoJel.Type = "Char";            
               _CustomId.Name = "EREC_Csatolmanyok.CustomId";
               _CustomId.Type = "String";            
               _ErvKezd.Name = "EREC_Csatolmanyok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Csatolmanyok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// K�ldem�nyazonos�t�, k�ldem�nyhez kapcsol�d� csatolm�ny eset�n.
        /// </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// IraIrat_Id property
        /// Irat Id
        /// </summary>
        private Field _IraIrat_Id = new Field();

        public Field IraIrat_Id
        {
            get { return _IraIrat_Id; }
            set { _IraIrat_Id = value; }
        }
                          
           
        /// <summary>
        /// Dokumentum_Id property
        /// Dokumentum Id
        /// </summary>
        private Field _Dokumentum_Id = new Field();

        public Field Dokumentum_Id
        {
            get { return _Dokumentum_Id; }
            set { _Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// Leiras property
        /// A dokumentum kapcsolasahoz tartozo szabad szoveg.
        /// </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Lapszam property
        /// NEM HASZN�LT!
        /// Lapsz�m
        /// </summary>
        private Field _Lapszam = new Field();

        public Field Lapszam
        {
            get { return _Lapszam; }
            set { _Lapszam = value; }
        }
                          
           
        /// <summary>
        /// KapcsolatJelleg property
        /// Az irat �s a dokumentum k�z�tti kapcsolat jellege. KCS: DOKUMENTUM_KAPCSOLAT
        /// 1 - Alapkapcsolat (az irathoz alapb�l kapcsol�d� dokumentum)
        /// 2 - Duplik�lt kapcsolat (alapkapcsolattal rendelkez� dokumentum kapcsol�d�sa m�sik irathoz, fizikai szinten is - pl. m�solat)
        /// 3 - Hivatkoz�s (alapkapcsolattal rendekez� dokumentum kapcsol�d�sa m�sik irathoz, fizikai kapcsol�d�s n�lk�l)
        /// </summary>
        private Field _KapcsolatJelleg = new Field();

        public Field KapcsolatJelleg
        {
            get { return _KapcsolatJelleg; }
            set { _KapcsolatJelleg = value; }
        }
                          
           
        /// <summary>
        /// DokumentumSzerep property
        /// A dokumentum szerepe. KCS:DOKUMENTUM_SZEREP (F�dokumentum, Dokumentum mell�klet, Seg�ddokumentum)
        /// </summary>
        private Field _DokumentumSzerep = new Field();

        public Field DokumentumSzerep
        {
            get { return _DokumentumSzerep; }
            set { _DokumentumSzerep = value; }
        }
                          
           
        /// <summary>
        /// IratAlairoJel property
        /// A dokumentum al��rhat�s�ga az irat al��r�k �ltal, az adott kapcsolatban.
        /// 0 - nem al��rhat�
        /// 1 - al��rhat�
        /// </summary>
        private Field _IratAlairoJel = new Field();

        public Field IratAlairoJel
        {
            get { return _IratAlairoJel; }
            set { _IratAlairoJel = value; }
        }
                          
           
        /// <summary>
        /// CustomId property
        /// Sz�khely azonos�t�
        /// </summary>
        private Field _CustomId = new Field();

        public Field CustomId
        {
            get { return _CustomId; }
            set { _CustomId = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}