
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_FeladatDefinicio eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_FeladatDefinicioSearch: BaseSearchObject    {
      public EREC_FeladatDefinicioSearch()
      {            
                     _Id.Name = "EREC_FeladatDefinicio.Id";
               _Id.Type = "Guid";            
               _Org.Name = "EREC_FeladatDefinicio.Org";
               _Org.Type = "Guid";            
               _Funkcio_Id_Kivalto.Name = "EREC_FeladatDefinicio.Funkcio_Id_Kivalto";
               _Funkcio_Id_Kivalto.Type = "Guid";            
               _Obj_Metadefinicio_Id.Name = "EREC_FeladatDefinicio.Obj_Metadefinicio_Id";
               _Obj_Metadefinicio_Id.Type = "Guid";            
               _ObjStateValue_Id.Name = "EREC_FeladatDefinicio.ObjStateValue_Id";
               _ObjStateValue_Id.Type = "Guid";            
               _FeladatDefinicioTipus.Name = "EREC_FeladatDefinicio.FeladatDefinicioTipus";
               _FeladatDefinicioTipus.Type = "String";            
               _FeladatSorszam.Name = "EREC_FeladatDefinicio.FeladatSorszam";
               _FeladatSorszam.Type = "Int32";            
               _MuveletDefinicio.Name = "EREC_FeladatDefinicio.MuveletDefinicio";
               _MuveletDefinicio.Type = "String";            
               _SpecMuveletDefinicio.Name = "EREC_FeladatDefinicio.SpecMuveletDefinicio";
               _SpecMuveletDefinicio.Type = "String";            
               _Funkcio_Id_Inditando.Name = "EREC_FeladatDefinicio.Funkcio_Id_Inditando";
               _Funkcio_Id_Inditando.Type = "Guid";            
               _Funkcio_Id_Futtatando.Name = "EREC_FeladatDefinicio.Funkcio_Id_Futtatando";
               _Funkcio_Id_Futtatando.Type = "Guid";            
               _BazisObjLeiro.Name = "EREC_FeladatDefinicio.BazisObjLeiro";
               _BazisObjLeiro.Type = "String";            
               _FelelosTipus.Name = "EREC_FeladatDefinicio.FelelosTipus";
               _FelelosTipus.Type = "String";            
               _Obj_Id_Felelos.Name = "EREC_FeladatDefinicio.Obj_Id_Felelos";
               _Obj_Id_Felelos.Type = "Guid";            
               _FelelosLeiro.Name = "EREC_FeladatDefinicio.FelelosLeiro";
               _FelelosLeiro.Type = "String";            
               _Idobazis.Name = "EREC_FeladatDefinicio.Idobazis";
               _Idobazis.Type = "String";            
               _ObjTip_Id_DateCol.Name = "EREC_FeladatDefinicio.ObjTip_Id_DateCol";
               _ObjTip_Id_DateCol.Type = "Guid";            
               _AtfutasiIdo.Name = "EREC_FeladatDefinicio.AtfutasiIdo";
               _AtfutasiIdo.Type = "Int32";            
               _Idoegyseg.Name = "EREC_FeladatDefinicio.Idoegyseg";
               _Idoegyseg.Type = "String";            
               _FeladatLeiras.Name = "EREC_FeladatDefinicio.FeladatLeiras";
               _FeladatLeiras.Type = "String";            
               _Tipus.Name = "EREC_FeladatDefinicio.Tipus";
               _Tipus.Type = "String";            
               _Prioritas.Name = "EREC_FeladatDefinicio.Prioritas";
               _Prioritas.Type = "String";            
               _LezarasPrioritas.Name = "EREC_FeladatDefinicio.LezarasPrioritas";
               _LezarasPrioritas.Type = "String";            
               _Allapot.Name = "EREC_FeladatDefinicio.Allapot";
               _Allapot.Type = "String";            
               _ObjTip_Id.Name = "EREC_FeladatDefinicio.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Obj_type.Name = "EREC_FeladatDefinicio.Obj_type";
               _Obj_type.Type = "String";            
               _Obj_SzukitoFeltetel.Name = "EREC_FeladatDefinicio.Obj_SzukitoFeltetel";
               _Obj_SzukitoFeltetel.Type = "String";            
               _ErvKezd.Name = "EREC_FeladatDefinicio.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_FeladatDefinicio.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Funkcio_Id_Kivalto property
        /// Az �j funkci� id�t�s�t kiv�lt� el�zm�ny funkci�.
        /// </summary>
        private Field _Funkcio_Id_Kivalto = new Field();

        public Field Funkcio_Id_Kivalto
        {
            get { return _Funkcio_Id_Kivalto; }
            set { _Funkcio_Id_Kivalto = value; }
        }
                          
           
        /// <summary>
        /// Obj_Metadefinicio_Id property
        /// Objektum metadefin�ci� azonos�t�
        /// </summary>
        private Field _Obj_Metadefinicio_Id = new Field();

        public Field Obj_Metadefinicio_Id
        {
            get { return _Obj_Metadefinicio_Id; }
            set { _Obj_Metadefinicio_Id = value; }
        }
                          
           
        /// <summary>
        /// ObjStateValue_Id property
        /// Az objektum �llapota a az adott feladat v�grehajt�sa el�tt.
        /// </summary>
        private Field _ObjStateValue_Id = new Field();

        public Field ObjStateValue_Id
        {
            get { return _ObjStateValue_Id; }
            set { _ObjStateValue_Id = value; }
        }
                          
           
        /// <summary>
        /// FeladatDefinicioTipus property
        /// A feladat vagy �zenet gener�l�s felt�teleinek kateg�ri�i.
        /// KCS: FELADAT_DEFINICIO_TIPUS
        /// 1 - Esem�nyf�gg�
        /// 2 - Esem�ny- �s kateg�riaf�gg� (Obj_Metadefininicio_Id-vel defini�lt)
        /// 3 - Esem�ny-, kateg�ria- �s �llapotf�gg� (Obj_Metadefinicio_Id-vel �s ObjStateValue-val defini�lt)
        /// 4 - Esem�ny-, kateg�ria- �s folyamatf�gg� (mint 3, workflow �llapottal)
        /// 5 - �llapotf�gg� (�temezett �llapotvizsg�lat)
        /// </summary>
        private Field _FeladatDefinicioTipus = new Field();

        public Field FeladatDefinicioTipus
        {
            get { return _FeladatDefinicioTipus; }
            set { _FeladatDefinicioTipus = value; }
        }
                          
           
        /// <summary>
        /// FeladatSorszam property
        /// Adott kiv�lt� esem�nyen bel�l futtatand� feladat sorrendi sz�ma.
        /// </summary>
        private Field _FeladatSorszam = new Field();

        public Field FeladatSorszam
        {
            get { return _FeladatSorszam; }
            set { _FeladatSorszam = value; }
        }
                          
           
        /// <summary>
        /// MuveletDefinicio property
        /// A v�grehajtand� feldolgoz�si m�velet. KCS: MUVELET_DEFINICIO
        /// pl. sp_WF_FeladatKezelo - hat�rid�s feladatok/�rtes�t�sek kezel�se.
        /// </summary>
        private Field _MuveletDefinicio = new Field();

        public Field MuveletDefinicio
        {
            get { return _MuveletDefinicio; }
            set { _MuveletDefinicio = value; }
        }
                          
           
        /// <summary>
        /// SpecMuveletDefinicio property
        /// A v�grehajtand� speci�lis m�velet defin�ci�ja. KCS: SPECMUVELET_DEFINICIO
        /// pl. sp_WF_UgyitatSzignalasFeladat - feladat gener�l�s �gyirat szign�l�shoz
        /// </summary>
        private Field _SpecMuveletDefinicio = new Field();

        public Field SpecMuveletDefinicio
        {
            get { return _SpecMuveletDefinicio; }
            set { _SpecMuveletDefinicio = value; }
        }
                          
           
        /// <summary>
        /// Funkcio_Id_Inditando property
        /// KRT_Funkciok FK, a hat�rid�s feladatok eset�ben ez lesz ind�tand� funkci�k�nt be�ll�tva.
        /// </summary>
        private Field _Funkcio_Id_Inditando = new Field();

        public Field Funkcio_Id_Inditando
        {
            get { return _Funkcio_Id_Inditando; }
            set { _Funkcio_Id_Inditando = value; }
        }
                          
           
        /// <summary>
        /// Funkcio_Id_Futtatando property
        /// 
        /// </summary>
        private Field _Funkcio_Id_Futtatando = new Field();

        public Field Funkcio_Id_Futtatando
        {
            get { return _Funkcio_Id_Futtatando; }
            set { _Funkcio_Id_Futtatando = value; }
        }
                          
           
        /// <summary>
        /// BazisObjLeiro property
        /// A b�zisobjektum le�r�ja.
        /// </summary>
        private Field _BazisObjLeiro = new Field();

        public Field BazisObjLeiro
        {
            get { return _BazisObjLeiro; }
            set { _BazisObjLeiro = value; }
        }
                          
           
        /// <summary>
        /// FelelosTipus property
        /// Az Obj_Id_Felelos attributum szerinti felel�s t�pusa.
        /// KCS: FELADAT_FELELOS_TIPUS
        /// 1 - Csoport
        /// 2 - T�blaoszlop
        /// </summary>
        private Field _FelelosTipus = new Field();

        public Field FelelosTipus
        {
            get { return _FelelosTipus; }
            set { _FelelosTipus = value; }
        }
                          
           
        /// <summary>
        /// Obj_Id_Felelos property
        /// A felel�s csoportot vagy csoporttagot defini�l� attributum azonos�t�ja.
        /// </summary>
        private Field _Obj_Id_Felelos = new Field();

        public Field Obj_Id_Felelos
        {
            get { return _Obj_Id_Felelos; }
            set { _Obj_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// FelelosLeiro property
        /// Az automatikusan el��rhat� feledat felel�s�nek meghat�roz�s�ra vonatkoz� defin�ci�.
        /// </summary>
        private Field _FelelosLeiro = new Field();

        public Field FelelosLeiro
        {
            get { return _FelelosLeiro; }
            set { _FelelosLeiro = value; }
        }
                          
           
        /// <summary>
        /// Idobazis property
        /// A hat�rid� sz�m�t�s b�zisideje. NULL �rt�k eset�n egyedileg meghat�rozand� adat. D�tumoszlop szerinti �rt�k meghat�roz�sa az esem�nyben �rintett objektum ObjTipus_Id_DateCol adata alapj�nt�rt�nik. KCS:FELADAT_IDOBAZIS
        /// 1 - Aktu�lis id�pont
        /// 2 - Aktu�lis d�tum
        /// 3 - D�tumoszlop
        /// 4 - D�tum t�rgysz�
        /// 5 - Helyettes�t� id�pont (ha az azonos�t� szerinti d�tum nem ismert vagy �res)
        /// 6 - Helyettes�t� d�tum (ha az azonos�t� szerinti d�tum nem ismert vagy �res)
        /// </summary>
        private Field _Idobazis = new Field();

        public Field Idobazis
        {
            get { return _Idobazis; }
            set { _Idobazis = value; }
        }
                          
           
        /// <summary>
        /// ObjTip_Id_DateCol property
        /// Szabv�nyos d�tumoszlop vagy d�tumaadatot tartalmaz� t�rgysz� azonos�t�ja.
        /// </summary>
        private Field _ObjTip_Id_DateCol = new Field();

        public Field ObjTip_Id_DateCol
        {
            get { return _ObjTip_Id_DateCol; }
            set { _ObjTip_Id_DateCol = value; }
        }
                          
           
        /// <summary>
        /// AtfutasiIdo property
        /// A feladat �tut�si ideje (a megadott Idoegyseg-ben). NULL �rt�k eset�n egyedileg meghat�rozand� adat.
        /// </summary>
        private Field _AtfutasiIdo = new Field();

        public Field AtfutasiIdo
        {
            get { return _AtfutasiIdo; }
            set { _AtfutasiIdo = value; }
        }
                          
           
        /// <summary>
        /// Idoegyseg property
        /// Az �tfut�si id� id�egys�ge. KCS: IDOEGYSEG
        /// (perc, �ra, nap, ...)
        /// A k�d egyben az id�egys�g percben megadott �rt�ke.
        /// 1- perc
        /// 60 - �ra
        /// 1440 - nap
        /// </summary>
        private Field _Idoegyseg = new Field();

        public Field Idoegyseg
        {
            get { return _Idoegyseg; }
            set { _Idoegyseg = value; }
        }
                          
           
        /// <summary>
        /// FeladatLeiras property
        /// Az automatikusan el��rhat� feladat sz�veges le�r�sa
        /// </summary>
        private Field _FeladatLeiras = new Field();

        public Field FeladatLeiras
        {
            get { return _FeladatLeiras; }
            set { _FeladatLeiras = value; }
        }
                          
           
        /// <summary>
        /// Tipus property
        /// A feladat t�pusa. KCS: FELADAT_TIPUS
        /// F - Feladat (szabv�nyos vagy egyedileg defini�lt)
        /// M - Megjegyz�s (feladat n�lk�li megjegyz�s)
        /// 
        /// 
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Prioritas property
        /// Feladat priorit�s kateg�ri�k: KCS: FELADAT_PRIORITAS
        /// 11, 12, ... - Alacsony (�rtes�t�s n�lk�li)
        /// 21, 22, ... - K�zepes (T�R �rtes�t�s)
        /// 31, 32, ... - Magas  (E-mail �rtes�t�s)
        /// </summary>
        private Field _Prioritas = new Field();

        public Field Prioritas
        {
            get { return _Prioritas; }
            set { _Prioritas = value; }
        }
                          
           
        /// <summary>
        /// LezarasPrioritas property
        /// Feladat lez�r�s�nak priorit�sa, ha az �llapot 3 vagy 4 lesz: KCS: FELADAT_PRIORITAS
        /// 11 - Alacsony (�rtes�t�s n�lk�li)
        /// 21 - K�zepes (T�R �rtes�t�s)
        /// 31 - Magas  (E-mail �rtes�t�s)
        /// </summary>
        private Field _LezarasPrioritas = new Field();

        public Field LezarasPrioritas
        {
            get { return _LezarasPrioritas; }
            set { _LezarasPrioritas = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// Az esem�ny hat�s�ra be�ll�tand� �llapot a hat�rid�s feladatn�l. NULL �rt�k eset�n egyedileg meghat�rozand� adat.
        /// KCS: FELADAT_ALLAPOT �rt�kei:
        /// 0 - �j
        /// 1 - Nyitott
        /// 2 - Folyamatban
        /// 3 - Lez�rt
        /// 4 - Sztorn�zott
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// ObjTip_Id property
        /// Objektum tipus Id
        /// Kapcsol�t�bl�ba: Ez annak a valaminek a t�pusa, amit csatoltunk
        /// </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Obj_type property
        /// Obj. t�pus, itt: a t�bla neve
        /// </summary>
        private Field _Obj_type = new Field();

        public Field Obj_type
        {
            get { return _Obj_type; }
            set { _Obj_type = value; }
        }
                          
           
        /// <summary>
        /// Obj_SzukitoFeltetel property
        /// A konkr�t feladatra vagy �zenetre vonatkoz� lek�rdez�s sz�k�t� felt�tele.
        /// </summary>
        private Field _Obj_SzukitoFeltetel = new Field();

        public Field Obj_SzukitoFeltetel
        {
            get { return _Obj_SzukitoFeltetel; }
            set { _Obj_SzukitoFeltetel = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}