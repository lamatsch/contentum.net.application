
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_FeladatErtesites eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_FeladatErtesitesSearch: BaseSearchObject    {
      public EREC_FeladatErtesitesSearch()
      {            
                     _Id.Name = "EREC_FeladatErtesites.Id";
               _Id.Type = "Guid";            
               _FeladatDefinicio_id.Name = "EREC_FeladatErtesites.FeladatDefinicio_id";
               _FeladatDefinicio_id.Type = "Guid";            
               _Felhasznalo_Id.Name = "EREC_FeladatErtesites.Felhasznalo_Id";
               _Felhasznalo_Id.Type = "Guid";            
               _Ertesites.Name = "EREC_FeladatErtesites.Ertesites";
               _Ertesites.Type = "Char";            
               _ErvKezd.Name = "EREC_FeladatErtesites.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_FeladatErtesites.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// FeladatDefinicio_id property
        /// 
        /// </summary>
        private Field _FeladatDefinicio_id = new Field();

        public Field FeladatDefinicio_id
        {
            get { return _FeladatDefinicio_id; }
            set { _FeladatDefinicio_id = value; }
        }
                          
           
        /// <summary>
        /// Felhasznalo_Id property
        /// 
        /// </summary>
        private Field _Felhasznalo_Id = new Field();

        public Field Felhasznalo_Id
        {
            get { return _Felhasznalo_Id; }
            set { _Felhasznalo_Id = value; }
        }
                          
           
        /// <summary>
        /// Ertesites property
        /// 
        /// </summary>
        private Field _Ertesites = new Field();

        public Field Ertesites
        {
            get { return _Ertesites; }
            set { _Ertesites = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}