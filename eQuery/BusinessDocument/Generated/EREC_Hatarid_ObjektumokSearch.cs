
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Hatarid_Objektumok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_Hatarid_ObjektumokSearch: BaseSearchObject    {
      public EREC_Hatarid_ObjektumokSearch()
      {            
                     _Id.Name = "EREC_Hatarid_Objektumok.Id";
               _Id.Type = "Guid";            
               _HataridosFeladat_Id.Name = "EREC_Hatarid_Objektumok.HataridosFeladat_Id";
               _HataridosFeladat_Id.Type = "Guid";            
               _Obj_Id.Name = "EREC_Hatarid_Objektumok.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id.Name = "EREC_Hatarid_Objektumok.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Obj_type.Name = "EREC_Hatarid_Objektumok.Obj_type";
               _Obj_type.Type = "String";            
               _ErvKezd.Name = "EREC_Hatarid_Objektumok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Hatarid_Objektumok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HataridosFeladat_Id = new Field();

        public Field HataridosFeladat_Id
        {
            get { return _HataridosFeladat_Id; }
            set { _HataridosFeladat_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_type = new Field();

        public Field Obj_type
        {
            get { return _Obj_type; }
            set { _Obj_type = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}