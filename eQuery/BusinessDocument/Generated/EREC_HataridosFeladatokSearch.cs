
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_HataridosFeladatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_HataridosFeladatokSearch: BaseSearchObject    {
      public EREC_HataridosFeladatokSearch()
      {            
		Init_ManualFields();
                     _Id.Name = "EREC_HataridosFeladatok.Id";
               _Id.Type = "Guid";            
               _Esemeny_Id_Kivalto.Name = "EREC_HataridosFeladatok.Esemeny_Id_Kivalto";
               _Esemeny_Id_Kivalto.Type = "Guid";            
               _Esemeny_Id_Lezaro.Name = "EREC_HataridosFeladatok.Esemeny_Id_Lezaro";
               _Esemeny_Id_Lezaro.Type = "Guid";            
               _Funkcio_Id_Inditando.Name = "EREC_HataridosFeladatok.Funkcio_Id_Inditando";
               _Funkcio_Id_Inditando.Type = "Guid";            
               _Csoport_Id_Felelos.Name = "EREC_HataridosFeladatok.Csoport_Id_Felelos";
               _Csoport_Id_Felelos.Type = "Guid";            
               _Felhasznalo_Id_Felelos.Name = "EREC_HataridosFeladatok.Felhasznalo_Id_Felelos";
               _Felhasznalo_Id_Felelos.Type = "Guid";            
               _Csoport_Id_Kiado.Name = "EREC_HataridosFeladatok.Csoport_Id_Kiado";
               _Csoport_Id_Kiado.Type = "Guid";            
               _Felhasznalo_Id_Kiado.Name = "EREC_HataridosFeladatok.Felhasznalo_Id_Kiado";
               _Felhasznalo_Id_Kiado.Type = "Guid";            
               _HataridosFeladat_Id.Name = "EREC_HataridosFeladatok.HataridosFeladat_Id";
               _HataridosFeladat_Id.Type = "Guid";            
               _ReszFeladatSorszam.Name = "EREC_HataridosFeladatok.ReszFeladatSorszam";
               _ReszFeladatSorszam.Type = "Int32";            
               _FeladatDefinicio_Id.Name = "EREC_HataridosFeladatok.FeladatDefinicio_Id";
               _FeladatDefinicio_Id.Type = "Guid";            
               _FeladatDefinicio_Id_Lezaro.Name = "EREC_HataridosFeladatok.FeladatDefinicio_Id_Lezaro";
               _FeladatDefinicio_Id_Lezaro.Type = "Guid";            
               _Forras.Name = "EREC_HataridosFeladatok.Forras";
               _Forras.Type = "Char";            
               _Tipus.Name = "EREC_HataridosFeladatok.Tipus";
               _Tipus.Type = "String";            
               _Altipus.Name = "EREC_HataridosFeladatok.Altipus";
               _Altipus.Type = "String";            
               _Memo.Name = "EREC_HataridosFeladatok.Memo";
               _Memo.Type = "Char";            
               _Leiras.Name = "EREC_HataridosFeladatok.Leiras";
               _Leiras.Type = "String";            
               _Prioritas.Name = "EREC_HataridosFeladatok.Prioritas";
               _Prioritas.Type = "String";            
               _LezarasPrioritas.Name = "EREC_HataridosFeladatok.LezarasPrioritas";
               _LezarasPrioritas.Type = "String";            
               _KezdesiIdo.Name = "EREC_HataridosFeladatok.KezdesiIdo";
               _KezdesiIdo.Type = "DateTime";            
               _IntezkHatarido.Name = "EREC_HataridosFeladatok.IntezkHatarido";
               _IntezkHatarido.Type = "DateTime";            
               _LezarasDatuma.Name = "EREC_HataridosFeladatok.LezarasDatuma";
               _LezarasDatuma.Type = "DateTime";            
               _Azonosito.Name = "EREC_HataridosFeladatok.Azonosito";
               _Azonosito.Type = "String";            
               _Obj_Id.Name = "EREC_HataridosFeladatok.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id.Name = "EREC_HataridosFeladatok.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Obj_type.Name = "EREC_HataridosFeladatok.Obj_type";
               _Obj_type.Type = "String";            
               _Allapot.Name = "EREC_HataridosFeladatok.Allapot";
               _Allapot.Type = "String";            
               _Megoldas.Name = "EREC_HataridosFeladatok.Megoldas";
               _Megoldas.Type = "String";            
               _ErvKezd.Name = "EREC_HataridosFeladatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_HataridosFeladatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Esemeny_Id_Kivalto = new Field();

        public Field Esemeny_Id_Kivalto
        {
            get { return _Esemeny_Id_Kivalto; }
            set { _Esemeny_Id_Kivalto = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Esemeny_Id_Lezaro = new Field();

        public Field Esemeny_Id_Lezaro
        {
            get { return _Esemeny_Id_Lezaro; }
            set { _Esemeny_Id_Lezaro = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Funkcio_Id_Inditando = new Field();

        public Field Funkcio_Id_Inditando
        {
            get { return _Funkcio_Id_Inditando; }
            set { _Funkcio_Id_Inditando = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Felelos = new Field();

        public Field Csoport_Id_Felelos
        {
            get { return _Csoport_Id_Felelos; }
            set { _Csoport_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id_Felelos = new Field();

        public Field Felhasznalo_Id_Felelos
        {
            get { return _Felhasznalo_Id_Felelos; }
            set { _Felhasznalo_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Kiado = new Field();

        public Field Csoport_Id_Kiado
        {
            get { return _Csoport_Id_Kiado; }
            set { _Csoport_Id_Kiado = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id_Kiado = new Field();

        public Field Felhasznalo_Id_Kiado
        {
            get { return _Felhasznalo_Id_Kiado; }
            set { _Felhasznalo_Id_Kiado = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HataridosFeladat_Id = new Field();

        public Field HataridosFeladat_Id
        {
            get { return _HataridosFeladat_Id; }
            set { _HataridosFeladat_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ReszFeladatSorszam = new Field();

        public Field ReszFeladatSorszam
        {
            get { return _ReszFeladatSorszam; }
            set { _ReszFeladatSorszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FeladatDefinicio_Id = new Field();

        public Field FeladatDefinicio_Id
        {
            get { return _FeladatDefinicio_Id; }
            set { _FeladatDefinicio_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FeladatDefinicio_Id_Lezaro = new Field();

        public Field FeladatDefinicio_Id_Lezaro
        {
            get { return _FeladatDefinicio_Id_Lezaro; }
            set { _FeladatDefinicio_Id_Lezaro = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Forras = new Field();

        public Field Forras
        {
            get { return _Forras; }
            set { _Forras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Altipus = new Field();

        public Field Altipus
        {
            get { return _Altipus; }
            set { _Altipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Memo = new Field();

        public Field Memo
        {
            get { return _Memo; }
            set { _Memo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Prioritas = new Field();

        public Field Prioritas
        {
            get { return _Prioritas; }
            set { _Prioritas = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LezarasPrioritas = new Field();

        public Field LezarasPrioritas
        {
            get { return _LezarasPrioritas; }
            set { _LezarasPrioritas = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KezdesiIdo = new Field();

        public Field KezdesiIdo
        {
            get { return _KezdesiIdo; }
            set { _KezdesiIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IntezkHatarido = new Field();

        public Field IntezkHatarido
        {
            get { return _IntezkHatarido; }
            set { _IntezkHatarido = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LezarasDatuma = new Field();

        public Field LezarasDatuma
        {
            get { return _LezarasDatuma; }
            set { _LezarasDatuma = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_type = new Field();

        public Field Obj_type
        {
            get { return _Obj_type; }
            set { _Obj_type = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Megoldas = new Field();

        public Field Megoldas
        {
            get { return _Megoldas; }
            set { _Megoldas = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}