
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IraElosztoivTetelek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IraElosztoivTetelekSearch: BaseSearchObject    {
      public EREC_IraElosztoivTetelekSearch()
      {            
                     _Id.Name = "EREC_IraElosztoivTetelek.Id";
               _Id.Type = "Guid";            
               _ElosztoIv_Id.Name = "EREC_IraElosztoivTetelek.ElosztoIv_Id";
               _ElosztoIv_Id.Type = "Guid";            
               _Sorszam.Name = "EREC_IraElosztoivTetelek.Sorszam";
               _Sorszam.Type = "Int32";            
               _Partner_Id.Name = "EREC_IraElosztoivTetelek.Partner_Id";
               _Partner_Id.Type = "Guid";            
               _Cim_Id.Name = "EREC_IraElosztoivTetelek.Cim_Id";
               _Cim_Id.Type = "Guid";            
               _CimSTR.Name = "EREC_IraElosztoivTetelek.CimSTR";
               _CimSTR.Type = "String";            
               _NevSTR.Name = "EREC_IraElosztoivTetelek.NevSTR";
               _NevSTR.Type = "String";            
               _Kuldesmod.Name = "EREC_IraElosztoivTetelek.Kuldesmod";
               _Kuldesmod.Type = "String";            
               _Visszavarolag.Name = "EREC_IraElosztoivTetelek.Visszavarolag";
               _Visszavarolag.Type = "Char";            
               _VisszavarasiIdo.Name = "EREC_IraElosztoivTetelek.VisszavarasiIdo";
               _VisszavarasiIdo.Type = "DateTime";            
               _Vissza_Nap_Mulva.Name = "EREC_IraElosztoivTetelek.Vissza_Nap_Mulva";
               _Vissza_Nap_Mulva.Type = "Int32";            
               _AlairoSzerep.Name = "EREC_IraElosztoivTetelek.AlairoSzerep";
               _AlairoSzerep.Type = "Char";            
               _ErvKezd.Name = "EREC_IraElosztoivTetelek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IraElosztoivTetelek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ElosztoIv_Id = new Field();

        public Field ElosztoIv_Id
        {
            get { return _ElosztoIv_Id; }
            set { _ElosztoIv_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Partner_Id = new Field();

        public Field Partner_Id
        {
            get { return _Partner_Id; }
            set { _Partner_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Cim_Id = new Field();

        public Field Cim_Id
        {
            get { return _Cim_Id; }
            set { _Cim_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _CimSTR = new Field();

        public Field CimSTR
        {
            get { return _CimSTR; }
            set { _CimSTR = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _NevSTR = new Field();

        public Field NevSTR
        {
            get { return _NevSTR; }
            set { _NevSTR = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kuldesmod = new Field();

        public Field Kuldesmod
        {
            get { return _Kuldesmod; }
            set { _Kuldesmod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Visszavarolag = new Field();

        public Field Visszavarolag
        {
            get { return _Visszavarolag; }
            set { _Visszavarolag = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _VisszavarasiIdo = new Field();

        public Field VisszavarasiIdo
        {
            get { return _VisszavarasiIdo; }
            set { _VisszavarasiIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Vissza_Nap_Mulva = new Field();

        public Field Vissza_Nap_Mulva
        {
            get { return _Vissza_Nap_Mulva; }
            set { _Vissza_Nap_Mulva = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairoSzerep = new Field();

        public Field AlairoSzerep
        {
            get { return _AlairoSzerep; }
            set { _AlairoSzerep = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}