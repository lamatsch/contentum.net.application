
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IraElosztoivek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IraElosztoivekSearch: BaseSearchObject    {
      public EREC_IraElosztoivekSearch()
      {            
                     _Id.Name = "EREC_IraElosztoivek.Id";
               _Id.Type = "Guid";            
               _Org.Name = "EREC_IraElosztoivek.Org";
               _Org.Type = "Guid";            
               _Ev.Name = "EREC_IraElosztoivek.Ev";
               _Ev.Type = "Int32";            
               _Fajta.Name = "EREC_IraElosztoivek.Fajta";
               _Fajta.Type = "String";            
               _Kod.Name = "EREC_IraElosztoivek.Kod";
               _Kod.Type = "String";            
               _NEV.Name = "EREC_IraElosztoivek.NEV";
               _NEV.Type = "String";            
               _Hasznalat.Name = "EREC_IraElosztoivek.Hasznalat";
               _Hasznalat.Type = "String";            
               _Csoport_Id_Tulaj.Name = "EREC_IraElosztoivek.Csoport_Id_Tulaj";
               _Csoport_Id_Tulaj.Type = "Guid";            
               _ErvKezd.Name = "EREC_IraElosztoivek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IraElosztoivek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Ev property
        /// decimal(4)
        /// </summary>
        private Field _Ev = new Field();

        public Field Ev
        {
            get { return _Ev; }
            set { _Ev = value; }
        }
                          
           
        /// <summary>
        /// Fajta property
        /// Az eloszt��v t�pusa, k�dt�ras. KCS: ELOSZTOIV_FAJTA 
        /// 1 - Irat eloszt��v
        /// 3 - Al��r�-lista
        /// 4 - Titkos�t� lista
        /// </summary>
        private Field _Fajta = new Field();

        public Field Fajta
        {
            get { return _Fajta; }
            set { _Fajta = value; }
        }
                          
           
        /// <summary>
        /// Kod property
        /// R�gi neve: ROVIDNEV
        /// </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// NEV property
        /// R�gi neve: MEGNEVEZES
        /// </summary>
        private Field _NEV = new Field();

        public Field NEV
        {
            get { return _NEV; }
            set { _NEV = value; }
        }
                          
           
        /// <summary>
        /// Hasznalat property
        /// Az eloszt��v haszn�lati k�r�t jel�li. KCS:ELOSZTOIV_HASZNALAT
        /// 0 - Glob�lis (mindenki �ltal el�rhet�)
        /// 1 - Szervezeti (a tulajdonos szervezeten bel�l el�rhet�)
        /// 2 - Egy�ni (a tulajdonos felhaszn�l� �ltal el�rhet�)
        /// </summary>
        private Field _Hasznalat = new Field();

        public Field Hasznalat
        {
            get { return _Hasznalat; }
            set { _Hasznalat = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Tulaj property
        /// 
        /// </summary>
        private Field _Csoport_Id_Tulaj = new Field();

        public Field Csoport_Id_Tulaj
        {
            get { return _Csoport_Id_Tulaj; }
            set { _Csoport_Id_Tulaj = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}