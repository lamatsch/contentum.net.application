
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IraIktatoKonyvek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IraIktatoKonyvekSearch: BaseSearchObject    {
      public EREC_IraIktatoKonyvekSearch()
      {            
                     _Id.Name = "EREC_IraIktatoKonyvek.Id";
               _Id.Type = "Guid";            
               _Org.Name = "EREC_IraIktatoKonyvek.Org";
               _Org.Type = "Guid";            
               _Ev.Name = "EREC_IraIktatoKonyvek.Ev";
               _Ev.Type = "Int32";            
               _Azonosito.Name = "EREC_IraIktatoKonyvek.Azonosito";
               _Azonosito.Type = "String";            
               _DefaultIrattariTetelszam.Name = "EREC_IraIktatoKonyvek.DefaultIrattariTetelszam";
               _DefaultIrattariTetelszam.Type = "Guid";            
               _Nev.Name = "EREC_IraIktatoKonyvek.Nev";
               _Nev.Type = "String";            
               _MegkulJelzes.Name = "EREC_IraIktatoKonyvek.MegkulJelzes";
               _MegkulJelzes.Type = "String";            
               _Iktatohely.Name = "EREC_IraIktatoKonyvek.Iktatohely";
               _Iktatohely.Type = "String";            
               _KozpontiIktatasJelzo.Name = "EREC_IraIktatoKonyvek.KozpontiIktatasJelzo";
               _KozpontiIktatasJelzo.Type = "Char";            
               _UtolsoFoszam.Name = "EREC_IraIktatoKonyvek.UtolsoFoszam";
               _UtolsoFoszam.Type = "Int32";            
               _UtolsoSorszam.Name = "EREC_IraIktatoKonyvek.UtolsoSorszam";
               _UtolsoSorszam.Type = "Int32";            
               _Csoport_Id_Olvaso.Name = "EREC_IraIktatoKonyvek.Csoport_Id_Olvaso";
               _Csoport_Id_Olvaso.Type = "Guid";            
               _Csoport_Id_Tulaj.Name = "EREC_IraIktatoKonyvek.Csoport_Id_Tulaj";
               _Csoport_Id_Tulaj.Type = "Guid";            
               _IktSzamOsztas.Name = "EREC_IraIktatoKonyvek.IktSzamOsztas";
               _IktSzamOsztas.Type = "String";            
               _FormatumKod.Name = "EREC_IraIktatoKonyvek.FormatumKod";
               _FormatumKod.Type = "Char";            
               _Titkos.Name = "EREC_IraIktatoKonyvek.Titkos";
               _Titkos.Type = "Char";            
               _IktatoErkezteto.Name = "EREC_IraIktatoKonyvek.IktatoErkezteto";
               _IktatoErkezteto.Type = "Char";            
               _LezarasDatuma.Name = "EREC_IraIktatoKonyvek.LezarasDatuma";
               _LezarasDatuma.Type = "DateTime";            
               _PostakonyvVevokod.Name = "EREC_IraIktatoKonyvek.PostakonyvVevokod";
               _PostakonyvVevokod.Type = "String";            
               _PostakonyvMegallapodasAzon.Name = "EREC_IraIktatoKonyvek.PostakonyvMegallapodasAzon";
               _PostakonyvMegallapodasAzon.Type = "String";            
               _EFeladoJegyzekUgyfelAdatok.Name = "EREC_IraIktatoKonyvek.EFeladoJegyzekUgyfelAdatok";
               _EFeladoJegyzekUgyfelAdatok.Type = "String";            
               _HitelesExport_Dok_ID.Name = "EREC_IraIktatoKonyvek.HitelesExport_Dok_ID";
               _HitelesExport_Dok_ID.Type = "Guid";            
               _Ujranyitando.Name = "EREC_IraIktatoKonyvek.Ujranyitando";
               _Ujranyitando.Type = "Char";            
               _ErvKezd.Name = "EREC_IraIktatoKonyvek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IraIktatoKonyvek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _Statusz.Name = "EREC_IraIktatoKonyvek.Statusz";
               _Statusz.Type = "Char";            
               _Terjedelem.Name = "EREC_IraIktatoKonyvek.Terjedelem";
               _Terjedelem.Type = "String";            
               _SelejtezesDatuma.Name = "EREC_IraIktatoKonyvek.SelejtezesDatuma";
               _SelejtezesDatuma.Type = "DateTime";            
               _KezelesTipusa.Name = "EREC_IraIktatoKonyvek.KezelesTipusa";
               _KezelesTipusa.Type = "Char";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Ev property
        /// Iktat�k�nyv �ve.
        /// </summary>
        private Field _Ev = new Field();

        public Field Ev
        {
            get { return _Ev; }
            set { _Ev = value; }
        }
                          
           
        /// <summary>
        /// Azonosito property
        /// Iktat�k�nyv azonos�t�,egyedi
        /// </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// DefaultIrattariTetelszam property
        /// R�gebben: FK volt, k�sobb nem kell
        /// </summary>
        private Field _DefaultIrattariTetelszam = new Field();

        public Field DefaultIrattariTetelszam
        {
            get { return _DefaultIrattariTetelszam; }
            set { _DefaultIrattariTetelszam = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// Iktat�k�nyv megnevez�se
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// MegkulJelzes property
        /// Iktat�sz�m megk�l�nb�zteto jelz�se (az iktat�sz�m elso eleme)
        /// </summary>
        private Field _MegkulJelzes = new Field();

        public Field MegkulJelzes
        {
            get { return _MegkulJelzes; }
            set { _MegkulJelzes = value; }
        }
                          
           
        /// <summary>
        /// Iktatohely property
        /// Sz�veges meghat�roz�s
        /// </summary>
        private Field _Iktatohely = new Field();

        public Field Iktatohely
        {
            get { return _Iktatohely; }
            set { _Iktatohely = value; }
        }
                          
           
        /// <summary>
        /// KozpontiIktatasJelzo property
        /// 1-k�zponti iktat�s, 0-egy�bk�nt.
        /// </summary>
        private Field _KozpontiIktatasJelzo = new Field();

        public Field KozpontiIktatasJelzo
        {
            get { return _KozpontiIktatasJelzo; }
            set { _KozpontiIktatasJelzo = value; }
        }
                          
           
        /// <summary>
        /// UtolsoFoszam property
        /// Utols� �rkeztetosz�m, vagy iktat�si sz�m (fosz�m).
        /// </summary>
        private Field _UtolsoFoszam = new Field();

        public Field UtolsoFoszam
        {
            get { return _UtolsoFoszam; }
            set { _UtolsoFoszam = value; }
        }
                          
           
        /// <summary>
        /// UtolsoSorszam property
        /// Adott iktat�k�nyvbe val� munka�gy iktat�s eset�n az utols� sorsz�m.
        /// </summary>
        private Field _UtolsoSorszam = new Field();

        public Field UtolsoSorszam
        {
            get { return _UtolsoSorszam; }
            set { _UtolsoSorszam = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Olvaso property
        /// �ltal�ban a mindenki csoport �ll itt. Ha nem mindenki l�thatja, akkor egy konkr�t. Ez t�bbs�g�ben mesters�ges csoport (azaz nem felhaszn�l�, persze ez is lehet,
        /// de ekkor neki kell adminisztr�lnia.)
        /// </summary>
        private Field _Csoport_Id_Olvaso = new Field();

        public Field Csoport_Id_Olvaso
        {
            get { return _Csoport_Id_Olvaso; }
            set { _Csoport_Id_Olvaso = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Tulaj property
        /// Ebben kiz�r�lag a Rendszergazd�k csoportja lehet. (Egyedi rendszergazda sem lehet!)
        /// </summary>
        private Field _Csoport_Id_Tulaj = new Field();

        public Field Csoport_Id_Tulaj
        {
            get { return _Csoport_Id_Tulaj; }
            set { _Csoport_Id_Tulaj = value; }
        }
                          
           
        /// <summary>
        /// IktSzamOsztas property
        /// KCS: IKT_SZAM_OSZTAS
        /// </summary>
        private Field _IktSzamOsztas = new Field();

        public Field IktSzamOsztas
        {
            get { return _IktSzamOsztas; }
            set { _IktSzamOsztas = value; }
        }
                          
           
        /// <summary>
        /// FormatumKod property
        /// Iktat�sz�m form�tum t�pusa
        /// 0-  megk�l�nb�zteto jelz�s/fosz�m-alsz�m/�v/
        /// 1-  fosz�m-alsz�m/�v/megk�l�nb�zteto jelz�s
        /// 
        /// </summary>
        private Field _FormatumKod = new Field();

        public Field FormatumKod
        {
            get { return _FormatumKod; }
            set { _FormatumKod = value; }
        }
                          
           
        /// <summary>
        /// Titkos property
        /// 1-Igen/0-nem.
        /// </summary>
        private Field _Titkos = new Field();

        public Field Titkos
        {
            get { return _Titkos; }
            set { _Titkos = value; }
        }
                          
           
        /// <summary>
        /// IktatoErkezteto property
        /// A k�nyv t�pusa:
        /// I = Iktat�k�nyv
        /// E = �rkeztetok�nyv,
        /// P = Postak�nyv
        /// </summary>
        private Field _IktatoErkezteto = new Field();

        public Field IktatoErkezteto
        {
            get { return _IktatoErkezteto; }
            set { _IktatoErkezteto = value; }
        }
                          
           
        /// <summary>
        /// LezarasDatuma property
        /// Az iktat�k�ny �ves lez�r�s�nak idopontja.
        /// </summary>
        private Field _LezarasDatuma = new Field();

        public Field LezarasDatuma
        {
            get { return _LezarasDatuma; }
            set { _LezarasDatuma = value; }
        }
                          
           
        /// <summary>
        /// PostakonyvVevokod property
        /// A Magyar Posta Zrt-n�l a Hivatalt azonos�t� k�d.
        /// </summary>
        private Field _PostakonyvVevokod = new Field();

        public Field PostakonyvVevokod
        {
            get { return _PostakonyvVevokod; }
            set { _PostakonyvVevokod = value; }
        }
                          
           
        /// <summary>
        /// PostakonyvMegallapodasAzon property
        /// A postak�nyv meg�llapod�st a Magyar Posta ZRT-n�l azonos�t� k�d.
        /// </summary>
        private Field _PostakonyvMegallapodasAzon = new Field();

        public Field PostakonyvMegallapodasAzon
        {
            get { return _PostakonyvMegallapodasAzon; }
            set { _PostakonyvMegallapodasAzon = value; }
        }
                          
           
        /// <summary>
        /// EFeladoJegyzekUgyfelAdatok property
        /// Az elekronikus felad�jegyz�k elk�sz�t�s�hez sz�ks�ges UGYFEL.XML (ahol az �gyf�l a felad� szervezet, a posta szempontj�b�l �rtendo) - postak�nyvh�z kapcsol�dik. Nem XML-k�nt, hanem nvarchar-k�nt t�roljuk.
        /// </summary>
        private Field _EFeladoJegyzekUgyfelAdatok = new Field();

        public Field EFeladoJegyzekUgyfelAdatok
        {
            get { return _EFeladoJegyzekUgyfelAdatok; }
            set { _EFeladoJegyzekUgyfelAdatok = value; }
        }
                          
           
        /// <summary>
        /// HitelesExport_Dok_ID property
        /// �ltal�ban a mindenki csoport �ll itt. Ha nem mindenki l�thatja, akkor egy konkr�t. Ez t�bbs�g�ben mesters�ges csoport (azaz nem felhaszn�l�, persze ez is lehet,
        /// de ekkor neki kell adminisztr�lnia.)
        /// </summary>
        private Field _HitelesExport_Dok_ID = new Field();

        public Field HitelesExport_Dok_ID
        {
            get { return _HitelesExport_Dok_ID; }
            set { _HitelesExport_Dok_ID = value; }
        }
                          
           
        /// <summary>
        /// Ujranyitando property
        /// 1-Igen/0-nem.
        /// </summary>
        private Field _Ujranyitando = new Field();

        public Field Ujranyitando
        {
            get { return _Ujranyitando; }
            set { _Ujranyitando = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// Statusz property
        /// 
        /// </summary>
        private Field _Statusz = new Field();

        public Field Statusz
        {
            get { return _Statusz; }
            set { _Statusz = value; }
        }
                          
           
        /// <summary>
        /// Terjedelem property
        /// Iktat�sz�m megk�l�nb�zteto jelz�se (az iktat�sz�m elso eleme)
        /// </summary>
        private Field _Terjedelem = new Field();

        public Field Terjedelem
        {
            get { return _Terjedelem; }
            set { _Terjedelem = value; }
        }
                          
           
        /// <summary>
        /// SelejtezesDatuma property
        /// Az iktat�k�ny �ves lez�r�s�nak idopontja.
        /// </summary>
        private Field _SelejtezesDatuma = new Field();

        public Field SelejtezesDatuma
        {
            get { return _SelejtezesDatuma; }
            set { _SelejtezesDatuma = value; }
        }
                          
           
        /// <summary>
        /// KezelesTipusa property
        /// 1-k�zponti iktat�s, 0-egy�bk�nt.
        /// </summary>
        private Field _KezelesTipusa = new Field();

        public Field KezelesTipusa
        {
            get { return _KezelesTipusa; }
            set { _KezelesTipusa = value; }
        }
                              }

}