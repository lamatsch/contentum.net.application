
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IraIratok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_IraIratokSearch: BaseSearchObject    {
      public EREC_IraIratokSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "EREC_IraIratok.Id";
               _Id.Type = "Guid";            
               _PostazasIranya.Name = "EREC_IraIratok.PostazasIranya";
               _PostazasIranya.Type = "Char";            
               _Alszam.Name = "EREC_IraIratok.Alszam";
               _Alszam.Type = "Int32";            
               _Sorszam.Name = "EREC_IraIratok.Sorszam";
               _Sorszam.Type = "Int32";            
               _UtolsoSorszam.Name = "EREC_IraIratok.UtolsoSorszam";
               _UtolsoSorszam.Type = "Int32";            
               _UgyUgyIratDarab_Id.Name = "EREC_IraIratok.UgyUgyIratDarab_Id";
               _UgyUgyIratDarab_Id.Type = "Guid";            
               _Kategoria.Name = "EREC_IraIratok.Kategoria";
               _Kategoria.Type = "String";            
               _HivatkozasiSzam.Name = "EREC_IraIratok.HivatkozasiSzam";
               _HivatkozasiSzam.Type = "String";            
               _IktatasDatuma.Name = "EREC_IraIratok.IktatasDatuma";
               _IktatasDatuma.Type = "DateTime";            
               _ExpedialasDatuma.Name = "EREC_IraIratok.ExpedialasDatuma";
               _ExpedialasDatuma.Type = "DateTime";            
               _ExpedialasModja.Name = "EREC_IraIratok.ExpedialasModja";
               _ExpedialasModja.Type = "String";            
               _FelhasznaloCsoport_Id_Expedial.Name = "EREC_IraIratok.FelhasznaloCsoport_Id_Expedial";
               _FelhasznaloCsoport_Id_Expedial.Type = "Guid";            
               _Targy.Name = "EREC_IraIratok.Targy";
               _Targy.Type = "FTSString";            
               _Jelleg.Name = "EREC_IraIratok.Jelleg";
               _Jelleg.Type = "String";            
               _SztornirozasDat.Name = "EREC_IraIratok.SztornirozasDat";
               _SztornirozasDat.Type = "DateTime";            
               _FelhasznaloCsoport_Id_Iktato.Name = "EREC_IraIratok.FelhasznaloCsoport_Id_Iktato";
               _FelhasznaloCsoport_Id_Iktato.Type = "Guid";            
               _FelhasznaloCsoport_Id_Kiadmany.Name = "EREC_IraIratok.FelhasznaloCsoport_Id_Kiadmany";
               _FelhasznaloCsoport_Id_Kiadmany.Type = "Guid";            
               _UgyintezesAlapja.Name = "EREC_IraIratok.UgyintezesAlapja";
               _UgyintezesAlapja.Type = "String";            
               _AdathordozoTipusa.Name = "EREC_IraIratok.AdathordozoTipusa";
               _AdathordozoTipusa.Type = "String";            
               _Csoport_Id_Felelos.Name = "EREC_IraIratok.Csoport_Id_Felelos";
               _Csoport_Id_Felelos.Type = "Guid";            
               _Csoport_Id_Ugyfelelos.Name = "EREC_IraIratok.Csoport_Id_Ugyfelelos";
               _Csoport_Id_Ugyfelelos.Type = "Guid";            
               _FelhasznaloCsoport_Id_Orzo.Name = "EREC_IraIratok.FelhasznaloCsoport_Id_Orzo";
               _FelhasznaloCsoport_Id_Orzo.Type = "Guid";            
               _KiadmanyozniKell.Name = "EREC_IraIratok.KiadmanyozniKell";
               _KiadmanyozniKell.Type = "Char";            
               _FelhasznaloCsoport_Id_Ugyintez.Name = "EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez";
               _FelhasznaloCsoport_Id_Ugyintez.Type = "Guid";            
               _Hatarido.Name = "EREC_IraIratok.Hatarido";
               _Hatarido.Type = "DateTime";            
               _MegorzesiIdo.Name = "EREC_IraIratok.MegorzesiIdo";
               _MegorzesiIdo.Type = "DateTime";            
               _IratFajta.Name = "EREC_IraIratok.IratFajta";
               _IratFajta.Type = "String";            
               _Irattipus.Name = "EREC_IraIratok.Irattipus";
               _Irattipus.Type = "String";            
               _KuldKuldemenyek_Id.Name = "EREC_IraIratok.KuldKuldemenyek_Id";
               _KuldKuldemenyek_Id.Type = "Guid";            
               _Allapot.Name = "EREC_IraIratok.Allapot";
               _Allapot.Type = "String";            
               _Azonosito.Name = "EREC_IraIratok.Azonosito";
               _Azonosito.Type = "String";            
               _GeneraltTargy.Name = "EREC_IraIratok.GeneraltTargy";
               _GeneraltTargy.Type = "String";            
               _IratMetaDef_Id.Name = "EREC_IraIratok.IratMetaDef_Id";
               _IratMetaDef_Id.Type = "Guid";            
               _IrattarbaKuldDatuma .Name = "EREC_IraIratok.IrattarbaKuldDatuma ";
               _IrattarbaKuldDatuma .Type = "DateTime";            
               _IrattarbaVetelDat.Name = "EREC_IraIratok.IrattarbaVetelDat";
               _IrattarbaVetelDat.Type = "DateTime";            
               _Ugyirat_Id.Name = "EREC_IraIratok.Ugyirat_Id";
               _Ugyirat_Id.Type = "Guid";            
               _Minosites.Name = "EREC_IraIratok.Minosites";
               _Minosites.Type = "String";            
               _Elintezett.Name = "EREC_IraIratok.Elintezett";
               _Elintezett.Type = "Char";            
               _IratHatasaUgyintezesre.Name = "EREC_IraIratok.IratHatasaUgyintezesre";
               _IratHatasaUgyintezesre.Type = "String";            
               _FelfuggesztesOka.Name = "EREC_IraIratok.FelfuggesztesOka";
               _FelfuggesztesOka.Type = "String";            
               _LezarasOka.Name = "EREC_IraIratok.LezarasOka";
               _LezarasOka.Type = "String";            
               _Munkaallomas.Name = "EREC_IraIratok.Munkaallomas";
               _Munkaallomas.Type = "String";            
               _UgyintezesModja.Name = "EREC_IraIratok.UgyintezesModja";
               _UgyintezesModja.Type = "String";            
               _IntezesIdopontja.Name = "EREC_IraIratok.IntezesIdopontja";
               _IntezesIdopontja.Type = "DateTime";            
               _IntezesiIdo.Name = "EREC_IraIratok.IntezesiIdo";
               _IntezesiIdo.Type = "String";            
               _IntezesiIdoegyseg.Name = "EREC_IraIratok.IntezesiIdoegyseg";
               _IntezesiIdoegyseg.Type = "String";            
               _UzemzavarKezdete.Name = "EREC_IraIratok.UzemzavarKezdete";
               _UzemzavarKezdete.Type = "DateTime";            
               _UzemzavarVege.Name = "EREC_IraIratok.UzemzavarVege";
               _UzemzavarVege.Type = "DateTime";            
               _UzemzavarIgazoloIratSzama.Name = "EREC_IraIratok.UzemzavarIgazoloIratSzama";
               _UzemzavarIgazoloIratSzama.Type = "String";            
               _FelfuggesztettNapokSzama.Name = "EREC_IraIratok.FelfuggesztettNapokSzama";
               _FelfuggesztettNapokSzama.Type = "Int32";            
               _VisszafizetesJogcime.Name = "EREC_IraIratok.VisszafizetesJogcime";
               _VisszafizetesJogcime.Type = "String";            
               _VisszafizetesOsszege.Name = "EREC_IraIratok.VisszafizetesOsszege";
               _VisszafizetesOsszege.Type = "Int32";            
               _VisszafizetesHatarozatSzama.Name = "EREC_IraIratok.VisszafizetesHatarozatSzama";
               _VisszafizetesHatarozatSzama.Type = "String";            
               _Partner_Id_VisszafizetesCimzet.Name = "EREC_IraIratok.Partner_Id_VisszafizetesCimzet";
               _Partner_Id_VisszafizetesCimzet.Type = "Guid";            
               _Partner_Nev_VisszafizetCimzett.Name = "EREC_IraIratok.Partner_Nev_VisszafizetCimzett";
               _Partner_Nev_VisszafizetCimzett.Type = "String";            
               _VisszafizetesHatarido.Name = "EREC_IraIratok.VisszafizetesHatarido";
               _VisszafizetesHatarido.Type = "DateTime";            
               _Minosito.Name = "EREC_IraIratok.Minosito";
               _Minosito.Type = "Guid";            
               _MinositesErvenyessegiIdeje.Name = "EREC_IraIratok.MinositesErvenyessegiIdeje";
               _MinositesErvenyessegiIdeje.Type = "DateTime";            
               _TerjedelemMennyiseg.Name = "EREC_IraIratok.TerjedelemMennyiseg";
               _TerjedelemMennyiseg.Type = "Int32";            
               _TerjedelemMennyisegiEgyseg.Name = "EREC_IraIratok.TerjedelemMennyisegiEgyseg";
               _TerjedelemMennyisegiEgyseg.Type = "String";            
               _TerjedelemMegjegyzes.Name = "EREC_IraIratok.TerjedelemMegjegyzes";
               _TerjedelemMegjegyzes.Type = "String";            
               _SelejtezesDat.Name = "EREC_IraIratok.SelejtezesDat";
               _SelejtezesDat.Type = "DateTime";            
               _FelhCsoport_Id_Selejtezo.Name = "EREC_IraIratok.FelhCsoport_Id_Selejtezo";
               _FelhCsoport_Id_Selejtezo.Type = "Guid";            
               _LeveltariAtvevoNeve.Name = "EREC_IraIratok.LeveltariAtvevoNeve";
               _LeveltariAtvevoNeve.Type = "String";            
               _ModositasErvenyessegKezdete.Name = "EREC_IraIratok.ModositasErvenyessegKezdete";
               _ModositasErvenyessegKezdete.Type = "DateTime";            
               _MegszuntetoHatarozat.Name = "EREC_IraIratok.MegszuntetoHatarozat";
               _MegszuntetoHatarozat.Type = "String";            
               _FelulvizsgalatDatuma.Name = "EREC_IraIratok.FelulvizsgalatDatuma";
               _FelulvizsgalatDatuma.Type = "DateTime";            
               _Felulvizsgalo_id.Name = "EREC_IraIratok.Felulvizsgalo_id";
               _Felulvizsgalo_id.Type = "Guid";            
               _MinositesFelulvizsgalatEredm.Name = "EREC_IraIratok.MinositesFelulvizsgalatEredm";
               _MinositesFelulvizsgalatEredm.Type = "String";            
               _UgyintezesKezdoDatuma.Name = "EREC_IraIratok.UgyintezesKezdoDatuma";
               _UgyintezesKezdoDatuma.Type = "DateTime";            
               _EljarasiSzakasz.Name = "EREC_IraIratok.EljarasiSzakasz";
               _EljarasiSzakasz.Type = "String";            
               _HatralevoNapok.Name = "EREC_IraIratok.HatralevoNapok";
               _HatralevoNapok.Type = "Int32";            
               _HatralevoMunkaNapok.Name = "EREC_IraIratok.HatralevoMunkaNapok";
               _HatralevoMunkaNapok.Type = "Int32";            
               _Ugy_Fajtaja.Name = "EREC_IraIratok.Ugy_Fajtaja";
               _Ugy_Fajtaja.Type = "String";            
               _MinositoSzervezet.Name = "EREC_IraIratok.MinositoSzervezet";
               _MinositoSzervezet.Type = "Guid";            
               _MinositesFelulvizsgalatBizTag.Name = "EREC_IraIratok.MinositesFelulvizsgalatBizTag";
               _MinositesFelulvizsgalatBizTag.Type = "String";            
               _ErvKezd.Name = "EREC_IraIratok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IraIratok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi GUID azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// PostazasIranya property
        /// KCS: POSTAZAS_IRANYA
        /// 0 - Bels�
        /// 1 - Bej�v�
        /// </summary>
        private Field _PostazasIranya = new Field();

        public Field PostazasIranya
        {
            get { return _PostazasIranya; }
            set { _PostazasIranya = value; }
        }
                          
           
        /// <summary>
        /// Alszam property
        /// Iktat�si alsz�m
        /// </summary>
        private Field _Alszam = new Field();

        public Field Alszam
        {
            get { return _Alszam; }
            set { _Alszam = value; }
        }
                          
           
        /// <summary>
        /// Sorszam property
        /// Munkairat sorsz�m.
        /// </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }
                          
           
        /// <summary>
        /// UtolsoSorszam property
        /// Az irathoz (vagy munkairathoz) tartoz� iratp�ld�nyok utols� sorsz�ma.
        /// </summary>
        private Field _UtolsoSorszam = new Field();

        public Field UtolsoSorszam
        {
            get { return _UtolsoSorszam; }
            set { _UtolsoSorszam = value; }
        }
                          
           
        /// <summary>
        /// UgyUgyIratDarab_Id property
        /// �gyirat darab Id
        /// </summary>
        private Field _UgyUgyIratDarab_Id = new Field();

        public Field UgyUgyIratDarab_Id
        {
            get { return _UgyUgyIratDarab_Id; }
            set { _UgyUgyIratDarab_Id = value; }
        }
                          
           
        /// <summary>
        /// Kategoria property
        /// KCS: IRATKATEGORIA
        /// </summary>
        private Field _Kategoria = new Field();

        public Field Kategoria
        {
            get { return _Kategoria; }
            set { _Kategoria = value; }
        }
                          
           
        /// <summary>
        /// HivatkozasiSzam property
        /// �gyf�l, bek�ld� dokumenum�nak (fizikailag olvashat�) hivatkoz�si sz�ma (pl. egy sz�mlasz�m).
        /// </summary>
        private Field _HivatkozasiSzam = new Field();

        public Field HivatkozasiSzam
        {
            get { return _HivatkozasiSzam; }
            set { _HivatkozasiSzam = value; }
        }
                          
           
        /// <summary>
        /// IktatasDatuma property
        /// Iktat�s d�tuma
        /// </summary>
        private Field _IktatasDatuma = new Field();

        public Field IktatasDatuma
        {
            get { return _IktatasDatuma; }
            set { _IktatasDatuma = value; }
        }
                          
           
        /// <summary>
        /// ExpedialasDatuma property
        /// Expedi�l�s d�tuma, ez sem kell
        /// </summary>
        private Field _ExpedialasDatuma = new Field();

        public Field ExpedialasDatuma
        {
            get { return _ExpedialasDatuma; }
            set { _ExpedialasDatuma = value; }
        }
                          
           
        /// <summary>
        /// ExpedialasModja property
        /// KCS: EXPEDIALAS_MODJA, nem kell
        /// </summary>
        private Field _ExpedialasModja = new Field();

        public Field ExpedialasModja
        {
            get { return _ExpedialasModja; }
            set { _ExpedialasModja = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Expedial property
        /// Expedi�l�s m�dja, ez sem kell
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Expedial = new Field();

        public Field FelhasznaloCsoport_Id_Expedial
        {
            get { return _FelhasznaloCsoport_Id_Expedial; }
            set { _FelhasznaloCsoport_Id_Expedial = value; }
        }
                          
           
        /// <summary>
        /// Targy property
        /// Irat t�rgya, megnevez�se
        /// </summary>
        private Field _Targy = new Field();

        public Field Targy
        {
            get { return _Targy; }
            set { _Targy = value; }
        }
                          
           
        /// <summary>
        /// Jelleg property
        /// KCS.??
        /// </summary>
        private Field _Jelleg = new Field();

        public Field Jelleg
        {
            get { return _Jelleg; }
            set { _Jelleg = value; }
        }
                          
           
        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Iktato property
        /// Iktat� felhaszn�l� Id
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Iktato = new Field();

        public Field FelhasznaloCsoport_Id_Iktato
        {
            get { return _FelhasznaloCsoport_Id_Iktato; }
            set { _FelhasznaloCsoport_Id_Iktato = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Kiadmany property
        /// 
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Kiadmany = new Field();

        public Field FelhasznaloCsoport_Id_Kiadmany
        {
            get { return _FelhasznaloCsoport_Id_Kiadmany; }
            set { _FelhasznaloCsoport_Id_Kiadmany = value; }
        }
                          
           
        /// <summary>
        /// UgyintezesAlapja property
        /// KCS: UGYINTEZES_ALAPJA:
        /// 0 - Hagyom�nyos, pap�r
        /// 1 - Elektronikus (nincs pap�r)
        /// </summary>
        private Field _UgyintezesAlapja = new Field();

        public Field UgyintezesAlapja
        {
            get { return _UgyintezesAlapja; }
            set { _UgyintezesAlapja = value; }
        }
                          
           
        /// <summary>
        /// AdathordozoTipusa property
        /// KCS:UGYINTEZES_ALAPJA
        /// 
        /// (Kor�bban: ADATHORDOZO_TIPUSA)
        /// 
        /// </summary>
        private Field _AdathordozoTipusa = new Field();

        public Field AdathordozoTipusa
        {
            get { return _AdathordozoTipusa; }
            set { _AdathordozoTipusa = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Az irat iktat�j�nak az iktat�s id�pontj�ban �rv�nyes szervezeti azonos�t�ja.
        /// </summary>
        private Field _Csoport_Id_Felelos = new Field();

        public Field Csoport_Id_Felelos
        {
            get { return _Csoport_Id_Felelos; }
            set { _Csoport_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Ugyfelelos property
        /// �gyfelel�s
        /// </summary>
        private Field _Csoport_Id_Ugyfelelos = new Field();

        public Field Csoport_Id_Ugyfelelos
        {
            get { return _Csoport_Id_Ugyfelelos; }
            set { _Csoport_Id_Ugyfelelos = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Orzo property
        /// Az irat iktat�j�nak azonos�t�ja.
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Orzo = new Field();

        public Field FelhasznaloCsoport_Id_Orzo
        {
            get { return _FelhasznaloCsoport_Id_Orzo; }
            set { _FelhasznaloCsoport_Id_Orzo = value; }
        }
                          
           
        /// <summary>
        /// KiadmanyozniKell property
        /// Az adott iratot kell-e postazas elott kiadmanyozni? 
        /// Alapertelmezes szerint nem (bejovoknel eleve nem, kimenoknel altalaban nem). 1-igen, 0-nem
        /// </summary>
        private Field _KiadmanyozniKell = new Field();

        public Field KiadmanyozniKell
        {
            get { return _KiadmanyozniKell; }
            set { _KiadmanyozniKell = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Ugyintez property
        /// �gyint�z�, �ltal�ban el�ad�
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Ugyintez = new Field();

        public Field FelhasznaloCsoport_Id_Ugyintez
        {
            get { return _FelhasznaloCsoport_Id_Ugyintez; }
            set { _FelhasznaloCsoport_Id_Ugyintez = value; }
        }
                          
           
        /// <summary>
        /// Hatarido property
        /// Elint�z�si hat�rid�
        /// </summary>
        private Field _Hatarido = new Field();

        public Field Hatarido
        {
            get { return _Hatarido; }
            set { _Hatarido = value; }
        }
                          
           
        /// <summary>
        /// MegorzesiIdo property
        /// Irat meg�rz�s eddig a d�tumig.
        /// </summary>
        private Field _MegorzesiIdo = new Field();

        public Field MegorzesiIdo
        {
            get { return _MegorzesiIdo; }
            set { _MegorzesiIdo = value; }
        }
                          
           
        /// <summary>
        /// IratFajta property
        /// KCS: IRAT_FAJTA
        /// </summary>
        private Field _IratFajta = new Field();

        public Field IratFajta
        {
            get { return _IratFajta; }
            set { _IratFajta = value; }
        }
                          
           
        /// <summary>
        /// Irattipus property
        /// KCS:??
        /// </summary>
        private Field _Irattipus = new Field();

        public Field Irattipus
        {
            get { return _Irattipus; }
            set { _Irattipus = value; }
        }
                          
           
        /// <summary>
        /// KuldKuldemenyek_Id property
        /// K�ldem�ny Id
        /// </summary>
        private Field _KuldKuldemenyek_Id = new Field();

        public Field KuldKuldemenyek_Id
        {
            get { return _KuldKuldemenyek_Id; }
            set { _KuldKuldemenyek_Id = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// KCS: IRAT_ALLAPOT
        /// 0 - Megnyitott (munkairat)
        /// 1 - Iktatand� (munkairat)
        /// 2 - Befagyasztott (munkairat)
        /// 04 - Iktatott
        /// 06 - �tiktatott
        /// 30 - Kiadm�nyozott
        /// 90 - Sztorn�zott
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Azonosito property
        /// Iktat�sz�m sz�vegesen 
        /// </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// GeneraltTargy property
        /// el�re r�gz�tett sz�veg, t�rgy az iratmetadefici�k t�bl�b�l 
        /// </summary>
        private Field _GeneraltTargy = new Field();

        public Field GeneraltTargy
        {
            get { return _GeneraltTargy; }
            set { _GeneraltTargy = value; }
        }
                          
           
        /// <summary>
        /// IratMetaDef_Id property
        /// Az iratmetadefinici� t�bl�ra mutat
        /// </summary>
        private Field _IratMetaDef_Id = new Field();

        public Field IratMetaDef_Id
        {
            get { return _IratMetaDef_Id; }
            set { _IratMetaDef_Id = value; }
        }
                          
           
        /// <summary>
        /// IrattarbaKuldDatuma  property
        /// Iratt�rba k�ld�s d�tuma 
        /// </summary>
        private Field _IrattarbaKuldDatuma  = new Field();

        public Field IrattarbaKuldDatuma 
        {
            get { return _IrattarbaKuldDatuma ; }
            set { _IrattarbaKuldDatuma  = value; }
        }
                          
           
        /// <summary>
        /// IrattarbaVetelDat property
        /// Iratt�rba v�tel d�tuma
        /// </summary>
        private Field _IrattarbaVetelDat = new Field();

        public Field IrattarbaVetelDat
        {
            get { return _IrattarbaVetelDat; }
            set { _IrattarbaVetelDat = value; }
        }
                          
           
        /// <summary>
        /// Ugyirat_Id property
        /// Az atu�lis �gyirat Id-je (szerel�s eset�n a legf�ls� szint), egyel�re nem haszn�lt
        /// </summary>
        private Field _Ugyirat_Id = new Field();

        public Field Ugyirat_Id
        {
            get { return _Ugyirat_Id; }
            set { _Ugyirat_Id = value; }
        }
                          
           
        /// <summary>
        /// Minosites property
        /// A nyilv�nos �s nem nyilv�nos iratok megk�l�nb�ztet�se. KCS: IRAT_MINOSITES
        ///  �res- nyilv�nos
        ///  10 - bels� haszn�lat�, nem nyilv�nos
        ///  20 - d�nt�s el�k�sz�t�, nem nyilv�nos
        /// </summary>
        private Field _Minosites = new Field();

        public Field Minosites
        {
            get { return _Minosites; }
            set { _Minosites = value; }
        }
                          
           
        /// <summary>
        /// Elintezett property
        /// 
        /// </summary>
        private Field _Elintezett = new Field();

        public Field Elintezett
        {
            get { return _Elintezett; }
            set { _Elintezett = value; }
        }
                          
           
        /// <summary>
        /// IratHatasaUgyintezesre property
        /// KCS: IRAT_HATASA_UGYINTEZESRE
        /// </summary>
        private Field _IratHatasaUgyintezesre = new Field();

        public Field IratHatasaUgyintezesre
        {
            get { return _IratHatasaUgyintezesre; }
            set { _IratHatasaUgyintezesre = value; }
        }
                          
           
        /// <summary>
        /// FelfuggesztesOka property
        /// 
        /// </summary>
        private Field _FelfuggesztesOka = new Field();

        public Field FelfuggesztesOka
        {
            get { return _FelfuggesztesOka; }
            set { _FelfuggesztesOka = value; }
        }
                          
           
        /// <summary>
        /// LezarasOka property
        /// 
        /// </summary>
        private Field _LezarasOka = new Field();

        public Field LezarasOka
        {
            get { return _LezarasOka; }
            set { _LezarasOka = value; }
        }
                          
           
        /// <summary>
        /// Munkaallomas property
        /// 
        /// </summary>
        private Field _Munkaallomas = new Field();

        public Field Munkaallomas
        {
            get { return _Munkaallomas; }
            set { _Munkaallomas = value; }
        }
                          
           
        /// <summary>
        /// UgyintezesModja property
        /// 
        /// </summary>
        private Field _UgyintezesModja = new Field();

        public Field UgyintezesModja
        {
            get { return _UgyintezesModja; }
            set { _UgyintezesModja = value; }
        }
                          
           
        /// <summary>
        /// IntezesIdopontja property
        /// 
        /// </summary>
        private Field _IntezesIdopontja = new Field();

        public Field IntezesIdopontja
        {
            get { return _IntezesIdopontja; }
            set { _IntezesIdopontja = value; }
        }
                          
           
        /// <summary>
        /// IntezesiIdo property
        /// �tfut�si id� a megadott id�egys�gben (�gyirat, elj�r�si szakasz, irat)
        /// </summary>
        private Field _IntezesiIdo = new Field();

        public Field IntezesiIdo
        {
            get { return _IntezesiIdo; }
            set { _IntezesiIdo = value; }
        }
                          
           
        /// <summary>
        /// IntezesiIdoegyseg property
        /// Az �tfut�si id� id�egys�ge. KCS: IDOEGYSEG
        /// (perc, �ra, nap, ...)
        /// A k�d egyben az id�egys�g percben megadott �rt�ke.
        /// 1- perc
        /// 60 - �ra
        /// 1440 - nap
        /// </summary>
        private Field _IntezesiIdoegyseg = new Field();

        public Field IntezesiIdoegyseg
        {
            get { return _IntezesiIdoegyseg; }
            set { _IntezesiIdoegyseg = value; }
        }
                          
           
        /// <summary>
        /// UzemzavarKezdete property
        /// 
        /// </summary>
        private Field _UzemzavarKezdete = new Field();

        public Field UzemzavarKezdete
        {
            get { return _UzemzavarKezdete; }
            set { _UzemzavarKezdete = value; }
        }
                          
           
        /// <summary>
        /// UzemzavarVege property
        /// 
        /// </summary>
        private Field _UzemzavarVege = new Field();

        public Field UzemzavarVege
        {
            get { return _UzemzavarVege; }
            set { _UzemzavarVege = value; }
        }
                          
           
        /// <summary>
        /// UzemzavarIgazoloIratSzama property
        /// 
        /// </summary>
        private Field _UzemzavarIgazoloIratSzama = new Field();

        public Field UzemzavarIgazoloIratSzama
        {
            get { return _UzemzavarIgazoloIratSzama; }
            set { _UzemzavarIgazoloIratSzama = value; }
        }
                          
           
        /// <summary>
        /// FelfuggesztettNapokSzama property
        /// 
        /// </summary>
        private Field _FelfuggesztettNapokSzama = new Field();

        public Field FelfuggesztettNapokSzama
        {
            get { return _FelfuggesztettNapokSzama; }
            set { _FelfuggesztettNapokSzama = value; }
        }
                          
           
        /// <summary>
        /// VisszafizetesJogcime property
        /// KCS: VISSZAFIZETES_JOGCIME
        /// - 01- d�j
        /// - 02 - illet�k
        /// </summary>
        private Field _VisszafizetesJogcime = new Field();

        public Field VisszafizetesJogcime
        {
            get { return _VisszafizetesJogcime; }
            set { _VisszafizetesJogcime = value; }
        }
                          
           
        /// <summary>
        /// VisszafizetesOsszege property
        /// 
        /// </summary>
        private Field _VisszafizetesOsszege = new Field();

        public Field VisszafizetesOsszege
        {
            get { return _VisszafizetesOsszege; }
            set { _VisszafizetesOsszege = value; }
        }
                          
           
        /// <summary>
        /// VisszafizetesHatarozatSzama property
        /// 
        /// </summary>
        private Field _VisszafizetesHatarozatSzama = new Field();

        public Field VisszafizetesHatarozatSzama
        {
            get { return _VisszafizetesHatarozatSzama; }
            set { _VisszafizetesHatarozatSzama = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id_VisszafizetesCimzet property
        /// 
        /// </summary>
        private Field _Partner_Id_VisszafizetesCimzet = new Field();

        public Field Partner_Id_VisszafizetesCimzet
        {
            get { return _Partner_Id_VisszafizetesCimzet; }
            set { _Partner_Id_VisszafizetesCimzet = value; }
        }
                          
           
        /// <summary>
        /// Partner_Nev_VisszafizetCimzett property
        /// 
        /// </summary>
        private Field _Partner_Nev_VisszafizetCimzett = new Field();

        public Field Partner_Nev_VisszafizetCimzett
        {
            get { return _Partner_Nev_VisszafizetCimzett; }
            set { _Partner_Nev_VisszafizetCimzett = value; }
        }
                          
           
        /// <summary>
        /// VisszafizetesHatarido property
        /// 
        /// </summary>
        private Field _VisszafizetesHatarido = new Field();

        public Field VisszafizetesHatarido
        {
            get { return _VisszafizetesHatarido; }
            set { _VisszafizetesHatarido = value; }
        }
                          
           
        /// <summary>
        /// Minosito property
        /// 
        /// </summary>
        private Field _Minosito = new Field();

        public Field Minosito
        {
            get { return _Minosito; }
            set { _Minosito = value; }
        }
                          
           
        /// <summary>
        /// MinositesErvenyessegiIdeje property
        /// 
        /// </summary>
        private Field _MinositesErvenyessegiIdeje = new Field();

        public Field MinositesErvenyessegiIdeje
        {
            get { return _MinositesErvenyessegiIdeje; }
            set { _MinositesErvenyessegiIdeje = value; }
        }
                          
           
        /// <summary>
        /// TerjedelemMennyiseg property
        /// 
        /// </summary>
        private Field _TerjedelemMennyiseg = new Field();

        public Field TerjedelemMennyiseg
        {
            get { return _TerjedelemMennyiseg; }
            set { _TerjedelemMennyiseg = value; }
        }
                          
           
        /// <summary>
        /// TerjedelemMennyisegiEgyseg property
        /// 
        /// </summary>
        private Field _TerjedelemMennyisegiEgyseg = new Field();

        public Field TerjedelemMennyisegiEgyseg
        {
            get { return _TerjedelemMennyisegiEgyseg; }
            set { _TerjedelemMennyisegiEgyseg = value; }
        }
                          
           
        /// <summary>
        /// TerjedelemMegjegyzes property
        /// 
        /// </summary>
        private Field _TerjedelemMegjegyzes = new Field();

        public Field TerjedelemMegjegyzes
        {
            get { return _TerjedelemMegjegyzes; }
            set { _TerjedelemMegjegyzes = value; }
        }
                          
           
        /// <summary>
        /// SelejtezesDat property
        /// Selejtez�s v. lev�lt�rba ad�s d�tuma
        /// </summary>
        private Field _SelejtezesDat = new Field();

        public Field SelejtezesDat
        {
            get { return _SelejtezesDat; }
            set { _SelejtezesDat = value; }
        }
                          
           
        /// <summary>
        /// FelhCsoport_Id_Selejtezo property
        /// Selejtez� v. lev�lt�rba ad� felh. csoport Id-je
        /// </summary>
        private Field _FelhCsoport_Id_Selejtezo = new Field();

        public Field FelhCsoport_Id_Selejtezo
        {
            get { return _FelhCsoport_Id_Selejtezo; }
            set { _FelhCsoport_Id_Selejtezo = value; }
        }
                          
           
        /// <summary>
        /// LeveltariAtvevoNeve property
        /// Lev�lt�ri �tvev� neve
        /// </summary>
        private Field _LeveltariAtvevoNeve = new Field();

        public Field LeveltariAtvevoNeve
        {
            get { return _LeveltariAtvevoNeve; }
            set { _LeveltariAtvevoNeve = value; }
        }
                          
           
        /// <summary>
        /// ModositasErvenyessegKezdete property
        /// 
        /// </summary>
        private Field _ModositasErvenyessegKezdete = new Field();

        public Field ModositasErvenyessegKezdete
        {
            get { return _ModositasErvenyessegKezdete; }
            set { _ModositasErvenyessegKezdete = value; }
        }
                          
           
        /// <summary>
        /// MegszuntetoHatarozat property
        /// 
        /// </summary>
        private Field _MegszuntetoHatarozat = new Field();

        public Field MegszuntetoHatarozat
        {
            get { return _MegszuntetoHatarozat; }
            set { _MegszuntetoHatarozat = value; }
        }
                          
           
        /// <summary>
        /// FelulvizsgalatDatuma property
        /// 
        /// </summary>
        private Field _FelulvizsgalatDatuma = new Field();

        public Field FelulvizsgalatDatuma
        {
            get { return _FelulvizsgalatDatuma; }
            set { _FelulvizsgalatDatuma = value; }
        }
                          
           
        /// <summary>
        /// Felulvizsgalo_id property
        /// 
        /// </summary>
        private Field _Felulvizsgalo_id = new Field();

        public Field Felulvizsgalo_id
        {
            get { return _Felulvizsgalo_id; }
            set { _Felulvizsgalo_id = value; }
        }
                          
           
        /// <summary>
        /// MinositesFelulvizsgalatEredm property
        /// 
        /// </summary>
        private Field _MinositesFelulvizsgalatEredm = new Field();

        public Field MinositesFelulvizsgalatEredm
        {
            get { return _MinositesFelulvizsgalatEredm; }
            set { _MinositesFelulvizsgalatEredm = value; }
        }
                          
           
        /// <summary>
        /// UgyintezesKezdoDatuma property
        /// 
        /// </summary>
        private Field _UgyintezesKezdoDatuma = new Field();

        public Field UgyintezesKezdoDatuma
        {
            get { return _UgyintezesKezdoDatuma; }
            set { _UgyintezesKezdoDatuma = value; }
        }
                          
           
        /// <summary>
        /// EljarasiSzakasz property
        /// 
        /// </summary>
        private Field _EljarasiSzakasz = new Field();

        public Field EljarasiSzakasz
        {
            get { return _EljarasiSzakasz; }
            set { _EljarasiSzakasz = value; }
        }
                          
           
        /// <summary>
        /// HatralevoNapok property
        /// 
        /// </summary>
        private Field _HatralevoNapok = new Field();

        public Field HatralevoNapok
        {
            get { return _HatralevoNapok; }
            set { _HatralevoNapok = value; }
        }
                          
           
        /// <summary>
        /// HatralevoMunkaNapok property
        /// 
        /// </summary>
        private Field _HatralevoMunkaNapok = new Field();

        public Field HatralevoMunkaNapok
        {
            get { return _HatralevoMunkaNapok; }
            set { _HatralevoMunkaNapok = value; }
        }
                          
           
        /// <summary>
        /// Ugy_Fajtaja property
        /// 
        /// </summary>
        private Field _Ugy_Fajtaja = new Field();

        public Field Ugy_Fajtaja
        {
            get { return _Ugy_Fajtaja; }
            set { _Ugy_Fajtaja = value; }
        }
                          
           
        /// <summary>
        /// MinositoSzervezet property
        /// 
        /// </summary>
        private Field _MinositoSzervezet = new Field();

        public Field MinositoSzervezet
        {
            get { return _MinositoSzervezet; }
            set { _MinositoSzervezet = value; }
        }
                          
           
        /// <summary>
        /// MinositesFelulvizsgalatBizTag property
        /// 
        /// </summary>
        private Field _MinositesFelulvizsgalatBizTag = new Field();

        public Field MinositesFelulvizsgalatBizTag
        {
            get { return _MinositesFelulvizsgalatBizTag; }
            set { _MinositesFelulvizsgalatBizTag = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}