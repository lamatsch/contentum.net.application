
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IraIrattariTetelek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IraIrattariTetelekSearch: BaseSearchObject    {
      public EREC_IraIrattariTetelekSearch()
      {            
                     _Id.Name = "EREC_IraIrattariTetelek.Id";
               _Id.Type = "Guid";            
               _Org.Name = "EREC_IraIrattariTetelek.Org";
               _Org.Type = "Guid";            
               _IrattariTetelszam.Name = "EREC_IraIrattariTetelek.IrattariTetelszam";
               _IrattariTetelszam.Type = "String";            
               _Nev.Name = "EREC_IraIrattariTetelek.Nev";
               _Nev.Type = "String";            
               _IrattariJel.Name = "EREC_IraIrattariTetelek.IrattariJel";
               _IrattariJel.Type = "String";            
               _MegorzesiIdo.Name = "EREC_IraIrattariTetelek.MegorzesiIdo";
               _MegorzesiIdo.Type = "String";            
               _Idoegyseg.Name = "EREC_IraIrattariTetelek.Idoegyseg";
               _Idoegyseg.Type = "String";            
               _Folyo_CM.Name = "EREC_IraIrattariTetelek.Folyo_CM";
               _Folyo_CM.Type = "String";            
               _AgazatiJel_Id.Name = "EREC_IraIrattariTetelek.AgazatiJel_Id";
               _AgazatiJel_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_IraIrattariTetelek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IraIrattariTetelek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _EvenTuliIktatas.Name = "EREC_IraIrattariTetelek.EvenTuliIktatas";
               _EvenTuliIktatas.Type = "Char";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// IrattariTetelszam property
        /// Iratt�ri t�telsz�m
        /// </summary>
        private Field _IrattariTetelszam = new Field();

        public Field IrattariTetelszam
        {
            get { return _IrattariTetelszam; }
            set { _IrattariTetelszam = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// Iratt�ri t�tel megnevez�se
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// IrattariJel property
        /// Iratt�ri jel
        /// </summary>
        private Field _IrattariJel = new Field();

        public Field IrattariJel
        {
            get { return _IrattariJel; }
            set { _IrattariJel = value; }
        }
                          
           
        /// <summary>
        /// MegorzesiIdo property
        /// KCS: SELEJTEZESI_IDO
        /// 1 = 2
        /// 2 = 5
        /// 3 = 10
        /// 4 = 15
        /// 5 = 30
        /// 6 = 75
        /// 0 = nem selejtezhet�
        /// 
        /// </summary>
        private Field _MegorzesiIdo = new Field();

        public Field MegorzesiIdo
        {
            get { return _MegorzesiIdo; }
            set { _MegorzesiIdo = value; }
        }
                          
           
        /// <summary>
        /// Idoegyseg property
        /// //A meg�rz�si id� egys�ge: nap, �v, vagy szab�ly alapj�n sz�molt.
        /// //N - nap, E-�v,  S-szab�ly alapj�n sz�molt.
        /// A meg�rz�si id� egys�ge:IDOEGYSEG k�dt�rb�l.
        /// </summary>
        private Field _Idoegyseg = new Field();

        public Field Idoegyseg
        {
            get { return _Idoegyseg; }
            set { _Idoegyseg = value; }
        }
                          
           
        /// <summary>
        /// Folyo_CM property
        /// ??
        /// </summary>
        private Field _Folyo_CM = new Field();

        public Field Folyo_CM
        {
            get { return _Folyo_CM; }
            set { _Folyo_CM = value; }
        }
                          
           
        /// <summary>
        /// AgazatiJel_Id property
        /// �gazati jel Id
        /// </summary>
        private Field _AgazatiJel_Id = new Field();

        public Field AgazatiJel_Id
        {
            get { return _AgazatiJel_Id; }
            set { _AgazatiJel_Id = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// EvenTuliIktatas property
        /// 
        /// </summary>
        private Field _EvenTuliIktatas = new Field();

        public Field EvenTuliIktatas
        {
            get { return _EvenTuliIktatas; }
            set { _EvenTuliIktatas = value; }
        }
                              }

}