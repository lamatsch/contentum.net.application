
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IraJegyzekTetelek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IraJegyzekTetelekSearch: BaseSearchObject    {
      public EREC_IraJegyzekTetelekSearch()
      {            
                     _Id.Name = "EREC_IraJegyzekTetelek.Id";
               _Id.Type = "Guid";            
               _Jegyzek_Id.Name = "EREC_IraJegyzekTetelek.Jegyzek_Id";
               _Jegyzek_Id.Type = "Guid";            
               _Sorszam.Name = "EREC_IraJegyzekTetelek.Sorszam";
               _Sorszam.Type = "Int32";            
               _Obj_Id.Name = "EREC_IraJegyzekTetelek.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id.Name = "EREC_IraJegyzekTetelek.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Obj_type.Name = "EREC_IraJegyzekTetelek.Obj_type";
               _Obj_type.Type = "String";            
               _Allapot.Name = "EREC_IraJegyzekTetelek.Allapot";
               _Allapot.Type = "String";            
               _Megjegyzes.Name = "EREC_IraJegyzekTetelek.Megjegyzes";
               _Megjegyzes.Type = "String";            
               _Azonosito.Name = "EREC_IraJegyzekTetelek.Azonosito";
               _Azonosito.Type = "String";            
               _SztornozasDat.Name = "EREC_IraJegyzekTetelek.SztornozasDat";
               _SztornozasDat.Type = "DateTime";            
               _AtadasDatuma.Name = "EREC_IraJegyzekTetelek.AtadasDatuma";
               _AtadasDatuma.Type = "DateTime";            
               _ErvKezd.Name = "EREC_IraJegyzekTetelek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IraJegyzekTetelek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Jegyzek_Id property
        /// Iratt�ri jegyz�k t�tel Id
        /// </summary>
        private Field _Jegyzek_Id = new Field();

        public Field Jegyzek_Id
        {
            get { return _Jegyzek_Id; }
            set { _Jegyzek_Id = value; }
        }
                          
           
        /// <summary>
        /// Sorszam property
        /// Review: decimal(18)
        /// </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }
                          
           
        /// <summary>
        /// Obj_Id property
        /// Ez annak a valaminek a GUID -ja ("allObjGuids"-b�l)  amit csatoltunk
        /// </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// ObjTip_Id property
        /// Ez annak a valaminek a t�pusa, amit csatoltunk
        /// </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Obj_type property
        /// Obj. t�pus, itt: a t�bla neve
        /// </summary>
        private Field _Obj_type = new Field();

        public Field Obj_type
        {
            get { return _Obj_type; }
            set { _Obj_type = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// KCS: ??
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Megjegyzes property
        /// Megjegyz�s
        /// </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// Azonosito property
        /// Objektum azonos�t� sz�vegesen
        /// </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// SztornozasDat property
        /// Sztorn�z�s d�tuma
        /// </summary>
        private Field _SztornozasDat = new Field();

        public Field SztornozasDat
        {
            get { return _SztornozasDat; }
            set { _SztornozasDat = value; }
        }
                          
           
        /// <summary>
        /// AtadasDatuma property
        /// Sztorn�z�s d�tuma
        /// </summary>
        private Field _AtadasDatuma = new Field();

        public Field AtadasDatuma
        {
            get { return _AtadasDatuma; }
            set { _AtadasDatuma = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}