
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IraJegyzekek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_IraJegyzekekSearch: BaseSearchObject    {
      public EREC_IraJegyzekekSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "EREC_IraJegyzekek.Id";
               _Id.Type = "Guid";            
               _Org.Name = "EREC_IraJegyzekek.Org";
               _Org.Type = "Guid";            
               _Tipus.Name = "EREC_IraJegyzekek.Tipus";
               _Tipus.Type = "Char";            
               _Minosites.Name = "EREC_IraJegyzekek.Minosites";
               _Minosites.Type = "String";            
               _Csoport_Id_felelos.Name = "EREC_IraJegyzekek.Csoport_Id_felelos";
               _Csoport_Id_felelos.Type = "Guid";            
               _FelhasznaloCsoport_Id_Vegrehaj.Name = "EREC_IraJegyzekek.FelhasznaloCsoport_Id_Vegrehaj";
               _FelhasznaloCsoport_Id_Vegrehaj.Type = "Guid";            
               _Partner_Id_LeveltariAtvevo.Name = "EREC_IraJegyzekek.Partner_Id_LeveltariAtvevo";
               _Partner_Id_LeveltariAtvevo.Type = "Guid";            
               _LezarasDatuma.Name = "EREC_IraJegyzekek.LezarasDatuma";
               _LezarasDatuma.Type = "DateTime";            
               _SztornirozasDat.Name = "EREC_IraJegyzekek.SztornirozasDat";
               _SztornirozasDat.Type = "DateTime";            
               _Nev.Name = "EREC_IraJegyzekek.Nev";
               _Nev.Type = "String";            
               _VegrehajtasDatuma.Name = "EREC_IraJegyzekek.VegrehajtasDatuma";
               _VegrehajtasDatuma.Type = "DateTime";            
               _Foszam.Name = "EREC_IraJegyzekek.Foszam";
               _Foszam.Type = "Int32";            
               _Azonosito.Name = "EREC_IraJegyzekek.Azonosito";
               _Azonosito.Type = "String";            
               _MegsemmisitesDatuma.Name = "EREC_IraJegyzekek.MegsemmisitesDatuma";
               _MegsemmisitesDatuma.Type = "DateTime";            
               _Csoport_Id_FelelosSzervezet.Name = "EREC_IraJegyzekek.Csoport_Id_FelelosSzervezet";
               _Csoport_Id_FelelosSzervezet.Type = "Guid";            
               _IraIktatokonyv_Id.Name = "EREC_IraJegyzekek.IraIktatokonyv_Id";
               _IraIktatokonyv_Id.Type = "Guid";            
               _Allapot.Name = "EREC_IraJegyzekek.Allapot";
               _Allapot.Type = "String";            
               _VegrehajtasKezdoDatuma.Name = "EREC_IraJegyzekek.VegrehajtasKezdoDatuma";
               _VegrehajtasKezdoDatuma.Type = "DateTime";            
               _ErvKezd.Name = "EREC_IraJegyzekek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IraJegyzekek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Tipus property
        /// ??
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Minosites property
        /// nem haszn�lt
        /// </summary>
        private Field _Minosites = new Field();

        public Field Minosites
        {
            get { return _Minosites; }
            set { _Minosites = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_felelos property
        /// Felel�s felhaszn�l� Id
        /// </summary>
        private Field _Csoport_Id_felelos = new Field();

        public Field Csoport_Id_felelos
        {
            get { return _Csoport_Id_felelos; }
            set { _Csoport_Id_felelos = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Vegrehaj property
        /// V�grehajt� felhaszn�l�
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Vegrehaj = new Field();

        public Field FelhasznaloCsoport_Id_Vegrehaj
        {
            get { return _FelhasznaloCsoport_Id_Vegrehaj; }
            set { _FelhasznaloCsoport_Id_Vegrehaj = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id_LeveltariAtvevo property
        /// Lev�lt�ri �tvev�
        /// </summary>
        private Field _Partner_Id_LeveltariAtvevo = new Field();

        public Field Partner_Id_LeveltariAtvevo
        {
            get { return _Partner_Id_LeveltariAtvevo; }
            set { _Partner_Id_LeveltariAtvevo = value; }
        }
                          
           
        /// <summary>
        /// LezarasDatuma property
        /// Lez�r�s d�tuma
        /// </summary>
        private Field _LezarasDatuma = new Field();

        public Field LezarasDatuma
        {
            get { return _LezarasDatuma; }
            set { _LezarasDatuma = value; }
        }
                          
           
        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// Iratt�ri jegyz�k megnevez�se
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// VegrehajtasDatuma property
        /// Leveltari �tadas id�pontja
        /// </summary>
        private Field _VegrehajtasDatuma = new Field();

        public Field VegrehajtasDatuma
        {
            get { return _VegrehajtasDatuma; }
            set { _VegrehajtasDatuma = value; }
        }
                          
           
        /// <summary>
        /// Foszam property
        /// Iktat�si f�sz�m
        /// </summary>
        private Field _Foszam = new Field();

        public Field Foszam
        {
            get { return _Foszam; }
            set { _Foszam = value; }
        }
                          
           
        /// <summary>
        /// Azonosito property
        /// �gyirat azonos�t�: f�sz�m, �v 
        /// </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// MegsemmisitesDatuma property
        /// Leveltari �tadas id�pontja
        /// </summary>
        private Field _MegsemmisitesDatuma = new Field();

        public Field MegsemmisitesDatuma
        {
            get { return _MegsemmisitesDatuma; }
            set { _MegsemmisitesDatuma = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_FelelosSzervezet property
        /// 
        /// </summary>
        private Field _Csoport_Id_FelelosSzervezet = new Field();

        public Field Csoport_Id_FelelosSzervezet
        {
            get { return _Csoport_Id_FelelosSzervezet; }
            set { _Csoport_Id_FelelosSzervezet = value; }
        }
                          
           
        /// <summary>
        /// IraIktatokonyv_Id property
        /// IraIktatokonyv Id
        /// </summary>
        private Field _IraIktatokonyv_Id = new Field();

        public Field IraIktatokonyv_Id
        {
            get { return _IraIktatokonyv_Id; }
            set { _IraIktatokonyv_Id = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// KCS: JEGYZEK_ALLAPOT
        /// 
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// VegrehajtasKezdoDatuma property
        /// Aszinkron v�grehajt�s eset�n a v�grehajt�s kezdet�nek d�tuma.
        /// </summary>
        private Field _VegrehajtasKezdoDatuma = new Field();

        public Field VegrehajtasKezdoDatuma
        {
            get { return _VegrehajtasKezdoDatuma; }
            set { _VegrehajtasKezdoDatuma = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}