
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IraKezbesitesiFejek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_IraKezbesitesiFejekSearch: BaseSearchObject    {
      public EREC_IraKezbesitesiFejekSearch()
      {
          Init_ManualFields();

                     _Id.Name = "EREC_IraKezbesitesiFejek.Id";
               _Id.Type = "Guid";            
               _CsomagAzonosito.Name = "EREC_IraKezbesitesiFejek.CsomagAzonosito";
               _CsomagAzonosito.Type = "String";            
               _Tipus.Name = "EREC_IraKezbesitesiFejek.Tipus";
               _Tipus.Type = "Char";            
               _UtolsoNyomtatasIdo.Name = "EREC_IraKezbesitesiFejek.UtolsoNyomtatasIdo";
               _UtolsoNyomtatasIdo.Type = "DateTime";            
               _UtolsoNyomtatas_Id.Name = "EREC_IraKezbesitesiFejek.UtolsoNyomtatas_Id";
               _UtolsoNyomtatas_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_IraKezbesitesiFejek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IraKezbesitesiFejek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _CsomagAzonosito = new Field();

        public Field CsomagAzonosito
        {
            get { return _CsomagAzonosito; }
            set { _CsomagAzonosito = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UtolsoNyomtatasIdo = new Field();

        public Field UtolsoNyomtatasIdo
        {
            get { return _UtolsoNyomtatasIdo; }
            set { _UtolsoNyomtatasIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UtolsoNyomtatas_Id = new Field();

        public Field UtolsoNyomtatas_Id
        {
            get { return _UtolsoNyomtatas_Id; }
            set { _UtolsoNyomtatas_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}