
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IraKezbesitesiTetelek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_IraKezbesitesiTetelekSearch: BaseSearchObject    {
      public EREC_IraKezbesitesiTetelekSearch()
      {
          Init_ManualFields();

                     _Id.Name = "EREC_IraKezbesitesiTetelek.Id";
               _Id.Type = "Guid";            
               _KezbesitesFej_Id.Name = "EREC_IraKezbesitesiTetelek.KezbesitesFej_Id";
               _KezbesitesFej_Id.Type = "Guid";            
               _AtveteliFej_Id.Name = "EREC_IraKezbesitesiTetelek.AtveteliFej_Id";
               _AtveteliFej_Id.Type = "Guid";            
               _Obj_Id.Name = "EREC_IraKezbesitesiTetelek.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id.Name = "EREC_IraKezbesitesiTetelek.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Obj_type.Name = "EREC_IraKezbesitesiTetelek.Obj_type";
               _Obj_type.Type = "String";            
               _AtadasDat.Name = "EREC_IraKezbesitesiTetelek.AtadasDat";
               _AtadasDat.Type = "DateTime";            
               _AtvetelDat.Name = "EREC_IraKezbesitesiTetelek.AtvetelDat";
               _AtvetelDat.Type = "DateTime";            
               _Megjegyzes.Name = "EREC_IraKezbesitesiTetelek.Megjegyzes";
               _Megjegyzes.Type = "String";            
               _SztornirozasDat.Name = "EREC_IraKezbesitesiTetelek.SztornirozasDat";
               _SztornirozasDat.Type = "DateTime";            
               _UtolsoNyomtatas_Id.Name = "EREC_IraKezbesitesiTetelek.UtolsoNyomtatas_Id";
               _UtolsoNyomtatas_Id.Type = "Guid";            
               _UtolsoNyomtatasIdo.Name = "EREC_IraKezbesitesiTetelek.UtolsoNyomtatasIdo";
               _UtolsoNyomtatasIdo.Type = "DateTime";            
               _Felhasznalo_Id_Atado_USER.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER";
               _Felhasznalo_Id_Atado_USER.Type = "Guid";            
               _Felhasznalo_Id_Atado_LOGIN.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_LOGIN";
               _Felhasznalo_Id_Atado_LOGIN.Type = "Guid";            
               _Csoport_Id_Cel.Name = "EREC_IraKezbesitesiTetelek.Csoport_Id_Cel";
               _Csoport_Id_Cel.Type = "Guid";            
               _Felhasznalo_Id_AtvevoUser.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser";
               _Felhasznalo_Id_AtvevoUser.Type = "Guid";            
               _Felhasznalo_Id_AtvevoLogin.Name = "EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoLogin";
               _Felhasznalo_Id_AtvevoLogin.Type = "Guid";            
               _Allapot.Name = "EREC_IraKezbesitesiTetelek.Allapot";
               _Allapot.Type = "Char";            
               _Azonosito_szoveges.Name = "EREC_IraKezbesitesiTetelek.Azonosito_szoveges";
               _Azonosito_szoveges.Type = "String";            
               _UgyintezesModja.Name = "EREC_IraKezbesitesiTetelek.UgyintezesModja";
               _UgyintezesModja.Type = "String";            
               _ErvKezd.Name = "EREC_IraKezbesitesiTetelek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IraKezbesitesiTetelek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KezbesitesFej_Id = new Field();

        public Field KezbesitesFej_Id
        {
            get { return _KezbesitesFej_Id; }
            set { _KezbesitesFej_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AtveteliFej_Id = new Field();

        public Field AtveteliFej_Id
        {
            get { return _AtveteliFej_Id; }
            set { _AtveteliFej_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_type = new Field();

        public Field Obj_type
        {
            get { return _Obj_type; }
            set { _Obj_type = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AtadasDat = new Field();

        public Field AtadasDat
        {
            get { return _AtadasDat; }
            set { _AtadasDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AtvetelDat = new Field();

        public Field AtvetelDat
        {
            get { return _AtvetelDat; }
            set { _AtvetelDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UtolsoNyomtatas_Id = new Field();

        public Field UtolsoNyomtatas_Id
        {
            get { return _UtolsoNyomtatas_Id; }
            set { _UtolsoNyomtatas_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UtolsoNyomtatasIdo = new Field();

        public Field UtolsoNyomtatasIdo
        {
            get { return _UtolsoNyomtatasIdo; }
            set { _UtolsoNyomtatasIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id_Atado_USER = new Field();

        public Field Felhasznalo_Id_Atado_USER
        {
            get { return _Felhasznalo_Id_Atado_USER; }
            set { _Felhasznalo_Id_Atado_USER = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id_Atado_LOGIN = new Field();

        public Field Felhasznalo_Id_Atado_LOGIN
        {
            get { return _Felhasznalo_Id_Atado_LOGIN; }
            set { _Felhasznalo_Id_Atado_LOGIN = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Cel = new Field();

        public Field Csoport_Id_Cel
        {
            get { return _Csoport_Id_Cel; }
            set { _Csoport_Id_Cel = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id_AtvevoUser = new Field();

        public Field Felhasznalo_Id_AtvevoUser
        {
            get { return _Felhasznalo_Id_AtvevoUser; }
            set { _Felhasznalo_Id_AtvevoUser = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id_AtvevoLogin = new Field();

        public Field Felhasznalo_Id_AtvevoLogin
        {
            get { return _Felhasznalo_Id_AtvevoLogin; }
            set { _Felhasznalo_Id_AtvevoLogin = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Azonosito_szoveges = new Field();

        public Field Azonosito_szoveges
        {
            get { return _Azonosito_szoveges; }
            set { _Azonosito_szoveges = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgyintezesModja = new Field();

        public Field UgyintezesModja
        {
            get { return _UgyintezesModja; }
            set { _UgyintezesModja = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}