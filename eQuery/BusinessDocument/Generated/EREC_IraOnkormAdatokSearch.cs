
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IraOnkormAdatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IraOnkormAdatokSearch: BaseSearchObject    {
      public EREC_IraOnkormAdatokSearch()
      {            
                     _Id.Name = "EREC_IraOnkormAdatok.Id";
               _Id.Type = "Guid";            
               _IraIratok_Id.Name = "EREC_IraOnkormAdatok.IraIratok_Id";
               _IraIratok_Id.Type = "Guid";            
               _UgyFajtaja.Name = "EREC_IraOnkormAdatok.UgyFajtaja";
               _UgyFajtaja.Type = "String";            
               _DontestHozta.Name = "EREC_IraOnkormAdatok.DontestHozta";
               _DontestHozta.Type = "String";            
               _DontesFormaja.Name = "EREC_IraOnkormAdatok.DontesFormaja";
               _DontesFormaja.Type = "String";            
               _UgyintezesHataridore.Name = "EREC_IraOnkormAdatok.UgyintezesHataridore";
               _UgyintezesHataridore.Type = "String";            
               _HataridoTullepes.Name = "EREC_IraOnkormAdatok.HataridoTullepes";
               _HataridoTullepes.Type = "Int32";            
               _JogorvoslatiEljarasTipusa.Name = "EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa";
               _JogorvoslatiEljarasTipusa.Type = "String";            
               _JogorvoslatiDontesTipusa.Name = "EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa";
               _JogorvoslatiDontesTipusa.Type = "String";            
               _JogorvoslatiDontestHozta.Name = "EREC_IraOnkormAdatok.JogorvoslatiDontestHozta";
               _JogorvoslatiDontestHozta.Type = "String";            
               _JogorvoslatiDontesTartalma.Name = "EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma";
               _JogorvoslatiDontesTartalma.Type = "String";            
               _JogorvoslatiDontes.Name = "EREC_IraOnkormAdatok.JogorvoslatiDontes";
               _JogorvoslatiDontes.Type = "String";            
               _HatosagiEllenorzes.Name = "EREC_IraOnkormAdatok.HatosagiEllenorzes";
               _HatosagiEllenorzes.Type = "Char";            
               _MunkaorakSzama.Name = "EREC_IraOnkormAdatok.MunkaorakSzama";
               _MunkaorakSzama.Type = "Double";            
               _EljarasiKoltseg.Name = "EREC_IraOnkormAdatok.EljarasiKoltseg";
               _EljarasiKoltseg.Type = "Int32";            
               _KozigazgatasiBirsagMerteke.Name = "EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke";
               _KozigazgatasiBirsagMerteke.Type = "Int32";            
               _SommasEljDontes.Name = "EREC_IraOnkormAdatok.SommasEljDontes";
               _SommasEljDontes.Type = "String";            
               _NyolcNapBelulNemSommas.Name = "EREC_IraOnkormAdatok.NyolcNapBelulNemSommas";
               _NyolcNapBelulNemSommas.Type = "String";            
               _FuggoHatalyuHatarozat.Name = "EREC_IraOnkormAdatok.FuggoHatalyuHatarozat";
               _FuggoHatalyuHatarozat.Type = "String";            
               _FuggoHatHatalybaLepes.Name = "EREC_IraOnkormAdatok.FuggoHatHatalybaLepes";
               _FuggoHatHatalybaLepes.Type = "String";            
               _FuggoHatalyuVegzes.Name = "EREC_IraOnkormAdatok.FuggoHatalyuVegzes";
               _FuggoHatalyuVegzes.Type = "String";            
               _FuggoVegzesHatalyba.Name = "EREC_IraOnkormAdatok.FuggoVegzesHatalyba";
               _FuggoVegzesHatalyba.Type = "String";            
               _HatAltalVisszafizOsszeg.Name = "EREC_IraOnkormAdatok.HatAltalVisszafizOsszeg";
               _HatAltalVisszafizOsszeg.Type = "Int32";            
               _HatTerheloEljKtsg.Name = "EREC_IraOnkormAdatok.HatTerheloEljKtsg";
               _HatTerheloEljKtsg.Type = "Int32";            
               _FelfuggHatarozat.Name = "EREC_IraOnkormAdatok.FelfuggHatarozat";
               _FelfuggHatarozat.Type = "String";            
               _ErvKezd.Name = "EREC_IraOnkormAdatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IraOnkormAdatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�, azonos az �gyirat Id-vel
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// IraIratok_Id property
        /// 
        /// </summary>
        private Field _IraIratok_Id = new Field();

        public Field IraIratok_Id
        {
            get { return _IraIratok_Id; }
            set { _IraIratok_Id = value; }
        }
                          
           
        /// <summary>
        /// UgyFajtaja property
        /// 2008.03.31 KCS: kcs.UGY_FAJTAJA
        /// 
        /// </summary>
        private Field _UgyFajtaja = new Field();

        public Field UgyFajtaja
        {
            get { return _UgyFajtaja; }
            set { _UgyFajtaja = value; }
        }
                          
           
        /// <summary>
        /// DontestHozta property
        /// 2. A d�nt�st hozta -> KCS: DONTEST_HOZTA
        /// k�zgy�l�s test�lete (1)
        /// k�zgy�l�s bizotts�ga (2)
        /// a r�sz�nkorm�nyzat test�lete (3)
        /// a (f�)polg�rmester (4)
        /// a (f�)jegyz� (5)
        /// </summary>
        private Field _DontestHozta = new Field();

        public Field DontestHozta
        {
            get { return _DontestHozta; }
            set { _DontestHozta = value; }
        }
                          
           
        /// <summary>
        /// DontesFormaja property
        /// 3. A d�nt�s form�ja -> KCS: DONTES_FORMAJA
        /// hat�rozat (1)
        /// egyezs�g j�v�hagy�s�t tartalmaz� hat�rozat (2)
        /// hat�s�gi szerz�d�s (3)
        /// a Ket. 30. � alapj�n t�rt�n� elutas�t�s (4)
        /// a Ket. 31. � alapj�n t�rt�n� megsz�ntet�se (5)
        /// az els�fok� elj�r�sban hozott egy�b v�gz�s (6)
        /// 
        /// </summary>
        private Field _DontesFormaja = new Field();

        public Field DontesFormaja
        {
            get { return _DontesFormaja; }
            set { _DontesFormaja = value; }
        }
                          
           
        /// <summary>
        /// UgyintezesHataridore property
        /// 4. Az �gyint�z�s id�tartama -> KCS: UGYINTEZES_IDOTARTAMA
        /// hat�rid�n bel�l (1)
        /// hat�rid�n t�l (2)
        /// </summary>
        private Field _UgyintezesHataridore = new Field();

        public Field UgyintezesHataridore
        {
            get { return _UgyintezesHataridore; }
            set { _UgyintezesHataridore = value; }
        }
                          
           
        /// <summary>
        /// HataridoTullepes property
        /// Az �rdemi d�nt�ssel kapcsolatos esetleges hat�rid�-t�ll�p�s id�tartlma napokban (0-999)
        /// </summary>
        private Field _HataridoTullepes = new Field();

        public Field HataridoTullepes
        {
            get { return _HataridoTullepes; }
            set { _HataridoTullepes = value; }
        }
                          
           
        /// <summary>
        /// JogorvoslatiEljarasTipusa property
        /// 5. A jogorvoslati elj�r�s t�pusa -> KCS: JOGORVOSLATI_ELJARAS_TIPUSA
        /// k�relem alapj�n indult (1)
        /// hivatalb�l indult (2)
        /// </summary>
        private Field _JogorvoslatiEljarasTipusa = new Field();

        public Field JogorvoslatiEljarasTipusa
        {
            get { return _JogorvoslatiEljarasTipusa; }
            set { _JogorvoslatiEljarasTipusa = value; }
        }
                          
           
        /// <summary>
        /// JogorvoslatiDontesTipusa property
        /// 6. A jogorvoslati elj�r�s sor�n �rintett d�nt�s t�pusa -> 
        /// KCS: JOGORVOSLATI_DONTES_TIPUSA
        /// v�gz�s (1)
        /// �rdemi d�nt�s (2)
        /// </summary>
        private Field _JogorvoslatiDontesTipusa = new Field();

        public Field JogorvoslatiDontesTipusa
        {
            get { return _JogorvoslatiDontesTipusa; }
            set { _JogorvoslatiDontesTipusa = value; }
        }
                          
           
        /// <summary>
        /// JogorvoslatiDontestHozta property
        /// 7. A jogorvoslati elj�r�sban sz�letett d�nt�st meghozta -> KCS: JOGORVOSLATI_DONTEST_HOZTA
        /// az els�fok� hat�s�g (1)
        /// az els�fok� hat�s�g �jrafelv�teli elj�r�sban (2)
        /// az els�fok� hat�s�g m�lt�nyoss�gi elj�r�sban (3)
        /// a k�pvisel�test�let m�sodfok� hat�sk�rben (4)
        /// a k�zigazgat�si hivatal (5)
        /// a dekoncentr�lt szerv (6)
        /// a b�r�s�g (7)
        /// a fel�gyeleti szerv/fel�gyeleti int�zked�s eset�n (8)
        /// </summary>
        private Field _JogorvoslatiDontestHozta = new Field();

        public Field JogorvoslatiDontestHozta
        {
            get { return _JogorvoslatiDontestHozta; }
            set { _JogorvoslatiDontestHozta = value; }
        }
                          
           
        /// <summary>
        /// JogorvoslatiDontesTartalma property
        /// 8. A jogorvoslati elj�r�sban sz�letett d�nt�st tartalma -> KCS: JOGORVOSLATI_DONTES_TARTALMA
        /// kicser�l�s, kieg�sz�t�s vagy kijav�t�s (1)
        /// m�dos�t�s (2)
        /// visszavon�s (3)
        /// �j d�nt�s (4)
        /// helybenhagy�s/kereset ill. k�relem, �jrafelv�teli vagy m�lt�nyoss�gi k�relem elutas�t�sa (5)
        /// megv�ltoztat�s (6)
        /// megsemmis�t�s vagy hat�lyon k�v�l helyez�s (7)
        /// megsemmis�t�s vagy hat�lyon k�v�l helyez�s �s �j elj�r�sra utas�t�s (8)
        /// </summary>
        private Field _JogorvoslatiDontesTartalma = new Field();

        public Field JogorvoslatiDontesTartalma
        {
            get { return _JogorvoslatiDontesTartalma; }
            set { _JogorvoslatiDontesTartalma = value; }
        }
                          
           
        /// <summary>
        /// JogorvoslatiDontes property
        /// A d�nt�s: 
        /// KCS: JOGORVOSLATI_DONTES
        /// 1, Kijav�t�sra ker�lt sor
        /// 2, Kieg�sz�t�sre ker�lt sor
        /// </summary>
        private Field _JogorvoslatiDontes = new Field();

        public Field JogorvoslatiDontes
        {
            get { return _JogorvoslatiDontes; }
            set { _JogorvoslatiDontes = value; }
        }
                          
           
        /// <summary>
        /// HatosagiEllenorzes property
        /// �rt�k: 0/1
        /// 
        /// K�rj�k felt�ntetni az adott elj�r�st�pus sor�n a t�rgyid�szakban lefolytatott hat�s�gi ellen�rz�sek sz�m�t.
        /// </summary>
        private Field _HatosagiEllenorzes = new Field();

        public Field HatosagiEllenorzes
        {
            get { return _HatosagiEllenorzes; }
            set { _HatosagiEllenorzes = value; }
        }
                          
           
        /// <summary>
        /// MunkaorakSzama property
        /// �rt�k: 3 eg�sz + 1 tizedes
        /// 
        /// Az adott �gazatban az egy �gy elint�z�s�re ford�tott munka�r�k �tlagos sz�m�t kell felt�ntetni (�sszes munka�ra / elj�r�sok sz�ma).
        /// </summary>
        private Field _MunkaorakSzama = new Field();

        public Field MunkaorakSzama
        {
            get { return _MunkaorakSzama; }
            set { _MunkaorakSzama = value; }
        }
                          
           
        /// <summary>
        /// EljarasiKoltseg property
        /// �rt�k: 6 jegy� eg�sz sz�m.
        /// 
        /// Az adott �gazatban az egy �gyre jut� �tlagos elj�r�si k�lts�get k�rj�k felt�ntetni (�sszes elj�r�si k�lts�g / elj�r�sok sz�ma).
        /// Elj�r�si k�lts�gen ebben a tekintetben a hat�s�g �ltal meg�llap�tott, a Ket. vagy m�s vonatkoz� jogszab�ly szerinti elj�r�si k�lts�get kell �rteni. A hat�s�g m�k�d�si k�lts�gei figyelmen k�v�l hagyand�ak.
        /// </summary>
        private Field _EljarasiKoltseg = new Field();

        public Field EljarasiKoltseg
        {
            get { return _EljarasiKoltseg; }
            set { _EljarasiKoltseg = value; }
        }
                          
           
        /// <summary>
        /// KozigazgatasiBirsagMerteke property
        /// 
        /// </summary>
        private Field _KozigazgatasiBirsagMerteke = new Field();

        public Field KozigazgatasiBirsagMerteke
        {
            get { return _KozigazgatasiBirsagMerteke; }
            set { _KozigazgatasiBirsagMerteke = value; }
        }
                          
           
        /// <summary>
        /// SommasEljDontes property
        /// 
        /// </summary>
        private Field _SommasEljDontes = new Field();

        public Field SommasEljDontes
        {
            get { return _SommasEljDontes; }
            set { _SommasEljDontes = value; }
        }
                          
           
        /// <summary>
        /// NyolcNapBelulNemSommas property
        /// 
        /// </summary>
        private Field _NyolcNapBelulNemSommas = new Field();

        public Field NyolcNapBelulNemSommas
        {
            get { return _NyolcNapBelulNemSommas; }
            set { _NyolcNapBelulNemSommas = value; }
        }
                          
           
        /// <summary>
        /// FuggoHatalyuHatarozat property
        /// 
        /// </summary>
        private Field _FuggoHatalyuHatarozat = new Field();

        public Field FuggoHatalyuHatarozat
        {
            get { return _FuggoHatalyuHatarozat; }
            set { _FuggoHatalyuHatarozat = value; }
        }
                          
           
        /// <summary>
        /// FuggoHatHatalybaLepes property
        /// 
        /// </summary>
        private Field _FuggoHatHatalybaLepes = new Field();

        public Field FuggoHatHatalybaLepes
        {
            get { return _FuggoHatHatalybaLepes; }
            set { _FuggoHatHatalybaLepes = value; }
        }
                          
           
        /// <summary>
        /// FuggoHatalyuVegzes property
        /// 
        /// </summary>
        private Field _FuggoHatalyuVegzes = new Field();

        public Field FuggoHatalyuVegzes
        {
            get { return _FuggoHatalyuVegzes; }
            set { _FuggoHatalyuVegzes = value; }
        }
                          
           
        /// <summary>
        /// FuggoVegzesHatalyba property
        /// 
        /// </summary>
        private Field _FuggoVegzesHatalyba = new Field();

        public Field FuggoVegzesHatalyba
        {
            get { return _FuggoVegzesHatalyba; }
            set { _FuggoVegzesHatalyba = value; }
        }
                          
           
        /// <summary>
        /// HatAltalVisszafizOsszeg property
        /// 
        /// </summary>
        private Field _HatAltalVisszafizOsszeg = new Field();

        public Field HatAltalVisszafizOsszeg
        {
            get { return _HatAltalVisszafizOsszeg; }
            set { _HatAltalVisszafizOsszeg = value; }
        }
                          
           
        /// <summary>
        /// HatTerheloEljKtsg property
        /// 
        /// </summary>
        private Field _HatTerheloEljKtsg = new Field();

        public Field HatTerheloEljKtsg
        {
            get { return _HatTerheloEljKtsg; }
            set { _HatTerheloEljKtsg = value; }
        }
                          
           
        /// <summary>
        /// FelfuggHatarozat property
        /// 
        /// </summary>
        private Field _FelfuggHatarozat = new Field();

        public Field FelfuggHatarozat
        {
            get { return _FelfuggHatarozat; }
            set { _FelfuggHatarozat = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}