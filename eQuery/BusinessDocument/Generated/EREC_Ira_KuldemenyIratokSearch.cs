
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Ira_KuldemenyIratok eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_Ira_KuldemenyIratokSearch
    {
      public EREC_Ira_KuldemenyIratokSearch()
      {               _Id.Name = "EREC_Ira_KuldemenyIratok.Id";
               _Id.Type = "Guid";            
               _Kuldemeny_Id.Name = "EREC_Ira_KuldemenyIratok.Kuldemeny_Id";
               _Kuldemeny_Id.Type = "Guid";            
               _Irat_Id.Name = "EREC_Ira_KuldemenyIratok.Irat_Id";
               _Irat_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_Ira_KuldemenyIratok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Ira_KuldemenyIratok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kuldemeny_Id = new Field();

        public Field Kuldemeny_Id
        {
            get { return _Kuldemeny_Id; }
            set { _Kuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Irat_Id = new Field();

        public Field Irat_Id
        {
            get { return _Irat_Id; }
            set { _Irat_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}