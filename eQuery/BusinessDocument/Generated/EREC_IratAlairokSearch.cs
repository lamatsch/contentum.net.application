
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IratAlairok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IratAlairokSearch: BaseSearchObject    {
      public EREC_IratAlairokSearch()
      {            
                     _Id.Name = "EREC_IratAlairok.Id";
               _Id.Type = "Guid";            
               _PldIratPeldany_Id.Name = "EREC_IratAlairok.PldIratPeldany_Id";
               _PldIratPeldany_Id.Type = "Guid";            
               _Objtip_Id.Name = "EREC_IratAlairok.Objtip_Id";
               _Objtip_Id.Type = "Guid";            
               _Obj_Id.Name = "EREC_IratAlairok.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _AlairasDatuma.Name = "EREC_IratAlairok.AlairasDatuma";
               _AlairasDatuma.Type = "DateTime";            
               _Leiras.Name = "EREC_IratAlairok.Leiras";
               _Leiras.Type = "String";            
               _AlairoSzerep.Name = "EREC_IratAlairok.AlairoSzerep";
               _AlairoSzerep.Type = "String";            
               _AlairasSorrend.Name = "EREC_IratAlairok.AlairasSorrend";
               _AlairasSorrend.Type = "Int32";            
               _FelhasznaloCsoport_Id_Alairo.Name = "EREC_IratAlairok.FelhasznaloCsoport_Id_Alairo";
               _FelhasznaloCsoport_Id_Alairo.Type = "Guid";            
               _FelhaszCsoport_Id_Helyettesito.Name = "EREC_IratAlairok.FelhaszCsoport_Id_Helyettesito";
               _FelhaszCsoport_Id_Helyettesito.Type = "Guid";            
               _Azonosito.Name = "EREC_IratAlairok.Azonosito";
               _Azonosito.Type = "String";            
               _AlairasMod.Name = "EREC_IratAlairok.AlairasMod";
               _AlairasMod.Type = "String";            
               _AlairasSzabaly_Id.Name = "EREC_IratAlairok.AlairasSzabaly_Id";
               _AlairasSzabaly_Id.Type = "Guid";            
               _Allapot.Name = "EREC_IratAlairok.Allapot";
               _Allapot.Type = "String";            
               _FelhaszCsop_Id_HelyettesAlairo.Name = "EREC_IratAlairok.FelhaszCsop_Id_HelyettesAlairo";
               _FelhaszCsop_Id_HelyettesAlairo.Type = "Guid";            
               _ErvKezd.Name = "EREC_IratAlairok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IratAlairok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// PldIratPeldany_Id property
        /// Al��r�i p�ld�ny azonos�t�ja (ha van).
        /// </summary>
        private Field _PldIratPeldany_Id = new Field();

        public Field PldIratPeldany_Id
        {
            get { return _PldIratPeldany_Id; }
            set { _PldIratPeldany_Id = value; }
        }
                          
           
        /// <summary>
        /// Objtip_Id property
        /// 
        /// </summary>
        private Field _Objtip_Id = new Field();

        public Field Objtip_Id
        {
            get { return _Objtip_Id; }
            set { _Objtip_Id = value; }
        }
                          
           
        /// <summary>
        /// Obj_Id property
        /// 
        /// </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// AlairasDatuma property
        /// Al��r�s d�tuma
        /// </summary>
        private Field _AlairasDatuma = new Field();

        public Field AlairasDatuma
        {
            get { return _AlairasDatuma; }
            set { _AlairasDatuma = value; }
        }
                          
           
        /// <summary>
        /// Leiras property
        /// /*KIR2.5*/ v0.2 �j attrib�tum. Le�r�s, egy�b jellemz�.
        /// </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// AlairoSzerep property
        /// KCS: ALAIRO_SZEREP 
        /// 1- Kiadm�nyoz�s
        /// 2 - J�v�hagy�s
        /// 3 - L�ttamoz�s
        /// 
        /// </summary>
        private Field _AlairoSzerep = new Field();

        public Field AlairoSzerep
        {
            get { return _AlairoSzerep; }
            set { _AlairoSzerep = value; }
        }
                          
           
        /// <summary>
        /// AlairasSorrend property
        /// Al��r�si sorrend szerinti csoportos�t�s (1, 2, ...n). Az 'n' az utols�.
        /// </summary>
        private Field _AlairasSorrend = new Field();

        public Field AlairasSorrend
        {
            get { return _AlairasSorrend; }
            set { _AlairasSorrend = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Alairo property
        /// Az al��r� azonos�t�ja.
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Alairo = new Field();

        public Field FelhasznaloCsoport_Id_Alairo
        {
            get { return _FelhasznaloCsoport_Id_Alairo; }
            set { _FelhasznaloCsoport_Id_Alairo = value; }
        }
                          
           
        /// <summary>
        /// FelhaszCsoport_Id_Helyettesito property
        /// A helyettes al��r� azonos�t�ja.
        /// </summary>
        private Field _FelhaszCsoport_Id_Helyettesito = new Field();

        public Field FelhaszCsoport_Id_Helyettesito
        {
            get { return _FelhaszCsoport_Id_Helyettesito; }
            set { _FelhaszCsoport_Id_Helyettesito = value; }
        }
                          
           
        /// <summary>
        /// Azonosito property
        /// Objektum azonos�t� sz�vegesen
        /// </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// AlairasMod property
        /// Az �l��r�s p�tl�lagos jelleg�t jelz� adat.
        /// 0 - Norm�l
        /// 1 - P�tl�s
        /// </summary>
        private Field _AlairasMod = new Field();

        public Field AlairasMod
        {
            get { return _AlairasMod; }
            set { _AlairasMod = value; }
        }
                          
           
        /// <summary>
        /// AlairasSzabaly_Id property
        /// 
        /// </summary>
        private Field _AlairasSzabaly_Id = new Field();

        public Field AlairasSzabaly_Id
        {
            get { return _AlairasSzabaly_Id; }
            set { _AlairasSzabaly_Id = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// Az adott felhaszn�l�i al��r�s �llapota. K�dcsoport: IRATALAIRAS_ALLAPOT
        /// 1 - Al��rand�
        /// 2 - Al��rt (minden al��rand� dokumentum al��r�sa megt�rt�nt)
        /// 3 - Visszautas�tott (az irat al��r�sa elutas�tva )
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// FelhaszCsop_Id_HelyettesAlairo property
        /// Helyettes al��r�.
        /// </summary>
        private Field _FelhaszCsop_Id_HelyettesAlairo = new Field();

        public Field FelhaszCsop_Id_HelyettesAlairo
        {
            get { return _FelhaszCsop_Id_HelyettesAlairo; }
            set { _FelhaszCsop_Id_HelyettesAlairo = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}