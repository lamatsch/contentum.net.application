
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IratKapcsolatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IratKapcsolatokSearch: BaseSearchObject    {
      public EREC_IratKapcsolatokSearch()
      {            
                     _Id.Name = "EREC_IratKapcsolatok.Id";
               _Id.Type = "Guid";            
               _KapcsolatTipus.Name = "EREC_IratKapcsolatok.KapcsolatTipus";
               _KapcsolatTipus.Type = "String";            
               _Leiras.Name = "EREC_IratKapcsolatok.Leiras";
               _Leiras.Type = "String";            
               _Kezi.Name = "EREC_IratKapcsolatok.Kezi";
               _Kezi.Type = "Char";            
               _Irat_Irat_Beepul.Name = "EREC_IratKapcsolatok.Irat_Irat_Beepul";
               _Irat_Irat_Beepul.Type = "Guid";            
               _Irat_Irat_Felepul.Name = "EREC_IratKapcsolatok.Irat_Irat_Felepul";
               _Irat_Irat_Felepul.Type = "Guid";            
               _ErvKezd.Name = "EREC_IratKapcsolatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IratKapcsolatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KapcsolatTipus = new Field();

        public Field KapcsolatTipus
        {
            get { return _KapcsolatTipus; }
            set { _KapcsolatTipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kezi = new Field();

        public Field Kezi
        {
            get { return _Kezi; }
            set { _Kezi = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Irat_Irat_Beepul = new Field();

        public Field Irat_Irat_Beepul
        {
            get { return _Irat_Irat_Beepul; }
            set { _Irat_Irat_Beepul = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Irat_Irat_Felepul = new Field();

        public Field Irat_Irat_Felepul
        {
            get { return _Irat_Irat_Felepul; }
            set { _Irat_Irat_Felepul = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}