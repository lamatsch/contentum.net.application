
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IratMellekletek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IratMellekletekSearch: BaseSearchObject    {
      public EREC_IratMellekletekSearch()
      {            
                     _Id.Name = "EREC_IratMellekletek.Id";
               _Id.Type = "Guid";            
               _IraIrat_Id.Name = "EREC_IratMellekletek.IraIrat_Id";
               _IraIrat_Id.Type = "Guid";            
               _KuldMellekletek_Id.Name = "EREC_IratMellekletek.KuldMellekletek_Id";
               _KuldMellekletek_Id.Type = "Guid";            
               _AdathordozoTipus.Name = "EREC_IratMellekletek.AdathordozoTipus";
               _AdathordozoTipus.Type = "String";            
               _Megjegyzes.Name = "EREC_IratMellekletek.Megjegyzes";
               _Megjegyzes.Type = "String";            
               _SztornirozasDat.Name = "EREC_IratMellekletek.SztornirozasDat";
               _SztornirozasDat.Type = "DateTime";            
               _MennyisegiEgyseg.Name = "EREC_IratMellekletek.MennyisegiEgyseg";
               _MennyisegiEgyseg.Type = "String";            
               _Mennyiseg.Name = "EREC_IratMellekletek.Mennyiseg";
               _Mennyiseg.Type = "Int32";            
               _BarCode.Name = "EREC_IratMellekletek.BarCode";
               _BarCode.Type = "String";            
               _ErvKezd.Name = "EREC_IratMellekletek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IratMellekletek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IraIrat_Id = new Field();

        public Field IraIrat_Id
        {
            get { return _IraIrat_Id; }
            set { _IraIrat_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KuldMellekletek_Id = new Field();

        public Field KuldMellekletek_Id
        {
            get { return _KuldMellekletek_Id; }
            set { _KuldMellekletek_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AdathordozoTipus = new Field();

        public Field AdathordozoTipus
        {
            get { return _AdathordozoTipus; }
            set { _AdathordozoTipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MennyisegiEgyseg = new Field();

        public Field MennyisegiEgyseg
        {
            get { return _MennyisegiEgyseg; }
            set { _MennyisegiEgyseg = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Mennyiseg = new Field();

        public Field Mennyiseg
        {
            get { return _Mennyiseg; }
            set { _Mennyiseg = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}