
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IratMetaDefinicio eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_IratMetaDefinicioSearch: BaseSearchObject    {
      public EREC_IratMetaDefinicioSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "EREC_IratMetaDefinicio.Id";
               _Id.Type = "Guid";            
               _Org.Name = "EREC_IratMetaDefinicio.Org";
               _Org.Type = "Guid";            
               _Ugykor_Id.Name = "EREC_IratMetaDefinicio.Ugykor_Id";
               _Ugykor_Id.Type = "Guid";            
               _UgykorKod.Name = "EREC_IratMetaDefinicio.UgykorKod";
               _UgykorKod.Type = "String";            
               _Ugytipus.Name = "EREC_IratMetaDefinicio.Ugytipus";
               _Ugytipus.Type = "String";            
               _UgytipusNev.Name = "EREC_IratMetaDefinicio.UgytipusNev";
               _UgytipusNev.Type = "String";            
               _EljarasiSzakasz.Name = "EREC_IratMetaDefinicio.EljarasiSzakasz";
               _EljarasiSzakasz.Type = "String";            
               _Irattipus.Name = "EREC_IratMetaDefinicio.Irattipus";
               _Irattipus.Type = "String";            
               _GeneraltTargy.Name = "EREC_IratMetaDefinicio.GeneraltTargy";
               _GeneraltTargy.Type = "String";            
               _Rovidnev.Name = "EREC_IratMetaDefinicio.Rovidnev";
               _Rovidnev.Type = "String";            
               _UgyFajta.Name = "EREC_IratMetaDefinicio.UgyFajta";
               _UgyFajta.Type = "String";            
               _UgyiratIntezesiIdo.Name = "EREC_IratMetaDefinicio.UgyiratIntezesiIdo";
               _UgyiratIntezesiIdo.Type = "Int32";            
               _Idoegyseg.Name = "EREC_IratMetaDefinicio.Idoegyseg";
               _Idoegyseg.Type = "String";            
               _UgyiratIntezesiIdoKotott.Name = "EREC_IratMetaDefinicio.UgyiratIntezesiIdoKotott";
               _UgyiratIntezesiIdoKotott.Type = "Char";            
               _UgyiratHataridoKitolas.Name = "EREC_IratMetaDefinicio.UgyiratHataridoKitolas";
               _UgyiratHataridoKitolas.Type = "Char";            
               _ErvKezd.Name = "EREC_IratMetaDefinicio.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IratMetaDefinicio.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Ugykor_Id = new Field();

        public Field Ugykor_Id
        {
            get { return _Ugykor_Id; }
            set { _Ugykor_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgykorKod = new Field();

        public Field UgykorKod
        {
            get { return _UgykorKod; }
            set { _UgykorKod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Ugytipus = new Field();

        public Field Ugytipus
        {
            get { return _Ugytipus; }
            set { _Ugytipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgytipusNev = new Field();

        public Field UgytipusNev
        {
            get { return _UgytipusNev; }
            set { _UgytipusNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _EljarasiSzakasz = new Field();

        public Field EljarasiSzakasz
        {
            get { return _EljarasiSzakasz; }
            set { _EljarasiSzakasz = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Irattipus = new Field();

        public Field Irattipus
        {
            get { return _Irattipus; }
            set { _Irattipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _GeneraltTargy = new Field();

        public Field GeneraltTargy
        {
            get { return _GeneraltTargy; }
            set { _GeneraltTargy = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Rovidnev = new Field();

        public Field Rovidnev
        {
            get { return _Rovidnev; }
            set { _Rovidnev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgyFajta = new Field();

        public Field UgyFajta
        {
            get { return _UgyFajta; }
            set { _UgyFajta = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgyiratIntezesiIdo = new Field();

        public Field UgyiratIntezesiIdo
        {
            get { return _UgyiratIntezesiIdo; }
            set { _UgyiratIntezesiIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Idoegyseg = new Field();

        public Field Idoegyseg
        {
            get { return _Idoegyseg; }
            set { _Idoegyseg = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgyiratIntezesiIdoKotott = new Field();

        public Field UgyiratIntezesiIdoKotott
        {
            get { return _UgyiratIntezesiIdoKotott; }
            set { _UgyiratIntezesiIdoKotott = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgyiratHataridoKitolas = new Field();

        public Field UgyiratHataridoKitolas
        {
            get { return _UgyiratHataridoKitolas; }
            set { _UgyiratHataridoKitolas = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}