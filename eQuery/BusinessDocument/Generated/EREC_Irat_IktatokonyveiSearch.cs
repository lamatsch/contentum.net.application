
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Irat_Iktatokonyvei eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_Irat_IktatokonyveiSearch: BaseSearchObject    {
      public EREC_Irat_IktatokonyveiSearch()
      {            
                     _Id.Name = "EREC_Irat_Iktatokonyvei.Id";
               _Id.Type = "Guid";            
               _IraIktatokonyv_Id.Name = "EREC_Irat_Iktatokonyvei.IraIktatokonyv_Id";
               _IraIktatokonyv_Id.Type = "Guid";            
               _Csoport_Id_Iktathat.Name = "EREC_Irat_Iktatokonyvei.Csoport_Id_Iktathat";
               _Csoport_Id_Iktathat.Type = "Guid";            
               _ErvKezd.Name = "EREC_Irat_Iktatokonyvei.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Irat_Iktatokonyvei.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IraIktatokonyv_Id = new Field();

        public Field IraIktatokonyv_Id
        {
            get { return _IraIktatokonyv_Id; }
            set { _IraIktatokonyv_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Iktathat = new Field();

        public Field Csoport_Id_Iktathat
        {
            get { return _Csoport_Id_Iktathat; }
            set { _Csoport_Id_Iktathat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}