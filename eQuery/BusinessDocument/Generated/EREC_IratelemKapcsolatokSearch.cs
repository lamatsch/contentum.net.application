
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IratelemKapcsolatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IratelemKapcsolatokSearch: BaseSearchObject    {
      public EREC_IratelemKapcsolatokSearch()
      {            
                     _Id.Name = "EREC_IratelemKapcsolatok.Id";
               _Id.Type = "Guid";            
               _Melleklet_Id.Name = "EREC_IratelemKapcsolatok.Melleklet_Id";
               _Melleklet_Id.Type = "Guid";            
               _Csatolmany_Id.Name = "EREC_IratelemKapcsolatok.Csatolmany_Id";
               _Csatolmany_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_IratelemKapcsolatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IratelemKapcsolatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Melleklet_Id = new Field();

        public Field Melleklet_Id
        {
            get { return _Melleklet_Id; }
            set { _Melleklet_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csatolmany_Id = new Field();

        public Field Csatolmany_Id
        {
            get { return _Csatolmany_Id; }
            set { _Csatolmany_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}