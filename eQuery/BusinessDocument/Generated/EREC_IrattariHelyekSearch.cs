
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IrattariHelyek eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_IrattariHelyekSearch : BaseSearchObject
    {
        public EREC_IrattariHelyekSearch()
        {
            _Id.Name = "EREC_IrattariHelyek.Id";
            _Id.Type = "Guid";
            _Ertek.Name = "EREC_IrattariHelyek.Ertek";
            _Ertek.Type = "String";
            _Nev.Name = "EREC_IrattariHelyek.Nev";
            _Nev.Type = "String";
            _Vonalkod.Name = "EREC_IrattariHelyek.Vonalkod";
            _Vonalkod.Type = "String";
            // CR3246 Irattári Helyek kezelésének módosítása
            //_IrattarTipus.Name = "EREC_IrattariHelyek.IrattarTipus";
            //_IrattarTipus.Type = "String";
            // CR3246 Irattári Helyek kezelésének módosítása
            _Felelos_Csoport_Id.Name = "EREC_IrattariHelyek.Felelos_Csoport_Id";
            _Felelos_Csoport_Id.Type = "Guid";

            _SzuloId.Name = "EREC_IrattariHelyek.SzuloId";
            _SzuloId.Type = "Guid";
            _Kapacitas.Name = "EREC_IrattariHelyek.Kapacitas";
            _Kapacitas.Type = "Double";
            _ErvKezd.Name = "EREC_IrattariHelyek.ErvKezd";
            _ErvKezd.Type = "DateTime"; _ErvKezd.Operator = Query.Operators.lessorequal;
            _ErvKezd.Value = "getdate()";
            _ErvVege.Name = "EREC_IrattariHelyek.ErvVege";
            _ErvVege.Type = "DateTime"; _ErvVege.Operator = Query.Operators.greaterorequal;

            _ErvVege.Value = "getdate()";
        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// Ertek property
        /// 
        /// </summary>
        private Field _Ertek = new Field();

        public Field Ertek
        {
            get { return _Ertek; }
            set { _Ertek = value; }
        }

        // CR3246 Irattári Helyek kezelésének módosítása
        ///// <summary>
        ///// IrattariTipus property
        ///// 
        ///// </summary>
        //private Field _IrattarTipus = new Field();

        //public Field IrattarTipus
        //{
        //    get { return _IrattarTipus; }
        //    set { _IrattarTipus = value; }
        //}

        // CR3246 Irattári Helyek kezelésének módosítása
        /// <summary>
        /// Felelos_Csoport_Id property
        /// </summary>
        private Field _Felelos_Csoport_Id = new Field();

        public Field Felelos_Csoport_Id
        {
            get { return _Felelos_Csoport_Id; }
            set { _Felelos_Csoport_Id = value; }
        }

        /// <summary>
        /// Nev property
        /// 
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }


        /// <summary>
        /// Vonalkod property
        /// 
        /// </summary>
        private Field _Vonalkod = new Field();

        public Field Vonalkod
        {
            get { return _Vonalkod; }
            set { _Vonalkod = value; }
        }


        /// <summary>
        /// SzuloId property
        /// 
        /// </summary>
        private Field _SzuloId = new Field();

        public Field SzuloId
        {
            get { return _SzuloId; }
            set { _SzuloId = value; }
        }

        /// <summary>
        /// Kapacitas property
        /// 
        /// </summary>
        private Field _Kapacitas = new Field();

        public Field Kapacitas
        {
            get { return _Kapacitas; }
            set { _Kapacitas = value; }
        }

        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }


        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
    }

}