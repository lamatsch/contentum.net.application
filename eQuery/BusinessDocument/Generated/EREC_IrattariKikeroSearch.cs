
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IrattariKikero eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IrattariKikeroSearch: BaseSearchObject    {
      public EREC_IrattariKikeroSearch()
      {            
                     _Id.Name = "EREC_IrattariKikero.Id";
               _Id.Type = "Guid";            
               _Keres_note.Name = "EREC_IrattariKikero.Keres_note";
               _Keres_note.Type = "String";            
               _FelhasznalasiCel.Name = "EREC_IrattariKikero.FelhasznalasiCel";
               _FelhasznalasiCel.Type = "Char";            
               _DokumentumTipus.Name = "EREC_IrattariKikero.DokumentumTipus";
               _DokumentumTipus.Type = "Char";            
               _Indoklas_note.Name = "EREC_IrattariKikero.Indoklas_note";
               _Indoklas_note.Type = "String";            
               _FelhasznaloCsoport_Id_Kikero.Name = "EREC_IrattariKikero.FelhasznaloCsoport_Id_Kikero";
               _FelhasznaloCsoport_Id_Kikero.Type = "Guid";            
               _KeresDatuma.Name = "EREC_IrattariKikero.KeresDatuma";
               _KeresDatuma.Type = "DateTime";            
               _KulsoAzonositok.Name = "EREC_IrattariKikero.KulsoAzonositok";
               _KulsoAzonositok.Type = "String";            
               _UgyUgyirat_Id.Name = "EREC_IrattariKikero.UgyUgyirat_Id";
               _UgyUgyirat_Id.Type = "Guid";            
               _Tertiveveny_Id.Name = "EREC_IrattariKikero.Tertiveveny_Id";
               _Tertiveveny_Id.Type = "Guid";            
               _Obj_Id.Name = "EREC_IrattariKikero.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id.Name = "EREC_IrattariKikero.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _KikerKezd.Name = "EREC_IrattariKikero.KikerKezd";
               _KikerKezd.Type = "DateTime";            
               _KikerVege.Name = "EREC_IrattariKikero.KikerVege";
               _KikerVege.Type = "DateTime";            
               _FelhasznaloCsoport_Id_Jovahagy.Name = "EREC_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy";
               _FelhasznaloCsoport_Id_Jovahagy.Type = "Guid";            
               _JovagyasDatuma.Name = "EREC_IrattariKikero.JovagyasDatuma";
               _JovagyasDatuma.Type = "DateTime";            
               _FelhasznaloCsoport_Id_Kiado.Name = "EREC_IrattariKikero.FelhasznaloCsoport_Id_Kiado";
               _FelhasznaloCsoport_Id_Kiado.Type = "Guid";            
               _KiadasDatuma.Name = "EREC_IrattariKikero.KiadasDatuma";
               _KiadasDatuma.Type = "DateTime";            
               _FelhasznaloCsoport_Id_Visszaad.Name = "EREC_IrattariKikero.FelhasznaloCsoport_Id_Visszaad";
               _FelhasznaloCsoport_Id_Visszaad.Type = "Guid";            
               _FelhasznaloCsoport_Id_Visszave.Name = "EREC_IrattariKikero.FelhasznaloCsoport_Id_Visszave";
               _FelhasznaloCsoport_Id_Visszave.Type = "Guid";            
               _VisszaadasDatuma.Name = "EREC_IrattariKikero.VisszaadasDatuma";
               _VisszaadasDatuma.Type = "DateTime";            
               _SztornirozasDat.Name = "EREC_IrattariKikero.SztornirozasDat";
               _SztornirozasDat.Type = "DateTime";            
               _Allapot.Name = "EREC_IrattariKikero.Allapot";
               _Allapot.Type = "String";            
               _BarCode.Name = "EREC_IrattariKikero.BarCode";
               _BarCode.Type = "String";            
               _Irattar_Id.Name = "EREC_IrattariKikero.Irattar_Id";
               _Irattar_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_IrattariKikero.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IrattariKikero.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Keres_note = new Field();

        public Field Keres_note
        {
            get { return _Keres_note; }
            set { _Keres_note = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FelhasznalasiCel = new Field();

        public Field FelhasznalasiCel
        {
            get { return _FelhasznalasiCel; }
            set { _FelhasznalasiCel = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _DokumentumTipus = new Field();

        public Field DokumentumTipus
        {
            get { return _DokumentumTipus; }
            set { _DokumentumTipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Indoklas_note = new Field();

        public Field Indoklas_note
        {
            get { return _Indoklas_note; }
            set { _Indoklas_note = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FelhasznaloCsoport_Id_Kikero = new Field();

        public Field FelhasznaloCsoport_Id_Kikero
        {
            get { return _FelhasznaloCsoport_Id_Kikero; }
            set { _FelhasznaloCsoport_Id_Kikero = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KeresDatuma = new Field();

        public Field KeresDatuma
        {
            get { return _KeresDatuma; }
            set { _KeresDatuma = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KulsoAzonositok = new Field();

        public Field KulsoAzonositok
        {
            get { return _KulsoAzonositok; }
            set { _KulsoAzonositok = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgyUgyirat_Id = new Field();

        public Field UgyUgyirat_Id
        {
            get { return _UgyUgyirat_Id; }
            set { _UgyUgyirat_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tertiveveny_Id = new Field();

        public Field Tertiveveny_Id
        {
            get { return _Tertiveveny_Id; }
            set { _Tertiveveny_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KikerKezd = new Field();

        public Field KikerKezd
        {
            get { return _KikerKezd; }
            set { _KikerKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KikerVege = new Field();

        public Field KikerVege
        {
            get { return _KikerVege; }
            set { _KikerVege = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FelhasznaloCsoport_Id_Jovahagy = new Field();

        public Field FelhasznaloCsoport_Id_Jovahagy
        {
            get { return _FelhasznaloCsoport_Id_Jovahagy; }
            set { _FelhasznaloCsoport_Id_Jovahagy = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _JovagyasDatuma = new Field();

        public Field JovagyasDatuma
        {
            get { return _JovagyasDatuma; }
            set { _JovagyasDatuma = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FelhasznaloCsoport_Id_Kiado = new Field();

        public Field FelhasznaloCsoport_Id_Kiado
        {
            get { return _FelhasznaloCsoport_Id_Kiado; }
            set { _FelhasznaloCsoport_Id_Kiado = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KiadasDatuma = new Field();

        public Field KiadasDatuma
        {
            get { return _KiadasDatuma; }
            set { _KiadasDatuma = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FelhasznaloCsoport_Id_Visszaad = new Field();

        public Field FelhasznaloCsoport_Id_Visszaad
        {
            get { return _FelhasznaloCsoport_Id_Visszaad; }
            set { _FelhasznaloCsoport_Id_Visszaad = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FelhasznaloCsoport_Id_Visszave = new Field();

        public Field FelhasznaloCsoport_Id_Visszave
        {
            get { return _FelhasznaloCsoport_Id_Visszave; }
            set { _FelhasznaloCsoport_Id_Visszave = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _VisszaadasDatuma = new Field();

        public Field VisszaadasDatuma
        {
            get { return _VisszaadasDatuma; }
            set { _VisszaadasDatuma = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Irattar_Id = new Field();

        public Field Irattar_Id
        {
            get { return _Irattar_Id; }
            set { _Irattar_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}