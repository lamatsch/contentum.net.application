
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_IrattariTetel_Iktatokonyv eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_IrattariTetel_IktatokonyvSearch: BaseSearchObject    {
      public EREC_IrattariTetel_IktatokonyvSearch()
      {            
                     _Id.Name = "EREC_IrattariTetel_Iktatokonyv.Id";
               _Id.Type = "Guid";            
               _IrattariTetel_Id.Name = "EREC_IrattariTetel_Iktatokonyv.IrattariTetel_Id";
               _IrattariTetel_Id.Type = "Guid";            
               _Iktatokonyv_Id.Name = "EREC_IrattariTetel_Iktatokonyv.Iktatokonyv_Id";
               _Iktatokonyv_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_IrattariTetel_Iktatokonyv.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_IrattariTetel_Iktatokonyv.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IrattariTetel_Id = new Field();

        public Field IrattariTetel_Id
        {
            get { return _IrattariTetel_Id; }
            set { _IrattariTetel_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Iktatokonyv_Id = new Field();

        public Field Iktatokonyv_Id
        {
            get { return _Iktatokonyv_Id; }
            set { _Iktatokonyv_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}