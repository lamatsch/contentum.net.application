
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_KuldBekuldok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_KuldBekuldokSearch: BaseSearchObject    {
      public EREC_KuldBekuldokSearch()
      {            
                     _Id.Name = "EREC_KuldBekuldok.Id";
               _Id.Type = "Guid";            
               _KuldKuldemeny_Id.Name = "EREC_KuldBekuldok.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _Partner_Id_Bekuldo.Name = "EREC_KuldBekuldok.Partner_Id_Bekuldo";
               _Partner_Id_Bekuldo.Type = "Guid";            
               _PartnerCim_Id_Bekuldo.Name = "EREC_KuldBekuldok.PartnerCim_Id_Bekuldo";
               _PartnerCim_Id_Bekuldo.Type = "Guid";            
               _NevSTR.Name = "EREC_KuldBekuldok.NevSTR";
               _NevSTR.Type = "String";            
               _CimSTR.Name = "EREC_KuldBekuldok.CimSTR";
               _CimSTR.Type = "String";            
               _ErvKezd.Name = "EREC_KuldBekuldok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_KuldBekuldok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Partner_Id_Bekuldo = new Field();

        public Field Partner_Id_Bekuldo
        {
            get { return _Partner_Id_Bekuldo; }
            set { _Partner_Id_Bekuldo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _PartnerCim_Id_Bekuldo = new Field();

        public Field PartnerCim_Id_Bekuldo
        {
            get { return _PartnerCim_Id_Bekuldo; }
            set { _PartnerCim_Id_Bekuldo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _NevSTR = new Field();

        public Field NevSTR
        {
            get { return _NevSTR; }
            set { _NevSTR = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _CimSTR = new Field();

        public Field CimSTR
        {
            get { return _CimSTR; }
            set { _CimSTR = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}