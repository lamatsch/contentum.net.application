
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_KuldDokumentumok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_KuldDokumentumokSearch: BaseSearchObject    {
      public EREC_KuldDokumentumokSearch()
      {            
                     _Id.Name = "EREC_KuldDokumentumok.Id";
               _Id.Type = "Guid";            
               _Dokumentum_Id.Name = "EREC_KuldDokumentumok.Dokumentum_Id";
               _Dokumentum_Id.Type = "Guid";            
               _KuldKuldemeny_Id.Name = "EREC_KuldDokumentumok.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _Leiras.Name = "EREC_KuldDokumentumok.Leiras";
               _Leiras.Type = "String";            
               _Lapszam.Name = "EREC_KuldDokumentumok.Lapszam";
               _Lapszam.Type = "Int32";            
               _BarCode.Name = "EREC_KuldDokumentumok.BarCode";
               _BarCode.Type = "String";            
               _Forras.Name = "EREC_KuldDokumentumok.Forras";
               _Forras.Type = "String";            
               _Formatum.Name = "EREC_KuldDokumentumok.Formatum";
               _Formatum.Type = "String";            
               _Vonalkodozas.Name = "EREC_KuldDokumentumok.Vonalkodozas";
               _Vonalkodozas.Type = "Char";            
               _DokumentumSzerep.Name = "EREC_KuldDokumentumok.DokumentumSzerep";
               _DokumentumSzerep.Type = "String";            
               _ErvKezd.Name = "EREC_KuldDokumentumok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_KuldDokumentumok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Dokumentum_Id = new Field();

        public Field Dokumentum_Id
        {
            get { return _Dokumentum_Id; }
            set { _Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Lapszam = new Field();

        public Field Lapszam
        {
            get { return _Lapszam; }
            set { _Lapszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Forras = new Field();

        public Field Forras
        {
            get { return _Forras; }
            set { _Forras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Formatum = new Field();

        public Field Formatum
        {
            get { return _Formatum; }
            set { _Formatum = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Vonalkodozas = new Field();

        public Field Vonalkodozas
        {
            get { return _Vonalkodozas; }
            set { _Vonalkodozas = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _DokumentumSzerep = new Field();

        public Field DokumentumSzerep
        {
            get { return _DokumentumSzerep; }
            set { _DokumentumSzerep = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}