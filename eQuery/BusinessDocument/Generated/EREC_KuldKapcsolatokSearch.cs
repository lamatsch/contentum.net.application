
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_KuldKapcsolatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_KuldKapcsolatokSearch: BaseSearchObject    {
      public EREC_KuldKapcsolatokSearch()
      {            
                     _Id.Name = "EREC_KuldKapcsolatok.Id";
               _Id.Type = "Guid";            
               _KapcsolatTipus.Name = "EREC_KuldKapcsolatok.KapcsolatTipus";
               _KapcsolatTipus.Type = "String";            
               _Leiras.Name = "EREC_KuldKapcsolatok.Leiras";
               _Leiras.Type = "String";            
               _Kezi.Name = "EREC_KuldKapcsolatok.Kezi";
               _Kezi.Type = "Char";            
               _Kuld_Kuld_Beepul.Name = "EREC_KuldKapcsolatok.Kuld_Kuld_Beepul";
               _Kuld_Kuld_Beepul.Type = "Guid";            
               _Kuld_Kuld_Felepul.Name = "EREC_KuldKapcsolatok.Kuld_Kuld_Felepul";
               _Kuld_Kuld_Felepul.Type = "Guid";            
               _ErvKezd.Name = "EREC_KuldKapcsolatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_KuldKapcsolatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KapcsolatTipus = new Field();

        public Field KapcsolatTipus
        {
            get { return _KapcsolatTipus; }
            set { _KapcsolatTipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kezi = new Field();

        public Field Kezi
        {
            get { return _Kezi; }
            set { _Kezi = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kuld_Kuld_Beepul = new Field();

        public Field Kuld_Kuld_Beepul
        {
            get { return _Kuld_Kuld_Beepul; }
            set { _Kuld_Kuld_Beepul = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kuld_Kuld_Felepul = new Field();

        public Field Kuld_Kuld_Felepul
        {
            get { return _Kuld_Kuld_Felepul; }
            set { _Kuld_Kuld_Felepul = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}