
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_KuldKezFeljegyzesek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_KuldKezFeljegyzesekSearch: BaseSearchObject    {
      public EREC_KuldKezFeljegyzesekSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "EREC_KuldKezFeljegyzesek.Id";
               _Id.Type = "Guid";            
               _KuldKuldemeny_Id.Name = "EREC_KuldKezFeljegyzesek.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _KezelesTipus.Name = "EREC_KuldKezFeljegyzesek.KezelesTipus";
               _KezelesTipus.Type = "String";            
               _Leiras.Name = "EREC_KuldKezFeljegyzesek.Leiras";
               _Leiras.Type = "String";            
               _ErvKezd.Name = "EREC_KuldKezFeljegyzesek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_KuldKezFeljegyzesek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KezelesTipus = new Field();

        public Field KezelesTipus
        {
            get { return _KezelesTipus; }
            set { _KezelesTipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}