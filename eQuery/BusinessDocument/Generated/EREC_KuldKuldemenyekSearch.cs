
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_KuldKuldemenyek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_KuldKuldemenyekSearch: BaseSearchObject    {
      public EREC_KuldKuldemenyekSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "EREC_KuldKuldemenyek.Id";
               _Id.Type = "Guid";            
               _Allapot.Name = "EREC_KuldKuldemenyek.Allapot";
               _Allapot.Type = "String";            
               _BeerkezesIdeje.Name = "EREC_KuldKuldemenyek.BeerkezesIdeje";
               _BeerkezesIdeje.Type = "DateTime";            
               _FelbontasDatuma.Name = "EREC_KuldKuldemenyek.FelbontasDatuma";
               _FelbontasDatuma.Type = "DateTime";            
               _IraIktatokonyv_Id.Name = "EREC_KuldKuldemenyek.IraIktatokonyv_Id";
               _IraIktatokonyv_Id.Type = "Guid";            
               _KuldesMod.Name = "EREC_KuldKuldemenyek.KuldesMod";
               _KuldesMod.Type = "String";            
               _Erkezteto_Szam.Name = "EREC_KuldKuldemenyek.Erkezteto_Szam";
               _Erkezteto_Szam.Type = "Int32";            
               _HivatkozasiSzam.Name = "EREC_KuldKuldemenyek.HivatkozasiSzam";
               _HivatkozasiSzam.Type = "String";            
               _Targy.Name = "EREC_KuldKuldemenyek.Targy";
               _Targy.Type = "FTSString";            
               _Tartalom.Name = "EREC_KuldKuldemenyek.Tartalom";
               _Tartalom.Type = "String";            
               _RagSzam.Name = "EREC_KuldKuldemenyek.RagSzam";
               _RagSzam.Type = "String";            
               _Surgosseg.Name = "EREC_KuldKuldemenyek.Surgosseg";
               _Surgosseg.Type = "String";            
               _BelyegzoDatuma.Name = "EREC_KuldKuldemenyek.BelyegzoDatuma";
               _BelyegzoDatuma.Type = "DateTime";            
               _UgyintezesModja.Name = "EREC_KuldKuldemenyek.UgyintezesModja";
               _UgyintezesModja.Type = "String";            
               _PostazasIranya.Name = "EREC_KuldKuldemenyek.PostazasIranya";
               _PostazasIranya.Type = "String";            
               _Tovabbito.Name = "EREC_KuldKuldemenyek.Tovabbito";
               _Tovabbito.Type = "String";            
               _PeldanySzam.Name = "EREC_KuldKuldemenyek.PeldanySzam";
               _PeldanySzam.Type = "Int32";            
               _IktatniKell.Name = "EREC_KuldKuldemenyek.IktatniKell";
               _IktatniKell.Type = "String";            
               _Iktathato.Name = "EREC_KuldKuldemenyek.Iktathato";
               _Iktathato.Type = "String";            
               _SztornirozasDat.Name = "EREC_KuldKuldemenyek.SztornirozasDat";
               _SztornirozasDat.Type = "DateTime";            
               _KuldKuldemeny_Id_Szulo.Name = "EREC_KuldKuldemenyek.KuldKuldemeny_Id_Szulo";
               _KuldKuldemeny_Id_Szulo.Type = "Guid";            
               _Erkeztetes_Ev.Name = "EREC_KuldKuldemenyek.Erkeztetes_Ev";
               _Erkeztetes_Ev.Type = "Int32";            
               _Csoport_Id_Cimzett.Name = "EREC_KuldKuldemenyek.Csoport_Id_Cimzett";
               _Csoport_Id_Cimzett.Type = "Guid";            
               _Csoport_Id_Felelos.Name = "EREC_KuldKuldemenyek.Csoport_Id_Felelos";
               _Csoport_Id_Felelos.Type = "Guid";            
               _FelhasznaloCsoport_Id_Expedial.Name = "EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Expedial";
               _FelhasznaloCsoport_Id_Expedial.Type = "Guid";            
               _ExpedialasIdeje.Name = "EREC_KuldKuldemenyek.ExpedialasIdeje";
               _ExpedialasIdeje.Type = "DateTime";            
               _FelhasznaloCsoport_Id_Orzo.Name = "EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo";
               _FelhasznaloCsoport_Id_Orzo.Type = "Guid";            
               _FelhasznaloCsoport_Id_Atvevo.Name = "EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Atvevo";
               _FelhasznaloCsoport_Id_Atvevo.Type = "Guid";            
               _Partner_Id_Bekuldo.Name = "EREC_KuldKuldemenyek.Partner_Id_Bekuldo";
               _Partner_Id_Bekuldo.Type = "Guid";            
               _Cim_Id.Name = "EREC_KuldKuldemenyek.Cim_Id";
               _Cim_Id.Type = "Guid";            
               _CimSTR_Bekuldo.Name = "EREC_KuldKuldemenyek.CimSTR_Bekuldo";
               _CimSTR_Bekuldo.Type = "String";            
               _NevSTR_Bekuldo.Name = "EREC_KuldKuldemenyek.NevSTR_Bekuldo";
               _NevSTR_Bekuldo.Type = "FTSString";            
               _AdathordozoTipusa.Name = "EREC_KuldKuldemenyek.AdathordozoTipusa";
               _AdathordozoTipusa.Type = "String";            
               _ElsodlegesAdathordozoTipusa.Name = "EREC_KuldKuldemenyek.ElsodlegesAdathordozoTipusa";
               _ElsodlegesAdathordozoTipusa.Type = "String";            
               _FelhasznaloCsoport_Id_Alairo.Name = "EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Alairo";
               _FelhasznaloCsoport_Id_Alairo.Type = "Guid";            
               _BarCode.Name = "EREC_KuldKuldemenyek.BarCode";
               _BarCode.Type = "String";            
               _FelhasznaloCsoport_Id_Bonto.Name = "EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto";
               _FelhasznaloCsoport_Id_Bonto.Type = "Guid";            
               _CsoportFelelosEloszto_Id.Name = "EREC_KuldKuldemenyek.CsoportFelelosEloszto_Id";
               _CsoportFelelosEloszto_Id.Type = "Guid";            
               _Csoport_Id_Felelos_Elozo.Name = "EREC_KuldKuldemenyek.Csoport_Id_Felelos_Elozo";
               _Csoport_Id_Felelos_Elozo.Type = "Guid";            
               _Kovetkezo_Felelos_Id.Name = "EREC_KuldKuldemenyek.Kovetkezo_Felelos_Id";
               _Kovetkezo_Felelos_Id.Type = "Guid";            
               _Elektronikus_Kezbesitesi_Allap.Name = "EREC_KuldKuldemenyek.Elektronikus_Kezbesitesi_Allap";
               _Elektronikus_Kezbesitesi_Allap.Type = "String";            
               _Kovetkezo_Orzo_Id.Name = "EREC_KuldKuldemenyek.Kovetkezo_Orzo_Id";
               _Kovetkezo_Orzo_Id.Type = "Guid";            
               _Fizikai_Kezbesitesi_Allapot.Name = "EREC_KuldKuldemenyek.Fizikai_Kezbesitesi_Allapot";
               _Fizikai_Kezbesitesi_Allapot.Type = "String";            
               _IraIratok_Id.Name = "EREC_KuldKuldemenyek.IraIratok_Id";
               _IraIratok_Id.Type = "Guid";            
               _BontasiMegjegyzes.Name = "EREC_KuldKuldemenyek.BontasiMegjegyzes";
               _BontasiMegjegyzes.Type = "String";            
               _Tipus.Name = "EREC_KuldKuldemenyek.Tipus";
               _Tipus.Type = "String";            
               _Minosites.Name = "EREC_KuldKuldemenyek.Minosites";
               _Minosites.Type = "String";            
               _MegtagadasIndoka.Name = "EREC_KuldKuldemenyek.MegtagadasIndoka";
               _MegtagadasIndoka.Type = "String";            
               _Megtagado_Id.Name = "EREC_KuldKuldemenyek.Megtagado_Id";
               _Megtagado_Id.Type = "Guid";            
               _MegtagadasDat.Name = "EREC_KuldKuldemenyek.MegtagadasDat";
               _MegtagadasDat.Type = "DateTime";            
               _KimenoKuldemenyFajta.Name = "EREC_KuldKuldemenyek.KimenoKuldemenyFajta";
               _KimenoKuldemenyFajta.Type = "String";            
               _Elsobbsegi.Name = "EREC_KuldKuldemenyek.Elsobbsegi";
               _Elsobbsegi.Type = "Char";            
               _Ajanlott.Name = "EREC_KuldKuldemenyek.Ajanlott";
               _Ajanlott.Type = "Char";            
               _Tertiveveny.Name = "EREC_KuldKuldemenyek.Tertiveveny";
               _Tertiveveny.Type = "Char";            
               _SajatKezbe.Name = "EREC_KuldKuldemenyek.SajatKezbe";
               _SajatKezbe.Type = "Char";            
               _E_ertesites.Name = "EREC_KuldKuldemenyek.E_ertesites";
               _E_ertesites.Type = "Char";            
               _E_elorejelzes.Name = "EREC_KuldKuldemenyek.E_elorejelzes";
               _E_elorejelzes.Type = "Char";            
               _PostaiLezaroSzolgalat.Name = "EREC_KuldKuldemenyek.PostaiLezaroSzolgalat";
               _PostaiLezaroSzolgalat.Type = "Char";            
               _Ar.Name = "EREC_KuldKuldemenyek.Ar";
               _Ar.Type = "String";            
               _KimenoKuld_Sorszam .Name = "EREC_KuldKuldemenyek.KimenoKuld_Sorszam ";
               _KimenoKuld_Sorszam .Type = "Int32";            
               _IrattarId.Name = "EREC_KuldKuldemenyek.IrattarId";
               _IrattarId.Type = "Guid";            
               _IrattariHely.Name = "EREC_KuldKuldemenyek.IrattariHely";
               _IrattariHely.Type = "String";            
               _TovabbitasAlattAllapot.Name = "EREC_KuldKuldemenyek.TovabbitasAlattAllapot";
               _TovabbitasAlattAllapot.Type = "String";            
               _Azonosito.Name = "EREC_KuldKuldemenyek.Azonosito";
               _Azonosito.Type = "String";            
               _BoritoTipus.Name = "EREC_KuldKuldemenyek.BoritoTipus";
               _BoritoTipus.Type = "String";            
               _MegorzesJelzo.Name = "EREC_KuldKuldemenyek.MegorzesJelzo";
               _MegorzesJelzo.Type = "Char";            
               _KulsoAzonosito.Name = "EREC_KuldKuldemenyek.KulsoAzonosito";
               _KulsoAzonosito.Type = "String";            
               _ErvKezd.Name = "EREC_KuldKuldemenyek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_KuldKuldemenyek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _IktatastNemIgenyel.Name = "EREC_KuldKuldemenyek.IktatastNemIgenyel";
               _IktatastNemIgenyel.Type = "Char";            
               _KezbesitesModja.Name = "EREC_KuldKuldemenyek.KezbesitesModja";
               _KezbesitesModja.Type = "String";            
               _Munkaallomas.Name = "EREC_KuldKuldemenyek.Munkaallomas";
               _Munkaallomas.Type = "String";            
               _SerultKuldemeny.Name = "EREC_KuldKuldemenyek.SerultKuldemeny";
               _SerultKuldemeny.Type = "Char";            
               _TevesCimzes.Name = "EREC_KuldKuldemenyek.TevesCimzes";
               _TevesCimzes.Type = "Char";            
               _TevesErkeztetes.Name = "EREC_KuldKuldemenyek.TevesErkeztetes";
               _TevesErkeztetes.Type = "Char";            
               _CimzesTipusa.Name = "EREC_KuldKuldemenyek.CimzesTipusa";
               _CimzesTipusa.Type = "String";            
               _FutarJegyzekListaSzama.Name = "EREC_KuldKuldemenyek.FutarJegyzekListaSzama";
               _FutarJegyzekListaSzama.Type = "String";            
               _KezbesitoAtvetelIdopontja.Name = "EREC_KuldKuldemenyek.KezbesitoAtvetelIdopontja";
               _KezbesitoAtvetelIdopontja.Type = "DateTime";            
               _KezbesitoAtvevoNeve.Name = "EREC_KuldKuldemenyek.KezbesitoAtvevoNeve";
               _KezbesitoAtvevoNeve.Type = "String";            
               _Minosito.Name = "EREC_KuldKuldemenyek.Minosito";
               _Minosito.Type = "Guid";            
               _MinositesErvenyessegiIdeje.Name = "EREC_KuldKuldemenyek.MinositesErvenyessegiIdeje";
               _MinositesErvenyessegiIdeje.Type = "DateTime";            
               _TerjedelemMennyiseg.Name = "EREC_KuldKuldemenyek.TerjedelemMennyiseg";
               _TerjedelemMennyiseg.Type = "Int32";            
               _TerjedelemMennyisegiEgyseg.Name = "EREC_KuldKuldemenyek.TerjedelemMennyisegiEgyseg";
               _TerjedelemMennyisegiEgyseg.Type = "String";            
               _TerjedelemMegjegyzes.Name = "EREC_KuldKuldemenyek.TerjedelemMegjegyzes";
               _TerjedelemMegjegyzes.Type = "String";            
               _ModositasErvenyessegKezdete.Name = "EREC_KuldKuldemenyek.ModositasErvenyessegKezdete";
               _ModositasErvenyessegKezdete.Type = "DateTime";            
               _MegszuntetoHatarozat.Name = "EREC_KuldKuldemenyek.MegszuntetoHatarozat";
               _MegszuntetoHatarozat.Type = "String";            
               _FelulvizsgalatDatuma.Name = "EREC_KuldKuldemenyek.FelulvizsgalatDatuma";
               _FelulvizsgalatDatuma.Type = "DateTime";            
               _Felulvizsgalo_id.Name = "EREC_KuldKuldemenyek.Felulvizsgalo_id";
               _Felulvizsgalo_id.Type = "Guid";            
               _MinositesFelulvizsgalatEredm.Name = "EREC_KuldKuldemenyek.MinositesFelulvizsgalatEredm";
               _MinositesFelulvizsgalatEredm.Type = "String";            
               _MinositoSzervezet.Name = "EREC_KuldKuldemenyek.MinositoSzervezet";
               _MinositoSzervezet.Type = "Guid";            
               _Partner_Id_BekuldoKapcsolt.Name = "EREC_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt";
               _Partner_Id_BekuldoKapcsolt.Type = "Guid";            
               _MinositesFelulvizsgalatBizTag.Name = "EREC_KuldKuldemenyek.MinositesFelulvizsgalatBizTag";
               _MinositesFelulvizsgalatBizTag.Type = "String";            
               _Erteknyilvanitas.Name = "EREC_KuldKuldemenyek.Erteknyilvanitas";
               _Erteknyilvanitas.Type = "Char";            
               _ErteknyilvanitasOsszege.Name = "EREC_KuldKuldemenyek.ErteknyilvanitasOsszege";
               _ErteknyilvanitasOsszege.Type = "Int32";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// A k�ldem�ny �llapota. KCS: KULDEMENY_ALLAPOT
        /// 00 - �tvett
        /// 01 - �rkeztetett
        /// 03 - Tov�bb�t�s alatt
        /// 04 - Iktatott
        /// 05 - Szign�lt
        /// 06 - Post�zott
        /// 10 - Iktat�s megtagadva
        /// 51 - Kimen�, �ssze�ll�t�s alatt
        /// 52 - Expedi�lt
        /// 90 - Sztorn�zott
        /// 99 - Lez�rt
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// BeerkezesIdeje property
        /// A k�ldem�ny be�rkez�si ideje 
        /// </summary>
        private Field _BeerkezesIdeje = new Field();

        public Field BeerkezesIdeje
        {
            get { return _BeerkezesIdeje; }
            set { _BeerkezesIdeje = value; }
        }
                          
           
        /// <summary>
        /// FelbontasDatuma property
        /// 
        /// </summary>
        private Field _FelbontasDatuma = new Field();

        public Field FelbontasDatuma
        {
            get { return _FelbontasDatuma; }
            set { _FelbontasDatuma = value; }
        }
                          
           
        /// <summary>
        /// IraIktatokonyv_Id property
        /// IraIktatokonyv Id
        /// </summary>
        private Field _IraIktatokonyv_Id = new Field();

        public Field IraIktatokonyv_Id
        {
            get { return _IraIktatokonyv_Id; }
            set { _IraIktatokonyv_Id = value; }
        }
                          
           
        /// <summary>
        /// KuldesMod property
        /// KCS: KULDEMENY_KULDES_MODJA
        /// 01 - Postai sima
        /// 02 - Postai t�rtivev�nyes
        /// 03 - Postai aj�nlott
        /// 04 - �gyf�l
        /// 05 - �llami fut�rszolg�lat
        /// 07 - Diplom�ciai fut�rszolg�lat
        /// 08 - L�giposta
        /// 09 - K�zbes�t� �tj�n
        /// 10 - Fax
        /// 11 - E-mail
        /// 13 - Helyben
        /// 14 - Postai t�virat
        /// 15 - Port�lon kereszt�l
        /// 17 - Postai els�bbs�gi
        /// 18 - Fut�rszolg�lat
        /// </summary>
        private Field _KuldesMod = new Field();

        public Field KuldesMod
        {
            get { return _KuldesMod; }
            set { _KuldesMod = value; }
        }
                          
           
        /// <summary>
        /// Erkezteto_Szam property
        /// EDOK szerinti �rkeztet�sz�m.
        /// 
        /// </summary>
        private Field _Erkezteto_Szam = new Field();

        public Field Erkezteto_Szam
        {
            get { return _Erkezteto_Szam; }
            set { _Erkezteto_Szam = value; }
        }
                          
           
        /// <summary>
        /// HivatkozasiSzam property
        /// �gyf�l, bek�ld� hivatkoz�si sz�ma
        /// </summary>
        private Field _HivatkozasiSzam = new Field();

        public Field HivatkozasiSzam
        {
            get { return _HivatkozasiSzam; }
            set { _HivatkozasiSzam = value; }
        }
                          
           
        /// <summary>
        /// Targy property
        /// K�ldem�ny t�rgya, le�r�sa
        /// </summary>
        private Field _Targy = new Field();

        public Field Targy
        {
            get { return _Targy; }
            set { _Targy = value; }
        }
                          
           
        /// <summary>
        /// Tartalom property
        /// 
        /// </summary>
        private Field _Tartalom = new Field();

        public Field Tartalom
        {
            get { return _Tartalom; }
            set { _Tartalom = value; }
        }
                          
           
        /// <summary>
        /// RagSzam property
        /// Postai ragsz�m 
        /// </summary>
        private Field _RagSzam = new Field();

        public Field RagSzam
        {
            get { return _RagSzam; }
            set { _RagSzam = value; }
        }
                          
           
        /// <summary>
        /// Surgosseg property
        /// KCS: SURGOSSEG
        /// 
        /// </summary>
        private Field _Surgosseg = new Field();

        public Field Surgosseg
        {
            get { return _Surgosseg; }
            set { _Surgosseg = value; }
        }
                          
           
        /// <summary>
        /// BelyegzoDatuma property
        /// B�lyegz� d�tuma, post�z�s d�tuma, kemen�re, �s bej�v�re
        /// </summary>
        private Field _BelyegzoDatuma = new Field();

        public Field BelyegzoDatuma
        {
            get { return _BelyegzoDatuma; }
            set { _BelyegzoDatuma = value; }
        }
                          
           
        /// <summary>
        /// UgyintezesModja property
        /// KCS: 'UGYINTEZES_ALAPJA'
        /// 0 - Hagyom�nyos, pap�r
        /// 1 - Elektronikus (nincs pap�r)
        /// 
        /// </summary>
        private Field _UgyintezesModja = new Field();

        public Field UgyintezesModja
        {
            get { return _UgyintezesModja; }
            set { _UgyintezesModja = value; }
        }
                          
           
        /// <summary>
        /// PostazasIranya property
        /// KCS: POSTAZAS_IRANYA
        /// 1 - Bej�v�
        /// 2 - Kimen�
        /// </summary>
        private Field _PostazasIranya = new Field();

        public Field PostazasIranya
        {
            get { return _PostazasIranya; }
            set { _PostazasIranya = value; }
        }
                          
           
        /// <summary>
        /// Tovabbito property
        /// KCS: TOVABBITO_SZERVEZET
        /// </summary>
        private Field _Tovabbito = new Field();

        public Field Tovabbito
        {
            get { return _Tovabbito; }
            set { _Tovabbito = value; }
        }
                          
           
        /// <summary>
        /// PeldanySzam property
        /// K�ldem�ny p�ld�nysz�ma
        /// </summary>
        private Field _PeldanySzam = new Field();

        public Field PeldanySzam
        {
            get { return _PeldanySzam; }
            set { _PeldanySzam = value; }
        }
                          
           
        /// <summary>
        /// IktatniKell property
        /// 0 - Nem, 1 - Igen
        /// 
        /// </summary>
        private Field _IktatniKell = new Field();

        public Field IktatniKell
        {
            get { return _IktatniKell; }
            set { _IktatniKell = value; }
        }
                          
           
        /// <summary>
        /// Iktathato property
        /// A t�tel szign�l�si �llapota. KCS: KuldIktSzignTipus
        ///  0 - szign�l�s el�tti
        ///  S - szervezeti szign�l�s sz�ks�ges
        ///  U - szervezeti �s �gyint�z�re val� szign�l�s sz�ks�ges
        ///  1- iktathat�
        /// 
        /// </summary>
        private Field _Iktathato = new Field();

        public Field Iktathato
        {
            get { return _Iktathato; }
            set { _Iktathato = value; }
        }
                          
           
        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }
                          
           
        /// <summary>
        /// KuldKuldemeny_Id_Szulo property
        /// K�ldem�ny Id
        /// </summary>
        private Field _KuldKuldemeny_Id_Szulo = new Field();

        public Field KuldKuldemeny_Id_Szulo
        {
            get { return _KuldKuldemeny_Id_Szulo; }
            set { _KuldKuldemeny_Id_Szulo = value; }
        }
                          
           
        /// <summary>
        /// Erkeztetes_Ev property
        /// �rkeztet�s �ve.
        /// </summary>
        private Field _Erkeztetes_Ev = new Field();

        public Field Erkeztetes_Ev
        {
            get { return _Erkeztetes_Ev; }
            set { _Erkeztetes_Ev = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Cimzett property
        /// Minden jogad�s K�VETKEZETESEN CSOPORTnak t�rt�nik! 
        /// M�rpedig c�mzett csak olyan lehet, aki kezdeni is tud valamit a k�ldem�nynel (�k lesznek az els� felel�s). 
        /// Ez�rt mind a c�mzett, mind a felel�s csoport. A felel�s MINDEN�TT csoport, a kimen� k�ldem�ny (iratp�ld�ny) c�mzettje - aki tipikusan k�ls� - PARTNER!
        /// 
        /// </summary>
        private Field _Csoport_Id_Cimzett = new Field();

        public Field Csoport_Id_Cimzett
        {
            get { return _Csoport_Id_Cimzett; }
            set { _Csoport_Id_Cimzett = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Felel�s felhaszn�l� Id
        /// </summary>
        private Field _Csoport_Id_Felelos = new Field();

        public Field Csoport_Id_Felelos
        {
            get { return _Csoport_Id_Felelos; }
            set { _Csoport_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Expedial property
        /// 
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Expedial = new Field();

        public Field FelhasznaloCsoport_Id_Expedial
        {
            get { return _FelhasznaloCsoport_Id_Expedial; }
            set { _FelhasznaloCsoport_Id_Expedial = value; }
        }
                          
           
        /// <summary>
        /// ExpedialasIdeje property
        /// 
        /// </summary>
        private Field _ExpedialasIdeje = new Field();

        public Field ExpedialasIdeje
        {
            get { return _ExpedialasIdeje; }
            set { _ExpedialasIdeje = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Orzo property
        /// Minden olyan helyen, ahol adott SZEM�LY azonos�that� (valamit csin�lt, pl. �tvett, vagy v�grehajtott valamit), ott FELHASZN�L� van r�gz�tve, mert TUDJUK, hogy ki volt az elk�vet� (a felel�s eset�ben ezt SOHA nem tudjuk, mert helyettes�t�s, vagy m�s jog miatt m�g a szem�lyre szign�l�sn�l is LEHET elt�r� a t�nyleges v�grehajt�).
        /// (Kovax)
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Orzo = new Field();

        public Field FelhasznaloCsoport_Id_Orzo
        {
            get { return _FelhasznaloCsoport_Id_Orzo; }
            set { _FelhasznaloCsoport_Id_Orzo = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Atvevo property
        /// Minden olyan helyen, ahol adott SZEM�LY azonos�that� (valamit csin�lt, pl. �tvett, vagy v�grehajtott valamit), ott FELHASZN�L� van r�gz�tve, mert TUDJUK, hogy ki volt az elk�vet� (a felel�s eset�ben ezt SOHA nem tudjuk, mert helyettes�t�s, vagy m�s jog miatt m�g a szem�lyre szign�l�sn�l is LEHET elt�r� a t�nyleges v�grehajt�). (Kovax)
        /// 
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Atvevo = new Field();

        public Field FelhasznaloCsoport_Id_Atvevo
        {
            get { return _FelhasznaloCsoport_Id_Atvevo; }
            set { _FelhasznaloCsoport_Id_Atvevo = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id_Bekuldo property
        /// Bek�ld� Id -ja...
        /// </summary>
        private Field _Partner_Id_Bekuldo = new Field();

        public Field Partner_Id_Bekuldo
        {
            get { return _Partner_Id_Bekuldo; }
            set { _Partner_Id_Bekuldo = value; }
        }
                          
           
        /// <summary>
        /// Cim_Id property
        /// C�m Id
        /// </summary>
        private Field _Cim_Id = new Field();

        public Field Cim_Id
        {
            get { return _Cim_Id; }
            set { _Cim_Id = value; }
        }
                          
           
        /// <summary>
        /// CimSTR_Bekuldo property
        /// Bek�ld� c�me sz�vegesen
        /// </summary>
        private Field _CimSTR_Bekuldo = new Field();

        public Field CimSTR_Bekuldo
        {
            get { return _CimSTR_Bekuldo; }
            set { _CimSTR_Bekuldo = value; }
        }
                          
           
        /// <summary>
        /// NevSTR_Bekuldo property
        /// Bek�ld� neve sz�vegesen
        /// </summary>
        private Field _NevSTR_Bekuldo = new Field();

        public Field NevSTR_Bekuldo
        {
            get { return _NevSTR_Bekuldo; }
            set { _NevSTR_Bekuldo = value; }
        }
                          
           
        /// <summary>
        /// AdathordozoTipusa property
        /// KCS:ADATHORDOZO_TIPUSA
        /// 
        /// </summary>
        private Field _AdathordozoTipusa = new Field();

        public Field AdathordozoTipusa
        {
            get { return _AdathordozoTipusa; }
            set { _AdathordozoTipusa = value; }
        }
                          
           
        /// <summary>
        /// ElsodlegesAdathordozoTipusa property
        /// KCS: ELSODLEGES_ADATHORDOZO pap�r, elektronikus
        /// </summary>
        private Field _ElsodlegesAdathordozoTipusa = new Field();

        public Field ElsodlegesAdathordozoTipusa
        {
            get { return _ElsodlegesAdathordozoTipusa; }
            set { _ElsodlegesAdathordozoTipusa = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Alairo property
        /// Al��r� felhaszn�l� Id
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Alairo = new Field();

        public Field FelhasznaloCsoport_Id_Alairo
        {
            get { return _FelhasznaloCsoport_Id_Alairo; }
            set { _FelhasznaloCsoport_Id_Alairo = value; }
        }
                          
           
        /// <summary>
        /// BarCode property
        /// Vonalk�d �rt�ke
        /// </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Bonto property
        /// A kuldemenyt felbonto felhasznalo. A minosites miatt kotelezo a lehetoseg. Nem bonthato kuldemeny eseten (pl. nincs targy) maradhat uresen.
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Bonto = new Field();

        public Field FelhasznaloCsoport_Id_Bonto
        {
            get { return _FelhasznaloCsoport_Id_Bonto; }
            set { _FelhasznaloCsoport_Id_Bonto = value; }
        }
                          
           
        /// <summary>
        /// CsoportFelelosEloszto_Id property
        /// Felel�s csoport/szem�ly Id-je
        /// </summary>
        private Field _CsoportFelelosEloszto_Id = new Field();

        public Field CsoportFelelosEloszto_Id
        {
            get { return _CsoportFelelosEloszto_Id; }
            set { _CsoportFelelosEloszto_Id = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Felelos_Elozo property
        /// Elozo felelos CSOPORT Id
        /// </summary>
        private Field _Csoport_Id_Felelos_Elozo = new Field();

        public Field Csoport_Id_Felelos_Elozo
        {
            get { return _Csoport_Id_Felelos_Elozo; }
            set { _Csoport_Id_Felelos_Elozo = value; }
        }
                          
           
        /// <summary>
        /// Kovetkezo_Felelos_Id property
        /// 
        /// </summary>
        private Field _Kovetkezo_Felelos_Id = new Field();

        public Field Kovetkezo_Felelos_Id
        {
            get { return _Kovetkezo_Felelos_Id; }
            set { _Kovetkezo_Felelos_Id = value; }
        }
                          
           
        /// <summary>
        /// Elektronikus_Kezbesitesi_Allap property
        /// 
        /// </summary>
        private Field _Elektronikus_Kezbesitesi_Allap = new Field();

        public Field Elektronikus_Kezbesitesi_Allap
        {
            get { return _Elektronikus_Kezbesitesi_Allap; }
            set { _Elektronikus_Kezbesitesi_Allap = value; }
        }
                          
           
        /// <summary>
        /// Kovetkezo_Orzo_Id property
        /// 
        /// </summary>
        private Field _Kovetkezo_Orzo_Id = new Field();

        public Field Kovetkezo_Orzo_Id
        {
            get { return _Kovetkezo_Orzo_Id; }
            set { _Kovetkezo_Orzo_Id = value; }
        }
                          
           
        /// <summary>
        /// Fizikai_Kezbesitesi_Allapot property
        /// KCS: ??
        /// </summary>
        private Field _Fizikai_Kezbesitesi_Allapot = new Field();

        public Field Fizikai_Kezbesitesi_Allapot
        {
            get { return _Fizikai_Kezbesitesi_Allapot; }
            set { _Fizikai_Kezbesitesi_Allapot = value; }
        }
                          
           
        /// <summary>
        /// IraIratok_Id property
        /// Visszamutat az iratra, 1..1-es kapcsolat.
        /// </summary>
        private Field _IraIratok_Id = new Field();

        public Field IraIratok_Id
        {
            get { return _IraIratok_Id; }
            set { _IraIratok_Id = value; }
        }
                          
           
        /// <summary>
        /// BontasiMegjegyzes property
        /// A bot�ssal kapcsolatos megjegyz�s helye
        /// </summary>
        private Field _BontasiMegjegyzes = new Field();

        public Field BontasiMegjegyzes
        {
            get { return _BontasiMegjegyzes; }
            set { _BontasiMegjegyzes = value; }
        }
                          
           
        /// <summary>
        /// Tipus property
        /// KCS: KULDEMENY_TIPUS
        /// 01 - Sz�mla
        /// 02 - Szerz�d�s
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Minosites property
        /// A nyilv�nos �s nem nyilv�nos iratok (irat alapj�ul szolg�l� k�ldem�nyek) megk�l�nb�ztet�se. KCS: IRAT_MINOSITES
        ///  �res- nyilv�nos
        ///  10 - bels� haszn�lat�, nem nyilv�nos
        ///  20 - d�nt�s el�k�sz�t�, nem nyilv�nos
        /// </summary>
        private Field _Minosites = new Field();

        public Field Minosites
        {
            get { return _Minosites; }
            set { _Minosites = value; }
        }
                          
           
        /// <summary>
        /// MegtagadasIndoka property
        /// Csak elektronikus k�ldem�ny iktat�s megtagad�sa eset�n
        /// </summary>
        private Field _MegtagadasIndoka = new Field();

        public Field MegtagadasIndoka
        {
            get { return _MegtagadasIndoka; }
            set { _MegtagadasIndoka = value; }
        }
                          
           
        /// <summary>
        /// Megtagado_Id property
        /// Felhaszn�l� id az elektronikus k�ldem�ny iktat�s megtagad�sa eset�n
        /// </summary>
        private Field _Megtagado_Id = new Field();

        public Field Megtagado_Id
        {
            get { return _Megtagado_Id; }
            set { _Megtagado_Id = value; }
        }
                          
           
        /// <summary>
        /// MegtagadasDat property
        /// Az elektronikus k�ldem�ny iktat�s megtagad�si d�tuma
        /// </summary>
        private Field _MegtagadasDat = new Field();

        public Field MegtagadasDat
        {
            get { return _MegtagadasDat; }
            set { _MegtagadasDat = value; }
        }
                          
           
        /// <summary>
        /// KimenoKuldemenyFajta property
        /// Postazasi adat,  KCS: KIMENO_KULDEMENY_FAJTA
        /// </summary>
        private Field _KimenoKuldemenyFajta = new Field();

        public Field KimenoKuldemenyFajta
        {
            get { return _KimenoKuldemenyFajta; }
            set { _KimenoKuldemenyFajta = value; }
        }
                          
           
        /// <summary>
        /// Elsobbsegi property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        private Field _Elsobbsegi = new Field();

        public Field Elsobbsegi
        {
            get { return _Elsobbsegi; }
            set { _Elsobbsegi = value; }
        }
                          
           
        /// <summary>
        /// Ajanlott property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        private Field _Ajanlott = new Field();

        public Field Ajanlott
        {
            get { return _Ajanlott; }
            set { _Ajanlott = value; }
        }
                          
           
        /// <summary>
        /// Tertiveveny property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        private Field _Tertiveveny = new Field();

        public Field Tertiveveny
        {
            get { return _Tertiveveny; }
            set { _Tertiveveny = value; }
        }
                          
           
        /// <summary>
        /// SajatKezbe property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        private Field _SajatKezbe = new Field();

        public Field SajatKezbe
        {
            get { return _SajatKezbe; }
            set { _SajatKezbe = value; }
        }
                          
           
        /// <summary>
        /// E_ertesites property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        private Field _E_ertesites = new Field();

        public Field E_ertesites
        {
            get { return _E_ertesites; }
            set { _E_ertesites = value; }
        }
                          
           
        /// <summary>
        /// E_elorejelzes property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        private Field _E_elorejelzes = new Field();

        public Field E_elorejelzes
        {
            get { return _E_elorejelzes; }
            set { _E_elorejelzes = value; }
        }
                          
           
        /// <summary>
        /// PostaiLezaroSzolgalat property
        /// Postazasi adat, 1-igen, 0-nem
        /// </summary>
        private Field _PostaiLezaroSzolgalat = new Field();

        public Field PostaiLezaroSzolgalat
        {
            get { return _PostaiLezaroSzolgalat; }
            set { _PostaiLezaroSzolgalat = value; }
        }
                          
           
        /// <summary>
        /// Ar property
        /// Postazasi adat, a feladand� k�ldem�ny post�z�si d�ja
        /// </summary>
        private Field _Ar = new Field();

        public Field Ar
        {
            get { return _Ar; }
            set { _Ar = value; }
        }
                          
           
        /// <summary>
        /// KimenoKuld_Sorszam  property
        /// Postazasi adat, gener�lt sorsz�m
        /// </summary>
        private Field _KimenoKuld_Sorszam  = new Field();

        public Field KimenoKuld_Sorszam 
        {
            get { return _KimenoKuld_Sorszam ; }
            set { _KimenoKuld_Sorszam  = value; }
        }
                          
           
        /// <summary>
        /// IrattarId property
        /// 
        /// </summary>
        private Field _IrattarId = new Field();

        public Field IrattarId
        {
            get { return _IrattarId; }
            set { _IrattarId = value; }
        }
                          
           
        /// <summary>
        /// IrattariHely property
        /// 
        /// </summary>
        private Field _IrattariHely = new Field();

        public Field IrattariHely
        {
            get { return _IrattariHely; }
            set { _IrattariHely = value; }
        }
                          
           
        /// <summary>
        /// TovabbitasAlattAllapot property
        /// KCS: KULDEMENY_ALLAPOT
        /// </summary>
        private Field _TovabbitasAlattAllapot = new Field();

        public Field TovabbitasAlattAllapot
        {
            get { return _TovabbitasAlattAllapot; }
            set { _TovabbitasAlattAllapot = value; }
        }
                          
           
        /// <summary>
        /// Azonosito property
        /// A k�ldem�ny azonos�t�ja k�ls� rendszerbeli (pl. eGovPortal) azonos�t�ja.
        /// </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// BoritoTipus property
        /// KCS:KULDEMENY_BORITO_TIPUS, 01-Bor�t�k, 02-Bor�t�, 03-Irat
        /// </summary>
        private Field _BoritoTipus = new Field();

        public Field BoritoTipus
        {
            get { return _BoritoTipus; }
            set { _BoritoTipus = value; }
        }
                          
           
        /// <summary>
        /// MegorzesJelzo property
        /// 1:meg�rzend�, 0-egy�bk�nt
        /// </summary>
        private Field _MegorzesJelzo = new Field();

        public Field MegorzesJelzo
        {
            get { return _MegorzesJelzo; }
            set { _MegorzesJelzo = value; }
        }
                          
           
        /// <summary>
        /// KulsoAzonosito property
        /// 
        /// </summary>
        private Field _KulsoAzonosito = new Field();

        public Field KulsoAzonosito
        {
            get { return _KulsoAzonosito; }
            set { _KulsoAzonosito = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// IktatastNemIgenyel property
        /// 
        /// </summary>
        private Field _IktatastNemIgenyel = new Field();

        public Field IktatastNemIgenyel
        {
            get { return _IktatastNemIgenyel; }
            set { _IktatastNemIgenyel = value; }
        }
                          
           
        /// <summary>
        /// KezbesitesModja property
        /// 
        /// </summary>
        private Field _KezbesitesModja = new Field();

        public Field KezbesitesModja
        {
            get { return _KezbesitesModja; }
            set { _KezbesitesModja = value; }
        }
                          
           
        /// <summary>
        /// Munkaallomas property
        /// 
        /// </summary>
        private Field _Munkaallomas = new Field();

        public Field Munkaallomas
        {
            get { return _Munkaallomas; }
            set { _Munkaallomas = value; }
        }
                          
           
        /// <summary>
        /// SerultKuldemeny property
        /// 
        /// </summary>
        private Field _SerultKuldemeny = new Field();

        public Field SerultKuldemeny
        {
            get { return _SerultKuldemeny; }
            set { _SerultKuldemeny = value; }
        }
                          
           
        /// <summary>
        /// TevesCimzes property
        /// 
        /// </summary>
        private Field _TevesCimzes = new Field();

        public Field TevesCimzes
        {
            get { return _TevesCimzes; }
            set { _TevesCimzes = value; }
        }
                          
           
        /// <summary>
        /// TevesErkeztetes property
        /// 
        /// </summary>
        private Field _TevesErkeztetes = new Field();

        public Field TevesErkeztetes
        {
            get { return _TevesErkeztetes; }
            set { _TevesErkeztetes = value; }
        }
                          
           
        /// <summary>
        /// CimzesTipusa property
        /// 
        /// </summary>
        private Field _CimzesTipusa = new Field();

        public Field CimzesTipusa
        {
            get { return _CimzesTipusa; }
            set { _CimzesTipusa = value; }
        }
                          
           
        /// <summary>
        /// FutarJegyzekListaSzama property
        /// Fut�rszolg�lati k�zbes�t�s eset�n Fut�rjegyz�k-lista sz�ma, Szem�lyes k�zbes�t�s eset�n K�ls� k�zbes�t�k�nyv nyilv�ntart�si sz�ma. Csak T�K-ben haszn�ljuk
        /// </summary>
        private Field _FutarJegyzekListaSzama = new Field();

        public Field FutarJegyzekListaSzama
        {
            get { return _FutarJegyzekListaSzama; }
            set { _FutarJegyzekListaSzama = value; }
        }
                          
           
        /// <summary>
        /// KezbesitoAtvetelIdopontja property
        /// 
        /// </summary>
        private Field _KezbesitoAtvetelIdopontja = new Field();

        public Field KezbesitoAtvetelIdopontja
        {
            get { return _KezbesitoAtvetelIdopontja; }
            set { _KezbesitoAtvetelIdopontja = value; }
        }
                          
           
        /// <summary>
        /// KezbesitoAtvevoNeve property
        /// 
        /// </summary>
        private Field _KezbesitoAtvevoNeve = new Field();

        public Field KezbesitoAtvevoNeve
        {
            get { return _KezbesitoAtvevoNeve; }
            set { _KezbesitoAtvevoNeve = value; }
        }
                          
           
        /// <summary>
        /// Minosito property
        /// 
        /// </summary>
        private Field _Minosito = new Field();

        public Field Minosito
        {
            get { return _Minosito; }
            set { _Minosito = value; }
        }
                          
           
        /// <summary>
        /// MinositesErvenyessegiIdeje property
        /// 
        /// </summary>
        private Field _MinositesErvenyessegiIdeje = new Field();

        public Field MinositesErvenyessegiIdeje
        {
            get { return _MinositesErvenyessegiIdeje; }
            set { _MinositesErvenyessegiIdeje = value; }
        }
                          
           
        /// <summary>
        /// TerjedelemMennyiseg property
        /// 
        /// </summary>
        private Field _TerjedelemMennyiseg = new Field();

        public Field TerjedelemMennyiseg
        {
            get { return _TerjedelemMennyiseg; }
            set { _TerjedelemMennyiseg = value; }
        }
                          
           
        /// <summary>
        /// TerjedelemMennyisegiEgyseg property
        /// 
        /// </summary>
        private Field _TerjedelemMennyisegiEgyseg = new Field();

        public Field TerjedelemMennyisegiEgyseg
        {
            get { return _TerjedelemMennyisegiEgyseg; }
            set { _TerjedelemMennyisegiEgyseg = value; }
        }
                          
           
        /// <summary>
        /// TerjedelemMegjegyzes property
        /// 
        /// </summary>
        private Field _TerjedelemMegjegyzes = new Field();

        public Field TerjedelemMegjegyzes
        {
            get { return _TerjedelemMegjegyzes; }
            set { _TerjedelemMegjegyzes = value; }
        }
                          
           
        /// <summary>
        /// ModositasErvenyessegKezdete property
        /// 
        /// </summary>
        private Field _ModositasErvenyessegKezdete = new Field();

        public Field ModositasErvenyessegKezdete
        {
            get { return _ModositasErvenyessegKezdete; }
            set { _ModositasErvenyessegKezdete = value; }
        }
                          
           
        /// <summary>
        /// MegszuntetoHatarozat property
        /// 
        /// </summary>
        private Field _MegszuntetoHatarozat = new Field();

        public Field MegszuntetoHatarozat
        {
            get { return _MegszuntetoHatarozat; }
            set { _MegszuntetoHatarozat = value; }
        }
                          
           
        /// <summary>
        /// FelulvizsgalatDatuma property
        /// 
        /// </summary>
        private Field _FelulvizsgalatDatuma = new Field();

        public Field FelulvizsgalatDatuma
        {
            get { return _FelulvizsgalatDatuma; }
            set { _FelulvizsgalatDatuma = value; }
        }
                          
           
        /// <summary>
        /// Felulvizsgalo_id property
        /// 
        /// </summary>
        private Field _Felulvizsgalo_id = new Field();

        public Field Felulvizsgalo_id
        {
            get { return _Felulvizsgalo_id; }
            set { _Felulvizsgalo_id = value; }
        }
                          
           
        /// <summary>
        /// MinositesFelulvizsgalatEredm property
        /// 
        /// </summary>
        private Field _MinositesFelulvizsgalatEredm = new Field();

        public Field MinositesFelulvizsgalatEredm
        {
            get { return _MinositesFelulvizsgalatEredm; }
            set { _MinositesFelulvizsgalatEredm = value; }
        }
                          
           
        /// <summary>
        /// MinositoSzervezet property
        /// 
        /// </summary>
        private Field _MinositoSzervezet = new Field();

        public Field MinositoSzervezet
        {
            get { return _MinositoSzervezet; }
            set { _MinositoSzervezet = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id_BekuldoKapcsolt property
        /// 
        /// </summary>
        private Field _Partner_Id_BekuldoKapcsolt = new Field();

        public Field Partner_Id_BekuldoKapcsolt
        {
            get { return _Partner_Id_BekuldoKapcsolt; }
            set { _Partner_Id_BekuldoKapcsolt = value; }
        }
                          
           
        /// <summary>
        /// MinositesFelulvizsgalatBizTag property
        /// 
        /// </summary>
        private Field _MinositesFelulvizsgalatBizTag = new Field();

        public Field MinositesFelulvizsgalatBizTag
        {
            get { return _MinositesFelulvizsgalatBizTag; }
            set { _MinositesFelulvizsgalatBizTag = value; }
        }
                          
           
        /// <summary>
        /// Erteknyilvanitas property
        /// 
        /// </summary>
        private Field _Erteknyilvanitas = new Field();

        public Field Erteknyilvanitas
        {
            get { return _Erteknyilvanitas; }
            set { _Erteknyilvanitas = value; }
        }
                          
           
        /// <summary>
        /// ErteknyilvanitasOsszege property
        /// 
        /// </summary>
        private Field _ErteknyilvanitasOsszege = new Field();

        public Field ErteknyilvanitasOsszege
        {
            get { return _ErteknyilvanitasOsszege; }
            set { _ErteknyilvanitasOsszege = value; }
        }
                              }

}