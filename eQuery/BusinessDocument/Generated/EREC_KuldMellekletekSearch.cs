
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_KuldMellekletek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_KuldMellekletekSearch: BaseSearchObject    {
      public EREC_KuldMellekletekSearch()
      {            
                     _Id.Name = "EREC_KuldMellekletek.Id";
               _Id.Type = "Guid";            
               _KuldKuldemeny_Id.Name = "EREC_KuldMellekletek.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _Mennyiseg.Name = "EREC_KuldMellekletek.Mennyiseg";
               _Mennyiseg.Type = "Int32";            
               _Megjegyzes.Name = "EREC_KuldMellekletek.Megjegyzes";
               _Megjegyzes.Type = "String";            
               _SztornirozasDat.Name = "EREC_KuldMellekletek.SztornirozasDat";
               _SztornirozasDat.Type = "DateTime";            
               _AdathordozoTipus.Name = "EREC_KuldMellekletek.AdathordozoTipus";
               _AdathordozoTipus.Type = "String";            
               _MennyisegiEgyseg.Name = "EREC_KuldMellekletek.MennyisegiEgyseg";
               _MennyisegiEgyseg.Type = "String";            
               _BarCode.Name = "EREC_KuldMellekletek.BarCode";
               _BarCode.Type = "String";            
               _ErvKezd.Name = "EREC_KuldMellekletek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_KuldMellekletek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Mennyiseg = new Field();

        public Field Mennyiseg
        {
            get { return _Mennyiseg; }
            set { _Mennyiseg = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AdathordozoTipus = new Field();

        public Field AdathordozoTipus
        {
            get { return _AdathordozoTipus; }
            set { _AdathordozoTipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MennyisegiEgyseg = new Field();

        public Field MennyisegiEgyseg
        {
            get { return _MennyisegiEgyseg; }
            set { _MennyisegiEgyseg = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}