
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_KuldTertivevenyek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_KuldTertivevenyekSearch: BaseSearchObject    {
      public EREC_KuldTertivevenyekSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "EREC_KuldTertivevenyek.Id";
               _Id.Type = "Guid";            
               _Kuldemeny_Id.Name = "EREC_KuldTertivevenyek.Kuldemeny_Id";
               _Kuldemeny_Id.Type = "Guid";            
               _Ragszam.Name = "EREC_KuldTertivevenyek.Ragszam";
               _Ragszam.Type = "String";            
               _TertivisszaKod.Name = "EREC_KuldTertivevenyek.TertivisszaKod";
               _TertivisszaKod.Type = "String";            
               _TertivisszaDat.Name = "EREC_KuldTertivevenyek.TertivisszaDat";
               _TertivisszaDat.Type = "DateTime";            
               _AtvevoSzemely.Name = "EREC_KuldTertivevenyek.AtvevoSzemely";
               _AtvevoSzemely.Type = "String";            
               _AtvetelDat.Name = "EREC_KuldTertivevenyek.AtvetelDat";
               _AtvetelDat.Type = "DateTime";            
               _BarCode.Name = "EREC_KuldTertivevenyek.BarCode";
               _BarCode.Type = "String";            
               _Allapot.Name = "EREC_KuldTertivevenyek.Allapot";
               _Allapot.Type = "String";            
               _ErvKezd.Name = "EREC_KuldTertivevenyek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_KuldTertivevenyek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _KezbVelelemBeallta.Name = "EREC_KuldTertivevenyek.KezbVelelemBeallta";
               _KezbVelelemBeallta.Type = "Char";            
               _KezbVelelemDatuma.Name = "EREC_KuldTertivevenyek.KezbVelelemDatuma";
               _KezbVelelemDatuma.Type = "DateTime";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Kuldemeny_Id property
        /// FK
        /// </summary>
        private Field _Kuldemeny_Id = new Field();

        public Field Kuldemeny_Id
        {
            get { return _Kuldemeny_Id; }
            set { _Kuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// Ragszam property
        /// Postai ragsz�m 
        /// </summary>
        private Field _Ragszam = new Field();

        public Field Ragszam
        {
            get { return _Ragszam; }
            set { _Ragszam = value; }
        }
                          
           
        /// <summary>
        /// TertivisszaKod property
        /// KCS: TERTIVEVENY_VISSZA_KOD
        /// </summary>
        private Field _TertivisszaKod = new Field();

        public Field TertivisszaKod
        {
            get { return _TertivisszaKod; }
            set { _TertivisszaKod = value; }
        }
                          
           
        /// <summary>
        /// TertivisszaDat property
        /// T�rtivev�ny vissza�rkez�si d�tuma
        /// </summary>
        private Field _TertivisszaDat = new Field();

        public Field TertivisszaDat
        {
            get { return _TertivisszaDat; }
            set { _TertivisszaDat = value; }
        }
                          
           
        /// <summary>
        /// AtvevoSzemely property
        /// �tvev� szem�ly neve 
        /// </summary>
        private Field _AtvevoSzemely = new Field();

        public Field AtvevoSzemely
        {
            get { return _AtvevoSzemely; }
            set { _AtvevoSzemely = value; }
        }
                          
           
        /// <summary>
        /// AtvetelDat property
        /// T�rtivev�ny �tv�tel�nek d�tuma
        /// </summary>
        private Field _AtvetelDat = new Field();

        public Field AtvetelDat
        {
            get { return _AtvetelDat; }
            set { _AtvetelDat = value; }
        }
                          
           
        /// <summary>
        /// BarCode property
        /// Vonalk�d �rt�ke
        /// </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// KCS: TERTIVEV_ALLAPOT
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// KezbVelelemBeallta property
        /// 
        /// </summary>
        private Field _KezbVelelemBeallta = new Field();

        public Field KezbVelelemBeallta
        {
            get { return _KezbVelelemBeallta; }
            set { _KezbVelelemBeallta = value; }
        }
                          
           
        /// <summary>
        /// KezbVelelemDatuma property
        /// 
        /// </summary>
        private Field _KezbVelelemDatuma = new Field();

        public Field KezbVelelemDatuma
        {
            get { return _KezbVelelemDatuma; }
            set { _KezbVelelemDatuma = value; }
        }
                              }

}