
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Kuldemeny_IratPeldanyai eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_Kuldemeny_IratPeldanyaiSearch: BaseSearchObject    {
      public EREC_Kuldemeny_IratPeldanyaiSearch()
      {            
                     _Id.Name = "EREC_Kuldemeny_IratPeldanyai.Id";
               _Id.Type = "Guid";            
               _KuldKuldemeny_Id.Name = "EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _Peldany_Id.Name = "EREC_Kuldemeny_IratPeldanyai.Peldany_Id";
               _Peldany_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_Kuldemeny_IratPeldanyai.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Kuldemeny_IratPeldanyai.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Peldany_Id = new Field();

        public Field Peldany_Id
        {
            get { return _Peldany_Id; }
            set { _Peldany_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}