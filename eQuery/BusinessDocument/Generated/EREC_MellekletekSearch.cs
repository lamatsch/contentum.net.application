
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Mellekletek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_MellekletekSearch: BaseSearchObject    {
      public EREC_MellekletekSearch()
      {            
                     _Id.Name = "EREC_Mellekletek.Id";
               _Id.Type = "Guid";            
               _KuldKuldemeny_Id.Name = "EREC_Mellekletek.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _IraIrat_Id.Name = "EREC_Mellekletek.IraIrat_Id";
               _IraIrat_Id.Type = "Guid";            
               _AdathordozoTipus.Name = "EREC_Mellekletek.AdathordozoTipus";
               _AdathordozoTipus.Type = "String";            
               _Megjegyzes.Name = "EREC_Mellekletek.Megjegyzes";
               _Megjegyzes.Type = "String";            
               _SztornirozasDat.Name = "EREC_Mellekletek.SztornirozasDat";
               _SztornirozasDat.Type = "DateTime";            
               _MennyisegiEgyseg.Name = "EREC_Mellekletek.MennyisegiEgyseg";
               _MennyisegiEgyseg.Type = "String";            
               _Mennyiseg.Name = "EREC_Mellekletek.Mennyiseg";
               _Mennyiseg.Type = "Int32";            
               _BarCode.Name = "EREC_Mellekletek.BarCode";
               _BarCode.Type = "String";            
               _ErvKezd.Name = "EREC_Mellekletek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Mellekletek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _MellekletAdathordozoTipus.Name = "EREC_Mellekletek.MellekletAdathordozoTipus";
               _MellekletAdathordozoTipus.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// K�ldem�nyazonos�t�, k�ldem�nyhez kapcsol�d� mell�klet eset�n.
        /// </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// IraIrat_Id property
        /// Irat Id
        /// </summary>
        private Field _IraIrat_Id = new Field();

        public Field IraIrat_Id
        {
            get { return _IraIrat_Id; }
            set { _IraIrat_Id = value; }
        }
                          
           
        /// <summary>
        /// AdathordozoTipus property
        /// KCS:ADATHORDOZO_TIPUSA
        /// 01 - Pap�r  irat
        /// 02 - Fax
        /// 03 - E-mail
        /// 04 - Floppy lemez
        /// 05 - CD
        /// 06 - DVD
        /// 07 - Vide�
        /// 08 - Hang kazetta
        /// 09 - Egy�b
        /// 10 - Csomag
        /// 11 - Pendrive
        /// 12 - Bor�t�k
        /// 13 - Egy�b pap�r
        /// 
        /// </summary>
        private Field _AdathordozoTipus = new Field();

        public Field AdathordozoTipus
        {
            get { return _AdathordozoTipus; }
            set { _AdathordozoTipus = value; }
        }
                          
           
        /// <summary>
        /// Megjegyzes property
        /// Megjegyz�s
        /// </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }
                          
           
        /// <summary>
        /// MennyisegiEgyseg property
        /// KCS: MENNYISEGI_EGYSEG
        /// 
        /// </summary>
        private Field _MennyisegiEgyseg = new Field();

        public Field MennyisegiEgyseg
        {
            get { return _MennyisegiEgyseg; }
            set { _MennyisegiEgyseg = value; }
        }
                          
           
        /// <summary>
        /// Mennyiseg property
        /// Mennyis�gi egys�g szerinti mennyis�g
        /// </summary>
        private Field _Mennyiseg = new Field();

        public Field Mennyiseg
        {
            get { return _Mennyiseg; }
            set { _Mennyiseg = value; }
        }
                          
           
        /// <summary>
        /// BarCode property
        /// Vonalk�d �rt�ke
        /// </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// MellekletAdathordozoTipus property
        /// 
        /// </summary>
        private Field _MellekletAdathordozoTipus = new Field();

        public Field MellekletAdathordozoTipus
        {
            get { return _MellekletAdathordozoTipus; }
            set { _MellekletAdathordozoTipus = value; }
        }
                              }

}