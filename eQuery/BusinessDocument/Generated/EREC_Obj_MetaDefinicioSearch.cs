
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Obj_MetaDefinicio eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_Obj_MetaDefinicioSearch: BaseSearchObject    {
      public EREC_Obj_MetaDefinicioSearch()
      {            
                     _Id.Name = "EREC_Obj_MetaDefinicio.Id";
               _Id.Type = "Guid";            
               _Org.Name = "EREC_Obj_MetaDefinicio.Org";
               _Org.Type = "Guid";            
               _Objtip_Id.Name = "EREC_Obj_MetaDefinicio.Objtip_Id";
               _Objtip_Id.Type = "Guid";            
               _Objtip_Id_Column.Name = "EREC_Obj_MetaDefinicio.Objtip_Id_Column";
               _Objtip_Id_Column.Type = "Guid";            
               _ColumnValue.Name = "EREC_Obj_MetaDefinicio.ColumnValue";
               _ColumnValue.Type = "String";            
               _DefinicioTipus.Name = "EREC_Obj_MetaDefinicio.DefinicioTipus";
               _DefinicioTipus.Type = "String";            
               _Felettes_Obj_Meta.Name = "EREC_Obj_MetaDefinicio.Felettes_Obj_Meta";
               _Felettes_Obj_Meta.Type = "Guid";            
               _MetaXSD.Name = "EREC_Obj_MetaDefinicio.MetaXSD";
               _MetaXSD.Type = "String";            
               _ImportXML.Name = "EREC_Obj_MetaDefinicio.ImportXML";
               _ImportXML.Type = "String";            
               _EgyebXML.Name = "EREC_Obj_MetaDefinicio.EgyebXML";
               _EgyebXML.Type = "String";            
               _ContentType.Name = "EREC_Obj_MetaDefinicio.ContentType";
               _ContentType.Type = "String";            
               _SPSSzinkronizalt.Name = "EREC_Obj_MetaDefinicio.SPSSzinkronizalt";
               _SPSSzinkronizalt.Type = "Char";            
               _SPS_CTT_Id.Name = "EREC_Obj_MetaDefinicio.SPS_CTT_Id";
               _SPS_CTT_Id.Type = "String";            
               _WorkFlowVezerles.Name = "EREC_Obj_MetaDefinicio.WorkFlowVezerles";
               _WorkFlowVezerles.Type = "Char";            
               _ErvKezd.Name = "EREC_Obj_MetaDefinicio.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Obj_MetaDefinicio.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Objtip_Id property
        /// Definici�Tipus vez�rel:
        /// 0., nincs objtip => KRT_DOKUMENTUMOK -ban t�roljuk a meta-t
        /// 1., nem az �gyiratnyilv�ntart�  alap t�bl�ihoz kapcsol�d�
        /// 2., az �gyiratnyilv�ntart�  alap t�bl�ihoz kapcsol�d�
        ///  Az �gyt�pus al�bont�si m�lys�g�nek megfelel� egyedt�pust azonos�tja. (�gyirat/�gyiratdarab/Irat eset�n az 5 attrib�tum �sszerendel�se,
        /// m�g m�s objtip -n�l az objtip_id_column, �s a Ertek).
        /// 
        /// </summary>
        private Field _Objtip_Id = new Field();

        public Field Objtip_Id
        {
            get { return _Objtip_Id; }
            set { _Objtip_Id = value; }
        }
                          
           
        /// <summary>
        /// Objtip_Id_Column property
        /// NEM HASZN�LT!!! T�bla oszlop objektum Id. ( az el�fordul�s ft�pus besorol� oszlopa,  
        /// (melyhez �ltal�ban kodcsoport - kodt�r �rt�k tartozik, de lehet flag, vagy ak�rmilyen konkr�t �rt�k is)
        /// </summary>
        private Field _Objtip_Id_Column = new Field();

        public Field Objtip_Id_Column
        {
            get { return _Objtip_Id_Column; }
            set { _Objtip_Id_Column = value; }
        }
                          
           
        /// <summary>
        /// ColumnValue property
        /// Az Objtip_Id_Column t�blaoszlop �rt�ke.
        /// </summary>
        private Field _ColumnValue = new Field();

        public Field ColumnValue
        {
            get { return _ColumnValue; }
            set { _ColumnValue = value; }
        }
                          
           
        /// <summary>
        /// DefinicioTipus property
        /// Az irat metadefin�ci� t�pusa. KCS: OBJ_METADEF_TIPUS 
        /// A0 - nincs objtip => KRT_DOKUMENTUMOK -ban CTT-vel hivatkozunk (pl. sz�mla)
        /// B1 - �ltal�nos objektum kieg�sz�t�s (pl. partner)
        /// B2 - �ltal�nos t�pusos objektum kieg�sz�t�s (pl. adott t�pus� partner)
        /// C2 - az �gyirat nyilv�ntart�shoz kapcsol�d� t�pusos objektum kieg�sz�t�s (az IratMetaDefinicio t�telein kereszt�l)
        /// 
        /// </summary>
        private Field _DefinicioTipus = new Field();

        public Field DefinicioTipus
        {
            get { return _DefinicioTipus; }
            set { _DefinicioTipus = value; }
        }
                          
           
        /// <summary>
        /// Felettes_Obj_Meta property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Felettes_Obj_Meta = new Field();

        public Field Felettes_Obj_Meta
        {
            get { return _Felettes_Obj_Meta; }
            set { _Felettes_Obj_Meta = value; }
        }
                          
           
        /// <summary>
        /// MetaXSD property
        ///  2008.03.31: NGYD&PP: Jelenleg NEM HASZN�LT!!
        /// Ez lenne a Meta tipusa, ha pl. InfoPath �rlappal dolgozn�nk.
        /// 
        /// </summary>
        private Field _MetaXSD = new Field();

        public Field MetaXSD
        {
            get { return _MetaXSD; }
            set { _MetaXSD = value; }
        }
                          
           
        /// <summary>
        /// ImportXML property
        ///  2008.03.31: NGYD&PP: Jelenleg NEM HASZN�LT!!
        /// </summary>
        private Field _ImportXML = new Field();

        public Field ImportXML
        {
            get { return _ImportXML; }
            set { _ImportXML = value; }
        }
                          
           
        /// <summary>
        /// EgyebXML property
        ///  2008.03.31: NGYD&PP: Jelenleg NEM HASZN�LT!! V�sztartal�knak: hat�rid�k+feladatok 
        /// </summary>
        private Field _EgyebXML = new Field();

        public Field EgyebXML
        {
            get { return _EgyebXML; }
            set { _EgyebXML = value; }
        }
                          
           
        /// <summary>
        /// ContentType property
        /// A sablonazonos�t� els� eleme (a SharePoint-ban).
        /// </summary>
        private Field _ContentType = new Field();

        public Field ContentType
        {
            get { return _ContentType; }
            set { _ContentType = value; }
        }
                          
           
        /// <summary>
        /// SPSSzinkronizalt property
        /// A t�rgysz� szinkroniz�l�sa az SPS-sel (0/1)
        /// </summary>
        private Field _SPSSzinkronizalt = new Field();

        public Field SPSSzinkronizalt
        {
            get { return _SPSSzinkronizalt; }
            set { _SPSSzinkronizalt = value; }
        }
                          
           
        /// <summary>
        /// SPS_CTT_Id property
        /// A ContentType bels� azonos�t�ja a dokumentumt�rban (SPS-ben).
        /// </summary>
        private Field _SPS_CTT_Id = new Field();

        public Field SPS_CTT_Id
        {
            get { return _SPS_CTT_Id; }
            set { _SPS_CTT_Id = value; }
        }
                          
           
        /// <summary>
        /// WorkFlowVezerles property
        /// Jelzi, hogy a  CTT szerinti dokumentum �llapotvez�relt-e, workflow defin�ci�n kereszt�l (0/1).
        /// </summary>
        private Field _WorkFlowVezerles = new Field();

        public Field WorkFlowVezerles
        {
            get { return _WorkFlowVezerles; }
            set { _WorkFlowVezerles = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}