
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Obj_MetaAdatai eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_Obj_MetaAdataiSearch: BaseSearchObject    {
      public EREC_Obj_MetaAdataiSearch()
      {            
                     _Id.Name = "EREC_Obj_MetaAdatai.Id";
               _Id.Type = "Guid";            
               _Obj_MetaDefinicio_Id.Name = "EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id";
               _Obj_MetaDefinicio_Id.Type = "Guid";            
               _Targyszavak_Id.Name = "EREC_Obj_MetaAdatai.Targyszavak_Id";
               _Targyszavak_Id.Type = "Guid";            
               _AlapertelmezettErtek.Name = "EREC_Obj_MetaAdatai.AlapertelmezettErtek";
               _AlapertelmezettErtek.Type = "String";            
               _Sorszam.Name = "EREC_Obj_MetaAdatai.Sorszam";
               _Sorszam.Type = "Int32";            
               _Opcionalis.Name = "EREC_Obj_MetaAdatai.Opcionalis";
               _Opcionalis.Type = "Char";            
               _Ismetlodo.Name = "EREC_Obj_MetaAdatai.Ismetlodo";
               _Ismetlodo.Type = "Char";            
               _Funkcio.Name = "EREC_Obj_MetaAdatai.Funkcio";
               _Funkcio.Type = "String";            
               _ControlTypeSource.Name = "EREC_Obj_MetaAdatai.ControlTypeSource";
               _ControlTypeSource.Type = "String";            
               _ControlTypeDataSource.Name = "EREC_Obj_MetaAdatai.ControlTypeDataSource";
               _ControlTypeDataSource.Type = "String";            
               _SPSSzinkronizalt.Name = "EREC_Obj_MetaAdatai.SPSSzinkronizalt";
               _SPSSzinkronizalt.Type = "Char";            
               _SPS_Field_Id.Name = "EREC_Obj_MetaAdatai.SPS_Field_Id";
               _SPS_Field_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_Obj_MetaAdatai.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Obj_MetaAdatai.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_MetaDefinicio_Id = new Field();

        public Field Obj_MetaDefinicio_Id
        {
            get { return _Obj_MetaDefinicio_Id; }
            set { _Obj_MetaDefinicio_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Targyszavak_Id = new Field();

        public Field Targyszavak_Id
        {
            get { return _Targyszavak_Id; }
            set { _Targyszavak_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlapertelmezettErtek = new Field();

        public Field AlapertelmezettErtek
        {
            get { return _AlapertelmezettErtek; }
            set { _AlapertelmezettErtek = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Opcionalis = new Field();

        public Field Opcionalis
        {
            get { return _Opcionalis; }
            set { _Opcionalis = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Ismetlodo = new Field();

        public Field Ismetlodo
        {
            get { return _Ismetlodo; }
            set { _Ismetlodo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Funkcio = new Field();

        public Field Funkcio
        {
            get { return _Funkcio; }
            set { _Funkcio = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ControlTypeSource = new Field();

        public Field ControlTypeSource
        {
            get { return _ControlTypeSource; }
            set { _ControlTypeSource = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ControlTypeDataSource = new Field();

        public Field ControlTypeDataSource
        {
            get { return _ControlTypeDataSource; }
            set { _ControlTypeDataSource = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SPSSzinkronizalt = new Field();

        public Field SPSSzinkronizalt
        {
            get { return _SPSSzinkronizalt; }
            set { _SPSSzinkronizalt = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SPS_Field_Id = new Field();

        public Field SPS_Field_Id
        {
            get { return _SPS_Field_Id; }
            set { _SPS_Field_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}