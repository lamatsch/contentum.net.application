
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Obj_TargySzavai eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class EREC_Obj_TargySzavaiSearch
    {
      public EREC_Obj_TargySzavaiSearch()
      {               _Id.Name = "EREC_Obj_TargySzavai.Id";
               _Id.Type = "Guid";            
               _TargySzo_Id.Name = "EREC_Obj_TargySzavai.TargySzo_Id";
               _TargySzo_Id.Type = "Guid";            
               _ObjTip_Id_AdatElem.Name = "EREC_Obj_TargySzavai.ObjTip_Id_AdatElem";
               _ObjTip_Id_AdatElem.Type = "Guid";            
               _Obj_Id.Name = "EREC_Obj_TargySzavai.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _Tipus.Name = "EREC_Obj_TargySzavai.Tipus";
               _Tipus.Type = "Char";            
               _ErtekTipus_Id.Name = "EREC_Obj_TargySzavai.ErtekTipus_Id";
               _ErtekTipus_Id.Type = "Guid";            
               _Ertek.Name = "EREC_Obj_TargySzavai.Ertek";
               _Ertek.Type = "String";            
               _xml.Name = "EREC_Obj_TargySzavai.xml";
               _xml.Type = "String";            
               _ErvKezd.Name = "EREC_Obj_TargySzavai.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Obj_TargySzavai.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _TargySzo_Id = new Field();

        public Field TargySzo_Id
        {
            get { return _TargySzo_Id; }
            set { _TargySzo_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id_AdatElem = new Field();

        public Field ObjTip_Id_AdatElem
        {
            get { return _ObjTip_Id_AdatElem; }
            set { _ObjTip_Id_AdatElem = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErtekTipus_Id = new Field();

        public Field ErtekTipus_Id
        {
            get { return _ErtekTipus_Id; }
            set { _ErtekTipus_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Ertek = new Field();

        public Field Ertek
        {
            get { return _Ertek; }
            set { _Ertek = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _xml = new Field();

        public Field xml
        {
            get { return _xml; }
            set { _xml = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}