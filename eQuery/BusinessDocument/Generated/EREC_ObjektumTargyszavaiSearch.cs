
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_ObjektumTargyszavai eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_ObjektumTargyszavaiSearch: BaseSearchObject    {
      public EREC_ObjektumTargyszavaiSearch()
      {            
                     _Id.Name = "EREC_ObjektumTargyszavai.Id";
               _Id.Type = "Guid";            
               _Targyszo_Id.Name = "EREC_ObjektumTargyszavai.Targyszo_Id";
               _Targyszo_Id.Type = "Guid";            
               _Obj_Metaadatai_Id.Name = "EREC_ObjektumTargyszavai.Obj_Metaadatai_Id";
               _Obj_Metaadatai_Id.Type = "Guid";            
               _Obj_Id.Name = "EREC_ObjektumTargyszavai.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id.Name = "EREC_ObjektumTargyszavai.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Targyszo.Name = "EREC_ObjektumTargyszavai.Targyszo";
               _Targyszo.Type = "FTSString";            
               _Forras.Name = "EREC_ObjektumTargyszavai.Forras";
               _Forras.Type = "String";            
               _SPSSzinkronizalt.Name = "EREC_ObjektumTargyszavai.SPSSzinkronizalt";
               _SPSSzinkronizalt.Type = "Char";            
               _Ertek.Name = "EREC_ObjektumTargyszavai.Ertek";
               _Ertek.Type = "FTSString";            
               _Sorszam.Name = "EREC_ObjektumTargyszavai.Sorszam";
               _Sorszam.Type = "Int32";            
               _Targyszo_XML.Name = "EREC_ObjektumTargyszavai.Targyszo_XML";
               _Targyszo_XML.Type = "String";            
               _Targyszo_XML_Ervenyes.Name = "EREC_ObjektumTargyszavai.Targyszo_XML_Ervenyes";
               _Targyszo_XML_Ervenyes.Type = "Char";            
               _ErvKezd.Name = "EREC_ObjektumTargyszavai.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_ObjektumTargyszavai.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Targyszo_Id = new Field();

        public Field Targyszo_Id
        {
            get { return _Targyszo_Id; }
            set { _Targyszo_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Metaadatai_Id = new Field();

        public Field Obj_Metaadatai_Id
        {
            get { return _Obj_Metaadatai_Id; }
            set { _Obj_Metaadatai_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Targyszo = new Field();

        public Field Targyszo
        {
            get { return _Targyszo; }
            set { _Targyszo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Forras = new Field();

        public Field Forras
        {
            get { return _Forras; }
            set { _Forras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SPSSzinkronizalt = new Field();

        public Field SPSSzinkronizalt
        {
            get { return _SPSSzinkronizalt; }
            set { _SPSSzinkronizalt = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Ertek = new Field();

        public Field Ertek
        {
            get { return _Ertek; }
            set { _Ertek = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Targyszo_XML = new Field();

        public Field Targyszo_XML
        {
            get { return _Targyszo_XML; }
            set { _Targyszo_XML = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Targyszo_XML_Ervenyes = new Field();

        public Field Targyszo_XML_Ervenyes
        {
            get { return _Targyszo_XML_Ervenyes; }
            set { _Targyszo_XML_Ervenyes = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}