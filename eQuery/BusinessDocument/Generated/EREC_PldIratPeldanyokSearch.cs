
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_PldIratPeldanyok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_PldIratPeldanyokSearch: BaseSearchObject    {
      public EREC_PldIratPeldanyokSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "EREC_PldIratPeldanyok.Id";
               _Id.Type = "Guid";            
               _IraIrat_Id.Name = "EREC_PldIratPeldanyok.IraIrat_Id";
               _IraIrat_Id.Type = "Guid";            
               _UgyUgyirat_Id_Kulso.Name = "EREC_PldIratPeldanyok.UgyUgyirat_Id_Kulso";
               _UgyUgyirat_Id_Kulso.Type = "Guid";            
               _Sorszam.Name = "EREC_PldIratPeldanyok.Sorszam";
               _Sorszam.Type = "Int32";            
               _SztornirozasDat.Name = "EREC_PldIratPeldanyok.SztornirozasDat";
               _SztornirozasDat.Type = "DateTime";            
               _AtvetelDatuma.Name = "EREC_PldIratPeldanyok.AtvetelDatuma";
               _AtvetelDatuma.Type = "DateTime";            
               _Eredet.Name = "EREC_PldIratPeldanyok.Eredet";
               _Eredet.Type = "String";            
               _KuldesMod.Name = "EREC_PldIratPeldanyok.KuldesMod";
               _KuldesMod.Type = "String";            
               _Ragszam.Name = "EREC_PldIratPeldanyok.Ragszam";
               _Ragszam.Type = "String";            
               _UgyintezesModja.Name = "EREC_PldIratPeldanyok.UgyintezesModja";
               _UgyintezesModja.Type = "String";            
               _VisszaerkezesiHatarido.Name = "EREC_PldIratPeldanyok.VisszaerkezesiHatarido";
               _VisszaerkezesiHatarido.Type = "DateTime";            
               _Visszavarolag.Name = "EREC_PldIratPeldanyok.Visszavarolag";
               _Visszavarolag.Type = "String";            
               _VisszaerkezesDatuma.Name = "EREC_PldIratPeldanyok.VisszaerkezesDatuma";
               _VisszaerkezesDatuma.Type = "DateTime";            
               _Cim_id_Cimzett.Name = "EREC_PldIratPeldanyok.Cim_id_Cimzett";
               _Cim_id_Cimzett.Type = "Guid";            
               _Partner_Id_Cimzett.Name = "EREC_PldIratPeldanyok.Partner_Id_Cimzett";
               _Partner_Id_Cimzett.Type = "Guid";   
			   _Partner_Id_CimzettKapcsolt.Name = "EREC_PldIratPeldanyok.Partner_Id_CimzettKapcsolt";
               _Partner_Id_CimzettKapcsolt.Type = "Guid";              			   
               _Csoport_Id_Felelos.Name = "EREC_PldIratPeldanyok.Csoport_Id_Felelos";
               _Csoport_Id_Felelos.Type = "Guid";            
               _FelhasznaloCsoport_Id_Orzo.Name = "EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo";
               _FelhasznaloCsoport_Id_Orzo.Type = "Guid";            
               _Csoport_Id_Felelos_Elozo.Name = "EREC_PldIratPeldanyok.Csoport_Id_Felelos_Elozo";
               _Csoport_Id_Felelos_Elozo.Type = "Guid";            
               _CimSTR_Cimzett.Name = "EREC_PldIratPeldanyok.CimSTR_Cimzett";
               _CimSTR_Cimzett.Type = "String";            
               _NevSTR_Cimzett.Name = "EREC_PldIratPeldanyok.NevSTR_Cimzett";
               _NevSTR_Cimzett.Type = "String";            
               _Tovabbito.Name = "EREC_PldIratPeldanyok.Tovabbito";
               _Tovabbito.Type = "String";            
               _IraIrat_Id_Kapcsolt.Name = "EREC_PldIratPeldanyok.IraIrat_Id_Kapcsolt";
               _IraIrat_Id_Kapcsolt.Type = "Guid";            
               _IrattariHely.Name = "EREC_PldIratPeldanyok.IrattariHely";
               _IrattariHely.Type = "String";            
               _Gener_Id.Name = "EREC_PldIratPeldanyok.Gener_Id";
               _Gener_Id.Type = "Guid";            
               _PostazasDatuma.Name = "EREC_PldIratPeldanyok.PostazasDatuma";
               _PostazasDatuma.Type = "DateTime";            
               _BarCode.Name = "EREC_PldIratPeldanyok.BarCode";
               _BarCode.Type = "String";            
               _Allapot.Name = "EREC_PldIratPeldanyok.Allapot";
               _Allapot.Type = "String";            
               _Azonosito.Name = "EREC_PldIratPeldanyok.Azonosito";
               _Azonosito.Type = "String";            
               _Kovetkezo_Felelos_Id.Name = "EREC_PldIratPeldanyok.Kovetkezo_Felelos_Id";
               _Kovetkezo_Felelos_Id.Type = "Guid";            
               _Elektronikus_Kezbesitesi_Allap.Name = "EREC_PldIratPeldanyok.Elektronikus_Kezbesitesi_Allap";
               _Elektronikus_Kezbesitesi_Allap.Type = "String";            
               _Kovetkezo_Orzo_Id.Name = "EREC_PldIratPeldanyok.Kovetkezo_Orzo_Id";
               _Kovetkezo_Orzo_Id.Type = "Guid";            
               _Fizikai_Kezbesitesi_Allapot.Name = "EREC_PldIratPeldanyok.Fizikai_Kezbesitesi_Allapot";
               _Fizikai_Kezbesitesi_Allapot.Type = "String";            
               _TovabbitasAlattAllapot.Name = "EREC_PldIratPeldanyok.TovabbitasAlattAllapot";
               _TovabbitasAlattAllapot.Type = "String";            
               _PostazasAllapot.Name = "EREC_PldIratPeldanyok.PostazasAllapot";
               _PostazasAllapot.Type = "String";            
               _ValaszElektronikus.Name = "EREC_PldIratPeldanyok.ValaszElektronikus";
               _ValaszElektronikus.Type = "Char";            
               _SelejtezesDat.Name = "EREC_PldIratPeldanyok.SelejtezesDat";
               _SelejtezesDat.Type = "DateTime";            
               _FelhCsoport_Id_Selejtezo.Name = "EREC_PldIratPeldanyok.FelhCsoport_Id_Selejtezo";
               _FelhCsoport_Id_Selejtezo.Type = "Guid";            
               _LeveltariAtvevoNeve.Name = "EREC_PldIratPeldanyok.LeveltariAtvevoNeve";
               _LeveltariAtvevoNeve.Type = "String";                           
               _IrattarId.Name = "EREC_PldIratPeldanyok.IrattarId";
               _IrattarId.Type = "Guid";            
               _ErvKezd.Name = "EREC_PldIratPeldanyok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_PldIratPeldanyok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// IraIrat_Id property
        /// 
        /// </summary>
        private Field _IraIrat_Id = new Field();

        public Field IraIrat_Id
        {
            get { return _IraIrat_Id; }
            set { _IraIrat_Id = value; }
        }
                          
           
        /// <summary>
        /// UgyUgyirat_Id_Kulso property
        /// Az iratp�ld�nyhoz tartoz� legf�ls�bb szint� �gyirat azonos�t�ja.
        /// </summary>
        private Field _UgyUgyirat_Id_Kulso = new Field();

        public Field UgyUgyirat_Id_Kulso
        {
            get { return _UgyUgyirat_Id_Kulso; }
            set { _UgyUgyirat_Id_Kulso = value; }
        }
                          
           
        /// <summary>
        /// Sorszam property
        /// Iratp�ld�ny sorsz�ma
        /// </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }
                          
           
        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }
                          
           
        /// <summary>
        /// AtvetelDatuma property
        /// Iratp�ld�ny �tv�tel d�tuma
        /// </summary>
        private Field _AtvetelDatuma = new Field();

        public Field AtvetelDatuma
        {
            get { return _AtvetelDatuma; }
            set { _AtvetelDatuma = value; }
        }
                          
           
        /// <summary>
        /// Eredet property
        /// KCS: IRAT_FAJTA 
        /// (eredeti, szign�lt iratt�ri p�ld�ny, m�solat, m�sodlat, ...)
        /// </summary>
        private Field _Eredet = new Field();

        public Field Eredet
        {
            get { return _Eredet; }
            set { _Eredet = value; }
        }
                          
           
        /// <summary>
        /// KuldesMod property
        /// M�OSOSITAND�, KCS:KULDEMENY_KULDES_MODJA, pap�ros v. elekronikus �ton k�ri a v�laszt.
        /// 
        /// </summary>
        private Field _KuldesMod = new Field();

        public Field KuldesMod
        {
            get { return _KuldesMod; }
            set { _KuldesMod = value; }
        }
                          
           
        /// <summary>
        /// Ragszam property
        /// Nem haszn�lt! (Postai ragsz�m.)
        /// </summary>
        private Field _Ragszam = new Field();

        public Field Ragszam
        {
            get { return _Ragszam; }
            set { _Ragszam = value; }
        }
                          
           
        /// <summary>
        /// UgyintezesModja property
        /// KCS: 'UGYINTEZES_ALAPJA'
        /// 0 - Hagyom�nyos, pap�r
        /// 1 - Elektronikus (nincs pap�r)
        /// </summary>
        private Field _UgyintezesModja = new Field();

        public Field UgyintezesModja
        {
            get { return _UgyintezesModja; }
            set { _UgyintezesModja = value; }
        }
                          
           
        /// <summary>
        /// VisszaerkezesiHatarido property
        /// V�lasz vissza�rkez�si hat�rideje
        /// </summary>
        private Field _VisszaerkezesiHatarido = new Field();

        public Field VisszaerkezesiHatarido
        {
            get { return _VisszaerkezesiHatarido; }
            set { _VisszaerkezesiHatarido = value; }
        }
                          
           
        /// <summary>
        /// Visszavarolag property
        /// KCS: VISSZAVAROLAG 
        /// (visszav�r�lag t�j�koztat�sra, ...). K�perny�n: Elv�r�s n�v alatt.
        /// 
        /// </summary>
        private Field _Visszavarolag = new Field();

        public Field Visszavarolag
        {
            get { return _Visszavarolag; }
            set { _Visszavarolag = value; }
        }
                          
           
        /// <summary>
        /// VisszaerkezesDatuma property
        /// V�lasz vissz�rkez�si d�tuma
        /// </summary>
        private Field _VisszaerkezesDatuma = new Field();

        public Field VisszaerkezesDatuma
        {
            get { return _VisszaerkezesDatuma; }
            set { _VisszaerkezesDatuma = value; }
        }
                          
           
        /// <summary>
        /// Cim_id_Cimzett property
        /// C�mzett Partner Cim_Id 
        /// </summary>
        private Field _Cim_id_Cimzett = new Field();

        public Field Cim_id_Cimzett
        {
            get { return _Cim_id_Cimzett; }
            set { _Cim_id_Cimzett = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id_Cimzett property
        /// Bek�ld� Id -ja...
        /// </summary>
        private Field _Partner_Id_Cimzett = new Field();

        public Field Partner_Id_Cimzett
        {
            get { return _Partner_Id_Cimzett; }
            set { _Partner_Id_Cimzett = value; }
        }
                          
        /// <summary>
        /// Partner_Id_CimzettKapcsolt property
        /// 
        /// </summary>
        private Field _Partner_Id_CimzettKapcsolt = new Field();

        public Field Partner_Id_CimzettKapcsolt
        {
            get { return _Partner_Id_CimzettKapcsolt; }
            set { _Partner_Id_CimzettKapcsolt = value; }
        }
		
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Felel�s felhaszn�l� Id
        /// </summary>
        private Field _Csoport_Id_Felelos = new Field();

        public Field Csoport_Id_Felelos
        {
            get { return _Csoport_Id_Felelos; }
            set { _Csoport_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Orzo property
        /// Iratpld �rz� Id
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Orzo = new Field();

        public Field FelhasznaloCsoport_Id_Orzo
        {
            get { return _FelhasznaloCsoport_Id_Orzo; }
            set { _FelhasznaloCsoport_Id_Orzo = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Felelos_Elozo property
        /// Elozo felelos CSOPORT Id
        /// </summary>
        private Field _Csoport_Id_Felelos_Elozo = new Field();

        public Field Csoport_Id_Felelos_Elozo
        {
            get { return _Csoport_Id_Felelos_Elozo; }
            set { _Csoport_Id_Felelos_Elozo = value; }
        }
                          
           
        /// <summary>
        /// CimSTR_Cimzett property
        /// C�mzett sz�veges c�me
        /// </summary>
        private Field _CimSTR_Cimzett = new Field();

        public Field CimSTR_Cimzett
        {
            get { return _CimSTR_Cimzett; }
            set { _CimSTR_Cimzett = value; }
        }
                          
           
        /// <summary>
        /// NevSTR_Cimzett property
        /// C�mzett teljes neve
        /// </summary>
        private Field _NevSTR_Cimzett = new Field();

        public Field NevSTR_Cimzett
        {
            get { return _NevSTR_Cimzett; }
            set { _NevSTR_Cimzett = value; }
        }
                          
           
        /// <summary>
        /// Tovabbito property
        /// K�s�bb nem kell!! KCS: TOV�BB�T� SZERVEZET (Posta, Fut�rszolg�lat...).
        /// </summary>
        private Field _Tovabbito = new Field();

        public Field Tovabbito
        {
            get { return _Tovabbito; }
            set { _Tovabbito = value; }
        }
                          
           
        /// <summary>
        /// IraIrat_Id_Kapcsolt property
        /// Irat Id
        /// </summary>
        private Field _IraIrat_Id_Kapcsolt = new Field();

        public Field IraIrat_Id_Kapcsolt
        {
            get { return _IraIrat_Id_Kapcsolt; }
            set { _IraIrat_Id_Kapcsolt = value; }
        }
                          
           
        /// <summary>
        /// IrattariHely property
        /// Iratt�ri hely, amennyiben iratt�roz�sra ker�lt.
        /// </summary>
        private Field _IrattariHely = new Field();

        public Field IrattariHely
        {
            get { return _IrattariHely; }
            set { _IrattariHely = value; }
        }
                          
           
        /// <summary>
        /// Gener_Id property
        /// ??
        /// </summary>
        private Field _Gener_Id = new Field();

        public Field Gener_Id
        {
            get { return _Gener_Id; }
            set { _Gener_Id = value; }
        }
                          
           
        /// <summary>
        /// PostazasDatuma property
        /// NEM kell!!! P�ld�ny post�z�si d�tuma (kimen� pld elest�n)
        /// </summary>
        private Field _PostazasDatuma = new Field();

        public Field PostazasDatuma
        {
            get { return _PostazasDatuma; }
            set { _PostazasDatuma = value; }
        }
                          
           
        /// <summary>
        /// BarCode property
        /// Vonalk�d �rt�ke
        /// </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// KCS: IRATPELDANY_ALLAPOT
        /// 0 - Megnyitott (munkap�ld�ny)
        /// 2 - Befagyasztott (munkap�ld�ny)
        /// 04 - Iktatott
        /// 30 - Kiadm�nyozott
        /// 31 - Lez�rt jegyz�ken
        /// 32 - Jegyz�kre helyezett
        /// 40 - C�mzett �tvette
        /// 45 - Post�zott
        /// 50 - Tov�bb�t�s alatt
        /// 60 - J�v�hagy�s alatt
        /// 61 - �jrak�ldend�
        /// 63 - Kimen� k�ldem�nyben
        /// 64 - Expedi�lt
        /// 90 - Sztorn�zott
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Azonosito property
        /// Iktat�sz�m sz�vegesen.
        /// </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// Kovetkezo_Felelos_Id property
        /// 
        /// </summary>
        private Field _Kovetkezo_Felelos_Id = new Field();

        public Field Kovetkezo_Felelos_Id
        {
            get { return _Kovetkezo_Felelos_Id; }
            set { _Kovetkezo_Felelos_Id = value; }
        }
                          
           
        /// <summary>
        /// Elektronikus_Kezbesitesi_Allap property
        /// 
        /// </summary>
        private Field _Elektronikus_Kezbesitesi_Allap = new Field();

        public Field Elektronikus_Kezbesitesi_Allap
        {
            get { return _Elektronikus_Kezbesitesi_Allap; }
            set { _Elektronikus_Kezbesitesi_Allap = value; }
        }
                          
           
        /// <summary>
        /// Kovetkezo_Orzo_Id property
        /// 
        /// </summary>
        private Field _Kovetkezo_Orzo_Id = new Field();

        public Field Kovetkezo_Orzo_Id
        {
            get { return _Kovetkezo_Orzo_Id; }
            set { _Kovetkezo_Orzo_Id = value; }
        }
                          
           
        /// <summary>
        /// Fizikai_Kezbesitesi_Allapot property
        /// KCS: ??
        /// </summary>
        private Field _Fizikai_Kezbesitesi_Allapot = new Field();

        public Field Fizikai_Kezbesitesi_Allapot
        {
            get { return _Fizikai_Kezbesitesi_Allapot; }
            set { _Fizikai_Kezbesitesi_Allapot = value; }
        }
                          
           
        /// <summary>
        /// TovabbitasAlattAllapot property
        /// KCS: IRATPELDANY_ALLAPOT
        /// </summary>
        private Field _TovabbitasAlattAllapot = new Field();

        public Field TovabbitasAlattAllapot
        {
            get { return _TovabbitasAlattAllapot; }
            set { _TovabbitasAlattAllapot = value; }
        }
                          
           
        /// <summary>
        /// PostazasAllapot property
        /// KCS: IRATPELDANY_POSTAZAS_ALLAPOT
        /// </summary>
        private Field _PostazasAllapot = new Field();

        public Field PostazasAllapot
        {
            get { return _PostazasAllapot; }
            set { _PostazasAllapot = value; }
        }
                          
           
        /// <summary>
        /// ValaszElektronikus property
        /// 0-nem, 1-igen. 2. mindegy. 
        ///  A partnernek joga van meg,hat�rozni, hogy a v�laszt elektronikusan k�ri e.
        /// </summary>
        private Field _ValaszElektronikus = new Field();

        public Field ValaszElektronikus
        {
            get { return _ValaszElektronikus; }
            set { _ValaszElektronikus = value; }
        }
                          
           
        /// <summary>
        /// SelejtezesDat property
        /// Selejtez�s v. lev�lt�rba ad�s d�tuma
        /// </summary>
        private Field _SelejtezesDat = new Field();

        public Field SelejtezesDat
        {
            get { return _SelejtezesDat; }
            set { _SelejtezesDat = value; }
        }
                          
           
        /// <summary>
        /// FelhCsoport_Id_Selejtezo property
        /// Selejtez� v. lev�lt�rba ad� felh. csoport Id-je
        /// </summary>
        private Field _FelhCsoport_Id_Selejtezo = new Field();

        public Field FelhCsoport_Id_Selejtezo
        {
            get { return _FelhCsoport_Id_Selejtezo; }
            set { _FelhCsoport_Id_Selejtezo = value; }
        }
                          
           
        /// <summary>
        /// LeveltariAtvevoNeve property
        /// Lev�lt�ri �tvev� neve
        /// </summary>
        private Field _LeveltariAtvevoNeve = new Field();

        public Field LeveltariAtvevoNeve
        {
            get { return _LeveltariAtvevoNeve; }
            set { _LeveltariAtvevoNeve = value; }
        }                 
           
        /// <summary>
        /// IrattarId property
        /// Selejtezo v. lev�lt�rba ad� felh. csoport Id-je
        /// </summary>
        private Field _IrattarId = new Field();

        public Field IrattarId
        {
            get { return _IrattarId; }
            set { _IrattarId = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}