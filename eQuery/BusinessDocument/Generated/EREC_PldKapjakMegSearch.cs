
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_PldKapjakMeg eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_PldKapjakMegSearch: BaseSearchObject    {
      public EREC_PldKapjakMegSearch()
      {            
                     _Id.Name = "EREC_PldKapjakMeg.Id";
               _Id.Type = "Guid";            
               _PldIratPeldany_Id.Name = "EREC_PldKapjakMeg.PldIratPeldany_Id";
               _PldIratPeldany_Id.Type = "Guid";            
               _Csoport_Id.Name = "EREC_PldKapjakMeg.Csoport_Id";
               _Csoport_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_PldKapjakMeg.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_PldKapjakMeg.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _PldIratPeldany_Id = new Field();

        public Field PldIratPeldany_Id
        {
            get { return _PldIratPeldany_Id; }
            set { _PldIratPeldany_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id = new Field();

        public Field Csoport_Id
        {
            get { return _Csoport_Id; }
            set { _Csoport_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}