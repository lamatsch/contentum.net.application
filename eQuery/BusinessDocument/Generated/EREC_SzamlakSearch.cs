
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_Szamlak eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_SzamlakSearch: BaseSearchObject    {
      public EREC_SzamlakSearch()
      {
          Init_ManualFields();
               
                     _Id.Name = "EREC_Szamlak.Id";
               _Id.Type = "Guid";            
               _KuldKuldemeny_Id.Name = "EREC_Szamlak.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _Dokumentum_Id.Name = "EREC_Szamlak.Dokumentum_Id";
               _Dokumentum_Id.Type = "Guid";            
               _Partner_Id_Szallito.Name = "EREC_Szamlak.Partner_Id_Szallito";
               _Partner_Id_Szallito.Type = "Guid";            
               _Cim_Id_Szallito.Name = "EREC_Szamlak.Cim_Id_Szallito";
               _Cim_Id_Szallito.Type = "Guid";            
               _NevSTR_Szallito.Name = "EREC_Szamlak.NevSTR_Szallito";
               _NevSTR_Szallito.Type = "FTSString";            
               _CimSTR_Szallito.Name = "EREC_Szamlak.CimSTR_Szallito";
               _CimSTR_Szallito.Type = "String";            
               _Bankszamlaszam_Id.Name = "EREC_Szamlak.Bankszamlaszam_Id";
               _Bankszamlaszam_Id.Type = "Guid";            
               _SzamlaSorszam.Name = "EREC_Szamlak.SzamlaSorszam";
               _SzamlaSorszam.Type = "String";            
               _FizetesiMod.Name = "EREC_Szamlak.FizetesiMod";
               _FizetesiMod.Type = "String";            
               _TeljesitesDatuma.Name = "EREC_Szamlak.TeljesitesDatuma";
               _TeljesitesDatuma.Type = "DateTime";            
               _BizonylatDatuma.Name = "EREC_Szamlak.BizonylatDatuma";
               _BizonylatDatuma.Type = "DateTime";            
               _FizetesiHatarido.Name = "EREC_Szamlak.FizetesiHatarido";
               _FizetesiHatarido.Type = "DateTime";            
               _DevizaKod.Name = "EREC_Szamlak.DevizaKod";
               _DevizaKod.Type = "String";            
               _VisszakuldesDatuma.Name = "EREC_Szamlak.VisszakuldesDatuma";
               _VisszakuldesDatuma.Type = "DateTime";            
               _EllenorzoOsszeg.Name = "EREC_Szamlak.EllenorzoOsszeg";
               _EllenorzoOsszeg.Type = "Double";            
               _PIRAllapot.Name = "EREC_Szamlak.PIRAllapot";
               _PIRAllapot.Type = "Char";            
               _ErvKezd.Name = "EREC_Szamlak.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_Szamlak.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// K�ldem�ny Id
        /// </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// Dokumentum_Id property
        /// Dokumentum Id
        /// </summary>
        private Field _Dokumentum_Id = new Field();

        public Field Dokumentum_Id
        {
            get { return _Dokumentum_Id; }
            set { _Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id_Szallito property
        /// 
        /// </summary>
        private Field _Partner_Id_Szallito = new Field();

        public Field Partner_Id_Szallito
        {
            get { return _Partner_Id_Szallito; }
            set { _Partner_Id_Szallito = value; }
        }
                          
           
        /// <summary>
        /// Cim_Id_Szallito property
        /// 
        /// </summary>
        private Field _Cim_Id_Szallito = new Field();

        public Field Cim_Id_Szallito
        {
            get { return _Cim_Id_Szallito; }
            set { _Cim_Id_Szallito = value; }
        }
                          
           
        /// <summary>
        /// NevSTR_Szallito property
        /// Bek�ld� neve sz�vegesen
        /// </summary>
        private Field _NevSTR_Szallito = new Field();

        public Field NevSTR_Szallito
        {
            get { return _NevSTR_Szallito; }
            set { _NevSTR_Szallito = value; }
        }
                          
           
        /// <summary>
        /// CimSTR_Szallito property
        /// Bek�ld� c�me sz�vegesen
        /// </summary>
        private Field _CimSTR_Szallito = new Field();

        public Field CimSTR_Szallito
        {
            get { return _CimSTR_Szallito; }
            set { _CimSTR_Szallito = value; }
        }
                          
           
        /// <summary>
        /// Bankszamlaszam_Id property
        /// Az �tutal�shoz megadott banksz�mlasz�mot meghat�roz� azonos�t�
        /// </summary>
        private Field _Bankszamlaszam_Id = new Field();

        public Field Bankszamlaszam_Id
        {
            get { return _Bankszamlaszam_Id; }
            set { _Bankszamlaszam_Id = value; }
        }
                          
           
        /// <summary>
        /// SzamlaSorszam property
        /// 
        /// </summary>
        private Field _SzamlaSorszam = new Field();

        public Field SzamlaSorszam
        {
            get { return _SzamlaSorszam; }
            set { _SzamlaSorszam = value; }
        }
                          
           
        /// <summary>
        /// FizetesiMod property
        /// 
        /// </summary>
        private Field _FizetesiMod = new Field();

        public Field FizetesiMod
        {
            get { return _FizetesiMod; }
            set { _FizetesiMod = value; }
        }
                          
           
        /// <summary>
        /// TeljesitesDatuma property
        /// 
        /// </summary>
        private Field _TeljesitesDatuma = new Field();

        public Field TeljesitesDatuma
        {
            get { return _TeljesitesDatuma; }
            set { _TeljesitesDatuma = value; }
        }
                          
           
        /// <summary>
        /// BizonylatDatuma property
        /// 
        /// </summary>
        private Field _BizonylatDatuma = new Field();

        public Field BizonylatDatuma
        {
            get { return _BizonylatDatuma; }
            set { _BizonylatDatuma = value; }
        }
                          
           
        /// <summary>
        /// FizetesiHatarido property
        /// 
        /// </summary>
        private Field _FizetesiHatarido = new Field();

        public Field FizetesiHatarido
        {
            get { return _FizetesiHatarido; }
            set { _FizetesiHatarido = value; }
        }
                          
           
        /// <summary>
        /// DevizaKod property
        /// 
        /// </summary>
        private Field _DevizaKod = new Field();

        public Field DevizaKod
        {
            get { return _DevizaKod; }
            set { _DevizaKod = value; }
        }
                          
           
        /// <summary>
        /// VisszakuldesDatuma property
        /// 
        /// </summary>
        private Field _VisszakuldesDatuma = new Field();

        public Field VisszakuldesDatuma
        {
            get { return _VisszakuldesDatuma; }
            set { _VisszakuldesDatuma = value; }
        }
                          
           
        /// <summary>
        /// EllenorzoOsszeg property
        /// 
        /// </summary>
        private Field _EllenorzoOsszeg = new Field();

        public Field EllenorzoOsszeg
        {
            get { return _EllenorzoOsszeg; }
            set { _EllenorzoOsszeg = value; }
        }
                          
           
        /// <summary>
        /// PIRAllapot property
        /// 
        /// </summary>
        private Field _PIRAllapot = new Field();

        public Field PIRAllapot
        {
            get { return _PIRAllapot; }
            set { _PIRAllapot = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }

        // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
        /// <summary>
        /// VevoBesorolas property
        /// Sz�mla �tad�s�hoz haszn�lhat� csoportos�t� mez�
        /// </summary>
        private Field _VevoBesorolas = new Field();

        public Field VevoBesorolas
        {
            get { return _VevoBesorolas; }
            set { _VevoBesorolas = value; }
        }
    }

}