
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_SzignalasiJegyzekek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_SzignalasiJegyzekekSearch: BaseSearchObject    {
      public EREC_SzignalasiJegyzekekSearch()
      {            
                     _Id.Name = "EREC_SzignalasiJegyzekek.Id";
               _Id.Type = "Guid";            
               _UgykorTargykor_Id.Name = "EREC_SzignalasiJegyzekek.UgykorTargykor_Id";
               _UgykorTargykor_Id.Type = "Guid";            
               _Csoport_Id_Felelos.Name = "EREC_SzignalasiJegyzekek.Csoport_Id_Felelos";
               _Csoport_Id_Felelos.Type = "Guid";            
               _SzignalasTipusa.Name = "EREC_SzignalasiJegyzekek.SzignalasTipusa";
               _SzignalasTipusa.Type = "String";            
               _SzignalasiKotelezettseg.Name = "EREC_SzignalasiJegyzekek.SzignalasiKotelezettseg";
               _SzignalasiKotelezettseg.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgykorTargykor_Id = new Field();

        public Field UgykorTargykor_Id
        {
            get { return _UgykorTargykor_Id; }
            set { _UgykorTargykor_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Felelos = new Field();

        public Field Csoport_Id_Felelos
        {
            get { return _Csoport_Id_Felelos; }
            set { _Csoport_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SzignalasTipusa = new Field();

        public Field SzignalasTipusa
        {
            get { return _SzignalasTipusa; }
            set { _SzignalasTipusa = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SzignalasiKotelezettseg = new Field();

        public Field SzignalasiKotelezettseg
        {
            get { return _SzignalasiKotelezettseg; }
            set { _SzignalasiKotelezettseg = value; }
        }
                              }

}