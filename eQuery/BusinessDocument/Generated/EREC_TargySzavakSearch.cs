
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_TargySzavak eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_TargySzavakSearch: BaseSearchObject    {
      public EREC_TargySzavakSearch()
      {            
                     _Id.Name = "EREC_TargySzavak.Id";
               _Id.Type = "Guid";            
               _Org.Name = "EREC_TargySzavak.Org";
               _Org.Type = "Guid";            
               _Tipus.Name = "EREC_TargySzavak.Tipus";
               _Tipus.Type = "Char";            
               _TargySzavak.Name = "EREC_TargySzavak.TargySzavak";
               _TargySzavak.Type = "String";            
               _AlapertelmezettErtek.Name = "EREC_TargySzavak.AlapertelmezettErtek";
               _AlapertelmezettErtek.Type = "String";            
               _BelsoAzonosito.Name = "EREC_TargySzavak.BelsoAzonosito";
               _BelsoAzonosito.Type = "String";            
               _SPSSzinkronizalt.Name = "EREC_TargySzavak.SPSSzinkronizalt";
               _SPSSzinkronizalt.Type = "Char";            
               _SPS_Field_Id.Name = "EREC_TargySzavak.SPS_Field_Id";
               _SPS_Field_Id.Type = "Guid";            
               _Csoport_Id_Tulaj.Name = "EREC_TargySzavak.Csoport_Id_Tulaj";
               _Csoport_Id_Tulaj.Type = "Guid";            
               _RegExp.Name = "EREC_TargySzavak.RegExp";
               _RegExp.Type = "String";            
               _ToolTip.Name = "EREC_TargySzavak.ToolTip";
               _ToolTip.Type = "String";            
               _ControlTypeSource.Name = "EREC_TargySzavak.ControlTypeSource";
               _ControlTypeSource.Type = "String";            
               _ControlTypeDataSource.Name = "EREC_TargySzavak.ControlTypeDataSource";
               _ControlTypeDataSource.Type = "String";            
               _Targyszo_Id_Parent.Name = "EREC_TargySzavak.Targyszo_Id_Parent";
               _Targyszo_Id_Parent.Type = "Guid";            
               _XSD.Name = "EREC_TargySzavak.XSD";
               _XSD.Type = "String";            
               _ErvKezd.Name = "EREC_TargySzavak.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_TargySzavak.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Tipus property
        /// Review: T�rgysz� �rt�kkel/�rt�k n�lk�l (1/0).
        /// (Pl. TAJ Sz�m + �rt�k)
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// TargySzavak property
        /// ez nem felt�tlen egyetlen sz�!!
        /// </summary>
        private Field _TargySzavak = new Field();

        public Field TargySzavak
        {
            get { return _TargySzavak; }
            set { _TargySzavak = value; }
        }
                          
           
        /// <summary>
        /// AlapertelmezettErtek property
        /// A t�rgysz� alap�rtelmezett �rt�ke.
        /// </summary>
        private Field _AlapertelmezettErtek = new Field();

        public Field AlapertelmezettErtek
        {
            get { return _AlapertelmezettErtek; }
            set { _AlapertelmezettErtek = value; }
        }
                          
           
        /// <summary>
        /// BelsoAzonosito property
        /// A TargySzavak �rt�kb�l a (CovetrToIdentifier) f�ggv�nnyel k�pzett (�kezetes karakterek, spec karakterek konverzi�ja) �rt�k.
        /// </summary>
        private Field _BelsoAzonosito = new Field();

        public Field BelsoAzonosito
        {
            get { return _BelsoAzonosito; }
            set { _BelsoAzonosito = value; }
        }
                          
           
        /// <summary>
        /// SPSSzinkronizalt property
        /// A t�rgysz� szinkroniz�l�sa az SPS-sel (0/1)
        /// </summary>
        private Field _SPSSzinkronizalt = new Field();

        public Field SPSSzinkronizalt
        {
            get { return _SPSSzinkronizalt; }
            set { _SPSSzinkronizalt = value; }
        }
                          
           
        /// <summary>
        /// SPS_Field_Id property
        /// A t�rgysz� azonos�t�ja a dokumentumt�rban (SPS-ben).
        /// </summary>
        private Field _SPS_Field_Id = new Field();

        public Field SPS_Field_Id
        {
            get { return _SPS_Field_Id; }
            set { _SPS_Field_Id = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Tulaj property
        /// Tulajdonos Id-je
        /// </summary>
        private Field _Csoport_Id_Tulaj = new Field();

        public Field Csoport_Id_Tulaj
        {
            get { return _Csoport_Id_Tulaj; }
            set { _Csoport_Id_Tulaj = value; }
        }
                          
           
        /// <summary>
        /// RegExp property
        /// Regul�ris kifejer�s a t�rgysz� �rt�k�re.
        /// </summary>
        private Field _RegExp = new Field();

        public Field RegExp
        {
            get { return _RegExp; }
            set { _RegExp = value; }
        }
                          
           
        /// <summary>
        /// ToolTip property
        /// A regul�ris kifejez�shez tartoz� magyar�z� sz�veg.
        /// </summary>
        private Field _ToolTip = new Field();

        public Field ToolTip
        {
            get { return _ToolTip; }
            set { _ToolTip = value; }
        }
                          
           
        /// <summary>
        /// ControlTypeSource property
        /// Vez�rl�elem t�pusok KCS: CONTROLTYPE_SOURCE
        /// (Vez�rl�elem forr�sa (n�vt�rrel min�s�tett oszt�lyn�v, pontosvessz�vel elv�lasztva k�vetheti az Assembly neve, vagy bet�ltend� ascx-f�jl virtu�lis �tvonallal))
        /// Contentum.eUIControls.CustomTextBox;Contentum.eUIControls - Sz�vegmez� (CustomTextBox)
        /// System.Web.UI.WebControls.CheckBox;System.Web - Jel�l�n�gyzet (CheckBox)
        /// ~/Component/CalendarControl.ascx - Napt�r (CalendarControl)
        /// ~/Component/KodTarakDropDownList.ascx - K�dv�laszt� leg�rd�l� lista (KodTarakDropDownList)
        /// ~/Component/EditablePartnerTextBox.ascx - Partnerv�laszt� mez�, szerkeszthet� (EditablePartnerTextBox)
        /// Contentum.eUIControls.eDropDownList,Contentum.eUIControls - Leg�rd�l� lista (eDropDownList)
        /// System.Web.UI.WebControls.Label;System.Web - C�mke (Label)
        /// </summary>
        private Field _ControlTypeSource = new Field();

        public Field ControlTypeSource
        {
            get { return _ControlTypeSource; }
            set { _ControlTypeSource = value; }
        }
                          
           
        /// <summary>
        /// ControlTypeDataSource property
        /// Vez�rl�elem felt�lt�s�hez haszn�lt forr�s (jelenleg csak a k�dcsoportot �rtelmezi az alkalmaz�s, ha ControlTypeSource �rt�ke "KodTarakDropDownList", de k�s�bb esetleg szerv�z + met�dus + param�terek k�dol�s is kidolgozhat�)
        /// </summary>
        private Field _ControlTypeDataSource = new Field();

        public Field ControlTypeDataSource
        {
            get { return _ControlTypeDataSource; }
            set { _ControlTypeDataSource = value; }
        }
                          
           
        /// <summary>
        /// Targyszo_Id_Parent property
        /// 
        /// </summary>
        private Field _Targyszo_Id_Parent = new Field();

        public Field Targyszo_Id_Parent
        {
            get { return _Targyszo_Id_Parent; }
            set { _Targyszo_Id_Parent = value; }
        }
                          
           
        /// <summary>
        /// XSD property
        /// Ha Metaaatot �r le, akkor az XSD kit�lt�tt...
        /// </summary>
        private Field _XSD = new Field();

        public Field XSD
        {
            get { return _XSD; }
            set { _XSD = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}