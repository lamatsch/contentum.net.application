
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_TomegesIktatasFolyamat eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_TomegesIktatasFolyamatSearch: BaseSearchObject    {
      public EREC_TomegesIktatasFolyamatSearch()
      {            
                     _Id.Name = "EREC_TomegesIktatasFolyamat.Id";
               _Id.Type = "Guid";            
               _Forras.Name = "EREC_TomegesIktatasFolyamat.Forras";
               _Forras.Type = "String";            
               _Forras_Azonosito.Name = "EREC_TomegesIktatasFolyamat.Forras_Azonosito";
               _Forras_Azonosito.Type = "String";            
               _Prioritas.Name = "EREC_TomegesIktatasFolyamat.Prioritas";
               _Prioritas.Type = "String";            
               _RQ_Dokumentum_Id.Name = "EREC_TomegesIktatasFolyamat.RQ_Dokumentum_Id";
               _RQ_Dokumentum_Id.Type = "Guid";            
               _RS_Dokumentum_Id.Name = "EREC_TomegesIktatasFolyamat.RS_Dokumentum_Id";
               _RS_Dokumentum_Id.Type = "Guid";            
               _Allapot.Name = "EREC_TomegesIktatasFolyamat.Allapot";
               _Allapot.Type = "String";            
               _ErvKezd.Name = "EREC_TomegesIktatasFolyamat.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_TomegesIktatasFolyamat.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Forras property
        /// Melyik rendszer hozta l�tre a folyamatot
        /// </summary>
        private Field _Forras = new Field();

        public Field Forras
        {
            get { return _Forras; }
            set { _Forras = value; }
        }
                          
           
        /// <summary>
        /// Forras_Azonosito property
        /// Az adott sor egyedi azonos�t�ja, amivel egy k�ls� rendszer lek�rdezheti a st�tusz�t
        /// </summary>
        private Field _Forras_Azonosito = new Field();

        public Field Forras_Azonosito
        {
            get { return _Forras_Azonosito; }
            set { _Forras_Azonosito = value; }
        }
                          
           
        /// <summary>
        /// Prioritas property
        /// TOMEGES_IKTATAS_FOLYAMAT_PRIORITAS k�dcsoport �rt�kk�szlet�nek egy eleme, default: 2
        /// </summary>
        private Field _Prioritas = new Field();

        public Field Prioritas
        {
            get { return _Prioritas; }
            set { _Prioritas = value; }
        }
                          
           
        /// <summary>
        /// RQ_Dokumentum_Id property
        /// A t�meges iktat�st kezdem�nyez� nyers adat dokumentum_id-ja
        /// </summary>
        private Field _RQ_Dokumentum_Id = new Field();

        public Field RQ_Dokumentum_Id
        {
            get { return _RQ_Dokumentum_Id; }
            set { _RQ_Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// RS_Dokumentum_Id property
        /// A t�meges iktat�s kezdem�nyez�sre adott v�lasz dokumentum_id-ja
        /// </summary>
        private Field _RS_Dokumentum_Id = new Field();

        public Field RS_Dokumentum_Id
        {
            get { return _RS_Dokumentum_Id; }
            set { _RS_Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT: inicializ�l�s folyamatban, inicializ�lt, v�grehajt�s folyamatban, v�grehajtott.
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}