
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_TomegesIktatas eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_TomegesIktatasSearch: BaseSearchObject    {
      public EREC_TomegesIktatasSearch()
      {            
                     _Id.Name = "EREC_TomegesIktatas.Id";
               _Id.Type = "Guid";            
               _ForrasTipusNev.Name = "EREC_TomegesIktatas.ForrasTipusNev";
               _ForrasTipusNev.Type = "String";            
               _FelelosCsoport_Id.Name = "EREC_TomegesIktatas.FelelosCsoport_Id";
               _FelelosCsoport_Id.Type = "Guid";            
               _AgazatiJelek_Id.Name = "EREC_TomegesIktatas.AgazatiJelek_Id";
               _AgazatiJelek_Id.Type = "Guid";            
               _IraIrattariTetelek_Id.Name = "EREC_TomegesIktatas.IraIrattariTetelek_Id";
               _IraIrattariTetelek_Id.Type = "Guid";            
               _Ugytipus_Id.Name = "EREC_TomegesIktatas.Ugytipus_Id";
               _Ugytipus_Id.Type = "Guid";            
               _Irattipus_Id.Name = "EREC_TomegesIktatas.Irattipus_Id";
               _Irattipus_Id.Type = "Guid";            
               _TargyPrefix.Name = "EREC_TomegesIktatas.TargyPrefix";
               _TargyPrefix.Type = "String";            
               _AlairasKell.Name = "EREC_TomegesIktatas.AlairasKell";
               _AlairasKell.Type = "Char";            
               _AlairasMod.Name = "EREC_TomegesIktatas.AlairasMod";
               _AlairasMod.Type = "Guid";            
               _FelhasznaloCsoport_Id_Alairo.Name = "EREC_TomegesIktatas.FelhasznaloCsoport_Id_Alairo";
               _FelhasznaloCsoport_Id_Alairo.Type = "Guid";            
               _FelhaszCsoport_Id_Helyettesito.Name = "EREC_TomegesIktatas.FelhaszCsoport_Id_Helyettesito";
               _FelhaszCsoport_Id_Helyettesito.Type = "Guid";            
               _AlairasSzabaly_Id.Name = "EREC_TomegesIktatas.AlairasSzabaly_Id";
               _AlairasSzabaly_Id.Type = "Guid";            
               _HatosagiAdatlapKell.Name = "EREC_TomegesIktatas.HatosagiAdatlapKell";
               _HatosagiAdatlapKell.Type = "Char";            
               _UgyFajtaja.Name = "EREC_TomegesIktatas.UgyFajtaja";
               _UgyFajtaja.Type = "String";            
               _DontestHozta.Name = "EREC_TomegesIktatas.DontestHozta";
               _DontestHozta.Type = "String";            
               _DontesFormaja.Name = "EREC_TomegesIktatas.DontesFormaja";
               _DontesFormaja.Type = "String";            
               _UgyintezesHataridore.Name = "EREC_TomegesIktatas.UgyintezesHataridore";
               _UgyintezesHataridore.Type = "String";            
               _HataridoTullepes.Name = "EREC_TomegesIktatas.HataridoTullepes";
               _HataridoTullepes.Type = "Int32";            
               _HatosagiEllenorzes.Name = "EREC_TomegesIktatas.HatosagiEllenorzes";
               _HatosagiEllenorzes.Type = "Char";            
               _MunkaorakSzama.Name = "EREC_TomegesIktatas.MunkaorakSzama";
               _MunkaorakSzama.Type = "Double";            
               _EljarasiKoltseg.Name = "EREC_TomegesIktatas.EljarasiKoltseg";
               _EljarasiKoltseg.Type = "Int32";            
               _KozigazgatasiBirsagMerteke.Name = "EREC_TomegesIktatas.KozigazgatasiBirsagMerteke";
               _KozigazgatasiBirsagMerteke.Type = "Int32";            
               _SommasEljDontes.Name = "EREC_TomegesIktatas.SommasEljDontes";
               _SommasEljDontes.Type = "String";            
               _NyolcNapBelulNemSommas.Name = "EREC_TomegesIktatas.NyolcNapBelulNemSommas";
               _NyolcNapBelulNemSommas.Type = "String";            
               _FuggoHatalyuHatarozat.Name = "EREC_TomegesIktatas.FuggoHatalyuHatarozat";
               _FuggoHatalyuHatarozat.Type = "String";            
               _FuggoHatalyuVegzes.Name = "EREC_TomegesIktatas.FuggoHatalyuVegzes";
               _FuggoHatalyuVegzes.Type = "String";            
               _HatAltalVisszafizOsszeg.Name = "EREC_TomegesIktatas.HatAltalVisszafizOsszeg";
               _HatAltalVisszafizOsszeg.Type = "Int32";            
               _HatTerheloEljKtsg.Name = "EREC_TomegesIktatas.HatTerheloEljKtsg";
               _HatTerheloEljKtsg.Type = "Int32";            
               _FelfuggHatarozat.Name = "EREC_TomegesIktatas.FelfuggHatarozat";
               _FelfuggHatarozat.Type = "String";            
               _ErvKezd.Name = "EREC_TomegesIktatas.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_TomegesIktatas.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// ForrasTipusNev property
        /// 
        /// </summary>
        private Field _ForrasTipusNev = new Field();

        public Field ForrasTipusNev
        {
            get { return _ForrasTipusNev; }
            set { _ForrasTipusNev = value; }
        }
                          
           
        /// <summary>
        /// FelelosCsoport_Id property
        /// 
        /// </summary>
        private Field _FelelosCsoport_Id = new Field();

        public Field FelelosCsoport_Id
        {
            get { return _FelelosCsoport_Id; }
            set { _FelelosCsoport_Id = value; }
        }
                          
           
        /// <summary>
        /// AgazatiJelek_Id property
        /// 
        /// </summary>
        private Field _AgazatiJelek_Id = new Field();

        public Field AgazatiJelek_Id
        {
            get { return _AgazatiJelek_Id; }
            set { _AgazatiJelek_Id = value; }
        }
                          
           
        /// <summary>
        /// IraIrattariTetelek_Id property
        /// 
        /// </summary>
        private Field _IraIrattariTetelek_Id = new Field();

        public Field IraIrattariTetelek_Id
        {
            get { return _IraIrattariTetelek_Id; }
            set { _IraIrattariTetelek_Id = value; }
        }
                          
           
        /// <summary>
        /// Ugytipus_Id property
        /// 
        /// </summary>
        private Field _Ugytipus_Id = new Field();

        public Field Ugytipus_Id
        {
            get { return _Ugytipus_Id; }
            set { _Ugytipus_Id = value; }
        }
                          
           
        /// <summary>
        /// Irattipus_Id property
        /// 
        /// </summary>
        private Field _Irattipus_Id = new Field();

        public Field Irattipus_Id
        {
            get { return _Irattipus_Id; }
            set { _Irattipus_Id = value; }
        }
                          
           
        /// <summary>
        /// TargyPrefix property
        /// 
        /// </summary>
        private Field _TargyPrefix = new Field();

        public Field TargyPrefix
        {
            get { return _TargyPrefix; }
            set { _TargyPrefix = value; }
        }
                          
           
        /// <summary>
        /// AlairasKell property
        /// 
        /// </summary>
        private Field _AlairasKell = new Field();

        public Field AlairasKell
        {
            get { return _AlairasKell; }
            set { _AlairasKell = value; }
        }
                          
           
        /// <summary>
        /// AlairasMod property
        /// 
        /// </summary>
        private Field _AlairasMod = new Field();

        public Field AlairasMod
        {
            get { return _AlairasMod; }
            set { _AlairasMod = value; }
        }
                          
           
        /// <summary>
        /// FelhasznaloCsoport_Id_Alairo property
        /// 
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Alairo = new Field();

        public Field FelhasznaloCsoport_Id_Alairo
        {
            get { return _FelhasznaloCsoport_Id_Alairo; }
            set { _FelhasznaloCsoport_Id_Alairo = value; }
        }
                          
           
        /// <summary>
        /// FelhaszCsoport_Id_Helyettesito property
        /// 
        /// </summary>
        private Field _FelhaszCsoport_Id_Helyettesito = new Field();

        public Field FelhaszCsoport_Id_Helyettesito
        {
            get { return _FelhaszCsoport_Id_Helyettesito; }
            set { _FelhaszCsoport_Id_Helyettesito = value; }
        }
                          
           
        /// <summary>
        /// AlairasSzabaly_Id property
        /// 
        /// </summary>
        private Field _AlairasSzabaly_Id = new Field();

        public Field AlairasSzabaly_Id
        {
            get { return _AlairasSzabaly_Id; }
            set { _AlairasSzabaly_Id = value; }
        }
                          
           
        /// <summary>
        /// HatosagiAdatlapKell property
        /// 
        /// </summary>
        private Field _HatosagiAdatlapKell = new Field();

        public Field HatosagiAdatlapKell
        {
            get { return _HatosagiAdatlapKell; }
            set { _HatosagiAdatlapKell = value; }
        }
                          
           
        /// <summary>
        /// UgyFajtaja property
        /// 2008.03.31 KCS: kcs.UGY_FAJTAJA
        /// 
        /// </summary>
        private Field _UgyFajtaja = new Field();

        public Field UgyFajtaja
        {
            get { return _UgyFajtaja; }
            set { _UgyFajtaja = value; }
        }
                          
           
        /// <summary>
        /// DontestHozta property
        /// 2. A d�nt�st hozta -> KCS: DONTEST_HOZTA
        /// k�zgy�l�s test�lete (1)
        /// k�zgy�l�s bizotts�ga (2)
        /// a r�sz�nkorm�nyzat test�lete (3)
        /// a (f�)polg�rmester (4)
        /// a (f�)jegyz� (5)
        /// </summary>
        private Field _DontestHozta = new Field();

        public Field DontestHozta
        {
            get { return _DontestHozta; }
            set { _DontestHozta = value; }
        }
                          
           
        /// <summary>
        /// DontesFormaja property
        /// 3. A d�nt�s form�ja -> KCS: DONTES_FORMAJA
        /// hat�rozat (1)
        /// egyezs�g j�v�hagy�s�t tartalmaz� hat�rozat (2)
        /// hat�s�gi szerz�d�s (3)
        /// a Ket. 30. � alapj�n t�rt�n� elutas�t�s (4)
        /// a Ket. 31. � alapj�n t�rt�n� megsz�ntet�se (5)
        /// az els�fok� elj�r�sban hozott egy�b v�gz�s (6)
        /// 
        /// </summary>
        private Field _DontesFormaja = new Field();

        public Field DontesFormaja
        {
            get { return _DontesFormaja; }
            set { _DontesFormaja = value; }
        }
                          
           
        /// <summary>
        /// UgyintezesHataridore property
        /// 4. Az �gyint�z�s id�tartama -> KCS: UGYINTEZES_IDOTARTAMA
        /// hat�rid�n bel�l (1)
        /// hat�rid�n t�l (2)
        /// </summary>
        private Field _UgyintezesHataridore = new Field();

        public Field UgyintezesHataridore
        {
            get { return _UgyintezesHataridore; }
            set { _UgyintezesHataridore = value; }
        }
                          
           
        /// <summary>
        /// HataridoTullepes property
        /// Az �rdemi d�nt�ssel kapcsolatos esetleges hat�rid�-t�ll�p�s id�tartlma napokban (0-999)
        /// </summary>
        private Field _HataridoTullepes = new Field();

        public Field HataridoTullepes
        {
            get { return _HataridoTullepes; }
            set { _HataridoTullepes = value; }
        }
                          
           
        /// <summary>
        /// HatosagiEllenorzes property
        /// �rt�k: 0/1
        /// 
        /// K�rj�k felt�ntetni az adott elj�r�st�pus sor�n a t�rgyid�szakban lefolytatott hat�s�gi ellen�rz�sek sz�m�t.
        /// </summary>
        private Field _HatosagiEllenorzes = new Field();

        public Field HatosagiEllenorzes
        {
            get { return _HatosagiEllenorzes; }
            set { _HatosagiEllenorzes = value; }
        }
                          
           
        /// <summary>
        /// MunkaorakSzama property
        /// �rt�k: 3 eg�sz + 1 tizedes
        /// 
        /// Az adott �gazatban az egy �gy elint�z�s�re ford�tott munka�r�k �tlagos sz�m�t kell felt�ntetni (�sszes munka�ra / elj�r�sok sz�ma).
        /// </summary>
        private Field _MunkaorakSzama = new Field();

        public Field MunkaorakSzama
        {
            get { return _MunkaorakSzama; }
            set { _MunkaorakSzama = value; }
        }
                          
           
        /// <summary>
        /// EljarasiKoltseg property
        /// �rt�k: 6 jegy� eg�sz sz�m.
        /// 
        /// Az adott �gazatban az egy �gyre jut� �tlagos elj�r�si k�lts�get k�rj�k felt�ntetni (�sszes elj�r�si k�lts�g / elj�r�sok sz�ma).
        /// Elj�r�si k�lts�gen ebben a tekintetben a hat�s�g �ltal meg�llap�tott, a Ket. vagy m�s vonatkoz� jogszab�ly szerinti elj�r�si k�lts�get kell �rteni. A hat�s�g m�k�d�si k�lts�gei figyelmen k�v�l hagyand�ak.
        /// </summary>
        private Field _EljarasiKoltseg = new Field();

        public Field EljarasiKoltseg
        {
            get { return _EljarasiKoltseg; }
            set { _EljarasiKoltseg = value; }
        }
                          
           
        /// <summary>
        /// KozigazgatasiBirsagMerteke property
        /// 
        /// </summary>
        private Field _KozigazgatasiBirsagMerteke = new Field();

        public Field KozigazgatasiBirsagMerteke
        {
            get { return _KozigazgatasiBirsagMerteke; }
            set { _KozigazgatasiBirsagMerteke = value; }
        }
                          
           
        /// <summary>
        /// SommasEljDontes property
        /// 
        /// </summary>
        private Field _SommasEljDontes = new Field();

        public Field SommasEljDontes
        {
            get { return _SommasEljDontes; }
            set { _SommasEljDontes = value; }
        }
                          
           
        /// <summary>
        /// NyolcNapBelulNemSommas property
        /// 
        /// </summary>
        private Field _NyolcNapBelulNemSommas = new Field();

        public Field NyolcNapBelulNemSommas
        {
            get { return _NyolcNapBelulNemSommas; }
            set { _NyolcNapBelulNemSommas = value; }
        }
                          
           
        /// <summary>
        /// FuggoHatalyuHatarozat property
        /// 
        /// </summary>
        private Field _FuggoHatalyuHatarozat = new Field();

        public Field FuggoHatalyuHatarozat
        {
            get { return _FuggoHatalyuHatarozat; }
            set { _FuggoHatalyuHatarozat = value; }
        }
                          
           
        /// <summary>
        /// FuggoHatalyuVegzes property
        /// 
        /// </summary>
        private Field _FuggoHatalyuVegzes = new Field();

        public Field FuggoHatalyuVegzes
        {
            get { return _FuggoHatalyuVegzes; }
            set { _FuggoHatalyuVegzes = value; }
        }
                          
           
        /// <summary>
        /// HatAltalVisszafizOsszeg property
        /// 
        /// </summary>
        private Field _HatAltalVisszafizOsszeg = new Field();

        public Field HatAltalVisszafizOsszeg
        {
            get { return _HatAltalVisszafizOsszeg; }
            set { _HatAltalVisszafizOsszeg = value; }
        }
                          
           
        /// <summary>
        /// HatTerheloEljKtsg property
        /// 
        /// </summary>
        private Field _HatTerheloEljKtsg = new Field();

        public Field HatTerheloEljKtsg
        {
            get { return _HatTerheloEljKtsg; }
            set { _HatTerheloEljKtsg = value; }
        }
                          
           
        /// <summary>
        /// FelfuggHatarozat property
        /// 
        /// </summary>
        private Field _FelfuggHatarozat = new Field();

        public Field FelfuggHatarozat
        {
            get { return _FelfuggHatarozat; }
            set { _FelfuggHatarozat = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}