
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_TomegesIktatasTetelek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_TomegesIktatasTetelekSearch: BaseSearchObject    {
      public EREC_TomegesIktatasTetelekSearch()
      {            
                     _Id.Name = "EREC_TomegesIktatasTetelek.Id";
               _Id.Type = "Guid";            
               _Folyamat_Id.Name = "EREC_TomegesIktatasTetelek.Folyamat_Id";
               _Folyamat_Id.Type = "Guid";            
               _Forras_Azonosito.Name = "EREC_TomegesIktatasTetelek.Forras_Azonosito";
               _Forras_Azonosito.Type = "String";            
               _IktatasTipus.Name = "EREC_TomegesIktatasTetelek.IktatasTipus";
               _IktatasTipus.Type = "Int32";            
               _Alszamra.Name = "EREC_TomegesIktatasTetelek.Alszamra";
               _Alszamra.Type = "Int32";            
               _Iktatokonyv_Id.Name = "EREC_TomegesIktatasTetelek.Iktatokonyv_Id";
               _Iktatokonyv_Id.Type = "Guid";            
               _Ugyirat_Id.Name = "EREC_TomegesIktatasTetelek.Ugyirat_Id";
               _Ugyirat_Id.Type = "Guid";            
               _Kuldemeny_Id.Name = "EREC_TomegesIktatasTetelek.Kuldemeny_Id";
               _Kuldemeny_Id.Type = "Guid";            
               _ExecParam.Name = "EREC_TomegesIktatasTetelek.ExecParam";
               _ExecParam.Type = "String";            
               _EREC_UgyUgyiratok.Name = "EREC_TomegesIktatasTetelek.EREC_UgyUgyiratok";
               _EREC_UgyUgyiratok.Type = "String";            
               _EREC_UgyUgyiratdarabok.Name = "EREC_TomegesIktatasTetelek.EREC_UgyUgyiratdarabok";
               _EREC_UgyUgyiratdarabok.Type = "String";            
               _EREC_IraIratok.Name = "EREC_TomegesIktatasTetelek.EREC_IraIratok";
               _EREC_IraIratok.Type = "String";            
               _EREC_PldIratPeldanyok.Name = "EREC_TomegesIktatasTetelek.EREC_PldIratPeldanyok";
               _EREC_PldIratPeldanyok.Type = "String";            
               _EREC_HataridosFeladatok.Name = "EREC_TomegesIktatasTetelek.EREC_HataridosFeladatok";
               _EREC_HataridosFeladatok.Type = "String";            
               _EREC_KuldKuldemenyek.Name = "EREC_TomegesIktatasTetelek.EREC_KuldKuldemenyek";
               _EREC_KuldKuldemenyek.Type = "String";            
               _IktatasiParameterek.Name = "EREC_TomegesIktatasTetelek.IktatasiParameterek";
               _IktatasiParameterek.Type = "String";            
               _ErkeztetesParameterek.Name = "EREC_TomegesIktatasTetelek.ErkeztetesParameterek";
               _ErkeztetesParameterek.Type = "String";            
               _Dokumentumok.Name = "EREC_TomegesIktatasTetelek.Dokumentumok";
               _Dokumentumok.Type = "String";            
               _Azonosito.Name = "EREC_TomegesIktatasTetelek.Azonosito";
               _Azonosito.Type = "String";            
               _Allapot.Name = "EREC_TomegesIktatasTetelek.Allapot";
               _Allapot.Type = "String";            
               _Irat_Id.Name = "EREC_TomegesIktatasTetelek.Irat_Id";
               _Irat_Id.Type = "Guid";            
               _Hiba.Name = "EREC_TomegesIktatasTetelek.Hiba";
               _Hiba.Type = "String";            
               _Expedialas.Name = "EREC_TomegesIktatasTetelek.Expedialas";
               _Expedialas.Type = "Int32";            
               _TobbIratEgyKuldemenybe.Name = "EREC_TomegesIktatasTetelek.TobbIratEgyKuldemenybe";
               _TobbIratEgyKuldemenybe.Type = "Int32";            
               _KuldemenyAtadas.Name = "EREC_TomegesIktatasTetelek.KuldemenyAtadas";
               _KuldemenyAtadas.Type = "Guid";            
               _HatosagiStatAdat.Name = "EREC_TomegesIktatasTetelek.HatosagiStatAdat";
               _HatosagiStatAdat.Type = "String";            
               _EREC_IratAlairok.Name = "EREC_TomegesIktatasTetelek.EREC_IratAlairok";
               _EREC_IratAlairok.Type = "String";            
               _Lezarhato.Name = "EREC_TomegesIktatasTetelek.Lezarhato";
               _Lezarhato.Type = "Int32";            
               _ErvKezd.Name = "EREC_TomegesIktatasTetelek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_TomegesIktatasTetelek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Folyamat_Id property
        /// 
        /// </summary>
        private Field _Folyamat_Id = new Field();

        public Field Folyamat_Id
        {
            get { return _Folyamat_Id; }
            set { _Folyamat_Id = value; }
        }
                          
           
        /// <summary>
        /// Forras_Azonosito property
        /// Egy adott t�tel forr�s oldali azonos�t�ja, amivel lek�rdezhet� az adott t�tel st�tusza
        /// </summary>
        private Field _Forras_Azonosito = new Field();

        public Field Forras_Azonosito
        {
            get { return _Forras_Azonosito; }
            set { _Forras_Azonosito = value; }
        }
                          
           
        /// <summary>
        /// IktatasTipus property
        /// bej�v� = 0 vagy bels� = 1
        /// </summary>
        private Field _IktatasTipus = new Field();

        public Field IktatasTipus
        {
            get { return _IktatasTipus; }
            set { _IktatasTipus = value; }
        }
                          
           
        /// <summary>
        /// Alszamra property
        /// norm�l = 0 vagy alsz�mra = 1
        /// </summary>
        private Field _Alszamra = new Field();

        public Field Alszamra
        {
            get { return _Alszamra; }
            set { _Alszamra = value; }
        }
                          
           
        /// <summary>
        /// Iktatokonyv_Id property
        /// FK az EREC_IraIktatokonyvek.Id mez�re
        /// </summary>
        private Field _Iktatokonyv_Id = new Field();

        public Field Iktatokonyv_Id
        {
            get { return _Iktatokonyv_Id; }
            set { _Iktatokonyv_Id = value; }
        }
                          
           
        /// <summary>
        /// Ugyirat_Id property
        /// FK az EREC_UgyUgyratok.Id mez�re (ez csak alsz�mos iktat�sn�l van kit�ltve)
        /// </summary>
        private Field _Ugyirat_Id = new Field();

        public Field Ugyirat_Id
        {
            get { return _Ugyirat_Id; }
            set { _Ugyirat_Id = value; }
        }
                          
           
        /// <summary>
        /// Kuldemeny_Id property
        /// FK az EREC_KuldKuldemenyek.Id mez�re (ez csak bej�v� irat iktat�sn�l van kit�ltve, amikor a Contentum-ban azonos�tott k�ldem�nyt iktatunk)
        /// </summary>
        private Field _Kuldemeny_Id = new Field();

        public Field Kuldemeny_Id
        {
            get { return _Kuldemeny_Id; }
            set { _Kuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// ExecParam property
        /// A futtat�shoz sz�ks�ges ExecParam oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        private Field _ExecParam = new Field();

        public Field ExecParam
        {
            get { return _ExecParam; }
            set { _ExecParam = value; }
        }
                          
           
        /// <summary>
        /// EREC_UgyUgyiratok property
        /// A futtat�shoz sz�ks�ges EREC_UgyUgyiratok oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        private Field _EREC_UgyUgyiratok = new Field();

        public Field EREC_UgyUgyiratok
        {
            get { return _EREC_UgyUgyiratok; }
            set { _EREC_UgyUgyiratok = value; }
        }
                          
           
        /// <summary>
        /// EREC_UgyUgyiratdarabok property
        /// A futtat�shoz sz�ks�ges EREC_UgyUgyiratdarabok oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        private Field _EREC_UgyUgyiratdarabok = new Field();

        public Field EREC_UgyUgyiratdarabok
        {
            get { return _EREC_UgyUgyiratdarabok; }
            set { _EREC_UgyUgyiratdarabok = value; }
        }
                          
           
        /// <summary>
        /// EREC_IraIratok property
        /// A futtat�shoz sz�ks�ges EREC_IraIratok oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        private Field _EREC_IraIratok = new Field();

        public Field EREC_IraIratok
        {
            get { return _EREC_IraIratok; }
            set { _EREC_IraIratok = value; }
        }
                          
           
        /// <summary>
        /// EREC_PldIratPeldanyok property
        /// A futtat�shoz sz�ks�ges EREC_PldIratPeldanyok oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        private Field _EREC_PldIratPeldanyok = new Field();

        public Field EREC_PldIratPeldanyok
        {
            get { return _EREC_PldIratPeldanyok; }
            set { _EREC_PldIratPeldanyok = value; }
        }
                          
           
        /// <summary>
        /// EREC_HataridosFeladatok property
        /// A futtat�shoz sz�ks�ges EREC_HataridosFeladatok oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        private Field _EREC_HataridosFeladatok = new Field();

        public Field EREC_HataridosFeladatok
        {
            get { return _EREC_HataridosFeladatok; }
            set { _EREC_HataridosFeladatok = value; }
        }
                          
           
        /// <summary>
        /// EREC_KuldKuldemenyek property
        /// A futtat�shoz sz�ks�ges EREC_KuldKuldemenyek oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        private Field _EREC_KuldKuldemenyek = new Field();

        public Field EREC_KuldKuldemenyek
        {
            get { return _EREC_KuldKuldemenyek; }
            set { _EREC_KuldKuldemenyek = value; }
        }
                          
           
        /// <summary>
        /// IktatasiParameterek property
        /// A futtat�shoz sz�ks�ges IktatasiParameterek oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        private Field _IktatasiParameterek = new Field();

        public Field IktatasiParameterek
        {
            get { return _IktatasiParameterek; }
            set { _IktatasiParameterek = value; }
        }
                          
           
        /// <summary>
        /// ErkeztetesParameterek property
        /// A futtat�shoz sz�ks�ges ErkeztetesParametere oszt�ly t�rol�sa JSON form�tumban
        /// </summary>
        private Field _ErkeztetesParameterek = new Field();

        public Field ErkeztetesParameterek
        {
            get { return _ErkeztetesParameterek; }
            set { _ErkeztetesParameterek = value; }
        }
                          
           
        /// <summary>
        /// Dokumentumok property
        /// A csatoland� dokumentumok neve �s el�rhet�s�ge (UNC filepath, vagy URL)
        /// </summary>
        private Field _Dokumentumok = new Field();

        public Field Dokumentumok
        {
            get { return _Dokumentumok; }
            set { _Dokumentumok = value; }
        }
                          
           
        /// <summary>
        /// Azonosito property
        /// Az adott irat iktat�sz�ma
        /// </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// TOMEGES_IKTATAS_TETEL_ALLAPOT
        /// - 1 - inicializ�lt (iktat�sz�m meghat�rozott, de m�g nincs iktatva)
        /// - 2 - iktatott (sikerese befejez�d�tt az iktat�s)
        /// - 9 - hib�s (az iktat�s hib�ra futott, jav�tani kell az adatokat)
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Irat_Id property
        /// FK az EREC_IraIratok.Id mez�re, az adott sorhoz tartoz� irat azonos�t�ja
        /// </summary>
        private Field _Irat_Id = new Field();

        public Field Irat_Id
        {
            get { return _Irat_Id; }
            set { _Irat_Id = value; }
        }
                          
           
        /// <summary>
        /// Hiba property
        /// 
        /// </summary>
        private Field _Hiba = new Field();

        public Field Hiba
        {
            get { return _Hiba; }
            set { _Hiba = value; }
        }
                          
           
        /// <summary>
        /// Expedialas property
        ///   0|1 - iktat�s ut�na az iratp�ld�ny expedi�land�
        /// </summary>
        private Field _Expedialas = new Field();

        public Field Expedialas
        {
            get { return _Expedialas; }
            set { _Expedialas = value; }
        }
                          
           
        /// <summary>
        /// TobbIratEgyKuldemenybe property
        ///  0|1 - expedi�l�skor az azonos t�meges iktat�si folyamatban l�v�, azonos c�mzettnek, azonos c�mre k�ldend� iratp�ld�nnyal egy�tt tehet� egy k�ldem�nybe
        /// </summary>
        private Field _TobbIratEgyKuldemenybe = new Field();

        public Field TobbIratEgyKuldemenybe
        {
            get { return _TobbIratEgyKuldemenybe; }
            set { _TobbIratEgyKuldemenybe = value; }
        }
                          
           
        /// <summary>
        /// KuldemenyAtadas property
        /// expedi�l�s ut�n a k�ldem�ny melyik "csoport"-nak ker�lj�n �tad�sra (�tad�sra kijel�l, �tad)
        /// </summary>
        private Field _KuldemenyAtadas = new Field();

        public Field KuldemenyAtadas
        {
            get { return _KuldemenyAtadas; }
            set { _KuldemenyAtadas = value; }
        }
                          
           
        /// <summary>
        /// HatosagiStatAdat property
        /// Az adott irat iktat�sz�ma
        /// </summary>
        private Field _HatosagiStatAdat = new Field();

        public Field HatosagiStatAdat
        {
            get { return _HatosagiStatAdat; }
            set { _HatosagiStatAdat = value; }
        }
                          
           
        /// <summary>
        /// EREC_IratAlairok property
        /// Az adott irat iktat�sz�ma
        /// </summary>
        private Field _EREC_IratAlairok = new Field();

        public Field EREC_IratAlairok
        {
            get { return _EREC_IratAlairok; }
            set { _EREC_IratAlairok = value; }
        }
                          
           
        /// <summary>
        /// Lezarhato property
        ///  0|1 - expedi�l�skor az azonos t�meges iktat�si folyamatban l�vo, azonos c�mzettnek, azonos c�mre k�ldendo iratp�ld�nnyal egy�tt teheto egy k�ldem�nybe
        /// </summary>
        private Field _Lezarhato = new Field();

        public Field Lezarhato
        {
            get { return _Lezarhato; }
            set { _Lezarhato = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}