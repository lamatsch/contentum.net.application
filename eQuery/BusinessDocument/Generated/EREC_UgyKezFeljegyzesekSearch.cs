
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_UgyKezFeljegyzesek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_UgyKezFeljegyzesekSearch: BaseSearchObject    {
      public EREC_UgyKezFeljegyzesekSearch()
      {            
                     _Id.Name = "EREC_UgyKezFeljegyzesek.Id";
               _Id.Type = "Guid";            
               _UgyUgyirat_Id.Name = "EREC_UgyKezFeljegyzesek.UgyUgyirat_Id";
               _UgyUgyirat_Id.Type = "Guid";            
               _KezelesTipus.Name = "EREC_UgyKezFeljegyzesek.KezelesTipus";
               _KezelesTipus.Type = "String";            
               _Leiras.Name = "EREC_UgyKezFeljegyzesek.Leiras";
               _Leiras.Type = "String";            
               _Note.Name = "EREC_UgyKezFeljegyzesek.Note";
               _Note.Type = "String";            
               _ErvKezd.Name = "EREC_UgyKezFeljegyzesek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_UgyKezFeljegyzesek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgyUgyirat_Id = new Field();

        public Field UgyUgyirat_Id
        {
            get { return _UgyUgyirat_Id; }
            set { _UgyUgyirat_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KezelesTipus = new Field();

        public Field KezelesTipus
        {
            get { return _KezelesTipus; }
            set { _KezelesTipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Note = new Field();

        public Field Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}