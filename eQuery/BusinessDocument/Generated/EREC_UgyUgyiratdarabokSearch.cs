
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_UgyUgyiratdarabok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_UgyUgyiratdarabokSearch: BaseSearchObject    {
      public EREC_UgyUgyiratdarabokSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "EREC_UgyUgyiratdarabok.Id";
               _Id.Type = "Guid";            
               _UgyUgyirat_Id.Name = "EREC_UgyUgyiratdarabok.UgyUgyirat_Id";
               _UgyUgyirat_Id.Type = "Guid";            
               _EljarasiSzakasz.Name = "EREC_UgyUgyiratdarabok.EljarasiSzakasz";
               _EljarasiSzakasz.Type = "String";            
               _Sorszam.Name = "EREC_UgyUgyiratdarabok.Sorszam";
               _Sorszam.Type = "Int32";            
               _Azonosito.Name = "EREC_UgyUgyiratdarabok.Azonosito";
               _Azonosito.Type = "String";            
               _Hatarido.Name = "EREC_UgyUgyiratdarabok.Hatarido";
               _Hatarido.Type = "DateTime";            
               _FelhasznaloCsoport_Id_Ugyintez.Name = "EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez";
               _FelhasznaloCsoport_Id_Ugyintez.Type = "Guid";            
               _ElintezesDat.Name = "EREC_UgyUgyiratdarabok.ElintezesDat";
               _ElintezesDat.Type = "DateTime";            
               _LezarasDat.Name = "EREC_UgyUgyiratdarabok.LezarasDat";
               _LezarasDat.Type = "DateTime";            
               _ElintezesMod.Name = "EREC_UgyUgyiratdarabok.ElintezesMod";
               _ElintezesMod.Type = "String";            
               _LezarasOka.Name = "EREC_UgyUgyiratdarabok.LezarasOka";
               _LezarasOka.Type = "String";            
               _Leiras.Name = "EREC_UgyUgyiratdarabok.Leiras";
               _Leiras.Type = "String";            
               _UgyUgyirat_Id_Elozo.Name = "EREC_UgyUgyiratdarabok.UgyUgyirat_Id_Elozo";
               _UgyUgyirat_Id_Elozo.Type = "Guid";            
               _IraIktatokonyv_Id.Name = "EREC_UgyUgyiratdarabok.IraIktatokonyv_Id";
               _IraIktatokonyv_Id.Type = "Guid";            
               _Foszam.Name = "EREC_UgyUgyiratdarabok.Foszam";
               _Foszam.Type = "Int32";            
               _UtolsoAlszam.Name = "EREC_UgyUgyiratdarabok.UtolsoAlszam";
               _UtolsoAlszam.Type = "Int32";            
               _Csoport_Id_Felelos.Name = "EREC_UgyUgyiratdarabok.Csoport_Id_Felelos";
               _Csoport_Id_Felelos.Type = "Guid";            
               _Csoport_Id_Felelos_Elozo.Name = "EREC_UgyUgyiratdarabok.Csoport_Id_Felelos_Elozo";
               _Csoport_Id_Felelos_Elozo.Type = "Guid";            
               _IratMetadefinicio_Id.Name = "EREC_UgyUgyiratdarabok.IratMetadefinicio_Id";
               _IratMetadefinicio_Id.Type = "Guid";            
               _Allapot.Name = "EREC_UgyUgyiratdarabok.Allapot";
               _Allapot.Type = "String";            
               _ErvKezd.Name = "EREC_UgyUgyiratdarabok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_UgyUgyiratdarabok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgyUgyirat_Id = new Field();

        public Field UgyUgyirat_Id
        {
            get { return _UgyUgyirat_Id; }
            set { _UgyUgyirat_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _EljarasiSzakasz = new Field();

        public Field EljarasiSzakasz
        {
            get { return _EljarasiSzakasz; }
            set { _EljarasiSzakasz = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Hatarido = new Field();

        public Field Hatarido
        {
            get { return _Hatarido; }
            set { _Hatarido = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FelhasznaloCsoport_Id_Ugyintez = new Field();

        public Field FelhasznaloCsoport_Id_Ugyintez
        {
            get { return _FelhasznaloCsoport_Id_Ugyintez; }
            set { _FelhasznaloCsoport_Id_Ugyintez = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ElintezesDat = new Field();

        public Field ElintezesDat
        {
            get { return _ElintezesDat; }
            set { _ElintezesDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LezarasDat = new Field();

        public Field LezarasDat
        {
            get { return _LezarasDat; }
            set { _LezarasDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ElintezesMod = new Field();

        public Field ElintezesMod
        {
            get { return _ElintezesMod; }
            set { _ElintezesMod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LezarasOka = new Field();

        public Field LezarasOka
        {
            get { return _LezarasOka; }
            set { _LezarasOka = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UgyUgyirat_Id_Elozo = new Field();

        public Field UgyUgyirat_Id_Elozo
        {
            get { return _UgyUgyirat_Id_Elozo; }
            set { _UgyUgyirat_Id_Elozo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IraIktatokonyv_Id = new Field();

        public Field IraIktatokonyv_Id
        {
            get { return _IraIktatokonyv_Id; }
            set { _IraIktatokonyv_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Foszam = new Field();

        public Field Foszam
        {
            get { return _Foszam; }
            set { _Foszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UtolsoAlszam = new Field();

        public Field UtolsoAlszam
        {
            get { return _UtolsoAlszam; }
            set { _UtolsoAlszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Felelos = new Field();

        public Field Csoport_Id_Felelos
        {
            get { return _Csoport_Id_Felelos; }
            set { _Csoport_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Felelos_Elozo = new Field();

        public Field Csoport_Id_Felelos_Elozo
        {
            get { return _Csoport_Id_Felelos_Elozo; }
            set { _Csoport_Id_Felelos_Elozo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IratMetadefinicio_Id = new Field();

        public Field IratMetadefinicio_Id
        {
            get { return _IratMetadefinicio_Id; }
            set { _IratMetadefinicio_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}