
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_UgyUgyiratok eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public partial class EREC_UgyUgyiratokSearch : BaseSearchObject
    {
        public EREC_UgyUgyiratokSearch()
        {
            Init_ManualFields();

            _Id.Name = "EREC_UgyUgyiratok.Id";
            _Id.Type = "Guid";
            _Foszam.Name = "EREC_UgyUgyiratok.Foszam";
            _Foszam.Type = "Int32";
            _Sorszam.Name = "EREC_UgyUgyiratok.Sorszam";
            _Sorszam.Type = "Int32";
            _Ugyazonosito.Name = "EREC_UgyUgyiratok.Ugyazonosito ";
            _Ugyazonosito.Type = "String";
            _Alkalmazas_Id.Name = "EREC_UgyUgyiratok.Alkalmazas_Id";
            _Alkalmazas_Id.Type = "Guid";
            _UgyintezesModja.Name = "EREC_UgyUgyiratok.UgyintezesModja";
            _UgyintezesModja.Type = "String";
            _Hatarido.Name = "EREC_UgyUgyiratok.Hatarido";
            _Hatarido.Type = "DateTime";
            _SkontrobaDat.Name = "EREC_UgyUgyiratok.SkontrobaDat";
            _SkontrobaDat.Type = "DateTime";
            _LezarasDat.Name = "EREC_UgyUgyiratok.LezarasDat";
            _LezarasDat.Type = "DateTime";
            _IrattarbaKuldDatuma.Name = "EREC_UgyUgyiratok.IrattarbaKuldDatuma ";
            _IrattarbaKuldDatuma.Type = "DateTime";
            _IrattarbaVetelDat.Name = "EREC_UgyUgyiratok.IrattarbaVetelDat";
            _IrattarbaVetelDat.Type = "DateTime";
            _FelhCsoport_Id_IrattariAtvevo.Name = "EREC_UgyUgyiratok.FelhCsoport_Id_IrattariAtvevo";
            _FelhCsoport_Id_IrattariAtvevo.Type = "Guid";
            _SelejtezesDat.Name = "EREC_UgyUgyiratok.SelejtezesDat";
            _SelejtezesDat.Type = "DateTime";
            _FelhCsoport_Id_Selejtezo.Name = "EREC_UgyUgyiratok.FelhCsoport_Id_Selejtezo";
            _FelhCsoport_Id_Selejtezo.Type = "Guid";
            _LeveltariAtvevoNeve.Name = "EREC_UgyUgyiratok.LeveltariAtvevoNeve";
            _LeveltariAtvevoNeve.Type = "String";
            _FelhCsoport_Id_Felulvizsgalo.Name = "EREC_UgyUgyiratok.FelhCsoport_Id_Felulvizsgalo";
            _FelhCsoport_Id_Felulvizsgalo.Type = "Guid";
            _FelulvizsgalatDat.Name = "EREC_UgyUgyiratok.FelulvizsgalatDat";
            _FelulvizsgalatDat.Type = "DateTime";
            _IktatoszamKieg.Name = "EREC_UgyUgyiratok.IktatoszamKieg";
            _IktatoszamKieg.Type = "String";
            _Targy.Name = "EREC_UgyUgyiratok.Targy";
            _Targy.Type = "FTSString";
            _UgyUgyirat_Id_Szulo.Name = "EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo";
            _UgyUgyirat_Id_Szulo.Type = "Guid";
            _UgyUgyirat_Id_Kulso.Name = "EREC_UgyUgyiratok.UgyUgyirat_Id_Kulso";
            _UgyUgyirat_Id_Kulso.Type = "Guid";
            _UgyTipus.Name = "EREC_UgyUgyiratok.UgyTipus";
            _UgyTipus.Type = "String";
            _IrattariHely.Name = "EREC_UgyUgyiratok.IrattariHely";
            _IrattariHely.Type = "String";
            _SztornirozasDat.Name = "EREC_UgyUgyiratok.SztornirozasDat";
            _SztornirozasDat.Type = "DateTime";
            _Csoport_Id_Felelos.Name = "EREC_UgyUgyiratok.Csoport_Id_Felelos";
            _Csoport_Id_Felelos.Type = "Guid";
            _FelhasznaloCsoport_Id_Orzo.Name = "EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo";
            _FelhasznaloCsoport_Id_Orzo.Type = "Guid";
            _Csoport_Id_Cimzett.Name = "EREC_UgyUgyiratok.Csoport_Id_Cimzett";
            _Csoport_Id_Cimzett.Type = "Guid";
            _Jelleg.Name = "EREC_UgyUgyiratok.Jelleg";
            _Jelleg.Type = "String";
            _IraIrattariTetel_Id.Name = "EREC_UgyUgyiratok.IraIrattariTetel_Id";
            _IraIrattariTetel_Id.Type = "Guid";
            _IraIktatokonyv_Id.Name = "EREC_UgyUgyiratok.IraIktatokonyv_Id";
            _IraIktatokonyv_Id.Type = "Guid";
            _SkontroOka.Name = "EREC_UgyUgyiratok.SkontroOka";
            _SkontroOka.Type = "String";
            _SkontroVege.Name = "EREC_UgyUgyiratok.SkontroVege";
            _SkontroVege.Type = "DateTime";
            _Surgosseg.Name = "EREC_UgyUgyiratok.Surgosseg";
            _Surgosseg.Type = "String";
            _SkontrobanOsszesen.Name = "EREC_UgyUgyiratok.SkontrobanOsszesen";
            _SkontrobanOsszesen.Type = "Int32";
            _MegorzesiIdoVege.Name = "EREC_UgyUgyiratok.MegorzesiIdoVege";
            _MegorzesiIdoVege.Type = "DateTime";
            _Partner_Id_Ugyindito.Name = "EREC_UgyUgyiratok.Partner_Id_Ugyindito";
            _Partner_Id_Ugyindito.Type = "Guid";
            _NevSTR_Ugyindito.Name = "EREC_UgyUgyiratok.NevSTR_Ugyindito";
            _NevSTR_Ugyindito.Type = "FTSString";
            _ElintezesDat.Name = "EREC_UgyUgyiratok.ElintezesDat";
            _ElintezesDat.Type = "DateTime";
            _FelhasznaloCsoport_Id_Ugyintez.Name = "EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez";
            _FelhasznaloCsoport_Id_Ugyintez.Type = "Guid";
            _Csoport_Id_Felelos_Elozo.Name = "EREC_UgyUgyiratok.Csoport_Id_Felelos_Elozo";
            _Csoport_Id_Felelos_Elozo.Type = "Guid";
            _KolcsonKikerDat.Name = "EREC_UgyUgyiratok.KolcsonKikerDat";
            _KolcsonKikerDat.Type = "DateTime";
            _KolcsonKiadDat.Name = "EREC_UgyUgyiratok.KolcsonKiadDat";
            _KolcsonKiadDat.Type = "DateTime";
            _Kolcsonhatarido.Name = "EREC_UgyUgyiratok.Kolcsonhatarido";
            _Kolcsonhatarido.Type = "DateTime";
            _BARCODE.Name = "EREC_UgyUgyiratok.BARCODE";
            _BARCODE.Type = "String";
            _IratMetadefinicio_Id.Name = "EREC_UgyUgyiratok.IratMetadefinicio_Id";
            _IratMetadefinicio_Id.Type = "Guid";
            _Allapot.Name = "EREC_UgyUgyiratok.Allapot";
            _Allapot.Type = "String";
            _TovabbitasAlattAllapot.Name = "EREC_UgyUgyiratok.TovabbitasAlattAllapot";
            _TovabbitasAlattAllapot.Type = "String";
            _Megjegyzes.Name = "EREC_UgyUgyiratok.Megjegyzes";
            _Megjegyzes.Type = "String";
            _Azonosito.Name = "EREC_UgyUgyiratok.Azonosito";
            _Azonosito.Type = "String";
            _Fizikai_Kezbesitesi_Allapot.Name = "EREC_UgyUgyiratok.Fizikai_Kezbesitesi_Allapot";
            _Fizikai_Kezbesitesi_Allapot.Type = "String";
            _Kovetkezo_Orzo_Id.Name = "EREC_UgyUgyiratok.Kovetkezo_Orzo_Id";
            _Kovetkezo_Orzo_Id.Type = "Guid";
            _Csoport_Id_Ugyfelelos.Name = "EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos";
            _Csoport_Id_Ugyfelelos.Type = "Guid";
            _Elektronikus_Kezbesitesi_Allap.Name = "EREC_UgyUgyiratok.Elektronikus_Kezbesitesi_Allap";
            _Elektronikus_Kezbesitesi_Allap.Type = "String";
            _Kovetkezo_Felelos_Id.Name = "EREC_UgyUgyiratok.Kovetkezo_Felelos_Id";
            _Kovetkezo_Felelos_Id.Type = "Guid";
            _UtolsoAlszam.Name = "EREC_UgyUgyiratok.UtolsoAlszam";
            _UtolsoAlszam.Type = "Int32";
            _UtolsoSorszam.Name = "EREC_UgyUgyiratok.UtolsoSorszam";
            _UtolsoSorszam.Type = "Int32";
            _IratSzam.Name = "EREC_UgyUgyiratok.IratSzam";
            _IratSzam.Type = "Int32";
            _ElintezesMod.Name = "EREC_UgyUgyiratok.ElintezesMod";
            _ElintezesMod.Type = "String";
            _RegirendszerIktatoszam.Name = "EREC_UgyUgyiratok.RegirendszerIktatoszam";
            _RegirendszerIktatoszam.Type = "String";
            _GeneraltTargy.Name = "EREC_UgyUgyiratok.GeneraltTargy";
            _GeneraltTargy.Type = "String";
            _Cim_Id_Ugyindito.Name = "EREC_UgyUgyiratok.Cim_Id_Ugyindito ";
            _Cim_Id_Ugyindito.Type = "Guid";
            _CimSTR_Ugyindito.Name = "EREC_UgyUgyiratok.CimSTR_Ugyindito ";
            _CimSTR_Ugyindito.Type = "String";
            _LezarasOka.Name = "EREC_UgyUgyiratok.LezarasOka";
            _LezarasOka.Type = "String";
            _FelfuggesztesOka.Name = "EREC_UgyUgyiratok.FelfuggesztesOka";
            _FelfuggesztesOka.Type = "String";
            _IrattarId.Name = "EREC_UgyUgyiratok.IrattarId";
            _IrattarId.Type = "Guid";
            _SkontroOka_Kod.Name = "EREC_UgyUgyiratok.SkontroOka_Kod";
            _SkontroOka_Kod.Type = "String";
            _UjOrzesiIdo.Name = "EREC_UgyUgyiratok.UjOrzesiIdo";
            _UjOrzesiIdo.Type = "Int32";
            _UjOrzesiIdoIdoegyseg.Name = "EREC_UgyUgyiratok.UjOrzesiIdoIdoegyseg";
            _UjOrzesiIdoIdoegyseg.Type = "String";
            _UgyintezesKezdete.Name = "EREC_UgyUgyiratok.UgyintezesKezdete";
            _UgyintezesKezdete.Type = "DateTime";
            _FelfuggesztettNapokSzama.Name = "EREC_UgyUgyiratok.FelfuggesztettNapokSzama";
            _FelfuggesztettNapokSzama.Type = "Int32";
            _IntezesiIdo.Name = "EREC_UgyUgyiratok.IntezesiIdo";
            _IntezesiIdo.Type = "String";
            _IntezesiIdoegyseg.Name = "EREC_UgyUgyiratok.IntezesiIdoegyseg";
            _IntezesiIdoegyseg.Type = "String";
            _Ugy_Fajtaja.Name = "EREC_UgyUgyiratok.Ugy_Fajtaja";
            _Ugy_Fajtaja.Type = "String";
            _SzignaloId.Name = "EREC_UgyUgyiratok.SzignaloId";
            _SzignaloId.Type = "Guid";
            _SzignalasIdeje.Name = "EREC_UgyUgyiratok.SzignalasIdeje";
            _SzignalasIdeje.Type = "DateTime";
            _ElteltIdo.Name = "EREC_UgyUgyiratok.ElteltIdo";
            _ElteltIdo.Type = "String";
            _ElteltIdoIdoEgyseg.Name = "EREC_UgyUgyiratok.ElteltIdoIdoEgyseg";
            _ElteltIdoIdoEgyseg.Type = "String";
            _ElteltidoAllapot.Name = "EREC_UgyUgyiratok.ElteltidoAllapot";
            _ElteltidoAllapot.Type = "Int32";
            _ElteltIdoUtolsoModositas.Name = "EREC_UgyUgyiratok.ElteltIdoUtolsoModositas";
            _ElteltIdoUtolsoModositas.Type = "DateTime";
            _SakkoraAllapot.Name = "EREC_UgyUgyiratok.SakkoraAllapot";
            _SakkoraAllapot.Type = "String";
            _HatralevoNapok.Name = "EREC_UgyUgyiratok.HatralevoNapok";
            _HatralevoNapok.Type = "Int32";
            _HatralevoMunkaNapok.Name = "EREC_UgyUgyiratok.HatralevoMunkaNapok";
            _HatralevoMunkaNapok.Type = "Int32";
            _ElozoAllapot.Name = "EREC_UgyUgyiratok.ElozoAllapot";
            _ElozoAllapot.Type = "String";
            _Aktiv.Name = "EREC_UgyUgyiratok.Aktiv";
            _Aktiv.Type = "Int32";
            _IrattarHelyfoglalas.Name = "EREC_UgyUgyiratok.IrattarHelyfoglalas";
            _IrattarHelyfoglalas.Type = "Double";
            _ErvKezd.Name = "EREC_UgyUgyiratok.ErvKezd";
            _ErvKezd.Type = "DateTime"; _ErvKezd.Operator = Query.Operators.lessorequal;
            _ErvKezd.Value = "getdate()";
            _ErvVege.Name = "EREC_UgyUgyiratok.ErvVege";
            _ErvVege.Type = "DateTime"; _ErvVege.Operator = Query.Operators.greaterorequal;
            _ErvVege.Value = "getdate()";
        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// Foszam property
        /// Iktat�si fosz�m
        /// </summary>
        private Field _Foszam = new Field();

        public Field Foszam
        {
            get { return _Foszam; }
            set { _Foszam = value; }
        }


        /// <summary>
        /// Sorszam property
        /// Munka�gy iktat�si sorsz�ma.
        /// </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }


        /// <summary>
        /// Ugyazonosito  property
        /// �gyazonos�t�, k�lso rendszerbol
        /// </summary>
        private Field _Ugyazonosito = new Field();

        public Field Ugyazonosito
        {
            get { return _Ugyazonosito; }
            set { _Ugyazonosito = value; }
        }


        /// <summary>
        /// Alkalmazas_Id property
        /// Alkalmazas_Id, k�lso rendszer Id-je
        /// </summary>
        private Field _Alkalmazas_Id = new Field();

        public Field Alkalmazas_Id
        {
            get { return _Alkalmazas_Id; }
            set { _Alkalmazas_Id = value; }
        }


        /// <summary>
        /// UgyintezesModja property
        /// KCS: UGYINTEZES_ALAPJA
        /// 0 - Hagyom�nyos, pap�r
        /// 1 - Elektronikus (nincs pap�r)
        /// </summary>
        private Field _UgyintezesModja = new Field();

        public Field UgyintezesModja
        {
            get { return _UgyintezesModja; }
            set { _UgyintezesModja = value; }
        }


        /// <summary>
        /// Hatarido property
        /// Elint�z�si hat�rido
        /// </summary>
        private Field _Hatarido = new Field();

        public Field Hatarido
        {
            get { return _Hatarido; }
            set { _Hatarido = value; }
        }


        /// <summary>
        /// SkontrobaDat property
        /// Skont�ba t�tel d�tuma
        /// </summary>
        private Field _SkontrobaDat = new Field();

        public Field SkontrobaDat
        {
            get { return _SkontrobaDat; }
            set { _SkontrobaDat = value; }
        }


        /// <summary>
        /// LezarasDat property
        /// Lez�r�s d�tuma
        /// </summary>
        private Field _LezarasDat = new Field();

        public Field LezarasDat
        {
            get { return _LezarasDat; }
            set { _LezarasDat = value; }
        }


        /// <summary>
        /// IrattarbaKuldDatuma  property
        /// Iratt�rba k�ld�s d�tuma 
        /// </summary>
        private Field _IrattarbaKuldDatuma = new Field();

        public Field IrattarbaKuldDatuma
        {
            get { return _IrattarbaKuldDatuma; }
            set { _IrattarbaKuldDatuma = value; }
        }


        /// <summary>
        /// IrattarbaVetelDat property
        /// Iratt�rba v�tel d�tuma
        /// </summary>
        private Field _IrattarbaVetelDat = new Field();

        public Field IrattarbaVetelDat
        {
            get { return _IrattarbaVetelDat; }
            set { _IrattarbaVetelDat = value; }
        }


        /// <summary>
        /// FelhCsoport_Id_IrattariAtvevo property
        /// Iratt�ri �tvevo Id-je
        /// </summary>
        private Field _FelhCsoport_Id_IrattariAtvevo = new Field();

        public Field FelhCsoport_Id_IrattariAtvevo
        {
            get { return _FelhCsoport_Id_IrattariAtvevo; }
            set { _FelhCsoport_Id_IrattariAtvevo = value; }
        }


        /// <summary>
        /// SelejtezesDat property
        /// Selejtez�s v. lev�lt�rba ad�s d�tuma
        /// </summary>
        private Field _SelejtezesDat = new Field();

        public Field SelejtezesDat
        {
            get { return _SelejtezesDat; }
            set { _SelejtezesDat = value; }
        }


        /// <summary>
        /// FelhCsoport_Id_Selejtezo property
        /// Selejtezo v. lev�lt�rba ad� felh. csoport Id-je
        /// </summary>
        private Field _FelhCsoport_Id_Selejtezo = new Field();

        public Field FelhCsoport_Id_Selejtezo
        {
            get { return _FelhCsoport_Id_Selejtezo; }
            set { _FelhCsoport_Id_Selejtezo = value; }
        }


        /// <summary>
        /// LeveltariAtvevoNeve property
        /// Lev�lt�ri �tvevo neve
        /// </summary>
        private Field _LeveltariAtvevoNeve = new Field();

        public Field LeveltariAtvevoNeve
        {
            get { return _LeveltariAtvevoNeve; }
            set { _LeveltariAtvevoNeve = value; }
        }


        /// <summary>
        /// FelhCsoport_Id_Felulvizsgalo property
        /// Fel�lvizsg�l� Id-je
        /// </summary>
        private Field _FelhCsoport_Id_Felulvizsgalo = new Field();

        public Field FelhCsoport_Id_Felulvizsgalo
        {
            get { return _FelhCsoport_Id_Felulvizsgalo; }
            set { _FelhCsoport_Id_Felulvizsgalo = value; }
        }


        /// <summary>
        /// FelulvizsgalatDat property
        /// Fel�lvizsg�lat d�tuma
        /// </summary>
        private Field _FelulvizsgalatDat = new Field();

        public Field FelulvizsgalatDat
        {
            get { return _FelulvizsgalatDat; }
            set { _FelulvizsgalatDat = value; }
        }


        /// <summary>
        /// IktatoszamKieg property
        /// ?? Az iktat�sz�m kieg�sz�to r�sze
        /// </summary>
        private Field _IktatoszamKieg = new Field();

        public Field IktatoszamKieg
        {
            get { return _IktatoszamKieg; }
            set { _IktatoszamKieg = value; }
        }


        /// <summary>
        /// Targy property
        /// Ugyirat t�rgya (le�r�sa)
        /// </summary>
        private Field _Targy = new Field();

        public Field Targy
        {
            get { return _Targy; }
            set { _Targy = value; }
        }


        /// <summary>
        /// UgyUgyirat_Id_Szulo property
        /// Szerelt �gyirat eset�n a befogad� (amibe a szerel�s t�rt�nt) �gyirat Id-je.
        /// </summary>
        private Field _UgyUgyirat_Id_Szulo = new Field();

        public Field UgyUgyirat_Id_Szulo
        {
            get { return _UgyUgyirat_Id_Szulo; }
            set { _UgyUgyirat_Id_Szulo = value; }
        }


        /// <summary>
        /// UgyUgyirat_Id_Kulso property
        /// A szerelt �gyiratok eset�ben a legf�lsobb szintu �gyirat azonos�t�ja. A nem szereltekn�l NULL.
        /// </summary>
        private Field _UgyUgyirat_Id_Kulso = new Field();

        public Field UgyUgyirat_Id_Kulso
        {
            get { return _UgyUgyirat_Id_Kulso; }
            set { _UgyUgyirat_Id_Kulso = value; }
        }


        /// <summary>
        /// UgyTipus property
        /// Az IratMetaDefinici� szerinti besorol�s.
        /// </summary>
        private Field _UgyTipus = new Field();

        public Field UgyTipus
        {
            get { return _UgyTipus; }
            set { _UgyTipus = value; }
        }


        /// <summary>
        /// IrattariHely property
        /// 
        /// </summary>
        private Field _IrattariHely = new Field();

        public Field IrattariHely
        {
            get { return _IrattariHely; }
            set { _IrattariHely = value; }
        }


        /// <summary>
        /// SztornirozasDat property
        /// Storn�roz�s d�tuma
        /// </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }


        /// <summary>
        /// Csoport_Id_Felelos property
        /// Helyette: a folyamatban o a k�v.felhaszn�l�, akinek ezzel az �ggyel kolga van. A fel�leten: kezelo.
        /// -- Tulajdonos... (iratt�rba ker�l�sn�l az iratt�rosra ker�l) -- 
        /// </summary>
        private Field _Csoport_Id_Felelos = new Field();

        public Field Csoport_Id_Felelos
        {
            get { return _Csoport_Id_Felelos; }
            set { _Csoport_Id_Felelos = value; }
        }


        /// <summary>
        /// FelhasznaloCsoport_Id_Orzo property
        /// �gyiratot orzo felhaszn�l� (akin�l van), fel�leten: az �gyirat helye.
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Orzo = new Field();

        public Field FelhasznaloCsoport_Id_Orzo
        {
            get { return _FelhasznaloCsoport_Id_Orzo; }
            set { _FelhasznaloCsoport_Id_Orzo = value; }
        }


        /// <summary>
        /// Csoport_Id_Cimzett property
        /// A j�v�hagy�st ig�nyl� folyamat c�mzettje
        /// </summary>
        private Field _Csoport_Id_Cimzett = new Field();

        public Field Csoport_Id_Cimzett
        {
            get { return _Csoport_Id_Cimzett; }
            set { _Csoport_Id_Cimzett = value; }
        }


        /// <summary>
        /// Jelleg property
        /// KCS: UGY_JELLEG
        /// </summary>
        private Field _Jelleg = new Field();

        public Field Jelleg
        {
            get { return _Jelleg; }
            set { _Jelleg = value; }
        }


        /// <summary>
        /// IraIrattariTetel_Id property
        /// Iratt�ri t�tel Id 
        /// </summary>
        private Field _IraIrattariTetel_Id = new Field();

        public Field IraIrattariTetel_Id
        {
            get { return _IraIrattariTetel_Id; }
            set { _IraIrattariTetel_Id = value; }
        }


        /// <summary>
        /// IraIktatokonyv_Id property
        /// IraIktatokonyv Id
        /// </summary>
        private Field _IraIktatokonyv_Id = new Field();

        public Field IraIktatokonyv_Id
        {
            get { return _IraIktatokonyv_Id; }
            set { _IraIktatokonyv_Id = value; }
        }


        /// <summary>
        /// SkontroOka property
        /// A skontr�ba helyez�s ok le�r�sa
        /// </summary>
        private Field _SkontroOka = new Field();

        public Field SkontroOka
        {
            get { return _SkontroOka; }
            set { _SkontroOka = value; }
        }


        /// <summary>
        /// SkontroVege property
        /// Skontr� v�ge d�tuma
        /// </summary>
        private Field _SkontroVege = new Field();

        public Field SkontroVege
        {
            get { return _SkontroVege; }
            set { _SkontroVege = value; }
        }


        /// <summary>
        /// Surgosseg property
        /// KCS: SURGOSSEG
        /// </summary>
        private Field _Surgosseg = new Field();

        public Field Surgosseg
        {
            get { return _Surgosseg; }
            set { _Surgosseg = value; }
        }


        /// <summary>
        /// SkontrobanOsszesen property
        /// Skont�ban t�lt�tt napok sz�ma
        /// </summary>
        private Field _SkontrobanOsszesen = new Field();

        public Field SkontrobanOsszesen
        {
            get { return _SkontrobanOsszesen; }
            set { _SkontrobanOsszesen = value; }
        }


        /// <summary>
        /// MegorzesiIdoVege property
        /// �gyirat megorz�se eddig a d�tumig
        /// </summary>
        private Field _MegorzesiIdoVege = new Field();

        public Field MegorzesiIdoVege
        {
            get { return _MegorzesiIdoVege; }
            set { _MegorzesiIdoVege = value; }
        }


        /// <summary>
        /// Partner_Id_Ugyindito property
        /// �gyind�t� partner (�gyf�l) Id-je
        /// </summary>
        private Field _Partner_Id_Ugyindito = new Field();

        public Field Partner_Id_Ugyindito
        {
            get { return _Partner_Id_Ugyindito; }
            set { _Partner_Id_Ugyindito = value; }
        }


        /// <summary>
        /// NevSTR_Ugyindito property
        /// Az �gyind�t�/bek�ldo neve
        /// </summary>
        private Field _NevSTR_Ugyindito = new Field();

        public Field NevSTR_Ugyindito
        {
            get { return _NevSTR_Ugyindito; }
            set { _NevSTR_Ugyindito = value; }
        }


        /// <summary>
        /// ElintezesDat property
        /// Ugyirat elintezesenek datuma.
        /// </summary>
        private Field _ElintezesDat = new Field();

        public Field ElintezesDat
        {
            get { return _ElintezesDat; }
            set { _ElintezesDat = value; }
        }


        /// <summary>
        /// FelhasznaloCsoport_Id_Ugyintez property
        /// Az �gyet fel�gyeli, az �rdemi munk�t �sszefogja. Fel�leten: �gyint�zo. T�vlatilag v�gig viszi az �gyet.
        /// Ha m�r iratt�rba ker�lt, itt l�that�, ki volt az �gyint�zo...
        /// </summary>
        private Field _FelhasznaloCsoport_Id_Ugyintez = new Field();

        public Field FelhasznaloCsoport_Id_Ugyintez
        {
            get { return _FelhasznaloCsoport_Id_Ugyintez; }
            set { _FelhasznaloCsoport_Id_Ugyintez = value; }
        }


        /// <summary>
        /// Csoport_Id_Felelos_Elozo property
        /// Elozo felelos CSOPORT Id. Szignalas - tovabbitas visszavonasakor tudni kell, ki volt elozoleg a felelos.
        /// </summary>
        private Field _Csoport_Id_Felelos_Elozo = new Field();

        public Field Csoport_Id_Felelos_Elozo
        {
            get { return _Csoport_Id_Felelos_Elozo; }
            set { _Csoport_Id_Felelos_Elozo = value; }
        }


        /// <summary>
        /// KolcsonKikerDat property
        /// �gyirat kik�r�si d�tuma
        /// </summary>
        private Field _KolcsonKikerDat = new Field();

        public Field KolcsonKikerDat
        {
            get { return _KolcsonKikerDat; }
            set { _KolcsonKikerDat = value; }
        }


        /// <summary>
        /// KolcsonKiadDat property
        /// �gyirat (k�lcs�nz�sre) kiad�si d�tuma 
        /// </summary>
        private Field _KolcsonKiadDat = new Field();

        public Field KolcsonKiadDat
        {
            get { return _KolcsonKiadDat; }
            set { _KolcsonKiadDat = value; }
        }


        /// <summary>
        /// Kolcsonhatarido property
        /// K�lcs�nz�si hat�rido
        /// </summary>
        private Field _Kolcsonhatarido = new Field();

        public Field Kolcsonhatarido
        {
            get { return _Kolcsonhatarido; }
            set { _Kolcsonhatarido = value; }
        }


        /// <summary>
        /// BARCODE property
        /// Vonalk�d �rt�ke
        /// </summary>
        private Field _BARCODE = new Field();

        public Field BARCODE
        {
            get { return _BARCODE; }
            set { _BARCODE = value; }
        }


        /// <summary>
        /// IratMetadefinicio_Id property
        /// Id
        /// </summary>
        private Field _IratMetadefinicio_Id = new Field();

        public Field IratMetadefinicio_Id
        {
            get { return _IratMetadefinicio_Id; }
            set { _IratMetadefinicio_Id = value; }
        }


        /// <summary>
        /// Allapot property
        /// KCS: UGYIRAT_ALLAPOT
        /// 0 - Megnyitott (munka�gy)
        /// 2 - Befagyasztott (munka�gy)
        /// 03	Szign�lt
        /// 04	Iktatott
        /// 06	�gyint�z�s alatt
        /// 07	Skontr�ban
        /// 09	Lez�rt
        /// 10	Iratt�rban orz�tt
        /// 11	Iratt�rba k�ld�tt (�ton l�vo)
        /// 13	K�lcs�nz�tt (k�zponti iratt�r)
        /// 30	Jegyz�kre helyezett
        /// 31	Lez�rt jegyz�kben l�vo
        /// 32	Selejtezett
        /// 33	Lev�lt�rba adott
        /// 50	Tov�bb�t�s alatt (�ton l�vo)
        /// 52	Iratt�roz�sra j�v�hagy�s alatt
        /// 55	Iratt�rb�l elk�rt (�ton l�vo)
        /// 56	Enged�lyezett kik�ron l�vo
        /// 57	Skontr�b�l elk�rt
        /// 60	Szerelt
        /// 70	Skontr�ba helyez�s j�v�hagy�sa
        /// 71	Skontr�b�l kik�rt
        /// 90	Sztorn�zott
        /// 99	Elint�zett
        /// 
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }


        /// <summary>
        /// TovabbitasAlattAllapot property
        /// KCS: UGYIRAT_ALLAPOT
        /// </summary>
        private Field _TovabbitasAlattAllapot = new Field();

        public Field TovabbitasAlattAllapot
        {
            get { return _TovabbitasAlattAllapot; }
            set { _TovabbitasAlattAllapot = value; }
        }


        /// <summary>
        /// Megjegyzes property
        /// Megjegyz�s
        /// </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }


        /// <summary>
        /// Azonosito property
        /// �gyirat azonos�t�: fosz�m, �v 
        /// </summary>
        private Field _Azonosito = new Field();

        public Field Azonosito
        {
            get { return _Azonosito; }
            set { _Azonosito = value; }
        }


        /// <summary>
        /// Fizikai_Kezbesitesi_Allapot property
        /// KCS: ??
        /// </summary>
        private Field _Fizikai_Kezbesitesi_Allapot = new Field();

        public Field Fizikai_Kezbesitesi_Allapot
        {
            get { return _Fizikai_Kezbesitesi_Allapot; }
            set { _Fizikai_Kezbesitesi_Allapot = value; }
        }


        /// <summary>
        /// Kovetkezo_Orzo_Id property
        /// 
        /// </summary>
        private Field _Kovetkezo_Orzo_Id = new Field();

        public Field Kovetkezo_Orzo_Id
        {
            get { return _Kovetkezo_Orzo_Id; }
            set { _Kovetkezo_Orzo_Id = value; }
        }


        /// <summary>
        /// Csoport_Id_Ugyfelelos property
        /// Fel�leten: �gyfelelos. 
        /// Az �gy jogi felelose, rendszerint az �gyoszt�ly felelos vezetoje. Hosszabb t�von �lland� marad.
        /// </summary>
        private Field _Csoport_Id_Ugyfelelos = new Field();

        public Field Csoport_Id_Ugyfelelos
        {
            get { return _Csoport_Id_Ugyfelelos; }
            set { _Csoport_Id_Ugyfelelos = value; }
        }


        /// <summary>
        /// Elektronikus_Kezbesitesi_Allap property
        /// 
        /// </summary>
        private Field _Elektronikus_Kezbesitesi_Allap = new Field();

        public Field Elektronikus_Kezbesitesi_Allap
        {
            get { return _Elektronikus_Kezbesitesi_Allap; }
            set { _Elektronikus_Kezbesitesi_Allap = value; }
        }


        /// <summary>
        /// Kovetkezo_Felelos_Id property
        /// 
        /// </summary>
        private Field _Kovetkezo_Felelos_Id = new Field();

        public Field Kovetkezo_Felelos_Id
        {
            get { return _Kovetkezo_Felelos_Id; }
            set { _Kovetkezo_Felelos_Id = value; }
        }


        /// <summary>
        /// UtolsoAlszam property
        /// Utols� iktat�si alsz�m
        /// </summary>
        private Field _UtolsoAlszam = new Field();

        public Field UtolsoAlszam
        {
            get { return _UtolsoAlszam; }
            set { _UtolsoAlszam = value; }
        }


        /// <summary>
        /// UtolsoSorszam property
        /// A �gyiratba (vagy munk�gyiratba) val� munkairat iktat�s utols� sorsz�ma.
        /// </summary>
        private Field _UtolsoSorszam = new Field();

        public Field UtolsoSorszam
        {
            get { return _UtolsoSorszam; }
            set { _UtolsoSorszam = value; }
        }


        /// <summary>
        /// IratSzam property
        /// Az �gyirathoz tartot� iratok aktu�lis sz�ma (az �tiktattott �s sztorn�zott iratok sz�m�val kevesebb, mint az utols� alsz�m).
        /// </summary>
        private Field _IratSzam = new Field();

        public Field IratSzam
        {
            get { return _IratSzam; }
            set { _IratSzam = value; }
        }


        /// <summary>
        /// ElintezesMod property
        /// KCS: ELINTEZESMOD
        /// </summary>
        private Field _ElintezesMod = new Field();

        public Field ElintezesMod
        {
            get { return _ElintezesMod; }
            set { _ElintezesMod = value; }
        }


        /// <summary>
        /// RegirendszerIktatoszam property
        /// A r�gi iktat�rendszerbol sz�rmaz� �gyirat sz�veges azonos�t�ja
        /// </summary>
        private Field _RegirendszerIktatoszam = new Field();

        public Field RegirendszerIktatoszam
        {
            get { return _RegirendszerIktatoszam; }
            set { _RegirendszerIktatoszam = value; }
        }


        /// <summary>
        /// GeneraltTargy property
        /// elore r�gz�tett sz�veg, t�rgy az iratmetadefici�k t�bl�b�l 
        /// </summary>
        private Field _GeneraltTargy = new Field();

        public Field GeneraltTargy
        {
            get { return _GeneraltTargy; }
            set { _GeneraltTargy = value; }
        }


        /// <summary>
        /// Cim_Id_Ugyindito  property
        /// 
        /// </summary>
        private Field _Cim_Id_Ugyindito = new Field();

        public Field Cim_Id_Ugyindito
        {
            get { return _Cim_Id_Ugyindito; }
            set { _Cim_Id_Ugyindito = value; }
        }


        /// <summary>
        /// CimSTR_Ugyindito  property
        /// 
        /// </summary>
        private Field _CimSTR_Ugyindito = new Field();

        public Field CimSTR_Ugyindito
        {
            get { return _CimSTR_Ugyindito; }
            set { _CimSTR_Ugyindito = value; }
        }


        /// <summary>
        /// LezarasOka property
        /// KCS: LEZARAS_OKA
        /// </summary>
        private Field _LezarasOka = new Field();

        public Field LezarasOka
        {
            get { return _LezarasOka; }
            set { _LezarasOka = value; }
        }


        /// <summary>
        /// FelfuggesztesOka property
        /// 
        /// </summary>
        private Field _FelfuggesztesOka = new Field();

        public Field FelfuggesztesOka
        {
            get { return _FelfuggesztesOka; }
            set { _FelfuggesztesOka = value; }
        }


        /// <summary>
        /// IrattarId property
        /// 
        /// </summary>
        private Field _IrattarId = new Field();

        public Field IrattarId
        {
            get { return _IrattarId; }
            set { _IrattarId = value; }
        }


        /// <summary>
        /// SkontroOka_Kod property
        /// KCS: SKONTRO_OKA
        /// </summary>
        private Field _SkontroOka_Kod = new Field();

        public Field SkontroOka_Kod
        {
            get { return _SkontroOka_Kod; }
            set { _SkontroOka_Kod = value; }
        }


        /// <summary>
        /// UjOrzesiIdo property
        /// 
        /// </summary>
        private Field _UjOrzesiIdo = new Field();

        public Field UjOrzesiIdo
        {
            get { return _UjOrzesiIdo; }
            set { _UjOrzesiIdo = value; }
        }


        /// <summary>
        /// UjOrzesiIdoIdoegyseg property
        /// KCS:IDOEGYSEG
        /// </summary>
        private Field _UjOrzesiIdoIdoegyseg = new Field();

        public Field UjOrzesiIdoIdoegyseg
        {
            get { return _UjOrzesiIdoIdoegyseg; }
            set { _UjOrzesiIdoIdoegyseg = value; }
        }


        /// <summary>
        /// UgyintezesKezdete property
        /// 
        /// </summary>
        private Field _UgyintezesKezdete = new Field();

        public Field UgyintezesKezdete
        {
            get { return _UgyintezesKezdete; }
            set { _UgyintezesKezdete = value; }
        }


        /// <summary>
        /// FelfuggesztettNapokSzama property
        /// 
        /// </summary>
        private Field _FelfuggesztettNapokSzama = new Field();

        public Field FelfuggesztettNapokSzama
        {
            get { return _FelfuggesztettNapokSzama; }
            set { _FelfuggesztettNapokSzama = value; }
        }


        /// <summary>
        /// IntezesiIdo property
        /// �tfut�si ido a megadott idoegys�gben (�gyirat, elj�r�si szakasz, irat)
        /// </summary>
        private Field _IntezesiIdo = new Field();

        public Field IntezesiIdo
        {
            get { return _IntezesiIdo; }
            set { _IntezesiIdo = value; }
        }


        /// <summary>
        /// IntezesiIdoegyseg property
        /// Az �tfut�si ido idoegys�ge. KCS: IDOEGYSEG
        /// (perc, �ra, nap, ...)
        /// A k�d egyben az idoegys�g percben megadott �rt�ke.
        /// 1- perc
        /// 60 - �ra
        /// 1440 - nap
        /// </summary>
        private Field _IntezesiIdoegyseg = new Field();

        public Field IntezesiIdoegyseg
        {
            get { return _IntezesiIdoegyseg; }
            set { _IntezesiIdoegyseg = value; }
        }


        /// <summary>
        /// Ugy_Fajtaja property
        /// 
        /// </summary>
        private Field _Ugy_Fajtaja = new Field();

        public Field Ugy_Fajtaja
        {
            get { return _Ugy_Fajtaja; }
            set { _Ugy_Fajtaja = value; }
        }


        /// <summary>
        /// SzignaloId property
        /// 
        /// </summary>
        private Field _SzignaloId = new Field();

        public Field SzignaloId
        {
            get { return _SzignaloId; }
            set { _SzignaloId = value; }
        }


        /// <summary>
        /// SzignalasIdeje property
        /// 
        /// </summary>
        private Field _SzignalasIdeje = new Field();

        public Field SzignalasIdeje
        {
            get { return _SzignalasIdeje; }
            set { _SzignalasIdeje = value; }
        }


        /// <summary>
        /// ElteltIdo property
        /// 
        /// </summary>
        private Field _ElteltIdo = new Field();

        public Field ElteltIdo
        {
            get { return _ElteltIdo; }
            set { _ElteltIdo = value; }
        }


        /// <summary>
        /// ElteltIdoIdoEgyseg property
        /// 
        /// </summary>
        private Field _ElteltIdoIdoEgyseg = new Field();

        public Field ElteltIdoIdoEgyseg
        {
            get { return _ElteltIdoIdoEgyseg; }
            set { _ElteltIdoIdoEgyseg = value; }
        }


        /// <summary>
        /// ElteltidoAllapot property
        /// 
        /// </summary>
        private Field _ElteltidoAllapot = new Field();

        public Field ElteltidoAllapot
        {
            get { return _ElteltidoAllapot; }
            set { _ElteltidoAllapot = value; }
        }


        /// <summary>
        /// ElteltIdoUtolsoModositas property
        /// 
        /// </summary>
        private Field _ElteltIdoUtolsoModositas = new Field();

        public Field ElteltIdoUtolsoModositas
        {
            get { return _ElteltIdoUtolsoModositas; }
            set { _ElteltIdoUtolsoModositas = value; }
        }


        /// <summary>
        /// SakkoraAllapot property
        /// 
        /// </summary>
        private Field _SakkoraAllapot = new Field();

        public Field SakkoraAllapot
        {
            get { return _SakkoraAllapot; }
            set { _SakkoraAllapot = value; }
        }


        /// <summary>
        /// HatralevoNapok property
        /// 
        /// </summary>
        private Field _HatralevoNapok = new Field();

        public Field HatralevoNapok
        {
            get { return _HatralevoNapok; }
            set { _HatralevoNapok = value; }
        }


        /// <summary>
        /// HatralevoMunkaNapok property
        /// 
        /// </summary>
        private Field _HatralevoMunkaNapok = new Field();

        public Field HatralevoMunkaNapok
        {
            get { return _HatralevoMunkaNapok; }
            set { _HatralevoMunkaNapok = value; }
        }


        /// <summary>
        /// ElozoAllapot property
        /// KCS: UGYIRAT_ALLAPOT
        /// 0 - Megnyitott (munka�gy)
        /// 2 - Befagyasztott (munka�gy)
        /// 03	Szign�lt
        /// 04	Iktatott
        /// 06	�gyint�z�s alatt
        /// 07	Skontr�ban
        /// 09	Lez�rt
        /// 10	Iratt�rban orz�tt
        /// 11	Iratt�rba k�ld�tt (�ton l�vo)
        /// 13	K�lcs�nz�tt (k�zponti iratt�r)
        /// 30	Jegyz�kre helyezett
        /// 31	Lez�rt jegyz�kben l�vo
        /// 32	Selejtezett
        /// 33	Lev�lt�rba adott
        /// 50	Tov�bb�t�s alatt (�ton l�vo)
        /// 52	Iratt�roz�sra j�v�hagy�s alatt
        /// 55	Iratt�rb�l elk�rt (�ton l�vo)
        /// 56	Enged�lyezett kik�ron l�vo
        /// 57	Skontr�b�l elk�rt
        /// 60	Szerelt
        /// 70	Skontr�ba helyez�s j�v�hagy�sa
        /// 71	Skontr�b�l kik�rt
        /// 90	Sztorn�zott
        /// 99	Elint�zett
        /// 
        /// </summary>
        private Field _ElozoAllapot = new Field();

        public Field ElozoAllapot
        {
            get { return _ElozoAllapot; }
            set { _ElozoAllapot = value; }
        }


        /// <summary>
        /// Aktiv property
        /// 
        /// </summary>
        private Field _Aktiv = new Field();

        public Field Aktiv
        {
            get { return _Aktiv; }
            set { _Aktiv = value; }
        }

        /// <summary>
        /// IrattarHelyfoglalas property
        /// 
        /// </summary>
        private Field _IrattarHelyfoglalas = new Field();

        public Field IrattarHelyfoglalas
        {
            get { return _IrattarHelyfoglalas; }
            set { _IrattarHelyfoglalas = value; }
        }

        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }


        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
    }

}