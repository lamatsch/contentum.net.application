
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_UgyiratKapcsolatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_UgyiratKapcsolatokSearch: BaseSearchObject    {
      public EREC_UgyiratKapcsolatokSearch()
      {            
                     _Id.Name = "EREC_UgyiratKapcsolatok.Id";
               _Id.Type = "Guid";            
               _KapcsolatTipus.Name = "EREC_UgyiratKapcsolatok.KapcsolatTipus";
               _KapcsolatTipus.Type = "String";            
               _Leiras.Name = "EREC_UgyiratKapcsolatok.Leiras";
               _Leiras.Type = "String";            
               _Kezi.Name = "EREC_UgyiratKapcsolatok.Kezi";
               _Kezi.Type = "Char";            
               _Ugyirat_Ugyirat_Beepul.Name = "EREC_UgyiratKapcsolatok.Ugyirat_Ugyirat_Beepul";
               _Ugyirat_Ugyirat_Beepul.Type = "Guid";            
               _Ugyirat_Ugyirat_Felepul.Name = "EREC_UgyiratKapcsolatok.Ugyirat_Ugyirat_Felepul";
               _Ugyirat_Ugyirat_Felepul.Type = "Guid";            
               _ErvKezd.Name = "EREC_UgyiratKapcsolatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_UgyiratKapcsolatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KapcsolatTipus = new Field();

        public Field KapcsolatTipus
        {
            get { return _KapcsolatTipus; }
            set { _KapcsolatTipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kezi = new Field();

        public Field Kezi
        {
            get { return _Kezi; }
            set { _Kezi = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Ugyirat_Ugyirat_Beepul = new Field();

        public Field Ugyirat_Ugyirat_Beepul
        {
            get { return _Ugyirat_Ugyirat_Beepul; }
            set { _Ugyirat_Ugyirat_Beepul = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Ugyirat_Ugyirat_Felepul = new Field();

        public Field Ugyirat_Ugyirat_Felepul
        {
            get { return _Ugyirat_Ugyirat_Felepul; }
            set { _Ugyirat_Ugyirat_Felepul = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}