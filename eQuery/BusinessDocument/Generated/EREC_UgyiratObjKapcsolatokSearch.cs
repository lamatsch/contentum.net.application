
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_UgyiratObjKapcsolatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_UgyiratObjKapcsolatokSearch: BaseSearchObject    {
      public EREC_UgyiratObjKapcsolatokSearch()
      {            
                     _Id.Name = "EREC_UgyiratObjKapcsolatok.Id";
               _Id.Type = "Guid";            
               _KapcsolatTipus.Name = "EREC_UgyiratObjKapcsolatok.KapcsolatTipus";
               _KapcsolatTipus.Type = "String";            
               _Leiras.Name = "EREC_UgyiratObjKapcsolatok.Leiras";
               _Leiras.Type = "String";            
               _Kezi.Name = "EREC_UgyiratObjKapcsolatok.Kezi";
               _Kezi.Type = "Char";            
               _Obj_Id_Elozmeny.Name = "EREC_UgyiratObjKapcsolatok.Obj_Id_Elozmeny";
               _Obj_Id_Elozmeny.Type = "Guid";            
               _Obj_Tip_Id_Elozmeny.Name = "EREC_UgyiratObjKapcsolatok.Obj_Tip_Id_Elozmeny";
               _Obj_Tip_Id_Elozmeny.Type = "Guid";            
               _Obj_Type_Elozmeny.Name = "EREC_UgyiratObjKapcsolatok.Obj_Type_Elozmeny";
               _Obj_Type_Elozmeny.Type = "String";            
               _Obj_Id_Kapcsolt.Name = "EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt";
               _Obj_Id_Kapcsolt.Type = "Guid";            
               _Obj_Tip_Id_Kapcsolt.Name = "EREC_UgyiratObjKapcsolatok.Obj_Tip_Id_Kapcsolt";
               _Obj_Tip_Id_Kapcsolt.Type = "Guid";            
               _Obj_Type_Kapcsolt.Name = "EREC_UgyiratObjKapcsolatok.Obj_Type_Kapcsolt";
               _Obj_Type_Kapcsolt.Type = "String";            
               _ErvKezd.Name = "EREC_UgyiratObjKapcsolatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_UgyiratObjKapcsolatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KapcsolatTipus = new Field();

        public Field KapcsolatTipus
        {
            get { return _KapcsolatTipus; }
            set { _KapcsolatTipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kezi = new Field();

        public Field Kezi
        {
            get { return _Kezi; }
            set { _Kezi = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id_Elozmeny = new Field();

        public Field Obj_Id_Elozmeny
        {
            get { return _Obj_Id_Elozmeny; }
            set { _Obj_Id_Elozmeny = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Tip_Id_Elozmeny = new Field();

        public Field Obj_Tip_Id_Elozmeny
        {
            get { return _Obj_Tip_Id_Elozmeny; }
            set { _Obj_Tip_Id_Elozmeny = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Type_Elozmeny = new Field();

        public Field Obj_Type_Elozmeny
        {
            get { return _Obj_Type_Elozmeny; }
            set { _Obj_Type_Elozmeny = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id_Kapcsolt = new Field();

        public Field Obj_Id_Kapcsolt
        {
            get { return _Obj_Id_Kapcsolt; }
            set { _Obj_Id_Kapcsolt = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Tip_Id_Kapcsolt = new Field();

        public Field Obj_Tip_Id_Kapcsolt
        {
            get { return _Obj_Tip_Id_Kapcsolt; }
            set { _Obj_Tip_Id_Kapcsolt = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Type_Kapcsolt = new Field();

        public Field Obj_Type_Kapcsolt
        {
            get { return _Obj_Type_Kapcsolt; }
            set { _Obj_Type_Kapcsolt = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}