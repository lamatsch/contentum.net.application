
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_eBeadvanyCsatolmanyok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_eBeadvanyCsatolmanyokSearch: BaseSearchObject    {
      public EREC_eBeadvanyCsatolmanyokSearch()
      {            
                     _Id.Name = "EREC_eBeadvanyCsatolmanyok.Id";
               _Id.Type = "Guid";            
               _eBeadvany_Id.Name = "EREC_eBeadvanyCsatolmanyok.eBeadvany_Id";
               _eBeadvany_Id.Type = "Guid";            
               _Dokumentum_Id.Name = "EREC_eBeadvanyCsatolmanyok.Dokumentum_Id";
               _Dokumentum_Id.Type = "Guid";            
               _Nev.Name = "EREC_eBeadvanyCsatolmanyok.Nev";
               _Nev.Type = "String";            
               _ErvKezd.Name = "EREC_eBeadvanyCsatolmanyok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_eBeadvanyCsatolmanyok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// eBeadvany_Id property
        /// FK EREC_ElektronikusBeadvanyok.Id Elektronikus beadv�ny azonos�t�
        /// </summary>
        private Field _eBeadvany_Id = new Field();

        public Field eBeadvany_Id
        {
            get { return _eBeadvany_Id; }
            set { _eBeadvany_Id = value; }
        }
                          
           
        /// <summary>
        /// Dokumentum_Id property
        /// FK KRT_Dokumentumok.Id Dokumentum azonos�t�.
        /// </summary>
        private Field _Dokumentum_Id = new Field();

        public Field Dokumentum_Id
        {
            get { return _Dokumentum_Id; }
            set { _Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// A f�jl neve.
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}