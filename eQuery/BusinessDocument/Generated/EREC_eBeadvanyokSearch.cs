
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_eBeadvanyok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_eBeadvanyokSearch: BaseSearchObject    {
      public EREC_eBeadvanyokSearch()
      {            
                     _Id.Name = "EREC_eBeadvanyok.Id";
               _Id.Type = "Guid";            
               _Irany.Name = "EREC_eBeadvanyok.Irany";
               _Irany.Type = "Char";            
               _Allapot.Name = "EREC_eBeadvanyok.Allapot";
               _Allapot.Type = "String";            
               _KuldoRendszer.Name = "EREC_eBeadvanyok.KuldoRendszer";
               _KuldoRendszer.Type = "String";            
               _UzenetTipusa.Name = "EREC_eBeadvanyok.UzenetTipusa";
               _UzenetTipusa.Type = "String";            
               _FeladoTipusa.Name = "EREC_eBeadvanyok.FeladoTipusa";
               _FeladoTipusa.Type = "Int32";            
               _PartnerKapcsolatiKod.Name = "EREC_eBeadvanyok.PartnerKapcsolatiKod";
               _PartnerKapcsolatiKod.Type = "String";            
               _PartnerNev.Name = "EREC_eBeadvanyok.PartnerNev";
               _PartnerNev.Type = "String";            
               _PartnerEmail.Name = "EREC_eBeadvanyok.PartnerEmail";
               _PartnerEmail.Type = "String";            
               _PartnerRovidNev.Name = "EREC_eBeadvanyok.PartnerRovidNev";
               _PartnerRovidNev.Type = "String";            
               _PartnerMAKKod.Name = "EREC_eBeadvanyok.PartnerMAKKod";
               _PartnerMAKKod.Type = "String";            
               _PartnerKRID.Name = "EREC_eBeadvanyok.PartnerKRID";
               _PartnerKRID.Type = "String";            
               _Partner_Id.Name = "EREC_eBeadvanyok.Partner_Id";
               _Partner_Id.Type = "Guid";            
               _Cim_Id.Name = "EREC_eBeadvanyok.Cim_Id";
               _Cim_Id.Type = "Guid";            
               _KR_HivatkozasiSzam.Name = "EREC_eBeadvanyok.KR_HivatkozasiSzam";
               _KR_HivatkozasiSzam.Type = "String";            
               _KR_ErkeztetesiSzam.Name = "EREC_eBeadvanyok.KR_ErkeztetesiSzam";
               _KR_ErkeztetesiSzam.Type = "String";            
               _Contentum_HivatkozasiSzam.Name = "EREC_eBeadvanyok.Contentum_HivatkozasiSzam";
               _Contentum_HivatkozasiSzam.Type = "Guid";            
               _PR_HivatkozasiSzam.Name = "EREC_eBeadvanyok.PR_HivatkozasiSzam";
               _PR_HivatkozasiSzam.Type = "String";            
               _PR_ErkeztetesiSzam.Name = "EREC_eBeadvanyok.PR_ErkeztetesiSzam";
               _PR_ErkeztetesiSzam.Type = "String";            
               _KR_DokTipusHivatal.Name = "EREC_eBeadvanyok.KR_DokTipusHivatal";
               _KR_DokTipusHivatal.Type = "String";            
               _KR_DokTipusAzonosito.Name = "EREC_eBeadvanyok.KR_DokTipusAzonosito";
               _KR_DokTipusAzonosito.Type = "String";            
               _KR_DokTipusLeiras.Name = "EREC_eBeadvanyok.KR_DokTipusLeiras";
               _KR_DokTipusLeiras.Type = "String";            
               _KR_Megjegyzes.Name = "EREC_eBeadvanyok.KR_Megjegyzes";
               _KR_Megjegyzes.Type = "String";            
               _KR_ErvenyessegiDatum.Name = "EREC_eBeadvanyok.KR_ErvenyessegiDatum";
               _KR_ErvenyessegiDatum.Type = "DateTime";            
               _KR_ErkeztetesiDatum.Name = "EREC_eBeadvanyok.KR_ErkeztetesiDatum";
               _KR_ErkeztetesiDatum.Type = "DateTime";            
               _KR_FileNev.Name = "EREC_eBeadvanyok.KR_FileNev";
               _KR_FileNev.Type = "String";            
               _KR_Kezbesitettseg.Name = "EREC_eBeadvanyok.KR_Kezbesitettseg";
               _KR_Kezbesitettseg.Type = "Int32";            
               _KR_Idopecset.Name = "EREC_eBeadvanyok.KR_Idopecset";
               _KR_Idopecset.Type = "String";            
               _KR_Valasztitkositas.Name = "EREC_eBeadvanyok.KR_Valasztitkositas";
               _KR_Valasztitkositas.Type = "Char";            
               _KR_Valaszutvonal.Name = "EREC_eBeadvanyok.KR_Valaszutvonal";
               _KR_Valaszutvonal.Type = "Int32";            
               _KR_Rendszeruzenet.Name = "EREC_eBeadvanyok.KR_Rendszeruzenet";
               _KR_Rendszeruzenet.Type = "Char";            
               _KR_Tarterulet.Name = "EREC_eBeadvanyok.KR_Tarterulet";
               _KR_Tarterulet.Type = "Int32";            
               _KR_ETertiveveny.Name = "EREC_eBeadvanyok.KR_ETertiveveny";
               _KR_ETertiveveny.Type = "Char";            
               _KR_Lenyomat.Name = "EREC_eBeadvanyok.KR_Lenyomat";
               _KR_Lenyomat.Type = "String";            
               _KuldKuldemeny_Id.Name = "EREC_eBeadvanyok.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _IraIrat_Id.Name = "EREC_eBeadvanyok.IraIrat_Id";
               _IraIrat_Id.Type = "Guid";            
               _IratPeldany_Id.Name = "EREC_eBeadvanyok.IratPeldany_Id";
               _IratPeldany_Id.Type = "Guid";            
               _Cel.Name = "EREC_eBeadvanyok.Cel";
               _Cel.Type = "String";            
               _PR_Parameterek.Name = "EREC_eBeadvanyok.PR_Parameterek";
               _PR_Parameterek.Type = "String";            
               _KR_Fiok.Name = "EREC_eBeadvanyok.KR_Fiok";
               _KR_Fiok.Type = "String";            
               _FeldolgozasStatusz.Name = "EREC_eBeadvanyok.FeldolgozasStatusz";
               _FeldolgozasStatusz.Type = "Int32";            
               _FeldolgozasiHiba.Name = "EREC_eBeadvanyok.FeldolgozasiHiba";
               _FeldolgozasiHiba.Type = "String";            
               _ErvKezd.Name = "EREC_eBeadvanyok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_eBeadvanyok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";

            _LetrehozasIdo.Name = "EREC_eBeadvanyok.LetrehozasIdo";
            _LetrehozasIdo.Type = "DateTime";
        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Irany property
        /// bej�v�, vagy kimen� �zenet
        /// </summary>
        private Field _Irany = new Field();

        public Field Irany
        {
            get { return _Irany; }
            set { _Irany = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// FK KRT_Kodtarak, ahol a k�dcsoport neve: �eBEADVANY_ALLAPOT� Az Elektronikus beadv�ny �llapota.
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// KuldoRendszer property
        /// Az elektronikus beadv�nyt k�ld� rendszer (pl. �NYK, HivataliKapu, stb.)
        /// </summary>
        private Field _KuldoRendszer = new Field();

        public Field KuldoRendszer
        {
            get { return _KuldoRendszer; }
            set { _KuldoRendszer = value; }
        }
                          
           
        /// <summary>
        /// UzenetTipusa property
        /// UZENET_TIPUS k�dcsoport egy �rt�ke
        /// </summary>
        private Field _UzenetTipusa = new Field();

        public Field UzenetTipusa
        {
            get { return _UzenetTipusa; }
            set { _UzenetTipusa = value; }
        }
                          
           
        /// <summary>
        /// FeladoTipusa property
        /// Szervezet, szem�ly, stb.
        /// </summary>
        private Field _FeladoTipusa = new Field();

        public Field FeladoTipusa
        {
            get { return _FeladoTipusa; }
            set { _FeladoTipusa = value; }
        }
                          
           
        /// <summary>
        /// PartnerKapcsolatiKod property
        /// �llampolg�r felad� eset�n a kapcsolati k�d.
        /// </summary>
        private Field _PartnerKapcsolatiKod = new Field();

        public Field PartnerKapcsolatiKod
        {
            get { return _PartnerKapcsolatiKod; }
            set { _PartnerKapcsolatiKod = value; }
        }
                          
           
        /// <summary>
        /// PartnerNev property
        /// �llampolg�r teljes neve, vagy a Hivatal teljes neve.
        /// </summary>
        private Field _PartnerNev = new Field();

        public Field PartnerNev
        {
            get { return _PartnerNev; }
            set { _PartnerNev = value; }
        }
                          
           
        /// <summary>
        /// PartnerEmail property
        /// �llampolg�r email c�me-
        /// </summary>
        private Field _PartnerEmail = new Field();

        public Field PartnerEmail
        {
            get { return _PartnerEmail; }
            set { _PartnerEmail = value; }
        }
                          
           
        /// <summary>
        /// PartnerRovidNev property
        /// Hivatal r�vid neve.
        /// </summary>
        private Field _PartnerRovidNev = new Field();

        public Field PartnerRovidNev
        {
            get { return _PartnerRovidNev; }
            set { _PartnerRovidNev = value; }
        }
                          
           
        /// <summary>
        /// PartnerMAKKod property
        /// Hivatal M�K k�dja.
        /// </summary>
        private Field _PartnerMAKKod = new Field();

        public Field PartnerMAKKod
        {
            get { return _PartnerMAKKod; }
            set { _PartnerMAKKod = value; }
        }
                          
           
        /// <summary>
        /// PartnerKRID property
        /// Hivatal KapuRendszer azonos�t�ja.
        /// </summary>
        private Field _PartnerKRID = new Field();

        public Field PartnerKRID
        {
            get { return _PartnerKRID; }
            set { _PartnerKRID = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id property
        /// FK KRT_Partnerek
        /// </summary>
        private Field _Partner_Id = new Field();

        public Field Partner_Id
        {
            get { return _Partner_Id; }
            set { _Partner_Id = value; }
        }
                          
           
        /// <summary>
        /// Cim_Id property
        /// FK KRT_Cimek
        /// </summary>
        private Field _Cim_Id = new Field();

        public Field Cim_Id
        {
            get { return _Cim_Id; }
            set { _Cim_Id = value; }
        }
                          
           
        /// <summary>
        /// KR_HivatkozasiSzam property
        /// KapuRendszer el�zm�ny azonos�t�ja
        /// </summary>
        private Field _KR_HivatkozasiSzam = new Field();

        public Field KR_HivatkozasiSzam
        {
            get { return _KR_HivatkozasiSzam; }
            set { _KR_HivatkozasiSzam = value; }
        }
                          
           
        /// <summary>
        /// KR_ErkeztetesiSzam property
        /// KapuRendszer iktat�sz�ma
        /// </summary>
        private Field _KR_ErkeztetesiSzam = new Field();

        public Field KR_ErkeztetesiSzam
        {
            get { return _KR_ErkeztetesiSzam; }
            set { _KR_ErkeztetesiSzam = value; }
        }
                          
           
        /// <summary>
        /// Contentum_HivatkozasiSzam property
        /// Contentum-os el�zm�ny azonos�t�ja (�gyirat ID)
        /// </summary>
        private Field _Contentum_HivatkozasiSzam = new Field();

        public Field Contentum_HivatkozasiSzam
        {
            get { return _Contentum_HivatkozasiSzam; }
            set { _Contentum_HivatkozasiSzam = value; }
        }
                          
           
        /// <summary>
        /// PR_HivatkozasiSzam property
        /// El�t�t (proxy) rendszer el�zm�ny azonos�t�ja
        /// </summary>
        private Field _PR_HivatkozasiSzam = new Field();

        public Field PR_HivatkozasiSzam
        {
            get { return _PR_HivatkozasiSzam; }
            set { _PR_HivatkozasiSzam = value; }
        }
                          
           
        /// <summary>
        /// PR_ErkeztetesiSzam property
        /// El�t�t (proxy) rendszer �rkeztet�si azonos�t�ja
        /// </summary>
        private Field _PR_ErkeztetesiSzam = new Field();

        public Field PR_ErkeztetesiSzam
        {
            get { return _PR_ErkeztetesiSzam; }
            set { _PR_ErkeztetesiSzam = value; }
        }
                          
           
        /// <summary>
        /// KR_DokTipusHivatal property
        /// KapuRendszer META adat
        /// </summary>
        private Field _KR_DokTipusHivatal = new Field();

        public Field KR_DokTipusHivatal
        {
            get { return _KR_DokTipusHivatal; }
            set { _KR_DokTipusHivatal = value; }
        }
                          
           
        /// <summary>
        /// KR_DokTipusAzonosito property
        /// KapuRendszer META adat
        /// </summary>
        private Field _KR_DokTipusAzonosito = new Field();

        public Field KR_DokTipusAzonosito
        {
            get { return _KR_DokTipusAzonosito; }
            set { _KR_DokTipusAzonosito = value; }
        }
                          
           
        /// <summary>
        /// KR_DokTipusLeiras property
        /// KapuRendszer META adat
        /// </summary>
        private Field _KR_DokTipusLeiras = new Field();

        public Field KR_DokTipusLeiras
        {
            get { return _KR_DokTipusLeiras; }
            set { _KR_DokTipusLeiras = value; }
        }
                          
           
        /// <summary>
        /// KR_Megjegyzes property
        /// KapuRendszer META adat
        /// </summary>
        private Field _KR_Megjegyzes = new Field();

        public Field KR_Megjegyzes
        {
            get { return _KR_Megjegyzes; }
            set { _KR_Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// KR_ErvenyessegiDatum property
        /// KapuRendszer �ltal az �zenethez r�gz�tett �rv�nyess�gi d�tum.
        /// </summary>
        private Field _KR_ErvenyessegiDatum = new Field();

        public Field KR_ErvenyessegiDatum
        {
            get { return _KR_ErvenyessegiDatum; }
            set { _KR_ErvenyessegiDatum = value; }
        }
                          
           
        /// <summary>
        /// KR_ErkeztetesiDatum property
        /// Az �zenet KapuRendszer-ben t�rt�n� regisztr�l�s�nak id�pontja.
        /// </summary>
        private Field _KR_ErkeztetesiDatum = new Field();

        public Field KR_ErkeztetesiDatum
        {
            get { return _KR_ErkeztetesiDatum; }
            set { _KR_ErkeztetesiDatum = value; }
        }
                          
           
        /// <summary>
        /// KR_FileNev property
        /// KapuRendszer META adat, a csatolm�nyk�nt k�ld�tt f�jl neve
        /// </summary>
        private Field _KR_FileNev = new Field();

        public Field KR_FileNev
        {
            get { return _KR_FileNev; }
            set { _KR_FileNev = value; }
        }
                          
           
        /// <summary>
        /// KR_Kezbesitettseg property
        /// Az �zenet KapuRendszer beli k�zbes�tetts�g�nek st�tusza.
        /// </summary>
        private Field _KR_Kezbesitettseg = new Field();

        public Field KR_Kezbesitettseg
        {
            get { return _KR_Kezbesitettseg; }
            set { _KR_Kezbesitettseg = value; }
        }
                          
           
        /// <summary>
        /// KR_Idopecset property
        /// A KapuRendszerben az �zenethez rendelt id�pecs�t.
        /// </summary>
        private Field _KR_Idopecset = new Field();

        public Field KR_Idopecset
        {
            get { return _KR_Idopecset; }
            set { _KR_Idopecset = value; }
        }
                          
           
        /// <summary>
        /// KR_Valasztitkositas property
        /// A felad� k�ri-e a v�lasz �zenet titkos�t�s�t. Amennyiben igen, akkor a v�laszt csak a KR rendszerb�l let�lthet� publikus kulccsal titkos�tva lehet elk�ldeni.
        /// </summary>
        private Field _KR_Valasztitkositas = new Field();

        public Field KR_Valasztitkositas
        {
            get { return _KR_Valasztitkositas; }
            set { _KR_Valasztitkositas = value; }
        }
                          
           
        /// <summary>
        /// KR_Valaszutvonal property
        /// Az �rkeztet�st visszaigazol� v�lasz �zenet �tvonala (0-�rtes�t�si t�rhely, 1-k�zvetlen e-mail). A szervezetnek nem kell a v�lasz�tvonallal foglalkoznia. Az csak az �rkeztet�si visszaigazol�sra vonatkozik, melyet a KR v�gez.
        /// </summary>
        private Field _KR_Valaszutvonal = new Field();

        public Field KR_Valaszutvonal
        {
            get { return _KR_Valaszutvonal; }
            set { _KR_Valaszutvonal = value; }
        }
                          
           
        /// <summary>
        /// KR_Rendszeruzenet property
        /// Felhaszn�l� �ltal k�ld�tt dokumentum eset�n �rt�ke hamis (false), olvas�si visszaigazol�s eset�n igaz (true)
        /// </summary>
        private Field _KR_Rendszeruzenet = new Field();

        public Field KR_Rendszeruzenet
        {
            get { return _KR_Rendszeruzenet; }
            set { _KR_Rendszeruzenet = value; }
        }
                          
           
        /// <summary>
        /// KR_Tarterulet property
        /// �tmeneti vagy tart�s t�rb�l sz�rmazik az �zenet.
        /// </summary>
        private Field _KR_Tarterulet = new Field();

        public Field KR_Tarterulet
        {
            get { return _KR_Tarterulet; }
            set { _KR_Tarterulet = value; }
        }
                          
           
        /// <summary>
        /// KR_ETertiveveny property
        /// Kimen� �zenethez k�r�nk-e eTertivevenyt (jelenleg nincs haszn�latban, de kompatibilit�si okokb�l megtartjuk).
        /// </summary>
        private Field _KR_ETertiveveny = new Field();

        public Field KR_ETertiveveny
        {
            get { return _KR_ETertiveveny; }
            set { _KR_ETertiveveny = value; }
        }
                          
           
        /// <summary>
        /// KR_Lenyomat property
        /// A dokumentum SHA-256 lenyomata.
        /// </summary>
        private Field _KR_Lenyomat = new Field();

        public Field KR_Lenyomat
        {
            get { return _KR_Lenyomat; }
            set { _KR_Lenyomat = value; }
        }
                          
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// 
        /// </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// IraIrat_Id property
        /// 
        /// </summary>
        private Field _IraIrat_Id = new Field();

        public Field IraIrat_Id
        {
            get { return _IraIrat_Id; }
            set { _IraIrat_Id = value; }
        }
                          
           
        /// <summary>
        /// IratPeldany_Id property
        /// 
        /// </summary>
        private Field _IratPeldany_Id = new Field();

        public Field IratPeldany_Id
        {
            get { return _IratPeldany_Id; }
            set { _IratPeldany_Id = value; }
        }
                          
           
        /// <summary>
        /// Cel property
        /// Az automatikus feldolgoz�st seg�t� META adat, pl. NMHH szervezeti egys�g, vagy c�lrendszer (HAIR)
        /// </summary>
        private Field _Cel = new Field();

        public Field Cel
        {
            get { return _Cel; }
            set { _Cel = value; }
        }
                          
           
        /// <summary>
        /// PR_Parameterek property
        /// Elhisz param�terek
        /// </summary>
        private Field _PR_Parameterek = new Field();

        public Field PR_Parameterek
        {
            get { return _PR_Parameterek; }
            set { _PR_Parameterek = value; }
        }
                          
           
        /// <summary>
        /// KR_Fiok property
        /// Hivatali kapu fi�k: melyik fi�kkal lett let�ltve illetve elk�ldve az �zenet
        /// </summary>
        private Field _KR_Fiok = new Field();

        public Field KR_Fiok
        {
            get { return _KR_Fiok; }
            set { _KR_Fiok = value; }
        }
                          
           
        /// <summary>
        /// FeldolgozasStatusz property
        /// 
        /// </summary>
        private Field _FeldolgozasStatusz = new Field();

        public Field FeldolgozasStatusz
        {
            get { return _FeldolgozasStatusz; }
            set { _FeldolgozasStatusz = value; }
        }
                          
           
        /// <summary>
        /// FeldolgozasiHiba property
        /// 
        /// </summary>
        private Field _FeldolgozasiHiba = new Field();

        public Field FeldolgozasiHiba
        {
            get { return _FeldolgozasiHiba; }
            set { _FeldolgozasiHiba = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }


        private Field _LetrehozasIdo = new Field();

        public Field LetrehozasIdo
        {
            get { return _LetrehozasIdo; }
            set { _LetrehozasIdo = value; }
        }

    }

}