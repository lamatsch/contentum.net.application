
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_eMailBoritekCimei eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_eMailBoritekCimeiSearch: BaseSearchObject    {
      public EREC_eMailBoritekCimeiSearch()
      {            
                     _Id.Name = "EREC_eMailBoritekCimei.Id";
               _Id.Type = "Guid";            
               _eMailBoritek_Id.Name = "EREC_eMailBoritekCimei.eMailBoritek_Id";
               _eMailBoritek_Id.Type = "Guid";            
               _Sorszam.Name = "EREC_eMailBoritekCimei.Sorszam";
               _Sorszam.Type = "Int32";            
               _Tipus.Name = "EREC_eMailBoritekCimei.Tipus";
               _Tipus.Type = "String";            
               _MailCim.Name = "EREC_eMailBoritekCimei.MailCim";
               _MailCim.Type = "String";            
               _Partner_Id.Name = "EREC_eMailBoritekCimei.Partner_Id";
               _Partner_Id.Type = "Guid";            
               _ErvKezd.Name = "EREC_eMailBoritekCimei.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_eMailBoritekCimei.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _eMailBoritek_Id = new Field();

        public Field eMailBoritek_Id
        {
            get { return _eMailBoritek_Id; }
            set { _eMailBoritek_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MailCim = new Field();

        public Field MailCim
        {
            get { return _MailCim; }
            set { _MailCim = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Partner_Id = new Field();

        public Field Partner_Id
        {
            get { return _Partner_Id; }
            set { _Partner_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}