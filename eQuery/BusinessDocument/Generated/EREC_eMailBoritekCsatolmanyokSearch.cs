
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_eMailBoritekCsatolmanyok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_eMailBoritekCsatolmanyokSearch: BaseSearchObject    {
      public EREC_eMailBoritekCsatolmanyokSearch()
      {            
                     _Id.Name = "EREC_eMailBoritekCsatolmanyok.Id";
               _Id.Type = "Guid";            
               _eMailBoritek_Id.Name = "EREC_eMailBoritekCsatolmanyok.eMailBoritek_Id";
               _eMailBoritek_Id.Type = "Guid";            
               _Dokumentum_Id.Name = "EREC_eMailBoritekCsatolmanyok.Dokumentum_Id";
               _Dokumentum_Id.Type = "Guid";            
               _Nev.Name = "EREC_eMailBoritekCsatolmanyok.Nev";
               _Nev.Type = "String";            
               _Tomoritve.Name = "EREC_eMailBoritekCsatolmanyok.Tomoritve";
               _Tomoritve.Type = "Char";            
               _ErvKezd.Name = "EREC_eMailBoritekCsatolmanyok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_eMailBoritekCsatolmanyok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _eMailBoritek_Id = new Field();

        public Field eMailBoritek_Id
        {
            get { return _eMailBoritek_Id; }
            set { _eMailBoritek_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Dokumentum_Id = new Field();

        public Field Dokumentum_Id
        {
            get { return _Dokumentum_Id; }
            set { _Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tomoritve = new Field();

        public Field Tomoritve
        {
            get { return _Tomoritve; }
            set { _Tomoritve = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}