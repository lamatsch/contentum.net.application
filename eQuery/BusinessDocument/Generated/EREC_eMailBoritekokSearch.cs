
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_eMailBoritekok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class EREC_eMailBoritekokSearch: BaseSearchObject    {
      public EREC_eMailBoritekokSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "EREC_eMailBoritekok.Id";
               _Id.Type = "Guid";            
               _Felado.Name = "EREC_eMailBoritekok.Felado";
               _Felado.Type = "String";            
               _Cimzett.Name = "EREC_eMailBoritekok.Cimzett";
               _Cimzett.Type = "String";            
               _CC.Name = "EREC_eMailBoritekok.CC";
               _CC.Type = "String";            
               _Targy.Name = "EREC_eMailBoritekok.Targy";
               _Targy.Type = "String";            
               _FeladasDatuma.Name = "EREC_eMailBoritekok.FeladasDatuma";
               _FeladasDatuma.Type = "DateTime";            
               _ErkezesDatuma.Name = "EREC_eMailBoritekok.ErkezesDatuma";
               _ErkezesDatuma.Type = "DateTime";            
               _Fontossag.Name = "EREC_eMailBoritekok.Fontossag";
               _Fontossag.Type = "String";            
               _KuldKuldemeny_Id.Name = "EREC_eMailBoritekok.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _IraIrat_Id.Name = "EREC_eMailBoritekok.IraIrat_Id";
               _IraIrat_Id.Type = "Guid";            
               _DigitalisAlairas.Name = "EREC_eMailBoritekok.DigitalisAlairas";
               _DigitalisAlairas.Type = "String";            
               _Uzenet.Name = "EREC_eMailBoritekok.Uzenet";
               _Uzenet.Type = "String";            
               _EmailForras.Name = "EREC_eMailBoritekok.EmailForras";
               _EmailForras.Type = "String";            
               _EmailGuid.Name = "EREC_eMailBoritekok.EmailGuid";
               _EmailGuid.Type = "String";            
               _FeldolgozasIdo.Name = "EREC_eMailBoritekok.FeldolgozasIdo";
               _FeldolgozasIdo.Type = "DateTime";            
               _ForrasTipus.Name = "EREC_eMailBoritekok.ForrasTipus";
               _ForrasTipus.Type = "String";            
               _Allapot.Name = "EREC_eMailBoritekok.Allapot";
               _Allapot.Type = "String";            
               _ErvKezd.Name = "EREC_eMailBoritekok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_eMailBoritekok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Felado property
        /// 
        /// </summary>
        private Field _Felado = new Field();

        public Field Felado
        {
            get { return _Felado; }
            set { _Felado = value; }
        }
                          
           
        /// <summary>
        /// Cimzett property
        /// 
        /// </summary>
        private Field _Cimzett = new Field();

        public Field Cimzett
        {
            get { return _Cimzett; }
            set { _Cimzett = value; }
        }
                          
           
        /// <summary>
        /// CC property
        /// EMail CarbonCopy c�mzettjei
        /// </summary>
        private Field _CC = new Field();

        public Field CC
        {
            get { return _CC; }
            set { _CC = value; }
        }
                          
           
        /// <summary>
        /// Targy property
        /// Mail t�rgya, megnevez�se
        /// </summary>
        private Field _Targy = new Field();

        public Field Targy
        {
            get { return _Targy; }
            set { _Targy = value; }
        }
                          
           
        /// <summary>
        /// FeladasDatuma property
        /// Felad�s d�tuma
        /// </summary>
        private Field _FeladasDatuma = new Field();

        public Field FeladasDatuma
        {
            get { return _FeladasDatuma; }
            set { _FeladasDatuma = value; }
        }
                          
           
        /// <summary>
        /// ErkezesDatuma property
        /// Email �rkez�s d�tuma
        /// </summary>
        private Field _ErkezesDatuma = new Field();

        public Field ErkezesDatuma
        {
            get { return _ErkezesDatuma; }
            set { _ErkezesDatuma = value; }
        }
                          
           
        /// <summary>
        /// Fontossag property
        /// KCS: ??
        /// </summary>
        private Field _Fontossag = new Field();

        public Field Fontossag
        {
            get { return _Fontossag; }
            set { _Fontossag = value; }
        }
                          
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// K�ldem�ny Id-je
        /// </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// IraIrat_Id property
        /// 
        /// </summary>
        private Field _IraIrat_Id = new Field();

        public Field IraIrat_Id
        {
            get { return _IraIrat_Id; }
            set { _IraIrat_Id = value; }
        }
                          
           
        /// <summary>
        /// DigitalisAlairas property
        /// ??
        /// </summary>
        private Field _DigitalisAlairas = new Field();

        public Field DigitalisAlairas
        {
            get { return _DigitalisAlairas; }
            set { _DigitalisAlairas = value; }
        }
                          
           
        /// <summary>
        /// Uzenet property
        /// �zenet sz�vege
        /// </summary>
        private Field _Uzenet = new Field();

        public Field Uzenet
        {
            get { return _Uzenet; }
            set { _Uzenet = value; }
        }
                          
           
        /// <summary>
        /// EmailForras property
        /// 
        /// </summary>
        private Field _EmailForras = new Field();

        public Field EmailForras
        {
            get { return _EmailForras; }
            set { _EmailForras = value; }
        }
                          
           
        /// <summary>
        /// EmailGuid property
        /// 
        /// </summary>
        private Field _EmailGuid = new Field();

        public Field EmailGuid
        {
            get { return _EmailGuid; }
            set { _EmailGuid = value; }
        }
                          
           
        /// <summary>
        /// FeldolgozasIdo property
        /// Feldolgoz�s ideje
        /// </summary>
        private Field _FeldolgozasIdo = new Field();

        public Field FeldolgozasIdo
        {
            get { return _FeldolgozasIdo; }
            set { _FeldolgozasIdo = value; }
        }
                          
           
        /// <summary>
        /// ForrasTipus property
        /// KCS: EMAILFORRASTIPUS B-bels�, K: k�ls�
        /// </summary>
        private Field _ForrasTipus = new Field();

        public Field ForrasTipus
        {
            get { return _ForrasTipus; }
            set { _ForrasTipus = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// KCS: EMAILBORITEK_ALLAPOT T:t�rolt, F: feldolgozot
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}