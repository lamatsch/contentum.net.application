
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// EREC_eMailFiokok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class EREC_eMailFiokokSearch: BaseSearchObject    {
      public EREC_eMailFiokokSearch()
      {            
                     _Id.Name = "EREC_eMailFiokok.Id";
               _Id.Type = "Guid";            
               _Nev.Name = "EREC_eMailFiokok.Nev";
               _Nev.Type = "String";            
               _UserNev.Name = "EREC_eMailFiokok.UserNev";
               _UserNev.Type = "String";            
               _Csoportok_Id.Name = "EREC_eMailFiokok.Csoportok_Id";
               _Csoportok_Id.Type = "Guid";            
               _Jelszo.Name = "EREC_eMailFiokok.Jelszo";
               _Jelszo.Type = "String";            
               _Tipus.Name = "EREC_eMailFiokok.Tipus";
               _Tipus.Type = "Char";            
               _NapiMax.Name = "EREC_eMailFiokok.NapiMax";
               _NapiMax.Type = "Int32";            
               _NapiTeny.Name = "EREC_eMailFiokok.NapiTeny";
               _NapiTeny.Type = "Int32";            
               _EmailCim.Name = "EREC_eMailFiokok.EmailCim";
               _EmailCim.Type = "String";            
               _Modul_Id.Name = "EREC_eMailFiokok.Modul_Id";
               _Modul_Id.Type = "Guid";            
               _MappaNev.Name = "EREC_eMailFiokok.MappaNev";
               _MappaNev.Type = "String";            
               _ErvKezd.Name = "EREC_eMailFiokok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "EREC_eMailFiokok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UserNev = new Field();

        public Field UserNev
        {
            get { return _UserNev; }
            set { _UserNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoportok_Id = new Field();

        public Field Csoportok_Id
        {
            get { return _Csoportok_Id; }
            set { _Csoportok_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Jelszo = new Field();

        public Field Jelszo
        {
            get { return _Jelszo; }
            set { _Jelszo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _NapiMax = new Field();

        public Field NapiMax
        {
            get { return _NapiMax; }
            set { _NapiMax = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _NapiTeny = new Field();

        public Field NapiTeny
        {
            get { return _NapiTeny; }
            set { _NapiTeny = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _EmailCim = new Field();

        public Field EmailCim
        {
            get { return _EmailCim; }
            set { _EmailCim = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Modul_Id = new Field();

        public Field Modul_Id
        {
            get { return _Modul_Id; }
            set { _Modul_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MappaNev = new Field();

        public Field MappaNev
        {
            get { return _MappaNev; }
            set { _MappaNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}