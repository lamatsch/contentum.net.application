
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// Fact_Feladatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class Fact_FeladatokSearch: BaseSearchObject    {
      public Fact_FeladatokSearch()
      {            
         Init_ManualFields();
               
                     _FeladatFelelos_Id.Name = "Fact_Feladatok.FeladatFelelos_Id";
               _FeladatFelelos_Id.Type = "Int32";            
               _FrissitesDatum_Id.Name = "Fact_Feladatok.FrissitesDatum_Id";
               _FrissitesDatum_Id.Type = "Int32";            
               _FeladatTipus_Id.Name = "Fact_Feladatok.FeladatTipus_Id";
               _FeladatTipus_Id.Type = "Int32";            
               _FeladatStatusz_Id.Name = "Fact_Feladatok.FeladatStatusz_Id";
               _FeladatStatusz_Id.Type = "Int32";            
               _LejaratKategoria_Id.Name = "Fact_Feladatok.LejaratKategoria_Id";
               _LejaratKategoria_Id.Type = "Int32";            
               _Prioritas_Id.Name = "Fact_Feladatok.Prioritas_Id";
               _Prioritas_Id.Type = "Int32";            
               _Telelszam.Name = "Fact_Feladatok.Telelszam";
               _Telelszam.Type = "Int32";            
               _Obj_Id.Name = "Fact_Feladatok.Obj_Id";
               _Obj_Id.Type = "String";            
               _ETL_Load_Id.Name = "Fact_Feladatok.ETL_Load_Id";
               _ETL_Load_Id.Type = "Int32";            
               _LetrehozasIdo.Name = "Fact_Feladatok.LetrehozasIdo";
               _LetrehozasIdo.Type = "DateTime";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _FeladatFelelos_Id = new Field();

        public Field FeladatFelelos_Id
        {
            get { return _FeladatFelelos_Id; }
            set { _FeladatFelelos_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FrissitesDatum_Id = new Field();

        public Field FrissitesDatum_Id
        {
            get { return _FrissitesDatum_Id; }
            set { _FrissitesDatum_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FeladatTipus_Id = new Field();

        public Field FeladatTipus_Id
        {
            get { return _FeladatTipus_Id; }
            set { _FeladatTipus_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FeladatStatusz_Id = new Field();

        public Field FeladatStatusz_Id
        {
            get { return _FeladatStatusz_Id; }
            set { _FeladatStatusz_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LejaratKategoria_Id = new Field();

        public Field LejaratKategoria_Id
        {
            get { return _LejaratKategoria_Id; }
            set { _LejaratKategoria_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Prioritas_Id = new Field();

        public Field Prioritas_Id
        {
            get { return _Prioritas_Id; }
            set { _Prioritas_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Telelszam = new Field();

        public Field Telelszam
        {
            get { return _Telelszam; }
            set { _Telelszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ETL_Load_Id = new Field();

        public Field ETL_Load_Id
        {
            get { return _ETL_Load_Id; }
            set { _ETL_Load_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LetrehozasIdo = new Field();

        public Field LetrehozasIdo
        {
            get { return _LetrehozasIdo; }
            set { _LetrehozasIdo = value; }
        }
                              }

}