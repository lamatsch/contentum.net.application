
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// HKP_DokumentumAdatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class HKP_DokumentumAdatokSearch: BaseSearchObject    {
      public HKP_DokumentumAdatokSearch()
      {            
                     _Id.Name = "HKP_DokumentumAdatok.Id";
               _Id.Type = "Guid";            
               _Irany.Name = "HKP_DokumentumAdatok.Irany";
               _Irany.Type = "Int32";            
               _Allapot.Name = "HKP_DokumentumAdatok.Allapot";
               _Allapot.Type = "Int32";            
               _FeladoTipusa.Name = "HKP_DokumentumAdatok.FeladoTipusa";
               _FeladoTipusa.Type = "Int32";            
               _KapcsolatiKod.Name = "HKP_DokumentumAdatok.KapcsolatiKod";
               _KapcsolatiKod.Type = "String";            
               _Nev.Name = "HKP_DokumentumAdatok.Nev";
               _Nev.Type = "String";            
               _Email.Name = "HKP_DokumentumAdatok.Email";
               _Email.Type = "String";            
               _RovidNev.Name = "HKP_DokumentumAdatok.RovidNev";
               _RovidNev.Type = "String";            
               _MAKKod.Name = "HKP_DokumentumAdatok.MAKKod";
               _MAKKod.Type = "String";            
               _KRID.Name = "HKP_DokumentumAdatok.KRID";
               _KRID.Type = "Int32";            
               _ErkeztetesiSzam.Name = "HKP_DokumentumAdatok.ErkeztetesiSzam";
               _ErkeztetesiSzam.Type = "String";            
               _HivatkozasiSzam.Name = "HKP_DokumentumAdatok.HivatkozasiSzam";
               _HivatkozasiSzam.Type = "String";            
               _DokTipusHivatal.Name = "HKP_DokumentumAdatok.DokTipusHivatal";
               _DokTipusHivatal.Type = "String";            
               _DokTipusAzonosito.Name = "HKP_DokumentumAdatok.DokTipusAzonosito";
               _DokTipusAzonosito.Type = "String";            
               _DokTipusLeiras.Name = "HKP_DokumentumAdatok.DokTipusLeiras";
               _DokTipusLeiras.Type = "String";            
               _Megjegyzes.Name = "HKP_DokumentumAdatok.Megjegyzes";
               _Megjegyzes.Type = "String";            
               _FileNev.Name = "HKP_DokumentumAdatok.FileNev";
               _FileNev.Type = "String";            
               _ErvenyessegiDatum.Name = "HKP_DokumentumAdatok.ErvenyessegiDatum";
               _ErvenyessegiDatum.Type = "DateTime";            
               _ErkeztetesiDatum.Name = "HKP_DokumentumAdatok.ErkeztetesiDatum";
               _ErkeztetesiDatum.Type = "DateTime";            
               _Kezbesitettseg.Name = "HKP_DokumentumAdatok.Kezbesitettseg";
               _Kezbesitettseg.Type = "Int32";            
               _Idopecset.Name = "HKP_DokumentumAdatok.Idopecset";
               _Idopecset.Type = "String";            
               _ValaszTitkositas.Name = "HKP_DokumentumAdatok.ValaszTitkositas";
               _ValaszTitkositas.Type = "Char";            
               _ValaszUtvonal.Name = "HKP_DokumentumAdatok.ValaszUtvonal";
               _ValaszUtvonal.Type = "Int32";            
               _Rendszeruzenet.Name = "HKP_DokumentumAdatok.Rendszeruzenet";
               _Rendszeruzenet.Type = "Char";            
               _Tarterulet.Name = "HKP_DokumentumAdatok.Tarterulet";
               _Tarterulet.Type = "Int32";            
               _ETertiveveny.Name = "HKP_DokumentumAdatok.ETertiveveny";
               _ETertiveveny.Type = "Int32";            
               _Lenyomat.Name = "HKP_DokumentumAdatok.Lenyomat";
               _Lenyomat.Type = "String";            
               _Dokumentum_Id.Name = "HKP_DokumentumAdatok.Dokumentum_Id";
               _Dokumentum_Id.Type = "Guid";            
               _KuldKuldemeny_Id.Name = "HKP_DokumentumAdatok.KuldKuldemeny_Id";
               _KuldKuldemeny_Id.Type = "Guid";            
               _IraIrat_Id.Name = "HKP_DokumentumAdatok.IraIrat_Id";
               _IraIrat_Id.Type = "Guid";            
               _IratPeldany_Id.Name = "HKP_DokumentumAdatok.IratPeldany_Id";
               _IratPeldany_Id.Type = "Guid";            
               _Rendszer.Name = "HKP_DokumentumAdatok.Rendszer";
               _Rendszer.Type = "String";            
               _ErvKezd.Name = "HKP_DokumentumAdatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "HKP_DokumentumAdatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Irany property
        /// 0 - Bej�v�, 1 - Kimen�
        /// </summary>
        private Field _Irany = new Field();

        public Field Irany
        {
            get { return _Irany; }
            set { _Irany = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// 0 - Let�ltve
        /// 10 - Elk�ld�sre el�k�sz�tve
        /// 11 - �gyf�lkapunak elk�ldve
        /// 12 - C�mzett �tvette
        /// 13 - C�mzett nem vette �t
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// FeladoTipusa property
        /// 0-�llampolg�r, 1-Hivatal, 2 -Rendszer
        /// </summary>
        private Field _FeladoTipusa = new Field();

        public Field FeladoTipusa
        {
            get { return _FeladoTipusa; }
            set { _FeladoTipusa = value; }
        }
                          
           
        /// <summary>
        /// KapcsolatiKod property
        /// A felad� �llampolg�r kapcsolati k�dja
        /// K�ld�s eset�n a felad� azonos�t�ja.
        /// </summary>
        private Field _KapcsolatiKod = new Field();

        public Field KapcsolatiKod
        {
            get { return _KapcsolatiKod; }
            set { _KapcsolatiKod = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// A felad� �llampolg�r/hivatal teljes neve
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Email property
        /// A felad� �llampolg�r email c�me
        /// </summary>
        private Field _Email = new Field();

        public Field Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
                          
           
        /// <summary>
        /// RovidNev property
        /// A felad� hivatal r�vidneve
        /// </summary>
        private Field _RovidNev = new Field();

        public Field RovidNev
        {
            get { return _RovidNev; }
            set { _RovidNev = value; }
        }
                          
           
        /// <summary>
        /// MAKKod property
        /// A felad� hivatal M�K k�dja 
        /// </summary>
        private Field _MAKKod = new Field();

        public Field MAKKod
        {
            get { return _MAKKod; }
            set { _MAKKod = value; }
        }
                          
           
        /// <summary>
        /// KRID property
        /// A felad� hivatal KRID-je.
        /// K�ld�s eset�n a felad� azonos�t�ja.
        /// </summary>
        private Field _KRID = new Field();

        public Field KRID
        {
            get { return _KRID; }
            set { _KRID = value; }
        }
                          
           
        /// <summary>
        /// ErkeztetesiSzam property
        /// A dokumentumhoz a rendszer �ltal gener�lt �rkeztet�si sz�m
        /// Az �rkeztet�si sz�m fel�p�t�se a k�vetkez�: 9 jegy� KR ID azonos�t� +����HHNN��PP+ 6 jegy� foly� sz�msorozat
        /// </summary>
        private Field _ErkeztetesiSzam = new Field();

        public Field ErkeztetesiSzam
        {
            get { return _ErkeztetesiSzam; }
            set { _ErkeztetesiSzam = value; }
        }
                          
           
        /// <summary>
        /// HivatkozasiSzam property
        /// A dokumentum hivatkoz�si sz�ma. Kit�lt�tt, amennyiben van hivatkozott dokumentum. �rt�ke a hivatkozott dokumentum �rkeztet�si sz�ma. 
        /// Jelenleg felt�lt�si nyugt�kban, �s olvasatlan �zenetek visszaigazol�saiban van szerepe, illetve dokumentum felt�lt�skor.
        /// </summary>
        private Field _HivatkozasiSzam = new Field();

        public Field HivatkozasiSzam
        {
            get { return _HivatkozasiSzam; }
            set { _HivatkozasiSzam = value; }
        }
                          
           
        /// <summary>
        /// DokTipusHivatal property
        /// A dokumentumot kibocs�t� szervezet r�vid neve
        /// </summary>
        private Field _DokTipusHivatal = new Field();

        public Field DokTipusHivatal
        {
            get { return _DokTipusHivatal; }
            set { _DokTipusHivatal = value; }
        }
                          
           
        /// <summary>
        /// DokTipusAzonosito property
        /// A dokumentum t�pus�nak azonos�t�ja
        /// </summary>
        private Field _DokTipusAzonosito = new Field();

        public Field DokTipusAzonosito
        {
            get { return _DokTipusAzonosito; }
            set { _DokTipusAzonosito = value; }
        }
                          
           
        /// <summary>
        /// DokTipusLeiras property
        /// A dokumentum t�pus�nak megnevez�se
        /// </summary>
        private Field _DokTipusLeiras = new Field();

        public Field DokTipusLeiras
        {
            get { return _DokTipusLeiras; }
            set { _DokTipusLeiras = value; }
        }
                          
           
        /// <summary>
        /// Megjegyzes property
        /// Megjegyz�s
        /// </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// FileNev property
        /// A feladott f�jl (SOAP mell�kletben utaz�) neve
        /// </summary>
        private Field _FileNev = new Field();

        public Field FileNev
        {
            get { return _FileNev; }
            set { _FileNev = value; }
        }
                          
           
        /// <summary>
        /// ErvenyessegiDatum property
        /// A dokumentum t�rol�si ideje (ezen id�pontig t�rol�dik a BEDSZ-ben)
        /// </summary>
        private Field _ErvenyessegiDatum = new Field();

        public Field ErvenyessegiDatum
        {
            get { return _ErvenyessegiDatum; }
            set { _ErvenyessegiDatum = value; }
        }
                          
           
        /// <summary>
        /// ErkeztetesiDatum property
        /// A dokumentum �rkeztet�si (befogad�si) d�tuma
        /// </summary>
        private Field _ErkeztetesiDatum = new Field();

        public Field ErkeztetesiDatum
        {
            get { return _ErkeztetesiDatum; }
            set { _ErkeztetesiDatum = value; }
        }
                          
           
        /// <summary>
        /// Kezbesitettseg property
        /// A dokumentum k�zbes�tetts�g�nek jelz�je
        /// (0-K�zbes�t�sre v�r, 2-K�zbes�tetlen, 3-K�zbes�t�si nyugta).
        /// </summary>
        private Field _Kezbesitettseg = new Field();

        public Field Kezbesitettseg
        {
            get { return _Kezbesitettseg; }
            set { _Kezbesitettseg = value; }
        }
                          
           
        /// <summary>
        /// Idopecset property
        /// Az �rkeztet�st igazol� id�b�lyegz�s. Az id�b�lyeg a felk�ld�tt dokumentumb�l SHA1-es algoritmussal k�sz�lt lenyomatra ker�l. Az elem tartalmazza az id�b�lyeget base64 k�doltan, alkalmas szervezeti oldalon a dokumentum integrit�s�nak ellen�rz�s�re. Az id�b�lyegz�s az RFC 3161 szabv�nynak megfelel� form�tum�.
        /// </summary>
        private Field _Idopecset = new Field();

        public Field Idopecset
        {
            get { return _Idopecset; }
            set { _Idopecset = value; }
        }
                          
           
        /// <summary>
        /// ValaszTitkositas property
        /// A felad� k�rheti a szervezetet, hogy a v�laszt titkos�tva k�ri. 
        /// (false � nem kell titkos�tani, true � titkos�tani kell) 
        /// A titkos�t�s a felad� nyilv�nos kulcs�val t�rt�nik, amit az �gyf�l kapcsolati k�dj�val lehet lek�rdezni a kulcst�rb�l. 
        /// </summary>
        private Field _ValaszTitkositas = new Field();

        public Field ValaszTitkositas
        {
            get { return _ValaszTitkositas; }
            set { _ValaszTitkositas = value; }
        }
                          
           
        /// <summary>
        /// ValaszUtvonal property
        /// Az �rkeztet�st visszaigazol� v�lasz �zenet �tvonala (0-�rtes�t�si t�rhely, 1-k�zvetlen e-mail). 
        /// A szervezetnek nem kell a v�lasz�tvonallal foglalkoznia. Az csak az �rkeztet�si visszaigazol�sra vonatkozik, melyet a KR v�gez.
        /// </summary>
        private Field _ValaszUtvonal = new Field();

        public Field ValaszUtvonal
        {
            get { return _ValaszUtvonal; }
            set { _ValaszUtvonal = value; }
        }
                          
           
        /// <summary>
        /// Rendszeruzenet property
        /// Felhaszn�l� �ltal k�ld�tt dokumentum eset�n �rt�ke hamis (false), olvas�si visszaigazol�s eset�n igaz (true)
        /// </summary>
        private Field _Rendszeruzenet = new Field();

        public Field Rendszeruzenet
        {
            get { return _Rendszeruzenet; }
            set { _Rendszeruzenet = value; }
        }
                          
           
        /// <summary>
        /// Tarterulet property
        /// A T�R t�pusa (0-�tmeneti t�r)
        /// 
        /// Technikai param�ter, amely jelzi, hogy az adott �zenet ideiglenes, vagy tart�s t�rban tal�lhat�-e. Szervezetek eset�n �rt�ke mindig 0, azaz ideiglenes t�r.
        /// </summary>
        private Field _Tarterulet = new Field();

        public Field Tarterulet
        {
            get { return _Tarterulet; }
            set { _Tarterulet = value; }
        }
                          
           
        /// <summary>
        /// ETertiveveny property
        /// true/false, A visszaigazol�s, tipus�t tekintve ET�rtivev�ny legyen
        /// </summary>
        private Field _ETertiveveny = new Field();

        public Field ETertiveveny
        {
            get { return _ETertiveveny; }
            set { _ETertiveveny = value; }
        }
                          
           
        /// <summary>
        /// Lenyomat property
        /// A dokumentum SHA1 lenyomata. Nem k�telez�.
        /// </summary>
        private Field _Lenyomat = new Field();

        public Field Lenyomat
        {
            get { return _Lenyomat; }
            set { _Lenyomat = value; }
        }
                          
           
        /// <summary>
        /// Dokumentum_Id property
        /// A dokumentumhoz tartoz� f�jl.
        /// </summary>
        private Field _Dokumentum_Id = new Field();

        public Field Dokumentum_Id
        {
            get { return _Dokumentum_Id; }
            set { _Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// KuldKuldemeny_Id property
        /// �rkeztetett k�ldem�ny id-ja.
        /// </summary>
        private Field _KuldKuldemeny_Id = new Field();

        public Field KuldKuldemeny_Id
        {
            get { return _KuldKuldemeny_Id; }
            set { _KuldKuldemeny_Id = value; }
        }
                          
           
        /// <summary>
        /// IraIrat_Id property
        /// 
        /// </summary>
        private Field _IraIrat_Id = new Field();

        public Field IraIrat_Id
        {
            get { return _IraIrat_Id; }
            set { _IraIrat_Id = value; }
        }
                          
           
        /// <summary>
        /// IratPeldany_Id property
        /// Expedi�lt iratp�ld�ny azonos�t�ja.
        /// </summary>
        private Field _IratPeldany_Id = new Field();

        public Field IratPeldany_Id
        {
            get { return _IratPeldany_Id; }
            set { _IratPeldany_Id = value; }
        }
                          
           
        /// <summary>
        /// Rendszer property
        /// Melyik rendszer fel� tov�bb�tjuk az adott �zenetet:
        /// WEB - Contentum fel�leten jelen�tj�k meg
        /// HAIR - HAIR rendszernek �djuk �t lek�rdez�s hat�s�ra (csatolm�nnyal egy�tt)
        /// </summary>
        private Field _Rendszer = new Field();

        public Field Rendszer
        {
            get { return _Rendszer; }
            set { _Rendszer = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}