
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// INT_AutoEUzenetSzabalyNaplo eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class INT_AutoEUzenetSzabalyNaploSearch: BaseSearchObject    {
      public INT_AutoEUzenetSzabalyNaploSearch()
      {            
                     _Id.Name = "INT_AutoEUzenetSzabalyNaplo.Id";
               _Id.Type = "Guid";            
               _ESzabalyId.Name = "INT_AutoEUzenetSzabalyNaplo.ESzabalyId";
               _ESzabalyId.Type = "Guid";            
               _EUzenetId.Name = "INT_AutoEUzenetSzabalyNaplo.EUzenetId";
               _EUzenetId.Type = "Guid";            
               _EUzenetTipusId.Name = "INT_AutoEUzenetSzabalyNaplo.EUzenetTipusId";
               _EUzenetTipusId.Type = "String";            
               _Megjegyzes.Name = "INT_AutoEUzenetSzabalyNaplo.Megjegyzes";
               _Megjegyzes.Type = "String";            
               _ErvKezd.Name = "INT_AutoEUzenetSzabalyNaplo.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "INT_AutoEUzenetSzabalyNaplo.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// ESzabalyId property
        /// 
        /// </summary>
        private Field _ESzabalyId = new Field();

        public Field ESzabalyId
        {
            get { return _ESzabalyId; }
            set { _ESzabalyId = value; }
        }
                          
           
        /// <summary>
        /// EUzenetId property
        /// 
        /// </summary>
        private Field _EUzenetId = new Field();

        public Field EUzenetId
        {
            get { return _EUzenetId; }
            set { _EUzenetId = value; }
        }
                          
           
        /// <summary>
        /// EUzenetTipusId property
        /// 
        /// </summary>
        private Field _EUzenetTipusId = new Field();

        public Field EUzenetTipusId
        {
            get { return _EUzenetTipusId; }
            set { _EUzenetTipusId = value; }
        }
                          
           
        /// <summary>
        /// Megjegyzes property
        /// 
        /// </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// 
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}