
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// INT_AutoEUzenetSzabaly eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class INT_AutoEUzenetSzabalySearch: BaseSearchObject    {
      public INT_AutoEUzenetSzabalySearch()
      {            
                     _Id.Name = "INT_AutoEUzenetSzabaly.Id";
               _Id.Type = "Guid";            
               _Nev.Name = "INT_AutoEUzenetSzabaly.Nev";
               _Nev.Type = "String";            
               _Leiras.Name = "INT_AutoEUzenetSzabaly.Leiras";
               _Leiras.Type = "String";            
               _Szabaly.Name = "INT_AutoEUzenetSzabaly.Szabaly";
               _Szabaly.Type = "String";            
               _Sorrend.Name = "INT_AutoEUzenetSzabaly.Sorrend";
               _Sorrend.Type = "Int32";            
               _Tipus.Name = "INT_AutoEUzenetSzabaly.Tipus";
               _Tipus.Type = "String";            
               _Leallito.Name = "INT_AutoEUzenetSzabaly.Leallito";
               _Leallito.Type = "Char";            
               _ErvKezd.Name = "INT_AutoEUzenetSzabaly.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "INT_AutoEUzenetSzabaly.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// 
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Leiras property
        /// 
        /// </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Szabaly property
        /// 
        /// </summary>
        private Field _Szabaly = new Field();

        public Field Szabaly
        {
            get { return _Szabaly; }
            set { _Szabaly = value; }
        }
                          
           
        /// <summary>
        /// Sorrend property
        /// 
        /// </summary>
        private Field _Sorrend = new Field();

        public Field Sorrend
        {
            get { return _Sorrend; }
            set { _Sorrend = value; }
        }
                          
           
        /// <summary>
        /// Tipus property
        /// 
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Leallito property
        /// 
        /// </summary>
        private Field _Leallito = new Field();

        public Field Leallito
        {
            get { return _Leallito; }
            set { _Leallito = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// 
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}