
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// INT_ExternalIDs eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class INT_ExternalIDsSearch: BaseSearchObject    {
      public INT_ExternalIDsSearch()
      {            
                     _Id.Name = "INT_ExternalIDs.Id";
               _Id.Type = "Guid";            
               _Modul_Id.Name = "INT_ExternalIDs.Modul_Id";
               _Modul_Id.Type = "Guid";            
               _Edok_Type.Name = "INT_ExternalIDs.Edok_Type";
               _Edok_Type.Type = "String";            
               _Edok_Id.Name = "INT_ExternalIDs.Edok_Id";
               _Edok_Id.Type = "Guid";            
               _Edok_ExpiredDate.Name = "INT_ExternalIDs.Edok_ExpiredDate";
               _Edok_ExpiredDate.Type = "DateTime";            
               _External_Group.Name = "INT_ExternalIDs.External_Group";
               _External_Group.Type = "String";            
               _External_Id.Name = "INT_ExternalIDs.External_Id";
               _External_Id.Type = "Guid";            
               _Deleted.Name = "INT_ExternalIDs.Deleted";
               _Deleted.Type = "Char";            
               _LastSync.Name = "INT_ExternalIDs.LastSync";
               _LastSync.Type = "DateTime";            
               _State.Name = "INT_ExternalIDs.State";
               _State.Type = "String";            
               _ErvKezd.Name = "INT_ExternalIDs.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "INT_ExternalIDs.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Modul_Id property
        /// 
        /// </summary>
        private Field _Modul_Id = new Field();

        public Field Modul_Id
        {
            get { return _Modul_Id; }
            set { _Modul_Id = value; }
        }
                          
           
        /// <summary>
        /// Edok_Type property
        /// 
        /// </summary>
        private Field _Edok_Type = new Field();

        public Field Edok_Type
        {
            get { return _Edok_Type; }
            set { _Edok_Type = value; }
        }
                          
           
        /// <summary>
        /// Edok_Id property
        /// 
        /// </summary>
        private Field _Edok_Id = new Field();

        public Field Edok_Id
        {
            get { return _Edok_Id; }
            set { _Edok_Id = value; }
        }
                          
           
        /// <summary>
        /// Edok_ExpiredDate property
        /// 
        /// </summary>
        private Field _Edok_ExpiredDate = new Field();

        public Field Edok_ExpiredDate
        {
            get { return _Edok_ExpiredDate; }
            set { _Edok_ExpiredDate = value; }
        }
                          
           
        /// <summary>
        /// External_Group property
        /// 
        /// </summary>
        private Field _External_Group = new Field();

        public Field External_Group
        {
            get { return _External_Group; }
            set { _External_Group = value; }
        }
                          
           
        /// <summary>
        /// External_Id property
        /// 
        /// </summary>
        private Field _External_Id = new Field();

        public Field External_Id
        {
            get { return _External_Id; }
            set { _External_Id = value; }
        }
                          
           
        /// <summary>
        /// Deleted property
        /// 
        /// </summary>
        private Field _Deleted = new Field();

        public Field Deleted
        {
            get { return _Deleted; }
            set { _Deleted = value; }
        }
                          
           
        /// <summary>
        /// LastSync property
        /// 
        /// </summary>
        private Field _LastSync = new Field();

        public Field LastSync
        {
            get { return _LastSync; }
            set { _LastSync = value; }
        }
                          
           
        /// <summary>
        /// State property
        /// 
        /// </summary>
        private Field _State = new Field();

        public Field State
        {
            get { return _State; }
            set { _State = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}