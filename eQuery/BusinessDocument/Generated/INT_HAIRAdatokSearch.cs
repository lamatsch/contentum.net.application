
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// INT_HAIRAdatok eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class INT_HAIRAdatokSearch : BaseSearchObject
    {
        public INT_HAIRAdatokSearch()
        {
            _Id.Name = "INT_HAIRAdatok.Id";
            _Id.Type = "Guid";
            _Obj_Id.Name = "INT_HAIRAdatok.Obj_Id";
            _Obj_Id.Type = "Guid";
            _ObjTip_Id.Name = "INT_HAIRAdatok.ObjTip_Id";
            _ObjTip_Id.Type = "Guid";
            _Tipus.Name = "INT_HAIRAdatok.Tipus";
            _Tipus.Type = "String";
            _Ertek.Name = "INT_HAIRAdatok.Ertek";
            _Ertek.Type = "String";
            _ErvKezd.Name = "INT_HAIRAdatok.ErvKezd";
            _ErvKezd.Type = "DateTime"; _ErvKezd.Operator = Query.Operators.lessorequal;
            _ErvKezd.Value = "getdate()";
            _ErvVege.Name = "INT_HAIRAdatok.ErvVege";
            _ErvVege.Type = "DateTime"; _ErvVege.Operator = Query.Operators.greaterorequal;
            _ErvVege.Value = "getdate()";
            _Tranz_id.Name = "INT_HAIRAdatok.Tranz_id";
            _Tranz_id.Type = "Guid";
        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// Obj_Id property
        /// 
        /// </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }


        /// <summary>
        /// ObjTip_Id property
        /// 
        /// </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }


        /// <summary>
        /// Tipus property
        /// 
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }

        /// <summary>
        /// Ertek property
        /// 
        /// </summary>
        private Field _Ertek = new Field();

        public Field Ertek
        {
            get { return _Ertek; }
            set { _Ertek = value; }
        }

        /// <summary>
        /// ErvKezd property
        /// 
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }


        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }


        /// <summary>
        /// Tranz_id property
        /// 
        /// </summary>
        private Field _Tranz_id = new Field();

        public Field Tranz_id
        {
            get { return _Tranz_id; }
            set { _Tranz_id = value; }
        }
    }

}