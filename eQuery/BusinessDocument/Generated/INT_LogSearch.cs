
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// INT_Log eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class INT_LogSearch: BaseSearchObject    {
      public INT_LogSearch()
      {            
                     _Id.Name = "INT_Log.Id";
               _Id.Type = "Guid";            
               _Org.Name = "INT_Log.Org";
               _Org.Type = "Guid";            
               _Modul_id.Name = "INT_Log.Modul_id";
               _Modul_id.Type = "Guid";            
               _Sync_StartDate.Name = "INT_Log.Sync_StartDate";
               _Sync_StartDate.Type = "DateTime";            
               _Parancs.Name = "INT_Log.Parancs";
               _Parancs.Type = "String";            
               _Machine.Name = "INT_Log.Machine";
               _Machine.Type = "String";            
               _Sync_EndDate.Name = "INT_Log.Sync_EndDate";
               _Sync_EndDate.Type = "DateTime";            
               _HibaKod.Name = "INT_Log.HibaKod";
               _HibaKod.Type = "String";            
               _HibaUzenet.Name = "INT_Log.HibaUzenet";
               _HibaUzenet.Type = "String";            
               _ExternalId.Name = "INT_Log.ExternalId";
               _ExternalId.Type = "String";            
               _Status.Name = "INT_Log.Status";
               _Status.Type = "String";            
               _Dokumentumok_Id_Sent.Name = "INT_Log.Dokumentumok_Id_Sent";
               _Dokumentumok_Id_Sent.Type = "Guid";            
               _Dokumentumok_Id_Error.Name = "INT_Log.Dokumentumok_Id_Error";
               _Dokumentumok_Id_Error.Type = "Guid";            
               _PackageHash.Name = "INT_Log.PackageHash";
               _PackageHash.Type = "String";            
               _PackageVer.Name = "INT_Log.PackageVer";
               _PackageVer.Type = "Int32";            
               _ErvKezd.Name = "INT_Log.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "INT_Log.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// 
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Modul_id property
        /// 
        /// </summary>
        private Field _Modul_id = new Field();

        public Field Modul_id
        {
            get { return _Modul_id; }
            set { _Modul_id = value; }
        }
                          
           
        /// <summary>
        /// Sync_StartDate property
        /// 
        /// </summary>
        private Field _Sync_StartDate = new Field();

        public Field Sync_StartDate
        {
            get { return _Sync_StartDate; }
            set { _Sync_StartDate = value; }
        }
                          
           
        /// <summary>
        /// Parancs property
        /// 
        /// </summary>
        private Field _Parancs = new Field();

        public Field Parancs
        {
            get { return _Parancs; }
            set { _Parancs = value; }
        }
                          
           
        /// <summary>
        /// Machine property
        /// 
        /// </summary>
        private Field _Machine = new Field();

        public Field Machine
        {
            get { return _Machine; }
            set { _Machine = value; }
        }
                          
           
        /// <summary>
        /// Sync_EndDate property
        /// 
        /// </summary>
        private Field _Sync_EndDate = new Field();

        public Field Sync_EndDate
        {
            get { return _Sync_EndDate; }
            set { _Sync_EndDate = value; }
        }
                          
           
        /// <summary>
        /// HibaKod property
        /// 
        /// </summary>
        private Field _HibaKod = new Field();

        public Field HibaKod
        {
            get { return _HibaKod; }
            set { _HibaKod = value; }
        }
                          
           
        /// <summary>
        /// HibaUzenet property
        /// 
        /// </summary>
        private Field _HibaUzenet = new Field();

        public Field HibaUzenet
        {
            get { return _HibaUzenet; }
            set { _HibaUzenet = value; }
        }
                          
           
        /// <summary>
        /// ExternalId property
        /// 
        /// </summary>
        private Field _ExternalId = new Field();

        public Field ExternalId
        {
            get { return _ExternalId; }
            set { _ExternalId = value; }
        }
                          
           
        /// <summary>
        /// Status property
        /// 
        /// </summary>
        private Field _Status = new Field();

        public Field Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
                          
           
        /// <summary>
        /// Dokumentumok_Id_Sent property
        /// 
        /// </summary>
        private Field _Dokumentumok_Id_Sent = new Field();

        public Field Dokumentumok_Id_Sent
        {
            get { return _Dokumentumok_Id_Sent; }
            set { _Dokumentumok_Id_Sent = value; }
        }
                          
           
        /// <summary>
        /// Dokumentumok_Id_Error property
        /// 
        /// </summary>
        private Field _Dokumentumok_Id_Error = new Field();

        public Field Dokumentumok_Id_Error
        {
            get { return _Dokumentumok_Id_Error; }
            set { _Dokumentumok_Id_Error = value; }
        }
                          
           
        /// <summary>
        /// PackageHash property
        /// 
        /// </summary>
        private Field _PackageHash = new Field();

        public Field PackageHash
        {
            get { return _PackageHash; }
            set { _PackageHash = value; }
        }
                          
           
        /// <summary>
        /// PackageVer property
        /// 
        /// </summary>
        private Field _PackageVer = new Field();

        public Field PackageVer
        {
            get { return _PackageVer; }
            set { _PackageVer = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// 
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}