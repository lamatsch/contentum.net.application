
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// INT_ManagedEmailAddress eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class INT_ManagedEmailAddressSearch: BaseSearchObject    {
      public INT_ManagedEmailAddressSearch()
      {            
                     _Id.Name = "INT_ManagedEmailAddress.Id";
               _Id.Type = "Guid";            
               _Org.Name = "INT_ManagedEmailAddress.Org";
               _Org.Type = "Guid";            
               _Felhasznalo_id.Name = "INT_ManagedEmailAddress.Felhasznalo_id";
               _Felhasznalo_id.Type = "Guid";            
               _Nev.Name = "INT_ManagedEmailAddress.Nev";
               _Nev.Type = "String";            
               _Ertek.Name = "INT_ManagedEmailAddress.Ertek";
               _Ertek.Type = "String";            
               _Karbantarthato.Name = "INT_ManagedEmailAddress.Karbantarthato";
               _Karbantarthato.Type = "Char";            
               _ErvKezd.Name = "INT_ManagedEmailAddress.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "INT_ManagedEmailAddress.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// 
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Felhasznalo_id property
        /// 
        /// </summary>
        private Field _Felhasznalo_id = new Field();

        public Field Felhasznalo_id
        {
            get { return _Felhasznalo_id; }
            set { _Felhasznalo_id = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// 
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Ertek property
        /// 
        /// </summary>
        private Field _Ertek = new Field();

        public Field Ertek
        {
            get { return _Ertek; }
            set { _Ertek = value; }
        }
                          
           
        /// <summary>
        /// Karbantarthato property
        /// 
        /// </summary>
        private Field _Karbantarthato = new Field();

        public Field Karbantarthato
        {
            get { return _Karbantarthato; }
            set { _Karbantarthato = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// 
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}