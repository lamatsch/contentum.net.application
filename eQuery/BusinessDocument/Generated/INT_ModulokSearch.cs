
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// INT_Modulok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class INT_ModulokSearch: BaseSearchObject    {
      public INT_ModulokSearch()
      {            
                     _Id.Name = "INT_Modulok.Id";
               _Id.Type = "Guid";            
               _Org.Name = "INT_Modulok.Org";
               _Org.Type = "Guid";            
               _Nev.Name = "INT_Modulok.Nev";
               _Nev.Type = "String";            
               _Statusz.Name = "INT_Modulok.Statusz";
               _Statusz.Type = "Char";            
               _Leiras.Name = "INT_Modulok.Leiras";
               _Leiras.Type = "String";            
               _Parancs.Name = "INT_Modulok.Parancs";
               _Parancs.Type = "String";            
			   _Gyakorisag.Name = "INT_Modulok.Gyakorisag";
               _Gyakorisag.Type = "Int32";
               _GyakorisagMertekegyseg.Name = "INT_Modulok.GyakorisagMertekegyseg";
               _GyakorisagMertekegyseg.Type = "String";            
               _UtolsoFutas.Name = "INT_Modulok.UtolsoFutas";
               _UtolsoFutas.Type = "DateTime";            
               _ErvKezd.Name = "INT_Modulok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "INT_Modulok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _Tranz_id.Name = "INT_Modulok.Tranz_id";
               _Tranz_id.Type = "Guid";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// 
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// 
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Statusz property
        /// 
        /// </summary>
        private Field _Statusz = new Field();

        public Field Statusz
        {
            get { return _Statusz; }
            set { _Statusz = value; }
        }
                          
           
        /// <summary>
        /// Leiras property
        /// 
        /// </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Parancs property
        /// 
        /// </summary>
        private Field _Parancs = new Field();

        public Field Parancs
        {
            get { return _Parancs; }
            set { _Parancs = value; }
        }
                          
		/// <summary>
        /// Gyakorisag property
        /// 
        /// </summary>
        private Field _Gyakorisag = new Field();

        public Field Gyakorisag
        {
            get { return _Gyakorisag; }
            set { _Gyakorisag = value; }
        }         
  
        /// <summary>
        /// GyakorisagMertekegyseg property
        /// 
        /// </summary>
        private Field _GyakorisagMertekegyseg = new Field();

        public Field GyakorisagMertekegyseg
        {
            get { return _GyakorisagMertekegyseg; }
            set { _GyakorisagMertekegyseg = value; }
        }
                          
           
        /// <summary>
        /// UtolsoFutas property
        /// 
        /// </summary>
        private Field _UtolsoFutas = new Field();

        public Field UtolsoFutas
        {
            get { return _UtolsoFutas; }
            set { _UtolsoFutas = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// 
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// 
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// Tranz_id property
        /// 
        /// </summary>
        private Field _Tranz_id = new Field();

        public Field Tranz_id
        {
            get { return _Tranz_id; }
            set { _Tranz_id = value; }
        }
                              }

}