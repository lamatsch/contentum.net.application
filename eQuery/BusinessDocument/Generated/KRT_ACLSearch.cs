
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_ACL eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_ACLSearch: BaseSearchObject    {
      public KRT_ACLSearch()
      {            
                     _Id.Name = "KRT_ACL.Id";
               _Id.Type = "Guid";            
               _Obj_Id.Name = "KRT_ACL.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTipus_Id.Name = "KRT_ACL.ObjTipus_Id";
               _ObjTipus_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_ACL.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_ACL.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTipus_Id = new Field();

        public Field ObjTipus_Id
        {
            get { return _ObjTipus_Id; }
            set { _ObjTipus_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}