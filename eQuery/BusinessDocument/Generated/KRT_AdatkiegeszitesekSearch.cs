
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Adatkiegeszitesek eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_AdatkiegeszitesekSearch
    {
      public KRT_AdatkiegeszitesekSearch()
      {               _Id.Name = "KRT_Adatkiegeszitesek.Id";
               _Id.Type = "Guid";            
               _ObjTip_Id.Name = "KRT_Adatkiegeszitesek.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Obj_Id.Name = "KRT_Adatkiegeszitesek.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id_Kapcsolt.Name = "KRT_Adatkiegeszitesek.ObjTip_Id_Kapcsolt";
               _ObjTip_Id_Kapcsolt.Type = "Guid";            
               _Obj_Id_Kapcsolt.Name = "KRT_Adatkiegeszitesek.Obj_Id_Kapcsolt";
               _Obj_Id_Kapcsolt.Type = "Guid";            
               _ObjTip_Id_Adat.Name = "KRT_Adatkiegeszitesek.ObjTip_Id_Adat";
               _ObjTip_Id_Adat.Type = "Guid";            
               _Obj_Id_Adat.Name = "KRT_Adatkiegeszitesek.Obj_Id_Adat";
               _Obj_Id_Adat.Type = "Guid";            
               _ErvKezd.Name = "KRT_Adatkiegeszitesek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Adatkiegeszitesek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id_Kapcsolt = new Field();

        public Field ObjTip_Id_Kapcsolt
        {
            get { return _ObjTip_Id_Kapcsolt; }
            set { _ObjTip_Id_Kapcsolt = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id_Kapcsolt = new Field();

        public Field Obj_Id_Kapcsolt
        {
            get { return _Obj_Id_Kapcsolt; }
            set { _Obj_Id_Kapcsolt = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id_Adat = new Field();

        public Field ObjTip_Id_Adat
        {
            get { return _ObjTip_Id_Adat; }
            set { _ObjTip_Id_Adat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id_Adat = new Field();

        public Field Obj_Id_Adat
        {
            get { return _Obj_Id_Adat; }
            set { _Obj_Id_Adat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}