
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Adatvaltozasok eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_AdatvaltozasokSearch
    {
      public KRT_AdatvaltozasokSearch()
      {               _Id.Name = "KRT_Adatvaltozasok.Id";
               _Id.Type = "Guid";            
               _Obj_Id.Name = "KRT_Adatvaltozasok.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTipus_Id_Tabla.Name = "KRT_Adatvaltozasok.ObjTipus_Id_Tabla";
               _ObjTipus_Id_Tabla.Type = "Guid";            
               _ObjTipus_Id_Oszlop.Name = "KRT_Adatvaltozasok.ObjTipus_Id_Oszlop";
               _ObjTipus_Id_Oszlop.Type = "Guid";            
               _RegiErtek.Name = "KRT_Adatvaltozasok.RegiErtek";
               _RegiErtek.Type = "String";            
               _UjErtek.Name = "KRT_Adatvaltozasok.UjErtek";
               _UjErtek.Type = "String";            
               _ErvKezd.Name = "KRT_Adatvaltozasok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Adatvaltozasok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTipus_Id_Tabla = new Field();

        public Field ObjTipus_Id_Tabla
        {
            get { return _ObjTipus_Id_Tabla; }
            set { _ObjTipus_Id_Tabla = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTipus_Id_Oszlop = new Field();

        public Field ObjTipus_Id_Oszlop
        {
            get { return _ObjTipus_Id_Oszlop; }
            set { _ObjTipus_Id_Oszlop = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _RegiErtek = new Field();

        public Field RegiErtek
        {
            get { return _RegiErtek; }
            set { _RegiErtek = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UjErtek = new Field();

        public Field UjErtek
        {
            get { return _UjErtek; }
            set { _UjErtek = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}