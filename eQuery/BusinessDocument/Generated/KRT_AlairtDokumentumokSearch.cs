
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_AlairtDokumentumok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_AlairtDokumentumokSearch: BaseSearchObject    {
      public KRT_AlairtDokumentumokSearch()
      {            
                     _Id.Name = "KRT_AlairtDokumentumok.Id";
               _Id.Type = "Guid";            
               _AlairasMod.Name = "KRT_AlairtDokumentumok.AlairasMod";
               _AlairasMod.Type = "String";            
               _AlairtFajlnev.Name = "KRT_AlairtDokumentumok.AlairtFajlnev";
               _AlairtFajlnev.Type = "String";            
               _AlairtTartalomHash.Name = "KRT_AlairtDokumentumok.AlairtTartalomHash";
               _AlairtTartalomHash.Type = "String";            
               _IratAlairasSzabaly_Id.Name = "KRT_AlairtDokumentumok.IratAlairasSzabaly_Id";
               _IratAlairasSzabaly_Id.Type = "Guid";            
               _AlairasRendben.Name = "KRT_AlairtDokumentumok.AlairasRendben";
               _AlairasRendben.Type = "Char";            
               _KivarasiIdoVege.Name = "KRT_AlairtDokumentumok.KivarasiIdoVege";
               _KivarasiIdoVege.Type = "DateTime";            
               _AlairasVeglegRendben.Name = "KRT_AlairtDokumentumok.AlairasVeglegRendben";
               _AlairasVeglegRendben.Type = "Char";            
               _Idopecset.Name = "KRT_AlairtDokumentumok.Idopecset";
               _Idopecset.Type = "DateTime";            
               _Csoport_Id_Alairo.Name = "KRT_AlairtDokumentumok.Csoport_Id_Alairo";
               _Csoport_Id_Alairo.Type = "Guid";            
               _AlairasTulajdonos.Name = "KRT_AlairtDokumentumok.AlairasTulajdonos";
               _AlairasTulajdonos.Type = "String";            
               _Tanusitvany_Id.Name = "KRT_AlairtDokumentumok.Tanusitvany_Id";
               _Tanusitvany_Id.Type = "Guid";            
               _External_Link.Name = "KRT_AlairtDokumentumok.External_Link";
               _External_Link.Type = "String";            
               _External_Id.Name = "KRT_AlairtDokumentumok.External_Id";
               _External_Id.Type = "Guid";            
               _External_Source.Name = "KRT_AlairtDokumentumok.External_Source";
               _External_Source.Type = "String";            
               _External_Info.Name = "KRT_AlairtDokumentumok.External_Info";
               _External_Info.Type = "String";            
               _Allapot.Name = "KRT_AlairtDokumentumok.Allapot";
               _Allapot.Type = "String";            
               _ErvKezd.Name = "KRT_AlairtDokumentumok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_AlairtDokumentumok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairasMod = new Field();

        public Field AlairasMod
        {
            get { return _AlairasMod; }
            set { _AlairasMod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairtFajlnev = new Field();

        public Field AlairtFajlnev
        {
            get { return _AlairtFajlnev; }
            set { _AlairtFajlnev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairtTartalomHash = new Field();

        public Field AlairtTartalomHash
        {
            get { return _AlairtTartalomHash; }
            set { _AlairtTartalomHash = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IratAlairasSzabaly_Id = new Field();

        public Field IratAlairasSzabaly_Id
        {
            get { return _IratAlairasSzabaly_Id; }
            set { _IratAlairasSzabaly_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairasRendben = new Field();

        public Field AlairasRendben
        {
            get { return _AlairasRendben; }
            set { _AlairasRendben = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KivarasiIdoVege = new Field();

        public Field KivarasiIdoVege
        {
            get { return _KivarasiIdoVege; }
            set { _KivarasiIdoVege = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairasVeglegRendben = new Field();

        public Field AlairasVeglegRendben
        {
            get { return _AlairasVeglegRendben; }
            set { _AlairasVeglegRendben = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Idopecset = new Field();

        public Field Idopecset
        {
            get { return _Idopecset; }
            set { _Idopecset = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Alairo = new Field();

        public Field Csoport_Id_Alairo
        {
            get { return _Csoport_Id_Alairo; }
            set { _Csoport_Id_Alairo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairasTulajdonos = new Field();

        public Field AlairasTulajdonos
        {
            get { return _AlairasTulajdonos; }
            set { _AlairasTulajdonos = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tanusitvany_Id = new Field();

        public Field Tanusitvany_Id
        {
            get { return _Tanusitvany_Id; }
            set { _Tanusitvany_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _External_Link = new Field();

        public Field External_Link
        {
            get { return _External_Link; }
            set { _External_Link = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _External_Id = new Field();

        public Field External_Id
        {
            get { return _External_Id; }
            set { _External_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _External_Source = new Field();

        public Field External_Source
        {
            get { return _External_Source; }
            set { _External_Source = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _External_Info = new Field();

        public Field External_Info
        {
            get { return _External_Info; }
            set { _External_Info = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}