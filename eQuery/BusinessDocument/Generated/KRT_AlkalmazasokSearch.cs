
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Alkalmazasok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_AlkalmazasokSearch: BaseSearchObject    {
      public KRT_AlkalmazasokSearch()
      {            
                     _Id.Name = "KRT_Alkalmazasok.Id";
               _Id.Type = "Guid";            
               _KRT_Id.Name = "KRT_Alkalmazasok.KRT_Id";
               _KRT_Id.Type = "Guid";            
               _Kod.Name = "KRT_Alkalmazasok.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_Alkalmazasok.Nev";
               _Nev.Type = "String";            
               _Kulso.Name = "KRT_Alkalmazasok.Kulso";
               _Kulso.Type = "Char";            
               _Org.Name = "KRT_Alkalmazasok.Org";
               _Org.Type = "Guid";            
               _ErvKezd.Name = "KRT_Alkalmazasok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Alkalmazasok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KRT_Id = new Field();

        public Field KRT_Id
        {
            get { return _KRT_Id; }
            set { _KRT_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kulso = new Field();

        public Field Kulso
        {
            get { return _Kulso; }
            set { _Kulso = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}