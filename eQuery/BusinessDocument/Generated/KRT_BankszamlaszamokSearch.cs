
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Bankszamlaszamok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_BankszamlaszamokSearch: BaseSearchObject    {
      public KRT_BankszamlaszamokSearch()
      {            
                     _Id.Name = "KRT_Bankszamlaszamok.Id";
               _Id.Type = "Guid";            
               _Partner_Id.Name = "KRT_Bankszamlaszamok.Partner_Id";
               _Partner_Id.Type = "Guid";            
               _Bankszamlaszam.Name = "KRT_Bankszamlaszamok.Bankszamlaszam";
               _Bankszamlaszam.Type = "String";            
               _Partner_Id_Bank.Name = "KRT_Bankszamlaszamok.Partner_Id_Bank";
               _Partner_Id_Bank.Type = "Guid";            
               _ErvKezd.Name = "KRT_Bankszamlaszamok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Bankszamlaszamok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonosító
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id property
        /// Partner id
        /// </summary>
        private Field _Partner_Id = new Field();

        public Field Partner_Id
        {
            get { return _Partner_Id; }
            set { _Partner_Id = value; }
        }
                          
           
        /// <summary>
        /// Bankszamlaszam property
        /// Bankszámlaszám
        /// </summary>
        private Field _Bankszamlaszam = new Field();

        public Field Bankszamlaszam
        {
            get { return _Bankszamlaszam; }
            set { _Bankszamlaszam = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id_Bank property
        /// Partner id
        /// </summary>
        private Field _Partner_Id_Bank = new Field();

        public Field Partner_Id_Bank
        {
            get { return _Partner_Id_Bank; }
            set { _Partner_Id_Bank = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}