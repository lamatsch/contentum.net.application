
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_BarkodSavok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_BarkodSavokSearch: BaseSearchObject    {
      public KRT_BarkodSavokSearch()
      {            
                     _Id.Name = "KRT_BarkodSavok.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_BarkodSavok.Org";
               _Org.Type = "Guid";            
               _Csoport_Id_Felelos.Name = "KRT_BarkodSavok.Csoport_Id_Felelos";
               _Csoport_Id_Felelos.Type = "Guid";            
               _SavKezd.Name = "KRT_BarkodSavok.SavKezd";
               _SavKezd.Type = "String";            
               _SavVege.Name = "KRT_BarkodSavok.SavVege";
               _SavVege.Type = "String";            
               _SavType.Name = "KRT_BarkodSavok.SavType";
               _SavType.Type = "Char";            
               _SavAllapot.Name = "KRT_BarkodSavok.SavAllapot";
               _SavAllapot.Type = "Char";            
               _ErvKezd.Name = "KRT_BarkodSavok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_BarkodSavok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonosító
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Felelos property
        /// Aki a vonalkód tartományt létrehozta, vagy akinél éppen van.
        /// </summary>
        private Field _Csoport_Id_Felelos = new Field();

        public Field Csoport_Id_Felelos
        {
            get { return _Csoport_Id_Felelos; }
            set { _Csoport_Id_Felelos = value; }
        }
                          
           
        /// <summary>
        /// SavKezd property
        /// 
        /// </summary>
        private Field _SavKezd = new Field();

        public Field SavKezd
        {
            get { return _SavKezd; }
            set { _SavKezd = value; }
        }
                          
           
        /// <summary>
        /// SavVege property
        /// 
        /// </summary>
        private Field _SavVege = new Field();

        public Field SavVege
        {
            get { return _SavVege; }
            set { _SavVege = value; }
        }
                          
           
        /// <summary>
        /// SavType property
        /// N,P
        /// </summary>
        private Field _SavType = new Field();

        public Field SavType
        {
            get { return _SavType; }
            set { _SavType = value; }
        }
                          
           
        /// <summary>
        /// SavAllapot property
        /// nyomtatás alatt, felhasználható, törölt
        /// </summary>
        private Field _SavAllapot = new Field();

        public Field SavAllapot
        {
            get { return _SavAllapot; }
            set { _SavAllapot = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}