
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// Az B�rk�ddal ell�tott objektum t�pusok, objektumok.
    /// B�rk�dnyilv�ntart�s egyik alap t�bl�ja....
    ///</summary>
    [Serializable()]    
        public class KRT_BarkodokSearch: BaseSearchObject    {
      public KRT_BarkodokSearch()
      {            
                     _Id.Name = "KRT_Barkodok.Id";
               _Id.Type = "Guid";            
               _Kod.Name = "KRT_Barkodok.Kod";
               _Kod.Type = "String";            
               _Obj_Id.Name = "KRT_Barkodok.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id.Name = "KRT_Barkodok.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Obj_type.Name = "KRT_Barkodok.Obj_type";
               _Obj_type.Type = "String";            
               _KodType.Name = "KRT_Barkodok.KodType";
               _KodType.Type = "Char";            
               _Allapot.Name = "KRT_Barkodok.Allapot";
               _Allapot.Type = "String";            
               _ErvKezd.Name = "KRT_Barkodok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Barkodok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _Tranz_id.Name = "KRT_Barkodok.Tranz_id";
               _Tranz_id.Type = "Guid";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        /// <summary>
        /// Egyedi azonos�t�
        /// </summary>
        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Kod property </summary>
        private Field _Kod = new Field();

        /// <summary>
        /// A b�rk�d azonos�t�ja.
        /// </summary>
        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Obj_Id property </summary>
        private Field _Obj_Id = new Field();

        /// <summary>
        /// A b�rk�ddal ell�tott objektum ID -ja. 
        /// Bts: r�gt�n lehessen l�tni, hova ker�lt a b�rk�d felhaszn�l�sra.
        /// </summary>
        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// ObjTip_Id property </summary>
        private Field _ObjTip_Id = new Field();

        /// <summary>
        /// Ez annak a valaminek a t�pusa, amit csatoltunk
        /// </summary>
        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Obj_type property </summary>
        private Field _Obj_type = new Field();

        /// <summary>
        /// Obj. t�pus, itt: a t�bla neve
        /// </summary>
        public Field Obj_type
        {
            get { return _Obj_type; }
            set { _Obj_type = value; }
        }
                          
           
        /// <summary>
        /// KodType property </summary>
        private Field _KodType = new Field();

        /// <summary>
        /// G,N,P
        /// </summary>
        public Field KodType
        {
            get { return _KodType; }
            set { _KodType = value; }
        }
                          
           
        /// <summary>
        /// Allapot property </summary>
        private Field _Allapot = new Field();

        /// <summary>
        /// KCS: BARKOD_ALLAPOT
        /// S - Szabad
        /// F - Felhaszn�lt
        /// T - T�r�lt
        /// </summary>
        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property </summary>
        private Field _ErvKezd = new Field();

        /// <summary>
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property </summary>
        private Field _ErvVege = new Field();

        /// <summary>
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// Tranz_id property </summary>
        private Field _Tranz_id = new Field();

        /// <summary>
        /// Tranzakci� azonos�t�: egy program �ltal egy logikai egys�gk�nt t�rolt inform�ci�kat 
        /// egyetlen Guid -al azonos�tunk. (pl. egy ASP.Net lap �ltal egy ment�ssel, ha t�bb t�bl�ba t�rol
        /// inform�ci�kat, ezen azonos�t� alapj�n lehet tudni, egy program volt az inform�ci� forr�s.Vagy: 
        /// Az XMLImport seg�dprogram fut�s�t c�lszer� az xml file-ban egyetlen tranz_id -val azonos�tani)
        /// </summary>
        public Field Tranz_id
        {
            get { return _Tranz_id; }
            set { _Tranz_id = value; }
        }
                              }

}