
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Cimek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_CimekSearch: BaseSearchObject    {
      public KRT_CimekSearch()
      {            
                     _Id.Name = "KRT_Cimek.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_Cimek.Org";
               _Org.Type = "Guid";            
               _Kategoria.Name = "KRT_Cimek.Kategoria";
               _Kategoria.Type = "Char";            
               _Tipus.Name = "KRT_Cimek.Tipus";
               _Tipus.Type = "String";            
               _Nev.Name = "KRT_Cimek.Nev";
               _Nev.Type = "String";            
               _KulsoAzonositok.Name = "KRT_Cimek.KulsoAzonositok";
               _KulsoAzonositok.Type = "String";            
               _Forras.Name = "KRT_Cimek.Forras";
               _Forras.Type = "Char";            
               _Orszag_Id.Name = "KRT_Cimek.Orszag_Id";
               _Orszag_Id.Type = "Guid";            
               _OrszagNev.Name = "KRT_Cimek.OrszagNev";
               _OrszagNev.Type = "String";            
               _Telepules_Id.Name = "KRT_Cimek.Telepules_Id";
               _Telepules_Id.Type = "Guid";            
               _TelepulesNev.Name = "KRT_Cimek.TelepulesNev";
               _TelepulesNev.Type = "String";            
               _IRSZ.Name = "KRT_Cimek.IRSZ";
               _IRSZ.Type = "String";            
               _CimTobbi.Name = "KRT_Cimek.CimTobbi";
               _CimTobbi.Type = "String";            
               _Kozterulet_Id.Name = "KRT_Cimek.Kozterulet_Id";
               _Kozterulet_Id.Type = "Guid";            
               _KozteruletNev.Name = "KRT_Cimek.KozteruletNev";
               _KozteruletNev.Type = "String";            
               _KozteruletTipus_Id.Name = "KRT_Cimek.KozteruletTipus_Id";
               _KozteruletTipus_Id.Type = "Guid";            
               _KozteruletTipusNev.Name = "KRT_Cimek.KozteruletTipusNev";
               _KozteruletTipusNev.Type = "String";            
               _Hazszam.Name = "KRT_Cimek.Hazszam";
               _Hazszam.Type = "String";            
               _Hazszamig.Name = "KRT_Cimek.Hazszamig";
               _Hazszamig.Type = "String";            
               _HazszamBetujel.Name = "KRT_Cimek.HazszamBetujel";
               _HazszamBetujel.Type = "String";            
               _MindketOldal.Name = "KRT_Cimek.MindketOldal";
               _MindketOldal.Type = "Char";            
               _HRSZ.Name = "KRT_Cimek.HRSZ";
               _HRSZ.Type = "String";            
               _Lepcsohaz.Name = "KRT_Cimek.Lepcsohaz";
               _Lepcsohaz.Type = "String";            
               _Szint.Name = "KRT_Cimek.Szint";
               _Szint.Type = "String";            
               _Ajto.Name = "KRT_Cimek.Ajto";
               _Ajto.Type = "String";            
               _AjtoBetujel.Name = "KRT_Cimek.AjtoBetujel";
               _AjtoBetujel.Type = "String";            
               _Tobbi.Name = "KRT_Cimek.Tobbi";
               _Tobbi.Type = "String";            
               _ErvKezd.Name = "KRT_Cimek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Cimek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kategoria = new Field();

        public Field Kategoria
        {
            get { return _Kategoria; }
            set { _Kategoria = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KulsoAzonositok = new Field();

        public Field KulsoAzonositok
        {
            get { return _KulsoAzonositok; }
            set { _KulsoAzonositok = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Forras = new Field();

        public Field Forras
        {
            get { return _Forras; }
            set { _Forras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Orszag_Id = new Field();

        public Field Orszag_Id
        {
            get { return _Orszag_Id; }
            set { _Orszag_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _OrszagNev = new Field();

        public Field OrszagNev
        {
            get { return _OrszagNev; }
            set { _OrszagNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Telepules_Id = new Field();

        public Field Telepules_Id
        {
            get { return _Telepules_Id; }
            set { _Telepules_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _TelepulesNev = new Field();

        public Field TelepulesNev
        {
            get { return _TelepulesNev; }
            set { _TelepulesNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IRSZ = new Field();

        public Field IRSZ
        {
            get { return _IRSZ; }
            set { _IRSZ = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _CimTobbi = new Field();

        public Field CimTobbi
        {
            get { return _CimTobbi; }
            set { _CimTobbi = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kozterulet_Id = new Field();

        public Field Kozterulet_Id
        {
            get { return _Kozterulet_Id; }
            set { _Kozterulet_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KozteruletNev = new Field();

        public Field KozteruletNev
        {
            get { return _KozteruletNev; }
            set { _KozteruletNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KozteruletTipus_Id = new Field();

        public Field KozteruletTipus_Id
        {
            get { return _KozteruletTipus_Id; }
            set { _KozteruletTipus_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KozteruletTipusNev = new Field();

        public Field KozteruletTipusNev
        {
            get { return _KozteruletTipusNev; }
            set { _KozteruletTipusNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Hazszam = new Field();

        public Field Hazszam
        {
            get { return _Hazszam; }
            set { _Hazszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Hazszamig = new Field();

        public Field Hazszamig
        {
            get { return _Hazszamig; }
            set { _Hazszamig = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HazszamBetujel = new Field();

        public Field HazszamBetujel
        {
            get { return _HazszamBetujel; }
            set { _HazszamBetujel = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MindketOldal = new Field();

        public Field MindketOldal
        {
            get { return _MindketOldal; }
            set { _MindketOldal = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HRSZ = new Field();

        public Field HRSZ
        {
            get { return _HRSZ; }
            set { _HRSZ = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Lepcsohaz = new Field();

        public Field Lepcsohaz
        {
            get { return _Lepcsohaz; }
            set { _Lepcsohaz = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Szint = new Field();

        public Field Szint
        {
            get { return _Szint; }
            set { _Szint = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Ajto = new Field();

        public Field Ajto
        {
            get { return _Ajto; }
            set { _Ajto = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AjtoBetujel = new Field();

        public Field AjtoBetujel
        {
            get { return _AjtoBetujel; }
            set { _AjtoBetujel = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tobbi = new Field();

        public Field Tobbi
        {
            get { return _Tobbi; }
            set { _Tobbi = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}