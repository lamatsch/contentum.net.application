
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_CsoportTagok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_CsoportTagokSearch: BaseSearchObject    {
      public KRT_CsoportTagokSearch()
      {            
                     _Id.Name = "KRT_CsoportTagok.Id";
               _Id.Type = "Guid";            
               _Tipus.Name = "KRT_CsoportTagok.Tipus";
               _Tipus.Type = "String";            
               _Csoport_Id.Name = "KRT_CsoportTagok.Csoport_Id";
               _Csoport_Id.Type = "Guid";            
               _Csoport_Id_Jogalany.Name = "KRT_CsoportTagok.Csoport_Id_Jogalany";
               _Csoport_Id_Jogalany.Type = "Guid";            
               _ObjTip_Id_Jogalany.Name = "KRT_CsoportTagok.ObjTip_Id_Jogalany";
               _ObjTip_Id_Jogalany.Type = "Guid";            
               _ErtesitesMailCim.Name = "KRT_CsoportTagok.ErtesitesMailCim";
               _ErtesitesMailCim.Type = "String";            
               _ErtesitesKell.Name = "KRT_CsoportTagok.ErtesitesKell";
               _ErtesitesKell.Type = "Char";            
               _System.Name = "KRT_CsoportTagok.System";
               _System.Type = "Char";            
               _Orokolheto.Name = "KRT_CsoportTagok.Orokolheto";
               _Orokolheto.Type = "Char";            
               _ErvKezd.Name = "KRT_CsoportTagok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_CsoportTagok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id = new Field();

        public Field Csoport_Id
        {
            get { return _Csoport_Id; }
            set { _Csoport_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Jogalany = new Field();

        public Field Csoport_Id_Jogalany
        {
            get { return _Csoport_Id_Jogalany; }
            set { _Csoport_Id_Jogalany = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id_Jogalany = new Field();

        public Field ObjTip_Id_Jogalany
        {
            get { return _ObjTip_Id_Jogalany; }
            set { _ObjTip_Id_Jogalany = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErtesitesMailCim = new Field();

        public Field ErtesitesMailCim
        {
            get { return _ErtesitesMailCim; }
            set { _ErtesitesMailCim = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErtesitesKell = new Field();

        public Field ErtesitesKell
        {
            get { return _ErtesitesKell; }
            set { _ErtesitesKell = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _System = new Field();

        public Field System
        {
            get { return _System; }
            set { _System = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Orokolheto = new Field();

        public Field Orokolheto
        {
            get { return _Orokolheto; }
            set { _Orokolheto = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}