
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Csoportok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_CsoportokSearch: BaseSearchObject    {
      public KRT_CsoportokSearch()
      {            
                     _Id.Name = "KRT_Csoportok.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_Csoportok.Org";
               _Org.Type = "Guid";            
               _Kod.Name = "KRT_Csoportok.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_Csoportok.Nev";
               _Nev.Type = "String";            
               _Tipus.Name = "KRT_Csoportok.Tipus";
               _Tipus.Type = "String";            
               _Jogalany.Name = "KRT_Csoportok.Jogalany";
               _Jogalany.Type = "Char";            
               _ErtesitesEmail.Name = "KRT_Csoportok.ErtesitesEmail";
               _ErtesitesEmail.Type = "String";            
               _System.Name = "KRT_Csoportok.System";
               _System.Type = "Char";            
               _Adatforras.Name = "KRT_Csoportok.Adatforras";
               _Adatforras.Type = "Char";            
               _ObjTipus_Id_Szulo.Name = "KRT_Csoportok.ObjTipus_Id_Szulo";
               _ObjTipus_Id_Szulo.Type = "Guid";            
               _Kiszolgalhato.Name = "KRT_Csoportok.Kiszolgalhato";
               _Kiszolgalhato.Type = "Char";            
               _JogosultsagOroklesMod.Name = "KRT_Csoportok.JogosultsagOroklesMod";
               _JogosultsagOroklesMod.Type = "Char";                     
               _ErvKezd.Name = "KRT_Csoportok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Csoportok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Kod property
        /// Csoport k�dja
        /// </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// Csoport megnevez�se
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Tipus property
        /// Csoportt�pusok KCS: CSOPORTTIPUS
        /// 0 - Szervezet
        /// 1 - Dolgoz�
        /// 2 - Szerepk�r
        /// 4 - Projekt
        /// 5 - Test�let, bizotts�g
        /// 6 - Hivatal vezet�s
        /// 7 - �nkorm�nyzat
        /// 9 - Org
        /// M - Mindenki
        /// R - Rendszergazd�k
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Jogalany property
        /// Jogalanyk�nt �rtelmezhet� csoport?  (1- Iigen, /0 vagy NULL - Nem)
        /// </summary>
        private Field _Jogalany = new Field();

        public Field Jogalany
        {
            get { return _Jogalany; }
            set { _Jogalany = value; }
        }
                          
           
        /// <summary>
        /// ErtesitesEmail property
        /// �rtes�t�si e_mail c�m (2007.06.18, KovAx)
        /// </summary>
        private Field _ErtesitesEmail = new Field();

        public Field ErtesitesEmail
        {
            get { return _ErtesitesEmail; }
            set { _ErtesitesEmail = value; }
        }
                          
           
        /// <summary>
        /// System property
        /// Csak a rendszer kezelheti? 1-I/0-N
        /// </summary>
        private Field _System = new Field();

        public Field System
        {
            get { return _System; }
            set { _System = value; }
        }
                          
           
        /// <summary>
        /// Adatforras property
        /// AD-b�l intef�szelve (A),  vagy manu�lisan (M).
        /// </summary>
        private Field _Adatforras = new Field();

        public Field Adatforras
        {
            get { return _Adatforras; }
            set { _Adatforras = value; }
        }
                          
           
        /// <summary>
        /// ObjTipus_Id_Szulo property
        /// A SZULO_ID tartalma vagy egy partner ID (belso szervezet tipusu), vagy egy felhasznalo ID. 
        /// Ezt meg kell tudni mondani, hogy melyik. Ez a szulo tipus mezo feladata. 
        /// Kodszotaras ertek volt, most az obtipusok t�bl�b�l j�n...
        /// </summary>
        private Field _ObjTipus_Id_Szulo = new Field();

        public Field ObjTipus_Id_Szulo
        {
            get { return _ObjTipus_Id_Szulo; }
            set { _ObjTipus_Id_Szulo = value; }
        }
                          
           
        /// <summary>
        /// Kiszolgalhato property
        /// 1-igen, 0-nem
        /// </summary>
        private Field _Kiszolgalhato = new Field();

        public Field Kiszolgalhato
        {
            get { return _Kiszolgalhato; }
            set { _Kiszolgalhato = value; }
        }
                          
           
        /// <summary>
        /// JogosultsagOroklesMod property
        /// A felhaszn�l�k hozz�f�r�si jogosults�g�nak �r�kl�si m�dja adott szervezeten bel�l:
        ///  1- egym�s t�teleit l�thatj�k
        ///  0 vagy Null: - egym�s t�teleit nem l�thatj�k
        /// </summary>
        private Field _JogosultsagOroklesMod = new Field();

        public Field JogosultsagOroklesMod
        {
            get { return _JogosultsagOroklesMod; }
            set { _JogosultsagOroklesMod = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}