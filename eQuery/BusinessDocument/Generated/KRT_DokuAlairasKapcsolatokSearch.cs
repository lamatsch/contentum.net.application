
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_DokuAlairasKapcsolatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_DokuAlairasKapcsolatokSearch: BaseSearchObject    {
      public KRT_DokuAlairasKapcsolatokSearch()
      {            
                     _Id.Name = "KRT_DokuAlairasKapcsolatok.Id";
               _Id.Type = "Guid";            
               _Dokumentum_id.Name = "KRT_DokuAlairasKapcsolatok.Dokumentum_id";
               _Dokumentum_id.Type = "Guid";            
               _DokumentumAlairas_Id.Name = "KRT_DokuAlairasKapcsolatok.DokumentumAlairas_Id";
               _DokumentumAlairas_Id.Type = "Guid";            
               _DokuKapcsolatSorrend.Name = "KRT_DokuAlairasKapcsolatok.DokuKapcsolatSorrend";
               _DokuKapcsolatSorrend.Type = "Int32";            
               _ErvKezd.Name = "KRT_DokuAlairasKapcsolatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_DokuAlairasKapcsolatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Dokumentum_id = new Field();

        public Field Dokumentum_id
        {
            get { return _Dokumentum_id; }
            set { _Dokumentum_id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _DokumentumAlairas_Id = new Field();

        public Field DokumentumAlairas_Id
        {
            get { return _DokumentumAlairas_Id; }
            set { _DokumentumAlairas_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _DokuKapcsolatSorrend = new Field();

        public Field DokuKapcsolatSorrend
        {
            get { return _DokuKapcsolatSorrend; }
            set { _DokuKapcsolatSorrend = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}