
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_DokumentumAlairasok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_DokumentumAlairasokSearch: BaseSearchObject    {
      public KRT_DokumentumAlairasokSearch()
      {            
                     _Id.Name = "KRT_DokumentumAlairasok.Id";
               _Id.Type = "Guid";            
               _Dokumentum_Id_Alairt.Name = "KRT_DokumentumAlairasok.Dokumentum_Id_Alairt";
               _Dokumentum_Id_Alairt.Type = "Guid";            
               _AlairasMod.Name = "KRT_DokumentumAlairasok.AlairasMod";
               _AlairasMod.Type = "String";            
               _AlairasSzabaly_Id.Name = "KRT_DokumentumAlairasok.AlairasSzabaly_Id";
               _AlairasSzabaly_Id.Type = "Guid";            
               _AlairasRendben.Name = "KRT_DokumentumAlairasok.AlairasRendben";
               _AlairasRendben.Type = "Char";            
               _KivarasiIdoVege.Name = "KRT_DokumentumAlairasok.KivarasiIdoVege";
               _KivarasiIdoVege.Type = "DateTime";            
               _AlairasVeglegRendben.Name = "KRT_DokumentumAlairasok.AlairasVeglegRendben";
               _AlairasVeglegRendben.Type = "Char";            
               _Idopecset.Name = "KRT_DokumentumAlairasok.Idopecset";
               _Idopecset.Type = "DateTime";            
               _Csoport_Id_Alairo.Name = "KRT_DokumentumAlairasok.Csoport_Id_Alairo";
               _Csoport_Id_Alairo.Type = "Guid";            
               _AlairasTulajdonos.Name = "KRT_DokumentumAlairasok.AlairasTulajdonos";
               _AlairasTulajdonos.Type = "String";            
               _Tanusitvany_Id.Name = "KRT_DokumentumAlairasok.Tanusitvany_Id";
               _Tanusitvany_Id.Type = "Guid";            
               _Allapot.Name = "KRT_DokumentumAlairasok.Allapot";
               _Allapot.Type = "String";            
               _ErvKezd.Name = "KRT_DokumentumAlairasok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_DokumentumAlairasok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Dokumentum_Id_Alairt = new Field();

        public Field Dokumentum_Id_Alairt
        {
            get { return _Dokumentum_Id_Alairt; }
            set { _Dokumentum_Id_Alairt = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairasMod = new Field();

        public Field AlairasMod
        {
            get { return _AlairasMod; }
            set { _AlairasMod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairasSzabaly_Id = new Field();

        public Field AlairasSzabaly_Id
        {
            get { return _AlairasSzabaly_Id; }
            set { _AlairasSzabaly_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairasRendben = new Field();

        public Field AlairasRendben
        {
            get { return _AlairasRendben; }
            set { _AlairasRendben = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KivarasiIdoVege = new Field();

        public Field KivarasiIdoVege
        {
            get { return _KivarasiIdoVege; }
            set { _KivarasiIdoVege = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairasVeglegRendben = new Field();

        public Field AlairasVeglegRendben
        {
            get { return _AlairasVeglegRendben; }
            set { _AlairasVeglegRendben = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Idopecset = new Field();

        public Field Idopecset
        {
            get { return _Idopecset; }
            set { _Idopecset = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Alairo = new Field();

        public Field Csoport_Id_Alairo
        {
            get { return _Csoport_Id_Alairo; }
            set { _Csoport_Id_Alairo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _AlairasTulajdonos = new Field();

        public Field AlairasTulajdonos
        {
            get { return _AlairasTulajdonos; }
            set { _AlairasTulajdonos = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tanusitvany_Id = new Field();

        public Field Tanusitvany_Id
        {
            get { return _Tanusitvany_Id; }
            set { _Tanusitvany_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}