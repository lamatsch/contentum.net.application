
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_DokumentumCsatolasok eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_DokumentumCsatolasokSearch
    {
      public KRT_DokumentumCsatolasokSearch()
      {               _Id.Name = "KRT_DokumentumCsatolasok.Id";
               _Id.Type = "Guid";            
               _ObjTip_Id_Dokumentum.Name = "KRT_DokumentumCsatolasok.ObjTip_Id_Dokumentum";
               _ObjTip_Id_Dokumentum.Type = "Guid";            
               _Obj_Id_Dokumentum.Name = "KRT_DokumentumCsatolasok.Obj_Id_Dokumentum";
               _Obj_Id_Dokumentum.Type = "Guid";            
               _ObjTip_Id.Name = "KRT_DokumentumCsatolasok.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Obj_Id.Name = "KRT_DokumentumCsatolasok.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _Tipus.Name = "KRT_DokumentumCsatolasok.Tipus";
               _Tipus.Type = "String";            
               _Tipus_Id.Name = "KRT_DokumentumCsatolasok.Tipus_Id";
               _Tipus_Id.Type = "Guid";            
               _Leiras.Name = "KRT_DokumentumCsatolasok.Leiras";
               _Leiras.Type = "String";            
               _ErvKezd.Name = "KRT_DokumentumCsatolasok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_DokumentumCsatolasok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id_Dokumentum = new Field();

        public Field ObjTip_Id_Dokumentum
        {
            get { return _ObjTip_Id_Dokumentum; }
            set { _ObjTip_Id_Dokumentum = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id_Dokumentum = new Field();

        public Field Obj_Id_Dokumentum
        {
            get { return _Obj_Id_Dokumentum; }
            set { _Obj_Id_Dokumentum = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus_Id = new Field();

        public Field Tipus_Id
        {
            get { return _Tipus_Id; }
            set { _Tipus_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}