
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_DokumentumKapcsolatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_DokumentumKapcsolatokSearch: BaseSearchObject    {
      public KRT_DokumentumKapcsolatokSearch()
      {            
                     _Id.Name = "KRT_DokumentumKapcsolatok.Id";
               _Id.Type = "Guid";            
               _Tipus.Name = "KRT_DokumentumKapcsolatok.Tipus";
               _Tipus.Type = "String";            
               _Dokumentum_Id_Fo.Name = "KRT_DokumentumKapcsolatok.Dokumentum_Id_Fo";
               _Dokumentum_Id_Fo.Type = "Guid";            
               _Dokumentum_Id_Al.Name = "KRT_DokumentumKapcsolatok.Dokumentum_Id_Al";
               _Dokumentum_Id_Al.Type = "Guid";            
               _DokumentumKapcsolatJelleg.Name = "KRT_DokumentumKapcsolatok.DokumentumKapcsolatJelleg";
               _DokumentumKapcsolatJelleg.Type = "String";            
               _DokuKapcsolatSorrend.Name = "KRT_DokumentumKapcsolatok.DokuKapcsolatSorrend";
               _DokuKapcsolatSorrend.Type = "Int32";            
               _ErvKezd.Name = "KRT_DokumentumKapcsolatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_DokumentumKapcsolatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Dokumentum_Id_Fo = new Field();

        public Field Dokumentum_Id_Fo
        {
            get { return _Dokumentum_Id_Fo; }
            set { _Dokumentum_Id_Fo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Dokumentum_Id_Al = new Field();

        public Field Dokumentum_Id_Al
        {
            get { return _Dokumentum_Id_Al; }
            set { _Dokumentum_Id_Al = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _DokumentumKapcsolatJelleg = new Field();

        public Field DokumentumKapcsolatJelleg
        {
            get { return _DokumentumKapcsolatJelleg; }
            set { _DokumentumKapcsolatJelleg = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _DokuKapcsolatSorrend = new Field();

        public Field DokuKapcsolatSorrend
        {
            get { return _DokuKapcsolatSorrend; }
            set { _DokuKapcsolatSorrend = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}