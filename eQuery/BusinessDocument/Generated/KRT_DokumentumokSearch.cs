
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Dokumentumok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class KRT_DokumentumokSearch: BaseSearchObject    {
      public KRT_DokumentumokSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "KRT_Dokumentumok.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_Dokumentumok.Org";
               _Org.Type = "Guid";            
               _FajlNev.Name = "KRT_Dokumentumok.FajlNev";
               _FajlNev.Type = "String";            
               _VerzioJel.Name = "KRT_Dokumentumok.VerzioJel";
               _VerzioJel.Type = "String";            
               _Nev.Name = "KRT_Dokumentumok.Nev";
               _Nev.Type = "String";            
               _Tipus.Name = "KRT_Dokumentumok.Tipus";
               _Tipus.Type = "String";            
               _Formatum.Name = "KRT_Dokumentumok.Formatum";
               _Formatum.Type = "String";            
               _Leiras.Name = "KRT_Dokumentumok.Leiras";
               _Leiras.Type = "String";            
               _Meret.Name = "KRT_Dokumentumok.Meret";
               _Meret.Type = "Int32";            
               _TartalomHash.Name = "KRT_Dokumentumok.TartalomHash";
               _TartalomHash.Type = "String";            
               _AlairtTartalomHash.Name = "KRT_Dokumentumok.AlairtTartalomHash";
               _AlairtTartalomHash.Type = "String";            
               _KivonatHash.Name = "KRT_Dokumentumok.KivonatHash";
               _KivonatHash.Type = "String";            
               _Dokumentum_Id.Name = "KRT_Dokumentumok.Dokumentum_Id";
               _Dokumentum_Id.Type = "Guid";            
               _Dokumentum_Id_Kovetkezo.Name = "KRT_Dokumentumok.Dokumentum_Id_Kovetkezo";
               _Dokumentum_Id_Kovetkezo.Type = "Guid";            
               _Alkalmazas_Id.Name = "KRT_Dokumentumok.Alkalmazas_Id";
               _Alkalmazas_Id.Type = "Guid";            
               _Csoport_Id_Tulaj.Name = "KRT_Dokumentumok.Csoport_Id_Tulaj";
               _Csoport_Id_Tulaj.Type = "Guid";            
               _KulsoAzonositok.Name = "KRT_Dokumentumok.KulsoAzonositok";
               _KulsoAzonositok.Type = "String";            
               _External_Source.Name = "KRT_Dokumentumok.External_Source";
               _External_Source.Type = "String";            
               _External_Link.Name = "KRT_Dokumentumok.External_Link";
               _External_Link.Type = "String";            
               _External_Id.Name = "KRT_Dokumentumok.External_Id";
               _External_Id.Type = "Guid";            
               _External_Info.Name = "KRT_Dokumentumok.External_Info";
               _External_Info.Type = "String";            
               _CheckedOut.Name = "KRT_Dokumentumok.CheckedOut";
               _CheckedOut.Type = "Guid";            
               _CheckedOutTime.Name = "KRT_Dokumentumok.CheckedOutTime";
               _CheckedOutTime.Type = "DateTime";            
               _BarCode.Name = "KRT_Dokumentumok.BarCode";
               _BarCode.Type = "String";            
               _OCRPrioritas.Name = "KRT_Dokumentumok.OCRPrioritas";
               _OCRPrioritas.Type = "Int32";            
               _OCRAllapot.Name = "KRT_Dokumentumok.OCRAllapot";
               _OCRAllapot.Type = "Char";            
               _Allapot.Name = "KRT_Dokumentumok.Allapot";
               _Allapot.Type = "String";            
               _WorkFlowAllapot.Name = "KRT_Dokumentumok.WorkFlowAllapot";
               _WorkFlowAllapot.Type = "String";            
               _Megnyithato.Name = "KRT_Dokumentumok.Megnyithato";
               _Megnyithato.Type = "Char";            
               _Olvashato.Name = "KRT_Dokumentumok.Olvashato";
               _Olvashato.Type = "Char";            
               _ElektronikusAlairas.Name = "KRT_Dokumentumok.ElektronikusAlairas";
               _ElektronikusAlairas.Type = "String";            
               _AlairasFelulvizsgalat.Name = "KRT_Dokumentumok.AlairasFelulvizsgalat";
               _AlairasFelulvizsgalat.Type = "DateTime";            
               _Titkositas.Name = "KRT_Dokumentumok.Titkositas";
               _Titkositas.Type = "Char";            
               _SablonAzonosito.Name = "KRT_Dokumentumok.SablonAzonosito";
               _SablonAzonosito.Type = "String";            
               _ErvKezd.Name = "KRT_Dokumentumok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Dokumentumok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _AlairasFelulvizsgalatEredmeny.Name = "KRT_Dokumentumok.AlairasFelulvizsgalatEredmeny";
               _AlairasFelulvizsgalatEredmeny.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// Org (alkalmaz�sk�rnyezet) azonos�t�ja
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// FajlNev property
        /// Eredeti dokumentum fajlneve (kiterjesztessel egyutt).
        /// </summary>
        private Field _FajlNev = new Field();

        public Field FajlNev
        {
            get { return _FajlNev; }
            set { _FajlNev = value; }
        }
                          
           
        /// <summary>
        /// VerzioJel property
        /// A verzio azonositja (FPH h�rmas ...)
        /// </summary>
        private Field _VerzioJel = new Field();

        public Field VerzioJel
        {
            get { return _VerzioJel; }
            set { _VerzioJel = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// A dokumentum szabad szoveges megnevezese.
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Tipus property
        /// KCS: DOKUMENTUMTIPUS
        /// 01 - Sz�mla
        /// 02 - Szerz�d�s
        /// 11 - Besz�mol�
        /// 12 - Elemz�s; ellen�rz�s;el�terjeszt�s;
        /// 13 - Feljegyz�s
        /// 14 - Jogszab�ly
        /// 15 - P�ly�zat
        /// 16 - Prezent�ci�
        /// 17 - Tanulm�ny
        /// 18 - Digitaliz�lt dokumentum
        /// 19 - Bont�si jegyz�k�nyv
        /// 20 - �tv�teli elismerv�ny
        /// 21 - Egy�b
        /// 
        /// 
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Formatum property
        /// KCS: DOKUMENTUM_FORMATUM F�jln�v kiterjeszt�s (�s annak felhaszn�l�i szint� �rtelmez�se)
        /// </summary>
        private Field _Formatum = new Field();

        public Field Formatum
        {
            get { return _Formatum; }
            set { _Formatum = value; }
        }
                          
           
        /// <summary>
        /// Leiras property
        /// Szabad szoveg
        /// </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Meret property
        /// Fajlmeret (byte).
        /// </summary>
        private Field _Meret = new Field();

        public Field Meret
        {
            get { return _Meret; }
            set { _Meret = value; }
        }
                          
           
        /// <summary>
        /// TartalomHash property
        /// Eredeti dokumentum teljes tartalm�nak ujjlenyomata.
        /// </summary>
        private Field _TartalomHash = new Field();

        public Field TartalomHash
        {
            get { return _TartalomHash; }
            set { _TartalomHash = value; }
        }
                          
           
        /// <summary>
        /// AlairtTartalomHash property
        /// Detached al��r�si m�d eset�n az �rintett dokumentumok konkaten�lt TartalomHash �rt�k�nek elektronikusan al��rt form�ja.
        /// </summary>
        private Field _AlairtTartalomHash = new Field();

        public Field AlairtTartalomHash
        {
            get { return _AlairtTartalomHash; }
            set { _AlairtTartalomHash = value; }
        }
                          
           
        /// <summary>
        /// KivonatHash property
        /// Eredeti dokumentum kivonat�nak (beltartalm�nak) ujjlenyomata (docx-n�l �rtelmezett).
        /// </summary>
        private Field _KivonatHash = new Field();

        public Field KivonatHash
        {
            get { return _KivonatHash; }
            set { _KivonatHash = value; }
        }
                          
           
        /// <summary>
        /// Dokumentum_Id property
        /// Egy dokumentum minden verziojaban ugyanaz az ertek (az elso verzio ID-je).
        /// </summary>
        private Field _Dokumentum_Id = new Field();

        public Field Dokumentum_Id
        {
            get { return _Dokumentum_Id; }
            set { _Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// Dokumentum_Id_Kovetkezo property
        /// K�vetkez� verzi� ID-je. Ha �res, akkor � az aktu�lis (verzio), a munkapeldany.
        /// </summary>
        private Field _Dokumentum_Id_Kovetkezo = new Field();

        public Field Dokumentum_Id_Kovetkezo
        {
            get { return _Dokumentum_Id_Kovetkezo; }
            set { _Dokumentum_Id_Kovetkezo = value; }
        }
                          
           
        /// <summary>
        /// Alkalmazas_Id property
        /// Alkalmaz�s Id 
        /// </summary>
        private Field _Alkalmazas_Id = new Field();

        public Field Alkalmazas_Id
        {
            get { return _Alkalmazas_Id; }
            set { _Alkalmazas_Id = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Tulaj property
        /// Letrehozo (vagy mas, aki letrehozatta) csoportja. Megfelel az eRecordbeli PRT_ID_FELELOS-nek. Mindig legyen legalabb egy csoport, amelynek explicit karbantartasi joga van az adott objektumra. Ezt a jogat mas szabalyok korlatozhatjak, de nem fordulhat elo olyan eset, hogy egy akarmilyen szulo ervenytelenitese objektumokat kezelhetetlenne tesz!
        /// </summary>
        private Field _Csoport_Id_Tulaj = new Field();

        public Field Csoport_Id_Tulaj
        {
            get { return _Csoport_Id_Tulaj; }
            set { _Csoport_Id_Tulaj = value; }
        }
                          
           
        /// <summary>
        /// KulsoAzonositok property
        /// Az adott alkalmaz�s szeinti saj�t egyedi azonos�t�. (A 2007.6.6 -i megbesz�l�sen mer�lt fel a sz�ks�gess�ge.)
        /// </summary>
        private Field _KulsoAzonositok = new Field();

        public Field KulsoAzonositok
        {
            get { return _KulsoAzonositok; }
            set { _KulsoAzonositok = value; }
        }
                          
           
        /// <summary>
        /// External_Source property
        /// K�ls� dokumentumt�r forr�s t�pusa: KCS: DOKUMENTUM_FORRAS_TIPUS (1-SharePoint, 2-File system, 3-Database).
        /// 
        /// </summary>
        private Field _External_Source = new Field();

        public Field External_Source
        {
            get { return _External_Source; }
            set { _External_Source = value; }
        }
                          
           
        /// <summary>
        /// External_Link property
        /// A dokumentum el�r�s teljes linkje. SharePoint eset�n URL, f�jlrendszer eset�n teljes el�r�si �t (g�p, �tvonal, ...)
        /// </summary>
        private Field _External_Link = new Field();

        public Field External_Link
        {
            get { return _External_Link; }
            set { _External_Link = value; }
        }
                          
           
        /// <summary>
        /// External_Id property
        /// SharePoint for��s eset�n �rtelmezett adat, SharePoint ID (GUID).
        /// </summary>
        private Field _External_Id = new Field();

        public Field External_Id
        {
            get { return _External_Id; }
            set { _External_Id = value; }
        }
                          
           
        /// <summary>
        /// External_Info property
        /// A k�ls� forr�s t�pus�t�l f�gg� szerkezet�, az el�r�si adatokat tartalmaz� sz�veg, pl. egyedi jellemz�k a dokumentumt�rr�l.
        /// </summary>
        private Field _External_Info = new Field();

        public Field External_Info
        {
            get { return _External_Info; }
            set { _External_Info = value; }
        }
                          
           
        /// <summary>
        /// CheckedOut property
        /// 
        /// </summary>
        private Field _CheckedOut = new Field();

        public Field CheckedOut
        {
            get { return _CheckedOut; }
            set { _CheckedOut = value; }
        }
                          
           
        /// <summary>
        /// CheckedOutTime property
        /// 
        /// </summary>
        private Field _CheckedOutTime = new Field();

        public Field CheckedOutTime
        {
            get { return _CheckedOutTime; }
            set { _CheckedOutTime = value; }
        }
                          
           
        /// <summary>
        /// BarCode property
        /// Vonalk�d �rt�ke
        /// </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// OCRPrioritas property
        /// Az OCR-ezend� (multipage TIFF) f�jlok OCR-ez�s�nek priorit�s�t meghat�roz� �rt�k.
        /// NULL - nem OCR-ezend� dokumentum
        /// 1-9: priorit�s (a magasabb �rt�k jelenti a nagyobb priorit�st)
        /// </summary>
        private Field _OCRPrioritas = new Field();

        public Field OCRPrioritas
        {
            get { return _OCRPrioritas; }
            set { _OCRPrioritas = value; }
        }
                          
           
        /// <summary>
        /// OCRAllapot property
        /// Az OCR-ezend� (multipage TIFF) f�jlok OCR �llapota.
        /// NULL - nem OCR-ezend� dokumentum
        /// 1 - OCR-ezend� dokumentum
        /// 2 - OCR-ez�s alatt l�v� dokumentum
        /// 3 - OCR-ezett dokumentum
        /// </summary>
        private Field _OCRAllapot = new Field();

        public Field OCRAllapot
        {
            get { return _OCRAllapot; }
            set { _OCRAllapot = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// Dokumentum�llapot. KCS: DOKUMENTUM_ALLAPOT.
        /// (0 - Iktat�rendszeren k�v�li, 1 - Iktat�rendszerben nyilv�ntartott) 
        /// ObjStatok szinon�m�b�l j�n (Sz�r�s miatt denormaliz�lt!)
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// WorkFlowAllapot property
        /// Dokumentum workflow �llapot. KCS: DOKUMENTUM_WORKFLOW_ALLAPOT.
        /// 
        /// </summary>
        private Field _WorkFlowAllapot = new Field();

        public Field WorkFlowAllapot
        {
            get { return _WorkFlowAllapot; }
            set { _WorkFlowAllapot = value; }
        }
                          
           
        /// <summary>
        /// Megnyithato property
        /// 1-igen, 0-nem
        /// </summary>
        private Field _Megnyithato = new Field();

        public Field Megnyithato
        {
            get { return _Megnyithato; }
            set { _Megnyithato = value; }
        }
                          
           
        /// <summary>
        /// Olvashato property
        /// 1-igen, 0-nem
        /// </summary>
        private Field _Olvashato = new Field();

        public Field Olvashato
        {
            get { return _Olvashato; }
            set { _Olvashato = value; }
        }
                          
           
        /// <summary>
        /// ElektronikusAlairas property
        /// A dokumentum al��r�s min�s�t�se.
        /// KCS:DOKUMETUM_ALAIRAS_PKI  vagy a DOKUMENTUM_ALAIRAS_MANUALIS,
        /// a PKI_INTEGRACO rendszerparam�ter be�ll�t�s�t�l f�gg�en(PKI/MANUALIS):
        /// 0 - Nincs vagy nem hiteles(mindk�t k�dcsoport)
        /// 1 - Min�s�tett al��r�s/Hiteles al��r�s
        /// 2 - Fokozott biztons�g� al��r�s/Hiteles al��r�s
        /// 3 - Hivatali bels� al��r�s/Nem hiteles al��r�s
        /// 4 - Egyszer� al��r�s
        /// 8 - Hiteles al��r�s (mindk�t k�dcsoport)
        /// 9 - V�glegesen nem hiteles al��r�s/Nem hiteles al��r�s
        /// 
        /// </summary>
        private Field _ElektronikusAlairas = new Field();

        public Field ElektronikusAlairas
        {
            get { return _ElektronikusAlairas; }
            set { _ElektronikusAlairas = value; }
        }
                          
           
        /// <summary>
        /// AlairasFelulvizsgalat property
        /// Az al��r�s �rv�nyess�g�nek �s/vagy az al��rt dokumentum s�rtetlens�g�nek utols� vizsg�lati id�pontja.
        /// </summary>
        private Field _AlairasFelulvizsgalat = new Field();

        public Field AlairasFelulvizsgalat
        {
            get { return _AlairasFelulvizsgalat; }
            set { _AlairasFelulvizsgalat = value; }
        }
                          
           
        /// <summary>
        /// Titkositas property
        /// Tartalom titkos�t�sa: 0 Nem, 1 Igen
        /// </summary>
        private Field _Titkositas = new Field();

        public Field Titkositas
        {
            get { return _Titkositas; }
            set { _Titkositas = value; }
        }
                          
           
        /// <summary>
        /// SablonAzonosito property
        /// <ContentType>+<Verzi�>
        /// </summary>
        private Field _SablonAzonosito = new Field();

        public Field SablonAzonosito
        {
            get { return _SablonAzonosito; }
            set { _SablonAzonosito = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// AlairasFelulvizsgalatEredmeny property
        /// 
        /// </summary>
        private Field _AlairasFelulvizsgalatEredmeny = new Field();

        public Field AlairasFelulvizsgalatEredmeny
        {
            get { return _AlairasFelulvizsgalatEredmeny; }
            set { _AlairasFelulvizsgalatEredmeny = value; }
        }
                              }

}