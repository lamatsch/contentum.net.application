
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Erintett_Tablak eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_Erintett_TablakSearch: BaseSearchObject    {
      public KRT_Erintett_TablakSearch()
      {            
                     _Id.Name = "KRT_Erintett_Tablak.Id";
               _Id.Type = "Guid";            
               _Tranzakci_id.Name = "KRT_Erintett_Tablak.Tranzakci_id";
               _Tranzakci_id.Type = "Guid";            
               _ObjTip_Id.Name = "KRT_Erintett_Tablak.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_Erintett_Tablak.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Erintett_Tablak.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tranzakci_id = new Field();

        public Field Tranzakci_id
        {
            get { return _Tranzakci_id; }
            set { _Tranzakci_id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}