
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Esemenyek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class KRT_EsemenyekSearch: BaseSearchObject    {
      public KRT_EsemenyekSearch()
      {                           
               _Id.Name = "KRT_Esemenyek.Id";
               _Id.Type = "Guid";            
               _Obj_Id.Name = "KRT_Esemenyek.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id.Name = "KRT_Esemenyek.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Azonositoja.Name = "KRT_Esemenyek.Azonositoja";
               _Azonositoja.Type = "String";            
               _Felhasznalo_Id_User.Name = "KRT_Esemenyek.Felhasznalo_Id_User";
               _Felhasznalo_Id_User.Type = "Guid";            
               _Csoport_Id_FelelosUserSzerveze.Name = "KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze";
               _Csoport_Id_FelelosUserSzerveze.Type = "Guid";            
               _Felhasznalo_Id_Login.Name = "KRT_Esemenyek.Felhasznalo_Id_Login";
               _Felhasznalo_Id_Login.Type = "Guid";            
               _Csoport_Id_Cel.Name = "KRT_Esemenyek.Csoport_Id_Cel";
               _Csoport_Id_Cel.Type = "Guid";            
               _Helyettesites_Id.Name = "KRT_Esemenyek.Helyettesites_Id";
               _Helyettesites_Id.Type = "Guid";            
               _Tranzakcio_Id.Name = "KRT_Esemenyek.Tranzakcio_Id";
               _Tranzakcio_Id.Type = "Guid";            
               _Funkcio_Id.Name = "KRT_Esemenyek.Funkcio_Id";
               _Funkcio_Id.Type = "Guid";            
               _CsoportHierarchia.Name = "KRT_Esemenyek.CsoportHierarchia";
               _CsoportHierarchia.Type = "String";            
               _KeresesiFeltetel.Name = "KRT_Esemenyek.KeresesiFeltetel";
               _KeresesiFeltetel.Type = "String";            
               _TalalatokSzama.Name = "KRT_Esemenyek.TalalatokSzama";
               _TalalatokSzama.Type = "Int32";            
               _ErvKezd.Name = "KRT_Esemenyek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Esemenyek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _Munkaallomas.Name = "KRT_Esemenyek.Munkaallomas";
               _Munkaallomas.Type = "String";            
               _MuveletKimenete.Name = "KRT_Esemenyek.MuveletKimenete";
               _MuveletKimenete.Type = "String";            
               _CheckSum.Name = "KRT_Esemenyek.CheckSum";
               _CheckSum.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Obj_Id property
        /// Objektum id, amire az esemeny vonatkozik
        /// </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// ObjTip_Id property
        /// Ez annak a valaminek a t�pusa, amit csatoltunk
        /// </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Azonositoja property
        /// Objektum kulso azonositoja. (pl ha irat, iktatosz�m...)
        /// kell ez?
        /// </summary>
        private Field _Azonositoja = new Field();

        public Field Azonositoja
        {
            get { return _Azonositoja; }
            set { _Azonositoja = value; }
        }
                          
           
        /// <summary>
        /// Felhasznalo_Id_User property
        /// Helyettesitett vegrehajto felhasznalo, alapertelmezetten megegyezik a prt_id_login-nal
        /// </summary>
        private Field _Felhasznalo_Id_User = new Field();

        public Field Felhasznalo_Id_User
        {
            get { return _Felhasznalo_Id_User; }
            set { _Felhasznalo_Id_User = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_FelelosUserSzerveze property
        /// Helyettesitett vegrehajto felhasznalo PRT_ID_MUNKAHELY-e
        /// </summary>
        private Field _Csoport_Id_FelelosUserSzerveze = new Field();

        public Field Csoport_Id_FelelosUserSzerveze
        {
            get { return _Csoport_Id_FelelosUserSzerveze; }
            set { _Csoport_Id_FelelosUserSzerveze = value; }
        }
                          
           
        /// <summary>
        /// Felhasznalo_Id_Login property
        /// Helyettesito vegrehajto felhasznalo, az esemeny tenyleges vegrehajtoja
        /// </summary>
        private Field _Felhasznalo_Id_Login = new Field();

        public Field Felhasznalo_Id_Login
        {
            get { return _Felhasznalo_Id_Login; }
            set { _Felhasznalo_Id_Login = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id_Cel property
        /// Csoport Id
        /// </summary>
        private Field _Csoport_Id_Cel = new Field();

        public Field Csoport_Id_Cel
        {
            get { return _Csoport_Id_Cel; }
            set { _Csoport_Id_Cel = value; }
        }
                          
           
        /// <summary>
        /// Helyettesites_Id property
        /// Helyettes�t�s vagy megb�z�s eset�n a KRT_Helyettesitesek azonos�t�.
        /// </summary>
        private Field _Helyettesites_Id = new Field();

        public Field Helyettesites_Id
        {
            get { return _Helyettesites_Id; }
            set { _Helyettesites_Id = value; }
        }
                          
           
        /// <summary>
        /// Tranzakcio_Id property
        /// tranzakci� Id, nem Tranz_id?
        /// </summary>
        private Field _Tranzakcio_Id = new Field();

        public Field Tranzakcio_Id
        {
            get { return _Tranzakcio_Id; }
            set { _Tranzakcio_Id = value; }
        }
                          
           
        /// <summary>
        /// Funkcio_Id property
        /// KRT_Funkciok FK
        /// </summary>
        private Field _Funkcio_Id = new Field();

        public Field Funkcio_Id
        {
            get { return _Funkcio_Id; }
            set { _Funkcio_Id = value; }
        }
                          
           
        /// <summary>
        /// CsoportHierarchia property
        /// Aktu�lis felhaszn�l�i csoport hierarchia let�rol�sa
        /// </summary>
        private Field _CsoportHierarchia = new Field();

        public Field CsoportHierarchia
        {
            get { return _CsoportHierarchia; }
            set { _CsoportHierarchia = value; }
        }
                          
           
        /// <summary>
        /// KeresesiFeltetel property
        /// Select where felt�tel let�rol�sa
        /// </summary>
        private Field _KeresesiFeltetel = new Field();

        public Field KeresesiFeltetel
        {
            get { return _KeresesiFeltetel; }
            set { _KeresesiFeltetel = value; }
        }
                          
           
        /// <summary>
        /// TalalatokSzama property
        /// Az elozo select rekordsz�ma
        /// </summary>
        private Field _TalalatokSzama = new Field();

        public Field TalalatokSzama
        {
            get { return _TalalatokSzama; }
            set { _TalalatokSzama = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// Munkaallomas property
        /// 
        /// </summary>
        private Field _Munkaallomas = new Field();

        public Field Munkaallomas
        {
            get { return _Munkaallomas; }
            set { _Munkaallomas = value; }
        }
                          
           
        /// <summary>
        /// MuveletKimenete property
        /// 
        /// </summary>
        private Field _MuveletKimenete = new Field();

        public Field MuveletKimenete
        {
            get { return _MuveletKimenete; }
            set { _MuveletKimenete = value; }
        }
                          
           
        /// <summary>
        /// CheckSum property
        /// 
        /// </summary>
        private Field _CheckSum = new Field();

        public Field CheckSum
        {
            get { return _CheckSum; }
            set { _CheckSum = value; }
        }
                              }

}