
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Extra_Napok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_Extra_NapokSearch: BaseSearchObject    {
      public KRT_Extra_NapokSearch()
      {            
                     _Id.Name = "KRT_Extra_Napok.Id";
               _Id.Type = "Guid";            
               _Datum.Name = "KRT_Extra_Napok.Datum";
               _Datum.Type = "DateTime";            
               _Jelzo.Name = "KRT_Extra_Napok.Jelzo";
               _Jelzo.Type = "Int32";            
               _Megjegyzes.Name = "KRT_Extra_Napok.Megjegyzes";
               _Megjegyzes.Type = "String";            
               _ErvKezd.Name = "KRT_Extra_Napok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Extra_Napok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Datum = new Field();

        public Field Datum
        {
            get { return _Datum; }
            set { _Datum = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Jelzo = new Field();

        public Field Jelzo
        {
            get { return _Jelzo; }
            set { _Jelzo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}