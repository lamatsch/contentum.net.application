
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_FelhasznaloProfilok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_FelhasznaloProfilokSearch: BaseSearchObject    {
      public KRT_FelhasznaloProfilokSearch()
      {            
                     _Id.Name = "KRT_FelhasznaloProfilok.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_FelhasznaloProfilok.Org";
               _Org.Type = "Guid";            
               _Tipus.Name = "KRT_FelhasznaloProfilok.Tipus";
               _Tipus.Type = "Char";            
               _Felhasznalo_id.Name = "KRT_FelhasznaloProfilok.Felhasznalo_id";
               _Felhasznalo_id.Type = "Guid";            
               _Obj_Id.Name = "KRT_FelhasznaloProfilok.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _Nev.Name = "KRT_FelhasznaloProfilok.Nev";
               _Nev.Type = "String";            
               _XML.Name = "KRT_FelhasznaloProfilok.XML";
               _XML.Type = "String";            
               _ErvKezd.Name = "KRT_FelhasznaloProfilok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_FelhasznaloProfilok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_id = new Field();

        public Field Felhasznalo_id
        {
            get { return _Felhasznalo_id; }
            set { _Felhasznalo_id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _XML = new Field();

        public Field XML
        {
            get { return _XML; }
            set { _XML = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}