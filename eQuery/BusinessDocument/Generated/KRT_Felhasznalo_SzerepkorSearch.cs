
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Felhasznalo_Szerepkor eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_Felhasznalo_SzerepkorSearch: BaseSearchObject    {
      public KRT_Felhasznalo_SzerepkorSearch()
      {            
                     _Id.Name = "KRT_Felhasznalo_Szerepkor.Id";
               _Id.Type = "Guid";            
               _CsoportTag_Id.Name = "KRT_Felhasznalo_Szerepkor.CsoportTag_Id";
               _CsoportTag_Id.Type = "Guid";            
               _Csoport_Id.Name = "KRT_Felhasznalo_Szerepkor.Csoport_Id";
               _Csoport_Id.Type = "Guid";            
               _Felhasznalo_Id.Name = "KRT_Felhasznalo_Szerepkor.Felhasznalo_Id";
               _Felhasznalo_Id.Type = "Guid";            
               _Szerepkor_Id.Name = "KRT_Felhasznalo_Szerepkor.Szerepkor_Id";
               _Szerepkor_Id.Type = "Guid";            
               _Helyettesites_Id.Name = "KRT_Felhasznalo_Szerepkor.Helyettesites_Id";
               _Helyettesites_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_Felhasznalo_Szerepkor.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Felhasznalo_Szerepkor.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _CsoportTag_Id = new Field();

        public Field CsoportTag_Id
        {
            get { return _CsoportTag_Id; }
            set { _CsoportTag_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id = new Field();

        public Field Csoport_Id
        {
            get { return _Csoport_Id; }
            set { _Csoport_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id = new Field();

        public Field Felhasznalo_Id
        {
            get { return _Felhasznalo_Id; }
            set { _Felhasznalo_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Szerepkor_Id = new Field();

        public Field Szerepkor_Id
        {
            get { return _Szerepkor_Id; }
            set { _Szerepkor_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Helyettesites_Id = new Field();

        public Field Helyettesites_Id
        {
            get { return _Helyettesites_Id; }
            set { _Helyettesites_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}