
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Felhasznalok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_FelhasznalokSearch: BaseSearchObject    {
      public KRT_FelhasznalokSearch()
      {            
               _Id.Name = "KRT_Felhasznalok.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_Felhasznalok.Org";
               _Org.Type = "Guid";            
               _Partner_id.Name = "KRT_Felhasznalok.Partner_id";
               _Partner_id.Type = "Guid";
               _WrongPassCnt.Name = "KRT_Felhasznalok.WrongPassCnt";
               _WrongPassCnt.Type = "int";
               _Tipus.Name = "KRT_Felhasznalok.Tipus";
               _Tipus.Type = "String";            
               _UserNev.Name = "KRT_Felhasznalok.UserNev";
               _UserNev.Type = "String";            
               _Nev.Name = "KRT_Felhasznalok.Nev";
               _Nev.Type = "String";            
               _Jelszo.Name = "KRT_Felhasznalok.Jelszo";
               _Jelszo.Type = "String";            
               _JelszoLejaratIdo.Name = "KRT_Felhasznalok.JelszoLejaratIdo";
               _JelszoLejaratIdo.Type = "DateTime";            
               _System.Name = "KRT_Felhasznalok.System";
               _System.Type = "Char";            
               _Kiszolgalo.Name = "KRT_Felhasznalok.Kiszolgalo";
               _Kiszolgalo.Type = "Char";            
               _DefaultPrivatKulcs.Name = "KRT_Felhasznalok.DefaultPrivatKulcs";
               _DefaultPrivatKulcs.Type = "String";            
               _Partner_Id_Munkahely.Name = "KRT_Felhasznalok.Partner_Id_Munkahely";
               _Partner_Id_Munkahely.Type = "Guid";            
               _MaxMinosites.Name = "KRT_Felhasznalok.MaxMinosites";
               _MaxMinosites.Type = "String";            
               _EMail.Name = "KRT_Felhasznalok.EMail";
               _EMail.Type = "String";            
               _Engedelyezett.Name = "KRT_Felhasznalok.Engedelyezett";
               _Engedelyezett.Type = "Char";            
               _Telefonszam.Name = "KRT_Felhasznalok.Telefonszam";
               _Telefonszam.Type = "String";            
               _Beosztas.Name = "KRT_Felhasznalok.Beosztas";
               _Beosztas.Type = "String";            
               _ErvKezd.Name = "KRT_Felhasznalok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Felhasznalok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Partner_id = new Field();

        public Field Partner_id
        {
            get { return _Partner_id; }
            set { _Partner_id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UserNev = new Field();

        public Field UserNev
        {
            get { return _UserNev; }
            set { _UserNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Jelszo = new Field();

        public Field Jelszo
        {
            get { return _Jelszo; }
            set { _Jelszo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _JelszoLejaratIdo = new Field();

        public Field JelszoLejaratIdo
        {
            get { return _JelszoLejaratIdo; }
            set { _JelszoLejaratIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _System = new Field();

        public Field System
        {
            get { return _System; }
            set { _System = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kiszolgalo = new Field();

        public Field Kiszolgalo
        {
            get { return _Kiszolgalo; }
            set { _Kiszolgalo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _DefaultPrivatKulcs = new Field();

        public Field DefaultPrivatKulcs
        {
            get { return _DefaultPrivatKulcs; }
            set { _DefaultPrivatKulcs = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Partner_Id_Munkahely = new Field();

        public Field Partner_Id_Munkahely
        {
            get { return _Partner_Id_Munkahely; }
            set { _Partner_Id_Munkahely = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MaxMinosites = new Field();

        public Field MaxMinosites
        {
            get { return _MaxMinosites; }
            set { _MaxMinosites = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _EMail = new Field();

        public Field EMail
        {
            get { return _EMail; }
            set { _EMail = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Engedelyezett = new Field();

        public Field Engedelyezett
        {
            get { return _Engedelyezett; }
            set { _Engedelyezett = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Telefonszam = new Field();

        public Field Telefonszam
        {
            get { return _Telefonszam; }
            set { _Telefonszam = value; }
        }

        /// <summary>
        /// WrongPassCnt property </summary>
        private Field _WrongPassCnt = new Field();

        public Field WrongPassCnt
        {
            get { return _WrongPassCnt; }
            set { _WrongPassCnt = value; }
        }

        /// <summary>
        /// Id property </summary>
        private Field _Beosztas = new Field();

        public Field Beosztas
        {
            get { return _Beosztas; }
            set { _Beosztas = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}