﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Felhasznalok_Halozati_Nyomtatoi eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public partial class KRT_Felhasznalok_Halozati_NyomtatoiSearch
    {
        public KRT_Felhasznalok_Halozati_NyomtatoiSearch()
        {
            _Id.Name = "KRT_Felhasznalok_Halozati_Nyomtatoi.Id";
            _Id.Type = "Guid";
            _Felhasznalo_Id.Name = "KRT_Felhasznalok_Halozati_Nyomtatoi.Felhasznalo_Id";
            _Felhasznalo_Id.Type = "Guid";
            _Halozati_Nyomtato_Id.Name = "KRT_Felhasznalok_Halozati_Nyomtatoi.Halozati_Nyomtato_Id";
            _Halozati_Nyomtato_Id.Type = "Guid";
            _ErvKezd.Name = "KRT_Felhasznalok_Halozati_Nyomtatoi.ErvKezd";
            _ErvKezd.Type = "DateTime"; _ErvKezd.Operator = Query.Operators.lessorequal;
            _ErvKezd.Value = "getdate()";
            _ErvVege.Name = "KRT_Felhasznalok_Halozati_Nyomtatoi.ErvVege";
            _ErvVege.Type = "DateTime"; _ErvVege.Operator = Query.Operators.greaterorequal;
            _ErvVege.Value = "getdate()";
        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonosító
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Felhasznalo_Id property
        /// Felhasznalo_Id
        /// </summary>
        private Field _Felhasznalo_Id = new Field();

        public Field Felhasznalo_Id
        {
            get { return _Felhasznalo_Id; }
            set { _Felhasznalo_Id = value; }
        }
                          
           
        /// <summary>
        /// Halozati_Nyomtato_Id property
        /// Halozati_Nyomtato_Id
        /// </summary>
        private Field _Halozati_Nyomtato_Id = new Field();

        public Field Halozati_Nyomtato_Id
        {
            get { return _Halozati_Nyomtato_Id; }
            set { _Halozati_Nyomtato_Id = value; }
        }
       
        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
    }
}
