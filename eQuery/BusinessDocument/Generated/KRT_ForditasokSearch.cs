
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Forditasok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_ForditasokSearch: BaseSearchObject    {
      public KRT_ForditasokSearch()
      {            
                     _Id.Name = "KRT_Forditasok.Id";
               _Id.Type = "Guid";            
               _NyelvKod.Name = "KRT_Forditasok.NyelvKod";
               _NyelvKod.Type = "String";            
               _OrgKod.Name = "KRT_Forditasok.OrgKod";
               _OrgKod.Type = "String";            
               _Modul.Name = "KRT_Forditasok.Modul";
               _Modul.Type = "String";            
               _Komponens.Name = "KRT_Forditasok.Komponens";
               _Komponens.Type = "String";            
               _ObjAzonosito.Name = "KRT_Forditasok.ObjAzonosito";
               _ObjAzonosito.Type = "String";            
               _Forditas.Name = "KRT_Forditasok.Forditas";
               _Forditas.Type = "String";            
               _ErvKezd.Name = "KRT_Forditasok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Forditasok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// NyelvKod property
        /// pl. hu-HU
        /// </summary>
        private Field _NyelvKod = new Field();

        public Field NyelvKod
        {
            get { return _NyelvKod; }
            set { _NyelvKod = value; }
        }
                          
           
        /// <summary>
        /// OrgKod property
        /// pl. FPH, BOPMH, NMHH
        /// </summary>
        private Field _OrgKod = new Field();

        public Field OrgKod
        {
            get { return _OrgKod; }
            set { _OrgKod = value; }
        }
                          
           
        /// <summary>
        /// Modul property
        /// pl. eAdmin, eRecord
        /// </summary>
        private Field _Modul = new Field();

        public Field Modul
        {
            get { return _Modul; }
            set { _Modul = value; }
        }
                          
           
        /// <summary>
        /// Komponens property
        /// ASP-NET oldal vagy control neve, el�r�s� �ttal.
        /// pl EgyszerusitettIktatasForm.aspx
        /// pl.Component/ListHeader.ascx
        /// </summary>
        private Field _Komponens = new Field();

        public Field Komponens
        {
            get { return _Komponens; }
            set { _Komponens = value; }
        }
                          
           
        /// <summary>
        /// ObjAzonosito property
        /// A ford�tand� elem azonos�t�ja: label id, k�dt�r id.....
        /// </summary>
        private Field _ObjAzonosito = new Field();

        public Field ObjAzonosito
        {
            get { return _ObjAzonosito; }
            set { _ObjAzonosito = value; }
        }
                          
           
        /// <summary>
        /// Forditas property
        /// 
        /// </summary>
        private Field _Forditas = new Field();

        public Field Forditas
        {
            get { return _Forditas; }
            set { _Forditas = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}