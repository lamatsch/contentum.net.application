
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_FunkciLista eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_FunkciListaSearch
    {
      public KRT_FunkciListaSearch()
      {               _Id.Name = "KRT_FunkciLista.Id";
               _Id.Type = "Guid";            
               _Funkcio_Id.Name = "KRT_FunkciLista.Funkcio_Id";
               _Funkcio_Id.Type = "Guid";            
               _Funkcio_Id_Hivott.Name = "KRT_FunkciLista.Funkcio_Id_Hivott";
               _Funkcio_Id_Hivott.Type = "Guid";            
               _FutasiSorrend.Name = "KRT_FunkciLista.FutasiSorrend";
               _FutasiSorrend.Type = "Int32";            
               _ErvKezd.Name = "KRT_FunkciLista.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_FunkciLista.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Funkcio_Id = new Field();

        public Field Funkcio_Id
        {
            get { return _Funkcio_Id; }
            set { _Funkcio_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Funkcio_Id_Hivott = new Field();

        public Field Funkcio_Id_Hivott
        {
            get { return _Funkcio_Id_Hivott; }
            set { _Funkcio_Id_Hivott = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FutasiSorrend = new Field();

        public Field FutasiSorrend
        {
            get { return _FutasiSorrend; }
            set { _FutasiSorrend = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}