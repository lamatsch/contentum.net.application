
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Funkciok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class KRT_FunkciokSearch: BaseSearchObject    {
      public KRT_FunkciokSearch()
      {            
                     _Id.Name = "KRT_Funkciok.Id";
               _Id.Type = "Guid";            
               _Kod.Name = "KRT_Funkciok.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_Funkciok.Nev";
               _Nev.Type = "String";            
               _ObjTipus_Id_AdatElem.Name = "KRT_Funkciok.ObjTipus_Id_AdatElem";
               _ObjTipus_Id_AdatElem.Type = "Guid";            
               _ObjStat_Id_Kezd.Name = "KRT_Funkciok.ObjStat_Id_Kezd";
               _ObjStat_Id_Kezd.Type = "Guid";            
               _Alkalmazas_Id.Name = "KRT_Funkciok.Alkalmazas_Id";
               _Alkalmazas_Id.Type = "Guid";            
               _Muvelet_Id.Name = "KRT_Funkciok.Muvelet_Id";
               _Muvelet_Id.Type = "Guid";            
               _ObjStat_Id_Veg.Name = "KRT_Funkciok.ObjStat_Id_Veg";
               _ObjStat_Id_Veg.Type = "Guid";            
               _Leiras.Name = "KRT_Funkciok.Leiras";
               _Leiras.Type = "String";            
               _Funkcio_Id_Szulo.Name = "KRT_Funkciok.Funkcio_Id_Szulo";
               _Funkcio_Id_Szulo.Type = "Guid";            
               _Csoportosito.Name = "KRT_Funkciok.Csoportosito";
               _Csoportosito.Type = "Char";            
               _Modosithato.Name = "KRT_Funkciok.Modosithato";
               _Modosithato.Type = "Char";            
               _Org.Name = "KRT_Funkciok.Org";
               _Org.Type = "Guid";            
               _MunkanaploJelzo.Name = "KRT_Funkciok.MunkanaploJelzo";
               _MunkanaploJelzo.Type = "Char";            
               _FeladatJelzo.Name = "KRT_Funkciok.FeladatJelzo";
               _FeladatJelzo.Type = "Char";            
               _KeziFeladatJelzo.Name = "KRT_Funkciok.KeziFeladatJelzo";
               _KeziFeladatJelzo.Type = "Char";            
               _ErvKezd.Name = "KRT_Funkciok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Funkciok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTipus_Id_AdatElem = new Field();

        public Field ObjTipus_Id_AdatElem
        {
            get { return _ObjTipus_Id_AdatElem; }
            set { _ObjTipus_Id_AdatElem = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjStat_Id_Kezd = new Field();

        public Field ObjStat_Id_Kezd
        {
            get { return _ObjStat_Id_Kezd; }
            set { _ObjStat_Id_Kezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Alkalmazas_Id = new Field();

        public Field Alkalmazas_Id
        {
            get { return _Alkalmazas_Id; }
            set { _Alkalmazas_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Muvelet_Id = new Field();

        public Field Muvelet_Id
        {
            get { return _Muvelet_Id; }
            set { _Muvelet_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjStat_Id_Veg = new Field();

        public Field ObjStat_Id_Veg
        {
            get { return _ObjStat_Id_Veg; }
            set { _ObjStat_Id_Veg = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Funkcio_Id_Szulo = new Field();

        public Field Funkcio_Id_Szulo
        {
            get { return _Funkcio_Id_Szulo; }
            set { _Funkcio_Id_Szulo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoportosito = new Field();

        public Field Csoportosito
        {
            get { return _Csoportosito; }
            set { _Csoportosito = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Modosithato = new Field();

        public Field Modosithato
        {
            get { return _Modosithato; }
            set { _Modosithato = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MunkanaploJelzo = new Field();

        public Field MunkanaploJelzo
        {
            get { return _MunkanaploJelzo; }
            set { _MunkanaploJelzo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _FeladatJelzo = new Field();

        public Field FeladatJelzo
        {
            get { return _FeladatJelzo; }
            set { _FeladatJelzo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KeziFeladatJelzo = new Field();

        public Field KeziFeladatJelzo
        {
            get { return _KeziFeladatJelzo; }
            set { _KeziFeladatJelzo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}