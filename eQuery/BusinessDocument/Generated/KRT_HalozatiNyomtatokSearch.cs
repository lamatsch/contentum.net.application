﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Halozati_Nyomtatok eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public partial class KRT_HalozatiNyomtatokSearch: BaseSearchObject    {
        public KRT_HalozatiNyomtatokSearch()
        {
            _Id.Name = "KRT_Halozati_Nyomtatok.Id";
            _Id.Type = "Guid";
            _Nev.Name = "KRT_Halozati_Nyomtatok.Nev";
            _Nev.Type = "String";
            _Cim.Name = "KRT_Halozati_Nyomtatok.Cim";
            _Cim.Type = "String";
            _ErvKezd.Name = "KRT_Halozati_Nyomtatok.ErvKezd";
            _ErvKezd.Type = "DateTime"; _ErvKezd.Operator = Query.Operators.lessorequal;
            _ErvKezd.Value = "getdate()";
            _ErvVege.Name = "KRT_Halozati_Nyomtatok.ErvVege";
            _ErvVege.Type = "DateTime"; _ErvVege.Operator = Query.Operators.greaterorequal;
            _ErvVege.Value = "getdate()";
        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonosító
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// Nev
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Cim property
        /// Cim
        /// </summary>
        private Field _Cim = new Field();

        public Field Cim
        {
            get { return _Cim; }
            set { _Cim = value; }
        }
       
        /// <summary>
        /// ErvKezd property
        /// Érvényesség kezdete (általában dátum!) (lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// Érvényesség  vége (általában dátum!)(lehet, h csinálunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
    }
}
