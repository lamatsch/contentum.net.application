
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_HataridosFeladatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_HataridosFeladatokSearch    {
      public KRT_HataridosFeladatokSearch()
      {            
                     _Id.Name = "KRT_HataridosFeladatok.Id";
               _Id.Type = "Guid";            
               _Felhasznalo_id.Name = "KRT_HataridosFeladatok.Felhasznalo_id";
               _Felhasznalo_id.Type = "Guid";            
               _Kod.Name = "KRT_HataridosFeladatok.Kod";
               _Kod.Type = "String";            
               _Prioritas.Name = "KRT_HataridosFeladatok.Prioritas";
               _Prioritas.Type = "Int32";            
               _Feladat.Name = "KRT_HataridosFeladatok.Feladat";
               _Feladat.Type = "String";            
               _ReagalasiIdo.Name = "KRT_HataridosFeladatok.ReagalasiIdo";
               _ReagalasiIdo.Type = "DateTime";            
               _Scontro.Name = "KRT_HataridosFeladatok.Scontro";
               _Scontro.Type = "DateTime";            
               _Hatarido.Name = "KRT_HataridosFeladatok.Hatarido";
               _Hatarido.Type = "DateTime";            
               _Obj_Id.Name = "KRT_HataridosFeladatok.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id.Name = "KRT_HataridosFeladatok.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_HataridosFeladatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_HataridosFeladatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_id = new Field();

        public Field Felhasznalo_id
        {
            get { return _Felhasznalo_id; }
            set { _Felhasznalo_id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Prioritas = new Field();

        public Field Prioritas
        {
            get { return _Prioritas; }
            set { _Prioritas = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Feladat = new Field();

        public Field Feladat
        {
            get { return _Feladat; }
            set { _Feladat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ReagalasiIdo = new Field();

        public Field ReagalasiIdo
        {
            get { return _ReagalasiIdo; }
            set { _ReagalasiIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Scontro = new Field();

        public Field Scontro
        {
            get { return _Scontro; }
            set { _Scontro = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Hatarido = new Field();

        public Field Hatarido
        {
            get { return _Hatarido; }
            set { _Hatarido = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}