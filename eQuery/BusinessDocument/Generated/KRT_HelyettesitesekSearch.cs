
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Helyettesitesek eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public partial class KRT_HelyettesitesekSearch : BaseSearchObject
    {
      public KRT_HelyettesitesekSearch()
      {            
                     _Id.Name = "KRT_Helyettesitesek.Id";
               _Id.Type = "Guid";            
               _Felhasznalo_ID_helyettesitett.Name = "KRT_Helyettesitesek.Felhasznalo_ID_helyettesitett";
               _Felhasznalo_ID_helyettesitett.Type = "Guid";            
               _CsoportTag_ID_helyettesitett.Name = "KRT_Helyettesitesek.CsoportTag_ID_helyettesitett";
               _CsoportTag_ID_helyettesitett.Type = "Guid";            
               _Felhasznalo_ID_helyettesito.Name = "KRT_Helyettesitesek.Felhasznalo_ID_helyettesito";
               _Felhasznalo_ID_helyettesito.Type = "Guid";            
               _HelyettesitesMod.Name = "KRT_Helyettesitesek.HelyettesitesMod";
               _HelyettesitesMod.Type = "String";            
               _HelyettesitesKezd.Name = "KRT_Helyettesitesek.HelyettesitesKezd";
               _HelyettesitesKezd.Type = "DateTime";            
               _HelyettesitesVege.Name = "KRT_Helyettesitesek.HelyettesitesVege";
               _HelyettesitesVege.Type = "DateTime";            
               _Megjegyzes.Name = "KRT_Helyettesitesek.Megjegyzes";
               _Megjegyzes.Type = "String";            
               _ErvKezd.Name = "KRT_Helyettesitesek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Helyettesitesek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_ID_helyettesitett = new Field();

        public Field Felhasznalo_ID_helyettesitett
        {
            get { return _Felhasznalo_ID_helyettesitett; }
            set { _Felhasznalo_ID_helyettesitett = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _CsoportTag_ID_helyettesitett = new Field();

        public Field CsoportTag_ID_helyettesitett
        {
            get { return _CsoportTag_ID_helyettesitett; }
            set { _CsoportTag_ID_helyettesitett = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_ID_helyettesito = new Field();

        public Field Felhasznalo_ID_helyettesito
        {
            get { return _Felhasznalo_ID_helyettesito; }
            set { _Felhasznalo_ID_helyettesito = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HelyettesitesMod = new Field();

        public Field HelyettesitesMod
        {
            get { return _HelyettesitesMod; }
            set { _HelyettesitesMod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HelyettesitesKezd = new Field();

        public Field HelyettesitesKezd
        {
            get { return _HelyettesitesKezd; }
            set { _HelyettesitesKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HelyettesitesVege = new Field();

        public Field HelyettesitesVege
        {
            get { return _HelyettesitesVege; }
            set { _HelyettesitesVege = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Megjegyzes = new Field();

        public Field Megjegyzes
        {
            get { return _Megjegyzes; }
            set { _Megjegyzes = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}