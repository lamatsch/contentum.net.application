
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Jogosultak eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_JogosultakSearch: BaseSearchObject    {
      public KRT_JogosultakSearch()
      {            
                     _Id.Name = "KRT_Jogosultak.Id";
               _Id.Type = "Guid";            
               _Csoport_Id_Jogalany.Name = "KRT_Jogosultak.Csoport_Id_Jogalany";
               _Csoport_Id_Jogalany.Type = "Guid";            
               _Jogszint.Name = "KRT_Jogosultak.Jogszint";
               _Jogszint.Type = "String";            
               _Obj_Id.Name = "KRT_Jogosultak.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _Orokolheto.Name = "KRT_Jogosultak.Orokolheto";
               _Orokolheto.Type = "Char";            
               _Kezi.Name = "KRT_Jogosultak.Kezi";
               _Kezi.Type = "Char";            
               _Tipus.Name = "KRT_Jogosultak.Tipus";
               _Tipus.Type = "Char";            
               _ErvKezd.Name = "KRT_Jogosultak.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Jogosultak.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Jogalany = new Field();

        public Field Csoport_Id_Jogalany
        {
            get { return _Csoport_Id_Jogalany; }
            set { _Csoport_Id_Jogalany = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Jogszint = new Field();

        public Field Jogszint
        {
            get { return _Jogszint; }
            set { _Jogszint = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Orokolheto = new Field();

        public Field Orokolheto
        {
            get { return _Orokolheto; }
            set { _Orokolheto = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kezi = new Field();

        public Field Kezi
        {
            get { return _Kezi; }
            set { _Kezi = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}