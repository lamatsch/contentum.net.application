
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_KodCsoportok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_KodCsoportokSearch: BaseSearchObject    {
      public KRT_KodCsoportokSearch()
      {            
                     _Id.Name = "KRT_KodCsoportok.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_KodCsoportok.Org";
               _Org.Type = "Guid";            
               _KodTarak_Id_KodcsoportTipus.Name = "KRT_KodCsoportok.KodTarak_Id_KodcsoportTipus";
               _KodTarak_Id_KodcsoportTipus.Type = "Guid";            
               _Kod.Name = "KRT_KodCsoportok.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_KodCsoportok.Nev";
               _Nev.Type = "String";            
               _Modosithato.Name = "KRT_KodCsoportok.Modosithato";
               _Modosithato.Type = "Char";            
               _Hossz.Name = "KRT_KodCsoportok.Hossz";
               _Hossz.Type = "Int32";            
               _Csoport_Id_Tulaj.Name = "KRT_KodCsoportok.Csoport_Id_Tulaj";
               _Csoport_Id_Tulaj.Type = "Guid";            
               _Leiras.Name = "KRT_KodCsoportok.Leiras";
               _Leiras.Type = "String";            
               _BesorolasiSema.Name = "KRT_KodCsoportok.BesorolasiSema";
               _BesorolasiSema.Type = "Char";            
               _KiegAdat.Name = "KRT_KodCsoportok.KiegAdat";
               _KiegAdat.Type = "Char";            
               _KiegMezo.Name = "KRT_KodCsoportok.KiegMezo";
               _KiegMezo.Type = "String";            
               _KiegAdattipus.Name = "KRT_KodCsoportok.KiegAdattipus";
               _KiegAdattipus.Type = "Char";            
               _KiegSzotar.Name = "KRT_KodCsoportok.KiegSzotar";
               _KiegSzotar.Type = "String";            
               _ErvKezd.Name = "KRT_KodCsoportok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_KodCsoportok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KodTarak_Id_KodcsoportTipus = new Field();

        public Field KodTarak_Id_KodcsoportTipus
        {
            get { return _KodTarak_Id_KodcsoportTipus; }
            set { _KodTarak_Id_KodcsoportTipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Modosithato = new Field();

        public Field Modosithato
        {
            get { return _Modosithato; }
            set { _Modosithato = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Hossz = new Field();

        public Field Hossz
        {
            get { return _Hossz; }
            set { _Hossz = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Tulaj = new Field();

        public Field Csoport_Id_Tulaj
        {
            get { return _Csoport_Id_Tulaj; }
            set { _Csoport_Id_Tulaj = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _BesorolasiSema = new Field();

        public Field BesorolasiSema
        {
            get { return _BesorolasiSema; }
            set { _BesorolasiSema = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KiegAdat = new Field();

        public Field KiegAdat
        {
            get { return _KiegAdat; }
            set { _KiegAdat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KiegMezo = new Field();

        public Field KiegMezo
        {
            get { return _KiegMezo; }
            set { _KiegMezo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KiegAdattipus = new Field();

        public Field KiegAdattipus
        {
            get { return _KiegAdattipus; }
            set { _KiegAdattipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KiegSzotar = new Field();

        public Field KiegSzotar
        {
            get { return _KiegSzotar; }
            set { _KiegSzotar = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}