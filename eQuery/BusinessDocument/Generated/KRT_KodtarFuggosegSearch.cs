
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_KodtarFuggoseg eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_KodtarFuggosegSearch: BaseSearchObject    {
      public KRT_KodtarFuggosegSearch()
      {            
                     _Id.Name = "KRT_KodtarFuggoseg.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_KodtarFuggoseg.Org";
               _Org.Type = "Guid";            
               _Vezerlo_KodCsoport_Id.Name = "KRT_KodtarFuggoseg.Vezerlo_KodCsoport_Id";
               _Vezerlo_KodCsoport_Id.Type = "Guid";            
               _Fuggo_KodCsoport_Id.Name = "KRT_KodtarFuggoseg.Fuggo_KodCsoport_Id";
               _Fuggo_KodCsoport_Id.Type = "Guid";            
               _Adat.Name = "KRT_KodtarFuggoseg.Adat";
               _Adat.Type = "String";            
               _Aktiv.Name = "KRT_KodtarFuggoseg.Aktiv";
               _Aktiv.Type = "Char";            
               _ErvKezd.Name = "KRT_KodtarFuggoseg.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_KodtarFuggoseg.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Vezerlo_KodCsoport_Id property
        /// Kodcsoport
        /// </summary>
        private Field _Vezerlo_KodCsoport_Id = new Field();

        public Field Vezerlo_KodCsoport_Id
        {
            get { return _Vezerlo_KodCsoport_Id; }
            set { _Vezerlo_KodCsoport_Id = value; }
        }
                          
           
        /// <summary>
        /// Fuggo_KodCsoport_Id property
        /// Sok esetben a Kodt�r �rt�k konkr�t t�bl�t jelent... 
        /// Ezen esetekben (pl. kcs.JOGALANY_TIPUS) a v�gfelhaszn�l�s nem a Kod, hanem 
        /// az AdatElem (legt�bbsz�r t�bla) GUID -ja.
        /// Az�rt nevezz�k az oszlopot AdatElem -nek, mivel Meta defin�ci�t
        /// Min�s�t�siSzint-�gyK�d-Elj�r�siSzint-T�rgyk�d �sszerendel�sre is megadhatunk.
        /// </summary>
        private Field _Fuggo_KodCsoport_Id = new Field();

        public Field Fuggo_KodCsoport_Id
        {
            get { return _Fuggo_KodCsoport_Id; }
            set { _Fuggo_KodCsoport_Id = value; }
        }
                          
           
        /// <summary>
        /// Adat property
        /// 
        /// </summary>
        private Field _Adat = new Field();

        public Field Adat
        {
            get { return _Adat; }
            set { _Adat = value; }
        }
                          
           
        /// <summary>
        /// Aktiv property
        /// Review: Nvarchar(80) Sorrend
        /// </summary>
        private Field _Aktiv = new Field();

        public Field Aktiv
        {
            get { return _Aktiv; }
            set { _Aktiv = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}