
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_KodTarak eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_KodTarakSearch: BaseSearchObject    {
      public KRT_KodTarakSearch()
      {            
                     _Id.Name = "KRT_KodTarak.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_KodTarak.Org";
               _Org.Type = "Guid";            
               _KodCsoport_Id.Name = "KRT_KodTarak.KodCsoport_Id";
               _KodCsoport_Id.Type = "Guid";            
               _ObjTip_Id_AdatElem.Name = "KRT_KodTarak.ObjTip_Id_AdatElem";
               _ObjTip_Id_AdatElem.Type = "Guid";            
               _Obj_Id.Name = "KRT_KodTarak.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _Kod.Name = "KRT_KodTarak.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_KodTarak.Nev";
               _Nev.Type = "String";            
               _RovidNev.Name = "KRT_KodTarak.RovidNev";
               _RovidNev.Type = "String";            
               _Egyeb.Name = "KRT_KodTarak.Egyeb";
               _Egyeb.Type = "String";            
               _Modosithato.Name = "KRT_KodTarak.Modosithato";
               _Modosithato.Type = "Char";            
               _Sorrend.Name = "KRT_KodTarak.Sorrend";
               _Sorrend.Type = "String";            
               _ErvKezd.Name = "KRT_KodTarak.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_KodTarak.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KodCsoport_Id = new Field();

        public Field KodCsoport_Id
        {
            get { return _KodCsoport_Id; }
            set { _KodCsoport_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id_AdatElem = new Field();

        public Field ObjTip_Id_AdatElem
        {
            get { return _ObjTip_Id_AdatElem; }
            set { _ObjTip_Id_AdatElem = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _RovidNev = new Field();

        public Field RovidNev
        {
            get { return _RovidNev; }
            set { _RovidNev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Egyeb = new Field();

        public Field Egyeb
        {
            get { return _Egyeb; }
            set { _Egyeb = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Modosithato = new Field();

        public Field Modosithato
        {
            get { return _Modosithato; }
            set { _Modosithato = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Sorrend = new Field();

        public Field Sorrend
        {
            get { return _Sorrend; }
            set { _Sorrend = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}