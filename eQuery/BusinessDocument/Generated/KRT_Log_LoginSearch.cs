
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Log_Login eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_Log_LoginSearch: BaseSearchObject    {
      public KRT_Log_LoginSearch()
      {            
                     _Date.Name = "KRT_Log_Login.Date";
               _Date.Type = "DateTime";            
               _Status.Name = "KRT_Log_Login.Status";
               _Status.Type = "Char";            
               _StartDate.Name = "KRT_Log_Login.StartDate";
               _StartDate.Type = "DateTime";            
               _Felhasznalo_Id.Name = "KRT_Log_Login.Felhasznalo_Id";
               _Felhasznalo_Id.Type = "Guid";            
               _Felhasznalo_Nev.Name = "KRT_Log_Login.Felhasznalo_Nev";
               _Felhasznalo_Nev.Type = "String";            
               _CsoportTag_Id.Name = "KRT_Log_Login.CsoportTag_Id";
               _CsoportTag_Id.Type = "Guid";            
               _Helyettesito_Id.Name = "KRT_Log_Login.Helyettesito_Id";
               _Helyettesito_Id.Type = "Guid";            
               _Helyettesito_Nev.Name = "KRT_Log_Login.Helyettesito_Nev";
               _Helyettesito_Nev.Type = "String";            
               _Helyettesites_Id.Name = "KRT_Log_Login.Helyettesites_Id";
               _Helyettesites_Id.Type = "Guid";            
               _Helyettesites_Mod.Name = "KRT_Log_Login.Helyettesites_Mod";
               _Helyettesites_Mod.Type = "String";            
               _LoginType.Name = "KRT_Log_Login.LoginType";
               _LoginType.Type = "String";            
               _UserHostAddress.Name = "KRT_Log_Login.UserHostAddress";
               _UserHostAddress.Type = "String";            
               _Level.Name = "KRT_Log_Login.Level";
               _Level.Type = "String";            
               _HibaKod.Name = "KRT_Log_Login.HibaKod";
               _HibaKod.Type = "String";            
               _HibaUzenet.Name = "KRT_Log_Login.HibaUzenet";
               _HibaUzenet.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Date = new Field();

        public Field Date
        {
            get { return _Date; }
            set { _Date = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Status = new Field();

        public Field Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _StartDate = new Field();

        public Field StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id = new Field();

        public Field Felhasznalo_Id
        {
            get { return _Felhasznalo_Id; }
            set { _Felhasznalo_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Nev = new Field();

        public Field Felhasznalo_Nev
        {
            get { return _Felhasznalo_Nev; }
            set { _Felhasznalo_Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _CsoportTag_Id = new Field();

        public Field CsoportTag_Id
        {
            get { return _CsoportTag_Id; }
            set { _CsoportTag_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Helyettesito_Id = new Field();

        public Field Helyettesito_Id
        {
            get { return _Helyettesito_Id; }
            set { _Helyettesito_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Helyettesito_Nev = new Field();

        public Field Helyettesito_Nev
        {
            get { return _Helyettesito_Nev; }
            set { _Helyettesito_Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Helyettesites_Id = new Field();

        public Field Helyettesites_Id
        {
            get { return _Helyettesites_Id; }
            set { _Helyettesites_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Helyettesites_Mod = new Field();

        public Field Helyettesites_Mod
        {
            get { return _Helyettesites_Mod; }
            set { _Helyettesites_Mod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LoginType = new Field();

        public Field LoginType
        {
            get { return _LoginType; }
            set { _LoginType = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UserHostAddress = new Field();

        public Field UserHostAddress
        {
            get { return _UserHostAddress; }
            set { _UserHostAddress = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Level = new Field();

        public Field Level
        {
            get { return _Level; }
            set { _Level = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HibaKod = new Field();

        public Field HibaKod
        {
            get { return _HibaKod; }
            set { _HibaKod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HibaUzenet = new Field();

        public Field HibaUzenet
        {
            get { return _HibaUzenet; }
            set { _HibaUzenet = value; }
        }
                              }

}