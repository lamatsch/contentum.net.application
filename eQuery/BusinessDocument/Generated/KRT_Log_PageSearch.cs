
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Log_Page eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_Log_PageSearch: BaseSearchObject    {
      public KRT_Log_PageSearch()
      {            
                     _Date.Name = "KRT_Log_Page.Date";
               _Date.Type = "DateTime";            
               _Status.Name = "KRT_Log_Page.Status";
               _Status.Type = "Char";            
               _StartDate.Name = "KRT_Log_Page.StartDate";
               _StartDate.Type = "DateTime";            
               _Modul_Id.Name = "KRT_Log_Page.Modul_Id";
               _Modul_Id.Type = "Guid";            
               _Login_Tranz_id.Name = "KRT_Log_Page.Login_Tranz_id";
               _Login_Tranz_id.Type = "Guid";            
               _Name.Name = "KRT_Log_Page.Name";
               _Name.Type = "String";            
               _Url.Name = "KRT_Log_Page.Url";
               _Url.Type = "String";            
               _QueryString.Name = "KRT_Log_Page.QueryString";
               _QueryString.Type = "String";            
               _Command.Name = "KRT_Log_Page.Command";
               _Command.Type = "String";            
               _IsPostBack.Name = "KRT_Log_Page.IsPostBack";
               _IsPostBack.Type = "Char";            
               _IsAsync.Name = "KRT_Log_Page.IsAsync";
               _IsAsync.Type = "Char";            
               _Level.Name = "KRT_Log_Page.Level";
               _Level.Type = "String";            
               _HibaKod.Name = "KRT_Log_Page.HibaKod";
               _HibaKod.Type = "String";            
               _HibaUzenet.Name = "KRT_Log_Page.HibaUzenet";
               _HibaUzenet.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Date = new Field();

        public Field Date
        {
            get { return _Date; }
            set { _Date = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Status = new Field();

        public Field Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _StartDate = new Field();

        public Field StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Modul_Id = new Field();

        public Field Modul_Id
        {
            get { return _Modul_Id; }
            set { _Modul_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Login_Tranz_id = new Field();

        public Field Login_Tranz_id
        {
            get { return _Login_Tranz_id; }
            set { _Login_Tranz_id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Name = new Field();

        public Field Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Url = new Field();

        public Field Url
        {
            get { return _Url; }
            set { _Url = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _QueryString = new Field();

        public Field QueryString
        {
            get { return _QueryString; }
            set { _QueryString = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Command = new Field();

        public Field Command
        {
            get { return _Command; }
            set { _Command = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IsPostBack = new Field();

        public Field IsPostBack
        {
            get { return _IsPostBack; }
            set { _IsPostBack = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IsAsync = new Field();

        public Field IsAsync
        {
            get { return _IsAsync; }
            set { _IsAsync = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Level = new Field();

        public Field Level
        {
            get { return _Level; }
            set { _Level = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HibaKod = new Field();

        public Field HibaKod
        {
            get { return _HibaKod; }
            set { _HibaKod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HibaUzenet = new Field();

        public Field HibaUzenet
        {
            get { return _HibaUzenet; }
            set { _HibaUzenet = value; }
        }
                              }

}