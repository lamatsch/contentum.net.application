
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Log_StoredProcedure eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class KRT_Log_StoredProcedureSearch: BaseSearchObject    {
      public KRT_Log_StoredProcedureSearch()
      {
          Init_ManualFields();
                     _Date.Name = "KRT_Log_StoredProcedure.Date";
               _Date.Type = "DateTime";            
               _Status.Name = "KRT_Log_StoredProcedure.Status";
               _Status.Type = "Char";            
               _StartDate.Name = "KRT_Log_StoredProcedure.StartDate";
               _StartDate.Type = "DateTime";            
               _WS_StartDate.Name = "KRT_Log_StoredProcedure.WS_StartDate";
               _WS_StartDate.Type = "DateTime";            
               _Name.Name = "KRT_Log_StoredProcedure.Name";
               _Name.Type = "String";            
               _Level.Name = "KRT_Log_StoredProcedure.Level";
               _Level.Type = "String";            
               _HibaKod.Name = "KRT_Log_StoredProcedure.HibaKod";
               _HibaKod.Type = "String";            
               _HibaUzenet.Name = "KRT_Log_StoredProcedure.HibaUzenet";
               _HibaUzenet.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Date = new Field();

        public Field Date
        {
            get { return _Date; }
            set { _Date = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Status = new Field();

        public Field Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _StartDate = new Field();

        public Field StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _WS_StartDate = new Field();

        public Field WS_StartDate
        {
            get { return _WS_StartDate; }
            set { _WS_StartDate = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Name = new Field();

        public Field Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Level = new Field();

        public Field Level
        {
            get { return _Level; }
            set { _Level = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HibaKod = new Field();

        public Field HibaKod
        {
            get { return _HibaKod; }
            set { _HibaKod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HibaUzenet = new Field();

        public Field HibaUzenet
        {
            get { return _HibaUzenet; }
            set { _HibaUzenet = value; }
        }
                              }

}