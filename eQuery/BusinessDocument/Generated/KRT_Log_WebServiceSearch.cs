
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Log_WebService eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class KRT_Log_WebServiceSearch: BaseSearchObject    {
      public KRT_Log_WebServiceSearch()
      {
          Init_ManualFields();
                     _Date.Name = "KRT_Log_WebService.Date";
               _Date.Type = "DateTime";            
               _Status.Name = "KRT_Log_WebService.Status";
               _Status.Type = "Char";            
               _StartDate.Name = "KRT_Log_WebService.StartDate";
               _StartDate.Type = "DateTime";            
               _ParentWS_StartDate.Name = "KRT_Log_WebService.ParentWS_StartDate";
               _ParentWS_StartDate.Type = "DateTime";            
               _Machine.Name = "KRT_Log_WebService.Machine";
               _Machine.Type = "String";            
               _Url.Name = "KRT_Log_WebService.Url";
               _Url.Type = "String";            
               _Name.Name = "KRT_Log_WebService.Name";
               _Name.Type = "String";            
               _Method.Name = "KRT_Log_WebService.Method";
               _Method.Type = "String";            
               _Level.Name = "KRT_Log_WebService.Level";
               _Level.Type = "String";            
               _HibaKod.Name = "KRT_Log_WebService.HibaKod";
               _HibaKod.Type = "String";            
               _HibaUzenet.Name = "KRT_Log_WebService.HibaUzenet";
               _HibaUzenet.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Date = new Field();

        public Field Date
        {
            get { return _Date; }
            set { _Date = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Status = new Field();

        public Field Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _StartDate = new Field();

        public Field StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ParentWS_StartDate = new Field();

        public Field ParentWS_StartDate
        {
            get { return _ParentWS_StartDate; }
            set { _ParentWS_StartDate = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Machine = new Field();

        public Field Machine
        {
            get { return _Machine; }
            set { _Machine = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Url = new Field();

        public Field Url
        {
            get { return _Url; }
            set { _Url = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Name = new Field();

        public Field Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Method = new Field();

        public Field Method
        {
            get { return _Method; }
            set { _Method = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Level = new Field();

        public Field Level
        {
            get { return _Level; }
            set { _Level = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HibaKod = new Field();

        public Field HibaKod
        {
            get { return _HibaKod; }
            set { _HibaKod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _HibaUzenet = new Field();

        public Field HibaUzenet
        {
            get { return _HibaUzenet; }
            set { _HibaUzenet = value; }
        }
                              }

}