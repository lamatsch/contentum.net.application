
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_MappaSzulei eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_MappaSzuleiSearch
    {
      public KRT_MappaSzuleiSearch()
      {               _Id.Name = "KRT_MappaSzulei.Id";
               _Id.Type = "Guid";            
               _Mappa_Id.Name = "KRT_MappaSzulei.Mappa_Id";
               _Mappa_Id.Type = "Guid";            
               _Mappa_Id_Szulo.Name = "KRT_MappaSzulei.Mappa_Id_Szulo";
               _Mappa_Id_Szulo.Type = "Guid";            
               _ErvKezd.Name = "KRT_MappaSzulei.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_MappaSzulei.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Mappa_Id = new Field();

        public Field Mappa_Id
        {
            get { return _Mappa_Id; }
            set { _Mappa_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Mappa_Id_Szulo = new Field();

        public Field Mappa_Id_Szulo
        {
            get { return _Mappa_Id_Szulo; }
            set { _Mappa_Id_Szulo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}