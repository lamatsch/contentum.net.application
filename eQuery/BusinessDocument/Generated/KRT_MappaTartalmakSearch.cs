
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_MappaTartalmak eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class KRT_MappaTartalmakSearch: BaseSearchObject    {
      public KRT_MappaTartalmakSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "KRT_MappaTartalmak.Id";
               _Id.Type = "Guid";            
               _Mappa_Id.Name = "KRT_MappaTartalmak.Mappa_Id";
               _Mappa_Id.Type = "Guid";            
               _Obj_Id.Name = "KRT_MappaTartalmak.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _Obj_type.Name = "KRT_MappaTartalmak.Obj_type";
               _Obj_type.Type = "String";            
               _Leiras.Name = "KRT_MappaTartalmak.Leiras";
               _Leiras.Type = "String";            
               _ErvKezd.Name = "KRT_MappaTartalmak.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_MappaTartalmak.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Mappa_Id = new Field();

        public Field Mappa_Id
        {
            get { return _Mappa_Id; }
            set { _Mappa_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_type = new Field();

        public Field Obj_type
        {
            get { return _Obj_type; }
            set { _Obj_type = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}