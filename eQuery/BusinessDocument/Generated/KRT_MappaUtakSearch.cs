
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_MappaUtak eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_MappaUtakSearch: BaseSearchObject    {
      public KRT_MappaUtakSearch()
      {            
                     _Id.Name = "KRT_MappaUtak.Id";
               _Id.Type = "Guid";            
               _Mappa_Id.Name = "KRT_MappaUtak.Mappa_Id";
               _Mappa_Id.Type = "Guid";            
               _PathFromORG.Name = "KRT_MappaUtak.PathFromORG";
               _PathFromORG.Type = "String";            
               _ErvKezd.Name = "KRT_MappaUtak.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_MappaUtak.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Mappa_Id = new Field();

        public Field Mappa_Id
        {
            get { return _Mappa_Id; }
            set { _Mappa_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _PathFromORG = new Field();

        public Field PathFromORG
        {
            get { return _PathFromORG; }
            set { _PathFromORG = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}