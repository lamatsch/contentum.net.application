
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Mappak eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_MappakSearch: BaseSearchObject    {
      public KRT_MappakSearch()
      {            
                     _Id.Name = "KRT_Mappak.Id";
               _Id.Type = "Guid";            
               _SzuloMappa_Id.Name = "KRT_Mappak.SzuloMappa_Id";
               _SzuloMappa_Id.Type = "Guid";            
               _BarCode.Name = "KRT_Mappak.BarCode";
               _BarCode.Type = "String";            
               _Tipus.Name = "KRT_Mappak.Tipus";
               _Tipus.Type = "String";            
               _Nev.Name = "KRT_Mappak.Nev";
               _Nev.Type = "String";            
               _Leiras.Name = "KRT_Mappak.Leiras";
               _Leiras.Type = "String";            
               _Csoport_Id_Tulaj.Name = "KRT_Mappak.Csoport_Id_Tulaj";
               _Csoport_Id_Tulaj.Type = "Guid";            
               _ErvKezd.Name = "KRT_Mappak.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Mappak.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SzuloMappa_Id = new Field();

        public Field SzuloMappa_Id
        {
            get { return _SzuloMappa_Id; }
            set { _SzuloMappa_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _BarCode = new Field();

        public Field BarCode
        {
            get { return _BarCode; }
            set { _BarCode = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Tulaj = new Field();

        public Field Csoport_Id_Tulaj
        {
            get { return _Csoport_Id_Tulaj; }
            set { _Csoport_Id_Tulaj = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}