
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Mentett_Talalati_Listak eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_Mentett_Talalati_ListakSearch: BaseSearchObject    {
      public KRT_Mentett_Talalati_ListakSearch()
      {            
                     _Id.Name = "KRT_Mentett_Talalati_Listak.Id";
               _Id.Type = "Guid";            
               _ListName.Name = "KRT_Mentett_Talalati_Listak.ListName";
               _ListName.Type = "String";            
               _SaveDate.Name = "KRT_Mentett_Talalati_Listak.SaveDate";
               _SaveDate.Type = "DateTime";            
               _Searched_Table.Name = "KRT_Mentett_Talalati_Listak.Searched_Table";
               _Searched_Table.Type = "String";            
               _Requestor.Name = "KRT_Mentett_Talalati_Listak.Requestor";
               _Requestor.Type = "String";            
               _HeaderXML.Name = "KRT_Mentett_Talalati_Listak.HeaderXML";
               _HeaderXML.Type = "String";            
               _BodyXML.Name = "KRT_Mentett_Talalati_Listak.BodyXML";
               _BodyXML.Type = "String";            
               _WorkStation.Name = "KRT_Mentett_Talalati_Listak.WorkStation";
               _WorkStation.Type = "String";            
               _ErvKezd.Name = "KRT_Mentett_Talalati_Listak.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Mentett_Talalati_Listak.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// ListName property
        /// 
        /// </summary>
        private Field _ListName = new Field();

        public Field ListName
        {
            get { return _ListName; }
            set { _ListName = value; }
        }
                          
           
        /// <summary>
        /// SaveDate property
        /// 
        /// </summary>
        private Field _SaveDate = new Field();

        public Field SaveDate
        {
            get { return _SaveDate; }
            set { _SaveDate = value; }
        }
                          
           
        /// <summary>
        /// Searched_Table property
        /// 
        /// </summary>
        private Field _Searched_Table = new Field();

        public Field Searched_Table
        {
            get { return _Searched_Table; }
            set { _Searched_Table = value; }
        }
                          
           
        /// <summary>
        /// Requestor property
        /// 
        /// </summary>
        private Field _Requestor = new Field();

        public Field Requestor
        {
            get { return _Requestor; }
            set { _Requestor = value; }
        }
                          
           
        /// <summary>
        /// HeaderXML property
        /// 
        /// </summary>
        private Field _HeaderXML = new Field();

        public Field HeaderXML
        {
            get { return _HeaderXML; }
            set { _HeaderXML = value; }
        }
                          
           
        /// <summary>
        /// BodyXML property
        /// 
        /// </summary>
        private Field _BodyXML = new Field();

        public Field BodyXML
        {
            get { return _BodyXML; }
            set { _BodyXML = value; }
        }
                          
           
        /// <summary>
        /// WorkStation property
        /// 
        /// </summary>
        private Field _WorkStation = new Field();

        public Field WorkStation
        {
            get { return _WorkStation; }
            set { _WorkStation = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}