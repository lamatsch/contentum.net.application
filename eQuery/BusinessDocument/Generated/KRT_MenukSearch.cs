
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Menuk eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_MenukSearch: BaseSearchObject    {
      public KRT_MenukSearch()
      {            
                     _Id.Name = "KRT_Menuk.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_Menuk.Org";
               _Org.Type = "Guid";            
               _Menu_Id_Szulo.Name = "KRT_Menuk.Menu_Id_Szulo";
               _Menu_Id_Szulo.Type = "Guid";            
               _Kod.Name = "KRT_Menuk.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_Menuk.Nev";
               _Nev.Type = "String";            
               _Funkcio_Id.Name = "KRT_Menuk.Funkcio_Id";
               _Funkcio_Id.Type = "Guid";            
               _Modul_Id.Name = "KRT_Menuk.Modul_Id";
               _Modul_Id.Type = "Guid";            
               _Parameter.Name = "KRT_Menuk.Parameter";
               _Parameter.Type = "String";            
               _ImageURL.Name = "KRT_Menuk.ImageURL";
               _ImageURL.Type = "String";            
               _Sorrend.Name = "KRT_Menuk.Sorrend";
               _Sorrend.Type = "String";            
               _ErvKezd.Name = "KRT_Menuk.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Menuk.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Menu_Id_Szulo = new Field();

        public Field Menu_Id_Szulo
        {
            get { return _Menu_Id_Szulo; }
            set { _Menu_Id_Szulo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Funkcio_Id = new Field();

        public Field Funkcio_Id
        {
            get { return _Funkcio_Id; }
            set { _Funkcio_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Modul_Id = new Field();

        public Field Modul_Id
        {
            get { return _Modul_Id; }
            set { _Modul_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Parameter = new Field();

        public Field Parameter
        {
            get { return _Parameter; }
            set { _Parameter = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ImageURL = new Field();

        public Field ImageURL
        {
            get { return _ImageURL; }
            set { _ImageURL = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Sorrend = new Field();

        public Field Sorrend
        {
            get { return _Sorrend; }
            set { _Sorrend = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}