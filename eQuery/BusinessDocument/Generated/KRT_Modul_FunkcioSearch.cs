
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Modul_Funkcio eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_Modul_FunkcioSearch: BaseSearchObject    {
      public KRT_Modul_FunkcioSearch()
      {            
                     _Id.Name = "KRT_Modul_Funkcio.Id";
               _Id.Type = "Guid";            
               _Funkcio_Id.Name = "KRT_Modul_Funkcio.Funkcio_Id";
               _Funkcio_Id.Type = "Guid";            
               _Modul_Id.Name = "KRT_Modul_Funkcio.Modul_Id";
               _Modul_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_Modul_Funkcio.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Modul_Funkcio.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Funkcio_Id = new Field();

        public Field Funkcio_Id
        {
            get { return _Funkcio_Id; }
            set { _Funkcio_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Modul_Id = new Field();

        public Field Modul_Id
        {
            get { return _Modul_Id; }
            set { _Modul_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}