
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Modulok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_ModulokSearch: BaseSearchObject    {
      public KRT_ModulokSearch()
      {            
                     _Id.Name = "KRT_Modulok.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_Modulok.Org";
               _Org.Type = "Guid";            
               _Tipus.Name = "KRT_Modulok.Tipus";
               _Tipus.Type = "String";            
               _Kod.Name = "KRT_Modulok.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_Modulok.Nev";
               _Nev.Type = "String";            
               _Leiras.Name = "KRT_Modulok.Leiras";
               _Leiras.Type = "String";            
               _SelectString.Name = "KRT_Modulok.SelectString";
               _SelectString.Type = "String";            
               _Csoport_Id_Tulaj.Name = "KRT_Modulok.Csoport_Id_Tulaj";
               _Csoport_Id_Tulaj.Type = "Guid";            
               _Parameterek.Name = "KRT_Modulok.Parameterek";
               _Parameterek.Type = "String";            
               _ObjTip_Id_Adatelem.Name = "KRT_Modulok.ObjTip_Id_Adatelem";
               _ObjTip_Id_Adatelem.Type = "Guid";            
               _ObjTip_Adatelem.Name = "KRT_Modulok.ObjTip_Adatelem";
               _ObjTip_Adatelem.Type = "String";            
               _ErvKezd.Name = "KRT_Modulok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Modulok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SelectString = new Field();

        public Field SelectString
        {
            get { return _SelectString; }
            set { _SelectString = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id_Tulaj = new Field();

        public Field Csoport_Id_Tulaj
        {
            get { return _Csoport_Id_Tulaj; }
            set { _Csoport_Id_Tulaj = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Parameterek = new Field();

        public Field Parameterek
        {
            get { return _Parameterek; }
            set { _Parameterek = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id_Adatelem = new Field();

        public Field ObjTip_Id_Adatelem
        {
            get { return _ObjTip_Id_Adatelem; }
            set { _ObjTip_Id_Adatelem = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Adatelem = new Field();

        public Field ObjTip_Adatelem
        {
            get { return _ObjTip_Adatelem; }
            set { _ObjTip_Adatelem = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}