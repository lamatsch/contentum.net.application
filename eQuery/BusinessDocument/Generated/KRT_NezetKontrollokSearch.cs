
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_NezetKontrollok eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_NezetKontrollokSearch
    {
      public KRT_NezetKontrollokSearch()
      {               _Id.Name = "KRT_NezetKontrollok.Id";
               _Id.Type = "Guid";            
               _Nezet_Id.Name = "KRT_NezetKontrollok.Nezet_Id";
               _Nezet_Id.Type = "Guid";            
               _Muvelet_Id.Name = "KRT_NezetKontrollok.Muvelet_Id";
               _Muvelet_Id.Type = "Guid";            
               _Csoport_Id.Name = "KRT_NezetKontrollok.Csoport_Id";
               _Csoport_Id.Type = "Guid";            
               _Feltetel.Name = "KRT_NezetKontrollok.Feltetel";
               _Feltetel.Type = "String";            
               _ObjTipus_id_UIElem.Name = "KRT_NezetKontrollok.ObjTipus_id_UIElem";
               _ObjTipus_id_UIElem.Type = "Guid";            
               _Tiltas.Name = "KRT_NezetKontrollok.Tiltas";
               _Tiltas.Type = "String";            
               _ObjTipus_id_Adatelem.Name = "KRT_NezetKontrollok.ObjTipus_id_Adatelem";
               _ObjTipus_id_Adatelem.Type = "Guid";            
               _ObjStat_id_Adatelem.Name = "KRT_NezetKontrollok.ObjStat_id_Adatelem";
               _ObjStat_id_Adatelem.Type = "Guid";            
               _ErvKezd.Name = "KRT_NezetKontrollok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_NezetKontrollok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nezet_Id = new Field();

        public Field Nezet_Id
        {
            get { return _Nezet_Id; }
            set { _Nezet_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Muvelet_Id = new Field();

        public Field Muvelet_Id
        {
            get { return _Muvelet_Id; }
            set { _Muvelet_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id = new Field();

        public Field Csoport_Id
        {
            get { return _Csoport_Id; }
            set { _Csoport_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Feltetel = new Field();

        public Field Feltetel
        {
            get { return _Feltetel; }
            set { _Feltetel = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTipus_id_UIElem = new Field();

        public Field ObjTipus_id_UIElem
        {
            get { return _ObjTipus_id_UIElem; }
            set { _ObjTipus_id_UIElem = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tiltas = new Field();

        public Field Tiltas
        {
            get { return _Tiltas; }
            set { _Tiltas = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTipus_id_Adatelem = new Field();

        public Field ObjTipus_id_Adatelem
        {
            get { return _ObjTipus_id_Adatelem; }
            set { _ObjTipus_id_Adatelem = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjStat_id_Adatelem = new Field();

        public Field ObjStat_id_Adatelem
        {
            get { return _ObjStat_id_Adatelem; }
            set { _ObjStat_id_Adatelem = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}