
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Nezetek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public partial class KRT_NezetekSearch: BaseSearchObject    {
      public KRT_NezetekSearch()
      {            
         Init_ManualFields();
               
                     _Id.Name = "KRT_Nezetek.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_Nezetek.Org";
               _Org.Type = "Guid";            
               _Form_Id.Name = "KRT_Nezetek.Form_Id";
               _Form_Id.Type = "Guid";            
               _Nev.Name = "KRT_Nezetek.Nev";
               _Nev.Type = "String";            
               _Leiras.Name = "KRT_Nezetek.Leiras";
               _Leiras.Type = "String";            
               _Csoport_Id.Name = "KRT_Nezetek.Csoport_Id";
               _Csoport_Id.Type = "Guid";            
               _Muvelet_Id.Name = "KRT_Nezetek.Muvelet_Id";
               _Muvelet_Id.Type = "Guid";            
               _DisableControls.Name = "KRT_Nezetek.DisableControls";
               _DisableControls.Type = "String";            
               _InvisibleControls.Name = "KRT_Nezetek.InvisibleControls";
               _InvisibleControls.Type = "String";            
               _ReadOnlyControls.Name = "KRT_Nezetek.ReadOnlyControls";
               _ReadOnlyControls.Type = "String";            
               _TabIndexControls.Name = "KRT_Nezetek.TabIndexControls";
               _TabIndexControls.Type = "String";            
               _Prioritas.Name = "KRT_Nezetek.Prioritas";
               _Prioritas.Type = "Int32";            
               _ErvKezd.Name = "KRT_Nezetek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Nezetek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// A CSOP_ID_TULAJ mellett ez valojaban mar nem szukseges.
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Form_Id property
        /// Hivatkoz�s a Form
        /// </summary>
        private Field _Form_Id = new Field();

        public Field Form_Id
        {
            get { return _Form_Id; }
            set { _Form_Id = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// Nezet neve. Celszeru, hogy egyedi legyen, mert ez beszedes, olvashato, az ID nem.
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Leiras property
        /// Tudnivalok a nezetrol
        /// </summary>
        private Field _Leiras = new Field();

        public Field Leiras
        {
            get { return _Leiras; }
            set { _Leiras = value; }
        }
                          
           
        /// <summary>
        /// Csoport_Id property
        /// Minden objektumnak KELL legyen egy ERVENYES tulajdonosa (hogy garantaltan mindig legyen nem elveheto kezeloi jog). Default az indulo Admin, aki nem torolheto.
        /// </summary>
        private Field _Csoport_Id = new Field();

        public Field Csoport_Id
        {
            get { return _Csoport_Id; }
            set { _Csoport_Id = value; }
        }
                          
           
        /// <summary>
        /// Muvelet_Id property
        /// Muvelet, amit a n�zet v�grehajt
        /// </summary>
        private Field _Muvelet_Id = new Field();

        public Field Muvelet_Id
        {
            get { return _Muvelet_Id; }
            set { _Muvelet_Id = value; }
        }
                          
           
        /// <summary>
        /// DisableControls property
        /// ??
        /// </summary>
        private Field _DisableControls = new Field();

        public Field DisableControls
        {
            get { return _DisableControls; }
            set { _DisableControls = value; }
        }
                          
           
        /// <summary>
        /// InvisibleControls property
        /// Nem l�that� konrolok list�ja (UniqueId-k) vesszovel elv�lasztva
        /// </summary>
        private Field _InvisibleControls = new Field();

        public Field InvisibleControls
        {
            get { return _InvisibleControls; }
            set { _InvisibleControls = value; }
        }
                          
           
        /// <summary>
        /// ReadOnlyControls property
        /// ??
        /// </summary>
        private Field _ReadOnlyControls = new Field();

        public Field ReadOnlyControls
        {
            get { return _ReadOnlyControls; }
            set { _ReadOnlyControls = value; }
        }
                          
           
        /// <summary>
        /// TabIndexControls property
        /// ??
        /// </summary>
        private Field _TabIndexControls = new Field();

        public Field TabIndexControls
        {
            get { return _TabIndexControls; }
            set { _TabIndexControls = value; }
        }
                          
           
        /// <summary>
        /// Prioritas property
        /// Sorrendben kell felaj�nlani a legnagyobb priorit�s� n�zetet (azaz azt, mellyel a lehetos�gek k�z�l a legt�bbet lehet v�grehajtani...)
        /// 3., 2007.04.04. Konzult�ci�: A n�zet kiv�laszt�s a csoport jogok alapj�n t�rt�nik, ha t�bb n�zet lesz egy adott ascx laphoz, akkor ezekbol a legnagyobb lehetos�geket biztos�t�t
        ///      v�lasztja a program. (azt, hogy melyik n�zettel lehet a legt�bb funkci�t el�rni, a n�zetben egy "s�ly" attrib�tummal jel�lj�k, e szerint van cs�kkeno sorrendbe rendezve.
        /// </summary>
        private Field _Prioritas = new Field();

        public Field Prioritas
        {
            get { return _Prioritas; }
            set { _Prioritas = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}