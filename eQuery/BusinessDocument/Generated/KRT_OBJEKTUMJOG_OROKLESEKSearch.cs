
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_OBJEKTUMJOG_OROKLESEK eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_OBJEKTUMJOG_OROKLESEKSearch: BaseSearchObject    {
      public KRT_OBJEKTUMJOG_OROKLESEKSearch()
      {            
                     _Id.Name = "KRT_OBJEKTUMJOG_OROKLESEK.Id";
               _Id.Type = "Guid";            
               _Obj_Id.Name = "KRT_OBJEKTUMJOG_OROKLESEK.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _Obj_Id_Orokos.Name = "KRT_OBJEKTUMJOG_OROKLESEK.Obj_Id_Orokos";
               _Obj_Id_Orokos.Type = "Guid";            
               _JogszintOrokolheto.Name = "KRT_OBJEKTUMJOG_OROKLESEK.JogszintOrokolheto";
               _JogszintOrokolheto.Type = "Char";            
               _ErvKezd.Name = "KRT_OBJEKTUMJOG_OROKLESEK.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_OBJEKTUMJOG_OROKLESEK.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id_Orokos = new Field();

        public Field Obj_Id_Orokos
        {
            get { return _Obj_Id_Orokos; }
            set { _Obj_Id_Orokos = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _JogszintOrokolheto = new Field();

        public Field JogszintOrokolheto
        {
            get { return _JogszintOrokolheto; }
            set { _JogszintOrokolheto = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}