
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_OBJEKTUM_ACLEK eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_OBJEKTUM_ACLEKSearch: BaseSearchObject    {
      public KRT_OBJEKTUM_ACLEKSearch()
      {            
                     _Obj_Id.Name = "KRT_OBJEKTUM_ACLEK.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _Obj_Id_Orokos.Name = "KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos";
               _Obj_Id_Orokos.Type = "Guid";            
               _OrokolhetoJogszint.Name = "KRT_OBJEKTUM_ACLEK.OrokolhetoJogszint";
               _OrokolhetoJogszint.Type = "Char";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id_Orokos = new Field();

        public Field Obj_Id_Orokos
        {
            get { return _Obj_Id_Orokos; }
            set { _Obj_Id_Orokos = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _OrokolhetoJogszint = new Field();

        public Field OrokolhetoJogszint
        {
            get { return _OrokolhetoJogszint; }
            set { _OrokolhetoJogszint = value; }
        }
                              }

}