
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_ObjTip_TargySzo eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_ObjTip_TargySzoSearch    {
      public KRT_ObjTip_TargySzoSearch()
      {            
                     _Id.Name = "KRT_ObjTip_TargySzo.Id";
               _Id.Type = "Guid";            
               _TargySzo_Id.Name = "KRT_ObjTip_TargySzo.TargySzo_Id";
               _TargySzo_Id.Type = "Guid";            
               _ObjTip_Id_AdatElem.Name = "KRT_ObjTip_TargySzo.ObjTip_Id_AdatElem";
               _ObjTip_Id_AdatElem.Type = "Guid";            
               _Tipus.Name = "KRT_ObjTip_TargySzo.Tipus";
               _Tipus.Type = "Char";            
               _Sorszam.Name = "KRT_ObjTip_TargySzo.Sorszam";
               _Sorszam.Type = "Int32";            
               _Opcionalis.Name = "KRT_ObjTip_TargySzo.Opcionalis";
               _Opcionalis.Type = "Char";            
               _Nev.Name = "KRT_ObjTip_TargySzo.Nev";
               _Nev.Type = "String";            
               _ErvKezd.Name = "KRT_ObjTip_TargySzo.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_ObjTip_TargySzo.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _TargySzo_Id = new Field();

        public Field TargySzo_Id
        {
            get { return _TargySzo_Id; }
            set { _TargySzo_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id_AdatElem = new Field();

        public Field ObjTip_Id_AdatElem
        {
            get { return _ObjTip_Id_AdatElem; }
            set { _ObjTip_Id_AdatElem = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Opcionalis = new Field();

        public Field Opcionalis
        {
            get { return _Opcionalis; }
            set { _Opcionalis = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}