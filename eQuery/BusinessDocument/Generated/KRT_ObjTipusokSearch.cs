
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_ObjTipusok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_ObjTipusokSearch: BaseSearchObject    {
      public KRT_ObjTipusokSearch()
      {            
                     _Id.Name = "KRT_ObjTipusok.Id";
               _Id.Type = "Guid";            
               _ObjTipus_Id_Tipus.Name = "KRT_ObjTipusok.ObjTipus_Id_Tipus";
               _ObjTipus_Id_Tipus.Type = "Guid";            
               _Kod.Name = "KRT_ObjTipusok.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_ObjTipusok.Nev";
               _Nev.Type = "String";            
               _Obj_Id_Szulo.Name = "KRT_ObjTipusok.Obj_Id_Szulo";
               _Obj_Id_Szulo.Type = "Guid";            
               _KodCsoport_Id.Name = "KRT_ObjTipusok.KodCsoport_Id";
               _KodCsoport_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_ObjTipusok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_ObjTipusok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTipus_Id_Tipus = new Field();

        public Field ObjTipus_Id_Tipus
        {
            get { return _ObjTipus_Id_Tipus; }
            set { _ObjTipus_Id_Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id_Szulo = new Field();

        public Field Obj_Id_Szulo
        {
            get { return _Obj_Id_Szulo; }
            set { _Obj_Id_Szulo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KodCsoport_Id = new Field();

        public Field KodCsoport_Id
        {
            get { return _KodCsoport_Id; }
            set { _KodCsoport_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}