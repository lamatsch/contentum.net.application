
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Orgok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_OrgokSearch: BaseSearchObject    {
      public KRT_OrgokSearch()
      {            
                     _Id.Name = "KRT_Orgok.Id";
               _Id.Type = "Guid";            
               _Kod.Name = "KRT_Orgok.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_Orgok.Nev";
               _Nev.Type = "String";            
               _Sorszam.Name = "KRT_Orgok.Sorszam";
               _Sorszam.Type = "Int32";            
               _Partner_id_szulo.Name = "KRT_Orgok.Partner_id_szulo";
               _Partner_id_szulo.Type = "Guid";            
               _ErvKezd.Name = "KRT_Orgok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Orgok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Sorszam = new Field();

        public Field Sorszam
        {
            get { return _Sorszam; }
            set { _Sorszam = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Partner_id_szulo = new Field();

        public Field Partner_id_szulo
        {
            get { return _Partner_id_szulo; }
            set { _Partner_id_szulo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}