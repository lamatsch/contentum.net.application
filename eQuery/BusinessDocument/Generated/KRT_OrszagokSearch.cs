
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Orszagok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_OrszagokSearch: BaseSearchObject    {
      public KRT_OrszagokSearch()
      {            
                     _Id.Name = "KRT_Orszagok.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_Orszagok.Org";
               _Org.Type = "Guid";            
               _Kod.Name = "KRT_Orszagok.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_Orszagok.Nev";
               _Nev.Type = "String";            
               _Viszonylatkod.Name = "KRT_Orszagok.Viszonylatkod";
               _Viszonylatkod.Type = "String";            
               _ErvKezd.Name = "KRT_Orszagok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Orszagok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Kod property
        /// Az orsz�g szabv�nyos k�dja
        /// </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// Orsz�g neve
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Viszonylatkod property
        /// Az orsz�g postai viszonylatk�dja:
        /// Eur�pai orsz�g: K1
        /// Egy�b k�lf�ld: K2
        /// az aktu�lis, �rv�nyben l�v� �zletszab�lyzat szerint (Magyar Posta)
        /// </summary>
        private Field _Viszonylatkod = new Field();

        public Field Viszonylatkod
        {
            get { return _Viszonylatkod; }
            set { _Viszonylatkod = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}