
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_PartnerKapcsolatok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_PartnerKapcsolatokSearch: BaseSearchObject    {
      public KRT_PartnerKapcsolatokSearch()
      {            
                     _Id.Name = "KRT_PartnerKapcsolatok.Id";
               _Id.Type = "Guid";            
               _Tipus.Name = "KRT_PartnerKapcsolatok.Tipus";
               _Tipus.Type = "String";            
               _Partner_id.Name = "KRT_PartnerKapcsolatok.Partner_id";
               _Partner_id.Type = "Guid";            
               _Partner_id_kapcsolt.Name = "KRT_PartnerKapcsolatok.Partner_id_kapcsolt";
               _Partner_id_kapcsolt.Type = "Guid";            
               _ErvKezd.Name = "KRT_PartnerKapcsolatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_PartnerKapcsolatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Partner_id = new Field();

        public Field Partner_id
        {
            get { return _Partner_id; }
            set { _Partner_id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Partner_id_kapcsolt = new Field();

        public Field Partner_id_kapcsolt
        {
            get { return _Partner_id_kapcsolt; }
            set { _Partner_id_kapcsolt = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}