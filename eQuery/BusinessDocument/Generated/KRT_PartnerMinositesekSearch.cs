
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_PartnerMinositesek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_PartnerMinositesekSearch: BaseSearchObject    {
      public KRT_PartnerMinositesekSearch()
      {            
                     _Id.Name = "KRT_PartnerMinositesek.Id";
               _Id.Type = "Guid";            
               _ALLAPOT.Name = "KRT_PartnerMinositesek.ALLAPOT";
               _ALLAPOT.Type = "Char";            
               _Partner_id.Name = "KRT_PartnerMinositesek.Partner_id";
               _Partner_id.Type = "Guid";            
               _Felhasznalo_id_kero.Name = "KRT_PartnerMinositesek.Felhasznalo_id_kero";
               _Felhasznalo_id_kero.Type = "Guid";            
               _KertMinosites.Name = "KRT_PartnerMinositesek.KertMinosites";
               _KertMinosites.Type = "String";            
               _KertKezdDat.Name = "KRT_PartnerMinositesek.KertKezdDat";
               _KertKezdDat.Type = "DateTime";            
               _KertVegeDat.Name = "KRT_PartnerMinositesek.KertVegeDat";
               _KertVegeDat.Type = "DateTime";            
               _KerelemAzonosito.Name = "KRT_PartnerMinositesek.KerelemAzonosito";
               _KerelemAzonosito.Type = "String";            
               _KerelemBeadIdo.Name = "KRT_PartnerMinositesek.KerelemBeadIdo";
               _KerelemBeadIdo.Type = "DateTime";            
               _KerelemDontesIdo.Name = "KRT_PartnerMinositesek.KerelemDontesIdo";
               _KerelemDontesIdo.Type = "DateTime";            
               _Felhasznalo_id_donto.Name = "KRT_PartnerMinositesek.Felhasznalo_id_donto";
               _Felhasznalo_id_donto.Type = "Guid";            
               _DontesAzonosito.Name = "KRT_PartnerMinositesek.DontesAzonosito";
               _DontesAzonosito.Type = "String";            
               _Minosites.Name = "KRT_PartnerMinositesek.Minosites";
               _Minosites.Type = "String";            
               _MinositesKezdDat.Name = "KRT_PartnerMinositesek.MinositesKezdDat";
               _MinositesKezdDat.Type = "DateTime";            
               _MinositesVegDat.Name = "KRT_PartnerMinositesek.MinositesVegDat";
               _MinositesVegDat.Type = "DateTime";            
               _SztornirozasDat.Name = "KRT_PartnerMinositesek.SztornirozasDat";
               _SztornirozasDat.Type = "DateTime";            
               _ErvKezd.Name = "KRT_PartnerMinositesek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_PartnerMinositesek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ALLAPOT = new Field();

        public Field ALLAPOT
        {
            get { return _ALLAPOT; }
            set { _ALLAPOT = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Partner_id = new Field();

        public Field Partner_id
        {
            get { return _Partner_id; }
            set { _Partner_id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_id_kero = new Field();

        public Field Felhasznalo_id_kero
        {
            get { return _Felhasznalo_id_kero; }
            set { _Felhasznalo_id_kero = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KertMinosites = new Field();

        public Field KertMinosites
        {
            get { return _KertMinosites; }
            set { _KertMinosites = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KertKezdDat = new Field();

        public Field KertKezdDat
        {
            get { return _KertKezdDat; }
            set { _KertKezdDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KertVegeDat = new Field();

        public Field KertVegeDat
        {
            get { return _KertVegeDat; }
            set { _KertVegeDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KerelemAzonosito = new Field();

        public Field KerelemAzonosito
        {
            get { return _KerelemAzonosito; }
            set { _KerelemAzonosito = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KerelemBeadIdo = new Field();

        public Field KerelemBeadIdo
        {
            get { return _KerelemBeadIdo; }
            set { _KerelemBeadIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KerelemDontesIdo = new Field();

        public Field KerelemDontesIdo
        {
            get { return _KerelemDontesIdo; }
            set { _KerelemDontesIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_id_donto = new Field();

        public Field Felhasznalo_id_donto
        {
            get { return _Felhasznalo_id_donto; }
            set { _Felhasznalo_id_donto = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _DontesAzonosito = new Field();

        public Field DontesAzonosito
        {
            get { return _DontesAzonosito; }
            set { _DontesAzonosito = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Minosites = new Field();

        public Field Minosites
        {
            get { return _Minosites; }
            set { _Minosites = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MinositesKezdDat = new Field();

        public Field MinositesKezdDat
        {
            get { return _MinositesKezdDat; }
            set { _MinositesKezdDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MinositesVegDat = new Field();

        public Field MinositesVegDat
        {
            get { return _MinositesVegDat; }
            set { _MinositesVegDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _SztornirozasDat = new Field();

        public Field SztornirozasDat
        {
            get { return _SztornirozasDat; }
            set { _SztornirozasDat = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}