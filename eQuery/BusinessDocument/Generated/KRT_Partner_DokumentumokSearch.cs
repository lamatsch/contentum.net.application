
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Partner_Dokumentumok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_Partner_DokumentumokSearch: BaseSearchObject    {
      public KRT_Partner_DokumentumokSearch()
      {            
                     _Id.Name = "KRT_Partner_Dokumentumok.Id";
               _Id.Type = "Guid";            
               _Partner_id.Name = "KRT_Partner_Dokumentumok.Partner_id";
               _Partner_id.Type = "Guid";            
               _Dokumentum_Id.Name = "KRT_Partner_Dokumentumok.Dokumentum_Id";
               _Dokumentum_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_Partner_Dokumentumok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Partner_Dokumentumok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// 
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Partner_id property
        /// Partner id
        /// </summary>
        private Field _Partner_id = new Field();

        public Field Partner_id
        {
            get { return _Partner_id; }
            set { _Partner_id = value; }
        }
                          
           
        /// <summary>
        /// Dokumentum_Id property
        /// Dokumentum Id
        /// </summary>
        private Field _Dokumentum_Id = new Field();

        public Field Dokumentum_Id
        {
            get { return _Dokumentum_Id; }
            set { _Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}