
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_PartnerCimek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_PartnerCimekSearch: BaseSearchObject    {
      public KRT_PartnerCimekSearch()
      {            
                     _Id.Name = "KRT_PartnerCimek.Id";
               _Id.Type = "Guid";            
               _Partner_id.Name = "KRT_PartnerCimek.Partner_id";
               _Partner_id.Type = "Guid";            
               _Cim_Id.Name = "KRT_PartnerCimek.Cim_Id";
               _Cim_Id.Type = "Guid";            
               _UtolsoHasznalatSiker.Name = "KRT_PartnerCimek.UtolsoHasznalatSiker";
               _UtolsoHasznalatSiker.Type = "Char";            
               _UtolsoHasznalatiIdo.Name = "KRT_PartnerCimek.UtolsoHasznalatiIdo";
               _UtolsoHasznalatiIdo.Type = "DateTime";            
               _eMailKuldes.Name = "KRT_PartnerCimek.eMailKuldes";
               _eMailKuldes.Type = "Char";            
               _Fajta.Name = "KRT_PartnerCimek.Fajta";
               _Fajta.Type = "String";            
               _ErvKezd.Name = "KRT_PartnerCimek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_PartnerCimek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Partner_id = new Field();

        public Field Partner_id
        {
            get { return _Partner_id; }
            set { _Partner_id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Cim_Id = new Field();

        public Field Cim_Id
        {
            get { return _Cim_Id; }
            set { _Cim_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UtolsoHasznalatSiker = new Field();

        public Field UtolsoHasznalatSiker
        {
            get { return _UtolsoHasznalatSiker; }
            set { _UtolsoHasznalatSiker = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UtolsoHasznalatiIdo = new Field();

        public Field UtolsoHasznalatiIdo
        {
            get { return _UtolsoHasznalatiIdo; }
            set { _UtolsoHasznalatiIdo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _eMailKuldes = new Field();

        public Field eMailKuldes
        {
            get { return _eMailKuldes; }
            set { _eMailKuldes = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Fajta = new Field();

        public Field Fajta
        {
            get { return _Fajta; }
            set { _Fajta = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}