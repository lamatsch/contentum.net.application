
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Partnerek eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public partial class KRT_PartnerekSearch : BaseSearchObject
    {
        public KRT_PartnerekSearch()
        {
            Init_ManualFields();

            _Id.Name = "KRT_Partnerek.Id";
            _Id.Type = "Guid";
            _Org.Name = "KRT_Partnerek.Org";
            _Org.Type = "Guid";
            _Orszag_Id.Name = "KRT_Partnerek.Orszag_Id";
            _Orszag_Id.Type = "Guid";
            _Tipus.Name = "KRT_Partnerek.Tipus";
            _Tipus.Type = "String";
            _Nev.Name = "KRT_Partnerek.Nev";
            _Nev.Type = "String";
            _KulsoAzonositok.Name = "KRT_Partnerek.KulsoAzonositok";
            _KulsoAzonositok.Type = "String";
            _PublikusKulcs.Name = "KRT_Partnerek.PublikusKulcs";
            _PublikusKulcs.Type = "String";
            _Kiszolgalo.Name = "KRT_Partnerek.Kiszolgalo";
            _Kiszolgalo.Type = "Guid";
            _LetrehozoSzervezet.Name = "KRT_Partnerek.LetrehozoSzervezet";
            _LetrehozoSzervezet.Type = "Guid";
            _MaxMinosites.Name = "KRT_Partnerek.MaxMinosites";
            _MaxMinosites.Type = "String";
            _MinositesKezdDat.Name = "KRT_Partnerek.MinositesKezdDat";
            _MinositesKezdDat.Type = "DateTime";
            _MinositesVegDat.Name = "KRT_Partnerek.MinositesVegDat";
            _MinositesVegDat.Type = "DateTime";
            _MaxMinositesSzervezet.Name = "KRT_Partnerek.MaxMinositesSzervezet";
            _MaxMinositesSzervezet.Type = "String";
            _Belso.Name = "KRT_Partnerek.Belso";
            _Belso.Type = "Char";
            _Forras.Name = "KRT_Partnerek.Forras";
            _Forras.Type = "Char";
            _Allapot.Name = "KRT_Partnerek.Allapot";
            _Allapot.Type = "String";
            _AspAdoTorolve.Name = "KRT_Partnerek.AspAdoTorolve";
            _AspAdoTorolve.Type = "BIT";
            _ErvKezd.Name = "KRT_Partnerek.ErvKezd";
            _ErvKezd.Type = "DateTime"; _ErvKezd.Operator = Query.Operators.lessorequal;
            _ErvKezd.Value = "getdate()";
            _ErvVege.Name = "KRT_Partnerek.ErvVege";
            _ErvVege.Type = "DateTime"; _ErvVege.Operator = Query.Operators.greaterorequal;
            _ErvVege.Value = "getdate()";
            _UtolsoHasznalat.Name = "KRT_Partnerek.UtolsoHasznalat";
            _UtolsoHasznalat.Type = "DateTime";
        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// Org property
        /// ORG Id
        /// </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }


        /// <summary>
        /// Orszag_Id property
        /// Orsz�g Id
        /// </summary>
        private Field _Orszag_Id = new Field();

        public Field Orszag_Id
        {
            get { return _Orszag_Id; }
            set { _Orszag_Id = value; }
        }


        /// <summary>
        /// Tipus property
        /// KCS: Partner_Tipus_JDV -b�l, DNvarchar(2).
        /// 10: Szervezet
        /// 20: Szemely...
        /// 30: Alkalmaz�s (kiz�r�lag technikai userek r�sz�re)
        /// 
        /// 
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }


        /// <summary>
        /// Nev property
        /// Partner megnevezese
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }


        /// <summary>
        /// KulsoAzonositok property
        /// Tetsz�leges k�ls� azonos�t�k
        /// </summary>
        private Field _KulsoAzonositok = new Field();

        public Field KulsoAzonositok
        {
            get { return _KulsoAzonositok; }
            set { _KulsoAzonositok = value; }
        }


        /// <summary>
        /// PublikusKulcs property
        /// Partner digit�lis al��r�s�hoz
        /// </summary>
        private Field _PublikusKulcs = new Field();

        public Field PublikusKulcs
        {
            get { return _PublikusKulcs; }
            set { _PublikusKulcs = value; }
        }


        /// <summary>
        /// Kiszolgalo property
        /// ??
        /// </summary>
        private Field _Kiszolgalo = new Field();

        public Field Kiszolgalo
        {
            get { return _Kiszolgalo; }
            set { _Kiszolgalo = value; }
        }


        /// <summary>
        /// LetrehozoSzervezet property
        /// L�trehoz� szervezet Id-je
        /// </summary>
        private Field _LetrehozoSzervezet = new Field();

        public Field LetrehozoSzervezet
        {
            get { return _LetrehozoSzervezet; }
            set { _LetrehozoSzervezet = value; }
        }


        /// <summary>
        /// MaxMinosites property
        /// nem haszn�lt
        /// </summary>
        private Field _MaxMinosites = new Field();

        public Field MaxMinosites
        {
            get { return _MaxMinosites; }
            set { _MaxMinosites = value; }
        }


        /// <summary>
        /// MinositesKezdDat property
        /// nem haszn�lt
        /// </summary>
        private Field _MinositesKezdDat = new Field();

        public Field MinositesKezdDat
        {
            get { return _MinositesKezdDat; }
            set { _MinositesKezdDat = value; }
        }


        /// <summary>
        /// MinositesVegDat property
        /// nem haszn�lt
        /// </summary>
        private Field _MinositesVegDat = new Field();

        public Field MinositesVegDat
        {
            get { return _MinositesVegDat; }
            set { _MinositesVegDat = value; }
        }


        /// <summary>
        /// MaxMinositesSzervezet property
        /// nem haszn�lt
        /// </summary>
        private Field _MaxMinositesSzervezet = new Field();

        public Field MaxMinositesSzervezet
        {
            get { return _MaxMinositesSzervezet; }
            set { _MaxMinositesSzervezet = value; }
        }


        /// <summary>
        /// Belso property
        /// 1-bels�, 0-k�ls�
        /// </summary>
        private Field _Belso = new Field();

        public Field Belso
        {
            get { return _Belso; }
            set { _Belso = value; }
        }


        /// <summary>
        /// Forras property
        /// ??Partner adatbazisba kerulesenk forrasa. Leggyakoribb a felhasznalo kezi rogzites, de lehet cimtar, SAP, OApplication, stb.
        /// </summary>
        private Field _Forras = new Field();

        public Field Forras
        {
            get { return _Forras; }
            set { _Forras = value; }
        }

        /// <summary>
        /// Allapot property
        /// 
        /// </summary>
        private Field _AspAdoTorolve = new Field();

        public Field AspAdoTorolve
        {
            get { return _AspAdoTorolve; }
            set { _AspAdoTorolve = value; }
        }
        /// <summary>
        /// Allapot property
        /// 
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }


        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }


        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }


        /// <summary>
        /// UtolsoHasznalat property
        /// 
        /// </summary>
        private Field _UtolsoHasznalat = new Field();

        public Field UtolsoHasznalat
        {
            get { return _UtolsoHasznalat; }
            set { _UtolsoHasznalat = value; }
        }
    }

}