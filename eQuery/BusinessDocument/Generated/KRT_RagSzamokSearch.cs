
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_RagSzamok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_RagSzamokSearch: BaseSearchObject    {
      public KRT_RagSzamokSearch()
      {            
                     _Id.Name = "KRT_RagSzamok.Id";
               _Id.Type = "Guid";            
               _Kod.Name = "KRT_RagSzamok.Kod";
               _Kod.Type = "String";            
               _KodNum.Name = "KRT_RagSzamok.KodNum";
               _KodNum.Type = "Double";            
               _Postakonyv_Id.Name = "KRT_RagSzamok.Postakonyv_Id";
               _Postakonyv_Id.Type = "Guid";            
               _RagszamSav_Id.Name = "KRT_RagSzamok.RagszamSav_Id";
               _RagszamSav_Id.Type = "Guid";            
               _Obj_Id.Name = "KRT_RagSzamok.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ObjTip_Id.Name = "KRT_RagSzamok.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _Obj_type.Name = "KRT_RagSzamok.Obj_type";
               _Obj_type.Type = "String";            
               _KodType.Name = "KRT_RagSzamok.KodType";
               _KodType.Type = "Char";            
               _Allapot.Name = "KRT_RagSzamok.Allapot";
               _Allapot.Type = "String";            
               _ErvKezd.Name = "KRT_RagSzamok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_RagSzamok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _Tranz_id.Name = "KRT_RagSzamok.Tranz_id";
               _Tranz_id.Type = "Guid";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Kod property
        /// A b�rk�d azonos�t�ja.
        /// </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// KodNum property
        /// 
        /// </summary>
        private Field _KodNum = new Field();

        public Field KodNum
        {
            get { return _KodNum; }
            set { _KodNum = value; }
        }
                          
           
        /// <summary>
        /// Postakonyv_Id property
        /// 
        /// </summary>
        private Field _Postakonyv_Id = new Field();

        public Field Postakonyv_Id
        {
            get { return _Postakonyv_Id; }
            set { _Postakonyv_Id = value; }
        }
                          
           
        /// <summary>
        /// RagszamSav_Id property
        /// 
        /// </summary>
        private Field _RagszamSav_Id = new Field();

        public Field RagszamSav_Id
        {
            get { return _RagszamSav_Id; }
            set { _RagszamSav_Id = value; }
        }
                          
           
        /// <summary>
        /// Obj_Id property
        /// A b�rk�ddal ell�tott objektum ID -ja. 
        /// Bts: r�gt�n lehessen l�tni, hova ker�lt a b�rk�d felhaszn�l�sra.
        /// </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// ObjTip_Id property
        /// Ez annak a valaminek a t�pusa, amit csatoltunk
        /// </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Obj_type property
        /// Obj. t�pus, itt: a t�bla neve
        /// </summary>
        private Field _Obj_type = new Field();

        public Field Obj_type
        {
            get { return _Obj_type; }
            set { _Obj_type = value; }
        }
                          
           
        /// <summary>
        /// KodType property
        /// G,N,P
        /// </summary>
        private Field _KodType = new Field();

        public Field KodType
        {
            get { return _KodType; }
            set { _KodType = value; }
        }
                          
           
        /// <summary>
        /// Allapot property
        /// KCS: BARKOD_ALLAPOT
        /// S - Szabad
        /// F - Felhaszn�lt
        /// T - T�r�lt
        /// </summary>
        private Field _Allapot = new Field();

        public Field Allapot
        {
            get { return _Allapot; }
            set { _Allapot = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// Tranz_id property
        /// Tranzakci� azonos�t�: egy program �ltal egy logikai egys�gk�nt t�rolt inform�ci�kat 
        /// egyetlen Guid -al azonos�tunk. (pl. egy ASP.Net lap �ltal egy ment�ssel, ha t�bb t�bl�ba t�rol
        /// inform�ci�kat, ezen azonos�t� alapj�n lehet tudni, egy program volt az inform�ci� forr�s.Vagy: 
        /// Az XMLImport seg�dprogram fut�s�t c�lszer� az xml file-ban egyetlen tranz_id -val azonos�tani)
        /// </summary>
        private Field _Tranz_id = new Field();

        public Field Tranz_id
        {
            get { return _Tranz_id; }
            set { _Tranz_id = value; }
        }
                              }

}