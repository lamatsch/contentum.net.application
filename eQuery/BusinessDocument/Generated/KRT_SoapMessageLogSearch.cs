
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_SoapMessageLog eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_SoapMessageLogSearch: BaseSearchObject    {
      public KRT_SoapMessageLogSearch()
      {            
                     _Id.Name = "KRT_SoapMessageLog.Id";
               _Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_SoapMessageLog.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_SoapMessageLog.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _ForrasRendszer.Name = "KRT_SoapMessageLog.ForrasRendszer";
               _ForrasRendszer.Type = "String";            
               _Url.Name = "KRT_SoapMessageLog.Url";
               _Url.Type = "String";            
               _CelRendszer.Name = "KRT_SoapMessageLog.CelRendszer";
               _CelRendszer.Type = "String";            
               _MetodusNeve.Name = "KRT_SoapMessageLog.MetodusNeve";
               _MetodusNeve.Type = "String";            
               _KeresFogadas.Name = "KRT_SoapMessageLog.KeresFogadas";
               _KeresFogadas.Type = "DateTime";            
               _KeresKuldes.Name = "KRT_SoapMessageLog.KeresKuldes";
               _KeresKuldes.Type = "DateTime";            
               _RequestData.Name = "KRT_SoapMessageLog.RequestData";
               _RequestData.Type = "String";            
               _ResponseData.Name = "KRT_SoapMessageLog.ResponseData";
               _ResponseData.Type = "String";            
               _FuttatoFelhasznalo.Name = "KRT_SoapMessageLog.FuttatoFelhasznalo";
               _FuttatoFelhasznalo.Type = "String";            
               _Local.Name = "KRT_SoapMessageLog.Local";
               _Local.Type = "Char";            
               _Hiba.Name = "KRT_SoapMessageLog.Hiba";
               _Hiba.Type = "String";            
               _Sikeres.Name = "KRT_SoapMessageLog.Sikeres";
               _Sikeres.Type = "Char";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// ForrasRendszer property
        /// 
        /// </summary>
        private Field _ForrasRendszer = new Field();

        public Field ForrasRendszer
        {
            get { return _ForrasRendszer; }
            set { _ForrasRendszer = value; }
        }
                          
           
        /// <summary>
        /// Url property
        /// 
        /// </summary>
        private Field _Url = new Field();

        public Field Url
        {
            get { return _Url; }
            set { _Url = value; }
        }
                          
           
        /// <summary>
        /// CelRendszer property
        /// 
        /// </summary>
        private Field _CelRendszer = new Field();

        public Field CelRendszer
        {
            get { return _CelRendszer; }
            set { _CelRendszer = value; }
        }
                          
           
        /// <summary>
        /// MetodusNeve property
        /// 
        /// </summary>
        private Field _MetodusNeve = new Field();

        public Field MetodusNeve
        {
            get { return _MetodusNeve; }
            set { _MetodusNeve = value; }
        }
                          
           
        /// <summary>
        /// KeresFogadas property
        /// 
        /// </summary>
        private Field _KeresFogadas = new Field();

        public Field KeresFogadas
        {
            get { return _KeresFogadas; }
            set { _KeresFogadas = value; }
        }
                          
           
        /// <summary>
        /// KeresKuldes property
        /// 
        /// </summary>
        private Field _KeresKuldes = new Field();

        public Field KeresKuldes
        {
            get { return _KeresKuldes; }
            set { _KeresKuldes = value; }
        }
                          
           
        /// <summary>
        /// RequestData property
        /// 
        /// </summary>
        private Field _RequestData = new Field();

        public Field RequestData
        {
            get { return _RequestData; }
            set { _RequestData = value; }
        }
                          
           
        /// <summary>
        /// ResponseData property
        /// 
        /// </summary>
        private Field _ResponseData = new Field();

        public Field ResponseData
        {
            get { return _ResponseData; }
            set { _ResponseData = value; }
        }
                          
           
        /// <summary>
        /// FuttatoFelhasznalo property
        /// 
        /// </summary>
        private Field _FuttatoFelhasznalo = new Field();

        public Field FuttatoFelhasznalo
        {
            get { return _FuttatoFelhasznalo; }
            set { _FuttatoFelhasznalo = value; }
        }
                          
           
        /// <summary>
        /// Local property
        /// 
        /// </summary>
        private Field _Local = new Field();

        public Field Local
        {
            get { return _Local; }
            set { _Local = value; }
        }
                          
           
        /// <summary>
        /// Hiba property
        /// 
        /// </summary>
        private Field _Hiba = new Field();

        public Field Hiba
        {
            get { return _Hiba; }
            set { _Hiba = value; }
        }
                          
           
        /// <summary>
        /// Sikeres property
        /// 
        /// </summary>
        private Field _Sikeres = new Field();

        public Field Sikeres
        {
            get { return _Sikeres; }
            set { _Sikeres = value; }
        }
                              }

}