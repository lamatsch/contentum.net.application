
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_States eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_StatesSearch
    {
      public KRT_StatesSearch()
      {               _Id.Name = "KRT_States.Id";
               _Id.Type = "Guid";            
               _Kod.Name = "KRT_States.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "KRT_States.Nev";
               _Nev.Type = "String";            
               _Stat_Id_Szulo.Name = "KRT_States.Stat_Id_Szulo";
               _Stat_Id_Szulo.Type = "Guid";            
               _ErvKezd.Name = "KRT_States.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_States.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Stat_Id_Szulo = new Field();

        public Field Stat_Id_Szulo
        {
            get { return _Stat_Id_Szulo; }
            set { _Stat_Id_Szulo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}