
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Szemelyek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_SzemelyekSearch: BaseSearchObject    {
      public KRT_SzemelyekSearch()
      {            
                     _Id.Name = "KRT_Szemelyek.Id";
               _Id.Type = "Guid";            
               _Partner_Id.Name = "KRT_Szemelyek.Partner_Id";
               _Partner_Id.Type = "Guid";            
               _AnyjaNeve.Name = "KRT_Szemelyek.AnyjaNeve";
               _AnyjaNeve.Type = "String";            
               _AnyjaNeveCsaladiNev.Name = "KRT_Szemelyek.AnyjaNeveCsaladiNev";
               _AnyjaNeveCsaladiNev.Type = "String";            
               _AnyjaNeveElsoUtonev.Name = "KRT_Szemelyek.AnyjaNeveElsoUtonev";
               _AnyjaNeveElsoUtonev.Type = "String";            
               _AnyjaNeveTovabbiUtonev.Name = "KRT_Szemelyek.AnyjaNeveTovabbiUtonev";
               _AnyjaNeveTovabbiUtonev.Type = "String";            
               _ApjaNeve.Name = "KRT_Szemelyek.ApjaNeve";
               _ApjaNeve.Type = "String";            
               _SzuletesiNev.Name = "KRT_Szemelyek.SzuletesiNev";
               _SzuletesiNev.Type = "String";            
               _SzuletesiCsaladiNev.Name = "KRT_Szemelyek.SzuletesiCsaladiNev";
               _SzuletesiCsaladiNev.Type = "String";            
               _SzuletesiElsoUtonev.Name = "KRT_Szemelyek.SzuletesiElsoUtonev";
               _SzuletesiElsoUtonev.Type = "String";            
               _SzuletesiTovabbiUtonev.Name = "KRT_Szemelyek.SzuletesiTovabbiUtonev";
               _SzuletesiTovabbiUtonev.Type = "String";            
               _SzuletesiOrszag.Name = "KRT_Szemelyek.SzuletesiOrszag";
               _SzuletesiOrszag.Type = "String";            
               _SzuletesiOrszagId.Name = "KRT_Szemelyek.SzuletesiOrszagId";
               _SzuletesiOrszagId.Type = "Guid";            
               _UjTitulis.Name = "KRT_Szemelyek.UjTitulis";
               _UjTitulis.Type = "String";            
               _UjCsaladiNev.Name = "KRT_Szemelyek.UjCsaladiNev";
               _UjCsaladiNev.Type = "String";            
               _UjUtonev.Name = "KRT_Szemelyek.UjUtonev";
               _UjUtonev.Type = "String";            
               _UjTovabbiUtonev.Name = "KRT_Szemelyek.UjTovabbiUtonev";
               _UjTovabbiUtonev.Type = "String";            
               _SzuletesiHely.Name = "KRT_Szemelyek.SzuletesiHely";
               _SzuletesiHely.Type = "String";            
               _SzuletesiHely_id.Name = "KRT_Szemelyek.SzuletesiHely_id";
               _SzuletesiHely_id.Type = "Guid";            
               _SzuletesiIdo.Name = "KRT_Szemelyek.SzuletesiIdo";
               _SzuletesiIdo.Type = "DateTime";            
               _Allampolgarsag.Name = "KRT_Szemelyek.Allampolgarsag";
               _Allampolgarsag.Type = "String";            
               _TAJSzam.Name = "KRT_Szemelyek.TAJSzam";
               _TAJSzam.Type = "String";            
               _SZIGSzam.Name = "KRT_Szemelyek.SZIGSzam";
               _SZIGSzam.Type = "String";            
               _Neme.Name = "KRT_Szemelyek.Neme";
               _Neme.Type = "Char";            
               _SzemelyiAzonosito.Name = "KRT_Szemelyek.SzemelyiAzonosito";
               _SzemelyiAzonosito.Type = "String";            
               _Adoazonosito.Name = "KRT_Szemelyek.Adoazonosito";
               _Adoazonosito.Type = "String";            
               _Adoszam.Name = "KRT_Szemelyek.Adoszam";
               _Adoszam.Type = "String";            
               _KulfoldiAdoszamJelolo.Name = "KRT_Szemelyek.KulfoldiAdoszamJelolo";
               _KulfoldiAdoszamJelolo.Type = "Char";            
               _Beosztas.Name = "KRT_Szemelyek.Beosztas";
               _Beosztas.Type = "String";            
               _MinositesiSzint.Name = "KRT_Szemelyek.MinositesiSzint";
               _MinositesiSzint.Type = "String";            
               _ErvKezd.Name = "KRT_Szemelyek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Szemelyek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id property
        /// Partner id
        /// </summary>
        private Field _Partner_Id = new Field();

        public Field Partner_Id
        {
            get { return _Partner_Id; }
            set { _Partner_Id = value; }
        }
                          
           
        /// <summary>
        /// AnyjaNeve property
        /// Anyja neve. Ha tudjuk, akkor reszletesen az ANYJA_VEZETEKNEVE+ANYJA_KERESZTNEVEI.
        /// </summary>
        private Field _AnyjaNeve = new Field();

        public Field AnyjaNeve
        {
            get { return _AnyjaNeve; }
            set { _AnyjaNeve = value; }
        }
                          
           
        /// <summary>
        /// AnyjaNeveCsaladiNev property
        /// 
        /// </summary>
        private Field _AnyjaNeveCsaladiNev = new Field();

        public Field AnyjaNeveCsaladiNev
        {
            get { return _AnyjaNeveCsaladiNev; }
            set { _AnyjaNeveCsaladiNev = value; }
        }
                          
           
        /// <summary>
        /// AnyjaNeveElsoUtonev property
        /// 
        /// </summary>
        private Field _AnyjaNeveElsoUtonev = new Field();

        public Field AnyjaNeveElsoUtonev
        {
            get { return _AnyjaNeveElsoUtonev; }
            set { _AnyjaNeveElsoUtonev = value; }
        }
                          
           
        /// <summary>
        /// AnyjaNeveTovabbiUtonev property
        /// 
        /// </summary>
        private Field _AnyjaNeveTovabbiUtonev = new Field();

        public Field AnyjaNeveTovabbiUtonev
        {
            get { return _AnyjaNeveTovabbiUtonev; }
            set { _AnyjaNeveTovabbiUtonev = value; }
        }
                          
           
        /// <summary>
        /// ApjaNeve property
        /// Apja neve (KANYV-igeny). Ha tudjuk, akkor az APJA_VEZETEKNEVE+APJA_KERESZTNEVEI.
        /// </summary>
        private Field _ApjaNeve = new Field();

        public Field ApjaNeve
        {
            get { return _ApjaNeve; }
            set { _ApjaNeve = value; }
        }
                          
           
        /// <summary>
        /// SzuletesiNev property
        /// Most elozo nev a hivatalos megnevezese.
        /// </summary>
        private Field _SzuletesiNev = new Field();

        public Field SzuletesiNev
        {
            get { return _SzuletesiNev; }
            set { _SzuletesiNev = value; }
        }
                          
           
        /// <summary>
        /// SzuletesiCsaladiNev property
        /// 
        /// </summary>
        private Field _SzuletesiCsaladiNev = new Field();

        public Field SzuletesiCsaladiNev
        {
            get { return _SzuletesiCsaladiNev; }
            set { _SzuletesiCsaladiNev = value; }
        }
                          
           
        /// <summary>
        /// SzuletesiElsoUtonev property
        /// 
        /// </summary>
        private Field _SzuletesiElsoUtonev = new Field();

        public Field SzuletesiElsoUtonev
        {
            get { return _SzuletesiElsoUtonev; }
            set { _SzuletesiElsoUtonev = value; }
        }
                          
           
        /// <summary>
        /// SzuletesiTovabbiUtonev property
        /// 
        /// </summary>
        private Field _SzuletesiTovabbiUtonev = new Field();

        public Field SzuletesiTovabbiUtonev
        {
            get { return _SzuletesiTovabbiUtonev; }
            set { _SzuletesiTovabbiUtonev = value; }
        }
                          
           
        /// <summary>
        /// SzuletesiOrszag property
        /// Szem�ly sz�let�si hely�nek orsz�ga
        /// </summary>
        private Field _SzuletesiOrszag = new Field();

        public Field SzuletesiOrszag
        {
            get { return _SzuletesiOrszag; }
            set { _SzuletesiOrszag = value; }
        }
                          
           
        /// <summary>
        /// SzuletesiOrszagId property
        /// Sz�let�si orsz�g Id
        /// </summary>
        private Field _SzuletesiOrszagId = new Field();

        public Field SzuletesiOrszagId
        {
            get { return _SzuletesiOrszagId; }
            set { _SzuletesiOrszagId = value; }
        }
                          
           
        /// <summary>
        /// UjTitulis property
        /// Szem�ly titulusa: Dr, id.
        /// </summary>
        private Field _UjTitulis = new Field();

        public Field UjTitulis
        {
            get { return _UjTitulis; }
            set { _UjTitulis = value; }
        }
                          
           
        /// <summary>
        /// UjCsaladiNev property
        /// Szem�ly "�j" csal�dneve(i)
        /// </summary>
        private Field _UjCsaladiNev = new Field();

        public Field UjCsaladiNev
        {
            get { return _UjCsaladiNev; }
            set { _UjCsaladiNev = value; }
        }
                          
           
        /// <summary>
        /// UjUtonev property
        /// Szem�ly aktu�lis ut�neve
        /// </summary>
        private Field _UjUtonev = new Field();

        public Field UjUtonev
        {
            get { return _UjUtonev; }
            set { _UjUtonev = value; }
        }
                          
           
        /// <summary>
        /// UjTovabbiUtonev property
        /// 
        /// </summary>
        private Field _UjTovabbiUtonev = new Field();

        public Field UjTovabbiUtonev
        {
            get { return _UjTovabbiUtonev; }
            set { _UjTovabbiUtonev = value; }
        }
                          
           
        /// <summary>
        /// SzuletesiHely property
        /// Szuletesi hely szovegesen. Ha megvan, akkor a telepules ID a SZUL_HELY-ben van.
        /// </summary>
        private Field _SzuletesiHely = new Field();

        public Field SzuletesiHely
        {
            get { return _SzuletesiHely; }
            set { _SzuletesiHely = value; }
        }
                          
           
        /// <summary>
        /// SzuletesiHely_id property
        /// Review: mire mutat? A cimeket kell legel�sz�r Kovax -al megtervezni! Telepules ID. Szovegesen is megvan a SZUL_HELY_STR-ben.
        /// </summary>
        private Field _SzuletesiHely_id = new Field();

        public Field SzuletesiHely_id
        {
            get { return _SzuletesiHely_id; }
            set { _SzuletesiHely_id = value; }
        }
                          
           
        /// <summary>
        /// SzuletesiIdo property
        /// Szuletesi datum.
        /// </summary>
        private Field _SzuletesiIdo = new Field();

        public Field SzuletesiIdo
        {
            get { return _SzuletesiIdo; }
            set { _SzuletesiIdo = value; }
        }
                          
           
        /// <summary>
        /// Allampolgarsag property
        /// 
        /// </summary>
        private Field _Allampolgarsag = new Field();

        public Field Allampolgarsag
        {
            get { return _Allampolgarsag; }
            set { _Allampolgarsag = value; }
        }
                          
           
        /// <summary>
        /// TAJSzam property
        /// Szem�ly TAJ sz�ma
        /// </summary>
        private Field _TAJSzam = new Field();

        public Field TAJSzam
        {
            get { return _TAJSzam; }
            set { _TAJSzam = value; }
        }
                          
           
        /// <summary>
        /// SZIGSzam property
        /// Aktualis szemelyi igazolvany szama.
        /// </summary>
        private Field _SZIGSzam = new Field();

        public Field SZIGSzam
        {
            get { return _SZIGSzam; }
            set { _SZIGSzam = value; }
        }
                          
           
        /// <summary>
        /// Neme property
        /// Szem�ly neme
        /// F�rfi: 1
        /// N�: 2
        /// </summary>
        private Field _Neme = new Field();

        public Field Neme
        {
            get { return _Neme; }
            set { _Neme = value; }
        }
                          
           
        /// <summary>
        /// SzemelyiAzonosito property
        /// A regi szemelyi szam.
        /// </summary>
        private Field _SzemelyiAzonosito = new Field();

        public Field SzemelyiAzonosito
        {
            get { return _SzemelyiAzonosito; }
            set { _SzemelyiAzonosito = value; }
        }
                          
           
        /// <summary>
        /// Adoazonosito property
        /// Ad�azonosit� jel.
        /// </summary>
        private Field _Adoazonosito = new Field();

        public Field Adoazonosito
        {
            get { return _Adoazonosito; }
            set { _Adoazonosito = value; }
        }
                          
           
        /// <summary>
        /// Adoszam property
        /// Egyeni vallalkozonal, illetve adoszamos maganszemelynel van.
        /// </summary>
        private Field _Adoszam = new Field();

        public Field Adoszam
        {
            get { return _Adoszam; }
            set { _Adoszam = value; }
        }
                          
           
        /// <summary>
        /// KulfoldiAdoszamJelolo property
        /// Belf�ldi vagy k�lf�ldi ad�sz�m:
        /// Ha �rt�ke '1', az ad�sz�m k�lf�ldi (a magyar form�tum nem alkalmazhat�)
        /// Minden m�s �rt�k eset�n az ad�sz�m belf�ldi.
        /// </summary>
        private Field _KulfoldiAdoszamJelolo = new Field();

        public Field KulfoldiAdoszamJelolo
        {
            get { return _KulfoldiAdoszamJelolo; }
            set { _KulfoldiAdoszamJelolo = value; }
        }
                          
           
        /// <summary>
        /// Beosztas property
        /// 
        /// </summary>
        private Field _Beosztas = new Field();

        public Field Beosztas
        {
            get { return _Beosztas; }
            set { _Beosztas = value; }
        }
                          
           
        /// <summary>
        /// MinositesiSzint property
        /// 
        /// </summary>
        private Field _MinositesiSzint = new Field();

        public Field MinositesiSzint
        {
            get { return _MinositesiSzint; }
            set { _MinositesiSzint = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}