
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Szerepkor_Funkcio eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_Szerepkor_FunkcioSearch: BaseSearchObject    {
      public KRT_Szerepkor_FunkcioSearch()
      {            
                     _Id.Name = "KRT_Szerepkor_Funkcio.Id";
               _Id.Type = "Guid";            
               _Funkcio_Id.Name = "KRT_Szerepkor_Funkcio.Funkcio_Id";
               _Funkcio_Id.Type = "Guid";            
               _Szerepkor_Id.Name = "KRT_Szerepkor_Funkcio.Szerepkor_Id";
               _Szerepkor_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_Szerepkor_Funkcio.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Szerepkor_Funkcio.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Funkcio_Id = new Field();

        public Field Funkcio_Id
        {
            get { return _Funkcio_Id; }
            set { _Funkcio_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Szerepkor_Id = new Field();

        public Field Szerepkor_Id
        {
            get { return _Szerepkor_Id; }
            set { _Szerepkor_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}