
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Szerepkorok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_SzerepkorokSearch: BaseSearchObject    {
      public KRT_SzerepkorokSearch()
      {            
                     _Id.Name = "KRT_Szerepkorok.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_Szerepkorok.Org";
               _Org.Type = "Guid";            
               _Nev.Name = "KRT_Szerepkorok.Nev";
               _Nev.Type = "String";            
               _KulsoAzonositok.Name = "KRT_Szerepkorok.KulsoAzonositok";
               _KulsoAzonositok.Type = "String";            
               _ErvKezd.Name = "KRT_Szerepkorok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Szerepkorok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KulsoAzonositok = new Field();

        public Field KulsoAzonositok
        {
            get { return _KulsoAzonositok; }
            set { _KulsoAzonositok = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}