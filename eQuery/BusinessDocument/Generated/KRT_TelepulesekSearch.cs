
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Telepulesek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_TelepulesekSearch: BaseSearchObject    {
      public KRT_TelepulesekSearch()
      {            
                     _Id.Name = "KRT_Telepulesek.Id";
               _Id.Type = "Guid";            
               _Org.Name = "KRT_Telepulesek.Org";
               _Org.Type = "Guid";            
               _IRSZ.Name = "KRT_Telepulesek.IRSZ";
               _IRSZ.Type = "String";            
               _Telepules_Id_Fo.Name = "KRT_Telepulesek.Telepules_Id_Fo";
               _Telepules_Id_Fo.Type = "Guid";            
               _Nev.Name = "KRT_Telepulesek.Nev";
               _Nev.Type = "String";            
               _Orszag_Id.Name = "KRT_Telepulesek.Orszag_Id";
               _Orszag_Id.Type = "Guid";            
               _Megye.Name = "KRT_Telepulesek.Megye";
               _Megye.Type = "String";            
               _Regio.Name = "KRT_Telepulesek.Regio";
               _Regio.Type = "String";            
               _ErvKezd.Name = "KRT_Telepulesek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Telepulesek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Org = new Field();

        public Field Org
        {
            get { return _Org; }
            set { _Org = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _IRSZ = new Field();

        public Field IRSZ
        {
            get { return _IRSZ; }
            set { _IRSZ = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Telepules_Id_Fo = new Field();

        public Field Telepules_Id_Fo
        {
            get { return _Telepules_Id_Fo; }
            set { _Telepules_Id_Fo = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Orszag_Id = new Field();

        public Field Orszag_Id
        {
            get { return _Orszag_Id; }
            set { _Orszag_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Megye = new Field();

        public Field Megye
        {
            get { return _Megye; }
            set { _Megye = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Regio = new Field();

        public Field Regio
        {
            get { return _Regio; }
            set { _Regio = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}