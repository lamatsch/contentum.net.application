
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_TemplateManager eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_TemplateManagerSearch: BaseSearchObject    {
      public KRT_TemplateManagerSearch()
      {            
                     _Id.Name = "KRT_TemplateManager.Id";
               _Id.Type = "Guid";            
               _Nev.Name = "KRT_TemplateManager.Nev";
               _Nev.Type = "String";            
               _Tipus.Name = "KRT_TemplateManager.Tipus";
               _Tipus.Type = "String";            
               _KRT_Modul_Id.Name = "KRT_TemplateManager.KRT_Modul_Id";
               _KRT_Modul_Id.Type = "Guid";            
               _KRT_Dokumentum_Id.Name = "KRT_TemplateManager.KRT_Dokumentum_Id";
               _KRT_Dokumentum_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_TemplateManager.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_TemplateManager.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KRT_Modul_Id = new Field();

        public Field KRT_Modul_Id
        {
            get { return _KRT_Modul_Id; }
            set { _KRT_Modul_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _KRT_Dokumentum_Id = new Field();

        public Field KRT_Dokumentum_Id
        {
            get { return _KRT_Dokumentum_Id; }
            set { _KRT_Dokumentum_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}