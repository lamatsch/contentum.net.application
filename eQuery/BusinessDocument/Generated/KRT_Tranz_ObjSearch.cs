
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Tranz_Obj eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_Tranz_ObjSearch: BaseSearchObject    {
      public KRT_Tranz_ObjSearch()
      {            
                     _Id.Name = "KRT_Tranz_Obj.Id";
               _Id.Type = "Guid";            
               _ObjTip_Id_AdatElem.Name = "KRT_Tranz_Obj.ObjTip_Id_AdatElem";
               _ObjTip_Id_AdatElem.Type = "Guid";            
               _Obj_Id.Name = "KRT_Tranz_Obj.Obj_Id";
               _Obj_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_Tranz_Obj.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Tranz_Obj.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";            
               _Tranz_id.Name = "KRT_Tranz_Obj.Tranz_id";
               _Tranz_id.Type = "Guid";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id_AdatElem = new Field();

        public Field ObjTip_Id_AdatElem
        {
            get { return _ObjTip_Id_AdatElem; }
            set { _ObjTip_Id_AdatElem = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Obj_Id = new Field();

        public Field Obj_Id
        {
            get { return _Obj_Id; }
            set { _Obj_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Tranz_id = new Field();

        public Field Tranz_id
        {
            get { return _Tranz_id; }
            set { _Tranz_id = value; }
        }
                              }

}