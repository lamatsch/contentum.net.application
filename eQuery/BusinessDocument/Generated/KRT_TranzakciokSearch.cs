
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Tranzakciok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_TranzakciokSearch: BaseSearchObject    {
      public KRT_TranzakciokSearch()
      {            
                     _Id.Name = "KRT_Tranzakciok.Id";
               _Id.Type = "Guid";            
               _Alkalmazas_Id.Name = "KRT_Tranzakciok.Alkalmazas_Id";
               _Alkalmazas_Id.Type = "Guid";            
               _Felhasznalo_Id.Name = "KRT_Tranzakciok.Felhasznalo_Id";
               _Felhasznalo_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_Tranzakciok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Tranzakciok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Alkalmazas_Id = new Field();

        public Field Alkalmazas_Id
        {
            get { return _Alkalmazas_Id; }
            set { _Alkalmazas_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id = new Field();

        public Field Felhasznalo_Id
        {
            get { return _Felhasznalo_Id; }
            set { _Felhasznalo_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}