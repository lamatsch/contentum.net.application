
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_UIMezoObjektumErtekek eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_UIMezoObjektumErtekekSearch: BaseSearchObject    {
      public KRT_UIMezoObjektumErtekekSearch()
      {            
                     _Id.Name = "KRT_UIMezoObjektumErtekek.Id";
               _Id.Type = "Guid";            
               _TemplateTipusNev.Name = "KRT_UIMezoObjektumErtekek.TemplateTipusNev";
               _TemplateTipusNev.Type = "String";            
               _Nev.Name = "KRT_UIMezoObjektumErtekek.Nev";
               _Nev.Type = "String";            
               _Alapertelmezett.Name = "KRT_UIMezoObjektumErtekek.Alapertelmezett";
               _Alapertelmezett.Type = "Char";            
               _Felhasznalo_Id.Name = "KRT_UIMezoObjektumErtekek.Felhasznalo_Id";
               _Felhasznalo_Id.Type = "Guid";            
               _TemplateXML.Name = "KRT_UIMezoObjektumErtekek.TemplateXML";
               _TemplateXML.Type = "String";            
               _UtolsoHasznIdo.Name = "KRT_UIMezoObjektumErtekek.UtolsoHasznIdo";
               _UtolsoHasznIdo.Type = "DateTime";            
               _Org_Id.Name = "KRT_UIMezoObjektumErtekek.Org_Id";
               _Org_Id.Type = "Guid";            
               _Publikus.Name = "KRT_UIMezoObjektumErtekek.Publikus";
               _Publikus.Type = "Char";            
               _Szervezet_Id.Name = "KRT_UIMezoObjektumErtekek.Szervezet_Id";
               _Szervezet_Id.Type = "Guid";            
               _ErvKezd.Name = "KRT_UIMezoObjektumErtekek.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_UIMezoObjektumErtekek.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// TemplateTipusNev property
        /// A template t�pusa (ami sokszor egyezik a t�bl�val), de lehet teljesen egyedi is.
        /// </summary>
        private Field _TemplateTipusNev = new Field();

        public Field TemplateTipusNev
        {
            get { return _TemplateTipusNev; }
            set { _TemplateTipusNev = value; }
        }
                          
           
        /// <summary>
        /// Nev property
        /// A template neve
        /// </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Alapertelmezett property
        /// Alap�rtemezett template Igen/nem (1/0).
        /// </summary>
        private Field _Alapertelmezett = new Field();

        public Field Alapertelmezett
        {
            get { return _Alapertelmezett; }
            set { _Alapertelmezett = value; }
        }
                          
           
        /// <summary>
        /// Felhasznalo_Id property
        /// El�g lenne, ha a letrehozo_id lenne benne, de az�rt hagytuk m�gis, mivel �gy lehetnek nem felhaszn�l�hoz k�t�tt,
        /// �ltal�nos haszn�lat� template ment�sek.
        /// </summary>
        private Field _Felhasznalo_Id = new Field();

        public Field Felhasznalo_Id
        {
            get { return _Felhasznalo_Id; }
            set { _Felhasznalo_Id = value; }
        }
                          
           
        /// <summary>
        /// TemplateXML property
        /// Ez a mentett "�rlap" xml form�tumban
        /// </summary>
        private Field _TemplateXML = new Field();

        public Field TemplateXML
        {
            get { return _TemplateXML; }
            set { _TemplateXML = value; }
        }
                          
           
        /// <summary>
        /// UtolsoHasznIdo property
        /// Az objektum utols� bet�lt�si ideje
        /// </summary>
        private Field _UtolsoHasznIdo = new Field();

        public Field UtolsoHasznIdo
        {
            get { return _UtolsoHasznIdo; }
            set { _UtolsoHasznIdo = value; }
        }
                          
           
        /// <summary>
        /// Org_Id property
        /// 
        /// </summary>
        private Field _Org_Id = new Field();

        public Field Org_Id
        {
            get { return _Org_Id; }
            set { _Org_Id = value; }
        }
                          
           
        /// <summary>
        /// Publikus property
        /// 
        /// </summary>
        private Field _Publikus = new Field();

        public Field Publikus
        {
            get { return _Publikus; }
            set { _Publikus = value; }
        }
                          
           
        /// <summary>
        /// Szervezet_Id property
        /// Publikus template eset�n a felhaszn�l� szervezete, melyen bel�l publikus lesz a template
        /// </summary>
        private Field _Szervezet_Id = new Field();

        public Field Szervezet_Id
        {
            get { return _Szervezet_Id; }
            set { _Szervezet_Id = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}