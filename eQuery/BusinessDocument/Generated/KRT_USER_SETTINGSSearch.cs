
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_USER_SETTINGS eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class KRT_USER_SETTINGSSearch
    {
      public KRT_USER_SETTINGSSearch()
      {               _Id.Name = "KRT_USER_SETTINGS.Id";
               _Id.Type = "Guid";            
               _Csoport_Id.Name = "KRT_USER_SETTINGS.Csoport_Id";
               _Csoport_Id.Type = "Guid";            
               _BEALLITASTIPUS.Name = "KRT_USER_SETTINGS.BEALLITASTIPUS";
               _BEALLITASTIPUS.Type = "Char";            
               _BEALLITASOK_XML.Name = "KRT_USER_SETTINGS.BEALLITASOK_XML";
               _BEALLITASOK_XML.Type = "String";            
               _ErvKezd.Name = "KRT_USER_SETTINGS.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_USER_SETTINGS.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Csoport_Id = new Field();

        public Field Csoport_Id
        {
            get { return _Csoport_Id; }
            set { _Csoport_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _BEALLITASTIPUS = new Field();

        public Field BEALLITASTIPUS
        {
            get { return _BEALLITASTIPUS; }
            set { _BEALLITASTIPUS = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _BEALLITASOK_XML = new Field();

        public Field BEALLITASOK_XML
        {
            get { return _BEALLITASOK_XML; }
            set { _BEALLITASOK_XML = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}