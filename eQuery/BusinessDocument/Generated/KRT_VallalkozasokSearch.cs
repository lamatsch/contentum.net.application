
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Vallalkozasok eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class KRT_VallalkozasokSearch: BaseSearchObject    {
      public KRT_VallalkozasokSearch()
      {            
                     _Id.Name = "KRT_Vallalkozasok.Id";
               _Id.Type = "Guid";            
               _Partner_Id.Name = "KRT_Vallalkozasok.Partner_Id";
               _Partner_Id.Type = "Guid";            
               _Adoszam.Name = "KRT_Vallalkozasok.Adoszam";
               _Adoszam.Type = "String";            
               _KulfoldiAdoszamJelolo.Name = "KRT_Vallalkozasok.KulfoldiAdoszamJelolo";
               _KulfoldiAdoszamJelolo.Type = "Char";            
               _TB_Torzsszam.Name = "KRT_Vallalkozasok.TB_Torzsszam";
               _TB_Torzsszam.Type = "String";            
               _Cegjegyzekszam.Name = "KRT_Vallalkozasok.Cegjegyzekszam";
               _Cegjegyzekszam.Type = "String";            
               _Tipus.Name = "KRT_Vallalkozasok.Tipus";
               _Tipus.Type = "String";            
               _ErvKezd.Name = "KRT_Vallalkozasok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "KRT_Vallalkozasok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property
        /// Egyedi azonos�t�
        /// </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Partner_Id property
        /// Partner id
        /// </summary>
        private Field _Partner_Id = new Field();

        public Field Partner_Id
        {
            get { return _Partner_Id; }
            set { _Partner_Id = value; }
        }
                          
           
        /// <summary>
        /// Adoszam property
        /// V�llalkoz�s ad�sz�ma
        /// </summary>
        private Field _Adoszam = new Field();

        public Field Adoszam
        {
            get { return _Adoszam; }
            set { _Adoszam = value; }
        }
                          
           
        /// <summary>
        /// KulfoldiAdoszamJelolo property
        /// Belf�ldi vagy k�lf�ldi ad�sz�m:
        /// Ha �rt�ke '1', az ad�sz�m k�lf�ldi (a magyar form�tum nem alkalmazhat�)
        /// Minden m�s �rt�k eset�n az ad�sz�m belf�ldi.
        /// </summary>
        private Field _KulfoldiAdoszamJelolo = new Field();

        public Field KulfoldiAdoszamJelolo
        {
            get { return _KulfoldiAdoszamJelolo; }
            set { _KulfoldiAdoszamJelolo = value; }
        }
                          
           
        /// <summary>
        /// TB_Torzsszam property
        /// ??
        /// </summary>
        private Field _TB_Torzsszam = new Field();

        public Field TB_Torzsszam
        {
            get { return _TB_Torzsszam; }
            set { _TB_Torzsszam = value; }
        }
                          
           
        /// <summary>
        /// Cegjegyzekszam property
        /// C�gb�r�s�gi c�gjegyz�ksz�m
        /// </summary>
        private Field _Cegjegyzekszam = new Field();

        public Field Cegjegyzekszam
        {
            get { return _Cegjegyzekszam; }
            set { _Cegjegyzekszam = value; }
        }
                          
           
        /// <summary>
        /// Tipus property
        /// KCS:CEGTIPUS, K�dt�rb�l, Nvarchar(2).
        /// pl.:
        /// 01 - V�llalkoz�
        /// 02 - Bt
        /// 03 - Kft
        /// 04 - Rt
        /// 05 - Kht
        /// 06 - Kkt 
        /// </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// ErvKezd property
        /// �rv�nyess�g kezdete (�ltal�ban d�tum!) (lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// ErvVege property
        /// �rv�nyess�g  v�ge (�ltal�ban d�tum!)(lehet, h csin�lunk majd ErvKezdDat -ot is, erre Domain -t)
        /// </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}