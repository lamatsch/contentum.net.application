
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// LoginLogs eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class LoginLogsSearch    {
      public LoginLogsSearch()
      {            
                     _Id.Name = "LoginLogs.Id";
               _Id.Type = "Guid";            
               _MachineIPAddress.Name = "LoginLogs.MachineIPAddress";
               _MachineIPAddress.Type = "String";            
               _MachineName.Name = "LoginLogs.MachineName";
               _MachineName.Type = "String";            
               _Felhasznalo_Id.Name = "LoginLogs.Felhasznalo_Id";
               _Felhasznalo_Id.Type = "Guid";            
               _Jelszo.Name = "LoginLogs.Jelszo";
               _Jelszo.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MachineIPAddress = new Field();

        public Field MachineIPAddress
        {
            get { return _MachineIPAddress; }
            set { _MachineIPAddress = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _MachineName = new Field();

        public Field MachineName
        {
            get { return _MachineName; }
            set { _MachineName = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Felhasznalo_Id = new Field();

        public Field Felhasznalo_Id
        {
            get { return _Felhasznalo_Id; }
            set { _Felhasznalo_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Jelszo = new Field();

        public Field Jelszo
        {
            get { return _Jelszo; }
            set { _Jelszo = value; }
        }
                              }

}