
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// ObjLovs eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class ObjLovsSearch
    {
      public ObjLovsSearch()
      {               _Id.Name = "ObjLovs.Id";
               _Id.Type = "Guid";            
               _ObjTip_id.Name = "ObjLovs.ObjTip_id";
               _ObjTip_id.Type = "Guid";            
               _Kod.Name = "ObjLovs.Kod";
               _Kod.Type = "String";            
               _Nev.Name = "ObjLovs.Nev";
               _Nev.Type = "String";            
               _ErvKezd.Name = "ObjLovs.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "ObjLovs.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_id = new Field();

        public Field ObjTip_id
        {
            get { return _ObjTip_id; }
            set { _ObjTip_id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Kod = new Field();

        public Field Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}