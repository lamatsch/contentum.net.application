
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// ObjStatok eQuery.BusinessDocument Class </summary>
    [Serializable()]
    public class ObjStatokSearch
    {
      public ObjStatokSearch()
      {               _Id.Name = "ObjStatok.Id";
               _Id.Type = "Guid";            
               _Nev.Name = "ObjStatok.Nev";
               _Nev.Type = "String";            
               _ObjTip_Id.Name = "ObjStatok.ObjTip_Id";
               _ObjTip_Id.Type = "Guid";            
               _ObjStat_Id.Name = "ObjStatok.ObjStat_Id";
               _ObjStat_Id.Type = "Guid";            
               _ErvKezd.Name = "ObjStatok.ErvKezd";
               _ErvKezd.Type = "DateTime";                            _ErvKezd.Operator = Query.Operators.lessorequal;                   
               _ErvKezd.Value = "getdate()";            
               _ErvVege.Name = "ObjStatok.ErvVege";
               _ErvVege.Type = "DateTime";                           _ErvVege.Operator = Query.Operators.greaterorequal; 
               _ErvVege.Value = "getdate()";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjTip_Id = new Field();

        public Field ObjTip_Id
        {
            get { return _ObjTip_Id; }
            set { _ObjTip_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ObjStat_Id = new Field();

        public Field ObjStat_Id
        {
            get { return _ObjStat_Id; }
            set { _ObjStat_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }
                              }

}