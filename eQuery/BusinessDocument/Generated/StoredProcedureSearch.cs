﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    [Serializable()]
    public class StoredProcedureSearch : BaseSearchObject
    {
        public StoredProcedureSearch()
        {
            _Id.Name = "object_id";
            _Id.Type = "Int32";
            _Nev.Name = "name";
            _Nev.Type = "String";
        }

        public String OrderBy = "";
        public Int32 TopRow = 0;

        private Field _Id = new Field();
        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private Field _Nev = new Field();
        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
    }
}
