
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// UIAccessLogs eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class UIAccessLogsSearch    {
      public UIAccessLogsSearch()
      {            
                     _Id.Name = "UIAccessLogs.Id";
               _Id.Type = "Guid";            
               _LoginLog_Id.Name = "UIAccessLogs.LoginLog_Id";
               _LoginLog_Id.Type = "Guid";            
               _StartDateOfAsp.Name = "UIAccessLogs.StartDateOfAsp";
               _StartDateOfAsp.Type = "DateTime";            
               _EndDateOfAsp.Name = "UIAccessLogs.EndDateOfAsp";
               _EndDateOfAsp.Type = "DateTime";            
               _StartOfDBConn.Name = "UIAccessLogs.StartOfDBConn";
               _StartOfDBConn.Type = "DateTime";            
               _EndOfDBConn.Name = "UIAccessLogs.EndOfDBConn";
               _EndOfDBConn.Type = "DateTime";            
               _PathInfo.Name = "UIAccessLogs.PathInfo";
               _PathInfo.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LoginLog_Id = new Field();

        public Field LoginLog_Id
        {
            get { return _LoginLog_Id; }
            set { _LoginLog_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _StartDateOfAsp = new Field();

        public Field StartDateOfAsp
        {
            get { return _StartDateOfAsp; }
            set { _StartDateOfAsp = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _EndDateOfAsp = new Field();

        public Field EndDateOfAsp
        {
            get { return _EndDateOfAsp; }
            set { _EndDateOfAsp = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _StartOfDBConn = new Field();

        public Field StartOfDBConn
        {
            get { return _StartOfDBConn; }
            set { _StartOfDBConn = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _EndOfDBConn = new Field();

        public Field EndOfDBConn
        {
            get { return _EndOfDBConn; }
            set { _EndOfDBConn = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _PathInfo = new Field();

        public Field PathInfo
        {
            get { return _PathInfo; }
            set { _PathInfo = value; }
        }
                              }

}