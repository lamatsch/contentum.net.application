
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// WSAccessLogs eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class WSAccessLogsSearch    {
      public WSAccessLogsSearch()
      {            
                     _Id.Name = "WSAccessLogs.Id";
               _Id.Type = "Guid";            
               _LoginLog_Id.Name = "WSAccessLogs.LoginLog_Id";
               _LoginLog_Id.Type = "Guid";            
               _UIAccessLog_Id.Name = "WSAccessLogs.UIAccessLog_Id";
               _UIAccessLog_Id.Type = "Guid";            
               _StartDateOfWS.Name = "WSAccessLogs.StartDateOfWS";
               _StartDateOfWS.Type = "DateTime";            
               _EndDateOfWS.Name = "WSAccessLogs.EndDateOfWS";
               _EndDateOfWS.Type = "DateTime";            
               _StartOfDBConn.Name = "WSAccessLogs.StartOfDBConn";
               _StartOfDBConn.Type = "DateTime";            
               _EndOfDBConn.Name = "WSAccessLogs.EndOfDBConn";
               _EndOfDBConn.Type = "DateTime";            
               _PathInfo.Name = "WSAccessLogs.PathInfo";
               _PathInfo.Type = "String";                   }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
                          
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _LoginLog_Id = new Field();

        public Field LoginLog_Id
        {
            get { return _LoginLog_Id; }
            set { _LoginLog_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _UIAccessLog_Id = new Field();

        public Field UIAccessLog_Id
        {
            get { return _UIAccessLog_Id; }
            set { _UIAccessLog_Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _StartDateOfWS = new Field();

        public Field StartDateOfWS
        {
            get { return _StartDateOfWS; }
            set { _StartDateOfWS = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _EndDateOfWS = new Field();

        public Field EndDateOfWS
        {
            get { return _EndDateOfWS; }
            set { _EndDateOfWS = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _StartOfDBConn = new Field();

        public Field StartOfDBConn
        {
            get { return _StartOfDBConn; }
            set { _StartOfDBConn = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _EndOfDBConn = new Field();

        public Field EndOfDBConn
        {
            get { return _EndOfDBConn; }
            set { _EndOfDBConn = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _PathInfo = new Field();

        public Field PathInfo
        {
            get { return _PathInfo; }
            set { _PathInfo = value; }
        }
                              }

}