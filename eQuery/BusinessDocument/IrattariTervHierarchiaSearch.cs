using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class IrattariTervHierarchiaSearch : BaseSearchObject
    {
        public enum Validity { Valid, Invalid, All };

        public IrattariTervHierarchiaSearch()
        {
            /// alapb�l kivessz�k az �rv�nyess�gre val� sz�r�st, hogy ha m�s sz�r�si felt�tel nincs megadva ezekn�l,
            /// ne gener�ljon semmit, hogy feleslegesen ne lass�tsa a lek�rdez�st

            ClearAllErvenyessegFields();

            _ErvKezd.Name = "ErvKezd";
            _ErvKezd.Type = "DateTime"; 
            _ErvKezd.Operator = Query.Operators.lessorequal;
            _ErvKezd.Value = "getdate()";
            _ErvVege.Name = "ErvVege";
            _ErvVege.Type = "DateTime"; 
            _ErvVege.Operator = Query.Operators.greaterorequal;
            _ErvVege.Value = "getdate()"; 
        }

        
        private EREC_AgazatiJelekSearch _EREC_AgazatiJelekSearch = new EREC_AgazatiJelekSearch();
        
        public EREC_AgazatiJelekSearch EREC_AgazatiJelekSearch
        {
            get { return _EREC_AgazatiJelekSearch; }
            set { _EREC_AgazatiJelekSearch = value; }        
        
        }

        private EREC_IraIrattariTetelekSearch _EREC_IraIrattariTetelekSearch = new EREC_IraIrattariTetelekSearch();

        public EREC_IraIrattariTetelekSearch EREC_IraIrattariTetelekSearch
        {
            get { return _EREC_IraIrattariTetelekSearch; }
            set { _EREC_IraIrattariTetelekSearch = value; }
        }

        private EREC_IratMetaDefinicioSearch _EREC_IratMetaDefinicioSearch = new EREC_IratMetaDefinicioSearch();

        public EREC_IratMetaDefinicioSearch EREC_IratMetaDefinicioSearch
        {
            get { return _EREC_IratMetaDefinicioSearch; }
            set { _EREC_IratMetaDefinicioSearch = value; }
        }


        #region Methods

        public void SetAllErvenyessegFields(Validity validity)
        {
            Field ervKezd = new Field();
            Field ervVege = new Field();

            if (validity == Validity.Valid)
            {
                // �rv�nyes
                ervKezd.Value = Query.SQLFunction.getdate;
                ervKezd.Operator = Query.Operators.lessorequal;
                ervKezd.Group = "100";
                ervKezd.GroupOperator = Query.Operators.and;

                ervVege.Value = Query.SQLFunction.getdate;
                ervVege.Operator = Query.Operators.greaterorequal;
                ervVege.Group = "100";
                ervVege.GroupOperator = Query.Operators.and;
            }
            else if (validity == Validity.Invalid)
            {
                // �rv�nytelen
                ervKezd.Value = Query.SQLFunction.getdate;
                ervKezd.Operator = Query.Operators.greaterorequal;
                ervKezd.Group = "100";
                ervKezd.GroupOperator = Query.Operators.or;

                ervVege.Value = Query.SQLFunction.getdate;
                ervVege.Operator = Query.Operators.lessorequal;
                ervVege.Group = "100";
                ervVege.GroupOperator = Query.Operators.or;
            }
            else if (validity == Validity.All)
            {
                // �sszes
                ervKezd.Value = "";
                ervKezd.Operator = "";
                ervKezd.GroupOperator = "";

                ervVege.Value = "";
                ervVege.Operator = "";
                ervVege.GroupOperator = "";
            }

            SetAllErvenyessegFields(ervKezd, ervVege);
        }

        public void SetAllErvenyessegFields(String ErvKezd, String ErvVege)
        {
            EREC_AgazatiJelekSearch.ErvKezd.Value = ErvKezd;
            EREC_AgazatiJelekSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            EREC_AgazatiJelekSearch.ErvKezd.Group = "100";
            EREC_AgazatiJelekSearch.ErvKezd.GroupOperator = Query.Operators.and;
            EREC_AgazatiJelekSearch.ErvVege.Value = ErvVege;
            EREC_AgazatiJelekSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            EREC_AgazatiJelekSearch.ErvVege.Group = "100";
            EREC_AgazatiJelekSearch.ErvVege.GroupOperator = Query.Operators.and;

            EREC_IraIrattariTetelekSearch.ErvKezd.Value = ErvKezd;
            EREC_IraIrattariTetelekSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            EREC_IraIrattariTetelekSearch.ErvKezd.Group = "100";
            EREC_IraIrattariTetelekSearch.ErvKezd.GroupOperator = Query.Operators.and;
            EREC_IraIrattariTetelekSearch.ErvVege.Value = ErvVege;
            EREC_IraIrattariTetelekSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            EREC_IraIrattariTetelekSearch.ErvVege.Group = "100";
            EREC_IraIrattariTetelekSearch.ErvVege.GroupOperator = Query.Operators.and;
 
            EREC_IratMetaDefinicioSearch.ErvKezd.Value = ErvKezd;
            EREC_IratMetaDefinicioSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            EREC_IratMetaDefinicioSearch.ErvKezd.Group = "100";
            EREC_IratMetaDefinicioSearch.ErvKezd.GroupOperator = Query.Operators.and;
            EREC_IratMetaDefinicioSearch.ErvVege.Value = ErvVege;
            EREC_IratMetaDefinicioSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            EREC_IratMetaDefinicioSearch.ErvVege.Group = "100";
            EREC_IratMetaDefinicioSearch.ErvVege.GroupOperator = Query.Operators.and;

        }

        public void SetAllErvenyessegFields(Field ErvKezd, Field ErvVege)
        {
            EREC_AgazatiJelekSearch.ErvKezd = ErvKezd;
            EREC_AgazatiJelekSearch.ErvVege = ErvVege;

            EREC_IraIrattariTetelekSearch.ErvKezd = ErvKezd;
            EREC_IraIrattariTetelekSearch.ErvVege = ErvVege;

            EREC_IratMetaDefinicioSearch.ErvKezd = ErvKezd;
            EREC_IratMetaDefinicioSearch.ErvVege = ErvVege;
        }

        public void ClearAllErvenyessegFields()
        {
            EREC_AgazatiJelekSearch.ErvKezd.Value = "";
            EREC_AgazatiJelekSearch.ErvKezd.Operator = "";
            EREC_AgazatiJelekSearch.ErvKezd.GroupOperator = "";
            EREC_AgazatiJelekSearch.ErvVege.Value = "";
            EREC_AgazatiJelekSearch.ErvVege.Operator = "";
            EREC_AgazatiJelekSearch.ErvVege.GroupOperator = "";

            EREC_IraIrattariTetelekSearch.ErvKezd.Value = "";
            EREC_IraIrattariTetelekSearch.ErvKezd.Operator = "";
            EREC_IraIrattariTetelekSearch.ErvKezd.GroupOperator = "";
            EREC_IraIrattariTetelekSearch.ErvVege.Value = "";
            EREC_IraIrattariTetelekSearch.ErvVege.Operator = "";
            EREC_IraIrattariTetelekSearch.ErvVege.GroupOperator = "";

            EREC_IratMetaDefinicioSearch.ErvKezd.Value = "";
            EREC_IratMetaDefinicioSearch.ErvKezd.Operator = "";
            EREC_IratMetaDefinicioSearch.ErvKezd.GroupOperator = "";
            EREC_IratMetaDefinicioSearch.ErvVege.Value = "";
            EREC_IratMetaDefinicioSearch.ErvVege.Operator = "";
            EREC_IratMetaDefinicioSearch.ErvVege.GroupOperator = "";
        }

        #endregion Methods

        private Field _ErvKezd = new Field();

        public Field ErvKezd
        {
            get { return _ErvKezd; }
            set { _ErvKezd = value; }
        }

        private Field _ErvVege = new Field();

        public Field ErvVege
        {
            get { return _ErvVege; }
            set { _ErvVege = value; }
        }

    }
}

