﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class KRT_DokumentumokSearch
    {
        private void Init_ManualFields()
        {
            _Manual_LetrehozasIdo.Name = "KRT_Dokumentumok.LetrehozasIdo";
            _Manual_LetrehozasIdo.Type = "DateTime";

            _Manual_Iktatokonyv_Ev.Name = "EREC_IraIktatokonyvek.Ev";
            _Manual_Iktatokonyv_Ev.Type = "Int32";

            _Manual_Iktatokonyv_Megkuljelzes.Name = "EREC_IraIktatoKonyvek.MegkulJelzes";
            _Manual_Iktatokonyv_Megkuljelzes.Type = "String";

            _Manual_Iktatokonyv_Iktatohely.Name = "EREC_IraIktatoKonyvek.Iktatohely";
            _Manual_Iktatokonyv_Iktatohely.Type = "String";

            _Manual_Ugyirat_Id.Name = "EREC_UgyUgyiratok.Id";
            _Manual_Ugyirat_Id.Type = "Guid";

            _Manual_Ugyirat_Foszam.Name = "EREC_UgyUgyiratok.Foszam";
            _Manual_Ugyirat_Foszam.Type = "Int32";

            _Manual_Irat_Id.Name = "EREC_IraIratok.Id";
            _Manual_Irat_Id.Type = "Guid";

            _Manual_Irat_Alszam.Name = "EREC_IraIratok.Alszam";
            _Manual_Irat_Alszam.Type = "Int32";

            _Manual_Erkeztetokonyv_Ev.Name = "Erkeztetokonyv.Ev";
            _Manual_Erkeztetokonyv_Ev.Type = "Int32";

            _Manual_Erkeztetokonyv_Megkuljelzes.Name = "Erkeztetokonyv.MegkulJelzes";
            _Manual_Erkeztetokonyv_Megkuljelzes.Type = "String";

            _Manual_Erkeztetokonyv_Iktatohely.Name = "Erkeztetokonyv.Iktatohely";
            _Manual_Erkeztetokonyv_Iktatohely.Type = "String";

            _Manual_Kuldemeny_Id.Name = "EREC_KuldKuldemenyek.Id";
            _Manual_Kuldemeny_Id.Type = "Guid";

            _Manual_Kuldemeny_ErkeztetoSzam.Name = "EREC_KuldKuldemenyek.Erkezteto_Szam";
            _Manual_Kuldemeny_ErkeztetoSzam.Type = "Int32";
            

            //// FIGYELEM, nincs mögötte oszlop, csak a szöveg elmentésére szolgál (a fájltartalomra keresésnél sharePoint-os ws-t hívunk)
            //// --> az Operator mezőt nem kell beállítani, és akkor nem generálja bele ezt a mezőt a Where feltételbe
            //_Manual_FajlTartalom.Name = "KRT_Dokumentumok.FajlTartalom";
            //_Manual_FajlTartalom.Type = "String";

            //// FIGYELEM, nincs mögötte oszlop (pontosabban: több is van mögötte), csak a szöveg elmentésére szolgál (a vonalkódos keresésnél a KRT_BarKodok/(EREC_PldIratPeldanyok/EREC_IraIratok) v. EREC_KuldKuldemenyek/EREC_Csatolmanyok/KRT_Dokumentumok felől is kell keresni)
            //// --> az Operator mezőt nem kell beállítani, és akkor nem generálja bele ezt a mezőt a Where feltételbe
            //_Manual_BarCodeFilter.Name = "KRT_BarKodok.Kod"; // vagy: "KRT_Dokumentumok.BarCode"
            //_Manual_BarCode.Type = "String";
        }

        private Field _Manual_LetrehozasIdo = new Field();
        public Field Manual_LetrehozasIdo
        {
            get { return _Manual_LetrehozasIdo; }
            set { _Manual_LetrehozasIdo = value; }
        }

        private Field _Manual_Iktatokonyv_Ev = new Field();
        public Field Manual_Iktatokonyv_Ev
        {
            get { return _Manual_Iktatokonyv_Ev; }
            set { _Manual_Iktatokonyv_Ev = value; }
        }

        private Field _Manual_Iktatokonyv_Megkuljelzes = new Field();
        public Field Manual_Iktatokonyv_Megkuljelzes
        {
            get { return _Manual_Iktatokonyv_Megkuljelzes; }
            set { _Manual_Iktatokonyv_Megkuljelzes = value; }
        }

        private Field _Manual_Iktatokonyv_Iktatohely = new Field();
        public Field Manual_Iktatokonyv_Iktatohely
        {
            get { return _Manual_Iktatokonyv_Iktatohely; }
            set { _Manual_Iktatokonyv_Iktatohely = value; }
        }

        private Field _Manual_Ugyirat_Id = new Field();
        public Field Manual_Ugyirat_Id
        {
            get { return _Manual_Ugyirat_Id; }
            set { _Manual_Ugyirat_Id = value; }
        }

        private Field _Manual_Ugyirat_Foszam = new Field();
        public Field Manual_Ugyirat_Foszam
        {
            get { return _Manual_Ugyirat_Foszam; }
            set { _Manual_Ugyirat_Foszam = value; }
        }

        private Field _Manual_Irat_Id = new Field();
        public Field Manual_Irat_Id
        {
            get { return _Manual_Irat_Id; }
            set { _Manual_Irat_Id = value; }
        }

        private Field _Manual_Irat_Alszam = new Field();
        public Field Manual_Irat_Alszam
        {
            get { return _Manual_Irat_Alszam; }
            set { _Manual_Irat_Alszam = value; }
        }


        private Field _Manual_Erkeztetokonyv_Ev = new Field();
        public Field Manual_Erkeztetokonyv_Ev
        {
            get { return _Manual_Erkeztetokonyv_Ev; }
            set { _Manual_Erkeztetokonyv_Ev = value; }
        }

        private Field _Manual_Erkeztetokonyv_Megkuljelzes = new Field();
        public Field Manual_Erkeztetokonyv_Megkuljelzes
        {
            get { return _Manual_Erkeztetokonyv_Megkuljelzes; }
            set { _Manual_Erkeztetokonyv_Megkuljelzes = value; }
        }

        private Field _Manual_Erkeztetokonyv_Iktatohely = new Field();
        public Field Manual_Erkeztetokonyv_Iktatohely
        {
            get { return _Manual_Erkeztetokonyv_Iktatohely; }
            set { _Manual_Erkeztetokonyv_Iktatohely = value; }
        }

        private Field _Manual_Kuldemeny_Id = new Field();
        public Field Manual_Kuldemeny_Id
        {
            get { return _Manual_Kuldemeny_Id; }
            set { _Manual_Kuldemeny_Id = value; }
        }

        private Field _Manual_Kuldemeny_ErkeztetoSzam = new Field();
        public Field Manual_Kuldemeny_ErkeztetoSzam
        {
            get { return _Manual_Kuldemeny_ErkeztetoSzam; }
            set { _Manual_Kuldemeny_ErkeztetoSzam = value; }
        }


        private FullTextSearchField _Manual_FajlTartalom = new FullTextSearchField();
        public FullTextSearchField Manual_FajlTartalom
        {
            get { return _Manual_FajlTartalom; }
            set { _Manual_FajlTartalom = value; }
        }

        // Szűrőmező az iratkezelési elemeken keresztüli integrált vonalkódos kereséshez
        private Field _Manual_BarCodeFilter = new Field();
        public Field Manual_BarCodeFilter
        {
            get { return _Manual_BarCodeFilter; }
            set { _Manual_BarCodeFilter = value; }
        }

        private FullTextSearchField _Fts_altalanos = new FullTextSearchField();
        // az ügyirat és irat tárgyában, valamint a beküldő nevében való közös keresési
        // kifejezés, ami VAGY kapcsolattal értendően szűri az ügyiratokat
        public FullTextSearchField Fts_altalanos
        {
            get { return _Fts_altalanos; }
            set { _Fts_altalanos = value; }
        }

        public Contentum.eQuery.FullTextSearch.FullTextSearchTree fts_tree_objektumtargyszavai = null;
        public Contentum.eQuery.FullTextSearch.FullTextSearchTree fts_tree_egyeb = null;

        // szűrőfeltétel az objektumok tárgyszavaiban lévő Obj_Id-ekre
        // pl. SELECT Obj_Id FROM EREC_ObjektumTargyszavai WHERE ...
        public string ObjektumTargyszavai_ObjIdFilter = "";

        private string _Partner_id;

        public string Partner_id
        {
            get
            {
                return _Partner_id;
            }
            set
            {
                _Partner_id = value;
            }
        }
    }
}
