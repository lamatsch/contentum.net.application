using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class KRT_EsemenyekSearch
    {
        public KRT_EsemenyekSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_KRT_ObjTipusokSearch = new KRT_ObjTipusokSearch();
                Extended_KRT_FunkciokSearch = new KRT_FunkciokSearch();
                Extended_KRT_HelyettesitesekSearch = new KRT_HelyettesitesekSearch();

                // objtipus ervkez,ervvege
                Extended_KRT_ObjTipusokSearch.ErvKezd.Clear();
                Extended_KRT_ObjTipusokSearch.ErvVege.Clear();

                // funkcio ervkez,ervvege
                Extended_KRT_FunkciokSearch.ErvKezd.Clear();
                Extended_KRT_FunkciokSearch.ErvVege.Clear();

                // helyettesitesek ervkez,ervvege
                Extended_KRT_HelyettesitesekSearch.ErvKezd.Clear();
                Extended_KRT_HelyettesitesekSearch.ErvVege.Clear();

                // l�trehoz�si id� field
                _LetrehozasIdo.Name = "KRT_Esemenyek.LetrehozasIdo";
                _LetrehozasIdo.Type = "DateTime";
            }
        }

        public KRT_ObjTipusokSearch Extended_KRT_ObjTipusokSearch;

        public KRT_FunkciokSearch Extended_KRT_FunkciokSearch;

        public KRT_HelyettesitesekSearch Extended_KRT_HelyettesitesekSearch;

        private Field _LetrehozasIdo = new Field();

        public Field LetrehozasIdo
        {
            get { return _LetrehozasIdo; }
            set { _LetrehozasIdo = value; }
        }
    }
}
