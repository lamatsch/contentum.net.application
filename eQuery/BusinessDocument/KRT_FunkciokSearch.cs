﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class KRT_FunkciokSearch
    {
        public KRT_FunkciokSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_KRT_ObjTipusokSearch = new KRT_ObjTipusokSearch();

                Extended_KRT_ObjTipusokSearch.ErvKezd.Clear();
                Extended_KRT_ObjTipusokSearch.ErvVege.Clear();
            }
        }

        public KRT_ObjTipusokSearch Extended_KRT_ObjTipusokSearch;
    }
}
