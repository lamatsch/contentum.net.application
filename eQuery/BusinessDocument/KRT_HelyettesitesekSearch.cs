
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// KRT_Helyettesitesek eQuery.BusinessDocument Class </summary>
    public partial class KRT_HelyettesitesekSearch
    {
        public ExtendedFilterClass ExtendedFilter = new ExtendedFilterClass();

        public class ExtendedFilterClass
        {
            // sz�r�st szab�lyoz� mez�k, GetAllWithExtension param�terez�s�re
            private string _CsoportTag_Id;

            public ExtendedFilterClass()
            {
                _CsoportTag_Id = "";
            }
            public string CsoportTag_Id
            {
                get { return _CsoportTag_Id; }
                set { _CsoportTag_Id = value; }
            }
        }

    }
}