using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    [Serializable()]
    public class KRT_LogSearch : BaseSearchObject
    {
        [NonSerialized]
        public KRT_Log_LoginSearch KRT_Log_LoginSearch = new KRT_Log_LoginSearch();
        [NonSerialized]
        public KRT_Log_PageSearch KRT_Log_PageSearch = new KRT_Log_PageSearch();
        [NonSerialized]
        public KRT_Log_WebServiceSearch KRT_Log_WebServiceSearch = new KRT_Log_WebServiceSearch();
        [NonSerialized]
        public KRT_Log_StoredProcedureSearch KRT_Log_StoredProcedureSearch = new KRT_Log_StoredProcedureSearch();

        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        public KRT_LogSearch()
        {
        }

        public KRT_LogSearch GetPageSearch()
        {
            KRT_LogSearch result = new KRT_LogSearch();
            result.KRT_Log_LoginSearch = null;
            result.KRT_Log_PageSearch = this.KRT_Log_PageSearch;
            result.KRT_Log_WebServiceSearch = this.KRT_Log_WebServiceSearch;
            result.KRT_Log_StoredProcedureSearch = this.KRT_Log_StoredProcedureSearch;
            return result;
        }

        public KRT_LogSearch GetWebServiceSearch()
        {
            KRT_LogSearch result = new KRT_LogSearch();
            result.KRT_Log_LoginSearch = null;
            result.KRT_Log_PageSearch = null;
            result.KRT_Log_WebServiceSearch = this.KRT_Log_WebServiceSearch;
            result.KRT_Log_StoredProcedureSearch = this.KRT_Log_StoredProcedureSearch;
            return result;
        }

        public KRT_LogSearch GetStoredProcedureSearch()
        {
            KRT_LogSearch result = new KRT_LogSearch();
            result.KRT_Log_LoginSearch = null;
            result.KRT_Log_PageSearch = null;
            result.KRT_Log_WebServiceSearch = null;
            result.KRT_Log_StoredProcedureSearch = this.KRT_Log_StoredProcedureSearch;
            return result;
        }

        public string Where_Log_Login
        {
            get
            {
                if (this.KRT_Log_LoginSearch != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(this.KRT_Log_LoginSearch);
                    return query.Where;
                }

                return String.Empty;
            }
        }

        public string Where_Log_Page
        {
            get
            {
                if (this.KRT_Log_PageSearch != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(this.KRT_Log_PageSearch);
                    return query.Where;
                }

                return String.Empty;
            }
        }

        public string Where_Log_WebService
        {
            get
            {
                if (this.KRT_Log_WebServiceSearch != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(this.KRT_Log_WebServiceSearch);
                    return query.Where;
                }

                return String.Empty;
            }
        }

        public string Where_Log_StoredProcedure
        {
            get
            {
                if (this.KRT_Log_StoredProcedureSearch != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(this.KRT_Log_StoredProcedureSearch);
                    return query.Where;
                }

                return String.Empty;
            }
        }

        public bool ShowStart = false;
    }
}
