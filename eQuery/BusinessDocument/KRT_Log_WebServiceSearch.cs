﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class KRT_Log_WebServiceSearch
    {
        private void Init_ManualFields()
        {
            _Tranz_id.Name = "KRT_Log_WebService.Tranz_id";
            _Tranz_id.Type = "Guid"; 
        }

        private Field _Tranz_id = new Field();

        public Field Tranz_id
        {
            get { return _Tranz_id; }
            set { _Tranz_id = value; }
        }
    }
}
