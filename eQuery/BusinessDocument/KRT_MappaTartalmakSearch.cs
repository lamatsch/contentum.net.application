using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class KRT_MappaTartalmakSearch
    {
        public KRT_MappaTartalmakSearch(bool createExtendedSearchObjects)
            : this()
        {
            if (createExtendedSearchObjects)
            {
                Extended_KRT_DokumentumokSearch_Tartalom = new KRT_DokumentumokSearch();
                Extended_KRT_MappakSearch_Szulo = new KRT_MappakSearch();
                Extended_KRT_MappakSearch_Tartalom = new KRT_MappakSearch();

                /// alapb�l kivessz�k az �rv�nyess�gre val� sz�r�st, hogy ha m�s sz�r�si felt�tel nincs megadva ezekn�l,
                /// ne gener�ljon semmit, hogy feleslegesen ne lass�tsa a lek�rdez�st
                /// 

                Extended_KRT_DokumentumokSearch_Tartalom.ErvKezd.Clear();
                Extended_KRT_DokumentumokSearch_Tartalom.ErvVege.Clear();

                Extended_KRT_MappakSearch_Szulo.ErvKezd.Clear();
                Extended_KRT_MappakSearch_Szulo.ErvVege.Clear();

                Extended_KRT_MappakSearch_Tartalom.ErvKezd.Clear();
                Extended_KRT_MappakSearch_Tartalom.ErvVege.Clear();

            }
        }

        public KRT_MappakSearch Extended_KRT_MappakSearch_Szulo;

        public KRT_MappakSearch Extended_KRT_MappakSearch_Tartalom;

        public KRT_DokumentumokSearch Extended_KRT_DokumentumokSearch_Tartalom;

        private void Init_ManualFields()
        {

        }
    }
}
