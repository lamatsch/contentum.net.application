﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class KRT_NezetekSearch
    {
        private void Init_ManualFields()
        {
        }

        public KRT_NezetekSearch(bool createExtendedSearchObjects)
            : this()
        {
            /// létrehozzuk a belső, más táblára vonatkozó keresési objektumokat, ha 
            /// createExtendedSearchObjects==true, egyébként null értéken hagyjuk            
            if (createExtendedSearchObjects)
            {
                Extended_KRT_ModulokSearch = new KRT_ModulokSearch();
                Extended_KRT_ModulokSearch.ErvKezd.Clear();
                Extended_KRT_ModulokSearch.ErvVege.Clear();

                Extended_KRT_MuveletekSearch = new KRT_MuveletekSearch();
                Extended_KRT_MuveletekSearch.ErvKezd.Clear();
                Extended_KRT_MuveletekSearch.ErvVege.Clear();
            }
        }

        [Contentum.eQuery.Base.AddToWhere]
        public KRT_ModulokSearch Extended_KRT_ModulokSearch;

        [Contentum.eQuery.Base.AddToWhere]
        public KRT_MuveletekSearch Extended_KRT_MuveletekSearch;
    }
}
