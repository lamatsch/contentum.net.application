using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class KRT_PartnerekSearch
    {
        [NonSerialized]
        public KRT_SzemelyekSearch SzemelyekSearch = new KRT_SzemelyekSearch();

        [NonSerialized]
        public KRT_VallalkozasokSearch VallalkozasokSearch = new KRT_VallalkozasokSearch();

        private void Init_ManualFields()
        {
            //�rv�nyess�g figyelmen kiv�l hagy�sa
            this.SzemelyekSearch.ErvKezd.Clear();
            this.SzemelyekSearch.ErvVege.Clear();

            this.VallalkozasokSearch.ErvKezd.Clear();
            this.VallalkozasokSearch.ErvVege.Clear();

            #region partnerek l�trehoz�s�ra keres�s
            _LetrehozasKezd.Name = "KRT_Partnerek.LetrehozasIdo";
            _LetrehozasKezd.Type = "DateTime"; _ErvKezd.Operator = Query.Operators.lessorequal;

            _LetrehozasVege.Name = "KRT_Partnerek.LetrehozasIdo";
            _LetrehozasVege.Type = "DateTime"; _ErvVege.Operator = Query.Operators.greaterorequal;

            #endregion
        }

        #region partnerek l�trehoz�s keres�s
        /// <summary>
        /// Letrehozas property
        /// Letrehozas kezdete (�ltal�ban d�tum!) 
        /// </summary>
        private Field _LetrehozasKezd = new Field();

        public Field LetrehozasKezd
        {
            get { return _LetrehozasKezd; }
            set { _LetrehozasKezd = value; }
        }


        /// <summary>
        /// LetrehozasVege property
        /// Letrehozas  v�ge (�ltal�ban d�tum!)
        /// </summary>
        private Field _LetrehozasVege = new Field();

        public Field LetrehozasVege
        {
            get { return _LetrehozasVege; }
            set { _LetrehozasVege = value; }
        }
        #endregion
    }
}
