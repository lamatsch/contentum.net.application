using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eQuery.BusinessDocuments
{
    public partial class MetaAdatHierarchiaSearch : BaseSearchObject
    {
        public enum Validity { Valid, Invalid, All };

        public MetaAdatHierarchiaSearch()
        {
            /// alapb�l kivessz�k az �rv�nyess�gre val� sz�r�st, hogy ha m�s sz�r�si felt�tel nincs megadva ezekn�l,
            /// ne gener�ljon semmit, hogy feleslegesen ne lass�tsa a lek�rdez�st

            ClearAllErvenyessegFields();
        }

        
        private EREC_AgazatiJelekSearch _EREC_AgazatiJelekSearch = new EREC_AgazatiJelekSearch();
        
        public EREC_AgazatiJelekSearch EREC_AgazatiJelekSearch
        {
            get { return _EREC_AgazatiJelekSearch; }
            set { _EREC_AgazatiJelekSearch = value; }        
        
        }

        private EREC_IraIrattariTetelekSearch _EREC_IraIrattariTetelekSearch = new EREC_IraIrattariTetelekSearch();

        public EREC_IraIrattariTetelekSearch EREC_IraIrattariTetelekSearch
        {
            get { return _EREC_IraIrattariTetelekSearch; }
            set { _EREC_IraIrattariTetelekSearch = value; }
        }

        private EREC_IratMetaDefinicioSearch _EREC_IratMetaDefinicioSearch = new EREC_IratMetaDefinicioSearch();

        public EREC_IratMetaDefinicioSearch EREC_IratMetaDefinicioSearch
        {
            get { return _EREC_IratMetaDefinicioSearch; }
            set { _EREC_IratMetaDefinicioSearch = value; }
        }

        private EREC_Obj_MetaDefinicioSearch _EREC_Obj_MetaDefinicioSearch = new EREC_Obj_MetaDefinicioSearch();

        public EREC_Obj_MetaDefinicioSearch EREC_Obj_MetaDefinicioSearch
        {
            get { return _EREC_Obj_MetaDefinicioSearch; }
            set { _EREC_Obj_MetaDefinicioSearch = value; }
        }

        private EREC_Obj_MetaAdataiSearch _EREC_Obj_MetaAdataiSearch = new EREC_Obj_MetaAdataiSearch();

        public EREC_Obj_MetaAdataiSearch EREC_Obj_MetaAdataiSearch
        {
            get { return _EREC_Obj_MetaAdataiSearch; }
            set { _EREC_Obj_MetaAdataiSearch = value; }
        }

        private EREC_TargySzavakSearch _EREC_TargySzavakSearch = new EREC_TargySzavakSearch();

        public EREC_TargySzavakSearch EREC_TargySzavakSearch
        {
            get { return _EREC_TargySzavakSearch; }
            set { _EREC_TargySzavakSearch = value; }
        }

        #region Methods

        public void SetAllErvenyessegFields(Validity validity)
        {
            Field ervKezd = new Field();
            Field ervVege = new Field();

            if (validity == Validity.Valid)
            {
                // �rv�nyes
                ervKezd.Value = Query.SQLFunction.getdate;
                ervKezd.Operator = Query.Operators.lessorequal;
                ervKezd.Group = "100";
                ervKezd.GroupOperator = Query.Operators.and;

                ervVege.Value = Query.SQLFunction.getdate;
                ervVege.Operator = Query.Operators.greaterorequal;
                ervVege.Group = "100";
                ervVege.GroupOperator = Query.Operators.and;
            }
            else if (validity == Validity.Invalid)
            {
                // �rv�nytelen
                ervKezd.Value = Query.SQLFunction.getdate;
                ervKezd.Operator = Query.Operators.greaterorequal;
                ervKezd.Group = "100";
                ervKezd.GroupOperator = Query.Operators.or;

                ervVege.Value = Query.SQLFunction.getdate;
                ervVege.Operator = Query.Operators.lessorequal;
                ervVege.Group = "100";
                ervVege.GroupOperator = Query.Operators.or;
            }
            else if (validity == Validity.All)
            {
                // �sszes
                ervKezd.Clear();

                ervVege.Clear();
            }

            SetAllErvenyessegFields(ervKezd, ervVege);
        }

        public void SetAllErvenyessegFields(String ErvKezd, String ErvVege)
        {
            EREC_AgazatiJelekSearch.ErvKezd.Value = ErvKezd;
            EREC_AgazatiJelekSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            EREC_AgazatiJelekSearch.ErvKezd.Group = "100";
            EREC_AgazatiJelekSearch.ErvKezd.GroupOperator = Query.Operators.and;
            EREC_AgazatiJelekSearch.ErvVege.Value = ErvVege;
            EREC_AgazatiJelekSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            EREC_AgazatiJelekSearch.ErvVege.Group = "100";
            EREC_AgazatiJelekSearch.ErvVege.GroupOperator = Query.Operators.and;

            EREC_IraIrattariTetelekSearch.ErvKezd.Value = ErvKezd;
            EREC_IraIrattariTetelekSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            EREC_IraIrattariTetelekSearch.ErvKezd.Group = "100";
            EREC_IraIrattariTetelekSearch.ErvKezd.GroupOperator = Query.Operators.and;
            EREC_IraIrattariTetelekSearch.ErvVege.Value = ErvVege;
            EREC_IraIrattariTetelekSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            EREC_IraIrattariTetelekSearch.ErvVege.Group = "100";
            EREC_IraIrattariTetelekSearch.ErvVege.GroupOperator = Query.Operators.and;
 
            EREC_IratMetaDefinicioSearch.ErvKezd.Value = ErvKezd;
            EREC_IratMetaDefinicioSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            EREC_IratMetaDefinicioSearch.ErvKezd.Group = "100";
            EREC_IratMetaDefinicioSearch.ErvKezd.GroupOperator = Query.Operators.and;
            EREC_IratMetaDefinicioSearch.ErvVege.Value = ErvVege;
            EREC_IratMetaDefinicioSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            EREC_IratMetaDefinicioSearch.ErvVege.Group = "100";
            EREC_IratMetaDefinicioSearch.ErvVege.GroupOperator = Query.Operators.and;

            EREC_Obj_MetaDefinicioSearch.ErvKezd.Value = ErvKezd;
            EREC_Obj_MetaDefinicioSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            EREC_Obj_MetaDefinicioSearch.ErvKezd.Group = "100";
            EREC_Obj_MetaDefinicioSearch.ErvKezd.GroupOperator = Query.Operators.and;
            EREC_Obj_MetaDefinicioSearch.ErvVege.Value = ErvVege;
            EREC_Obj_MetaDefinicioSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            EREC_Obj_MetaDefinicioSearch.ErvVege.Group = "100";
            EREC_Obj_MetaDefinicioSearch.ErvVege.GroupOperator = Query.Operators.and;

            EREC_Obj_MetaAdataiSearch.ErvKezd.Value = ErvKezd;
            EREC_Obj_MetaAdataiSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            EREC_Obj_MetaAdataiSearch.ErvKezd.Group = "100";
            EREC_Obj_MetaAdataiSearch.ErvKezd.GroupOperator = Query.Operators.and;
            EREC_Obj_MetaAdataiSearch.ErvVege.Value = ErvVege;
            EREC_Obj_MetaAdataiSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            EREC_Obj_MetaAdataiSearch.ErvVege.Group = "100";
            EREC_Obj_MetaAdataiSearch.ErvVege.GroupOperator = Query.Operators.and;

            EREC_TargySzavakSearch.ErvKezd.Value = ErvKezd;
            EREC_TargySzavakSearch.ErvKezd.Operator = Query.Operators.lessorequal;
            EREC_TargySzavakSearch.ErvKezd.Group = "100";
            EREC_TargySzavakSearch.ErvKezd.GroupOperator = Query.Operators.and;
            EREC_TargySzavakSearch.ErvVege.Value = ErvVege;
            EREC_TargySzavakSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            EREC_TargySzavakSearch.ErvVege.Group = "100";
            EREC_TargySzavakSearch.ErvVege.GroupOperator = Query.Operators.and;
        }

        public void SetAllErvenyessegFields(Field ErvKezd, Field ErvVege)
        {
            EREC_AgazatiJelekSearch.ErvKezd = ErvKezd;
            EREC_AgazatiJelekSearch.ErvVege = ErvVege;

            EREC_IraIrattariTetelekSearch.ErvKezd = ErvKezd;
            EREC_IraIrattariTetelekSearch.ErvVege = ErvVege;

            EREC_IratMetaDefinicioSearch.ErvKezd = ErvKezd;
            EREC_IratMetaDefinicioSearch.ErvVege = ErvVege;

            EREC_Obj_MetaDefinicioSearch.ErvKezd = ErvKezd;
            EREC_Obj_MetaDefinicioSearch.ErvVege = ErvVege;

            EREC_Obj_MetaAdataiSearch.ErvKezd = ErvKezd;
            EREC_Obj_MetaAdataiSearch.ErvVege = ErvVege;

            EREC_TargySzavakSearch.ErvKezd = ErvKezd;
            EREC_TargySzavakSearch.ErvVege = ErvVege;
        }

        public void ClearAllErvenyessegFields()
        {
            EREC_AgazatiJelekSearch.ErvKezd.Clear();
            EREC_AgazatiJelekSearch.ErvVege.Clear();

            EREC_IraIrattariTetelekSearch.ErvKezd.Clear();
            EREC_IraIrattariTetelekSearch.ErvVege.Clear();

            EREC_IratMetaDefinicioSearch.ErvKezd.Clear();
            EREC_IratMetaDefinicioSearch.ErvVege.Clear();

            EREC_Obj_MetaDefinicioSearch.ErvKezd.Clear();
            EREC_Obj_MetaDefinicioSearch.ErvVege.Clear();

            EREC_Obj_MetaAdataiSearch.ErvKezd.Clear();
            EREC_Obj_MetaAdataiSearch.ErvVege.Clear();

            EREC_TargySzavakSearch.ErvKezd.Clear();
            EREC_TargySzavakSearch.ErvVege.Clear();
        }

        #endregion Methods

    }
}

