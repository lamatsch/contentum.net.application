using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// sorjogosultság listázásának feltételei
    /// </summary>
    [Serializable()]
    public class RowRightsCsoportSearch
    {
        public RowRightsCsoportSearch()
        {

            _Id.Name = "KRT_Jogosultak.Id";
            _Id.Type = "Guid";
            _Nev.Name = "KRT_Csoportok.Nev";
            _Nev.Type = "String";
            _LetrehozasIdo.Name = "KRT_Jogosultak.LetrehozasIdo";
            _LetrehozasIdo.Type = "DateTime";
            _LetrehozasIdo.Value = "getdate()";
        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;

        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }


        /// <summary>
        /// LetrehozasIdo property
        /// </summary>
        private Field _LetrehozasIdo = new Field();

        public Field LetrehozasIdo
        {
            get { return _LetrehozasIdo; }
            set { _LetrehozasIdo = value; }
        }

    }

}