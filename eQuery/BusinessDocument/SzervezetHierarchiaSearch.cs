
using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Contentum.eQuery.BusinessDocuments
{
    /// <summary>
    /// SzervezetHierarchia eQuery.BusinessDocument Class </summary>
    [Serializable()]    
        public class SzervezetHierarchiaSearch    {
        public SzervezetHierarchiaSearch()
        {            
        }
        public String WhereByManual = "";
        public String OrderBy = "";
        public Int32 TopRow = 0;
        
         private void Init_ManualFields()
        {
            Id.Name = "Id";
            Id.Type = "Guid";

            Nev.Name = "Nev";
            Nev.Type = "String";

            Tipus.Name = "Tipus";
            Tipus.Type = "String";

            Szint.Name = "Szint";
            Szint.Type = "String";

        }
                                      
        /// <summary>
        /// Id property </summary>
        private Field _Id = new Field();

        public Field Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Nev = new Field();

        public Field Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }
           
        /// <summary>
        /// Id property </summary>
        private Field _Tipus = new Field();

        public Field Tipus
        {
            get { return _Tipus; }
            set { _Tipus = value; }
        }
                          
           
        /// <summary>
        /// Id property </summary>
        private Field _Szint = new Field();

        public Field Szint
        {
            get { return _Szint; }
            set { _Szint = value; }
        }
                          

     }

}