using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eQuery.BusinessDocuments;
using System.Reflection;
using Contentum.eQuery.Base;

[assembly: CLSCompliant(true)]
namespace Contentum.eQuery
{ 
    public class Query
    {
        public void BuildFromBusinessDocument(object BusinessDocument)
        {
            if (BusinessDocument != null)
            {
                try
                {
                    Group maingroup = MainGroup();

                    Query.Group g = maingroup.AddGroup(Query.Operators.and);

                    Dictionary<String, Group> Gr = new Dictionary<String, Group>();
                    Query.Group _g = null;

                    System.Reflection.PropertyInfo[] BusinessDocumentProperties = BusinessDocument.GetType().GetProperties();

                    foreach (System.Reflection.PropertyInfo P in BusinessDocumentProperties)
                    {
                        if (P != null && P.PropertyType == typeof(Field))
                        {
                            System.Reflection.PropertyInfo BusinessDocumentProperty = BusinessDocument.GetType().GetProperty(P.Name);

                            object FieldObject = BusinessDocumentProperty.GetValue(BusinessDocument, null);
                            System.Reflection.PropertyInfo[] FieldProperties = FieldObject.GetType().GetProperties();

                            System.Reflection.PropertyInfo FieldProperty = null;
                            FieldProperty = FieldObject.GetType().GetProperty("Name");
                            string _name = (FieldProperty.GetValue(FieldObject, null) as String);
                            FieldProperty = FieldObject.GetType().GetProperty("Operator");
                            string _operator = (FieldProperty.GetValue(FieldObject, null) as String);
                            FieldProperty = FieldObject.GetType().GetProperty("Type");
                            string _type = (FieldProperty.GetValue(FieldObject, null) as String);

                            FieldProperty = FieldObject.GetType().GetProperty("Value");
                            string _value = (FieldProperty.GetValue(FieldObject, null) as string);

                            FieldProperty = FieldObject.GetType().GetProperty("ValueTo");
                            string _valueto = (FieldProperty.GetValue(FieldObject, null) as string);

                            FieldProperty = FieldObject.GetType().GetProperty("Group");
                            string _group = (FieldProperty.GetValue(FieldObject, null) as String);
                            FieldProperty = FieldObject.GetType().GetProperty("GroupOperator");
                            string _groupoperator = (FieldProperty.GetValue(FieldObject, null) as String);

                            if (!String.IsNullOrEmpty(_operator))
                            {

                                if (!Gr.ContainsKey(_group))
                                {
                                    _g = g.AddGroup(_groupoperator);
                                    Gr.Add(_group, _g);
                                }
                                else
                                {
                                    Gr.TryGetValue(_group, out _g);
                                }

                                if (!String.IsNullOrEmpty(_operator))
                                {
                                    if (_operator == Operators.isnullorequals)
                                    {
                                        Group newGroup = _g.AddGroup(Operators.or);
                                        _g = newGroup;
                                        _g.AddExpression(_name, Operators.isnull, String.Empty, String.Empty, _type);
                                        _operator = Query.Operators.equals;
                                    }
                                    else if (_operator == Operators.isnullornotequals)
                                    {
                                        Group newGroup = _g.AddGroup(Operators.or);
                                        _g = newGroup;
                                        _g.AddExpression(_name, Operators.isnull, String.Empty, String.Empty, _type);
                                        _operator = Query.Operators.notequals;
                                    }
                                    else if (_operator == Operators.isnullorinner)
                                    {
                                        Group newGroup = _g.AddGroup(Operators.or);
                                        _g = newGroup;
                                        _g.AddExpression(_name, Operators.isnull, String.Empty, String.Empty, _type);
                                        _operator = Query.Operators.inner;
                                    }
                                    else if (_operator == Operators.isnullornotinner)
                                    {
                                        Group newGroup = _g.AddGroup(Operators.or);
                                        _g = newGroup;
                                        _g.AddExpression(_name, Operators.isnull, String.Empty, String.Empty, _type);
                                        _operator = Query.Operators.notinner;
                                    }

                                    _g.AddExpression(_name, _operator, _value, _valueto, _type);
                                }
                            }
                        }
                    }

                    Build(maingroup);
                }
                catch
                { }

                // loggol�s, ha voltak hib�s vagy hibagyan�s felt�telek
                _ParseFailedConditions.LogByLevelIfNotEmpty(_Where);
            }
        }

        public void BuildFromBusinessDocument(object BusinessDocument, bool AddExtendedSearchWhere)
        {
            if (BusinessDocument != null)
            {
                this.BuildFromBusinessDocument(BusinessDocument);
                Type t = BusinessDocument.GetType();
                foreach (FieldInfo fi in t.GetFields())
                {
                    object[] attributes = fi.GetCustomAttributes(typeof(AddToWhereAttribute), false);
                    if (attributes.Length > 0)
                    {
                        AddToWhereAttribute attrAddToWhere = attributes[0] as AddToWhereAttribute;
                        if (attrAddToWhere != null && attrAddToWhere.AddToWhere)
                        {
                            object extendedBusinessDocument = fi.GetValue(BusinessDocument);
                            if (extendedBusinessDocument != null)
                            {
                                Query q = new Query();
                                q.BuildFromBusinessDocument(extendedBusinessDocument);
                                string where = q.Where;
                                FieldInfo fiWhereByManual = extendedBusinessDocument.GetType().GetField("WhereByManual");
                                if (fiWhereByManual != null)
                                {
                                    string wherebymanual = fiWhereByManual.GetValue(extendedBusinessDocument).ToString();
                                    if (!String.IsNullOrEmpty(wherebymanual))
                                    {
                                        if (!String.IsNullOrEmpty(where))
                                            where += " and ";

                                        where += wherebymanual;
                                    }
                                }
                                if (!String.IsNullOrEmpty(where))
                                {
                                    if (!String.IsNullOrEmpty(this._Where))
                                    {
                                        this._Where += " and ";
                                    }

                                    this._Where += where;
                                }
                            }
                        }
                    }
                }
            }
        }

        private string _Where = "";

        public string Where
        {
            get {

                if (!String.IsNullOrEmpty(_Where))
                {
                    if (String.IsNullOrEmpty(System.Text.RegularExpressions.Regex.Replace(_Where, @"[() ]", String.Empty)))
                    {
                        _ParseFailedConditions.AddError(_Where);
                        return "";
                    }
                    else
                    {
                        return String.Format("({0})",_Where);
                    }
                }
                return _Where;
            }
            set { _Where = value; }
        }

        // documentates, if a condition has been removed because of its incorrect syntax
        private class ParseFailedConditions
        {
            private class ConditionElement
            {
                private string _condition;

                public string Condition
                {
                    get { return this._condition; }
                    set { this._condition = value; }
                }

                private log4net.Core.Level _level;
                public log4net.Core.Level Level
                {
                    get { return this._level; }
                    set { this._level = value; }
                }

                public ConditionElement(string condition, log4net.Core.Level level)
                {
                    this._condition = condition;
                    this._level = level;
                }
            }

            List<ConditionElement> ConditionList = new List<ConditionElement>();
            public void AddError(string Condition)
            {
                this.Add(Condition, log4net.Core.Level.Error);
            }

            public void AddWarning(string Condition)
            {
                this.Add(Condition, log4net.Core.Level.Warn);
            }

            public void Add(string Condition, log4net.Core.Level Level)
            {
                ConditionList.Add(new ConditionElement(Condition, Level));
            }

            public void Clear()
            {
                ConditionList.Clear();
            }

            public override string ToString()
            {
                int c = 0;
                StringBuilder sbConcatenated = new StringBuilder();
                foreach (ConditionElement item in ConditionList)
                {
                    c++;
                    sbConcatenated.AppendFormat("[{0}][{1}]{2}", c.ToString(), item.Level.DisplayName, item.Condition);
                }
                return sbConcatenated.ToString();
            }

            public void LogByLevelIfNotEmpty()
            {
                LogByLevelIfNotEmpty(null);
            }

            public void LogByLevelIfNotEmpty(string where)
            {
                if (ConditionList.Count > 0)
                {
                    StringBuilder sbConcatenated = new StringBuilder();
                    sbConcatenated.Append("Kiemelt elemek: ");
                    int c = 0;
                    foreach (ConditionElement item in ConditionList)
                    {
                        c++;
                        sbConcatenated.AppendFormat("[{0}][{1}]{2}", c.ToString(), item.Level.DisplayName, item.Condition);
                    }

                    if (ConditionList.Exists(delegate (ConditionElement item) {return item.Level >= log4net.Core.Level.Error;}))
                    {
                        Contentum.eQuery.Utility.Logger.Error(String.Format("Hib�s form�tum� keres�si felt�tel: {0}", where), "[eQuery.BuildFromBusinessDocument]", null);
                        Contentum.eQuery.Utility.Logger.Error(sbConcatenated.ToString(), "[eQuery.BuildFromBusinessDocument]", null);
                    }
                    else
                    {
                        Contentum.eQuery.Utility.Logger.Warn(String.Format("Keres�si felt�tel: {0}", where), "[eQuery.BuildFromBusinessDocument]", null);
                        Contentum.eQuery.Utility.Logger.Warn(sbConcatenated.ToString(), "[eQuery.BuildFromBusinessDocument]", null);
                    }
                }
            }

        }

        private ParseFailedConditions _ParseFailedConditions = new ParseFailedConditions();

        public string GetParseFailedConditions
        {
            get { return _ParseFailedConditions.ToString(); }
        }

        private string SetItem(Group g)
        {
            //StringBuilder sbWhere = new StringBuilder();

            if (g.expressions == null)
                return String.Empty;
            int rec = 0;
            while (rec < g.expressions.Count)
            {
                // kitorli azokat, ahol nincsen feltetel!!!
                if (String.IsNullOrEmpty(g.expressions[rec].Operator))
                {
                    g.expressions.RemoveAt(rec);
                }
                else if (g.expressions[rec].Value.ToString() == "*")
                {
                    g.expressions.RemoveAt(rec);
                }
                else
                {
                    rec++;
                }
            }

            List<string> SqlLocalConditions = new List<string>();
            for (int x = 0; x < g.expressions.Count; x++)
            {
                Expression expression = g.expressions[x];
                //if (expression.Value.ToString() == "*")
                //{
                //    continue;
                //}
                // _Where += "(";
                bool bParseOK = expression.Check();
                string localCondition = "";

                string _operator = expression.Operator;

                string _value = expression.Value.ToString();
                string _valueto = expression.ValueTo.ToString();
                string _valuestartend = "'";
                switch (expression.Operator)
                {
                    case Operators.isnull:
                        goto case Operators.notnull;
                    case Operators.notnull:
                        _value = "";
                        _valuestartend = "";
                        break;
                }

                switch (expression.Type)
                {
                    case "FTSString":
                        if (expression.Operator == Operators.contains && !String.IsNullOrEmpty(_value))
                        {
                            string sqlContains = new FullTextSearch.SQLContainsCondition(_value).Normalized;

                            if (!String.IsNullOrEmpty(sqlContains))
                            {
                                localCondition += " " + String.Format(_operator, expression.Field, "'" + sqlContains + "'") + " ";
                            }
                        }
                        else
                        {
                            goto case "String";
                        }
                        break;
                    case "Char":
                        goto case "String";
                    case "Guid":
                        goto case "String";
                    case "String":
                        {
                            string _likestart = "";
                            string _likeend = "";
                            string _innerstart = "";
                            string _innerend = "";
                            string _unicode = "";

                            switch (expression.Operator)
                            {
                                case Operators.like:
                                    _value = _value.Replace('*', '%');
                                    break;
                                case Operators.likefull:
                                    _likeend = "%";
                                    goto case Operators.likestart;

                                case Operators.likestart:
                                    _likestart = "%";
                                    _operator = "like";
                                    break;
                                case Operators.notinner:
                                    goto case Operators.inner;
                                case Operators.inner:
                                    _valuestartend = "";
                                    _innerstart = "(";
                                    _innerend = ")";
                                    break;
                            }

                            // aposztr�fot lecser�lj�k k�t aposztr�fra, k�l�nben incorrect syntax lesz
                            // (ahol a _value el� �s m�g� aposztr�fot akarunk tenni)
                            if (_valuestartend == "'")
                            {
                                _value = _value.Replace("'", "''");
                            }

                            if (expression.Type == "String" && _valuestartend == "'")
                            {
                                _unicode = "N";
                            }

                            localCondition += expression.Field + " " + _operator + " ";
                            localCondition += _innerstart + _unicode + _valuestartend + _likestart + _value + _likeend + _valuestartend + _innerend;
                            if (_operator == Operators.between)
                            {
                                localCondition += " and ";
                                localCondition += _innerstart + _unicode + _valuestartend + _likestart + _valueto + _likeend + _valuestartend + _innerend;
                            }
                        }
                        break;
                    case "Int32":
                        {
                            string _likestart = "";
                            string _likeend = "";

                            switch (expression.Operator)
                            {
                                case Operators.like:
                                    _value = _value.Replace('*', '%');
                                    localCondition += expression.Field + " " + _operator + " " + _valuestartend + _value + _valuestartend;
                                    break;
                                case Operators.likefull:
                                    _likeend = "%";
                                    goto case Operators.likestart;
                                case Operators.likestart:
                                    _likestart = "%";
                                    _operator = "like";
                                    localCondition += expression.Field + " " + _operator + " " + _valuestartend + _value + _valuestartend;
                                    break;
                                case Operators.notinner:
                                    goto case Operators.inner;
                                case Operators.inner:
                                    localCondition += expression.Field + " " + _operator + " ";
                                    localCondition += "(" + _value + ")";
                                    break;
                                default:
                                    localCondition += expression.Field + " " + _operator + " ";
                                    localCondition += _value;
                                    break;
                            }

                            if (_operator == Operators.between)
                            {
                                localCondition += " and ";
                                localCondition += _valueto;
                            }
                        }
                        break;
                    case "Double":
                        {
                            string _likestart = "";
                            string _likeend = "";

                            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                            nfi.NumberDecimalSeparator = ".";

                            System.Globalization.NumberStyles ns = System.Globalization.NumberStyles.Float;

                            // konverzi� ponttal elv�lasztott form�tumm�
                            if (expression.Operator == Operators.inner || expression.Operator == Operators.notinner)
                            {
                                string[] items = _value.Replace(" ", "").Split(new char[] { ',' });
                                if (items.Length > 0)
                                {
                                    string[] parsedItems = new string[items.Length];
                                    int i = 0;
                                    Double dParse;
                                    foreach (string item in items)
                                    {
                                        
                                        //dParse = Double.Parse(item);
                                        if (Double.TryParse(item, out dParse))
                                        {
                                            parsedItems[i] = dParse.ToString(nfi);
                                        }
                                        else
                                        {
                                            parsedItems[i] = item;
                                        }

                                        i++;
                                    }

                                    _value = String.Join(",", parsedItems);
                                }
                            }
                            else if (expression.Operator == Operators.like || expression.Operator == Operators.likestart || expression.Operator == Operators.likefull)
                            {
                                // TODO:?
                            }
                            else
                            {
                                //Double dParse = Double.Parse(_value);
                                Double dParse;
                                if (Double.TryParse(_value, out dParse))
                                {
                                    _value = dParse.ToString(nfi);
                                }
                            }


                            switch (expression.Operator)
                            {
                                case Operators.like:
                                    _value = _value.Replace('*', '%');
                                    localCondition += expression.Field + " " + _operator + " " + _valuestartend + _value + _valuestartend;
                                    break;
                                case Operators.likefull:
                                    _likeend = "%";
                                    goto case Operators.likestart;
                                case Operators.likestart:
                                    _likestart = "%";
                                    _operator = "like";
                                    localCondition += expression.Field + " " + _operator + " " + _valuestartend + _value + _valuestartend;
                                    break;
                                case Operators.notinner:
                                    goto case Operators.inner;
                                case Operators.inner:
                                    localCondition += expression.Field + " " + _operator + " ";
                                    localCondition += "(" + _value + ")";
                                    break;
                                default:
                                    localCondition += expression.Field + " " + _operator + " ";
                                    localCondition += _value;
                                    break;
                            }

                            if (_operator == Operators.between)
                            {
                                //Double dParse = Double.Parse(_valueto);
                                Double dParse;
                                if (Double.TryParse(_valueto, out dParse))
                                {
                                    _valueto = dParse.ToString(nfi);
                                }
                                
                                localCondition += " and ";
                                localCondition += _valueto;
                            }
                        }
                        break;
                    case "DateTime":
                        {
                            if (!String.IsNullOrEmpty(_value))
                            {
                                if (_value == SQLFunction.getdate)
                                {
                                    //IFormatProvider culture = new System.Globalization.CultureInfo("hu-HU", true);
                                    //_value = DateTime.ParseExact(DateTime.Now.ToString(), "yyyy.MM.dd HH:mm:ss", culture).ToString();
                                    //_value = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");
                                    _valuestartend = "";
                                }
                                else
                                {
                                    DateTime dt;
                                    if (DateTime.TryParse(_value, out dt))
                                    {
                                        _value = DateTime.Parse(_value).ToString("yyyy.MM.dd HH:mm:ss.fff");
                                    }
                                }
                            }

                            if (!String.IsNullOrEmpty(_valueto))
                            {
                                if (_valueto == SQLFunction.getdate)
                                {
                                    //IFormatProvider culture = new System.Globalization.CultureInfo("hu-HU", true);
                                    //_value = DateTime.ParseExact(DateTime.Now.ToString(), "yyyy.MM.dd HH:mm:ss", culture).ToString();
                                    //_value = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");                                
                                    //_valuestartend = "";
                                }
                                else
                                {
                                    DateTime dt;
                                    if (DateTime.TryParse(_valueto, out dt))
                                    {
                                        _valueto = dt.ToString("yyyy.MM.dd HH:mm:ss.fff");
                                    }
                                }
                            }

                            localCondition += expression.Field + " " + _operator + " ";
                            localCondition += _valuestartend + _value + _valuestartend;

                            if (_operator == Operators.between)
                            {
                                localCondition += " and ";
                                localCondition += _valuestartend + _valueto + _valuestartend;
                            }
                        }
                        break;
                    case "Munkanap":
                        if (!String.IsNullOrEmpty(_value))
                            _value = String.Format("(select dbo.fn_kovetkezo_munkanap(null,{0}))", _value);
                        if (!String.IsNullOrEmpty(_valueto))
                            _valueto = String.Format("(select dbo.fn_kovetkezo_munkanap(null,{0}))", _valueto);
                        goto case "Int32";

                }

                if (bParseOK && !String.IsNullOrEmpty(localCondition))
                {
                    #region z�r�jel ellen�rz�s
                    // z�r�jelek ellen�rz�se, csak figyelmeztet�s, mert elvileg lehet like "...(...%" is
                    if (System.Text.RegularExpressions.Regex.Matches(localCondition, @"\(").Count
                        != System.Text.RegularExpressions.Regex.Matches(localCondition, @"\)").Count)
                    {
                        if (expression.Operator != Operators.like && expression.Operator != Operators.likefull)
                        {
                            _ParseFailedConditions.AddWarning(localCondition);
                        }
                    }
                    #endregion z�r�jel ellen�rz�s

                    //sbWhere.AppendFormat("({0})", localCondition);
                    SqlLocalConditions.Add(String.Format("({0})", localCondition));
                }
                else if (!bParseOK && !String.IsNullOrEmpty(localCondition))
                {
                    //sbWhere.AppendFormat("[{0}]", localCondition); // megjel�lj�k �s sz�nd�kosan 4145-�s hib�t pr�b�lunk okozni
                    SqlLocalConditions.Add(String.Format("({0})", localCondition));
                    //_ParseFailedConditions.AddError(localCondition);
                }
                else if (!bParseOK)
                {
                    _ParseFailedConditions.AddError(localCondition);
                }

                //if (!String.IsNullOrEmpty(expression.Operator) && (x + 1 != g.expressions.Count))
                //{
                //    if (!String.IsNullOrEmpty(g.Operator) && sbWhere.Length > 0)
                //    {
                //        sbWhere.AppendFormat(" {0} ", g.Operator);
                //    }
                //}

            }

            //return sbWhere.ToString();
            if (!String.IsNullOrEmpty(g.Operator) && SqlLocalConditions.Count > 0)
            {
                return String.Join(String.Format(" {0} ", g.Operator), SqlLocalConditions.ToArray());
            }

            return String.Empty;
        }

        public void Build(Group group)
        {
            Build(group, true);
        }

        public void Build(Group group, bool Main)
        {
            if (group.groups != null)
            {
                List<string> SqlGroupExpressions = new List<string>();
                for (int i = 0; i < group.groups.Count; i++)
                {
                    //if (!Main)
                    //    _Where += "(";
                    if (group.groups[i] != null)
                        Build(group.groups[i], false);
                    string whereFromSetItem = SetItem(group.groups[i]);
                    if (!String.IsNullOrEmpty(whereFromSetItem))
                    {
                        //_Where += (Main ? whereFromSetItem : String.Format("({0})", whereFromSetItem));
                        SqlGroupExpressions.Add(Main ? whereFromSetItem : String.Format("({0})", whereFromSetItem));
                    }
                    //if (!Main)
                    //    _Where += ")";
                    //if (group.groups[i].expressions != null && group.groups[i].expressions.Count > 0
                    //    && !String.IsNullOrEmpty(group.Operator) && (i + 1 != group.groups.Count)
                    //    && !String.IsNullOrEmpty(whereFromSetItem))
                    //{
                    //    _Where += String.Format(" {0} ", group.Operator);
                    //}
                }
                //if (!Main && group.expressions != null && group.expressions.Count > 0)
                if (!String.IsNullOrEmpty(group.Operator) && SqlGroupExpressions.Count > 0)
                {
                    //_Where += String.Format(" {0} ", group.Operator);
                    if (!String.IsNullOrEmpty(_Where))
                    {
                        _Where += " and ";
                    }
                    _Where += String.Join(String.Format(" {0} ", group.Operator), SqlGroupExpressions.ToArray());
                }
            }
        }

        public Group MainGroup()
        {
            Group _ret = new Group();
            return _ret;
        }

        public class Group
        {
            public Group()
            {
            }
            public Group(String Operator)
            {
                _Operator = Operator;
            }

            public List<Group> groups = null;

            private string _Operator = "";

            public string Operator
            {
                get { return _Operator; }
                set { _Operator = value; }
            }

            public List<Expression> expressions = null;

            public Group AddGroup(string Operator)
            {
                if (groups == null)
                {
                    groups = new List<Group>();
                }
                groups.Add(new Group(Operator));
                return groups[groups.Count - 1];
            }

            public Expression AddExpression(String Field, String Operator, object Value, object ValueTo, String Type)
            {
                if (this.expressions == null)
                {
                    this.expressions = new List<Expression>();
                }
                this.expressions.Add(new Expression(Field, Operator, Value, ValueTo, Type));
                return expressions[expressions.Count - 1];
            }

        }

        public class Expression
        {
            public Expression(String Field, String Operator, object Value, object ValueTo, String Type)
            {
                _Field = Field;
                _Operator = Operator;
                _Value = Value;
                _ValueTo = ValueTo;
                _Type = Type;
            }

            private string _Field = "";

            public string Field
            {
                get { return _Field; }
                set { _Field = value; }
            }

            private string _Operator = "";

            public string Operator
            {
                get { return _Operator; }
                set { _Operator = value; }
            }

            private object _Value = null;

            public object Value
            {
                get { return _Value; }
                set { _Value = value; }
            }

            private object _ValueTo = null;

            public object ValueTo
            {
                get { return _ValueTo; }
                set { _ValueTo = value; }
            }

            private string _Type = null;

            public string Type
            {
                get { return _Type; }
                set { _Type = value; }
            }

            public bool Check()
            {
                bool isOK = true;

                string _value = this._Value.ToString();
                string _valueto = this._ValueTo.ToString();

                int numberOfOperands = Operators.NumberOfOperands(Operator);
                switch (numberOfOperands)
                {
                    case 1:
                        if (String.IsNullOrEmpty(_value))
                            return false;
                        //// f�ggv�nyeket egyel�re csak z�r�jelekre ellen�rz�nk
                        //if (System.Text.RegularExpressions.Regex.Matches(_value, @"\(").Count
                        //    != System.Text.RegularExpressions.Regex.Matches(_value, @"\)").Count)
                        //    return false;
                        break;
                    case 2:
                        if (String.IsNullOrEmpty(_valueto))
                            return false;
                        //// f�ggv�nyeket egyel�re csak z�r�jelekre ellen�rz�nk
                        //if (System.Text.RegularExpressions.Regex.Matches(_valueto, @"\(").Count
                        //        != System.Text.RegularExpressions.Regex.Matches(_valueto, @"\)").Count)
                        //        return false;
                        goto case 1;
                    default:
                        break;
                }

                switch (Type)
                {
                    case "FTSString":
                        goto case "String";
                    case "Char":
                           if (numberOfOperands > 0)
                           {
                               // nem f�ggv�ny
                               if (!_value.Contains("("))
                               {
                                   if (Operator == Operators.inner || Operator == Operators.notinner)
                                   {
                                       string[] items = System.Text.RegularExpressions.Regex.Replace(_value, "[ ']", String.Empty).Split(new char[] { ',' });
                                       foreach (string item in items)
                                       {
                                           if (item.Length > 1)
                                               return false;
                                       }
                                   }
                                   else
                                   {
                                           if (_value.Length > 1)
                                               return false;
                                   }

                                   if (numberOfOperands > 1)
                                   {
                                       // nem f�ggv�ny
                                       if (!_valueto.Contains("("))
                                       {
                                           if (Operator == Operators.inner || Operator == Operators.notinner)
                                           {
                                               string[] items = System.Text.RegularExpressions.Regex.Replace(_valueto, "[ ']", String.Empty).Split(new char[] { ',' });
                                               foreach (string item in items)
                                               {
                                                   if (item.Length > 1)
                                                       return false;
                                               }
                                           }
                                           else
                                           {
                                               if (_valueto.Length > 1)
                                                   return false;
                                           }
                                       }
                                   }
                               }
                           }
                        goto case "String";
                    case "Guid":
                        goto case "String";
                    case "String":
                       break;
                    case "Int32":
                       {
                           if (numberOfOperands > 0)
                           {
                               // nem f�ggv�ny
                               if (!_value.Contains("("))
                               {
                                   if (Operator == Operators.inner || Operator == Operators.notinner)
                                   {
                                       string[] items = _value.Replace(" ", "").Split(new char[] { ',' });
                                       int nParse;
                                       foreach (string item in items)
                                       {
                                           if (!Int32.TryParse(item, out nParse))
                                               return false;
                                       }
                                   }
                                   else if (Operator == Operators.like || Operator == Operators.likestart || Operator == Operators.likefull)
                                   {
                                       if (System.Text.RegularExpressions.Regex.Replace(_value, @"[*+\-0-9]", String.Empty) != String.Empty)
                                           return false;
                                   }
                                   else
                                   {
                                       int nParse;
                                       if (!Int32.TryParse(_value, out nParse))
                                           return false;
                                   }
                               }

                               if (numberOfOperands > 1)
                               {
                                   // nem f�ggv�ny
                                   if (!_valueto.Contains("("))
                                   {
                                       if (Operator == Operators.inner || Operator == Operators.notinner)
                                       {
                                           string[] items = _valueto.Replace(" ", "").Split(new char[] { ',' });
                                           int nParse;
                                           foreach (string item in items)
                                           {
                                               if (!Int32.TryParse(item, out nParse))
                                                   return false;
                                           }
                                       }
                                       else if (Operator == Operators.like || Operator == Operators.likestart || Operator == Operators.likefull)
                                       {
                                           if (System.Text.RegularExpressions.Regex.Replace(_valueto, @"[*+\-0-9]", String.Empty) != String.Empty)
                                               return false;
                                       }
                                       else
                                       {
                                           int nParse;
                                           if (!Int32.TryParse(_valueto, out nParse))
                                               return false;
                                       }
                                   }
                               }
                           }
                       }
                        break;
                    case "Double":
                        {
                            if (numberOfOperands > 0)
                            {
                                System.Text.RegularExpressions.Regex regexDouble = new System.Text.RegularExpressions.Regex(@"^([+-]?[*0-9]*)?(\.[*0-9]*)?$");
                                System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                                nfi.NumberDecimalSeparator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

                                System.Globalization.NumberStyles ns = System.Globalization.NumberStyles.Float;

                                // nem f�ggv�ny
                                if (!_value.Contains("("))
                                {
                                    if (Operator == Operators.inner || Operator == Operators.notinner)
                                    {
                                        string[] items = _value.Replace(" ", "").Split(new char[] { ',' });

                                        // itt ponttal kell elv�lasztani, m�sk�pp sz�tv�g�dik a tizedes vessz�n�l
                                        nfi.NumberDecimalSeparator = ".";

                                        Double dParse;
                                        foreach (string item in items)
                                        {
                                            if (!Double.TryParse(item, ns, nfi, out dParse))
                                                return false;
                                        }
                                    }
                                    else if (Operator == Operators.like || Operator == Operators.likestart || Operator == Operators.likefull)
                                    {
                                        if (regexDouble.IsMatch(_value) == false)
                                            return false;
                                    }
                                    else
                                    {
                                        Double dParse;
                                        if (!Double.TryParse(_value, ns, nfi, out dParse))
                                            return false;
                                    }
                                }

                                if (numberOfOperands > 1)
                                {
                                    // nem f�ggv�ny
                                    if (!_valueto.Contains("("))
                                    {
                                        if (Operator == Operators.inner || Operator == Operators.notinner)
                                        {
                                            string[] items = _valueto.Replace(" ", "").Split(new char[] { ',' });
                                            Double dParse;
                                            foreach (string item in items)
                                            {
                                                if (!Double.TryParse(item, ns, nfi, out dParse))
                                                    return false;
                                            }
                                        }
                                        else if (Operator == Operators.like || Operator == Operators.likestart || Operator == Operators.likefull)
                                        {
                                            if (regexDouble.IsMatch(_valueto) == false)
                                                return false;
                                        }
                                        else
                                        {
                                            Double dParse;
                                            if (!Double.TryParse(_valueto, ns, nfi, out dParse))
                                                return false;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case "DateTime":
                        {
                            if (numberOfOperands > 0)
                           {
                               // nem f�ggv�ny
                               if (!_value.Contains("("))
                               {
                                   if (Operator == Operators.inner || Operator == Operators.notinner)
                                   {
                                       string[] items = System.Text.RegularExpressions.Regex.Replace(_value, "[ ']", String.Empty).Split(new char[] { ',' });
                                       DateTime dtParse;
                                       foreach (string item in items)
                                       {
                                           if (!DateTime.TryParse(item, out dtParse))
                                               return false;
                                       }
                                   }
                                   else
                                   {
                                       DateTime dtParse;
                                       if (!DateTime.TryParse(_value, out dtParse))
                                           return false;
                                   }
                               }

                               if (numberOfOperands > 1)
                               {
                                   // nem f�ggv�ny
                                   if (!_valueto.Contains("("))
                                   {
                                       if (Operator == Operators.inner || Operator == Operators.notinner)
                                       {
                                           string[] items = System.Text.RegularExpressions.Regex.Replace(_valueto, "[ ']", String.Empty).Split(new char[] { ',' });
                                           DateTime dtParse;
                                           foreach (string item in items)
                                           {
                                               if (!DateTime.TryParse(item, out dtParse))
                                                   return false;
                                           }
                                       }
                                       else
                                       {
                                           DateTime dtParse;
                                           if (!DateTime.TryParse(_valueto, out dtParse))
                                               return false;
                                       }
                                   }
                               }
                           }
                       }
                        break;
                    case "Munkanap":
                        goto case "Int32";

                }

                return isOK;
            }
        }

        public static class Operators
        {
            public const string and = "and";
            public const string or = "or";
            public const string like = "like";
            public const string likefull = "likefull";
            public const string likestart = "likestart";
            public const string equals = "=";
            public const string notequals = "<>";
            public const string inner = "in";
            public const string notinner = "not in";
            public const string less = "<";
            public const string greater = ">";
            public const string lessorequal = "<=";
            public const string greaterorequal = ">=";
            public const string isnull = "is null";
            public const string notnull = "is not null";
            public const string between = "between";
            public const string isnullorequals = "is null or =";
            public const string isnullornotequals = "is null or <>";
            public const string isnullorinner = "is null or in";
            public const string isnullornotinner = "is null or not in";

            public const string contains = "contains({0}, {1})";

            public static int NumberOfOperands(string op)
            {
                switch (op)
                {
                    case and:
                    case or:
                    case between:
                        return 2;
                    case like:
                    case likefull:
                    case likestart:
                    case equals:
                    case notequals:
                    case inner:
                    case notinner:
                    case less:
                    case greater:
                    case lessorequal:
                    case greaterorequal:
                    case isnullorequals:
                    case isnullornotequals:
                    case isnullorinner:
                    case isnullornotinner:
                    case contains:
                        return 1;
                    case isnull:
                    case notnull:
                        return 0;
                    default:
                        return -1;
                }
            }
        }

        private static Dictionary<string, string> operatorNames = null;

        public static Dictionary<string, string> OperatorNames
        {
            get
            {
                if (operatorNames == null)
                {
                    operatorNames = new Dictionary<string, string>(16);
                    operatorNames.Add(Operators.and," �s ");
                    operatorNames.Add(Operators.or, " vagy ");
                    operatorNames.Add(Operators.like, " like ");
                    operatorNames.Add(Operators.likefull, " likefull ");
                    operatorNames.Add(Operators.likestart, " likestart ");
                    operatorNames.Add(Operators.equals, " = ");
                    operatorNames.Add(Operators.notequals, " <> ");
                    operatorNames.Add(Operators.inner, " in ");
                    operatorNames.Add(Operators.notinner, " not in ");
                    operatorNames.Add(Operators.less, " < ");
                    operatorNames.Add(Operators.greater, " > ");
                    operatorNames.Add(Operators.lessorequal, " <= ");
                    operatorNames.Add(Operators.greaterorequal, " >= ");
                    operatorNames.Add(Operators.isnull, " is null ");
                    operatorNames.Add(Operators.notnull, " is not null ");
                    operatorNames.Add(Operators.between, " - ");
                    operatorNames.Add(Operators.isnullorequals, "null vagy =");
                    operatorNames.Add(Operators.isnullornotequals, "null vagy <>");
                    operatorNames.Add(Operators.isnullorinner, "null vagy in ");
                    operatorNames.Add(Operators.isnullornotinner, "null vagy not in ");

                    operatorNames.Add(Operators.contains, " tartalmaz ");
                }

                return operatorNames;
            }
        }

        public static class SQLFunction
        {
            public const string getdate = "getdate()";
        }
    }
}
