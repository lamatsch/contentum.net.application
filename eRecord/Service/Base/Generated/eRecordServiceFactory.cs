
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Text;
namespace Contentum.eRecord.Service
{

    public partial class ServiceFactory
    {
        private static void SetDefaults(System.Web.Services.Protocols.SoapHttpClientProtocol service)
        {
            service.PreAuthenticate = true;
            // BUG_11753
            service.AllowAutoRedirect = true;
        }

        public EREC_UgyUgyiratokService GetEREC_UgyUgyiratokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_UgyUgyiratokService _Service = new EREC_UgyUgyiratokService(_BusinessServiceUrl + "EREC_UgyUgyiratokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IraIratokService GetEREC_IraIratokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraIratokService _Service = new EREC_IraIratokService(_BusinessServiceUrl + "EREC_IraIratokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IratKapcsolatokService GetEREC_IratKapcsolatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IratKapcsolatokService _Service = new EREC_IratKapcsolatokService(_BusinessServiceUrl + "EREC_IratKapcsolatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IratAlairokService GetEREC_IratAlairokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IratAlairokService _Service = new EREC_IratAlairokService(_BusinessServiceUrl + "EREC_IratAlairokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_Irat_IktatokonyveiService GetEREC_Irat_IktatokonyveiService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_Irat_IktatokonyveiService _Service = new EREC_Irat_IktatokonyveiService(_BusinessServiceUrl + "EREC_Irat_IktatokonyveiService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IraIktatoKonyvekService GetEREC_IraIktatoKonyvekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraIktatoKonyvekService _Service = new EREC_IraIktatoKonyvekService(_BusinessServiceUrl + "EREC_IraIktatoKonyvekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IraIrattariTetelekService GetEREC_IraIrattariTetelekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraIrattariTetelekService _Service = new EREC_IraIrattariTetelekService(_BusinessServiceUrl + "EREC_IraIrattariTetelekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IraJegyzekekService GetEREC_IraJegyzekekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraJegyzekekService _Service = new EREC_IraJegyzekekService(_BusinessServiceUrl + "EREC_IraJegyzekekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IraJegyzekTetelekService GetEREC_IraJegyzekTetelekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraJegyzekTetelekService _Service = new EREC_IraJegyzekTetelekService(_BusinessServiceUrl + "EREC_IraJegyzekTetelekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IraKezFeljegyzesekService GetEREC_IraKezFeljegyzesekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraKezFeljegyzesekService _Service = new EREC_IraKezFeljegyzesekService(_BusinessServiceUrl + "EREC_IraKezFeljegyzesekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }


        public EREC_IraOnkormAdatokService GetEREC_IraOnkormAdatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraOnkormAdatokService _Service = new EREC_IraOnkormAdatokService(_BusinessServiceUrl + "EREC_IraOnkormAdatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_TargySzavakService GetEREC_TargySzavakService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_TargySzavakService _Service = new EREC_TargySzavakService(_BusinessServiceUrl + "EREC_TargySzavakService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_KuldBekuldokService GetEREC_KuldBekuldokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_KuldBekuldokService _Service = new EREC_KuldBekuldokService(_BusinessServiceUrl + "EREC_KuldBekuldokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_KuldKezFeljegyzesekService GetEREC_KuldKezFeljegyzesekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_KuldKezFeljegyzesekService _Service = new EREC_KuldKezFeljegyzesekService(_BusinessServiceUrl + "EREC_KuldKezFeljegyzesekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_KuldKuldemenyekService GetEREC_KuldKuldemenyekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_KuldKuldemenyekService _Service = new EREC_KuldKuldemenyekService(_BusinessServiceUrl + "EREC_KuldKuldemenyekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_KuldTertivevenyekService GetEREC_KuldTertivevenyekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_KuldTertivevenyekService _Service = new EREC_KuldTertivevenyekService(_BusinessServiceUrl + "EREC_KuldTertivevenyekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_PldIratPeldanyokService GetEREC_PldIratPeldanyokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_PldIratPeldanyokService _Service = new EREC_PldIratPeldanyokService(_BusinessServiceUrl + "EREC_PldIratPeldanyokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_eMailBoritekokService GetEREC_eMailBoritekokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_eMailBoritekokService _Service = new EREC_eMailBoritekokService(_BusinessServiceUrl + "EREC_eMailBoritekokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_eMailBoritekCimeiService GetEREC_eMailBoritekCimeiService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_eMailBoritekCimeiService _Service = new EREC_eMailBoritekCimeiService(_BusinessServiceUrl + "EREC_eMailBoritekCimeiService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_eMailFiokokService GetEREC_eMailFiokokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_eMailFiokokService _Service = new EREC_eMailFiokokService(_BusinessServiceUrl + "EREC_eMailFiokokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_UgyKezFeljegyzesekService GetEREC_UgyKezFeljegyzesekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_UgyKezFeljegyzesekService _Service = new EREC_UgyKezFeljegyzesekService(_BusinessServiceUrl + "EREC_UgyKezFeljegyzesekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_Obj_MetaAdataiService GetEREC_Obj_MetaAdataiService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_Obj_MetaAdataiService _Service = new EREC_Obj_MetaAdataiService(_BusinessServiceUrl + "EREC_Obj_MetaAdataiService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public EREC_Obj_MetaDefinicioService GetEREC_Obj_MetaDefinicioService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_Obj_MetaDefinicioService _Service = new EREC_Obj_MetaDefinicioService(_BusinessServiceUrl + "EREC_Obj_MetaDefinicioService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_UgyUgyiratdarabokService GetEREC_UgyUgyiratdarabokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_UgyUgyiratdarabokService _Service = new EREC_UgyUgyiratdarabokService(_BusinessServiceUrl + "EREC_UgyUgyiratdarabokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_Kuldemeny_IratPeldanyaiService GetEREC_Kuldemeny_IratPeldanyaiService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_Kuldemeny_IratPeldanyaiService _Service = new EREC_Kuldemeny_IratPeldanyaiService(_BusinessServiceUrl + "EREC_Kuldemeny_IratPeldanyaiService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_ObjTip_TargySzoService GetKRT_ObjTip_TargySzoService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_ObjTip_TargySzoService _Service = new KRT_ObjTip_TargySzoService(_BusinessServiceUrl + "KRT_ObjTip_TargySzoService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_HataridosFeladatokService GetEREC_HataridosFeladatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_HataridosFeladatokService _Service = new EREC_HataridosFeladatokService(_BusinessServiceUrl + "EREC_HataridosFeladatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_FeladatDefinicioService GetEREC_FeladatDefinicioService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_FeladatDefinicioService _Service = new EREC_FeladatDefinicioService(_BusinessServiceUrl + "EREC_FeladatDefinicioService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public EREC_Hatarid_ObjektumokService GetEREC_Hatarid_ObjektumokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_Hatarid_ObjektumokService _Service = new EREC_Hatarid_ObjektumokService(_BusinessServiceUrl + "EREC_Hatarid_ObjektumokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IraKezbesitesiTetelekService GetEREC_IraKezbesitesiTetelekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraKezbesitesiTetelekService _Service = new EREC_IraKezbesitesiTetelekService(_BusinessServiceUrl + "EREC_IraKezbesitesiTetelekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IrattariKikeroService GetEREC_IrattariKikeroService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IrattariKikeroService _Service = new EREC_IrattariKikeroService(_BusinessServiceUrl + "EREC_IrattariKikeroService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IratMetaDefinicioService GetEREC_IratMetaDefinicioService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IratMetaDefinicioService _Service = new EREC_IratMetaDefinicioService(_BusinessServiceUrl + "EREC_IratMetaDefinicioService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_KuldKapcsolatokService GetEREC_KuldKapcsolatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_KuldKapcsolatokService _Service = new EREC_KuldKapcsolatokService(_BusinessServiceUrl + "EREC_KuldKapcsolatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_AgazatiJelekService GetEREC_AgazatiJelekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_AgazatiJelekService _Service = new EREC_AgazatiJelekService(_BusinessServiceUrl + "EREC_AgazatiJelekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_eMailBoritekCsatolmanyokService GetEREC_eMailBoritekCsatolmanyokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_eMailBoritekCsatolmanyokService _Service = new EREC_eMailBoritekCsatolmanyokService(_BusinessServiceUrl + "EREC_eMailBoritekCsatolmanyokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_UgyiratKapcsolatokService GetEREC_UgyiratKapcsolatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_UgyiratKapcsolatokService _Service = new EREC_UgyiratKapcsolatokService(_BusinessServiceUrl + "EREC_UgyiratKapcsolatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IrattariTetel_IktatokonyvService GetEREC_IrattariTetel_IktatokonyvService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IrattariTetel_IktatokonyvService _Service = new EREC_IrattariTetel_IktatokonyvService(_BusinessServiceUrl + "EREC_IrattariTetel_IktatokonyvService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_BarkodokService GetKRT_BarkodokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_BarkodokService _Service = new KRT_BarkodokService(_BusinessServiceUrl + "KRT_BarkodokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public KRT_BarkodSavokService GetKRT_BarkodSavokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_BarkodSavokService _Service = new KRT_BarkodSavokService(_BusinessServiceUrl + "KRT_BarkodSavokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_SzignalasiJegyzekekService GetEREC_SzignalasiJegyzekekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_SzignalasiJegyzekekService _Service = new EREC_SzignalasiJegyzekekService(_BusinessServiceUrl + "EREC_SzignalasiJegyzekekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }


        public SzervezetHierarchiaService GetSzervezetHierarchiaService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                SzervezetHierarchiaService _Service = new SzervezetHierarchiaService(_BusinessServiceUrl + "SzervezetHierarchiaService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_ObjektumTargyszavaiService GetEREC_ObjektumTargyszavaiService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_ObjektumTargyszavaiService _Service = new EREC_ObjektumTargyszavaiService(_BusinessServiceUrl + "EREC_ObjektumTargyszavaiService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_IraKezbesitesiFejekService GetEREC_IraKezbesitesiFejekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IraKezbesitesiFejekService _Service = new EREC_IraKezbesitesiFejekService(_BusinessServiceUrl + "EREC_IraKezbesitesiFejekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_UgyiratObjKapcsolatokService GetEREC_UgyiratObjKapcsolatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_UgyiratObjKapcsolatokService _Service = new EREC_UgyiratObjKapcsolatokService(_BusinessServiceUrl + "EREC_UgyiratObjKapcsolatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_CsatolmanyokService GetEREC_CsatolmanyokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_CsatolmanyokService _Service = new EREC_CsatolmanyokService(_BusinessServiceUrl + "EREC_CsatolmanyokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_MellekletekService GetEREC_MellekletekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_MellekletekService _Service = new EREC_MellekletekService(_BusinessServiceUrl + "EREC_MellekletekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }


        public AlairasokService GetAlairasokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                AlairasokService _Service = new AlairasokService(_BusinessServiceUrl + "AlairasokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }


        public EREC_IratelemKapcsolatokService GetEREC_IratelemKapcsolatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IratelemKapcsolatokService _Service = new EREC_IratelemKapcsolatokService(_BusinessServiceUrl + "EREC_IratelemKapcsolatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_PldKapjakMegService GetEREC_PldKapjakMegService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_PldKapjakMegService _Service = new EREC_PldKapjakMegService(_BusinessServiceUrl + "EREC_PldKapjakMegService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public KRT_MappakService GetKRT_MappakService()

        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_MappakService _Service = new KRT_MappakService(_BusinessServiceUrl + "KRT_MappakService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);

                return _Service;
            }

            return null;
        }

        public KRT_MappaTartalmakService GetKRT_MappaTartalmakService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_MappaTartalmakService _Service = new KRT_MappaTartalmakService(_BusinessServiceUrl + "KRT_MappaTartalmakService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public EREC_SzamlakService GetEREC_SzamlakService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_SzamlakService _Service = new EREC_SzamlakService(_BusinessServiceUrl + "EREC_SzamlakService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public PirEdokInterfaceService GetPirEdokInterfaceService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                PirEdokInterfaceService _Service = new PirEdokInterfaceService(_BusinessServiceUrl + "PirEdokInterfaceService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public KRT_Mentett_Talalati_ListakService GetKRT_Mentett_Talalati_ListakService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Mentett_Talalati_ListakService _Service = new KRT_Mentett_Talalati_ListakService(_BusinessServiceUrl + "KRT_Mentett_Talalati_ListakService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public HKP_DokumentumAdatokService GetHKP_DokumentumAdatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                HKP_DokumentumAdatokService _Service = new HKP_DokumentumAdatokService(_BusinessServiceUrl + "HKP_DokumentumAdatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_TomegesIktatasService GetEREC_TomegesIktatasService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_TomegesIktatasService _Service = new EREC_TomegesIktatasService(_BusinessServiceUrl + "EREC_TomegesIktatasService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
        public EREC_IrattariHelyekService GetEREC_IrattariHelyekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_IrattariHelyekService _Service = new EREC_IrattariHelyekService(_BusinessServiceUrl + "EREC_IrattariHelyekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }
        public KRT_RagszamSavokService GetKRT_RagszamSavokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_RagszamSavokService _Service = new KRT_RagszamSavokService(_BusinessServiceUrl + "KRT_RagszamSavokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }
        public KRT_RagSzamokService GetKRT_RagSzamokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_RagSzamokService _Service = new KRT_RagSzamokService(_BusinessServiceUrl + "KRT_RagSzamokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }
        public EREC_Alairas_FolyamatokService GetEREC_Alairas_FolyamatokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_Alairas_FolyamatokService _Service = new EREC_Alairas_FolyamatokService(_BusinessServiceUrl + "EREC_Alairas_FolyamatokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
        public EREC_Alairas_Folyamat_TetelekService GetEREC_Alairas_Folyamat_TetelekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_Alairas_Folyamat_TetelekService _Service = new EREC_Alairas_Folyamat_TetelekService(_BusinessServiceUrl + "EREC_Alairas_Folyamat_TetelekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public SakkoraService Get_SakkoraService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                SakkoraService _Service = new SakkoraService(_BusinessServiceUrl + "SakkoraService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public KRT_Partner_DokumentumokService GetKRT_Partner_DokumentumokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRT_Partner_DokumentumokService _Service = new KRT_Partner_DokumentumokService(_BusinessServiceUrl + "KRT_Partner_DokumentumokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public EREC_FeladatErtesitesService GetEREC_FeladatErtesitesService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_FeladatErtesitesService _Service = new EREC_FeladatErtesitesService(_BusinessServiceUrl + "EREC_FeladatErtesitesService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public EREC_TomegesIktatasFolyamatService GetEREC_TomegesIktatasFolyamatService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_TomegesIktatasFolyamatService _Service = new EREC_TomegesIktatasFolyamatService(_BusinessServiceUrl + "EREC_TomegesIktatasFolyamatService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        public EREC_TomegesIktatasTetelekService GetEREC_TomegesIktatasTetelekService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_TomegesIktatasTetelekService _Service = new EREC_TomegesIktatasTetelekService(_BusinessServiceUrl + "EREC_TomegesIktatasTetelekService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                return _Service;
            }

            return null;
        }

        //public EREC_IraIratokDokumentumokService GetEREC_IraIratokDokumentumokService()
        //{
        //    if (_BusinessServiceType == "SOAP")
        //    {
        //        EREC_IraIratokDokumentumokService _Service = new EREC_IraIratokDokumentumokService(_BusinessServiceUrl + "EREC_IraIratokDokumentumokService.asmx");
        //        _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
        //        return _Service;
        //    }

        //    return null;
        //}
    }
}