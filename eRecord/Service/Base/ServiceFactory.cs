
using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eBusinessDocuments;
using Contentum.eScanWebService;

namespace Contentum.eRecord.Service
{
    public partial class ServiceFactory
    {
        string _BusinessServiceType = "SOAP";
        string _BusinessServiceUrl = "";
        string _BusinessServiceAuthentication = "Windows";
        string _BusinessServiceUserDomain = "";
        string _BusinessServiceUserName = "";
        string _BusinessServicePassword = "";

        public string BusinessServiceType
        {
            get { return _BusinessServiceType; }
            set { _BusinessServiceType = value; }
        }

        public string BusinessServiceUrl
        {
            get { return _BusinessServiceUrl; }
            set { _BusinessServiceUrl = value; }
        }

        public string BusinessServiceAuthentication
        {
            get { return _BusinessServiceAuthentication; }
            set { _BusinessServiceAuthentication = value; }
        }

        public string BusinessServiceUserDomain
        {
            get { return _BusinessServiceUserDomain; }
            set { _BusinessServiceUserDomain = value; }
        }

        public string BusinessServiceUserName
        {
            get { return _BusinessServiceUserName; }
            set { _BusinessServiceUserName = value; }
        }

        public string BusinessServicePassword
        {
            get { return _BusinessServicePassword; }
            set { _BusinessServicePassword = value; }
        }

        public EREC_WorkFlowService GetEREC_WorkFlowService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_WorkFlowService _Service = new EREC_WorkFlowService(_BusinessServiceUrl + "EREC_WorkFlowService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
        /// <summary>
        /// Return scan service instance
        /// </summary>
        /// <returns></returns>
        public ScanService GetScanService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                ScanService _Service = new ScanService(_BusinessServiceUrl + "ScanService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_eBeadvanyokService GetEREC_eBeadvanyokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_eBeadvanyokService _Service = new EREC_eBeadvanyokService(_BusinessServiceUrl + "EREC_eBeadvanyokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public EREC_eBeadvanyCsatolmanyokService GetEREC_eBeadvanyCsatolmanyokService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                EREC_eBeadvanyCsatolmanyokService _Service = new EREC_eBeadvanyCsatolmanyokService(_BusinessServiceUrl + "EREC_eBeadvanyCsatolmanyokService.asmx");
                _Service.Credentials = Utility.GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }
    }
}