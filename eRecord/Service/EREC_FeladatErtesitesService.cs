namespace Contentum.eRecord.Service
{
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;

    /// <remarks/>
    //[System.Web.Services.WebServiceBindingAttribute(Name = "EREC_FeladatErtesitesServiceSoap", Namespace = "Contentum.eRecord.WebService")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseEREC_FeladatErtesites))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class EREC_FeladatErtesitesService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        #region GetAllWithExtension

        private System.Threading.SendOrPostCallback GetAllWithExtensionOperationCompleted;

        /// <remarks/>
        public event GetAllWithExtensionCompletedEventHandler GetAllWithExtensionCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAllWithExtension", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllWithExtension(ExecParam ExecParam, EREC_FeladatErtesitesSearch _EREC_FeladatErtesitesSearch)
        {
            object[] results = this.Invoke("GetAllWithExtension", new object[] {
                        ExecParam,
                        _EREC_FeladatErtesitesSearch});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllWithExtension(ExecParam ExecParam, EREC_FeladatErtesitesSearch _EREC_FeladatErtesitesSearch, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllWithExtension", new object[] {
                        ExecParam,
                        _EREC_FeladatErtesitesSearch}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllWithExtension(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllWithExtensionAsync(ExecParam ExecParam, EREC_FeladatErtesitesSearch _EREC_FeladatErtesitesSearch)
        {
            this.GetAllWithExtensionAsync(ExecParam, _EREC_FeladatErtesitesSearch, null);
        }

        /// <remarks/>
        public void GetAllWithExtensionAsync(ExecParam ExecParam, EREC_FeladatErtesitesSearch _EREC_FeladatErtesitesSearch, object userState)
        {
            if ((this.GetAllWithExtensionOperationCompleted == null))
            {
                this.GetAllWithExtensionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWithExtensionOperationCompleted);
            }
            this.InvokeAsync("GetAllWithExtension", new object[] {
                        ExecParam,
                        _EREC_FeladatErtesitesSearch}, this.GetAllWithExtensionOperationCompleted, userState);
        }

        private void OnGetAllWithExtensionOperationCompleted(object arg)
        {
            if ((this.GetAllWithExtensionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWithExtensionCompleted(this, new GetAllWithExtensionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllWithExtensionCompletedEventHandler(object sender, GetAllWithExtensionCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllWithExtensionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllWithExtensionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion
    }
}