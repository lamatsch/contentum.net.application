﻿

namespace Contentum.eRecord.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;


    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseEREC_IrattariHelyek))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class EREC_IrattariHelyekService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        #region GetAllRoots

        private System.Threading.SendOrPostCallback GetAllRootsOperationCompleted;

        /// <remarks/>
        public event GetAllRootsCompletedEventHandler GetAllRootsCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAllRoots", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllRoots(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
        {
            object[] results = this.Invoke("GetAllRoots", new object[] {
                        ExecParam,
                        _EREC_IrattariHelyekSearch});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllRoots(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllRoots", new object[] {
                        ExecParam,
                        _EREC_IrattariHelyekSearch}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllRoots(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllRootsAsync(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
        {
            this.GetAllRootsAsync(ExecParam, _EREC_IrattariHelyekSearch, null);
        }

        /// <remarks/>
        public void GetAllRootsAsync(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch, object userState)
        {
            if ((this.GetAllRootsOperationCompleted == null))
            {
                this.GetAllRootsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllRootsOperationCompleted);
            }
            this.InvokeAsync("GetAllRoots", new object[] {
                        ExecParam,
                        _EREC_IrattariHelyekSearch}, this.GetAllRootsOperationCompleted, userState);
        }

        private void OnGetAllRootsOperationCompleted(object arg)
        {
            if ((this.GetAllRootsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllRootsCompleted(this, new GetAllRootsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllRootsCompletedEventHandler(object sender, GetAllRootsCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllRootsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllRootsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region GetAllWithPartnerKapcsolatok

        private System.Threading.SendOrPostCallback GetAllWithPartnerKapcsolatokOperationCompleted;

        /// <remarks/>
        public event GetAllWithPartnerKapcsolatokCompletedEventHandler GetAllWithPartnerKapcsolatokCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAllWithPartnerKapcsolatok", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllWithPartnerKapcsolatok(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
        {
            object[] results = this.Invoke("GetAllWithPartnerKapcsolatok", new object[] {
                        ExecParam,
                        _EREC_IrattariHelyekSearch});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllWithPartnerKapcsolatok(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllWithPartnerKapcsolatok", new object[] {
                        ExecParam,
                        _EREC_IrattariHelyekSearch}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllWithPartnerKapcsolatok(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllWithPartnerKapcsolatokAsync(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
        {
            this.GetAllWithPartnerKapcsolatokAsync(ExecParam, _EREC_IrattariHelyekSearch, null);
        }

        /// <remarks/>
        public void GetAllWithPartnerKapcsolatokAsync(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch, object userState)
        {
            if ((this.GetAllWithPartnerKapcsolatokOperationCompleted == null))
            {
                this.GetAllWithPartnerKapcsolatokOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWithPartnerKapcsolatokOperationCompleted);
            }
            this.InvokeAsync("GetAllWithPartnerKapcsolatok", new object[] {
                        ExecParam,
                        _EREC_IrattariHelyekSearch}, this.GetAllWithPartnerKapcsolatokOperationCompleted, userState);
        }

        private void OnGetAllWithPartnerKapcsolatokOperationCompleted(object arg)
        {
            if ((this.GetAllWithPartnerKapcsolatokCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWithPartnerKapcsolatokCompleted(this, new GetAllWithPartnerKapcsolatokCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllWithPartnerKapcsolatokCompletedEventHandler(object sender, GetAllWithPartnerKapcsolatokCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllWithPartnerKapcsolatokCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllWithPartnerKapcsolatokCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region GetAllLeafs

        private System.Threading.SendOrPostCallback GetAllLeafsOperationCompleted;

        /// <remarks/>
        public event GetAllLeafsCompletedEventHandler GetAllLeafsCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAllLeafs", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllLeafs(ExecParam ExecParam)
        {
            object[] results = this.Invoke("GetAllLeafs", new object[] {
                        ExecParam
                        });
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllLeafs(ExecParam ExecParam, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllLeafs", new object[] {
                        ExecParam
                        }, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllLeafs(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllLeafsAsync(ExecParam ExecParam)
        {
            this.GetAllLeafsAsync(ExecParam, null);
        }

        /// <remarks/>
        public void GetAllLeafsAsync(ExecParam ExecParam, object userState)
        {
            if ((this.GetAllLeafsOperationCompleted == null))
            {
                this.GetAllLeafsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllLeafsOperationCompleted);
            }
            this.InvokeAsync("GetAllLeafs", new object[] {
                        ExecParam
                        }, this.GetAllLeafsOperationCompleted, userState);
        }

        private void OnGetAllLeafsOperationCompleted(object arg)
        {
            if ((this.GetAllLeafsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllLeafsCompleted(this, new GetAllLeafsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllLeafsCompletedEventHandler(object sender, GetAllLeafsCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllLeafsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllLeafsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region IrattariHelyStrukturaGetByRoots

        private System.Threading.SendOrPostCallback IrattariHelyStrukturaGetByRootsOperationCompleted;

        /// <remarks/>
        public event IrattariHelyStrukturaGetByRootsCompletedEventHandler IrattariHelyStrukturaGetByRootsCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/IrattariHelyStrukturaGetByRoots", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result IrattariHelyStrukturaGetByRoots(ExecParam ExecParam, string RootId)
        {
            object[] results = this.Invoke("IrattariHelyStrukturaGetByRoots", new object[] {
                        ExecParam,
                        RootId});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginIrattariHelyStrukturaGetByRoots(ExecParam ExecParam, string RootId, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("IrattariHelyStrukturaGetByRoots", new object[] {
                        ExecParam,
                        RootId}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndIrattariHelyStrukturaGetByRoots(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void IrattariHelyStrukturaGetByRootsAsync(ExecParam ExecParam, string RootId)
        {
            this.IrattariHelyStrukturaGetByRootsAsync(ExecParam, RootId, null);
        }

        /// <remarks/>
        public void IrattariHelyStrukturaGetByRootsAsync(ExecParam ExecParam, string RootId, object userState)
        {
            if ((this.IrattariHelyStrukturaGetByRootsOperationCompleted == null))
            {
                this.IrattariHelyStrukturaGetByRootsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnIrattariHelyStrukturaGetByRootsOperationCompleted);
            }
            this.InvokeAsync("IrattariHelyStrukturaGetByRoots", new object[] {
                        ExecParam,
                        RootId}, this.IrattariHelyStrukturaGetByRootsOperationCompleted, userState);
        }

        private void OnIrattariHelyStrukturaGetByRootsOperationCompleted(object arg)
        {
            if ((this.IrattariHelyStrukturaGetByRootsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.IrattariHelyStrukturaGetByRootsCompleted(this, new IrattariHelyStrukturaGetByRootsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void IrattariHelyStrukturaGetByRootsCompletedEventHandler(object sender, IrattariHelyStrukturaGetByRootsCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class IrattariHelyStrukturaGetByRootsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal IrattariHelyStrukturaGetByRootsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region GetREadOnlyIds
        private System.Threading.SendOrPostCallback GetReadOnlyIdsOperationCompleted;

        /// <remarks/>
        public event GetReadOnlyIdsCompletedEventHandler GetReadOnlyIdsCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetReadOnlyIds", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetReadOnlyIds(ExecParam ExecParam)
        {
            object[] results = this.Invoke("GetReadOnlyIds", new object[] {
                        ExecParam});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetReadOnlyIds(ExecParam ExecParam, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetReadOnlyIds", new object[] {
                        ExecParam}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetReadOnlyIds(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetReadOnlyIdsAsync(ExecParam ExecParam)
        {
            this.GetReadOnlyIdsAsync(ExecParam, null);
        }

        /// <remarks/>
        public void GetReadOnlyIdsAsync(ExecParam ExecParam, object userState)
        {
            if ((this.GetReadOnlyIdsOperationCompleted == null))
            {
                this.GetReadOnlyIdsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetReadOnlyIdsOperationCompleted);
            }
            this.InvokeAsync("GetReadOnlyIds", new object[] {
                        ExecParam}, this.GetReadOnlyIdsOperationCompleted, userState);
        }

        private void OnGetReadOnlyIdsOperationCompleted(object arg)
        {
            if ((this.GetReadOnlyIdsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetReadOnlyIdsCompleted(this, new GetReadOnlyIdsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetReadOnlyIdsCompletedEventHandler(object sender, GetReadOnlyIdsCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetReadOnlyIdsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetReadOnlyIdsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region GetByVonalkod

        private System.Threading.SendOrPostCallback GetByVonalkodOperationCompleted;

        /// <remarks/>
        public event GetByVonalkodCompletedEventHandler GetByVonalkodCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetByVonalkod", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetByVonalkod(ExecParam ExecParam, string Vonalkod)
        {
            object[] results = this.Invoke("GetByVonalkod", new object[] {
                        ExecParam,
                        Vonalkod});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetByVonalkod(ExecParam ExecParam, string Vonalkod, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetByVonalkod", new object[] {
                        ExecParam,
                        Vonalkod}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetByVonalkod(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetByVonalkodAsync(ExecParam ExecParam, string Vonalkod)
        {
            this.GetByVonalkodAsync(ExecParam, Vonalkod, null);
        }

        /// <remarks/>
        public void GetByVonalkodAsync(ExecParam ExecParam, string Vonalkod, object userState)
        {
            if ((this.GetByVonalkodOperationCompleted == null))
            {
                this.GetByVonalkodOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetByVonalkodOperationCompleted);
            }
            this.InvokeAsync("GetByVonalkod", new object[] {
                        ExecParam,
                        Vonalkod}, this.GetByVonalkodOperationCompleted, userState);
        }

        private void OnGetByVonalkodOperationCompleted(object arg)
        {
            if ((this.GetByVonalkodCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetByVonalkodCompleted(this, new GetByVonalkodCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetByVonalkodCompletedEventHandler(object sender, GetByVonalkodCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetByVonalkodCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetByVonalkodCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region IrattarRendezes

        private System.Threading.SendOrPostCallback IrattarRendezesOperationCompleted;

        /// <remarks/>
        public event IrattarRendezesCompletedEventHandler IrattarRendezesCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/IrattarRendezes", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result IrattarRendezes(ExecParam ExecParam, string UgyiratIds, string IrattarId, string Ertek)
        {
            object[] results = this.Invoke("IrattarRendezes", new object[] {
                        ExecParam,
                        UgyiratIds,
                        IrattarId,
                        Ertek});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginIrattarRendezes(ExecParam ExecParam, string UgyiratIds, string IrattarId, string Ertek, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("IrattarRendezes", new object[] {
                        ExecParam,
                        UgyiratIds,
                        IrattarId,
                        Ertek}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndIrattarRendezes(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void IrattarRendezesAsync(ExecParam ExecParam, string UgyiratIds, string IrattarId, string Ertek)
        {
            this.IrattarRendezesAsync(ExecParam, UgyiratIds, IrattarId, Ertek, null);
        }

        /// <remarks/>
        public void IrattarRendezesAsync(ExecParam ExecParam, string UgyiratIds, string IrattarId, string Ertek, object userState)
        {
            if ((this.IrattarRendezesOperationCompleted == null))
            {
                this.IrattarRendezesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnIrattarRendezesOperationCompleted);
            }
            this.InvokeAsync("IrattarRendezes", new object[] {
                        ExecParam,
                        UgyiratIds,
                        IrattarId,
                        Ertek}, this.IrattarRendezesOperationCompleted, userState);
        }

        private void OnIrattarRendezesOperationCompleted(object arg)
        {
            if ((this.IrattarRendezesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.IrattarRendezesCompleted(this, new IrattarRendezesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void IrattarRendezesCompletedEventHandler(object sender, IrattarRendezesCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class IrattarRendezesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal IrattarRendezesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region GetAllForTerkep

        private System.Threading.SendOrPostCallback GetAllForTerkepOperationCompleted;

        /// <remarks/>
        public event GetAllForTerkepCompletedEventHandler GetAllForTerkepCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAllForTerkep", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllForTerkep(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
        {
            object[] results = this.Invoke("GetAllForTerkep", new object[] {
                        ExecParam,
                        _EREC_IrattariHelyekSearch});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllForTerkep(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllForTerkep", new object[] {
                        ExecParam,
                        _EREC_IrattariHelyekSearch}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllForTerkep(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllForTerkepAsync(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
        {
            this.GetAllForTerkepAsync(ExecParam, _EREC_IrattariHelyekSearch, null);
        }

        /// <remarks/>
        public void GetAllForTerkepAsync(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch, object userState)
        {
            if ((this.GetAllForTerkepOperationCompleted == null))
            {
                this.GetAllForTerkepOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllForTerkepOperationCompleted);
            }
            this.InvokeAsync("GetAllForTerkep", new object[] {
                        ExecParam,
                        _EREC_IrattariHelyekSearch}, this.GetAllForTerkepOperationCompleted, userState);
        }

        private void OnGetAllForTerkepOperationCompleted(object arg)
        {
            if ((this.GetAllForTerkepCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllForTerkepCompleted(this, new GetAllForTerkepCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllForTerkepCompletedEventHandler(object sender, GetAllForTerkepCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllForTerkepCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllForTerkepCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region GetAllJogosultIrattariHely

        private System.Threading.SendOrPostCallback GetAllJogosultIrattariHelyOperationCompleted;

        /// <remarks/>
        public event GetAllJogosultIrattariHelyCompletedEventHandler GetAllJogosultIrattariHelyCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAllJogosultIrattariHely", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllJogosultIrattariHely(ExecParam execParam)
        {
            object[] results = this.Invoke("GetAllJogosultIrattariHely", new object[] {
                        execParam});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllJogosultIrattariHely(ExecParam execParam, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllJogosultIrattariHely", new object[] {
                        execParam}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllJogosultIrattariHely(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllJogosultIrattariHelyAsync(ExecParam execParam)
        {
            this.GetAllJogosultIrattariHelyAsync(execParam, null);
        }

        /// <remarks/>
        public void GetAllJogosultIrattariHelyAsync(ExecParam execParam, object userState)
        {
            if ((this.GetAllJogosultIrattariHelyOperationCompleted == null))
            {
                this.GetAllJogosultIrattariHelyOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllJogosultIrattariHelyOperationCompleted);
            }
            this.InvokeAsync("GetAllJogosultIrattariHely", new object[] {
                        execParam}, this.GetAllJogosultIrattariHelyOperationCompleted, userState);
        }

        private void OnGetAllJogosultIrattariHelyOperationCompleted(object arg)
        {
            if ((this.GetAllJogosultIrattariHelyCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllJogosultIrattariHelyCompleted(this, new GetAllJogosultIrattariHelyCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
        public delegate void GetAllJogosultIrattariHelyCompletedEventHandler(object sender, GetAllJogosultIrattariHelyCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllJogosultIrattariHelyCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllJogosultIrattariHelyCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion
    }
}
