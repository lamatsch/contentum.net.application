﻿namespace Contentum.eRecord.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;
    using System.Xml;

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseEREC_TomegesIktatas))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class EREC_TomegesIktatasService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        #region Insert with alairasSzablay
        private System.Threading.SendOrPostCallback InsertOperationCompleted;

        /// <remarks/>
        public event InsertCompletedEventHandler InsertCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/Insert", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Insert(ExecParam ExecParam, EREC_TomegesIktatas Record, string AlairoSzerep)
        {
            object[] results = this.Invoke("Insert", new object[] {
                        ExecParam,
                        Record,
                        AlairoSzerep});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsert(ExecParam ExecParam, EREC_TomegesIktatas Record, string AlairoSzerep, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Insert", new object[] {
                        ExecParam,
                        Record,
                        AlairoSzerep}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndInsert(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void InsertAsync(ExecParam ExecParam, EREC_TomegesIktatas Record, string AlairoSzerep)
        {
            this.InsertAsync(ExecParam, Record, AlairoSzerep, null);
        }

        /// <remarks/>
        public void InsertAsync(ExecParam ExecParam, EREC_TomegesIktatas Record, string AlairoSzerep, object userState)
        {
            if ((this.InsertOperationCompleted == null))
            {
                this.InsertOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertOperationCompleted);
            }
            this.InvokeAsync("Insert", new object[] {
                        ExecParam,
                        Record,
                        AlairoSzerep}, this.InsertOperationCompleted, userState);
        }

        private void OnInsertOperationCompleted(object arg)
        {
            if ((this.InsertCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertCompleted(this, new InsertCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void InsertCompletedEventHandler(object sender, InsertCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class InsertCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal InsertCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region Update with alairasSzabaly
        private System.Threading.SendOrPostCallback UpdateOperationCompleted;

        /// <remarks/>
        public event UpdateCompletedEventHandler UpdateCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/Update", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Update(ExecParam ExecParam, EREC_TomegesIktatas Record, string AlairoSzerep)
        {
            object[] results = this.Invoke("Update", new object[] {
                        ExecParam,
                        Record,
                        AlairoSzerep});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdate(ExecParam ExecParam, EREC_TomegesIktatas Record, string AlairoSzerep, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Update", new object[] {
                        ExecParam,
                        Record,
                        AlairoSzerep}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndUpdate(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void UpdateAsync(ExecParam ExecParam, EREC_TomegesIktatas Record, string AlairoSzerep)
        {
            this.UpdateAsync(ExecParam, Record, AlairoSzerep, null);
        }

        /// <remarks/>
        public void UpdateAsync(ExecParam ExecParam, EREC_TomegesIktatas Record, string AlairoSzerep, object userState)
        {
            if ((this.UpdateOperationCompleted == null))
            {
                this.UpdateOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateOperationCompleted);
            }
            this.InvokeAsync("Update", new object[] {
                        ExecParam,
                        Record,
                        AlairoSzerep}, this.UpdateOperationCompleted, userState);
        }

        private void OnUpdateOperationCompleted(object arg)
        {
            if ((this.UpdateCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateCompleted(this, new UpdateCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void UpdateCompletedEventHandler(object sender, UpdateCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class UpdateCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal UpdateCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region GetAlairoSzabylById
        private System.Threading.SendOrPostCallback GetAlairoSzabylByIdOperationCompleted;

        /// <remarks/>
        public event GetAlairoSzabylByIdCompletedEventHandler GetAlairoSzabylByIdCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAlairoSzabylById", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAlairoSzabylById(ExecParam ExecParam)
        {
            object[] results = this.Invoke("GetAlairoSzabylById", new object[] {
                        ExecParam});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAlairoSzabylById(ExecParam ExecParam, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAlairoSzabylById", new object[] {
                        ExecParam}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAlairoSzabylById(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAlairoSzabylByIdAsync(ExecParam ExecParam)
        {
            this.GetAlairoSzabylByIdAsync(ExecParam, null);
        }

        /// <remarks/>
        public void GetAlairoSzabylByIdAsync(ExecParam ExecParam, object userState)
        {
            if ((this.GetAlairoSzabylByIdOperationCompleted == null))
            {
                this.GetAlairoSzabylByIdOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAlairoSzabylByIdOperationCompleted);
            }
            this.InvokeAsync("GetAlairoSzabylById", new object[] {
                        ExecParam}, this.GetAlairoSzabylByIdOperationCompleted, userState);
        }

        private void OnGetAlairoSzabylByIdOperationCompleted(object arg)
        {
            if ((this.GetAlairoSzabylByIdCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAlairoSzabylByIdCompleted(this, new GetAlairoSzabylByIdCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAlairoSzabylByIdCompletedEventHandler(object sender, GetAlairoSzabylByIdCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAlairoSzabylByIdCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAlairoSzabylByIdCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region GetAlairasTipusok
        private System.Threading.SendOrPostCallback GetAlairasTipusokOperationCompleted;

        /// <remarks/>
        public event GetAlairasTipusokCompletedEventHandler GetAlairasTipusokCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAlairasTipusok", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAlairasTipusok(ExecParam ExecParam)
        {
            object[] results = this.Invoke("GetAlairasTipusok", new object[] {
                        ExecParam});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAlairasTipusok(ExecParam ExecParam, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAlairasTipusok", new object[] {
                        ExecParam}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAlairasTipusok(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAlairasTipusokAsync(ExecParam ExecParam)
        {
            this.GetAlairasTipusokAsync(ExecParam, null);
        }

        /// <remarks/>
        public void GetAlairasTipusokAsync(ExecParam ExecParam, object userState)
        {
            if ((this.GetAlairasTipusokOperationCompleted == null))
            {
                this.GetAlairasTipusokOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAlairasTipusokOperationCompleted);
            }
            this.InvokeAsync("GetAlairasTipusok", new object[] {
                        ExecParam}, this.GetAlairasTipusokOperationCompleted, userState);
        }

        private void OnGetAlairasTipusokOperationCompleted(object arg)
        {
            if ((this.GetAlairasTipusokCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAlairasTipusokCompleted(this, new GetAlairasTipusokCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAlairasTipusokCompletedEventHandler(object sender, GetAlairasTipusokCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAlairasTipusokCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAlairasTipusokCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region getAllWithFK
        private System.Threading.SendOrPostCallback GetAllWithFKOperationCompleted;

        /// <remarks/>
        public event GetAllWithFKCompletedEventHandler GetAllWithFKCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAllWithFK", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllWithFK(ExecParam ExecParam, EREC_TomegesIktatasSearch _EREC_TomegesIktatasSearch)
        {
            object[] results = this.Invoke("GetAllWithFK", new object[] {
                        ExecParam,
                        _EREC_TomegesIktatasSearch});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllWithFK(ExecParam ExecParam, EREC_TomegesIktatasSearch _EREC_TomegesIktatasSearch, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllWithFK", new object[] {
                        ExecParam,
                        _EREC_TomegesIktatasSearch}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllWithFK(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllWithFKAsync(ExecParam ExecParam, EREC_TomegesIktatasSearch _EREC_TomegesIktatasSearch)
        {
            this.GetAllWithFKAsync(ExecParam, _EREC_TomegesIktatasSearch, null);
        }

        /// <remarks/>
        public void GetAllWithFKAsync(ExecParam ExecParam, EREC_TomegesIktatasSearch _EREC_TomegesIktatasSearch, object userState)
        {
            if ((this.GetAllWithFKOperationCompleted == null))
            {
                this.GetAllWithFKOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWithFKOperationCompleted);
            }
            this.InvokeAsync("GetAllWithFK", new object[] {
                        ExecParam,
                        _EREC_TomegesIktatasSearch}, this.GetAllWithFKOperationCompleted, userState);
        }

        private void OnGetAllWithFKOperationCompleted(object arg)
        {
            if ((this.GetAllWithFKCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWithFKCompleted(this, new GetAllWithFKCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllWithFKCompletedEventHandler(object sender, GetAllWithFKCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllWithFKCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllWithFKCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region Tömegesiktatás, , CR3172 paraméter, hogy kell-e előzmény isElozmeny paraméter hozzáadása
        private System.Threading.SendOrPostCallback TomegesIktatasOperationCompleted;

        /// <remarks/>
        public event TomegesIktatasCompletedEventHandler TomegesIktatasCompleted;


        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/TomegesIktatas", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result TomegesIktatas(ExecParam ExecParam, XmlDocument xmlDoc, string IktatoKonyv, string IratTipus, string dbfFilePath, bool isElozmenyes)
        {
            object[] results = this.Invoke("TomegesIktatas", new object[] {
                        ExecParam,
                        xmlDoc,
                        IktatoKonyv,
                        IratTipus,
                        dbfFilePath,
                        isElozmenyes});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginTomegesIktatas(ExecParam ExecParam, XmlDocument xmlDoc, string IktatoKonyv, string IratTipus, string dbfFilePath, bool isElozmenyes, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("TomegesIktatas", new object[] {
                        ExecParam,
                        xmlDoc,
                        IktatoKonyv,
                        IratTipus,
                        dbfFilePath,
                        isElozmenyes}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndTomegesIktatas(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void TomegesIktatasAsync(ExecParam ExecParam, XmlDocument xmlDoc, string IktatoKonyv, string IratTipus, string dbfFilePath, bool isElozmenyes)
        {
            this.TomegesIktatasAsync(ExecParam, xmlDoc, IktatoKonyv, IratTipus, dbfFilePath, isElozmenyes, null);
        }

        /// <remarks/>
        public void TomegesIktatasAsync(ExecParam ExecParam, XmlDocument xmlDoc, string IktatoKonyv, string IratTipus, string dbfFilePath, bool isElozmenyes, object userState)
        {
            if ((this.TomegesIktatasOperationCompleted == null))
            {
                this.TomegesIktatasOperationCompleted = new System.Threading.SendOrPostCallback(this.OnTomegesIktatasOperationCompleted);
            }
            this.InvokeAsync("TomegesIktatas", new object[] {
                        ExecParam,
                        xmlDoc,
                        IktatoKonyv,
                        IratTipus,
                        dbfFilePath,
                        isElozmenyes}, this.TomegesIktatasOperationCompleted, userState);
        }

        private void OnTomegesIktatasOperationCompleted(object arg)
        {
            if ((this.TomegesIktatasCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.TomegesIktatasCompleted(this, new TomegesIktatasCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void TomegesIktatasCompletedEventHandler(object sender, TomegesIktatasCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class TomegesIktatasCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal TomegesIktatasCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region GetAlairasTipusokSzerepAlapjan
       
        private System.Threading.SendOrPostCallback GetAlairasTipusokSzerepAlapjanOperationCompleted;

        /// <remarks/>
        public event GetAlairasTipusokSzerepAlapjanCompletedEventHandler GetAlairasTipusokSzerepAlapjanCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAlairasTipusokSzerepAlapjan", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAlairasTipusokSzerepAlapjan(ExecParam ExecParam, string AlairoSzerep)
        {
            object[] results = this.Invoke("GetAlairasTipusokSzerepAlapjan", new object[] {
                        ExecParam, AlairoSzerep});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAlairasTipusokSzerepAlapjan(ExecParam ExecParam, string AlairoSzerep, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAlairasTipusokSzerepAlapjan", new object[] {
                        ExecParam, AlairoSzerep}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAlairasTipusokSzerepAlapjan(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAlairasTipusokSzerepAlapjanAsync(ExecParam ExecParam, string AlairoSzerep)
        {
            this.GetAlairasTipusokSzerepAlapjanAsync(ExecParam, AlairoSzerep, null);
        }

        /// <remarks/>
        public void GetAlairasTipusokSzerepAlapjanAsync(ExecParam ExecParam, string AlairoSzerep, object userState)
        {
            if ((this.GetAlairasTipusokSzerepAlapjanOperationCompleted == null))
            {
                this.GetAlairasTipusokSzerepAlapjanOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAlairasTipusokSzerepAlapjanOperationCompleted);
            }
            this.InvokeAsync("GetAlairasTipusokSzerepAlapjan", new object[] {
                        ExecParam, AlairoSzerep}, this.GetAlairasTipusokSzerepAlapjanOperationCompleted, userState);
        }

        private void OnGetAlairasTipusokSzerepAlapjanOperationCompleted(object arg)
        {
            if ((this.GetAlairasTipusokSzerepAlapjanCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAlairasTipusokSzerepAlapjanCompleted(this, new GetAlairasTipusokSzerepAlapjanCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAlairasTipusokSzerepAlapjanCompletedEventHandler(object sender, GetAlairasTipusokSzerepAlapjanCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAlairasTipusokSzerepAlapjanCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAlairasTipusokSzerepAlapjanCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion
    }
}