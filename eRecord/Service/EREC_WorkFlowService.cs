﻿namespace Contentum.eRecord.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "EREC_WorkFlowServiceSoap", Namespace = "Contentum.eRecord.WebService")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public class EREC_WorkFlowService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        public EREC_WorkFlowService(string ServiceUrl)
        {
            this.Url = ServiceUrl;
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        #region GetWorkFlowAdat

        private System.Threading.SendOrPostCallback GetWorkFlowAdatOperationCompleted;

        /// <remarks/>
        public event GetWorkFlowAdatCompletedEventHandler GetWorkFlowAdatCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetWorkFlowAdat", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetWorkFlowAdat(ExecParam ExecParam, string ContentType, string FazisErtek, string AllapotErtek, string Kimenet, string BazisObj_Id)
        {
            object[] results = this.Invoke("GetWorkFlowAdat", new object[] {
                        ExecParam,
                        ContentType,
                        FazisErtek,
                        AllapotErtek,
                        Kimenet,
                        BazisObj_Id});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetWorkFlowAdat(ExecParam ExecParam, string ContentType, string FazisErtek, string AllapotErtek, string Kimenet, string BazisObj_Id, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetWorkFlowAdat", new object[] {
                        ExecParam,
                        ContentType,
                        FazisErtek,
                        AllapotErtek,
                        Kimenet,
                        BazisObj_Id}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetWorkFlowAdat(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetWorkFlowAdatAsync(ExecParam ExecParam, string ContentType, string FazisErtek, string AllapotErtek, string Kimenet, string BazisObj_Id)
        {
            this.GetWorkFlowAdatAsync(ExecParam, ContentType, FazisErtek, AllapotErtek, Kimenet, BazisObj_Id, null);
        }

        /// <remarks/>
        public void GetWorkFlowAdatAsync(ExecParam ExecParam, string ContentType, string FazisErtek, string AllapotErtek, string Kimenet, string BazisObj_Id, object userState)
        {
            if ((this.GetWorkFlowAdatOperationCompleted == null))
            {
                this.GetWorkFlowAdatOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetWorkFlowAdatOperationCompleted);
            }
            this.InvokeAsync("GetWorkFlowAdat", new object[] {
                        ExecParam,
                        ContentType,
                        FazisErtek,
                        AllapotErtek,
                        Kimenet,
                        BazisObj_Id}, this.GetWorkFlowAdatOperationCompleted, userState);
        }

        private void OnGetWorkFlowAdatOperationCompleted(object arg)
        {
            if ((this.GetWorkFlowAdatCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetWorkFlowAdatCompleted(this, new GetWorkFlowAdatCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetWorkFlowAdatCompletedEventHandler(object sender, GetWorkFlowAdatCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetWorkFlowAdatCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetWorkFlowAdatCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion
    }
}
