namespace Contentum.eRecord.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;

    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseEREC_eMailBoritekCsatolmanyok))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class EREC_eMailBoritekCsatolmanyokService
    {
        private System.Threading.SendOrPostCallback GetAllWithExtensionOperationCompleted;

        /// <remarks/>
        public event GetAllWithExtensionCompletedEventHandler GetAllWithExtensionCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAllWithExtension", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllWithExtension(ExecParam ExecParam, EREC_eMailBoritekCsatolmanyokSearch _EREC_eMailBoritekCsatolmanyokSearch)
        {
            object[] results = this.Invoke("GetAllWithExtension", new object[] {
                        ExecParam,
                        _EREC_eMailBoritekCsatolmanyokSearch});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllWithExtension(ExecParam ExecParam, EREC_eMailBoritekCsatolmanyokSearch _EREC_eMailBoritekCsatolmanyokSearch, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllWithExtension", new object[] {
                        ExecParam,
                        _EREC_eMailBoritekCsatolmanyokSearch}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetAllWithExtension(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllWithExtensionAsync(ExecParam ExecParam, EREC_eMailBoritekCsatolmanyokSearch _EREC_eMailBoritekCsatolmanyokSearch)
        {
            this.GetAllWithExtensionAsync(ExecParam, _EREC_eMailBoritekCsatolmanyokSearch, null);
        }

        /// <remarks/>
        public void GetAllWithExtensionAsync(ExecParam ExecParam, EREC_eMailBoritekCsatolmanyokSearch _EREC_eMailBoritekCsatolmanyokSearch, object userState)
        {
            if ((this.GetAllWithExtensionOperationCompleted == null))
            {
                this.GetAllWithExtensionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWithExtensionOperationCompleted);
            }
            this.InvokeAsync("GetAllWithExtension", new object[] {
                        ExecParam,
                        _EREC_eMailBoritekCsatolmanyokSearch}, this.GetAllWithExtensionOperationCompleted, userState);
        }

        private void OnGetAllWithExtensionOperationCompleted(object arg)
        {
            if ((this.GetAllWithExtensionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWithExtensionCompleted(this, new GetAllWithExtensionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllWithExtensionCompletedEventHandler(object sender, GetAllWithExtensionCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllWithExtensionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllWithExtensionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        /*        DocumentumCsatolas            */

        private System.Threading.SendOrPostCallback DocumentumCsatolasOperationCompleted;
        /// <remarks/>
        public event DocumentumCsatolasCompletedEventHandler DocumentumCsatolasCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/DocumentumCsatolas", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result DocumentumCsatolas(ExecParam ExecParam, EREC_eMailBoritekok erec_eMailBoritek, Csatolmany[] csatolmanyok, string egyebParamsXml)
        {
            object[] results = this.Invoke("DocumentumCsatolas", new object[] {
                        ExecParam,
                        erec_eMailBoritek,
                        csatolmanyok,
                        egyebParamsXml});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDocumentumCsatolas(ExecParam ExecParam, EREC_eMailBoritekok erec_eMailBoritek, Csatolmany[] csatolmanyok, string egyebParamsXml, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DocumentumCsatolas", new object[] {
                        ExecParam,
                        erec_eMailBoritek,
                        csatolmanyok,
                        egyebParamsXml}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndDocumentumCsatolas(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void DocumentumCsatolasAsync(ExecParam ExecParam, EREC_eMailBoritekok erec_eMailBoritek, Csatolmany[] csatolmanyok, string egyebParamsXml)
        {
            this.DocumentumCsatolasAsync(ExecParam, erec_eMailBoritek, csatolmanyok, egyebParamsXml, null);
        }

        /// <remarks/>
        public void DocumentumCsatolasAsync(ExecParam ExecParam, EREC_eMailBoritekok erec_eMailBoritek, Csatolmany[] csatolmanyok, string egyebParamsXml, object userState)
        {
            if ((this.DocumentumCsatolasOperationCompleted == null))
            {
                this.DocumentumCsatolasOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDocumentumCsatolasOperationCompleted);
            }
            this.InvokeAsync("DocumentumCsatolas", new object[] {
                        ExecParam,
                        erec_eMailBoritek,
                        csatolmanyok,
                        egyebParamsXml}, this.DocumentumCsatolasOperationCompleted, userState);
        }

        private void OnDocumentumCsatolasOperationCompleted(object arg)
        {
            if ((this.DocumentumCsatolasCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DocumentumCsatolasCompleted(this, new DocumentumCsatolasCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void DocumentumCsatolasCompletedEventHandler(object sender, DocumentumCsatolasCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class DocumentumCsatolasCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal DocumentumCsatolasCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        /*        /DocumentumCsatolas            */


    }
}
