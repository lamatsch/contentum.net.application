//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.42
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=2.0.50727.42.
// 
namespace Contentum.eRecord.Service
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    using System.Data;
    using Contentum.eBusinessDocuments;
    using Contentum.eQuery.BusinessDocuments;

    /// <remarks/>
    //[System.Web.Services.WebServiceBindingAttribute(Name = "EREC_IraJegyzekekServiceSoap", Namespace = "Contentum.eRecord.WebService")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseEREC_IraJegyzekek))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]
    public partial class KRT_BarkodokService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {


        #region Atadas_Tomeges

        private System.Threading.SendOrPostCallback Atadas_TomegesOperationCompleted;

        /// <remarks/>
        public event Atadas_TomegesCompletedEventHandler Atadas_TomegesCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/Atadas_Tomeges", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Atadas_Tomeges(ExecParam execParam, string[] erec_UgyUgyiratok_Id_Array, string[] erec_KuldKuldemenyek_Id_Array, string[] erec_PldIratPeldanyok_Id_Array, string[] krt_mappak_Id_Array, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            object[] results = this.Invoke("Atadas_Tomeges", new object[] {
                        execParam,
                        erec_UgyUgyiratok_Id_Array,
                        erec_KuldKuldemenyek_Id_Array,
                        erec_PldIratPeldanyok_Id_Array,
                        krt_mappak_Id_Array,
                        csoport_Id_Felelos_Kovetkezo,
                        erec_HataridosFeladatok});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginAtadas_Tomeges(ExecParam execParam, string[] erec_UgyUgyiratok_Id_Array, string[] erec_KuldKuldemenyek_Id_Array, string[] erec_PldIratPeldanyok_Id_Array, string[] krt_mappak_Id_Array, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Atadas_Tomeges", new object[] {
                        execParam,
                        erec_UgyUgyiratok_Id_Array,
                        erec_KuldKuldemenyek_Id_Array,
                        erec_PldIratPeldanyok_Id_Array,
                        krt_mappak_Id_Array,
                        csoport_Id_Felelos_Kovetkezo,
                        erec_HataridosFeladatok}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndAtadas_Tomeges(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void Atadas_TomegesAsync(ExecParam execParam, string[] erec_UgyUgyiratok_Id_Array, string[] erec_KuldKuldemenyek_Id_Array, string[] erec_PldIratPeldanyok_Id_Array, string[] krt_mappak_Id_Array, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            this.Atadas_TomegesAsync(execParam, erec_UgyUgyiratok_Id_Array, erec_KuldKuldemenyek_Id_Array, erec_PldIratPeldanyok_Id_Array, krt_mappak_Id_Array, csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok, null);
        }

        /// <remarks/>
        public void Atadas_TomegesAsync(ExecParam execParam, string[] erec_UgyUgyiratok_Id_Array, string[] erec_KuldKuldemenyek_Id_Array, string[] erec_PldIratPeldanyok_Id_Array, string[] krt_mappak_Id_Array, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok, object userState)
        {
            if ((this.Atadas_TomegesOperationCompleted == null))
            {
                this.Atadas_TomegesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnAtadas_TomegesOperationCompleted);
            }
            this.InvokeAsync("Atadas_Tomeges", new object[] {
                        execParam,
                        erec_UgyUgyiratok_Id_Array,
                        erec_KuldKuldemenyek_Id_Array,
                        erec_PldIratPeldanyok_Id_Array,
                        krt_mappak_Id_Array,
                        csoport_Id_Felelos_Kovetkezo,
                        erec_HataridosFeladatok}, this.Atadas_TomegesOperationCompleted, userState);
        }

        private void OnAtadas_TomegesOperationCompleted(object arg)
        {
            if ((this.Atadas_TomegesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.Atadas_TomegesCompleted(this, new Atadas_TomegesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
        public delegate void Atadas_TomegesCompletedEventHandler(object sender, Atadas_TomegesCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class Atadas_TomegesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal Atadas_TomegesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region Atvetel_Tomeges

        private System.Threading.SendOrPostCallback Atvetel_TomegesOperationCompleted;

        /// <remarks/>
        public event Atvetel_TomegesCompletedEventHandler Atvetel_TomegesCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/Atvetel_Tomeges", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result Atvetel_Tomeges(ExecParam execParam, string[] erec_UgyUgyiratok_Id_Array, string[] erec_KuldKuldemenyek_Id_Array, string[] erec_PldIratPeldanyok_Id_Array, string[] krt_mappak_Id_Array)
        {
            object[] results = this.Invoke("Atvetel_Tomeges", new object[] {
                        execParam,
                        erec_UgyUgyiratok_Id_Array,
                        erec_KuldKuldemenyek_Id_Array,
                        erec_PldIratPeldanyok_Id_Array,
                        krt_mappak_Id_Array});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginAtvetel_Tomeges(ExecParam execParam, string[] erec_UgyUgyiratok_Id_Array, string[] erec_KuldKuldemenyek_Id_Array, string[] erec_PldIratPeldanyok_Id_Array, string[] krt_mappak_Id_Array, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("Atvetel_Tomeges", new object[] {
                        execParam,
                        erec_UgyUgyiratok_Id_Array,
                        erec_KuldKuldemenyek_Id_Array,
                        erec_PldIratPeldanyok_Id_Array,
                        krt_mappak_Id_Array}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndAtvetel_Tomeges(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void Atvetel_TomegesAsync(ExecParam execParam, string[] erec_UgyUgyiratok_Id_Array, string[] erec_KuldKuldemenyek_Id_Array, string[] erec_PldIratPeldanyok_Id_Array, string[] krt_mappak_Id_Array)
        {
            this.Atvetel_TomegesAsync(execParam, erec_UgyUgyiratok_Id_Array, erec_KuldKuldemenyek_Id_Array, erec_PldIratPeldanyok_Id_Array, krt_mappak_Id_Array, null);
        }

        /// <remarks/>
        public void Atvetel_TomegesAsync(ExecParam execParam, string[] erec_UgyUgyiratok_Id_Array, string[] erec_KuldKuldemenyek_Id_Array, string[] erec_PldIratPeldanyok_Id_Array, string[] krt_mappak_Id_Array, object userState)
        {
            if ((this.Atvetel_TomegesOperationCompleted == null))
            {
                this.Atvetel_TomegesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnAtvetel_TomegesOperationCompleted);
            }
            this.InvokeAsync("Atvetel_Tomeges", new object[] {
                        execParam,
                        erec_UgyUgyiratok_Id_Array,
                        erec_KuldKuldemenyek_Id_Array,
                        erec_PldIratPeldanyok_Id_Array,
                        krt_mappak_Id_Array}, this.Atvetel_TomegesOperationCompleted, userState);
        }

        private void OnAtvetel_TomegesOperationCompleted(object arg)
        {
            if ((this.Atvetel_TomegesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.Atvetel_TomegesCompleted(this, new Atvetel_TomegesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
        public delegate void Atvetel_TomegesCompletedEventHandler(object sender, Atvetel_TomegesCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class Atvetel_TomegesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal Atvetel_TomegesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region GetAllWithExtension

        private System.Threading.SendOrPostCallback GetAllWithExtensionOperationCompleted;

        /// <remarks/>
        public event GetAllWithExtensionCompletedEventHandler GetAllWithExtensionCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetAllWithExtension", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetAllWithExtension(ExecParam execParam, KRT_BarkodokSearch krt_barkodokSearch)
        {
            object[] results = this.Invoke("GetAllWithExtension", new object[] {
                        execParam,
                        krt_barkodokSearch});


            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetAllWithExtension(ExecParam execParam, KRT_BarkodokSearch krt_barkodokSearch, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetAllWithExtension", new object[] {
                        execParam,
                        krt_barkodokSearch}, callback, asyncState);


        }

        /// <remarks/>
        public Result EndGetAllWithExtension(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetAllWithExtensionAsync(ExecParam execParam, KRT_BarkodokSearch krt_barkodokSearch)
        {
            this.GetAllWithExtensionAsync(execParam, krt_barkodokSearch, null);
        }

        /// <remarks/>
        public void GetAllWithExtensionAsync(ExecParam execParam, KRT_BarkodokSearch krt_barkodokSearch, object userState)
        {
            if ((this.GetAllWithExtensionOperationCompleted == null))
            {
                this.GetAllWithExtensionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWithExtensionOperationCompleted);
            }
            this.InvokeAsync("GetAllWithExtension", new object[] {
                        execParam,
                        krt_barkodokSearch}, this.GetAllWithExtensionOperationCompleted, userState);


        }

        private void OnGetAllWithExtensionOperationCompleted(object arg)
        {
            if ((this.GetAllWithExtensionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWithExtensionCompleted(this, new GetAllWithExtensionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetAllWithExtensionCompletedEventHandler(object sender, GetAllWithExtensionCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetAllWithExtensionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetAllWithExtensionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region CheckBarcode

        private System.Threading.SendOrPostCallback CheckBarcodeOperationCompleted;

        /// <remarks/>
        public event CheckBarcodeCompletedEventHandler CheckBarcodeCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/CheckBarcode", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result CheckBarcode(ExecParam execParam, string barkodValue)
        {
            object[] results = this.Invoke("CheckBarcode", new object[] {
                        execParam,
                        barkodValue});


            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginCheckBarcode(ExecParam execParam, string barkodValue, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("CheckBarcode", new object[] {
                        execParam,
                        barkodValue}, callback, asyncState);


        }

        /// <remarks/>
        public Result EndCheckBarcode(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void CheckBarcodeAsync(ExecParam execParam, string barkodValue)
        {
            this.CheckBarcodeAsync(execParam, barkodValue, null);
        }

        /// <remarks/>
        public void CheckBarcodeAsync(ExecParam execParam, string barkodValue, object userState)
        {
            if ((this.CheckBarcodeOperationCompleted == null))
            {
                this.CheckBarcodeOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCheckBarcodeOperationCompleted);
            }
            this.InvokeAsync("CheckBarcode", new object[] {
                        execParam,
                        barkodValue}, this.CheckBarcodeOperationCompleted, userState);


        }

        private void OnCheckBarcodeOperationCompleted(object arg)
        {
            if ((this.CheckBarcodeCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CheckBarcodeCompleted(this, new CheckBarcodeCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void CheckBarcodeCompletedEventHandler(object sender, CheckBarcodeCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class CheckBarcodeCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal CheckBarcodeCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region BarCodeBindToObject

        private System.Threading.SendOrPostCallback BarCodeBindToObjectOperationCompleted;

        /// <remarks/>
        public event BarCodeBindToObjectCompletedEventHandler BarCodeBindToObjectCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/BarCodeBindToObject", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result BarCodeBindToObject(ExecParam execParam, string barkod_Id, string Obj_Id, string Obj_type)
        {
            object[] results = this.Invoke("BarCodeBindToObject", new object[] {
                         execParam,  barkod_Id,  Obj_Id,  Obj_type});


            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginBarCodeBindToObject(ExecParam execParam, string barkod_Id, string Obj_Id, string Obj_type, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("BarCodeBindToObject", new object[] {
                         execParam,  barkod_Id,  Obj_Id,  Obj_type
                        }, callback, asyncState);
        }

        /// <remarks/>
        public Result EndBarCodeBindToObject(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void BarCodeBindToObjectAsync(ExecParam execParam, string barkod_Id, string Obj_Id, string Obj_type)
        {
            this.BarCodeBindToObjectAsync(execParam, barkod_Id, Obj_Id, Obj_type, null);
        }

        /// <remarks/>
        public void BarCodeBindToObjectAsync(ExecParam execParam, string barkod_Id, string Obj_Id, string Obj_type, object userState)
        {
            if ((this.BarCodeBindToObjectOperationCompleted == null))
            {
                this.BarCodeBindToObjectOperationCompleted = new System.Threading.SendOrPostCallback(this.OnBarCodeBindToObjectOperationCompleted);
            }
            this.InvokeAsync("BarCodeBindToObject", new object[] {
                         execParam,  barkod_Id,  Obj_Id,  Obj_type
                        }, this.BarCodeBindToObjectOperationCompleted, userState);
        }

        private void OnBarCodeBindToObjectOperationCompleted(object arg)
        {
            if ((this.BarCodeBindToObjectCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.BarCodeBindToObjectCompleted(this, new BarCodeBindToObjectCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void BarCodeBindToObjectCompletedEventHandler(object sender, BarCodeBindToObjectCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class BarCodeBindToObjectCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal BarCodeBindToObjectCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region BARKODSAV_IGENYLES
        private System.Threading.SendOrPostCallback BarkodSav_IgenylesOperationCompleted;
        /// <remarks/>
        public event BarkodSav_IgenylesCompletedEventHandler BarkodSav_IgenylesCompleted;
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/BarkodSav_Igenyles", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result BarkodSav_Igenyles(ExecParam ExecParam, string SavType, int FoglalandoDarab)
        {
            object[] results = this.Invoke("BarkodSav_Igenyles", new object[] {
                        ExecParam,
                        SavType,
                        FoglalandoDarab});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginBarkodSav_Igenyles(ExecParam ExecParam, string SavType, int FoglalandoDarab, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("BarkodSav_Igenyles", new object[] {
                        ExecParam,
                        SavType,
                        FoglalandoDarab}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndBarkodSav_Igenyles(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void BarkodSav_IgenylesAsync(ExecParam ExecParam, string SavType, int FoglalandoDarab)
        {
            this.BarkodSav_IgenylesAsync(ExecParam, SavType, FoglalandoDarab, null);
        }

        /// <remarks/>
        public void BarkodSav_IgenylesAsync(ExecParam ExecParam, string SavType, int FoglalandoDarab, object userState)
        {
            if ((this.BarkodSav_IgenylesOperationCompleted == null))
            {
                this.BarkodSav_IgenylesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnBarkodSav_IgenylesOperationCompleted);
            }
            this.InvokeAsync("BarkodSav_Igenyles", new object[] {
                        ExecParam,
                        SavType,
                        FoglalandoDarab}, this.BarkodSav_IgenylesOperationCompleted, userState);
        }

        private void OnBarkodSav_IgenylesOperationCompleted(object arg)
        {
            if ((this.BarkodSav_IgenylesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.BarkodSav_IgenylesCompleted(this, new BarkodSav_IgenylesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
		[System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void BarkodSav_IgenylesCompletedEventHandler(object sender, BarkodSav_IgenylesCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class BarkodSav_IgenylesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal BarkodSav_IgenylesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion
    }
}