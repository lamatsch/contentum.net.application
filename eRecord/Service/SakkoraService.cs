﻿
namespace Contentum.eRecord.Service
{
    using Contentum.eBusinessDocuments;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "SakkoraServiceSoap", Namespace = "Contentum.eRecord.WebService")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseEREC_IraIratok))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseEREC_UgyUgyiratok))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseKRT_KodTarak))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseKRT_KodCsoportok))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Base_BaseDocument))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BaseExecParam))]

    public partial class SakkoraService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ServiceUrl"></param>
        public SakkoraService(string ServiceUrl)
        {
            this.Url = ServiceUrl;//ServiceUrl;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public SakkoraService()
        {

        }
        #region CanSetSakkoraStatus

        private System.Threading.SendOrPostCallback CanSetSakkoraStatusOperationCompleted;

        /// <remarks/>
        public event CanSetSakkoraStatusCompletedEventHandler CanSetSakkoraStatusCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/CanSetSakkoraStatus", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result CanSetSakkoraStatus(ExecParam execParam, string ugyiratId, string sakkoraKod)
        {
            object[] results = this.Invoke("CanSetSakkoraStatus", new object[] {
                        execParam,
                        ugyiratId,
                        sakkoraKod });
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginCanSetSakkoraStatus(ExecParam execParam, string ugyiratId, string sakkoraKod, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("CanSetSakkoraStatus", new object[] {
                        execParam,
                        ugyiratId, sakkoraKod}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndCanSetSakkoraStatus(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void CanSetSakkoraStatusAsync(ExecParam execParam, string ugyiratId, string sakkoraKod)
        {
            this.CanSetSakkoraStatusAsync(execParam, ugyiratId, sakkoraKod, null);
        }

        /// <remarks/>
        public void CanSetSakkoraStatusAsync(ExecParam execParam, string ugyiratId, string sakkoraKod, object userState)
        {
            if ((this.CanSetSakkoraStatusOperationCompleted == null))
            {
                this.CanSetSakkoraStatusOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCanSetSakkoraStatusOperationCompleted);
            }
            this.InvokeAsync("CanSetSakkoraStatus", new object[] {
                        execParam,
                         ugyiratId,  sakkoraKod}, this.CanSetSakkoraStatusOperationCompleted, userState);
        }

        private void OnCanSetSakkoraStatusOperationCompleted(object arg)
        {
            if ((this.CanSetSakkoraStatusCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CanSetSakkoraStatusCompleted(this, new CanSetSakkoraStatusCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void CanSetSakkoraStatusCompletedEventHandler(object sender, CanSetSakkoraStatusCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class CanSetSakkoraStatusCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal CanSetSakkoraStatusCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region SetSakkoraStatus

        private System.Threading.SendOrPostCallback SetSakkoraStatusOperationCompleted;

        /// <remarks/>
        public event SetSakkoraStatusCompletedEventHandler SetSakkoraStatusCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/SetSakkoraStatus", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result SetSakkoraStatus(ExecParam execParam, string iratId, string ugyiratId, string sakkoraStatusz, string okKod, string userID)
        {
            object[] results = this.Invoke("SetSakkoraStatus", new object[] {
                        execParam,
                         iratId,  ugyiratId,  sakkoraStatusz,  okKod,  userID});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginSetSakkoraStatus(ExecParam execParam, string iratId, string ugyiratId, string sakkoraStatusz, string okKod, string userID, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("SetSakkoraStatus", new object[] {
                        execParam,
                         iratId,  ugyiratId,  sakkoraStatusz,  okKod,  userID}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndSetSakkoraStatus(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void SetSakkoraStatusAsync(ExecParam execParam, string iratId, string ugyiratId, string sakkoraStatusz, string okKod, string userID)
        {
            this.SetSakkoraStatusAsync(execParam, iratId, ugyiratId, sakkoraStatusz, okKod, userID, null);
        }

        /// <remarks/>
        public void SetSakkoraStatusAsync(ExecParam execParam, string iratId, string ugyiratId, string sakkoraStatusz, string okKod, string userID, object userState)
        {
            if ((this.SetSakkoraStatusOperationCompleted == null))
            {
                this.SetSakkoraStatusOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSetSakkoraStatusOperationCompleted);
            }
            this.InvokeAsync("SetSakkoraStatus", new object[] {
                        execParam,
                        iratId,  ugyiratId,  sakkoraStatusz,  okKod,  userID}, this.SetSakkoraStatusOperationCompleted, userState);
        }

        private void OnSetSakkoraStatusOperationCompleted(object arg)
        {
            if ((this.SetSakkoraStatusCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SetSakkoraStatusCompleted(this, new SetSakkoraStatusCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void SetSakkoraStatusCompletedEventHandler(object sender, SetSakkoraStatusCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class SetSakkoraStatusCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal SetSakkoraStatusCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region GetSakkoraHataridok

        private System.Threading.SendOrPostCallback GetSakkoraHataridokOperationCompleted;

        /// <remarks/>
        public event GetSakkoraHataridokCompletedEventHandler GetSakkoraHataridokCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetSakkoraHataridok", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetSakkoraHataridok(ExecParam execParam, string ugyiratId, bool hataridoMarNovelve)
        {
            object[] results = this.Invoke("GetSakkoraHataridok", new object[] {
                        execParam
                        ,ugyiratId,  hataridoMarNovelve});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetSakkoraHataridok(ExecParam execParam, string ugyiratId, bool hataridoMarNovelve, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetSakkoraHataridok", new object[] {
                        execParam,
                        ugyiratId,  hataridoMarNovelve}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetSakkoraHataridok(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetSakkoraHataridokAsync(ExecParam execParam, string ugyiratId, bool hataridoMarNovelve)
        {
            this.GetSakkoraHataridokAsync(execParam, ugyiratId, hataridoMarNovelve, null);
        }

        /// <remarks/>
        public void GetSakkoraHataridokAsync(ExecParam execParam, string ugyiratId, bool hataridoMarNovelve, object userState)
        {
            if ((this.GetSakkoraHataridokOperationCompleted == null))
            {
                this.GetSakkoraHataridokOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetSakkoraHataridokOperationCompleted);
            }
            this.InvokeAsync("GetSakkoraHataridok", new object[] {
                        execParam,
                        ugyiratId,  hataridoMarNovelve}, this.GetSakkoraHataridokOperationCompleted, userState);
        }

        private void OnGetSakkoraHataridokOperationCompleted(object arg)
        {
            if ((this.GetSakkoraHataridokCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetSakkoraHataridokCompleted(this, new GetSakkoraHataridokCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetSakkoraHataridokCompletedEventHandler(object sender, GetSakkoraHataridokCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetSakkoraHataridokCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetSakkoraHataridokCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion

        #region SAKKORA_UGYINTEZESI_IDO_UJRA_SZAMOLAS

        private System.Threading.SendOrPostCallback SakkoraUgyintezesiIdoUjraSzamolasOperationCompleted;

        /// <remarks/>
        public event SakkoraUgyintezesiIdoUjraSzamolasCompletedEventHandler SakkoraUgyintezesiIdoUjraSzamolasCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/SakkoraUgyintezesiIdoUjraSzamolas", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result SakkoraUgyintezesiIdoUjraSzamolas(ExecParam execParam, string ugyiratId)
        {
            object[] results = this.Invoke("SakkoraUgyintezesiIdoUjraSzamolas", new object[] {
                        execParam
                        ,ugyiratId});
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginSakkoraUgyintezesiIdoUjraSzamolas(ExecParam execParam, string ugyiratId, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("SakkoraUgyintezesiIdoUjraSzamolas", new object[] {
                        execParam,
                        ugyiratId}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndSakkoraUgyintezesiIdoUjraSzamolas(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void SakkoraUgyintezesiIdoUjraSzamolasAsync(ExecParam execParam, string ugyiratId)
        {
            this.SakkoraUgyintezesiIdoUjraSzamolasAsync(execParam, ugyiratId, null);
        }

        /// <remarks/>
        public void SakkoraUgyintezesiIdoUjraSzamolasAsync(ExecParam execParam, string ugyiratId, object userState)
        {
            if ((this.SakkoraUgyintezesiIdoUjraSzamolasOperationCompleted == null))
            {
                this.SakkoraUgyintezesiIdoUjraSzamolasOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSakkoraUgyintezesiIdoUjraSzamolasOperationCompleted);
            }
            this.InvokeAsync("SakkoraUgyintezesiIdoUjraSzamolas", new object[] {
                        execParam,
                        ugyiratId}, this.SakkoraUgyintezesiIdoUjraSzamolasOperationCompleted, userState);
        }

        private void OnSakkoraUgyintezesiIdoUjraSzamolasOperationCompleted(object arg)
        {
            if ((this.SakkoraUgyintezesiIdoUjraSzamolasCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SakkoraUgyintezesiIdoUjraSzamolasCompleted(this, new SakkoraUgyintezesiIdoUjraSzamolasCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void SakkoraUgyintezesiIdoUjraSzamolasCompletedEventHandler(object sender, SakkoraUgyintezesiIdoUjraSzamolasCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class SakkoraUgyintezesiIdoUjraSzamolasCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal SakkoraUgyintezesiIdoUjraSzamolasCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }

        #endregion


        #region CANSETSAKKORA

        private System.Threading.SendOrPostCallback CanSetSakkoraOperationCompleted;

        /// <remarks/>
        public event CanSetSakkoraCompletedEventHandler CanSetSakkoraCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/CanSetSakkora", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result CanSetSakkora(ExecParam execParam, string iratId, string ugyiratId, string sakkoraKod)
        {
            object[] results = this.Invoke("CanSetSakkora", new object[] {
                        execParam,
                        iratId,
                        ugyiratId,
                        sakkoraKod });
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginCanSetSakkora(ExecParam execParam, string iratId, string ugyiratId, string sakkoraKod, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("CanSetSakkora", new object[] {
                        execParam,
                        iratId, ugyiratId, sakkoraKod}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndCanSetSakkora(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void CanSetSakkoraAsync(ExecParam execParam, string iratId, string ugyiratId, string sakkoraKod)
        {
            this.CanSetSakkoraAsync(execParam, iratId, ugyiratId, sakkoraKod, null);
        }

        /// <remarks/>
        public void CanSetSakkoraAsync(ExecParam execParam, string iratId, string ugyiratId, string sakkoraKod, object userState)
        {
            if ((this.CanSetSakkoraOperationCompleted == null))
            {
                this.CanSetSakkoraOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCanSetSakkoraOperationCompleted);
            }
            this.InvokeAsync("CanSetSakkora", new object[] {
                        execParam,
                         iratId ,ugyiratId,  sakkoraKod}, this.CanSetSakkoraOperationCompleted, userState);
        }

        private void OnCanSetSakkoraOperationCompleted(object arg)
        {
            if ((this.CanSetSakkoraCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CanSetSakkoraCompleted(this, new CanSetSakkoraCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void CanSetSakkoraCompletedEventHandler(object sender, CanSetSakkoraCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class CanSetSakkoraCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal CanSetSakkoraCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region GETSAKKORALATHATOALLAPOTOK

        private System.Threading.SendOrPostCallback GetSakkoraLathatoAllapotokOperationCompleted;

        /// <remarks/>
        public event GetSakkoraLathatoAllapotokCompletedEventHandler GetSakkoraLathatoAllapotokCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/GetSakkoraLathatoAllapotok", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result GetSakkoraLathatoAllapotok(ExecParam execParam, string iratId, string ugyiratId)
        {
            object[] results = this.Invoke("GetSakkoraLathatoAllapotok", new object[] {
                        execParam,
                        iratId,
                        ugyiratId,
                        });
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetSakkoraLathatoAllapotok(ExecParam execParam, string iratId, string ugyiratId, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetSakkoraLathatoAllapotok", new object[] {
                        execParam,
                        iratId, ugyiratId}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndGetSakkoraLathatoAllapotok(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void GetSakkoraLathatoAllapotokAsync(ExecParam execParam, string iratId, string ugyiratId)
        {
            this.GetSakkoraLathatoAllapotokAsync(execParam, iratId, ugyiratId, null);
        }

        /// <remarks/>
        public void GetSakkoraLathatoAllapotokAsync(ExecParam execParam, string iratId, string ugyiratId, object userState)
        {
            if ((this.GetSakkoraLathatoAllapotokOperationCompleted == null))
            {
                this.GetSakkoraLathatoAllapotokOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetSakkoraLathatoAllapotokOperationCompleted);
            }
            this.InvokeAsync("GetSakkoraLathatoAllapotok", new object[] {
                        execParam,
                         iratId ,ugyiratId}, this.GetSakkoraLathatoAllapotokOperationCompleted, userState);
        }

        private void OnGetSakkoraLathatoAllapotokOperationCompleted(object arg)
        {
            if ((this.GetSakkoraLathatoAllapotokCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetSakkoraLathatoAllapotokCompleted(this, new GetSakkoraLathatoAllapotokCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void GetSakkoraLathatoAllapotokCompletedEventHandler(object sender, GetSakkoraLathatoAllapotokCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class GetSakkoraLathatoAllapotokCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal GetSakkoraLathatoAllapotokCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region SAKKORAFELFUGGESZTETTALLAPOTESETENHATARIDONOVELES

        private System.Threading.SendOrPostCallback SakkoraFelfuggesztettAllapotEsetenHataridoNovelesOperationCompleted;

        /// <remarks/>
        public event SakkoraFelfuggesztettAllapotEsetenHataridoNovelesCompletedEventHandler SakkoraFelfuggesztettAllapotEsetenHataridoNovelesCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/SakkoraFelfuggesztettAllapotEsetenHataridoNoveles", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result SakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ExecParam execParam, string ugyiratId)
        {
            object[] results = this.Invoke("SakkoraFelfuggesztettAllapotEsetenHataridoNoveles", new object[] {
                        execParam,
                        ugyiratId,
                        });
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ExecParam execParam, string ugyiratId, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("SakkoraFelfuggesztettAllapotEsetenHataridoNoveles", new object[] {
                        execParam,
                        ugyiratId}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndSakkoraFelfuggesztettAllapotEsetenHataridoNoveles(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void SakkoraFelfuggesztettAllapotEsetenHataridoNovelesAsync(ExecParam execParam, string ugyiratId)
        {
            this.SakkoraFelfuggesztettAllapotEsetenHataridoNovelesAsync(execParam, ugyiratId, null);
        }

        /// <remarks/>
        public void SakkoraFelfuggesztettAllapotEsetenHataridoNovelesAsync(ExecParam execParam, string ugyiratId, object userState)
        {
            if ((this.SakkoraFelfuggesztettAllapotEsetenHataridoNovelesOperationCompleted == null))
            {
                this.SakkoraFelfuggesztettAllapotEsetenHataridoNovelesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSakkoraFelfuggesztettAllapotEsetenHataridoNovelesOperationCompleted);
            }
            this.InvokeAsync("SakkoraFelfuggesztettAllapotEsetenHataridoNoveles", new object[] {
                        execParam,
                        ugyiratId}, this.SakkoraFelfuggesztettAllapotEsetenHataridoNovelesOperationCompleted, userState);
        }

        private void OnSakkoraFelfuggesztettAllapotEsetenHataridoNovelesOperationCompleted(object arg)
        {
            if ((this.SakkoraFelfuggesztettAllapotEsetenHataridoNovelesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SakkoraFelfuggesztettAllapotEsetenHataridoNovelesCompleted(this, new SakkoraFelfuggesztettAllapotEsetenHataridoNovelesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void SakkoraFelfuggesztettAllapotEsetenHataridoNovelesCompletedEventHandler(object sender, SakkoraFelfuggesztettAllapotEsetenHataridoNovelesCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class SakkoraFelfuggesztettAllapotEsetenHataridoNovelesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal SakkoraFelfuggesztettAllapotEsetenHataridoNovelesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion

        #region SAKKORAELTELTIDONOVELES

        private System.Threading.SendOrPostCallback SakkoraElteltIdoNovelesOperationCompleted;

        /// <remarks/>
        public event SakkoraElteltIdoNovelesCompletedEventHandler SakkoraElteltIdoNovelesCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("Contentum.eRecord.WebService/SakkoraElteltIdoNoveles", RequestNamespace = "Contentum.eRecord.WebService", ResponseNamespace = "Contentum.eRecord.WebService", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Result SakkoraElteltIdoNoveles(ExecParam execParam, string ugyiratId)
        {
            object[] results = this.Invoke("SakkoraElteltIdoNoveles", new object[] {
                        execParam,
                        ugyiratId,
                        });
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginSakkoraElteltIdoNoveles(ExecParam execParam, string ugyiratId, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("SakkoraElteltIdoNoveles", new object[] {
                        execParam,
                        ugyiratId}, callback, asyncState);
        }

        /// <remarks/>
        public Result EndSakkoraElteltIdoNoveles(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((Result)(results[0]));
        }

        /// <remarks/>
        public void SakkoraElteltIdoNovelesAsync(ExecParam execParam, string ugyiratId)
        {
            this.SakkoraElteltIdoNovelesAsync(execParam, ugyiratId, null);
        }

        /// <remarks/>
        public void SakkoraElteltIdoNovelesAsync(ExecParam execParam, string ugyiratId, object userState)
        {
            if ((this.SakkoraElteltIdoNovelesOperationCompleted == null))
            {
                this.SakkoraElteltIdoNovelesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSakkoraElteltIdoNovelesOperationCompleted);
            }
            this.InvokeAsync("SakkoraElteltIdoNoveles", new object[] {
                        execParam,
                        ugyiratId}, this.SakkoraElteltIdoNovelesOperationCompleted, userState);
        }

        private void OnSakkoraElteltIdoNovelesOperationCompleted(object arg)
        {
            if ((this.SakkoraElteltIdoNovelesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SakkoraElteltIdoNovelesCompleted(this, new SakkoraElteltIdoNovelesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        public delegate void SakkoraElteltIdoNovelesCompletedEventHandler(object sender, SakkoraElteltIdoNovelesCompletedEventArgs e);

        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        public partial class SakkoraElteltIdoNovelesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
        {

            private object[] results;

            internal SakkoraElteltIdoNovelesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
                :
                    base(exception, cancelled, userState)
            {
                this.results = results;
            }

            /// <remarks/>
            public Result Result
            {
                get
                {
                    this.RaiseExceptionIfNecessary();
                    return ((Result)(this.results[0]));
                }
            }
        }
        #endregion
    }

}