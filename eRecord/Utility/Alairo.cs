﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.BaseUtility;
using Contentum.eRecord.Service;
using System;
using System.Web.UI;

namespace Contentum.eRecord.Utility
{
    public class Alairo
    {
        private readonly ExecParam _execParam;
        private readonly string _iratId;
        private readonly Page _page;

        public Alairo(Page page, string iratId) : this(UI.SetExecParamDefault(page, new ExecParam()), iratId)
        {
            _page = page;
        }

        public Alairo(ExecParam execParam, string iratId)
        {
            _execParam = execParam;
            _iratId = iratId;
        }

        public bool Felveheto(out string errormsg)
        {
            //felvétel gomb megnyomásakor nem nézzük
            return Felveheto("M_UTO", out errormsg);
        }

        public bool Felveheto(string alairasMod, out string errormsg)
        {
            errormsg = String.Empty;

            string ertek = Rendszerparameterek.Get(_execParam, Rendszerparameterek.ALAIROK_CSATOLMANY_NELKUL);

            switch (ertek)
            {
                //csatolmány nélkül is fel lehet venni aláírót
                case "1":
                    return true;

                //csak utólagosan adminisztrált aláírót vehet fel csatolmány nélkül
                case "3":
                    return alairasMod == "M_UTO" ? true : CheckCsatolmany(out errormsg);
            }

            //csak csatolmánnyal vehet fel aláírót
            return CheckCsatolmany(out errormsg);
        }

        // CR3196 Ne lehessen aláírót felvenni, ha nincs csatolmány
        public bool CheckCsatolmany(out string errormsg)
        {
            var ExecParam = _execParam.Clone();
            Result result = null;

            EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            EREC_CsatolmanyokSearch search = _page == null ? new EREC_CsatolmanyokSearch() : (EREC_CsatolmanyokSearch)Search.GetSearchObject(_page, new EREC_CsatolmanyokSearch());
            if (!String.IsNullOrEmpty(_iratId))
            {
                search.IraIrat_Id.Filter(_iratId);
            }
            else
            {
                errormsg = Contentum.eUtility.Resources.Error.GetString("UINoIdParam");
                return false;
            }
            result = service.GetAll(ExecParam, search);

            if (result.IsError)
            {
                errormsg = result.ErrorMessage;
                return false;
            }

            if (result.GetCount > 0)
            {
                errormsg = "";
                return true;
            }
            else
            {
                errormsg = Contentum.eUtility.Resources.Error.GetString("UIIratNincsCsatolmany");
                return false;
            }
        }
    }
}
