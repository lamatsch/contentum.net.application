﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Contentum.eRecord.BaseUtility
{
    public class CsatolmanyHelper
    {

        public static Result HandleAzonosNevuWordFodokumentum(ExecParam execParam, EREC_Csatolmanyok newCsatolmany,string newCsatolmanyFajlNev, string iratId)
        {
            if (!Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.AZONOS_NEVU_CSATOLMANY_ENABLED, false))
            {
                return null;
            }
            Logger.Debug("HandleAzonosNevuWordFodokumentum.iratId: " + iratId);
            EREC_CsatolmanyokService csatolmanyokService = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();
            search.IraIrat_Id.Value = iratId;
            search.IraIrat_Id.Operator = Query.Operators.equals;
            Result iratCsatolmanyokResult = csatolmanyokService.GetAllWithExtension(execParam, search);
            if (iratCsatolmanyokResult.IsError  )
            {
                return iratCsatolmanyokResult;
            }

            if (!iratCsatolmanyokResult.HasData())
            {
                Logger.Debug("HandleAzonosNevuWordFodokumentum.iratCsatolmanyokResult is empty");
                return null;
            }
            string fajlNevOfNewCsatolmany = newCsatolmanyFajlNev.Replace(".pdf", "");
            Logger.Debug("HandleAzonosNevuWordFodokumentum.fajlNevOfNewCsatolmany: "+ fajlNevOfNewCsatolmany);
            string foundAzonosNevuWordCsatolmanyID = string.Empty;
            foreach (DataRow row in iratCsatolmanyokResult.Ds.Tables[0].Rows)
            {
                string currentCsatolmanyFajlNev = row["FajlNev"].ToString();
                string currentCsatolmanyFormatum = row["Formatum_Nev"].ToString();
                Logger.Debug("HandleAzonosNevuWordFodokumentum.currentCsatolmanyFajlNev: " + currentCsatolmanyFajlNev);
                Logger.Debug("HandleAzonosNevuWordFodokumentum.currentCsatolmanyFormatum: " + currentCsatolmanyFormatum);

                if (string.IsNullOrEmpty(currentCsatolmanyFajlNev))
                {
                    continue;
                }
                //nem eggyezik a két fájl neve akkor tovább
                string fajlNevOfCurrentCsatolmany = currentCsatolmanyFajlNev.Remove(currentCsatolmanyFajlNev.LastIndexOf('.'), currentCsatolmanyFajlNev.Length - currentCsatolmanyFajlNev.LastIndexOf('.'));
                Logger.Debug("HandleAzonosNevuWordFodokumentum.fajlNevOfCurrentCsatolmany: " + fajlNevOfCurrentCsatolmany);

                if (fajlNevOfNewCsatolmany != fajlNevOfCurrentCsatolmany)
                { continue; }

                //Ha nem doc vagy docx akkor tovább
                if (currentCsatolmanyFormatum.ToLower() != "word (doc)" && currentCsatolmanyFormatum.ToLower() != "word (docx)")
                {
                    continue;
                }

                foundAzonosNevuWordCsatolmanyID = row["id"].ToString();
                break;
            }
            //nincs olyan amit módosítani kellett volna
            if (string.IsNullOrEmpty(foundAzonosNevuWordCsatolmanyID))
            {
                return null;
            }

             var searchExecParam = execParam.Clone();
            searchExecParam.Record_Id = foundAzonosNevuWordCsatolmanyID;

            EREC_Csatolmanyok WordCsatolmany = (EREC_Csatolmanyok)csatolmanyokService.Get(searchExecParam).Record;
            WordCsatolmany.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Tervezet;
            WordCsatolmany.Updated.DokumentumSzerep = true;
            execParam.Record_Id = foundAzonosNevuWordCsatolmanyID;
            Result WordCsatolmanyUpdateResult = csatolmanyokService.Update(execParam, WordCsatolmany);

            if (WordCsatolmanyUpdateResult.IsError)
            {
                return WordCsatolmanyUpdateResult;
            }
            //beállitjuk az uj dokumentumot fodokumentumnak!
            newCsatolmany.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
            newCsatolmany.Updated.DokumentumSzerep = true;
            return null;
        }            
    }
}
