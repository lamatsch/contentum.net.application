using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;

namespace Contentum.eRecord.BaseUtility
{
    public class FelhasznaloProfil : Contentum.eUtility.FelhasznaloProfil
    {

        /// <summary>
        /// Visszaadja a felhasználó egyszemélyes csoportjának Id-ját a megadott ExecParam-ból
        /// </summary>
        /// <param name="execParam"></param>
        /// <returns></returns>
        public static String GetFelhasznaloCsoport(ExecParam execParam)
        {
            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
            {
                return "";
            }
            else
            {
                // A felhasználó egyszemélyes csoportjának Id-ja jelenleg megegyezik a Felhasznalo_Id-val:

                return execParam.Felhasznalo_Id;
            }

        }
    }

}
