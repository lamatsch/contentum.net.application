﻿using Contentum.eUtility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.UI.WebControls;

namespace Contentum.eRecord.BaseUtility
{
    /// <summary>
    /// This class holds the state of the visibility of the passed GridView and used as a POCO object which is passed to *SSRS pages to detemine which column to show in the report.
    /// </summary>
    public class GridViewColumnVisibilityState
    {


        public static class Constants
        {
            public static char ITEM_SEPARATOR = '|';
            public static char KEY_VALUE_SEPARATOR = ':';

            public class SessionVariables
            {
                public const string IraIratokSSRS = "IraIratokSSRS";
                public const string IrattarbaVetelSSRS = "IrattarbaVetelSSRS";
                public const string IraIratokSSRSMunkaugyiratokIratai = "IraIratokSSRSMunkaugyiratokIratai";
                public const string IraIratokSSRSKozgyulesiEloterjesztesek = "IraIratokSSRSKozgyulesiEloterjesztesek";
                public const string IraIratokSSRSKozgyulesiEloterjesztesekIratmozgas = "IraIratokSSRSKozgyulesiEloterjesztesekIratmozgas";
                public const string IraIratokSSRSBizottsagiEloterjesztesek = "IraIratokSSRSBizottsagiEloterjesztesek";
                public const string IraIratokSSRSBizottsagiEloterjesztesekIratmozgas = "IraIratokSSRSBizottsagiEloterjesztesekIratmozgas";
                public const string IraJegyzekTetelekSSRS = "IraJegyzekTetelekSSRS";
                public const string IraJegyzekekSSRS = "IraJegyzekekSSRS";
                public const string IraJegyzekekSSRSTetelek = "IraJegyzekekSSRSTetelek";
                public const string PldIratPeldanyokSSSRS = "PldIratPeldanyokSSSRS";
                public const string UgyUgyiratokSSRS = "UgyUgyiratokSSRS";
                public const string JegyzekTetelekSSRS = "JegyzekTetelekSSRS";
                public const string JegyzekTetelekSSRSTetelek = "JegyzekTetelekSSRSTetelek";                
            }
        }

        Dictionary<string, string> columns;
        public GridViewColumnVisibilityState LoadFromGrid(GridView grid, Type columnNameHolderClass)
        {
            columns = GetAllColumnNames(columnNameHolderClass);
            foreach (DataControlField column in grid.Columns)
            {
                columns[column.AccessibleHeaderText.ToLower()] = column.Visible ? "1" : "0";
            }
            return this;
        }

        public bool IsVisible(string columnName)
        {
            if (this.columns == null) { return true; }
            if (!this.columns.ContainsKey(columnName.ToLower()))
            {
                return false;
            }
            return this.columns[columnName.ToLower()] == "1" ? true : false;
        }

        public string GetIsVisibleValue(string columnName)
        {
            if (IsVisible(columnName))
            {
                return Boolean.TrueString;
            }
            return Boolean.FalseString;
        }

        public GridViewColumnVisibilityState LoadFromCustomString(string customString)
        {
            if (string.IsNullOrEmpty(customString)) { return this; }
            this.columns = new Dictionary<string, string>();
            string[] items = customString.Split(Constants.ITEM_SEPARATOR);
            foreach (var item in items)
            {
                string[] keyValue = item.Split(Constants.KEY_VALUE_SEPARATOR);
                if (keyValue.Length == 0) { continue; }
                this.columns[keyValue[0].ToLower()] = keyValue[1];
            }
            return this;
        }
        public GridViewColumnVisibilityState LoadFromJSON(string json)
        {
            this.columns = new Dictionary<string, string>();
            JObject obj = (JObject)JsonConvert.DeserializeObject(json);
            foreach (var property in obj)
            {
                columns[property.Key.ToLower()] = (string)property.Value;
            }
            return this;
        }

        public string ToCustomString()
        {
            string result = string.Empty;
            foreach (var keyValue in this.columns)
            {
                string keyValueString = keyValue.Key.ToLower() + Constants.KEY_VALUE_SEPARATOR + keyValue.Value;
                result += keyValueString + Constants.ITEM_SEPARATOR;
            }
            result = result.TrimEnd(Constants.ITEM_SEPARATOR);
            return result;
        }
        public string ToJson()
        {
            return JsonConvert.SerializeObject(columns);
        }

        private static Dictionary<string, string> GetAllColumnNames(Type columnNameHolderClass)
        {
            return columnNameHolderClass
             .GetFields(BindingFlags.Public | BindingFlags.Static)
             .Where(f => f.FieldType == typeof(string))
             .ToDictionary(f => f.Name.ToLower(),
                           f => ((string)f.GetValue(null)).ToLower());
        }
    }
}
