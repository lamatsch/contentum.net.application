﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eRecord.BaseUtility
{
    /// <summary>
    /// This class contains the AccessibleHeader values of the named lists which are used when passing the visibility state to the SSRS report generator aspx file.
    /// </summary>
    public static class GridViewColumns
    {
        public class UgyiratokListColumnNames
        {
            public static string AdathordozoTipusa_Nev = "AdathordozoTipusa_Nev";
            public static string Allapot = "Allapot";
            public static string AlszamDb = "AlszamDb";
            public static string BARCODE = "BARCODE";
            public static string Csatolmany = "Csatolmany";
            public static string Csny = "Csny";
            public static string ElintezesDat = "ElintezesDat";
            public static string EljarasiSzakaszFokNev = "EljarasiSzakaszFokNev";
            public static string ElteltUgyintezesiIdo = "ElteltUgyintezesiIdo";
            public static string Ev = "Ev";
            public static string F = "F";
            public static string Felelos_Nev = "Felelos_Nev";
            public static string FelulvizsgalatDat = "FelulvizsgalatDat";
            public static string Foszam = "Foszam";
            public static string Foszam_Merge = "Foszam_Merge";
            public static string FT1 = "FT1";
            public static string FT2 = "FT2";
            public static string FT3 = "FT3";
            public static string Hatarido = "Hatarido";
            public static string HatralevoMunkanapok = "HatralevoMunkanapok";
            public static string HatralevoNapok = "HatralevoNapok";
            public static string IH = "IH";
            public static string Iktato = "Iktato";
            public static string Iktatohely = "Iktatohely";
            public static string IrattarbaKuldDatuma = "IrattarbaKuldDatuma";
            public static string IrattarbaVetelDat = "IrattarbaVetelDat";
            public static string ITSZ = "ITSZ";
            public static string Jelleg = "Jelleg";
            public static string KezelesTipusNev = "KezelesTipusNev";
            public static string KolcsonzesDatuma = "KolcsonzesDatuma";
            public static string KolcsonzesiHatarido = "KolcsonzesiHatarido";
            public static string KRT_Csoportok_Orzo_Nev = "KRT_Csoportok_Orzo_Nev";
            public static string LetrehozasIdo_Rovid = "LetrehozasIdo_Rovid";
            public static string LezarasDat = "LezarasDat";
            public static string LezarasOka_Nev = "LezarasOka_Nev";
            public static string Megjegyzes = "Megjegyzes";
            public static string MegorzesiIdoVege = "MegorzesiIdoVege";
            public static string NevSTR_Ugyindito = "NevSTR_Ugyindito";
            public static string SakkoraAllapotNev = "SakkoraAllapotNev";
            public static string SelejtezesDat = "SelejtezesDat";
            public static string SkontrobaDat = "SkontrobaDat";
            public static string SkontrobanOsszesen = "SkontrobanOsszesen";
            public static string SkontroVege_Rovid = "SkontroVege_Rovid";
            public static string Sz = "Sz";
            public static string SztornirozasDat = "SztornirozasDat";
            public static string Targy = "Targy";
            public static string UgyFajtaja = "UgyFajtaja";
            public static string UgyfeleloSzervezetKod = "UgyfeleloSzervezetKod";
            public static string Ugyindito_Cim = "Ugyindito_Cim";
            public static string UgyintezesIdeje_Nev = "UgyintezesIdeje_Nev";
            public static string UgyintezesModja_Nev = "UgyintezesModja_Nev";
            public static string Ugyintezo_Nev = "Ugyintezo_Nev";
            public static string UgyTipus_Nev = "UgyTipus_Nev";
            public static string Zarolas = "Zarolas";

        }
        public class IratokListColumnNames
        {
            public static string AdathordozoTipusa_Nev = "AdathordozoTipusa_Nev";
            public static string Allapot = "Allapot";
            public static string Alszam = "Alszam";
            public static string BeerkezesIdeje = "BeerkezesIdeje";
            public static string CimSTR_Bekuldo = "CimSTR_Bekuldo";
            public static string CimSTR_Cimzett = "CimSTR_Cimzett";
            public static string ElintezesDat = "ElintezesDat";
            public static string Elintezett = "Elintezett";
            public static string EljarasiSzakaszFokNev = "EljarasiSzakaszFokNev";
            public static string ElsoIratPeldanyAllapot_Nev = "ElsoIratPeldanyAllapot_Nev";
            public static string ElsoIratPeldanyBarCode = "ElsoIratPeldanyBarCode";
            public static string ElsoIratPeldanyFelelos_Nev = "ElsoIratPeldanyFelelos_Nev";
            public static string ElsoIratPeldanyOrzo_Nev = "ElsoIratPeldanyOrzo_Nev";
            public static string ErkeztetoSzam = "ErkeztetoSzam";
            public static string Ev = "Ev";
            public static string F = "F";
            public static string FelhasznaloCsoport_Id_Iktato_Nev = "FelhasznaloCsoport_Id_Iktato_Nev";
            public static string FelhasznaloCsoport_Id_Ugyintezo_Nev = "FelhasznaloCsoport_Id_Ugyintezo_Nev";
            public static string Foszam = "Foszam";
            public static string Hatarido = "Hatarido";
            public static string HatralevoMunkaNapok = "HatralevoMunkaNapok";
            public static string HatralevoNapok = "HatralevoNapok";
            public static string HivatkozasiSzam = "HivatkozasiSzam";
            public static string IktatasDatuma = "IktatasDatuma";
            public static string Iktatohely = "Iktatohely";
            public static string IktatoSzam_Merge = "IktatoSzam_Merge";
            public static string IntezesIdopontja = "IntezesIdopontja";
            public static string IratFelelos_SzervezetKod = "IratFelelos_SzervezetKod";
            public static string IratHatasaUgyintezesreNev = "IratHatasaUgyintezesreNev";
            public static string IratHatasaUgyintezesreTipus = "IratHatasaUgyintezesreTipus";
            public static string IrattarbaHelyezesDat = "IrattarbaHelyezesDat";
            public static string ITSZ = "ITSZ";
            public static string K = "K";
            public static string KezbesitesModja_Nev = "KezbesitesModja_Nev";
            public static string KezelesiFelj = "KezelesiFelj";
            public static string KuldesMod = "KuldesMod";
            public static string LezarasDat = "LezarasDat";
            public static string MinositesNev = "MinositesNev";
            public static string Munkaallomas = "Munkaallomas";
            public static string NevSTR_Bekuldo = "NevSTR_Bekuldo";
            public static string NevSTR_Cimzett = "NevSTR_Cimzett";
            public static string PeldanyKuldesMod = "PeldanyKuldesMod";
            public static string PostazasDatuma = "PostazasDatuma";
            public static string PostazasIranyaNev = "PostazasIranyaNev";
            public static string SztornirozasDat = "SztornirozasDat";
            public static string Targy1 = "Targy1";
            public static string UgyFajtaja = "UgyFajtaja";
            public static string UgyintezesModja = "UgyintezesModja";
            public static string Ugyintezo_Nev = "Ugyintezo_Nev";
            public static string Ugyirat_Hatarido = "Ugyirat_Hatarido";
            public static string Ugyirat_targy = "Ugyirat_targy";
            public static string Csny = "Csny";

        }
        public class IraJegyzekekColumnNames
        {
            public static string AllapotNev = "AllapotNev";
            public static string Azonosito = "Azonosito";
            public static string Csoport_Id_Felelos_Nev = "Csoport_Id_Felelos_Nev";
            public static string Csoport_Id_FelelosSzervezet_Nev = "Csoport_Id_FelelosSzervezet_Nev";
            public static string FelhasznaloCsoport_Id_Vegrehajto_Nev = "FelhasznaloCsoport_Id_Vegrehajto_Nev";
            public static string LetrehozasIdo = "LetrehozasIdo";
            public static string LezarasDatuma = "LezarasDatuma";
            public static string MegsemmisitesDatuma = "MegsemmisitesDatuma";
            public static string Note = "Note";
            public static string SztornirozasDat = "SztornirozasDat";
            public static string Tipus = "Tipus";
            public static string VegrehajtasDatuma = "VegrehajtasDatuma";
            public static string Zarolas = "Zarolas";
            public static string Nev = "Nev";

        }
        public class JegyzekekColumnNames
        {
            public static string Nev = "Nev";
            public static string Tipus = "Tipus";
            public static string Felelos_Nev = "Felelos_Nev";
            public static string Vegrehajto_Nev = "Vegrehajto_Nev";
            public static string Atvevo_Nev = "Atvevo_Nev";
            public static string LetrehozasIdo = "LetrehozasIdo";
            public static string LezarasDatuma = "LezarasDatuma";
            public static string VegrehajtasDatuma = "VegrehajtasDatuma";
            public static string SztornozasDat = "SztornozasDat";
            public static string MegsemmisitesDatuma = "MegsemmisitesDatuma";
            public static string Allapot_Nev = "Allapot_Nev";
        }
        public class IratPeldanyokListColumnName
        {
            public static string Allapot_Nev = "Allapot_Nev";
            public static string Alszam = "Alszam";
            public static string BarCode = "BarCode";
            public static string CimSTR_Cimzett = "CimSTR_Cimzett";
            public static string Csoport_Id_Felelos_Nev = "Csoport_Id_Felelos_Nev";
            public static string EREC_IraIratok_Targy = "EREC_IraIratok_Targy";
            public static string Eredet_Nev = "Eredet_Nev";
            public static string Ev = "Ev";
            public static string F = "F";
            public static string FelhasznaloCsoport_Id_Orzo_Nev = "FelhasznaloCsoport_Id_Orzo_Nev";
            public static string Foszam = "Foszam";
            public static string IktatoHely = "IktatoHely";
            public static string IktatoSzam_Merge = "IktatoSzam_Merge";
            public static string IratFelelos_SzervezetKod = "IratFelelos_SzervezetKod";
            public static string KezelesTipusNev = "KezelesTipusNev";
            public static string KuldesMod_Nev = "KuldesMod_Nev";
            public static string LetrehozasIdo_Rovid = "LetrehozasIdo_Rovid";
            public static string Letrehozo_Nev = "Letrehozo_Nev";
            public static string MinositesNev = "MinositesNev";
            public static string NevSTR_Cimzett = "NevSTR_Cimzett";
            public static string PostazasDatuma = "PostazasDatuma";
            public static string RagSzam = "RagSzam";
            public static string Sorszam = "Sorszam";
            public static string Tv = "Tv";
            public static string VisszaerkezesiHatarido = "VisszaerkezesiHatarido";
            public static string Zarolas = "Zarolas";
            public static string Csny = "Csny";

        }

        public class IraJegyzekTetelekColumnName
        {
            public static string Azonosito = "Azonosito";
            public static string Megjegyzes = "Megjegyzes";
            public static string LetrehozasIdo = "LetrehozasIdo";
            public static string Note = "Note";
            public static string AtadasDatuma = "AtadasDatuma";
        }

        public class JegyzekTetelekColumnName
        {
            public static string Azonosito = "Azonosito";
            public static string Megjegyzes = "Megjegyzes";
            public static string LetrehozasIdo = "LetrehozasIdo";
            public static string AtadasDatuma = "AtadasDatuma";
            public static string AtvevoIktatoszama = "AtvevoIktatoszama";
        }

    }
}
