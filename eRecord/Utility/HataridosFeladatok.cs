﻿using System;
using System.Collections.Generic;
using System.Text;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System.Data;

namespace Contentum.eRecord.BaseUtility
{
    public class HataridosFeladatok : Contentum.eUtility.HataridosFeladatok
    {
        public class Forras : Contentum.eUtility.HataridosFeladatok.Forras
        {
        }

        public static bool Sztornozhato(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            ErrorDetails errorDetails;
            return Sztornozhato(execParam, erec_HataridosFeladatok, out errorDetails);
        }


        public static class ErrorMessages
        {
            public const string NemKiado = "Nem Ön a feladat kiadója!";
            public const string NemFelelos = "Nem Ön a feladat felelőse!";
            public const string NemJoAllapot = "A feladat állapota nem megfelelő!";
            public const string Lezart = "A feladat már le van zárva!";
            public const string Sztornozott = "A feladat már sztornózott!";
            public const string Atvett = "A feladat már Önnél van!";
            public const string CsakFeladatAtveheto = "Csak feladat vehető át!";
            public const string NemSzervezetFelelos = "Nem az Ön szervezete a feladat felelőse!";
            public const string NemMemo = "Nem memo!";
            public const string NemFeladat = "Csak feladat helyezhető át!";
            public const string NemFeladatVagyMemo_Modositas = "Csak feladat vagy memo típusú feljegyzés módosítható!";
            public const string NemLetrehozo = "Nem Ön a feljegyzés létrehozója!";
        }

        public static ErrorDetails CreateErrorDetails(EREC_HataridosFeladatok erec_HataridosFeladatok, string columnType)
        {
            string message;
            string columnValue;
            switch (columnType)
            {
                case Constants.ErrorDetails.ColumnTypes.Felhasznalo_Id_kiado:
                    message = ErrorMessages.NemKiado;
                    columnValue = erec_HataridosFeladatok.Felhasznalo_Id_Kiado;
                    break;
                case Constants.ErrorDetails.ColumnTypes.Csoport_Id_FelelosFelhasznalo:
                    message = ErrorMessages.NemFelelos;
                    columnValue = erec_HataridosFeladatok.Felhasznalo_Id_Felelos;
                    break;
                case Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos:
                    message = ErrorMessages.NemSzervezetFelelos;
                    columnValue = erec_HataridosFeladatok.Felhasznalo_Id_Felelos;
                    break;
                case Constants.ErrorDetails.ColumnTypes.Allapot:
                    message = ErrorMessages.NemJoAllapot;
                    columnValue = erec_HataridosFeladatok.Allapot;
                    break;
                case Constants.ErrorDetails.ColumnTypes.Letrehozo_Id:
                    message = ErrorMessages.NemLetrehozo;
                    columnValue = erec_HataridosFeladatok.Base.Letrehozo_id;
                    break;
                default:
                    message = String.Empty;
                    columnValue = String.Empty;
                    break;
            }

            return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_HataridosFeladatok, erec_HataridosFeladatok.Id, columnType, columnValue);
        }

        public static ErrorDetails CreateErrorDetails(string message)
        {
            return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_HataridosFeladatok, null, null, null);
        }

        public static bool Sztornozhato(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok, out ErrorDetails errorDetails)
        {
            errorDetails = null;
            if (erec_HataridosFeladatok.Allapot == KodTarak.FELADAT_ALLAPOT.Sztornozott)
            {
                errorDetails = CreateErrorDetails(ErrorMessages.Sztornozott);
                return false;
            }
            if (erec_HataridosFeladatok.Felhasznalo_Id_Kiado != execParam.Felhasznalo_Id)
            {
                errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Felhasznalo_Id_kiado);
                return false;
            }

            switch (erec_HataridosFeladatok.Allapot)
            {
                case KodTarak.FELADAT_ALLAPOT.Uj:
                case KodTarak.FELADAT_ALLAPOT.Nyitott:
                    return true;
                default:
                    errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Allapot);
                    return false;
            }
        }

        #region BLG_2956
        //LZS - Visszadja, hogy az adott feladat delegált állapotú-e.
        public static bool Delegalt(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            if (erec_HataridosFeladatok.Allapot == KodTarak.FELADAT_ALLAPOT.Delegalt)
                return true;
            else
                return false;
        }
        #endregion
        public static bool Lezarhato(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            ErrorDetails errorDetails;
            return Lezarhato(execParam, erec_HataridosFeladatok, out errorDetails);
        }

        public static bool Lezarhato(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok, out ErrorDetails errorDetails)
        {
            errorDetails = null;
            if(erec_HataridosFeladatok.Allapot == KodTarak.FELADAT_ALLAPOT.Lezart)
            {
                errorDetails = CreateErrorDetails(ErrorMessages.Lezart);
                return false;
            }
            if (execParam.Felhasznalo_Id != erec_HataridosFeladatok.Felhasznalo_Id_Felelos)
            {
                errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Csoport_Id_FelelosFelhasznalo);
                return false;
            }

            switch (erec_HataridosFeladatok.Allapot)
            {
                case KodTarak.FELADAT_ALLAPOT.Uj:
                case KodTarak.FELADAT_ALLAPOT.Nyitott:
                case KodTarak.FELADAT_ALLAPOT.Folyamatban:
                    return true;
                default:
                    errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool Lezart(EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            return Lezart(erec_HataridosFeladatok.Allapot);
        }

        public static List<string> GetLezartAllapotok()
        {
            List<string> listResult = new List<string>();
            listResult.Add(KodTarak.FELADAT_ALLAPOT.Lezart);
            listResult.Add(KodTarak.FELADAT_ALLAPOT.KeruloutonMegoldott);
            listResult.Add(KodTarak.FELADAT_ALLAPOT.NemMegoldhato);
            listResult.Add(KodTarak.FELADAT_ALLAPOT.Sztornozott);
            listResult.Add(KodTarak.FELADAT_ALLAPOT.Lezart_Megoldott);
            listResult.Add(KodTarak.FELADAT_ALLAPOT.Lezart_KeruloUtonMegoldott);
            listResult.Add(KodTarak.FELADAT_ALLAPOT.Lezart_NemMegoldhato);
            return listResult;
        }

        public static bool Lezart(string allapot)
        {
            return (GetLezartAllapotok().Contains(allapot));

            //switch (allapot)
            //{
            //    case KodTarak.FELADAT_ALLAPOT.Lezart:
            //    case KodTarak.FELADAT_ALLAPOT.KeruloutonMegoldott:
            //    case KodTarak.FELADAT_ALLAPOT.NemMegoldhato:
            //    case KodTarak.FELADAT_ALLAPOT.Sztornozott:
            //    case KodTarak.FELADAT_ALLAPOT.Lezart_Megoldott:
            //    case KodTarak.FELADAT_ALLAPOT.Lezart_KeruloUtonMegoldott:
            //    case KodTarak.FELADAT_ALLAPOT.Lezart_NemMegoldhato:
            //        return true;
            //    default:
            //        return false;
            //}
        }

        public static bool Atveheto(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            ErrorDetails errorDetails;
            return Atveheto(execParam, erec_HataridosFeladatok, out errorDetails);
        }

        public static bool Atveheto(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok, out ErrorDetails errorDetails)
        {
            errorDetails = null;

            if (erec_HataridosFeladatok.Tipus != KodTarak.FELADAT_TIPUS.Feladat)
            {
                errorDetails = CreateErrorDetails(ErrorMessages.CsakFeladatAtveheto);
                return false;
            }

            if (execParam.Felhasznalo_Id == erec_HataridosFeladatok.Felhasznalo_Id_Felelos)
            {
                errorDetails = CreateErrorDetails(ErrorMessages.Atvett);
                return false;
            }

            if (execParam.FelhasznaloSzervezet_Id != erec_HataridosFeladatok.Felhasznalo_Id_Felelos)
            {
                errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos);
                return false;
            }

            return true;
        }

        public static bool ReszfeladatFelveheto(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            ErrorDetails errorDetails;
            return ReszfeladatFelveheto(execParam, erec_HataridosFeladatok, out errorDetails);
        }

        public static bool ReszfeladatFelveheto(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok, out ErrorDetails errorDetails)
        {
            errorDetails = null;

            switch (erec_HataridosFeladatok.Allapot)
            {
                case KodTarak.FELADAT_ALLAPOT.Uj:
                case KodTarak.FELADAT_ALLAPOT.Nyitott:
                case KodTarak.FELADAT_ALLAPOT.Folyamatban:
                    return true;
                default:
                    errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool Modosithato(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            ErrorDetails errorDetails;
            return Modosithato(execParam, erec_HataridosFeladatok, out errorDetails);
        }

        public static bool Modosithato(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok, out ErrorDetails errorDetails)
        {
            errorDetails = null;

            if (erec_HataridosFeladatok.Memo != Constants.Database.Yes)
            {
                if (erec_HataridosFeladatok.Tipus != KodTarak.FELADAT_TIPUS.Feladat)
                {
                    errorDetails = CreateErrorDetails(ErrorMessages.NemFeladatVagyMemo_Modositas);
                    return false;
                }

                if (execParam.Felhasznalo_Id != erec_HataridosFeladatok.Felhasznalo_Id_Felelos
                    && execParam.Felhasznalo_Id != erec_HataridosFeladatok.Felhasznalo_Id_Kiado)
                {
                    errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Csoport_Id_FelelosFelhasznalo);
                    return false;
                }

                if (HataridosFeladatok.Lezart(erec_HataridosFeladatok.Allapot))
                {
                    errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Allapot);
                    return false;
                }

                if (HataridosFeladatok.Delegalt(new ExecParam(), erec_HataridosFeladatok ))
                {
                    errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Allapot);
                    return false;
                }

            }
            return true;
        }

        public static bool Invalidalhato(ExecParam execParam,EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            ErrorDetails errorDetails;
            return Invalidalhato(execParam, erec_HataridosFeladatok, out errorDetails);
        }

        public static bool Invalidalhato(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok, out ErrorDetails errorDetails)
        {
            errorDetails = null;

            if (erec_HataridosFeladatok.Memo != Constants.Database.Yes)
            {
                errorDetails = CreateErrorDetails(ErrorMessages.NemMemo);
                return false;
            }

            if (execParam.Felhasznalo_Id != erec_HataridosFeladatok.Base.Letrehozo_id)
            {
                errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Letrehozo_Id);
                return false;
            }

            return true;
        }

        public static bool Athelyezheto(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok, EREC_HataridosFeladatok erec_HataridosFeladatok_Cel)
        {
            ErrorDetails errorDetails;
            return Athelyezheto(execParam, erec_HataridosFeladatok, erec_HataridosFeladatok_Cel, out errorDetails);
        }

        public static bool Athelyezheto(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok, EREC_HataridosFeladatok erec_HataridosFeladatok_Cel, out ErrorDetails errorDetails)
        {
            errorDetails = null;

            if (erec_HataridosFeladatok.Tipus != KodTarak.FELADAT_TIPUS.Feladat)
            {
                errorDetails = CreateErrorDetails(ErrorMessages.NemFeladat);
                return false;
            }

            if (execParam.Felhasznalo_Id != erec_HataridosFeladatok.Felhasznalo_Id_Kiado)
            {
                errorDetails = CreateErrorDetails(erec_HataridosFeladatok, Constants.ErrorDetails.ColumnTypes.Felhasznalo_Id_kiado);
                return false;
            }

            if (erec_HataridosFeladatok_Cel != null)
            {
                if (erec_HataridosFeladatok_Cel.Id == erec_HataridosFeladatok.Id)
                {
                    return false;
                }
                if (erec_HataridosFeladatok_Cel.HataridosFeladat_Id == erec_HataridosFeladatok.Id)
                {
                    return false;
                }

                if (String.IsNullOrEmpty(erec_HataridosFeladatok_Cel.HataridosFeladat_Id))
                {
                    return true;
                }

                
                EREC_HataridosFeladatokService svc = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                ExecParam xpm = execParam.Clone();
                xpm.Record_Id = erec_HataridosFeladatok.Id;
                Result res = svc.GetAllReszfeladat(xpm);
                if (res.IsError)
                {
                    return false;
                }

                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    string reszfeladatId = row["Id"].ToString();
                    if (reszfeladatId == erec_HataridosFeladatok_Cel.Id)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
