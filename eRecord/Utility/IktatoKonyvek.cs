using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System.Data;

namespace Contentum.eRecord.BaseUtility
{
    [Serializable]
    public class IktatoKonyvek
    {
        public const string txtDelimeter = "/";
        public const string valueDelimeter = "|";
        public const string nevBeginDelimeter = " (";
        public const string nevEndDelimeter = ")";

        private static class ErrorDetailMessages
        {
            public const string IktatokonyvMarLezart = "Az iktat�k�nyv m�r le van z�rva.\n";
            public const string IktatokonyvJovobeni = "Az iktat�k�nyv az aktu�lis �vet k�vet�en l�p csak �rv�nybe.\n";
            public const string IktatokonyvNemTalalhato = "Az iktat�k�nyv nem tal�lhat�.";
            public const string IktatokonyvZarolt = "Az iktat�k�nyvet m�s felhaszn�l� z�rolja.\n";
            public const string IktatokonyvErvenytelen = "Az iktat�k�nyv �rv�nytelen.\n";

            public const string IktatokonyvEvFormatumHiba = "Hib�s form�tum: iktat�k�nyv �v.\n";

            public const string ErkeztetokonyvMarLezart = "Az �rkeztet�k�nyv m�r le van z�rva.\n";
            public const string ErkeztetokonyvJovobeni = "Az �rkeztet�k�nyv az aktu�lis �vet k�vet�en l�p csak �rv�nybe.\n";
            public const string ErkeztetokonyvNemTalalhato = "Az �rkeztet�k�nyv nem tal�lhat�.";
            public const string ErkeztetokonyvZarolt = "Az �rkeztet�k�nyvet m�s felhaszn�l� z�rolja.\n";
            public const string ErkeztetokonyvErvenytelen = "Az �rkeztet�k�nyv �rv�nytelen.\n";
        }

        public static bool Lezart(EREC_IraIktatoKonyvek erec_IraIktatoKonyv)
        {
            ErrorDetails errorDetail;
            return Lezart(erec_IraIktatoKonyv, out errorDetail);
        }

        public static bool Lezart(EREC_IraIktatoKonyvek erec_IraIktatoKonyv, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (erec_IraIktatoKonyv == null)
            {
                errorDetail = new ErrorDetails(ErrorDetailMessages.IktatokonyvEvFormatumHiba, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, "null", "", "");
                return false;
            }
            int ikt_ev;
            if (!Int32.TryParse(erec_IraIktatoKonyv.Ev, out ikt_ev))
            {
                errorDetail = new ErrorDetails(ErrorDetailMessages.IktatokonyvEvFormatumHiba, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, "null", "", "");
                return false;
            }

            if (ikt_ev < DateTime.Today.Year)
            {
                return true;
            }
            else
            {
                //LZS - BLG_2549 - Lez�r�s d�tum�nak vizsg�lata
                //if (!String.IsNullOrEmpty(erec_IraIktatoKonyv.LezarasDatuma))
                if (string.IsNullOrEmpty(erec_IraIktatoKonyv.LezarasDatuma))
                {
                    return false;
                }
                try
                {
                    if (Convert.ToDateTime(erec_IraIktatoKonyv.LezarasDatuma) < DateTime.Now)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return true;
                }
            }
        }

        public static bool Lezart(String Id, ExecParam execParam)
        {
            ErrorDetails errorDetail;
            return Lezart(Id, execParam, out errorDetail);
        }

        public static bool Lezart(String Id, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            EREC_IraIktatoKonyvek erec_IraIktatoKonyv = null;

            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            execParam.Record_Id = Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_IraIktatoKonyv = (EREC_IraIktatoKonyvek)result.Record;
                return Lezart(erec_IraIktatoKonyv, out errorDetail);
            }
            else
            {
                errorDetail = new ErrorDetails(ErrorDetailMessages.IktatokonyvNemTalalhato, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, "null", "", "");
                return false;
            }
        }

        public static bool Lezarhato(ExecParam execParam, EREC_IraIktatoKonyvek erec_IraIktatoKonyv)
        {
            ErrorDetails errorDetail;
            return Lezarhato(execParam, erec_IraIktatoKonyv, out errorDetail);
        }

        public static bool Lezarhato(ExecParam execParam, EREC_IraIktatoKonyvek erec_IraIktatoKonyv, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            if (erec_IraIktatoKonyv == null)
            {
                errorDetail = new ErrorDetails(ErrorDetailMessages.IktatokonyvNemTalalhato, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, "null", "", "");
                return false;
            }

            #region LezarasDatuma
            if (!String.IsNullOrEmpty(erec_IraIktatoKonyv.LezarasDatuma))
            {
                string errorDetailMessage = ErrorDetailMessages.IktatokonyvMarLezart;
                if (erec_IraIktatoKonyv.IktatoErkezteto == Constants.IktatoErkezteto.Erkezteto)
                {
                    errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvMarLezart;
                }
                errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, erec_IraIktatoKonyv.Id, Constants.ErrorDetails.ColumnTypes.LezarasDatuma, erec_IraIktatoKonyv.LezarasDatuma);
                return false;
            }
            #endregion LezarasDatuma

            #region Ev
            int ikt_ev = Int32.Parse(erec_IraIktatoKonyv.Ev);

            if (ikt_ev > DateTime.Today.Year)
            {
                string errorDetailMessage = ErrorDetailMessages.IktatokonyvJovobeni;
                if (erec_IraIktatoKonyv.IktatoErkezteto == Constants.IktatoErkezteto.Erkezteto)
                {
                    errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvJovobeni;
                }
                errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, erec_IraIktatoKonyv.Id, Constants.ErrorDetails.ColumnTypes.Ev, erec_IraIktatoKonyv.Ev);
                return false;
            }
            #endregion Ev

            #region Zarolo
            if (!String.IsNullOrEmpty(erec_IraIktatoKonyv.Base.Zarolo_id) && erec_IraIktatoKonyv.Base.Zarolo_id != execParam.Felhasznalo_Id)
            {
                string errorDetailMessage = ErrorDetailMessages.IktatokonyvZarolt;
                if (erec_IraIktatoKonyv.IktatoErkezteto == Constants.IktatoErkezteto.Erkezteto)
                {
                    errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvZarolt;
                }
                errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, erec_IraIktatoKonyv.Id, Constants.ErrorDetails.ColumnTypes.Zarolo_id, erec_IraIktatoKonyv.Base.Zarolo_id);
                return false;
            }
            #endregion Zarolo

            #region Ervenyesseg
            DateTime ErvVege = new DateTime();
            if (DateTime.TryParse(erec_IraIktatoKonyv.ErvVege, out ErvVege) && ErvVege < DateTime.Now)
            {
                string errorDetailMessage = ErrorDetailMessages.IktatokonyvErvenytelen;
                if (erec_IraIktatoKonyv.IktatoErkezteto == Constants.IktatoErkezteto.Erkezteto)
                {
                    errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvErvenytelen;
                }
                errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, erec_IraIktatoKonyv.Id, Constants.ErrorDetails.ColumnTypes.ErvVege, erec_IraIktatoKonyv.ErvVege);
                return false;
            }

            DateTime ErvKezd = new DateTime();
            if (DateTime.TryParse(erec_IraIktatoKonyv.ErvKezd, out ErvKezd) && ErvKezd > DateTime.Now)
            {
                string errorDetailMessage = ErrorDetailMessages.IktatokonyvErvenytelen;
                if (erec_IraIktatoKonyv.IktatoErkezteto == Constants.IktatoErkezteto.Erkezteto)
                {
                    errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvErvenytelen;
                }
                errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, erec_IraIktatoKonyv.Id, Constants.ErrorDetails.ColumnTypes.ErvKezd, erec_IraIktatoKonyv.ErvKezd);
                return false;
            }
            #endregion Ervenyesseg

            return true;

        }

        public static bool Lezarhato(String Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            ErrorDetails errorDetail;
            return Lezarhato(Id, execParam, EErrorPanel, out errorDetail);
        }

        public static bool Lezarhato(String Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            EREC_IraIktatoKonyvek erec_IraIktatoKonyv = null;

            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            execParam.Record_Id = Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_IraIktatoKonyv = (EREC_IraIktatoKonyvek)result.Record;
                return Lezarhato(execParam, erec_IraIktatoKonyv, out errorDetail);
            }
            else
            {
                if (EErrorPanel != null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                }
                return false;
            }
        }

        public static bool Lezarhato(ExecParam execParam, DataRow drv)
        {
            ErrorDetails errorDetail;
            return Lezarhato(execParam, drv, out errorDetail);
        }

        public static bool Lezarhato(ExecParam execParam, DataRow row, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            #region LezarasDatuma
            if (!String.IsNullOrEmpty(row["LezarasDatuma"].ToString()))
            {
                string errorDetailMessage = ErrorDetailMessages.IktatokonyvMarLezart;
                if (row["IktatoErkezteto"].ToString() == Constants.IktatoErkezteto.Erkezteto)
                {
                    errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvMarLezart;
                }
                errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, row["Id"].ToString(), Constants.ErrorDetails.ColumnTypes.LezarasDatuma, row["LezarasDatuma"].ToString());
                return false;
            }
            #endregion LezarasDatuma

            #region Ev
            int ikt_ev = Int32.Parse(row["Ev"].ToString());

            if (ikt_ev > DateTime.Today.Year)
            {
                string errorDetailMessage = ErrorDetailMessages.IktatokonyvJovobeni;
                if (row["IktatoErkezteto"].ToString() == Constants.IktatoErkezteto.Erkezteto)
                {
                    errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvJovobeni;
                }
                errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, row["Id"].ToString(), Constants.ErrorDetails.ColumnTypes.Ev, ikt_ev.ToString());
                return false;
            }
            #endregion Ev

            #region Zarolo

            if (!String.IsNullOrEmpty(row["Zarolo_id"].ToString()) && row["Zarolo_id"].ToString() != execParam.Felhasznalo_Id)
            {
                string errorDetailMessage = ErrorDetailMessages.IktatokonyvZarolt;
                if (row["IktatoErkezteto"].ToString() == Constants.IktatoErkezteto.Erkezteto)
                {
                    errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvZarolt;
                }
                errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, row["Id"].ToString(), Constants.ErrorDetails.ColumnTypes.Zarolo_id, row["Zarolo_id"].ToString());
                return false;
            }
            #endregion Zarolo

            #region Ervenyesseg
            DateTime ErvVege = new DateTime();
            if (DateTime.TryParse(row["ErvVege"].ToString(), out ErvVege) && ErvVege < DateTime.Now)
            {
                string errorDetailMessage = ErrorDetailMessages.IktatokonyvErvenytelen;
                if (row["IktatoErkezteto"].ToString() == Constants.IktatoErkezteto.Erkezteto)
                {
                    errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvErvenytelen;
                }
                errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, row["Id"].ToString(), Constants.ErrorDetails.ColumnTypes.ErvVege, row["ErvVege"].ToString());
                return false;
            }

            DateTime ErvKezd = new DateTime();
            if (DateTime.TryParse(row["ErvKezd"].ToString(), out ErvKezd) && ErvKezd > DateTime.Now)
            {
                string errorDetailMessage = ErrorDetailMessages.IktatokonyvErvenytelen;
                if (row["IktatoErkezteto"].ToString() == Constants.IktatoErkezteto.Erkezteto)
                {
                    errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvErvenytelen;
                }
                errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, row["Id"].ToString(), Constants.ErrorDetails.ColumnTypes.ErvKezd, row["ErvKezd"].ToString());
                return false;
            }
            #endregion Ervenyesseg

            return true;

        }

        public static bool Lezarhato(ExecParam execParam, DataRowView drv)
        {
            ErrorDetails errorDetail;
            return Lezarhato(execParam, drv, out errorDetail);
        }

        public static bool Lezarhato(ExecParam execParam, DataRowView drv, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            #region LezarasDatuma
            if (drv.Row.Table.Columns.Contains("LezarasDatuma"))
            {
                if (!String.IsNullOrEmpty(drv["LezarasDatuma"].ToString()))
                {
                    string errorDetailMessage = ErrorDetailMessages.IktatokonyvMarLezart;
                    if (drv.Row.Table.Columns.Contains("IktatoErkezteto") && drv["IktatoErkezteto"].ToString() == Constants.IktatoErkezteto.Erkezteto)
                    {
                        errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvMarLezart;
                    }
                    errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, drv["Id"].ToString(), Constants.ErrorDetails.ColumnTypes.LezarasDatuma, drv["LezarasDatuma"].ToString());
                    return false;
                }
            }
            #endregion LezarasDatuma

            #region Ev
            if (drv.Row.Table.Columns.Contains("Ev"))
            {
                int ikt_ev = Int32.Parse(drv["Ev"].ToString());

                if (ikt_ev > DateTime.Today.Year)
                {
                    string errorDetailMessage = ErrorDetailMessages.IktatokonyvJovobeni;
                    if (drv.Row.Table.Columns.Contains("IktatoErkezteto") && drv["IktatoErkezteto"].ToString() == Constants.IktatoErkezteto.Erkezteto)
                    {
                        errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvJovobeni;
                    }

                    errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, drv["Id"].ToString(), Constants.ErrorDetails.ColumnTypes.Ev, ikt_ev.ToString());
                    return false;
                }
            }
            #endregion Ev

            #region Zarolo
            if (drv.Row.Table.Columns.Contains("Zarolo_id"))
            {
                if (!String.IsNullOrEmpty(drv["Zarolo_id"].ToString()) && drv["Zarolo_id"].ToString() != execParam.Felhasznalo_Id)
                {
                    string errorDetailMessage = ErrorDetailMessages.IktatokonyvZarolt;
                    if (drv.Row.Table.Columns.Contains("IktatoErkezteto"))
                    {
                        if (drv["IktatoErkezteto"].ToString() == Constants.IktatoErkezteto.Erkezteto)
                        {
                            errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvZarolt;
                        }
                    }
                    errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, drv["Id"].ToString(), Constants.ErrorDetails.ColumnTypes.Zarolo_id, drv["Zarolo_id"].ToString());
                    return false;
                }
            }
            #endregion Zarolo

            #region Ervenyesseg
            if (drv.Row.Table.Columns.Contains("ErvVege"))
            {
                DateTime ErvVege = new DateTime();
                if (DateTime.TryParse(drv["ErvVege"].ToString(), out ErvVege) && ErvVege < DateTime.Now)
                {
                    string errorDetailMessage = ErrorDetailMessages.IktatokonyvErvenytelen;
                    if (drv.Row.Table.Columns.Contains("IktatoErkezteto"))
                    {
                        if (drv["IktatoErkezteto"].ToString() == Constants.IktatoErkezteto.Erkezteto)
                        {
                            errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvErvenytelen;
                        }
                    }
                    errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, drv["Id"].ToString(), Constants.ErrorDetails.ColumnTypes.ErvVege, drv["ErvVege"].ToString());
                    return false;
                }
            }

            if (drv.Row.Table.Columns.Contains("ErvKezd"))
            {
                DateTime ErvKezd = new DateTime();
                if (DateTime.TryParse(drv["ErvKezd"].ToString(), out ErvKezd) && ErvKezd > DateTime.Now)
                {
                    string errorDetailMessage = ErrorDetailMessages.IktatokonyvErvenytelen;
                    if (drv.Row.Table.Columns.Contains("IktatoErkezteto"))
                    {
                        if (drv["IktatoErkezteto"].ToString() == Constants.IktatoErkezteto.Erkezteto)
                        {
                            errorDetailMessage = ErrorDetailMessages.ErkeztetokonyvErvenytelen;
                        }
                    }
                    errorDetail = new ErrorDetails(errorDetailMessage, Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek, drv["Id"].ToString(), Constants.ErrorDetails.ColumnTypes.ErvKezd, drv["ErvKezd"].ToString());
                    return false;
                }
            }
            #endregion Ervenyesseg

            return true;

        }

        public static string GetSavFromIktatohely(string iktatohely)
        {
            if (String.IsNullOrEmpty(iktatohely))
            {
                return String.Empty;
            }

            string sav = iktatohely.StartsWith("FPH") ? iktatohely.Substring(3) : iktatohely;

            if (String.IsNullOrEmpty(sav))
                return String.Empty;

            if (sav.StartsWith("0") && sav.Length > 2)
                sav = sav.Substring(1, 2);

            return sav;
        }

        public static string GetSavFromIktatokonyvValue(string value)
        {
            string iktatohely = String.Empty;
            string[] values = value.Split(new string[] { valueDelimeter }, StringSplitOptions.None);

            iktatohely = values[0].Trim();

            return GetSavFromIktatohely(iktatohely);
        }

    }
}
