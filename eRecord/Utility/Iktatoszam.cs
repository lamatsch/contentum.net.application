using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using System.Web;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Contentum.eRecord.BaseUtility
{
    [Serializable]
    public class Iktatoszam
    { 
        public static string GetFullAzonosito(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIktatoKonyvek erec_IraIktatoKonyvek)
        {
            return GetFullAzonosito(execParam, erec_UgyUgyiratok, erec_IraIktatoKonyvek, null, null, null, "U");
        }

        public static string GetFullAzonosito(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIktatoKonyvek erec_IraIktatoKonyvek, EREC_IraIratok erec_IraIratok)
        {
            return GetFullAzonosito(execParam, erec_UgyUgyiratok, erec_IraIktatoKonyvek, erec_IraIratok, null, null, "UI");
        }

        public static string GetFullAzonosito(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIktatoKonyvek erec_IraIktatoKonyvek, EREC_IraIratok erec_IraIratok, EREC_PldIratPeldanyok erec_PldIratPeldanyok)
        {
            return GetFullAzonosito(execParam, erec_UgyUgyiratok, erec_IraIktatoKonyvek, erec_IraIratok, erec_PldIratPeldanyok, null, "UIP");
        }

        public static string GetFullAzonosito(ExecParam execParam, EREC_KuldKuldemenyek erec_KuldKuldemenyek, EREC_IraIktatoKonyvek erec_IraIktatoKonyvek)
        {
            return GetFullAzonosito(execParam, null, erec_IraIktatoKonyvek, null, null, erec_KuldKuldemenyek, "K");
        }
        public static string GetFullAzonosito(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIktatoKonyvek erec_IraIktatoKonyvek, EREC_IraIratok erec_IraIratok, EREC_PldIratPeldanyok erec_PldIratPeldanyok, EREC_KuldKuldemenyek erec_KuldKuldemenyek, string selectors)
        {
            //'*U*$K.Iktatohely$/#U#*U*$U.Foszam$#U#*I*-$I.Alszam$#I#*U*/$K.Ev$#U#*U*/$K.MegkulJelzes$#U#*P* (P.sorszam)#P#'
            string iktatoszam = "";
            //if (erec_UgyUgyiratok != null && erec_IraIktatoKonyvek != null)
            //{
            String iktatoszam_formatum;
            if (selectors == "K")
            {
                iktatoszam_formatum = Rendszerparameterek.Get(execParam, Rendszerparameterek.ERKEZTETOSZAM_FORMATUM);

            }
            else if (selectors == "L")
            {
                iktatoszam_formatum = Rendszerparameterek.Get(execParam, Rendszerparameterek.KULDEMENYAZONOSITO_FORMATUM);
            }
            else
                iktatoszam_formatum = Rendszerparameterek.Get(execParam, Rendszerparameterek.IKTATOSZAM_FORMATUM);

                if (String.IsNullOrEmpty(iktatoszam_formatum))
                {
                    Logger.Error("Nincs iktat�sz�m form�tum meghat�rozva");
                    throw new Exception("Nincs iktat�sz�m form�tum meghat�rozva");
                }

                //string pattern = @"\*[UI]\*(.*?)#[UI]#";
                string pattern = @"\*["+selectors+@"]\*(.*?)#["+selectors+@"U]#";

                string[] matches;
                string field = "";
                Match selectorMatch;
                Object o = new Object();
                PropertyInfo u;
                string fieldname;
                object[] obRetVal = new Object[0];
                foreach (Match m in Regex.Matches(iktatoszam_formatum, pattern))
                {
                    field = m.Value.Substring(3, m.Value.Length - 6);
                    selectorMatch = Regex.Match(m.Value, @"\$(.*?)\$");
                    matches = selectorMatch.Value.Split('.');

                    if (matches.Length == 2)
                    {
                        switch (matches[0])
                        {
                            case "$K":
                                {
                                    o = erec_IraIktatoKonyvek;

                                    break;
                                }
                            case "$U":
                                {
                                    o = erec_UgyUgyiratok;
                                    //u = erec_UgyUgyiratok.GetType().GetProperty(matches[1]);
                                    break;
                                }
                            case "$I":
                                {
                                    o = erec_IraIratok;
                                    //u = erec_UgyUgyiratok.GetType().GetProperty(matches[1]);
                                    break;
                                }
                            case "$P":
                                {
                                    o = erec_PldIratPeldanyok;
                                    //u = erec_UgyUgyiratok.GetType().GetProperty(matches[1]);
                                    break;
                                }
                        case "$E":
                            {
                                o = erec_KuldKuldemenyek;
                                //u = erec_UgyUgyiratok.GetType().GetProperty(matches[1]);
                                break;
                            }
                        default:
                                {
                                    Logger.Error("Hib�s t�blav�laszt�:" + matches[0]);
                                     Logger.Error("Iktat�sz�m form�tum:" + iktatoszam_formatum);
                                    throw new Exception("Hib�s t�blav�laszt�:" + matches[0]);
                                }
                        }
                        if (o != null)
                        {

                            fieldname = matches[1].Substring(0, matches[1].Length - 1);
                            if (!((fieldname == "IktatoHely") && (erec_IraIktatoKonyvek.KozpontiIktatasJelzo == "1")))
                            {
                                u = o.GetType().GetProperty(fieldname);
                                if (u == null)
                                {
                                    Logger.Error("Nincs ilyen mez� a keresett objektumban:" + field);
                                    Logger.Error("Iktat�sz�m form�tum:" + iktatoszam_formatum);
                                    throw new Exception("Nincs ilyen mez� a keresett objektumban:" + field);
                                }
                                if (!String.IsNullOrEmpty(u.GetValue(o, obRetVal).ToString()))
                                {
                                    if ((fieldname == "Foszam") && (Ugyiratok.Munkaanyag(erec_UgyUgyiratok)))
                                    {
                                        iktatoszam += field.Replace(selectorMatch.Value, "MU" + erec_UgyUgyiratok.Sorszam.ToString());
                                    }
                                    else if ((fieldname == "Alszam") && (Iratok.Munkaanyag(erec_IraIratok)))
                                    {
                                        iktatoszam += field.Replace(selectorMatch.Value, "MI" + erec_IraIratok.Sorszam.ToString());
                                    }
                                    else
                                        iktatoszam += field.Replace(selectorMatch.Value, u.GetValue(o, obRetVal).ToString());
                                }
                            }
                        }
                    }
                    else
                    {
                        Logger.Error("Hib�s mez�:" + selectorMatch);
                        Logger.Error("Iktat�sz�m form�tum:" + iktatoszam_formatum);
                        throw new Exception("Hib�s mez� (�gyirat):" + selectorMatch);
                    }

                }
            //}
            return iktatoszam;
        }

    }

}

