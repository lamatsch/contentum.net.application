using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;
using Contentum.eAdmin.Service;

namespace Contentum.eRecord.BaseUtility
{
    [Serializable]
    public class IratPeldanyok
    {
        [Serializable]
        public class Statusz
        {
            private string id = "";
            public string Id
            {
                get { return id; }
            }

            private string allapot = "";
            public string Allapot
            {
                get { return allapot; }
            }

            private string irat_Id;
            public string Irat_Id
            {
                get { return irat_Id; }
            }

            private string felhCsopId_Orzo;
            public string FelhCsopId_Orzo
            {
                get { return felhCsopId_Orzo; }
            }

            private string csopId_Felelos;
            public string CsopId_Felelos
            {
                get { return csopId_Felelos; }
            }

            private string _ugyiratId = null;
            public string UgyiratId
            {
                get { return _ugyiratId; }
                set { _ugyiratId = value; }
            }

            private string sorszam;
            public string Sorszam
            {
                get { return sorszam; }
            }

            private string _KuldesMod;

            public string KuldesMod
            {
                get { return _KuldesMod; }
            }


            /// <summary>
            /// Konstruktor
            /// </summary>
            /// <param name="_Allapot"></param>
            /// <param name="_UgyiratDarab_Id"></param>
            public Statusz(string _Id, string _Allapot, string _Irat_Id, string _FelhCsopId_Orzo, string _CsopId_Felelos, string _Sorszam, string _KuldesMod)
            {
                this.id = _Id;
                this.allapot = _Allapot;
                this.irat_Id = _Irat_Id;
                this.felhCsopId_Orzo = _FelhCsopId_Orzo;
                this.csopId_Felelos = _CsopId_Felelos;
                this.sorszam = _Sorszam;
                this._KuldesMod = _KuldesMod;
            }

            public Statusz(string _Id, string _Allapot, string _Irat_Id, string _FelhCsopId_Orzo, string _CsopId_Felelos, string UgyiratId, string _Sorszam, string _KuldesMod)
            {
                this.id = _Id;
                this.allapot = _Allapot;
                this.irat_Id = _Irat_Id;
                this.felhCsopId_Orzo = _FelhCsopId_Orzo;
                this.csopId_Felelos = _CsopId_Felelos;
                this.UgyiratId = UgyiratId;
                this.sorszam = _Sorszam;
                this._KuldesMod = _KuldesMod;
            }

            public enum ColumnTypes
            {
                Allapot,
                FelhCsopId_Orzo,
                CsopId_Felelos,
                Sorszam,
                KuldesMod,
                Cim
            }


            public ErrorDetails CreateErrorDetail(string message, Statusz.ColumnTypes columnType)
            {
                string str_columnType = "";
                string str_columnValue = "";
                switch (columnType)
                {
                    case ColumnTypes.Allapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Allapot;
                        str_columnValue = this.Allapot;
                        break;
                    case ColumnTypes.CsopId_Felelos:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos;
                        str_columnValue = this.CsopId_Felelos;
                        break;
                    case ColumnTypes.FelhCsopId_Orzo:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo;
                        str_columnValue = this.FelhCsopId_Orzo;
                        break;
                    case ColumnTypes.Sorszam:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Sorszam;
                        str_columnValue = this.Sorszam;
                        break;
                }

                return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, this.Id, str_columnType, str_columnValue);
            }


            public ErrorDetails CreateErrorDetail(int code, Statusz.ColumnTypes columnType)
            {
                string str_columnType = "";
                string str_columnValue = "";
                switch (columnType)
                {
                    case ColumnTypes.Allapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Allapot;
                        str_columnValue = this.Allapot;
                        break;
                    case ColumnTypes.CsopId_Felelos:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos;
                        str_columnValue = this.CsopId_Felelos;
                        break;
                    case ColumnTypes.FelhCsopId_Orzo:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo;
                        str_columnValue = this.FelhCsopId_Orzo;
                        break;
                }

                return new ErrorDetails(code, Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, this.Id, str_columnType, str_columnValue);
            }

        }


        private static class ErrorDetailMessages
        {
            public const string IratpeldanyAllapotNemMegfelelo = "Az iratp�ld�ny �llapota nem megfelel�.";
            public const string IratpeldanyAllapotNemExpedialt = "Az iratp�ld�ny nem expedi�lt �llapot�.";
            public const string IratpeldanynakNemOnAzOrzoje = "Az iratp�ld�nynak nem �n az �rz�je.";
            public const string UgyiratAllapotNemMegfelelo = "Az �gyirat �llapota nem megfelel�.";
            public const string IratAllapotNemMegfelelo = "Az irat �llapota nem megfelel�.";
            //public const string IratpeldanyEsUgyiratOrzojeEsKezelojeMegegyezik = "Az iratp�ld�nynak �s az �gyiratnak az �rz�je �s a kezel�je is megegyezik.";
            public const string IratpeldanyUgyiratbanVan = "Az iratp�ld�ny az �gyiratban van.";
            public const string CsakElsodlegesPldHelyezhetoUgyiratba = "Csak els�dleges iratp�ld�ny helyezhet� �t �gyiratba.";
            // CR3394
            public const string IratKiadmanyozando = "Az irat kiadm�nyozand�.";
        }

        public static Statusz GetAllapotById(String IratPeldany_Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            return GetAllapotById(IratPeldany_Id, execParam, EErrorPanel, false);
        }

        public static Statusz GetAllapotById(String IratPeldany_Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel, bool useGetAllWithExtension)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

            if (useGetAllWithExtension)
            {
                EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
                search.Id.Value = IratPeldany_Id;
                search.Id.Operator = Query.Operators.equals;

                Result result = service.GetAllWithExtension(execParam, search);
                ResultError.CreateNoRowResultError(result);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    return GetAllapotByDataSet(execParam, result.Ds)[IratPeldany_Id] as Statusz;
                }
                else
                {
                    if (EErrorPanel != null)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                    }
                    return null;
                }
            }
            else
            {
                EREC_PldIratPeldanyok erec_PldIratPeldanyok = null;
                execParam.Record_Id = IratPeldany_Id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result.Record;
                    return GetAllapotByBusinessDocument(erec_PldIratPeldanyok);
                }
                else
                {
                    if (EErrorPanel != null)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                    }
                    return null;
                }
            }
        }

        public static Statusz GetAllapotByBusinessDocument(EREC_PldIratPeldanyok erec_PldIratPeldanyok)
        {
            Statusz statusz = new Statusz(erec_PldIratPeldanyok.Id, erec_PldIratPeldanyok.Allapot, erec_PldIratPeldanyok.IraIrat_Id,
                erec_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo, erec_PldIratPeldanyok.Csoport_Id_Felelos, erec_PldIratPeldanyok.Sorszam, erec_PldIratPeldanyok.KuldesMod);

            return statusz;
        }

        /// <summary>
        /// T�bb iratp�ld�ny �llapot�t adja vissza egyszerre DataSet-b�l
        /// </summary>
        /// <param name="ep">execParam</param>
        /// <param name="ds">DataSet</param>
        /// <returns>HashTable Statusz elemekkel. Hivatkozni r�juk: statusz["Id"]-val lehet.</returns>
        public static Hashtable GetAllapotByDataSet(ExecParam ep, System.Data.DataSet ds)
        {
            if (ds.Tables.Count == 0) return new Hashtable(0);
            if (ds.Tables[0].Rows.Count == 0) return new Hashtable(0);
            Hashtable statusz = new Hashtable(ds.Tables[0].Rows.Count);

            try
            {
                foreach (System.Data.DataRow r in ds.Tables[0].Rows)
                {
                    statusz[r["Id"].ToString()] = GetAllapotByDataRow(r);
                }
            }
            catch
            {
                return new Hashtable(0);
            }

            return statusz;
        }

        public static Statusz GetAllapotByDataRow(DataRow row)
        {
            bool IsUgyiratId = false;
            if (row.Table.Columns.Contains("UgyiratId"))
                IsUgyiratId = true;

            Statusz statusz = null;
            if (IsUgyiratId)
                statusz = new Statusz(
                    row["Id"].ToString(),
                    row["Allapot"].ToString(),
                    row["IraIrat_Id"].ToString(),
                    row["FelhasznaloCsoport_Id_Orzo"].ToString(),
                    row["Csoport_Id_Felelos"].ToString(),
                    row["UgyiratId"].ToString(),
                    row["Sorszam"].ToString(),
                    row["KuldesMod"].ToString());
            else
                statusz = new Statusz(
                    row["Id"].ToString(),
                    row["Allapot"].ToString(),
                    row["IraIrat_Id"].ToString(),
                    row["FelhasznaloCsoport_Id_Orzo"].ToString(),
                    row["Csoport_Id_Felelos"].ToString(),
                    row["Sorszam"].ToString(),
                    row["KuldesMod"].ToString());

            return statusz;
        }


        /// <summary>
        /// Az iratp�ld�ny st�tuszok mellett az irat, �gyirat st�tuszokat is visszaadja egy-egy HashTable-ben. 
        /// Ezeket is az iratp�ld�ny id-j�val lehet megc�mezni.
        /// A dataset-et az Iratp�ld�nyok GetAllWithExtensionnel kell el��ll�tani!
        /// </summary>        
        public static void GetAllapotHierarchiaByDataSet(ExecParam execParam, System.Data.DataSet ds
            , out Hashtable iratPeldanyStatuszok, out Hashtable iratStatuszok, out Hashtable ugyiratStatuszok)
        {
            iratPeldanyStatuszok = null;
            iratStatuszok = null;
            ugyiratStatuszok = null;
            if (ds == null) { return; }

            iratPeldanyStatuszok = GetAllapotByDataSet(execParam, ds);

            // irat �s �gyirat st�tuszok el��ll�t�sa:
            iratStatuszok = new Hashtable(ds.Tables[0].Rows.Count);
            ugyiratStatuszok = new Hashtable(ds.Tables[0].Rows.Count);

            foreach (System.Data.DataRow row in ds.Tables[0].Rows)
            {
                string pld_Id = row["Id"].ToString();

                #region Irat st�tusz:

                // hozz�ad�s a HashTable-hez:
                iratStatuszok[pld_Id] = GetIratAllapotByDataRow(row);

                #endregion

                #region �gyirat st�tusz:

                // hozz�ad�s a HashTable-hez:
                ugyiratStatuszok[pld_Id] = GetUgyiratAllapotByDataRow(row);

                #endregion
            }

        }

        public static Ugyiratok.Statusz GetUgyiratAllapotByDataRow(DataRow row)
        {
            string Ugyirat_Id = row["UgyiratId"].ToString();
            string Ugyirat_Allapot = row["EREC_UgyUgyiratok_Allapot"].ToString();
            string Ugyirat_FelhCsopIdOrzo = row["EREC_UgyUgyiratok_FelhCsopIdOrzo"].ToString();
            string Ugyirat_TovabbitasAlattAllapot = row["EREC_UgyUgyiratok_TovabbitasAlattAllapot"].ToString();
            string Ugyirat_CsopIdFelelos = row["EREC_UgyUgyiratok_CsopIdFelelos"].ToString();
            string Ugyirat_Kovetkezo_Felelos_Id = row["EREC_UgyUgyiratok_Kovetkezo_Felelos_Id"].ToString();
            string Ugyirat_FelhCsopIdUgyintezo = row["EREC_UgyUgyiratok_FelhCsopIdUgyintezo"].ToString();
            string Ugyirat_RegirendszerIktatoszam = row["EREC_UgyUgyiratok_RegirendszerIktatoszam"].ToString();
            string Ugyirat_LezarasDat = row["EREC_UgyUgyiratok_LezarasDat"].ToString();
            string Ugyirat_IraIktatokonyv_Id = row["EREC_UgyUgyiratok_IraIktatokonyv_Id"].ToString();
            string Ugyirat_Foszam = row["Foszam"].ToString();
            string Ugyirat_Jelleg = row["EREC_UgyUgyiratok_Jelleg"].ToString();
            // CR3348 Iktat�k�nyv vlaszt�s ITSZ n�lk�l (ITSZ_ELORE rendszerparam�tert�l f�gg)
            // ITSZ ellen�rz�s lez�r�skor (St�tusz b�v�t�s)
            string Ugyirat_IraIrattariTetel_Id = row["EREC_UgyUgyiratok_IraIrattariTetel_Id"].ToString();

            return new Ugyiratok.Statusz(
                Ugyirat_Id, Ugyirat_Allapot, Ugyirat_FelhCsopIdOrzo, Ugyirat_TovabbitasAlattAllapot
                , Ugyirat_CsopIdFelelos, Ugyirat_Kovetkezo_Felelos_Id, Ugyirat_FelhCsopIdUgyintezo
                , Ugyirat_RegirendszerIktatoszam, Ugyirat_LezarasDat, Ugyirat_IraIktatokonyv_Id, Ugyirat_Foszam, Ugyirat_Jelleg, Ugyirat_IraIrattariTetel_Id);
        }

        public static Iratok.Statusz GetIratAllapotByDataRow(DataRow row)
        {
            string Irat_Id = row["IraIrat_Id"].ToString();
            string Irat_Allapot = row["EREC_IraIratok_Allapot"].ToString();
            string Irat_UgyiratDarab_Id = row["EREC_IraIratok_UgyUgyIratDarab_Id"].ToString();
            string Irat_felhCsopId_Iktato = row["FelhasznaloCsoport_Id_Iktato"].ToString();
            string Irat_Ugyirat_Id = row["EREC_UgyUgyiratdarabok_UgyUgyirat_Id"].ToString();
            string Irat_Alszam = row["Alszam"].ToString();

            // irathoz els� iratp�ld�ny �rz�j�t csak akkor �ll�tjuk be, ha ez az iratp�ld�ny volt az els�
            // TODO: minden esetben be k�ne �ll�tani az els� iratp�ld�ny �rz�j�t
            string elsoIratPeldanyOrzo_Id = String.Empty;
            string elsoIratPeldanyAllapot = String.Empty;
            string iratPeldany_Sorszam = row["Sorszam"].ToString();
            if (iratPeldany_Sorszam == "1")
            {
                // iratp�ld�ny �rz�je:
                elsoIratPeldanyOrzo_Id = row["FelhasznaloCsoport_Id_Orzo"].ToString();
                // iratp�ld�ny �llapota:
                elsoIratPeldanyAllapot = row["Allapot"].ToString();
            }

            return new Iratok.Statusz(Irat_Id, Irat_Allapot, Irat_UgyiratDarab_Id, Irat_Ugyirat_Id
                , Irat_felhCsopId_Iktato, elsoIratPeldanyOrzo_Id, elsoIratPeldanyAllapot, Irat_Alszam);
        }

        public static bool Modosithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (statusz.FelhCsopId_Orzo != FelhasznaloProfil.GetFelhasznaloCsoport(execParam))
            {
                // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }
            else
            {
                return ModosithatoAllapotu(statusz, execParam, out errorDetail);
            }
        }

        public static bool IsModosithatoOveride(Statusz statusz, ExecParam execParam)
        {
            if (statusz.FelhCsopId_Orzo != FelhasznaloProfil.GetFelhasznaloCsoport(execParam))
            {
                return false;
            }
            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRATPELDANY_MINDIG_MODOSITHATO) && statusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Sztornozott)
            {
                return true;
            }
            return false;
        }

        public static bool Modosithato(Statusz statusz, Iratok.Statusz iratokStatusz, Ugyiratok.Statusz ugyiratokStatusz
            , ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (statusz.FelhCsopId_Orzo != FelhasznaloProfil.GetFelhasznaloCsoport(execParam))
            {
                // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }
            else
            {
                return ModosithatoAllapotu(statusz, iratokStatusz, ugyiratokStatusz, execParam, out errorDetail);
            }
        }

        public static bool ModosithatoAllapotu(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.IRATPELDANY_ALLAPOT.Iktatott:
                case KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt:
                case KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban:

                    // st�tuszok lek�r�se:
                    Iratok.Statusz iratStatusz = Iratok.GetAllapotById(statusz.Irat_Id, execParam, null);
                    UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotById(iratStatusz.UgyiratDarab_Id, execParam, null);
                    Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(ugyiratDarabStatusz.Ugyirat_Id, execParam, null);

                    return ModosithatoAllapotu(statusz, iratStatusz, ugyiratStatusz, execParam, out errorDetail);

                //return Iratok.ModosithatoAllapotu(iratStatusz, execParam);
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }


        public static bool ModosithatoAllapotu(Statusz statusz, Iratok.Statusz iratokStatusz, Ugyiratok.Statusz ugyiratokStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.IRATPELDANY_ALLAPOT.Iktatott:
                case KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt:
                case KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban:
                case KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott:

                    // BUG_8003
                    if (Iratok.IsIratIntezkedesreAtvetelElerheto(execParam) && iratokStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Szignalt)
                    {
                        errorDetail = iratokStatusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotNemMegfelelo, Iratok.Statusz.ColumnTypes.Allapot);
                        return false;
                    }

                    // Irat �llapot�nak vizsg�lata:
                    switch (iratokStatusz.Allapot)
                    {
                        case KodTarak.IRAT_ALLAPOT.Iktatott:
                        case KodTarak.IRAT_ALLAPOT.Elintezett:
                        case KodTarak.IRAT_ALLAPOT.IntezkedesAlatt:
                        case KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat:
                        case KodTarak.IRAT_ALLAPOT.Iktatando_Munkairat:
                        case KodTarak.IRAT_ALLAPOT.Szignalt:
                        case KodTarak.IRAT_ALLAPOT.Kiadmanyozott:
                            {
                                // �gyirat �llapot vizsg�lata:
                                // (A Tov�bb�t�s alatti, �s a Szign�lt �llapotot is bele kell venni) 
                                switch (ugyiratokStatusz.Allapot)
                                {
                                    case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                                    case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                                    case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                                    case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
                                    case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                                        return true;
                                    case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                                        {
                                            switch (ugyiratokStatusz.TovabbitasAlattiAllapot)
                                            {
                                                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                                                case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                                                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                                                case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
                                                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                                                    return true;
                                                default:
                                                    errorDetail = ugyiratokStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                                                    return false;
                                            }
                                        }
                                    default:
                                        errorDetail = ugyiratokStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                                        return false;
                                }
                            }
                        default:
                            {
                                errorDetail = iratokStatusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotNemMegfelelo, Iratok.Statusz.ColumnTypes.Allapot);
                                return false;
                            }
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool Sztornozott(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Sztornozott)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool Jovahagyas_alatt(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool Iktatott(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Iktatott)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool Kiadmanyozott(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool Ujrakuldendo(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool Postazott(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Postazott)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        //public static bool AtadasraKijelolheto(Statusz iratPeldanyStatusz, ExecParam execParam)
        //{
        //    ErrorDetails errorDetail;
        //    return AtadasraKijelolheto(iratPeldanyStatusz, execParam, out errorDetail);
        //}

        public static bool AtadasraKijelolheto(Statusz iratPeldanyStatusz
           , ExecParam execParam, out ErrorDetails errorDetail)
        {
            // Gyors�tott kil�p�s:
            if (iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Iktatott
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban
                )
            {
                errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
            else
            {
                Iratok.Statusz iratStatusz = Iratok.GetAllapotById(iratPeldanyStatusz.Irat_Id, execParam, null);
                UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotById(
                    iratStatusz.UgyiratDarab_Id, execParam, null);
                Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(
                    ugyiratDarabStatusz.Ugyirat_Id, execParam, null);

                return AtadasraKijelolheto(iratPeldanyStatusz, iratStatusz, ugyiratStatusz, execParam, out errorDetail);
            }
        }

        public static bool KiadmoanyozoAlairhatja(Statusz statusz)
        {
            return true;
            /*BUG 7623*/
            /* switch(statusz.Allapot){
                 case KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt:
                 case KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt:
                     return false;
                 default:
                     return true;
             }*/
        }


        //public static bool AtadasraKijelolheto(Statusz iratPeldanyStatusz
        //    , Iratok.Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam)
        //{
        //    ErrorDetails errorDetail;
        //    return AtadasraKijelolheto(iratPeldanyStatusz, iratStatusz, ugyiratStatusz, execParam, out errorDetail);
        //}

        public static bool AtadasraKijelolheto(Statusz iratPeldanyStatusz
            , Iratok.Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz
            , ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhaszn�l� az �rz�:
            if (iratPeldanyStatusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (iratPeldanyStatusz.Allapot)
            {
                case KodTarak.IRATPELDANY_ALLAPOT.Iktatott:
                case KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt:
                case KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott:
                case KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban:
                    {
                        // Ellen�rz�s felfele (�gyirat szint):
                        switch (ugyiratStatusz.Allapot)
                        {
                            // Ezekben az esetekben nem adhat� �t:
                            case KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett:
                            case KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo:
                            case KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott:
                            case KodTarak.UGYIRAT_ALLAPOT.Selejtezett:
                                //case KodTarak.UGYIRAT_ALLAPOT.Szerelt:
                                //case KodTarak.UGYIRAT_ALLAPOT.Sztornozott:
                                errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                                return false;
                            default:
                                return true;
                        }
                    }
                default:
                    errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }

        }

        public static bool Atadhato(Statusz iratPeldanyStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            // Gyors�tott kil�p�s:
            if (iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Iktatott
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban
                )
            {
                errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
            else
            {
                Iratok.Statusz iratStatusz = Iratok.GetAllapotById(iratPeldanyStatusz.Irat_Id, execParam, null);
                UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotById(
                    iratStatusz.UgyiratDarab_Id, execParam, null);
                Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(
                    ugyiratDarabStatusz.Ugyirat_Id, execParam, null);

                return Atadhato(iratPeldanyStatusz, iratStatusz
                    , ugyiratStatusz, execParam, out errorDetail);
            }
        }

        public static bool Atadhato(Statusz iratPeldanyStatusz
            , Iratok.Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz
            , ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhaszn�l� az �rz�:
            if (iratPeldanyStatusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (iratPeldanyStatusz.Allapot)
            {
                case KodTarak.IRATPELDANY_ALLAPOT.Iktatott:
                case KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt:
                case KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott:
                case KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban:
                    {
                        // Ellen�rz�s felfele (�gyirat szint):
                        switch (ugyiratStatusz.Allapot)
                        {
                            // Ezekben az esetekben nem adhat� �t:
                            case KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett:
                            case KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo:
                            case KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott:
                            case KodTarak.UGYIRAT_ALLAPOT.Selejtezett:
                                //case KodTarak.UGYIRAT_ALLAPOT.Szerelt:
                                //case KodTarak.UGYIRAT_ALLAPOT.Sztornozott:
                                errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                                return false;
                            default:
                                return true;
                        }
                    }
                default:
                    errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }




        public static bool AtadasraKijelolesVisszavonhato(Statusz statusz, ExecParam execParam)
        {
            // ha nem a felhaszn�l� az �rz�:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Atveheto(Statusz iratPeldanyStatusz, Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam)
        {
            ErrorDetails errorDetail;
            return Atveheto(iratPeldanyStatusz, ugyiratStatusz, execParam, out errorDetail);
        }

        public static bool Atveheto(Statusz iratPeldanyStatusz, Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (AtvehetoAllapotu(iratPeldanyStatusz, out errorDetail) == false)
            {
                return false;
            }
            else
            {
                // Ezt nem itt ellen�rizz�k:
                //// A felhaszn�l� tagja-e a felel�s csoportnak:
                //if (Csoportok.IsMember(execParam.Felhasznalo_Id, iratPeldanyStatusz.CsopId_Felelos) == false)
                //{
                //    return false;
                //}

                // Ellen�rz�s felfele (�gyirat szint):
                switch (ugyiratStatusz.Allapot)
                {
                    // Ezekben az esetekben nem vehet� m�r �t:
                    case KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett:
                    case KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo:
                    case KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott:
                    case KodTarak.UGYIRAT_ALLAPOT.Selejtezett:
                        //case KodTarak.UGYIRAT_ALLAPOT.Szerelt:
                        //case KodTarak.UGYIRAT_ALLAPOT.Sztornozott:
                        errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                        return false;
                    default:
                        return true;
                }
            }
        }

        public static bool Visszakuldheto(ExecParam execParam, Statusz statusz)
        {
            ErrorDetails errorDetail;
            return Visszakuldheto(execParam, statusz, out errorDetail);
        }

        public static bool Visszakuldheto(ExecParam execParam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt:
                    if (execParam.Felhasznalo_Id.Equals(statusz.FelhCsopId_Orzo, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // A t�tel nem k�ldhet� vissza, mert azt �n adta �t �s csak a c�mzett k�ldheti vissza �nnek!
                        errorDetail = statusz.CreateErrorDetail("A t�tel nem k�ldhet� vissza, mert azt �n adta �t �s csak a c�mzett k�ldheti vissza �nnek!", Statusz.ColumnTypes.CsopId_Felelos);
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }

        }

        private static string GetOrgKod(ExecParam execParam)
        {
            String orgKod = String.Empty;
            Result org = new Result();
            using (KRT_OrgokService service = eAdminService.ServiceFactory.GetKRT_OrgokService())
            {
                ExecParam _exec = execParam.Clone();
                _exec.Record_Id = execParam.Org_Id;
                org = service.Get(_exec);
            }

            if (!org.IsError) orgKod = ((KRT_Orgok)org.Record).Kod;
            return orgKod;
        }

        public static bool UgyiratbaHelyezheto(Statusz iratPeldanyStatusz, Iratok.Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz
            , ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Nem csak els�dleges iratp�ld�nyokra enged�lyezett, b�rmelyikre mehet (2009.03.09 CR#1962)
            //// Csak els�dleges iratp�ld�nyokra enged�lyezett:
            //if (iratPeldanyStatusz.Sorszam != "1")
            //{
            //    errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.CsakElsodlegesPldHelyezhetoUgyiratba, Statusz.ColumnTypes.Sorszam);
            //    return false;
            //}

            // ha nem a felhaszn�l� az �rz�:
            if (iratPeldanyStatusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            if (iratPeldanyStatusz.FelhCsopId_Orzo == ugyiratStatusz.FelhCsopId_Orzo
                && iratPeldanyStatusz.CsopId_Felelos == ugyiratStatusz.CsopId_Felelos)
            {
                errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyUgyiratbanVan, Ugyiratok.Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            String orgKod = GetOrgKod(execParam);
            bool isBOPMH = (orgKod == Constants.OrgKod.BOPMH);
            bool isNotHivataliKapusKikuldes = (iratPeldanyStatusz.KuldesMod != KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu);
            bool isPeldanyExpedialt = (iratPeldanyStatusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Expedialt);


            // Iratp�ld�ny �llapot ellen�rz�s:
            switch (iratPeldanyStatusz.Allapot)
            {
                // Csak iktatott �llapot� lehet:
                case KodTarak.IRATPELDANY_ALLAPOT.Iktatott:
                case KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban:
                // CR3108 : Post�zott st�tusz� iratp�ld�nyok Ugyiratba helyezhet�ek
                case KodTarak.IRATPELDANY_ALLAPOT.Postazott:
                // CR3255 : �jrak�ldend� iratp�ld�nyt is lehessen �gyiratba szerelni
                case KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo:
                // CR3309 : Expedi�lt iratp�ld�nyt is lehessen �gyiratba helyezni
                case KodTarak.IRATPELDANY_ALLAPOT.Expedialt:
                // BLG_2768
                case KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette:
                case KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at:
                //BUG_4051
                case KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott:
                    {
                        //BOPMH: post�n kik�ld�tt iratp�ld�ny csak post�zott �llapotban ker�lhessen vissza az �gyiratba
                        if (isBOPMH && isPeldanyExpedialt && isNotHivataliKapusKikuldes)
                        {
                            errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                            return false;
                        }

                        // �gyirat �llapot ellen�rz�se:
                        switch (ugyiratStatusz.Allapot)
                        {
                            // ezek az �llapotok NEM lehetnek:
                            case KodTarak.UGYIRAT_ALLAPOT.Sztornozott:
                            case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                                errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                                return false;
                            default:
                                return true;
                        }
                    }
                default:
                    errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool AtvehetoAllapotu(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.IRATPELDANY_ALLAPOT.Iktatott:
                case KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt:
                case KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott:
                case KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        /// <summary>
        /// Iratp�ld�ny sztorn�zhat�-e        
        /// </summary>
        /// <param name="statusz"></param>
        /// <param name="execParam"></param>
        /// <returns></returns>
        public static bool Sztornozhato(Statusz statusz, Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhaszn�l� az �rz�:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            // return true, ha iratp�ld�ny �llapota ok, �s �gyirat nyitott
            switch (statusz.Allapot)
            {
                case KodTarak.IRATPELDANY_ALLAPOT.Iktatott:
                case KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban:
                case KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt:
                case KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott:
                case KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt:
                    if (Ugyiratok.Nyitott(ugyiratStatusz))
                    {
                        return true;
                    }
                    else
                    {
                        errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                        return false;
                    }

                // iratp�ld�ny �llapot miatt nem sztorn�zhat�
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static string[] GetExpedialhatoAllapotokFilter()
        {
            return new string[] {
                KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott,
                KodTarak.IRATPELDANY_ALLAPOT.Iktatott,
                KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben,
                KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo
            };
        }

        /// <summary>
        /// Iratp�ld�ny expedi�lhat�-e?
        /// </summary>
        public static bool Expedialhato(Statusz iratPeldanyStatusz, bool kiadmanyozando, Iratok.Statusz iratStatusz
            , Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhaszn�l� az �rz�:
            if (iratPeldanyStatusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                //CR 3048: Iratp�ld�nynak nem az �rz�je (errorcode-val)
                // errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                errorDetail = iratPeldanyStatusz.CreateErrorDetail(80011, Statusz.ColumnTypes.FelhCsopId_Orzo);  //ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje
                return false;
            }
            Logger.Debug("EXPED:" + iratPeldanyStatusz.Allapot);
            if (iratPeldanyStatusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott
                || (iratPeldanyStatusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Iktatott && !kiadmanyozando)
                || iratPeldanyStatusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben
                || iratPeldanyStatusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo)
            {
                return Iratok.ExpedialhatoAllapotu(iratStatusz, kiadmanyozando, ugyiratStatusz, execParam, out errorDetail);
            }
            else
            {
                //CR3394
                if (iratPeldanyStatusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Iktatott && kiadmanyozando)
                {
                    errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratKiadmanyozando, Statusz.ColumnTypes.Allapot);
                }
                else
                    errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
        }

        public static bool TomegesenExpedialhato(EREC_PldIratPeldanyok peldany, bool kiadmanyozando, Iratok.Statusz iratStatusz
            , Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotByBusinessDocument(peldany);
            if (String.IsNullOrEmpty(peldany.KuldesMod))
            {
                errorDetail = iratPeldanyStatusz.CreateErrorDetail("Az expedi�l�s m�dja �res!", Statusz.ColumnTypes.KuldesMod);
                return false;
            }
            if (string.IsNullOrEmpty(peldany.CimSTR_Cimzett) && !string.IsNullOrEmpty(peldany.Cim_id_Cimzett)) // elofordul olyan eset amikor az iratpeldanyon ures a CimSTR_Cimzett de a Cim_id_cimzett ott van.
            {
                peldany.CimSTR_Cimzett = Contentum.eUtility.CimUtility.GetCimSTRById(execParam, peldany.Cim_id_Cimzett);
            }
            if (String.IsNullOrEmpty(peldany.CimSTR_Cimzett) &&
                peldany.NevSTR_Cimzett != Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.UGYIRATBAN_MARADO_PELDANY_CIMZETTJE) // BUG_6549
            )
            {
                errorDetail = iratPeldanyStatusz.CreateErrorDetail("A c�mzett c�me �res!", Statusz.ColumnTypes.Cim);
                return false;
            }

            return Expedialhato(iratPeldanyStatusz, kiadmanyozando, iratStatusz, ugyiratStatusz, execParam, out errorDetail);
        }

        //nekrisz CR 2961
        public static string[] GetPostazhatoAllapotokFilter()
        {
            return new string[] {
                KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott,
                KodTarak.IRATPELDANY_ALLAPOT.Iktatott,
                KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben,
                KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo,
           //     KodTarak.IRATPELDANY_ALLAPOT.Expedialt

            };
        }

        /// <summary>
        /// Iratp�ld�ny post�zhat�-e?
        /// </summary>
        public static bool Postazhato(Statusz iratPeldanyStatusz, bool kiadmanyozando, Iratok.Statusz iratStatusz
            , Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhaszn�l� az �rz�:
            if (iratPeldanyStatusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                //CR 3048: Iratp�ld�nynak nem az �rz�je (errorcode-val)
                //errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                errorDetail = iratPeldanyStatusz.CreateErrorDetail(80011, Statusz.ColumnTypes.FelhCsopId_Orzo); //ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje

                return false;
            }

            if (iratPeldanyStatusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Expedialt)
            {
                // TODO: elvileg j� ez is, de k�ne egy saj�t a post�z�sra
                return Iratok.ExpedialhatoAllapotu(iratStatusz, kiadmanyozando, ugyiratStatusz, execParam, out errorDetail);
            }
            else
            {
                errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemExpedialt, Statusz.ColumnTypes.Allapot);
                return false;
            }
        }

        // BLG_292
        // egy�gk�nt kezelend� azonos�t� r�sz + egyedi azonos�t�s, ha van
        //private static string GetIktatoSzam_Azonosito(string foszamStr, string alszamStr, string evStr, string azonositoStr)
        //{
        //    return foszamStr + " - " + alszamStr + " /" + evStr + (!String.IsNullOrEmpty(azonositoStr) ? " /" + azonositoStr : ""); ;
        //}


        // BLG_292
        //public static String GetFullIktatoszam(EREC_IraIktatoKonyvek erec_IraIktatoKonyvek, EREC_UgyUgyiratok erec_UgyUgyiratok,
        //   EREC_IraIratok erec_IraIratok, EREC_PldIratPeldanyok erec_PldIratPeldanyok)
        //{
        //    if (erec_IraIktatoKonyvek != null && erec_UgyUgyiratok != null && erec_IraIratok != null && erec_PldIratPeldanyok != null)
        //    {
        //        string alszamStr = erec_IraIratok.Alszam;

        //        if (Iratok.Munkaanyag(erec_IraIratok))
        //        {
        //            // munkaanyagn�l 'MI[Sorszam]' form�tumot jelen�t�nk meg:
        //            //alszamStr = "??";
        //            alszamStr = "MI" + erec_IraIratok.Sorszam;
        //        }

        //        string foszamStr = erec_UgyUgyiratok.Foszam;

        //        if (Ugyiratok.Munkaanyag(erec_UgyUgyiratok))
        //        {
        //            // munkaanyagn�l 'MU[Sorszam]' form�tumot jelen�t�nk meg:
        //            //foszamStr = "??";
        //            foszamStr = "MU" + erec_UgyUgyiratok.Sorszam;
        //        }

        //        if (erec_IraIktatoKonyvek.KozpontiIktatasJelzo == "1")
        //        {
        //            // K�zponti iktat�s jelz�:
        //            //return foszamStr + " - " + alszamStr + " /" + erec_IraIktatoKonyvek.Ev
        //            //    + (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.MegkulJelzes) ? " /" + erec_IraIktatoKonyvek.MegkulJelzes : "") + " (" + erec_PldIratPeldanyok.Sorszam + ")";
        //            return GetIktatoSzam_Azonosito(foszamStr, alszamStr, erec_IraIktatoKonyvek.Ev, erec_IraIktatoKonyvek.MegkulJelzes)
        //                + " (" + erec_PldIratPeldanyok.Sorszam + ")";

        //        }
        //        else
        //        {
        //            //return erec_IraIktatoKonyvek.Iktatohely + "/" + foszamStr + " - " + alszamStr + "/" + erec_IraIktatoKonyvek.Ev
        //            //    + (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.MegkulJelzes) ? " /" + erec_IraIktatoKonyvek.MegkulJelzes : "") + " (" + erec_PldIratPeldanyok.Sorszam + ")";
        //            return erec_IraIktatoKonyvek.Iktatohely + " /" + GetIktatoSzam_Azonosito(foszamStr, alszamStr, erec_IraIktatoKonyvek.Ev, erec_IraIktatoKonyvek.MegkulJelzes)
        //                + " (" + erec_PldIratPeldanyok.Sorszam + ")";

        //        }
        //    }
        //    else
        //    {
        //        return String.Empty;
        //    }
        //}

        public static String GetFullIktatoszam(ExecParam execParam, EREC_IraIktatoKonyvek erec_IraIktatoKonyvek, EREC_UgyUgyiratok erec_UgyUgyiratok,
           EREC_IraIratok erec_IraIratok, EREC_PldIratPeldanyok erec_PldIratPeldanyok)
        {
            return Iktatoszam.GetFullAzonosito(execParam, erec_UgyUgyiratok, erec_IraIktatoKonyvek, erec_IraIratok, erec_PldIratPeldanyok);
        }

        /// <summary>
        /// Iratp�ld�ny kapcsolhat� m�sik objektumhoz
        /// Ha �llapot nem szt�rnoz�tt, lez�t jegyz�ken l�v� vagy jegyz�kre helyzett
        /// </summary>
        /// <param name="statusz"></param>
        /// <returns>bool</returns>  
        public static bool Csatolhato(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            switch (statusz.Allapot)
            {
                case KodTarak.IRATPELDANY_ALLAPOT.Sztornozott:
                case KodTarak.IRATPELDANY_ALLAPOT.Lezart_jegyzeken:
                case KodTarak.IRATPELDANY_ALLAPOT.Jegyzekre_helyezett:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
                default:
                    return true;
            }
        }

        public static void SetCsatolhatoSearchObject(EREC_PldIratPeldanyokSearch search)
        {
            search.Manual_Csatolhato.Value = "'"
                + KodTarak.IRATPELDANY_ALLAPOT.Sztornozott + "','"
                + KodTarak.IRATPELDANY_ALLAPOT.Lezart_jegyzeken + "','"
                + KodTarak.IRATPELDANY_ALLAPOT.Jegyzekre_helyezett + "'";

            search.Manual_Csatolhato.Operator = Query.Operators.notinner;
        }

        /// <summary>
        /// Az iat �s az iratp�ld�ny azonos �gyirathoz tartoznak-e
        /// </summary>
        /// <param name="statuszIrat"></param>
        /// <param name="statuszIratPeldany"></param>
        /// <returns></returns>
        public static bool IsIratAndIratPeldanyInSameUgyirat(Iratok.Statusz statuszIrat, Statusz statuszIratPeldany)
        {
            if (statuszIrat.UgyiratId == null || statuszIratPeldany.UgyiratId == null)
                return true;

            if (statuszIrat.UgyiratId != statuszIratPeldany.UgyiratId)
                return false;

            return true;
        }

        public static bool JegyzekreHelyezheto(Statusz iratPeldanyStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Iktatott
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt
                && iratPeldanyStatusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban
                )
            {
                errorDetail = iratPeldanyStatusz.CreateErrorDetail(ErrorDetailMessages.IratpeldanyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }

            return true;
        }

    }
}
