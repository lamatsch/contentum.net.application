using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eAdmin.Service;
using Contentum.eUtility;
using System.Collections;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;

namespace Contentum.eRecord.BaseUtility
{
    [Serializable]
    public class Iratok
    {
        [Serializable]
        public class Statusz
        {
            private string id = "";
            public string Id
            {
                get { return id; }
            }

            private string allapot = "";
            public string Allapot
            {
                get { return allapot; }
            }

            private string ugyiratDarab_Id;
            public string UgyiratDarab_Id
            {
                get { return ugyiratDarab_Id; }
            }

            private string _ugyiratId = null;
            public string UgyiratId
            {
                get { return _ugyiratId; }                
            }

            private String felhCsopId_Orzo;
            public String FelhCsopId_Orzo
            {
                get { return felhCsopId_Orzo; }
            }

            private string felhCsopId_Iktato;
            public string FelhCsopId_Iktato
            {
                get { return felhCsopId_Iktato; }
            }

            private string elsoIratPeldanyOrzo_Id = String.Empty;
            public string ElsoIratPeldanyOrzo_Id
            {
                get { return elsoIratPeldanyOrzo_Id; }
            }

            private string elsoIratPeldanyAllapot = String.Empty;
            public string ElsoIratPeldanyAllapot
            {
                get { return elsoIratPeldanyAllapot; }
            }
            
            private string alszam = null;
            public bool Munkairat
            {
                get
                {
                    if (String.IsNullOrEmpty(alszam) || alszam == "0")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            //public Statusz(string _Id, string _Allapot, string _UgyiratDarab_Id, string UgyiratId
            //    , string _FelhCsopId_Iktato, string _ElsoIratPeldanyOrzo_Id, string _alszam)
            //{
            //    this.id = _Id;
            //    this.allapot = _Allapot;
            //    this.ugyiratDarab_Id = _UgyiratDarab_Id;
            //    this._ugyiratId = UgyiratId;
            //    this.felhCsopId_Iktato = _FelhCsopId_Iktato;
            //    this.elsoIratPeldanyOrzo_Id = _ElsoIratPeldanyOrzo_Id;
            //    this.alszam = _alszam;
            //}

            public Statusz(string _Id, string _Allapot, string _UgyiratDarab_Id, string UgyiratId
            , string _FelhCsopId_Iktato, string _ElsoIratPeldanyOrzo_Id, string _ElsoIratPeldanyAllapot, string _alszam)
            {
                this.id = _Id;
                this.allapot = _Allapot;
                this.ugyiratDarab_Id = _UgyiratDarab_Id;
                this._ugyiratId = UgyiratId;
                this.felhCsopId_Iktato = _FelhCsopId_Iktato;
                this.elsoIratPeldanyOrzo_Id = _ElsoIratPeldanyOrzo_Id;
                this.elsoIratPeldanyAllapot = _ElsoIratPeldanyAllapot;
                this.alszam = _alszam;
            }

            public Statusz(string _Id, string _Allapot, string _FelhCsopId_Orzo, string _UgyiratDarab_Id, string UgyiratId
                , string _FelhCsopId_Iktato, string _ElsoIratPeldanyOrzo_Id, string _ElsoIratPeldanyAllapot, string _alszam)
            {
                this.id = _Id;
                this.allapot = _Allapot;
                this.felhCsopId_Orzo = _FelhCsopId_Orzo;
                this.ugyiratDarab_Id = _UgyiratDarab_Id;
                this._ugyiratId = UgyiratId;
                this.felhCsopId_Iktato = _FelhCsopId_Iktato;
                this.elsoIratPeldanyOrzo_Id = _ElsoIratPeldanyOrzo_Id;
                this.elsoIratPeldanyAllapot = _ElsoIratPeldanyAllapot;
                this.alszam = _alszam;
            }

            public enum ColumnTypes
            {
                Allapot,
                Munkaanyag,
                FelhCsopId_Orzo
            }

            public ErrorDetails CreateErrorDetail(string message)
            {
                return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_IraIratok, this.Id, String.Empty, String.Empty);
            }

            public ErrorDetails CreateErrorDetail(string message, Statusz.ColumnTypes columnType)
            {
                string str_columnType = "";
                string str_columnValue = "";
                switch (columnType)
                {
                    case Statusz.ColumnTypes.Allapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Allapot;
                        str_columnValue = this.Allapot;
                        break;
                    case ColumnTypes.FelhCsopId_Orzo:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo;
                        str_columnValue = this.FelhCsopId_Orzo;
                        break;
                }

                return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_IraIratok, this.Id, str_columnType, str_columnValue);
            }

            public ErrorDetails CreateErrorDetail(int code, Statusz.ColumnTypes columnType)
            {
                string str_columnType = "";
                string str_columnValue = "";
                switch (columnType)
                {
                    case Statusz.ColumnTypes.Allapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Allapot;
                        str_columnValue = this.Allapot;
                        break;
                }

                return new ErrorDetails(code, Constants.ErrorDetails.ObjectTypes.EREC_IraIratok, this.Id, str_columnType, str_columnValue);
            }


        }

        public static class ErrorDetailMessages
        {
            public const string IratAllapotaNemMegfelelo = "Az irat �llapota nem megfelel�.";
            public const string IratMunkaanyag = "Az irat m�g munkaanyag.";
            public const string IratNEMmunkaanyag = "Az irat nem munkaanyag.";
            public const string IratElsoPeldanyTovabbitasAlatt = "Az irat els� p�ld�nya tov�bb�t�s alatt �ll.";
            public const string UgyiratAllapotNemMegfelelo = "Az �gyirat �llapota nem megfelel�.";
            public const string UgyiratAllapotNemUgyintezesAlatti = "Az �gyirat �llapota nem �gyint�z�s alatti.";
            public const string UgyiratnakNemOnAzOrzoje = "Az �gyiratnak nem �n az �rz�je.";
            public const string IratElintezettNyilvanitva = "Az irat m�r elint�zettnek van nyilv�n�tva";
            public const string IratNemKiadmanyozhato = "A kiadm�nyozand� irat egyik p�ld�nya �tad�s alatt van, ez�rt nem kiadm�nyozhat�. Vegye fel a kapcsolatot az irat �gyint�z�j�vel!";
            // CR3394
            public const string IratKiadmanyozando = "Az irat kiadm�nyozand�.";
            // BUG_4731
            public const string IratElintezettKiadmanyozhato = "Az irat csak abban az esetben kiadm�nyozhat�, ha a st�tusza Elint�zett!";
            // BUG_8003
            public const string IratCsakIntezkedesAlattiElintezheto = "Csak Int�zked�s alatt �llapot� irat nyilv�n�that� elint�zett�!";
        }

        // CR 3107 : Iratp�ld�ny l�trehoz�s post�z�skor a "postazas" funkci�k�dhoz k�tve
        private const string FunkcioKod_Postazas = "Postazas";

        public class FilterObject
        {
            // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
            public enum SpecialFilterType { Letrehozas, Kritikus, Lejart };
            private SpecialFilterType _SpecialFilter;
            public SpecialFilterType SpecialFilter
            {
                get { return _SpecialFilter; }
                set { _SpecialFilter = value; }
            }

            private List<string> _Allapot;
            public void AddAllapot(string Allapot)
            {
                if (_Allapot == null)
                {
                    _Allapot = new List<string>();
                }

                if (!_Allapot.Contains(Allapot))
                {
                    _Allapot.Add(Allapot);
                }
            }

            public void RemoveAllapot(string Allapot)
            {
                if (_Allapot != null)
                {
                    while (_Allapot.IndexOf(Allapot) >= 0)
                    {
                        _Allapot.RemoveAt(_Allapot.IndexOf(Allapot));
                    }
                }
            }

            public void ClearAllapot()
            {
                if (_Allapot != null)
                {
                    _Allapot.Clear();
                }
                else
                {
                    _Allapot = new List<string>();
                }
            }

            public string[] GetAllapotArray()
            {
                if (_Allapot != null)
                {
                    return _Allapot.ToArray();
                }
                else return null;
            }

            private List<string> _ExcludeAllapot;
            public void AddExcludeAllapot(string ExcludeAllapot)
            {
                if (_ExcludeAllapot == null)
                {
                    _ExcludeAllapot = new List<string>();
                }

                if (!_ExcludeAllapot.Contains(ExcludeAllapot))
                {
                    _ExcludeAllapot.Add(ExcludeAllapot);
                }
            }

            public void RemoveExcludeAllapot(string ExcludeAllapot)
            {
                if (_ExcludeAllapot != null)
                {
                    while (_ExcludeAllapot.IndexOf(ExcludeAllapot) >= 0)
                    {
                        _ExcludeAllapot.RemoveAt(_ExcludeAllapot.IndexOf(ExcludeAllapot));
                    }
                }
            }

            public void ClearExcludeAllapot()
            {
                if (_ExcludeAllapot != null)
                {
                    _ExcludeAllapot.Clear();
                }
                else
                {
                    _ExcludeAllapot = new List<string>();
                }
            }

            public string[] GetExcludeAllapotArray()
            {
                if (_ExcludeAllapot != null)
                {
                    return _ExcludeAllapot.ToArray();
                }
                else return null;
            }

            private string _PostazasIranya;
            public string PostazasIranya
            {
                get { return _PostazasIranya; }
                set { _PostazasIranya = value; }
            }

            private string _CsatolmanySzuro;
            public string CsatolmanySzures
            {
                get { return _CsatolmanySzuro; }
                set { _CsatolmanySzuro = value; }
            }

            private string _MinDate;
            public string MinDate
            {
                get { return _MinDate; }
                set { _MinDate = value; }
            }

            public void ClearMinDate()
            {
                _MinDate = null;
            }

            private string _MaxDate;
            public string MaxDate
            {
                get { return _MaxDate; }
                set { _MaxDate = value; }
            }

            public void ClearMaxDate()
            {
                _MaxDate = null;
            }

            private string _AlSzervezetId;

            public string AlSzervezetId
            {
                get { return _AlSzervezetId; }
                set { _AlSzervezetId = value; }
            }

            private string _ReadableWhere;
            public string ReadableWhere
            {
                get { return _ReadableWhere; }
                set { _ReadableWhere = (value ?? ""); }
            }

            public FilterObject()
            {
                this._PostazasIranya = null;
                this.CsatolmanySzures = null;
                this._MinDate = null;
                this._MaxDate = null;
                this._ReadableWhere = "";
                // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
                this.SpecialFilter = SpecialFilterType.Letrehozas;
            }

            public void Clear()
            {
                this._PostazasIranya = null;
                this.CsatolmanySzures = null;
                this._MinDate = null;
                this._MaxDate = null;
                this._ReadableWhere = "";
                this._AlSzervezetId = null;
                // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
                this._SpecialFilter = SpecialFilterType.Letrehozas;
            }

            private const string queryPrefix = "FilterBy";

            public string QsSerialize()
            {
                Logger.Debug("QueryString soros�t�s START");
                List<string> queryStringItems = new List<string>();
                string query = String.Empty;
                if (!String.IsNullOrEmpty(this._PostazasIranya))
                {
                    queryStringItems.Add(queryPrefix + "PostazasIranya=" + System.Web.HttpUtility.UrlEncode(this._PostazasIranya));
                }
                // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
                if (this._Allapot != null && this._Allapot.Count > 0)
                {
                    queryStringItems.Add(queryPrefix + "Allapot=" + System.Web.HttpUtility.UrlEncode(String.Join(";", this._Allapot.ToArray())));
                }
                if (this._ExcludeAllapot != null && this._ExcludeAllapot.Count > 0)
                {
                    queryStringItems.Add(queryPrefix + "ExcludeAllapot=" + System.Web.HttpUtility.UrlEncode(String.Join(";", this._ExcludeAllapot.ToArray())));
                }

                queryStringItems.Add(queryPrefix + "SpecialFilter=" + System.Web.HttpUtility.UrlEncode(this._SpecialFilter.ToString()));

                if (!String.IsNullOrEmpty(this._MinDate))
                {
                    queryStringItems.Add(queryPrefix+ "MinDate=" + System.Web.HttpUtility.UrlEncode(this._MinDate));
                }

                if (!String.IsNullOrEmpty(this._MaxDate))
                {
                    queryStringItems.Add(queryPrefix + "MaxDate=" + System.Web.HttpUtility.UrlEncode(this._MaxDate));
                }

                if (!String.IsNullOrEmpty(this._AlSzervezetId))
                {
                    queryStringItems.Add(String.Format("{0}AlSzervezetId={1}", queryPrefix, System.Web.HttpUtility.UrlEncode(this._AlSzervezetId)));
                }

                if (!String.IsNullOrEmpty(this._ReadableWhere))
                {
                    queryStringItems.Add(queryPrefix + "ReadableWhere=" + System.Web.HttpUtility.UrlEncode(this._ReadableWhere));
                }

                if (queryStringItems.Count > 0)
                {
                    query = String.Join("&", queryStringItems.ToArray());
                }

                Logger.Debug("QueryString: " + query);
                Logger.Debug("QueryString soros�t�s END");

                return query;
            }

            public void QsDeserialize(System.Collections.Specialized.NameValueCollection queryString)
            {
                Logger.Debug("QueryString desoros�t�s START");
                Logger.Debug("QueryString: " + queryString.ToString());

                string postazasIranya = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "PostazasIranya"));
                // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
                string allapotok = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "Allapot"));
                string excludeallapotok = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "ExcludeAllapot"));
                string specialFilter = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "SpecialFilter"));
                string minDate = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "MinDate"));
                string maxDate = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "MaxDate"));
                this._AlSzervezetId = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "AlSzervezetId"));
                string readableWhere = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "ReadableWhere"));

                this._PostazasIranya = postazasIranya;
                // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
                if (!String.IsNullOrEmpty(allapotok))
                {
                    char[] separator = { ';' };
                    string[] allapotokArray = allapotok.Split(separator);
                    if (this._Allapot == null)
                    {
                        this._Allapot = new List<string>();
                    }
                    else
                    {
                        this._Allapot.Clear();
                    }

                    foreach (string allapot in allapotokArray)
                    {
                        this._Allapot.Add(allapot);
                    }
                }
                if (!String.IsNullOrEmpty(excludeallapotok))
                {
                    char[] separator = { ';' };
                    string[] excludeallapotokArray = excludeallapotok.Split(separator);
                    if (this._ExcludeAllapot == null)
                    {
                        this._ExcludeAllapot = new List<string>();
                    }
                    else
                    {
                        this._ExcludeAllapot.Clear();
                    }

                    foreach (string excludeallapot in excludeallapotokArray)
                    {
                        this._ExcludeAllapot.Add(excludeallapot);
                    }
                }
                if (specialFilter == SpecialFilterType.Kritikus.ToString())
                {
                    this._SpecialFilter = SpecialFilterType.Kritikus;
                }
                else if (specialFilter == SpecialFilterType.Lejart.ToString())
                {
                    this._SpecialFilter = SpecialFilterType.Lejart;
                }
                else
                {
                    this._SpecialFilter = SpecialFilterType.Letrehozas;
                }
                this._MinDate = minDate;
                this._MaxDate = maxDate;
                this._ReadableWhere = (readableWhere ?? "");

                Logger.Debug("QueryString desoros�t�s END");
                //Logger.Debug(String.Format("Eredm�ny: PostazasIranya: {0}, Szervezet Id: {1}", (this._PostazasIranya == null ? "null" : this._PostazasIranya), (this._SzervezetId == null ? "null" : this._SzervezetId)));
                Logger.Debug(String.Format("Eredm�ny: PostazasIranya: {0}", (this._PostazasIranya == null ? "null" : this._PostazasIranya)));
                Logger.Debug(String.Format("MinDate: {0}, MaxDate: {1}, SpecialFilter: {2}", (this._MinDate == null ? "null" : this._MinDate), (this._MaxDate == null ? "null" : this._MaxDate), this._SpecialFilter.ToString()));
                Logger.Debug(String.Format("ReadableWhere: {0}", this._ReadableWhere));
            }

            public void SetSearchObject(ExecParam execParam, ref EREC_IraIratokSearch searchObject)
            {
                if (!String.IsNullOrEmpty(this._PostazasIranya))
                {
                    searchObject.PostazasIranya.Value = this._PostazasIranya;
                    searchObject.PostazasIranya.Operator = Query.Operators.equals;
                }

                if (!String.IsNullOrEmpty(this.CsatolmanySzures))
                {
                    searchObject.CsatolmanySzures.Value = this._CsatolmanySzuro;
                    searchObject.CsatolmanySzures.Operator = Query.Operators.equals;
                }

                if (!String.IsNullOrEmpty(this._ReadableWhere))
                {
                    searchObject.ReadableWhere = this._ReadableWhere;
                }

                // CR3135: Vezet�i panelen lej�rt �s kritikus hat�ridej� iratok megjelen�t�se
                //if (!String.IsNullOrEmpty(this._MinDate) && this._MinDate == this._MaxDate)
                //{
                //    searchObject.Manual_LetrehozasIdo.Value = this._MinDate;
                //    searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.equals;
                //}
                //else if (!String.IsNullOrEmpty(this._MinDate) && !String.IsNullOrEmpty(this._MaxDate))
                //{
                //    searchObject.Manual_LetrehozasIdo.Value = this._MinDate;
                //    searchObject.Manual_LetrehozasIdo.ValueTo = this._MaxDate;
                //    searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.between;
                //}
                //else if (!String.IsNullOrEmpty(this._MinDate))
                //{
                //    searchObject.Manual_LetrehozasIdo.Value = this._MinDate;
                //    searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.greaterorequal;
                //}
                //else if (!String.IsNullOrEmpty(this._MaxDate))
                //{
                //    searchObject.Manual_LetrehozasIdo.Value = this._MaxDate;
                //    searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.lessorequal;
                //}

                if (_Allapot != null && _Allapot.Count > 0)
                {
                    string[] allapotArray = GetAllapotArray();
                    string allapotok = "'" + String.Join("','", allapotArray) + "'";

                   
                        searchObject.Allapot.Value = allapotok;
                        searchObject.Allapot.Operator = Query.Operators.equals;
                        searchObject.Allapot.Group = "0";
                        searchObject.Allapot.GroupOperator = Query.Operators.and;

                        //searchObject.TovabbitasAlattAllapot.Value = "";
                        //searchObject.TovabbitasAlattAllapot.Operator = "";
                        //searchObject.TovabbitasAlattAllapot.Group = "0";
                        //searchObject.TovabbitasAlattAllapot.GroupOperator = Query.Operators.and;
                }
                else
                {
                    searchObject.Allapot.Value = "";
                    searchObject.Allapot.Operator = "";
                    searchObject.Allapot.Group = "0";
                    searchObject.Allapot.GroupOperator = Query.Operators.and;

                    //searchObject.TovabbitasAlattAllapot.Value = "";
                    //searchObject.TovabbitasAlattAllapot.Operator = "";
                    //searchObject.TovabbitasAlattAllapot.Group = "0";
                    //searchObject.TovabbitasAlattAllapot.GroupOperator = Query.Operators.and;
                }

                // a kiz�rand� �llapotokat a manu�lis filter mez�ben adjuk meg
                if (_ExcludeAllapot != null && _ExcludeAllapot.Count > 0)
                {
                    string[] excludeallapotArray = GetExcludeAllapotArray();
                    string excludeallapotok = "'" + String.Join("','", excludeallapotArray) + "'";

                    searchObject.Manual_Allapot_Filter.Value = excludeallapotok;
                    searchObject.Manual_Allapot_Filter.Operator = Query.Operators.notinner;
                }

                if (this._SpecialFilter == SpecialFilterType.Kritikus || this._SpecialFilter == SpecialFilterType.Lejart)
                {
                    if (!String.IsNullOrEmpty(this._MinDate) && this._MinDate == this._MaxDate)
                    {
                        searchObject.Hatarido.Value = this._MinDate;
                        searchObject.Hatarido.Operator = Query.Operators.equals;
                    }
                    else if (!String.IsNullOrEmpty(this._MinDate) && !String.IsNullOrEmpty(this._MaxDate))
                    {
                        searchObject.Hatarido.Value = this._MinDate;
                        searchObject.Hatarido.ValueTo = this._MaxDate;
                        searchObject.Hatarido.Operator = Query.Operators.between;
                    }
                    else if (!String.IsNullOrEmpty(this._MinDate))
                    {
                        searchObject.Hatarido.Value = this._MinDate;
                        searchObject.Hatarido.Operator = Query.Operators.greaterorequal;
                    }
                    else if (!String.IsNullOrEmpty(this._MaxDate))
                    {
                        searchObject.Hatarido.Value = this._MaxDate;
                        searchObject.Hatarido.Operator = Query.Operators.lessorequal;
                    }
                }
                else
                {

                    if (!String.IsNullOrEmpty(this._MinDate) && this._MinDate == this._MaxDate)
                    {
                        searchObject.Manual_LetrehozasIdo.Value = this._MinDate;
                        searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.equals;
                    }
                    else if (!String.IsNullOrEmpty(this._MinDate) && !String.IsNullOrEmpty(this._MaxDate))
                    {
                        searchObject.Manual_LetrehozasIdo.Value = this._MinDate;
                        searchObject.Manual_LetrehozasIdo.ValueTo = this._MaxDate;
                        searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.between;
                    }
                    else if (!String.IsNullOrEmpty(this._MinDate))
                    {
                        searchObject.Manual_LetrehozasIdo.Value = this._MinDate;
                        searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.greaterorequal;
                    }
                    else if (!String.IsNullOrEmpty(this._MaxDate))
                    {
                        searchObject.Manual_LetrehozasIdo.Value = this._MaxDate;
                        searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.lessorequal;
                    }
                }

                if (!String.IsNullOrEmpty(this._AlSzervezetId))
                {
                    // BUG_8847
                    // searchObject.AlSzervezetId = this._AlSzervezetId;
                    searchObject.Csoport_Id_Ugyfelelos.Value = this.AlSzervezetId;
                    searchObject.Csoport_Id_Ugyfelelos.Operator = Query.Operators.equals;
                }
            }

        }

        public static Statusz GetAllapotById(String Irat_Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            return GetAllapotById(Irat_Id, execParam, EErrorPanel, false);
        }

        public static Statusz GetAllapotById(String Irat_Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel, bool useGetAllWithExtension)
        {
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            if (useGetAllWithExtension)
            {

                EREC_IraIratokSearch search = new EREC_IraIratokSearch();
                search.Id.Value = Irat_Id;
                search.Id.Operator = Query.Operators.equals;

                Result result = service.GetAllWithExtension(execParam, search);
                ResultError.CreateNoRowResultError(result);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    return GetAllapotByDataSet(result.Ds)[Irat_Id] as Statusz;
                }
                else
                {
                    if (EErrorPanel != null)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                    }
                    return null;
                }
            }
            else
            {
                EREC_IraIratok erec_irairatok = null;
                execParam.Record_Id = Irat_Id;

                Result result = service.Get(execParam);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    erec_irairatok = (EREC_IraIratok)result.Record;

                    // TODO: els� iratp�ld�nyt lek�rni:

                    return GetAllapotByBusinessDocument(erec_irairatok, new EREC_PldIratPeldanyok());
                }
                else
                {
                    if (EErrorPanel != null)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                    }
                    return null;
                }
            }
        }

        /// <summary>
        /// Figyelem: els� iratp�ld�ny �rz�je NINCS be�ll�tva
        /// </summary>   
        ///
        public static Statusz GetAllapotByBusinessDocument(EREC_IraIratok erec_IraIratok)
        {
            return GetAllapotByBusinessDocument(erec_IraIratok, String.Empty, String.Empty);
        }

        public static Statusz GetAllapotByBusinessDocument(EREC_IraIratok erec_IraIratok, EREC_PldIratPeldanyok elsoIratPeldany)
        {
            string elsoIratPeldanyOrzo_Id = (elsoIratPeldany == null) ? String.Empty : elsoIratPeldany.FelhasznaloCsoport_Id_Orzo;
            string elsoIratPeldanyAllapot = (elsoIratPeldany == null) ? String.Empty : elsoIratPeldany.Allapot;


            return GetAllapotByBusinessDocument(erec_IraIratok, elsoIratPeldanyOrzo_Id, elsoIratPeldanyAllapot);
        }

        public static Statusz GetAllapotByBusinessDocument(EREC_IraIratok erec_IraIratok, string elsoIratPeldanyOrzo_Id)
        {
            //Statusz statusz = new Statusz(erec_IraIratok.Id, erec_IraIratok.Allapot, erec_IraIratok.UgyUgyIratDarab_Id, erec_IraIratok.Ugyirat_Id
            //    , erec_IraIratok.FelhasznaloCsoport_Id_Iktato, elsoIratPeldanyOrzo_Id, erec_IraIratok.Alszam);

            //return statusz;

            return GetAllapotByBusinessDocument(erec_IraIratok, elsoIratPeldanyOrzo_Id, String.Empty);
        }

        public static Statusz GetAllapotByBusinessDocument(EREC_IraIratok erec_IraIratok, string elsoIratPeldanyOrzo_Id, string elsoIratPeldanyAllapot)
        {
            Statusz statusz = new Statusz(erec_IraIratok.Id, erec_IraIratok.Allapot, erec_IraIratok.FelhasznaloCsoport_Id_Orzo, erec_IraIratok.UgyUgyIratDarab_Id, erec_IraIratok.Ugyirat_Id
                , erec_IraIratok.FelhasznaloCsoport_Id_Iktato, elsoIratPeldanyOrzo_Id, elsoIratPeldanyAllapot, erec_IraIratok.Alszam);

            return statusz;
        }

        /// <summary>
        /// Figyelem: els� iratp�ld�ny �rz�je NINCS be�ll�tva
        /// </summary>   
        ///
        public static Statusz GetAllapotByDataRow(DataRow row)
        {

            Statusz statusz = new Statusz(row["Id"].ToString(),
                        row["Allapot"].ToString(),
                        row["FelhasznaloCsoport_Id_Orzo"].ToString(),
                        row["UgyUgyIratDarab_Id"].ToString(),
                        row["Ugyirat_Id"].ToString(),
                        row["FelhasznaloCsoport_Id_Iktato"].ToString(),
                        String.Empty,             // ElsoIratPeldanyOrzo_Id,
                        String.Empty,             // ElsoIratPeldanyAllapot,
                        row["Alszam"].ToString()); 

            return statusz;
        }

        /// <summary>
        /// T�bb irat �llapot�t adja vissza egyszerre DataSet-b�l
        /// </summary>
        /// <param name="ep">execParam</param>
        /// <param name="ds">DataSet</param>
        /// <returns>HashTable Statusz elemekkel. Hivatkozni r�juk: statusz["Id"]-val lehet.</returns>
        public static Hashtable GetAllapotByDataSet(System.Data.DataSet ds)
        {
            if (ds.Tables.Count == 0) return new Hashtable(0);
            if (ds.Tables[0].Rows.Count == 0) return new Hashtable(0);
            Hashtable statusz = new Hashtable(ds.Tables[0].Rows.Count);

            try
            {                
                bool IsElsoIratPeldanyOrzo_Id = false;
                if (ds.Tables[0].Columns.Contains("ElsoIratPeldanyOrzo_Id"))
                {
                    IsElsoIratPeldanyOrzo_Id = true;
                }
                bool IsElsoIratPeldanyAllapot = ds.Tables[0].Columns.Contains("ElsoIratPeldanyAllapot");

                foreach (System.Data.DataRow r in ds.Tables[0].Rows)
                {
                    string row_ElsoIratPeldanyOrzo_Id = String.Empty;
                    if (IsElsoIratPeldanyOrzo_Id)
                    {
                        row_ElsoIratPeldanyOrzo_Id = r["ElsoIratPeldanyOrzo_Id"].ToString();
                    }

                    string row_ElsoIratPeldanyAllapot = String.Empty;
                    if (IsElsoIratPeldanyAllapot)
                    {
                        row_ElsoIratPeldanyAllapot = r["ElsoIratPeldanyAllapot"].ToString();
                    }
                    
                    statusz[r["Id"].ToString()] = new Statusz(r["Id"].ToString(),
                        r["Allapot"].ToString(),
                        r["FelhasznaloCsoport_Id_Orzo"].ToString(),
                        r["UgyUgyIratDarab_Id"].ToString(),
                        r["Ugyirat_Id"].ToString(),
                        r["FelhasznaloCsoport_Id_Iktato"].ToString(),
                        row_ElsoIratPeldanyOrzo_Id,
                        row_ElsoIratPeldanyAllapot,
                        r["Alszam"].ToString());
                }
            }
            catch
            {
                return new Hashtable(0);
            }

            return statusz;
        }
        #region CR3106 - CR3103
        private static string orgKod = "";
        #endregion CR3106 - CR3103
        /// <summary>
        /// A felhaszn�l� az �rz�, �s az �llapota m�dos�that�
        /// </summary>
        /// <param name="statusz"></param>
        /// <param name="execParam"></param>
        /// <returns></returns>
        public static bool Modosithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Gyors�tott visszat�r�s:
            #region CR3106  - CR3103
            String orgKod = GetOrgKod(execParam);
            if (orgKod == Constants.OrgKod.BOPMH)
            {
                switch (statusz.Allapot)
                {
                    case KodTarak.IRAT_ALLAPOT.Sztornozott:
                    case KodTarak.IRAT_ALLAPOT.Atiktatott:
                    case KodTarak.IRAT_ALLAPOT.Befagyasztott_Munkairat:
                        {
                            errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                            return false;
                        }
                }
            }
            else
            {
                switch (statusz.Allapot)
                {
                    case KodTarak.IRAT_ALLAPOT.Kiadmanyozott:
                    case KodTarak.IRAT_ALLAPOT.Sztornozott:
                    case KodTarak.IRAT_ALLAPOT.Atiktatott:
                    case KodTarak.IRAT_ALLAPOT.Befagyasztott_Munkairat:
                        {
                            errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                            return false;
                        }
                }
            }
            #endregion
            // �gyirat st�tusz lek�r�se:            
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(
                statusz.UgyiratId, execParam, null);

            return Modosithato(statusz, ugyiratStatusz, execParam, out errorDetail);
        }

        private static string GetOrgKod(ExecParam execParam)
        {
            String orgKod = String.Empty;
            Result org = new Result();
            using (KRT_OrgokService service = eAdminService.ServiceFactory.GetKRT_OrgokService())
            {
                ExecParam _exec = execParam.Clone();
                _exec.Record_Id = execParam.Org_Id;
                org = service.Get(_exec);
            }

            if (!org.IsError) orgKod = ((KRT_Orgok)org.Record).Kod;
            return orgKod;
        }

        // CR 3107 : Iratp�ld�ny l�trehoz�s post�z�skor a "postazas" funkci�k�dhoz k�tve
        // Azt hogy �j iratp�ld�ny l�trehoz�s�b�l hivt�k-e, azt egy plussz param�terrel d�ntj�k el (ld lejjebb)
        public static bool Modosithato(Statusz statusz, Ugyiratok.Statusz ugyiratokStatusz
            , ExecParam execParam, out ErrorDetails errorDetail)
        {
            return Modosithato(statusz, ugyiratokStatusz, execParam, out errorDetail, false);
        }

        // CR 3107 : Iratp�ld�ny l�trehoz�s post�z�skor a "postazas" funkci�k�dhoz k�tve
        // Azt hogy �j iratp�ld�ny l�trehoz�s�b�l hivt�k-e, azt egy plussz param�terrel d�ntj�k el 
        public static bool Modosithato(Statusz statusz, Ugyiratok.Statusz ugyiratokStatusz
      , ExecParam execParam, out ErrorDetails errorDetail, bool forAddNewIratpld)
        {
            errorDetail = null;
            #region CR3106 - CR3103
            if (String.IsNullOrEmpty(orgKod))
            {

                orgKod = GetOrgKod(execParam);
                
            }
            #endregion
            // CR 3107 : Iratp�ld�ny l�trehoz�s post�z�skor a "postazas" funkci�k�dhoz k�tve
            // Megford�tottam a felt�teleket (megfelel�re k�rdezek �s nem a hib�sra)
            /// Akkor m�dos�that�, ha M�dos�that� �llapot�, 
            /// �S a felhaszn�l� az �GYIRAT �rz�je (NEM az Irat �rz�j�t kell n�zni) (Vagy k�zponti iktat�s)            
            /// VAGY a felhaszn�l� az els� iratp�ld�ny �rz�je
            /// 
            //if (ugyiratokStatusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam)
            //    && execParam.FelhasznaloSzervezet_Id != KodTarak.SPEC_SZERVEK.GetKozpontiIktato(execParam).Obj_Id
            //    //Kiv�ve, el�g az els� iratp�ld�ny figyel�se: (2008.11.06)
            //    //&& statusz.FelhCsopId_Iktato != Csoportok.GetFelhasznaloSajatCsoportId(execParam)
            //    && statusz.ElsoIratPeldanyOrzo_Id != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            //{
            //    errorDetail = ugyiratokStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Ugyiratok.Statusz.ColumnTypes.FelhCsopId_Orzo);
            //    return false;
            //}
            //else
            //{
            //    return ModosithatoAllapotu(statusz, ugyiratokStatusz, out errorDetail);
            //}
            if ((ugyiratokStatusz.FelhCsopId_Orzo == Csoportok.GetFelhasznaloSajatCsoportId(execParam))
                || (execParam.FelhasznaloSzervezet_Id == KodTarak.SPEC_SZERVEK.GetKozpontiIktato(execParam).Obj_Id)
                //Kiv�ve, el�g az els� iratp�ld�ny figyel�se: (2008.11.06)
                //&& statusz.FelhCsopId_Iktato != Csoportok.GetFelhasznaloSajatCsoportId(execParam)
                || (statusz.ElsoIratPeldanyOrzo_Id == Csoportok.GetFelhasznaloSajatCsoportId(execParam))
                || (forAddNewIratpld && FunctionRights.HasFunctionRight(execParam, FunkcioKod_Postazas)))
            {
                return ModosithatoAllapotu(statusz, ugyiratokStatusz, execParam, out errorDetail);

            }
            else
            {
                errorDetail = ugyiratokStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Ugyiratok.Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }
        }

        public static bool ModosithatoAllapotu(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            #region CR3106 - CR3103
            orgKod = GetOrgKod(execParam);
            #endregion
            #region CR3106 - CR3103
            if (orgKod == Constants.OrgKod.BOPMH)
            {
                // gyors�tott kil�p�s:
                if (statusz.Allapot == KodTarak.IRAT_ALLAPOT.Sztornozott
                    || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Atiktatott
                    || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Befagyasztott_Munkairat)
                {
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
                }

                switch (statusz.Allapot)
                {
                    case KodTarak.IRAT_ALLAPOT.Kiadmanyozott:
                    case KodTarak.IRAT_ALLAPOT.Iktatott:
                    case KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat:
                    case KodTarak.IRAT_ALLAPOT.Iktatando_Munkairat:
                    case KodTarak.IRAT_ALLAPOT.Szignalt:
                    case KodTarak.IRAT_ALLAPOT.IntezkedesAlatt:
                        {
                            //Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(statusz.UgyiratId, execParam, null);
                            return ModosithatoAllapotu(statusz, null /*ugyiratStatusz*/, execParam, out errorDetail);
                        }
                    default:
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                        return false;
                }
            }
            else
            {
                // gyors�tott kil�p�s:
                if (statusz.Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott
                    || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Sztornozott
                    || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Atiktatott
                    || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Befagyasztott_Munkairat)
                {
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
                }

                switch (statusz.Allapot)
                {
                    case KodTarak.IRAT_ALLAPOT.Iktatott:
                    case KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat:
                    case KodTarak.IRAT_ALLAPOT.Iktatando_Munkairat:
                    case KodTarak.IRAT_ALLAPOT.Szignalt:
                    case KodTarak.IRAT_ALLAPOT.IntezkedesAlatt:
                        {
                            //Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(statusz.UgyiratId, execParam, null);
                            return ModosithatoAllapotu(statusz, null /*ugyiratStatusz*/, execParam, out errorDetail);
                        }
                    default:
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                        return false;
                }
            }
            #endregion
        }

        public static bool ModosithatoAllapotu(Statusz statusz, Ugyiratok.Statusz ugyiratokStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // kell az irat
            EREC_IraIratok irat = null;
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam ep = new ExecParam();
            ep.Record_Id = statusz.Id;
            Result result = service.Get(ep);
            if (result.IsError)
            {
                errorDetail = result.ErrorDetail;
                return false;
            }
            else
            {
                irat = result.Record as EREC_IraIratok;
            }

            #region CR3106 - CR3103
            // CR3229: Az EXPEDIALAS_UJ_IRATPELDANNYAL param�ter �rt�k�t�l f�gg�en �j iratp�ld�ny j�n l�tre
            // FPH-ban is post�zhat� a kiadm�nyozott irat
            //if (orgKod == Constants.OrgKod.BOPMH)
            //{
            if (statusz.Allapot == KodTarak.IRAT_ALLAPOT.Sztornozott
                || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Atiktatott
                || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Befagyasztott_Munkairat
                || (IsIratIntezkedesreAtvetelElerheto(execParam) && statusz.Allapot == KodTarak.IRAT_ALLAPOT.Szignalt // <- BUG_8003
                    && (irat == null || (irat.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Belso && irat.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Kimeno))) // <- BUG_8003 (belso keletkezesu, kimeno iratot szign�lt �llapotban is meg lehet nyitni)
                )
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.IRAT_ALLAPOT.Kiadmanyozott:
                case KodTarak.IRAT_ALLAPOT.Iktatott:
                case KodTarak.IRAT_ALLAPOT.IntezkedesAlatt:
                case KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat:
                case KodTarak.IRAT_ALLAPOT.Iktatando_Munkairat:
                case KodTarak.IRAT_ALLAPOT.Szignalt:
                    {
                        //CR 2081 fix: Laura: szedj�k ki az �gyirat �llapot�nak vizsg�lat�t
                        //Bele iktatott iratot nem tudnak m�dos�tani, ha az �gyirat skontr�ban vagy iratt�rban van
                        //return Ugyiratok.ModosithatoAllapotu(ugyiratokStatusz, out errorDetail);
                        return true;
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
            //}
            //else
            //{
            //    // gyors�tott kil�p�s:
            //    if (statusz.Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott
            //        || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Sztornozott
            //        || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Atiktatott
            //        || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Befagyasztott_Munkairat)
            //    {
            //        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
            //        return false;
            //    }
            //    switch (statusz.Allapot)
            //    {
            //        case KodTarak.IRAT_ALLAPOT.Iktatott:
            //        case KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat:
            //        case KodTarak.IRAT_ALLAPOT.Iktatando_Munkairat:
            //            {
            //                //CR 2081 fix: Laura: szedj�k ki az �gyirat �llapot�nak vizsg�lat�t
            //                //Bele iktatott iratot nem tudnak m�dos�tani, ha az �gyirat skontr�ban vagy iratt�rban van
            //                //return Ugyiratok.ModosithatoAllapotu(ugyiratokStatusz, out errorDetail);
            //                return true;
            //            }
            //        default:
            //            errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
            //            return false;
            //    }
            //}
            #endregion CR3106 - CR3103
        }

        public static bool Iktatott(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.IRAT_ALLAPOT.Iktatott)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Kiadmanyozott(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Sztornozott(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.IRAT_ALLAPOT.Sztornozott)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Atiktatott(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.IRAT_ALLAPOT.Atiktatott)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Kiadmanyozhato(Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // BUG_8003
            if (IsIratIntezkedesreAtvetelElerheto(execParam) && iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Szignalt)
            {
                errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }

            // Csak ha Irat �llapot iktatott, �gyirat �llapot �gyint�z�s alatti
            if (iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Iktatott || iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Szignalt || iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Elintezett)
            {
                return true;
            }
            else
            {
                errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
        }

        public static bool isElintezett(Statusz iratStatusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            bool elintezheto = ElintzhetoAllapotu(iratStatusz, out errorDetail);
            //Kell a rekord :(            
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam ep = new ExecParam();
            ep.Record_Id = iratStatusz.Id;
            Result result = service.Get(ep);
            if (result.IsError)
            {
                errorDetail = iratStatusz.CreateErrorDetail(result.ErrorMessage);
                return false;
            }
            EREC_IraIratok rec = result.Record as EREC_IraIratok;
            return rec.Elintezett == "1";
        }

        public static bool Elintezheto(Statusz iratStatusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;                        
            bool elintezheto = ElintzhetoAllapotu(iratStatusz,out errorDetail);            
          
            if (isElintezett(iratStatusz,out errorDetail))
            {
                elintezheto = false;
                errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratElintezettNyilvanitva);                
            }
            else
            {
                elintezheto &= true;
            }
            return elintezheto;
         }
        public static bool ElintezesVisszavonhato(Statusz iratStatusz, out ErrorDetails errorDetail)
        {
            bool visszavonhato = false;
            errorDetail = null;
            if (isElintezett(iratStatusz, out errorDetail))
            {
                visszavonhato = true;
            }
            else
            {
                visszavonhato = false;

            }
            return visszavonhato;
        }

        public static bool ElintzhetoAllapotu(Statusz iratStatusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            
            bool elintezhetoallapotu = true;
            //    errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);           
            return elintezhetoallapotu;
        }

        public static bool AtIktathato(Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            /// K�zponti iktat�n�l nem kell az �rz�t figyelni,
            /// egy�bk�nt vagy az �gyirat �rz�j�nek, vagy az els�dleges iratp�ld�ny �rz�j�nek kell lennie a felhaszn�l�nak
            if (execParam.FelhasznaloSzervezet_Id != KodTarak.SPEC_SZERVEK.GetKozpontiIktato(execParam).Obj_Id
                && ugyiratStatusz.FelhCsopId_Orzo != FelhasznaloProfil.GetFelhasznaloCsoport(execParam)
                && iratStatusz.ElsoIratPeldanyOrzo_Id != FelhasznaloProfil.GetFelhasznaloCsoport(execParam))
            {
                // ha nem a felhaszn�l� az �rz�:
                errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Ugyiratok.Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            /// Olyan irat, melynek els� p�ld�nya tov�bb�t�s alatt van, nem iktathat� �t
            if (iratStatusz.ElsoIratPeldanyAllapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt)
            {
                // az els� iratp�ld�ny tov�bb�t�s alatt van:
                errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratElsoPeldanyTovabbitasAlatt);
                return false;
            }

            // Munkairat nem iktathat� �t:
            if (iratStatusz.Munkairat)
            {
                errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratMunkaanyag, Statusz.ColumnTypes.Munkaanyag);
                return false;
            }

            if (iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Iktatott || iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Szignalt 
                || iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Elintezett || iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.IntezkedesAlatt // BUG_11120
                )
            {
                if (Ugyiratok.Nyitott(ugyiratStatusz))
                {
                    return true;
                }
                else
                {
                    switch (ugyiratStatusz.Allapot)
                    {
                        case KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott:
                        case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                            return true;
                    }

                    errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                    return false;
                }
            }
            else
            {
                errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
        }



        public static bool Felszabadithato(Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Az iratnak bej�v� iratnak kell lennie (ezt a ws-en tudjuk ellen�rizni)

            // �rz� figyel�se:
            if (ugyiratStatusz.FelhCsopId_Orzo != FelhasznaloProfil.GetFelhasznaloCsoport(execParam))
            {
                // ha nem a felhaszn�l� az �rz�:
                errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Ugyiratok.Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (iratStatusz.Allapot)
            {
                case KodTarak.IRAT_ALLAPOT.Iktatott:
                case KodTarak.IRAT_ALLAPOT.IntezkedesAlatt:
                case KodTarak.IRAT_ALLAPOT.Elintezett:
                case KodTarak.IRAT_ALLAPOT.Iktatando_Munkairat:
                case KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat:
                case KodTarak.IRAT_ALLAPOT.Sztornozott:
                case KodTarak.IRAT_ALLAPOT.Szignalt:
                case KodTarak.IRAT_ALLAPOT.Foglalt:
                    {
                        if (Ugyiratok.Nyitott(ugyiratStatusz))
                        {
                            return true;
                        }
                        else
                        {
                            switch (ugyiratStatusz.Allapot)
                            {
                                case KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott:
                                case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                                    return true;
                            }

                            errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                            return false;
                        }
                    }
                default:
                    errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool KiadmonyozoAlairhatja(Iratok.Statusz statusz, ExecParam execParam, out ErrorDetails errorDetails)
        {
            errorDetails = null;

            // BUG_6209
            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.ELINTEZETTE_NYILV_KIADM_FELTETELE, false))
            {
                if (!Iratok.isElintezett(statusz, out errorDetails))
                {
                    errorDetails = statusz.CreateErrorDetail(ErrorDetailMessages.IratElintezettKiadmanyozhato);
                    return false;
                }
            }
            #region Iratp�ld�nyok GetAll + Statusz
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
            search.IraIrat_Id.Value = statusz.Id;
            search.IraIrat_Id.Operator = Query.Operators.equals;
            Result result = service.GetAll(execParam.Clone(), search);

            if (result.IsError)
                errorDetails = statusz.CreateErrorDetail(ResultError.GetErrorMessageFromResultObject(result));
            if (result.Ds.Tables[0].Rows.Count == 0)
                errorDetails = statusz.CreateErrorDetail(ResultError.GetErrorMessageByErrorCode(51104));
            
            // St�tusz lek�r�s
            Hashtable peldany_statuszok = IratPeldanyok.GetAllapotByDataSet(execParam.Clone(), result.Ds);
            foreach (DataRow stat in result.Ds.Tables[0].Rows)
            {
                if (!IratPeldanyok.KiadmoanyozoAlairhatja(peldany_statuszok[stat["Id"].ToString()] as IratPeldanyok.Statusz))
                {
                    errorDetails = statusz.CreateErrorDetail(ErrorDetailMessages.IratNemKiadmanyozhato);
                    return false;
                }
            }
            #endregion

            return true;
        }

        public static bool ExpedialhatoAllapotu(Statusz iratStatusz, bool kiadmanyozando
            , Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // BUG_8003
            if (IsIratIntezkedesreAtvetelElerheto(execParam) && iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Szignalt)
            {
                errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }

            // Munkairat nem expedi�lhat�:
            if (iratStatusz.Munkairat)
            {
                errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratMunkaanyag, Statusz.ColumnTypes.Munkaanyag);
                return false;
            }

            if (iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott
                || ((iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Iktatott || iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Szignalt 
                || iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Elintezett // BUG_11120
                ) && !kiadmanyozando)
                )
            {
                switch (ugyiratStatusz.Allapot)
                {
                    case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                    case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                    case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
                    case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                    case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                    // BLG_293
                    case KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo:
                        return true;
                    case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                        switch (ugyiratStatusz.TovabbitasAlattiAllapot)
                        {
                            case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                            case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                            case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
                            case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                            // BLG_293
                            case KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo:
                                return true;
                            default:
                                errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                                return false;
                        }
                    default:
                        errorDetail = ugyiratStatusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Ugyiratok.Statusz.ColumnTypes.Allapot);
                        return false;
                }
            }
            else
            {
                // CR3394
                if ((iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Iktatott || iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Szignalt) && kiadmanyozando)
                {
                    errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratKiadmanyozando, Statusz.ColumnTypes.Allapot);
                }
                else
                    errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
        }

        public static bool Expedialhato(Statusz iratStatusz, bool kiadmanyozando
            , Ugyiratok.Statusz ugyiratStatusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            return ExpedialhatoAllapotu(iratStatusz, kiadmanyozando, ugyiratStatusz, execParam, out errorDetail);
        }



        public static bool BeiktathatoAMunkairat(Statusz iratStatusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Az iratnak munkaanyagnak kell lennie:
            if (!iratStatusz.Munkairat)
            {
                errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratNEMmunkaanyag, Statusz.ColumnTypes.Munkaanyag);
                return false;
            }

            switch (iratStatusz.Allapot)
            {
                case KodTarak.IRAT_ALLAPOT.Iktatando_Munkairat:
                case KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat:
                    return true;
                default:
                    errorDetail = iratStatusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

      

        public static bool Munkaanyag(EREC_IraIratok erec_IraIratok)
        {
            // ha az alsz�m null, akkor munkaanyag az irat
            if (erec_IraIratok.Typed.Alszam.IsNull
                || erec_IraIratok.Alszam == "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetAlszamString(EREC_IraIratok erec_IraIratok)
        {
            if(Munkaanyag(erec_IraIratok))
            {
                return "MI" + erec_IraIratok.Sorszam;
            }

            return erec_IraIratok.Alszam;
        }


        // BLG_292
        // egy�gk�nt kezelend� azonos�t� r�sz + egyedi azonos�t�s, ha van
        //private static string GetIktatoSzam_Azonosito(string foszamStr, string alszamStr, string evStr, string azonositoStr)
        //{
        //    return foszamStr + " - " + alszamStr + " /" + evStr + (!String.IsNullOrEmpty(azonositoStr) ? " /" + azonositoStr : ""); ;
        //}


        //public static string GetFullIktatoszam(EREC_IraIktatoKonyvek erec_IraIktatoKonyvek, EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIratok erec_IraIratok)
        //{
        //    if (erec_IraIktatoKonyvek != null && erec_UgyUgyiratok != null && erec_IraIratok != null)
        //    {
        //        string alszamStr = GetAlszamString(erec_IraIratok);

        //        string foszamStr = Ugyiratok.GetFoszamString(erec_UgyUgyiratok);

        //        if (erec_IraIktatoKonyvek.KozpontiIktatasJelzo == "1")
        //        {
        //            // K�zponti iktat�s jelz�:
        //            //return foszamStr + " - " + alszamStr + " /" + erec_IraIktatoKonyvek.Ev + (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.MegkulJelzes) ? " /" + erec_IraIktatoKonyvek.MegkulJelzes : "");
        //            return GetIktatoSzam_Azonosito(foszamStr, alszamStr, erec_IraIktatoKonyvek.Ev, erec_IraIktatoKonyvek.MegkulJelzes);
        //        }
        //        else
        //        {
        //            //return erec_IraIktatoKonyvek.Iktatohely + " /" + foszamStr + " - " + alszamStr + " /" + erec_IraIktatoKonyvek.Ev + (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.MegkulJelzes) ? " /" + erec_IraIktatoKonyvek.MegkulJelzes : "");
        //            return erec_IraIktatoKonyvek.Iktatohely + " /" + GetIktatoSzam_Azonosito(foszamStr, alszamStr, erec_IraIktatoKonyvek.Ev, erec_IraIktatoKonyvek.MegkulJelzes);
        //        }
        //    }
        //    else
        //    {
        //        return String.Empty;
        //    }
        //}

        public static string GetFullIktatoszam(ExecParam execParam, EREC_IraIktatoKonyvek erec_IraIktatoKonyvek, EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIratok erec_IraIratok)
        {

            return Iktatoszam.GetFullAzonosito(execParam, erec_UgyUgyiratok, erec_IraIktatoKonyvek, erec_IraIratok);

        }

        /// <summary>
        /// Irat kapcsolhat� m�sik objektumhoz
        /// Allapot: nem Szt�rn�zott, nem Felszabad�tott
        /// </summary>
        /// <param name="statusz"></param>
        /// <returns>bool</returns>  
        public static bool Csatolhato(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.IRAT_ALLAPOT.Sztornozott:
                case KodTarak.IRAT_ALLAPOT.Atiktatott:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
                default:
                    return true;
            }
        }

        public static void SetCsatolhatoSearchObject(EREC_IraIratokSearch search)
        {
            search.Manual_Csatolhato.Value = "'"
                + KodTarak.IRAT_ALLAPOT.Sztornozott + "','"
                + KodTarak.IRAT_ALLAPOT.Atiktatott + "'";

            search.Manual_Csatolhato.Operator = Query.Operators.notinner;
        }

        /// <summary>
        /// Megvizsg�lja, hogy az iratok azonos �gyirathoz tartoznak-e
        /// </summary>
        /// <param name="HashTableStatusz">Irat statuszokat tartalmaz� hasht�bla</param>
        /// <returns></returns>
        public static bool IsIartokInSameUgyirat(Hashtable HashTableStatusz)
        {
            if (HashTableStatusz == null || HashTableStatusz.Count == 0 || HashTableStatusz.Count == 1)
                return true;

            string UgyiratId = null;

            foreach (object statusz in HashTableStatusz)
            {
                Statusz st = statusz as Statusz;
                if (st != null)
                {
                    if (st.UgyiratId != null)
                    {
                        if (UgyiratId == null)
                            UgyiratId = st.UgyiratId;

                        if (st.UgyiratId != UgyiratId)
                            return false;
                    }
                }
            }

            return true;
        }

        public static bool Szignalhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            //ha nem a felhaszn�l� az �rz�:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.IRAT_ALLAPOT.Iktatott:
                case KodTarak.IRAT_ALLAPOT.Elintezett:
                case KodTarak.IRAT_ALLAPOT.IntezkedesAlatt:
                // BUG#7575: Szign�lt �llapot�t is lehessen szign�lni
                case KodTarak.IRAT_ALLAPOT.Szignalt:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.IratAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        #region K�ldem�ny el�k�sz�t�se iktat�shoz (szign�l�s t�pusa: "Szervezetre, �gyint�z�re, Iktat�s" v. "Szervezetre, Iktat�s, �gyint�z�re")

        private static string rootBusinessObjectsXML = "BusinessObjects";

        #region Szign�l�s ellen�rz�s el�k�sz�tett k�ldem�nyre seg�delj�r�sok
        /// <summary>
        /// Az el�k�sz�tett irat Note-ba mentett XML form�tumban ellen�rzi, hogy volt-e megadva EREC_UgyUgyiartok.FelhasznaloCsoport_Id_Ugyintez
        /// </summary>
        /// <param name="_EREC_IraIratok"></param>
        /// <returns></returns>
        public static bool IsElokeszitettIratSzignalt(EREC_IraIratok _EREC_IraIratok)
        {
            return !String.IsNullOrEmpty(GetBusinessObjectFieldFromXML(_EREC_IraIratok.Base.Note, Constants.TableNames.EREC_UgyUgyiratok, "FelhasznaloCsoport_Id_Ugyintez"));
        }

        #endregion Szign�l�s ellen�rz�s el�k�sz�tett k�ldem�nyre seg�delj�r�sok

        #region Get...XMLForKuldemenyElokeszites seg�delj�r�sok
        /// <summary>
        /// Seg�delj�r�s
        /// Az el�k�sz�tett iratba ment�shez XML form�tumba alak�tja az EREC_UgyUgyiratok �s EREC_PldIratPeldanyok objektum relev�ns mez�it
        /// </summary>
        /// <param name="_EREC_UgyUgyiratok"></param>
        /// <param name="_EREC_PldIratPeldanyok"></param>
        /// <returns></returns>
        public static string GetBusinessObjectsXMLForKuldemenyElokeszites(string XML, EREC_UgyUgyiratok _EREC_UgyUgyiratok, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok)
        {
            if (_EREC_UgyUgyiratok == null || _EREC_PldIratPeldanyok == null)
            {
                return "";
            }

            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

            if (!String.IsNullOrEmpty(XML))
            {
                try
                {
                    xmlDoc.LoadXml(XML);
                }
                catch (System.Xml.XmlException)
                {
                    // nem megfelel� az XML form�tuma...
                    throw new ResultException("Hib�s XML form�tum: az �gyiratok �s iratp�ld�nyok adatai nem r�gz�thet�k!");
                }
            }

            GetUgyiratXMLForKuldemenyElokeszites(xmlDoc, _EREC_UgyUgyiratok);
            GetPldXMLForKuldemenyElokeszites(xmlDoc, _EREC_PldIratPeldanyok);

            return xmlDoc.InnerXml;

        }

        /// <summary>
        /// Seg�delj�r�s
        /// Az el�k�sz�tett iratba ment�shez XML form�tumba alak�tja az EREC_UgyUgyiratok objektum relev�ns mez�it
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="_EREC_UgyUgyiratok"></param>
        private static void GetUgyiratXMLForKuldemenyElokeszites(System.Xml.XmlDocument xmlDoc, EREC_UgyUgyiratok _EREC_UgyUgyiratok)
        {
            if (xmlDoc == null)
            {
                throw new ResultException("A XML dokumentum �rt�ke null!");
            }

            if (_EREC_UgyUgyiratok != null)
            {
                if (!String.IsNullOrEmpty(_EREC_UgyUgyiratok.Targy))
                {
                    SetBusinessObjectFieldXMLForKuldemenyElokeszites(xmlDoc, Constants.TableNames.EREC_UgyUgyiratok, "Targy", _EREC_UgyUgyiratok.Targy);
                }

                if (!String.IsNullOrEmpty(_EREC_UgyUgyiratok.IraIktatokonyv_Id))
                {
                    SetBusinessObjectFieldXMLForKuldemenyElokeszites(xmlDoc, Constants.TableNames.EREC_UgyUgyiratok, "IraIktatokonyv_Id", _EREC_UgyUgyiratok.IraIktatokonyv_Id);
                }

                if (!String.IsNullOrEmpty(_EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez))
                {
                    SetBusinessObjectFieldXMLForKuldemenyElokeszites(xmlDoc, Constants.TableNames.EREC_UgyUgyiratok, "FelhasznaloCsoport_Id_Ugyintez", _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez);
                }
            }
        }

        /// <summary>
        /// Seg�delj�r�s
        /// Az el�k�sz�tett iratba ment�shez XML form�tumba alak�tja az EREC_PldIratPeldanyok objektum relev�ns mez�it
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="_EREC_PldIratPeldanyok"></param>
        private static void GetPldXMLForKuldemenyElokeszites(System.Xml.XmlDocument xmlDoc, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok)
        {
            if (xmlDoc == null)
            {
                throw new ResultException("A XML dokumentum �rt�ke null!");
            }

            if (_EREC_PldIratPeldanyok != null)
            {
                if (!String.IsNullOrEmpty(_EREC_PldIratPeldanyok.BarCode))
                {
                    SetBusinessObjectFieldXMLForKuldemenyElokeszites(xmlDoc, Constants.TableNames.EREC_PldIratPeldanyok, "BarCode", _EREC_PldIratPeldanyok.BarCode);
                }
            }
        }

        #endregion Get...XMLForKuldemenyElokeszites seg�delj�r�sok

        #region Set...XMLForKuldemenyElokeszites seg�delj�r�sok

        /// <summary>
        /// Seg�delj�r�s
        /// Az el�k�sz�tett irat Note-ba mentett XML form�tumba beilleszti az Eloszt��v azonos�t�j�t
        /// </summary>
        /// <param name="Elosztoiv_Id"></param>
        /// <returns></returns>
        public static string SetElosztoivIdForBusinessObjectsXML(string XML, string Elosztoiv_Id)
        {
            if (!String.IsNullOrEmpty(Elosztoiv_Id))
            {
                return SetBusinessObjectFieldXMLForKuldemenyElokeszites(XML, "EREC_IraElosztoivek", "Id", Elosztoiv_Id);
            }
            return "";
        }


        /// <summary>
        /// Az el�k�sz�tett irat Note-ba mentett XML form�tumban be�ll�tja az adott BusinessObject
        /// megfelel� mez�j�nek a kapott �rt�ket. Ha az XML m�g nem tartalmazza a node-okat, l�trehozza.
        /// Hib�s form�tum� XML eset�n hib�t (ResultException) dob.
        /// </summary>
        /// <param name="XML"></param>
        /// <param name="BusinessObjectName"></param>
        /// <param name="FieldName"></param>
        /// <param name="FieldText"></param>
        /// <returns></returns>
        public static string SetBusinessObjectFieldXMLForKuldemenyElokeszites(string XML, string BusinessObjectName, string FieldName, string FieldText)
        {
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            try
            {
                if (!String.IsNullOrEmpty(XML))
                {
                    xmlDoc.LoadXml(XML);
                }

                SetBusinessObjectFieldXMLForKuldemenyElokeszites(xmlDoc, BusinessObjectName, FieldName, FieldText);
            }
            catch (System.Xml.XmlException)
            {
                // nem megfelel� az XML form�tuma...
                throw new ResultException(String.Format("Hib�s XML form�tum: a {0}.{1} mez� �rt�ke nem r�gz�thet�!", BusinessObjectName, FieldName));
            }

            return xmlDoc.InnerXml;
        }

        /// <summary>
        /// Az el�k�sz�tett irat Note-ba mentett XML form�tumban be�ll�tja az adott BusinessObject
        /// megfelel� mez�j�nek a kapott �rt�ket. Ha az XML m�g nem tartalmazza a node-okat, l�trehozza.
        /// Hib�s form�tum� XML eset�n hib�t (ResultException) dob.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="BusinessObjectName"></param>
        /// <param name="FieldName"></param>
        /// <param name="FieldText"></param>
        public static void SetBusinessObjectFieldXMLForKuldemenyElokeszites(System.Xml.XmlDocument xmlDoc, string BusinessObjectName, string FieldName, string FieldText)
        {
            if (xmlDoc == null)
            {
                throw new ResultException("A XML dokumentum �rt�ke null!");
            }

            System.Xml.XmlElement root = xmlDoc.DocumentElement;

            if (root == null)
            {
                System.Xml.XmlElement documentElement = xmlDoc.CreateElement(rootBusinessObjectsXML);
                xmlDoc.AppendChild(documentElement);

                root = xmlDoc.DocumentElement;
            }

            if (root != null && root.Name == rootBusinessObjectsXML)
            {
                System.Xml.XmlNode nodeBusinessObject = root.SelectSingleNode(BusinessObjectName);
                System.Xml.XmlNode nodeField = null;
                if (nodeBusinessObject != null)
                {
                    nodeField = nodeBusinessObject.SelectSingleNode(FieldName);
                }
                else
                {
                    nodeBusinessObject = xmlDoc.CreateElement(BusinessObjectName);
                    root.AppendChild(nodeBusinessObject);
                }


                if (nodeField != null)
                {
                    nodeField.InnerText = FieldText;
                }
                else
                {
                    nodeField = xmlDoc.CreateElement(FieldName);
                    nodeField.InnerText = FieldText;
                    nodeBusinessObject.AppendChild(nodeField);
                }
            }
            else
            {
                // nem megfelel� az XML form�tuma...
                throw new ResultException(String.Format("Hib�s XML form�tum, a gy�k�relem megnevez�se ({0}) nem megfelel�!", root.Name));
            }
        }

        #endregion Set...XMLForElokeszitettKuldemeny seg�delj�r�sok

        #region Get...FromXML seg�delj�r�sok
        /// <summary>
        /// Seg�delj�r�s
        /// Az el�k�sz�tett irat Note-ba mentett XML form�tumb�l
        /// visszaadja az Eloszt��v azonos�t�j�t
        /// </summary>
        /// <param name="XML"></param>
        /// <returns></returns>
        public static string GetElosztoivIdFromBusinessObjectsXML(string XML)
        {
            return GetBusinessObjectFieldFromXML(XML, "EREC_IraElosztoivek", "Id");
        }

        /// <summary>
        /// Seg�delj�r�s
        /// Az el�k�sz�tett irat Note-ba mentett XML form�tumb�l
        /// visszaadja az adott BusinessObject megfelel� mez�j�nek �rt�k�t - ha nem tal�lhat�, �res stringet ad vissza
        /// </summary>
        /// <param name="XML"></param>
        /// <param name="param name="BusinessObjectName"></param>
        /// <param name="FieldName"></param>
        /// <returns></returns>
        public static string GetBusinessObjectFieldFromXML(string XML, string BusinessObjectName, string FieldName)
        {
            if (String.IsNullOrEmpty(XML) || String.IsNullOrEmpty(BusinessObjectName) || String.IsNullOrEmpty(FieldName))
            {
                return "";
            }
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            try
            {
                xmlDoc.LoadXml(XML);

                if (xmlDoc != null)
                {
                    System.Xml.XmlNodeList nodeList = xmlDoc.SelectNodes(String.Format("{0}/{1}/{2}", rootBusinessObjectsXML, BusinessObjectName, FieldName));

                    if (nodeList.Count > 0)
                    {
                        return nodeList[0].InnerText;
                    }
                }
            }
            catch (System.Xml.XmlException)
            {
                // nem megfelel� az XML form�tuma...
            }
            return "";

        }
        #endregion Get...FromXML seg�delj�r�sok
        #endregion K�ldem�ny el�k�sz�t�se iktat�shoz (szign�l�s t�pusa: "Szervezetre, �gyint�z�re, Iktat�s" v. "Szervezetre, Iktat�s, �gyint�z�re")

        // BLG_354
        // BLG kapcs�n a hat�s�gi �gy ellen�rz�s kiemelve ide (eddig OnkormanyzatTab-on volt)
        // BLG_2020 (execParam)
        // BUG_6284
        public static bool IsHatosagiUgy(ExecParam execParam, string iratId)
        {
            // BUG_6284
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            EREC_IraIratok erec_irairatok = null;
            execParam.Record_Id = iratId;

            Result result = service.Get(execParam);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_irairatok = (EREC_IraIratok)result.Record;

                return Sakkora.IsUgyFajtaHatosagi(execParam, erec_irairatok.Ugy_Fajtaja);
            }
            else return true;

            //EREC_IraOnkormAdatokService service = eRecordService.ServiceFactory.GetEREC_IraOnkormAdatokService();

            //execParam.Record_Id = iratId;

            //Result res = service.Get(execParam);

            //if (!res.IsError)
            //{
            //    EREC_IraOnkormAdatok record = res.Record as EREC_IraOnkormAdatok;
            //    if (record != null)
            //    {
            //        return isHatosagiByValue(execParam,record.UgyFajtaja);
            //    }
            //}
            //return true;
        }

        // az �tadott �rt�k alapj�n meghat�rozza, hogy hat�s�gi �gyfajta van-e kiv�lasztva
        public static bool isHatosagiByValue(ExecParam execParam, string Value)
        {
            // BLG_2020
            if (GetOrgKod(execParam) == Constants.OrgKod.NMHH)
            {
                if (String.IsNullOrEmpty(Value)) return false;
                return Value != KodTarak.UGY_FAJTAJA.NemHatosagiUgy; // '0'
            }
            else
            {
                switch (Value)
                {
                    case "1":
                    case "2":
                        return true;
                    case "0":
                        return false;
                    default:
                        //return true;
                        return false;
                }
            }
        }

        public static bool IsIratIntezkedesreAtvetelElerheto(ExecParam execParam)
        {
            return Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRAT_INTEZKEDESREATVETEL_ELERHETO, false);
        }
    }
}
