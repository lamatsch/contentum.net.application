﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.Data;

namespace Contentum.eRecord.BaseUtility
{
    public class Irattar
    {
        public static List<string> GetAllIrattarIds(ExecParam execParam)
        {
            var service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
            Result result = service_csoportok.GetAllKezelhetoIrattar(execParam);
            var lstIrattarIds = new List<string>();
            if (!result.IsError)
            {
                #region CR3206 - Irattári struktúra
                if (result.GetCount > 0)
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string irattarId = row["Id"].ToString();
                        if (!String.IsNullOrEmpty(irattarId) && !lstIrattarIds.Contains(irattarId))
                        {
                            lstIrattarIds.Add(irattarId);
                        }
                    }
                    string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam.Clone()).Obj_Id;
                    lstIrattarIds.Remove(kozpontiIrattarId);
                }
                #endregion
            }
            return lstIrattarIds;
        }

        public static List<string> GetAllJogosultIrattariHelyIds(ExecParam execParam)
        {
            EREC_IrattariHelyekService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();
            Result result = service.GetAllJogosultIrattariHely(execParam);
            var irattariHelyIds = new List<string>();
            if (!result.IsError)
            {
                if (result.GetCount > 0)
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string irattariHelyId = row["Id"].ToString();
                        if (!String.IsNullOrEmpty(irattariHelyId) && !irattariHelyIds.Contains(irattariHelyId))
                        {
                            irattariHelyIds.Add(irattariHelyId);
                        }
                    }
                }
            }
            return irattariHelyIds;
        }

    }
}
