using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System.Collections;
using System.Data;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

namespace Contentum.eRecord.BaseUtility
{
    [Serializable]
    public class IrattariKikero
    {
        public class Statusz
        {
            private String _Allapot = "";
            public String Allapot
            {
                get { return _Allapot; }                
            }

            private String _Kikero_Id;
            public String Kikero_Id
            {
                get { return _Kikero_Id; }
            }

            private String _UgyUgyirat_Id;
            public String UgyUgyirat_Id
            {
                get { return _UgyUgyirat_Id; }
            }

            private String _Tertiveveny_Id;
            public String Tertiveveny_Id
            {
                get { return _Tertiveveny_Id; }
            }

            private String _Jovahagyo_Id;
            public String Jovahagyo_Id
            {
                get { return _Jovahagyo_Id; }
            }

            private String _FelhasznalasiCel;
            public String FelhasznalasiCel
            {
                get { return _FelhasznalasiCel; }
            }

            private String _KikerVege;
            public String KikerVege
            {
                get { return _KikerVege; }
                set { _KikerVege = value; }
            }

            public Statusz()
            {
            }

            public Statusz(String allapot, String felhasznalasiCel, String kikero_Id, String ugyUgyirat_Id, String tertiveveny_Id, String jovahagyo_Id, String kikerVege)
            {
                this._Allapot = allapot;
                this._FelhasznalasiCel = felhasznalasiCel;
                this._Kikero_Id = kikero_Id;
                this._UgyUgyirat_Id = ugyUgyirat_Id;
                this._Tertiveveny_Id = tertiveveny_Id;
                this._Jovahagyo_Id = jovahagyo_Id;
                this._KikerVege = kikerVege;
            }

            public enum ColumnTypes
            {
                Allapot
                , Kikero_Id
                , Jovahagyo_Id
            }

            public ErrorDetails CreateErrorDetail(string message, ColumnTypes columnType)
            {
                string str_columnType = String.Empty;
                string str_columnValue = String.Empty;
                switch (columnType)
                {
                    case ColumnTypes.Allapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Allapot;
                        str_columnValue = this.Allapot;
                        break;
                    case ColumnTypes.Kikero_Id:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Kikero_Id;
                        str_columnValue = this.Kikero_Id;
                        break;
                    case ColumnTypes.Jovahagyo_Id:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Jovahagyo_Id;
                        str_columnValue = this.Jovahagyo_Id;
                        break;
                }

                return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_IrattariKikero, String.Empty, str_columnType, str_columnValue);
            }
        }

        public enum KikeresIrattarTipus { None, AtmenetiIrattar, SkontroIrattar }

        public static Statusz GetAllapotById(String Id, Page ParentPage, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
            return GetAllapotById(Id, execParam, EErrorPanel);
        }
        public static Statusz GetAllapotById(String Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            EREC_IrattariKikero erec_IrattariKikero = null;

            EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
            execParam.Record_Id = Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_IrattariKikero = (EREC_IrattariKikero)result.Record;
                return GetAllapotByBusinessDocument(erec_IrattariKikero);
            }
            else
            {
                if (EErrorPanel != null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                }
                return null;
            }
        }

        public static Statusz GetAllapotByBusinessDocument(EREC_IrattariKikero erec_IrattariKikero)
        {
            Statusz statusz = new Statusz(erec_IrattariKikero.Allapot, erec_IrattariKikero.FelhasznalasiCel, erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero
                , erec_IrattariKikero.UgyUgyirat_Id, erec_IrattariKikero.Tertiveveny_Id, erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy, erec_IrattariKikero.KikerVege);

            return statusz;
        }

        public static Hashtable GetAllapotByDataSet(ExecParam execParam, DataSet ds)
        {
            if (ds.Tables.Count == 0) return new Hashtable(0);
            if (ds.Tables[0].Rows.Count == 0) return new Hashtable(0);
            Hashtable statusz = new Hashtable(ds.Tables[0].Rows.Count);

            try
            {
                foreach (System.Data.DataRow r in ds.Tables[0].Rows)
                {
                    statusz[r["Id"].ToString()] = new Statusz(
                        r["Allapot"].ToString(),
                        r["FelhasznalasiCel"].ToString(),
                        r["FelhasznaloCsoport_Id_Kikero"].ToString(),
                        r["UgyUgyirat_Id"].ToString(),
                        r["Tertiveveny_Id"].ToString(),
                        r["FelhasznaloCsoport_Id_Jovahagy"].ToString(),
                        r["KikerVege"].ToString());
                }
            }
            catch
            {
                return new Hashtable(0);
            }

            return statusz;
        }

        private static class ErrorDetailMessages
        {
            public const string AllapotNemMegfelelo = "Az iratt�ri kik�r� �llapota nem megfelel�.";
            public const string NemOnAKikero = "Nem �n k�rte/k�lcs�n�zte ki az �gyiratot.";
            public const string NemOnAJovahagyo = "�n nem hagyhatja j�v� a kik�r�st.";
        }

        public static bool Jovahagyhato(Statusz statusz, ExecParam execParam)
        {
            ErrorDetails errorDetail;
            return Jovahagyhato(statusz, execParam, out errorDetail);
        }

        public static bool Jovahagyhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            DateTime kikerVege;
            bool tp = DateTime.TryParse(statusz.KikerVege, out kikerVege);

            if (statusz.Allapot != KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.AllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }

            if (tp && kikerVege < DateTime.Now)
            {
                return false;
            }

            return true;
                   

            //if (statusz.Allapot == KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto
            //    && ((tp && kikerVege >= DateTime.Now) || !tp))
            //    return true;
            //else
            //    return false;
        }

        public static bool JovahagyhatoWithJovahagyoCheck(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (!Jovahagyhato(statusz, execParam, out errorDetail))
            {
                return false;
            }

            if (execParam.Felhasznalo_Id != statusz.Jovahagyo_Id)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.NemOnAJovahagyo, Statusz.ColumnTypes.Jovahagyo_Id);
                return false;
            }

            return true;

        }

        public static bool Visszautasithato(Statusz statusz, ExecParam execParam)
        {
            //if (statusz.Jovahagyo_Id != execParam.Felhasznalo_Id)
            //{
            //    // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
            //    return false;
            //}
            //else
            //{
                switch (statusz.Allapot)
                {
                    case KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto:
                        return true;
                    default:
                        return false;

                }
            //}
        }
        public static bool Sztornozhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (statusz.Kikero_Id != execParam.Felhasznalo_Id
                && !Contentum.eUtility.FunctionRights.HasFunctionRight(execParam, "IrattarKolcsonzesKiadasa"))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.NemOnAKikero, Statusz.ColumnTypes.Kikero_Id);
                return false;
            }

            switch (statusz.Allapot)    
            {
                case KodTarak.IRATTARIKIKEROALLAPOT.Nyitott:
                case KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto:
                case KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.AllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool Sztornozhato(Statusz statusz, Page ParentPage, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            ExecParam execParam = UI.SetExecParamDefault(ParentPage);

            if (statusz.Kikero_Id != execParam.Felhasznalo_Id
                && !Contentum.eUtility.FunctionRights.GetFunkcioJog(ParentPage, "IrattarKolcsonzesKiadasa"))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.NemOnAKikero, Statusz.ColumnTypes.Kikero_Id);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.IRATTARIKIKEROALLAPOT.Nyitott:
                case KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto:
                case KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.AllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool Kiadhato(Statusz statusz, ExecParam execParam)
        {
            ErrorDetails errorDetail;
            return Kiadhato(statusz, execParam, out errorDetail);
        }

        public static bool Kiadhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            //if (statusz.Jovahagyo_Id != execParam.Felhasznalo_Id)
            //{
            //    // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
            //    return false;
            //}
            //else
            //{
            switch (statusz.Allapot)
            {
                case KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.AllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool KiadhatoAtmenetiIrattarbol(Statusz statusz, ExecParam execParam)
        {
            ErrorDetails errorDetail;
            return KiadhatoAtmenetiIrattarbol(statusz, execParam, out errorDetail);
        }

        public static bool KiadhatoAtmenetiIrattarbol(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            switch (statusz.Allapot)
            {
                case KodTarak.IRATTARIKIKEROALLAPOT.Nyitott:
                case KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto:
                case KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.AllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool KiadhatoSkontroIrattarbol(Statusz statusz, ExecParam execParam)
        {
            ErrorDetails errorDetail;
            return KiadhatoSkontroIrattarbol(statusz, execParam, out errorDetail);
        }

        public static bool KiadhatoSkontroIrattarbol(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            return KiadhatoAtmenetiIrattarbol(statusz, execParam, out errorDetail);
        }


        public static void SetSearchFilterKiadhatoAtmenetiIrattarbol(EREC_IrattariKikeroSearch search)
        {
            List<string> AllapotList = new List<string>();
            AllapotList.Add(KodTarak.IRATTARIKIKEROALLAPOT.Nyitott);
            AllapotList.Add(KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto);
            AllapotList.Add(KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett);

            if (AllapotList.Count > 0)
            {
                search.Allapot.Value = Search.GetSqlInnerString(AllapotList.ToArray());
                search.Allapot.Operator = Query.Operators.inner;
            }
        }

        public static void SetSearchFilterKiadhatoSkontroIrattarbol(EREC_IrattariKikeroSearch search)
        {
            List<string> AllapotList = new List<string>();
            AllapotList.Add(KodTarak.IRATTARIKIKEROALLAPOT.Nyitott);
            AllapotList.Add(KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto);
            AllapotList.Add(KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett);

            if (AllapotList.Count > 0)
            {
                search.Allapot.Value = Search.GetSqlInnerString(AllapotList.ToArray());
                search.Allapot.Operator = Query.Operators.inner;
            }
        }


        public static bool Engedelyezett(Statusz statusz, ExecParam execParam)
        {
            //if (statusz.Jovahagyo_Id != execParam.Felhasznalo_Id)
            //{
            //    // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
            //    return false;
            //}
            //else
            //{
            switch (statusz.Allapot)
            {
                case KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Sztornozott(Statusz statusz, ExecParam execParam)
        {
            //if (statusz.Jovahagyo_Id != execParam.Felhasznalo_Id)
            //{
            //    // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
            //    return false;
            //}
            //else
            //{
            switch (statusz.Allapot)
            {
                case KodTarak.IRATTARIKIKEROALLAPOT.Sztornozott:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Kiadott(Statusz statusz, ExecParam execParam)
        {
            //if (statusz.Jovahagyo_Id != execParam.Felhasznalo_Id)
            //{
            //    // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
            //    return false;
            //}
            //else
            //{
            switch (statusz.Allapot)
            {
                case KodTarak.IRATTARIKIKEROALLAPOT.Kiadott:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Visszaadott(Statusz statusz, ExecParam execParam)
        {
            //if (statusz.Jovahagyo_Id != execParam.Felhasznalo_Id)
            //{
            //    // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
            //    return false;
            //}
            //else
            //{
            switch (statusz.Allapot)
            {
                case KodTarak.IRATTARIKIKEROALLAPOT.Visszaadott:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Szerelt(Statusz statusz, ExecParam execParam)
        {
            //if (statusz.Jovahagyo_Id != execParam.Felhasznalo_Id)
            //{
            //    // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
            //    return false;
            //}
            //else
            //{
            switch (statusz.Allapot)
            {
                case KodTarak.IRATTARIKIKEROALLAPOT.Szerelt:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Modosithato(Statusz statusz, ExecParam execParam)
        {
            //if (statusz.Jovahagyo_Id != execParam.Felhasznalo_Id)
            //{
            //    // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
            //    return false;
            //}
            //else
            //{
            
            switch (statusz.Allapot)
            {
                case KodTarak.IRATTARIKIKEROALLAPOT.Nyitott:
                case KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Visszaadhato(Statusz statusz, ExecParam execParam)
        {
            //if (statusz.Jovahagyo_Id != execParam.Felhasznalo_Id)
            //{
            //    // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
            //    return false;
            //}
            //else
            //{
            if (statusz.Allapot == KodTarak.IRATTARIKIKEROALLAPOT.Kiadott && statusz.FelhasznalasiCel == "B")
                    return true;
            else
                    return false;
            
        }

        public static bool JovahagyoVagyok(Statusz statusz, ExecParam execParam)
        {
            if (statusz.Jovahagyo_Id == execParam.Felhasznalo_Id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool KikeroVagyok(Statusz statusz, ExecParam execParam)
        {
            if (statusz.Kikero_Id == execParam.Felhasznalo_Id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
