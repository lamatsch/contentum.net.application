﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Contentum.eRecord.BaseUtility
{
    public class IrattariTetelekExport
    {
        public class IrattariTetel
        {
            public string IrattariTetelszam { get; set; }
            public string Nev { get; set; }
        }

        public class IrattariTetelek
        {
            [System.Xml.Serialization.XmlElement("IrattariTetel")]
            public List<IrattariTetel> IrattariTetelekList;
        }

        public class Utf8StringWriter : System.IO.StringWriter
        {
            public override System.Text.Encoding Encoding
            {
                get { return System.Text.Encoding.UTF8; }
            }
        }


        public Page Page { get; set; }
        public ExecParam Exec { get; set; }

        public IrattariTetelekExport(Page Page)
        {
            this.Page = Page;
        }

        public IrattariTetelekExport(ExecParam Exec)
        {
            this.Exec = Exec;
        }


        public Result Export(string[] IraIktatoKonyvekIds)
        {
            Result result = new Result();
            if (IraIktatoKonyvekIds != null)
            {
                foreach (string IktatokonyvId in IraIktatoKonyvekIds)
                {
                    try
                    {
                        string irattariTetelekXML = this.GetIrattariTetelekXML(IktatokonyvId);
                        this.SaveIrattariTetelekXML(IktatokonyvId, irattariTetelekXML);
                    }
                    catch (Exception ex)
                    {
                        result = Contentum.eUtility.ResultException.GetResultFromException(ex);
                    }
                }
            }

            return result;
        }

        private string GetIrattariTetelekXML(string IktatokonyvId)
        {
            EREC_IrattariTetel_IktatokonyvService service = eRecordService.ServiceFactory.GetEREC_IrattariTetel_IktatokonyvService();

            ExecParam ExecParam = null;

            if (this.Page != null)
                ExecParam = UI.SetExecParamDefault(this.Page);
            else
                ExecParam = this.Exec;

            EREC_IrattariTetel_IktatokonyvSearch search = new EREC_IrattariTetel_IktatokonyvSearch();
            search.TopRow = 20;
            Result res = service.GetAllByIktatoKonyv(ExecParam, search, IktatokonyvId);

            if (res.IsError)
            {
                throw new Contentum.eUtility.ResultException(res);
            }

            List<IrattariTetel> irattariTetelekList = new List<IrattariTetel>();
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                string IrattariTetelszam = row["IrattariTetelszam"].ToString();
                string Nev = row["Nev"].ToString();
                irattariTetelekList.Add(new IrattariTetel { IrattariTetelszam = IrattariTetelszam, Nev = Nev });
            }

            IrattariTetelek irattariTetelek = new IrattariTetelek();
            irattariTetelek.IrattariTetelekList = irattariTetelekList;
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(IrattariTetelek));
            string xml;
            using (Utf8StringWriter writer = new Utf8StringWriter())
            {
                serializer.Serialize(writer, irattariTetelek);
                xml = writer.ToString();
            }

            return xml;
        }

        private void SaveIrattariTetelekXML(string IktatokonyvId, string IrattariTetelekXML)
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();

            ExecParam xpm = null;

            if (this.Page != null)
                xpm = UI.SetExecParamDefault(this.Page);
            else
                xpm = this.Exec;

            xpm.Record_Id = IktatokonyvId;
            Result res = service.Get(xpm);

            if (res.IsError)
            {
                throw new Contentum.eUtility.ResultException(res);
            }

            EREC_IraIktatoKonyvek _EREC_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)res.Record;
            _EREC_IraIktatoKonyvek.Updated.SetValueAll(false);
            _EREC_IraIktatoKonyvek.Base.Updated.SetValueAll(false);
            _EREC_IraIktatoKonyvek.Base.Updated.Ver = true;

            _EREC_IraIktatoKonyvek.Base.Note = IrattariTetelekXML;
            _EREC_IraIktatoKonyvek.Base.Updated.Note = true;

            res = service.Update(xpm, _EREC_IraIktatoKonyvek);

            if (res.IsError)
            {
                throw new Contentum.eUtility.ResultException(res);
            }
        }
    }

}
