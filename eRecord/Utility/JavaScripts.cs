using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace Contentum.eRecord.BaseUtility
{
    public class JavaScripts : Contentum.eUtility.JavaScripts
    {
        public static string SetOnClientClickIrattarbaAtvetelConfirm(string GridViewClientId, string PostBackArgumen, string CheckBoxID)
        {
            string javascript = "";

            if (GridViewClientId == null || CheckBoxID == null) return javascript;

            javascript =
            "if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_IrattarbaAtvetel") + " \\n \\n"
            + Contentum.eUtility.Resources.Question.GetString("UIisIrattarbaAtvetel")            
            + "')) {  } else return false;";
            return javascript;
        }

        public static string SetOnClientClickIrattarbaAtvetelConfirm(string GridViewClientId)
        {
            return SetOnClientClickIrattarbaAtvetelConfirm(GridViewClientId, "", "");
        }

        public static string SetOnClientClickIrattarbaAtvetelConfirm(string PostBackControlClientName, string PostBackArgumen)
        {
            return SetOnClientClickIrattarbaAtvetelConfirm(PostBackControlClientName, PostBackArgumen, false);
        }

        public static string SetOnClientClickIrattarbaAtvetelConfirm(string PostBackControlClientName, string PostBackArgumen, bool ForwardRequestToServer)
        {
            return SetOnClientClickIrattarbaAtvetelConfirm(PostBackControlClientName);
        }    
    }
}
