using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Data;
using System.Linq;

namespace Contentum.eRecord.BaseUtility
{
    public class KodTarak : Contentum.eUtility.KodTarak
    {
        // BUG_6935
        public static string GetKodTarId(ExecParam execParam, string kod, string where)
        {
            Contentum.eAdmin.Service.KRT_KodTarakService kodTarakService = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            KRT_KodTarakSearch kodTarakSearch = new KRT_KodTarakSearch();
            kodTarakSearch.Kod.Value = kod;
            kodTarakSearch.Kod.Operator = Query.Operators.equals;
            kodTarakSearch.WhereByManual = where;

            Result kodTarakResult = kodTarakService.GetAllWithKodcsoport(execParam, kodTarakSearch);
            string result = "";
            if (!kodTarakResult.IsError && kodTarakResult.Ds.Tables[0].Rows.Count > 0)
            {
                result = kodTarakResult.Ds.Tables[0].AsEnumerable().FirstOrDefault(x => x["Kod"].ToString() == kod)["Id"].ToString();
            }
            return result;
        }

        public static string GetUjKodFromRegiAdatTipus(string regi_tipus)
        {
            string jelleg = string.Empty;
            switch (regi_tipus)
            {
                case "pap�r alap�": { jelleg = KodTarak.UGYIRAT_JELLEG.Papir; break; };
                case "elektronikus": { jelleg = KodTarak.UGYIRAT_JELLEG.Elektronikus; break; };
                case "vegyes": { jelleg = KodTarak.UGYIRAT_JELLEG.Vegyes; break; };
                default: { jelleg = KodTarak.UGYIRAT_JELLEG.Papir; break; };
            }
            return jelleg;
        }
    }
}
