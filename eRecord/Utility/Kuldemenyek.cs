using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

namespace Contentum.eRecord.BaseUtility
{
    [Serializable]
    public class Kuldemenyek
    {
        [Serializable]
        public class Statusz
        {
            private string id = "";
            public string Id
            {
                get { return id; }
            }

            private String allapot = "";

            public String Allapot
            {
                get { return allapot; }
                set { allapot = value; }
            }
            private String iktatniKell = "";

            public String IktatniKell
            {
                get { return iktatniKell; }
                set { iktatniKell = value; }
            }

            private String zarolo_id = "";
            public String Zarolo_id
            {
                get { return zarolo_id; }
                set { zarolo_id = value; }
            }

            private String felhCsopId_Orzo = "";
            public String FelhCsopId_Orzo
            {
                get { return felhCsopId_Orzo; }
                set { felhCsopId_Orzo = value; }
            }

            private String irat_Id;
            public String Irat_Id
            {
                get { return irat_Id; }
                set { irat_Id = value; }
            }

            private String iratpeldany_Id;
            public String Iratpeldany_Id
            {
                get { return iratpeldany_Id; }
                set { iratpeldany_Id = value; }
            }

            // Email megtagad�shoz sz�ks�ges
            private String kuldesMod;
            public String KuldesMod
            {
                get { return kuldesMod; }
                set { kuldesMod = value; }
            }

            private string csopId_Felelos;
            public string CsopId_Felelos
            {
                get { return csopId_Felelos; }
            }

            // Sz�mla vizsg�lat�hoz sz�ks�ges
            private String tipus;
            public String Tipus
            {
                get { return tipus; }
                set { tipus = value; }
            }

            private String pirAllapot;
            public String PIRAllapot
            {
                get { return pirAllapot; }
                set { pirAllapot = value; }
            }

            /// <summary>
            /// Konstruktor
            /// </summary>
            public Statusz(string _Id, string _Allapot, string _iktatniKell, string _Irat_Id, string _iratpeldany_Id
                , string _FelhCsopId_Orzo, string _zarolo_id, string _kuldesMod, string _csopId_Felelos, string _tipus, string _pirAllapot)
            {
                this.id = _Id;
                this.allapot = _Allapot;
                this.iktatniKell = _iktatniKell;
                this.irat_Id = _Irat_Id;
                this.iratpeldany_Id = _iratpeldany_Id;
                this.felhCsopId_Orzo = _FelhCsopId_Orzo;
                this.zarolo_id = _zarolo_id;
                this.kuldesMod = _kuldesMod;
                this.csopId_Felelos = _csopId_Felelos;
                this.tipus = _tipus;
                this.pirAllapot = _pirAllapot;
            }

            /// <summary>
            /// Konstruktor
            /// </summary>
            public Statusz(string _Id, string _Allapot, string _iktatniKell, string _Irat_Id, string _iratpeldany_Id
                , string _FelhCsopId_Orzo, string _zarolo_id, string _kuldesMod, string _csopId_Felelos)
            {
                this.id = _Id;
                this.allapot = _Allapot;
                this.iktatniKell = _iktatniKell;
                this.irat_Id = _Irat_Id;
                this.iratpeldany_Id = _iratpeldany_Id;
                this.felhCsopId_Orzo = _FelhCsopId_Orzo;
                this.zarolo_id = _zarolo_id;
                this.kuldesMod = _kuldesMod;
                this.csopId_Felelos = _csopId_Felelos;
            }

            public enum ColumnTypes
            {
                Allapot,
                IktatniKell,
                FelhCsopId_Orzo,
                CsopId_Felelos,
                PIRAllapot
            }


            public ErrorDetails CreateErrorDetail(string message, Statusz.ColumnTypes columnType)
            {
                string str_columnType = "";
                string str_columnValue = "";
                switch (columnType)
                {
                    case ColumnTypes.Allapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Allapot;
                        str_columnValue = this.Allapot;
                        break;
                    case ColumnTypes.IktatniKell:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.IktatniKell;
                        str_columnValue = this.IktatniKell;
                        break;
                    case ColumnTypes.FelhCsopId_Orzo:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo;
                        str_columnValue = this.FelhCsopId_Orzo;
                        break;
                    case ColumnTypes.CsopId_Felelos:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos;
                        str_columnValue = this.CsopId_Felelos;
                        break;
                    case ColumnTypes.PIRAllapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.PIRAllapot;
                        str_columnValue = this.PIRAllapot;
                        break;
                }

                return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek, this.Id, str_columnType, str_columnValue);
            }

            public ErrorDetails CreateErrorDetail(int code, Statusz.ColumnTypes columnType)
            {
                string str_columnType = "";
                string str_columnValue = "";
                switch (columnType)
                {
                    case ColumnTypes.Allapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Allapot;
                        str_columnValue = this.Allapot;
                        break;
                    case ColumnTypes.IktatniKell:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.IktatniKell;
                        str_columnValue = this.IktatniKell;
                        break;
                    case ColumnTypes.FelhCsopId_Orzo:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo;
                        str_columnValue = this.FelhCsopId_Orzo;
                        break;
                    case ColumnTypes.PIRAllapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.PIRAllapot;
                        str_columnValue = this.PIRAllapot;
                        break;
                }

                return new ErrorDetails(code, Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek, this.Id, str_columnType, str_columnValue);
            }
        }


        public class FilterObject
        {
            private string _PostazasIranya;
            public string PostazasIranya
            {
                get { return _PostazasIranya; }
                set { _PostazasIranya = value; }
            }

            private string _MinDate;
            public string MinDate
            {
                get { return _MinDate; }
                set { _MinDate = value; }
            }

            public void ClearMinDate()
            {
                _MinDate = null;
            }

            private string _MaxDate;
            public string MaxDate
            {
                get { return _MaxDate; }
                set { _MaxDate = value; }
            }

            public void ClearMaxDate()
            {
                _MaxDate = null;
            }

            private string _AlSzervezetId;

            public string AlSzervezetId
            {
                get { return _AlSzervezetId; }
                set { _AlSzervezetId = value; }
            }

            private string _ReadableWhere;
            public string ReadableWhere
            {
                get { return _ReadableWhere; }
                set { _ReadableWhere = (value ?? ""); }
            }

            public FilterObject()
            {
                this._PostazasIranya = null;
                this._MinDate = null;
                this._MaxDate = null;
                this._ReadableWhere = "";
            }

            public void Clear()
            {
                this._PostazasIranya = null;
                this._MinDate = null;
                this._MaxDate = null;
                this._ReadableWhere = "";
                this._AlSzervezetId = null;
            }

            private const string queryPrefix = "FilterBy";

            public string QsSerialize()
            {
                Logger.Debug("QueryString soros�t�s START");
                List<string> queryStringItems = new List<string>();
                string query = String.Empty;
                if (!String.IsNullOrEmpty(this._PostazasIranya))
                {
                    queryStringItems.Add(queryPrefix + "PostazasIranya=" + System.Web.HttpUtility.UrlEncode(this._PostazasIranya));
                }

                if (!String.IsNullOrEmpty(this._MinDate))
                {
                    queryStringItems.Add(queryPrefix + "MinDate=" + System.Web.HttpUtility.UrlEncode(this._MinDate));
                }

                if (!String.IsNullOrEmpty(this._MaxDate))
                {
                    queryStringItems.Add(queryPrefix + "MaxDate=" + System.Web.HttpUtility.UrlEncode(this._MaxDate));
                }

                if (!String.IsNullOrEmpty(this._AlSzervezetId))
                {
                    queryStringItems.Add(String.Format("{0}AlSzervezetId={1}", queryPrefix, System.Web.HttpUtility.UrlEncode(this._AlSzervezetId)));
                }

                if (!String.IsNullOrEmpty(this._ReadableWhere))
                {
                    queryStringItems.Add(queryPrefix + "ReadableWhere=" + System.Web.HttpUtility.UrlEncode(this._ReadableWhere));
                }

                if (queryStringItems.Count > 0)
                {
                    query = String.Join("&", queryStringItems.ToArray());
                }

                Logger.Debug("QueryString: " + query);
                Logger.Debug("QueryString soros�t�s END");

                return query;
            }

            public void QsDeserialize(System.Collections.Specialized.NameValueCollection queryString)
            {
                Logger.Debug("QueryString desoros�t�s START");
                Logger.Debug("QueryString: " + queryString.ToString());

                string postazasIranya = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "PostazasIranya"));
                string minDate = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "MinDate"));
                string maxDate = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "MaxDate"));
                this._AlSzervezetId = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "AlSzervezetId"));
                string readableWhere = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "ReadableWhere"));

                this._PostazasIranya = postazasIranya;
                this._MinDate = minDate;
                this._MaxDate = maxDate;
                this._ReadableWhere = (readableWhere ?? "");

                Logger.Debug("QueryString desoros�t�s END");
                //Logger.Debug(String.Format("Eredm�ny: PostazasIranya: {0}, Szervezet Id: {1}", (this._PostazasIranya == null ? "null" : this._PostazasIranya), (this._SzervezetId == null ? "null" : this._SzervezetId)));
                Logger.Debug(String.Format("Eredm�ny: PostazasIranya: {0}", (this._PostazasIranya == null ? "null" : this._PostazasIranya)));
                Logger.Debug(String.Format("MinDate: {0}, MaxDate: {1}", (this._MinDate == null ? "null" : this._MinDate), (this._MaxDate == null ? "null" : this._MaxDate)));
                Logger.Debug(String.Format("ReadableWhere: {0}", this._ReadableWhere));
            }

            public void SetSearchObject(ExecParam execParam, ref EREC_KuldKuldemenyekSearch searchObject)
            {
                if (!String.IsNullOrEmpty(this._PostazasIranya))
                {
                    searchObject.PostazasIranya.Value = this._PostazasIranya;
                    searchObject.PostazasIranya.Operator = Query.Operators.equals;
                }
                //else
                //{ 
                //    // default sz�r�s: nem hozzuk a kimen� k�ldem�nyeket
                //    searchObject.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Kimeno;
                //    searchObject.PostazasIranya.Operator = Query.Operators.notequals;
                //}

                if (!String.IsNullOrEmpty(this._ReadableWhere))
                {
                    searchObject.ReadableWhere = this._ReadableWhere;
                }

                if (!String.IsNullOrEmpty(this._MinDate) && this._MinDate == this._MaxDate)
                {
                    searchObject.BeerkezesIdeje.Value = this._MinDate;
                    searchObject.BeerkezesIdeje.Operator = Query.Operators.equals;
                }
                else if (!String.IsNullOrEmpty(this._MinDate) && !String.IsNullOrEmpty(this._MaxDate))
                {
                    searchObject.BeerkezesIdeje.Value = this._MinDate;
                    searchObject.BeerkezesIdeje.ValueTo = this._MaxDate;
                    searchObject.BeerkezesIdeje.Operator = Query.Operators.between;
                }
                else if (!String.IsNullOrEmpty(this._MinDate))
                {
                    searchObject.BeerkezesIdeje.Value = this._MinDate;
                    searchObject.BeerkezesIdeje.Operator = Query.Operators.greaterorequal;
                }
                else if (!String.IsNullOrEmpty(this._MaxDate))
                {
                    searchObject.BeerkezesIdeje.Value = this._MaxDate;
                    searchObject.BeerkezesIdeje.Operator = Query.Operators.lessorequal;
                }

                if (!String.IsNullOrEmpty(this._AlSzervezetId))
                {
                    searchObject.AlSzervezetId = this._AlSzervezetId;
                }
            }

        }

        private static class ErrorDetailMessages
        {
            public const string KuldemenyAllapotNemMegfelelo = "A k�ldem�ny �llapota nem megfelel�.";
            public const string KuldemenynekNemOnAzOrzoje = "A k�ldem�nynek nem �n az �rz�je.";
            public const string KuldemenyIktatando = "A k�ldem�nyt iktatni kell.";
            public const string KuldemenyNemIktatando = "A k�ldem�ny nem iktatand�.";
            public const string KuldemenyNemLezart = "A k�ldem�ny nincs lez�rva.";
            public const string KuldemenyAllapotNemExpedialt = "A k�ldem�ny nem expedi�lt �llapot�.";
            public const string SzamlaPIRAllapotNemMegfelelo = "A sz�mla PIR-beli �llapota nem megfelel�.";
        }


        public static Statusz GetAllapotById(String Id, Page ParentPage, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());

            return GetAllapotById(Id, execParam, EErrorPanel);
        }


        public static Statusz GetAllapotById(String Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            EREC_KuldKuldemenyek erec_KuldKuldemenyek = null;

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            //ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
            execParam.Record_Id = Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result.Record;

                bool isPIREnabled = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.PIR_INTERFACE_ENABLED);
                if (isPIREnabled && erec_KuldKuldemenyek.Tipus == KodTarak.KULDEMENY_TIPUS.Szamla)
                {
                    EREC_SzamlakService service_szamla = eRecordService.ServiceFactory.GetEREC_SzamlakService();
                    ExecParam execParam_szamla = execParam.Clone();
                    execParam_szamla.Record_Id = erec_KuldKuldemenyek.Id;

                    Result result_szamla = service_szamla.GetByKuldemenyId(execParam_szamla);

                    if (result_szamla.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result_szamla);
                        return null;
                    }

                    EREC_Szamlak erec_Szamlak = (EREC_Szamlak)result_szamla.Record;

                    Statusz statusz = GetAllapotByBusinessDocument(erec_KuldKuldemenyek);
                    statusz.Tipus = erec_KuldKuldemenyek.Tipus; // KodTarak.KULDEMENY_TIPUS.Szamla
                    statusz.PIRAllapot = erec_Szamlak.PIRAllapot; // egyel�re aszinkron vissza�r�s, nem biztos, hogy aktu�lis!

                    return statusz;
                }
                else
                {
                    return GetAllapotByBusinessDocument(erec_KuldKuldemenyek);
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                return null;
            }
        }

        /// <summary>
        /// T�bb k�ldem�ny �llapot�t adja vissza egyszerre DataSet-b�l
        /// </summary>
        /// <param name="ep">execParam</param>
        /// <param name="ds">DataSet</param>
        /// <returns>HashTable Statusz elemekkel. Hivatkozni r�juk: statusz["Id"]-val lehet.</returns>
        public static Hashtable GetAllapotByDataSet(ExecParam ep, System.Data.DataSet ds)
        {
            if (ds.Tables.Count == 0) return new Hashtable(0);
            if (ds.Tables[0].Rows.Count == 0) return new Hashtable(0);
            Hashtable statusz = new Hashtable(ds.Tables[0].Rows.Count);

            try
            {
                bool isPIREnabled = Rendszerparameterek.GetBoolean(ep, Rendszerparameterek.PIR_INTERFACE_ENABLED);
                bool hasPIRAllapot = ds.Tables[0].Columns.Contains("PIRAllapot");

                foreach (System.Data.DataRow r in ds.Tables[0].Rows)
                {

                    if (isPIREnabled && hasPIRAllapot)
                    {
                        statusz[r["Id"].ToString()] = new Statusz(
                            r["Id"].ToString(),
                            r["Allapot"].ToString(),
                            r["IktatniKell"].ToString(),
                            "",
                            "",
                            r["FelhasznaloCsoport_Id_Orzo"].ToString(),
                            r["Zarolo_id"].ToString(),
                            r["KuldesMod"].ToString(),
                            r["Csoport_Id_Felelos"].ToString(),
                            r["Tipus"].ToString(),
                            r["PIRAllapot"].ToString());
                    }
                    else
                    {
                        statusz[r["Id"].ToString()] = new Statusz(
                            r["Id"].ToString(),
                            r["Allapot"].ToString(),
                            r["IktatniKell"].ToString(),
                            "",
                            "",
                            r["FelhasznaloCsoport_Id_Orzo"].ToString(),
                            r["Zarolo_id"].ToString(),
                            r["KuldesMod"].ToString(),
                            r["Csoport_Id_Felelos"].ToString());
                    }
                }
            }
            catch
            {
                return new Hashtable(0);
            }

            return statusz;
        }

        public static Statusz GetAllapotByBusinessDocument(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
        {
            Statusz statusz = new Statusz(erec_KuldKuldemenyek.Id, erec_KuldKuldemenyek.Allapot, erec_KuldKuldemenyek.IktatniKell, "", ""
                , erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo, erec_KuldKuldemenyek.Base.Zarolo_id, erec_KuldKuldemenyek.KuldesMod, erec_KuldKuldemenyek.Csoport_Id_Felelos);
            return statusz;
        }

        public static bool Iktathato(ExecParam execparam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhaszn�l� az �rz�:
            if (execparam.Felhasznalo_Id != statusz.FelhCsopId_Orzo)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenynekNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            if (statusz.IktatniKell == KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando
                &&
                (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Erkeztetve
                    // CR#2303 - 2010.02.16: Tov�bb�t�s alatti k�ldem�ny nem iktathat�
                    // || statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt
                    || statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Szignalva
                    || statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Atveve)
                )
            {
                return true;
            }
            else
            {
                if (statusz.IktatniKell != KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando)
                {
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyNemIktatando, Statusz.ColumnTypes.IktatniKell);
                }
                else
                {
                    // �llapot nem megfelel�:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                }

                return false;
            }
        }



        public static bool Modosithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (statusz.FelhCsopId_Orzo != FelhasznaloProfil.GetFelhasznaloCsoport(execParam))
            {
                // ha nem a felhaszn�l� az �rz�, nem m�dos�that�
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenynekNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }
            else
            {
                bool isPIREnabled = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.PIR_INTERFACE_ENABLED);
                switch (statusz.Allapot)
                {
                    case KodTarak.KULDEMENY_ALLAPOT.Erkeztetve:
                    case KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt:
                    case KodTarak.KULDEMENY_ALLAPOT.Szignalva:
                    case KodTarak.KULDEMENY_ALLAPOT.Atveve:
                        if (isPIREnabled && statusz.Tipus == KodTarak.KULDEMENY_TIPUS.Szamla)
                        {
                            PirEdokSzamlak.Statusz statusz_piredokszamlak = new PirEdokSzamlak.Statusz(statusz.Id, null, statusz.PIRAllapot);
                            return PirEdokSzamlak.Modosithato(statusz_piredokszamlak, execParam, out errorDetail);
                        }
                        return true;
                    default:
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                        return false;
                }
            }
        }


        public static bool Lezarhato(ExecParam execparam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (execparam.Felhasznalo_Id != statusz.FelhCsopId_Orzo)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenynekNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.KULDEMENY_ALLAPOT.Iktatva:
                case KodTarak.KULDEMENY_ALLAPOT.Expedialt:
                case KodTarak.KULDEMENY_ALLAPOT.Kimeno_osszeallitas_alatt:
                case KodTarak.KULDEMENY_ALLAPOT.Lezarva:
                case KodTarak.KULDEMENY_ALLAPOT.Postazott:
                case KodTarak.KULDEMENY_ALLAPOT.Sztorno:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }

            if (statusz.IktatniKell == KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyIktatando, Statusz.ColumnTypes.IktatniKell);
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool LezarasVisszavonhato(ExecParam execparam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (execparam.Felhasznalo_Id != statusz.FelhCsopId_Orzo)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenynekNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            if (statusz.Allapot != KodTarak.KULDEMENY_ALLAPOT.Lezarva)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyNemLezart, Statusz.ColumnTypes.Allapot);
                return false;
            }
            else
            {
                return true;
            }
        }

        public static List<string> GetNemSztornozhatoAllapotok()
        {
            List<string> listResult = new List<string>();
            listResult.Add(KodTarak.KULDEMENY_ALLAPOT.Iktatva);
            listResult.Add(KodTarak.KULDEMENY_ALLAPOT.Lezarva);
            listResult.Add(KodTarak.KULDEMENY_ALLAPOT.Sztorno);
            listResult.Add(KodTarak.KULDEMENY_ALLAPOT.Iktatas_Megtagadva);
            listResult.Add(KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt);
            return listResult;
        }


        public static bool Sztornozhato(ExecParam execparam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (execparam.Felhasznalo_Id != statusz.FelhCsopId_Orzo)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenynekNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            if (Kuldemenyek.GetNemSztornozhatoAllapotok().Contains(statusz.Allapot))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
            else
            {
                bool isPIREnabled = Rendszerparameterek.GetBoolean(execparam, Rendszerparameterek.PIR_INTERFACE_ENABLED);
                if (isPIREnabled && statusz.Tipus == KodTarak.KULDEMENY_TIPUS.Szamla)
                {
                    PirEdokSzamlak.Statusz statusz_piredokszamlak = new PirEdokSzamlak.Statusz(statusz.Id, null, statusz.PIRAllapot);
                    return PirEdokSzamlak.Sztornozhato(statusz_piredokszamlak, execparam, out errorDetail);
                }

                return true;
            }
        }


        public static bool KimenoKuldemenySztornozhato(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.KULDEMENY_ALLAPOT.Postazott:
                case KodTarak.KULDEMENY_ALLAPOT.Expedialt:
                case KodTarak.KULDEMENY_ALLAPOT.Kimeno_osszeallitas_alatt:
                case KodTarak.KULDEMENY_ALLAPOT.Foglalt:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }


        public static bool Bonthato(ExecParam execparam, Statusz statusz)
        {
            ErrorDetails errorDetail;
            return Iktathato(execparam, statusz, out errorDetail);
        }

        /// <summary>
        /// A k�ldem�nyhez csatolhat� m�sik k�ldem�ny
        /// </summary>        
        public static bool Csatolhato(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.KULDEMENY_ALLAPOT.Erkeztetve:
                case KodTarak.KULDEMENY_ALLAPOT.Atveve:
                    return true;
                //case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                //    {
                //        switch (statusz.TovabbitasAlattiAllapot)
                //        {
                //            case KodTarak.KULDEMENY_ALLAPOT.Erkeztetve:
                //            case KodTarak.KULDEMENY_ALLAPOT.Atveve:
                //                return true;
                //            default:
                //                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.TovabbitasAlattiAllapot);
                //                return false;
                //        }
                //    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static void SetCsatolhatoSearchObject(EREC_KuldKuldemenyekSearch search)
        {
            search.Manual_Csatolhato.Value = "'"
                + KodTarak.KULDEMENY_ALLAPOT.Erkeztetve + "','"
                + KodTarak.KULDEMENY_ALLAPOT.Atveve + "'";

            search.Manual_Csatolhato.Operator = Query.Operators.inner;
        }


        public static bool Iktatott(Statusz statusz)
        {
            if (KodTarak.KULDEMENY_ALLAPOT.Iktatva == statusz.Allapot)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static bool AtadasraKijelolheto(ExecParam execParam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhaszn�l� az �rz�:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenynekNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.KULDEMENY_ALLAPOT.Erkeztetve:
                case KodTarak.KULDEMENY_ALLAPOT.Atveve:
                    return true;
                case KodTarak.KULDEMENY_ALLAPOT.Expedialt: // kimen� k�ldem�ny �tadhat� post�z�sra
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool Atadhato(ExecParam execParam, Statusz statusz)
        {
            // ha nem a felhaszn�l� az �rz�:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.KULDEMENY_ALLAPOT.Erkeztetve:
                case KodTarak.KULDEMENY_ALLAPOT.Atveve:
                case KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.KULDEMENY_ALLAPOT.Szignalva:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Atveheto(ExecParam execParam, Statusz statusz)
        {
            ErrorDetails errorDetail;
            return Atveheto(execParam, statusz, out errorDetail);
        }

        public static bool Atveheto(ExecParam execParam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Nem itt van az ellen�rz�s:
            //// A felhaszn�l� tagja-e a felel�s csoportnak:
            //if (Csoportok.IsMember(execParam.Felhasznalo_Id, statusz.CsopId_Felelos) == false)
            //{
            //    return false;
            //}

            switch (statusz.Allapot)
            {
                case KodTarak.KULDEMENY_ALLAPOT.Erkeztetve:
                case KodTarak.KULDEMENY_ALLAPOT.Atveve:
                case KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.KULDEMENY_ALLAPOT.Szignalva:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool Visszakuldheto(ExecParam execParam, Statusz statusz)
        {
            ErrorDetails errorDetail;
            return Visszakuldheto(execParam, statusz, out errorDetail);
        }

        public static bool Visszakuldheto(ExecParam execParam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt:
                    if (execParam.Felhasznalo_Id.Equals(statusz.FelhCsopId_Orzo, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // A t�tel nem k�ldhet� vissza, mert azt �n adta �t �s csak a c�mzett k�ldheti vissza �nnek!
                        errorDetail = statusz.CreateErrorDetail("A t�tel nem k�ldhet� vissza, mert azt �n adta �t �s csak a c�mzett k�ldheti vissza �nnek!", Statusz.ColumnTypes.CsopId_Felelos);
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }

        }

        public static bool AtadasraKijelolesVisszavonhato(Statusz statusz, ExecParam execParam)
        {
            // ha nem a felhaszn�l� az �rz�:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.KULDEMENY_ALLAPOT.Erkeztetve:
                case KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.KULDEMENY_ALLAPOT.Szignalva:
                case KodTarak.KULDEMENY_ALLAPOT.Atveve:
                    return true;
                default:
                    return false;
            }
        }


        public static bool Szignalhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (statusz == null)
            {
                return false;
            }

            // ha nem a felhaszn�l� az �rz�:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenynekNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.KULDEMENY_ALLAPOT.Erkeztetve:
                case KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.KULDEMENY_ALLAPOT.Szignalva:
                case KodTarak.KULDEMENY_ALLAPOT.Atveve:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static List<string> GetPostazhatoAllapotok()
        {
            List<string> listResult = new List<string>();
            listResult.Add(KodTarak.KULDEMENY_ALLAPOT.Expedialt);
            return listResult;
        }

        public static bool Postazhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhaszn�l� az �rz�:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenynekNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            if (Kuldemenyek.GetPostazhatoAllapotok().Contains(statusz.Allapot))
            {
                return true;
            }
            else
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemExpedialt, Statusz.ColumnTypes.Allapot);
                return false;
            }

            //switch (statusz.Allapot)
            //{
            //    case KodTarak.KULDEMENY_ALLAPOT.Expedialt:
            //        return true;
            //    default:
            //        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemExpedialt, Statusz.ColumnTypes.Allapot);
            //        return false;
            //}

        }


        public static bool VanElsodlegesKuldemenye(ExecParam execParam, EREC_KuldKuldemenyek kuldemeny)
        {
            return (kuldemeny.BoritoTipus == KodTarak.BoritoTipus.Irat || kuldemeny.MegorzesJelzo == "1");
        }

        public static bool IktatasMegtagadhato(ExecParam execparam, Statusz statusz)
        {
            ErrorDetails errorDetail;
            if (Iktathato(execparam, statusz, out errorDetail) == false)
            {
                // ha nem iktathat�, akkor nem is tagadhat� meg az iktat�s
                return false;
            }

            switch (statusz.KuldesMod)
            {
                case KodTarak.KULDEMENY_KULDES_MODJA.E_mail:
                    // az iktat�s megtagad�s�t jelenleg csak emailekre �rtelmezz�k
                    return true;
                default:
                    return false;
            }
        }

        public static bool Felszabadithato(Statusz kuldemenyStatusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Csak az iktatott �llapot� k�ldem�ny szabad�that� fel
            if (kuldemenyStatusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatva || kuldemenyStatusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Foglalt)
            {
                return true;
            }
            else
            {
                errorDetail = kuldemenyStatusz.CreateErrorDetail(ErrorDetailMessages.KuldemenyAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
        }

        public static string GetFullErkeztetoSzam(ExecParam execParam, EREC_IraIktatoKonyvek erkeztetokonyv, EREC_KuldKuldemenyek kuldemeny)
        {
            return Iktatoszam.GetFullAzonosito(execParam, null, erkeztetokonyv, null, null, kuldemeny, "K");
        }

        public static string GetFullKuldemenyAzonosito(ExecParam execParam, EREC_IraIktatoKonyvek iktatokonyv, EREC_KuldKuldemenyek kuldemeny)
        {
            return Iktatoszam.GetFullAzonosito(execParam, null, iktatokonyv, null, null, kuldemeny, "L");
        }

        public static bool IsPapirAlapu(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
        {
            return erec_KuldKuldemenyek.UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
        }

    }
}
