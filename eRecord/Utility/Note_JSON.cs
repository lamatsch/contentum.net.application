﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eRecord.BaseUtility
{
    //LZS - BUG_11081
    //Az EREC_UgyUgyiratok.Base.Note mezőjéhez tartozó JSON osztály, hogy könnyen lehessen serializálni/deserializálni.
    public class Note_JSON
    {
        public string JavasoltElozmenyek
        {
            get;
            set;
        }

        public string Megjegyzes
        {
            get;
            set;
        }

        public string SZK
        {
            get;
            set;
        }

        public Note_JSON()
        {

        }
    }
}
