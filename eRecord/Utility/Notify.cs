using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;

namespace Contentum.eRecord.BaseUtility
{
    public class Notify : Contentum.eUtility.Notify
    {
        public static bool SendNewFeladatNotification(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            if (SendFeladatNotification(execParam,erec_HataridosFeladatok))
            {
                int prioritas;
                if (Int32.TryParse(erec_HataridosFeladatok.Prioritas, out prioritas))
                {
                    try
                    {
                        int magas = Int32.Parse(KodTarak.FELADAT_Prioritas.Magas);
                        int kozepes = Int32.Parse(KodTarak.FELADAT_Prioritas.Kozepes);

                        if (prioritas >= magas)
                        {
                            //E-mail kuldes
                            ExecParam xpmEmail = execParam.Clone();
                            xpmEmail.CallingChain = new CallingChain();
                            if (execParam.CallingChain.Count > 0)
                            {
                                xpmEmail.CallingChain.Push(execParam.CallingChain.Peek());
                            }
                            Contentum.eAdmin.Service.EmailService emailService = eAdminService.ServiceFactory.GetEmailService();
                            emailService.BeginSendFeladatNotificationEmail(xpmEmail, erec_HataridosFeladatok, null, null);
                            return true;
                        }
                        // CR3239 Ha nem FPH, akkor nem pr�b�lunk meg �zenetet k�ldeni...
                        Result org = new Result();
                        using (eAdmin.Service.KRT_OrgokService service = eAdminService.ServiceFactory.GetKRT_OrgokService())
                        {
                            ExecParam _exec = execParam.Clone();
                            _exec.Record_Id = execParam.Org_Id;
                            org = service.Get(_exec);
                        }
                        string orgKod = "";
                        if (!org.IsError) orgKod = ((KRT_Orgok)org.Record).Kod;
                        if (orgKod == Contentum.eUtility.Constants.OrgKod.FPH)
                        {
                            if (prioritas >= kozepes)
                            {
                                //Uzenet kuldes
                                ExecParam xpmMessage = execParam.Clone();
                                xpmMessage.CallingChain = new CallingChain();
                                if (xpmMessage.CallingChain.Count > 0)
                                {
                                    xpmMessage.CallingChain.Push(execParam.CallingChain.Peek());
                                }
                                Contentum.eAdmin.Service.MessageService messageService = eAdminService.ServiceFactory.GetMessageService();
                                messageService.BeginSendFeladatNotificationMessage(xpmMessage, erec_HataridosFeladatok, null, null);
                                return true;
                            }
                        }
                    }
                    catch
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool SendUpdateFeladatNotification(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            if (SendFeladatNotification(execParam,erec_HataridosFeladatok))
            {
                if (erec_HataridosFeladatok.Updated.Allapot)
                {
                    int lezarasPrioritas;
                    if (Int32.TryParse(erec_HataridosFeladatok.LezarasPrioritas, out lezarasPrioritas))
                    {
                        try
                        {
                            int magas = Int32.Parse(KodTarak.FELADAT_Prioritas.Magas);
                            int kozepes = Int32.Parse(KodTarak.FELADAT_Prioritas.Kozepes);

                            if (lezarasPrioritas >= magas)
                            {
                                //E-mail kuldes
                                ExecParam xpmEmail = execParam.Clone();
                                xpmEmail.CallingChain = new CallingChain();
                                if (execParam.CallingChain.Count > 0)
                                {
                                    xpmEmail.CallingChain.Push(execParam.CallingChain.Peek());
                                }
                                Contentum.eAdmin.Service.EmailService emailService = eAdminService.ServiceFactory.GetEmailService();
                                emailService.BeginSendFeladatNotificationEmail(xpmEmail, erec_HataridosFeladatok, null, null);
                                return true;
                            }
                            // CR3239 Ha nem FPH, akkor nem pr�b�lunk meg �zenetet k�ldeni...
                            Result org = new Result();
                            using (eAdmin.Service.KRT_OrgokService service = eAdminService.ServiceFactory.GetKRT_OrgokService())
                            {
                                ExecParam _exec = execParam.Clone();
                                _exec.Record_Id = execParam.Org_Id;
                                org = service.Get(_exec);
                            }
                            string orgKod = "";
                            if (!org.IsError) orgKod = ((KRT_Orgok)org.Record).Kod;
                            if (orgKod == Contentum.eUtility.Constants.OrgKod.FPH)
                            {
                                if (lezarasPrioritas >= kozepes)
                                {
                                    //Uzenet kuldes
                                    ExecParam xpmMessage = execParam.Clone();
                                    xpmMessage.CallingChain = new CallingChain();
                                    if (xpmMessage.CallingChain.Count > 0)
                                    {
                                        xpmMessage.CallingChain.Push(execParam.CallingChain.Peek());
                                    }
                                    Contentum.eAdmin.Service.MessageService messageService = eAdminService.ServiceFactory.GetMessageService();
                                    messageService.BeginSendFeladatNotificationMessage(xpmMessage, erec_HataridosFeladatok, null, null);
                                    return true;
                                }
                            }
                        }
                        catch
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private static bool SendFeladatNotification(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
        {
            if (erec_HataridosFeladatok.Tipus == KodTarak.FELADAT_TIPUS.Feladat &&
                erec_HataridosFeladatok.Memo != Constants.Database.Yes)
            {
                return true;
            }

            return false;
        }
    }

}
