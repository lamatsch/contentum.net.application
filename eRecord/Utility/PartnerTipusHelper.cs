﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

namespace Contentum.eRecord.BaseUtility
{
    public class PartnerHelper
    {
        protected static class Constants
        {
            public const string TERMESZETES_PARTNER_TIPUS = "0";
            public const string NEM_TERMESZETES_PARTNER_TIPUS = "1";
            public const string RENDSZER_TIPUS = "2";
        }
        public static bool IsTermeszetesSzemely(string value)
        {
            if (value == Constants.TERMESZETES_PARTNER_TIPUS) { return true; }
            return false;
        }

        public static bool IsNemTermeszetesSzemely(string value)
        {
            if (value == Constants.NEM_TERMESZETES_PARTNER_TIPUS) { return true; }
            return false;
        }

        public static bool IsRendszer(string value)
        {
            if (value == Constants.RENDSZER_TIPUS) { return true; }
            return false;
        }

        public static string GetCorrectPartnerCim(string feladoTipusa, string partnerKapcsolatiKod, string partnerKRID)
        {
            if (IsTermeszetesSzemely(feladoTipusa)) { return partnerKapcsolatiKod; };
            if (IsNemTermeszetesSzemely(feladoTipusa)) { return partnerKRID; }
            return string.Empty;
        }

        public static string GetCorrectPartnerCim(EREC_eBeadvanyok beadvany)
        {
            if (IsTermeszetesSzemely(beadvany.FeladoTipusa)) { return beadvany.PartnerKapcsolatiKod; };
            if (IsNemTermeszetesSzemely(beadvany.FeladoTipusa)) { return beadvany.PartnerKRID; }
            return string.Empty;
        }

        public static Field GetCorrectPartnerCim(EREC_eBeadvanyokSearch search)
        {
            if (IsTermeszetesSzemely(search.FeladoTipusa.Value)) { return search.PartnerKapcsolatiKod; };
            if (IsNemTermeszetesSzemely(search.FeladoTipusa.Value)) { return search.PartnerKRID; }
            return new Field();
        }

        public static string GetPartnerTipus(string value,Page page)
        {
            return KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.EBEADVANY_KODCSOPORTOK.FELADO_TIPUSA,value,page);
        }
    }
}
