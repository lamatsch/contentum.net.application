using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using System.Web;

namespace Contentum.eRecord.BaseUtility
{
    [Serializable]
    public class PirEdokSzamlak
    {
        [Serializable]
        public class Statusz
        {
            private string id = "";
            public string Id
            {
                get { return id; }
            }

            private string vonalkod = "";
            public string Vonalkod
            {
                get { return vonalkod; }
            }

            private String pirAllapot = "";
            public String PIRAllapot
            {
                get { return pirAllapot; }
                set { pirAllapot = value; }
            }

            public Statusz()
            {
            }

            public Statusz(string _Id, string _Vonalkod, string _PIRAllapot)
            {
                this.id = _Id;
                this.vonalkod = _Vonalkod;
                this.pirAllapot = _PIRAllapot;
            }

            public enum ColumnTypes
            {
                Id,
                Vonalkod,
                PIRAllapot
            }

        //    public ErrorDetails CreateErrorDetail(string message, Statusz.ColumnTypes columnType)
        //    {
        //        string str_columnType = "";
        //        string str_columnValue = "";
        //        switch (columnType)
        //        {
        //            case ColumnTypes.PIRAllapot:
        //                str_columnType = Constants.ErrorDetails.ColumnTypes.PIRAllapot;
        //                str_columnValue = this.PIRAllapot;
        //                break;
        //        }

        //        return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_Szamlak, this.Id, str_columnType, str_columnValue);
        //    }

        //    public ErrorDetails CreateErrorDetail(int code, Statusz.ColumnTypes columnType)
        //    {
        //        string str_columnType = "";
        //        string str_columnValue = "";
        //        switch (columnType)
        //        {
        //            case ColumnTypes.PIRAllapot:
        //                str_columnType = Constants.ErrorDetails.ColumnTypes.PIRAllapot;
        //                str_columnValue = this.PIRAllapot;
        //                break;
        //        }

        //        return new ErrorDetails(code, Constants.ErrorDetails.ObjectTypes.EREC_Szamlak, this.Id, str_columnType, str_columnValue);
        //    }

        }

        //private static class ErrorDetailMessages
        //{
        //    public const string SzamlaPirAllapotNemMegfelelo = "A sz�mla PIR �llapota nem megfelel�.";
        //}

        public static bool ModosithatoAllapotu(string pirallapot, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (String.IsNullOrEmpty(pirallapot))
            {
                //"A sz�mla nem m�dos�that�! A sz�mla PIR �llapota nem azonos�that�!"
                errorDetail = new ErrorDetails(100010, Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.EREC_Szamlak, null, Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.PIRAllapot, pirallapot);
                return false;
            }

            switch (pirallapot)
            {
                case KodTarak.PIR_ALLAPOT.Fogadott:
                    return true;
                default:
                    // "A sz�mla nem m�dos�that�!"
                    errorDetail = new ErrorDetails(100011, Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.EREC_Szamlak, null, Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.PIRAllapot, pirallapot);
                    return false;
            }
        }

        public static bool ModosithatoAllapotu(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            return ModosithatoAllapotu(statusz.PIRAllapot, out errorDetail);
        }

        public static bool Modosithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            bool isPIREnabled = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.PIR_INTERFACE_ENABLED);
            if (isPIREnabled)
            {
                return ModosithatoAllapotu(statusz.PIRAllapot, out errorDetail);
            }

            return true;
        }

        public static bool SztornozhatoAllapotu(string pirallapot, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (String.IsNullOrEmpty(pirallapot))
            {
                //"A sz�mla nem sztorn�zhat�! A sz�mla PIR �llapota nem azonos�that�!"
                errorDetail = new ErrorDetails(100012, Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.EREC_Szamlak, null, Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.PIRAllapot, pirallapot);
                return false;
            }

            switch (pirallapot)
            {
                case KodTarak.PIR_ALLAPOT.Fogadott:
                case KodTarak.PIR_ALLAPOT.Elutasitott:
                    return true;
                default:
                    // "A sz�mla nem sztorn�zhat�!"
                    errorDetail = new ErrorDetails(100013, Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.EREC_Szamlak, null, Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.PIRAllapot, pirallapot);
                    return false;
            }
        }

        public static bool SztornozhatoAllapotu(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            return SztornozhatoAllapotu(statusz.PIRAllapot, out errorDetail);
        }

        public static bool Sztornozhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            bool isPIREnabled = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.PIR_INTERFACE_ENABLED);
            if (isPIREnabled)
            {
                return SztornozhatoAllapotu(statusz.PIRAllapot, out errorDetail);
            }

            return true;
        }


        // PIR �llapot aszinkron t�meges update
        public static void PIRAllapotUpdate_TomegesForAsync(Object objExecParam)
        {
            EREC_SzamlakService service_szamla = eRecordService.ServiceFactory.GetEREC_SzamlakService();
            service_szamla.PIRAllapotUpdate_Tomeges(objExecParam as ExecParam);
        }
    }

}
