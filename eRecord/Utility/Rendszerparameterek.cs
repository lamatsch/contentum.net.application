using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.eRecord.BaseUtility
{
	public class Rendszerparameterek : Contentum.eUtility.Rendszerparameterek
	{
        // BLG_236
        //public const string VONALKOD_KEZELES = "VONALKOD_KEZELES";
        //public const string VONALKOD_PREFIX = "VONALKOD_PREFIX";
		public const string VONALKOD_KIMENO_GENERALT = "VONALKOD_KIMENO_GENERALT";
		public const string VONALKOD_NYOMTATAS_FELULETEN = "VONALKOD_NYOMTATAS_FELULETEN";
        public const string IRATTARIHELY_KAPACITAS = "IRATTARIHELY_KAPACITAS";
        public const string AUTO_RAGSZAMOSZTAS_TOMEGESEN = "AUTO_RAGSZAMOSZTAS_TOMEGESEN";

        // BUG_6549
        public const string BELSO_KIMENO_CIMZETT_CIME_KOTELEZO = "BELSO_KIMENO_CIMZETT_CIME_KOTELEZO";

        // BLG_6295
        public const string SSRS_AUTO_PDF_RENDER = "SSRS_AUTO_PDF_RENDER";

        // BUG_6466
        public const string IRAT_SZIGNALAS_ELERHETO = "IRAT_SZIGNALAS_ELERHETO";

        public const string IRATPELDANY_MINDIG_MODOSITHATO = "IRATPELDANY_MINDIG_MODOSITHATO";//Ha true akkor az iratp�ld�nyt sztorn�zott �llapoton k�v�l is lehet m�dos�tani
    }
}
