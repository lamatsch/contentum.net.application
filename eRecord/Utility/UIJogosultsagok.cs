﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eRecord.BaseUtility
{
   public class UIJogosultsagok
    {
       
        private static string GetCurrentUserId(System.Web.UI.Page page)
        {
            string userIdToCheck = page.Session[eUtility.Constants.LoginUserId].ToString();

            var helyettesitettId = page.Session[eUtility.Constants.HelyettesitettId];
            //ha helyettesítünk valakit akkor az ő userIdját használjuk
            if (helyettesitettId != null && !string.IsNullOrEmpty(helyettesitettId.ToString()))
            {
                userIdToCheck = page.Session[eUtility.Constants.HelyettesitettId].ToString();
            }
            return userIdToCheck;
        }

        public static bool CanUserViewAttachements(System.Web.UI.Page page, string orzoId,List<string> alairoIds)
        {
            if (CanUserViewAttachements(page, orzoId))
            {
                return true;
            }

            if (!alairoIds.Contains(GetCurrentUserId(page)))
            {
                return false;
            }
            return true;
        }

        public static bool CanUserViewAttachements(System.Web.UI.Page page, string orzoId)
        {
            //ha nincs beállítva a paraméter nem ellenőrizünk
            if (!Rendszerparameterek.GetBoolean(page, Rendszerparameterek.CSATOLMANY_LATHATOSAG_IRATPELDANYTOL))
            {
                return true;
            }
           
            if (string.IsNullOrEmpty(orzoId)) { return false; }
    
            if (orzoId  != GetCurrentUserId(page))
            {
                return false;
            }
            return true;
        }
    }
}
