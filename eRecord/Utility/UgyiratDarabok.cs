using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;

namespace Contentum.eRecord.BaseUtility
{
    [Serializable]
    public class UgyiratDarabok
    {
        [Serializable]
        public class Statusz
        {
            private string id = "";
            public string Id
            {
                get { return id; }
            }

            private String allapot = "";
            public String Allapot
            {
                get { return allapot; }
            }

            private String ugyirat_Id;
            public String Ugyirat_Id
            {
                get { return ugyirat_Id; }
            }

            //private String felhCsopId_Orzo;
            //public String FelhCsopId_Orzo
            //{
            //    get { return felhCsopId_Orzo; }
            //}


            public Statusz(string _Id, string _Allapot, string _Ugyirat_Id)
            {
                this.id = _Id;
                this.allapot = _Allapot;
                this.ugyirat_Id = _Ugyirat_Id;
            }

            public enum ColumnTypes
            {
                Allapot
            }


            public ErrorDetails CreateErrorDetail(string message, Statusz.ColumnTypes columnType)
            {
                string str_columnType = "";
                string str_columnValue = "";
                switch (columnType)
                {
                    case Statusz.ColumnTypes.Allapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Allapot;
                        str_columnValue = this.Allapot;
                        break;
                }

                return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratdarabok, this.Id, str_columnType, str_columnValue);
            }
	
        }

        private static class ErrorDetailMessages
        {            
            public const string UgyiratDarabAllapotaNemMegfelelo = "Az �gyiratdarab �llapota nem megfelel�.";
        }


        public static Statusz GetAllapotById(String UgyiratDarab_Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok = null;

            EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
            execParam.Record_Id = UgyiratDarab_Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_UgyUgyiratdarabok = (EREC_UgyUgyiratdarabok)result.Record;
                return GetAllapotByBusinessDocument(erec_UgyUgyiratdarabok);
            }
            else
            {
                if (EErrorPanel != null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                }
                return null;
            }
        }

        public static Statusz GetAllapotByBusinessDocument(EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok)
        {
            Statusz statusz = new Statusz(erec_UgyUgyiratdarabok.Id, erec_UgyUgyiratdarabok.Allapot, erec_UgyUgyiratdarabok.UgyUgyirat_Id);

            return statusz;
        }


        public static bool Modosithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            return ModosithatoAllapotu(statusz, execParam, out errorDetail);
        }

        public static bool Modosithato(Statusz statusz, Ugyiratok.Statusz ugyiratStatusz, out ErrorDetails errorDetail)
        {
            return ModosithatoAllapotu(statusz, ugyiratStatusz, out errorDetail);
        }

        public static bool ModosithatoAllapotu(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott:
                case KodTarak.UGYIRATDARAB_ALLAPOT.Elintezette_nyilvanitott:
                    // vizsg�lni kell az �gyirat m�dos�that�s�g�t:
                    Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(statusz.Ugyirat_Id, execParam, null);

                    return Ugyiratok.ModosithatoAllapotu(ugyiratStatusz, out errorDetail);
                case KodTarak.UGYIRATDARAB_ALLAPOT.Lezart:
                case KodTarak.UGYIRATDARAB_ALLAPOT.Szerelt:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratDarabAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratDarabAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool ModosithatoAllapotu(Statusz statusz, Ugyiratok.Statusz ugyiratStatusz, out ErrorDetails errorDetail)
        {
            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott:
                case KodTarak.UGYIRATDARAB_ALLAPOT.Elintezette_nyilvanitott:
                    // vizsg�lni kell az �gyirat m�dos�that�s�g�t:
                    //Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotById(statusz.Ugyirat_Id, execParam, null);

                    return Ugyiratok.ModosithatoAllapotu(ugyiratStatusz, out errorDetail);
                case KodTarak.UGYIRATDARAB_ALLAPOT.Lezart:
                case KodTarak.UGYIRATDARAB_ALLAPOT.Szerelt:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratDarabAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratDarabAllapotaNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool Nyitott(Statusz statusz)
        {
            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott:
                case KodTarak.UGYIRATDARAB_ALLAPOT.Elintezette_nyilvanitott:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Lezart(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.UGYIRATDARAB_ALLAPOT.Lezart)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Szerelt(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.UGYIRATDARAB_ALLAPOT.Szerelt)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Iktatott(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Elintezett(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.UGYIRATDARAB_ALLAPOT.Elintezette_nyilvanitott)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static bool ElintezetteNyilvanithato(Statusz ugyiratDarabStatusz, Ugyiratok.Statusz ugyiratStatusz
            ,ExecParam execParam)
        {
            // �gyiratDarab-nak iktatottnak kell lennie + �gyirat elint�zett� nyilv�n�that�

            if (ugyiratDarabStatusz.Allapot != KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott)
            {
                return false;
            }
            else
            {
                ErrorDetails errorDetail;
                return Ugyiratok.ElintezetteNyilvanithato(ugyiratStatusz, execParam, out errorDetail);
            }
        }


        public static bool Lezarhato(Statusz ugyiratDarabStatusz, Ugyiratok.Statusz ugyiratStatusz
            , ExecParam execParam)
        {
            switch (ugyiratDarabStatusz.Allapot)
            {
                case KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott:
                case KodTarak.UGYIRATDARAB_ALLAPOT.Elintezette_nyilvanitott:
                    {
                        // ha nem a felhaszn�l� az �gyirat �rz�je:
                        if (ugyiratStatusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
                        {
                            return false;
                        }

                        switch (ugyiratStatusz.Allapot)
                        {
                            case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                            case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                            case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
                            case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
                                return true;
                            default:
                                return false;
                        }
                    }
                default:
                    return false;
            }
        }

        public static bool Szerelheto(Statusz ugyiratDarabStatusz, Ugyiratok.Statusz ugyiratStatusz
            , ExecParam execParam)
        {
            switch (ugyiratDarabStatusz.Allapot)
            {
                case KodTarak.UGYIRATDARAB_ALLAPOT.Lezart:
                case KodTarak.UGYIRATDARAB_ALLAPOT.Szerelt:
                    ErrorDetails errorDetail;
                    return Ugyiratok.Szerelheto(ugyiratStatusz, execParam, out errorDetail);
                default:
                    return false;
            }
        }


        // egy�gk�nt kezelend� azonos�t� r�sz + egyedi azonos�t�s, ha van
        private static string GetFoszam_Azonosito(string foszamStr, string evStr, string azonositoStr)
        {
            return foszamStr + " /" + evStr + (!String.IsNullOrEmpty(azonositoStr) ? " /" + azonositoStr : "");
        }
        
        public static string GetFullFoszam(EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok, EREC_IraIktatoKonyvek erec_IraIktatoKonyvek)
        {
            if (erec_UgyUgyiratdarabok != null && erec_IraIktatoKonyvek != null)
            {
                string foszamStr = erec_UgyUgyiratdarabok.Foszam;
                if (foszamStr == "0")
                {
                    // munkaanyagn�l ??-et jelen�t�nk meg:
                    foszamStr = "??";
                }


                if (erec_IraIktatoKonyvek.KozpontiIktatasJelzo == "1")
                {
                    // K�zponti iktat�s jelz�:
                    //return foszamStr + " /" + erec_IraIktatoKonyvek.Ev + (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.MegkulJelzes) ? " /" + erec_IraIktatoKonyvek.MegkulJelzes : "");
                    return GetFoszam_Azonosito(foszamStr, erec_IraIktatoKonyvek.Ev, erec_IraIktatoKonyvek.MegkulJelzes);
                }
                else
                {
                    //return erec_IraIktatoKonyvek.Iktatohely + " /" + foszamStr + " /" + erec_IraIktatoKonyvek.Ev + (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.MegkulJelzes) ? " /" + erec_IraIktatoKonyvek.MegkulJelzes : "");
                    return erec_IraIktatoKonyvek.Iktatohely + " /" + GetFoszam_Azonosito(foszamStr, erec_IraIktatoKonyvek.Ev, erec_IraIktatoKonyvek.MegkulJelzes);
                }


            }
            else
            {
                return "";
            }
        }

    }
}
