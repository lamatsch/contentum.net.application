﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Web;
using System.Reflection;
using System.Text.RegularExpressions;
using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using System.Linq;
using System.Data;


namespace Contentum.eRecord.BaseUtility
{
    [Serializable]
    public class Ugyiratok
    {
        [Serializable]
        public class Statusz
        {
            private string id = "";
            public string Id
            {
                get { return id; }
            }

            private String allapot = "";
            public String Allapot
            {
                get { return allapot; }
                set { allapot = value; }
            }

            private String tovabbitasAlattiAllapot;
            public String TovabbitasAlattiAllapot
            {
                get { return tovabbitasAlattiAllapot; }
            }


            private String felhCsopId_Orzo;
            public String FelhCsopId_Orzo
            {
                get { return felhCsopId_Orzo; }
            }

            private String csopId_Felelos;
            public String CsopId_Felelos
            {
                get { return csopId_Felelos; }
            }

            private string kovetkezo_Felelos_Id;
            public string Kovetkezo_Felelos_Id
            {
                get { return kovetkezo_Felelos_Id; }
            }


            private String csopId_Ugyintezo;
            public String CsopId_Ugyintezo
            {
                get { return csopId_Ugyintezo; }
            }

            private string regirendszerIktatoszam;
            public string RegirendszerIktatoszam
            {
                get { return regirendszerIktatoszam; }
            }

            private string lezarasDat;
            public string LezarasDat
            {
                get { return lezarasDat; }
            }

            private string iraIktatokonyv_Id;
            public string IraIktatokonyv_Id
            {
                get { return iraIktatokonyv_Id; }
            }

            private string foszam = null;
            public bool Munkaanyag
            {
                get
                {
                    if (String.IsNullOrEmpty(foszam) || foszam == "0")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            private string jelleg = String.Empty;

            public string Jelleg
            {
                get { return jelleg; }
                set { jelleg = value; }
            }

            public bool IsElektronikus
            {
                get
                {
                    return this.jelleg == KodTarak.UGYIRAT_JELLEG.Elektronikus;
                }
            }

            // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
            // ITSZ ellenörzés lezáráskor (Státusz bõvítés)
            private string iraIrattariTetel_Id;
            public string IraIrattariTetel_Id
            {
                get { return iraIrattariTetel_Id; }
            }

            public Statusz()
            {
            }

            public Statusz(string _Id, string _Allapot)
            {
                this.id = _Id;
                this.Allapot = _Allapot;
            }

            public Statusz(string _Id, string _Allapot, string _FelhCsopId_Orzo, string _TovabbitasAlattiAllapot, string _CsopId_Felelos,
             string _Kovetkezo_Felelos_Id, string _CsopId_Ugyintezo, string _RegirendszerIktatoszam, string _LezarasDat, string _IraIktatokonyv_Id, string _foszam,
                string _jelleg, string _IraIrattariTetel_Id)
            {
                this.id = _Id;
                this.allapot = _Allapot;
                this.felhCsopId_Orzo = _FelhCsopId_Orzo;
                this.tovabbitasAlattiAllapot = _TovabbitasAlattiAllapot;
                this.csopId_Felelos = _CsopId_Felelos;
                this.kovetkezo_Felelos_Id = _Kovetkezo_Felelos_Id;
                this.csopId_Ugyintezo = _CsopId_Ugyintezo;
                this.regirendszerIktatoszam = _RegirendszerIktatoszam;
                this.lezarasDat = _LezarasDat;
                this.iraIktatokonyv_Id = _IraIktatokonyv_Id;
                this.foszam = _foszam;
                this.jelleg = _jelleg;
                // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                // ITSZ ellenörzés lezáráskor (Státusz bõvítés)
                this.iraIrattariTetel_Id = _IraIrattariTetel_Id;
            }

            public enum ColumnTypes
            {
                Allapot,
                TovabbitasAlattiAllapot,
                FelhCsopId_Orzo,
                CsopId_Felelos,
                SkontrobaHelyezesJovahagyo,
                IrattarozasJovahagyo,
                LezarasDat,
                IraIktatokonyv_Id,
                Munkaanyag,
                Jelleg,
                ElintezesJovahagyo,
                IraIrattariTetel_Id
            }

            public ErrorDetails CreateErrorDetail(string message)
            {
                return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, this.Id, String.Empty, String.Empty);
            }

            public ErrorDetails CreateErrorDetail(string message, Statusz.ColumnTypes columnType)
            {
                string str_columnType = "";
                string str_columnValue = "";
                switch (columnType)
                {
                    case ColumnTypes.Allapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Allapot;
                        str_columnValue = this.Allapot;
                        break;
                    case ColumnTypes.CsopId_Felelos:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos;
                        str_columnValue = this.CsopId_Felelos;
                        break;
                    case ColumnTypes.FelhCsopId_Orzo:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo;
                        str_columnValue = this.FelhCsopId_Orzo;
                        break;
                    case ColumnTypes.TovabbitasAlattiAllapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.TovabbitasAlattAllapot;
                        str_columnValue = this.TovabbitasAlattiAllapot;
                        break;
                    case ColumnTypes.SkontrobaHelyezesJovahagyo:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.SkontrobaHelyezesJovahagyo;
                        str_columnValue = this.Kovetkezo_Felelos_Id;
                        break;
                    case ColumnTypes.IrattarozasJovahagyo:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.IrattarozasJovahagyo;
                        str_columnValue = this.Kovetkezo_Felelos_Id;
                        break;
                    case ColumnTypes.LezarasDat:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.LezarasDat;
                        str_columnValue = this.LezarasDat;
                        break;
                    case ColumnTypes.IraIktatokonyv_Id:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.IraIktatokonyv_Id;
                        str_columnValue = this.IraIktatokonyv_Id;
                        break;
                    case ColumnTypes.Jelleg:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Jelleg;
                        str_columnValue = this.Jelleg;
                        break;
                    case ColumnTypes.ElintezesJovahagyo:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.ElintezesJovahagyo;
                        str_columnValue = this.Kovetkezo_Felelos_Id;
                        break;
                    // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                    // ITSZ ellenörzés lezáráskor (Státusz bõvítés)
                    case ColumnTypes.IraIrattariTetel_Id:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.IraIrattariTetel_Id;
                        str_columnValue = this.IraIrattariTetel_Id;
                        break;
                }

                return new ErrorDetails(message, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, this.Id, str_columnType, str_columnValue);
            }

            public ErrorDetails CreateErrorDetail(int code, Statusz.ColumnTypes columnType)
            {
                string str_columnType = "";
                string str_columnValue = "";
                switch (columnType)
                {
                    case ColumnTypes.Allapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Allapot;
                        str_columnValue = this.Allapot;
                        break;
                    case ColumnTypes.CsopId_Felelos:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.Csoport_Id_Felelos;
                        str_columnValue = this.CsopId_Felelos;
                        break;
                    case ColumnTypes.FelhCsopId_Orzo:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo;
                        str_columnValue = this.FelhCsopId_Orzo;
                        break;
                    case ColumnTypes.TovabbitasAlattiAllapot:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.TovabbitasAlattAllapot;
                        str_columnValue = this.TovabbitasAlattiAllapot;
                        break;

                    case ColumnTypes.LezarasDat:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.LezarasDat;
                        str_columnValue = this.LezarasDat;
                        break;
                    case ColumnTypes.IraIktatokonyv_Id:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.IraIktatokonyv_Id;
                        str_columnValue = this.IraIktatokonyv_Id;
                        break;
                    // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                    // ITSZ ellenörzés lezáráskor (Státusz bõvítés)
                    case ColumnTypes.IraIrattariTetel_Id:
                        str_columnType = Constants.ErrorDetails.ColumnTypes.IraIrattariTetel_Id;
                        str_columnValue = this.IraIrattariTetel_Id;
                        break;
                }

                return new ErrorDetails(code, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, this.Id, str_columnType, str_columnValue);
            }

        }

        [Serializable]
        public class FilterObject
        {
            public enum SpecialFilterType { Letrehozas, Kritikus, Lejart };
            private SpecialFilterType _SpecialFilter;
            public SpecialFilterType SpecialFilter
            {
                get { return _SpecialFilter; }
                set { _SpecialFilter = value; }
            }

            private List<string> _Allapot;
            public void AddAllapot(string Allapot)
            {
                if (_Allapot == null)
                {
                    _Allapot = new List<string>();
                }

                if (!_Allapot.Contains(Allapot))
                {
                    _Allapot.Add(Allapot);
                }
            }

            public void RemoveAllapot(string Allapot)
            {
                if (_Allapot != null)
                {
                    while (_Allapot.IndexOf(Allapot) >= 0)
                    {
                        _Allapot.RemoveAt(_Allapot.IndexOf(Allapot));
                    }
                }
            }

            public void ClearAllapot()
            {
                if (_Allapot != null)
                {
                    _Allapot.Clear();
                }
                else
                {
                    _Allapot = new List<string>();
                }
            }

            public string[] GetAllapotArray()
            {
                if (_Allapot != null)
                {
                    return _Allapot.ToArray();
                }
                else return null;
            }

            private List<string> _ExcludeAllapot;
            public void AddExcludeAllapot(string ExcludeAllapot)
            {
                if (_ExcludeAllapot == null)
                {
                    _ExcludeAllapot = new List<string>();
                }

                if (!_ExcludeAllapot.Contains(ExcludeAllapot))
                {
                    _ExcludeAllapot.Add(ExcludeAllapot);
                }
            }

            public void RemoveExcludeAllapot(string ExcludeAllapot)
            {
                if (_ExcludeAllapot != null)
                {
                    while (_ExcludeAllapot.IndexOf(ExcludeAllapot) >= 0)
                    {
                        _ExcludeAllapot.RemoveAt(_ExcludeAllapot.IndexOf(ExcludeAllapot));
                    }
                }
            }

            public void ClearExcludeAllapot()
            {
                if (_ExcludeAllapot != null)
                {
                    _ExcludeAllapot.Clear();
                }
                else
                {
                    _ExcludeAllapot = new List<string>();
                }
            }

            public string[] GetExcludeAllapotArray()
            {
                if (_ExcludeAllapot != null)
                {
                    return _ExcludeAllapot.ToArray();
                }
                else return null;
            }

            // ha true, az állapotokat a TovabbitasAlatt mezõben is figyeljük, ha false, akkor csak az Allapot mezõben
            private bool _isTovabbitasAlattAllapotIncluded = true;
            public bool isTovabbitasAlattAllapotIncluded
            {
                get { return _isTovabbitasAlattAllapotIncluded; }
                set { _isTovabbitasAlattAllapotIncluded = value; }
            }

            private string _MinDate;
            public string MinDate
            {
                get { return _MinDate; }
                set { _MinDate = value; }
            }

            public void ClearMinDate()
            {
                _MinDate = null;
            }

            private string _MaxDate;
            public string MaxDate
            {
                get { return _MaxDate; }
                set { _MaxDate = value; }
            }

            private string _AlSzervezetId;

            public string AlSzervezetId
            {
                get { return _AlSzervezetId; }
                set { _AlSzervezetId = value; }
            }

            public void ClearMaxDate()
            {
                _MaxDate = null;
            }

            private string _ReadableWhere;
            public string ReadableWhere
            {
                get { return _ReadableWhere; }
                set { _ReadableWhere = (value ?? ""); }
            }

            // BUG_8847
            private Boolean _Aktiv;

            public Boolean Aktiv
            {
                get { return _Aktiv; }
                set { _Aktiv = value; }
            }

            public FilterObject()
            {
                this._Allapot = new List<string>();
                this._MinDate = null;
                this._MaxDate = null;
                this.SpecialFilter = SpecialFilterType.Letrehozas;
                this._ReadableWhere = "";
                // BUG_8847
                this.Aktiv = true;
            }

            public void Clear()
            {
                if (this._Allapot != null)
                {
                    this._Allapot.Clear();
                }
                this._MinDate = null;
                this._MaxDate = null;
                this._SpecialFilter = SpecialFilterType.Letrehozas;
                this._ReadableWhere = "";
                this._isTovabbitasAlattAllapotIncluded = true;
                this._AlSzervezetId = null;
                // BUG_8847
                this.Aktiv = true;
            }

            private const string queryPrefix = "FilterBy";

            public string QsSerialize()
            {
                Logger.Debug("QueryString sorosítás START");
                List<string> queryStringItems = new List<string>();
                string query = String.Empty;
                if (this._Allapot != null && this._Allapot.Count > 0)
                {
                    queryStringItems.Add(queryPrefix + "Allapot=" + System.Web.HttpUtility.UrlEncode(String.Join(";", this._Allapot.ToArray())));
                }
                if (this._ExcludeAllapot != null && this._ExcludeAllapot.Count > 0)
                {
                    queryStringItems.Add(queryPrefix + "ExcludeAllapot=" + System.Web.HttpUtility.UrlEncode(String.Join(";", this._ExcludeAllapot.ToArray())));
                }

                queryStringItems.Add(queryPrefix + "TovabbitasAlatt=" + (this._isTovabbitasAlattAllapotIncluded ? "1" : "0"));
                queryStringItems.Add(queryPrefix + "SpecialFilter=" + System.Web.HttpUtility.UrlEncode(this._SpecialFilter.ToString()));

                if (!String.IsNullOrEmpty(this._MinDate))
                {
                    queryStringItems.Add(queryPrefix + "MinDate=" + System.Web.HttpUtility.UrlEncode(this._MinDate));
                }

                if (!String.IsNullOrEmpty(this._MaxDate))
                {
                    queryStringItems.Add(queryPrefix + "MaxDate=" + System.Web.HttpUtility.UrlEncode(this._MaxDate));
                }


                if (!String.IsNullOrEmpty(this._AlSzervezetId))
                {
                    queryStringItems.Add(String.Format("{0}AlSzervezetId={1}", queryPrefix, HttpUtility.UrlEncode(this._AlSzervezetId)));
                }

                // BUG_8847
                queryStringItems.Add(String.Format("{0}CsakAktiv={1}", queryPrefix, this._Aktiv.ToString()));

                if (!String.IsNullOrEmpty(this._ReadableWhere))
                {
                    queryStringItems.Add(queryPrefix + "ReadableWhere=" + System.Web.HttpUtility.UrlEncode(this._ReadableWhere));
                }

                if (queryStringItems.Count > 0)
                {
                    query = String.Join("&", queryStringItems.ToArray());
                }

                Logger.Debug("QueryString: " + query);
                Logger.Debug("QueryString sorosítás END");

                return query;
            }

            public void QsDeserialize(System.Collections.Specialized.NameValueCollection queryString)
            {
                Logger.Debug("QueryString desorosítás START");
                Logger.Debug("QueryString: " + queryString.ToString());

                string allapotok = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "Allapot"));
                string excludeallapotok = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "ExcludeAllapot"));
                string tovabbitasalatt = queryString.Get(queryPrefix + "TovabbitasAlatt");
                string specialFilter = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "SpecialFilter"));
                string minDate = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "MinDate"));
                string maxDate = System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "MaxDate"));
                this._AlSzervezetId = HttpUtility.UrlDecode(queryString.Get(queryPrefix + "AlSzervezetId"));
                string readableWhere = (System.Web.HttpUtility.UrlDecode(queryString.Get(queryPrefix + "ReadableWhere")) ?? "");

                // BUG_8847
                this._Aktiv = queryString.Get(queryPrefix + "CsakAktiv") == "True";

                if (!String.IsNullOrEmpty(allapotok))
                {
                    char[] separator = { ';' };
                    string[] allapotokArray = allapotok.Split(separator);
                    if (this._Allapot == null)
                    {
                        this._Allapot = new List<string>();
                    }
                    else
                    {
                        this._Allapot.Clear();
                    }

                    foreach (string allapot in allapotokArray)
                    {
                        this._Allapot.Add(allapot);
                    }
                }
                if (!String.IsNullOrEmpty(excludeallapotok))
                {
                    char[] separator = { ';' };
                    string[] excludeallapotokArray = excludeallapotok.Split(separator);
                    if (this._ExcludeAllapot == null)
                    {
                        this._ExcludeAllapot = new List<string>();
                    }
                    else
                    {
                        this._ExcludeAllapot.Clear();
                    }

                    foreach (string excludeallapot in excludeallapotokArray)
                    {
                        this._ExcludeAllapot.Add(excludeallapot);
                    }
                }
                if (tovabbitasalatt == "0")
                {
                    this._isTovabbitasAlattAllapotIncluded = false;
                }
                else
                {
                    this._isTovabbitasAlattAllapotIncluded = true;
                }

                if (specialFilter == SpecialFilterType.Kritikus.ToString())
                {
                    this._SpecialFilter = SpecialFilterType.Kritikus;
                }
                else if (specialFilter == SpecialFilterType.Lejart.ToString())
                {
                    this._SpecialFilter = SpecialFilterType.Lejart;
                }
                else
                {
                    this._SpecialFilter = SpecialFilterType.Letrehozas;
                }

                this._MinDate = minDate;
                this._MaxDate = maxDate;
                this._ReadableWhere = (readableWhere ?? "");

                Logger.Debug("QueryString desorosítás END");
                Logger.Debug(String.Format("Eredmény: Allapot: {0}, ExcludeAllapot: {1}, TovabbitasAlatt: {2}", (this._Allapot == null ? "null" : this._Allapot.ToString()), (this._ExcludeAllapot == null ? "null" : this._ExcludeAllapot.ToString()), this._isTovabbitasAlattAllapotIncluded.ToString()));
                Logger.Debug(String.Format("MinDate: {0}, MaxDate: {1}, SpecialFilter: {2}", (this._MinDate == null ? "null" : this._MinDate), (this._MaxDate == null ? "null" : this._MaxDate), this._SpecialFilter.ToString()));
                Logger.Debug(String.Format("ReadableWhere: {0}", this._ReadableWhere));
                // BUG_8847
                Logger.Debug(String.Format("Aktiv: {0}", this._Aktiv));
            }

            public void SetSearchObject(ExecParam execParam, ref EREC_UgyUgyiratokSearch searchObject)
            {

                if (_Allapot != null && _Allapot.Count > 0)
                {
                    string[] allapotArray = GetAllapotArray();
                    string allapotok = "'" + String.Join("','", allapotArray) + "'";

                    if (this._isTovabbitasAlattAllapotIncluded == true)
                    {
                        // A TovabbitasAlattAllapot mezõben is kell keresni: (VAGY kapcsolat)

                        searchObject.Allapot.Value = allapotok;
                        searchObject.Allapot.Operator = Query.Operators.inner;
                        searchObject.Allapot.Group = "531";
                        searchObject.Allapot.GroupOperator = Query.Operators.or;

                        searchObject.TovabbitasAlattAllapot.Value = allapotok;
                        searchObject.TovabbitasAlattAllapot.Operator = Query.Operators.inner;
                        searchObject.TovabbitasAlattAllapot.Group = "531";
                        searchObject.TovabbitasAlattAllapot.GroupOperator = Query.Operators.or;
                    }
                    else
                    {

                        searchObject.Allapot.Value = allapotok;
                        searchObject.Allapot.Operator = Query.Operators.equals;
                        searchObject.Allapot.Group = "0";
                        searchObject.Allapot.GroupOperator = Query.Operators.and;

                        searchObject.TovabbitasAlattAllapot.Value = "";
                        searchObject.TovabbitasAlattAllapot.Operator = "";
                        searchObject.TovabbitasAlattAllapot.Group = "0";
                        searchObject.TovabbitasAlattAllapot.GroupOperator = Query.Operators.and;
                    }
                }
                //else
                //{
                //    // default szûrés: nem hozzuk az iktatásra elõkészített ügyiratokat
                //    searchObject.Allapot.Value = KodTarak.UGYIRAT_ALLAPOT.Iktatasraelokeszitett;
                //    searchObject.Allapot.Operator = Query.Operators.notequals;
                //}
                else
                {
                    searchObject.Allapot.Value = "";
                    searchObject.Allapot.Operator = "";
                    searchObject.Allapot.Group = "0";
                    searchObject.Allapot.GroupOperator = Query.Operators.and;

                    searchObject.TovabbitasAlattAllapot.Value = "";
                    searchObject.TovabbitasAlattAllapot.Operator = "";
                    searchObject.TovabbitasAlattAllapot.Group = "0";
                    searchObject.TovabbitasAlattAllapot.GroupOperator = Query.Operators.and;
                }

                // a kizárandó állapotokat a manuális filter mezõben adjuk meg
                if (_ExcludeAllapot != null && _ExcludeAllapot.Count > 0)
                {
                    string[] excludeallapotArray = GetExcludeAllapotArray();
                    string excludeallapotok = "'" + String.Join("','", excludeallapotArray) + "'";

                    searchObject.Manual_Allapot_Filter.Value = excludeallapotok;
                    searchObject.Manual_Allapot_Filter.Operator = Query.Operators.notinner;
                }

                searchObject.IsTovabbitasAlattAllapotIncluded = this._isTovabbitasAlattAllapotIncluded;

                if (!String.IsNullOrEmpty(this._ReadableWhere))
                {
                    searchObject.ReadableWhere = this._ReadableWhere;
                }

                if (this._SpecialFilter == SpecialFilterType.Kritikus || this._SpecialFilter == SpecialFilterType.Lejart)
                {
                    if (!String.IsNullOrEmpty(this._MinDate) && this._MinDate == this._MaxDate)
                    {
                        searchObject.Hatarido.Value = this._MinDate;
                        searchObject.Hatarido.Operator = Query.Operators.equals;
                    }
                    else if (!String.IsNullOrEmpty(this._MinDate) && !String.IsNullOrEmpty(this._MaxDate))
                    {
                        searchObject.Hatarido.Value = this._MinDate;
                        searchObject.Hatarido.ValueTo = this._MaxDate;
                        searchObject.Hatarido.Operator = Query.Operators.between;
                    }
                    else if (!String.IsNullOrEmpty(this._MinDate))
                    {
                        searchObject.Hatarido.Value = this._MinDate;
                        searchObject.Hatarido.Operator = Query.Operators.greaterorequal;
                    }
                    else if (!String.IsNullOrEmpty(this._MaxDate))
                    {
                        searchObject.Hatarido.Value = this._MaxDate;
                        searchObject.Hatarido.Operator = Query.Operators.lessorequal;
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(this._MinDate) && this._MinDate == this._MaxDate)
                    {
                        searchObject.Manual_LetrehozasIdo.Value = this._MinDate;
                        searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.equals;
                    }
                    else if (!String.IsNullOrEmpty(this._MinDate) && !String.IsNullOrEmpty(this._MaxDate))
                    {
                        searchObject.Manual_LetrehozasIdo.Value = this._MinDate;
                        searchObject.Manual_LetrehozasIdo.ValueTo = this._MaxDate;
                        searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.between;
                    }
                    else if (!String.IsNullOrEmpty(this._MinDate))
                    {
                        searchObject.Manual_LetrehozasIdo.Value = this._MinDate;
                        searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.greaterorequal;
                    }
                    else if (!String.IsNullOrEmpty(this._MaxDate))
                    {
                        searchObject.Manual_LetrehozasIdo.Value = this._MaxDate;
                        searchObject.Manual_LetrehozasIdo.Operator = Query.Operators.lessorequal;
                    }
                }

                if (!String.IsNullOrEmpty(this._AlSzervezetId))
                {
                    // BUG_8847
                    //searchObject.AlSzervezetId = this._AlSzervezetId;
                    searchObject.Csoport_Id_Ugyfelelos.Value = this.AlSzervezetId;
                    searchObject.Csoport_Id_Ugyfelelos.Operator = Query.Operators.equals;
                }

                // BUG_8847
                searchObject.CsakAktivIrat = this._Aktiv;

            }

        }

        protected static class ErrorDetailMessages
        {
            public const string UgyiratAllapotNemMegfelelo = "Az ügyirat állapota nem megfelelõ.";
            public const string UgyiratnakNemOnAzOrzoje = "Az ügyiratnak nem Ön az õrzõje.";
            public const string UgyiratOrzojenekNemtagja = "Az ügyirat nem Önnél vagy a szervezeténél található.";
            public const string UgyiratNemUgyintezesAlattAllapotu = "Az ügyirat állapota nem ügyintézés alatti.";
            public const string UgyiratNemLezartAllapotu = "Az ügyirat állapota nem lezárt.";
            public const string UgyiratNemSzereltAllapotu = "Az ügyirat nem szerelt állapotú.";
            public const string NemSkontroJovahagyoja = "Az skontróba helyezést Ön nem hagyhatja jóvá.\\n";
            public const string NemIrattarozasJovahagyoja = "Az ügyirat irattárba küldését nem Ön hagyhatja jóvá.";
            public const string SzerelendoUgyiratnakNemOnAzOrzoje = "A szerelendõ ügyiratnak nem Ön az õrzõje.";
            public const string UgyiratMunkaanyag = "Az ügyirat még munkaanyag.";
            public const string NemSzervezetAtmenetiIrattara = "Az ügyiratot nem adhatja ki, mert Ön jelenlegi szervezetében nem jogosult az átmeneti irattár kezelésére.";
            public const string UgyiratNemElektronikus = "Az ügyirat nem elektronikus.";
            public const string UgyiratElektronikus = "Az ügyirat elektronikus.";
            public const string NemElintezesJovahagyoja = "Az ügyirat elintézetté nyilvánítását nem Ön hagyhatja jóvá.";
            public const string VanOlyanAlszamNemElintezett = "Az ügyiratban van olyan irat, amely nem elintézett.";
            public const string UgyiratIrattarbanVan = "Az ügyirat irattárban van.";
            public const string UgyiratSakkoraAllapotNemMegfelelo = "Az ügyirat sakkóra állapota nem megfelelõ.";
            public const string UgyiratKiadmanyozandoIrat = "Az ügyirat nem minden irata kiadmányozott.";
        }

        public static Statusz GetAllapotById(String Id, Page ParentPage, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
            return GetAllapotById(Id, execParam, EErrorPanel);
        }
        public static Statusz GetAllapotById(String Id, ExecParam execParam, Contentum.eUIControls.eErrorPanel EErrorPanel)
        {
            EREC_UgyUgyiratok erec_UgyUgyiratok = null;

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            execParam.Record_Id = Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_UgyUgyiratok = (EREC_UgyUgyiratok)result.Record;
                return GetAllapotByBusinessDocument(erec_UgyUgyiratok);
            }
            else
            {
                if (EErrorPanel != null)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                }
                return null;
            }
        }

        // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
        // ITSZ ellenörzés lezáráskor (Státusz bõvítés)
        public static Statusz GetAllapotByBusinessDocument(EREC_UgyUgyiratok erec_UgyUgyiratok)
        {
            Statusz statusz = new Statusz(erec_UgyUgyiratok.Id, erec_UgyUgyiratok.Allapot, erec_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo
                , erec_UgyUgyiratok.TovabbitasAlattAllapot, erec_UgyUgyiratok.Csoport_Id_Felelos, erec_UgyUgyiratok.Kovetkezo_Felelos_Id
                , erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez, erec_UgyUgyiratok.RegirendszerIktatoszam, erec_UgyUgyiratok.LezarasDat
                , erec_UgyUgyiratok.IraIktatokonyv_Id, erec_UgyUgyiratok.Foszam, erec_UgyUgyiratok.Jelleg, erec_UgyUgyiratok.IraIrattariTetel_Id);

            return statusz;
        }

        // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
        // ITSZ ellenörzés lezáráskor (Státusz bõvítés)
        public static Statusz GetAllapotByDataRow(DataRow row)
        {
            string cNameRegiRendszerIktatoszam = "RegirendszerIktatoszam";
            if (row.Table.Columns.Contains("RegirendszerIktatoszamEredeti"))
                cNameRegiRendszerIktatoszam = "RegirendszerIktatoszamEredeti";

            Statusz statusz = new Statusz(
                        row["Id"].ToString(),
                        row["Allapot"].ToString(),
                        row["FelhasznaloCsoport_Id_Orzo"].ToString(),
                        row["TovabbitasAlattAllapot"].ToString(),
                        row["Csoport_Id_Felelos"].ToString(),
                        row["Kovetkezo_Felelos_Id"].ToString(),
                        row["FelhasznaloCsoport_Id_Ugyintez"].ToString(),
                        row[cNameRegiRendszerIktatoszam].ToString(),
                        row["LezarasDat"].ToString(),
                        row["IraIktatokonyv_Id"].ToString(),
                        row["Foszam"].ToString(),
                        row["Jelleg"].ToString(),
                        row["IraIrattariTetel_Id"].ToString());

            return statusz;
        }

        /// <summary>
        /// Több ügyirat állapotát adja vissza egyszerre DataSet-bõl
        /// </summary>
        /// <param name="ep">execParam</param>
        /// <param name="ds">DataSet</param>
        /// <returns>HashTable Statusz elemekkel. Hivatkozni rájuk: statusz["Id"]-val lehet.</returns>
        public static Hashtable GetAllapotByDataSet(ExecParam ep, System.Data.DataSet ds)
        {
            if (ds.Tables.Count == 0) return new Hashtable(0);
            if (ds.Tables[0].Rows.Count == 0) return new Hashtable(0);
            Hashtable statusz = new Hashtable(ds.Tables[0].Rows.Count);

            try
            {
                foreach (System.Data.DataRow r in ds.Tables[0].Rows)
                {
                    statusz[r["Id"].ToString()] = GetAllapotByDataRow(r);
                }
            }
            catch
            {
                return new Hashtable(0);
            }

            return statusz;
        }

        public static bool Modosithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            #region Központi iktató-e a szervezet?
            // Központi iktató akkor is módosíthat, ha nem nála van az ügyirat
            if (execParam.FelhasznaloSzervezet_Id == KodTarak.SPEC_SZERVEK.GetKozpontiIktato(execParam).Obj_Id)
            {
                return ModosithatoAllapotu(statusz, out errorDetail);
            }
            #endregion

            if (statusz.FelhCsopId_Orzo != FelhasznaloProfil.GetFelhasznaloCsoport(execParam))
            {
                // ha nem a felhasználó az õrzõ, nem módosítható
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }
            else
            {
                return ModosithatoAllapotu(statusz, out errorDetail);
            }
        }

        //public static bool ModosithatoAllapotu(Statusz statusz)
        //{ 
        //    ErrorDetails errorDetail;
        //    return ModosithatoAllapotu(statusz, out errorDetail);
        //}

        public static bool ModosithatoAllapotu(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                    //case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
                    return true;
                case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                    {
                        switch (statusz.TovabbitasAlattiAllapot)
                        {
                            case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                            case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                            case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                            case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                                return true;
                            default:
                                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.TovabbitasAlattiAllapot);
                                return false;
                        }
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }


        public static bool IrattarbolModosithato(Statusz statusz, ExecParam execParam)
        {
            ErrorDetails errorDetail;
            return IrattarbolModosithato(statusz, execParam, out errorDetail);
        }

        public static bool IrattarbolModosithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            //if (statusz.FelhCsopId_Orzo != FelhasznaloProfil.GetFelhasznaloCsoport(execParam))
            //{
            //// ha nem a felhasználó az õrzõ, nem módosítható
            //return false;
            //}

            if (!Csoportok.IsMember(execParam.Felhasznalo_Id, statusz.FelhCsopId_Orzo))
            {
                //a felhasználó nem tagja az örzõ csoportnak
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratOrzojenekNemtagja, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool Nyitott(Statusz statusz)
        {
            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                case KodTarak.UGYIRAT_ALLAPOT.Foglalt:
                    return true;
                case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                    switch (statusz.TovabbitasAlattiAllapot)
                    {
                        case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                        case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                        case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                        case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                        case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                            return true;
                        default:
                            return false;
                    }
                default:
                    return false;
            }
        }

        public static bool Sztornozott(Statusz statusz)
        {
            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Sztornozott:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Az adott állapotban lehet-e iktatni
        /// Az eOffice Addinokból való hívások miatt (GetKozpontiIktato kozvetlenul nem elerheto)
        /// külön metódus adja vissza az elfogadható állapotokat
        /// </summary>
        public static bool LehetAlszamraIktatni(ExecParam execParam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Munkaanyagba nem lehet iktatni:
            if (statusz.Munkaanyag)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratMunkaanyag, Statusz.ColumnTypes.Munkaanyag);
                return false;
            }

            //switch (statusz.Allapot)
            //{
            //    case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
            //    case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
            //    case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
            //    case KodTarak.UGYIRAT_ALLAPOT.Elintezett:                
            //    case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
            //    // 2008.03.17. FPH kérésére:
            //    case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
            //    case KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa:
            //    case KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert:
            //    case KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott:
            //    case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
            //    case KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert:
            //    case KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo:
            //    case KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott:
            //    case KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott:
            //        return true;
            //    //CR 2019 továbbítás alatti állapotú ügyiratba ne lehessen iktatni, kivéve KÜK
            //    case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
            //        #region Központi iktató-e a szervezet?
            //        //Ha nem központi iktató nem iktathat bele
            //        if (execParam.FelhasznaloSzervezet_Id != KodTarak.SPEC_SZERVEK.GetKozpontiIktato(execParam).Obj_Id)
            //        {
            //            goto default;
            //        }
            //        #endregion
            //        switch (statusz.TovabbitasAlattiAllapot)
            //        {
            //            case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
            //            case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
            //            case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
            //            case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
            //            case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
            //            case KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa:
            //            case KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert:
            //            case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
            //            case KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo:
            //                return true;
            //            default:
            //                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.TovabbitasAlattiAllapot);
            //                return false;
            //        }
            //    default:
            //        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
            //        return false;
            //}           

            if (!GetLehetAlszamraIktatniAllapotok().Contains(statusz.Allapot))
            {
                //CR 2019 továbbítás alatti állapotú ügyiratba ne lehessen iktatni, kivéve KÜK
                if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt)
                {
                    #region Központi iktató-e a szervezet?
                    //Ha nem központi iktató nem iktathat bele
                    if (execParam.FelhasznaloSzervezet_Id != KodTarak.SPEC_SZERVEK.GetKozpontiIktato(execParam).Obj_Id)
                    {
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                        return false;
                    }
                    #endregion
                    if (GetLehetAlszamraIktatniTovabbitasAllapotok().Contains(statusz.TovabbitasAlattiAllapot) == false)
                    {
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.TovabbitasAlattiAllapot);
                        return false;
                    }
                }
                else
                {
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
                }
            }

            #region Irattárban lévõbe iktathat-e (BUG#4758)

            /// Ha a státusza szerint irattárban van, akkor egy rendszerparaméter mondja meg, hogy engedélyezve van-e az irattárba iktatás, vagy nem
            /// 
            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                case KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott:
                case KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo:
                case KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert:
                    {
                        bool irattarbaIktathat = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRATTARBA_IKTATAS_ENABLED, true);
                        // Ha nem iktathat irattárban lévõbe, akkor return false:
                        if (!irattarbaIktathat)
                        {
                            errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratIrattarbanVan, Statusz.ColumnTypes.Allapot);
                            return false;
                        }
                    }
                    break;
            }

            #endregion

            return true;
        }

        public static List<string> GetLehetAlszamraIktatniAllapotok()
        {
            List<string> listResult = new List<string>();
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Iktatott);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Szignalt);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Elintezett);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart);
            // 2008.03.17. FPH kérésére);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Skontroban);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert);
            // CR#2580 (2010.12.27): Irattárba küldöttbe nem szabad iktatni
            //listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Felfuggesztett);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Foglalt);
            return listResult;
        }

        public static List<string> GetLehetAlszamraIktatniTovabbitasAllapotok()
        {
            List<string> listResult = new List<string>();
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Iktatott);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Szignalt);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Skontroban);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo);
            listResult.Add(KodTarak.UGYIRAT_ALLAPOT.Felfuggesztett);
            return listResult;
        }

        public static bool LehetMunkapeldanytLetrehozni(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
                case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                    return true;
                case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                    switch (statusz.TovabbitasAlattiAllapot)
                    {
                        case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                        case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                        case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                        case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
                        case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                        case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
                        case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                            return true;
                        default:
                            errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.TovabbitasAlattiAllapot);
                            return false;
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        // Allapot alapján való meghatározás
        public static bool Lezart(Statusz statusz)
        {
            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // LezarasDat alapján történõ meghatározás,
        // állapota lehet pl. irattárba küldött, irattárban õrzött, engedélyezett kikérõn lévõ stb.
        public static bool LezartVoltMar(Statusz statusz)
        {
            if (!String.IsNullOrEmpty(statusz.LezarasDat))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Szignalhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }

        }

        /// <summary>
        /// Ellenõrzi, hogy ügyintézõre szignálható-e az ügyirat
        /// Ha az ügyfelelõs nem a saját szervezet, vagy nem a saját szervezetbeli ember, akkor nem lehet ügyintézõre szignálni
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="csoportIdUgyfelelos"></param>
        /// <param name="sajatSzervezetId"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool UgyintezoreSzignalhato(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyiratok, string sajatSzervezetId, out string errorMessage)
        {
            return UgyintezoreSzignalhato(execParam, erec_UgyUgyiratok.Csoport_Id_Ugyfelelos, sajatSzervezetId, out errorMessage);
        }

        /// <summary>
        /// Ellenõrzi, hogy ügyintézõre szignálható-e az ügyirat
        /// Ha az ügyfelelõs nem a saját szervezet, vagy nem a saját szervezetbeli ember, akkor nem lehet ügyintézõre szignálni
        /// </summary>
        /// <param name="execParam"></param>
        /// <param name="csoportIdUgyfelelos"></param>
        /// <param name="sajatSzervezetId"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool UgyintezoreSzignalhato(ExecParam execParam, DataRow dataRow, string sajatSzervezetId, out string errorMessage)
        {
            return UgyintezoreSzignalhato(execParam, dataRow.IsNull("Csoport_Id_Ugyfelelos") ? null : dataRow["Csoport_Id_Ugyfelelos"].ToString(), sajatSzervezetId, out errorMessage);
        }

        private static bool UgyintezoreSzignalhato(ExecParam execParam, string csoportIdUgyfelelos, string sajatSzervezetId, out string errorMessage)
        {
            errorMessage = null;

            // ha nincs megadva az ügyfelelõs, nem szignálható ügyintézõre:
            if (string.IsNullOrEmpty(csoportIdUgyfelelos))
            {
                errorMessage = ResultError.GetErrorMessageByErrorCode(52108);
                return false;
            }

            // ha az ügyfelelõs a saját szervezet, ok:
            if (csoportIdUgyfelelos.Equals(sajatSzervezetId, StringComparison.OrdinalIgnoreCase))
                return true;

            // felettes szervezethez tartozó csoportok lekérése:
            eAdmin.Service.KRT_CsoportokService service_csoportok = eAdminService.ServiceFactory.GetKRT_CsoportokService();

            // ügyfelelõsre rögtön szûkítünk is:
            KRT_CsoportokSearch search_csoportok = new KRT_CsoportokSearch();
            search_csoportok.Id.Value = csoportIdUgyfelelos;
            search_csoportok.Id.Operator = Query.Operators.equals;

            Result result_subCsoportok = service_csoportok.GetAllSubCsoport(execParam, sajatSzervezetId, false, search_csoportok);
            if (result_subCsoportok.IsError)
            {
                errorMessage = result_subCsoportok.ErrorMessage;
                return false;
            }

            // ha az ügyfelelõs benne van az eredményhalmazban (ami elvileg egy találat, vagy semmi), ok:
            foreach (DataRow row in result_subCsoportok.Ds.Tables[0].Rows)
            {
                string csoportId = row["Id"].ToString();
                if (csoportIdUgyfelelos.Equals(csoportId, StringComparison.OrdinalIgnoreCase))
                    return true;
            }

            errorMessage = ResultError.GetErrorMessageByErrorCode(52613);
            return false;
        }

        /// <summary>
        /// Állapota alapján átvehetõ-e ügyintézésre, illetve 
        /// a felhasználó tagja-e a felelõs csoportnak
        /// </summary>        
        public static bool AtvehetoUgyintezesre(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (statusz.TovabbitasAlattiAllapot == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo)
            {
                EREC_IrattariKikeroService irattarService = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                Result irattarResult = irattarService.GetByUgyiratId(execParam);

                if (string.IsNullOrEmpty(irattarResult.ErrorCode))
                {
                    if ((irattarResult.Record as EREC_IrattariKikero).FelhasznalasiCel == "U")
                        return true;
                    else
                        return false;
                }
            }

            // BUG_14775 DUP - Elektronikus irattárban lévő iratok további kezelése
            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott && statusz.IsElektronikus)
            {
                // XXX

                #region Az ügyintzéző kiveheti az irattárból

                // A felhasználó az ügyintéző
                if (Csoportok.GetFelhasznaloSajatCsoportId(execParam) == statusz.CsopId_Ugyintezo)
                {
                    return true;
                }

                #endregion

                #region Az ügyintzéző vezetője kiveheti az irattárból
                
                // Akkor vehető át irattárból, ha elektronikus és az ügyirat ügyintézője, a szervezet vezetője vagy asszisztense

                // A felhasználó által vezetett vagy asszisztált csoportok lekérése
                ExecParam xpm = execParam.Clone();
                Contentum.eAdmin.Service.KRT_CsoportTagokService svc = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch sch = new KRT_CsoportTagokSearch();

                sch.Csoport_Id_Jogalany.Value = execParam.Felhasznalo_Id;
                sch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                sch.Tipus.Value = "3,4"; // vezeto vagy asszisztens
                sch.Tipus.Operator = Query.Operators.inner;

                Result res = svc.GetAll(xpm, sch);

                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    throw new ResultException(res);
                }

                List<string> vezetettcsoportok = new List<string>();

                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    vezetettcsoportok.Add(row["Csoport_Id"].ToString());
                }

                // Az ügyirat ügyintézőjének felettes csoportjainak lekérése
                xpm = execParam.Clone();
                sch = new KRT_CsoportTagokSearch();

                sch.Csoport_Id_Jogalany.Value = statusz.CsopId_Ugyintezo;
                sch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                res = svc.GetAll(xpm, sch);

                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    throw new ResultException(res);
                }

                List<string> felettescsoportok = new List<string>();

                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    felettescsoportok.Add(row["Csoport_Id"].ToString());
                }

                foreach (string vezetettcsoport in vezetettcsoportok)
                {
                    if (felettescsoportok.Contains(vezetettcsoport))
                    {
                        return true; // megtaláltuk, a felhasználó az ügyintéző vezetője
                    }
                }

                #endregion

                #region Adminisztrátor csoport kiveheti, ha már nem érvényes az ügyintéző vagy a szervezete

                Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok =
                    Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

                bool vanaktivszervezet = true;

                // megvizsgáljuk, hogy létezik-e az ügyintéző felettes szervezete, ha nem akkor az adminisztrátor csoportban lévő felhasználókkal kivehető az irattárból                
                if (felettescsoportok.Count > 0)
                {
                    ExecParam execParam_GetAllFelettesCsoportok = execParam.Clone();
                    KRT_CsoportokSearch search_GetAllFelettesCsoportok = new KRT_CsoportokSearch();

                    search_GetAllFelettesCsoportok.Id.Value = Search.GetSqlInnerString(felettescsoportok.ToArray());
                    search_GetAllFelettesCsoportok.Id.Operator = Query.Operators.inner;

                    Result result_GetAllFelettesCsoportok = service_csoportok.GetAll(execParam_GetAllFelettesCsoportok, search_GetAllFelettesCsoportok);

                    if (string.IsNullOrEmpty(result_GetAllFelettesCsoportok.ErrorCode) && result_GetAllFelettesCsoportok.Ds.Tables.Count > 0 && (result_GetAllFelettesCsoportok.Ds.Tables[0].Rows == null || result_GetAllFelettesCsoportok.Ds.Tables[0].Rows.Count == 0))
                    {
                        vanaktivszervezet = false;
                    }
                }
                else
                {
                    vanaktivszervezet = false;
                }

                // megvizsgáljuk, hogy létezik-e az ügyintéző, ha nem akkor az adminisztrátor csoportban lévő felhasználókkal kivehető az irattárból                

                ExecParam execParam_getCsoport = execParam.Clone();
                execParam_getCsoport.Record_Id = statusz.CsopId_Ugyintezo;

                Result result_GetCsoport = service_csoportok.Get(execParam_getCsoport);

                DateTime ErvKezd;
                DateTime ErvVege;

                // Ha nem találtuk meg az Ügyintéző csoportját vagy a felettes csoportját, mert már nem létezik
                if (String.IsNullOrEmpty(result_GetCsoport.ErrorCode) &&
                (result_GetCsoport.Record == null
                || (result_GetCsoport.Record != null &&
                    (DateTime.TryParse((result_GetCsoport.Record as KRT_Csoportok).ErvVege, out ErvVege) && ErvVege < DateTime.Now
                    || DateTime.TryParse((result_GetCsoport.Record as KRT_Csoportok).ErvKezd, out ErvKezd) && ErvKezd > DateTime.Now))
                || !vanaktivszervezet
                ))
                {
                    // Ellenőrizzük, hogy a felhasználó benne van-e adminisztrátor csoportban
                    Contentum.eAdmin.Service.KRT_CsoportokService service_admincsoportok =
                        Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

                    ExecParam execParam_getAdminCsoport = execParam.Clone();
                    KRT_CsoportokSearch krt_csoportoksearch = new KRT_CsoportokSearch();
                    krt_csoportoksearch.Tipus.Value = "R"; // Rendszergazdák
                    krt_csoportoksearch.Tipus.Operator = Query.Operators.equals;

                    Result result_GetAdminCsoport = service_admincsoportok.GetAll(execParam_getAdminCsoport, krt_csoportoksearch);
                    if (String.IsNullOrEmpty(result_GetAdminCsoport.ErrorCode) && result_GetAdminCsoport.Ds.Tables.Count > 0 && result_GetAdminCsoport.Ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in result_GetAdminCsoport.Ds.Tables[0].Rows)
                        {
                            if (Csoportok.IsMember(execParam.Felhasznalo_Id, row["Id"].ToString()))
                            {
                                return true;
                            }
                        }
                    }
                }

                #endregion
            }

            // END BUG_14775 DUP - Elektronikus irattárban lévő iratok további kezelése

            if (AtvehetoUgyintezesreAllapotu(statusz, out errorDetail) == false)
            {
                return false;
            }
            else
            {
                //ha már átvette ügyintézésre, akkor ne vehesse át újra
                //if (Csoportok.GetFelhasznaloSajatCsoportId(execParam) == statusz.CsopId_Ugyintezo)
                //{
                //    return false;
                //}
                // Nem csinálunk a tagság miatt plusz adatbázis lekérdezést itt,
                // az ellenõrzést majd webservice szinten, az Atvetel webmethodban végezzük

                //// Akkor veheti csak át, ha benne van a felelõs csoportban
                //if (Csoportok.IsMember(execParam.Felhasznalo_Id, statusz.CsopId_Felelos) == false)
                //{
                //    return false;
                //}
                //else
                //{
                //    return true;             
                //}

                return true;
            }
        }

        /// <summary>
        /// Állapota alapján Átvehetõ-e ügyintézésre (Nem figyeli, hogy a felhasználó
        /// benne van-e a felelõs csoportban)
        /// </summary>        
        public static bool AtvehetoUgyintezesreAllapotu(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
                case KodTarak.UGYIRAT_ALLAPOT.Felfuggesztett:
                    return true;
                case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                    {
                        switch (statusz.TovabbitasAlattiAllapot)
                        {
                            case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                            case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                            case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                            case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                                return true;
                            default:
                                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.TovabbitasAlattiAllapot);
                                return false;
                        }
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }


        public static bool SkontrobaHelyezheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Ha õ az õrzõje, és Ügyintézés alatt állapotban van

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            // Munkaügyirat nem tehetõ skontróba:
            if (statusz.Munkaanyag)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratMunkaanyag, Statusz.ColumnTypes.Munkaanyag);
                return false;
            }

            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt)
            {
                return true;
            }
            else
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratNemUgyintezesAlattAllapotu, Statusz.ColumnTypes.Allapot);
                return false;
            }

        }

        public static bool SkontrobolKiveheto(Statusz statusz, ExecParam execParam)
        {
            // Ha õ az õrzõje, és 'Skontróban' állapotban van

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam)
                && statusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert)
            {
                return false;
            }

            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban
                || statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool Sztornozhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                case KodTarak.UGYIRAT_ALLAPOT.Foglalt:
                    //case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                    return true;
                case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                    // Nem lehet lezártat sztornózni:
                    if (statusz.TovabbitasAlattiAllapot != KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart)
                    {
                        return true;
                    }
                    else
                    {
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                        return false;
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }


        public static bool AtadasraKijelolheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            //if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            //{
            //    return false;
            //}

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa:
                    if (statusz.Kovetkezo_Felelos_Id != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
                    {
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.NemIrattarozasJovahagyoja, Statusz.ColumnTypes.IrattarozasJovahagyo);
                        return false;
                    }
                    else
                        return true;
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                    if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
                    {
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                        return false;
                    }
                    else
                        return true;
                case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                    if (Csoportok.IsMember(execParam.Felhasznalo_Id, statusz.FelhCsopId_Orzo))
                        return true;
                    else
                    {
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                        return false;
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }

        }



        public static bool Atveheto(Statusz statusz, ExecParam execParam)
        {
            ErrorDetails errorDetail;
            return Atveheto(statusz, execParam, out errorDetail);
        }

        /// <summary>
        /// Állapota alapján átvehetõ-e, illetve benne van-e a felhasználó a felelõs csoportban?
        /// </summary>        
        public static bool Atveheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (AtvehetoAllapotu(statusz, out errorDetail) == false)
            {
                return false;
            }
            else
            {
                // Nem csinálunk a tagság miatt plusz adatbázis lekérdezést itt,
                // az ellenõrzést majd webservice szinten, az Atvetel webmethodban végezzük

                //// A felhasználó tagja-e a felelõs csoportnak:
                //if (Csoportok.IsMember(execParam.Felhasznalo_Id, statusz.CsopId_Felelos) == false)
                //{
                //    return false;
                //}
                //else
                //{
                //    return true;
                //}


                return true;
            }
        }


        /// <summary>
        /// Az állapota alapján Átvehetõ (Itt nincs ellenõrizve, hogy a felhasználó 
        /// tagja-e a felelõs csoportnak)
        /// </summary>        
        public static bool AtvehetoAllapotu(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo:
                case KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott:
                case KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott:
                case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool Visszakuldheto(ExecParam execParam, Statusz statusz)
        {
            ErrorDetails errorDetail;
            return Visszakuldheto(execParam, statusz, out errorDetail);
        }

        public static bool Visszakuldheto(ExecParam execParam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                    if (execParam.Felhasznalo_Id.Equals(statusz.FelhCsopId_Orzo, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // A tétel nem küldhetõ vissza, mert azt Ön adta át és csak a címzett küldheti vissza Önnek!
                        errorDetail = statusz.CreateErrorDetail("A tétel nem küldhetõ vissza, mert azt Ön adta át és csak a címzett küldheti vissza Önnek!", Statusz.ColumnTypes.CsopId_Felelos);
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }

        }

        public static bool AtmenetiIrattarbolVisszakuldheto(ExecParam execParam, Statusz statusz)
        {
            ErrorDetails errorDetail;
            return AtmenetiIrattarbolVisszakuldheto(execParam, statusz, out errorDetail);
        }

        public static bool AtmenetiIrattarbolVisszakuldheto(ExecParam execParam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (AtmenetiIrattarbaVeheto(statusz, execParam, out errorDetail))
            {
                switch (statusz.Allapot)
                {
                    case KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott:
                        return true;
                    default:
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                        return false;
                }
            }
            else
            {
                if (errorDetail == null)
                {
                    // A tétel nem küldhetõ vissza az irattárból, mert csak az irattárba átvehetõ tételek küldhetõk vissza!
                    errorDetail = statusz.CreateErrorDetail("A tétel nem küldhetõ vissza az irattárból, mert csak az irattárba átvehetõ tételek küldhetõk vissza!");
                }
                else
                {
                    // A tétel nem küldhetõ vissza az irattárból, mert csak az irattárba átvehetõ tételek küldhetõk vissza!
                    errorDetail.Message = String.Format("{0}\\n{1}", "A tétel nem küldhetõ vissza az irattárból, mert csak az irattárba átvehetõ tételek küldhetõk vissza!", errorDetail.Message ?? "");
                }

                return false;
            }
        }

        public static bool KozpontiIrattarbolVisszakuldheto(ExecParam execParam, Statusz statusz)
        {
            ErrorDetails errorDetail;
            return KozpontiIrattarbolVisszakuldheto(execParam, statusz, out errorDetail);
        }

        public static bool KozpontiIrattarbolVisszakuldheto(ExecParam execParam, Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (KozpontiIrattarbaVeheto(statusz, execParam, out errorDetail))
            {
                switch (statusz.Allapot)
                {
                    case KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott:
                        return true;
                    default:
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                        return false;
                }
            }
            else
            {
                if (errorDetail == null)
                {
                    // A tétel nem küldhetõ vissza az irattárból, mert csak az irattárba átvehetõ tételek küldhetõk vissza!
                    errorDetail = statusz.CreateErrorDetail("A tétel nem küldhetõ vissza az irattárból, mert csak az irattárba átvehetõ tételek küldhetõk vissza!");
                }
                else
                {
                    // A tétel nem küldhetõ vissza az irattárból, mert csak az irattárba átvehetõ tételek küldhetõk vissza!
                    errorDetail.Message = String.Format("{0}\\n{1}", "A tétel nem küldhetõ vissza az irattárból, mert csak az irattárba átvehetõ tételek küldhetõk vissza!", errorDetail.Message ?? "");
                }

                return false;
            }
        }

        public static bool AtadasraKijelolesVisszavonhato(Statusz statusz, ExecParam execParam)
        {
            // ha nem a felhasználó az õrzõ:
            // Ha irattárból kiadott az ügyirat, akkor nem kell figyelni az õrzõt (mert olyankor az õrzõ mindig az irattár)
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam)
                && !(statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt && statusz.TovabbitasAlattiAllapot == KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert)
                && !(statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt && statusz.TovabbitasAlattiAllapot == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo)
                )
            {
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                    return true;
                default:
                    return false;
            }
        }


        public static bool ElintezetteNyilvanithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            bool IsElintezetteNyilvanitasEnabled = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.ELINTEZETTE_NYILVANITAS_ENABLED, false);

            if (!IsElintezetteNyilvanitasEnabled)
            {
                if (!statusz.IsElektronikus)
                {
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratNemElektronikus, Statusz.ColumnTypes.Jelleg);
                    return false;
                }
            }

            // ha jóváhagyás alatt, akkor a következõ felelõsben a jelenlegi felhasználó (vezetõ) van,
            // és nem kell egyben õrzõnek is lennie,
            // egyébként neki kell az õrzõnek lennie
            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa)
            {
                if (statusz.Kovetkezo_Felelos_Id != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
                {
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.NemElintezesJovahagyoja, Statusz.ColumnTypes.ElintezesJovahagyo);
                    return false;
                }
            }
            else
            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.UGYIRATLEZARAS_POSTAZOTT_IRATPELDANY_ENABLED, false))
            {
                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
                ugyiratokSearch.Id.Value = statusz.Id;
                ugyiratokSearch.Id.Operator = Query.Operators.equals;
                EREC_PldIratPeldanyokSearch iratSearch = new EREC_PldIratPeldanyokSearch(true);
                iratSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Belso;
                iratSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Operator = Query.Operators.equals;
                Result result = service.GetAllByUgyirat(execParam, ugyiratokSearch, iratSearch, null);

                if (result.IsError)
                {
                    errorDetail = statusz.CreateErrorDetail("Hiba az iratpeldanyok lekÉrdezése során.");
                    return false;
                }
                else if (result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
                {
                    var rows = result.Ds.Tables[0].Rows;

                    string iktatoIds = "";

                    if (statusz.IsElektronikus)
                    {
                        for (int i = 0; i < rows.Count; i++)
                        {
                            string allapot = rows[i]["Allapot"].ToString();
                            string expMode = rows[i]["KuldesMod"].ToString();

                            if ((allapot == KodTarak.IRATPELDANY_ALLAPOT.Iktatott || allapot == KodTarak.IRATPELDANY_ALLAPOT.Expedialt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott
                                || allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo
                                || allapot == KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben || allapot == KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban) &&
                                (expMode == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu))
                            {
                                string azonosito = rows[i]["Azonosito"].ToString();
                                iktatoIds += azonosito + ",";
                            }
                        }

                        if (!string.IsNullOrEmpty(iktatoIds))
                        {
                            iktatoIds = iktatoIds.TrimEnd(',');
                            // BUG#6084:
                            errorDetail = statusz.CreateErrorDetail("Ügyirat nem nyilvánítható elintézetté, mert van még kiküldés alatti iratpéldánya" + " (" + iktatoIds + ")");
                            return false;
                        }
                    }
                }
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa:
                    // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                    // Lezáráskor ellenõrzés: már kell lennie ITSZ-nek
                    if (String.IsNullOrEmpty(statusz.IraIrattariTetel_Id))
                    {
                        errorDetail = statusz.CreateErrorDetail(52247, Statusz.ColumnTypes.IraIrattariTetel_Id);
                        return false;
                    }
                    else return true;
                default:
                    // nem ügyintézés alatti az állapot:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratNemUgyintezesAlattAllapotu, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool ElintezetteNyilvanitasJovahagyhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó a "következõ felelõs":
            if (statusz.Kovetkezo_Felelos_Id != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.NemElintezesJovahagyoja, Statusz.ColumnTypes.ElintezesJovahagyo);
                return false;
            }

            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.UGYIRATLEZARAS_POSTAZOTT_IRATPELDANY_ENABLED, false))
            {
                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
                ugyiratokSearch.Id.Value = statusz.Id;
                ugyiratokSearch.Id.Operator = Query.Operators.equals;
                EREC_PldIratPeldanyokSearch iratSearch = new EREC_PldIratPeldanyokSearch(true);
                iratSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Belso;
                iratSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Operator = Query.Operators.equals;
                Result result = service.GetAllByUgyirat(execParam, ugyiratokSearch, iratSearch, null);

                if (result.IsError)
                {
                    errorDetail = statusz.CreateErrorDetail("Hiba az iratpeldanyok lekÉrdezése során.");
                    return false;
                }
                else if (result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
                {
                    var rows = result.Ds.Tables[0].Rows;

                    string iktatoIds = "";

                    if (statusz.IsElektronikus)
                    {
                        for (int i = 0; i < rows.Count; i++)
                        {
                            string allapot = rows[i]["Allapot"].ToString();
                            string expMode = rows[i]["KuldesMod"].ToString();

                            if ((allapot == KodTarak.IRATPELDANY_ALLAPOT.Iktatott || allapot == KodTarak.IRATPELDANY_ALLAPOT.Expedialt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott
                                || allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo
                                || allapot == KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben || allapot == KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban) &&
                                (expMode == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu))
                            {
                                string azonosito = rows[i]["Azonosito"].ToString();
                                iktatoIds += azonosito + ",";
                            }
                        }

                        if (!string.IsNullOrEmpty(iktatoIds))
                        {
                            iktatoIds = iktatoIds.TrimEnd(',');
                            errorDetail = statusz.CreateErrorDetail("Ügyirat nem nyilvánítható elintézetté, mert van még kiküldés alatti iratpéldánya" + " (" + iktatoIds + ")");
                            return false;
                        }
                    }
                }
            }

            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa)
            {
                return true;
            }
            else
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }

        }

        public static bool ElintezetteNyilvanitasVisszautasithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            return ElintezetteNyilvanitasJovahagyhato(statusz, execParam, out errorDetail);

        }


        public static bool Lezarhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.UGYIRATLEZARAS_POSTAZOTT_IRATPELDANY_ENABLED, false))
            {
                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
                ugyiratokSearch.Id.Value = statusz.Id;
                ugyiratokSearch.Id.Operator = Query.Operators.equals;
                EREC_PldIratPeldanyokSearch iratSearch = new EREC_PldIratPeldanyokSearch(true);
                iratSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Belso;
                iratSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Operator = Query.Operators.equals;
                //LZS - BUG_12411 - Sztornózottat kiszűrjük.
                iratSearch.Allapot.Filter("90", Query.Operators.notequals);

                Result result = service.GetAllByUgyirat(execParam, ugyiratokSearch, iratSearch, null);

                if (result.IsError)
                {
                    errorDetail = statusz.CreateErrorDetail("Hiba az iratpeldanyok lekérdezése során.");
                    return false;
                }
                else if (result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
                {
                    var rows = result.Ds.Tables[0].Rows;

                    string iktatoIds = "";

                    if (!statusz.IsElektronikus)
                    {
                        for (int i = 0; i < rows.Count; i++)
                        {
                            string allapot = rows[i]["Allapot"].ToString();
                            string expMode = rows[i]["KuldesMod"].ToString();
                            // BUG_6935
                            //if ((allapot == KodTarak.IRATPELDANY_ALLAPOT.Iktatott || allapot == KodTarak.IRATPELDANY_ALLAPOT.Expedialt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott
                            //    || allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo
                            //    || allapot == KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben || allapot == KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban) &&
                            //    (expMode == KodTarak.KULDEMENY_KULDES_MODJA.Postai_sima || expMode == KodTarak.KULDEMENY_KULDES_MODJA.Allami_futarszolgalat || expMode == KodTarak.KULDEMENY_KULDES_MODJA.Postai_ajanlott
                            //    || expMode == KodTarak.KULDEMENY_KULDES_MODJA.Diplomaciai_futarszolgalat || expMode == "20"))
                            //{

                            //LZS - BUG_12411 - Sztornózottat nem vizsgálunk.
                            if (allapot != "90")
                            {
                                if (!PostazottIratpeldany(execParam, allapot, expMode))
                                {
                                    string azonosito = rows[i]["Azonosito"].ToString();
                                    iktatoIds += azonosito + ",";
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(iktatoIds))
                        {
                            iktatoIds = iktatoIds.TrimEnd(',');
                            errorDetail = statusz.CreateErrorDetail("Ügyirat nem lezárható, mert van még nem postázott iratpéldánya " + "(" + iktatoIds + ")");
                            return false;
                        }
                    }

                }
            }

            bool lezarasElintezesNelkul = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.LEZARAS_ELINTEZES_NELKUL_ENABLED, true); // BUG_5085

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                    {
                        // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                        // Lezáráskor ellenõrzés: már kell lennie ITSZ-nek
                        if (String.IsNullOrEmpty(statusz.IraIrattariTetel_Id))
                        {
                            errorDetail = statusz.CreateErrorDetail(52247, Statusz.ColumnTypes.IraIrattariTetel_Id);
                            return false;
                        }
                        else
                        {
                            return lezarasElintezesNelkul ? true : UgyiratAllapotNemMegfelelo(statusz, out errorDetail);
                        }
                    }

                case KodTarak.UGYIRAT_ALLAPOT.Felfuggesztett:
                    return lezarasElintezesNelkul ? true : UgyiratAllapotNemMegfelelo(statusz, out errorDetail);

                case KodTarak.UGYIRAT_ALLAPOT.Elintezett:  //CR3408
                    // BUG_5085
                    bool isSakkoraAllapotOk = false;
                    if (!lezarasElintezesNelkul)
                    {
                        if (Rendszerparameterek.UseSakkora(execParam))
                        {
                            EREC_UgyUgyiratok ugyirat = GetUgyiratById(statusz.Id, execParam, out errorDetail);
                            if (ugyirat == null)
                            {
                                return false;
                            }

                            bool isHatosagi = Contentum.eUtility.Sakkora.IsUgyFajtaHatosagi(execParam.Clone(), ugyirat.Ugy_Fajtaja);
                            string sakkoraAllapot = ugyirat.SakkoraAllapot;
                            isSakkoraAllapotOk = !isHatosagi || (sakkoraAllapot == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Harom || sakkoraAllapot == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Het ||
                                sakkoraAllapot == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Nyolc || sakkoraAllapot == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Tiz);

                            if (!isSakkoraAllapotOk)
                            {
                                errorDetail = new ErrorDetails(ErrorDetailMessages.UgyiratSakkoraAllapotNemMegfelelo, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok,
                                    statusz.Id, Constants.ErrorDetails.ColumnTypes.SakkoraAllapot, sakkoraAllapot);
                                return false;
                            }
                        }
                        else
                        {
                            isSakkoraAllapotOk = true;
                        }

                        // kiadmanyozando iratok ellenorzese
                        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
                        iratokSearch.Ugyirat_Id.Value = statusz.Id;
                        iratokSearch.Ugyirat_Id.Operator = Query.Operators.equals;
                        iratokSearch.KiadmanyozniKell.Value = "1";
                        iratokSearch.KiadmanyozniKell.Operator = Query.Operators.equals;
                        Result iratokResult = iratokService.GetAll(execParam, iratokSearch);

                        if (iratokResult.IsError)
                        {
                            errorDetail = statusz.CreateErrorDetail("Hiba az iratok lekérdezése során.");
                            return false;
                        }

                        bool osszesKiadmanyozandoIratKiadmanyozott = true;
                        if (iratokResult.Ds.Tables.Count > 0)
                        {
                            var iratok = iratokResult.Ds.Tables[0].Rows;
                            for (int i = 0; i < iratok.Count; i++)
                            {
                                if (iratok[i]["KiadmanyozniKell"].ToString() == "1" && // BUG_11644: csak kiadmányozandó, kimeno iratokat kell figyelni
                                    iratok[i]["PostazasIranya"].ToString() == KodTarak.POSTAZAS_IRANYA.Belso.ToString() // POSTAZAS_IRANYA.Belso a kimeno!
                                    )
                                {
                                    string iratAllapot = iratok[i]["Allapot"].ToString();
                                    // Ha van kiadmányozandó és nem kiadmányozott irata, akkor nem lezárható az ügyirat (a sztornózott vagy felszabadított irat nem számít)
                                    if (iratAllapot != KodTarak.IRAT_ALLAPOT.Kiadmanyozott
                                        && iratAllapot != KodTarak.IRAT_ALLAPOT.Sztornozott
                                        && iratAllapot != KodTarak.IRAT_ALLAPOT.Felszabaditva)
                                    {
                                        osszesKiadmanyozandoIratKiadmanyozott = false;
                                        break;
                                    }
                                }
                            }
                        }

                        if (!osszesKiadmanyozandoIratKiadmanyozott)
                        {
                            errorDetail = new ErrorDetails(ErrorDetailMessages.UgyiratKiadmanyozandoIrat,
                                Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, "", "");
                            return false;
                        }
                    }

                    return (lezarasElintezesNelkul || isSakkoraAllapotOk) ? true : UgyiratAllapotNemMegfelelo(statusz, out errorDetail);

                default:
                    return UgyiratAllapotNemMegfelelo(statusz, out errorDetail);
            }
        }


        // BUG_6935
        private static bool PostazottIratpeldany(ExecParam execParam, string iratpeldany_allapot, string kuldemeny_kuldesmod)
        {
            bool postazott = false;
            string kuldesmodGuid = "";
            string postazandoGuid = "";


            // postazandó megállapítás
            kuldesmodGuid = KodTarak.GetKodTarId(execParam, kuldemeny_kuldesmod, " and KRT_KodCsoportok.Kod='KULDEMENY_KULDES_MODJA' ");
            postazandoGuid = KodTarak.GetKodTarId(execParam, "IGEN", " and KRT_KodCsoportok.Kod='POSTAZANDO' ");

            Contentum.eAdmin.Service.KRT_KodtarFuggosegService kodservice = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
            KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
            search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", "KULDEMENY_KULDES_MODJA");
            search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
            search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", "POSTAZANDO");
            search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

            Result res = kodservice.GetAll(execParam, search);

            if (!res.IsError)
            {
                if (res.Ds.Tables[0].Rows.Count > 0)
                {
                    string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();
                    KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

                    List<string> fuggoKodtarak = new List<string>();

                    var item = fuggosegek.Items.FirstOrDefault(x => (x.VezerloKodTarId == kuldesmodGuid && (x.FuggoKodtarId == postazandoGuid)));

                    if (item == null)  // nem postazando
                    {
                        postazott = true;
                    }
                    else     // postázandó
                    {
                        // postazottság megállapítás
                        string allapotGuid = "";
                        string postazottGuid = "";
                        allapotGuid = KodTarak.GetKodTarId(execParam, iratpeldany_allapot, " and KRT_KodCsoportok.Kod='IRATPELDANY_ALLAPOT' ");
                        postazottGuid = KodTarak.GetKodTarId(execParam, "IGEN", " and KRT_KodCsoportok.Kod='POSTAZOTT' ");

                        // Contentum.eAdmin.Service.KRT_KodtarFuggosegService kodservice = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
                        search = new KRT_KodtarFuggosegSearch();
                        search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", "IRATPELDANY_ALLAPOT");
                        search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
                        search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", "POSTAZOTT");
                        search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

                        res = kodservice.GetAll(execParam, search);

                        if (!res.IsError)
                        {
                            if (res.Ds.Tables[0].Rows.Count > 0)
                            {
                                adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();
                                fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

                                fuggoKodtarak = new List<string>();

                                var postazottitem = fuggosegek.Items.FirstOrDefault(x => (x.VezerloKodTarId == allapotGuid && (x.FuggoKodtarId == postazottGuid)));

                                postazott = (postazottitem != null);
                            }
                        }

                    }
                }
            }

            return postazott;
        }

        private static bool UgyiratAllapotNemMegfelelo(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
            return false;
        }

        private static EREC_UgyUgyiratok GetUgyiratById(string id, ExecParam execParam, out ErrorDetails errorDetails)
        {
            errorDetails = null;
            ExecParam ep = execParam.Clone();
            ep.Record_Id = id;

            EREC_UgyUgyiratokService srv = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            Result res = srv.Get(ep);
            if (res.IsError || res.Record == null)
            {
                errorDetails = new Statusz().CreateErrorDetail("Ügyirat adatok lekérése sikertelen!");
                return null;
            }
            return res.Record as EREC_UgyUgyiratok;
        }

        public static bool LezarasVisszavonhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart)
            {
                return true;
            }
            else
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratNemLezartAllapotu, Statusz.ColumnTypes.Allapot);
                return false;
            }
        }



        /// <summary>
        /// Az ügyirat, amit szerelni akarunk egy másikba, szerelhetõ-e?
        /// </summary>  
        public static bool Szerelheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            // Munkaanyag nem szerelhetõ:
            if (statusz.Munkaanyag)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratMunkaanyag, Statusz.ColumnTypes.Munkaanyag);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        /// <summary>
        /// A szerelt ügyirat, amit vissza akarunk vonni egy másikból, visszavonhato-e?
        /// </summary>  
        public static bool SzereltVisszavonhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Szerelt:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratNemSzereltAllapotu, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        /// <summary>
        /// Az ügyiratba beleszerelhetõ másik ügyirat
        /// </summary>        
        public static bool SzerelhetoBeleUgyirat(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            return SzerelhetoBeleUgyiratAllapotu(statusz, out errorDetail);

            //switch (statusz.Allapot)
            //{
            //    case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
            //    case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
            //    case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
            //    case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
            //    case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
            //        return true;
            //    default:
            //        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
            //        return false;
            //}
        }

        /// <summary>
        /// Állapota alapján Átvehetõ-e ügyintézésre (Nem figyeli, hogy a felhasználó
        /// benne van-e a felelõs csoportban)
        /// </summary>        
        public static bool SzerelhetoBeleUgyiratAllapotu(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool ElokeszitettSzerelesVegrehajthato(Statusz statusz_szerelendoUgyirat, string UgyUgyiratId_Szulo, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            if (statusz_szerelendoUgyirat.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz_szerelendoUgyirat.CreateErrorDetail(ErrorDetailMessages.SzerelendoUgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            // ha ki van töltve a szülõ ügyirat id, és nem szerelt állapotban van

            if (!String.IsNullOrEmpty(UgyUgyiratId_Szulo)
                && statusz_szerelendoUgyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt
                && statusz_szerelendoUgyirat.TovabbitasAlattiAllapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt)
            {
                return true;
            }
            else
            {
                errorDetail = statusz_szerelendoUgyirat.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
        }



        public static bool ElokeszitettSzerelesFelbonthato(Statusz statusz_utoirat, Statusz statusz_szerelendo
            , string UgyUgyiratId_Szulo, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ (az utóiratnál)):
            if (statusz_utoirat.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz_utoirat.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            // ha ki van töltve a szülõ ügyirat id, és nem szerelt állapotban van a szerelendo ügyirat

            if (!String.IsNullOrEmpty(UgyUgyiratId_Szulo)
                && statusz_szerelendo.Allapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt
                && statusz_szerelendo.TovabbitasAlattiAllapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt)
            {
                return true;
            }
            else
            {
                errorDetail = statusz_szerelendo.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
        }


        /// <summary>
        /// Az ügyiratból visszavonható szerelt ügyirat
        /// </summary>        
        public static bool VisszavonhatoBeloleSzereltUgyirat(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool SzerelhetoBeleRegiUgyirat(Statusz statusz, ExecParam execPram)
        {
            return true;
        }


        /// <summary>
        /// Az ügyirathoz csatolható másik ügyirat
        /// </summary>        
        public static bool Csatolhato(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
                case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                    return true;
                case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                    {
                        switch (statusz.TovabbitasAlattiAllapot)
                        {
                            case KodTarak.UGYIRAT_ALLAPOT.Iktatott:
                            case KodTarak.UGYIRAT_ALLAPOT.Szignalt:
                            case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                            case KodTarak.UGYIRAT_ALLAPOT.Elintezett:
                            case KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag:
                                return true;
                            default:
                                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.TovabbitasAlattiAllapot);
                                return false;
                        }
                    }
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static void SetCsatolhatoSearchObject(EREC_UgyUgyiratokSearch search)
        {
            search.Manual_Csatolhato.Value = "'"
                + KodTarak.UGYIRAT_ALLAPOT.Iktatott + "','"
                + KodTarak.UGYIRAT_ALLAPOT.Szignalt + "','"
                + KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt + "','"
                + KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt + "','"
                + KodTarak.UGYIRAT_ALLAPOT.Elintezett + "','"
                + KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag + "'";

            search.Manual_Csatolhato.Operator = Query.Operators.inner;
        }


        public static bool LehetUjUgyiratDarabotLetrehozni(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratNemUgyintezesAlattAllapotu, Statusz.ColumnTypes.Allapot);
                    return false;
            }

        }


        #region Irattari funkciok, Irattarhoz tartozo allapotok

        // BUG_4765
        public static bool IsIrattarozasElektronikusJovahagyasNelkul(ExecParam xpm)
        {
            return Rendszerparameterek.GetBoolean(xpm, "IRATTAROZAS_ELEKTRONIKUS_JOVAHAGYAS_NELKUL", true);
        }

        public static bool IrattarbaKuldheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Munkaanyag nem küldhetõ irattárba:
            if (statusz.Munkaanyag)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratMunkaanyag, Statusz.ColumnTypes.Munkaanyag);
                return false;
            }

            // Elektronikus irat nem küldhetõ irattárba, ha IRATTAROZAS_ELEKTRONIKUS_JOVAHAGYAS_NELKUL=1
            if (IsIrattarozasElektronikusJovahagyasNelkul(execParam) && statusz.IsElektronikus)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratElektronikus);
                return false;
            }

            // ha jóváhagyás alatt, akkor a következõ felelõsben a jelenlegi felhasználó (vezetõ) van,
            // és nem kell egyben õrzõnek is lennie,
            // egyébként neki kell az õrzõnek lennie
            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
            {
                if (statusz.Kovetkezo_Felelos_Id != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
                {
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.NemIrattarozasJovahagyoja, Statusz.ColumnTypes.IrattarozasJovahagyo);
                    return false;
                }
            }
            else if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa:
                case KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart:
                case KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott:
                case KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool KozpontiIrattarbaKuldheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó az õrzõ:
            //if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            //{
            //    return false;
            //}

            // Munkaanyag nem küldhetõ irattárba:
            if (statusz.Munkaanyag)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratMunkaanyag, Statusz.ColumnTypes.Munkaanyag);
                return false;
            }

            // Elektronikus irat nem küldhetõ irattárba
            if (statusz.IsElektronikus)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratElektronikus);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }

        }

        public static bool IrattarbaKuldesJovahagyhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha nem a felhasználó a "következõ felelõs":
            if (statusz.Kovetkezo_Felelos_Id != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.NemIrattarozasJovahagyoja, Statusz.ColumnTypes.IrattarozasJovahagyo);
                return false;
            }

            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
            {
                return true;
            }
            else
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }

        }

        public static bool IrattarbaKuldesVisszautasithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            return IrattarbaKuldesJovahagyhato(statusz, execParam, out errorDetail);

        }

        public static bool AtmenetiIrattarbaVeheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Munkaanyag nem vehetõ irattárba:
            if (statusz.Munkaanyag)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratMunkaanyag, Statusz.ColumnTypes.Munkaanyag);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool KozpontiIrattarbaVeheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Munkaanyag nem vehetõ irattárba:
            if (statusz.Munkaanyag)
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratMunkaanyag, Statusz.ColumnTypes.Munkaanyag);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool IrattarbaKuldhetoTomeges(ExecParam execParam, string[] Ids, out ErrorDetails errorDetails)
        {
            errorDetails = null;
            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch(true);

            ExecParam ep = execParam.Clone();
            ep.Record_Id = Ids[0];
            Statusz statusz = new Statusz();

            EREC_UgyUgyiratokService srv = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            Result res = srv.Get(ep);
            if (res.IsError || res.Record == null)
            {
                errorDetails = statusz.CreateErrorDetail("Ügyirat adatok lekérése sikertelen!");
                return false;
            }
            EREC_UgyUgyiratok ugyiratObj = res.Record as EREC_UgyUgyiratok;
            // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
            if (!String.IsNullOrEmpty(ugyiratObj.IraIrattariTetel_Id.ToString()))
            {
                search.Id.Value = Search.GetSqlInnerString(Ids);
                search.Id.Operator = Query.Operators.inner;
                search.WhereByManual = String.Format(" and (EREC_UgyUgyiratok.IraIrattariTetel_Id is null or EREC_UgyUgyiratok.IraIrattariTetel_Id <> '{0}')", ugyiratObj.IraIrattariTetel_Id);
                search.TopRow = 1;

                res = srv.GetAll(ep, search);
                if (res.IsError || res.Ds == null)
                {
                    errorDetails = statusz.CreateErrorDetail("Ügyirat adatok lekérése sikertelen!");
                    return false;
                }

                if (res.Ds.Tables[0].Rows.Count > 0)
                {
                    errorDetails = statusz.CreateErrorDetail("A kijelölt ügyiratok között eltérnek az irattári tétel azonosítók vagy az irattári tétel azonosító nincs minden kijelölt ügyiratnál megadva!");
                    return false;
                }
                return true;
            }
            // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
            else
            {
                errorDetails = statusz.CreateErrorDetail("Az irattári tétel azonosító nincs minden kijelölt ügyiratnál megadva!");
                return false;
            }

        }

        // BUG_8005
        public static bool IrattarbaKuldesJovahagyhatoTomeges(ExecParam execParam, string[] Ids, out ErrorDetails errorDetails)
        {
            errorDetails = null;
            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch(true);

            ExecParam ep = execParam.Clone();

            EREC_UgyUgyiratokService srv = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            search.Id.Value = Search.GetSqlInnerString(Ids);
            search.Id.Operator = Query.Operators.inner;

            search.Kovetkezo_Felelos_Id.Value = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            search.Kovetkezo_Felelos_Id.Operator = Query.Operators.notequals;
            search.Kovetkezo_Felelos_Id.Group = "jovahagyhato";
            search.Kovetkezo_Felelos_Id.GroupOperator = Query.Operators.or;

            search.Allapot.Value = KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa;
            search.Allapot.Operator = Query.Operators.notequals;
            search.Allapot.Group = "jovahagyhato";
            search.Allapot.GroupOperator = Query.Operators.or;

            search.TopRow = 1;
            Statusz statusz = new Statusz();
            Result res = srv.GetAll(ep, search);
            if (res.IsError || res.Ds == null)
            {
                errorDetails = statusz.CreateErrorDetail("Ügyirat adatok lekérése sikertelen!");
                return false;
            }

            if (res.Ds.Tables[0].Rows.Count > 0)
            {
                errorDetails = statusz.CreateErrorDetail("A kijelölt ügyiratok között vannak olyan tételek amelyek nem hagyhatóak jóvá (nem Ön a jóváhagyó vagy nem megfelelõ az ügyirat státusza!");
                return false;
            }
            return true;
        }
        public static bool AtmenetiIrattarbanOrzott(Statusz statusz, ExecParam execParam)
        {
            // ha nem a felelos a Kozponti irattar:
            if (statusz.CsopId_Felelos.ToUpper() == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id.ToUpper())
            {
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                    return true;
                default:
                    return false;
            }
        }

        public static bool AtmenetiIrattarbolKikerheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            // ha nem a felelos a Kozponti irattar:
            if (statusz.CsopId_Felelos.ToUpper() == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id.ToUpper())
            {
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                // Iktatás miatt, hogy itt is képzõdjön irattári kikérõ:
                case KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool AtmenetiIrattarbolKikeresJovahagyhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        // adott felhasználó kiadhatja-e az ügyiratot az átmeneti irattárból
        // (az aktuális szervezetéhez hozzárendelt irattárak alapján)
        public static bool AtmenetiIrattarbolKiadhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (Ugyiratok.AtmenetiIrattarbolElkert(statusz, execParam, out errorDetail) == true)
            {
                //átmeneti irattárral belépve kiadható
                if (statusz.CsopId_Felelos == execParam.FelhasznaloSzervezet_Id)
                {
                    return true;
                }
                // CR#2220: Csak a saját szervezetéhez rendelt átmeneti irattárakból adhat ki,
                // a többiben lévõ ügyiratokat csak láthatja, ha egyébként jogosult
                // Felhasználóhoz rendelt átmeneti irattárak lekérése
                Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

                Result result_irattarak = service_csoportok.GetAllKezelhetoIrattar(execParam);
                if (String.IsNullOrEmpty(result_irattarak.ErrorCode))
                {
                    //if (result_irattarak.Ds.Tables[0].Rows.Count > 0)
                    //{
                    DataRow[] rows = result_irattarak.Ds.Tables[0].Select(String.Format("Id='{0}'", statusz.CsopId_Felelos));
                    if (rows.Length >= 1) // elvileg csak pontosan 1 lehetne...
                    {
                        return true;
                    }
                    else
                    {
                        errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.NemSzervezetAtmenetiIrattara, Statusz.ColumnTypes.CsopId_Felelos);
                        return false;
                    }
                    //}
                }
            }

            return false;
        }

        public static bool SkontroIrattarbolKikerheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            // ha a felelos nem a skontró irattáros:
            if (statusz.CsopId_Felelos.ToUpper() != KodTarak.SPEC_SZERVEK.GetSkontroIrattaros(execParam).Obj_Id.ToUpper())
            {
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }


        public static bool KozpontiIrattarbanOrzott(Statusz statusz, ExecParam execParam)
        {
            //// ha nem a felelos a Kozponti irattar:
            if (statusz.CsopId_Felelos.ToUpper() != KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id.ToUpper())
            {
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                    return true;
                default:
                    return false;
            }
        }

        public static bool KozpontiIrattarbaKuldott(Statusz statusz, ExecParam execParam)
        {
            // A felelõsnek a Központi irattárnak kell lennie:
            if (statusz.CsopId_Felelos.ToUpper() != KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id.ToUpper())
            {
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott:
                    return true;
                default:
                    return false;
            }
        }


        public static bool Kolcsonozheto(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }


        /// <summary>
        /// Kölcsönzés kiadható-e?
        /// </summary>
        /// <param name="statusz"></param>
        /// <param name="errorDetail"></param>
        /// <returns></returns>
        public static bool KiadhatoKozpontiIrattarbol(Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }


        public static bool KozpontiIrattarbolElkert(Statusz statusz, ExecParam execParam)
        {
            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert:
                    return true;
                default:
                    return false;
            }
        }


        public static bool AtmenetiIrattarbolElkert(Statusz statusz, ExecParam execParam)
        {
            ErrorDetails errorDertail = null;
            return AtmenetiIrattarbolElkert(statusz, execParam, out errorDertail);
        }
        public static bool AtmenetiIrattarbolElkert(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            bool kikeresJovahagyassal = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRATTAROZAS_ATMENETI_AZONOS_KEZELES, false);
            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert:
                    if (!kikeresJovahagyassal)
                    {
                        return true;
                    }
                    goto default;
                case KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        public static bool SkontroIrattarbolElkert(Statusz statusz, ExecParam execParam)
        {
            ErrorDetails errorDertail = null;
            return SkontroIrattarbolElkert(statusz, execParam, out errorDertail);
        }
        public static bool SkontroIrattarbolElkert(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;
            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert:
                    return true;
                default:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
            }
        }

        #endregion


        public static bool Munkaanyag(EREC_UgyUgyiratok erec_UgyUgyiratok)
        {
            // ha a fõszám null, akkor munkaanyag az ügyirat
            if (erec_UgyUgyiratok.Typed.Foszam.IsNull
                || erec_UgyUgyiratok.Foszam == "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static string GetFoszamString(EREC_UgyUgyiratok erec_UgyUgyiratok)
        {
            if (Munkaanyag(erec_UgyUgyiratok))
            {
                return "MU" + erec_UgyUgyiratok.Sorszam;
            }

            return erec_UgyUgyiratok.Foszam;
        }

        // BLG_292
        // egyégként kezelendõ azonosító rész + egyedi azonosítás, ha van
        //private static string GetFoszam_Azonosito(string foszamStr, string evStr, string azonositoStr)
        //{
        //    return foszamStr + " /" + evStr + (!String.IsNullOrEmpty(azonositoStr) ? " /" + azonositoStr : "");
        //}


        //public static string GetFullFoszam(EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIktatoKonyvek erec_IraIktatoKonyvek)
        //{
        //    if (erec_UgyUgyiratok != null && erec_IraIktatoKonyvek != null)
        //    {
        //        string foszamStr = GetFoszamString(erec_UgyUgyiratok);


        //        if (erec_IraIktatoKonyvek.KozpontiIktatasJelzo == "1")
        //        {
        //            // Központi iktatás jelzõ:
        //            //return foszamStr + " /" + erec_IraIktatoKonyvek.Ev + (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.MegkulJelzes) ? " /" + erec_IraIktatoKonyvek.MegkulJelzes : "");
        //            return GetFoszam_Azonosito(foszamStr, erec_IraIktatoKonyvek.Ev, erec_IraIktatoKonyvek.MegkulJelzes);
        //        }
        //        else
        //        {
        //            //return erec_IraIktatoKonyvek.Iktatohely + " /" + foszamStr + " /" + erec_IraIktatoKonyvek.Ev + (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.MegkulJelzes) ? " /" + erec_IraIktatoKonyvek.MegkulJelzes : "");
        //            return erec_IraIktatoKonyvek.Iktatohely + " /" + GetFoszam_Azonosito(foszamStr, erec_IraIktatoKonyvek.Ev, erec_IraIktatoKonyvek.MegkulJelzes);
        //        }


        //    }
        //    else
        //    {
        //        return "";
        //    }
        //}

        public static string GetFullFoszam(ExecParam exec_Param, EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIktatoKonyvek erec_IraIktatoKonyvek)
        {
            return Iktatoszam.GetFullAzonosito(exec_Param, erec_UgyUgyiratok, erec_IraIktatoKonyvek);
        }

        public static bool TeljessegEllenorizheto(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // ha a felhasználó nem tagja az örzõ csoportjának
            if (!Csoportok.IsMember(Csoportok.GetFelhasznaloSajatCsoportId(execParam), statusz.FelhCsopId_Orzo))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratOrzojenekNemtagja, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            switch (statusz.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt:
                case KodTarak.UGYIRAT_ALLAPOT.Sztornozott:
                    errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                    return false;
                default:
                    return true;
            }

        }

        public static bool SkontroJovahagyhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Ha õ a következõ felelõs
            // Figyelem:  skontróba helyezéskor a felhasználó csoportjának vezetõjét beállítjuk Kovetkezo_Felelos_Id-nak
            if (statusz.Kovetkezo_Felelos_Id != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.NemSkontroJovahagyoja, Statusz.ColumnTypes.SkontrobaHelyezesJovahagyo);
                return false;
            }

            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa)
            {
                return true;
            }
            else
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }

        }

        public static bool SkontroElutasithato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            return SkontroJovahagyhato(statusz, execParam, out errorDetail);

        }

        public static bool SkontroVisszavonhato(Statusz statusz, ExecParam execParam, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            //ha õ az örzõ
            if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratnakNemOnAzOrzoje, Statusz.ColumnTypes.FelhCsopId_Orzo);
                return false;
            }

            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa)
            {
                return true;
            }
            else
            {
                errorDetail = statusz.CreateErrorDetail(ErrorDetailMessages.UgyiratAllapotNemMegfelelo, Statusz.ColumnTypes.Allapot);
                return false;
            }
        }

        //A fizikai ügyirat nem kölcsönözhetõ ki betekintésre,
        //ha a FIZIKAI_UGYIRAT_KOLCSONZES_BETEKINTESRE_DISABLED rendszerparaméter be van kapcsolva
        public static bool IsUgyiratKolcsonozhetoBetekintesre(ExecParam execParam, EREC_UgyUgyiratok ugyirat)
        {
            bool isBetekintesDisabled = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.FIZIKAI_UGYIRAT_KOLCSONZES_BETEKINTESRE_DISABLED, false);


            if (isBetekintesDisabled)
            {
                return !IsUgyiratFizikai(ugyirat);
            }

            return true;
        }

        public static bool IsUgyiratFizikai(EREC_UgyUgyiratok ugyirat)
        {
            return ugyirat.Jelleg != KodTarak.UGYIRAT_JELLEG.Elektronikus;
        }
        private static bool Uj_Ugyirat_Hatarido_Kezeles(string value)
        {
            return "1".Equals(value);
        }

        public static bool Uj_Ugyirat_Hatarido_Kezeles(Page page)
        {
            string value = Rendszerparameterek.Get(page, Rendszerparameterek.UGYIRAT_HATARIDO_KEZELES);
            return Uj_Ugyirat_Hatarido_Kezeles(value);
        }

        public static bool Uj_Ugyirat_Hatarido_Kezeles(ExecParam execParam)
        {
            string value = Rendszerparameterek.Get(execParam, Rendszerparameterek.UGYIRAT_HATARIDO_KEZELES);
            return Uj_Ugyirat_Hatarido_Kezeles(value);
        }

    }

}
