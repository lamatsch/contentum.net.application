﻿using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Contentum.eRecord.BaseUtility
{
    public class eBeadvanyHelper
    {

        public static string GetAllapot(string value, Page page)
        {
            return KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.EBEADVANY_KODCSOPORTOK.ALLAPOT, value, page);
        }

        public static string GetTipus(string value, Page page)
        {
            return KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.EBEADVANY_KODCSOPORTOK.FELADO_TIPUSA, value, page);
        }

        public static string GetTarterulet(object value, Page page)
        {
            if (value == null || value.ToString() == string.Empty) { return string.Empty; }
            return Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.EBEADVANY_KODCSOPORTOK.TARTERULET, Convert.ToString(value), page);
        }

        public static string GetValaszUtvonal(object value, Page page)
        {
            if (value == null || value.ToString() == string.Empty) { return string.Empty; }
            return Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.EBEADVANY_KODCSOPORTOK.VALASZUTVONAL, Convert.ToString(value), page);
        }

        public static string GetCelRendszer(object value, Page page)
        {
            if (value == null || value.ToString() == string.Empty) { return string.Empty; }
            return Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.EBEADVANY_KODCSOPORTOK.eBeadvany_CelRendszer, Convert.ToString(value), page);
        }               
    }
}
