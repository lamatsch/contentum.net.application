﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class AlairasokService : System.Web.Services.WebService 
{
    private AlairasokStoredProcedure sp = null;
    
    private DataContext dataContext;
    
    public AlairasokService () 
    {
        dataContext = new DataContext(this.Application);

        sp = new AlairasokStoredProcedure(dataContext);
    }

    public AlairasokService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new AlairasokStoredProcedure(this.dataContext);
    }

    [WebMethod()]
    public Result GetAll_PKI_MuveletAlairasLista(ExecParam execParam, string alkalmazasKod, string folyamatKod, string muveletKod
        , string alairoSzerep, bool titkositas)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
     
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll_PKI_MuveletAlairasLista(execParam, alkalmazasKod, folyamatKod, muveletKod, alairoSzerep, titkositas);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    [WebMethod()]
    public Result GetAll_PKI_ElojegyzettAlairas(ExecParam execParam, string alkalmazasKod, string objektumTip, string objektum_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll_PKI_ElojegyzettAlairas(execParam, alkalmazasKod, objektumTip, objektum_Id);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }




    [WebMethod()]
    public Result GetAll_PKI_IratAlairas(ExecParam execParam, string alkalmazasKod, string objektumTip, string objektum_Id
        , string folyamatKod, string muveletKod, string alairoSzerep)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll_PKI_IratAlairas(execParam, alkalmazasKod, objektumTip, objektum_Id, folyamatKod, muveletKod, alairoSzerep);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }



    [WebMethod()]
    public Result GetAll_PKI_IratAlairoSzabalyok(ExecParam execParam, string felhasznaloId_Alairo, string alkalmazasKod, string objektumTip, string objektum_Id
        , string folyamatKod, string muveletKod, string alairoSzerep)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll_PKI_IratAlairoSzabalyok(execParam, felhasznaloId_Alairo, alkalmazasKod, objektumTip, objektum_Id, folyamatKod, muveletKod, alairoSzerep);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    [WebMethod()]
    public Result GetAll_PKI_AlairasAdatok(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll_PKI_AlairasAdatok(execParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    
}

