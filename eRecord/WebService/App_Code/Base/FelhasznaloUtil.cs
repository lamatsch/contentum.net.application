using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;

/// <summary>
/// Summary description for FelhasznaloUtil
/// </summary>
public class FelhasznaloUtil
{
    //public FelhasznaloUtil()
    //{
    //    //
    //    // TODO: Add constructor logic here
    //    //
    //}

    /// <summary>
    /// Visszaadja a felhasználó egyszemélyes csoportjának Id-ját a megadott ExecParam-ból
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    public static String GetFelhasznaloCsoport(ExecParam execParam)
    {
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)) 
        {
            return "";
        }
        else 
        {
            // A felhasználó egyszemélyes csoportjának Id-ja jelenleg megegyezik a Felhasznalo_Id-val:

            return execParam.Felhasznalo_Id;
        }
     
    }
}
