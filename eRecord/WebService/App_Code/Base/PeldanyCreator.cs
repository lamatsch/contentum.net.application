﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for PeldanyCreator
/// </summary>
public class PeldanyCreator
{
    private DataContext _DataContext;
    ExecParam _ExecParam;

    EREC_IraIktatoKonyvek IktatoKonyv { get; set; }
    EREC_UgyUgyiratok Ugyirat { get; set; }
    EREC_IraIratok Irat { get; set; }
    EREC_PldIratPeldanyok ElsoPeldany { get; set; }

    List<EREC_PldIratPeldanyok> Peldanyok;

    public PeldanyCreator(DataContext dataContext, ExecParam execParam, EREC_IraIktatoKonyvek iktatokonyv,
        EREC_UgyUgyiratok ugyirat, EREC_IraIratok irat, EREC_PldIratPeldanyok elsoPeldany, List<EREC_PldIratPeldanyok> peldanyok)
    {
        this._DataContext = dataContext;
        this._ExecParam = execParam;
        this.IktatoKonyv = iktatokonyv;
        this.Ugyirat = ugyirat;
        this.Irat = irat;
        this.ElsoPeldany = elsoPeldany;
        this.Peldanyok = peldanyok;
    }

    EREC_PldIratPeldanyok CreatePeldany(EREC_PldIratPeldanyok peldany, bool generateBarCode, out string barCodeId)
    {
        EREC_PldIratPeldanyok newPeldany = CloneObject<EREC_PldIratPeldanyok>(this.ElsoPeldany);

        bool isTUK = Rendszerparameterek.GetBoolean(_ExecParam, Rendszerparameterek.TUK, false);

        if (isTUK)
        {
            newPeldany.IrattarId = peldany.IrattarId;
            newPeldany.Updated.IrattarId = true;
            newPeldany.IrattariHely = peldany.IrattariHely;
            newPeldany.Updated.IrattariHely = true;
        }

        newPeldany.Sorszam = peldany.Sorszam;
        newPeldany.Updated.Sorszam = true;

        newPeldany.Partner_Id_Cimzett = peldany.Partner_Id_Cimzett;
        newPeldany.Updated.Partner_Id_Cimzett = true;

        // BUG_13213
        newPeldany.Partner_Id_CimzettKapcsolt = peldany.Partner_Id_CimzettKapcsolt;
        newPeldany.Updated.Partner_Id_CimzettKapcsolt = true;

        newPeldany.NevSTR_Cimzett = peldany.NevSTR_Cimzett;
        newPeldany.Updated.NevSTR_Cimzett = true;

        newPeldany.Cim_id_Cimzett = peldany.Cim_id_Cimzett;
        newPeldany.Updated.Partner_Id_Cimzett = true;

        newPeldany.CimSTR_Cimzett = peldany.CimSTR_Cimzett;
        newPeldany.Updated.NevSTR_Cimzett = true;

        newPeldany.Eredet = peldany.Eredet;
        newPeldany.Updated.Eredet = true;

        // Azonosító (Iktatószám) beállítása:
        newPeldany.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, IktatoKonyv, Ugyirat, Irat, newPeldany);
        newPeldany.Updated.Azonosito = true;

        barCodeId = String.Empty;
        string barCode = String.Empty;
        if (generateBarCode)
        {
            KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this._DataContext);
            ExecParam execParam_barkod = _ExecParam.Clone();
            Result result_barkod_pld = service_barkodok.BarkodGeneralas(execParam_barkod);
            if (!String.IsNullOrEmpty(result_barkod_pld.ErrorCode))
            {
                // hiba:
                Logger.Error("Vonalkód generálás az iratpéldánynak: Sikertelen", execParam_barkod);
                throw new ResultException(result_barkod_pld);
            }
            else
            {
                barCodeId = result_barkod_pld.Uid;
                barCode = (String)result_barkod_pld.Record;
            }

        }

        newPeldany.BarCode = barCode;
        newPeldany.Updated.BarCode = true;

        newPeldany.KuldesMod = peldany.KuldesMod;
        newPeldany.Updated.KuldesMod = true;

        return newPeldany;
    }

    void UpdateElsoPeldany(EREC_PldIratPeldanyok peldany)
    {
        this.ElsoPeldany.Partner_Id_Cimzett = peldany.Partner_Id_Cimzett;
        this.ElsoPeldany.Updated.Partner_Id_Cimzett = true;

		// BUG_13213
        this.ElsoPeldany.Partner_Id_CimzettKapcsolt = peldany.Partner_Id_CimzettKapcsolt;
        this.ElsoPeldany.Updated.Partner_Id_CimzettKapcsolt = true;

        this.ElsoPeldany.NevSTR_Cimzett = peldany.NevSTR_Cimzett;
        this.ElsoPeldany.Updated.NevSTR_Cimzett = true;

        if (!string.IsNullOrEmpty(peldany.Eredet))
        {
            this.ElsoPeldany.Eredet = peldany.Eredet;
            this.ElsoPeldany.Updated.Eredet = true;
        }

        bool isTUK = Rendszerparameterek.GetBoolean(_ExecParam, Rendszerparameterek.TUK, false);

        if (isTUK)
        {
            this.ElsoPeldany.IrattarId = peldany.IrattarId;
            this.ElsoPeldany.Updated.IrattarId = true;
            this.ElsoPeldany.IrattariHely = peldany.IrattariHely;
            this.ElsoPeldany.Updated.IrattariHely = true;
        }
    }

    public Result InsertPeldanyok(out int utolsoSorszam, out bool vanElsoPeldany)
    {
        EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = new EREC_PldIratPeldanyokService(this._DataContext);
        ExecParam execParam_IratPeldanyInsert = _ExecParam.Clone();
        Result result = new Result();
        utolsoSorszam = 0;
        vanElsoPeldany = false;

        foreach (EREC_PldIratPeldanyok peldany in Peldanyok.OrderBy(p => p.Typed.Sorszam.Value))
        {
            utolsoSorszam = peldany.Typed.Sorszam.Value;
            //első iratpéldányt nem itt hozzuk létre
            if (peldany.Sorszam == "1")
            {
                UpdateElsoPeldany(peldany);
                vanElsoPeldany = true;
                continue;
            }

            bool generateBarcode = true;

            string barcodeId;
            EREC_PldIratPeldanyok newPeldany = CreatePeldany(peldany, generateBarcode, out barcodeId);

            result = erec_PldIratPeldanyokService.Insert(
                        execParam_IratPeldanyInsert, newPeldany);

            if (result.IsError)
            {
                return result;
            }

            string newpldId = result.Uid;

            if (generateBarcode)
            {
                KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this._DataContext);
                ExecParam execParam_barcodeUpdate = _ExecParam.Clone();

                Result result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                        execParam_barcodeUpdate, barcodeId, newpldId);

                if (!String.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
                {
                    Logger.Error("Hiba: Barkod rekordhoz iratpéldány hozzákötése", execParam_barcodeUpdate);
                    throw new ResultException(result_barcodeUpdate);
                }
            }
        }

        return result;
    }

    public bool VanElsoIratpeldany()
    {
        return Peldanyok.Select(p => p.Sorszam).Contains("1");
    }

    private static T CloneObject<T>(T obj) where T : class
    {
        MemoryStream ms = new MemoryStream();
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        serializer.Serialize(ms, obj);
        ms.Position = 0;
        object result = serializer.Deserialize(ms);
        ms.Close();
        return (T)result;
    }

}