﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SOAPUtility
/// </summary>
public static class SOAPUtility
{
    public static string GetInputStream()
    {
        byte[] inputStream = new byte[HttpContext.Current.Request.ContentLength];
        //Get current stream position so we can set it back to that after logging 
        Int64 currentStreamPosition = HttpContext.Current.Request.InputStream.Position;
        HttpContext.Current.Request.InputStream.Position = 0;
        HttpContext.Current.Request.InputStream.Read(inputStream, 0, HttpContext.Current.Request.ContentLength);
        //Set back stream position to original position 
        HttpContext.Current.Request.InputStream.Position = currentStreamPosition;
        string xml = System.Text.Encoding.UTF8.GetString(inputStream);
        return xml;
    }

    public static void LogInputStream()
    {
        string fileName = String.Format(@"C:\Contentum.Net\Temp\SOAP\request_{0}.xml", DateTime.Now.ToString("yyyyMMHH_hhmmss_fff"));
        string xml = GetInputStream();

        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
        doc.LoadXml(xml);

        Contentum.eUtility.XmlHelper.RemoveEmptyNodes(doc);

        doc.Save(fileName);
        //System.IO.File.WriteAllText(fileName, xml, System.Text.Encoding.UTF8);

    }
}