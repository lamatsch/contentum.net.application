﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for AlairasokStoredProcedure
/// </summary>
public class AlairasokStoredProcedure
{
    private DataContext dataContext;

    public AlairasokStoredProcedure(DataContext _dataContext)
	{
        this.dataContext = _dataContext;
	}

    public Result GetAll_PKI_MuveletAlairasLista(ExecParam execParam, string alkalmazasKod, string folyamatKod, string muveletKod, string alairoSzerep, bool titkositas)
    {
        Log log = Log.SpStart(execParam, "sp_PKI_MuveletAlairasLista");

        Result _ret = new Result();

        try
        {           
            SqlCommand SqlComm = new SqlCommand("[sp_PKI_MuveletAlairasLista]");
            SqlComm.CommandType = CommandType.StoredProcedure;

            SqlComm.Parameters.Add(new SqlParameter("@Felhasznalo_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Felhasznalo_Id"].Value = execParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@AlkalmazasKod", SqlDbType.NVarChar,100);
            SqlComm.Parameters["@AlkalmazasKod"].Value = (string.IsNullOrEmpty(alkalmazasKod)) ? System.Data.SqlTypes.SqlString.Null : alkalmazasKod;

            SqlComm.Parameters.Add("@FolyamatKod", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@FolyamatKod"].Value = folyamatKod;

            SqlComm.Parameters.Add("@MuveletKod", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@MuveletKod"].Value = muveletKod;

            SqlComm.Parameters.Add("@AlairoSzerep", SqlDbType.NVarChar, 64);
            SqlComm.Parameters["@AlairoSzerep"].Value = (string.IsNullOrEmpty(alairoSzerep)) ? System.Data.SqlTypes.SqlString.Null : alairoSzerep;                

            SqlComm.Parameters.Add("@Titkositas", SqlDbType.Char, 1);
            SqlComm.Parameters["@Titkositas"].Value = titkositas ? '1' : '0';

            // Output paraméterek:

            SqlComm.Parameters.Add("@ResultAlairasEASZkd", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@ResultAlairasEASZkd"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@ResultTanusitvanyLn", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@ResultTanusitvanyLn"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@ResultAlairSzabaly", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ResultAlairSzabaly"].Direction = ParameterDirection.Output;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetAll_PKI_ElojegyzettAlairas(ExecParam execParam, string alkalmazasKod, string objektumTip, string objektum_Id)
    {
        Log log = Log.SpStart(execParam, "sp_PKI_ElojegyzettAlairas");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_PKI_ElojegyzettAlairas]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Parameters.Add(new SqlParameter("@Felhasznalo_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Felhasznalo_Id"].Value = execParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@AlkalmazasKod", System.Data.SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@AlkalmazasKod"].Value = (string.IsNullOrEmpty(alkalmazasKod)) ? System.Data.SqlTypes.SqlString.Null : alkalmazasKod;
            
            SqlComm.Parameters.Add("@ObjektumTip", SqlDbType.NVarChar, 4);
            SqlComm.Parameters["@ObjektumTip"].Value = objektumTip;

            SqlComm.Parameters.Add("@Objektum_Id", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@Objektum_Id"].Value = new System.Data.SqlTypes.SqlGuid(objektum_Id);

            // Output paraméterek:

            SqlComm.Parameters.Add("@ResultAlairasEASZkd", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@ResultAlairasEASZkd"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@ResultTanusitvanyLn", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@ResultTanusitvanyLn"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@ResultAlairSzabaly", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ResultAlairSzabaly"].Direction = ParameterDirection.Output;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;

    }



    

    public Result GetAll_PKI_IratAlairas(ExecParam execParam, string alkalmazasKod, string objektumTip, string objektum_Id
        , string folyamatKod, string muveletKod, string alairoSzerep)
    {
        Log log = Log.SpStart(execParam, "sp_PKI_IratAlairas");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_PKI_IratAlairas]");
            SqlComm.CommandType = CommandType.StoredProcedure;

            SqlComm.Parameters.Add(new SqlParameter("@Felhasznalo_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Felhasznalo_Id"].Value = execParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@AlkalmazasKod", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@AlkalmazasKod"].Value = (string.IsNullOrEmpty(alkalmazasKod)) ? System.Data.SqlTypes.SqlString.Null : alkalmazasKod;

            SqlComm.Parameters.Add("@ObjektumTip", SqlDbType.NVarChar, 4);
            SqlComm.Parameters["@ObjektumTip"].Value = objektumTip;

            SqlComm.Parameters.Add("@Objektum_Id", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@Objektum_Id"].Value = new System.Data.SqlTypes.SqlGuid(objektum_Id);

            SqlComm.Parameters.Add("@FolyamatKod", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@FolyamatKod"].Value = folyamatKod;

            SqlComm.Parameters.Add("@MuveletKod", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@MuveletKod"].Value = muveletKod;

            SqlComm.Parameters.Add("@AlairoSzerep", SqlDbType.NVarChar, 64);
            SqlComm.Parameters["@AlairoSzerep"].Value = (string.IsNullOrEmpty(alairoSzerep)) ? System.Data.SqlTypes.SqlString.Null : alairoSzerep;
                       

            // Output paraméterek:

            SqlComm.Parameters.Add("@ResultAlairasEASZkd", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@ResultAlairasEASZkd"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@ResultTanusitvanyLn", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@ResultTanusitvanyLn"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@ResultAlairSzabaly", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ResultAlairSzabaly"].Direction = ParameterDirection.Output;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetAll_PKI_AlairasAdatok(ExecParam execParam)
    {
        Log log = Log.SpStart(execParam, "sp_PKI_AlairasAdatok");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_PKI_AlairasAdatok]");
            SqlComm.CommandType = CommandType.StoredProcedure;

            SqlComm.Parameters.Add(new SqlParameter("@Dokumentum_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Dokumentum_Id"].Value = execParam.Typed.Record_Id;

            // Output paraméterek:

            /*SqlComm.Parameters.Add("@AlairtFajlnev", SqlDbType.NVarChar, 400);
            SqlComm.Parameters["@AlairtFajlnev"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@ElekrtonikusAlairas", SqlDbType.NVarChar, 400);
            SqlComm.Parameters["@ElekrtonikusAlairas"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@BelsoTipus", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@BelsoTipus"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@Idopecset", SqlDbType.DateTime);
            SqlComm.Parameters["@Idopecset"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@AlairasTulajdonos", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@AlairasTulajdonos"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@AlairasRendben", SqlDbType.Char, 1);
            SqlComm.Parameters["@AlairasRendben"].Direction = ParameterDirection.Output;

            SqlComm.Parameters.Add("@AlairasVeglegRendben", SqlDbType.Char, 1);
            SqlComm.Parameters["@AlairasVeglegRendben"].Direction = ParameterDirection.Output;*/

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;

    }




    public Result GetAll_PKI_IratAlairoSzabalyok(ExecParam execParam, string felhasznaloId_Alairo, string alkalmazasKod, string objektumTip, string objektum_Id
        , string folyamatKod, string muveletKod, string alairoSzerep)
    {
        Log log = Log.SpStart(execParam, "sp_PKI_IratAlairoSzabalyok ");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_PKI_IratAlairoSzabalyok]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Parameters.Add(new SqlParameter("@Felhasznalo_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Felhasznalo_Id"].Value = new System.Data.SqlTypes.SqlGuid(felhasznaloId_Alairo);

            SqlComm.Parameters.Add("@AlkalmazasKod", System.Data.SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@AlkalmazasKod"].Value = (string.IsNullOrEmpty(alkalmazasKod)) ? System.Data.SqlTypes.SqlString.Null : alkalmazasKod;

            SqlComm.Parameters.Add("@ObjektumTip", SqlDbType.NVarChar, 4);
            SqlComm.Parameters["@ObjektumTip"].Value = objektumTip;

            SqlComm.Parameters.Add("@Objektum_Id", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@Objektum_Id"].Value = new System.Data.SqlTypes.SqlGuid(objektum_Id);

            SqlComm.Parameters.Add("@FolyamatKod", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@FolyamatKod"].Value = folyamatKod;

            SqlComm.Parameters.Add("@MuveletKod", SqlDbType.NVarChar, 100);
            SqlComm.Parameters["@MuveletKod"].Value = (string.IsNullOrEmpty(muveletKod)) ? System.Data.SqlTypes.SqlString.Null : muveletKod;

            SqlComm.Parameters.Add("@AlairoSzerep", SqlDbType.NVarChar, 64);
            SqlComm.Parameters["@AlairoSzerep"].Value = (string.IsNullOrEmpty(alairoSzerep)) ? System.Data.SqlTypes.SqlString.Null : alairoSzerep;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }
}
