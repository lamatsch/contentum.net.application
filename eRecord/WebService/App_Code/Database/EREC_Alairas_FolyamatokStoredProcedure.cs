using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_AgazatiJelekStoredProcedure
/// </summary>
public partial class EREC_Alairas_FolyamatokStoredProcedure
{

    public Result GetToBeSignedDocumentsURL(ExecParam ExecParam, Guid proc_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_DokumentumAdatokGetToBeSignedDocumentsURL");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_DokumentumAdatokGetToBeSignedDocumentsURL]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@Proc_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Proc_Id"].Value = proc_Id;


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

}
