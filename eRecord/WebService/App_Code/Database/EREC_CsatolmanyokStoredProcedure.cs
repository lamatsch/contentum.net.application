﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for EREC_CsatolmanyokStoredProcedure
/// </summary>
public partial class EREC_CsatolmanyokStoredProcedure
{

    public Result GetAllWithExtension(ExecParam ExecParam, EREC_CsatolmanyokSearch _EREC_CsatolmanyokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_CsatolmanyokGetAllWithExtension");
        Result _ret = new Result();
       
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_CsatolmanyokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_CsatolmanyokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_CsatolmanyokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
			SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_CsatolmanyokSearch.OrderBy, _EREC_CsatolmanyokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    public Result GetAllByDokumentum(ExecParam execParam, string dokumentumId, EREC_CsatolmanyokSearch erec_CsatolmanyokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_CsatolmanyokGetAllByDokumentum");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(erec_CsatolmanyokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += erec_CsatolmanyokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_CsatolmanyokGetAllByDokumentum]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
			SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, erec_CsatolmanyokSearch.OrderBy, erec_CsatolmanyokSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@DokumentumId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@DokumentumId"].Value = Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(dokumentumId, System.Data.SqlTypes.SqlGuid.Null);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result TomegesInvalidate(ExecParam ExecParam, DateTime ExecutionTime, EREC_CsatolmanyokSearch _EREC_CsatolmanyokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_CsatolmanyokTomegesInvalidate");

        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_CsatolmanyokSearch);

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_CsatolmanyokTomegesInvalidate]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where"].Value = query.Where;

            Utility.AddExecuteDefaultParameter(SqlComm, ExecParam, ExecutionTime);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

}
