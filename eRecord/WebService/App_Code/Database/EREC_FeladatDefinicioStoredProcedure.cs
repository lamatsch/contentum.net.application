using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for EREC_FeladatDefinicioStoredProcedure
/// </summary>

public partial class EREC_FeladatDefinicioStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_FeladatDefinicioSearch _EREC_FeladatDefinicioSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_FeladatDefinicioGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_FeladatDefinicioSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_FeladatDefinicioSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_FeladatDefinicioGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_FeladatDefinicioSearch.OrderBy, _EREC_FeladatDefinicioSearch.TopRow);

            // Felhaszn�l� szervezete a jogosults�gsz�r�shez
            SqlComm.Parameters.Add("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}