using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for EREC_FeladatErtesitesStoredProcedure
/// </summary>
public partial class EREC_FeladatErtesitesStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_FeladatErtesitesSearch _EREC_FeladatErtesitesSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_FeladatErtesitesGetAllWithExtension");
        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_FeladatErtesitesSearch);

            query.Where += _EREC_FeladatErtesitesSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_FeladatErtesitesGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_FeladatErtesitesSearch.OrderBy, _EREC_FeladatErtesitesSearch.TopRow);

            SqlComm.Parameters.Add("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}