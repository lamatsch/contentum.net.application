using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for EREC_Hatarid_ObjektumokStoredProcedure
/// </summary>

public partial class EREC_Hatarid_ObjektumokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_Hatarid_ObjektumokSearch _EREC_Hatarid_ObjektumokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Hatarid_ObjektumokGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_Hatarid_ObjektumokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_Hatarid_ObjektumokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Hatarid_ObjektumokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Hatarid_ObjektumokSearch.OrderBy, _EREC_Hatarid_ObjektumokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}