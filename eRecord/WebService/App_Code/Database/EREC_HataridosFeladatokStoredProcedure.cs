using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for EREC_HataridosFeladatokStoredProcedure
/// </summary>

public partial class EREC_HataridosFeladatokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_HataridosFeladatokSearch _EREC_HataridosFeladatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_HataridosFeladatokGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            string parentFeladatId = String.Empty;

            if (_EREC_HataridosFeladatokSearch.DisplayChildsIfParentNotVisible)
            {
                if (!String.IsNullOrEmpty(_EREC_HataridosFeladatokSearch.HataridosFeladat_Id.Value) && _EREC_HataridosFeladatokSearch.HataridosFeladat_Id.Operator == Query.Operators.equals)
                {
                    parentFeladatId = _EREC_HataridosFeladatokSearch.HataridosFeladat_Id.Value;
                }
                _EREC_HataridosFeladatokSearch.HataridosFeladat_Id.Value = String.Empty;
                _EREC_HataridosFeladatokSearch.HataridosFeladat_Id.Operator = String.Empty;
            }

            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_HataridosFeladatokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_HataridosFeladatokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_HataridosFeladatokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
			SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_HataridosFeladatokSearch.OrderBy, _EREC_HataridosFeladatokSearch.TopRow);

            // Felhaszn�l� szervezete a jogosults�gsz�r�shez
            SqlComm.Parameters.Add("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            if (_EREC_HataridosFeladatokSearch.DisplayChildsIfParentNotVisible)
            {
                SqlComm.Parameters.Add("@DisplayChildsIfParentNotVisible", SqlDbType.Char, 1).Value = Contentum.eUtility.Constants.Database.Yes;
            }

            if (_EREC_HataridosFeladatokSearch.Jogosultak)
            {
                SqlComm.Parameters.Add("@Jogosultak", SqlDbType.Char, 1).Value = "1";
            }
            if (!String.IsNullOrEmpty(parentFeladatId))
            {
                SqlComm.Parameters.Add("@ParentFeladatId", SqlDbType.UniqueIdentifier).Value = SqlGuid.Parse(parentFeladatId);
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Feladatok_GetSummary(ExecParam ExecParam, string kolcsonzesreKezelhetoIrattarak, string kikeresreKezelhetoIrattarak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_Feladatok_GetSummary");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_Feladatok_GetSummary]");
            SqlComm.CommandType = CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            if (!String.IsNullOrEmpty(ExecParam.FelhasznaloSzervezet_Id))
            {
                SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;
            }

            SqlComm.Parameters.Add(new SqlParameter("@KolcsonzesreKezelhetoIrattarak", SqlDbType.NVarChar));
            SqlComm.Parameters["@KolcsonzesreKezelhetoIrattarak"].Value = kolcsonzesreKezelhetoIrattarak;

            SqlComm.Parameters.Add(new SqlParameter("@KikeresreKezelhetoIrattarak", SqlDbType.NVarChar));
            SqlComm.Parameters["@KikeresreKezelhetoIrattarak"].Value = kikeresreKezelhetoIrattarak;

            //Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_HataridosFeladatokSearch.OrderBy, _EREC_HataridosFeladatokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
            
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetCount(ExecParam ExecParam, string ObjektumId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_HataridosFeladatokGetCount");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_HataridosFeladatokGetCount]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@ObjektumId", SqlDbType.UniqueIdentifier).Value = SqlGuid.Parse(ObjektumId);
            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.Felhasznalo_Id;
            SqlComm.Parameters.Add("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllReszfeladat(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_HataridosFeladatokGetAllReszfeladat");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_HataridosFeladatokGetAllReszfeladat]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@FeladatId", SqlDbType.UniqueIdentifier).Value = SqlGuid.Parse(ExecParam.Record_Id);
            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.Felhasznalo_Id;
            SqlComm.Parameters.Add("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}