using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_IraIktatoKonyvekStoredProcedure
/// </summary>

public partial class EREC_IraIktatoKonyvekStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraIktatoKonyvekSearch _EREC_IraIktatoKonyvekSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIktatoKonyvekGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraIktatoKonyvekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IraIktatoKonyvekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIktatoKonyvekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraIktatoKonyvekSearch.OrderBy, _EREC_IraIktatoKonyvekSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
            SqlComm.Parameters["@Jogosultak"].Size = 1;
            SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';


            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraIktatoKonyvekSearch _EREC_IraIktatoKonyvekSearch)
    {
        return GetAllWithExtension(ExecParam, _EREC_IraIktatoKonyvekSearch, false);
    }

    public Result GetWithRightCheck(ExecParam ExecParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIktatoKonyvekGetWithRightCheck");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIktatoKonyvekGetWithRightCheck]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Jogszint", SqlDbType.Char));
            SqlComm.Parameters["@Jogszint"].Size = 1;
            SqlComm.Parameters["@Jogszint"].Value = Jogszint;

            EREC_IraIktatoKonyvek _EREC_IraIktatoKonyvek = new EREC_IraIktatoKonyvek();
            Utility.LoadBusinessDocumentFromDataAdapter(_EREC_IraIktatoKonyvek, SqlComm);
            _ret.Record = _EREC_IraIktatoKonyvek;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithIktathat(ExecParam ExecParam, EREC_IraIktatoKonyvekSearch _EREC_IraIktatoKonyvekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIktatoKonyvekGetAllWithIktathat");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraIktatoKonyvekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IraIktatoKonyvekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIktatoKonyvekGetAllWithIktathat]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraIktatoKonyvekSearch.OrderBy, _EREC_IraIktatoKonyvekSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }



    public Result GetAllWithIktathatByIrattariTetel(ExecParam ExecParam, EREC_IraIktatoKonyvekSearch _EREC_IraIktatoKonyvekSearch, string _EREC_IraIrattariTetel_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIktatoKonyvekGetAllWithIktathat");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraIktatoKonyvekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IraIktatoKonyvekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIktatoKonyvekGetAllWithIktathat]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraIktatoKonyvekSearch.OrderBy, _EREC_IraIktatoKonyvekSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            if (!String.IsNullOrEmpty(_EREC_IraIrattariTetel_Id))
            {

                System.Data.SqlTypes.SqlGuid irattariTetel_Id_Guid =
                    Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(_EREC_IraIrattariTetel_Id, System.Data.SqlTypes.SqlGuid.Null);

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Filter_IrattariTetelId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Filter_IrattariTetelId"].Value = irattariTetel_Id_Guid;

            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithCount(ExecParam ExecParam, EREC_IraIktatoKonyvekSearch _EREC_IraIktatoKonyvekSearch, string _KezdDate, string _VegeDate)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIktatoKonyvekGetAllWithCount");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraIktatoKonyvekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IraIktatoKonyvekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIktatoKonyvekGetAllWithCount]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlDateTime KezdDate_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                _KezdDate, SqlDateTime.Null);

            SqlDateTime VegeDate_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                _VegeDate, SqlDateTime.Null);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KezdDate", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@KezdDate"].Value = KezdDate_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VegeDate", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@VegeDate"].Value = VegeDate_sqltype;

            // Felhasználó szervezete a jogosultságszűréshez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraIktatoKonyvekSearch.OrderBy, _EREC_IraIktatoKonyvekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }



    public Result LezarasMasolas_Tomeges(ExecParam ExecParam, string Ids, string Ids_Copy, String IktatoErkezteto, bool? Auto)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIktatoKonyvek_LezarasMasolasTomeges");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIktatoKonyvek_LezarasMasolasTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Ids;

            SqlComm.Parameters.Add(new SqlParameter("@Ids_Copy", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids_Copy"].Value = Ids_Copy;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            if (!String.IsNullOrEmpty(IktatoErkezteto))
            {
                SqlComm.Parameters.Add(new SqlParameter("@IktatoErkezteto", SqlDbType.Char, 1));
                SqlComm.Parameters["@IktatoErkezteto"].Value = IktatoErkezteto;
            }

            //LZS - BUG_11964 - BUG_11965
            if (Auto != null)
            {
                SqlComm.Parameters.Add(new SqlParameter("@Auto", SqlDbType.Bit));
                SqlComm.Parameters["@Auto"].Value = Auto;
            }

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    #region Jelenleg nem kiajánlott szolgáltatások (szükség esetén bekapcsolhatók)

    public Result MasolKovetkezoEvre(ExecParam ExecParam, String IktatoErkezteto)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIktatoKonyvek_MasolasKovetkezoEvre");

        Result _ret = new Result();
        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_IraIktatoKonyvek_MasolasKovetkezoEvre]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);
            Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

            if (!String.IsNullOrEmpty(IktatoErkezteto))
            {
                SqlComm.Parameters.Add(new SqlParameter("@IktatoErkezteto", SqlDbType.Char, 1));
                SqlComm.Parameters["@IktatoErkezteto"].Value = IktatoErkezteto;
            }

            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }
    #endregion Jelenleg nem kiajánlott szolgáltatások (szükség esetén bekapcsolhatók)
}