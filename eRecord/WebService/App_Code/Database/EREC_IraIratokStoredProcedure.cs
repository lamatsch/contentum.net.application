using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data.SqlTypes;


/// <summary>
/// Summary description for EREC_IraIratokStoredProcedure
/// </summary>

public partial class EREC_IraIratokStoredProcedure
{

    private SqlCommand CreateSqlCommandForGetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_IraIratokSearch _EREC_IraIratokSearch, bool Jogosultak)
    {
        String str_where = "";
        Query query = new Query();
        query.BuildFromBusinessDocument(_EREC_IraIratokSearch, true);

        str_where += query.Where;

        //az egyedi szuresi feltel hozzaadasa!!!
        if (!String.IsNullOrEmpty(_EREC_IraIratokSearch.WhereByManual))
        {
            if (!String.IsNullOrEmpty(str_where))
            {
                str_where += " and ";
            }
            str_where += _EREC_IraIratokSearch.WhereByManual;
        }

        // bels� keres�si objektumok kibont�sa:

        if (_EREC_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch != null)
        {
            Query query_UgyUgyiratdarab = new Query();
            query_UgyUgyiratdarab.BuildFromBusinessDocument(_EREC_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch);
            query_UgyUgyiratdarab.Where += _EREC_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.WhereByManual;

            if (!String.IsNullOrEmpty(query_UgyUgyiratdarab.Where.Trim()))
            {
                if (!String.IsNullOrEmpty(str_where.Trim())) str_where += " and ";
                str_where += query_UgyUgyiratdarab.Where;
            }
        }
        if (_EREC_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch != null)
        {
            Query query_Kuldemeny = new Query();
            query_Kuldemeny.BuildFromBusinessDocument(_EREC_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch);
            query_Kuldemeny.Where += _EREC_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch.WhereByManual;

            if (!String.IsNullOrEmpty(query_Kuldemeny.Where.Trim()))
            {
                if (!String.IsNullOrEmpty(str_where.Trim())) str_where += " and ";
                str_where += query_Kuldemeny.Where;
            }
        }

        //#region FullTextSearch
        //string strContainsTargyszo = String.Empty;
        //string strContainsTargy = String.Empty;
        //string strWhere = String.Empty;
        ////if (!string.IsNullOrEmpty(_EREC_IraIratokSearch.Targy.Value))
        ////{
        ////    strContainsTargy = new FullTextSearch.SQLContainsCondition(_EREC_IraIratokSearch.Targy.Value).Normalized;
        ////    strWhere += "contains( EREC_IraIratok.Targy , '" + strContainsTargy + "' )";
        ////}
        //if (_EREC_IraIratokSearch.fts_targyszavak != null && !string.IsNullOrEmpty(_EREC_IraIratokSearch.fts_targyszavak.Filter))
        //{
        //    strContainsTargyszo = new FullTextSearch.SQLContainsCondition(_EREC_IraIratokSearch.fts_targyszavak.Filter).Normalized;
        //    if (!string.IsNullOrEmpty(strWhere))
        //    {
        //        strWhere += " and ";
        //    }
        //    strWhere += "contains( (EREC_ObjektumTargyszavai.Targyszo, EREC_ObjektumTargyszavai.Note, EREC_ObjektumTargyszavai.Ertek ), '" + strContainsTargyszo + "' )";
        //}

        //if (!string.IsNullOrEmpty(strWhere))
        //{
        //    if (!string.IsNullOrEmpty(str_where))
        //        str_where += " and " + strWhere;
        //    else
        //        str_where += strWhere;
        //}
        //#endregion

        SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIratokGetAllWithExtension]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
        SqlComm.Transaction = dataContext.Transaction;

        query.Where = str_where;
        Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraIratokSearch.OrderBy, _EREC_IraIratokSearch.TopRow);

        if (_EREC_IraIratokSearch.Extended_EREC_IratAlairokSearch != null)
        {
            Query query_Alairok = new Query();
            query_Alairok.BuildFromBusinessDocument(_EREC_IraIratokSearch.Extended_EREC_IratAlairokSearch);
            query_Alairok.Where += _EREC_IraIratokSearch.Extended_EREC_IratAlairokSearch.WhereByManual;

            if (!String.IsNullOrEmpty(query_Alairok.Where.Trim()))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Alairok", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_Alairok"].Value = query_Alairok.Where;
            }
        }

        if (_EREC_IraIratokSearch.Extended_EREC_PldIratPeldanyokSearch != null)
        {
            Query query_Pld = new Query();
            query_Pld.BuildFromBusinessDocument(_EREC_IraIratokSearch.Extended_EREC_PldIratPeldanyokSearch);
            query_Pld.Where += _EREC_IraIratokSearch.Extended_EREC_PldIratPeldanyokSearch.WhereByManual;

            if (!String.IsNullOrEmpty(query_Pld.Where.Trim()))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Pld", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_Pld"].Value = query_Pld.Where;
            }
        }

        // Felhaszn�l� szervezete a jogosults�gsz�r�shez
        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

        if (!String.IsNullOrEmpty(_EREC_IraIratokSearch.ObjektumTargyszavai_ObjIdFilter))
        {
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ObjektumTargyszavai_ObjIdFilter", System.Data.SqlDbType.NVarChar, 4000));
            SqlComm.Parameters["@ObjektumTargyszavai_ObjIdFilter"].Value = _EREC_IraIratokSearch.ObjektumTargyszavai_ObjIdFilter;
        }


        // CR#2035: feleslegess� v�lt, mert az EREC_IraIratok.Targy FTSString t�pus�
        //if (_EREC_IraIratokSearch.fts_targy != null && !string.IsNullOrEmpty(_EREC_IraIratokSearch.fts_targy.Filter))
        //{
        //    string strContains = new Contentum.eQuery.FullTextSearch.SQLContainsCondition(_EREC_IraIratokSearch.fts_targy.Filter).Normalized;

        //    SqlComm.Parameters.Add(new SqlParameter("@FullTextSearch_Targy", SqlDbType.NVarChar));
        //    SqlComm.Parameters["@FullTextSearch_Targy"].Value = strContains;
        //}

        SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
        SqlComm.Parameters["@Jogosultak"].Size = 1;
        SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';

        if (!String.IsNullOrEmpty(_EREC_IraIratokSearch.AlSzervezetId))
        {
            SqlComm.Parameters.Add("@AlSzervezetId", SqlDbType.UniqueIdentifier).Value =
                Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(_EREC_IraIratokSearch.AlSzervezetId, System.Data.SqlTypes.SqlGuid.Null);
        }

        if (_EREC_IraIratokSearch.Extended_EREC_IraOnkormAdatokSearch != null)
        {
            Query query_EREC_IraOnkormAdatok = new Query();
            query_EREC_IraOnkormAdatok.BuildFromBusinessDocument(_EREC_IraIratokSearch.Extended_EREC_IraOnkormAdatokSearch);
            query_EREC_IraOnkormAdatok.Where += _EREC_IraIratokSearch.Extended_EREC_IraOnkormAdatokSearch.WhereByManual;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_EREC_IraOnkormAdatok", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where_EREC_IraOnkormAdatok"].Value = query_EREC_IraOnkormAdatok.Where;
        }

        if (_EREC_IraIratokSearch.IsUgyiratTerkepQuery)
        {
            SqlComm.Parameters.Add(new SqlParameter("@IsUgyiratTerkepQuery", SqlDbType.Char));
            SqlComm.Parameters["@IsUgyiratTerkepQuery"].Size = 1;
            SqlComm.Parameters["@IsUgyiratTerkepQuery"].Value = _EREC_IraIratokSearch.IsUgyiratTerkepQuery ? '1' : '0';
        }

        SqlComm.Parameters.Add(new SqlParameter("@Csoporttagokkal", SqlDbType.Bit));
        SqlComm.Parameters["@Csoporttagokkal"].Value = _EREC_IraIratokSearch.Csoporttagokkal ? 1 : 0;

        SqlComm.Parameters.Add(new SqlParameter("@CsakAktivIrat", SqlDbType.Bit));
        SqlComm.Parameters["@CsakAktivIrat"].Value = _EREC_IraIratokSearch.CsakAktivIrat ? 1 : 0;

        //LZS
        SqlComm.Parameters.Add(new SqlParameter("@CsatolmanySzures", SqlDbType.Int));
        SqlComm.Parameters["@CsatolmanySzures"].Value = _EREC_IraIratokSearch.CsatolmanySzures.Value;

        return SqlComm;
    }

    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraIratokSearch _EREC_IraIratokSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIratokGetAllWithExtension");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            SqlCommand SqlComm = CreateSqlCommandForGetAllWithExtensionAndJogosultak(ExecParam, _EREC_IraIratokSearch, Jogosultak);
            SqlComm.Parameters["@Where"].Size = -1; // nvarchar(max)

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllWithExtensionAndMeta(ExecParam ExecParam, EREC_IraIratokSearch _EREC_IraIratokSearch, String ColumnNames, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIratokGetAllWithExtension");

        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = CreateSqlCommandForGetAllWithExtensionAndJogosultak(ExecParam, _EREC_IraIratokSearch, Jogosultak);

            if (!String.IsNullOrEmpty(ColumnNames))
            {
                SqlComm.Parameters.Add(new SqlParameter("@ColumnNames", SqlDbType.NVarChar));
                SqlComm.Parameters["@ColumnNames"].Value = ColumnNames;
            }

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllIratmozgasWithExtension(ExecParam execParam, string IratId, EREC_IraIratokSearch _EREC_IraIratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_IraIratokGetAllIratmozgasWithExtension");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIratokGetAllIratmozgasWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraIratokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IraIratokSearch.WhereByManual;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, _EREC_IraIratokSearch.OrderBy, _EREC_IraIratokSearch.TopRow);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IratId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IratId"].Value = SqlGuid.Parse(IratId);

            DataSet ds = new DataSet();
            if (!execParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetWithRightCheck(ExecParam ExecParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIratokGetWithRightCheck");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIratokGetWithRightCheck]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Jogszint", SqlDbType.Char));
            SqlComm.Parameters["@Jogszint"].Size = 1;
            SqlComm.Parameters["@Jogszint"].Value = Jogszint;

            EREC_IraIratok _EREC_IraIratok = new EREC_IraIratok();
            Utility.LoadBusinessDocumentFromDataAdapter(_EREC_IraIratok, SqlComm);
            _ret.Record = _EREC_IraIratok;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }



    public Result GetAll_AtiktatasiLanc(ExecParam execParam, string iratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_IraIratokGetAll_AtiktatasiLanc");
        Result _ret = new Result();
               
        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIratokGetAll_AtiktatasiLanc]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;            


            SqlComm.Parameters.Add(new SqlParameter("@IratId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IratId"].Value = System.Data.SqlTypes.SqlGuid.Parse(iratId);


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    // BLG_619
    public Result GetIratFelelosSzervezetKod(ExecParam execParam, string iratid)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_GetIratFelelosSzervezetKod");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_GetIratFelelosSzervezetKod]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;


            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IratId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IratId"].Value = SqlGuid.Parse(iratid);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

            string szervezetKod = ds.Tables[0].Rows[0]["IratFelelos_SzervezetKod"].ToString();

            _ret.Record = szervezetKod;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetCsatolmanyJogosultak(ExecParam execParam, string iratId, bool kezi)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_IraIratokGetCsatolmanyJogosultak");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIratokGetCsatolmanyJogosultak]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;


            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IratId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IratId"].Value = SqlGuid.Parse(iratId);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Kezi", System.Data.SqlDbType.Int));
            SqlComm.Parameters["@Kezi"].Value = kezi ? 1 : 0;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    /// <summary>
    /// GetIratVoltElsoInduloSakkora
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <returns></returns>
    public Result GetIratVoltElsoInduloSakkora(ExecParam execParam, EREC_IraIratok irat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_IraIratok_VoltElsoInduloSakkora");
        Result _ret = new Result();
        if (irat == null)
            return _ret;

        bool voltElsoInduloSakkora = false;
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIratok_VoltElsoInduloSakkora]")
            {
                CommandType = CommandType.StoredProcedure,
                Connection = dataContext.Connection,
                Transaction = dataContext.Transaction,
                CommandTimeout = 300
            };

            SqlComm.Parameters.Add(new SqlParameter("@IratId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IratId"].Value = SqlGuid.Parse(irat.Id);

            SqlParameter resultParameter = new SqlParameter("@Result", SqlDbType.Bit)
            {
                Direction = ParameterDirection.Output
            };
            SqlComm.Parameters.Add(resultParameter);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter
            {
                SelectCommand = SqlComm
            };
            adapter.Fill(ds);

            voltElsoInduloSakkora = Convert.ToBoolean(SqlComm.Parameters["@Result"].Value);

            _ret.Ds = ds;
            _ret.Record = voltElsoInduloSakkora;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    /// <summary>
    /// "A joger�s�t� �ltal l�ttamozand� iratok list�ja" (SZ�R webservice-ekhez kell: SZURGetJogeroLattamozandoWS)
    /// Egyedi lek�rdez�: azokat az iratokat adja vissza, ahol be van �ll�tva a "Joger�s�tend�" mez�, ki van t�ltve a "Joger�s�t�st v�gz�", de a "Joger�s�t�st v�gz� l�tta" mez� m�g �res.
    /// (Mindh�rom mez� a metaadatok k�z�tt van.)
    /// (Az eredm�ny lista 'Obj_Id' mez�je tartalmazza az irat Id-t.)
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    public Result GetAllJogeroLattamozandoIratok(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "GetAllJogeroLattamozandoIratok");
        Result _ret = new Result();

        try
        {
            SqlCommand sqlComm = new SqlCommand();
            sqlComm.CommandType = System.Data.CommandType.Text;
            sqlComm.Connection = dataContext.Connection;
            sqlComm.Transaction = dataContext.Transaction;

            sqlComm.CommandText = @"select t1.Obj_Id, t1.ObjTip_Id, t1.BelsoAzonosito as tsz1_BelsoAzonosito, t1.Ertek as tsz1_Ertek
	                , t2.BelsoAzonosito as tsz2_BelsoAzonosito, t2.Ertek as tsz2Ertek
	                , t3.BelsoAzonosito as tsz3_BelsoAzonosito, t3.Ertek as tsz3Ertek
                from
                -- Joger�s�tend� = 'True'
                (select otsz.Obj_Id, otsz.ObjTip_Id, tsz.BelsoAzonosito, otsz.Ertek
                from erec_objektumtargyszavai otsz
	                join EREC_TargySzavak tsz on otsz.Targyszo_Id = tsz.id and tsz.BelsoAzonosito = 'Jogerositendo'
                where Ertek = 'True' 
	                and getdate() between otsz.ErvKezd and otsz.ErvVege) as t1
                join
                -- Joger�s�t�st v�gz�: nem null/nem �res
                (select otsz.Obj_Id, otsz.ObjTip_Id, tsz.BelsoAzonosito, otsz.Ertek
                from erec_objektumtargyszavai otsz
	                join EREC_TargySzavak tsz on otsz.Targyszo_Id = tsz.id and tsz.BelsoAzonosito = 'Jogerositest_vegzo'
                where Ertek is not null and Ertek != ''
	                and getdate() between otsz.ErvKezd and otsz.ErvVege) as t2 on t1.Obj_Id = t2.Obj_Id
                -- Joger�s�t�st v�gz� l�tta:
                left join (select otsz.Id, otsz.Obj_Id, otsz.ObjTip_Id, tsz.BelsoAzonosito, otsz.Ertek
                from erec_objektumtargyszavai otsz
	                join EREC_TargySzavak tsz on otsz.Targyszo_Id = tsz.id and tsz.BelsoAzonosito = 'Jogerositest_vegzo_latta'
                where getdate() between otsz.ErvKezd and otsz.ErvVege) as t3 on t1.Obj_Id = t3.Obj_Id
                -- Joger�s�t�st v�gz� l�tta: �res, vagy nem is l�tezik
                where (t3.Id is null or t3.Ertek is null or t3.Ertek = '')";

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    /// <summary>
    /// "A joger�s�t� �ltal l�ttamozand� iratok list�ja" 
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    public Result GetAllJogeroLattamozandoIratokSimple(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "GetAllJogeroLattamozandoIratok");
        Result _ret = new Result();

        try
        {
            SqlCommand sqlComm = new SqlCommand();
            sqlComm.CommandType = System.Data.CommandType.Text;
            sqlComm.Connection = dataContext.Connection;
            sqlComm.Transaction = dataContext.Transaction;

            sqlComm.CommandText =
                @"select t1.Obj_Id, t1.ObjTip_Id, t1.BelsoAzonosito as tsz1_BelsoAzonosito, t1.Ertek as tsz1_Ertek
	                , t2.BelsoAzonosito as tsz2_BelsoAzonosito, t2.Ertek as tsz2Ertek
	                , t3.BelsoAzonosito as tsz3_BelsoAzonosito, t3.Ertek as tsz3Ertek
                    from
                    -- Joger�s�tend� = 'True'
                    (select otsz.Obj_Id, otsz.ObjTip_Id, tsz.BelsoAzonosito, otsz.Ertek
                    from erec_objektumtargyszavai otsz
	                    join EREC_TargySzavak tsz on otsz.Targyszo_Id = tsz.id and tsz.BelsoAzonosito = 'Jogerositendo'
                    where Ertek = 'True' 
	                    and getdate() between otsz.ErvKezd and otsz.ErvVege) as t1
                    LEFT JOIN
	                    -- Joger�s�t�st v�gz�:
	                    (select otsz.Obj_Id, otsz.ObjTip_Id, tsz.BelsoAzonosito, otsz.Ertek
	                    from erec_objektumtargyszavai otsz
		                    join EREC_TargySzavak tsz on otsz.Targyszo_Id = tsz.id and tsz.BelsoAzonosito = 'Jogerositest_vegzo'
		                    where  getdate() between otsz.ErvKezd and otsz.ErvVege) as t2 on t1.Obj_Id = t2.Obj_Id
                    -- Joger�s�t�st v�gz� l�tta:
                    LEFT JOIN (select otsz.Id, otsz.Obj_Id, otsz.ObjTip_Id, tsz.BelsoAzonosito, otsz.Ertek
	                    from erec_objektumtargyszavai otsz
		                    JOIN EREC_TargySzavak tsz on otsz.Targyszo_Id = tsz.id and tsz.BelsoAzonosito = 'Jogerositest_vegzo_latta'
	                    where getdate() between otsz.ErvKezd and otsz.ErvVege) as t3 on t1.Obj_Id = t3.Obj_Id
                     -- Joger�s�t�st v�gz� l�tta: �res, vagy nem is l�tezik
                     WHERE (t3.Id is null or t3.Ertek IS NULL OR t3.Ertek = '')
                    ";

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetTomegesOlvasasiJog(ExecParam execParam, string[] iratIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_IraIratokGetTomegesOlvasasiJog");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIratokGetTomegesOlvasasiJog]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IratIds", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@IratIds"].Value = String.Join(",", iratIds);
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Felhasznalo_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Felhasznalo_Id"].Value = execParam.Typed.Felhasznalo_Id;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = execParam.Typed.FelhasznaloSzervezet_Id;


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }
}