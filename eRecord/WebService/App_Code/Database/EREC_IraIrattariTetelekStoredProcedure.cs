using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_IraIrattariTetelekStoredProcedure
/// </summary>

public partial class EREC_IraIrattariTetelekStoredProcedure
{
    #region BLG_2968
    public String CreateIrattariTetelUgyiratokDexXml(ExecParam ExecParam, EREC_IraIrattariTetelekSearch _EREC_IraIrattariTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_Create_IrattariTetelUgyiratok_DEX_XML");
        String _ret = String.Empty;

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraIrattariTetelekSearch);

            SqlCommand SqlComm = new SqlCommand("sp_Create_IrattariTetelUgyiratok_DEX_XML");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IrattariTetel_ID", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IrattariTetel_ID"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUser_ID", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUser_ID"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlParameter outParam = new SqlParameter();
            outParam.SqlDbType = System.Data.SqlDbType.Xml;
            outParam.ParameterName = "@MetadataXML";
            outParam.Direction = ParameterDirection.Output;
            outParam.Size = int.MaxValue;

            SqlComm.Parameters.Add(outParam);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret = outParam.Value.ToString();
        }
        catch (SqlException e)
        {
            _ret = e.Message;
        }

        log.SpEnd(ExecParam);
        return _ret;
    }
    #endregion

    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraIrattariTetelekSearch _EREC_IraIrattariTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIrattariTetelekGetAllWithExtension");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_EREC_IraIrattariTetelekSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                if (!String.IsNullOrEmpty(_EREC_IraIrattariTetelekSearch.WhereByManual))
                {
                    if (String.IsNullOrEmpty(query.Where))
                    {
                        query.Where = _EREC_IraIrattariTetelekSearch.WhereByManual;
                    }
                    else
                    {
                        query.Where += " and " + _EREC_IraIrattariTetelekSearch.WhereByManual;
                    }
                }

                SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIrattariTetelekGetAllWithExtension]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraIrattariTetelekSearch.OrderBy, _EREC_IraIrattariTetelekSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithExtensionWithIktatoKonyvId(ExecParam ExecParam, EREC_IraIrattariTetelekSearch _EREC_IraIrattariTetelekSearch, string iktatokonyvId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIrattariTetelekGetAllWithExtensionWithIktatoKonyvId");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraIrattariTetelekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (!String.IsNullOrEmpty(_EREC_IraIrattariTetelekSearch.WhereByManual))
            {
                if (String.IsNullOrEmpty(query.Where))
                {
                    query.Where = _EREC_IraIrattariTetelekSearch.WhereByManual;
                }
                else
                {
                    query.Where += " and " + _EREC_IraIrattariTetelekSearch.WhereByManual;
                }
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIrattariTetelekGetAllWithExtensionWithIktatoKonyvId]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraIrattariTetelekSearch.OrderBy, _EREC_IraIrattariTetelekSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@Iktatokonyv_Id", SqlDbType.UniqueIdentifier));
            if (!string.IsNullOrEmpty(iktatokonyvId))
            {
                SqlComm.Parameters["@Iktatokonyv_Id"].Value = new System.Data.SqlTypes.SqlGuid(iktatokonyvId);
            }
            else
            {
                SqlComm.Parameters["@Iktatokonyv_Id"].Value = DBNull.Value;
            }


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
    

    public Result GetAllWithIktathat(ExecParam ExecParam, EREC_IraIrattariTetelekSearch _EREC_IraIrattariTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIrattariTetelekGetAllWithIktathat");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraIrattariTetelekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (!String.IsNullOrEmpty(_EREC_IraIrattariTetelekSearch.WhereByManual))
            {
                if (String.IsNullOrEmpty(query.Where))
                {
                    query.Where = _EREC_IraIrattariTetelekSearch.WhereByManual;
                }
                else
                {
                    query.Where += " and " + _EREC_IraIrattariTetelekSearch.WhereByManual;
                }
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIrattariTetelekGetAllWithIktathat]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraIrattariTetelekSearch.OrderBy, _EREC_IraIrattariTetelekSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }


}