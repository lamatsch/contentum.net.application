using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_IraJegyzekekStoredProcedure
/// </summary>

public partial class EREC_IraJegyzekekStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraJegyzekekSearch _EREC_IraJegyzekekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraJegyzekekGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraJegyzekekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IraJegyzekekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraJegyzekekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraJegyzekekSearch.OrderBy, _EREC_IraJegyzekekSearch.TopRow);

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    //sp_Create_MetadataXML
    public Result Create_MetadataXML(ExecParam ExecParam, Guid jegyzekId, string atadasStatusz)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_Create_MetadataXML");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_Create_MetadataXML]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IraJegyzek_ID", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IraJegyzek_ID"].Value = jegyzekId;

            if (!String.IsNullOrEmpty(atadasStatusz))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Atadas_Statusz", System.Data.SqlDbType.VarChar, 15));
                SqlComm.Parameters["@Atadas_Statusz"].Value = atadasStatusz;
            }

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUser_ID", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUser_ID"].Value = ExecParam.Typed.Felhasznalo_Id;

            //output paraméter
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MetadataXML", System.Data.SqlDbType.Xml, -1));
            SqlComm.Parameters["@MetadataXML"].Direction = ParameterDirection.Output;

            SqlComm.ExecuteNonQuery();
            _ret.Record = SqlComm.Parameters["@MetadataXML"].Value;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}