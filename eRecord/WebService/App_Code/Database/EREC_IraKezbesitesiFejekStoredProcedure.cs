﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_IraKezbesitesiFejekStoredProcedure
/// </summary>
public partial class EREC_IraKezbesitesiFejekStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraKezbesitesiFejekSearch _EREC_IraKezbesitesiFejekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraKezbesitesiFejekGetAllWithExtension");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraKezbesitesiFejekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IraKezbesitesiFejekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraKezbesitesiFejekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraKezbesitesiFejekSearch.OrderBy, _EREC_IraKezbesitesiFejekSearch.TopRow);

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}
