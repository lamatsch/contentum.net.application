using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_IraKezbesitesiTetelekStoredProcedure
/// </summary>

public partial class EREC_IraKezbesitesiTetelekStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraKezbesitesiTetelekSearch _EREC_IraKezbesitesiTetelekSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraKezbesitesiTetelekGetAllWithExtension");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_IraKezbesitesiTetelekSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_IraKezbesitesiTetelekSearch.WhereByManual;
            }


            #region Plusz Where felt�telek:

            string where_kuld = "";
            string where_ugyirat = "";
            string where_pld = "";

            if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch != null)
            {
                Query query_kuld = new Query();
                query_kuld.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch);

                where_kuld = query_kuld.Where;

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
                {
                    Query query_kuld_ikt = new Query();
                    query_kuld_ikt.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch);

                    string where_kuld_ikt = query_kuld_ikt.Where;
                    where_kuld_ikt = where_kuld_ikt.Replace("EREC_IraIktatoKonyvek", "EREC_IraIktatoKonyvek_Kuld");

                    if (!String.IsNullOrEmpty(where_kuld_ikt))
                    {
                        if (String.IsNullOrEmpty(where_kuld))
                        {
                            where_kuld += where_kuld_ikt;
                        }
                        else
                        {
                            where_kuld += " and " + where_kuld_ikt;
                        }
                    }
                }
                // csere: EREC_KuldKuldemenyek <--> EREC_KuldKuldemenyek_Kuld
                where_kuld = where_kuld.Replace("EREC_KuldKuldemenyek", "EREC_KuldKuldemenyek_Kuld");
            }
            if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch != null)
            {
                Query query_ugyirat = new Query();
                query_ugyirat.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch);

                where_ugyirat = query_ugyirat.Where;

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
                {
                    Query query_ugyirat_ikt = new Query();
                    query_ugyirat_ikt.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch);

                    string where_ugyirat_ikt = query_ugyirat_ikt.Where;
                    where_ugyirat_ikt = where_ugyirat_ikt.Replace("EREC_IraIktatoKonyvek", "EREC_IraIktatoKonyvek_Ugyirat");

                    if (!String.IsNullOrEmpty(where_ugyirat_ikt))
                    {
                        if (String.IsNullOrEmpty(where_ugyirat))
                        {
                            where_ugyirat += where_ugyirat_ikt;
                        }
                        else
                        {
                            where_ugyirat += " and " + where_ugyirat_ikt;
                        }
                    }
                }

                // csere: EREC_UgyUgyiratok <--> EREC_UgyUgyiratok_Ugyirat
                where_ugyirat = where_ugyirat.Replace("EREC_UgyUgyiratok", "EREC_UgyUgyiratok_Ugyirat");
            }
            if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch != null)
            {
                Query query_pld = new Query();
                query_pld.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch);

                where_pld = query_pld.Where;

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch != null)
                {
                    Query query_pld_irat = new Query();
                    query_pld_irat.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch
                            .Extended_EREC_IraIratokSearch);

                    if (!String.IsNullOrEmpty(query_pld_irat.Where))
                    {
                        if (String.IsNullOrEmpty(where_pld))
                        {
                            where_pld += query_pld_irat.Where;
                        }
                        else
                        {
                            where_pld += " and " + query_pld_irat.Where;
                        }
                    }
                }

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch
                        .Extended_EREC_UgyUgyiratdarabokSearch != null)
                {
                    Query query_pld_ugyiratDarab = new Query();
                    query_pld_ugyiratDarab.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch
                        .Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch);

                    if (!String.IsNullOrEmpty(query_pld_ugyiratDarab.Where))
                    {
                        if (String.IsNullOrEmpty(where_pld))
                        {
                            where_pld += query_pld_ugyiratDarab.Where;
                        }
                        else
                        {
                            where_pld += " and " + query_pld_ugyiratDarab.Where;
                        }
                    }
                }

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch
                       .Extended_EREC_UgyUgyiratokSearch != null)
                {
                    Query query_pld_ugyirat = new Query();
                    query_pld_ugyirat.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch
                        .Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch);

                    if (!String.IsNullOrEmpty(query_pld_ugyirat.Where))
                    {
                        if (String.IsNullOrEmpty(where_pld))
                        {
                            where_pld += query_pld_ugyirat.Where;
                        }
                        else
                        {
                            where_pld += " and " + query_pld_ugyirat.Where;
                        }
                    }
                }

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
                {
                    Query query_pld_ikt = new Query();
                    query_pld_ikt.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch);

                    string where_pld_ikt = query_pld_ikt.Where;
                    where_pld_ikt = where_pld_ikt.Replace("EREC_IraIktatoKonyvek", "EREC_IraIktatoKonyvek_IratPld");

                    if (!String.IsNullOrEmpty(where_pld_ikt))
                    {
                        if (String.IsNullOrEmpty(where_pld))
                        {
                            where_pld += where_pld_ikt;
                        }
                        else
                        {
                            where_pld += " and " + where_pld_ikt;
                        }
                    }
                }

                /// csere:  EREC_PldIratPeldanyok <--> EREC_PldIratPeldanyok_IratPld
                ///         EREC_IraIratok <--> EREC_IraIratok_IratPld
                ///         EREC_UgyUgyiratdarabok <--> EREC_UgyUgyiratdarabok_IratPld
                ///         EREC_IraIktatoKonyvek <--> EREC_IraIktatoKonyvek_IratPld
                where_pld = where_pld.Replace("EREC_PldIratPeldanyok", "EREC_PldIratPeldanyok_IratPld");
                where_pld = where_pld.Replace("EREC_IraIratok", "EREC_IraIratok_IratPld");
                where_pld = where_pld.Replace("EREC_UgyUgyiratdarabok", "EREC_UgyUgyiratdarabok_IratPld");
                //where_pld = where_pld.Replace("EREC_IraIktatoKonyvek", "EREC_IraIktatoKonyvek_IratPld");
            }

            string where_extended = "";
            if (!String.IsNullOrEmpty(where_kuld) || !String.IsNullOrEmpty(where_ugyirat)
                || !String.IsNullOrEmpty(where_pld))
            {
                where_extended += " and ((";

                // k�ldem�nyek:
                where_extended += _EREC_IraKezbesitesiTetelekSearch.Obj_type.Name
                    + "='" + Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek + "' ";

                if (!String.IsNullOrEmpty(where_kuld))
                {
                    where_extended += " and " + where_kuld;
                }

                where_extended += ") or (";

                // �gyiratok:
                where_extended += _EREC_IraKezbesitesiTetelekSearch.Obj_type.Name
                    + "='" + Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok + "' ";

                if (!String.IsNullOrEmpty(where_ugyirat))
                {
                    where_extended += " and " + where_ugyirat;
                }

                where_extended += ") or (";


                //dosszi�k
                where_extended += _EREC_IraKezbesitesiTetelekSearch.Obj_type.Name
                    + "='" + Contentum.eUtility.Constants.TableNames.KRT_Mappak + "' ";
                where_extended += ") or (";


                //iratp�ld�nyok:
                where_extended += _EREC_IraKezbesitesiTetelekSearch.Obj_type.Name
                    + "='" + Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok + "' ";

                if (!String.IsNullOrEmpty(where_pld))
                {
                    where_extended += " and " + where_pld;
                }

                where_extended += "))";


            }

            query.Where += where_extended;

            #endregion


            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraKezbesitesiTetelekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraKezbesitesiTetelekSearch.OrderBy, _EREC_IraKezbesitesiTetelekSearch.TopRow);

            // Felhaszn�l� szervezete a jogosults�gsz�r�shez
            SqlComm.Parameters.Add("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            if (Jogosultak)
            {
                SqlComm.Parameters.Add("@Jogosultak", System.Data.SqlDbType.Char).Value = "1";
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result AltalanosKeresesGetAllWithExtension(ExecParam ExecParam, EREC_IraKezbesitesiTetelekSearch _EREC_IraKezbesitesiTetelekSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraKezbesitesiTetelekAltalanosKeresesGetAllWithExtension");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_IraKezbesitesiTetelekSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_IraKezbesitesiTetelekSearch.WhereByManual;
            }

            #region Plusz Where felt�telek:

            string where_kuld = "";
            string where_ugyirat = "";
            string where_pld = "";
            string where_mappa = "";

            #region EREC_KuldKuldemenyek
            if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch != null)
            {
                Query query_kuld = new Query();
                query_kuld.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch);

                where_kuld = query_kuld.Where;

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
                {
                    Query query_kuld_ikt = new Query();
                    query_kuld_ikt.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch);

                    string where_kuld_ikt = query_kuld_ikt.Where;

                    if (!String.IsNullOrEmpty(where_kuld_ikt))
                    {
                        if (String.IsNullOrEmpty(where_kuld))
                        {
                            where_kuld += where_kuld_ikt;
                        }
                        else
                        {
                            where_kuld += " and " + where_kuld_ikt;
                        }
                    }
                }
            }
            #endregion EREC_KuldKuldemenyek

            #region EREC_UgyUgyiratok
            if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch != null)
            {
                Query query_ugyirat = new Query();
                query_ugyirat.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch);

                where_ugyirat = query_ugyirat.Where;

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
                {
                    Query query_ugyirat_ikt = new Query();
                    query_ugyirat_ikt.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch);

                    string where_ugyirat_ikt = query_ugyirat_ikt.Where;

                    if (!String.IsNullOrEmpty(where_ugyirat_ikt))
                    {
                        if (String.IsNullOrEmpty(where_ugyirat))
                        {
                            where_ugyirat += where_ugyirat_ikt;
                        }
                        else
                        {
                            where_ugyirat += " and " + where_ugyirat_ikt;
                        }
                    }
                }
            }
            #endregion EREC_UgyUgyiratok

            #region EREC_PldIratPeldanyok
            if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch != null)
            {
                Query query_pld = new Query();
                query_pld.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch);

                where_pld = query_pld.Where;

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch != null)
                {
                    Query query_pld_irat = new Query();
                    query_pld_irat.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch
                            .Extended_EREC_IraIratokSearch);

                    if (!String.IsNullOrEmpty(query_pld_irat.Where))
                    {
                        if (String.IsNullOrEmpty(where_pld))
                        {
                            where_pld += query_pld_irat.Where;
                        }
                        else
                        {
                            where_pld += " and " + query_pld_irat.Where;
                        }
                    }
                }

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch
                        .Extended_EREC_UgyUgyiratdarabokSearch != null)
                {
                    Query query_pld_ugyiratDarab = new Query();
                    query_pld_ugyiratDarab.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch
                        .Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch);

                    if (!String.IsNullOrEmpty(query_pld_ugyiratDarab.Where))
                    {
                        if (String.IsNullOrEmpty(where_pld))
                        {
                            where_pld += query_pld_ugyiratDarab.Where;
                        }
                        else
                        {
                            where_pld += " and " + query_pld_ugyiratDarab.Where;
                        }
                    }
                }

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch
                       .Extended_EREC_UgyUgyiratokSearch != null)
                {
                    Query query_pld_ugyirat = new Query();
                    query_pld_ugyirat.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch
                        .Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch);

                    if (!String.IsNullOrEmpty(query_pld_ugyirat.Where))
                    {
                        if (String.IsNullOrEmpty(where_pld))
                        {
                            where_pld += query_pld_ugyirat.Where;
                        }
                        else
                        {
                            where_pld += " and " + query_pld_ugyirat.Where;
                        }
                    }
                }

                if (_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
                {
                    Query query_pld_ikt = new Query();
                    query_pld_ikt.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch);

                    string where_pld_ikt = query_pld_ikt.Where;

                    if (!String.IsNullOrEmpty(where_pld_ikt))
                    {
                        if (String.IsNullOrEmpty(where_pld))
                        {
                            where_pld += where_pld_ikt;
                        }
                        else
                        {
                            where_pld += " and " + where_pld_ikt;
                        }
                    }
                }
            }
            #endregion EREC_PldIratPeldanyok


            #region KRT_Mappak
            // PlaceHolder
            #endregion KRT_Mappak
            // CR#2035: feleslegess� v�lt, mert a kiterjesztett keres�objektumok megfelel� mez�i FTSString t�pus�ak
            #region fts felt�telek
            
            //if (_EREC_IraKezbesitesiTetelekSearch.fts_altalanos != null)
            //{
            //    string where_kuld_fts = "";
            //    string where_ugyirat_fts = "";
            //    string where_pld_fts = "";

            //    string strContains = new Contentum.eQuery.FullTextSearch.SQLContainsCondition(_EREC_IraKezbesitesiTetelekSearch.fts_altalanos.Filter).Normalized;
            //    if (!String.IsNullOrEmpty(strContains))
            //    {
            //        if (!String.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.Manual_Obj_type_Kuldemeny.Value))
            //        {
            //            where_kuld_fts = "contains( EREC_KuldKuldemenyek.NevSTR_Bekuldo , '" + strContains + "' )";

            //            if (String.IsNullOrEmpty(where_kuld))
            //            {
            //                where_kuld += where_kuld_fts;
            //            }
            //            else
            //            {
            //                where_kuld += " and " + where_kuld_fts;
            //            }
            //        }

            //        if (!String.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.Manual_Obj_type_Ugyirat.Value))
            //        {
            //            where_ugyirat_fts = "contains( EREC_UgyUgyiratok.Targy , '" + strContains + "' )";

            //            if (String.IsNullOrEmpty(where_ugyirat))
            //            {
            //                where_ugyirat += where_ugyirat_fts;
            //            }
            //            else
            //            {
            //                where_ugyirat += " and " + where_ugyirat_fts;
            //            }
            //        }

            //        if (!String.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.Manual_Obj_type_IratPeldany.Value))
            //        {
            //            where_pld_fts = "contains( EREC_IraIratok.Targy , '" + strContains + "' )";

            //            if (String.IsNullOrEmpty(where_pld))
            //            {
            //                where_pld += where_pld_fts;
            //            }
            //            else
            //            {
            //                where_pld += " and " + where_pld_fts;
            //            }
            //        }
            //    }
            //}
            #endregion fts felt�telek

            #endregion Plusz Where felt�telek

            #region ObjectMode speci�lis be�ll�t�sok
            if (_EREC_IraKezbesitesiTetelekSearch.isObjectMode == true)
            {
                // ha a fel�leten nem defini�ltunk sz�r�st, nem adunk �t felt�telt a k�zbes�t�si t�telekre
                // ("alap�rtelmezett" sz�r�s t�rl�se)
                if (_EREC_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectMode.IsDefaultWhere == true)
                {
                    query.Where = "";
                }

                // de: ha ki volt jel�lve objektum t�pus �s nincs r� k�l�n sz�r�s,
                // nem �res felt�telt adunk �t:
                if (!String.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.Manual_Obj_type_Kuldemeny.Value)
                    && String.IsNullOrEmpty(where_kuld))
                {
                    where_kuld = "1=1";
                }
                if (!String.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.Manual_Obj_type_Ugyirat.Value)
                    && String.IsNullOrEmpty(where_ugyirat))
                {
                    where_ugyirat = "1=1";
                }
                if (!String.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.Manual_Obj_type_IratPeldany.Value)
                    && String.IsNullOrEmpty(where_pld))
                {
                    where_pld = "1=1";
                }
                if (!String.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.Manual_Obj_type_Dosszie.Value)
                    && String.IsNullOrEmpty(where_mappa))
                {
                    where_mappa = "1=1";
                }
            }
            #endregion ObjectMode speci�lis be�ll�t�sok

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraKezbesitesiTetelekAltalanosKeresesGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraKezbesitesiTetelekSearch.OrderBy, _EREC_IraKezbesitesiTetelekSearch.TopRow);

            // Felhaszn�l� szervezete a jogosults�gsz�r�shez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_KuldKuldemenyek", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where_KuldKuldemenyek"].Size = 4000;
            SqlComm.Parameters["@Where_KuldKuldemenyek"].Value = where_kuld;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_PldIratPeldanyok", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where_PldIratPeldanyok"].Size = 4000;
            SqlComm.Parameters["@Where_PldIratPeldanyok"].Value = where_pld;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_UgyUgyiratok", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where_UgyUgyiratok"].Size = 4000;
            SqlComm.Parameters["@Where_UgyUgyiratok"].Value = where_ugyirat;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Mappak", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where_Mappak"].Size = 4000;
            SqlComm.Parameters["@Where_Mappak"].Value = where_mappa;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@isObjectMode", System.Data.SqlDbType.Char));
            SqlComm.Parameters["@isObjectMode"].Size = 1;
            SqlComm.Parameters["@isObjectMode"].Value = (_EREC_IraKezbesitesiTetelekSearch.isObjectMode == false ? '0' : '1');

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KezbesitesiTetelekSearchMode", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@KezbesitesiTetelekSearchMode"].Size = 30;
            SqlComm.Parameters["@KezbesitesiTetelekSearchMode"].Value = _EREC_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectMode.KezbesitesiTetelekSearchMode.ToString();

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
            SqlComm.Parameters["@Jogosultak"].Size = 1;
            SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result ForXml(ExecParam ExecParam, EREC_IraKezbesitesiTetelekSearch _EREC_IraKezbesitesiTetelekSearch, string AtadasIrany, string fejId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraKezbesitesiTetelekForXml");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IraKezbesitesiTetelekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraKezbesitesiTetelekForXml]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@AtadasIrany", System.Data.SqlDbType.NVarChar, 10));
            SqlComm.Parameters["@AtadasIrany"].Value = AtadasIrany;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@fejId", System.Data.SqlDbType.NVarChar, 40));
            SqlComm.Parameters["@fejId"].Value = fejId;
            
            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraKezbesitesiTetelekSearch.OrderBy, _EREC_IraKezbesitesiTetelekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result AtadasTomeges(ExecParam ExecParam, string Ids, string Vers)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraKezbesitesiTetelek_AtadasAtvetelTomeges");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraKezbesitesiTetelek_AtadasAtvetelTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Ids;

            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = Vers;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlComm.Parameters.Add(new SqlParameter("@Mode", SqlDbType.Char));
            SqlComm.Parameters["@Mode"].Size = 1;
            SqlComm.Parameters["@Mode"].Value = "0";

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Uid = ds.Tables[0].Rows[0][0].ToString();
            //SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result AtvetelTomeges(ExecParam ExecParam, string Ids, string Vers)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraKezbesitesiTetelek_AtadasAtvetelTomeges");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraKezbesitesiTetelek_AtadasAtvetelTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Ids;

            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = Vers;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlComm.Parameters.Add(new SqlParameter("@Mode", SqlDbType.Char));
            SqlComm.Parameters["@Mode"].Size = 1;
            SqlComm.Parameters["@Mode"].Value = "1";

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Uid = ds.Tables[0].Rows[0][0].ToString();
            //SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

}