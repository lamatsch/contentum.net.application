using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_IraOnkormAdatokStoredProcedure
/// </summary>

public partial class EREC_IraOnkormAdatokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraOnkormAdatokSearch _EREC_IraOnkormAdatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraOnkormAdatokGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IraOnkormAdatokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IraOnkormAdatokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraOnkormAdatokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IraOnkormAdatokSearch.OrderBy, _EREC_IraOnkormAdatokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetWithExtensionForAdatlap(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraOnkormAdatokGetWithExtensionForAdatlap");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraOnkormAdatokGetWithExtensionForAdatlap]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);
            _ret.Ds = ds;

            //_ret.Record = _EREC_IraOnkormAdatok;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}