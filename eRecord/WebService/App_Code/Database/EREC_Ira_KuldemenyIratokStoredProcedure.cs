using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_Ira_KuldemenyIratokStoredProcedure
/// </summary>

public partial class EREC_Ira_KuldemenyIratokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_Ira_KuldemenyIratokSearch _EREC_Ira_KuldemenyIratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Ira_KuldemenyIratokGetAllWithExtension");
        Result _ret = new Result();
        using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        {
            sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_EREC_Ira_KuldemenyIratokSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _EREC_Ira_KuldemenyIratokSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_EREC_Ira_KuldemenyIratokGetAllWithExtension]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                SqlComm.Connection = sqlConnection;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Ira_KuldemenyIratokSearch.OrderBy, _EREC_Ira_KuldemenyIratokSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            sqlConnection.Close();
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}