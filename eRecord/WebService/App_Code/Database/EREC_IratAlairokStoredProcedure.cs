using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_IratAlairokStoredProcedure
/// </summary>

public partial class EREC_IratAlairokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IratAlairokSearch _EREC_IratAlairokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IratAlairokGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IratAlairokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IratAlairokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IratAlairokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IratAlairokSearch.OrderBy, _EREC_IratAlairokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
    public Result HistoryGetAllRecordByObjId(ExecParam ExecParam, string ObjId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IratAlairokHistoryGetAllRecordByObjId");
        Result _ret = new Result();
        
        try
        {
            
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IratAlairokHistoryGetAllRecordByObjId]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Obj_Id", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Org", System.Data.SqlDbType.NVarChar));
            
            SqlComm.Parameters["@Obj_Id"].Value = ObjId ;
            SqlComm.Parameters["@Org"].Value = ExecParam.Org_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}