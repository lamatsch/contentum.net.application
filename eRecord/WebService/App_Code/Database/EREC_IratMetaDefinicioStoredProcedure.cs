using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_IratMetaDefinicioStoredProcedure
/// </summary>

public partial class EREC_IratMetaDefinicioStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IratMetaDefinicioSearch _EREC_IratMetaDefinicioSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IratMetaDefinicioGetAllWithExtension");
        Logger.Info("EREC_IratMetaDefinicioStoredProcedure.GetAllWithExtension start");
        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IratMetaDefinicioSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IratMetaDefinicioSearch.WhereByManual;

            Logger.Info("sp_EREC_IratMetaDefinicioGetAllWithExtension");
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IratMetaDefinicioGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IratMetaDefinicioSearch.OrderBy, _EREC_IratMetaDefinicioSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            Logger.Error(e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        Logger.Info("EREC_IratMetaDefinicioStoredProcedure.GetAllWithExtension end");
        return _ret;

    }


    /// <summary>
    /// sp_EREC_IratMetaDefinicioByContentTypeGet hivasa.
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="contentType">A content type.</param>
    /// <returns>Result - a Uid mezobe kerul a fuggveny altal visszaadott guid.</returns>
    public Result GetIdByContentType(ExecParam ExecParam, String contentType)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IratMetaDefinicioByContentTypeGet");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IratMetaDefinicioByContentTypeGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@ContentType", SqlDbType.NVarChar));
            SqlComm.Parameters["@ContentType"].Size = 4000;
            SqlComm.Parameters["@ContentType"].Value = contentType;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = System.Data.SqlTypes.SqlGuid.Parse(ExecParam.Felhasznalo_Id);
                        
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Uid = Convert.ToString(ds.Tables[0].Rows[0][0]);
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    /// <summary>
    /// IratMetaDefinicioTerkephez meghat�rozza alulr�l felfel� hierarchikusan
    /// azon objektum Id-ket, melyek megfelelenek az �tadott keres�si felt�teleknek
    /// Ha nincs megadva felt�tel, �res t�bl�t ad vissza az �sszes Id helyett,
    /// az als�bb szinteken megfogalmazott felt�telekb�l sz�rmaz� sz�r�si
    /// t�bl�kkal akkor is megsz�ri a magasabb szinteket, ha azokra nincs egy�b
    /// felt�tel
    /// Nyilv�ntartja, ha egy szinten a felt�tel mellett m�r nem volt tal�lat,
    /// �gy megk�l�nb�ztethet� a felt�tel n�lk�li �s a felt�tel miatt �res
    /// sz�r�t�bla, �s nincs tov�bbi felesleges lek�r�s
    /// Visszaadott �rt�kek:
    ///    Table[0]: IsFilteredWithoutHit: '1', ha valamely felt�tel miatt nem volt tal�lat, 1 egy�bk�nt
    ///    Sz�r�t�bl�k (�res vagy Id-k, rendezve)
    ///    Table[0]: IsFilteredWithoutHit: '1', ha valamely felt�tel miatt nem volt tal�lat, 1 egy�bk�nt
    ///    Sz�r�t�bl�k (�res vagy Id-k, rendezve)
    ///    Table[1]: EREC_AgazatiJelek
    ///    Table[2]: EREC_IrattariTetelek
    ///    Table[3]: EREC_IratMetaDefinicio (�sszes t�nylegesen illeszked� rekord - a sz�l�k nem: Szint = 0)
    ///    Table[4]: EREC_IratMetaDefinicio (Irattipus, EljarasiSzakasz: null; Ugytipus: not null)
    ///    Table[5]: EREC_IratMetaDefinicio (Irattipus: null; EljarasiSzakasz: not null)
    ///    Table[6]: EREC_IratMetaDefinicio (Irattipus: not null)
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_IrattariTervHierarchiaSearch">Keres�si objektum, mely az EREC_AgazatiJelek, EREC_IrattariTetelek,
    ///    EREC_IratMetaDefinicio t�bl�kra vonatkoz� keres�si
    ///    objektumokat fogja egybe
    /// </param>
    /// <param name="bCsakSajatSzint">Ha �rt�ke True, a felettes objektum metadefin�ci�k (A0 defin�ci�t�pus) nem ker�lnek figyelembev�telre.
    /// Ha �rt�ke False, azon C2 t�pus� metadefin�ci�k is beker�lnek a sz�r�be, melyek feletteseihez vannak a felt�teleket teljes�t�
    /// t�rgyszavak, objektum metadatok k�tve</param>
    /// <returns>Hiba vagy a Record �rt�ke true, ha a sz�r�si felt�telek miatt nem volt tal�lat, egy�bk�nt false, a DS pedig
    /// a sz�r�s eredm�nyek�pp kapott Id-kb�l �ll� t�bl�kat tartalmazza (�resek, he nem volt felt�tel)</returns>
    public Result GetAllIrattariTervHierarchiaFilter(ExecParam ExecParam, IrattariTervHierarchiaSearch _IrattariTervHierarchiaSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_GetAllIrattariTervHierarchiaFilter");
        Result _ret = new Result();

        try
        {
            Query query_AgazatiJelek = new Query();
            query_AgazatiJelek.BuildFromBusinessDocument(_IrattariTervHierarchiaSearch.EREC_AgazatiJelekSearch);

            Query query_IrattariTetelek = new Query();
            query_IrattariTetelek.BuildFromBusinessDocument(_IrattariTervHierarchiaSearch.EREC_IraIrattariTetelekSearch);

            Query query_IratMetaDefinicio = new Query();
            query_IratMetaDefinicio.BuildFromBusinessDocument(_IrattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch);

            SqlCommand SqlComm = new SqlCommand("[sp_GetAllIrattariTervHierarchiaFilter]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_AgazatiJelek", System.Data.SqlDbType.NVarChar, 4000));
            SqlComm.Parameters["@Where_AgazatiJelek"].Value = query_AgazatiJelek.Where;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IraIrattariTetelek", System.Data.SqlDbType.NVarChar, 4000));
            SqlComm.Parameters["@Where_IraIrattariTetelek"].Value = query_IrattariTetelek.Where;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IratMetaDefinicio", System.Data.SqlDbType.NVarChar, 4000));
            SqlComm.Parameters["@Where_IratMetaDefinicio"].Value = query_IratMetaDefinicio.Where;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            // kiemelj�k a sz�r�si inform�ci�t
            string IsFilteredWithoutHit = ds.Tables[0].Rows[0]["IsFilteredWithoutHit"].ToString();
            _ret.Record = (IsFilteredWithoutHit == "1" ? true : false);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    /// <summary>
    /// GetNextUgytipusCodeByUgykor(ExecParam ExecParam)  
    /// A EREC_IratMetaDefinicio t�bla adott �gyk�rbe tartoz� automatikusan gener�lt k�d� �gyt�pusai alapj�n meghat�rozza a k�vetkez� k�dot.
    /// Az Ugykor_Id-t az ExecParam.Record_Id param�ter tartalmazza. 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <returns>Result - a Uid mezobe kerul a fuggveny altal visszaadott guid.</returns>
    public Result GetNextUgytipusCodeByUgykor(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IratMetaDefinicioGetNextUgytipusCodeByUgykor");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IratMetaDefinicioGetNextUgytipusCodeByUgykor]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = System.Data.SqlTypes.SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlComm.Parameters.Add(new SqlParameter("@Ugykor_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Ugykor_Id"].Value = System.Data.SqlTypes.SqlGuid.Parse(ExecParam.Record_Id);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Record = Convert.ToString(ds.Tables[0].Rows[0][0]);
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}