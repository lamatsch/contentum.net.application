using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_Irat_IktatokonyveiStoredProcedure
/// </summary>

public partial class EREC_Irat_IktatokonyveiStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_Irat_IktatokonyveiSearch _EREC_Irat_IktatokonyveiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Irat_IktatokonyveiGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_Irat_IktatokonyveiSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_Irat_IktatokonyveiSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Irat_IktatokonyveiGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Irat_IktatokonyveiSearch.OrderBy, _EREC_Irat_IktatokonyveiSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //        sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllByIktatoKonyv(ExecParam ExecParam, EREC_IraIktatoKonyvek _EREC_IraIktatoKonyvek
        , EREC_Irat_IktatokonyveiSearch _EREC_Irat_IktatokonyveiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Irat_IktatokonyveiGetAllByIktatoKonyv");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_Irat_IktatokonyveiSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_Irat_IktatokonyveiSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Irat_IktatokonyveiGetAllByIktatoKonyv]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IktatoKonyvId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IktatoKonyvId"].Value = _EREC_IraIktatoKonyvek.Typed.Id;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Irat_IktatokonyveiSearch.OrderBy, _EREC_Irat_IktatokonyveiSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}