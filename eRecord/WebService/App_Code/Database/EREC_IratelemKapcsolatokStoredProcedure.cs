﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_IratelemKapcsolatokStoredProcedure
/// </summary>
public partial class EREC_IratelemKapcsolatokStoredProcedure
{

    public Result GetAllWithExtension(ExecParam execParam, EREC_IratelemKapcsolatokSearch erec_IratelemKapcsolatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_IratelemKapcsolatokGetAllWithExtension");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(erec_IratelemKapcsolatokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += erec_IratelemKapcsolatokSearch.WhereByManual;
            

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IratelemKapcsolatokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, erec_IratelemKapcsolatokSearch.OrderBy, erec_IratelemKapcsolatokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }
}
