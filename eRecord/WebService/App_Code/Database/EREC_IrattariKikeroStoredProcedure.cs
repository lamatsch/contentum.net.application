using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Text;

/// <summary>
/// Summary description for EREC_IrattariKikeroStoredProcedure
/// </summary>

public partial class EREC_IrattariKikeroStoredProcedure
{

    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IrattariKikeroSearch _EREC_IrattariKikeroSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariKikeroGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IrattariKikeroSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_IrattariKikeroSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_IrattariKikeroSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_IrattariKikeroSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IrattariKikeroGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IrattariKikeroSearch.OrderBy, _EREC_IrattariKikeroSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Insert_Tomeges(String Method, ExecParam ExecParam,string[] Ids, string[] Vers, string[] UgyUgyirat_Ids, EREC_IrattariKikero Record)
    {
        return Insert_Tomeges(Method, ExecParam,Ids, Vers, UgyUgyirat_Ids, Record, DateTime.Now);
    }

    public Result Insert_Tomeges(String Method, ExecParam ExecParam,string[] Ids, string[] Vers, string[] UgyUgyirat_Ids, EREC_IrattariKikero Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
        if (Method == "") Method = "Insert";
        switch (Method)
        {
            case Constants.Insert:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariKikeroInsert_Tomeges");
                Record.Base.Updated.LetrehozasIdo = true;
                Record.Base.Updated.Letrehozo_id = true;
                Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_IrattariKikeroInsert_Tomeges]");
                if (sp_EREC_IrattariKikeroInsertParameters == null)
                {
                    //sp_EREC_IrattariKikeroInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_IrattariKikeroInsert]");
                    //sp_EREC_IrattariKikeroInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_IrattariKikeroInsert]");
                    sp_EREC_IrattariKikeroInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_IrattariKikeroInsert]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_IrattariKikeroInsertParameters);

                Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

                SqlComm.Parameters.Add("@UgyUgyirat_Ids", SqlDbType.NVarChar).Value = Search.GetSqlInnerString(UgyUgyirat_Ids);

                break;
            case Constants.Update:
                log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariKikeroUpdate_Tomeges");
                Record.Base.Updated.ModositasIdo = true;
                Record.Base.Updated.Modosito_id = true;
                Record.Base.ModositasIdo = ExecutionTime.ToString();
                Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_IrattariKikeroUpdate_Tomeges]");
                if (sp_EREC_IrattariKikeroUpdateParameters == null)
                {
                    //sp_EREC_IrattariKikeroUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_IrattariKikeroUpdate]");
                    //sp_EREC_IrattariKikeroUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_IrattariKikeroUpdate]");
                    sp_EREC_IrattariKikeroUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_IrattariKikeroUpdate]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_IrattariKikeroUpdateParameters);

                //Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);
                Utility.AddExecuteDefaultParameter(SqlComm, ExecParam, ExecutionTime);

                SqlComm.Parameters.Add("@Ids", SqlDbType.NVarChar).Value = Search.GetSqlInnerString(Ids);

                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < Vers.Length; i++)
                {
                    if (i > 0)
                        sb.Append(",");
                    sb.Append(Vers[i]);
                }

                SqlComm.Parameters.Add("@Vers", SqlDbType.NVarChar).Value = sb.ToString();

                break;
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            //if (Method == Constants.Insert)
            //{
            //    _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            //}
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }
    
}