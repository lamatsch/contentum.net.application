using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

/// <summary>
/// Summary description for EREC_IrattariTetel_IktatokonyvStoredProcedure
/// </summary>
public partial class EREC_IrattariTetel_IktatokonyvStoredProcedure
{

    #region BLG_2967
    public String CreateIktatokonyvIrattariTetelDexXml(ExecParam ExecParam, EREC_IrattariTetel_IktatokonyvSearch _EREC_IrattariTetel_IktatokonyvSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_Create_IktatokonyvIrattariTetel_DEX_XML");
        String _ret = String.Empty;

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IrattariTetel_IktatokonyvSearch);

            SqlCommand SqlComm = new SqlCommand("sp_Create_IktatokonyvIrattariTetel_DEX_XML");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Iktatokonyv_ID", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Iktatokonyv_ID"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUser_ID", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUser_ID"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlParameter outParam = new SqlParameter();
            outParam.SqlDbType = System.Data.SqlDbType.Xml;
            outParam.ParameterName = "@MetadataXML";
            outParam.Direction = ParameterDirection.Output;
            outParam.Size = int.MaxValue;

            SqlComm.Parameters.Add(outParam);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret = outParam.Value.ToString();
        }
        catch (SqlException e)
        {
            _ret = e.Message;
        }

        log.SpEnd(ExecParam);
        return _ret;
    }
    #endregion

    #region BLG_4977
    public String CreateErkeztetokonyvContentumXml(ExecParam ExecParam, EREC_IrattariTetel_IktatokonyvSearch _EREC_IrattariTetel_IktatokonyvSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_Create_Erkeztetokonyv_CONTENTUM_XML");
        String _ret = String.Empty;

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IrattariTetel_IktatokonyvSearch);

            SqlCommand SqlComm = new SqlCommand("sp_Create_Erkeztetokonyv_CONTENTUM_XML");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Iktatokonyv_ID", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Iktatokonyv_ID"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUser_ID", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUser_ID"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlParameter outParam = new SqlParameter();
            outParam.SqlDbType = System.Data.SqlDbType.Xml;
            outParam.ParameterName = "@MetadataXML";
            outParam.Direction = ParameterDirection.Output;
            outParam.Size = int.MaxValue;

            SqlComm.Parameters.Add(outParam);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret = outParam.Value.ToString();
        }
        catch (SqlException e)
        {
            _ret = e.Message;
        }

        log.SpEnd(ExecParam);
        return _ret;
    }
    #endregion

    public Result GetAllByIktatoKonyv(ExecParam ExecParam
        , EREC_IrattariTetel_IktatokonyvSearch _EREC_IrattariTetel_IktatokonyvSearch
        , String IktatoKonyv_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIrattariTetelekGetAllByIktatoKonyv");

        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_IrattariTetel_IktatokonyvSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_IrattariTetel_IktatokonyvSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IraIrattariTetelekGetAllByIktatoKonyv]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Iktatokonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Iktatokonyv_Id"].Value =
                Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(IktatoKonyv_Id, System.Data.SqlTypes.SqlGuid.Null);

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IrattariTetel_IktatokonyvSearch.OrderBy, _EREC_IrattariTetel_IktatokonyvSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result InsertAllByIktatokonyv(ExecParam execParam, string Iktatokonyv_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_IrattariTetel_IktatokonyvInsertAllByIktatokonyv");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IrattariTetel_IktatokonyvInsertAllByIktatokonyv]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Iktatokonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Iktatokonyv_Id"].Value =
                Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(Iktatokonyv_Id, System.Data.SqlTypes.SqlGuid.Null);

            Utility.AddExecuteDefaultParameter(SqlComm, execParam, DateTime.Now);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

}
