using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_KuldKuldemenyekStoredProcedure
/// </summary>
public partial class EREC_KuldKuldemenyekStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_KuldKuldemenyekSearch _EREC_KuldKuldemenyekSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldKuldemenyekGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch,true);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_KuldKuldemenyekSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_KuldKuldemenyekSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_KuldKuldemenyekSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KuldKuldemenyekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_KuldKuldemenyekSearch.OrderBy, _EREC_KuldKuldemenyekSearch.TopRow);

            if (_EREC_KuldKuldemenyekSearch.Extended_KRT_MappakSearch != null)
            {
                Query query_Mappak = new Query();
                query_Mappak.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch.Extended_KRT_MappakSearch);
                query_Mappak.Where += _EREC_KuldKuldemenyekSearch.Extended_KRT_MappakSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Dosszie", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_Dosszie"].Value = query_Mappak.Where;
            }

            if (_EREC_KuldKuldemenyekSearch.Extended_EREC_KuldKuldemenyekSearch != null)
            {
                Query query_KuldKuldemenyek_Csatolt = new Query();
                query_KuldKuldemenyek_Csatolt.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch.Extended_EREC_KuldKuldemenyekSearch);
                query_KuldKuldemenyek_Csatolt.Where += _EREC_KuldKuldemenyekSearch.Extended_EREC_KuldKuldemenyekSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_KuldKuldemenyek_Csatolt", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_KuldKuldemenyek_Csatolt"].Value = query_KuldKuldemenyek_Csatolt.Where;
            }

            if (_EREC_KuldKuldemenyekSearch.Extended_EREC_SzamlakSearch != null)
            {
                Query query_Szamlak = new Query();
                query_Szamlak.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch.Extended_EREC_SzamlakSearch);
                query_Szamlak.Where += _EREC_KuldKuldemenyekSearch.Extended_EREC_SzamlakSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Szamlak", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_Szamlak"].Value = query_Szamlak.Where;
            }

            // Felhaszn�l� szervezete a jogosults�gsz�r�shez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
            SqlComm.Parameters["@Jogosultak"].Size = 1;
            SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';

            //// WorkAround:
            //// mivel az FullTextSearchTree bels� f�ja jelenleg nem soros�that�,
            //// m�r k�szen, stringk�nt vessz�k �t a "SELECT" felt�telt
            //if (!String.IsNullOrEmpty(_EREC_KuldKuldemenyekSearch.ObjektumTargyszavai_ObjIdFilter))
            //{
            //    SqlComm.Parameters.Add(new SqlParameter("@ObjektumTargyszavai_ObjIdFilter", SqlDbType.NVarChar));
            //    SqlComm.Parameters["@ObjektumTargyszavai_ObjIdFilter"].Value = _EREC_KuldKuldemenyekSearch.ObjektumTargyszavai_ObjIdFilter;
            //}

            if (!String.IsNullOrEmpty(_EREC_KuldKuldemenyekSearch.AlSzervezetId))
            {
                SqlComm.Parameters.Add("@AlSzervezetId", SqlDbType.UniqueIdentifier).Value =
                    Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(_EREC_KuldKuldemenyekSearch.AlSzervezetId, SqlGuid.Null);
            }

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }



    public Result KimenoKuldGetAllWithExtension(ExecParam ExecParam, EREC_KuldKuldemenyekSearch _EREC_KuldKuldemenyekSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KimenoKuldemenyekGetAllWithExtension");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_KuldKuldemenyekSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_KuldKuldemenyekSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_KuldKuldemenyekSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KimenoKuldemenyekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_KuldKuldemenyekSearch.OrderBy, _EREC_KuldKuldemenyekSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
            SqlComm.Parameters["@Jogosultak"].Size = 1;
            SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';

            // Felhaszn�l� szervezete a jogosults�gsz�r�shez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            // C�mzett adatokra sz�r�si felt�tel:
            if (_EREC_KuldKuldemenyekSearch.Extended_EREC_PldIratPeldanyokSearch != null)
            {
                Query query_Pld = new Query();
                query_Pld.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch.Extended_EREC_PldIratPeldanyokSearch);
                query_Pld.Where += _EREC_KuldKuldemenyekSearch.Extended_EREC_PldIratPeldanyokSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_Pld.Where))
                {
                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IratPeldany_Cimzett", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@Where_IratPeldany_Cimzett"].Size = 4000;
                    SqlComm.Parameters["@Where_IratPeldany_Cimzett"].Value = query_Pld.Where;
                }
            }
            if (_EREC_KuldKuldemenyekSearch.Extended_EREC_UgyUgyiratdarabokSearch != null)
            {
                Query query_Pld = new Query();
                query_Pld.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch.Extended_EREC_UgyUgyiratdarabokSearch);
                query_Pld.Where += _EREC_KuldKuldemenyekSearch.Extended_EREC_UgyUgyiratdarabokSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_Pld.Where))
                {
                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_UgyUgyiratdarabok_Foszam", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@Where_UgyUgyiratdarabok_Foszam"].Size = 4000;
                    SqlComm.Parameters["@Where_UgyUgyiratdarabok_Foszam"].Value = query_Pld.Where;
                }
            }            
            if (_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIratokSearch!= null)
            {
                Query query_Pld = new Query();
                query_Pld.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIratokSearch);
                query_Pld.Where += _EREC_KuldKuldemenyekSearch.Extended_EREC_IraIratokSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_Pld.Where))
                {
                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IraIratok_Alszam", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@Where_IraIratok_Alszam"].Size = 4000;
                    SqlComm.Parameters["@Where_IraIratok_Alszam"].Value = query_Pld.Where;
                }
            }
            if (_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                Query query_Pld = new Query();
                query_Pld.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch);
                query_Pld.Where += _EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_Pld.Where))
                {
                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IraIktatoKonyvek", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@Where_IraIktatoKonyvek"].Size = 4000;
                    SqlComm.Parameters["@Where_IraIktatoKonyvek"].Value = query_Pld.Where;
                }
            }


            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result KimenoKuldGetAllWithExtensionForEFeladoJegyzek(ExecParam ExecParam, EPostazasiParameterek ePostazasiParameterek, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "dbo.usp_EREC_KimenoKuldemenyekGetAllWithExtensionForEFeladoJegyzek");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[dbo].[usp_EREC_KimenoKuldemenyekGetAllWithExtensionForEFeladoJegyzek]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            //Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_KuldKuldemenyekSearch.OrderBy, _EREC_KuldKuldemenyekSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
            SqlComm.Parameters["@Jogosultak"].Size = 1;
            SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';

            // Felhaszn�l� a jogosults�gsz�r�shez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            // Felhaszn�l� szervezete a jogosults�gsz�r�shez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            // k�telez� mez�k
            SqlComm.Parameters.Add(new SqlParameter("@Postakonyv_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Postakonyv_Id"].Value = ePostazasiParameterek.Typed.Postakonyv_Id;
            
            SqlComm.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
            SqlComm.Parameters["@StartDate"].Value = ePostazasiParameterek.Typed.StartDate;

            SqlComm.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
            SqlComm.Parameters["@EndDate"].Value = ePostazasiParameterek.Typed.EndDate;

            // Tov�bbi param�terek
            SqlComm.Parameters.Add(new SqlParameter("@IgnoreKozonseges", SqlDbType.Char, 1));
            SqlComm.Parameters["@IgnoreKozonseges"].Value = ePostazasiParameterek.Typed.IgnoreKozonsegesTetelek;

            SqlComm.Parameters.Add(new SqlParameter("@IgnoreNyilvantartott", SqlDbType.Char, 1));
            SqlComm.Parameters["@IgnoreNyilvantartott"].Value = ePostazasiParameterek.Typed.IgnoreNyilvantartottTetelek;

            SqlComm.Parameters.Add(new SqlParameter("@Kikuldo_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Kikuldo_Id"].Value = ePostazasiParameterek.Typed.Kikuldo_Id;

            SqlComm.Parameters.Add(new SqlParameter("@ForStatistics", SqlDbType.Char, 1));
            SqlComm.Parameters["@ForStatistics"].Value = ePostazasiParameterek.Typed.IsForStatistics;

            // "Lapoz�s"
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@StartPage", System.Data.SqlDbType.Int));
            SqlComm.Parameters["@StartPage"].Value = ePostazasiParameterek.Typed.StartPage;

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PageCount", System.Data.SqlDbType.Int));
            //SqlComm.Parameters["@PageCount"].Value = ePostazasiParameterek.Typed.PageCount;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@WindowSize", System.Data.SqlDbType.Int));
            SqlComm.Parameters["@WindowSize"].Value = ePostazasiParameterek.Typed.WindowSize;

            //if (ExecParam.Paging.PageNumber > 0)
            //{
            //    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@StartPage", System.Data.SqlDbType.Int));
            //    SqlComm.Parameters["@StartPage"].Value = ExecParam.Paging.PageNumber + 1;
            //}
            //if (ExecParam.Paging.PageSize > 0)
            //{
            //    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@WindowSize", System.Data.SqlDbType.Int));
            //    SqlComm.Parameters["@WindowSize"].Value = ExecParam.Paging.PageSize;
            //}

            // Ha van be�ll�tva lapoz�s, be�ll�tjuk a t�rolt elj�r�s param�tereit:
            if (ExecParam.Paging.PageNumber >= 0 && ExecParam.Paging.PageSize > 0)
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@pageNumber", System.Data.SqlDbType.Int));
                SqlComm.Parameters["@pageNumber"].Value = ExecParam.Paging.PageNumber;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@pageSize", System.Data.SqlDbType.Int));
                SqlComm.Parameters["@pageSize"].Value = ExecParam.Paging.PageSize;

                //if (!String.IsNullOrEmpty(ExecParam.Paging.SelectedRowId))
                //{
                //    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SelectedRowId", System.Data.SqlDbType.NVarChar));
                //    SqlComm.Parameters["@SelectedRowId"].Value = ExecParam.Paging.SelectedRowId;
                //}
            }

            //// WHERE felt�tel
            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar, 4000));
            //SqlComm.Parameters["@Where"].Value = query.Where;

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result KimenoKuldOsszesitoje(ExecParam ExecParam, EREC_KuldKuldemenyekSearch _EREC_KuldKuldemenyekSearch, string KezdDat, string VegeDat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KimenoKuldemenyekOsszesitoje");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_KuldKuldemenyekSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_KuldKuldemenyekSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_KuldKuldemenyekSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KimenoKuldemenyekOsszesitoje]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            SqlDateTime KezdDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                KezdDat, SqlDateTime.Null);

            SqlDateTime VegeDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                VegeDat, SqlDateTime.Null);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KezdDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@KezdDat"].Value = KezdDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VegeDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@VegeDat"].Value = VegeDat_sqltype;
            
            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_KuldKuldemenyekSearch.OrderBy, _EREC_KuldKuldemenyekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetWithRightCheck(ExecParam ExecParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldKuldemenyekGetWithRightCheck");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KuldKuldemenyekGetWithRightCheck]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Jogszint", SqlDbType.Char));
            SqlComm.Parameters["@Jogszint"].Size = 1;
            SqlComm.Parameters["@Jogszint"].Value = Jogszint;

            EREC_KuldKuldemenyek _EREC_KuldKuldemenyek = new EREC_KuldKuldemenyek();
            Utility.LoadBusinessDocumentFromDataAdapter(_EREC_KuldKuldemenyek, SqlComm);
            _ret.Record = _EREC_KuldKuldemenyek;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetByBarCode(ExecParam ExecParam, string BarCode)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldKuldemenyekGetByBarCode");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KuldKuldemenyekGetByBarCode]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);
            SqlComm.Parameters.Add(new SqlParameter("@BarCode", SqlDbType.Char));
            SqlComm.Parameters["@BarCode"].Value = BarCode;
            EREC_KuldKuldemenyek _EREC_KuldKuldemenyek = new EREC_KuldKuldemenyek();
            Utility.LoadBusinessDocumentFromDataAdapter(_EREC_KuldKuldemenyek, SqlComm);
            _ret.Record = _EREC_KuldKuldemenyek;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result AtadasraKijeloles_Tomeges(ExecParam ExecParam, string Ids, string Vers, string KovetkezoFelelosId, string KezFeljegyzesTipus, string Leiras)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldKuldemenyek_AtadasAtvetelTomeges");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KuldKuldemenyek_AtadasAtvetelTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;
            
            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Ids;

            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = Vers;

            SqlComm.Parameters.Add(new SqlParameter("@KovetkezoFelelos", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@KovetkezoFelelos"].Value = SqlGuid.Parse(KovetkezoFelelosId);

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlComm.Parameters.Add(new SqlParameter("@KezFeljegyzesTipus", SqlDbType.NVarChar));
            SqlComm.Parameters["@KezFeljegyzesTipus"].Size = 64;
            SqlComm.Parameters["@KezFeljegyzesTipus"].Value = KezFeljegyzesTipus;

            SqlComm.Parameters.Add(new SqlParameter("@Leiras", SqlDbType.NVarChar));
            SqlComm.Parameters["@Leiras"].Size = 100;
            SqlComm.Parameters["@Leiras"].Value = Leiras;

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Atvetel_Tomeges(ExecParam ExecParam, string Ids, string Vers, string KovetkezoOrzoId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldKuldemenyek_AtadasAtvetelTomeges");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KuldKuldemenyek_AtadasAtvetelTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Ids;

            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = Vers;

            SqlComm.Parameters.Add(new SqlParameter("@KovetkezoOrzo", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@KovetkezoOrzo"].Value = SqlGuid.Parse(KovetkezoOrzoId);

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result UpdateTomeges(ExecParam ExecParam, List<string> Ids, List<string> Vers, List<string> BarCode_List, List<string> Ragszam_List, EREC_KuldKuldemenyek Record, DateTime ExecutionTime)
    {
        string spName = "sp_EREC_KuldKuldemenyekUpdateTomeges";
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.


        log = Contentum.eUtility.Log.SpStart(ExecParam, spName);
        Record.Base.Updated.ModositasIdo = true;
        Record.Base.Updated.Modosito_id = true;
        Record.Base.ModositasIdo = ExecutionTime.ToString();
        Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
        SqlComm = new System.Data.SqlClient.SqlCommand("[" + spName + "]");
        if (sp_EREC_KuldKuldemenyekUpdateParameters == null)
        {
            //sp_EREC_KuldKuldemenyekUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_KuldKuldemenyekUpdate]");
            //sp_EREC_KuldKuldemenyekUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_KuldKuldemenyekUpdate]");
            sp_EREC_KuldKuldemenyekUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_KuldKuldemenyekUpdate]");
        }

        if (BarCode_List != null)
        {
            Record.Updated.BarCode = true;
        }

        if (Ragszam_List != null)
        {
            Record.Updated.RagSzam = true;
        }

        Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_KuldKuldemenyekUpdateParameters);

        Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

        SqlComm.Parameters.RemoveAt("@Id");
        SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
        SqlComm.Parameters["@Ids"].Value = String.Format("'{0}'", String.Join("','", Ids.ToArray()));
        SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
        SqlComm.Parameters["@Vers"].Value = String.Join(",", Vers.ToArray());

        if (BarCode_List != null)
        {
            SqlComm.Parameters.Add("@BarCode_List", SqlDbType.NVarChar).Value = String.Join(",", BarCode_List.ToArray());
        }

        if (Ragszam_List != null)
        {
            SqlComm.Parameters.Add("@RagSzam_List", SqlDbType.NVarChar).Value = String.Join(",", Ragszam_List.ToArray());
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result KimenoKuldemenyek_Moved(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_KimenoKuldemenyek_Moved");

        Result _ret = new Result();


        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KimenoKuldemenyek_Moved]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("Id", SqlDbType.UniqueIdentifier).Value = execParam.Typed.Record_Id;
            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier).Value = execParam.Typed.Felhasznalo_Id;
            SqlComm.Parameters.Add("@ExecutionTime", SqlDbType.DateTime).Value = DateTime.Now;

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result KimenoKuldemenyekFeladoJegyzek(ExecParam ExecParam, Guid? PostaKonyv_Id, DateTime? KezdDat, DateTime? VegeDat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KimenoKuldemenyekFeladoJegyzek");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("sp_EREC_KimenoKuldemenyekFeladoJegyzek");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            // k�telez� mez�k
            SqlComm.Parameters.Add(new SqlParameter("@PostaKonyv_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@PostaKonyv_Id"].Value = PostaKonyv_Id;

            SqlComm.Parameters.Add(new SqlParameter("@KezdDat", SqlDbType.DateTime));
            SqlComm.Parameters["@KezdDat"].Value = KezdDat;

            SqlComm.Parameters.Add(new SqlParameter("@VegeDat", SqlDbType.DateTime));
            SqlComm.Parameters["@VegeDat"].Value = VegeDat;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}