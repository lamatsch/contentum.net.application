using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_KuldMellekletekStoredProcedure
/// </summary>

public partial class EREC_KuldMellekletekStoredProcedure
{
    public Result GetAllByKuldKuldemenyek(ExecParam ExecParam, EREC_KuldKuldemenyek _EREC_KuldKuldemenyek, EREC_KuldMellekletekSearch _EREC_KuldMellekletekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldMellekletekGetAllByKuldKuldemenyek");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KuldMellekletekGetAllByKuldKuldemenyek]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KuldKuldemeny_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@KuldKuldemeny_Id"].Value = _EREC_KuldKuldemenyek.Typed.Id;

            Query query1 = new Query();
            query1.BuildFromBusinessDocument(_EREC_KuldMellekletekSearch);
            query1.Where += _EREC_KuldMellekletekSearch.WhereByManual;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where"].Size = 4000;
            SqlComm.Parameters["@Where"].Value = query1.Where;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@OrderBy"].Size = 200;
            SqlComm.Parameters["@OrderBy"].Value = _EREC_KuldMellekletekSearch.OrderBy;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@TopRow"].Size = 5;
            SqlComm.Parameters["@TopRow"].Value = _EREC_KuldMellekletekSearch.TopRow.ToString();

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //        sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }


    public Result GetAllWithExtension(ExecParam ExecParam, EREC_KuldMellekletekSearch _EREC_KuldMellekletekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldMellekletekGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_KuldMellekletekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_KuldMellekletekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KuldMellekletekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_KuldMellekletekSearch.OrderBy, _EREC_KuldMellekletekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}