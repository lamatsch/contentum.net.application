using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_KuldTertivevenyekStoredProcedure
/// </summary>

public partial class EREC_KuldTertivevenyekStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_KuldTertivevenyekSearch _EREC_KuldTertivevenyekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldTertivevenyekGetAllWithExtension");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_KuldTertivevenyekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_KuldTertivevenyekSearch.WhereByManual;

            // Sz�r�si felt�tel az Extended objektumokb�l:
            if (_EREC_KuldTertivevenyekSearch.Extended_EREC_KuldKuldemenyekSearch != null)
            {
                Query query_kuld = new Query();
                query_kuld.BuildFromBusinessDocument(_EREC_KuldTertivevenyekSearch.Extended_EREC_KuldKuldemenyekSearch);
                query_kuld.Where += _EREC_KuldTertivevenyekSearch.Extended_EREC_KuldKuldemenyekSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_kuld.Where))
                {
                    if (!String.IsNullOrEmpty(query.Where))
                    {
                        query.Where += " and (" + query_kuld.Where + ") ";
                    }
                    else
                    {
                        query.Where += " (" + query_kuld.Where + ") ";
                    }
                }

            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KuldTertivevenyekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_KuldTertivevenyekSearch.OrderBy, _EREC_KuldTertivevenyekSearch.TopRow);

            // C�mzett adatokra sz�r�si felt�tel:
            if (_EREC_KuldTertivevenyekSearch.Extended_EREC_KuldKuldemenyekSearch != null &&
                _EREC_KuldTertivevenyekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_PldIratPeldanyokSearch != null)
            {
                Query query_Pld = new Query();
                query_Pld.BuildFromBusinessDocument(_EREC_KuldTertivevenyekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_PldIratPeldanyokSearch);
                query_Pld.Where += _EREC_KuldTertivevenyekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_PldIratPeldanyokSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_Pld.Where))
                {
                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IratPeldany_Cimzett", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@Where_IratPeldany_Cimzett"].Size = 4000;
                    SqlComm.Parameters["@Where_IratPeldany_Cimzett"].Value = query_Pld.Where;
                }
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetTertivevenyKuldemenyeit(ExecParam ExecParam, EREC_KuldKuldemenyekSearch _EREC_KuldKuldemenyekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldTertivevenyekKuldemenyei");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_KuldKuldemenyekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_KuldTertivevenyekKuldemenyei]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(
                SqlComm,
                query,
                ExecParam,
                _EREC_KuldKuldemenyekSearch.OrderBy,
                _EREC_KuldKuldemenyekSearch.TopRow
            );

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result InsertTomeges(ExecParam ExecParam, List<string> Kuldemeny_Id_List, List<string> BarCode_List, List<string> Ragszam_List, EREC_KuldTertivevenyek Record, DateTime ExecutionTime)
    {
        string spName = "sp_EREC_KuldTertivevenyekInsertTomeges";
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.


        log = Contentum.eUtility.Log.SpStart(ExecParam, spName);
        Record.Base.Updated.LetrehozasIdo = true;
        Record.Base.Updated.Letrehozo_id = true;
        Record.Base.LetrehozasIdo = ExecutionTime.ToString();
        Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
        SqlComm = new System.Data.SqlClient.SqlCommand("[" + spName + "]");
        if (sp_EREC_KuldTertivevenyekInsertParameters == null)
        {
            //sp_EREC_KuldTertivevenyekInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_KuldTertivevenyekInsert]");
            //sp_EREC_KuldTertivevenyekInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_KuldTertivevenyekInsert]");
            sp_EREC_KuldTertivevenyekInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_KuldTertivevenyekInsert]");
        }

        if (Kuldemeny_Id_List != null && Kuldemeny_Id_List.Count > 0)
        {
            Record.Updated.Kuldemeny_Id = true;
        }

        if (BarCode_List != null && BarCode_List.Count > 0)
        {
            Record.Updated.BarCode = true;
        }

        if (Ragszam_List != null && Ragszam_List.Count > 0)
        {
            Record.Updated.Ragszam = true;
        }

        Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_KuldTertivevenyekInsertParameters);

        //Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

        if (Kuldemeny_Id_List != null && Kuldemeny_Id_List.Count > 0)
        {
            SqlComm.Parameters.Add("@Kuldemeny_Id_List", SqlDbType.NVarChar).Value = Contentum.eUtility.Search.GetSqlInnerString(Kuldemeny_Id_List.ToArray());
        }

        if (BarCode_List != null && BarCode_List.Count > 0)
        {
            SqlComm.Parameters.Add("@BarCode_List", SqlDbType.NVarChar).Value = String.Join(",", BarCode_List.ToArray());
        }

        if (Ragszam_List != null && Ragszam_List.Count > 0)
        {
            SqlComm.Parameters.Add("@Ragszam_List", SqlDbType.NVarChar).Value = String.Join(",", Ragszam_List.ToArray());
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        // hib�s t�telekhez tartoz� vonalk�dok visszad�sa is
        DataSet ds = null;

        try
        {
            //SqlComm.ExecuteNonQuery();

            // Id-k visszaad�sa
            //DataSet ds = new DataSet();
            ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;

            _ret.Ds = ds;
        }

        //sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result KuldTertivevenyekGetWithDokumentum(ExecParam ExecParam, string id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldTertivevenyekGetWithDokumentum");
        Result _ret = new Result();
        DataSet ds = new DataSet();

        SqlCommand SqlComm = new SqlCommand("[sp_EREC_KuldTertivevenyekGetWithDokumentum]");
        SqlDataAdapter adapter = new SqlDataAdapter();
        try
        {
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);
            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}
