using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_Kuldemeny_IratPeldanyaiStoredProcedure
/// </summary>

public partial class EREC_Kuldemeny_IratPeldanyaiStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_Kuldemeny_IratPeldanyaiSearch _EREC_Kuldemeny_IratPeldanyaiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Kuldemeny_IratPeldanyaiGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_Kuldemeny_IratPeldanyaiSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where = Search.ConcatWhereExpressions(query.Where, _EREC_Kuldemeny_IratPeldanyaiSearch.WhereByManual);

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Kuldemeny_IratPeldanyaiGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Kuldemeny_IratPeldanyaiSearch.OrderBy, _EREC_Kuldemeny_IratPeldanyaiSearch.TopRow);
            if (_EREC_Kuldemeny_IratPeldanyaiSearch.Extended_EREC_KuldKuldemenyekSearch != null)
            {
                Utils.AddExtendedSearchObjectToGetAllStoredProcedure(SqlComm, "@WhereKuldemeny", _EREC_Kuldemeny_IratPeldanyaiSearch.Extended_EREC_KuldKuldemenyekSearch, _EREC_Kuldemeny_IratPeldanyaiSearch.Extended_EREC_KuldKuldemenyekSearch.WhereByManual);
            }
            if (_EREC_Kuldemeny_IratPeldanyaiSearch.Extended_EREC_PldIratPeldanyokSearch != null)
            {
                Utils.AddExtendedSearchObjectToGetAllStoredProcedure(SqlComm, "@WherePeldany", _EREC_Kuldemeny_IratPeldanyaiSearch.Extended_EREC_PldIratPeldanyokSearch, _EREC_Kuldemeny_IratPeldanyaiSearch.Extended_EREC_PldIratPeldanyokSearch.WhereByManual);
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}