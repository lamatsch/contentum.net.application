﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_MellekletekStoredProcedure
/// </summary>
public partial class EREC_MellekletekStoredProcedure
{

    public Result GetAllWithExtension(ExecParam ExecParam, EREC_MellekletekSearch _EREC_MellekletekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_MellekletekGetAllWithExtension");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_MellekletekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_MellekletekSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_MellekletekGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_MellekletekSearch.OrderBy, _EREC_MellekletekSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }


    public Result InsertTertivevenyTomeges(ExecParam ExecParam, List<string> Kuldemeny_Id_List, List<string> BarCode_List, EREC_Mellekletek Record, DateTime ExecutionTime)
    {
        string spName = "sp_EREC_MellekletekInsertTertivevenyTomeges";
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.


        log = Contentum.eUtility.Log.SpStart(ExecParam, spName);
        Record.Base.Updated.LetrehozasIdo = true;
        Record.Base.Updated.Letrehozo_id = true;
        Record.Base.LetrehozasIdo = ExecutionTime.ToString();
        Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
        SqlComm = new System.Data.SqlClient.SqlCommand("[" + spName + "]");
        if (sp_EREC_MellekletekInsertParameters == null)
        {
            //sp_EREC_MellekletekInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_MellekletekInsert]");
            //sp_EREC_MellekletekInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_MellekletekInsert]");
            sp_EREC_MellekletekInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_MellekletekInsert]");
        }

        if (Kuldemeny_Id_List != null && Kuldemeny_Id_List.Count > 0)
        {
            Record.Updated.KuldKuldemeny_Id = true;
        }
        
        if (BarCode_List != null && BarCode_List.Count > 0)
        {
            Record.Updated.BarCode = true;
        }


        Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_MellekletekInsertParameters);

        //Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);

        if (Kuldemeny_Id_List != null || Kuldemeny_Id_List.Count > 0)
        {
            SqlComm.Parameters.Add("@Kuldemeny_Id_List", SqlDbType.NVarChar).Value = Contentum.eUtility.Search.GetSqlInnerString(Kuldemeny_Id_List.ToArray());
        }

        if (BarCode_List != null && BarCode_List.Count > 0)
        {
            SqlComm.Parameters.Add("@BarCode_List", SqlDbType.NVarChar).Value = String.Join(",", BarCode_List.ToArray());
        }

        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            //SqlComm.ExecuteNonQuery();

            // Id-k visszaadása
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }
}
