using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_Obj_MetaDefinicioStoredProcedure
/// </summary>

public partial class EREC_Obj_MetaDefinicioStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_Obj_MetaDefinicioSearch _EREC_Obj_MetaDefinicioSearch, string NoAncestorLoopWithId, string NoSuccessorLoopWithId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Obj_MetaDefinicioGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_Obj_MetaDefinicioSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_Obj_MetaDefinicioSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Obj_MetaDefinicioGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Obj_MetaDefinicioSearch.OrderBy, _EREC_Obj_MetaDefinicioSearch.TopRow);

            if (!String.IsNullOrEmpty(NoAncestorLoopWithId))
            {
                SqlComm.Parameters.Add("@NoAncestorLoopWithId", SqlDbType.UniqueIdentifier);
                SqlComm.Parameters["@NoAncestorLoopWithId"].Value = System.Data.SqlTypes.SqlGuid.Parse(NoAncestorLoopWithId);
            }

            if (!String.IsNullOrEmpty(NoSuccessorLoopWithId))
            {
                SqlComm.Parameters.Add("@NoSuccessorLoopWithId", SqlDbType.UniqueIdentifier);
                SqlComm.Parameters["@NoSuccessorLoopWithId"].Value = System.Data.SqlTypes.SqlGuid.Parse(NoSuccessorLoopWithId);
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    /// <summary>
    /// IratMetaDefinicioTerkephez meghat�rozza alulr�l felfel� hierarchikusan
    /// azon objektum Id-ket, melyek megfelelenek az �tadott keres�si felt�teleknek
    /// Ha nincs megadva felt�tel, �res t�bl�t ad vissza az �sszes Id helyett,
    /// az als�bb szinteken megfogalmazott felt�telekb�l sz�rmaz� sz�r�si
    /// t�bl�kkal akkor is megsz�ri a magasabb szinteket, ha azokra nincs egy�b
    /// felt�tel
    /// Nyilv�ntartja, ha egy szinten a felt�tel mellett m�r nem volt tal�lat,
    /// �gy megk�l�nb�ztethet� a felt�tel n�lk�li �s a felt�tel miatt �res
    /// sz�r�t�bla, �s nincs tov�bbi felesleges lek�r�s
    /// Visszaadott �rt�kek:
    ///    Table[0]: IsFilteredWithoutHit: '1', ha valamely felt�tel miatt nem volt tal�lat, 1 egy�bk�nt
    ///    Sz�r�t�bl�k (�res vagy Id-k, rendezve)
    ///    Table[0]: IsFilteredWithoutHit: '1', ha valamely felt�tel miatt nem volt tal�lat, 1 egy�bk�nt
    ///    Sz�r�t�bl�k (�res vagy Id-k, rendezve)
    ///    Table[1]: EREC_AgazatiJelek
    ///    Table[2]: EREC_IrattariTetelek
    ///    Table[3]: EREC_IratMetaDefinicio (�sszes t�nylegesen illeszked� rekord - a sz�l�k nem: Szint = 0)
    ///    Table[4]: EREC_IratMetaDefinicio (Irattipus, EljarasiSzakasz: null; Ugytipus: not null)
    ///    Table[5]: EREC_IratMetaDefinicio (Irattipus: null; EljarasiSzakasz: not null)
    ///    Table[6]: EREC_IratMetaDefinicio (Irattipus: not null)
    ///    Table[7]: EREC_ObjMetaDefinicio
    ///    Table[8]: EREC_ObjMetaAdatai
    ///    Table[9]: EREC_TargySzavak
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_MetaAdatHierarchiaSearch">Keres�si objektum, mely az EREC_AgazatiJelek, EREC_IrattariTetelek,
    ///    EREC_IratMetaDefinicio, EREC_ObjMetaDefinicio, EREC_ObjMetaAdatai, EREC_TargySzavak t�bl�kra vonatkoz� keres�si
    ///    objektumokat fogja egybe
    /// </param>
    /// <param name="bCsakSajatSzint">Ha �rt�ke True, a felettes objektum metadefin�ci�k (A0 defin�ci�t�pus) nem ker�lnek figyelembev�telre.
    /// Ha �rt�ke False, azon C2 t�pus� metadefin�ci�k is beker�lnek a sz�r�be, melyek feletteseihez vannak a felt�teleket teljes�t�
    /// t�rgyszavak, objektum metadatok k�tve</param>
    /// <returns>Hiba vagy a Record �rt�ke true, ha a sz�r�lsi felt�telek miatt nem volt tal�lat, egy�bk�nt false, a DS pedig
    /// a sz�r�s eredm�nyek�pp kapott Id-kb�l �ll� t�bl�kat tartalmazza (�resek, he nem volt felt�tel)</returns>
    public Result GetAllMetaAdatokHierarchiaFilter(ExecParam ExecParam, MetaAdatHierarchiaSearch _MetaAdatHierarchiaSearch, bool bCsakSajatSzint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_GetAllMetaAdatokHierarchiaFilter");
        Result _ret = new Result();

        try
        {
            Query query_AgazatiJelek = new Query();
            query_AgazatiJelek.BuildFromBusinessDocument(_MetaAdatHierarchiaSearch.EREC_AgazatiJelekSearch);

            Query query_IrattariTetelek = new Query();
            query_IrattariTetelek.BuildFromBusinessDocument(_MetaAdatHierarchiaSearch.EREC_IraIrattariTetelekSearch);

            Query query_IratMetaDefinicio = new Query();
            query_IratMetaDefinicio.BuildFromBusinessDocument(_MetaAdatHierarchiaSearch.EREC_IratMetaDefinicioSearch);

            Query query_Obj_MetaDefinicio = new Query();
            query_Obj_MetaDefinicio.BuildFromBusinessDocument(_MetaAdatHierarchiaSearch.EREC_Obj_MetaDefinicioSearch);

            Query query_Obj_MetaAdatai = new Query();
            query_Obj_MetaAdatai.BuildFromBusinessDocument(_MetaAdatHierarchiaSearch.EREC_Obj_MetaAdataiSearch);

            Query query_TargySzavak = new Query();
            query_TargySzavak.BuildFromBusinessDocument(_MetaAdatHierarchiaSearch.EREC_TargySzavakSearch);

            SqlCommand SqlComm = new SqlCommand("[sp_GetAllMetaAdatokHierarchiaFilter]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_AgazatiJelek", System.Data.SqlDbType.NVarChar, 4000));
            SqlComm.Parameters["@Where_AgazatiJelek"].Value = query_AgazatiJelek.Where;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IraIrattariTetelek", System.Data.SqlDbType.NVarChar, 4000));
            SqlComm.Parameters["@Where_IraIrattariTetelek"].Value = query_IrattariTetelek.Where;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IratMetaDefinicio", System.Data.SqlDbType.NVarChar, 4000));
            SqlComm.Parameters["@Where_IratMetaDefinicio"].Value = query_IratMetaDefinicio.Where;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Obj_MetaDefinicio", System.Data.SqlDbType.NVarChar, 4000));
            SqlComm.Parameters["@Where_Obj_MetaDefinicio"].Value = query_Obj_MetaDefinicio.Where;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Obj_MetaAdatai", System.Data.SqlDbType.NVarChar, 4000));
            SqlComm.Parameters["@Where_Obj_MetaAdatai"].Value = query_Obj_MetaAdatai.Where;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_TargySzavak", System.Data.SqlDbType.NVarChar, 4000));
            SqlComm.Parameters["@Where_TargySzavak"].Value = query_TargySzavak.Where;

            SqlComm.Parameters.Add("@CsakSajatSzint", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakSajatSzint"].Value = (bCsakSajatSzint == true ? '1' : '0');

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            // kiemelj�k a sz�r�si inform�ci�t
            string IsFilteredWithoutHit = ds.Tables[0].Rows[0]["IsFilteredWithoutHit"].ToString();
            _ret.Record = (IsFilteredWithoutHit == "1" ? true : false);
 
            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}