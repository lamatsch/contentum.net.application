using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_Obj_MetaAdataiStoredProcedure
/// </summary>

public partial class EREC_Obj_MetaAdataiStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_Obj_MetaAdataiSearch _EREC_Obj_MetaAdataiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Obj_MetaAdataiGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_Obj_MetaAdataiSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_Obj_MetaAdataiSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Obj_MetaAdataiGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Obj_MetaAdataiSearch.OrderBy, _EREC_Obj_MetaAdataiSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }


    public Result GetAllWithExtensionByObjMetaDefinicio(ExecParam ExecParam, EREC_Obj_MetaAdataiSearch _EREC_Obj_MetaAdataiSearch, String Obj_MetaDefinicio_Id, bool bCsakSajatSzint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Obj_MetaAdataiGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_Obj_MetaAdataiSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_Obj_MetaAdataiSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Obj_MetaAdataiGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Obj_MetaAdataiSearch.OrderBy, _EREC_Obj_MetaAdataiSearch.TopRow);

            SqlComm.Parameters.Add("@Obj_MetaDefinicio_Id", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@Obj_MetaDefinicio_Id"].Value = System.Data.SqlTypes.SqlGuid.Parse(Obj_MetaDefinicio_Id);

            SqlComm.Parameters.Add("@CsakSajatSzint", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakSajatSzint"].Value = (bCsakSajatSzint == true ? '1' : '0');


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}