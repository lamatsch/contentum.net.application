using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

using log4net;
using log4net.Config;

/// <summary>
/// Summary description for EREC_ObjektumTargyszavaiStoredProcedure
/// </summary>
public partial class EREC_ObjektumTargyszavaiStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_ObjektumTargyszavaiGetAllWithExtension");
        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_ObjektumTargyszavaiSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_ObjektumTargyszavaiSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_ObjektumTargyszavaiSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_ObjektumTargyszavaiSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_ObjektumTargyszavaiGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_ObjektumTargyszavaiSearch.OrderBy, _EREC_ObjektumTargyszavaiSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    /// <summary>
    /// 1.)
    /// EREC_Obj_MetaDefinicio t�bla defin�ci�i alapj�n egy adott t�pus� (pl.irat vagy �gyirat)
    /// objektum k�telez� metaadatainak �rv�nytelen�t�se
    /// 2.)
    /// Ha a @CsakSajatSzint �rt�ke '1', csak a saj�t szinten t�rt�nik �rv�nytelen�t�s,
    /// ha pedig az �rt�k '0', minden szinten, a Felettes_Obj_Meta k�vet�s�vel
    /// 3.)
    /// Ha a @CsakErvenytelen �rt�ke '0', a metadefin�ci� alapj�n minden minden
    /// hozz�rendel�st �rv�nytelen�t (m�dos�t�s el�tt c�lszer�)
    /// ha pedig az �rt�k '1', megkeresi azon objektum metaadatokat, melyeket a
    /// jelenlegi �llapotban nem rendeln�nk hozz� (m�dos�t�s ut�n c�lszer�)
    /// </summary>
    /// <param name="ExecParam">Felhaszn�l� Id</param>
    /// <param name="Obj_Id">Az objektum Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. K�telez�.</param>
    /// <param name="ObjTip_Id">Az objektum t�pus�nak Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. Ha nincs megadva az ObjTip_Kod, akkor k�telez�.</param>
    /// <param name="ObjTip_Kod">Az objektum t�pus�nak k�dja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. Ha nincs megadva az ObjTip_Id, akkor k�telez�.</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g, mely �rv�nytelen�t�seket kell elv�gezni - kcs:OBJMETADEFINICIO_TIPUS</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek �rv�nytelen�t�sre.</param>
    /// <param name="bCsakErvenytelen">Ha true, a metadefin�ci� alapj�n minden minden hozz�rendel�st �rv�nytelen�t (m�dos�t�s el�tt c�lszer�)
    /// ha pedig false, megkeresi azon objektum metaadatokat, melyeket a jelenlegi �llapotban nem rendeln�nk hozz� (m�dos�t�s ut�n c�lszer�)</param>
    /// <returns>Az �rv�nytelen�tett rekordok halmaza vagy hibak�d.</returns>
    public Result InvalidateByDefinicioTipus(ExecParam ExecParam, String Obj_Id, String ObjTip_Id, String ObjTip_Kod, String DefinicioTipus, bool bCsakSajatSzint, bool bCsakErvenytelen)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_ObjektumTargyszavaiInvalidateByDefinicioTipus");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_ObjektumTargyszavaiInvalidateByDefinicioTipus]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@Obj_Id", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@Obj_Id"].Value = System.Data.SqlTypes.SqlGuid.Parse(Obj_Id);

            if (String.IsNullOrEmpty(ObjTip_Id))
            {
                SqlComm.Parameters.Add("@ObjTip_Kod", SqlDbType.NVarChar, 100);
                SqlComm.Parameters["@ObjTip_Kod"].Value = ObjTip_Kod;

            }
            else
            {
                SqlComm.Parameters.Add("@ObjTip_Id", SqlDbType.UniqueIdentifier);
                SqlComm.Parameters["@ObjTip_id"].Value = System.Data.SqlTypes.SqlGuid.Parse(ObjTip_Id);
            }

            if (DefinicioTipus != null)
            {
                SqlComm.Parameters.Add("@DefinicioTipus", SqlDbType.VarChar, 2);
                SqlComm.Parameters["@DefinicioTipus"].Value = DefinicioTipus;
            }

            SqlComm.Parameters.Add("@CsakSajatSzint", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakSajatSzint"].Value = (bCsakSajatSzint == true ? '1' : '0');

            SqlComm.Parameters.Add("@CsakErvenytelen", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakErvenytelen"].Value = (bCsakErvenytelen == true ? '1' : '0');

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    /// <summary>
    /// 1.)
    /// EREC_Obj_MetaDefinicio t�bla defin�ci�i alapj�n egy adott t�pus� (pl.irat, �gyirat, dokumentum)
    /// objektum k�telez� metaadatainak �rv�nytelen�t�se
    /// 2.)
    /// Ha a @CsakSajatSzint �rt�ke '1', csak a saj�t szinten t�rt�nik �rv�nytelen�t�s,
    /// ha pedig az �rt�k '0', minden szinten, a Felettes_Obj_Meta k�vet�s�vel
    /// </summary>
    /// <param name="ExecParam">Felhaszn�l� Id</param>
    /// <param name="Obj_Id">Az objektum Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. K�telez�.</param>
    /// <param name="ObjTip_Id">Az objektum t�pus�nak Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. Ha nincs megadva az ObjTip_Kod, akkor k�telez�.</param>
    /// <param name="ObjTip_Kod">Az objektum t�pus�nak k�dja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. Ha nincs megadva az ObjTip_Id, akkor k�telez�.</param>
    /// <param name="ContentType">Egy�rtelm�en azonos�t egy metadefin�ci�t. K�telez�.</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek �rv�nytelen�t�sre.</param>
    /// <returns>Az �rv�nytelen�tett rekordok halmaza vagy hibak�d.</returns>
    public Result InvalidateByContentType(ExecParam ExecParam, String Obj_Id, String ObjTip_Id, String ObjTip_Kod, String ContentType, bool bCsakSajatSzint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_ObjektumTargyszavaiInvalidateByContentType");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_ObjektumTargyszavaiInvalidateByContentType]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add("@Obj_Id", SqlDbType.UniqueIdentifier);
            SqlComm.Parameters["@Obj_Id"].Value = System.Data.SqlTypes.SqlGuid.Parse(Obj_Id);

            if (String.IsNullOrEmpty(ObjTip_Id))
            {
                SqlComm.Parameters.Add("@ObjTip_Kod", SqlDbType.NVarChar, 100);
                SqlComm.Parameters["@ObjTip_Kod"].Value = ObjTip_Kod;

            }
            else
            {
                SqlComm.Parameters.Add("@ObjTip_Id", SqlDbType.UniqueIdentifier);
                SqlComm.Parameters["@ObjTip_id"].Value = System.Data.SqlTypes.SqlGuid.Parse(ObjTip_Id);
            }

            if (ContentType != null)
            {
                SqlComm.Parameters.Add("@ContentType", SqlDbType.VarChar, 100);
                SqlComm.Parameters["@ContentType"].Value = ContentType;
            }

            SqlComm.Parameters.Add("@CsakSajatSzint", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakSajatSzint"].Value = (bCsakSajatSzint == true ? '1' : '0');

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    /// <summary>
    /// Az EREC_ObjektumTargyszavaihoz hasonl� elnevez�ssel adja vissza az oszlopokat
    /// amelyek egy adott objektumt�pushoz k�telez�en tartoznak
    /// c�lja, hogy m�r akkor le lehessen k�rdezni a hozz�rendelend� t�rgyszavakat,
    /// amikor m�g nem ismert a konkr�t objektum Id, csak a t�pus
    /// A param�terek �ll�t�s�t�l f�gg�en a t�nylegesen (nem automatikusan) hozz�rendelt t�rgyszavak/metaadatok is lek�rhet�k
    /// </summary>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjTip_Id">Az objektum t�pus�nak Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat keress�k. Ha nincs megadva az ObjTip_Kod, akkor k�telez�.</param>
    /// <param name="ObjTip_Kod">Az objektum t�pus�nak k�dja, amelyhez rendelt t�rgyszavakat/metaadatokat keress�k. Ha nincs megadva az ObjTip_Id, akkor k�telez�.</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g - kcs:OBJMETADEFINICIO_TIPUS</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    public Result GetAllMetaByDefinicioTipus(ExecParam ExecParam, EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch, String Obj_Id, String ObjTip_Id, String ObjTip_Kod, String DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_ObjektumTargyszavaiGetAllMetaByDefinicioTipus");
        Result _ret = new Result();
        try
        {
            Query query = new Query();
            //query.BuildFromBusinessDocument(_EREC_ObjektumTargyszavaiSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_ObjektumTargyszavaiSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_ObjektumTargyszavaiSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_ObjektumTargyszavaiSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_ObjektumTargyszavaiGetAllMetaByDefinicioTipus]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_ObjektumTargyszavaiSearch.OrderBy, _EREC_ObjektumTargyszavaiSearch.TopRow);

            if (!String.IsNullOrEmpty(Obj_Id))
            {
                SqlComm.Parameters.Add("@Obj_Id", SqlDbType.UniqueIdentifier);
                SqlComm.Parameters["@Obj_Id"].Value = System.Data.SqlTypes.SqlGuid.Parse(Obj_Id);
            }

            if (String.IsNullOrEmpty(ObjTip_Id))
            {
                SqlComm.Parameters.Add("@ObjTip_Kod", SqlDbType.NVarChar, 100);
                SqlComm.Parameters["@ObjTip_Kod"].Value = ObjTip_Kod;

            }
            else
            {
                SqlComm.Parameters.Add("@ObjTip_Id", SqlDbType.UniqueIdentifier);
                SqlComm.Parameters["@ObjTip_id"].Value = System.Data.SqlTypes.SqlGuid.Parse(ObjTip_Id);
            }

            if (DefinicioTipus != null)
            {
                SqlComm.Parameters.Add("@DefinicioTipus", SqlDbType.VarChar, 2);
                SqlComm.Parameters["@DefinicioTipus"].Value = DefinicioTipus;
            }

            SqlComm.Parameters.Add("@CsakSajatSzint", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakSajatSzint"].Value = (bCsakSajatSzint == true ? '1' : '0');

            SqlComm.Parameters.Add("@CsakAutomatikus", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakAutomatikus"].Value = (bCsakAutomatikus == true ? '1' : '0');

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    /// <summary>
    /// Az EREC_ObjektumTargyszavaihoz hasonl� elnevez�ssel adja vissza az oszlopokat
    /// amelyek egy adott objektumt�pushoz k�telez�en tartoznak
    /// c�lja, hogy m�r akkor le lehessen k�rdezni a hozz�rendelend� t�rgyszavakat,
    /// amikor m�g nem ismert a konkr�t objektum Id, csak a t�pus
    /// A param�terek �ll�t�s�t�l f�gg�en a t�nylegesen (nem automatikusan) hozz�rendelt t�rgyszavak/metaadatok is lek�rhet�k
    /// </summary>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjTip_Id">Az objektum t�pus�nak Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat keress�k. Ha nincs megadva az ObjTip_Kod, akkor k�telez�.</param>
    /// <param name="ObjTip_Kod">Az objektum t�pus�nak k�dja, amelyhez rendelt t�rgyszavakat/metaadatokat keress�k. Ha nincs megadva az ObjTip_Id, akkor k�telez�.</param>
    /// <param name="ContentType">Egy�rtelm�en azonos�t egy metadefin�ci�t. K�telez�.</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    public Result GetAllMetaByContentType(ExecParam ExecParam, EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch, String Obj_Id, String ObjTip_Id, String ObjTip_Kod, String ContentType, bool bCsakSajatSzint, bool bCsakAutomatikus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_ObjektumTargyszavaiGetAllMetaByContentType");
        Result _ret = new Result();
        try
        {
            Query query = new Query();
            //query.BuildFromBusinessDocument(_EREC_ObjektumTargyszavaiSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_ObjektumTargyszavaiSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_ObjektumTargyszavaiSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_ObjektumTargyszavaiSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_ObjektumTargyszavaiGetAllMetaByContentType]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_ObjektumTargyszavaiSearch.OrderBy, _EREC_ObjektumTargyszavaiSearch.TopRow);

            if (!String.IsNullOrEmpty(Obj_Id))
            {
                SqlComm.Parameters.Add("@Obj_Id", SqlDbType.UniqueIdentifier);
                SqlComm.Parameters["@Obj_Id"].Value = System.Data.SqlTypes.SqlGuid.Parse(Obj_Id);
            }

            if (String.IsNullOrEmpty(ObjTip_Id))
            {
                SqlComm.Parameters.Add("@ObjTip_Kod", SqlDbType.NVarChar, 100);
                SqlComm.Parameters["@ObjTip_Kod"].Value = ObjTip_Kod;

            }
            else
            {
                SqlComm.Parameters.Add("@ObjTip_Id", SqlDbType.UniqueIdentifier);
                SqlComm.Parameters["@ObjTip_id"].Value = System.Data.SqlTypes.SqlGuid.Parse(ObjTip_Id);
            }

            if (ContentType != null)
            {
                SqlComm.Parameters.Add("@ContentType", SqlDbType.VarChar, 100);
                SqlComm.Parameters["@ContentType"].Value = ContentType;
            }

            SqlComm.Parameters.Add("@CsakSajatSzint", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakSajatSzint"].Value = (bCsakSajatSzint == true ? '1' : '0');

            SqlComm.Parameters.Add("@CsakAutomatikus", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakAutomatikus"].Value = (bCsakAutomatikus == true ? '1' : '0');

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    #region GetAllMetaByObjMetaDefinicio

    /// <summary>
    /// Az adott objektum elvben hozz�rendelend� metaadatait v�logatja le,
    /// �s ha m�r l�tezik a t�nyleges hozz�rendel�s, kieg�sz�ti annak adataival
    /// (az elvben hozz�rendelend� metaadatokat �sszef�s�li a t�nylegesen hozz�rendeltekkel)
    /// Az EREC_ObjektumTargyszavaihoz hasonl� elnevez�ssel adja vissza az oszlopokat.
    /// C�lja, hogy m�r akkor le lehessen k�rdezni a hozz�rendelend� t�rgyszavakat,
    /// amikor m�g nem ismert a konkr�t objektum Id, csak a t�pus
    /// Bemen� param�terek az objektum metadefin�ci� meghat�roz�s�hoz:
    /// Objektum metadefin�ci� Id VAGY
    /// a) t�blan�v (t�bl�hoz k�t�tt objektum metadefin�ci�: B1 t�pus),
    /// b) t�blan�v + oszlopn�v (oszlop�rt�khez k�t�tt objektum metadefin�ci�k B2 vagy C2, mind)
    /// c) t�blan�v + oszlopn�v + oszlop�rt�k (oszlop�rt�khez k�t�tt objektum metadefin�ci�, konkr�t)
    /// d) defin�ci� t�pus (Figyelmen k�v�l hagyva, ha az Objektum metadefin�ci� Id van megadva! A sz�r�s a nem automatikus elemekre nem vonatkozik!)
    /// A param�terek �ll�t�s�t�l f�gg�en a t�nylegesen (nem automatikusan) hozz�rendelt t�rgyszavak/metaadatok is lek�rhet�k
    /// </summary>
    /// <param name="ExecParam">A napl�z�shoz sz�ks�ges inform�ci�kat tartalmazza.</param>
    /// <param name="_EREC_ObjektumTargyszavaiSearch">Keres�si objektum, tov�bbi sz�r�si felt�telek megad�s�ra a kapcsolt t�rgyszavakra/metaadatokra vonatkoz�an.</param>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjMetaDefinicio_Id">Az objektum metadefin�ci� azonos�t�ja, amelyhez a kapcsolt t�rgyszavakat/metaadatokat keress�k.</param>
    /// <param name="TableName">Az objektum t�pus azonos�t�shoz a t�blan�v. Ha nincs megadva sem ObjMetaDefinicio_Id, sem DefinicioTipus, akkor k�telez�.</param>
    /// <param name="ColumnName">Ha a keresett objektum t�pus oszlop, akkor az oszlop neve. Csak megadott t�blan�vvel egy�tt haszn�lhat�!</param>
    /// <param param name="ColumnValues">Az adott oszlophoz kapcsolt objektum metadefin�ci�k k�r�nek sz�k�t�s�hez haszn�lt oszlop�rt�k(ek).</param>
    /// <param name="bColumnValuesExclude">A megadott oszlop�rt�k sz�r�s, ha van, a tartalmazott (false �rt�kn�l) vagy a kiz�rt (true �rt�kn�l) oszlop�rt�keket jelzik</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g - kcs:OBJMETADEFINICIO_TIPUS (A0, B1, B2 vagy C2 lehet)</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten (A0, �r�k�lt) defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    public Result GetAllMetaByObjMetaDefinicio(ExecParam ExecParam, EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch
        , String Obj_Id, String ObjMetaDefinicio_Id
        , String TableName, String ColumnName, String[] ColumnValues, bool bColumnValuesExclude
        , String DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_ObjektumTargyszavaiGetAllMetaByObjMetaDefinicio");
        Result _ret = new Result();
        try
        {
            Query query = new Query();
            //query.BuildFromBusinessDocument(_EREC_ObjektumTargyszavaiSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_ObjektumTargyszavaiSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_ObjektumTargyszavaiSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_ObjektumTargyszavaiSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_ObjektumTargyszavaiGetAllMetaByObjMetaDefinicio]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_ObjektumTargyszavaiSearch.OrderBy, _EREC_ObjektumTargyszavaiSearch.TopRow);

            if (!String.IsNullOrEmpty(Obj_Id))
            {
                SqlComm.Parameters.Add("@Obj_Id", SqlDbType.UniqueIdentifier);
                SqlComm.Parameters["@Obj_Id"].Value = System.Data.SqlTypes.SqlGuid.Parse(Obj_Id);
            }

            if (!String.IsNullOrEmpty(ObjMetaDefinicio_Id))
            {
                SqlComm.Parameters.Add("@ObjMetaDefinicio_Id", SqlDbType.UniqueIdentifier);
                SqlComm.Parameters["@ObjMetaDefinicio_Id"].Value = System.Data.SqlTypes.SqlGuid.Parse(ObjMetaDefinicio_Id);

            }

            if (!String.IsNullOrEmpty(TableName))
            {
                SqlComm.Parameters.Add("@TableName", SqlDbType.VarChar, 100);
                SqlComm.Parameters["@TableName"].Value = TableName;

                if (!String.IsNullOrEmpty(ColumnName))
                {
                    SqlComm.Parameters.Add("@ColumnName", SqlDbType.VarChar, 100);
                    SqlComm.Parameters["@ColumnName"].Value = ColumnName;

                    if (ColumnValues != null && ColumnValues.Length > 0)
                    {
                        if (bColumnValuesExclude == true)
                        {
                            SqlComm.Parameters.Add("@ColumnValuesExclude", SqlDbType.VarChar, 400);
                            SqlComm.Parameters["@ColumnValuesExclude"].Value = Contentum.eRecord.BaseUtility.Search.GetSqlInnerString(ColumnValues);
                        }
                        else
                        {
                            SqlComm.Parameters.Add("@ColumnValuesInclude", SqlDbType.VarChar, 400);
                            SqlComm.Parameters["@ColumnValuesInclude"].Value = Contentum.eRecord.BaseUtility.Search.GetSqlInnerString(ColumnValues);
                        }
                    }
                }
            }

            if (!String.IsNullOrEmpty(DefinicioTipus))
            {
                SqlComm.Parameters.Add("@DefinicioTipus", SqlDbType.VarChar, 2);
                SqlComm.Parameters["@DefinicioTipus"].Value = DefinicioTipus;
            }

            SqlComm.Parameters.Add("@CsakSajatSzint", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakSajatSzint"].Value = (bCsakSajatSzint == true ? '1' : '0');

            SqlComm.Parameters.Add("@CsakAutomatikus", SqlDbType.VarChar, 1);
            SqlComm.Parameters["@CsakAutomatikus"].Value = (bCsakAutomatikus == true ? '1' : '0');

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    #endregion GetAllMetaByObjMetaDefinicio


}