﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_PldIratPeldanyokStoredProcedure
/// </summary>

public partial class EREC_PldIratPeldanyokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_PldIratPeldanyokSearch _EREC_PldIratPeldanyokSearch, bool Jogosultak)
    {
        return GetAllWithExtension(ExecParam, _EREC_PldIratPeldanyokSearch, Jogosultak, null);
    }

    public Result GetAllWithExtension(ExecParam ExecParam, EREC_PldIratPeldanyokSearch _EREC_PldIratPeldanyokSearch, bool Jogosultak, string Filter_KimenoKuldemenyId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_PldIratPeldanyokGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            String str_where = "";
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch, true);

            str_where += query.Where;

            //az egyedi szuresi feltel hozzaadasa!!!
            if (!String.IsNullOrEmpty(_EREC_PldIratPeldanyokSearch.WhereByManual))
            {
                if (!String.IsNullOrEmpty(str_where))
                {
                    if (!_EREC_PldIratPeldanyokSearch.WhereByManual.Trim().StartsWith("and ", StringComparison.InvariantCultureIgnoreCase)) // kompatibilitás miatt kell tesztelni
                    { 
                        str_where += " and ";
                    }
                }
                str_where += _EREC_PldIratPeldanyokSearch.WhereByManual;
            }

            // belsõ keresési objektumok kibontása:

            if (_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch != null)
            {
                Query query_IraIrat = new Query();
                query_IraIrat.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch);
                query_IraIrat.Where += _EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_IraIrat.Where.Trim()))
                {
                    if (!String.IsNullOrEmpty(str_where.Trim())) str_where += " and ";
                    str_where += query_IraIrat.Where;
                }
            }
            if (_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch != null)
            {
                Query query_ugyiratDarab = new Query();
                query_ugyiratDarab.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch);
                query_ugyiratDarab.Where += _EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_ugyiratDarab.Where.Trim()))
                {
                    if (!String.IsNullOrEmpty(str_where.Trim())) str_where += " and ";
                    str_where += query_ugyiratDarab.Where;
                }
            }
            if (_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch != null)
            {
                Query query_UgyUgyirat = new Query();
                query_UgyUgyirat.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch);
                query_UgyUgyirat.Where += _EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_UgyUgyirat.Where.Trim()))
                {
                    if (!String.IsNullOrEmpty(str_where.Trim())) str_where += " and ";
                    str_where += query_UgyUgyirat.Where;
                }
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_PldIratPeldanyokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            query.Where = str_where;
            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_PldIratPeldanyokSearch.OrderBy, _EREC_PldIratPeldanyokSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
            SqlComm.Parameters["@Jogosultak"].Size = 1;
            SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';

            //if (!String.IsNullOrEmpty(Filter_KimenoKuldemenyId))
            //{
            //    System.Data.SqlTypes.SqlGuid kimenoKuldemenyId = 
            //        Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(Filter_KimenoKuldemenyId, System.Data.SqlTypes.SqlGuid.Null);

            //    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Filter_KimenoKuldemenyId", System.Data.SqlDbType.UniqueIdentifier));
            //    SqlComm.Parameters["@Filter_KimenoKuldemenyId"].Value = kimenoKuldemenyId;
            //}

            // több kimenõ küldeményre is lehet szûrni
            if (!String.IsNullOrEmpty(Filter_KimenoKuldemenyId))
            {
                // ha nem szerepel benne aposztróf (korábban Guid volt), átalakítjuk
                if (!Filter_KimenoKuldemenyId.Contains("'"))
                {
                    Filter_KimenoKuldemenyId = String.Format("'{0}'", Filter_KimenoKuldemenyId.Replace(",", "','"));
                }
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Filter_KimenoKuldemenyIds", System.Data.SqlDbType.VarChar));
                SqlComm.Parameters["@Filter_KimenoKuldemenyIds"].Value = Filter_KimenoKuldemenyId;
            }

            if (_EREC_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch != null)
            {
                Query query_IraKezFeljegyzesek = new Query();
                query_IraKezFeljegyzesek.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch);
                query_IraKezFeljegyzesek.Where += _EREC_PldIratPeldanyokSearch.Extended_EREC_HataridosFeladatokSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_IraKezFeljegyzesek.Where))
                {
                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_EREC_HataridosFeladatok", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@Where_EREC_HataridosFeladatok"].Value = query_IraKezFeljegyzesek.Where;
                }
            }

            if (_EREC_PldIratPeldanyokSearch.Extended_KRT_MappakSearch != null)
            {
                Query query_Mappak = new Query();
                query_Mappak.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch.Extended_KRT_MappakSearch);
                query_Mappak.Where += _EREC_PldIratPeldanyokSearch.Extended_KRT_MappakSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Dosszie", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_Dosszie"].Value = query_Mappak.Where;
            }

            SqlComm.Parameters.Add(new SqlParameter("@Csoporttagokkal", SqlDbType.Bit));
            SqlComm.Parameters["@Csoporttagokkal"].Value = _EREC_PldIratPeldanyokSearch.Csoporttagokkal ? 1 : 0;

            SqlComm.Parameters.Add(new SqlParameter("@CsakAktivIrat", SqlDbType.Bit));
            SqlComm.Parameters["@CsakAktivIrat"].Value = _EREC_PldIratPeldanyokSearch.CsakAktivIrat ? 1 : 0;

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetWithRightCheck(ExecParam ExecParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_PldIratPeldanyokGetWithRightCheck");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_EREC_PldIratPeldanyokGetWithRightCheck]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;
                SqlComm.CommandTimeout = 300;

                Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

                SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

                SqlComm.Parameters.Add(new SqlParameter("@Jogszint", SqlDbType.Char));
                SqlComm.Parameters["@Jogszint"].Size = 1;
                SqlComm.Parameters["@Jogszint"].Value = Jogszint;

                EREC_PldIratPeldanyok _EREC_PldIratPeldanyok = new EREC_PldIratPeldanyok();
                Utility.LoadBusinessDocumentFromDataAdapter(_EREC_PldIratPeldanyok, SqlComm);
                _ret.Record = _EREC_PldIratPeldanyok;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result AtadasraKijeloles_Tomeges(ExecParam ExecParam, string Ids, string Vers, string KovetkezoFelelosId, string KezFeljegyzesTipus, string Leiras)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_PldIratpeldanyok_AtadasAtvetelTomeges");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_PldIratpeldanyok_AtadasAtvetelTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Ids;

            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = Vers;

            SqlComm.Parameters.Add(new SqlParameter("@KovetkezoFelelos", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@KovetkezoFelelos"].Value = SqlGuid.Parse(KovetkezoFelelosId);

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlComm.Parameters.Add(new SqlParameter("@KezFeljegyzesTipus", SqlDbType.NVarChar));
            SqlComm.Parameters["@KezFeljegyzesTipus"].Size = 64;
            SqlComm.Parameters["@KezFeljegyzesTipus"].Value = KezFeljegyzesTipus;

            SqlComm.Parameters.Add(new SqlParameter("@Leiras", SqlDbType.NVarChar));
            SqlComm.Parameters["@Leiras"].Size = 100;
            SqlComm.Parameters["@Leiras"].Value = Leiras;

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Atvetel_Tomeges(ExecParam ExecParam, string Ids, string Vers, string KovetkezoOrzo)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_PldIratpeldanyok_AtadasAtvetelTomeges");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_PldIratpeldanyok_AtadasAtvetelTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Ids;

            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = Vers;

            SqlComm.Parameters.Add(new SqlParameter("@KovetkezoOrzo", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@KovetkezoOrzo"].Value = SqlGuid.Parse(KovetkezoOrzo);

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result UgyiratbaHelyezes_Tomeges(ExecParam execParam, string iratpeldanyIds, string iratpeldanyVers)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_PldIratpeldanyok_UgyiratbaHelyezesTomeges");

        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new SqlCommand("[sp_EREC_PldIratpeldanyok_UgyiratbaHelyezesTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloId"].Value = execParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezete", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezete"].Value = execParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LoginUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@LoginUserId"].Value = execParam.Typed.LoginUser_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = iratpeldanyIds;

            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = iratpeldanyVers;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Tranz_id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Tranz_id"].Value = Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(execParam.Page_Id, SqlGuid.Null);

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);

        return _ret;
    }

    public Result UpdateTomeges(ExecParam ExecParam, List<string> Ids, List<string> Vers, EREC_PldIratPeldanyok Record, DateTime ExecutionTime)
    {
        string spName = "sp_EREC_PldIratPeldanyokUpdate_Tomeges";

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, spName);
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new SqlCommand("[" + spName + "]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Dictionary<String, Utility.Parameter> sp_EREC_PldIratPeldanyokUpdateParameters = null;

            Record.Base.Updated.ModositasIdo = true;
            Record.Base.Updated.Modosito_id = true;
            Record.Base.ModositasIdo = ExecutionTime.ToString();
            Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
            if (sp_EREC_PldIratPeldanyokUpdateParameters == null)
            {
                sp_EREC_PldIratPeldanyokUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_PldIratPeldanyokUpdate]");
            }

            Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_PldIratPeldanyokUpdateParameters);

            Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.Parameters.RemoveAt("@Id");
            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = String.Format("'{0}'", String.Join("','", Ids.ToArray()));
            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = String.Join(",", Vers.ToArray());

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result GetAllByUgyirat(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch, EREC_PldIratPeldanyokSearch _EREC_PldIratPeldanyokSearch, EREC_PldIratPeldanyokSearch _PeldanyJoinFilter)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_PldIratPeldanyokGetAllByUgyirat");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_PldIratPeldanyokGetAllByUgyirat]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;
            
            String str_where = "";
            Query query = new Query();

            if (_EREC_PldIratPeldanyokSearch != null)
            {
                query.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch, true);

                //az egyedi szuresi feltel hozzaadasa!!!          
                query.Where += _EREC_PldIratPeldanyokSearch.WhereByManual;

                str_where += query.Where;
                
                // belsõ keresési objektumok kibontása:

                if (_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch != null)
                {
                    Query query_IraIrat = new Query();
                    query_IraIrat.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch);
                    query_IraIrat.Where += _EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.WhereByManual;

                    if (!String.IsNullOrEmpty(query_IraIrat.Where.Trim()))
                    {
                        if (!String.IsNullOrEmpty(str_where.Trim())) str_where += " and ";
                        str_where += query_IraIrat.Where;
                    }
                }
                if (_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch != null)
                {
                    Query query_ugyiratDarab = new Query();
                    query_ugyiratDarab.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch);
                    query_ugyiratDarab.Where += _EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.WhereByManual;

                    if (!String.IsNullOrEmpty(query_ugyiratDarab.Where.Trim()))
                    {
                        if (!String.IsNullOrEmpty(str_where.Trim())) str_where += " and ";
                        str_where += query_ugyiratDarab.Where;
                    }
                }
                if (_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch != null)
                {
                    Query query_UgyUgyirat = new Query();
                    query_UgyUgyirat.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch);
                    query_UgyUgyirat.Where += _EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.WhereByManual;

                    if (!String.IsNullOrEmpty(query_UgyUgyirat.Where.Trim()))
                    {
                        if (!String.IsNullOrEmpty(str_where.Trim())) str_where += " and ";
                        str_where += query_UgyUgyirat.Where;
                    }
                }
            }
            else
            {
                _EREC_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();
            }

            if (_EREC_UgyUgyiratokSearch.TopRow == 0)
            {
                _EREC_UgyUgyiratokSearch.TopRow = 1000;
            }

            query.Where = str_where;
            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_PldIratPeldanyokSearch.OrderBy, _EREC_PldIratPeldanyokSearch.TopRow);

            if (_EREC_UgyUgyiratokSearch != null)
            {
                Query query_ugyirat = new Query();
                query_ugyirat.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);
                query_ugyirat.Where += _EREC_UgyUgyiratokSearch.WhereByManual;

                if (!String.IsNullOrEmpty(query_ugyirat.Where.Trim()))
                {
                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratWhere", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@UgyiratWhere"].Value = query_ugyirat.Where;
                }
            }

            if (_PeldanyJoinFilter != null)
            {
                Query query_PeldanyJoin = new Query();
                query_PeldanyJoin.BuildFromBusinessDocument(_PeldanyJoinFilter);
                query_PeldanyJoin.Where += _PeldanyJoinFilter.WhereByManual;

                if (!String.IsNullOrEmpty(query_PeldanyJoin.Where.Trim()))
                {
                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PeldanyJoinFilter", System.Data.SqlDbType.NVarChar));
                    SqlComm.Parameters["@PeldanyJoinFilter"].Value = query_PeldanyJoin.Where;
                }
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Moved(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_PldIratPeldanyok_Moved");

        Result _ret = new Result();


        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_PldIratPeldanyok_Moved]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add("Id", SqlDbType.UniqueIdentifier).Value = execParam.Typed.Record_Id;
            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier).Value = execParam.Typed.Felhasznalo_Id;
            SqlComm.Parameters.Add("@ExecutionTime", SqlDbType.DateTime).Value = DateTime.Now;

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(execParam, _ret);
        return _ret;
    }

    /// <summary>
    /// Le kell válogatni azokat az iratpéldányokat, ahol a Státusz: Újraküldendő, 
    /// az Expediálás módja: Hivatali Kapu, és a kimenő küldemény 
    /// csatolmányai között van meghiusulási igazolás:
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    public Result GetAllIratPeldanyMeghiusulasiIgazolassalUjraPostazashoz(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "GetAllIratPeldanyMeghiusulasiIgazolassalUjraPostazashoz");
        Result _ret = new Result();

        try
        {
            SqlCommand sqlComm = new SqlCommand();
            sqlComm.CommandType = System.Data.CommandType.Text;
            sqlComm.Connection = dataContext.Connection;
            sqlComm.Transaction = dataContext.Transaction;

            sqlComm.CommandText =
                   @"
                    SELECT
                     p.* 
                    FROM 
                     EREC_PldIratPeldanyok p inner join 
                     EREC_Kuldemeny_IratPeldanyai kip on kip.Peldany_Id = p.Id inner join
                     EREC_Csatolmanyok cs on kip.KuldKuldemeny_Id = cs.KuldKuldemeny_Id inner join
                     KRT_Dokumentumok d on cs.Dokumentum_Id = d.Id
                    WHERE 
                     p.KuldesMod = '19' and 
                     p.Allapot = '61' and
                     d.FajlNev like 'meghiusulasi_igazolas%.pdf'
                    ";

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }
}