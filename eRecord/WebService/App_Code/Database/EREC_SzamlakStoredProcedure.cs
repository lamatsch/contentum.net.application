using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_SzamlakStoredProcedure
/// </summary>
public partial class EREC_SzamlakStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_SzamlakSearch _EREC_SzamlakSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_SzamlakGetAllWithExtension");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_EREC_SzamlakSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _EREC_SzamlakSearch.WhereByManual;

               SqlCommand SqlComm = new SqlCommand("[sp_EREC_SzamlakGetAllWithExtension]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_SzamlakSearch.OrderBy, _EREC_SzamlakSearch.TopRow);

               if (_EREC_SzamlakSearch.Extended_EREC_KuldKuldemenyekSearch != null)
               {
                   Query query_KuldKuldemenyek_Csatolt = new Query();
                   query_KuldKuldemenyek_Csatolt.BuildFromBusinessDocument(_EREC_SzamlakSearch.Extended_EREC_KuldKuldemenyekSearch);
                   query_KuldKuldemenyek_Csatolt.Where += _EREC_SzamlakSearch.Extended_EREC_KuldKuldemenyekSearch.WhereByManual;

                   SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_KuldKuldemenyek", System.Data.SqlDbType.NVarChar));
                   SqlComm.Parameters["@Where_KuldKuldemenyek"].Value = query_KuldKuldemenyek_Csatolt.Where;
               }

               if (_EREC_SzamlakSearch.Extended_KRT_DokumentumokSearch != null)
               {
                   Query query_Dokumentumok = new Query();
                   query_Dokumentumok.BuildFromBusinessDocument(_EREC_SzamlakSearch.Extended_KRT_DokumentumokSearch);
                   query_Dokumentumok.Where += _EREC_SzamlakSearch.Extended_KRT_DokumentumokSearch.WhereByManual;

                   SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Dokumentumok", System.Data.SqlDbType.NVarChar));
                   SqlComm.Parameters["@Where_Dokumentumok"].Value = query_Dokumentumok.Where;
               }


               // Felhaszn�l� szervezete a jogosults�gsz�r�shez
               SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
               SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

               SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
               SqlComm.Parameters["@Jogosultak"].Size = 1;
               SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result UpdatePIRAllapot_Tomeges(ExecParam ExecParam, string Ids, string Vers, string PIRAllapotok, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Szamlak_UpdatePIRAllapot_Tomeges");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new SqlCommand("[sp_EREC_Szamlak_UpdatePIRAllapot_Tomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            SqlComm.Parameters.RemoveAt("@Id");
            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Ids;
            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = Vers;
            SqlComm.Parameters.Add(new SqlParameter("@PIRAllapotok", SqlDbType.NVarChar));
            SqlComm.Parameters["@PIRAllapotok"].Value = PIRAllapotok;
            SqlComm.Parameters.Add(new SqlParameter("@Tranz_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Tranz_Id"].Value = System.Data.SqlTypes.SqlGuid.Parse(dataContext.Tranz_Id);

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }
}