﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System.Data.SqlClient;
using System.Data;
using Contentum.eQuery;

/// <summary>
/// Summary description for EREC_TomegesIktatasStoredProcedure
/// </summary>
public partial class EREC_TomegesIktatasStoredProcedure
{
    public Result GetAllWithFK(ExecParam ExecParam, EREC_TomegesIktatasSearch _EREC_TomegesIktatasSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_TomegesIktatasGetAllWithFk");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_TomegesIktatasSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_TomegesIktatasSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_TomegesIktatasGetAllWithFk]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_TomegesIktatasSearch.OrderBy, _EREC_TomegesIktatasSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAlairasTipusok(ExecParam ExecParam)
    {
        Result _ret = new Result();
        try
        {


            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.Connection = dataContext.Connection;
                sqlCommand.Transaction = dataContext.Transaction;
                sqlCommand.CommandText = "select * from KRT_AlairasTipusok where AlairasSzint = @AlairasSzint";
                sqlCommand.Parameters.Add("@AlairasSzint", SqlDbType.VarChar, 100, "AlairasSzint").Value = "4";
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCommand;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        return _ret;

    }

    public Result GetAlairoSzabylById(ExecParam ExecParam)
    {
        Result _ret = new Result();
        try
        {


            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.Connection = dataContext.Connection;
                sqlCommand.Transaction = dataContext.Transaction;

                sqlCommand.CommandText = "select p.Id,p.Nev,p.AlairoSzerep from KRT_AlairasSzabalyok p where p.Id=@azonosito";
                sqlCommand.Parameters.Add("@azonosito", SqlDbType.VarChar, 100, "azonosito").Value = ExecParam.Record_Id.ToString();

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCommand;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        return _ret;

    }

    /// <summary>
    /// GetAlairasTipusokSzerepAlapjan
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="AlairoSzerep"></param>
    /// <returns></returns>
    public Result GetAlairasTipusokSzerepAlapjan(ExecParam ExecParam, string AlairoSzerep)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_AlairasTipusokSzerepAlapjan");
        if (string.IsNullOrEmpty(AlairoSzerep))
            throw new Exception("Nincs megadva az aláíró szerep");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_AlairasTipusokSzerepAlapjan]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Parameters.Add("@AlairoSzerep", SqlDbType.VarChar, 64, "AlairoSzerep").Value = AlairoSzerep;
            SqlComm.Parameters.Add("@AlairasSzint", SqlDbType.Int).Value = DBNull.Value;
            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier).Value = new Guid(ExecParam.Felhasznalo_Id);

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}
