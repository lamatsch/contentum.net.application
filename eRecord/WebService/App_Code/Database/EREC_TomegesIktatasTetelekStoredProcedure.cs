using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_TomegesIktatasTetelekStoredProcedure
/// </summary>
public partial class EREC_TomegesIktatasTetelekStoredProcedure
{
    public Result InsertBulk(String Method, ExecParam ExecParam, EREC_TomegesIktatasTetelek[] Records)
    {
        return InsertBulk(Method, ExecParam, Records, DateTime.Now);
    }

    public Result InsertBulk(String Method, ExecParam ExecParam, EREC_TomegesIktatasTetelek[] Records, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_TomegesIktatasTetelekInsert_Bulk");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //sqlConnection.Open();

        System.Data.SqlClient.SqlCommand SqlComm = null;

        foreach (var Record in Records)
        {
            Record.Base.Updated.LetrehozasIdo = true;
            Record.Base.Updated.Letrehozo_id = true;
            Record.Base.LetrehozasIdo = ExecutionTime.ToString();
            Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
            Record.Base.Updated.ModositasIdo = true;
            Record.Base.Updated.Modosito_id = true;
            Record.Base.ModositasIdo = ExecutionTime.ToString();
            Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
        }

        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_TomegesIktatasTetelekInsert_Bulk]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        //SqlComm.Connection = sqlConnection;
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlParameter parameter = parameter = SqlComm.Parameters
                              .AddWithValue("@EREC_TomegesIktatasTetelek", CreateDataTable(Records));
        parameter.SqlDbType = SqlDbType.Structured;
        parameter.TypeName = "dbo.EREC_TomegesIktatasTetelek_Type";

        try
        {
            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}        

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    private static DataTable CreateDataTable(EREC_TomegesIktatasTetelek[] Records)
    {
        DataTable table = new DataTable();
        table.Columns.Add("Folyamat_Id", typeof(Guid));
        table.Columns.Add("Forras_Azonosito", typeof(string));
        table.Columns.Add("IktatasTipus", typeof(int));
        table.Columns.Add("Alszamra", typeof(int));
        table.Columns.Add("Iktatokonyv_Id", typeof(Guid));
        table.Columns.Add("Ugyirat_Id", typeof(Guid));
        table.Columns.Add("Kuldemeny_Id", typeof(Guid));
        table.Columns.Add("ExecParam", typeof(string));
        table.Columns.Add("EREC_UgyUgyiratok", typeof(string));
        table.Columns.Add("EREC_UgyUgyiratdarabok", typeof(string));
        table.Columns.Add("EREC_IraIratok", typeof(string));
        table.Columns.Add("EREC_PldIratPeldanyok", typeof(string));
        table.Columns.Add("EREC_HataridosFeladatok", typeof(string));
        table.Columns.Add("EREC_KuldKuldemenyek", typeof(string));
        table.Columns.Add("IktatasiParameterek", typeof(string));
        table.Columns.Add("ErkeztetesParameterek", typeof(string));
        table.Columns.Add("Dokumentumok", typeof(string));
        table.Columns.Add("Azonosito", typeof(string));
        table.Columns.Add("Allapot", typeof(string));
        table.Columns.Add("Irat_Id", typeof(Guid));
        table.Columns.Add("Hiba", typeof(string));
        table.Columns.Add("Ver", typeof(int));
        table.Columns.Add("Note", typeof(string));
        table.Columns.Add("Stat_id", typeof(Guid));
        table.Columns.Add("ErvKezd", typeof(DateTime));
        table.Columns.Add("ErvVege", typeof(DateTime));
        table.Columns.Add("Letrehozo_id", typeof(Guid));
        table.Columns.Add("LetrehozasIdo", typeof(DateTime));
        table.Columns.Add("Modosito_id", typeof(Guid));
        table.Columns.Add("ModositasIdo", typeof(DateTime));
        table.Columns.Add("Zarolo_id", typeof(Guid));
        table.Columns.Add("ZarolasIdo", typeof(DateTime));
        table.Columns.Add("Tranz_id", typeof(Guid));
        table.Columns.Add("UIAccessLog_id", typeof(Guid));
        table.Columns.Add("Expedialas", typeof(int));
        table.Columns.Add("TobbIratEgyKuldemenybe", typeof(int));
        table.Columns.Add("KuldemenyAtadas", typeof(Guid));
        table.Columns.Add("HatosagiStatAdat", typeof(string));
        table.Columns.Add("EREC_IratAlairok", typeof(string));
        table.Columns.Add("Lezarhato", typeof(int));

        foreach (EREC_TomegesIktatasTetelek id in Records)
        {
            DataRow row = table.NewRow();

            row["Folyamat_Id"] = id.Folyamat_Id;
            row["Forras_Azonosito"] = id.Forras_Azonosito;
            row["IktatasTipus"] = id.IktatasTipus;
            row["Alszamra"] = id.Alszamra;
            row["Iktatokonyv_Id"] = CheckNullValue(id.Iktatokonyv_Id);
            row["Ugyirat_Id"] = CheckNullValue(id.Ugyirat_Id);
            row["Kuldemeny_Id"] = CheckNullValue(id.Kuldemeny_Id);
            row["ExecParam"] = id.ExecParam;
            row["EREC_UgyUgyiratok"] = id.EREC_UgyUgyiratok;
            row["EREC_UgyUgyiratdarabok"] = id.EREC_UgyUgyiratdarabok;
            row["EREC_IraIratok"] = id.EREC_IraIratok;
            row["EREC_PldIratPeldanyok"] = id.EREC_PldIratPeldanyok;
            row["EREC_HataridosFeladatok"] = id.EREC_HataridosFeladatok;
            row["EREC_KuldKuldemenyek"] = id.EREC_KuldKuldemenyek;
            row["IktatasiParameterek"] = id.IktatasiParameterek;
            row["ErkeztetesParameterek"] = id.ErkeztetesParameterek;
            row["Dokumentumok"] = id.Dokumentumok;
            row["Azonosito"] = id.Azonosito;
            row["Allapot"] = id.Allapot;
            row["Irat_Id"] = CheckNullValue(id.Irat_Id);
            row["Hiba"] = id.Hiba;
            row["Expedialas"] = id.Expedialas;
            row["TobbIratEgyKuldemenybe"] = id.TobbIratEgyKuldemenybe;
            row["KuldemenyAtadas"] = CheckNullValue(id.KuldemenyAtadas);
            row["HatosagiStatAdat"] = id.HatosagiStatAdat;
            row["EREC_IratAlairok"] = id.EREC_IratAlairok;
            row["Lezarhato"] = id.Lezarhato;
            row["Note"] = id.Base.Note;
            row["Letrehozo_id"] = id.Base.Letrehozo_id;
            row["LetrehozasIdo"] = id.Base.LetrehozasIdo;
            row["Modosito_id"] = id.Base.Modosito_id;
            //LZS - BUG_12358 
            //Hi�ba van a t�bl�n default �rt�k, ha nem adjuk itt meg akkor NULL megy be.
            row["ErvKezd"] = DateTime.Now;
            row["ErvVege"] = "4700-12-31";

            //id.Base.ModositasIdo,                

            table.Rows.Add(row);
        }
        return table;
    }

    private static object CheckNullValue(string s)
    {
        if (string.IsNullOrEmpty(s))
            return DBNull.Value;
        else return s;
    }

}