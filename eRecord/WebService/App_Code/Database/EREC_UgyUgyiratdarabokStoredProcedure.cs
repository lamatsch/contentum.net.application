using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_UgyUgyiratdarabokStoredProcedure
/// </summary>

public partial class EREC_UgyUgyiratdarabokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_UgyUgyiratdarabokSearch _EREC_UgyUgyiratdarabokSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratdarabokGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratdarabokSearch);

            if (_EREC_UgyUgyiratdarabokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                string where_iktatokonyv = "";
                Query query_iktatokonyv = new Query();
                query_iktatokonyv.BuildFromBusinessDocument(_EREC_UgyUgyiratdarabokSearch.Extended_EREC_IraIktatoKonyvekSearch);
                where_iktatokonyv = query_iktatokonyv.Where;

                if (!String.IsNullOrEmpty(where_iktatokonyv))
                {
                    if (String.IsNullOrEmpty(query.Where))
                    {
                        query.Where = " (" + where_iktatokonyv + ") ";
                    }
                    else
                    {
                        query.Where += " and (" + where_iktatokonyv + ") ";
                    }
                }
            }

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_UgyUgyiratdarabokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratdarabokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratdarabokSearch.OrderBy, _EREC_UgyUgyiratdarabokSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
            SqlComm.Parameters["@Jogosultak"].Size = 1;
            SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetWithRightCheck(ExecParam ExecParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratdarabokGetWithRightCheck");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratdarabokGetWithRightCheck]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Jogszint", SqlDbType.Char));
            SqlComm.Parameters["@Jogszint"].Size = 1;
            SqlComm.Parameters["@Jogszint"].Value = Jogszint;


            EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok = new EREC_UgyUgyiratdarabok();
            Utility.LoadBusinessDocumentFromDataAdapter(_EREC_UgyUgyiratdarabok, SqlComm);
            _ret.Record = _EREC_UgyUgyiratdarabok;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}