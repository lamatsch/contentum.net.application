﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for EREC_UgyUgyiratokStoredProcedure
/// </summary>

public partial class EREC_UgyUgyiratokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch, bool Jogosultak, bool ForMunkanaplo)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetAllWithExtension");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();

            string Ugyintezo_Id_ForMunkanaplo = null;
            if (ForMunkanaplo)
            {
                if (!String.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value)
                    && _EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Operator == Query.Operators.equals)
                {
                    Ugyintezo_Id_ForMunkanaplo = _EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value;
                    _EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value = String.Empty;
                    _EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Operator = String.Empty;
                }

            }

            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch, true);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.WhereByManual))
            {
                if (_EREC_UgyUgyiratokSearch.WhereByManual.StartsWith("OR "))
                {
                    query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;
                }
                else
                {
                    query.Where += " and " + _EREC_UgyUgyiratokSearch.WhereByManual;
                }
            }

            //#region FullTextSearch
            //string strContainsTargyszo = String.Empty;
            //string strContainsTargy = String.Empty;
            //string strWhere = String.Empty;
            ////if (!string.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.Targy.Value))
            ////{
            ////    strContainsTargy = new FullTextSearch.SQLContainsCondition(_EREC_UgyUgyiratokSearch.Targy.Value).Normalized;
            ////    strWhere += "contains( EREC_UgyUgyiratok.Targy , '" + strContainsTargy + "' )";
            ////}
            //if (_EREC_UgyUgyiratokSearch.fts_targyszavak != null && !string.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.fts_targyszavak.Filter))
            //{
            //    strContainsTargyszo = new FullTextSearch.SQLContainsCondition(_EREC_UgyUgyiratokSearch.fts_targyszavak.Filter).Normalized;
            //    if (!string.IsNullOrEmpty(strWhere))
            //    {
            //        strWhere += " and ";
            //    }
            //    strWhere += "contains( (EREC_ObjektumTargyszavai.Targyszo, EREC_ObjektumTargyszavai.Note, EREC_ObjektumTargyszavai.Ertek ), '" + strContainsTargyszo + "' )";
            //}

            //if (!string.IsNullOrEmpty(strWhere))
            //{
            //    if (!string.IsNullOrEmpty(query.Where))
            //        query.Where += " and " + strWhere;
            //    else
            //        query.Where += strWhere;
            //}
            //#endregion

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            // Felhasználó szervezete a jogosultságszûréshez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters["@Where"].Size = -1; // nvarchar(max)
            /// Plusz Where feltételeket külön paraméterben átadva a tárolt eljárásnak:

            if (_EREC_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch != null)
            {
                string where_kuld = "";
                Query query_KuldKuldemenyek = new Query();
                query_KuldKuldemenyek.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch);
                query_KuldKuldemenyek.Where += _EREC_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.WhereByManual;
                where_kuld = query_KuldKuldemenyek.Where;

                if (_EREC_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
                {
                    Query query_kuld_ikt = new Query();
                    query_kuld_ikt.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch);

                    string where_kuld_ikt = query_kuld_ikt.Where;
                    where_kuld_ikt = where_kuld_ikt.Replace("EREC_IraIktatoKonyvek", "EREC_IraIktatoKonyvek_Kuld");

                    if (!String.IsNullOrEmpty(where_kuld_ikt))
                    {
                        if (String.IsNullOrEmpty(where_kuld))
                        {
                            where_kuld += where_kuld_ikt;
                        }
                        else
                        {
                            where_kuld += " and " + where_kuld_ikt;
                        }
                    }
                }

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_KuldKuldemenyek", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_KuldKuldemenyek"].Size = 4000;
                //SqlComm.Parameters["@Where_KuldKuldemenyek"].Value = query_KuldKuldemenyek.Where;
                SqlComm.Parameters["@Where_KuldKuldemenyek"].Value = where_kuld;
            }

            if (_EREC_UgyUgyiratokSearch.Extended_EREC_UgyUgyiratdarabokSearch != null)
            {
                Query query_UgyUgyiratdarabok = new Query();
                query_UgyUgyiratdarabok.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch.Extended_EREC_UgyUgyiratdarabokSearch);
                query_UgyUgyiratdarabok.Where += _EREC_UgyUgyiratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_UgyUgyiratdarabok", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_UgyUgyiratdarabok"].Size = 4000;
                SqlComm.Parameters["@Where_UgyUgyiratdarabok"].Value = query_UgyUgyiratdarabok.Where;
            }

            if (_EREC_UgyUgyiratokSearch.Extended_EREC_IraIratokSearch != null)
            {
                Query query_IraIratok = new Query();
                query_IraIratok.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch.Extended_EREC_IraIratokSearch);
                query_IraIratok.Where += _EREC_UgyUgyiratokSearch.Extended_EREC_IraIratokSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IraIratok", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_IraIratok"].Size = 4000;
                SqlComm.Parameters["@Where_IraIratok"].Value = query_IraIratok.Where;
            }

            if (_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                Query query_IraIktatokonyvek = new Query();
                query_IraIktatokonyvek.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch);
                query_IraIktatokonyvek.Where += _EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IraIktatokonyvek", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_IraIktatokonyvek"].Size = 4000;
                SqlComm.Parameters["@Where_IraIktatokonyvek"].Value = query_IraIktatokonyvek.Where;
            }

            if (!String.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.ObjektumTargyszavai_ObjIdFilter))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ObjektumTargyszavai_ObjIdFilter", System.Data.SqlDbType.NVarChar, 4000));
                SqlComm.Parameters["@ObjektumTargyszavai_ObjIdFilter"].Value = _EREC_UgyUgyiratokSearch.ObjektumTargyszavai_ObjIdFilter;
            }

            //if (_EREC_UgyUgyiratokSearch.Extended_EREC_IraIratokSearch != null)
            //{
            //    Query query_IraIratok = new Query();
            //    query_IraIratok.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch.Extended_EREC_IraIratokSearch);
            //    query_IraIratok.Where += _EREC_UgyUgyiratokSearch.Extended_EREC_IraIratokSearch.WhereByManual;

            //    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IraIratok", System.Data.SqlDbType.NVarChar));
            //    SqlComm.Parameters["@Where_IraIratok"].Size = 4000;
            //    SqlComm.Parameters["@Where_IraIratok"].Value = query_IraIratok.Where;
            //}

            // CR#2035: feleslegessé vált, mert az EREC_UgyUgyiratok.Targy FTSString típusú
            //if (_EREC_UgyUgyiratokSearch.fts_targy != null && !string.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.fts_targy.Filter))
            //{
            //    string strContains = new Contentum.eQuery.FullTextSearch.SQLContainsCondition(_EREC_UgyUgyiratokSearch.fts_targy.Filter).Normalized;

            //    SqlComm.Parameters.Add(new SqlParameter("@FullTextSearch_Targy", SqlDbType.NVarChar));
            //    SqlComm.Parameters["@FullTextSearch_Targy"].Value = strContains;
            //}

            if (_EREC_UgyUgyiratokSearch.fts_altalanos != null && !string.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.fts_altalanos.Filter))
            {
                string strContains = new Contentum.eQuery.FullTextSearch.SQLContainsCondition(_EREC_UgyUgyiratokSearch.fts_altalanos.Filter).Normalized;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Altalanos_FTS", System.Data.SqlDbType.NVarChar, 4000));
                SqlComm.Parameters["@Altalanos_FTS"].Value = strContains;
            }

            //LZS - BUG_7099
            //Ha ki van töltve a keresőképernyőtől érkező _EREC_UgyUgyiratokSearch  objektum fts_note property-je, akkor
            //ezt átadjuk a tárolt eljárás @Where_Note paraméterének.
            if (_EREC_UgyUgyiratokSearch.fts_note != null && !string.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.fts_note.Filter))
            {
                string strContains = new Contentum.eQuery.FullTextSearch.SQLContainsCondition(_EREC_UgyUgyiratokSearch.fts_note.Filter).Normalized;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Note", System.Data.SqlDbType.NVarChar, 4000));
                SqlComm.Parameters["@Where_Note"].Value = strContains;
            }

            if (_EREC_UgyUgyiratokSearch.Extended_KRT_MappakSearch != null)
            {
                Query query_Mappak = new Query();
                query_Mappak.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch.Extended_KRT_MappakSearch);
                query_Mappak.Where += _EREC_UgyUgyiratokSearch.Extended_KRT_MappakSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_Dosszie", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_Dosszie"].Value = query_Mappak.Where;
            }

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
            SqlComm.Parameters["@Jogosultak"].Size = 1;
            SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';

            if (ForMunkanaplo)
            {
                SqlComm.Parameters.Add(new SqlParameter("@ForMunkanaplo", SqlDbType.Char));
                SqlComm.Parameters["@ForMunkanaplo"].Value = Contentum.eUtility.Constants.Database.Yes;

                if (!String.IsNullOrEmpty(Ugyintezo_Id_ForMunkanaplo))
                {
                    SqlComm.Parameters.Add(new SqlParameter("@Ugyintezo_Id_ForMunkanaplo", SqlDbType.UniqueIdentifier));
                    SqlComm.Parameters["@Ugyintezo_Id_ForMunkanaplo"].Value = SqlGuid.Parse(Ugyintezo_Id_ForMunkanaplo);
                }
            }

            if (!String.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.AlSzervezetId))
            {
                SqlComm.Parameters.Add("@AlSzervezetId", SqlDbType.UniqueIdentifier).Value =
                    Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(_EREC_UgyUgyiratokSearch.AlSzervezetId, SqlGuid.Null);
            }

            if (_EREC_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch != null)
            {
                Query query_EREC_IratMetaDefinicio = new Query();
                query_EREC_IratMetaDefinicio.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch);
                query_EREC_IratMetaDefinicio.Where += _EREC_UgyUgyiratokSearch.Extended_EREC_IratMetaDefinicioSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_EREC_IratMetaDefinicio", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_EREC_IratMetaDefinicio"].Value = query_EREC_IratMetaDefinicio.Where;
            }

            SqlComm.Parameters.Add(new SqlParameter("@Csoporttagokkal", SqlDbType.Bit));
            SqlComm.Parameters["@Csoporttagokkal"].Value = _EREC_UgyUgyiratokSearch.Csoporttagokkal ? 1 : 0;

            SqlComm.Parameters.Add(new SqlParameter("@CsakAktivIrat", SqlDbType.Bit));
            SqlComm.Parameters["@CsakAktivIrat"].Value = _EREC_UgyUgyiratokSearch.CsakAktivIrat ? 1 : 0;

            if (_EREC_UgyUgyiratokSearch.NaponBelulNincsUjAlszam > -1)
            {
                SqlComm.Parameters.Add(new SqlParameter("@NaponBelulNincsUjAlszam", SqlDbType.Int));
                SqlComm.Parameters["@NaponBelulNincsUjAlszam"].Value = _EREC_UgyUgyiratokSearch.NaponBelulNincsUjAlszam;
            }

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //        sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    #region BLG 2969
    public String CreateUgyiratIratokDexXml(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_Create_UgyiratIratok_DEX_XML");
        String _ret = String.Empty;

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

            SqlCommand SqlComm = new SqlCommand("sp_Create_UgyiratIratok_DEX_XML");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            // Felhasználó szervezete a jogosultságszûréshez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Ugyirat_ID", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Ugyirat_ID"].Value = ExecParam.Typed.Record_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUser_ID", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUser_ID"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlParameter outParam = new SqlParameter();
            outParam.SqlDbType = System.Data.SqlDbType.Xml;
            outParam.ParameterName = "@MetadataXML";
            outParam.Direction = ParameterDirection.Output;
            outParam.Size = int.MaxValue;

            SqlComm.Parameters.Add(outParam);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            //_ret.Ds = ds;

            //DataSet dataSet = new DataSet();
            //DataTable dataTable = new DataTable("table1");
            //dataTable.Columns.Add("col1", typeof(string));
            //dataSet.Tables.Add(dataTable);
            _ret = outParam.Value.ToString();

            //System.IO.StringReader xmlSR = new System.IO.StringReader(outXML);

            //dataSet.ReadXml(xmlSR, XmlReadMode.IgnoreSchema);

            //_ret.Ds = dataSet;

        }
        catch (SqlException e)
        {
            //_ret.ErrorCode = e.ErrorCode.ToString();
            _ret = e.Message;
        }

        log.SpEnd(ExecParam);
        return _ret;
    }
    #endregion

    public Result GetAllWithExtension(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        return GetAllWithExtension(ExecParam, _EREC_UgyUgyiratokSearch, false, false);
    }

    public Result GetAllWithExtensionForStatistics(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetAllWithExtensionForStatistics");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllWithExtensionForStatistics]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllWithExtensionForEloadoiIvek(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvek");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvek]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllWithExtensionForUgyiratpotlo(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetAllWithExtensionForUgyiratpotlo");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllWithExtensionForUgyiratpotlo]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllWithExtensionAndSzereltek(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetAllWithExtensionAndSzereltek");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch, true);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_UgyUgyiratokSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllWithExtensionAndSzereltek]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            // Felhasználó szervezete a jogosultságszûréshez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            /// Plusz Where feltételeket külön paraméterben átadva a tárolt eljárásnak:
            if (_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
            {
                Query query_IraIktatokonyvek = new Query();
                query_IraIktatokonyvek.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch);
                query_IraIktatokonyvek.Where += _EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.WhereByManual;

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where_IraIktatokonyvek", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@Where_IraIktatokonyvek"].Size = 4000;
                SqlComm.Parameters["@Where_IraIktatokonyvek"].Value = query_IraIktatokonyvek.Where;
            }

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //        sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetWithRightCheck(ExecParam ExecParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetWithRightCheck");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetWithRightCheck]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

            SqlComm.Parameters.Add(new SqlParameter("@Jogszint", SqlDbType.Char));
            SqlComm.Parameters["@Jogszint"].Size = 1;
            SqlComm.Parameters["@Jogszint"].Value = Jogszint;

            SqlComm.Parameters.Add(new SqlParameter("@FelhasznaloSzervezet_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            EREC_UgyUgyiratok _EREC_UgyUgyiratok = new EREC_UgyUgyiratok();
            Utility.LoadBusinessDocumentFromDataAdapter(_EREC_UgyUgyiratok, SqlComm);
            _ret.Record = _EREC_UgyUgyiratok;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithExtensionForLeaders(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch
            , String KezdDat, String VegeDat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokForLeaders");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokForLeaders]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlDateTime KezdDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                KezdDat, SqlDateTime.Null);

            SqlDateTime VegeDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                VegeDat, SqlDateTime.Null);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KezdDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@KezdDat"].Value = KezdDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VegeDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@VegeDat"].Value = VegeDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result ForXml(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokForXml");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokForXml]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            // Felhasználó szervezete a jogosultságszûréshez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetSzereltekFoszam(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetSzereltekFoszam");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetSzereltekFoszam]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result ForElszamoltatasiJk(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokForElszamoltatasiJk");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokForElszamoltatasiJk]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    //LZS - BUG_4747
    //[sp_EREC_UgyUgyiratokForElszamoltatasiJkSSRS] tárolt eljárást meghívó metódus.
    //Lekérdezi az elszámoltatási jegyzőkönyvhöz a rekordokat Alszámokra és anélkül is a megfelelő paraméter átadásával.
    public Result ForElszamoltatasiJkSSRS(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch, bool Alszam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokForElszamoltatasiJkSSRS");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;


            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokForElszamoltatasiJkSSRS]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            //LZS - BUG_4747
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Alszam", System.Data.SqlDbType.Bit));
            SqlComm.Parameters["@Alszam"].Value = Alszam;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result ForXmlNincsUgyintezo(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokForXmlNincsUgyintezo");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_UgyUgyiratokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokForXmlNincsUgyintezo]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            // Felhasználó szervezete a jogosultságszûréshez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllForHatosagiStatisztikaUgyiratforgalom(ExecParam ExecParam, String KezdDat, String VegeDat, String WorkSheetName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_HatosagiStatisztikaUgyiratforgalom");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_HatosagiStatisztikaUgyiratforgalom]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlDateTime KezdDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                KezdDat, SqlDateTime.Null);

            SqlDateTime VegeDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                VegeDat, SqlDateTime.Null);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KezdDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@KezdDat"].Value = KezdDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VegeDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@VegeDat"].Value = VegeDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            if (!String.IsNullOrEmpty(WorkSheetName))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@sheet", System.Data.SqlDbType.NVarChar, 100));
                SqlComm.Parameters["@sheet"].Value = WorkSheetName;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllForHatosagiStatisztikaOnkormanyzat(ExecParam ExecParam, String KezdDat, String VegeDat, String WorkSheetName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_HatosagiStatisztikaOnkormanyzat");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_HatosagiStatisztikaOnkormanyzat]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 600;

            SqlDateTime KezdDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                KezdDat, SqlDateTime.Null);

            SqlDateTime VegeDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                VegeDat, SqlDateTime.Null);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KezdDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@KezdDat"].Value = KezdDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VegeDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@VegeDat"].Value = VegeDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            if (!String.IsNullOrEmpty(WorkSheetName))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@sheet", System.Data.SqlDbType.NVarChar, 100));
                SqlComm.Parameters["@sheet"].Value = WorkSheetName;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    public Result GetAllForHatosagiStatisztikaAllamigazgatas(ExecParam ExecParam, String KezdDat, String VegeDat, String WorkSheetName)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_HatosagiStatisztikaAllamigazgatas");
        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_HatosagiStatisztikaAllamigazgatas]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 600;

            SqlDateTime KezdDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                KezdDat, SqlDateTime.Null);

            SqlDateTime VegeDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                VegeDat, SqlDateTime.Null);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KezdDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@KezdDat"].Value = KezdDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VegeDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@VegeDat"].Value = VegeDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            if (!String.IsNullOrEmpty(WorkSheetName))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@sheet", System.Data.SqlDbType.NVarChar, 100));
                SqlComm.Parameters["@sheet"].Value = WorkSheetName;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetSzignaltCsoport(ExecParam ExecParam, string CsoportId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_GetSzignaltCsoport");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_GetSzignaltCsoport]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new SqlParameter("@JogtargyId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@JogtargyId"].Value = SqlGuid.Parse(ExecParam.Record_Id);

            SqlComm.Parameters.Add(new SqlParameter("@CsoportId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@CsoportId"].Value = SqlGuid.Parse(CsoportId);

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            KRT_Csoportok _KRT_Csoportok = new KRT_Csoportok();
            Utility.LoadBusinessDocumentFromDataAdapter(_KRT_Csoportok, SqlComm);
            _ret.Record = _KRT_Csoportok;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetSummaryForVezetoiPanel(ExecParam ExecParam, VezetoiPanelParameterek _VezetoiPanelParameterek)
    {
        Contentum.eUtility.Log log;
        if (_VezetoiPanelParameterek.isNMHH)
        {
            log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokForLeadersNMHH");
        }
        else
        {
            log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokForLeaders");
        }
        Result _ret = new Result();

        try
        {
            // BUG_8847
            //SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokForLeaders]");
            SqlCommand SqlComm;
            if (_VezetoiPanelParameterek.isNMHH)
            {
                SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokForLeadersNMHH]");
            }
            else
            {
                SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokForLeaders]");
            }
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;


            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Option_VezetoiPanel", System.Data.SqlDbType.Bit));
            SqlComm.Parameters["@Option_VezetoiPanel"].Value = true;


            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KezdDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@KezdDat"].Value = _VezetoiPanelParameterek.Typed.KezdDat;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VegeDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@VegeDat"].Value = _VezetoiPanelParameterek.Typed.VegeDat;

            SqlComm.Parameters.Add("@AlSzervezetId", SqlDbType.UniqueIdentifier).Value = _VezetoiPanelParameterek.Typed.AlSzervezetId;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            //Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_UgyUgyiratokSearch.OrderBy, _EREC_UgyUgyiratokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }



    public Result GetAllExcelDetails(ExecParam ExecParam, String KezdDat, String VegeDat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_ExcelDetailsForLeaders");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_ExcelDetailsForLeaders]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlDateTime KezdDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                KezdDat, SqlDateTime.Null);

            SqlDateTime VegeDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                VegeDat, SqlDateTime.Null);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KezdDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@KezdDat"].Value = KezdDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VegeDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@VegeDat"].Value = VegeDat_sqltype;

            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    public Result GetAllExcelLogDetails(ExecParam ExecParam, String KezdDat, String VegeDat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_ExcelLogStatistics");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_ExcelLogStatistics]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlDateTime KezdDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                KezdDat, SqlDateTime.Null);

            SqlDateTime VegeDat_sqltype = Contentum.eBusinessDocuments.Utility.SetSqlDateTimeFromString(
                VegeDat, SqlDateTime.Null);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KezdDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@KezdDat"].Value = KezdDat_sqltype;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@VegeDat", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@VegeDat"].Value = VegeDat_sqltype;

            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    /// <summary>
    /// A ParentIdArray-ban megadott ügyiratokhoz szerelt ügyiratok lekérése
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="ParentIdArray"></param>
    /// <returns></returns>
    public Result GetAllSzereltByParent(ExecParam ExecParam, string[] ParentIdArray)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetAllSzereltByParent");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllSzereltByParent]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            string ParentIdList = Contentum.eUtility.Utils.GetStringFromArray(ParentIdArray, ",");
            SqlComm.Parameters.Add(new SqlParameter("@ParentIdList", SqlDbType.NVarChar));
            SqlComm.Parameters["@ParentIdList"].Value = ParentIdList;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    /// <summary>
    /// ExecParam.RecorId-hoz szerelt ügyiratok lekérdezése bárhol is legyen a hiearchiában
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    public Result GetAllSzereltByNode(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetAllSzereltByNode");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllSzereltByNode]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new SqlParameter("@NodeId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@NodeId"].Value = SqlGuid.Parse(ExecParam.Record_Id);


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    /// <summary>
    /// NodeIdArray-ben megadott ügyiratokhoz lekéri az összes elõkészített szerelt ügyiratokat,
    /// ami hozzá vagy bármely szülõjéhez van szerelésre elõkészítve (mind, vagy csak a hierarchiában elsõket)
    /// </summary>
    /// <param name="ExecParam"></param>
    ///<param name="bOnlyFirstInHierarchy">Ha értéke true, megállunk a hierarchiában elsõ elõkészített szerelésnél, nem megyünk tovább a láncban</param>
    /// <returns></returns>
    public Result GetAllElokeszitettSzereltByNode(ExecParam ExecParam, string[] NodeIdArray, bool bOnlyFirstInHierarchy)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetAllElokeszitettSzereltByNode");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllElokeszitettSzereltByNode]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            string NodeIdList = Contentum.eUtility.Utils.GetStringFromArray(NodeIdArray, ",");
            SqlComm.Parameters.Add(new SqlParameter("@NodeIdList", SqlDbType.NVarChar));
            SqlComm.Parameters["@NodeIdList"].Value = NodeIdList;

            SqlComm.Parameters.Add(new SqlParameter("@OnlyFirstInHierarchy", SqlDbType.Char, 1));
            SqlComm.Parameters["@OnlyFirstInHierarchy"].Value = bOnlyFirstInHierarchy ? Contentum.eUtility.Constants.Database.Yes : Contentum.eUtility.Constants.Database.No;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result UpdateAllIraIratIrattarbaVetelDat(ExecParam ExecParam, string[] UgyiratIds, DateTime IrattarbaVetelDat)
    {
        return this.UpdateAllIraIratIrattarbaAdasVetelDat(ExecParam, UgyiratIds, null, IrattarbaVetelDat);
    }

    public Result UpdateAllIraIratIrattarbaAdasDat(ExecParam ExecParam, string[] UgyiratIds, DateTime IrattarbaAdasDat)
    {
        return this.UpdateAllIraIratIrattarbaAdasVetelDat(ExecParam, UgyiratIds, IrattarbaAdasDat, null);
    }

    private Result UpdateAllIraIratIrattarbaAdasVetelDat(ExecParam ExecParam, string[] UgyiratIds, DateTime? IrattarbaAdasDat, DateTime? IrattarbaVetelDat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokUpdateAllIraIratIrattarFields");

        Result _ret = new Result();

        if (UgyiratIds.Length > 0)
        {
            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokUpdateAllIraIratIrattarFields]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;
                SqlComm.CommandTimeout = 300;

                SqlComm.Parameters.Add(new SqlParameter("@UgyiratIds", SqlDbType.NVarChar));
                SqlComm.Parameters["@UgyiratIds"].Value = String.Join(",", UgyiratIds);

                if (IrattarbaAdasDat.HasValue)
                {
                    SqlComm.Parameters.Add(new SqlParameter("@IrattarbaKuldes", SqlDbType.DateTime));
                    SqlComm.Parameters["@IrattarbaKuldes"].Value = IrattarbaAdasDat.Value;
                }

                if (IrattarbaVetelDat.HasValue)
                {
                    SqlComm.Parameters.Add(new SqlParameter("@IrattarbaVetel", SqlDbType.DateTime));
                    SqlComm.Parameters["@IrattarbaVetel"].Value = IrattarbaVetelDat.Value;
                }

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result AtadasraKijeloles_Tomeges(ExecParam ExecParam, string Ids, string Vers, string KovetkezoFelelosId, string KezFeljegyzesTipus, string Leiras)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratok_AtadasAtvetelTomeges");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratok_AtadasAtvetelTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Ids;

            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = Vers;

            SqlComm.Parameters.Add(new SqlParameter("@KovetkezoFelelos", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@KovetkezoFelelos"].Value = SqlGuid.Parse(KovetkezoFelelosId);

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlComm.Parameters.Add(new SqlParameter("@KezFeljegyzesTipus", SqlDbType.NVarChar));
            SqlComm.Parameters["@KezFeljegyzesTipus"].Size = 64;
            SqlComm.Parameters["@KezFeljegyzesTipus"].Value = KezFeljegyzesTipus;

            SqlComm.Parameters.Add(new SqlParameter("@Leiras", SqlDbType.NVarChar));
            SqlComm.Parameters["@Leiras"].Size = 100;
            SqlComm.Parameters["@Leiras"].Value = Leiras;

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Atvetel_Tomeges(ExecParam ExecParam, string[] Ids, string[] Vers, string KovetkezoOrzo, string FelhCsoport_Id_IrattariAtvevo, DateTime? IrattarbaVetelDat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratok_AtadasAtvetelTomeges");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratok_AtadasAtvetelTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Contentum.eUtility.Search.GetSqlInnerString(Ids);

            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = String.Join(",", Vers);

            if (!String.IsNullOrEmpty(KovetkezoOrzo))
            {
                SqlComm.Parameters.Add(new SqlParameter("@KovetkezoOrzo", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@KovetkezoOrzo"].Value = SqlGuid.Parse(KovetkezoOrzo);
            }

            if (!String.IsNullOrEmpty(FelhCsoport_Id_IrattariAtvevo))
            {
                SqlComm.Parameters.Add(new SqlParameter("@FelhCsoport_Id_IrattariAtvevo", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@FelhCsoport_Id_IrattariAtvevo"].Value = SqlGuid.Parse(FelhCsoport_Id_IrattariAtvevo);
            }

            if (IrattarbaVetelDat.HasValue)
            {
                SqlComm.Parameters.Add(new SqlParameter("@IrattarbaVetelDat", SqlDbType.DateTime));
                SqlComm.Parameters["@IrattarbaVetelDat"].Value = IrattarbaVetelDat;
            }

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = SqlGuid.Parse(ExecParam.Felhasznalo_Id);

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }


    public Result Update_Tomeges(ExecParam ExecParam, string[] Ids, string[] Vers, string[] Csoport_Id_Felelos_Array, EREC_UgyUgyiratok Record, DateTime ExecutionTime, bool KellKezbesitesiTetel)
    {
        string spName = "sp_EREC_UgyUgyiratokUpdate_Tomeges";
        if (Csoport_Id_Felelos_Array != null)
        {
            spName = "sp_EREC_UgyUgyiratokUpdate_Tomeges2";
            KellKezbesitesiTetel = true;
        }

        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, spName);
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new SqlCommand("[" + spName + "]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Dictionary<String, Utility.Parameter> sp_EREC_UgyUgyiratokUpdateParameters = null;

            Record.Base.Updated.ModositasIdo = true;
            Record.Base.Updated.Modosito_id = true;
            Record.Base.ModositasIdo = ExecutionTime.ToString();
            Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
            if (sp_EREC_UgyUgyiratokUpdateParameters == null)
            {
                sp_EREC_UgyUgyiratokUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_UgyUgyiratokUpdate]");
            }

            if (Csoport_Id_Felelos_Array != null)
            {
                Record.Updated.Csoport_Id_Felelos = true;
            }

            Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_UgyUgyiratokUpdateParameters);

            Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

            //BLG_12197
            DataTable tIds = new DataTable();
            tIds.Columns.Add(new DataColumn("id", typeof(string)));
            tIds.Columns.Add(new DataColumn("ver", typeof(int)));

            if (spName == "sp_EREC_UgyUgyiratokUpdate_Tomeges2")
                tIds.Columns.Add(new DataColumn("Csoport_Id_Felelos", typeof(string)));

            if (Csoport_Id_Felelos_Array != null)
            {
                for (int j = 0; j < Csoport_Id_Felelos_Array.Length; j++)
                {
                    if (Csoport_Id_Felelos_Array[j] != null)
                        Csoport_Id_Felelos_Array[j] = Csoport_Id_Felelos_Array[j].Replace("'", "");
                }
            }

            if (Ids.Length > 0 && Vers.Length > 0)
            {
                for (int i = 0; i < Ids.Length; i++)
                {
                    if (Ids[i] != null && Vers[i] != null)
                    {
                        switch (spName)
                        {
                            case "sp_EREC_UgyUgyiratokUpdate_Tomeges":
                                tIds.Rows.Add(Ids[i], Vers[i]);
                                break;

                            case "sp_EREC_UgyUgyiratokUpdate_Tomeges2":
                                tIds.Rows.Add(Ids[i], Vers[i], Csoport_Id_Felelos_Array[i]);
                                break;

                        }
                    }
                }
            }

            //Old code
            //SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.VarChar));
            //SqlComm.Parameters["@Ids"].Value =


            SqlComm.Parameters.RemoveAt("@Id");
            SqlParameter tparam = SqlComm.Parameters.AddWithValue("@Ids", tIds);
            // these next lines are important to map the C# DataTable object to the correct SQL User Defined Type
            tparam.SqlDbType = SqlDbType.Structured;

            if (spName == "sp_EREC_UgyUgyiratokUpdate_Tomeges")
                tparam.TypeName = "dbo.IdTable";
            else
                tparam.TypeName = "dbo.IdTable2";

            //SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            //SqlComm.Parameters["@Vers"].Value = Vers;

            //if (Csoport_Id_Felelos_Array != null)
            //{
            //    SqlComm.Parameters.Add("@Csoport_Id_Felelos_List", SqlDbType.NVarChar).Value = Contentum.eUtility.Search.GetSqlInnerString(Csoport_Id_Felelos_Array);
            //}

            if (!KellKezbesitesiTetel)
            {
                SqlParameter sqlParam = SqlComm.Parameters.Add("@KellKezbesitesiTetel", SqlDbType.Char);
                sqlParam.Size = 1;
                sqlParam.Value = '0';
            }

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result SkontrobolKivetel_Tomeges(ExecParam ExecParam, string Ids, string Vers, DateTime skontroVege)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratok_SkontrobolKiTomeges");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratok_SkontrobolKiTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezete", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezete"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LoginUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@LoginUserId"].Value = ExecParam.Typed.LoginUser_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Ids", SqlDbType.NVarChar));
            SqlComm.Parameters["@Ids"].Value = Ids;

            SqlComm.Parameters.Add(new SqlParameter("@Vers", SqlDbType.NVarChar));
            SqlComm.Parameters["@Vers"].Value = Vers;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SkontroVege", SqlDbType.DateTime));
            SqlComm.Parameters["@SkontroVege"].Value = skontroVege;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Tranz_id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Tranz_id"].Value = Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(dataContext.Tranz_Id, SqlGuid.Null);


            SqlComm.Parameters.Add(new SqlParameter("@UserHostAddress", SqlDbType.NVarChar));
            SqlComm.Parameters["@UserHostAddress"].Value = ExecParam.UserHostAddress;

            SqlComm.Parameters.Add(new SqlParameter("@MuveletKim", SqlDbType.NVarChar));
            SqlComm.Parameters["@MuveletKim"].Value = "SIKERES";

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result UgyiratAtadasSztorno_ElsodlegesIratpeldanyokVissza(ExecParam execParam, string ugyiratId)
    {
        return this.UgyiratAtadasSztornoVagyVisszakuldes_ElsodlegesIratpeldanyokVissza(execParam, ugyiratId, false);
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_UgyiratAtadasSztorno_ElsodlegesIratpeldanyokVissza");
        //Result _ret = new Result();
        //_ret.Ds = new DataSet();
        //try
        //{
        //    System.Data.SqlClient.SqlCommand SqlComm = new SqlCommand("[sp_UgyiratAtadasSztorno_ElsodlegesIratpeldanyokVissza]");
        //    SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //    SqlComm.Connection = dataContext.Connection;
        //    SqlComm.Transaction = dataContext.Transaction;

        //    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
        //    SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

        //    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratId", SqlDbType.UniqueIdentifier));
        //    SqlComm.Parameters["@UgyiratId"].Value = SqlGuid.Parse(ugyiratId);

        //    if (!string.IsNullOrEmpty(execParam.Page_Id))
        //    {
        //        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Tranz_id", SqlDbType.UniqueIdentifier));
        //        SqlComm.Parameters["@Tranz_id"].Value = SqlGuid.Parse(execParam.Page_Id);
        //    }

        //    SqlDataAdapter adapter = new SqlDataAdapter();
        //    adapter.SelectCommand = SqlComm;
        //    adapter.Fill(_ret.Ds);
        //}
        //catch (SqlException e)
        //{
        //    _ret.ErrorCode = e.ErrorCode.ToString();
        //    _ret.ErrorMessage = e.Message;
        //}

        //log.SpEnd(execParam, _ret);

        //return _ret;
    }

    public Result UgyiratVisszakuldes_ElsodlegesIratpeldanyokVissza(ExecParam execParam, string ugyiratId)
    {
        return this.UgyiratAtadasSztornoVagyVisszakuldes_ElsodlegesIratpeldanyokVissza(execParam, ugyiratId, true);
    }

    private Result UgyiratAtadasSztornoVagyVisszakuldes_ElsodlegesIratpeldanyokVissza(ExecParam execParam, string ugyiratId, bool isVisszakuldes)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_UgyiratAtadasSztorno_ElsodlegesIratpeldanyokVissza");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new SqlCommand("[sp_UgyiratAtadasSztorno_ElsodlegesIratpeldanyokVissza]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@UgyiratId"].Value = SqlGuid.Parse(ugyiratId);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IsVisszakuldes", SqlDbType.Char, 1));
            SqlComm.Parameters["@IsVisszakuldes"].Value = isVisszakuldes ? '1' : '0';

            if (!string.IsNullOrEmpty(execParam.Page_Id))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Tranz_id", SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@Tranz_id"].Value = SqlGuid.Parse(execParam.Page_Id);
            }

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);

        return _ret;
    }



    /// <summary>
    /// ExecParam.RecorId-ban megadott szülõ ügyirat-hiearchiájának lekérdezése lekérdezése
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    public Result GetSzuloHiearchy(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetSzuloHiearchy");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetSzuloHiearchy]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetAllElsodlegesIratpeldany(string UgyiratId)
    {
        Result _ret = new Result();

        try
        {
            if (string.IsNullOrEmpty(UgyiratId))
                return new Result();

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllElsodlegesIratpeldany]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;


            SqlComm.Parameters.Add(new SqlParameter("@UgyiratId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@UgyiratId"].Value = SqlGuid.Parse(UgyiratId);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        return _ret;
    }

    public Result GetUtolsoAlszam(ExecParam execParam, string ugyirat_id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_GetUtolsoAlszam");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_GetUtolsoAlszam]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;


            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@EREC_UgyUgyiratok_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@EREC_UgyUgyiratok_Id"].Value = SqlGuid.Parse(ugyirat_id);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

            string UtolsoAlszam = ds.Tables[0].Rows[0]["UtolsoAlszam"].ToString();

            _ret.Record = UtolsoAlszam;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetIratszam(ExecParam execParam, string ugyiratid)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_GetIratszam");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_GetIratszam]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;


            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@UgyiratId"].Value = SqlGuid.Parse(ugyiratid);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

            string iratszam = ds.Tables[0].Rows[0]["Iratszam"].ToString();

            _ret.Record = iratszam;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    // BLG_619
    public Result GetUgyFelelosSzervezetKod(ExecParam execParam, string ugyiratid)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_GetUgyFelelosSzervezetKod");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_GetUgyFelelosSzervezetKod]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;


            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@UgyiratId"].Value = SqlGuid.Parse(ugyiratid);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

            string szervezetKod = ds.Tables[0].Rows[0]["UgyFelelos_SzervezetKod"].ToString();

            _ret.Record = szervezetKod;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetAllEsemenyWithExtension(ExecParam execParam, KRT_EsemenyekSearch _KRT_EsemenyekSearch, string ugyiratId, string ugyintezoId, bool ForMunkanaplo)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_UgyUgyiratokGetAllEsemenyWithExtension");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_EsemenyekSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where = Contentum.eUtility.Search.ConcatWhereExpressions(query.Where, _KRT_EsemenyekSearch.WhereByManual);

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllEsemenyWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, _KRT_EsemenyekSearch.OrderBy, _KRT_EsemenyekSearch.TopRow);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@UgyiratId"].Value = SqlGuid.Parse(ugyiratId);

            if (!String.IsNullOrEmpty(ugyintezoId))
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyintezoId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@UgyintezoId"].Value = SqlGuid.Parse(ugyintezoId);
            }

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
            //SqlComm.Parameters["@OrderBy"].Size = 200;
            //SqlComm.Parameters["@OrderBy"].Value = " order by " + orderBy;

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            //SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@pageNumber", System.Data.SqlDbType.Int));
            //SqlComm.Parameters["@pageNumber"].Value = execParam.Paging.PageNumber;

            //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@pageSize", System.Data.SqlDbType.Int));
            //SqlComm.Parameters["@pageSize"].Value = execParam.Paging.PageSize;

            //if (!string.IsNullOrEmpty(execParam.Paging.SelectedRowId))
            //{
            //    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SelectedRowId", System.Data.SqlDbType.UniqueIdentifier));
            //    SqlComm.Parameters["@SelectedRowId"].Value = SqlGuid.Parse(execParam.Paging.SelectedRowId);
            //}

            if (ForMunkanaplo)
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ForMunkanaplo", System.Data.SqlDbType.Char));
                SqlComm.Parameters["@ForMunkanaplo"].Value = Contentum.eUtility.Constants.Database.Yes;
            }


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetAllElozmeny(ExecParam execParam, string[] UgyiratIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_UgyUgyiratokGetAllElozmeny");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllElozmeny]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratIds", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@UgyiratIds"].Value = "(" + Contentum.eUtility.Search.GetSqlInnerString(UgyiratIds) + ")";

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetUgyiratObjektumai(ExecParam ExecParam, string Obj_Id, string Obj_type, bool WithParent)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokGetUgyiratObjektumai");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetUgyiratObjektumai]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add("@ExecutorUserId", SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.Felhasznalo_Id;
            SqlComm.Parameters.Add("@Obj_Id", SqlDbType.UniqueIdentifier).Value = SqlGuid.Parse(Obj_Id);
            SqlComm.Parameters.Add("@Obj_type", SqlDbType.NVarChar).Value = Obj_type;
            SqlComm.Parameters.Add("@WithParent", SqlDbType.Char).Value = WithParent ? Contentum.eUtility.Constants.Database.Yes : Contentum.eUtility.Constants.Database.No;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    #region CR3206 - Irattári struktúra
    public Result GetAllByIrattariHelyErtek(ExecParam execParam, string IrattariHely)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_UgyUgyiratokGetAllByIrattariHelyErtek");
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetAllByIrattariHelyErtek]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IrattarId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IrattarId"].Value = SqlGuid.Parse(IrattariHely);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }
    #endregion

    public Result CalendarView(ExecParam ExecParam, EREC_UgyUgyiratokCalendarViewSearch _EREC_UgyUgyiratokCalendarViewSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokCalendarView");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_UgyUgyiratokCalendarViewSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _EREC_UgyUgyiratokCalendarViewSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_EREC_UgyUgyiratokCalendarViewSearch.WhereByManual))
            {
                query.Where += " and " + _EREC_UgyUgyiratokCalendarViewSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokCalendarView]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@DatumTol", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@DatumTol"].Value = _EREC_UgyUgyiratokCalendarViewSearch.DatumTol;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@DatumIg", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@DatumIg"].Value = _EREC_UgyUgyiratokCalendarViewSearch.DatumIg;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where"].Value = query.Where;

            SqlComm.Parameters.Add(new SqlParameter("@CsakAktivIrat", SqlDbType.Bit));
            SqlComm.Parameters["@CsakAktivIrat"].Value = _EREC_UgyUgyiratokCalendarViewSearch.CsakAktivIrat ? 1 : 0;

            if (_EREC_UgyUgyiratokCalendarViewSearch.OrderBy.Trim() != "")
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@OrderBy"].Size = 400;
                if (_EREC_UgyUgyiratokCalendarViewSearch.OrderBy == Contentum.eUtility.Constants.BusinessDocument.nullString)
                    SqlComm.Parameters["@OrderBy"].Value = String.Empty;
                else
                    SqlComm.Parameters["@OrderBy"].Value = "order by " + _EREC_UgyUgyiratokCalendarViewSearch.OrderBy;
            }

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyintezoId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@UgyintezoId"].Value = _EREC_UgyUgyiratokCalendarViewSearch.UgyintezoId;

            DataSet ds = new DataSet();
            if (!ExecParam.Fake)
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;
            }
            else
            {
                _ret.Ds = ds;
                _ret.SqlCommand = new SerializableSqlCommand(SqlComm);
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetTomegesOlvasasiJog(ExecParam execParam, string[] ugyiratIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_EREC_UgyUgyiratokGetTomegesOlvasasiJog");
        Result _ret = new Result();

        try
        {

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratokGetTomegesOlvasasiJog]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratIds", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@UgyiratIds"].Value = String.Join(",", ugyiratIds);
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Felhasznalo_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Felhasznalo_Id"].Value = execParam.Typed.Felhasznalo_Id;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = execParam.Typed.FelhasznaloSzervezet_Id;


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }
}