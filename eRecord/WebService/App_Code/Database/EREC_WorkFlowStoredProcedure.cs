﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eUtility;
using System.Collections.Generic;
using Contentum.eBusinessDocuments;
using System.Data.SqlClient;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for WorkFlowStoredProcedure
/// </summary>
public class EREC_WorkFlowStoredProcedure
{
    private DataContext dataContext;

    public EREC_WorkFlowStoredProcedure(DataContext _dataContext)
	{
        this.dataContext = _dataContext;
	}

    public Result GetWorkFlowAdat(ExecParam ExecParam, SqlString ContentType, SqlString FazisErtek, SqlString AllapotErtek,
        SqlString Kimenet, SqlGuid BazisObj_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_WF_GetWorkFlowAdat");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_WF_GetWorkFlowAdat]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            #region add parameters

            SqlComm.Parameters.Add("@ContentType", SqlDbType.NVarChar, 100).Value = ContentType;
            SqlComm.Parameters.Add("@FazisErtek", SqlDbType.NVarChar, 64).Value = FazisErtek;
            SqlComm.Parameters.Add("@AllapotErtek", SqlDbType.NVarChar, 64).Value = AllapotErtek;
            SqlComm.Parameters.Add("@Kimenet", SqlDbType.NVarChar, 64).Value = Kimenet;
            SqlComm.Parameters.Add("@BazisObj_Id", SqlDbType.UniqueIdentifier).Value = BazisObj_Id;

            #endregion

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}
