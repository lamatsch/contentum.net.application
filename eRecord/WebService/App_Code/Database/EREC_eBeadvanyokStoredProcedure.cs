using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_eBeadvanyokStoredProcedure
/// </summary>
public partial class EREC_eBeadvanyokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_eBeadvanyokSearch _EREC_eBeadvanyokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_eBeadvanyokGetAllWithExtension");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_eBeadvanyokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_eBeadvanyokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_eBeadvanyokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;



            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_eBeadvanyokSearch.OrderBy, _EREC_eBeadvanyokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}