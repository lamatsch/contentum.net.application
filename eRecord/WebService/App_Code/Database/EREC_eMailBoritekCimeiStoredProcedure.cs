using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_eMailBoritekCimeiStoredProcedure
/// </summary>

public partial class EREC_eMailBoritekCimeiStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_eMailBoritekCimeiSearch _EREC_eMailBoritekCimeiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_eMailBoritekCimeiGetAllWithExtension");

        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_eMailBoritekCimeiSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_eMailBoritekCimeiSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_eMailBoritekCimeiGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_eMailBoritekCimeiSearch.OrderBy, _EREC_eMailBoritekCimeiSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}