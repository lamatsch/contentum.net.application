using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class EREC_eMailBoritekCsatolmanyokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_eMailBoritekCsatolmanyokSearch _EREC_eMailBoritekCsatolmanyokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_eMailBoritekCsatolmanyokGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_eMailBoritekCsatolmanyokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_eMailBoritekCsatolmanyokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_eMailBoritekCsatolmanyokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_eMailBoritekCsatolmanyokSearch.OrderBy, _EREC_eMailBoritekCsatolmanyokSearch.TopRow);

            // Felhaszn�l� szervezete a jogosults�gsz�r�shez
            SqlComm.Parameters.Add("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier).Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}
