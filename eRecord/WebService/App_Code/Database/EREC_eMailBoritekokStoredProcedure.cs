using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for EREC_eMailBoritekokStoredProcedure
/// </summary>

public partial class EREC_eMailBoritekokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_eMailBoritekokSearch _EREC_eMailBoritekokSearch,bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_eMailBoritekokGetAllWithExtension");
        Result _ret = new Result();
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_EREC_eMailBoritekokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _EREC_eMailBoritekokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_EREC_eMailBoritekokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_eMailBoritekokSearch.OrderBy, _EREC_eMailBoritekokSearch.TopRow);

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
            SqlComm.Parameters["@Jogosultak"].Size = 1;
            SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';

            // Felhaszn�l� szervezete a jogosults�gsz�r�shez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllWithExtension(ExecParam ExecParam, EREC_eMailBoritekokSearch _EREC_eMailBoritekokSearch)
    {
        return GetAllWithExtension(ExecParam,_EREC_eMailBoritekokSearch,false);
    }
    
    
    public Result GetEMailForrasTipusByFelado(ExecParam ExecParam, string felado)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_GetEMailForrasTipusByFelado");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_GetEMailForrasTipusByFelado]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@Felado", SqlDbType.Char));
            SqlComm.Parameters["@Felado"].Size = 4000;
            SqlComm.Parameters["@Felado"].Value = felado;

            // Org sz�r�s miatt
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Record = ds.Tables[0].Rows[0]["ForrasTipus"].ToString();

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result GetEMailBoritekCimTipusByFelado(ExecParam ExecParam, string felado)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_GetEMailBoritekCimTipusByFelado");
        Result _ret = new Result();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_GetEMailBoritekCimTipusByFelado]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new SqlParameter("@Felado", SqlDbType.Char));
            SqlComm.Parameters["@Felado"].Size = 4000;
            SqlComm.Parameters["@Felado"].Value = felado;

            // Org sz�r�s miatt
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Record = ds.Tables[0].Rows[0];

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}