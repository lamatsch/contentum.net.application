using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ErtesitesStoredProcedure
/// </summary>

public partial class ErtesitesStoredProcedure
{
    private DataContext dataContext;

    public ErtesitesStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }
    /// <summary>
    /// Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    public Result Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier) { Value = new Guid(ExecParam.Felhasznalo_Id) });

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
            Logger.Error(String.Format("Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas t�rolt elj�r�s hiba: {0},{1}", e.ErrorCode.ToString(), e.Message));
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    /// <summary>
    /// Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    public Result Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier) { Value = new Guid(ExecParam.Felhasznalo_Id) });

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
            Logger.Error(String.Format("Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas t�rolt elj�r�s hiba: {0},{1}", e.ErrorCode.ToString(), e.Message));
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}