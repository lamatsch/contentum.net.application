using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_Alairas_Folyamat_TetelekStoredProcedure
/// </summary>
public partial class EREC_Alairas_Folyamat_TetelekStoredProcedure
{
    //private SqlConnection Connection;
    
    //private String ConnectionString;
    
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_EREC_Alairas_Folyamat_TetelekInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_EREC_Alairas_Folyamat_TetelekUpdateParameters = null;
    
    public EREC_Alairas_Folyamat_TetelekStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;
         
        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
            //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
            //throw e;
        //}
	}
    public Result Insert(String Method, ExecParam ExecParam, EREC_Alairas_Folyamat_Tetelek Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, EREC_Alairas_Folyamat_Tetelek Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method=="") Method = "Insert";
            switch (Method)
            {
               case Constants.Insert:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Alairas_Folyamat_TetelekInsert");
                  Record.Base.Updated.LetrehozasIdo = true;
                  Record.Base.Updated.Letrehozo_id = true;
                  Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                  Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;       
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;                  
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_Alairas_Folyamat_TetelekInsert]");
                  if (sp_EREC_Alairas_Folyamat_TetelekInsertParameters == null)
                  {
                     //sp_EREC_Alairas_Folyamat_TetelekInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_Alairas_Folyamat_TetelekInsert]");
                     //sp_EREC_Alairas_Folyamat_TetelekInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_Alairas_Folyamat_TetelekInsert]");
                     sp_EREC_Alairas_Folyamat_TetelekInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_Alairas_Folyamat_TetelekInsert]");
                  }
                
                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_Alairas_Folyamat_TetelekInsertParameters);

                  Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                  break;
               case Constants.Update:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Alairas_Folyamat_TetelekUpdate");
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_Alairas_Folyamat_TetelekUpdate]");
                  if (sp_EREC_Alairas_Folyamat_TetelekUpdateParameters == null)
                  {
                     //sp_EREC_Alairas_Folyamat_TetelekUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_Alairas_Folyamat_TetelekUpdate]");
                     //sp_EREC_Alairas_Folyamat_TetelekUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_Alairas_Folyamat_TetelekUpdate]");
                     sp_EREC_Alairas_Folyamat_TetelekUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_Alairas_Folyamat_TetelekUpdate]");
                  }

                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_Alairas_Folyamat_TetelekUpdateParameters);

                  Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                  break;
         }
        
         SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         //SqlComm.Connection = Connection;
         //SqlComm.Connection = sqlConnection;
         SqlComm.Connection = dataContext.Connection;
         SqlComm.Transaction = dataContext.Transaction;

         try
         {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
       //}        
       
       log.SpEnd(ExecParam, _ret);
       
       return _ret;         
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_EREC_Alairas_Folyamat_TetelekGet");
    
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

         try
         {                        
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Alairas_Folyamat_TetelekGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
                       
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);            
            
             EREC_Alairas_Folyamat_Tetelek _EREC_Alairas_Folyamat_Tetelek = new EREC_Alairas_Folyamat_Tetelek();
             Utility.LoadBusinessDocumentFromDataAdapter(_EREC_Alairas_Folyamat_Tetelek, SqlComm);
             _ret.Record = _EREC_Alairas_Folyamat_Tetelek;

         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
                     
    }

    public Result GetAll(ExecParam ExecParam, EREC_Alairas_Folyamat_TetelekSearch _EREC_Alairas_Folyamat_TetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Alairas_Folyamat_TetelekGetAll");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_EREC_Alairas_Folyamat_TetelekSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _EREC_Alairas_Folyamat_TetelekSearch.WhereByManual;

               SqlCommand SqlComm = new SqlCommand("[sp_EREC_Alairas_Folyamat_TetelekGetAll]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Alairas_Folyamat_TetelekSearch.OrderBy, _EREC_Alairas_Folyamat_TetelekSearch.TopRow);

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Alairas_Folyamat_TetelekDelete");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();
            
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Alairas_Folyamat_TetelekDelete]");

            try
            {
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);
            
               SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    
    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Alairas_Folyamat_TetelekInvalidate");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               SqlCommand SqlComm = new SqlCommand("[sp_EREC_Alairas_Folyamat_TetelekInvalidate]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

               SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}