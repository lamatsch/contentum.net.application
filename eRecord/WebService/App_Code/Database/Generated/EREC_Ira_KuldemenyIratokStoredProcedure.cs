using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_Ira_KuldemenyIratokStoredProcedure
/// </summary>
public partial class EREC_Ira_KuldemenyIratokStoredProcedure
{
    //private SqlConnection Connection;

    private String ConnectionString;

    internal static Dictionary<String, Utility.Parameter> sp_EREC_Ira_KuldemenyIratokInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_EREC_Ira_KuldemenyIratokUpdateParameters = null;
    
    public EREC_Ira_KuldemenyIratokStoredProcedure(HttpApplicationState App)
	{
        ConnectionString = DataContext.GetConnectionString(App);

        try
        {
            //Connection = Database.Connect(App);
        }
        catch (Exception e)
        {
            throw e;
        }
	}
    public Result Insert(String Method, ExecParam ExecParam, EREC_Ira_KuldemenyIratok Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, EREC_Ira_KuldemenyIratok Record, DateTime ExecutionTime)
    {
        Result _ret = new Result();

        using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        {
            sqlConnection.Open();
        System.Data.SqlClient.SqlCommand SqlComm = null;
        // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
        if (Method=="") Method = "Insert";
        switch (Method)
        {
            case Constants.Insert:
                Record.Base.Updated.LetrehozasIdo = true;
                Record.Base.Updated.Letrehozo_id = true;
                Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_Ira_KuldemenyIratokInsert]");
                if (sp_EREC_Ira_KuldemenyIratokInsertParameters == null)
                {
                    //sp_EREC_Ira_KuldemenyIratokInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_Ira_KuldemenyIratokInsert]");
                    sp_EREC_Ira_KuldemenyIratokInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_Ira_KuldemenyIratokInsert]");
                }
                
                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_Ira_KuldemenyIratokInsertParameters);

                Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                break;
            case Constants.Update:
                Record.Base.Updated.ModositasIdo = true;
                Record.Base.Updated.Modosito_id = true;
                Record.Base.ModositasIdo = ExecutionTime.ToString();
                Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_Ira_KuldemenyIratokUpdate]");
                if (sp_EREC_Ira_KuldemenyIratokUpdateParameters == null)
                {
                    //sp_EREC_Ira_KuldemenyIratokUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_Ira_KuldemenyIratokUpdate]");
                    sp_EREC_Ira_KuldemenyIratokUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_Ira_KuldemenyIratokUpdate]");
                }

                Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_Ira_KuldemenyIratokUpdateParameters);

                Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                break;
        }
        
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
        //SqlComm.Connection = Connection;
        SqlComm.Connection = sqlConnection;

        try
        {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        sqlConnection.Close();
        } 

        return _ret;         
    }


    public Result Get(ExecParam ExecParam)
    {
        Result _ret = new Result();
        using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        {
            sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_EREC_Ira_KuldemenyIratokGet]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                SqlComm.Connection = sqlConnection;

                Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);

                EREC_Ira_KuldemenyIratok _EREC_Ira_KuldemenyIratok = new EREC_Ira_KuldemenyIratok();
                Utility.LoadBusinessDocumentFromDataAdapter(_EREC_Ira_KuldemenyIratok, SqlComm);
                _ret.Record = _EREC_Ira_KuldemenyIratok;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            sqlConnection.Close();
        }

        return _ret;

    }

    public Result GetAll(ExecParam ExecParam, EREC_Ira_KuldemenyIratokSearch _EREC_Ira_KuldemenyIratokSearch)
    {
        Result _ret = new Result();

        using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        {
            sqlConnection.Open();

            try
            {
                Query query = new Query();
                query.BuildFromBusinessDocument(_EREC_Ira_KuldemenyIratokSearch);

                //az egyedi szuresi feltel hozzaadasa!!!
                query.Where += _EREC_Ira_KuldemenyIratokSearch.WhereByManual;

                SqlCommand SqlComm = new SqlCommand("[sp_EREC_Ira_KuldemenyIratokGetAll]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                SqlComm.Connection = sqlConnection;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Ira_KuldemenyIratokSearch.OrderBy, _EREC_Ira_KuldemenyIratokSearch.TopRow);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            sqlConnection.Close();
        }

        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Result _ret = new Result();
        SqlCommand SqlComm = new SqlCommand("[sp_EREC_Ira_KuldemenyIratokDelete]");

        using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        {
            sqlConnection.Open();
            try
            {
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                SqlComm.Connection = sqlConnection;

                Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            sqlConnection.Close();
        }

        return _ret;
    }
    
    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Result _ret = new Result();

        using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        {
            sqlConnection.Open();

            try
            {
                SqlCommand SqlComm = new SqlCommand("[sp_EREC_Ira_KuldemenyIratokInvalidate]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                SqlComm.Connection = sqlConnection;

                Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

            sqlConnection.Close();
        }
        return _ret;
    }
}