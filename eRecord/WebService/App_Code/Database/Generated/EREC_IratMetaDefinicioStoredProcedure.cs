using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_IratMetaDefinicioStoredProcedure
/// </summary>
public partial class EREC_IratMetaDefinicioStoredProcedure
{
    //private SqlConnection Connection;
    
    //private String ConnectionString;
    
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_EREC_IratMetaDefinicioInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_EREC_IratMetaDefinicioUpdateParameters = null;
    
    public EREC_IratMetaDefinicioStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;
         
        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
            //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
            //throw e;
        //}
	}
    public Result Insert(String Method, ExecParam ExecParam, EREC_IratMetaDefinicio Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, EREC_IratMetaDefinicio Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        Logger.Info("EREC_IratMetaDefinicioStoredProcedure." + Method + " start");
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method=="") Method = "Insert";
            switch (Method)
            {
               case Constants.Insert:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IratMetaDefinicioInsert");
                  Record.Base.Updated.LetrehozasIdo = true;
                  Record.Base.Updated.Letrehozo_id = true;
                  Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                  Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                  Logger.Info("sp_EREC_IratMetaDefinicioInsert futtat�sa");
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_IratMetaDefinicioInsert]");
                  if (sp_EREC_IratMetaDefinicioInsertParameters == null)
                  {
                     //sp_EREC_IratMetaDefinicioInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_IratMetaDefinicioInsert]");
                     //sp_EREC_IratMetaDefinicioInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_IratMetaDefinicioInsert]");
                     sp_EREC_IratMetaDefinicioInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_IratMetaDefinicioInsert]");
                  }
                
                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_IratMetaDefinicioInsertParameters);

                  Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                  break;
               case Constants.Update:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IratMetaDefinicioUpdate");
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                  Logger.Info("sp_EREC_IratMetaDefinicioUpdate futtat�sa");
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_IratMetaDefinicioUpdate]");
                  if (sp_EREC_IratMetaDefinicioUpdateParameters == null)
                  {
                     //sp_EREC_IratMetaDefinicioUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_IratMetaDefinicioUpdate]");
                     //sp_EREC_IratMetaDefinicioUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_IratMetaDefinicioUpdate]");
                     sp_EREC_IratMetaDefinicioUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_IratMetaDefinicioUpdate]");
                  }

                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_IratMetaDefinicioUpdateParameters);

                  Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                  break;
         }
        
         SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         //SqlComm.Connection = Connection;
         //SqlComm.Connection = sqlConnection;
         SqlComm.Connection = dataContext.Connection;
         SqlComm.Transaction = dataContext.Transaction;

         try
         {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
         }
         catch (SqlException e)
         {
             Logger.Error(e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
       //}        
         Logger.Info("EREC_IratMetaDefinicioStoredProcedure." + Method + " end");
       log.SpEnd(ExecParam, _ret);
       
       return _ret;         
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_EREC_IratMetaDefinicioGet");
        Logger.Info("EREC_IratMetaDefinicioStoredProcedure.Get start");
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

         try
         {
            Logger.Info("sp_EREC_IratMetaDefinicioGet h�v�sa");   
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IratMetaDefinicioGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
                       
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);            
            
             EREC_IratMetaDefinicio _EREC_IratMetaDefinicio = new EREC_IratMetaDefinicio();
             Utility.LoadBusinessDocumentFromDataAdapter(_EREC_IratMetaDefinicio, SqlComm);
             _ret.Record = _EREC_IratMetaDefinicio;

         }
         catch (SqlException e)
         {
             Logger.Error(e.Message);
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
        //}
        Logger.Info("EREC_IratMetaDefinicioStoredProcedure.Get start");
        log.SpEnd(ExecParam, _ret);
        return _ret;
                     
    }

    public Result GetAll(ExecParam ExecParam, EREC_IratMetaDefinicioSearch _EREC_IratMetaDefinicioSearch)
    {
        Logger.Info("EREC_IratMetaDefinicioStoredProcedure.GetAll start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IratMetaDefinicioGetAll");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_EREC_IratMetaDefinicioSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _EREC_IratMetaDefinicioSearch.WhereByManual;
               Logger.Info("sp_EREC_IratMetaDefinicioGetAll futtat�sa");
               SqlCommand SqlComm = new SqlCommand("[sp_EREC_IratMetaDefinicioGetAll]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IratMetaDefinicioSearch.OrderBy, _EREC_IratMetaDefinicioSearch.TopRow);

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                Logger.Error(e.Message);
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
            //sqlConnection.Close();
        //}
            Logger.Info("EREC_IratMetaDefinicioStoredProcedure.GetAll end");
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Logger.Info("EREC_IratMetaDefinicioStoredProcedure.Delete start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IratMetaDefinicioDelete");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();
            
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IratMetaDefinicioDelete]");
            Logger.Info("sp_EREC_IratMetaDefinicioDelete h�v�sa");
            try
            {
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);
            
               SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
                Logger.Error(e.Message);
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        Logger.Info("EREC_IratMetaDefinicioStoredProcedure.Delete end");
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    
    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IratMetaDefinicioInvalidate");
        Logger.Info("EREC_IratMetaDefinicioStoredProcedure.Invalidate start");
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               SqlCommand SqlComm = new SqlCommand("[sp_EREC_IratMetaDefinicioInvalidate]");
               Logger.Info("sp_EREC_IratMetaDefinicioInvalidate h�v�sa");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

               SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Logger.Error(e.Message);
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        Logger.Info("EREC_IratMetaDefinicioStoredProcedure.Invalidate end");
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}