using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_IrattariHelyekStoredProcedure
/// </summary>
public partial class EREC_IrattariHelyekStoredProcedure
{
    //private SqlConnection Connection;
    
    //private String ConnectionString;
    
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_EREC_IrattariHelyekInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_EREC_IrattariHelyekUpdateParameters = null;
    
    public EREC_IrattariHelyekStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;
         
        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
            //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
            //throw e;
        //}
	}
    public Result Insert(String Method, ExecParam ExecParam, EREC_IrattariHelyek Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, EREC_IrattariHelyek Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method=="") Method = "Insert";
            switch (Method)
            {
               case Constants.Insert:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariHelyekInsert");
                  Record.Base.Updated.LetrehozasIdo = true;
                  Record.Base.Updated.Letrehozo_id = true;
                  Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                  Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;       
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;                  
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_IrattariHelyekInsert]");
                  if (sp_EREC_IrattariHelyekInsertParameters == null)
                  {
                     //sp_EREC_IrattariHelyekInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_IrattariHelyekInsert]");
                     //sp_EREC_IrattariHelyekInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_IrattariHelyekInsert]");
                     sp_EREC_IrattariHelyekInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_IrattariHelyekInsert]");
                  }
                
                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_IrattariHelyekInsertParameters);

                  Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                  break;
               case Constants.Update:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariHelyekUpdate");
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_IrattariHelyekUpdate]");
                  if (sp_EREC_IrattariHelyekUpdateParameters == null)
                  {
                     //sp_EREC_IrattariHelyekUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_IrattariHelyekUpdate]");
                     //sp_EREC_IrattariHelyekUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_IrattariHelyekUpdate]");
                     sp_EREC_IrattariHelyekUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_IrattariHelyekUpdate]");
                  }

                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_IrattariHelyekUpdateParameters);

                  Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                  break;
         }
        
         SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         //SqlComm.Connection = Connection;
         //SqlComm.Connection = sqlConnection;
         SqlComm.Connection = dataContext.Connection;
         SqlComm.Transaction = dataContext.Transaction;

         try
         {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
       //}        
       
       log.SpEnd(ExecParam, _ret);
       
       return _ret;         
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_EREC_IrattariHelyekGet");
    
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

         try
         {                        
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IrattariHelyekGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
                       
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);            
            
             EREC_IrattariHelyek _EREC_IrattariHelyek = new EREC_IrattariHelyek();
             Utility.LoadBusinessDocumentFromDataAdapter(_EREC_IrattariHelyek, SqlComm);
             _ret.Record = _EREC_IrattariHelyek;

         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
                     
    }
    /// <summary>
    /// Befriss�ti az iratt�ri hely mez�t tartalm�t az �sszes a param�terk�nt megadott ID-val ell�tott gy�k�r elem �sszes gyerek�n l�v� �gyiratokban.
    /// </summary>
    /// <param name="ExecParam">ExecParam objektum</param>
    /// <param name="IrattariHelyekId">Az iratt�ri hely amelynek a neve megv�ltozott.</param>
    /// <returns>Result objektum.</returns>
    public Result IrattariHelyekRename(ExecParam ExecParam, string IrattariHelyekId)  {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "[sp_EREC_IrattariHelyekRenameIrattariHely]");
        
        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IrattariHelyekRenameIrattariHely]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
           
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ModifyRootId", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@ModifyRootId"].Value = IrattariHelyekId;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExecutorUserId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.ExecuteNonQuery();

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    public Result GetAll(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariHelyekGetAll");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_EREC_IrattariHelyekSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _EREC_IrattariHelyekSearch.WhereByManual;

               SqlCommand SqlComm = new SqlCommand("[sp_EREC_IrattariHelyekGetAll]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_IrattariHelyekSearch.OrderBy, _EREC_IrattariHelyekSearch.TopRow);

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariHelyekDelete");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();
            
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_IrattariHelyekDelete]");

            try
            {
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);
            
               SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    
    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IrattariHelyekInvalidate");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               SqlCommand SqlComm = new SqlCommand("[sp_EREC_IrattariHelyekInvalidate]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

               SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}