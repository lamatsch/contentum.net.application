using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_Kuldemeny_IratPeldanyaiStoredProcedure
/// </summary>
public partial class EREC_Kuldemeny_IratPeldanyaiStoredProcedure
{
    //private SqlConnection Connection;
    
    //private String ConnectionString;
    
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_EREC_Kuldemeny_IratPeldanyaiInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_EREC_Kuldemeny_IratPeldanyaiUpdateParameters = null;
    
    public EREC_Kuldemeny_IratPeldanyaiStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;
         
        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
            //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
            //throw e;
        //}
	}
    public Result Insert(String Method, ExecParam ExecParam, EREC_Kuldemeny_IratPeldanyai Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, EREC_Kuldemeny_IratPeldanyai Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method=="") Method = "Insert";
            switch (Method)
            {
               case Constants.Insert:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Kuldemeny_IratPeldanyaiInsert");
                  Record.Base.Updated.LetrehozasIdo = true;
                  Record.Base.Updated.Letrehozo_id = true;
                  Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                  Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_Kuldemeny_IratPeldanyaiInsert]");
                  if (sp_EREC_Kuldemeny_IratPeldanyaiInsertParameters == null)
                  {
                     //sp_EREC_Kuldemeny_IratPeldanyaiInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_Kuldemeny_IratPeldanyaiInsert]");
                     //sp_EREC_Kuldemeny_IratPeldanyaiInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_Kuldemeny_IratPeldanyaiInsert]");
                     sp_EREC_Kuldemeny_IratPeldanyaiInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_Kuldemeny_IratPeldanyaiInsert]");
                  }
                
                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_Kuldemeny_IratPeldanyaiInsertParameters);

                  Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                  break;
               case Constants.Update:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Kuldemeny_IratPeldanyaiUpdate");
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_Kuldemeny_IratPeldanyaiUpdate]");
                  if (sp_EREC_Kuldemeny_IratPeldanyaiUpdateParameters == null)
                  {
                     //sp_EREC_Kuldemeny_IratPeldanyaiUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_Kuldemeny_IratPeldanyaiUpdate]");
                     //sp_EREC_Kuldemeny_IratPeldanyaiUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_Kuldemeny_IratPeldanyaiUpdate]");
                     sp_EREC_Kuldemeny_IratPeldanyaiUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_Kuldemeny_IratPeldanyaiUpdate]");
                  }

                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_Kuldemeny_IratPeldanyaiUpdateParameters);

                  Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                  break;
         }
        
         SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         //SqlComm.Connection = Connection;
         //SqlComm.Connection = sqlConnection;
         SqlComm.Connection = dataContext.Connection;
         SqlComm.Transaction = dataContext.Transaction;

         try
         {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
       //}        
       
       log.SpEnd(ExecParam, _ret);
       
       return _ret;         
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_EREC_Kuldemeny_IratPeldanyaiGet");
    
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

         try
         {                        
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Kuldemeny_IratPeldanyaiGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
                       
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);            
            
             EREC_Kuldemeny_IratPeldanyai _EREC_Kuldemeny_IratPeldanyai = new EREC_Kuldemeny_IratPeldanyai();
             Utility.LoadBusinessDocumentFromDataAdapter(_EREC_Kuldemeny_IratPeldanyai, SqlComm);
             _ret.Record = _EREC_Kuldemeny_IratPeldanyai;

         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
                     
    }

    public Result GetAll(ExecParam ExecParam, EREC_Kuldemeny_IratPeldanyaiSearch _EREC_Kuldemeny_IratPeldanyaiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Kuldemeny_IratPeldanyaiGetAll");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_EREC_Kuldemeny_IratPeldanyaiSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _EREC_Kuldemeny_IratPeldanyaiSearch.WhereByManual;

               SqlCommand SqlComm = new SqlCommand("[sp_EREC_Kuldemeny_IratPeldanyaiGetAll]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_Kuldemeny_IratPeldanyaiSearch.OrderBy, _EREC_Kuldemeny_IratPeldanyaiSearch.TopRow);

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Kuldemeny_IratPeldanyaiDelete");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();
            
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_Kuldemeny_IratPeldanyaiDelete]");

            try
            {
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);
            
               SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    
    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_Kuldemeny_IratPeldanyaiInvalidate");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               SqlCommand SqlComm = new SqlCommand("[sp_EREC_Kuldemeny_IratPeldanyaiInvalidate]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

               SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}