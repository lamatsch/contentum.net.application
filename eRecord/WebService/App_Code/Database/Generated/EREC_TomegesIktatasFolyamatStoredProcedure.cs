using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_TomegesIktatasFolyamatStoredProcedure
/// </summary>
public partial class EREC_TomegesIktatasFolyamatStoredProcedure
{
    //private SqlConnection Connection;
    
    //private String ConnectionString;
    
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_EREC_TomegesIktatasFolyamatInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_EREC_TomegesIktatasFolyamatUpdateParameters = null;
    
    public EREC_TomegesIktatasFolyamatStoredProcedure(DataContext _dataContext)
	{
         this.dataContext = _dataContext;
         
        //ConnectionString = Database.GetConnectionString(App);        
        //try
        //{
            //Connection = Database.Connect(App);
        //}
        //catch (Exception e)
        //{
            //throw e;
        //}
	}
    public Result Insert(String Method, ExecParam ExecParam, EREC_TomegesIktatasFolyamat Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now);
    }

    public Result Insert(String Method, ExecParam ExecParam, EREC_TomegesIktatasFolyamat Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            System.Data.SqlClient.SqlCommand SqlComm = null;
            // ide kell a UIAccessLog / WebServiceAccessLogId elkerese betele a recordba.
            if (Method=="") Method = "Insert";
            switch (Method)
            {
               case Constants.Insert:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_TomegesIktatasFolyamatInsert");
                  Record.Base.Updated.LetrehozasIdo = true;
                  Record.Base.Updated.Letrehozo_id = true;
                  Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                  Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;       
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;                  
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_TomegesIktatasFolyamatInsert]");
                  if (sp_EREC_TomegesIktatasFolyamatInsertParameters == null)
                  {
                     //sp_EREC_TomegesIktatasFolyamatInsertParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_TomegesIktatasFolyamatInsert]");
                     //sp_EREC_TomegesIktatasFolyamatInsertParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_TomegesIktatasFolyamatInsert]");
                     sp_EREC_TomegesIktatasFolyamatInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_TomegesIktatasFolyamatInsert]");
                  }
                
                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_TomegesIktatasFolyamatInsertParameters);

                  Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                
                  break;
               case Constants.Update:
                  log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_TomegesIktatasFolyamatUpdate");
                  Record.Base.Updated.ModositasIdo = true;
                  Record.Base.Updated.Modosito_id = true;
                  Record.Base.ModositasIdo = ExecutionTime.ToString();
                  Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;
                  SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_TomegesIktatasFolyamatUpdate]");
                  if (sp_EREC_TomegesIktatasFolyamatUpdateParameters == null)
                  {
                     //sp_EREC_TomegesIktatasFolyamatUpdateParameters = Utility.GetStoredProcedureParameter(Connection, "[sp_EREC_TomegesIktatasFolyamatUpdate]");
                     //sp_EREC_TomegesIktatasFolyamatUpdateParameters = Utility.GetStoredProcedureParameter(sqlConnection, "[sp_EREC_TomegesIktatasFolyamatUpdate]");
                     sp_EREC_TomegesIktatasFolyamatUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_EREC_TomegesIktatasFolyamatUpdate]");
                  }

                  Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_EREC_TomegesIktatasFolyamatUpdateParameters);

                  Utility.AddDefaultParameterToUpdateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

                  break;
         }
        
         SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
         //SqlComm.Connection = Connection;
         //SqlComm.Connection = sqlConnection;
         SqlComm.Connection = dataContext.Connection;
         SqlComm.Transaction = dataContext.Transaction;

         try
         {
            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();             
            }
         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
       //}        
       
       log.SpEnd(ExecParam, _ret);
       
       return _ret;         
    }


    public Result Get(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam,"sp_EREC_TomegesIktatasFolyamatGet");
    
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();

         try
         {                        
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_TomegesIktatasFolyamatGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;            
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
                       
            Utility.AddDefaultParameterToGetStoredProcedure(SqlComm, ExecParam);            
            
             EREC_TomegesIktatasFolyamat _EREC_TomegesIktatasFolyamat = new EREC_TomegesIktatasFolyamat();
             Utility.LoadBusinessDocumentFromDataAdapter(_EREC_TomegesIktatasFolyamat, SqlComm);
             _ret.Record = _EREC_TomegesIktatasFolyamat;

         }
         catch (SqlException e)
         {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
         }
         
         //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
                     
    }

    public Result GetAll(ExecParam ExecParam, EREC_TomegesIktatasFolyamatSearch _EREC_TomegesIktatasFolyamatSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_TomegesIktatasFolyamatGetAll");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               Query query = new Query();
               query.BuildFromBusinessDocument(_EREC_TomegesIktatasFolyamatSearch);

               //az egyedi szuresi feltel hozzaadasa!!!
               query.Where += _EREC_TomegesIktatasFolyamatSearch.WhereByManual;

               SqlCommand SqlComm = new SqlCommand("[sp_EREC_TomegesIktatasFolyamatGetAll]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _EREC_TomegesIktatasFolyamatSearch.OrderBy, _EREC_TomegesIktatasFolyamatSearch.TopRow);

               DataSet ds = new DataSet();
               SqlDataAdapter adapter = new SqlDataAdapter();
               adapter.SelectCommand = SqlComm;
               adapter.Fill(ds);

               _ret.Ds = ds;

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
        
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result Delete(ExecParam ExecParam)
    {
        return Delete(ExecParam, DateTime.Now);
    }

    public Result Delete(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_TomegesIktatasFolyamatDelete");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();
            
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_TomegesIktatasFolyamatDelete]");

            try
            {
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToDeleteStoredProcedure(SqlComm, ExecParam, ExecutionTime);
            
               SqlComm.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    
    public Result Invalidate(ExecParam ExecParam)
    {
        return Invalidate(ExecParam, DateTime.Now);
    }

    public Result Invalidate(ExecParam ExecParam, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_TomegesIktatasFolyamatInvalidate");
        
        Result _ret = new Result();
        
        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
            //sqlConnection.Open();

            try
            {
               SqlCommand SqlComm = new SqlCommand("[sp_EREC_TomegesIktatasFolyamatInvalidate]");
               SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
               //SqlComm.Connection = Connection;
               //SqlComm.Connection = sqlConnection;
               SqlComm.Connection = dataContext.Connection;
               SqlComm.Transaction = dataContext.Transaction;

               Utility.AddDefaultParameterToInvalidateStoredProcedure(SqlComm, ExecParam, ExecutionTime);

               SqlComm.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
               _ret.ErrorCode = e.ErrorCode.ToString();
               _ret.ErrorMessage = e.Message;
            }
            
            //sqlConnection.Close();
        //}
        
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}