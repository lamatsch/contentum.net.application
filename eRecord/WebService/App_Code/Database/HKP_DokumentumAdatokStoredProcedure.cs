using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for HKP_DokumentumAdatokStoredProcedure
/// </summary>

public partial class HKP_DokumentumAdatokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, HKP_DokumentumAdatokSearch _HKP_DokumentumAdatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_HKP_DokumentumAdatokGetAllWithExtension");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_HKP_DokumentumAdatokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _HKP_DokumentumAdatokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_HKP_DokumentumAdatokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _HKP_DokumentumAdatokSearch.OrderBy, _HKP_DokumentumAdatokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllFromeBeadvanyok(ExecParam ExecParam, HKP_DokumentumAdatokSearch _HKP_DokumentumAdatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_HKP_DokumentumAdatokGetAllFromeBeadvanyok");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_HKP_DokumentumAdatokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _HKP_DokumentumAdatokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_HKP_DokumentumAdatokGetAllFromeBeadvanyok]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _HKP_DokumentumAdatokSearch.OrderBy, _HKP_DokumentumAdatokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}