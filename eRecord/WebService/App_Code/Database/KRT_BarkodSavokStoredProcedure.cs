using System;
using System.Data;
using System.IO;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

using log4net;
using log4net.Config;

/// <summary>
/// Summary description for KRT_BarkodSavokStoredProcedure
/// </summary>
public partial class KRT_BarkodSavokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_BarkodSavokSearch _KRT_BarkodSavokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_BarkodSavokGetAllWithExtension");
        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_BarkodSavokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _KRT_BarkodSavokSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_KRT_BarkodSavokSearch.WhereByManual))
            {
                query.Where += " and " + _KRT_BarkodSavokSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_BarkodSavokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_BarkodSavokSearch.OrderBy, _KRT_BarkodSavokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    
    public Result BarkodSav_Igenyles(ExecParam ExecParam, String SavType, int FoglalandoDarab)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_BarkodSav_Igenyles");


        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_BarkodSav_Igenyles]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        // TODO: egyel�re t�kmindegy milyen szervezetet adunk be (Most beadjuk a felhaszn�l� egyszem�lyes csoportj�t)
        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Szervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Szervezet_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavType", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@SavType"].Size = 1;
        SqlComm.Parameters["@SavType"].Value = SavType;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FoglalandoDarab", System.Data.SqlDbType.Int));
        SqlComm.Parameters["@FoglalandoDarab"].Value = FoglalandoDarab;
        
        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;
        
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result BarkodSav_Feltoltes(ExecParam ExecParam, String SavId, String SavKezdete, String SavVege, String ReszSavFeltoltes)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        Result _ret = new Result();
        System.Data.SqlClient.SqlCommand SqlComm = null;
        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_BarkodSav_Feltoltes");

        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_BarkodSav_Feltoltes]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@BarkodSav_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@BarkodSav_Id"].Value = new Guid(SavId);

        // BLG_1940
        //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FirstKod", System.Data.SqlDbType.BigInt));
        //SqlComm.Parameters["@FirstKod"].Value = SavKezdete;

        //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LastKod", System.Data.SqlDbType.BigInt));
        //SqlComm.Parameters["@LastKod"].Value = SavVege;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FirstKod", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@FirstKod"].Value = SavKezdete;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LastKod", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@LastKod"].Value = SavVege;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ReszSavFeltoltes", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@ReszSavFeltoltes"].Size = 1;
        SqlComm.Parameters["@ReszSavFeltoltes"].Value = ReszSavFeltoltes;
        
        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FeltoltoSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@FeltoltoSzervezet_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;
        
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result BarkodSav_Szetosztas(ExecParam ExecParam, String SavId, String SavKezdete, String SavVege, String Szervezet1Id, String Szervezet2Id)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        Result _ret = new Result();
        System.Data.SqlClient.SqlCommand SqlComm = null;
        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_BarkodSav_Szetosztas");

        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_BarkodSav_Szetosztas]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@BarkodSav_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@BarkodSav_Id"].Value = new Guid(SavId);

        // BLG_1940
        //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FirstKod", System.Data.SqlDbType.BigInt));
        //SqlComm.Parameters["@FirstKod"].Value = SavKezdete;

        //SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LastKod", System.Data.SqlDbType.BigInt));
        //SqlComm.Parameters["@LastKod"].Value = SavVege;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FirstKod", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@FirstKod"].Value = SavKezdete;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LastKod", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@LastKod"].Value = SavVege;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Szervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Szervezet_Id"].Value = new Guid(Szervezet1Id);

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UjSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@UjSzervezet_Id"].Value = new Guid(Szervezet2Id);


        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;
        
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }


    public Result BarkodSav_Listazas(ExecParam ExecParam, String SavId, String SzervezetId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_BarkodSav_PrintListazas");
        Result _ret = new Result();
        _ret.Ds = new DataSet();

        try
        {
            //Contentum.eUtility.Log log = new Contentum.eUtility.Log();
            //ILog log0 = LogManager.GetLogger(typeof(Logger));
            //Result _ret = new Result();
            //string rv = "";
            //System.Data.SqlClient.SqlCommand SqlComm = null;
            //log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_BarkodSav_PrintListazas");
            //log0.Debug("sp_BarkodSav_PrintListazas -----");

            SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[sp_BarkodSav_PrintListazas]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@BarkodSav_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@BarkodSav_Id"].Value = new Guid(SavId);
            //log0.Debug("     SavId: "+SavId);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PrinteloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@PrinteloSzervezet_Id"].Value = new Guid(SzervezetId);
            //log0.Debug("     SzervezetId: "+SzervezetId);

            /*        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.UniqueIdentifier));
                    SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;*/

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

            #region save to file
            //FileInfo f = new FileInfo("C:\\Contentum.Net\\eRecord\\WebSite\\KRT_BarkodSavok.txt");
            //StreamWriter w = f.CreateText();
            //for (int i1 = 0; i1 < _ret.Ds.Tables[0].Rows.Count; i1++)
            //{
            //    w.WriteLine((string)_ret.Ds.Tables[0].Rows[i1].ItemArray[0]);
            //    w.Flush();
            //}
            //string rv = "C:\\Contentum.Net\\eRecord\\WebSite\\KRT_BarkodSavok.txt";
            //w.Close();
            #endregion save to file
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        //catch (Exception e)
        //{
        //    log.Error("Hiba: "+e.Message);
        //}

        log.SpEnd(ExecParam, _ret);
        //rv = rv.Substring(0, rv.Length);
        //log0.Debug("A vonalk�dok: "+rv);
        return _ret;
        //return rv;

    }
}