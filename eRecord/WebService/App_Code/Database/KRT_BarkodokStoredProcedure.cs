using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for KRT_BarkodokStoredProcedure
/// </summary>
public partial class KRT_BarkodokStoredProcedure
{

    public Result BarkodSav_Igenyles(ExecParam ExecParam, String SavType, int FoglalandoDarab)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();


        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_BarkodSav_Igenyles");


        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_BarkodSav_Igenyles]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        // TODO: egyel�re t�kmindegy milyen szervezetet adunk be (Most beadjuk a felhaszn�l� egyszem�lyes csoportj�t)
        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Szervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Szervezet_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavType", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@SavType"].Size = 1;
        SqlComm.Parameters["@SavType"].Value = SavType;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FoglalandoDarab", System.Data.SqlDbType.Int));
        SqlComm.Parameters["@FoglalandoDarab"].Value = FoglalandoDarab;
        

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;


        
        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
                
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
      

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result GetAllWithExtension(ExecParam ExecParam, KRT_BarkodokSearch _KRT_BarkodokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_BarkodokGetAllWithExtension");

        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_BarkodokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_BarkodokSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_BarkodokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_BarkodokSearch.OrderBy, _KRT_BarkodokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}
