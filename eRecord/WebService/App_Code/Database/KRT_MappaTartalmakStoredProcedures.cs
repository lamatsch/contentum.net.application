﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data.SqlTypes;

public partial class KRT_MappaTartalmakStoredProcedure
{

    public Result InsertTomeges(ExecParam execParam, KRT_MappaTartalmak krt_mappaTartalmak, string Ugyirat_Ids, string IratPeldany_Ids, string Kuldemeny_Ids)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_KRT_MappaTartalmakInsertTomeges");

        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_MappaTartalmakInsertTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            if (!string.IsNullOrEmpty(Ugyirat_Ids))
            {
                SqlComm.Parameters.Add(new SqlParameter("@Ugyirat_Ids", SqlDbType.NVarChar));
                SqlComm.Parameters["@Ugyirat_Ids"].Value = Ugyirat_Ids;
            }

            if (!string.IsNullOrEmpty(IratPeldany_Ids))
            {
                SqlComm.Parameters.Add(new SqlParameter("@IratPeldany_Ids", SqlDbType.NVarChar));
                SqlComm.Parameters["@IratPeldany_Ids"].Value = IratPeldany_Ids;
            }

            if (!string.IsNullOrEmpty(Kuldemeny_Ids))
            {
                SqlComm.Parameters.Add(new SqlParameter("@Kuldemeny_Ids", SqlDbType.NVarChar));
                SqlComm.Parameters["@Kuldemeny_Ids"].Value = Kuldemeny_Ids;
            }


            SqlComm.Parameters.Add(new SqlParameter("@Mappa_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Mappa_Id"].Value = SqlGuid.Parse(krt_mappaTartalmak.Mappa_Id);

            SqlComm.Parameters.Add(new SqlParameter("@Obj_type", SqlDbType.NVarChar));
            SqlComm.Parameters["@Obj_type"].Size = 100;
            SqlComm.Parameters["@Obj_type"].Value = krt_mappaTartalmak.Obj_type;

            SqlComm.Parameters.Add(new SqlParameter("@Leiras", SqlDbType.NVarChar));
            SqlComm.Parameters["@Leiras"].Size = 400;
            SqlComm.Parameters["@Leiras"].Value = krt_mappaTartalmak.Leiras;

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            // Tranzakció Id a Page_Id az ExecParamból:
            if (!string.IsNullOrEmpty(execParam.Page_Id))
            {
                SqlComm.Parameters.Add(new SqlParameter("@tranzId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@tranzId"].Value = SqlGuid.Parse(execParam.Page_Id);
            }

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result InvalidateTomeges(ExecParam execParam, string Mappa_Id, string Ugyirat_Ids, string IratPeldany_Ids, string Kuldemeny_Ids)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_KRT_MappaTartalmakInvalidateTomeges");

        Result _ret = new Result();
        _ret.Ds = new DataSet();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_KRT_MappaTartalmakInvalidateTomeges]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            if (!string.IsNullOrEmpty(Ugyirat_Ids))
            {
                SqlComm.Parameters.Add(new SqlParameter("@Ugyirat_Ids", SqlDbType.NVarChar));
                SqlComm.Parameters["@Ugyirat_Ids"].Value = Ugyirat_Ids;
            }

            if (!string.IsNullOrEmpty(IratPeldany_Ids))
            {
                SqlComm.Parameters.Add(new SqlParameter("@IratPeldany_Ids", SqlDbType.NVarChar));
                SqlComm.Parameters["@IratPeldany_Ids"].Value = IratPeldany_Ids;
            }

            if (!string.IsNullOrEmpty(Kuldemeny_Ids))
            {
                SqlComm.Parameters.Add(new SqlParameter("@Kuldemeny_Ids", SqlDbType.NVarChar));
                SqlComm.Parameters["@Kuldemeny_Ids"].Value = Kuldemeny_Ids;
            }

            SqlComm.Parameters.Add(new SqlParameter("@Mappa_Id", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Mappa_Id"].Value = SqlGuid.Parse(Mappa_Id);

            SqlComm.Parameters.Add(new SqlParameter("@ExecutorUserId", SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@ExecutorUserId"].Value = execParam.Typed.Felhasznalo_Id;

            // Tranzakció Id a Page_Id az ExecParamból:
            if (!string.IsNullOrEmpty(execParam.Page_Id))
            {
                SqlComm.Parameters.Add(new SqlParameter("@tranzId", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@tranzId"].Value = SqlGuid.Parse(execParam.Page_Id);
            }

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }

    public Result GetAllWithExtension(ExecParam execParam, KRT_MappaTartalmakSearch search, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(execParam, "sp_KRT_MappaTartalmakGetAllWithExtension");

        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(search);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += search.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(search.WhereByManual))
            {
                query.Where += " and " + search.WhereByManual;
            }



            SqlCommand SqlComm = new SqlCommand("[sp_KRT_MappaTartalmakGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, execParam, search.OrderBy, search.TopRow);

            // Felhasználó szervezete a jogosultságszűréshez
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = execParam.Typed.FelhasznaloSzervezet_Id;

            SqlComm.Parameters.Add(new SqlParameter("@Jogosultak", SqlDbType.Char));
            SqlComm.Parameters["@Jogosultak"].Size = 1;
            SqlComm.Parameters["@Jogosultak"].Value = Jogosultak ? '1' : '0';


            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(execParam, _ret);
        return _ret;
    }
}