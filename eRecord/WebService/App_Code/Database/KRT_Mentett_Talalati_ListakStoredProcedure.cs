using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for KRT_Mentett_Talalati_ListakStoredProcedure
/// </summary>

public partial class KRT_Mentett_Talalati_ListakStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_Mentett_Talalati_ListakSearch _KRT_Mentett_Talalati_ListakSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Mentett_Talalati_ListakGetAllWithExtension");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_Mentett_Talalati_ListakSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_Mentett_Talalati_ListakSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Mentett_Talalati_ListakGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_Mentett_Talalati_ListakSearch.OrderBy, _KRT_Mentett_Talalati_ListakSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result GetAllByMentettTalalatiListak(ExecParam ExecParam, KRT_Mentett_Talalati_Listak _KRT_Mentett_Talalati_Listak
        , KRT_Mentett_Talalati_ListakSearch _KRT_Mentett_Talalati_ListakSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_Mentett_Talalati_ListakGetAllByIktatoKonyv");
        Result _ret = new Result();

        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_Mentett_Talalati_ListakSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            query.Where += _KRT_Mentett_Talalati_ListakSearch.WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_Mentett_Talalati_ListakGetAllByIktatoKonyv]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Id"].Value = _KRT_Mentett_Talalati_Listak.Typed.Id;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_Mentett_Talalati_ListakSearch.OrderBy, _KRT_Mentett_Talalati_ListakSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}