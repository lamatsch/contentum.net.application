using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for KRT_RagszamSavokStoredProcedure
/// </summary>
public partial class KRT_RagszamSavokStoredProcedure
{
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_RagszamSavokSearch _KRT_RagszamSavokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_RagszamSavokGetAllWithExtension");
        Result _ret = new Result();
        try
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(_KRT_RagszamSavokSearch);

            //az egyedi szuresi feltel hozzaadasa!!!
            if (String.IsNullOrEmpty(query.Where))
            {
                query.Where += _KRT_RagszamSavokSearch.WhereByManual;
            }
            else if (!String.IsNullOrEmpty(_KRT_RagszamSavokSearch.WhereByManual))
            {
                query.Where += " and " + _KRT_RagszamSavokSearch.WhereByManual;
            }

            SqlCommand SqlComm = new SqlCommand("[sp_KRT_RagszamSavokGetAllWithExtension]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _KRT_RagszamSavokSearch.OrderBy, _KRT_RagszamSavokSearch.TopRow);

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result RagszamSav_Igenyles(ExecParam ExecParam, Guid RagszamSav_Id, Guid? Postakonyv_Id, int FoglalandoDarab, string @FoglalandoAllapota)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_RagszamSav_Igenyles");


        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_RagszamSav_Igenyles]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RagszamSav_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@RagszamSav_Id"].Value = RagszamSav_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Postakonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Postakonyv_Id"].Value = Postakonyv_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LetrehozoSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@LetrehozoSzervezet_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Letrehozo_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FoglalandoDarab", System.Data.SqlDbType.Int));
        SqlComm.Parameters["@FoglalandoDarab"].Value = FoglalandoDarab;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FoglalandoAllapota", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@FoglalandoAllapota"].Value = FoglalandoAllapota;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.NVarChar,1024));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;

        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;
        SqlComm.CommandTimeout = 600;

        try
        {
            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }


    public Result RagszamSav_Igenyles_WithoutInsert(ExecParam ExecParam,Guid RagszamSav_Id, string Kod, float KodNum, Guid? Postakonyv_Id, int FoglalandoDarab, string @FoglalandoAllapota)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_RagszamSav_Igenyles_WithoutInsert");


        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_RagszamSav_Igenyles_WithoutInsert]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RagszamSav_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@RagszamSav_Id"].Value = RagszamSav_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Postakonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Postakonyv_Id"].Value = Postakonyv_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Kod", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@Kod"].Value = Kod;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KodNum", System.Data.SqlDbType.Float));
        SqlComm.Parameters["@KodNum"].Value = KodNum;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LetrehozoSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@LetrehozoSzervezet_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Letrehozo_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FoglalandoDarab", System.Data.SqlDbType.Int));
        SqlComm.Parameters["@FoglalandoDarab"].Value = FoglalandoDarab;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FoglalandoAllapota", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@FoglalandoAllapota"].Value = FoglalandoAllapota;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.NVarChar, 1024));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;

        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;
        SqlComm.CommandTimeout = 600;

        try
        {
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;            
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result RagszamSav_IgenylesManual(ExecParam ExecParam, string Ragszam, float RagszamNum, Guid? Postakonyv_Id, string SavTipus)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_RagszamSav_IgenylesManual");


        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_RagszamSav_IgenylesManual]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Ragszam", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@Ragszam"].Value = Ragszam;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RagszamNum", System.Data.SqlDbType.Float));
        SqlComm.Parameters["@RagszamNum"].Value = RagszamNum;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavType", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@SavType"].Value = SavTipus;
        
        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Postakonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Postakonyv_Id"].Value = Postakonyv_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Org_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Org_Id"].Value = ExecParam.Typed.Org_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Letrehozo_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.NVarChar, 1024));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;

        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;
        SqlComm.CommandTimeout = 600;

        try
        {
            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }  

    public Result RagszamSav_Listazas(ExecParam ExecParam, String SavId, String SzervezetId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_RagszamSav_PrintListazas");
        Result _ret = new Result();
        _ret.Ds = new DataSet();

        try
        {

            SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_RagszamSav_PrintListazas]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RagszamSav_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@RagszamSav_Id"].Value = new Guid(SavId);

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PrinteloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@PrinteloSzervezet_Id"].Value = new Guid(SzervezetId);

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(_ret.Ds);

            #region save to file

            #endregion save to file
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }

    public Result RagszamSav_Letrehozas(ExecParam ExecParam, Guid SavId, Guid PostakonyvId, string SavKezdete, string SavVege, float savKezdeteNum, float SavVegeNum, float IgenyeltDarabszam, string SavTipus)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        Result _ret = new Result();
        System.Data.SqlClient.SqlCommand SqlComm = null;
        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_RagszamSav_Letrehozas");

        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_RagszamSav_Letrehozas]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RagszamSav_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@RagszamSav_Id"].Value = SavId;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Postakonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Postakonyv_Id"].Value = PostakonyvId;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavKezd", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@SavKezd"].Value = SavKezdete;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavVege", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@SavVege"].Value = SavVege;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavKezdNum", System.Data.SqlDbType.Float));
        SqlComm.Parameters["@SavKezdNum"].Value = savKezdeteNum;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavVegeNum", System.Data.SqlDbType.Float));
        SqlComm.Parameters["@SavVegeNum"].Value = SavVegeNum;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IgenyeltDarabszam", System.Data.SqlDbType.Float));
        SqlComm.Parameters["@IgenyeltDarabszam"].Value = IgenyeltDarabszam;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LetrehozoSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@LetrehozoSzervezet_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Letrehozo_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavType", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@SavType"].Value = SavTipus;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;

        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result RagszamSav_Modositas(ExecParam ExecParam, Guid SavId, Guid PostakonyvId, string SavKezdete, string SavVege, float SavKezdeteNum, float SavVegeNum, float IgenyeltDarabszam, string SzervezetId, string SavAllapot, string SavTipus)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();
        Result _ret = new Result();
        System.Data.SqlClient.SqlCommand SqlComm = null;
        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_RagszamSav_Modositas");

        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_RagszamSav_Modositas]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RagszamSav_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@RagszamSav_Id"].Value = SavId;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Postakonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Postakonyv_Id"].Value = PostakonyvId;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavKezd", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@SavKezd"].Value = SavKezdete;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavVege", System.Data.SqlDbType.VarChar));
        SqlComm.Parameters["@SavVege"].Value = SavVege;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavKezdNum", System.Data.SqlDbType.Float));
        SqlComm.Parameters["@SavKezdNum"].Value = SavKezdeteNum;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavVegeNum", System.Data.SqlDbType.Float));
        SqlComm.Parameters["@SavVegeNum"].Value = SavVegeNum;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IgenyeltDarabszam", System.Data.SqlDbType.Float));
        SqlComm.Parameters["@IgenyeltDarabszam"].Value = IgenyeltDarabszam;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavAllapot", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@SavAllapot"].Value = SavAllapot;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavType", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@SavType"].Value = SavTipus;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LetrehozoSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@LetrehozoSzervezet_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Modosito_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Modosito_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;

        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;

        try
        {
            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result RagszamSav_Foglalas(ExecParam ExecParam, Guid RagszamSav_Id, Guid? Postakonyv_Id, int FoglalandoDarab, string @FoglalandoAllapota)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_RagszamSav_Foglalas");


        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_RagszamSav_Foglalas]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RagszamSav_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@RagszamSav_Id"].Value = RagszamSav_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Postakonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Postakonyv_Id"].Value = Postakonyv_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@LetrehozoSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@LetrehozoSzervezet_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Letrehozo_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FoglalandoDarab", System.Data.SqlDbType.Int));
        SqlComm.Parameters["@FoglalandoDarab"].Value = FoglalandoDarab;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FoglalandoAllapota", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@FoglalandoAllapota"].Value = FoglalandoAllapota;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.NVarChar, 1024));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;

        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;
        SqlComm.CommandTimeout = 600;

        try
        {
            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result RagszamSav_FoglalasManual(ExecParam ExecParam, string Ragszam, float RagszamNum, Guid? Postakonyv_Id, string SavTipus)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();

        System.Data.SqlClient.SqlCommand SqlComm = null;

        log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KRT_RagszamSav_FoglalasManual");


        SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KRT_RagszamSav_FoglalasManual]");
        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Ragszam", System.Data.SqlDbType.NVarChar));
        SqlComm.Parameters["@Ragszam"].Value = Ragszam;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RagszamNum", System.Data.SqlDbType.Float));
        SqlComm.Parameters["@RagszamNum"].Value = RagszamNum;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SavType", System.Data.SqlDbType.Char));
        SqlComm.Parameters["@SavType"].Value = SavTipus;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Postakonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Postakonyv_Id"].Value = Postakonyv_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Org_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Org_Id"].Value = ExecParam.Typed.Org_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_Id", System.Data.SqlDbType.UniqueIdentifier));
        SqlComm.Parameters["@Letrehozo_Id"].Value = ExecParam.Typed.Felhasznalo_Id;

        SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ResultUid", System.Data.SqlDbType.NVarChar, 1024));
        SqlComm.Parameters["@ResultUid"].Direction = ParameterDirection.Output;

        SqlComm.Connection = dataContext.Connection;
        SqlComm.Transaction = dataContext.Transaction;
        SqlComm.CommandTimeout = 600;

        try
        {
            SqlComm.ExecuteNonQuery();
            _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

}