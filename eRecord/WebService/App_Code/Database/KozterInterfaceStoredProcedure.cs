﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using System.Data.SqlClient;
using Contentum.eQuery;
using System.Data;

/// <summary>
/// Summary description for KozterInterfaceStoredProcedure
/// </summary>
public class KozterInterfaceStoredProcedure
{
    private DataContext dataContext;

    internal static Dictionary<String, Utility.Parameter> sp_KozterInterfaceInsertParameters = null;
    internal static Dictionary<String, Utility.Parameter> sp_KozterInterfaceUpdateParameters = null;

    public KozterInterfaceStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }

    public Result Update(ExecParam ExecParam, KozterInterface Record, int updateId)
    {
        return Insert(Constants.Update, ExecParam, Record, DateTime.Now, updateId);
    }

    public Result Insert(ExecParam ExecParam, KozterInterface Record)
    {
        return Insert(Constants.Insert, ExecParam, Record, DateTime.Now, -1);
    }

    public Result Insert(String Method, ExecParam ExecParam, KozterInterface Record)
    {
        return Insert(Method, ExecParam, Record, DateTime.Now, -1);
    }

    public Result Insert(String Method, ExecParam ExecParam, KozterInterface Record, DateTime ExecutionTime, int updateId)
    {
        Contentum.eUtility.Log log = new Contentum.eUtility.Log();

        Result _ret = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            System.Data.SqlClient.SqlCommand SqlComm = null;

            if (Method == "") Method = Constants.Insert;
            switch (Method)
            {
                case Constants.Insert:
                    log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KozterInterfaceInsert");
                    Record.Base.Updated.LetrehozasIdo = true;
                    Record.Base.Updated.Letrehozo_id = true;
                    Record.Base.LetrehozasIdo = ExecutionTime.ToString();
                    Record.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;

                    SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KozterInterfaceInsert]");
                    SqlComm.Connection = dataContext.Connection;
                    SqlComm.Transaction = dataContext.Transaction;

                    //Logger.Info(String.Format("dataContext.Connection: {0}", (dataContext.Connection == null) ? "null" : "nem null"));

                    if (sp_KozterInterfaceInsertParameters == null)
                        sp_KozterInterfaceInsertParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KozterInterfaceInsert]");

                    Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KozterInterfaceInsertParameters);

                    Utility.AddDefaultParameterToInsertStoredProcedure(SqlComm);
                    SqlComm.Parameters["@ResultUid"].DbType = DbType.Int32;

                    break;
                case Constants.Update:
                    log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KozterInterfaceUpdate");
                    Record.Base.Updated.ModositasIdo = true;
                    Record.Base.Updated.Modosito_id = true;
                    Record.Base.ModositasIdo = ExecutionTime.ToString();
                    Record.Base.Modosito_id = ExecParam.Felhasznalo_Id;

                    SqlComm = new System.Data.SqlClient.SqlCommand("[sp_KozterInterfaceUpdate]");
                    SqlComm.Connection = dataContext.Connection;
                    SqlComm.Transaction = dataContext.Transaction;

                    if (sp_KozterInterfaceUpdateParameters == null)
                        sp_KozterInterfaceUpdateParameters = Utility.GetStoredProcedureParameter(dataContext.Connection, "[sp_KozterInterfaceUpdate]");

                    Utility.LoadBusinessDocumentToParameter(Record, SqlComm, sp_KozterInterfaceUpdateParameters);

                    SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.Int));
                    SqlComm.Parameters["@id"].Value = updateId;        

                    break;
            }

            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.ExecuteNonQuery();
            if (Method == Constants.Insert)
            {
                _ret.Uid = SqlComm.Parameters["@ResultUid"].Value.ToString();
            }
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result Get(ExecParam ExecParam, int id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KozterInterfaceGet");

        Result _ret = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            SqlCommand SqlComm = new SqlCommand("[sp_KozterInterfaceGet]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.Int));
            SqlComm.Parameters["@id"].Value = id;

            _ret.Record = GetKozterInterfaceBusinessDocumentFromDataAdapter(SqlComm);
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    private KozterInterface GetKozterInterfaceBusinessDocumentFromDataAdapter(SqlCommand SqlComm)
    {
        KozterInterface _KozterInterface = new KozterInterface();

        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = SqlComm;
        da.Fill(ds);

        if (ds.Tables[0].Rows.Count > 0)
        {
            System.Reflection.PropertyInfo[] Properties = _KozterInterface.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo P in Properties)
            {
                if (!(ds.Tables[0].Rows[0].IsNull(P.Name)))
                {
                    P.SetValue(_KozterInterface, ds.Tables[0].Rows[0][P.Name].ToString(), null);
                }
            }
        }

        return _KozterInterface;
    }

    public Result GetAll(ExecParam ExecParam, String WhereByManual)
    {
        return GetAll(ExecParam, WhereByManual, String.Empty, 0);
    }

    public Result GetAll(ExecParam ExecParam, String WhereByManual, String OrderBy, int TopRow)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_KozterInterfaceGetAll");

        Result _ret = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            Query query = new Query();

            query.Where = WhereByManual;

            SqlCommand SqlComm = new SqlCommand("[sp_KozterInterfaceGetAll]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Where", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Where"].Value = WhereByManual;

            if (!String.IsNullOrEmpty(OrderBy) && OrderBy.Trim() != "")
            {
                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@OrderBy", System.Data.SqlDbType.NVarChar));
                SqlComm.Parameters["@OrderBy"].Size = 400;
                if (OrderBy == Contentum.eUtility.Constants.BusinessDocument.nullString)
                    SqlComm.Parameters["@OrderBy"].Value = String.Empty;
                else
                    SqlComm.Parameters["@OrderBy"].Value = "order by " + OrderBy;
            }

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TopRow", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@TopRow"].Size = 5;
            SqlComm.Parameters["@TopRow"].Value = TopRow.ToString();

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

}
