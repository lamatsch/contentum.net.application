using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
/// Summary description for PirEdokInterfaceStoredProcedure
/// </summary>

public partial class PirEdokInterfaceStoredProcedure
{
    // TODO: connection
    private DataContext dataContext;

    public PirEdokInterfaceStoredProcedure(DataContext _dataContext)
	{
        this.dataContext = _dataContext;
	}

    /*
    f_vonalkod	char(13) 	Vonalk�d			Kit�lt�se k�telez�
    f_kbsz		char(16) 	Sz�mlasz�m			Kit�lt�se k�telez�
    f_erkdat		smalldatetime �rkeztet�s d�tuma		Kit�lt�se k�telez�
    f_fldhely		char(36)	Szerv. egys�g EDOK k�d	Kit�lt�se k�telez�
    f_fldhelynev	char(100)	Szerv. egys�g EDOK neve	Kit�lt�se k�telez�
    f_fizmod		char(4)		Fizet�si m�d			Kit�lt�se k�telez� (�K)
    f_tdat		smalldatetime Teljes�t�s d�tuma		Kit�lt�se opcion�lis
    f_bizdat		smalldatetime	Bizonylat d�tuma		Kit�lt�se opcion�lis
    f_fhi		smalldatetime	Fizet�si hat�rid�		Kit�lt�se opcion�lis
    f_dkod		char(3)		Deviza k�d			Kit. opc., alap�rt.: �HUF�
    f_vidat		smalldatetime	Visszak�ld�s d�tuma		Kit�lt�se opcion�lis
    f_csumma		float		Ellen�rz� �sszeg		Kit�lt�se k�telez�
    f_szrpartn		char(36)	EDOK partnerk�d		Kit�lt�se k�telez�
    f_nev1		varchar(400)	EDOK partner neve		Kit�lt�se k�telez�
    f_irsz		char(10)	EDOK part.ir�ny�t�sz�ma	Kit�lt�se k�telez�
    f_helyseg		char(20)	EDOK part. helys�ge		Kit�lt�se k�telez�
    f_hcim		char(100)	EDOK partner c�me		Kit�lt�se opcion�lis
    f_aiasz		char(15)	Belf�ldi ad�sz�m		Kit�lt�se opcion�lis
    f_kaiasz		char(15)	K�z�ss�gi (EU) ad�sz�m	Kit�lt�se opcion�lis
    f_adoazon		char(10)	Mag�nszem�ly ad�azon. jele	Kit�lt�se opcion�lis
    f_telex		char(15)	Helyrajzi sz�m		Kit�lt�se opcion�lis
    f_pbsz1		char(8)		Banksz�mlasz�m1		Kit�lt�se opcion�lis
    f_pbsz2		char(8)		Banksz�mlasz�m2		Kit�lt�se opcion�lis
    f_pbsz3		char(8)		Banksz�mlasz�m3		Kit�lt�se opcion�lis
    f_tel1		char(15)	Telefonsz�m1			Kit�lt�se opcion�lis
    f_fax		char(15)	Fax				Kit�lt�se opcion�lis
    f_email		varchar(60)	E-mail c�m			Kit�lt�se opcion�lis
    f_fstatus		char(1)	PIR-beli �llapot			Kit�lt�se opcion�lis
    f_mgj		varchar(40)	Megjegyz�s			Kit�lt�se opcion�lis
    */
    private void AddSqlParametersFromBusinessObject(SqlCommand SqlComm, PirEdokSzamla pirEdokSzamla)
    {
        if (SqlComm != null && pirEdokSzamla != null)
        {
            SqlComm.Parameters.Add("@f_vonalkod", SqlDbType.Char, 13);
            SqlComm.Parameters["@f_vonalkod"].Value = pirEdokSzamla.Typed.Vonalkod;

            SqlComm.Parameters.Add("@f_kbsz", SqlDbType.Char, 16);
            SqlComm.Parameters["@f_kbsz"].Value = pirEdokSzamla.Typed.Szamlaszam;

            SqlComm.Parameters.Add("@f_erkdat", SqlDbType.SmallDateTime);
            SqlComm.Parameters["@f_erkdat"].Value = pirEdokSzamla.Typed.ErkeztetesDatuma;

            SqlComm.Parameters.Add("@f_fldhely", SqlDbType.Char, 36);
            SqlComm.Parameters["@f_fldhely"].Value = pirEdokSzamla.Typed.SzervezetiEgysegKod;

            SqlComm.Parameters.Add("@f_fldhelynev", SqlDbType.Char, 100);
            SqlComm.Parameters["@f_fldhelynev"].Value = pirEdokSzamla.Typed.SzervezetiEgysegNev;

            SqlComm.Parameters.Add("@f_fizmod", SqlDbType.Char, 4);
            SqlComm.Parameters["@f_fizmod"].Value = pirEdokSzamla.Typed.FizetesiMod;

            SqlComm.Parameters.Add("@f_tdat", SqlDbType.SmallDateTime);
            SqlComm.Parameters["@f_tdat"].Value = pirEdokSzamla.Typed.TeljesitesDatuma;

            SqlComm.Parameters.Add("@f_bizdat", SqlDbType.SmallDateTime);
            SqlComm.Parameters["@f_bizdat"].Value = pirEdokSzamla.Typed.BizonylatDatuma;

            SqlComm.Parameters.Add("@f_fhi", SqlDbType.SmallDateTime);
            SqlComm.Parameters["@f_fhi"].Value = pirEdokSzamla.Typed.FizetesiHatarido;

            SqlComm.Parameters.Add("@f_dkod", SqlDbType.Char, 3);
            SqlComm.Parameters["@f_dkod"].Value = pirEdokSzamla.Typed.DevizaKod;

            SqlComm.Parameters.Add("@f_vidat", SqlDbType.SmallDateTime);
            SqlComm.Parameters["@f_vidat"].Value = pirEdokSzamla.Typed.VisszakuldesDatuma;

            SqlComm.Parameters.Add("@f_csumma", SqlDbType.Float);
            SqlComm.Parameters["@f_csumma"].Value = pirEdokSzamla.Typed.EllenorzoOsszeg;

            SqlComm.Parameters.Add("@f_szrpartn", SqlDbType.Char, 36);
            SqlComm.Parameters["@f_szrpartn"].Value = pirEdokSzamla.Typed.EDOKPartnerKod;

            SqlComm.Parameters.Add("@f_nev1", SqlDbType.VarChar, 400);
            SqlComm.Parameters["@f_nev1"].Value = pirEdokSzamla.Typed.EDOKPartnerNev;

            SqlComm.Parameters.Add("@f_irsz", SqlDbType.Char, 10);
            SqlComm.Parameters["@f_irsz"].Value = pirEdokSzamla.Typed.EDOKPartnerIranyitoszam;

            SqlComm.Parameters.Add("@f_helyseg", SqlDbType.Char, 20);
            SqlComm.Parameters["@f_helyseg"].Value = pirEdokSzamla.Typed.EDOKPartnerHelyseg;

            SqlComm.Parameters.Add("@f_hcim", SqlDbType.Char, 100);
            SqlComm.Parameters["@f_hcim"].Value = pirEdokSzamla.Typed.EDOKPartnerCim;

            SqlComm.Parameters.Add("@f_aiasz", SqlDbType.Char, 15);
            SqlComm.Parameters["@f_aiasz"].Value = pirEdokSzamla.Typed.BelfoldiAdoszam;

            SqlComm.Parameters.Add("@f_kaiasz", SqlDbType.Char, 15);
            SqlComm.Parameters["@f_kaiasz"].Value = pirEdokSzamla.Typed.KozossegiAdoszam;

            SqlComm.Parameters.Add("@f_adoazon", SqlDbType.Char, 10);
            SqlComm.Parameters["@f_adoazon"].Value = pirEdokSzamla.Typed.MaganszemelyAdoazonositoJel;

            SqlComm.Parameters.Add("@f_telex", SqlDbType.Char, 15);
            SqlComm.Parameters["@f_telex"].Value = pirEdokSzamla.Typed.HelyrajziSzam;

            SqlComm.Parameters.Add("@f_pbsz1", SqlDbType.Char, 8);
            SqlComm.Parameters["@f_pbsz1"].Value = pirEdokSzamla.Typed.Bankszamlaszam1;

            SqlComm.Parameters.Add("@f_pbsz2", SqlDbType.Char, 8);
            SqlComm.Parameters["@f_pbsz2"].Value = pirEdokSzamla.Typed.Bankszamlaszam2;

            SqlComm.Parameters.Add("@f_pbsz3", SqlDbType.Char, 8);
            SqlComm.Parameters["@f_pbsz3"].Value = pirEdokSzamla.Typed.Bankszamlaszam3;

            SqlComm.Parameters.Add("@f_tel1", SqlDbType.Char, 15);
            SqlComm.Parameters["@f_tel1"].Value = pirEdokSzamla.Typed.Telefonszam;

            SqlComm.Parameters.Add("@f_fax", SqlDbType.Char, 15);
            SqlComm.Parameters["@f_fax"].Value = pirEdokSzamla.Typed.Fax;

            SqlComm.Parameters.Add("@f_email", SqlDbType.VarChar, 60);
            SqlComm.Parameters["@f_email"].Value = pirEdokSzamla.Typed.EmailCim;

            SqlComm.Parameters.Add("@f_fstatus", SqlDbType.Char, 1);
            SqlComm.Parameters["@f_fstatus"].Value = pirEdokSzamla.Typed.PIRAllapot;

            SqlComm.Parameters.Add("@f_mgj", SqlDbType.VarChar, 40);
            SqlComm.Parameters["@f_mgj"].Value = pirEdokSzamla.Typed.Megjegyzes;
        }
    }

    public Result PirEdokSzamlaFejFelvitel(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "PirEdokSzamlaFejFelvitel");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                SqlCommand SqlComm = new SqlCommand("[PirEdokSzamlaFejFelvitel]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                AddSqlParametersFromBusinessObject(SqlComm, pirEdokSzamla);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

    public Result PirEdokSzamlaFejModositas(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "PirEdokSzamlaFejModositas");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                SqlCommand SqlComm = new SqlCommand("[PirEdokSzamlaFejModositas]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                AddSqlParametersFromBusinessObject(SqlComm, pirEdokSzamla);

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result PirEdokSzamlaFejRontas(ExecParam ExecParam, string vonalkod, string rontasOka)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "PirEdokSzamlaFejRontas");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[PirEdokSzamlaFejRontas]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            /*
            f_vonalkod	char(13) 	Vonalk�d			Kit�lt�se k�telez�
            f_rontok		varchar(60) 	Ront�s oka			Kit�lt�se k�telez�
            */

            SqlComm.Parameters.Add("@f_vonalkod", SqlDbType.Char, 13);
            SqlComm.Parameters["@f_vonalkod"].Value = vonalkod;

            SqlComm.Parameters.Add("@f_rontok", SqlDbType.VarChar, 60);
            SqlComm.Parameters["@f_rontok"].Value = vonalkod;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }

    public Result PirEdokSzamlaFejAllapotLekerdez(ExecParam ExecParam, string vonalkod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "PirEdokSzamlaFejAllapotLekerdez");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[PirEdokSzamlaFejAllapotLekerdez]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            /*
            f_vonalkod	char(13) 	Vonalk�d			Kit�lt�se k�telez�
            */

            SqlComm.Parameters.Add("@f_vonalkod", SqlDbType.Char, 13);
            SqlComm.Parameters["@f_vonalkod"].Value = vonalkod;

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }


    public Result PirEdokSzamlaFejAllapotValtozas(ExecParam ExecParam, DateTime datum)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "PirEdokSzamlaFejAllapotValtozas");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
        try
        {
            SqlCommand SqlComm = new SqlCommand("[PirEdokSzamlaFejAllapotValtozas]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            //SqlComm.Connection = Connection;
            //SqlComm.Connection = sqlConnection;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;

            /*
            f_datum	smalldatetime	D�tum			Kit�lt�se nem k�telez�
            */

            if (datum != DateTime.MinValue)
            {
                SqlComm.Parameters.Add("@f_datum", SqlDbType.SmallDateTime);
                SqlComm.Parameters["@f_datum"].Value = datum.ToString();
            }

            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;

        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam, _ret);
        return _ret;

    }
}