using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for SakkoraStoredProcedure
/// </summary>

public partial class SakkoraStoredProcedure
{
    private DataContext dataContext;

    public SakkoraStoredProcedure(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }
    /// <summary>
    /// sakk�ra hat�rid�k
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="UgyiratId"></param>
    /// <param name="hataridoMarNovelve"></param>
    /// <returns></returns>
    public Result GetSakkoraHataridok(ExecParam ExecParam, string UgyiratId, bool hataridoMarNovelve)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratok_Sakkora_GetHataridok");
        Result _ret = new Result();
        _ret.Ds = new DataSet();
        if (string.IsNullOrEmpty(UgyiratId))
            return _ret;
        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratok_Sakkora_GetHataridok]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
            SqlComm.Connection = dataContext.Connection;
            SqlComm.Transaction = dataContext.Transaction;
            SqlComm.CommandTimeout = 300;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Ugyirat_Id", SqlDbType.UniqueIdentifier));
            // BUG_2742
            //SqlComm.Parameters["@Ugyirat_Id"].Value = UgyiratId;
            SqlComm.Parameters["@Ugyirat_Id"].Value = string.IsNullOrEmpty(UgyiratId) ? SqlGuid.Null : SqlGuid.Parse(UgyiratId);

            // @Hatarido_Mar_Novelve:
            SqlParameter paramHatIdoMarNovelve = new SqlParameter("@Hatarido_Mar_Novelve", SqlDbType.Bit) { Value = hataridoMarNovelve };
            SqlComm.Parameters.Add(paramHatIdoMarNovelve);

            SqlParameter outputLetrehozasIdoParam = SqlComm.Parameters.Add("@LetrehozasIdo", SqlDbType.DateTime);
            outputLetrehozasIdoParam.Direction = ParameterDirection.Output;

            SqlParameter outputHataridooParam = SqlComm.Parameters.Add("@Hatarido", SqlDbType.DateTime);
            outputHataridooParam.Direction = ParameterDirection.Output;
            
            // BUG_2742
            //SqlParameter outputEltelt_NapParam = SqlComm.Parameters.Add("@Eltelt_Nap", SqlDbType.DateTime);
            SqlParameter outputEltelt_NapParam = SqlComm.Parameters.Add("@Eltelt_Nap", SqlDbType.Int);
            outputEltelt_NapParam.Direction = ParameterDirection.Output;

            // BUG_2742
            //SqlParameter outputEltelt_MunkaNapParam = SqlComm.Parameters.Add("@Eltelt_MunkaNap", SqlDbType.DateTime);
            SqlParameter outputEltelt_MunkaNapParam = SqlComm.Parameters.Add("@Eltelt_MunkaNap", SqlDbType.Int);
            outputEltelt_MunkaNapParam.Direction = ParameterDirection.Output;

            // BUG_2742
            //SqlParameter outputFennmarado_NapParam = SqlComm.Parameters.Add("@Fennmarado_Nap", SqlDbType.DateTime);
            SqlParameter outputFennmarado_NapParam = SqlComm.Parameters.Add("@Fennmarado_Nap", SqlDbType.Int);
            outputFennmarado_NapParam.Direction = ParameterDirection.Output;

            // BUG_2742
            //SqlParameter outputHatarido_TullepveParam = SqlComm.Parameters.Add("@Hatarido_Tullepve", SqlDbType.DateTime);
            SqlParameter outputHatarido_TullepveParam = SqlComm.Parameters.Add("@Hatarido_Tullepve", SqlDbType.Int);
            outputHatarido_TullepveParam.Direction = ParameterDirection.Output;
            
            SqlComm.ExecuteNonQuery();

            DateTime letrehozasIdo;
            DateTime.TryParse(SqlComm.Parameters["@LetrehozasIdo"].Value.ToString(), out letrehozasIdo);

            DateTime hatarido;
            DateTime.TryParse(SqlComm.Parameters["@Hatarido"].Value.ToString(), out hatarido);

            int eltelt_Nap;
            int.TryParse(SqlComm.Parameters["@Eltelt_Nap"].Value.ToString(), out eltelt_Nap);

            int eltelt_MunkaNap;
            int.TryParse(SqlComm.Parameters["@Eltelt_MunkaNap"].Value.ToString(), out eltelt_MunkaNap);

            int fennmarado_Nap;
            int.TryParse(SqlComm.Parameters["@Fennmarado_Nap"].Value.ToString(), out fennmarado_Nap);

            bool hatarido_Tullepve;
            if (SqlComm.Parameters["@Hatarido_Tullepve"].Value.ToString().Equals("1") || SqlComm.Parameters["@Hatarido_Tullepve"].Value.ToString().Equals("true"))
                hatarido_Tullepve = true;
            else
                hatarido_Tullepve = false;

            DataTable dt = new DataTable();

            #region COLUMNS
            DataColumn column1 = new DataColumn();
            column1.DataType = System.Type.GetType("System.DateTime");
            column1.ColumnName = "LetrehozasIdo";
            dt.Columns.Add(column1);

            DataColumn column2 = new DataColumn();
            column2.DataType = System.Type.GetType("System.DateTime");
            column2.ColumnName = "Hatarido";
            dt.Columns.Add(column2);

            DataColumn column3 = new DataColumn();
            column3.DataType = System.Type.GetType("System.Int32");
            column3.ColumnName = "Eltelt_Nap";
            dt.Columns.Add(column3);

            DataColumn column3b = new DataColumn();
            column3b.DataType = System.Type.GetType("System.Int32");
            column3b.ColumnName = "Eltelt_MunkaNap";
            dt.Columns.Add(column3b);

            DataColumn column4 = new DataColumn();
            column4.DataType = System.Type.GetType("System.Int32");
            column4.ColumnName = "Fennmarado_Nap";
            dt.Columns.Add(column4);

            DataColumn column5 = new DataColumn();
            column5.DataType = System.Type.GetType("System.Boolean");
            column5.ColumnName = "Hatarido_Tullepve";
            dt.Columns.Add(column5);
            #endregion

            DataRow dr = dt.NewRow();
            dr["LetrehozasIdo"] = letrehozasIdo;
            dr["Hatarido"] = hatarido;
            dr["Eltelt_Nap"] = eltelt_Nap;
            dr["Eltelt_MunkaNap"] = eltelt_MunkaNap;
            dr["Fennmarado_Nap"] = fennmarado_Nap;
            dr["Hatarido_Tullepve"] = hatarido_Tullepve;

            dt.Rows.Add(dr);
            // BUG_2742
            _ret.Ds.Tables.Add(dt);
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }


    /// <summary>
    /// FelfuggesztettAllapotEsetenHataridoNoveles
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    public Result SakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ExecParam ExecParam, string ugyiratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratok_FelfuggesztettHataridoNoveles");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratok_FelfuggesztettHataridoNoveles]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@UgyiratId"].Value = string.IsNullOrEmpty(ugyiratId) ? SqlGuid.Null : SqlGuid.Parse(ugyiratId);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
    /// <summary>
    /// ElteltIdoNoveles
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    public Result SakkoraElteltIdoNoveles(ExecParam ExecParam, string ugyiratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratok_ElteltIdoNoveles");

        Result _ret = new Result();

        try
        {
            SqlCommand SqlComm = new SqlCommand("[sp_EREC_UgyUgyiratok_ElteltIdoNoveles]");
            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyiratId", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@UgyiratId"].Value = string.IsNullOrEmpty(ugyiratId) ? SqlGuid.Null : SqlGuid.Parse(ugyiratId);

            SqlComm.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }

        log.SpEnd(ExecParam, _ret);
        return _ret;
    }
}