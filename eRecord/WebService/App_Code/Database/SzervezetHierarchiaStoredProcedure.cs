using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
//using System.EnterpriseServices;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
/// Summary description for SzervezetHierarchiaStoredProcedure
/// </summary>

public partial class SzervezetHierarchiaStoredProcedure
{
    private DataContext dataContext;

    public SzervezetHierarchiaStoredProcedure(DataContext _dataContext)
	{
        this.dataContext = _dataContext;
	}

    public Result GetAll(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_Szervezet_Hierarchia");

        Result _ret = new Result();

        //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
        //{
        //    sqlConnection.Open();
            try
            {
                Query query = new Query();

                SqlCommand SqlComm = new SqlCommand("[sp_Szervezet_Hierarchia]");
                SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
                //SqlComm.Connection = Connection;
                //SqlComm.Connection = sqlConnection;
                SqlComm.Connection = dataContext.Connection;
                SqlComm.Transaction = dataContext.Transaction;

                Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, "", 0);

                SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloSzervezet_Id", System.Data.SqlDbType.UniqueIdentifier));
                SqlComm.Parameters["@FelhasznaloSzervezet_Id"].Value = ExecParam.Typed.FelhasznaloSzervezet_Id;

                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = SqlComm;
                adapter.Fill(ds);

                _ret.Ds = ds;

            }
            catch (SqlException e)
            {
                _ret.ErrorCode = e.ErrorCode.ToString();
                _ret.ErrorMessage = e.Message;
            }

        //    sqlConnection.Close();
        //}

        log.SpEnd(ExecParam,_ret);
        return _ret;

    }

    //public Result GetAll(ExecParam ExecParam, SzervezetHierarchiaSearch _SzervezetHierarchiaSearch)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_Szervezet_Hierarchia");

    //    Result _ret = new Result();

    //    //using (SqlConnection sqlConnection = Database.GetSqlConnection(ConnectionString))
    //    //{
    //    //    sqlConnection.Open();
    //    try
    //    {
    //        Query query = new Query();
    //        query.BuildFromBusinessDocument(_SzervezetHierarchiaSearch);

    //        //az egyedi szuresi feltel hozzaadasa!!!
    //        if (String.IsNullOrEmpty(query.Where))
    //        {
    //            query.Where += _SzervezetHierarchiaSearch.WhereByManual;
    //        }
    //        else if (!String.IsNullOrEmpty(_SzervezetHierarchiaSearch.WhereByManual))
    //        {
    //            query.Where += " and " + _SzervezetHierarchiaSearch.WhereByManual;
    //        }

    //        SqlCommand SqlComm = new SqlCommand("[sp_Szervezet_Hierarchia]");
    //        SqlComm.CommandType = System.Data.CommandType.StoredProcedure;
    //        //SqlComm.Connection = Connection;
    //        //SqlComm.Connection = sqlConnection;
    //        SqlComm.Connection = dataContext.Connection;
    //        SqlComm.Transaction = dataContext.Transaction;

    //        Utility.AddDefaultParameterToGetAllStoredProcedure(SqlComm, query, ExecParam, _SzervezetHierarchiaSearch.OrderBy, _SzervezetHierarchiaSearch.TopRow);

    //        DataSet ds = new DataSet();
    //        SqlDataAdapter adapter = new SqlDataAdapter();
    //        adapter.SelectCommand = SqlComm;
    //        adapter.Fill(ds);

    //        _ret.Ds = ds;

    //    }
    //    catch (SqlException e)
    //    {
    //        _ret.ErrorCode = e.ErrorCode.ToString();
    //        _ret.ErrorMessage = e.Message;
    //    }

    //    //    sqlConnection.Close();
    //    //}

    //    log.SpEnd(ExecParam, _ret);
    //    return _ret;
    //}

}