﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TomegesIktatasDataBaseManager
/// </summary>
public class TomegesIktatasDataBaseManager
{
    private DataContext dataContext;

    public TomegesIktatasDataBaseManager(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }

    public Result KuldKuldemenyekTomegesInit(ExecParam ExecParam, EREC_KuldKuldemenyek Record, List<string> Erkezteto_SzamList, List<string> AzonositoList)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_KuldKuldemenyekTomegesInit");

        Result _ret = new Result();

        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_KuldKuldemenyekTomegesInit]");
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Allapot", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Allapot"].Value = Record.Typed.Allapot;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@BeerkezesIdeje", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@BeerkezesIdeje"].Value = Record.Typed.BeerkezesIdeje;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IraIktatokonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IraIktatokonyv_Id"].Value = Record.Typed.IraIktatokonyv_Id;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KuldesMod", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@KuldesMod"].Value = Record.Typed.KuldesMod;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Targy", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Targy"].Value = Record.Typed.Targy;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Surgosseg", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Surgosseg"].Value = Record.Typed.Surgosseg;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PostazasIranya", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@PostazasIranya"].Value = Record.Typed.PostazasIranya;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PeldanySzam", System.Data.SqlDbType.Int));
            SqlComm.Parameters["@PeldanySzam"].Value = Record.Typed.PeldanySzam;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Letrehozo_id"].Value = ExecParam.Typed.Felhasznalo_Id;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Erkezteto_SzamList", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Erkezteto_SzamList"].Value = String.Join(",", Erkezteto_SzamList.ToArray());
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@AzonositoList", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@AzonositoList"].Value = String.Join(",", AzonositoList.ToArray()); ;


            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;


            DataSet ds = new DataSet();
            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (System.Data.SqlClient.SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result UgyUgyiratokTomegesInit(ExecParam ExecParam, EREC_UgyUgyiratok Record, List<string> FoszamList, List<string> AzonositoList)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratokTomegesInit");

        Result _ret = new Result();

        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_UgyUgyiratokTomegesInit]");
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IraIktatokonyv_Id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@IraIktatokonyv_Id"].Value = Record.Typed.IraIktatokonyv_Id;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Targy", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Targy"].Value = Record.Typed.Targy;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Allapot", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Allapot"].Value = Record.Typed.Allapot;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UtolsoAlszam", System.Data.SqlDbType.Int));
            SqlComm.Parameters["@UtolsoAlszam"].Value = Record.Typed.UtolsoAlszam;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IratSzam", System.Data.SqlDbType.Int));
            SqlComm.Parameters["@IratSzam"].Value = Record.Typed.IratSzam;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Letrehozo_id"].Value = ExecParam.Typed.Felhasznalo_Id;
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FoszamList", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@FoszamList"].Value = String.Join(",", FoszamList.ToArray());
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@AzonositoList", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@AzonositoList"].Value = String.Join(",", AzonositoList.ToArray()); ;


            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;


            DataSet ds = new DataSet();
            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (System.Data.SqlClient.SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result UgyUgyiratdarabokTomegesInit(ExecParam ExecParam, EREC_UgyUgyiratdarabok Record, List<string> UgyUgyirat_IdList)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_UgyUgyiratdarabokTomegesInit");

        Result _ret = new Result();

        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_UgyUgyiratdarabokTomegesInit]");
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UgyUgyirat_IdList", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@UgyUgyirat_IdList"].Value = String.Join(",", UgyUgyirat_IdList.ToArray());

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@EljarasiSzakasz", System.Data.SqlDbType.Char));
            SqlComm.Parameters["@EljarasiSzakasz"].Value = Record.Typed.EljarasiSzakasz;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Allapot", System.Data.SqlDbType.Char));
            SqlComm.Parameters["@Allapot"].Value = Record.Typed.Allapot;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Letrehozo_id"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;


            DataSet ds = new DataSet();
            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (System.Data.SqlClient.SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);

        return _ret;
    }

    public Result IraIratokTomegesInit(ExecParam ExecParam, EREC_IraIratok Record, List<string> AlszamList, List<string> AzonositoList, List<string> KuldKuldemenyek_IdList, List<string> Ugyirat_IdList)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.SpStart(ExecParam, "sp_EREC_IraIratokTomegesInit");

        Result _ret = new Result();

        try
        {
            System.Data.SqlClient.SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand("[sp_EREC_IraIratokTomegesInit]");
            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@PostazasIranya", System.Data.SqlDbType.Char));
            SqlComm.Parameters["@PostazasIranya"].Value = Record.Typed.PostazasIranya;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@AlszamList", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@AlszamList"].Value = String.Join(",", AlszamList.ToArray());

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UtolsoSorszam", System.Data.SqlDbType.Int));
            SqlComm.Parameters["@UtolsoSorszam"].Value = Record.Typed.UtolsoSorszam;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@IktatasDatuma", System.Data.SqlDbType.DateTime));
            SqlComm.Parameters["@IktatasDatuma"].Value = Record.Typed.IktatasDatuma;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Targy", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Targy"].Value = Record.Typed.Targy;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@FelhasznaloCsoport_Id_Iktato", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@FelhasznaloCsoport_Id_Iktato"].Value = Record.Typed.FelhasznaloCsoport_Id_Iktato;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@KuldKuldemenyek_IdList", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@KuldKuldemenyek_IdList"].Value = String.Join(",", KuldKuldemenyek_IdList.ToArray());

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Allapot", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Allapot"].Value = Record.Typed.Allapot;

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@AzonositoList", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@AzonositoList"].Value = String.Join(",", AzonositoList.ToArray());

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Ugyirat_IdList", System.Data.SqlDbType.NVarChar));
            SqlComm.Parameters["@Ugyirat_IdList"].Value = String.Join(",", Ugyirat_IdList.ToArray());

            SqlComm.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Letrehozo_id", System.Data.SqlDbType.UniqueIdentifier));
            SqlComm.Parameters["@Letrehozo_id"].Value = ExecParam.Typed.Felhasznalo_Id;

            SqlComm.CommandType = System.Data.CommandType.StoredProcedure;

            SqlComm.Connection = dataContext.Connection; SqlComm.CommandTimeout = 300;
            SqlComm.Transaction = dataContext.Transaction;


            DataSet ds = new DataSet();
            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
            adapter.SelectCommand = SqlComm;
            adapter.Fill(ds);

            _ret.Ds = ds;
        }
        catch (System.Data.SqlClient.SqlException e)
        {
            _ret.ErrorCode = e.ErrorCode.ToString();
            _ret.ErrorMessage = e.Message;
        }


        log.SpEnd(ExecParam, _ret);

        return _ret;
    }
}