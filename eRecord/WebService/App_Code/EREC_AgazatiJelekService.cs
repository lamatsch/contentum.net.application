using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_AgazatiJelekService : System.Web.Services.WebService
{
    #region Generaltbol atvettek
    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az EREC_AgazatiJelek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s).
    /// Ha l�tezik az adott �gazati jelhez tartoz� iratt�ri t�tel, akkor azokat is �rv�nytelen�ti.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Ha l�tezik az adott �gazati jelhez tartoz� iratt�ri t�tel, akkor azokat is �rv�nytelen�ti. Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_AgazatiJelek))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_AgazatiJelek", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            #region Iratt�ri t�telek meghat�roz�sa

            EREC_IraIrattariTetelekService serviceIIT = new EREC_IraIrattariTetelekService(this.dataContext);
            EREC_IraIrattariTetelekSearch searchIIT = new EREC_IraIrattariTetelekSearch();
            ExecParam execParamIIT = ExecParam.Clone();

            searchIIT.AgazatiJel_Id.Value = ExecParam.Record_Id;
            searchIIT.AgazatiJel_Id.Operator = Query.Operators.equals;

            Result result_getallIIT = serviceIIT.GetAll(execParamIIT, searchIIT);

            if (!String.IsNullOrEmpty(result_getallIIT.ErrorCode))
            {
                throw new ResultException(result_getallIIT);
            }

            // al�rendelt elemek �rv�nytelen�t�se
            int count = result_getallIIT.Ds.Tables[0].Rows.Count;
            if (count > 0)
            {
                ExecParam[] execParams = new ExecParam[count];

                for (int i = 0; i < count; i++)
                {
                    execParams[i] = ExecParam.Clone();
                    execParams[i].Record_Id = result_getallIIT.Ds.Tables[0].Rows[i]["Id"].ToString();
                }

                Result result_invalidateIIT = serviceIIT.MultiInvalidate(execParams);

                if (!String.IsNullOrEmpty(result_invalidateIIT.ErrorCode))
                {
                    throw new ResultException(result_invalidateIIT);
                }

            }
            #endregion Iratt�ri t�telek meghat�roz�sa

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion Generaltbol atvettek

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_AgazatiJelekSearch))]
    public Result GetAllWithIktathat(ExecParam ExecParam, EREC_AgazatiJelekSearch _EREC_AgazatiJelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithIktathat(ExecParam, _EREC_AgazatiJelekSearch);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}