using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

/// <summary>
///    A(z) EREC_Alairas_Folyamat_Tetelek t�bl�hoz tartoz� Web szolg�ltat�sok.
/// </summary>
[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//[Transaction(Isolation = TransactionIsolationLevel.ReadCommitted)]
public partial class EREC_Alairas_Folyamat_TetelekService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result setSignedStatus(ExecParam ExecParam, Guid tetel_Id, String status)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        ExecParam execParam_aftGet = ExecParam.Clone();
        execParam_aftGet.Record_Id = tetel_Id.ToString();
        EREC_Alairas_Folyamat_TetelekService aFTService = new EREC_Alairas_Folyamat_TetelekService(this.dataContext);
        Result resultAFTGet = aFTService.Get(execParam_aftGet);
        if (!String.IsNullOrEmpty(resultAFTGet.ErrorCode))
        {
            return resultAFTGet;
        }

        EREC_Alairas_Folyamat_Tetelek eREC_Alairas_Folyamat_Tetelek = (EREC_Alairas_Folyamat_Tetelek)resultAFTGet.Record;
        eREC_Alairas_Folyamat_Tetelek.AlairasStatus = status;
        Result result = aFTService.Update(execParam_aftGet, eREC_Alairas_Folyamat_Tetelek);
        log.WsEnd(ExecParam, result);
        return result;
    }
}