using System;
using System.Web.Services;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Net;
using System.Text;
using Contentum.eQuery;
using System.Data;
using Contentum.eDocument.Service;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using Newtonsoft.Json;

/// <summary>
///    A(z) EREC_Alairas_Folyamatok t�bl�hoz tartoz� Web szolg�ltat�sok.
/// </summary>
[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//[Transaction(Isolation = TransactionIsolationLevel.ReadCommitted)]
public partial class EREC_Alairas_FolyamatokService : System.Web.Services.WebService
{

    // A HALK alkalmaz�ssal al��rand� dokumentumok k�r�nek a meghat�roz�sa
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result GetToBeSignedDocumentsURL(ExecParam ExecParam, Guid proc_Id)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetToBeSignedDocumentsURL(ExecParam, proc_Id);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public byte[] GetToBeSignedDocument(ExecParam ExecParam, string url)
    {
        byte[] cont = null;
        try
        {
            WebClient wc = new WebClient();
            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserName");
            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServicePassword");
            string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserDomain");
            wc.Credentials = new NetworkCredential(__usernev, __password, __doamin);
            cont = wc.DownloadData(url);
        }
        catch (Exception ex)
        {
            Logger.Error("GetToBeSignedDocument", ex);
            //result = ResultException.GetResultFromException(exc);
            //return cont;
        }

        return cont;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result setSignedStatus(ExecParam ExecParam, Guid proc_Id, String status)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());
        ExecParam execParam_afGet = ExecParam.Clone();
        execParam_afGet.Record_Id = proc_Id.ToString();
        EREC_Alairas_FolyamatokService aFService = new EREC_Alairas_FolyamatokService(this.dataContext);
        Result resultAFGet = aFService.Get(execParam_afGet);
        if (resultAFGet.IsError)
        {
            return resultAFGet;
        }

        EREC_Alairas_Folyamatok eREC_Alairas_Folyamatok = (EREC_Alairas_Folyamatok)resultAFGet.Record;
        eREC_Alairas_Folyamatok.AlairasStatus = status;
        if(status == "01")
        {
            eREC_Alairas_Folyamatok.AlairasStarted = DateTime.Now.ToString();
        }
        else
        {
            eREC_Alairas_Folyamatok.AlairasStopped = DateTime.Now.ToString();
        }
        Result result = aFService.Update(execParam_afGet, eREC_Alairas_Folyamatok);
        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result uploadDocumentsWithoutSignWithVerify(ExecParam ExecParam, Guid iratId, string csatolmanyId, byte[] signedData, String signedFileName, bool isVerifyMode)
    {
        // HALK sz�m�ra nem kell k�l�n al��r�s
        return uploadDocumentsAndSignFunction(ExecParam, iratId, csatolmanyId, signedData, signedFileName, false, isVerifyMode);
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result uploadDocumentsWithoutSign(ExecParam ExecParam, Guid iratId, string csatolmanyId, byte[] signedData, String signedFileName)
    {
        // HALK sz�m�ra nem kell k�l�n al��r�s
        return uploadDocumentsAndSignFunction(ExecParam, iratId, csatolmanyId, signedData, signedFileName, false, false);
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result uploadDocuments(ExecParam ExecParam, Guid iratId, string csatolmanyId, byte[] signedData, String signedFileName)
    {
        //kor�bbi �g, itt al� is �runk
        return uploadDocumentsAndSignFunction(ExecParam, iratId, csatolmanyId, signedData, signedFileName, true, false);
    }

    #region Seg�df�ggv�nyek
    private Result uploadDocumentsAndSignFunction(ExecParam ExecParam, Guid iratId, string csatolmanyId, byte[] signedData, String signedFileName, bool needSign, bool isVerifyMode)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());
        EREC_IraIratokService erec_IraIratokService = new EREC_IraIratokService(this.dataContext);
        ExecParam execParamIrat = ExecParam.Clone();
        execParamIrat.Record_Id = iratId.ToString();

        EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
        erec_Csatolmanyok.Updated.SetValueAll(false);
        erec_Csatolmanyok.Base.Updated.SetValueAll(false);

        erec_Csatolmanyok.IraIrat_Id = iratId.ToString();
        erec_Csatolmanyok.Updated.IraIrat_Id = true;

        Csatolmany csatolmany = new Csatolmany();
        csatolmany.Tartalom = signedData;
        csatolmany.Nev = signedFileName;

        //csak a HALK �gon
        if (!needSign)
        {
            erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.KikuldottDokumentum;
            erec_Csatolmanyok.Updated.DokumentumSzerep = true;
        }

        string uploadedCsatolmanyId = string.Empty;

        Result resultUpload = HALKCsatolmanyUpload(execParamIrat, erec_Csatolmanyok, csatolmany, false, erec_IraIratokService, out uploadedCsatolmanyId);

        if (!resultUpload.IsError)
        {
            //csak akkor ha sz�ks�ges al��r�s is.
            if (needSign)
            {
                Contentum.eDocument.Service.DokumentumAlairasService service = eDocumentService.ServiceFactory.GetDokumentumAlairasService();
                ExecParam execParamDoc = ExecParam.Clone();
                Result resultDok = service.IratCsatolmanyokAlairas(execParamDoc, new string[] { csatolmanyId }, "");

                if (resultDok.IsError)
                {
                    return resultDok;
                }
            }
            else
            {
                //ezen az �gon csat st�tuszt �ll�tunk
                Result result = new Result();
                bool isConnectionOpenHere = false;
                bool isTransactionBeginHere = false;

                try
                {
                    isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
                    isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

                    if (!string.IsNullOrEmpty(uploadedCsatolmanyId))
                    {
                        #region Csatolm�nyok ellen�rz�se, IratId meghat�roz�sa
                        /// eRecordWebservice h�v�s
                        EREC_CsatolmanyokService service_csatolmanyok = new EREC_CsatolmanyokService();

                        EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();
                        search_csatolmanyok.Id.Value = uploadedCsatolmanyId;
                        search_csatolmanyok.Id.Operator = Query.Operators.equals;

                        Result result_csatGetAll = service_csatolmanyok.GetAll(ExecParam.Clone(), search_csatolmanyok);

                        if (result_csatGetAll.IsError)
                        {
                            throw new ResultException(result_csatGetAll);
                        }

                        #endregion

                        #region EREC_IratAlairok rekord megkeres�se

                        // Majd a v�g�n update-elj�k, de most lek�rj�k, hogy ne h�zzuk majd vele a tranzakci�t

                        EREC_IratAlairokService service_IratAlairok = new EREC_IratAlairokService();

                        EREC_IratAlairokSearch search_iratAlairok = new EREC_IratAlairokSearch();

                        // Keres�s: az adott irathoz a felhasznalo az al��r�, �s �llapot al��rand�

                        search_iratAlairok.Obj_Id.Value = iratId.ToString();
                        search_iratAlairok.Obj_Id.Operator = Query.Operators.equals;

                        search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Value = Csoportok.GetFelhasznaloSajatCsoportId(ExecParam);
                        search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;

                        search_iratAlairok.Allapot.Value = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
                        search_iratAlairok.Allapot.Operator = Query.Operators.equals;

                        Result result_IratAlairokGetAll = service_IratAlairok.GetAll(ExecParam.Clone(), search_iratAlairok);
                        if (result_IratAlairokGetAll.IsError)
                        {
                            throw new ResultException(result_IratAlairokGetAll);
                        }

                        Logger.Info("EREC_IratAlairok lek�r�se: tal�latok sz�ma: " + result_IratAlairokGetAll.Ds.Tables[0].Rows.Count);

                        #endregion

                        #region �gyirat, Iktat�k�nyv lek�r�se

                        EREC_IraIratokService service_iratok = new EREC_IraIratokService();

                        ExecParam execParam_iratHier = ExecParam.Clone();
                        execParam_iratHier.Record_Id = iratId.ToString();

                        Result result_iratHier = service_iratok.GetIratHierarchia(execParam_iratHier);
                        if (result_iratHier.IsError)
                        {
                            throw new ResultException(result_iratHier);
                        }

                        IratHierarchia iratHier = (IratHierarchia)result_iratHier.Record;

                        // Iktat�k�nyv lek�r�se Iktat�helyhez:
                        EREC_IraIktatoKonyvekService service_iktKonyv = new EREC_IraIktatoKonyvekService();

                        ExecParam execParam_iktKonyvGet = ExecParam.Clone();
                        execParam_iktKonyvGet.Record_Id = iratHier.UgyiratObj.IraIktatokonyv_Id;

                        Result result_iktKonyvGet = service_iktKonyv.Get(execParam_iktKonyvGet);
                        if (result_iktKonyvGet.IsError)
                        {
                            throw new ResultException(result_iktKonyvGet);
                        }

                        EREC_IraIktatoKonyvek iktatoKonyv = (EREC_IraIktatoKonyvek)result_iktKonyvGet.Record;

                        #endregion

                        #region Al��rand� f�jl(ok) tartalm�nak lek�r�se

                        KRT_DokumentumokService krt_DokumentumokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

                        // dokumentumId-k �ssze�ll�t�sa:
                        StringBuilder sb_dokIds = new StringBuilder();
                        foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
                        {
                            string strToAppend = sb_dokIds.Length == 0 ? "'" + row["Dokumentum_Id"].ToString() + "'" : ",'" + row["Dokumentum_Id"].ToString() + "'";
                            sb_dokIds.Append(strToAppend);
                        }

                        KRT_DokumentumokSearch search_krtDok = new KRT_DokumentumokSearch();

                        search_krtDok.Id.Value = sb_dokIds.ToString();
                        search_krtDok.Id.Operator = Query.Operators.inner;

                        Result result_krtDokGetAll = krt_DokumentumokService.GetAll(ExecParam.Clone(), search_krtDok);
                        if (result_krtDokGetAll.IsError)
                        {
                            throw new ResultException(result_krtDokGetAll);
                        }

                        // F�jladatok elt�rol�s�hoz:
                        Dictionary<string, AlairandoDok> alairandoDokDict = new Dictionary<string, AlairandoDok>();

                        foreach (DataRow row in result_krtDokGetAll.Ds.Tables[0].Rows)
                        {
                            string row_Id = row["Id"].ToString();
                            string row_External_Link = row["External_Link"].ToString();
                            string row_External_Info = row["External_Info"].ToString();
                            string row_FajlNev = row["FajlNev"].ToString();

                            if (alairandoDokDict.ContainsKey(row_Id) == false/* && row_FajlNev.Contains(signedFileName)*/)
                            {
                                AlairandoDok alairandoDok = new AlairandoDok();
                                alairandoDok.fileUrl_eredeti = row_External_Link;
                                alairandoDok.fileNev_eredeti = row_FajlNev;
                                alairandoDok.external_Info_eredeti = row_External_Info;

                                alairandoDokDict.Add(row_Id, alairandoDok);
                            }
                        }

                        #endregion

                        #region Al��rt dokumentumok regisztr�l�sa

                        foreach (KeyValuePair<string, AlairandoDok> kvp in alairandoDokDict)
                        {
                            string dokumentumId = kvp.Key;
                            AlairandoDok alairandoDok = kvp.Value;
                            UpdateFoDokumentumElektronikusAlairas(ExecParam, krt_DokumentumokService, dokumentumId, KodTarak.DOKUMENTUM_ALAIRAS_PKI.Hivatali_Belso_Alairas, isVerifyMode);
                        }

                        #endregion

                        #region EREC_IratAlairok-ban az al��r�s beregisztr�l�sa

                        // m�r kor�bban lek�rt�k az iratAlairok rekordot

                        if (result_IratAlairokGetAll.Ds.Tables[0].Rows.Count > 0)
                        {
                            DataRow row_IratAlairok = result_IratAlairokGetAll.Ds.Tables[0].Rows[0];

                            string iratAlairok_Id = row_IratAlairok["Id"].ToString();
                            string iratAlairok_Ver = row_IratAlairok["Ver"].ToString();

                            EREC_IratAlairok iratAlairok = new EREC_IratAlairok();
                            iratAlairok.Updated.SetValueAll(false);
                            iratAlairok.Base.Updated.SetValueAll(false);

                            // �llapot �ll�t�sa al��rt-ra:
                            iratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                            iratAlairok.Updated.Allapot = true;

                            iratAlairok.FelhasznaloCsoport_Id_Alairo = ExecParam.Felhasznalo_Id;
                            iratAlairok.Updated.FelhasznaloCsoport_Id_Alairo = true;

                            iratAlairok.FelhaszCsoport_Id_Helyettesito = ExecParam.LoginUser_Id;
                            iratAlairok.Updated.FelhaszCsoport_Id_Helyettesito = true;

                            iratAlairok.AlairasDatuma = DateTime.Now.ToString();
                            iratAlairok.Updated.AlairasDatuma = true;

                            // Verzi� megad�sa:
                            iratAlairok.Base.Ver = iratAlairok_Ver;
                            iratAlairok.Base.Updated.Ver = true;

                            ExecParam execParam_iratAlairokUpdate = ExecParam.Clone();
                            execParam_iratAlairokUpdate.Record_Id = iratAlairok_Id;

                            Result result_iratAlairokUpdate = service_IratAlairok.Update(execParam_iratAlairokUpdate, iratAlairok);
                            if (result_iratAlairokUpdate.IsError)
                            {
                                throw new ResultException(result_iratAlairokUpdate);
                            }
                        }

                        #endregion

                        // COMMIT
                        dataContext.CommitIfRequired(isTransactionBeginHere);
                    }
                }
                catch (Exception e)
                {
                    dataContext.RollbackIfRequired(isTransactionBeginHere);
                    result = ResultException.GetResultFromException(e);
                }
                finally
                {
                    dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
                }

                log.WsEnd(ExecParam, result);
                return result;
            }
        }

        return resultUpload;
    }

    private void UpdateFoDokumentumElektronikusAlairas(ExecParam execparam, KRT_DokumentumokService service_krt_dok, string dokumentumId, string alairasSzint, bool isVerifyMode)
    {
        if (!string.IsNullOrEmpty(alairasSzint) && alairasSzint != KodTarak.DOKUMENTUM_ALAIRAS_PKI.Nincs_Vagy_Nem_Hiteles)
        {

            // KRT_Dokumentumok GET:
            ExecParam execParam_KRT_Dok_Get = execparam.Clone();
            execParam_KRT_Dok_Get.Record_Id = dokumentumId;

            Result result_krt_Dok_Get = service_krt_dok.Get(execParam_KRT_Dok_Get);
            if (result_krt_Dok_Get.IsError)
            {
                throw new ResultException(result_krt_Dok_Get);
            }

            KRT_Dokumentumok dokumentum = (KRT_Dokumentumok)result_krt_Dok_Get.Record;

            int foDokumentumAlairasSzint = 0;
            if (!string.IsNullOrEmpty(dokumentum.ElektronikusAlairas))
            {
                try
                {
                    foDokumentumAlairasSzint = Int32.Parse(dokumentum.ElektronikusAlairas);
                }
                catch
                {
                    foDokumentumAlairasSzint = 0;
                }
            }

            int alairasSzint_int = 0;
            if (!string.IsNullOrEmpty(alairasSzint))
            {
                try
                {
                    alairasSzint_int = Int32.Parse(alairasSzint);
                }
                catch
                {
                    alairasSzint_int = 0;
                }
            }

            if (!isVerifyMode)
            {
                // ElektronikusAlairas mez�t akkor kell update-elni, ha az alairaszint kisebb, mint a m�r megl�v�, de nem 0
                if (alairasSzint_int < foDokumentumAlairasSzint && alairasSzint_int > 0
                    || foDokumentumAlairasSzint == 0 && alairasSzint_int > 0)
                {
                    #region KRT_Dokumentumok UPDATE

                    dokumentum.Updated.SetValueAll(false);
                    dokumentum.Base.Updated.SetValueAll(false);
                    dokumentum.Base.Updated.Ver = true;

                    dokumentum.ElektronikusAlairas = alairasSzint;
                    dokumentum.Updated.ElektronikusAlairas = true;

                    // AlairasFelulvizsgalat mez�t is update-elj�k:
                    dokumentum.AlairasFelulvizsgalat = DateTime.Now.ToString();
                    dokumentum.Updated.AlairasFelulvizsgalat = true;

                    ExecParam execParam_krt_Dok_Update = execparam.Clone();
                    execParam_krt_Dok_Update.Record_Id = dokumentumId;

                    Result result_krt_Dok_Update = service_krt_dok.Update(execParam_krt_Dok_Update, dokumentum);
                    if (result_krt_Dok_Update.IsError)
                    {
                        throw new ResultException(result_krt_Dok_Update);
                    }

                    #endregion

                }
            }
            else
            {
                #region BLG 2947

                dokumentum.Updated.SetValueAll(false);
                dokumentum.Base.Updated.SetValueAll(false);
                dokumentum.Base.Updated.Ver = true;

                // AlairasFelulvizsgalat mez�t is update-elj�k:
                dokumentum.AlairasFelulvizsgalat = DateTime.Now.ToString();
                dokumentum.Updated.AlairasFelulvizsgalat = true;

                // JSON �ssze�ll�t�sa
                StringBuilder sbVerify = new StringBuilder();
                StringWriter swVerify = new StringWriter(sbVerify);
                using (JsonWriter writerVerify = new JsonTextWriter(swVerify))
                {
                    writerVerify.Formatting = Newtonsoft.Json.Formatting.Indented;
                    writerVerify.WriteStartObject();
                    writerVerify.WritePropertyName("AlairasFelulvizsgalatEredmeny");
                    writerVerify.WriteValue(KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikeres_felulvizsgalat);
                    //writerVerify.WriteValue("Sikeres fel�lvizsg�lat");
                    writerVerify.WriteEndObject();
                }

                var jsonVerified = sbVerify.ToString();

                dokumentum.AlairasFelulvizsgalatEredmeny = jsonVerified;
                dokumentum.Updated.AlairasFelulvizsgalatEredmeny = true;

                ExecParam execParam_krt_Dok_Update = execparam.Clone();
                execParam_krt_Dok_Update.Record_Id = dokumentumId;

                Result result_krt_Dok_Update = service_krt_dok.Update(execParam_krt_Dok_Update, dokumentum);
                if (result_krt_Dok_Update.IsError)
                {
                    throw new ResultException(result_krt_Dok_Update);
                }

                #endregion
            }
        }
    }

    //EREC_IraIratokService upload f�ggv�ny mint�j�ra HALK felt�lt�shez alak�tva
    public Result HALKCsatolmanyUpload(ExecParam ExecParam, EREC_Csatolmanyok Record, Csatolmany csatolmany, bool ujIrat, EREC_IraIratokService erec_IraIratokService, out string uploadedCsatolmanyId)
    {
        Logger.Debug(String.Format("Erec_Alairas_folyamatokService.HALKCsatolmanyUpload indul. Record.Id: {0} - csatolmany.Nev: {1}", Record.Id, csatolmany.Nev));

        uploadedCsatolmanyId = string.Empty;

        Log log = Log.WsStart(ExecParam, Context, GetType());
        DocumentService documentService = eDocumentService.ServiceFactory.GetDocumentService();

        Result result = new Result();

        #region irat id alapjan mindenfele uploadhoz kello dolog lekerdezese

        String ugyiratFoszam = String.Empty;
        String iktatokonyvIktatohely = String.Empty;
        string ev = String.Empty;

        EREC_IraIratokSearch iratSearch = new EREC_IraIratokSearch();
        iratSearch.Id.Value = Record.IraIrat_Id;
        iratSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

        ExecParam ep = ExecParam.Clone();
        Result resultIratExt = erec_IraIratokService.GetAllWithExtension(ep, iratSearch);

        if (!String.IsNullOrEmpty(resultIratExt.ErrorCode))
        {
            log.WsEnd(ExecParam, resultIratExt);
            throw new ResultException(resultIratExt);
        }

        if (resultIratExt.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Error(String.Format("Erec_Alairas_folyamatokService.HALKCsatolmanyUpload hiba: irat GetAllWithExtension nem adott vissza sorokat! Id: {0}", Record.IraIrat_Id));
            resultIratExt = new Result();
            resultIratExt.ErrorCode = "GETIEXT0001";
            resultIratExt.ErrorMessage = "Erec_Alairas_folyamatokService.HALKCsatolmanyUpload hiba: irat GetAllWithExtension nem adott vissza sorokat!";
            throw new ResultException(resultIratExt);
        }

        if (resultIratExt.Ds.Tables[0].Rows.Count > 1)
        {
            Logger.Error(String.Format("Erec_Alairas_folyamatokService.HALKCsatolmanyUpload hiba: irat GetAllWithExtension tul sok sort adott vissza! Id: {0}", Record.IraIrat_Id));
            resultIratExt = new Result();
            resultIratExt.ErrorCode = "GETIEXT0002";
            resultIratExt.ErrorMessage = "Erec_Alairas_folyamatokService.HALKCsatolmanyUpload hiba: irat GetAllWithExtension tul sok sort adott vissza!";
            throw new ResultException(resultIratExt);
        }

        try
        {
            ugyiratFoszam = Convert.ToString(resultIratExt.Ds.Tables[0].Rows[0]["Foszam"]);

            iktatokonyvIktatohely = Convert.ToString(resultIratExt.Ds.Tables[0].Rows[0]["Iktatoszam_Merge"]);
            iktatokonyvIktatohely = iktatokonyvIktatohely.Substring(0, iktatokonyvIktatohely.IndexOf("/", 0)).Trim();

            ev = Convert.ToString(resultIratExt.Ds.Tables[0].Rows[0]["Ev"]);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Erec_Alairas_folyamatokService.HALKCsatolmanyUpload hiba konvertalaskor: Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
            resultIratExt = new Result();
            resultIratExt.ErrorCode = "GETIEXT0003";
            resultIratExt.ErrorMessage = String.Format("Erec_Alairas_folyamatokService.HALKCsatolmanyUpload hiba konvertalaskor: Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            throw new ResultException(resultIratExt);
        }

        EREC_IraIratok irat = new EREC_IraIratok();
        Utility.LoadBusinessDocumentFromDataRow(irat, resultIratExt.Ds.Tables[0].Rows[0]);

        #region ugyirat lekerese
        EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();

        if (String.IsNullOrEmpty(ugyiratFoszam))
        {
            EREC_UgyUgyiratokService svcUgyirat = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam xpmUgyirat = ExecParam.Clone();
            xpmUgyirat.Record_Id = irat.Ugyirat_Id;
            Result resUgyirat = svcUgyirat.Get(xpmUgyirat);

            if (resUgyirat.IsError)
            {
                throw new ResultException(resUgyirat);
            }

            ugyirat = resUgyirat.Record as EREC_UgyUgyiratok;
        }
        else
        {
            ugyirat.Foszam = ugyiratFoszam;
        }
        #endregion

        #endregion

        try
        {
            string uploadXmlStrParams_alairasResz = "";
            if (csatolmany.AlairasAdatok.CsatolmanyAlairando)
            {
                uploadXmlStrParams_alairasResz = csatolmany.AlairasAdatok.GetInnerXml();
            }


            if (!String.IsNullOrEmpty(csatolmany.SourceSharePath))
            {
                #region Ha SourceSharePath meg van adva...

                string kivalasztottIratId = Record.IraIrat_Id;
                string uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                        "<iktatokonyv>{5}</iktatokonyv>" +
                                                        "<sourceSharePath>{6}</sourceSharePath>" +
                                                        "<foszam>{0}</foszam>" +
                                                        "<alszam>{8}</alszam>" +
                                                        "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                        "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                        "<megjegyzes></megjegyzes>" +
                                                        "<munkaanyag>{2}</munkaanyag>" +
                                                        "<ujirat>{3}</ujirat>" +
                                                        "<vonalkod></vonalkod>" +
                                                        "<docmetaIratId></docmetaIratId>" +
                                                        "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                        "<tartalomSHA1>{7}</tartalomSHA1>" +
                                                        "<ev>{9}</ev>" +
                                                        // Al��r�shoz sz�ks�ges r�sz:
                                                        uploadXmlStrParams_alairasResz +
                                                    "</uploadparameterek>"
                                                    , Contentum.eRecord.BaseUtility.Ugyiratok.GetFoszamString(ugyirat)
                                                    , ujIrat ? String.Empty : kivalasztottIratId
                                                    , Contentum.eRecord.BaseUtility.Iratok.Munkaanyag(irat) ? "IGEN" : "NEM"
                                                    , ujIrat ? "IGEN" : "NEM"
                                                    , "NEM"
                                                    , iktatokonyvIktatohely
                                                    , String.Empty           //csatolmany.SourceSharePath
                                                    , csatolmany.TartalomHash
                                                    , Contentum.eRecord.BaseUtility.Iratok.GetAlszamString(irat)
                                                    , ev
                                                    );

                result = documentService.UploadFromeRecordWithCTTCheckin
                    (
                       ExecParam
                       , Rendszerparameterek.Get(ExecParam, Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                       , csatolmany.Nev
                       , csatolmany.Tartalom
                       , null
                       , uploadXmlStrParams
                    );
                #endregion
            }
            else
            {
                // KRT_Dokumentumok objektum l�trehoz�sa
                KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();

                #region KRT_Dokumentumok mez�inek �ll�t�sa

                if (csatolmany.Megnyithato != null)
                {
                    krt_Dokumentumok.Megnyithato = csatolmany.Megnyithato;
                }
                if (csatolmany.Olvashato != null)
                {
                    krt_Dokumentumok.Olvashato = csatolmany.Olvashato;
                }
                if (csatolmany.ElektronikusAlairas != null)
                {
                    krt_Dokumentumok.ElektronikusAlairas = csatolmany.ElektronikusAlairas;
                }
                if (!String.IsNullOrEmpty(csatolmany.Titkositas))
                {
                    krt_Dokumentumok.Titkositas = csatolmany.Titkositas;
                }
                if (csatolmany.OCRAdatok.OCRAllapot == "1")
                {
                    krt_Dokumentumok.OCRAllapot = csatolmany.OCRAdatok.OCRAllapot;
                    krt_Dokumentumok.OCRPrioritas = csatolmany.OCRAdatok.OCRPrioritas;
                }

                #endregion

                string kivalasztottIratId = Record.IraIrat_Id;
                string uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                        "<iktatokonyv>{5}</iktatokonyv>" +
                                                        "<sourceSharePath></sourceSharePath>" +
                                                        "<foszam>{0}</foszam>" +
                                                        "<alszam>{7}</alszam>" +
                                                        "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                        "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                        "<megjegyzes></megjegyzes>" +
                                                        "<munkaanyag>{2}</munkaanyag>" +
                                                        "<ujirat>{3}</ujirat>" +
                                                        "<vonalkod></vonalkod>" +
                                                        "<docmetaIratId></docmetaIratId>" +
                                                        "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                        "<tartalomSHA1>{6}</tartalomSHA1>" +
                                                        "<ev>{8}</ev>" +
                                                        // Al��r�shoz sz�ks�ges r�sz:
                                                        uploadXmlStrParams_alairasResz +
                                                    "</uploadparameterek>"
                                                    , Contentum.eRecord.BaseUtility.Ugyiratok.GetFoszamString(ugyirat)
                                                    , ujIrat ? String.Empty : kivalasztottIratId
                                                    , Contentum.eRecord.BaseUtility.Iratok.Munkaanyag(irat) ? "IGEN" : "NEM"
                                                    , ujIrat ? "IGEN" : "NEM"
                                                    , "NEM"
                                                    , iktatokonyvIktatohely
                                                    , csatolmany.TartalomHash
                                                    , Contentum.eRecord.BaseUtility.Iratok.GetAlszamString(irat)
                                                    , ev);

                Logger.Debug(String.Format("Erec_Alairas_folyamatokService.HALKCsatolmanyUpload documentService.UploadFromeRecordWithCTTCheckin elott. - uploadXmlStrParams: {0}", uploadXmlStrParams));
                result = documentService.UploadFromeRecordWithCTTCheckin
                    (
                       ExecParam
                       , Rendszerparameterek.Get(ExecParam, Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                       , csatolmany.Nev
                       , csatolmany.Tartalom
                       , krt_Dokumentumok
                       , uploadXmlStrParams
                    );
                Logger.Debug("Erec_Alairas_folyamatokService.HALKCsatolmanyUpload documentService.UploadFromeRecordWithCTTCheckin utan.");

            }


            if (result.IsError)
            {
                // hiba:
                log.WsEnd(ExecParam, result);
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s

            Logger.Debug("Esem�nynapl�z�s 1 indul.");

            Result eventLogResult = null;
            KRT_EsemenyekService eventLogService = null;
            KRT_Esemenyek eventLogRecord = null;

            try
            {
                eventLogService = new KRT_EsemenyekService(this.dataContext);

                eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Dokumentumok", "DokumentumokNew").Record;

                if (String.IsNullOrEmpty(ExecParam.Felhasznalo_Id))
                {
                    Logger.Error("Ures a ExecParam.Felhasznalo_Id valtozo, ezert nem lesz bejegyezve a DokumentumokNew esemeny!");
                }
                else
                {
                    eventLogRecord.Felhasznalo_Id_Login = (ExecParam.Helyettesites_Id != null) ? ExecParam.LoginUser_Id : ExecParam.Felhasznalo_Id;
                }

                eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);

                if (!String.IsNullOrEmpty(eventLogResult.ErrorCode))
                {
                    Logger.Error(String.Format("Hibaval tert vissza az esemeny insert! - {0}\n{1}", eventLogResult.ErrorCode, eventLogResult.ErrorMessage));
                }
            }
            catch (Exception ex1)
            {
                Logger.Error(String.Format("Hiba az esem�nynapl�z�sban: {0}\nStack: {1}\nSource: {2}", ex1.Message, ex1.StackTrace, ex1.Source));
            }

            #endregion


            #region Eredm�ny feldolgoz�sa

            // result.Record -ban j�n vissza n�h�ny dolog, pl. hogy kell-e �j csatolm�ny bejegyz�s, vagy csak update kell
            // (ha volt m�r egy ugyanilyen nev� csatolm�ny, akkor nem j�tt l�tre �j KRT_Dokumentumok bejegyz�s, �gy nem kell
            // �j EREC_Csatolmanyok bejegyz�s sem)

            /// A visszaadott xml string: 
            /// <result>
            /// <dokumentumFullUrl>XXXXXXXXXXXX</dokumentumFullUrl>
            /// <tortentUjKrtDokBejegyzes>XXXXXXXXXXXXX</tortentUjKrtDokBejegyzes>
            /// <csatolmanyId>XXXXXXXXXXXX</csatolmanyId>
            /// <csatolmanyVerzio>XXXXXXXXXXXX</csatolmanyVerzio>
            /// </result>


            Logger.Debug("EREC_Alairas_FolyamatokService.HALKCsatolmanyUpload feltoltes ok.");

            bool csatolmanyUpdateKell = false;
            string csatolmanyIdToUpdate = String.Empty;
            string csatolmanyVerToUpdate = String.Empty;

            if (result.Record != null)
            {
                string resultXml = result.Record.ToString();
                if (!string.IsNullOrEmpty(resultXml))
                {
                    Logger.Debug(String.Format("feltoltesbol kapott resultXml: {0}", resultXml));

                    XmlDocument resultXmlDoc = new XmlDocument();
                    resultXmlDoc.LoadXml(resultXml);

                    string tortentUjKrtDokBejegyzes = GetParam("tortentUjKrtDokBejegyzes", resultXmlDoc);
                    csatolmanyIdToUpdate = GetParam("csatolmanyId", resultXmlDoc);
                    csatolmanyVerToUpdate = GetParam("csatolmanyVerzio", resultXmlDoc);

                    if (string.IsNullOrEmpty(tortentUjKrtDokBejegyzes)
                        && !string.IsNullOrEmpty(csatolmanyIdToUpdate))
                    {
                        csatolmanyUpdateKell = true;
                    }
                }
            }

            uploadedCsatolmanyId = csatolmanyIdToUpdate;

            #endregion

            EREC_CsatolmanyokService erec_CsatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);

            Logger.Debug(String.Format("csatolmanyUpdateKell: {0}", csatolmanyUpdateKell));

            if (csatolmanyUpdateKell)
            {
                #region update elotti csatolmany objektum lekeres, ha document_id ures

                if (String.IsNullOrEmpty(Record.Dokumentum_Id))
                {
                    Logger.Debug("update elotti csatolmany objektum lekeres, ha document_id ures");

                    ExecParam exPar = ExecParam.Clone();
                    exPar.Record_Id = csatolmanyIdToUpdate;
                    Result resultCsatSelect = erec_CsatolmanyokService.Get(exPar);

                    if (!String.IsNullOrEmpty(resultCsatSelect.ErrorCode))
                    {
                        Logger.Error(String.Format("csatolmany lekeres soran hiba: {0}\nErrorMessage: {1}\nErrorDetail: {2}", resultCsatSelect.ErrorCode, resultCsatSelect.ErrorMessage, resultCsatSelect.ErrorDetail));
                        throw new ResultException(resultCsatSelect);
                    }

                    if (resultCsatSelect.Record == null)
                    {
                        Logger.Error(String.Format("csatolmany lekeres soran hiba: null a csatolmany select resultjaban visszakapott Record mezo!"));
                        resultCsatSelect = new Result();
                        resultCsatSelect.ErrorCode = "CSAUPCSASZE001";
                        resultCsatSelect.ErrorMessage = "Hiba a felt�lt�tt csatolm�ny bejegyz�s visszaellen�rz�sekor! Az eredm�nyobjektumban visszakapott Record mez� �rt�ke null.";
                        throw new ResultException(resultCsatSelect);
                    }

                    Record = (EREC_Csatolmanyok)resultCsatSelect.Record;
                }

                #endregion

                #region EREC_Csatolmanyok UPDATE

                Logger.Debug(String.Format("erec csat update"));

                Record.Base.Ver = csatolmanyVerToUpdate;
                Record.Base.Updated.Ver = true;

                ExecParam execParam_update = ExecParam.Clone();
                execParam_update.Record_Id = csatolmanyIdToUpdate;

                result = erec_CsatolmanyokService.Update(execParam_update, Record);
                if (result.IsError)
                {
                    // hiba:
                    throw new ResultException(result);
                }

                #endregion
            }
            else
            {
                #region EREC_Csatolmanyok INSERT

                Logger.Debug(String.Format("erec csat insert"));

                Record.Dokumentum_Id = result.Uid;
                Record.Updated.Dokumentum_Id = true;

                #region F�dokumentum megl�t�nek ellen�rz�se
                if (String.IsNullOrEmpty(Record.DokumentumSzerep))
                {
                    Logger.Debug(String.Format("F�dokumentum keres�s ind�t�s"));
                    Result result_fodoku_search = null;
                    EREC_CsatolmanyokSearch foduko_search = new EREC_CsatolmanyokSearch();
                    foduko_search.DokumentumSzerep.Value = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
                    foduko_search.DokumentumSzerep.Operator = Query.Operators.equals;
                    foduko_search.TopRow = 1;
                    foduko_search.IraIrat_Id.Value = Record.IraIrat_Id;
                    foduko_search.IraIrat_Id.Operator = Query.Operators.equals;
                    ExecParam execParam_fodoku_search = ExecParam.Clone();
                    result_fodoku_search = erec_CsatolmanyokService.GetAll(execParam_fodoku_search, foduko_search);

                    if (result_fodoku_search.IsError)
                    {
                        Logger.Error(String.Format("F�dokumentum keres�s hib�ra futott"));
                    }
                    else if (result_fodoku_search.Ds.Tables[0].Rows.Count > 0)
                    {
                        Logger.Debug(String.Format("Van m�r f�dokumentum"));
                    }
                    else
                    {
                        Logger.Debug(String.Format("Nincs m�g f�dokumentum, az �j f�jlt be�ll�tjuk F�dokumentumnak"));
                        Record.DokumentumSzerep = "01";
                        Record.Updated.DokumentumSzerep = true;
                    }

                }
                #endregion


                result = erec_CsatolmanyokService.Insert(ExecParam, Record);
                if (result.IsError)
                {
                    // hiba:
                    throw new ResultException(result);
                }

                //�j csatolm�ny l�trehoz�sakor az id ment�se
                if (result != null && !string.IsNullOrEmpty(result.Uid))
                    uploadedCsatolmanyId = result.Uid;

                #endregion

                    //#region Esem�nynapl�z�s
                    //    eventLogService = new KRT_EsemenyekService(this.dataContext);

                    //    eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_Csatolmanyok", "New").Record;

                    //    eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
                    //#endregion

            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        //finally
        //{
        //    dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        //}

        Logger.Debug("Irat.CsatolmanyUpload vege ok.");

        log.WsEnd(ExecParam, result);
        return result;
    }

    private string GetParam(string pname, XmlDocument xdoc)
    {
        string _retval = String.Empty;

        if (xdoc == null) return _retval;

        string xpath = String.Format("//{0}", pname);

        if (xdoc.SelectNodes(xpath).Count == 1)
        {
            try
            {
                XmlNode xn = xdoc.SelectSingleNode(xpath);
                if (xn != null)
                {
                    _retval = Convert.ToString(xn.InnerText);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("DocumentService.GetParam hiba!");
                Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error(String.Format("panem: {0}\nxmlDoc: {1}", pname, xdoc.OuterXml.ToString()));
            }
        }

        return _retval;
    }

    #endregion

    private class AlairandoDok
    {
        public string fileNev_eredeti;
        public string fileNev_alairt;

        public string fileUrl_eredeti;

        public byte[] fileTartalom_eredeti;
        public byte[] fileTartalom_alairt;

        public string external_Id_alairt;
        public string external_Link_alairt;
        public string external_Source_alairt;

        public string external_Info_eredeti;
        public string external_Info_alairt;
    }
}