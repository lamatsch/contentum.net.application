﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eDocument.Service;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_CsatolmanyokService : System.Web.Services.WebService
{
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_CsatolmanyokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_CsatolmanyokSearch _EREC_CsatolmanyokSearch)
    {
        return GetAllWithExtensionInternal(ExecParam, _EREC_CsatolmanyokSearch, true);
    }

    public Result GetAllWithExtensionInternal(ExecParam ExecParam, EREC_CsatolmanyokSearch _EREC_CsatolmanyokSearch, bool refreshFormUCM)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        if (refreshFormUCM)
        {

            string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);

            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                try
                {
                    string iratId = _EREC_CsatolmanyokSearch.IraIrat_Id.Value;

                    if (!String.IsNullOrEmpty(iratId) && _EREC_CsatolmanyokSearch.IraIrat_Id.Operator == Contentum.eQuery.Query.Operators.equals)
                    {
                        UCMDocumentService service = eDocumentService.ServiceFactory.GetUCMDocumentService();
                        result = service.RefreshDocuments(ExecParam, iratId);
                    }
                }
                catch (Exception e)
                {
                    result = ResultException.GetResultFromException(e);
                }
            }
        }

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_CsatolmanyokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_CsatolmanyokSearch))]
    public Result GetAllByDokumentum(ExecParam execParam, string dokumentumId, EREC_CsatolmanyokSearch erec_CsatolmanyokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByDokumentum(execParam, dokumentumId,erec_CsatolmanyokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_CsatolmanyokSearch))]
    public Result TomegesInvalidate(ExecParam ExecParam, EREC_CsatolmanyokSearch _EREC_CsatolmanyokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.TomegesInvalidate(ExecParam, DateTime.Now, _EREC_CsatolmanyokSearch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}
