using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Web.Services;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_FeladatErtesitesService : System.Web.Services.WebService
{
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_FeladatErtesitesSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_FeladatErtesitesSearch _EREC_FeladatErtesitesSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_FeladatErtesitesSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}