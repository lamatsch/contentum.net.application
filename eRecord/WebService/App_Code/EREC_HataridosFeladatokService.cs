using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Collections.Generic;
using Contentum.eQuery;
using System.Data;
using System.Text;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_HataridosFeladatokService : System.Web.Services.WebService
{
    #region Gener�ltb�l �tvett

    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_HataridosFeladatok Record)
    /// Egy rekord felv�tele a EREC_HataridosFeladatok t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Hatarid_Objektumok))]
    public Result Insert(ExecParam ExecParam, EREC_HataridosFeladatok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            string feladatId = result.Uid;

            Logger.Debug("Ertesites kuldese start");
            Logger.Debug("Prioritas: " + Record.Prioritas);
            Record.Id = feladatId;
            //Email vagy uzenet k�ld�se
            Contentum.eRecord.BaseUtility.Notify.SendNewFeladatNotification(ExecParam, Record);
            Logger.Debug("Ertesites kuldese end");

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_HataridosFeladatok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Update(ExecParam ExecParam, EREC_HataridosFeladatok Record)
    /// Az EREC_HataridosFeladatok t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result Update(ExecParam ExecParam, EREC_HataridosFeladatok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            Logger.Debug("Ertesites kuldese start");
            Logger.Debug("Lezaras Prioritas: " + Record.LezarasPrioritas);
            //Email vagy uzenet k�ld�se
            Record.Id = ExecParam.Record_Id;
            if (Record.Updated.Felhasznalo_Id_Felelos)
                Contentum.eRecord.BaseUtility.Notify.SendNewFeladatNotification(ExecParam, Record);
            else
                Contentum.eRecord.BaseUtility.Notify.SendUpdateFeladatNotification(ExecParam, Record);
            Logger.Debug("Ertesites kuldese end");

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_HataridosFeladatok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Hatarid_Objektumok))]
    public Result Insert_Tomeges(ExecParam ExecParam, EREC_HataridosFeladatok Record, List<Objektum> Objektumok_List)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Debug("Az Objektumok lista merete: " + Objektumok_List.Count);

            ExecParam xpmTomeges = ExecParam.Clone();
            Result resTomeges;
            List<string> IdList = new List<string>();
            foreach (Objektum objektum in Objektumok_List)
            {
                if (!String.IsNullOrEmpty(objektum.Azonosito))
                {
                    Record.Azonosito = objektum.Azonosito;
                    Record.Updated.Azonosito = true;
                }
                
                Record.Obj_Id = objektum.Id;
                Record.Updated.Obj_Id = true;

                Record.Obj_type = objektum.Type;
                Record.Updated.Obj_type = true;


                resTomeges = this.Insert(xpmTomeges, Record);

                if (resTomeges.IsError)
                {
                    Logger.Error("EREC_HataridosFeladatok insert hiba", xpmTomeges, resTomeges);
                    throw new ResultException(resTomeges);
                }

                IdList.Add(resTomeges.Uid);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                string Ids = Search.GetSqlInnerString(IdList.ToArray());

                Result eventLogResult = eventLogService.InsertTomeges(ExecParam, Ids, "EREC_HataridosFeladatok", "New");
            }
            #endregion
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result Insert_OneTaskForMoreUsers(ExecParam ExecParam, EREC_HataridosFeladatok Record, List<string> FelhasznaloIds, List<string> CsoportIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (String.IsNullOrEmpty(Record.Felhasznalo_Id_Felelos))
            {
                throw new ResultException(59020); // Hiba a feladatcsoport l�trehoz�s�n�l: nincs megadva a f� feladat felel�se!
            }


            Logger.Debug(String.Format("Azonositok szama: felhasznalo {0}; csoport {1}", FelhasznaloIds == null ? 0 : FelhasznaloIds.Count, CsoportIds == null ? 0 : CsoportIds.Count));

            #region Csoportok dolgoz�inak lek�r�se
            if (CsoportIds != null && CsoportIds.Count > 0)
            {
                ExecParam execParam_csoporttagok = ExecParam.Clone();
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoporttagok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

                KRT_CsoportTagokSearch search_csoporttagok = new KRT_CsoportTagokSearch();
                search_csoporttagok.Csoport_Id.Value = Search.GetSqlInnerString(CsoportIds.ToArray());
                search_csoporttagok.Csoport_Id.Operator = Query.Operators.inner;

                search_csoporttagok.Tipus.Value = Search.GetSqlInnerString(new string[] { KodTarak.CsoprttagsagTipus.dolgozo
                    , KodTarak.CsoprttagsagTipus.vezeto});
                search_csoporttagok.Tipus.Operator = Query.Operators.inner;

                Result result_csoporttagok = service_csoporttagok.GetAll(execParam_csoporttagok, search_csoporttagok);

                if (result_csoporttagok.IsError)
                {
                    throw new ResultException(result_csoporttagok);
                }

                if (FelhasznaloIds == null)
                {
                    FelhasznaloIds = new List<string>();
                }

                foreach (DataRow row in result_csoporttagok.Ds.Tables[0].Rows)
                {
                    string id = row["Csoport_Id_Jogalany"].ToString();

                    if (!String.IsNullOrEmpty(id) && !FelhasznaloIds.Contains(id))
                    {
                        FelhasznaloIds.Add(id);
                    }
                }
            }
            #endregion Csoportok dolgoz�inak lek�r�se

            ExecParam xpmTomeges = ExecParam.Clone();
            List<string> IdList = new List<string>();

            #region F� feladat l�trehoz�sa
            Result resMain = this.Insert(xpmTomeges, Record);

            if (resMain.IsError)
            {
                Logger.Error("EREC_HataridosFeladatok insert hiba", xpmTomeges, resMain);
                throw new ResultException(resMain);
            }

            string MainUid = resMain.Uid;
            IdList.Add(MainUid);
            #endregion F� feladat l�trehoz�sa

            #region Al�rendelt kl�n r�szfeladatok l�trehoz�sa
            Record.HataridosFeladat_Id = MainUid;
            Record.Updated.HataridosFeladat_Id = true;

            if (FelhasznaloIds != null)
            {
                foreach (string id in FelhasznaloIds)
                {
                    Record.Felhasznalo_Id_Felelos = id;
                    Record.Updated.Felhasznalo_Id_Felelos = true;

                    Result resTomeges = this.Insert(xpmTomeges, Record);

                    if (resTomeges.IsError)
                    {
                        Logger.Error("EREC_HataridosFeladatok insert hiba", xpmTomeges, resTomeges);
                        throw new ResultException(resTomeges);
                    }

                    IdList.Add(resTomeges.Uid);
                }
            }
            #endregion Al�rendelt kl�n r�szfeladatok l�trehoz�sa

            result = resMain;

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                string Ids = Search.GetSqlInnerString(IdList.ToArray());

                Result eventLogResult = eventLogService.InsertTomeges(ExecParam, Ids, "EREC_HataridosFeladatok", "New");
            }
            #endregion
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatokSearch))]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Hatarid_ObjektumokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_HataridosFeladatokSearch _EREC_HataridosFeladatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            if (_EREC_HataridosFeladatokSearch != null)
            {
                if (!String.IsNullOrEmpty(_EREC_HataridosFeladatokSearch.Obj_Id.Value) && !String.IsNullOrEmpty(_EREC_HataridosFeladatokSearch.Obj_type.Value))
                {
                    if (_EREC_HataridosFeladatokSearch.UseUgyiratHierarchy &&
                        _EREC_HataridosFeladatokSearch.Obj_Id.Operator == Query.Operators.equals &&
                        (_EREC_HataridosFeladatokSearch.Obj_type.Value == Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok ||
                        _EREC_HataridosFeladatokSearch.Obj_type.Value == Contentum.eUtility.Constants.TableNames.EREC_IraIratok ||
                        _EREC_HataridosFeladatokSearch.Obj_type.Value == Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok ||
                        _EREC_HataridosFeladatokSearch.Obj_type.Value == Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek))
                    {
                        EREC_UgyUgyiratokService svc = new EREC_UgyUgyiratokService(this.dataContext);
                        ExecParam xpm = ExecParam.Clone();

                        Result res = svc.GetUgyiratObjektumai(xpm, _EREC_HataridosFeladatokSearch.Obj_Id.Value, _EREC_HataridosFeladatokSearch.Obj_type.Value, _EREC_HataridosFeladatokSearch.UseUgyiratHierarchyWithParent);
                        if (res.IsError)
                        {
                            throw new ResultException(res);
                        }

                        StringBuilder sb = new StringBuilder();
                        int i = 0;
                        foreach (DataRow row in res.Ds.Tables[0].Rows)
                        {
                            string id = row["Id"].ToString();
                            if (i > 0)
                            {
                                sb.Append(",");
                            }

                            sb.Append("'");
                            sb.Append(id);
                            sb.Append("'");

                            i++;
                        }

                        _EREC_HataridosFeladatokSearch.Obj_Id.Value = sb.ToString();
                        _EREC_HataridosFeladatokSearch.Obj_Id.Operator = Query.Operators.inner;

                        _EREC_HataridosFeladatokSearch.Obj_type.Value = String.Empty;
                        _EREC_HataridosFeladatokSearch.Obj_type.Operator = String.Empty;
                    }
                }
            }

            result = sp.GetAllWithExtension(ExecParam, _EREC_HataridosFeladatokSearch);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result Feladatok_GetSummary(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            // Snapshot Isolation Level
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            var IRATTAROZAS_HELY_FELH_ALTAL = Rendszerparameterek.GetBoolean(ExecParam, "IRATTAROZAS_HELY_FELH_ALTAL", true);

            var kolcsonzesreKezelhetoIrattarak = "";
            if (!IRATTAROZAS_HELY_FELH_ALTAL)
            {
                if (KellIrattarListaSzures(ExecParam, "FromKozpontiIrattar"))
                {
                    kolcsonzesreKezelhetoIrattarak = Guid.Empty.ToString();
                    var allJogosultIrattariHelyIds = Contentum.eRecord.BaseUtility.Irattar.GetAllJogosultIrattariHelyIds(ExecParam);
                    if (allJogosultIrattariHelyIds.Count > 0)
                    {
                        kolcsonzesreKezelhetoIrattarak = String.Join(",", allJogosultIrattariHelyIds.ToArray());
                    }
                }
            }

            var kikeresreKezelhetoIrattarak = "";
            if (!IRATTAROZAS_HELY_FELH_ALTAL)
            {
                if (KellIrattarListaSzures(ExecParam, "FromAtmenetiIrattar"))
                {
                    var allIrattar = Contentum.eRecord.BaseUtility.Irattar.GetAllIrattarIds(ExecParam);
                    if (allIrattar.Count > 0)
                    {
                        kikeresreKezelhetoIrattarak = String.Join(",", allIrattar.ToArray());
                    }
                }
            }

            result = sp.Feladatok_GetSummary(ExecParam, kolcsonzesreKezelhetoIrattarak, kikeresreKezelhetoIrattarak);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result Megtekint(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            bool IsUpdated = false;

            result = this.Get(ExecParam);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            EREC_HataridosFeladatok erec_hataridosfeladat = (EREC_HataridosFeladatok)result.Record;

            if (erec_hataridosfeladat != null)
            {
                if (erec_hataridosfeladat.Allapot == KodTarak.FELADAT_ALLAPOT.Uj &&
                    erec_hataridosfeladat.Felhasznalo_Id_Felelos == ExecParam.Felhasznalo_Id)
                {
                    erec_hataridosfeladat.Updated.SetValueAll(false);
                    erec_hataridosfeladat.Base.Updated.SetValueAll(false);
                    erec_hataridosfeladat.Base.Updated.Ver = true;

                    erec_hataridosfeladat.Allapot = KodTarak.FELADAT_ALLAPOT.Nyitott;
                    erec_hataridosfeladat.Updated.Allapot = true;

                    erec_hataridosfeladat.KezdesiIdo = DateTime.Now.ToString();
                    erec_hataridosfeladat.Updated.KezdesiIdo = true;

                    Result resUpdate = this.Update(ExecParam, erec_hataridosfeladat);

                    if (resUpdate.IsError)
                    {
                        throw new ResultException(resUpdate);
                    }

                    IsUpdated = true;

                    erec_hataridosfeladat.Base.Typed.Ver = erec_hataridosfeladat.Base.Typed.Ver.Value + 1;
                }
            }

            result.IsUpdated = IsUpdated;

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result Lezaras(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result resGet = this.Get(ExecParam);

            if (resGet.IsError)
            {
                Logger.Error("Feladat lekerese hiba", ExecParam, resGet);
                throw new ResultException(resGet);
            }

            EREC_HataridosFeladatok erec_hataridosFeladatok = (EREC_HataridosFeladatok)resGet.Record;

            ErrorDetails errorDetails;
            if (!Contentum.eRecord.BaseUtility.HataridosFeladatok.Lezarhato(ExecParam, erec_hataridosFeladatok, out errorDetails))
            {
                Logger.Warn("A feladat nem zarhato le",ExecParam);
                throw new ResultException(59002,errorDetails);
            }

            erec_hataridosFeladatok.Updated.SetValueAll(false);
            erec_hataridosFeladatok.Base.Updated.SetValueAll(false);
            erec_hataridosFeladatok.Base.Updated.Ver = true;

            erec_hataridosFeladatok.Allapot = KodTarak.FELADAT_ALLAPOT.Lezart;
            erec_hataridosFeladatok.Updated.Allapot = true;

            erec_hataridosFeladatok.LezarasDatuma = DateTime.Now.ToString();
            erec_hataridosFeladatok.Updated.LezarasDatuma = true;

            result = this.Update(ExecParam, erec_hataridosFeladatok);

            if(result.IsError)
            {
                Logger.Error("Feladat update hiba", ExecParam, result);
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_HataridosFeladatok", "FeladatLezaras").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result Sztorno(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result resGet = this.Get(ExecParam);

            if (resGet.IsError)
            {
                Logger.Error("Feladat lekerese hiba", ExecParam, resGet);
                throw new ResultException(resGet);
            }

            EREC_HataridosFeladatok erec_hataridosFeladatok = (EREC_HataridosFeladatok)resGet.Record;

            ErrorDetails errorDetails;
            if (!Contentum.eRecord.BaseUtility.HataridosFeladatok.Sztornozhato(ExecParam, erec_hataridosFeladatok, out errorDetails))
            {
                Logger.Warn("A feladat nem sztornozhato", ExecParam);
                throw new ResultException(59001, errorDetails);
            }

            erec_hataridosFeladatok.Updated.SetValueAll(false);
            erec_hataridosFeladatok.Base.Updated.SetValueAll(false);
            erec_hataridosFeladatok.Base.Updated.Ver = true;

            erec_hataridosFeladatok.Allapot = KodTarak.FELADAT_ALLAPOT.Sztornozott;
            erec_hataridosFeladatok.Updated.Allapot = true;

            result = this.Update(ExecParam, erec_hataridosFeladatok);

            if (result.IsError)
            {
                Logger.Error("Feladat update hiba", ExecParam, result);
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_HataridosFeladatok", "FeladatLezaras").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result Atvetel(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result resGet = this.Get(ExecParam);

            if (resGet.IsError)
            {
                Logger.Error("Feladat lekerese hiba", ExecParam, resGet);
                throw new ResultException(resGet);
            }

            EREC_HataridosFeladatok erec_hataridosFeladatok = (EREC_HataridosFeladatok)resGet.Record;

            ErrorDetails errorDetails;
            if (!Contentum.eRecord.BaseUtility.HataridosFeladatok.Atveheto(ExecParam, erec_hataridosFeladatok, out errorDetails))
            {
                Logger.Warn("A feladat nem atveheto", ExecParam);
                throw new ResultException(59003, errorDetails);
            }

            erec_hataridosFeladatok.Updated.SetValueAll(false);
            erec_hataridosFeladatok.Base.Updated.SetValueAll(false);
            erec_hataridosFeladatok.Base.Updated.Ver = true;

            erec_hataridosFeladatok.Felhasznalo_Id_Felelos = ExecParam.Felhasznalo_Id;
            erec_hataridosFeladatok.Updated.Felhasznalo_Id_Felelos = true;

            result = this.Update(ExecParam, erec_hataridosFeladatok);

            if (result.IsError)
            {
                Logger.Error("Feladat update hiba", ExecParam, result);
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_HataridosFeladatok", "FeladatAtvetel").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetCount(ExecParam ExecParam, string ObjektumId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            Arguments args = new Arguments();
            args.Add(new Argument("Obejktum azonos�t�", ObjektumId, ArgumentTypes.Guid));
            args.ValidateArguments();

            result = sp.GetCount(ExecParam, ObjektumId);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetAllReszfeladat(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.GetAllReszfeladat(ExecParam);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result Athelyezes(ExecParam ExecParam, string athelyezendoFeladatId, string celFeladatId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Arguments args = new Arguments();
            args.Add(new Argument("�thelyezend� feladat azonos�t�", athelyezendoFeladatId, ArgumentTypes.Guid));
            args.ValidateArguments();

            ExecParam xpm = ExecParam.Clone();
            xpm.Record_Id = athelyezendoFeladatId;
            Result res = this.Get(xpm);

            if (res.IsError)
            {
                throw new ResultException(res);
            }

            EREC_HataridosFeladatok athelyezendoFeladat = (EREC_HataridosFeladatok)res.Record;
            EREC_HataridosFeladatok celFeladat = null;

            if (!String.IsNullOrEmpty(celFeladatId))
            {
                xpm.Record_Id = celFeladatId;
                res = this.Get(xpm);

                if (res.IsError)
                {
                    throw new ResultException(res);
                }

                celFeladat = (EREC_HataridosFeladatok)res.Record;
            }

            ErrorDetails errorDetails;
            if(!Contentum.eRecord.BaseUtility.HataridosFeladatok.Athelyezheto(xpm,athelyezendoFeladat,celFeladat,out errorDetails))
            {
                //nem helyezhet� �t
                throw new ResultException(59007, errorDetails);
            }

            athelyezendoFeladat.Updated.SetValueAll(false);
            athelyezendoFeladat.Base.Updated.SetValueAll(false);
            athelyezendoFeladat.Base.Updated.Ver = true;

            athelyezendoFeladat.HataridosFeladat_Id = String.IsNullOrEmpty(celFeladatId) ? String.Empty : celFeladatId;
            athelyezendoFeladat.Updated.HataridosFeladat_Id = true;

            xpm.Record_Id = athelyezendoFeladatId;

            result = this.Update(xpm, athelyezendoFeladat);

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, athelyezendoFeladatId, "EREC_HataridosFeladatok", "FeladatAthelyezes").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    #region SegedFv

    public void SimpleInsertTomeges(ExecParam ExecParam, EREC_HataridosFeladatok Record, string Obj_Type, string[] Objektum_Id_Array)
    {
        if (Record != null)
        {
            List<Objektum> listObjektumok = new List<Objektum>(Objektum_Id_Array.Length);
            foreach (string objektumId in Objektum_Id_Array)
            {
                Objektum objektum = new Objektum();
                objektum.Id = objektumId;
                objektum.Type = Obj_Type;
                listObjektumok.Add(objektum);
            }
            Result result = this.Insert_Tomeges(ExecParam.Clone(), Record, listObjektumok);
            if (result.IsError)
            {
                throw new ResultException(result);
            }
        }
    }

    static bool KellIrattarListaSzures(ExecParam exexParam, string startup)
    {
        //Rendszerparam�ter
        if (Rendszerparameterek.GetBoolean(exexParam, "IRATTAROZAS_LISTA_SZURES_IRATTAROS_ALTAL", false))
        {
            //Iratt�ros
            return IsIrattarosFelhasznalo(exexParam, startup);
        }
        else
        {
            return false;
        }
    }

    static bool IsIrattarosFelhasznalo(ExecParam execParam, string startup)
    {
        string funkcioKod = "";
        if (startup == "FromAtmenetiIrattar")
        {
            funkcioKod = "AtmenetiIrattarAtvetel";
        }
        else if (startup == "FromKozpontiIrattar")
        {
            funkcioKod = "KozpontiIrattarAtvetel";
        }
        else
        {
            return false;
        }

        return FunctionRights.HasFunctionRight(execParam, funkcioKod);
    }

    #endregion


}