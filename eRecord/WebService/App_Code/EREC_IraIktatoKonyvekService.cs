﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using System.Data;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IraIktatoKonyvekService : System.Web.Services.WebService
{
    #region Generáltból átvettek
    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_IraIktatoKonyvek Record)
    /// Egy rekord felvétele a EREC_IraIktatoKonyvek táblába. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIktatoKonyvek))]
    public Result Insert(ExecParam ExecParam, EREC_IraIktatoKonyvek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            #region Azonosito elõállítása

            string utolsoAzonosito = "";
            if (!String.IsNullOrEmpty(Record.IktatoErkezteto))
            {
                utolsoAzonosito = GetLastAzonositoByIktatoErkezteto(ExecParam, Record.IktatoErkezteto);
            }

            //LZS - BLG 2199 - Az IKTATOKONYV_AZONOSITO_HOSSZ paraméternek megadott érték hosszúságúra egészítse ki az azonosítót
            // a kapott szám kiegészítése a megfelelõ formátumra: (0-ák beszúrása az elejére)
            int length = 0;
            length = Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.IKTATOKONYV_AZONOSITO_HOSSZ);

            string ujAzonosito = CreateNextAzonosito(utolsoAzonosito, length);

            Record.Azonosito = ujAzonosito;
            Record.Updated.Azonosito = true;

            #endregion

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Ha KozpontiIktatas be volt állítva, hozzáadjuk az Ide iktatókhoz:

            if (Record.KozpontiIktatasJelzo == "1")
            {
                Logger.Info("KözpontiIktatás jelzõ beállítva; központi iktató szervezet lekérése Start", ExecParam);

                KRT_KodTarakService service_kodtarak = eAdminService.ServiceFactory.GetKRT_KodTarakService();
                Result result_kt = service_kodtarak.GetByKodcsoportKod(ExecParam, KodTarak.SPEC_SZERVEK._kodcsoportKod, KodTarak.SPEC_SZERVEK.KOZPONTIIKTATO);
                if (!String.IsNullOrEmpty(result_kt.ErrorCode))
                {
                    // hiba:
                    // Központi iktató csoport lekérése sikertelen
                    Logger.Error("Központi iktató csoport lekérése sikertelen", ExecParam);
                    throw new ResultException(52571);
                }

                KRT_KodTarak kt_kozpontiIktato = (KRT_KodTarak)result_kt.Record;

                string kozpontiIktatoId = kt_kozpontiIktato.Obj_Id;
                if (String.IsNullOrEmpty(kozpontiIktatoId))
                {
                    // hiba:
                    Logger.Error("Nincs kitöltve a központi iktató kódtár obj_id mezõje!!", ExecParam);
                    throw new ResultException(52571);
                }

                EREC_Irat_IktatokonyveiService service_IratIktatokonyvei = new EREC_Irat_IktatokonyveiService(this.dataContext);
                EREC_Irat_Iktatokonyvei obj_EREC_Irat_Iktatokonyvei = new EREC_Irat_Iktatokonyvei();

                obj_EREC_Irat_Iktatokonyvei.Csoport_Id_Iktathat = kozpontiIktatoId;
                obj_EREC_Irat_Iktatokonyvei.Updated.Csoport_Id_Iktathat = true;

                obj_EREC_Irat_Iktatokonyvei.IraIktatokonyv_Id = result.Uid;
                obj_EREC_Irat_Iktatokonyvei.Updated.IraIktatokonyv_Id = true;

                ExecParam execParam_iratIkt = ExecParam.Clone();

                Result result_iratIkt = service_IratIktatokonyvei.Insert(execParam_iratIkt, obj_EREC_Irat_Iktatokonyvei);
                if (!String.IsNullOrEmpty(result_iratIkt.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_iratIkt);
                }
            }

            #endregion


            #region ACL kezelés

            string Uid = result.Uid;
            RightsService rs = new RightsService(this.dataContext);
            Result aclResult = rs.AddScopeIdToJogtargy(ExecParam, Uid, "EREC_IraIktatokonyvek", null, 'X', ExecParam.Felhasznalo_Id);
            if (!string.IsNullOrEmpty(aclResult.ErrorCode))
            {
                throw new ResultException(aclResult);
            }

            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                string FunkcioKod = "";
                if (Record.IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
                {
                    FunkcioKod = "IktatoKonyvNew";
                }
                else if (Record.IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Postakonyv)
                {
                    FunkcioKod = "PostaKonyvNew";
                }
                else
                {
                    FunkcioKod = "ErkeztetoKonyvNew";
                }

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_IraIktatokonyvek", FunkcioKod).Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Update(ExecParam ExecParam, EREC_IraIktatoKonyvek Record)
    /// Az EREC_IraIktatoKonyvek tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenõ paraméter. Az ExecParam adatai a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param>
    /// <param name="Record">A módosított adatokat a Record paraméter tartalmazza. </param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. Az ExecParam adatai a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIktatoKonyvek))]
    public Result Update(ExecParam ExecParam, EREC_IraIktatoKonyvek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                string FunkcioKod = "";
                if (Record.IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
                {
                    FunkcioKod = "IktatoKonyvModify";
                }
                else if (Record.IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Postakonyv)
                {
                    FunkcioKod = "PostaKonyvModify";
                }
                else
                {
                    FunkcioKod = "ErkeztetoKonyvModify";
                }

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_IraIktatokonyvek", FunkcioKod).Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az EREC_IraIktatoKonyvek tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának logikai törlése (érvényesség vege beállítás). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenõ paraméter. Az ExecParam adatai a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <returns>Hibaüzenet, eredményhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának logikai törlése (érvényesség vege beállítás). Az ExecParam. adatai a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIktatoKonyvek))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                EREC_IraIktatoKonyvek Record = sp.Get(ExecParam).Record as EREC_IraIktatoKonyvek;
                string FunkcioKod = "";
                if (Record.IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
                {
                    FunkcioKod = "IktatoKonyvInvalidate";
                }
                else if (Record.IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Postakonyv)
                {
                    FunkcioKod = "PostaKonyvInvalidate";
                }
                else
                {
                    FunkcioKod = "ErkeztetoKonyvInvalidate";
                }

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_IraIktatokonyvek", FunkcioKod).Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion

    /// <summary>
    /// Több egymáskoz kapcsolt táblára vonatkozó keresés eredményhalmazának elkérése. A szûrési feltételeket paraméter tartalmazza (*Search). 
    /// (A GetAll kiterjesztése join mûvelettel összekapcsolt táblákra). Az ExecParam.Record_Id nem használt, a további adatok a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_IraIktatoKonyvekSearch">A szûrési feltételeket tartalmazza</param>
    /// <returns>Result.Ds DataSet objektum kerül feltöltésre a lekérdezés eredményével</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIktatoKonyvekSearch))]
    public Result GetAllWithExtensionAndJogosultsag(ExecParam ExecParam, EREC_IraIktatoKonyvekSearch _EREC_IraIktatoKonyvekSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtension(ExecParam, _EREC_IraIktatoKonyvekSearch, Jogosultak);

            #region Eseménynaplózás
            if (!Search.IsIdentical(_EREC_IraIktatoKonyvekSearch, new EREC_IraIktatoKonyvekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                string FunkcioKod = "";
                if (_EREC_IraIktatoKonyvekSearch.IktatoErkezteto.Value == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
                {
                    FunkcioKod = "IktatoKonyvekList";
                }
                else if (_EREC_IraIktatoKonyvekSearch.IktatoErkezteto.Value == Contentum.eUtility.Constants.IktatoErkezteto.Postakonyv)
                {
                    FunkcioKod = "PostaKonyvekList";
                }
                else
                {
                    FunkcioKod = "ErkeztetoKonyvekList";
                }

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, null, "EREC_IraIktatokonyvek", FunkcioKod).Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_IraIktatoKonyvekSearch);

                    #region where feltétel összeállítása
                    if (!string.IsNullOrEmpty(_EREC_IraIktatoKonyvekSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _EREC_IraIktatoKonyvekSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Több egymáskoz kapcsolt táblára vonatkozó keresés eredményhalmazának elkérése. A szûrési feltételeket paraméter tartalmazza (*Search). 
    /// (A GetAll kiterjesztése join mûvelettel összekapcsolt táblákra). Az ExecParam.Record_Id nem használt, a további adatok a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_IraIktatoKonyvekSearch">A szûrési feltételeket tartalmazza</param>
    /// <returns>Result.Ds DataSet objektum kerül feltöltésre a lekérdezés eredményével</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIktatoKonyvekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraIktatoKonyvekSearch _EREC_IraIktatoKonyvekSearch)
    {
        return GetAllWithExtensionAndJogosultsag(ExecParam, _EREC_IraIktatoKonyvekSearch, false);
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIktatoKonyvekSearch))]
    public Result GetWithRightCheck(ExecParam execParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            EREC_IraIktatoKonyvek Record = result.Record as EREC_IraIktatoKonyvek;
            string FunkcioKod = "";
            if (Record.IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
            {
                FunkcioKod = "IktatoKonyvView";
            }
            else if (Record.IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Postakonyv)
            {
                FunkcioKod = "PostaKonyvView";
            }
            else
            {
                FunkcioKod = "ErkeztetoKonyvView";
            }

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, string.IsNullOrEmpty(dataContext.Tranz_Id) ? Guid.NewGuid().ToString() : dataContext.Tranz_Id, (result.Record as EREC_IraIktatoKonyvek).Id, "EREC_IraIktatokonyvek", FunkcioKod).Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIktatoKonyvekSearch))]
    public Result GetAllWithIktathat(ExecParam ExecParam, EREC_IraIktatoKonyvekSearch _EREC_IraIktatoKonyvekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithIktathat(ExecParam, _EREC_IraIktatoKonyvekSearch);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIktatoKonyvekSearch))]
    public Result GetAllWithCount(ExecParam ExecParam, EREC_IraIktatoKonyvekSearch _EREC_IraIktatoKonyvekSearch, string _KezdDate, string _VegeDate)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithCount(ExecParam, _EREC_IraIktatoKonyvekSearch, _KezdDate, _VegeDate);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIktatoKonyvekSearch))]
    public Result GetAllWithIktathatByIrattariTetel(ExecParam ExecParam, EREC_IraIktatoKonyvekSearch _EREC_IraIktatoKonyvekSearch, string _EREC_IraIrattariTetel_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithIktathatByIrattariTetel(ExecParam, _EREC_IraIktatoKonyvekSearch, _EREC_IraIrattariTetel_Id);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Iktatókönyvek tömeges lezárása, illetve lezárt iktatókönyvek helyett újak létrehozása a következõ évre.
    /// Az IraIktatokonyvek_Lezar_Id_Array a lezárandó iktatókönyvek azonosítóit, az IraIktatokonyvek_Masol_Id_Array pedig
    /// azon lezárt, vagy épp most lezárandó iktatókönyvek azonosítóit tartalmazza, melyeknek megfelelõ paraméterekkel
    /// a következõ évre új iktatókönyveket kell létrehozni. A naplózáshoz szükséges információkat az ExecParam tartalmazza.
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmaz.</param>
    /// <param name="IraIktatokonyvek_Lezar_Id_Array">A lezárandó iktatókönyvek azonosítói</param>
    /// <param name="IraIktatokonyvek_Masol_Id_Array">A lezárt iktatókönyvek azonosítói, melyek alapján a következõ évre létre kell hozni egy-egy új iktatókönyvet, azonos paraméterekkel.</param>
    /// <param name="IktatoErkezteto">Iktatókönyvre vagy érkeztetõkönyvre szûrés, NULL esetén nincs vizsgálat</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.</returns>
    [WebMethod(Description = "Iktatókönyvek tömeges lezárása, illetve lezárt iktatókönyvek helyett újak létrehozása a következõ évre. Az IraIktatokonyvek_Lezar_Id_Array a lezárandó iktatókönyvek azonosítóit, az IraIktatokonyvek_Masol_Id_Array pedig azon lezárt, vagy épp most lezárandó iktatókönyvek azonosítóit tartalmazza, melyeknek megfelelõ paraméterekkel a következõ évre új iktatókönyveket kell létrehozni. A naplózáshoz szükséges információkat az ExecParam tartalmazza.")]
    public Result LezarasMasolas_Tomeges(ExecParam execParam, String[] IraIktatokonyvek_Lezar_Id_Array, String[] IraIktatokonyvek_Masol_Id_Array, String IktatoErkezteto, bool? Auto)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
            {
                // hiba
                throw new ResultException(52906);// nincsenek megadva a naplózáshoz szükséges adatok
            }
            else if ((IraIktatokonyvek_Lezar_Id_Array == null || IraIktatokonyvek_Lezar_Id_Array.Length == 0)
                && (IraIktatokonyvek_Masol_Id_Array == null || IraIktatokonyvek_Masol_Id_Array.Length == 0))
            {
                // hiba - nincs megadva iktató-/érkeztetõkönyv
                if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
                {
                    throw new ResultException(52940);
                }
                else if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto)
                {
                    throw new ResultException(52920);
                }
                else
                {
                    throw new ResultException(52900);
                }
            }
            else
            {
                string iktatokonyvIds_lezar = "";
                if ((IraIktatokonyvek_Lezar_Id_Array != null || IraIktatokonyvek_Lezar_Id_Array.Length > 0))
                {
                    iktatokonyvIds_lezar = "'" + String.Join("','", IraIktatokonyvek_Lezar_Id_Array) + "'";
                }

                string iktatokonyvIds_masol = "";
                if ((IraIktatokonyvek_Masol_Id_Array != null || IraIktatokonyvek_Masol_Id_Array.Length > 0))
                {
                    iktatokonyvIds_masol = "'" + String.Join("','", IraIktatokonyvek_Masol_Id_Array) + "'";
                }

                result = sp.LezarasMasolas_Tomeges(execParam, iktatokonyvIds_lezar, iktatokonyvIds_masol, IktatoErkezteto, Auto);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }

                #region Eseménynaplózás

                string funkcioKod_Lezaras = "IktatoKonyvLezaras";
                if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto)
                {
                    funkcioKod_Lezaras = "ErkeztetoKonyvLezaras";
                }

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, iktatokonyvIds_lezar, funkcioKod_Lezaras);
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_IraIktatoKonyvek::LezarasMasolas_Tomeges: ", execParam, eventLogResult);
                #endregion

            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    #region Jelenleg nem kiajánlott szolgáltatások (szükség esetén bekapcsolhatók)
    /// <summary>
    /// Lezárt iktatókönyv helyett új létrehozása a következõ évre, azonos paraméterekkel.
    /// Az execParam.Record_Id a lezárt iktatókönyv azonosítóját tartalmazza, melynek megfelelõ paraméterekkel
    /// a következõ évre új iktatókönyvet kell létrehozni. A naplózáshoz szükséges információkat az execParam tartalmazza.
    /// </summary>
    /// <param name="execParam">A Record_Id a régi, lezárt iktatókönyv azonosítója, mely alapján a következõ évre létre kell hozni egy új iktatókönyvet, azonos paraméterekkel. Továbbá a végrehajtó felhasználót és egyéb általános paramétereket tartalmaz.</param>
    /// <param name="IktatoErkezteto">Iktatókönyvre vagy érkeztetõkönyvre szûrés, NULL esetén nincs vizsgálat</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.</returns>
    [WebMethod(Description = "Lezárt iktatókönyv helyett új létrehozása a következõ évre, azonos paraméterekkel. Az execParam.Record_Id a lezárt iktatókönyv azonosítóit tartalmazza, melynek megfelelõ paraméterekkel a következõ évre új iktatókönyvet kell létrehozni. A naplózáshoz szükséges információkat az execParam tartalmazza.")]
    public Result MasolKovetkezoEvre(ExecParam execParam, String IktatoErkezteto)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
            {
                // hiba
                throw new ResultException(52906);// nincsenek megadva a naplózáshoz szükséges adatok
            }
            else if (String.IsNullOrEmpty(execParam.Record_Id))
            {
                // hiba - nincs megadva iktató-/érkeztetõkönyv
                if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
                {
                    throw new ResultException(52940);
                }
                else if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto)
                {
                    throw new ResultException(52920);
                }
                else
                {
                    throw new ResultException(52900);
                }
            }
            else
            {
                result = sp.MasolKovetkezoEvre(execParam, IktatoErkezteto);
                if (!string.IsNullOrEmpty(result.ErrorCode))
                    throw new ResultException(result);

                #region Eseménynaplózás
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id, result.Uid, "EREC_IraIktatokonyvek", "Masolas").Record;
                eventLogService.Insert(execParam, eventLogRecord);
                #endregion

            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }


    /// <summary>
    /// /// LZS - BLG_2549
    /// Iktatókönyvek, érkeztetőkönyvek és postakönyvek automatikus lezárása és új létrehozása a következõ évre, azonos paraméterekkel.
    /// Megvizsgálja, hogy van-e zárandó iktatókönyv (Iktatóköny, Postakönyv, Érkeztetőköny), és ha nincs még hozzá létrehozva következő évi,
    /// és az újranyitandó 1, akkor létrehozza a MasolKovetkezoEvre webservice segítségével az új iktatókönyvet.
    /// </summary>
    /// <param name="execParam">String FelhasznaloId, String FelhasznaloSzervezetId</param>
    /// <param name="IktatoErkezteto">Iktatókönyvre vagy érkeztetõkönyvre szûrés, NULL esetén nincs vizsgálat</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.</returns>
    [WebMethod(Description = "")]
    public Result IraIktatoKonyvekProxyService(String FelhasznaloId, String FelhasznaloSzervezetId, String IktatoErkezteto, bool Auto)
    {
        List<EREC_IraIktatoKonyvek> iktatoKonyvek_Lezar = new List<EREC_IraIktatoKonyvek>();
        List<EREC_IraIktatoKonyvek> iktatoKonyvek_Masol = new List<EREC_IraIktatoKonyvek>();
        List<string> Item_IraIktatoKonyvekIds_Lezaras = new List<string>();
        List<string> Item_IraIktatoKonyvekIds_Masolas = new List<string>();
        string IktatoErkezteto_Param = Constants.IktatoErkezteto.Iktato;

        switch (IktatoErkezteto)
        {
            case "I":
                IktatoErkezteto_Param = Constants.IktatoErkezteto.Iktato;
                break;

            case "E":
                IktatoErkezteto_Param = Constants.IktatoErkezteto.Erkezteto;
                break;

            case "P":
                IktatoErkezteto_Param = Constants.IktatoErkezteto.Postakonyv;
                break;

            case "S":
                IktatoErkezteto_Param = Constants.IktatoErkezteto.IratkezelesiSegedlet;
                break;

            default:
                IktatoErkezteto_Param = Constants.IktatoErkezteto.Iktato;
                break;
        }

        ExecParam iktatoKonyvek_execParam = new ExecParam() { Felhasznalo_Id = FelhasznaloId, FelhasznaloSzervezet_Id = FelhasznaloSzervezetId };
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(iktatoKonyvek_execParam, Context, GetType());
        Result resultIktatoKonyvek = new Result();
        Result resultIktatoKonyvekNextYear = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (iktatoKonyvek_execParam == null || String.IsNullOrEmpty(iktatoKonyvek_execParam.Felhasznalo_Id))
            {
                // hiba
                throw new ResultException(52906);// nincsenek megadva a naplózáshoz szükséges adatok
            }
            else
            {
                EREC_IraIktatoKonyvekSearch iktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
                iktatoKonyvekSearch.IktatoErkezteto.Filter(IktatoErkezteto_Param, Query.Operators.equals);
                iktatoKonyvekSearch.LezarasDatuma.Filter(DateTime.Today.AddDays(1).ToString(), Query.Operators.lessorequal);
                iktatoKonyvekSearch.Statusz.Filter("1", Query.Operators.equals);

                resultIktatoKonyvek = GetAllWithExtension(iktatoKonyvek_execParam, iktatoKonyvekSearch);
                if (!string.IsNullOrEmpty(resultIktatoKonyvek.ErrorCode))
                {
                    throw new ResultException(resultIktatoKonyvek);
                }
                else
                {
                    foreach (DataRow row in resultIktatoKonyvek.Ds.Tables[0].Rows)
                    {
                        EREC_IraIktatoKonyvek iktatoKonyvItem = new EREC_IraIktatoKonyvek()
                        {
                            Nev = row["Nev"].ToString(),
                            MegkulJelzes = row["MegkulJelzes"].ToString(),
                            Iktatohely = row["Iktatohely"].ToString(),
                            Ev = row["Ev"].ToString(),
                            Id = row["Id"].ToString()
                        };

                        Item_IraIktatoKonyvekIds_Lezaras.Add(iktatoKonyvItem.Id);

                        iktatoKonyvek_Lezar.Add(iktatoKonyvItem);
                    }
                }

                foreach (EREC_IraIktatoKonyvek entry in iktatoKonyvek_Lezar)
                {
                    EREC_IraIktatoKonyvekSearch iktatoKonyvekNextYearSearch = new EREC_IraIktatoKonyvekSearch();

                    iktatoKonyvekNextYearSearch.Iktatohely.FilterIfNotEmpty(entry.Iktatohely, Query.Operators.equals);
                    iktatoKonyvekNextYearSearch.Ev.Filter((int.Parse(entry.Ev) + 1).ToString(), Query.Operators.equals);
                    iktatoKonyvekNextYearSearch.Nev.Filter(entry.Nev, Query.Operators.equals);
                    iktatoKonyvekNextYearSearch.MegkulJelzes.FilterIfNotEmpty(entry.MegkulJelzes, Query.Operators.equals);

                    resultIktatoKonyvekNextYear = GetAllWithExtension(iktatoKonyvek_execParam, iktatoKonyvekNextYearSearch);
                    resultIktatoKonyvekNextYear.CheckError();

                    bool hasNextYearIktatoKonyv = resultIktatoKonyvekNextYear.HasData();
                    bool isCurrentIktatokonyvUjranyitando = entry.Ujranyitando == "1";

                    if (!hasNextYearIktatoKonyv && isCurrentIktatokonyvUjranyitando)
                    {
                       iktatoKonyvek_Masol.Add(entry);
                       Item_IraIktatoKonyvekIds_Masolas.Add(entry.Id);
                    }

                    #region Eseménynaplózás
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(iktatoKonyvek_execParam, dataContext.Tranz_Id, resultIktatoKonyvekNextYear.Uid, "EREC_IraIktatokonyvek", "Masolas").Record;
                    eventLogService.Insert(iktatoKonyvek_execParam, eventLogRecord);
                    #endregion

                }

                // Lezárás

                String[] IraIktatoKonyvekIds_Lezaras = Item_IraIktatoKonyvekIds_Lezaras.ToArray();
                String[] IraIktatoKonyvekIds_Masolas = Item_IraIktatoKonyvekIds_Masolas.ToArray();

                ExecParam execParam = new ExecParam() { Felhasznalo_Id = FelhasznaloId, FelhasznaloSzervezet_Id = FelhasznaloSzervezetId };

                Result resultLezarasMasolas = LezarasMasolas_Tomeges(execParam, IraIktatoKonyvekIds_Lezaras, IraIktatoKonyvekIds_Masolas, IktatoErkezteto_Param, Auto);

                if (!resultLezarasMasolas.IsError)
                {
                    //CERTOP: Iktatókönyv lezárásánál az irattári tételeket menteni hozzá
                    Contentum.eRecord.BaseUtility.IrattariTetelekExport _IrattariTetelekExport = new Contentum.eRecord.BaseUtility.IrattariTetelekExport(execParam);
                    Result resultExport = _IrattariTetelekExport.Export(IraIktatoKonyvekIds_Lezaras);

                    if (resultExport.IsError)
                    {
                        //Loggolás:
                        Logger.Error("Hiba az irattári tételek exportálásánál.", execParam, resultExport);
                    }
                    else
                    {

                        string alairasStoreInited = "";

                        Result resultSign = Contentum.eUtility.AlairasokUtility.SignAndSavetoDocStore(null, execParam, IraIktatoKonyvekIds_Lezaras, out alairasStoreInited, true);

                        if (resultSign.IsError || !String.IsNullOrEmpty(alairasStoreInited))
                        {
                            if (resultSign.IsError)
                            {
                                //TODO: Loggolás
                                Logger.Error("Hiba az iktató/érkeztetőkönyv aláírása során.", execParam, resultSign);
                            }
                            else
                            {
                                Result fakeResult = new Result();
                                fakeResult.ErrorMessage = "Failed to initialize certificate store: " + alairasStoreInited;
                                //Loggolás
                                Logger.Error("Failed to initialize certificate store", execParam, fakeResult);

                            }
                        }
                        else
                        {
                            //Sikeres aláírás
                            Logger.Info("Sikeres iktató/érkeztetőkönyv aláírás.");
                        }
                    }
                }
                else
                {
                    //Loggolás
                    Logger.Error("Hiba az iktató/érkeztetőkönyv lezárása/másolása során.", execParam, resultLezarasMasolas);
                }

                // COMMIT
                dataContext.CommitIfRequired(isTransactionBeginHere);
            }
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            resultIktatoKonyvek = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(iktatoKonyvek_execParam, resultIktatoKonyvek);
        return resultIktatoKonyvek;

    }


    /// <summary>
    /// Iktatókönyv lezárása.
    /// Az execParam.Record_Id a lezárandó iktatókönyv azonosítója. A naplózáshoz szükséges információkat az execParam tartalmazza.
    /// </summary>
    /// <param name="execParam">A Record_Id a lezárandó iktatókönyv Id-ja. Ezen felül a végrehajtó felhasználót és egyéb általános paramétereket tartalmaz.</param>
    /// <param name="IktatoErkezteto">Iktatókönyvre vagy érkeztetõkönyvre szûrés, NULL esetén nincs vizsgálat</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.</returns>
    [WebMethod(Description = "Iktatókönyv lezárása. Az execParam.Record_Id a lezárandó iktatókönyv azonosítója. A naplózáshoz szükséges információkat az execParam tartalmazza.")]
    public Result Lezaras(ExecParam execParam, String IktatoErkezteto)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            // Paraméterek ellenõrzése:
            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
            {
                // hiba
                throw new ResultException(52906);// nincsenek megadva a naplózáshoz szükséges adatok
            }
            else if (String.IsNullOrEmpty(execParam.Record_Id))
            {
                // hiba: nincs megadva iktatókönyv
                if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
                {
                    throw new ResultException(52940);
                }
                else if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto)
                {
                    throw new ResultException(52920);
                }
                else
                {
                    throw new ResultException(52900);
                }
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Iktatókönyv lekérése:
            EREC_IraIktatoKonyvekService erec_IraIktatoKonyvekService = new EREC_IraIktatoKonyvekService(this.dataContext);

            Result result_iktatokonyvGet = erec_IraIktatoKonyvekService.Get(execParam);
            if (!String.IsNullOrEmpty(result_iktatokonyvGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_iktatokonyvGet);
            }
            else
            {
                if (result_iktatokonyvGet.Record == null)
                {
                    // hiba: az iktatókönyv lekérése az adatbázisból sikertelen
                    if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
                    {
                        throw new ResultException(52944);
                    }
                    else if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto)
                    {
                        throw new ResultException(52924);
                    }
                    else
                    {
                        throw new ResultException(52904);
                    }
                }

                EREC_IraIktatoKonyvek erec_IraIktatoKonyv = (EREC_IraIktatoKonyvek)result_iktatokonyvGet.Record;

                if (!String.IsNullOrEmpty(IktatoErkezteto) && erec_IraIktatoKonyv.IktatoErkezteto != IktatoErkezteto)
                {
                    // hiba: nem megfelelõ típus (nem ikgtatókönyv/nem érkeztetõkönyv)
                    if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
                    {
                        throw new ResultException(52946);
                    }
                    else if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto)
                    {
                        throw new ResultException(52926);
                    }
                }

                bool Lezarhato =
                    Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezarhato(execParam, erec_IraIktatoKonyv);

                if (!Lezarhato)
                {
                    // hiba: az iktatókönyv nem zárható le
                    if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
                    {
                        throw new ResultException(52945);
                    }
                    else if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto)
                    {
                        throw new ResultException(52925);
                    }
                    else
                    {
                        throw new ResultException(52905);
                    }
                }

                // IKTATOKONYV UPDATE:
                // mezõk módosíthatóságának letiltása:
                erec_IraIktatoKonyv.Updated.SetValueAll(false);
                erec_IraIktatoKonyv.Base.Updated.SetValueAll(false);
                erec_IraIktatoKonyv.Base.Updated.Ver = true;

                // Módosítani kell: LezarasDatuma
                erec_IraIktatoKonyv.LezarasDatuma = DateTime.Now.ToShortDateString(); ;
                erec_IraIktatoKonyv.Updated.LezarasDatuma = true;

                ExecParam execParam_iktatokonyvUpdate = execParam.Clone();
                execParam_iktatokonyvUpdate.Record_Id = erec_IraIktatoKonyv.Id;

                result = erec_IraIktatoKonyvekService.Update(execParam_iktatokonyvUpdate, erec_IraIktatoKonyv);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result);
                }
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                string funkcioKod_Lezaras = "IktatoKonyvLezaras";
                if (IktatoErkezteto == Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto)
                {
                    funkcioKod_Lezaras = "ErkeztetoKonyvLezaras";
                }

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_IraIktatoKonyvek", funkcioKod_Lezaras).Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #endregion Jelenleg nem kiajánlott szolgáltatások (szükség esetén bekapcsolhatók)

    #region Eredeti azonosító lekérési metódusok (kikommentezve, leváltva az összevont GetLastAzonositoByIktatoErkezteto-vel)
    //private string GetLastIktatoKonyvAzonosito(ExecParam execParam)
    //{
    //    EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();

    //    search.IktatoErkezteto.Value = Contentum.eUtility.Constants.IktatoErkezteto.Iktato;
    //    search.IktatoErkezteto.Operator = Query.Operators.equals;

    //    search.ErvKezd.Value = "";
    //    search.ErvKezd.Operator = "";

    //    search.ErvVege.Value = "";
    //    search.ErvVege.Operator = "";

    //    search.OrderBy = "LetrehozasIdo DESC,Azonosito DESC";
    //    search.TopRow = 1;

    //    Result result = this.GetAll(execParam, search);
    //    if (!String.IsNullOrEmpty(result.ErrorCode))
    //    {
    //        // hiba
    //        throw new ResultException(result);
    //    }
    //    string azonosito = "";

    //    if (result.Ds.Tables[0].Rows.Count != 0)
    //        azonosito = result.Ds.Tables[0].Rows[0]["Azonosito"].ToString();
    //    else azonosito = "00000";

    //    return azonosito;
    //}


    //private string GetLastErkeztetoKonyvAzonosito(ExecParam execParam)
    //{
    //    EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();

    //    search.IktatoErkezteto.Value = Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto;
    //    search.IktatoErkezteto.Operator = Query.Operators.equals;

    //    search.ErvKezd.Value = "";
    //    search.ErvKezd.Operator = "";

    //    search.ErvVege.Value = "";
    //    search.ErvVege.Operator = "";

    //    search.OrderBy = "LetrehozasIdo DESC,Azonosito DESC";
    //    search.TopRow = 1;

    //    Result result = this.GetAll(execParam, search);
    //    if (!String.IsNullOrEmpty(result.ErrorCode))
    //    {
    //        // hiba
    //        throw new ResultException(result);
    //    }

    //    string azonosito = "";
    //    if (result.Ds.Tables[0].Rows.Count != 0)
    //        azonosito = result.Ds.Tables[0].Rows[0]["Azonosito"].ToString();
    //    else azonosito = "00000";

    //    return azonosito;
    //}
    #endregion Eredeti azonosító lekérési metódusok (kikommentezve, leváltva az összevont GetLastAzonositoByIktatoErkezteto-vel)
    // CR3355 Fõnyilvántartó sorfolytonos azonosító képzés TODO
    private string GetLastAzonositoByIktatoErkezteto(ExecParam execParam, string IktatoErkezteto)
    {
        EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();
        // CR3355
        bool isTUK = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.TUK, false);
        if (!isTUK)
        {
            search.IktatoErkezteto.Value = IktatoErkezteto;
            search.IktatoErkezteto.Operator = Query.Operators.equals;
        }
        search.ErvKezd.Value = "";
        search.ErvKezd.Operator = "";

        search.ErvVege.Value = "";
        search.ErvVege.Operator = "";

        // BUG_4384
        //search.OrderBy = "Azonosito DESC";
        search.OrderBy = "cast(Azonosito as int) DESC";

        search.TopRow = 1;

        Result result = this.GetAll(execParam, search);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba
            throw new ResultException(result);
        }

        string azonosito = "";
        if (result.Ds.Tables[0].Rows.Count != 0)
            azonosito = result.Ds.Tables[0].Rows[0]["Azonosito"].ToString();
        else azonosito = "00000";

        return azonosito;
    }

    private string CreateNextAzonosito(string LastAzonosito, int length)
    {
        if (String.IsNullOrEmpty(LastAzonosito))
        {
            LastAzonosito = "00000";
        }

        try
        {
            int azonosito_old = Int32.Parse(LastAzonosito);

            int azonosito_new = azonosito_old + 1;

            string str_ujazonosito = azonosito_new.ToString();

            while (str_ujazonosito.Length < length)
            {
                str_ujazonosito = "0" + str_ujazonosito;
            }

            return str_ujazonosito;
        }
        catch
        {
            throw;
        }
    }

}