﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using System.Data;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using Contentum.eDocument.Service;
using System.Xml;
using System.IO;
using System.IO.Packaging;
using System.Collections;
using Contentum.eAdmin.Service;
using System.Linq;
using System.Net;
using System.Text;
using Contentum.eUIControls;
using AlairasokUtility = Contentum.eUtility.AlairasokUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Xml.Serialization.XmlInclude(typeof(ErkeztetesIktatasResult))]
public partial class EREC_IraIratokService : System.Web.Services.WebService
{
    #region Generáltból átvettek
    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_IraIratok Record)
    /// Egy rekord felvétele a EREC_IraIratok táblába. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result Insert(ExecParam ExecParam, EREC_IraIratok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);

            Record.Ugy_Fajtaja = ugyiratokService.GetUgyIratUgyFajta(ExecParam, Record.Ugyirat_Id);
            // nekrisz javítás develop-on
            Record.Updated.Ugy_Fajtaja = true;

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region ACL kezelés

            Result aclResult = result;
            RightsService rs = new RightsService(this.dataContext);
            aclResult = rs.AddScopeIdToJogtargy(ExecParam, result.Uid, "EREC_IraIratok", Record.UgyUgyIratDarab_Id, 'S', null);
            if (!string.IsNullOrEmpty(aclResult.ErrorCode))
            {
                throw new ResultException(aclResult);
            }

            #endregion
            SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
            #region ELSO INDULO ESETEN - UGYIRAT ADATOK FRISSITESE IRATEVAL
            sakkoraSvc.SetUgyHataridoSakkoraAlapjan(ExecParam.Clone(), result.Uid);
            #endregion ELSO INDULO ESETEN - UGYIRAT ADATOK FRISSITESE IRATEVAL

            #region UGY SAKKORA ALAP ALLAPOTBA TEHETO
            sakkoraSvc.SetUgySakkora(ExecParam.Clone(), ExecParam.Record_Id);
            #endregion UGY SAKKORA ALAP ALLAPOTBA TEHETO

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_IraIratok", "IraIratokNew").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Update(ExecParam ExecParam, EREC_IraIratok Record)
    /// Az EREC_IraIratok tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param>
    /// <param name="Record">A módosított adatokat a Record paraméter tartalmazza. </param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>
    [WebMethod(Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result Update(ExecParam ExecParam, EREC_IraIratok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Régi irat lekérése DB-ből

            Result result_iratGet = this.Get(ExecParam);
            EREC_IraIratok record_elozoVerzio = result_iratGet.Record as EREC_IraIratok;

            #endregion

            #region IratmetaDefId frissítése, ha szükséges

            bool isIrathierarchiaChanged = false;

            IratHierarchia iratHier = null;

            // ha változott az irattípus, vagy esetleg más ügyiratdarabba került át (és nem update-eljük az iratmetaDefId-t)
            if ((Record.Updated.Irattipus == true || Record.Updated.UgyUgyIratDarab_Id == true)
                 && Record.Updated.IratMetaDef_Id == false)
            {
                // Le kell kérni a régi adatokat, és ellenőrizni a tényleges változást:
                Result result_iratHier = this.GetIratHierarchia(ExecParam.Clone());
                if (!string.IsNullOrEmpty(result_iratHier.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_iratHier);
                }

                iratHier = (IratHierarchia)result_iratHier.Record;

                // ha az ügyiratdarab változott, akkor nem a régi ügyiratdarab kell:
                if (Record.Updated.UgyUgyIratDarab_Id == true)
                {
                    #region ÜgyiratDarab lekérése

                    if (!string.IsNullOrEmpty(Record.UgyUgyIratDarab_Id))
                    {
                        EREC_UgyUgyiratdarabokService service_ugyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);
                        ExecParam execParam_ugyiratDarabGet = ExecParam.Clone();
                        execParam_ugyiratDarabGet.Record_Id = Record.UgyUgyIratDarab_Id;

                        Result result_ugyiratDarabGet = service_ugyiratDarabok.Get(execParam_ugyiratDarabGet);
                        if (!String.IsNullOrEmpty(result_ugyiratDarabGet.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_ugyiratDarabGet);
                        }
                        // az iratHierarchiában az ügyiratdarab felülírása:
                        iratHier.UgyiratDarabObj = (EREC_UgyUgyiratdarabok)result_ugyiratDarabGet.Record;
                    }

                    #endregion
                }

                // van-e tényleges változás:
                if ((Record.Updated.Irattipus == true && Record.Irattipus != iratHier.IratObj.Irattipus)
                    || (Record.Updated.UgyUgyIratDarab_Id == true && Record.UgyUgyIratDarab_Id != iratHier.IratObj.UgyUgyIratDarab_Id))
                {

                    // IratMetaDefId meghatározása:
                    EREC_IratMetaDefinicioService service_iratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);

                    // ha nem az iratTípus változott, akkor a régi rekordból kell kiszedni az irattípust:
                    string iratTipus = (Record.Updated.Irattipus == true) ? Record.Irattipus : iratHier.IratObj.Irattipus;

                    Result result_iratMetaDef = service_iratMetaDef.GetIratMetaDefinicioByIrattipus(ExecParam.Clone()
                        , iratHier.UgyiratObj.IraIrattariTetel_Id, iratHier.UgyiratObj.UgyTipus, iratHier.UgyiratDarabObj.EljarasiSzakasz, iratTipus);
                    if (!String.IsNullOrEmpty(result_iratMetaDef.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_iratMetaDef);
                    }

                    EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_iratMetaDef.Record;
                    string iratMetaDefId = "";
                    string generaltTargy = "";
                    if (erec_IratMetaDefinicio != null)
                    {
                        iratMetaDefId = erec_IratMetaDefinicio.Id;
                        generaltTargy = erec_IratMetaDefinicio.GeneraltTargy;
                    }
                    Record.IratMetaDef_Id = iratMetaDefId;
                    Record.Updated.IratMetaDef_Id = true;

                    Record.GeneraltTargy = generaltTargy;
                    Record.Updated.GeneraltTargy = true;

                    // van változás
                    isIrathierarchiaChanged = true;
                }
            }

            #endregion

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Küldemény minősítés visszaírása
            // van-e tényleges változás:
            if (Record.Updated.Minosites == true)
            {
                if (iratHier == null)
                {
                    // Le kell kérni a régi adatokat, és ellenőrizni a tényleges változást:
                    Result result_iratHier = this.GetIratHierarchia(ExecParam.Clone());
                    if (result_iratHier.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_iratHier);
                    }

                    iratHier = (IratHierarchia)result_iratHier.Record;
                }

                if (!String.IsNullOrEmpty(iratHier.IratObj.KuldKuldemenyek_Id) && Record.Minosites != iratHier.IratObj.Minosites)
                {
                    EREC_KuldKuldemenyekService service_kuldemeny = new EREC_KuldKuldemenyekService(this.dataContext);
                    ExecParam execParam_kuld = ExecParam.Clone();
                    execParam_kuld.Record_Id = iratHier.IratObj.KuldKuldemenyek_Id;

                    Result result_kuldGet = service_kuldemeny.Get(execParam_kuld);

                    if (result_kuldGet.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_kuldGet);
                    }

                    EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result_kuldGet.Record;
                    erec_KuldKuldemenyek.Updated.SetValueAll(false);
                    erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);

                    erec_KuldKuldemenyek.Minosites = Record.Minosites;
                    erec_KuldKuldemenyek.Updated.Minosites = true;

                    erec_KuldKuldemenyek.Base.Updated.Ver = true;

                    Result result_kuldUpdate = service_kuldemeny.Update(execParam_kuld, erec_KuldKuldemenyek);

                    if (result_kuldUpdate.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_kuldUpdate);
                    }
                }
            }
            #endregion Küldemény minősítés visszaírása

            #region Metaadatok

            #region Metaadatok érvénytelenítése irat szinten
            // megnézzük, van-e érvénytelenné vált metadefiníció (pl. IratMetadefinicio_Id változás miatt)
            // és érvénytelenítjük az objektum tárgyszó rekordokat

            EREC_ObjektumTargyszavaiService service_ObjektumTargyszavai = new EREC_ObjektumTargyszavaiService(this.dataContext);
            ExecParam execParam_ObjektumTargyszavai = ExecParam.Clone();

            Logger.Debug("Metadatok automatikus érvénytelenítése irat szinten start.", execParam_ObjektumTargyszavai);

            Result result_ObjektumTargyszavai = service_ObjektumTargyszavai.InvalidateByDefinicioTipus(execParam_ObjektumTargyszavai
                , ExecParam.Record_Id
                , null
                , "EREC_IraIratok"
                , ""
                , false
                , true
                );

            if (!String.IsNullOrEmpty(result_ObjektumTargyszavai.ErrorCode))
            {
                Logger.Debug("Metaadatok automatikus érvénytelenítése irat szinten sikertelen: ", execParam_ObjektumTargyszavai, result_ObjektumTargyszavai);

                // hiba 
                throw new ResultException(result_ObjektumTargyszavai);
            }

            Logger.Debug("Metaadatok automatikus érvénytelenítése irat szinten sikeres.");

            #endregion Metaadatok érvénytelenítése irat szinten


            #region Metaadatok hozzárendelése irathoz
            // csak azokat rendeljük hozzá, melyekhez nem tartozik érték (Tipus = "0")!

            Logger.Debug("Metadatok automatikus hozzárendelése az irathoz az objektum metadefiníció alapján", execParam_ObjektumTargyszavai);

            result_ObjektumTargyszavai = service_ObjektumTargyszavai.AssignByDefinicioTipus(execParam_ObjektumTargyszavai
               , ExecParam.Record_Id
               , null
               , "EREC_IraIratok"
               , ""
               , false
               );

            if (!String.IsNullOrEmpty(result_ObjektumTargyszavai.ErrorCode))
            {
                Logger.Debug("Metaadatok automatikus hozzárendelése irathoz sikertelen: ", execParam_ObjektumTargyszavai, result_ObjektumTargyszavai);

                // hiba 
                throw new ResultException(result_ObjektumTargyszavai);
            }

            Logger.Debug("Metadatok automatikus hozzárendelése az irathoz az objektum metadefiníció alapján sikeres.");

            #endregion Metaadatok hozzárendelése irathoz

            #endregion Metaadatok

            #region AdathordozoTipusa mező változása esetén ügyirat Jelleg mező frissítése

            if (Record.Updated.AdathordozoTipusa == true
                && record_elozoVerzio.AdathordozoTipusa != Record.AdathordozoTipusa)
            {
                string ugyiratId = record_elozoVerzio.Ugyirat_Id;
                new EREC_UgyUgyiratokService(this.dataContext).UpdateUgyiratJellegEsUgyintezesModjaByIratokAndSzereltek(ExecParam, ugyiratId);
            }

            #endregion

            #region ELSO INDULO ESETEN - UGYIRAT ADATOK FRISSITESE IRATEVAL
            SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
            sakkoraSvc.SetUgyHataridoSakkoraAlapjan(ExecParam.Clone(), ExecParam.Record_Id);
            #endregion ELSO INDULO ESETEN - UGYIRAT ADATOK FRISSITESE IRATEVAL

            #region iktatott dokumentumba hivatkozási szám meta adatok bejegyzése - CR#3234

            if (Record.Updated.HivatkozasiSzam == true
               && record_elozoVerzio.HivatkozasiSzam != Record.HivatkozasiSzam)
            {

                Logger.Info("====== CR#3234 ======== innnen ");

                try
                {

                    EREC_CsatolmanyokService erec_CsatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);
                    EREC_CsatolmanyokSearch erec_csatolmanyokSearch = new EREC_CsatolmanyokSearch();

                    EREC_PldIratPeldanyokSearch erec_peldanySearch = new EREC_PldIratPeldanyokSearch();


                    #region csatolmanyok olvasasa

                    erec_csatolmanyokSearch.IraIrat_Id.Value = ExecParam.Record_Id;
                    erec_csatolmanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                    Result result_Csatolmanyok = erec_CsatolmanyokService.GetAll(ExecParam.Clone(), erec_csatolmanyokSearch);

                    if (String.IsNullOrEmpty(result_Csatolmanyok.ErrorCode))
                    {
                        #region  vonalkod lekerdezese az elso (!) iratbeldanybol

                        #region  property-k beallitasa

                        String propsXmlParameter = String.Format("<params>"
                                + "<krtdocument_id>{0}</krtdocument_id>"
                                + "<metaadatok>"
                                + "<metaadat internalName=\"edok_w_hivatkozasiszam\" value=\"{1}\" />"
                                + "</metaadatok>"
                                + "</params>"
                                , Convert.ToString(result_Csatolmanyok.Ds.Tables[0].Rows[0]["Dokumentum_Id"])
                                , Record.HivatkozasiSzam
                                );

                        DocumentService erec_documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
                        Result result_SetDocumentProperties = erec_documentService.SetDocumentPropertiesAndRevalidate(ExecParam.Clone(), propsXmlParameter);

                        if (String.IsNullOrEmpty(result_SetDocumentProperties.ErrorCode))
                        {
                            Logger.Error(String.Format("nem szakijuk meg a munkapeldany beiktatasa folyamatot !!!\n" +
                                "Hibaval tert vissza az erec_documentService.SetDocumentPropertiesAndRevalidate: {0} - {1}"
                                , result_SetDocumentProperties.ErrorCode
                                , result_SetDocumentProperties.ErrorMessage));
                        }

                        #endregion

                        #endregion

                    }
                    else
                    {
                        Logger.Error(String.Format("nem szakijuk meg a munkapeldany beiktatasa folyamatot !!!\n" +
                            "Hibaval tert vissza a erec_CsatolmanyokService.GetAllWithExtension: {0} - {1}"
                            , result_Csatolmanyok.ErrorCode
                            , result_Csatolmanyok.ErrorMessage));
                    }

                    #endregion

                }
                catch (Exception dsEx)
                {
                    // !!! nem szakijuk meg a munkapeldany beiktatasa folyamatot !!!
                    // !!! csak a hibat loggoljuk !!!
                    Logger.Error(String.Format("nem szakijuk meg a munkapeldany beiktatasa folyamatot !!!\nKivetel hiba az sps-es meta adatok bejegyzesekor: {0}\nSource: {1}\nStackTrace: {2}"
                        , dsEx.Message
                        , dsEx.Source
                        , dsEx.StackTrace));
                }

                Logger.Info("====== CR#3234 ======== idaig ");
            }

            #endregion

            #region SETUGYIRATSAKKORAALLAPOT
            sakkoraSvc.SetUgySakkora(ExecParam.Clone(), ExecParam.Record_Id);
            #endregion

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_IraIratok", "IraIratModify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion

    #region Get / GetAllWithExtension

    /// <summary>
    /// Több egymáskoz kapcsolt táblára vonatkozó keresés eredményhalmazának elkérése. A szűrési feltételeket paraméter tartalmazza (*Search). 
    /// (A GetAll kiterjesztése join művelettel összekapcsolt táblákra).
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_IraIratokSearch">A szűrési feltételeket tartalmazza</param>
    /// <returns>Result.Ds DataSet objektum kerül feltöltésre a lekérdezés eredményével</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraIratokSearch _EREC_IraIratokSearch)
    {
        return GetAllWithExtensionAndJogosultak(ExecParam, _EREC_IraIratokSearch, false);
    }

    /// <summary>
    /// Több egymáskoz kapcsolt táblára vonatkozó keresés eredményhalmazának elkérése. A szűrési feltételeket paraméter tartalmazza (*Search). 
    /// (A GetAll kiterjesztése join művelettel összekapcsolt táblákra).
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_IraIratokSearch">A szűrési feltételeket tartalmazza</param>
    /// <returns>Result.Ds DataSet objektum kerül feltöltésre a lekérdezés eredményével</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratokSearch))]
    public Result GetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_IraIratokSearch _EREC_IraIratokSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtension(ExecParam, _EREC_IraIratokSearch, Jogosultak);

            #region Eseménynaplózás
            if (isTransactionBeginHere && !Search.IsIdentical(_EREC_IraIratokSearch, new EREC_IraIratokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, null, "EREC_IraIratok", "IraIratokList").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_IraIratokSearch);

                    #region where feltétel összeállítása
                    if (!string.IsNullOrEmpty(_EREC_IraIratokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _EREC_IraIratokSearch.WhereByManual;
                    }

                    //#region FullTextSearch
                    //string strContainsTargyszo = String.Empty;
                    //string strContainsTargy = String.Empty;
                    //string strWhere = String.Empty;
                    ////if (!string.IsNullOrEmpty(_EREC_IraIratokSearch.Targy.Value))
                    ////{
                    ////    strContainsTargy = new FullTextSearch.SQLContainsCondition(_EREC_IraIratokSearch.Targy.Value).Normalized;
                    ////    strWhere += "contains( EREC_IraIratok.Targy , '" + strContainsTargy + "' )";
                    ////}
                    //if (_EREC_IraIratokSearch.fts_targyszavak != null && !string.IsNullOrEmpty(_EREC_IraIratokSearch.fts_targyszavak.Filter))
                    //{
                    //    strContainsTargyszo = new FullTextSearch.SQLContainsCondition(_EREC_IraIratokSearch.fts_targyszavak.Filter).Normalized;
                    //    if (!string.IsNullOrEmpty(strWhere))
                    //    {
                    //        strWhere += " and ";
                    //    }
                    //    strWhere += "contains( (EREC_ObjektumTargyszavai.Targyszo, EREC_ObjektumTargyszavai.Note, EREC_ObjektumTargyszavai.Ertek ), '" + strContainsTargyszo + "' )";
                    //}

                    //if (!string.IsNullOrEmpty(strWhere))
                    //{
                    //    if (!string.IsNullOrEmpty(query.Where))
                    //        query.Where += " and " + strWhere;
                    //    else
                    //        query.Where += strWhere;
                    //}
                    //#endregion

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Keresés iratokra jogosultságkezeléssel + tárgyszavak/metaadatok hozzáfűzése
    /// A szűrőfeltételeket a keresési objektumban kell megadni
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="_EREC_IraIratokSearch"></param>
    /// <param name="Jjogosultak"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratokSearch))]
    public Result GetAllWithExtensionAndMetaAndJogosultak(ExecParam ExecParam, EREC_IraIratokSearch _EREC_IraIratokSearch
        , string Obj_Id, String ObjMetaDefinicio_Id
        , String ColumnName, String[] ColumnValues, bool bColumnValuesExclude
        , String DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus
        , bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            // metaadatok lekérése
            EREC_ObjektumTargyszavaiService service_ot = new EREC_ObjektumTargyszavaiService(this.dataContext);
            ExecParam execParam_ot = ExecParam.Clone();

            EREC_ObjektumTargyszavaiSearch search_ot = new EREC_ObjektumTargyszavaiSearch();

            Result result_ot = service_ot.GetAllMetaByObjMetaDefinicio(execParam_ot
                , search_ot, Obj_Id, ObjMetaDefinicio_Id
                , Contentum.eUtility.Constants.TableNames.EREC_IraIratok
                , ColumnName, ColumnValues, bColumnValuesExclude
                , DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus);

            if (result_ot.IsError)
            {
                throw new ResultException(result_ot);
            }

            System.Collections.Generic.List<string> lstColumNames = new System.Collections.Generic.List<string>();
            foreach (System.Data.DataRow row in result_ot.Ds.Tables[0].Rows)
            {
                string Targyszo = row["Targyszo"].ToString();

                if (!String.IsNullOrEmpty(Targyszo) && !lstColumNames.Contains(String.Format("[{0}]", Targyszo)))
                {
                    lstColumNames.Add(String.Format("[{0}]", Targyszo));
                }
            }

            result = sp.GetAllWithExtensionAndMeta(ExecParam, _EREC_IraIratokSearch
                , String.Join(",", lstColumNames.ToArray()), Jogosultak);

            #region tárgyszó/metaadat oszlopok leíró adatainak hozzáadása
            if (!result.IsError)
            {
                System.Data.DataTable table = result_ot.Ds.Tables[0].Copy();
                table.TableName = Contentum.eUtility.Constants.TableNames.EREC_ObjektumTargyszavai;

                result.Ds.Tables.Add(table);
            }
            #endregion tárgyszó/metaadat oszlopok leíró adatainak hozzáadása



            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Keresés iratok mozgási eseményeire (ügyirat vagy első iratpéldány átadás/átvétel)
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="IratId"></param>
    /// <param name="_EREC_IraIratokSearch"></param>
    /// <returns></returns>
    [WebMethod(Description = "Keresés iratok mozgási eseményeire (ügyirat vagy első iratpéldány átadás/átvétel)")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratokSearch))]
    public Result GetAllIratmozgasWithExtension(ExecParam ExecParam, string IratId, EREC_IraIratokSearch _EREC_IraIratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllIratmozgasWithExtension(ExecParam, IratId, _EREC_IraIratokSearch);

            #region Eseménynaplózás
            if (isTransactionBeginHere && !Search.IsIdentical(_EREC_IraIratokSearch, new EREC_IraIratokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, null, "EREC_IraIratok", "IraIratokList").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_IraIratokSearch);

                    #region where feltétel összeállítása
                    if (!string.IsNullOrEmpty(_EREC_IraIratokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _EREC_IraIratokSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Egy irat rekord lekérése jogosultságellenőrzéssel (ha nincs megfelelő jogosultság, a hibaüzenet mező kitöltődik)
    /// </summary>
    /// <param name="execParam">RecordId mezőnek kell tartalmaznia a lekérendő irat Id-ját</param>
    /// <param name="Jogszint">I/O Írási vagy olvasási jogszin</param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratokSearch))]
    public Result GetWithRightCheck(ExecParam execParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);

            #region Eseménynaplózás
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_IraIratok", "IraIratView").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Egy irat rekord lekérése jogosultságellenőrzéssel (ha nincs megfelelő jogosultság, a hibaüzenet mező kitöltődik)
    /// </summary>
    /// <param name="execParam">RecordId mezőnek kell tartalmaznia a lekérendő irat Id-ját</param>
    /// <param name="Jogszint">I/O Írási vagy olvasási jogszin</param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratokSearch))]
    public Result GetWithRightCheckWithoutEventLogging(ExecParam execParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ha a megadott irat átiktatott, vagy egy másik irat erre lett átiktatva, 
    /// akkor ebben az átiktatási láncban szereplő iratokat adja vissza egy DataSet-ben
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetAll_AtiktatasiLanc(ExecParam execParam, string iratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll_AtiktatasiLanc(execParam, iratId);

            #region TODO: Eseménynaplózás
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// "A jogerősítő által láttamozandó iratok listája" (SZÜR webservice-ekhez kell: SZURGetJogeroLattamozandoWS)
    /// Egyedi lekérdező: azokat az iratokat adja vissza, ahol be van állítva a "Jogerősítendő" mező, ki van töltve a "Jogerősítést végző", de a "Jogerősítést végző látta" mező még üres.
    /// (Mindhárom mező a metaadatok között van.)
    /// (Az eredmény lista 'Obj_Id' mezője tartalmazza az irat Id-t.)
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetAllJogeroLattamozandoIratok(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllJogeroLattamozandoIratokSimple(execParam);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(execParam, result);
        return result;
    }



    #endregion

    #region Iktatás

    #region Egyszerűsített iktatás    

    /// <summary>
    /// Érkeztetés a megadott adatokkal, majd a keletkezett küldemény főszámra iktatása
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_IraIktatoKonyvek_Id"></param>
    /// <param name="_EREC_UgyUgyiratok"></param>
    /// <param name="_EREC_UgyUgyiratdarabok"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_HataridosFeladatok"></param>
    /// <param name="_EREC_Kuldemenyek"></param>
    /// <param name="ip"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result EgyszerusitettIktatasa(ExecParam _ExecParam, String _EREC_IraIktatoKonyvek_Id,
                                     EREC_UgyUgyiratok _EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                     EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok,
                                     EREC_KuldKuldemenyek _EREC_Kuldemenyek, IktatasiParameterek ip, ErkeztetesParameterek _ErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (ip == null) ip = new IktatasiParameterek();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(_ExecParam, ref _EREC_Kuldemenyek, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            #region Érkeztetés
            EREC_KuldKuldemenyekService svcKuld = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam xpmErkeztetes = _ExecParam.Clone();
            Result resErkeztetes = svcKuld.Erkeztetes(xpmErkeztetes, _EREC_Kuldemenyek, null, _ErkeztetesParameterek);

            if (!String.IsNullOrEmpty(resErkeztetes.ErrorCode))
            {
                throw new ResultException(resErkeztetes);
            }

            ip.KuldemenyId = resErkeztetes.Uid;

            #region Visszatérési információk
            ErkeztetesIktatasResult erkeztetesResult = null;
            if (resErkeztetes.Record != null && resErkeztetes.Record is ErkeztetesIktatasResult)
            {
                erkeztetesResult = (ErkeztetesIktatasResult)resErkeztetes.Record;
            }
            #endregion

            #endregion

            #region Küldemény vonalkódjának vizsgálata és leörököltetése iratpéldányra
            IktatasiParameterek iktParams = ip;
            EREC_MellekletekSearch search = new EREC_MellekletekSearch();

            search.AdathordozoTipus.Value = KodTarak.AdathordozoTipus.PapirAlapu;
            search.AdathordozoTipus.Operator = Contentum.eQuery.Query.Operators.equals;

            search.KuldKuldemeny_Id.Value = ip.KuldemenyId;
            search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            EREC_MellekletekService mellekletService = new EREC_MellekletekService(this.dataContext);

            Result mellekletGetAllResult = mellekletService.GetAll(_ExecParam.Clone(), search);
            if (string.IsNullOrEmpty(mellekletGetAllResult.ErrorCode) && mellekletGetAllResult.Ds.Tables[0].Rows.Count > 0)
            {
                iktParams.Iratpeldany_Vonalkod = mellekletGetAllResult.Ds.Tables[0].Rows[0]["BarCode"].ToString();
            }

            #endregion

            #region UgyintezesKezdoDatuma
            SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
            sakkoraSvc.SetUgyUgyintezesKezdoDatumaKuldemenyBeerkezesere(ref _EREC_IraIratok, _EREC_Kuldemenyek);
            #endregion

            #region Iktatasa
            ExecParam xpmIktatas = _ExecParam.Clone();
            Result resIktatas = this.BejovoIratIktatasa(xpmIktatas, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok, _EREC_UgyUgyiratdarabok,
                                _EREC_IraIratok, _EREC_HataridosFeladatok, iktParams);

            if (!String.IsNullOrEmpty(resIktatas.ErrorCode))
            {
                throw new ResultException(resIktatas);
            }

            string IratId = resIktatas.Uid;

            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = null;
            if (resIktatas.Record != null && resIktatas.Record is ErkeztetesIktatasResult)
            {
                iktatasResult = (ErkeztetesIktatasResult)resIktatas.Record;
            }
            resIktatas.Record = new ErkeztetesIktatasResult(erkeztetesResult, iktatasResult);
            #endregion

            #endregion

            #region Adószám
            Result resAddAdoszam = this.AddAdoszam(_ExecParam, ip.Adoszam, IratId);

            if (!String.IsNullOrEmpty(resAddAdoszam.ErrorCode))
            {
                throw new ResultException(resAddAdoszam);
            }

            #endregion

            result = resIktatas;

            #region BLG_1354
            sakkoraSvc.SetSakkora(_ExecParam.Clone(), iktatasResult, Sakkora.NormalVagyAlszamraIktatas.NormalIktatas);
            #endregion BLG_1354

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                // Iktatás az naplózódik az iktatásnál, az érkeztetést itt naplózzuk:

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                string kuldemenyId = resErkeztetes.Uid;

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id
                    , kuldemenyId, "EREC_KuldKuldemenyek", "KuldemenyNew").Record;

                eventLogService.Insert(_ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }

    private Result AddAdoszam(ExecParam _ExecParam, string Adoszam, string IratId)
    {
        if (Adoszam == null) Adoszam = String.Empty;
        if (String.IsNullOrEmpty(Adoszam.Trim()))
        {
            return new Result();
        }
        #region Adószám tárgyszó id
        EREC_TargySzavakService svcTargyszo = new EREC_TargySzavakService(this.dataContext);
        ExecParam xpmTargyszo = _ExecParam.Clone();
        EREC_TargySzavakSearch schTargyszo = new EREC_TargySzavakSearch();
        schTargyszo.TargySzavak.Value = "Adószám";
        schTargyszo.TargySzavak.Operator = Query.Operators.equals;
        Result resTargyszo = svcTargyszo.GetAll(xpmTargyszo, schTargyszo);
        if (!String.IsNullOrEmpty(resTargyszo.ErrorCode))
        {
            throw new ResultException(resTargyszo);
        }

        if (resTargyszo.Ds.Tables[0].Rows.Count == 0)
        {
            throw new ResultException("Adószám néven tárgyszó nem található");
        }

        string targyszoId = resTargyszo.Ds.Tables[0].Rows[0]["Id"].ToString();

        #endregion

        EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
        erec_ObjektumTargyszavai.Updated.SetValueAll(false);
        erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

        erec_ObjektumTargyszavai.Targyszo_Id = targyszoId;
        erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;
        erec_ObjektumTargyszavai.Targyszo = "Adószám";
        erec_ObjektumTargyszavai.Updated.Targyszo = true;
        erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Listas; // "02"
        erec_ObjektumTargyszavai.Updated.Forras = true;

        erec_ObjektumTargyszavai.Ertek = Adoszam;
        erec_ObjektumTargyszavai.Updated.Ertek = true;

        erec_ObjektumTargyszavai.Obj_Id = IratId;
        erec_ObjektumTargyszavai.Updated.Obj_Id = true;

        //Irat
        erec_ObjektumTargyszavai.ObjTip_Id = "AA5E7BBA-96A0-4C17-8709-06A6D297E107";
        erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;

        erec_ObjektumTargyszavai.ErvVege = "4700.12.31";
        erec_ObjektumTargyszavai.Updated.ErvVege = true;

        EREC_ObjektumTargyszavaiService svcObjektumTargyszavai = new EREC_ObjektumTargyszavaiService(this.dataContext);
        ExecParam xpmObjektumTargyszavai = _ExecParam.Clone();

        Result res = svcObjektumTargyszavai.Insert(xpmObjektumTargyszavai, erec_ObjektumTargyszavai);

        return res;
    }

    /// <summary>
    /// Érkeztetés a megadott adatokkal, majd a keletkezett küldemény alszámra iktatása a megadott ügyiratba
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_IraIktatoKonyvek_Id"></param>
    /// <param name="_EREC_UgyUgyiratok_Id"></param>
    /// <param name="_EREC_UgyUgyiratdarabok"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_HataridosFeladatok"></param>
    /// <param name="_EREC_Kuldemenyek"></param>
    /// <param name="ip"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result EgyszerusitettIktatasa_Alszamra(ExecParam _ExecParam, String _EREC_IraIktatoKonyvek_Id, String _EREC_UgyUgyiratok_Id,
                                              EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok, EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok,
                                              EREC_KuldKuldemenyek _EREC_Kuldemenyek, IktatasiParameterek ip, ErkeztetesParameterek _ErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Bejövő irat iktatása Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (ip == null) ip = new IktatasiParameterek();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(_ExecParam, ref _EREC_Kuldemenyek, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            #region Érkeztetés
            EREC_KuldKuldemenyekService svcKuld = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam xpmErkeztetes = _ExecParam.Clone();
            Result resErkeztetes = svcKuld.Erkeztetes(xpmErkeztetes, _EREC_Kuldemenyek, null, _ErkeztetesParameterek);

            if (!String.IsNullOrEmpty(resErkeztetes.ErrorCode))
            {
                throw new ResultException(resErkeztetes);
            }

            ip.KuldemenyId = resErkeztetes.Uid;

            #region Visszatérési információk
            ErkeztetesIktatasResult erkeztetesResult = null;
            if (resErkeztetes.Record != null && resErkeztetes.Record is ErkeztetesIktatasResult)
            {
                erkeztetesResult = (ErkeztetesIktatasResult)resErkeztetes.Record;
            }
            #endregion

            #endregion

            #region Küldemény vonalkódjának vizsgálata és leörököltetése iratpéldányra
            IktatasiParameterek iktParams = ip;
            EREC_MellekletekSearch search = new EREC_MellekletekSearch();

            search.AdathordozoTipus.Value = KodTarak.AdathordozoTipus.PapirAlapu;
            search.AdathordozoTipus.Operator = Contentum.eQuery.Query.Operators.equals;

            search.KuldKuldemeny_Id.Value = ip.KuldemenyId;
            search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            EREC_MellekletekService mellekletService = new EREC_MellekletekService(this.dataContext);

            Result mellekletGetAllResult = mellekletService.GetAll(_ExecParam.Clone(), search);
            if (string.IsNullOrEmpty(mellekletGetAllResult.ErrorCode) && mellekletGetAllResult.Ds.Tables[0].Rows.Count > 0)
                iktParams.Iratpeldany_Vonalkod = mellekletGetAllResult.Ds.Tables[0].Rows[0]["BarCode"].ToString();

            #endregion

            #region UgyintezesKezdoDatuma
            if (string.IsNullOrEmpty(_EREC_IraIratok.UgyintezesKezdoDatuma))
            {
                if (!string.IsNullOrEmpty(_EREC_Kuldemenyek.BeerkezesIdeje))
                    _EREC_IraIratok.UgyintezesKezdoDatuma = _EREC_Kuldemenyek.BeerkezesIdeje;
                else
                    _EREC_IraIratok.UgyintezesKezdoDatuma = DateTime.Now.ToString();

                _EREC_IraIratok.Updated.UgyintezesKezdoDatuma = true;
            }
            #endregion

            #region Iktatasa
            ExecParam xpmIktatas = _ExecParam.Clone();
            Result resIktatas = this.BejovoIratIktatasa_Alszamra(xpmIktatas, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok_Id, _EREC_UgyUgyiratdarabok,
                                _EREC_IraIratok, _EREC_HataridosFeladatok, iktParams);

            if (!String.IsNullOrEmpty(resIktatas.ErrorCode))
            {
                throw new ResultException(resIktatas);
            }
            #endregion

            string IratId = resIktatas.Uid;

            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = null;
            if (resIktatas.Record != null && resIktatas.Record is ErkeztetesIktatasResult)
            {
                iktatasResult = (ErkeztetesIktatasResult)resIktatas.Record;
            }
            resIktatas.Record = new ErkeztetesIktatasResult(erkeztetesResult, iktatasResult);
            #endregion

            #region Adószám
            Result resAddAdoszam = this.AddAdoszam(_ExecParam, ip.Adoszam, IratId);

            if (!String.IsNullOrEmpty(resAddAdoszam.ErrorCode))
            {
                throw new ResultException(resAddAdoszam);
            }
            #endregion

            result = resIktatas;

            #region BLG_1354
            SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
            sakkoraSvc.SetSakkora(_ExecParam.Clone(), iktatasResult, Sakkora.NormalVagyAlszamraIktatas.AlszamraIktatas);
            #endregion BLG_1354

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                // Iktatás az naplózódik az iktatásnál, az érkeztetést itt naplózzuk:

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                string kuldemenyId = resErkeztetes.Uid;

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id
                    , kuldemenyId, "EREC_KuldKuldemenyek", "KuldemenyNew").Record;

                eventLogService.Insert(_ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }

    #endregion

    #region EmailErkeztetesIktatas

    /// <summary>
    /// Email érkeztetése a megadott adatokkal, majd a keletkezett küldemény főszámra iktatása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kuldemenyAdatok"></param>
    /// <param name="emailBoritekAdatok"></param>
    /// <param name="erec_IraIktatoKonyvek_Id"></param>
    /// <param name="erec_UgyUgyiratok"></param>
    /// <param name="erec_UgyUgyiratdarabok"></param>
    /// <param name="erec_IraIratok"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <param name="ip"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result EmailErkeztetesIktatas(ExecParam execParam, EREC_KuldKuldemenyek kuldemenyAdatok, EREC_eMailBoritekok emailBoritekAdatok
                , string erec_IraIktatoKonyvek_Id, EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok,
                 EREC_IraIratok erec_IraIratok, EREC_HataridosFeladatok erec_HataridosFeladatok, IktatasiParameterek ip, ErkeztetesParameterek _ErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (ip == null) ip = new IktatasiParameterek();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(execParam, erec_IraIratok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            #region Érkeztetés

            EREC_KuldKuldemenyekService svcKuld = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam execParamErkeztetes = execParam.Clone();
            Result resErkeztetes = svcKuld.ErkeztetesEmailbol(execParamErkeztetes, kuldemenyAdatok, emailBoritekAdatok, null, _ErkeztetesParameterek);

            if (!String.IsNullOrEmpty(resErkeztetes.ErrorCode))
            {
                throw new ResultException(resErkeztetes);
            }

            string kuldemenyId = resErkeztetes.Uid;

            #region Visszatérési információk
            ErkeztetesIktatasResult erkeztetesResult = null;
            if (resErkeztetes.Record != null && resErkeztetes.Record is ErkeztetesIktatasResult)
            {
                erkeztetesResult = (ErkeztetesIktatasResult)resErkeztetes.Record;
            }
            #endregion

            #endregion

            #region Iktatas

            //  jelleg beallitasa
            if (String.IsNullOrEmpty(erec_IraIratok.Jelleg))
            {
                erec_IraIratok.Jelleg = KodTarak.IRAT_JELLEG.E_mail_uzenet;
                erec_IraIratok.Updated.Jelleg = true;
            }


            // küldemény vonalkódjának átmásolása iratpéldányhoz, ha nincs:
            if (String.IsNullOrEmpty(ip.Iratpeldany_Vonalkod)
                && !String.IsNullOrEmpty(kuldemenyAdatok.BarCode))
            {
                ip.Iratpeldany_Vonalkod = kuldemenyAdatok.BarCode;
            }

            ExecParam xpmIktatas = execParam.Clone();
            ip.KuldemenyId = kuldemenyId;

            // iratpéldánynak vonalkód generálás beállítása
            ip.IratpeldanyVonalkodGeneralasHaNincs = true;

            Result resIktatas = this.BejovoIratIktatasa(xpmIktatas, erec_IraIktatoKonyvek_Id, erec_UgyUgyiratok, erec_UgyUgyiratdarabok,
                                erec_IraIratok, erec_HataridosFeladatok, ip);

            if (!String.IsNullOrEmpty(resIktatas.ErrorCode))
            {
                throw new ResultException(resIktatas);
            }

            string IratId = resIktatas.Uid;

            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = null;
            if (resIktatas.Record != null && resIktatas.Record is ErkeztetesIktatasResult)
            {
                iktatasResult = (ErkeztetesIktatasResult)resIktatas.Record;
            }
            resIktatas.Record = new ErkeztetesIktatasResult(erkeztetesResult, iktatasResult);
            #endregion

            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                // Iktatás az naplózódik az iktatásnál, az érkeztetést itt naplózzuk:

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id
                    , kuldemenyId, "EREC_KuldKuldemenyek", "KuldemenyNew").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            result = resIktatas;
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Email érkeztetése a megadott adatokkal, majd a keletkezett küldemény alszámra iktatása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kuldemenyAdatok"></param>
    /// <param name="emailBoritekAdatok"></param>
    /// <param name="erec_IraIktatoKonyvek_Id"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <param name="erec_UgyUgyiratdarabok"></param>
    /// <param name="erec_IraIratok"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <param name="ip"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result EmailErkeztetesIktatas_Alszamra(ExecParam execParam, EREC_KuldKuldemenyek kuldemenyAdatok, EREC_eMailBoritekok emailBoritekAdatok
                    , string erec_IraIktatoKonyvek_Id, string erec_UgyUgyiratok_Id, EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok,
                     EREC_IraIratok erec_IraIratok, EREC_HataridosFeladatok erec_HataridosFeladatok, IktatasiParameterek ip, ErkeztetesParameterek _ErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (ip == null) ip = new IktatasiParameterek();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(execParam, erec_IraIratok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            #region Érkeztetés

            EREC_KuldKuldemenyekService svcKuld = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam execParamErkeztetes = execParam.Clone();
            Result resErkeztetes = svcKuld.ErkeztetesEmailbol(execParamErkeztetes, kuldemenyAdatok, emailBoritekAdatok, null, _ErkeztetesParameterek);

            if (!String.IsNullOrEmpty(resErkeztetes.ErrorCode))
            {
                throw new ResultException(resErkeztetes);
            }

            string kuldemenyId = resErkeztetes.Uid;

            #region Visszatérési információk
            ErkeztetesIktatasResult erkeztetesResult = null;
            if (resErkeztetes.Record != null && resErkeztetes.Record is ErkeztetesIktatasResult)
            {
                erkeztetesResult = (ErkeztetesIktatasResult)resErkeztetes.Record;
            }
            #endregion

            #endregion

            #region Iktatasa

            // küldemény vonalkódjának átmásolása iratpéldányhoz, ha nincs:
            if (String.IsNullOrEmpty(ip.Iratpeldany_Vonalkod)
                && !String.IsNullOrEmpty(kuldemenyAdatok.BarCode))
            {
                ip.Iratpeldany_Vonalkod = kuldemenyAdatok.BarCode;
            }

            ExecParam execParamIktatas = execParam.Clone();
            ip.KuldemenyId = kuldemenyId;
            // iratpéldánynak vonalkód generálás beállítása
            ip.IratpeldanyVonalkodGeneralasHaNincs = true;

            //  jelleg beallitasa
            if (String.IsNullOrEmpty(erec_IraIratok.Jelleg))
            {
                erec_IraIratok.Jelleg = KodTarak.IRAT_JELLEG.E_mail_uzenet;
                erec_IraIratok.Updated.Jelleg = true;
            }

            Result resIktatas = this.BejovoIratIktatasa_Alszamra(execParamIktatas, erec_IraIktatoKonyvek_Id, erec_UgyUgyiratok_Id, erec_UgyUgyiratdarabok,
                                erec_IraIratok, erec_HataridosFeladatok, ip);

            if (!String.IsNullOrEmpty(resIktatas.ErrorCode))
            {
                throw new ResultException(resIktatas);
            }

            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = null;
            if (resIktatas.Record != null && resIktatas.Record is ErkeztetesIktatasResult)
            {
                iktatasResult = (ErkeztetesIktatasResult)resIktatas.Record;
            }
            resIktatas.Record = new ErkeztetesIktatasResult(erkeztetesResult, iktatasResult);
            #endregion

            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                // Iktatás az naplózódik az iktatásnál, az érkeztetést itt naplózzuk:

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id
                    , kuldemenyId, "EREC_KuldKuldemenyek", "KuldemenyNew").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            result = resIktatas;
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Kimenő E-Mail főszámra iktatása
    /// </summary>
    /// <param name="execParam">Általános információk a futtató felhasználóról</param>
    /// <param name="erec_eMailBoritekok_Id">EREC_EmailBoritekok azonosítója, amit iktatni szeretnénk</param>
    /// <param name="erec_IraIktatoKonyvek_Id">EREC_IraIktatokonyvek azonosítója, amibe szeretnénk iktatni</param>
    /// <param name="erec_UgyUgyiratok">a létrejövő ügyirat adatai</param>
    /// <param name="erec_IraIratok">a létrejövő irat adatai</param>
    /// <param name="erec_PldIratPeldanyok">a létrejövő iratpéldány adatai</param>
    /// <param name="erec_HataridosFeladatok">Az új irathoz megadható kezelési feljegyzést tartalmazza.
    /// Megadása nem kötelező.</param>
    /// <param name="ip">IktatasiParameterek típusú objektum, az iktatáshoz opcionálisan adható egyéb paraméterek</param>
    /// <returns>A művelet eredményét jelző Result objektum; siker esetén az UID mezőben az újonnan
    /// létrejönn irat Id-ja, egyébként a hiba kódja az ErrorCode mezőben</returns>
    [WebMethod()]
    public Result KimenoEmailIktatas(ExecParam execParam, string erec_eMailBoritekok_Id, string erec_IraIktatoKonyvek_Id
        , EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIratok erec_IraIratok
        , EREC_PldIratPeldanyok erec_PldIratPeldanyok, EREC_HataridosFeladatok erec_HataridosFeladatok, IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(execParam, erec_IraIratok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            result = this.kimenoEmailIktatas_segedFv(NormalVagyAlszamraIktatas.NormalIktatas, execParam, erec_eMailBoritekok_Id
                , erec_IraIktatoKonyvek_Id, null, erec_UgyUgyiratok, erec_IraIratok, erec_PldIratPeldanyok, erec_HataridosFeladatok, ip);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Kimenő E-Mail iktatása alszámra
    /// </summary>
    /// <param name="execParam">Általános információk a futtató felhasználóról</param>
    /// <param name="erec_eMailBoritekok_Id">EREC_EmailBoritekok azonosítója, amit iktatni szeretnénk</param>
    /// <param name="erec_IraIktatoKonyvek_Id">EREC_IraIktatokonyvek azonosítója, amibe szeretnénk iktatni</param>
    /// <param name="erec_UgyUgyiratok_Id">EREC_UgyUgyirat azonosító, amibe szertnénk iktatni</param>
    /// <param name="erec_IraIratok"> létrejövő irat adatai</param>
    /// <param name="erec_PldIratPeldanyok">a létrejövő iratpéldány adatai</param>
    /// <param name="erec_HataridosFeladatok">Az új irathoz megadható kezelési feljegyzést tartalmazza.
    /// Megadása nem kötelező.</param>
    /// <param name="ip">IktatasiParameterek típusú objektum, az iktatáshoz opcionálisan adható egyéb paraméterek</param>
    /// <returns>IktatasiParameterek típusú objektum, az iktatáshoz opcionálisan adható egyéb paraméterek</param>
    /// <returns>A művelet eredményét jelző Result objektum; siker esetén az UID mezőben az újonnan
    /// létrejönn irat Id-ja, egyébként a hiba kódja az ErrorCode mezőben</returns>
    [WebMethod()]
    public Result KimenoEmailIktatas_Alszamra(ExecParam execParam, string erec_eMailBoritekok_Id, string erec_IraIktatoKonyvek_Id
        , string erec_UgyUgyiratok_Id, EREC_IraIratok erec_IraIratok, EREC_PldIratPeldanyok erec_PldIratPeldanyok
        , EREC_HataridosFeladatok erec_HataridosFeladatok, IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(execParam, erec_IraIratok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            result = this.kimenoEmailIktatas_segedFv(NormalVagyAlszamraIktatas.AlszamraIktatas, execParam, erec_eMailBoritekok_Id
                , erec_IraIktatoKonyvek_Id, erec_UgyUgyiratok_Id, null, erec_IraIratok, erec_PldIratPeldanyok, erec_HataridosFeladatok, ip);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    private Result kimenoEmailIktatas_segedFv(NormalVagyAlszamraIktatas normalVagyAlszamraIktatas, ExecParam execParam
        , string erec_eMailBoritekok_Id, string erec_IraIktatoKonyvek_Id, string erec_UgyUgyiratok_Id
        , EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_IraIratok erec_IraIratok,
        EREC_PldIratPeldanyok erec_PldIratPeldanyok, EREC_HataridosFeladatok erec_HataridosFeladatok, IktatasiParameterek ip)
    {

        #region Email boríték ellenőrzés, iktatható-e?

        if (string.IsNullOrEmpty(erec_eMailBoritekok_Id))
        {
            throw new ResultException("Nincs megadva az emailboritek Id!");
        }

        EREC_eMailBoritekokService service_emailboritekok = new EREC_eMailBoritekokService(this.dataContext);
        ExecParam execParam_emailboritekGet = execParam.Clone();
        execParam_emailboritekGet.Record_Id = erec_eMailBoritekok_Id;

        Result result_emailborGet = service_emailboritekok.Get(execParam_emailboritekGet);
        if (result_emailborGet.IsError)
        {
            // hiba:
            throw new ResultException(result_emailborGet);
        }

        EREC_eMailBoritekok erec_eMailBoritek = (EREC_eMailBoritekok)result_emailborGet.Record;

        // Iktatták-e már?
        if (!string.IsNullOrEmpty(erec_eMailBoritek.IraIrat_Id))
        {
            // Hiba: Az email már iktatva van!
            throw new ResultException(52122);
        }

        #endregion

        if (ip == null) ip = new IktatasiParameterek();

        // Iratpéldánynak vonalkód generálás beállítása:
        ip.IratpeldanyVonalkodGeneralasHaNincs = true;

        //
        // adathordozo tipus beallitasa

        if (String.IsNullOrEmpty(erec_IraIratok.AdathordozoTipusa))
        {
            erec_IraIratok.AdathordozoTipusa = "1";
            erec_IraIratok.Updated.AdathordozoTipusa = true;
        }

        //
        //  jelleg beallitasa
        if (String.IsNullOrEmpty(erec_IraIratok.Jelleg))
        {
            erec_IraIratok.Jelleg = KodTarak.IRAT_JELLEG.E_mail_uzenet;
            erec_IraIratok.Updated.Jelleg = true;
        }

        // BLG_11293, BLG_11294
        // BL kapcsán előjött hogy az addIn nem tölti a munkaállomást
        if (String.IsNullOrEmpty(erec_IraIratok.Munkaallomas))
        {
            erec_IraIratok.Munkaallomas = HttpContext.Current.Request.UserHostAddress;
            erec_IraIratok.Updated.Jelleg = true;
        }


        #region Iktatas
        ExecParam xpmIktatas = execParam.Clone();
        Result resIktatas = null;

        if (normalVagyAlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
        {
            resIktatas = this.BelsoIratIktatasa(xpmIktatas, erec_IraIktatoKonyvek_Id, erec_UgyUgyiratok
                , null, erec_IraIratok, erec_HataridosFeladatok, erec_PldIratPeldanyok, ip);
        }
        else
        {
            resIktatas = this.BelsoIratIktatasa_Alszamra(xpmIktatas, erec_IraIktatoKonyvek_Id, erec_UgyUgyiratok_Id
                , null, erec_IraIratok, erec_HataridosFeladatok, erec_PldIratPeldanyok, ip); // BLG_237, erec_eMailBoritek.Id);
        }

        if (resIktatas.IsError)
        {
            // hiba:
            throw new ResultException(resIktatas);
        }

        string IratId = resIktatas.Uid;

        #endregion

        // BLG_237
        // Átrakva IratIktatas végére
        #region Email boríték update

        erec_eMailBoritek.Updated.SetValueAll(false);
        erec_eMailBoritek.Base.Updated.SetValueAll(false);
        erec_eMailBoritek.Base.Updated.Ver = true;

        // IraIratId-t kell beállítani:
        erec_eMailBoritek.IraIrat_Id = IratId;
        erec_eMailBoritek.Updated.IraIrat_Id = true;

        //

        // Állapot?

        ExecParam execParam_emailborUpdate = execParam.Clone();
        execParam_emailborUpdate.Record_Id = erec_eMailBoritek.Id;

        Result result_emailborUpdate = service_emailboritekok.Update(execParam_emailborUpdate, erec_eMailBoritek);
        if (result_emailborUpdate.IsError)
        {
            // hiba:
            throw new ResultException(result_emailborUpdate);
        }

        #endregion

        #region Csatolmány létrehozása az irathoz

        // Email boríték csatolmányainak lekérdezése:
        EREC_eMailBoritekCsatolmanyokService service_emailboritekCsatolmanyok = new EREC_eMailBoritekCsatolmanyokService(this.dataContext);

        EREC_eMailBoritekCsatolmanyokSearch search_emailborCsat = new EREC_eMailBoritekCsatolmanyokSearch();

        search_emailborCsat.eMailBoritek_Id.Value = erec_eMailBoritek.Id;
        search_emailborCsat.eMailBoritek_Id.Operator = Query.Operators.equals;

        Result result_emailborCsatGetAll = service_emailboritekCsatolmanyok.GetAll(execParam.Clone(), search_emailborCsat);
        if (result_emailborCsatGetAll.IsError)
        {
            // hiba:
            throw new ResultException(result_emailborCsatGetAll);
        }

        // minden csatolmányhoz létrehozunk egy EREC_Csatolmanyok bejegyzést (elvileg csak egy csatolmány van)
        if (result_emailborCsatGetAll.Ds != null)
        {
            //"EredetiOutlookUzenet.msg";
            int cntOriginalEmailMessage = (result_emailborCsatGetAll.Ds.Tables[0].Select(String.Format("Nev='{0}'", Contentum.eUtility.Constants.EMail.EredetiUzenetFileName))).Length;
            Logger.Info(String.Format("{0} nevű azonosított csatolmányok száma: {1} (összes talált csatolmány: {2})", Contentum.eUtility.Constants.EMail.EredetiUzenetFileName
                , cntOriginalEmailMessage, result_emailborCsatGetAll.Ds.Tables[0].Rows.Count));

            if (result_emailborCsatGetAll.Ds.Tables[0].Rows.Count > 0 && cntOriginalEmailMessage != 1)
            {
                Logger.Warn("Kimenő email: Az eredeti email üzenet nem azonosítható, ezért nem lesz beállítva dokumentum szerep (fődokumentum, dokuemntum melléklet)!");
            }


            foreach (DataRow row in result_emailborCsatGetAll.Ds.Tables[0].Rows)
            {
                string row_IraIrat_eMailBoritek_Id = row["eMailBoritek_Id"].ToString();
                string row_Dokumentum_Id = row["Dokumentum_Id"].ToString();
                string row_Nev = row["Nev"].ToString();

                // ellenőrzés: (biztos, ami biztos)
                if (row_IraIrat_eMailBoritek_Id != erec_eMailBoritek.Id)
                {
                    // hiba:
                    throw new ResultException("Hiba az emailboríték csatolmányának lekérése során!");
                }

                #region EREC_Csatolmanyok INSERT

                EREC_Csatolmanyok erec_csatolmanyok = new EREC_Csatolmanyok();

                erec_csatolmanyok.Dokumentum_Id = row_Dokumentum_Id;
                erec_csatolmanyok.Updated.Dokumentum_Id = true;

                erec_csatolmanyok.IraIrat_Id = IratId;
                erec_csatolmanyok.Updated.IraIrat_Id = true;

                erec_csatolmanyok.Leiras = row_Nev;
                erec_csatolmanyok.Updated.Leiras = true;

                if (cntOriginalEmailMessage == 1)
                {
                    // DokumentumSzerep
                    if (row_Nev == Contentum.eUtility.Constants.EMail.EredetiUzenetFileName) //"EredetiOutlookUzenet.msg";
                    {
                        erec_csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
                        erec_csatolmanyok.Updated.DokumentumSzerep = true;
                    }
                    else
                    {
                        erec_csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.DokumentumMelleklet;
                        erec_csatolmanyok.Updated.DokumentumSzerep = true;
                    }
                }

                EREC_CsatolmanyokService service_csatolmany = new EREC_CsatolmanyokService(this.dataContext);

                Result result_csatolmanyInsert = service_csatolmany.Insert(execParam.Clone(), erec_csatolmanyok);
                if (result_csatolmanyInsert.IsError)
                {
                    // hiba:
                    throw new ResultException(result_csatolmanyInsert);
                }

                #endregion
            }
        }

        #endregion

        /// Nem kell, a resIktatas.Record egy ErkeztetesIktatasResult objektum, abban benne van:

        //#region iktatoszam visszaselectalasa, mert vissza kell adni - de lehetne kicsit redundansabb a DB s nem kene ez a resz! vagy van fuggveny?

        //String visszaIktatoszam = String.Empty;

        //Logger.Debug(String.Format("iktatoszam visszaselectalasa, mert vissza kell adni resz indul - kivalasztottIratId: {0}", IratId));

        //EREC_IraIratokService service_Iratok2 = new EREC_IraIratokService(this.dataContext);
        //ExecParam execParam_iratGet2 = execParam.Clone();
        //execParam_iratGet2.Record_Id = IratId;

        //EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();

        //iratokSearch.Id.Value = IratId;
        //iratokSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

        //Result result_IratGet2 = service_Iratok2.GetAllWithExtension(execParam_iratGet2, iratokSearch);
        //if (!String.IsNullOrEmpty(result_IratGet2.ErrorCode))
        //{
        //    // hiba:
        //    Logger.Error(String.Format("Irat leszedese hiba az iktatoszam miatti visszaselectkor! Code: {0} Message: {1}", result_IratGet2.ErrorCode, result_IratGet2.ErrorMessage));
        //    //throw new ResultException(result_IratGet);
        //    return result_IratGet2;
        //}
        //else
        //{
        //    if (result_IratGet2.Ds.Tables[0].Rows.Count == 0)
        //    {
        //        // hiba:
        //        Logger.Error(String.Format("Irat leszedese hiba miatti visszaselectkor! Nincsenek eredmenysorok!"));
        //        throw new ResultException(50101); //  Rekord lekérése az adatbázisból sikertelen
        //    }

        //    if (result_IratGet2.Ds.Tables[0].Rows.Count > 1)
        //    {
        //        // hiba:
        //        Logger.Error(String.Format("Irat leszedese hiba miatti visszaselectkor! Tul sok eredmenysor!"));
        //        throw new ResultException(52100);  //  Hiba az iktatás során
        //    }

        //    Logger.Debug(String.Format("Irat leszedese ok."));
        //    //visszaIktatoszam = Convert.ToString(result_IratGet2.Ds.Tables[0].Rows[0]["Iktatoszam_Merge"]);
        //    resIktatas.Record = String.Format("<result><iktatoszam>{0}</iktatoszam></result>", Convert.ToString(result_IratGet2.Ds.Tables[0].Rows[0]["Iktatoszam_Merge"]));

        //}

        //#endregion

        return resIktatas;
    }

    #endregion

    #region Hivatali kapu érkeztetés/iktatás

    private void BekuldoCimBeallitas(ExecParam execParam, EREC_UgyUgyiratok ugyirat, EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (erec_KuldKuldemenyek == null || ugyirat == null) { return; }

        try
        {
            // BUG_11574, további beküldők keresése
            var bekuldoNeve = erec_KuldKuldemenyek.NevSTR_Bekuldo;
            var bekuldoCime = erec_KuldKuldemenyek.CimSTR_Bekuldo;
            var bekuldoId = erec_KuldKuldemenyek.Partner_Id_Bekuldo;
            var bekuldoCimId = "";

            var bekuldokService = new EREC_KuldBekuldokService(dataContext);
            var bekuldokSearch = new EREC_KuldBekuldokSearch();
            bekuldokSearch.KuldKuldemeny_Id.Filter(erec_KuldKuldemenyek.Id);

            var bekuldokResult = bekuldokService.GetAll(execParam.Clone(), bekuldokSearch);
            if (!bekuldokResult.IsError && bekuldokResult.GetCount == 1)
            {
                // csak egy beküldő van
                var bekuldoRow = bekuldokResult.Ds.Tables[0].Rows[0];
                bekuldoNeve = bekuldoRow["NevSTR"].ToString();
                bekuldoCime = bekuldoRow["CimSTR"].ToString();
                bekuldoId = bekuldoRow["Partner_Id_Bekuldo"].ToString();
                bekuldoCimId = bekuldoRow["PartnerCim_Id_Bekuldo"] == DBNull.Value ? "" : bekuldoRow["PartnerCim_Id_Bekuldo"].ToString();
            }

            if (String.IsNullOrEmpty(bekuldoCimId) && !String.IsNullOrEmpty(bekuldoId))
            {
                // ha nem volt beállítva a cím id, megkeressük
                bekuldoCimId = CimUtility.GetPartnerCimId(execParam.Clone(), bekuldoId);
            }

            if (String.IsNullOrEmpty(ugyirat.CimSTR_Ugyindito)) // BUG_12726
            {
                ugyirat.CimSTR_Ugyindito = bekuldoCime;
                ugyirat.Updated.CimSTR_Ugyindito = true;
            }
            if (String.IsNullOrEmpty(ugyirat.NevSTR_Ugyindito)) // BUG_12726
            {
                ugyirat.NevSTR_Ugyindito = bekuldoNeve;
                ugyirat.Updated.NevSTR_Ugyindito = true;
            }

            if (!String.IsNullOrEmpty(bekuldoCimId) && String.IsNullOrEmpty(ugyirat.Cim_Id_Ugyindito)) // BUG_12726
            {
                ugyirat.Cim_Id_Ugyindito = bekuldoCimId;
                ugyirat.Updated.Cim_Id_Ugyindito = true;
            }

            if (!String.IsNullOrEmpty(bekuldoId) && String.IsNullOrEmpty(ugyirat.Partner_Id_Ugyindito)) // BUG_12726
            {
                ugyirat.Partner_Id_Ugyindito = bekuldoId;
                ugyirat.Updated.Partner_Id_Ugyindito = true;
            }
        }
        catch (Exception x)
        {
            Logger.Error("BekuldoCimBeallitas HIBA", x);
        }
    }

    /// <summary>
    /// Hivatali kapus üzenet érkeztetése a megadott adatokkal, majd a keletkezett küldemény főszámra iktatása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kuldemenyAdatok"></param>
    /// <param name="emailBoritekAdatok"></param>
    /// <param name="erec_IraIktatoKonyvek_Id"></param>
    /// <param name="erec_UgyUgyiratok"></param>
    /// <param name="erec_UgyUgyiratdarabok"></param>
    /// <param name="erec_IraIratok"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <param name="ip"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result HivataliKapusErkeztetesIktatas(ExecParam execParam, EREC_KuldKuldemenyek kuldemenyAdatok, string eBeadvany_Id
                , string erec_IraIktatoKonyvek_Id, EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok,
                 EREC_IraIratok erec_IraIratok, EREC_HataridosFeladatok erec_HataridosFeladatok, IktatasiParameterek ip, ErkeztetesParameterek _ErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (ip == null) ip = new IktatasiParameterek();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(execParam, ref kuldemenyAdatok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            #region Érkeztetés

            EREC_KuldKuldemenyekService svcKuld = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam execParamErkeztetes = execParam.Clone();
            Result resErkeztetes = svcKuld.ErkeztetesHivataliKapubol(execParamErkeztetes, kuldemenyAdatok, eBeadvany_Id, null, _ErkeztetesParameterek);
            resErkeztetes.CheckError();

            string kuldemenyId = resErkeztetes.Uid;

            ErkeztetesIktatasResult erkeztetesResult = null;
            if (resErkeztetes.Record != null && resErkeztetes.Record is ErkeztetesIktatasResult)
            {
                erkeztetesResult = (ErkeztetesIktatasResult)resErkeztetes.Record;
            }

            #endregion

            kuldemenyAdatok.Id = kuldemenyId;
            BekuldoCimBeallitas(execParam, erec_UgyUgyiratok, kuldemenyAdatok); // BUG_11574

            #region Iktatas

            //  jelleg beallitasa
            if (String.IsNullOrEmpty(erec_IraIratok.Jelleg))
            {
                erec_IraIratok.Jelleg = KodTarak.IRAT_JELLEG.E_mail_uzenet;
                erec_IraIratok.Updated.Jelleg = true;
            }

            // küldemény vonalkódjának átmásolása iratpéldányhoz, ha nincs:
            if (String.IsNullOrEmpty(ip.Iratpeldany_Vonalkod)
                && !String.IsNullOrEmpty(kuldemenyAdatok.BarCode))
            {
                ip.Iratpeldany_Vonalkod = kuldemenyAdatok.BarCode;
            }

            ExecParam xpmIktatas = execParam.Clone();
            ip.KuldemenyId = kuldemenyId;

            // iratpéldánynak vonalkód generálás beállítása
            ip.IratpeldanyVonalkodGeneralasHaNincs = true;

            Result resIktatas = this.BejovoIratIktatasa(xpmIktatas, erec_IraIktatoKonyvek_Id, erec_UgyUgyiratok, erec_UgyUgyiratdarabok,
                                erec_IraIratok, erec_HataridosFeladatok, ip);
            resIktatas.CheckError();

            string IratId = resIktatas.Uid;

            //LZS - BUG_12673
            //Nem írta vissza az eBeadványba az IratId-t, ezért itt beteszem.
            EREC_eBeadvanyokService eBeadvanyokService = new EREC_eBeadvanyokService(this.dataContext);
            ExecParam exec_eBeadvany = execParam.Clone();
            exec_eBeadvany.Record_Id = eBeadvany_Id;

            EREC_eBeadvanyok eBeadvany = eBeadvanyokService.Get(exec_eBeadvany).Record as EREC_eBeadvanyok;
            eBeadvany.IraIrat_Id = IratId;
            eBeadvany.Updated.IraIrat_Id = true;

            eBeadvanyokService.Update(exec_eBeadvany, eBeadvany);


            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = null;
            if (resIktatas.Record != null && resIktatas.Record is ErkeztetesIktatasResult)
            {
                iktatasResult = (ErkeztetesIktatasResult)resIktatas.Record;
            }
            resIktatas.Record = new ErkeztetesIktatasResult(erkeztetesResult, iktatasResult);
            #endregion

            #endregion

            #region BLG_1354
            try
            {
                SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
                sakkoraSvc.SetSakkora(execParam.Clone(), iktatasResult, Sakkora.NormalVagyAlszamraIktatas.NormalIktatas);
            }
            catch (Exception x)
            {
                Logger.Error("SetSakkora", x);
            }
            #endregion BLG_1354

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                // Iktatás az naplózódik az iktatásnál, az érkeztetést itt naplózzuk:

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id
                    , kuldemenyId, "EREC_KuldKuldemenyek", "KuldemenyNew").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            result = resIktatas;
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Hivatali kapus üzenet érkeztetése a megadott adatokkal, majd a keletkezett küldemény alszámra iktatása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kuldemenyAdatok"></param>
    /// <param name="emailBoritekAdatok"></param>
    /// <param name="erec_IraIktatoKonyvek_Id"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <param name="erec_UgyUgyiratdarabok"></param>
    /// <param name="erec_IraIratok"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <param name="ip"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result HivataliKapusErkeztetesIktatas_Alszamra(ExecParam execParam, EREC_KuldKuldemenyek kuldemenyAdatok, string eBeadvany_Id
                    , string erec_IraIktatoKonyvek_Id, string erec_UgyUgyiratok_Id, EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok,
                     EREC_IraIratok erec_IraIratok, EREC_HataridosFeladatok erec_HataridosFeladatok, IktatasiParameterek ip, ErkeztetesParameterek _ErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (ip == null) ip = new IktatasiParameterek();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(execParam, ref kuldemenyAdatok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            #region Érkeztetés

            EREC_KuldKuldemenyekService svcKuld = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam execParamErkeztetes = execParam.Clone();
            Result resErkeztetes = svcKuld.ErkeztetesHivataliKapubol(execParamErkeztetes, kuldemenyAdatok, eBeadvany_Id, null, _ErkeztetesParameterek);

            if (!String.IsNullOrEmpty(resErkeztetes.ErrorCode))
            {
                throw new ResultException(resErkeztetes);
            }

            string kuldemenyId = resErkeztetes.Uid;

            #region Visszatérési információk
            ErkeztetesIktatasResult erkeztetesResult = null;
            if (resErkeztetes.Record != null && resErkeztetes.Record is ErkeztetesIktatasResult)
            {
                erkeztetesResult = (ErkeztetesIktatasResult)resErkeztetes.Record;
            }
            #endregion

            #endregion

            #region Iktatasa

            // küldemény vonalkódjának átmásolása iratpéldányhoz, ha nincs:
            if (String.IsNullOrEmpty(ip.Iratpeldany_Vonalkod)
                && !String.IsNullOrEmpty(kuldemenyAdatok.BarCode))
            {
                ip.Iratpeldany_Vonalkod = kuldemenyAdatok.BarCode;
            }

            ExecParam execParamIktatas = execParam.Clone();
            ip.KuldemenyId = kuldemenyId;
            // iratpéldánynak vonalkód generálás beállítása
            ip.IratpeldanyVonalkodGeneralasHaNincs = true;

            //  jelleg beallitasa
            if (String.IsNullOrEmpty(erec_IraIratok.Jelleg))
            {
                erec_IraIratok.Jelleg = KodTarak.IRAT_JELLEG.E_mail_uzenet;
                erec_IraIratok.Updated.Jelleg = true;
            }

            Result resIktatas = this.BejovoIratIktatasa_Alszamra(execParamIktatas, erec_IraIktatoKonyvek_Id, erec_UgyUgyiratok_Id, erec_UgyUgyiratdarabok,
                                erec_IraIratok, erec_HataridosFeladatok, ip);

            if (!String.IsNullOrEmpty(resIktatas.ErrorCode))
            {
                throw new ResultException(resIktatas);
            }

            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = null;
            if (resIktatas.Record != null && resIktatas.Record is ErkeztetesIktatasResult)
            {
                iktatasResult = (ErkeztetesIktatasResult)resIktatas.Record;
            }
            resIktatas.Record = new ErkeztetesIktatasResult(erkeztetesResult, iktatasResult);
            #endregion

            #endregion

            #region BLG_1354
            SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
            sakkoraSvc.SetSakkora(execParamIktatas.Clone(), iktatasResult, Sakkora.NormalVagyAlszamraIktatas.AlszamraIktatas);
            #endregion BLG_1354

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                // Iktatás az naplózódik az iktatásnál, az érkeztetést itt naplózzuk:

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id
                    , kuldemenyId, "EREC_KuldKuldemenyek", "KuldemenyNew").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            result = resIktatas;
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #endregion

    #region Infoszab iktatás

    /// <summary>
    /// Belső irat főszámra iktatása
    /// </summary>
    /// <param name="_ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_IraIktatoKonyvek_Id">Az iktatókönyv Id-ja, ahová az ügyirat iktatásra kerül</param>
    /// <param name="_EREC_UgyUgyiratok">A létrehozandó ügyirat adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Ügyirat tárgya, Ügy típusa,stb...
    /// </param>
    /// <param name="_EREC_UgyUgyiratdarabok">A létrehozandó ügyiratdarab adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Leírás, Eljárási szakasz, stb...</param>
    /// <param name="_EREC_IraIratok">A létrehozandó irat adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Irat tárgya, Irat kategória, stb...</param>
    /// <param name="_EREC_HataridosFeladatok">Az új irathoz megadható kezelési feljegyzést tartalmazza.
    /// Megadása nem kötelező.</param>
    /// <param name="_EREC_PldIratPeldanyok">Az iktatás során létrehozandó iratpéldány adatait tartalmazza.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Címzett, Címzett címe, stb...</param>
    /// <param name="UgyiratpeldanySzukseges">Ha értéke true, plusz egy iratpéldány létrejön az ügyirat felelősének címezve</param>
    /// <param name="KeszitoPeldanyaSzukseges">Ha értéke true, plusz egy iratpéldány létrejön az iktatónak címezve</param>
    /// <returns>A művelet sikerességét jelző Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result InfoszabIratIktatasa(ExecParam _ExecParam, String _EREC_IraIktatoKonyvek_Id,
                                     EREC_UgyUgyiratok _EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                     EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok,
                                     IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Belső irat iktatása Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            IktatottVagyMunkaanyag iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Iktatottanyag;
            // Ha az iktatási paraméterekben megadták, akkor munkaanyagként kell létrehozni:
            if (ip != null && ip.MunkaPeldany == true)
            {
                iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Munkaanyag;
            }

            result = IratIktatasa(IktatasTipus.BelsoIratIktatas, NormalVagyAlszamraIktatas.NormalIktatas, iktatottVagyMunkaanyag,
                               _ExecParam, _EREC_IraIktatoKonyvek_Id, null, _EREC_UgyUgyiratok,
                               _EREC_UgyUgyiratdarabok, _EREC_IraIratok, _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            string funkcio = "BelsoIratIktatas";
            // ha munkapéldány létrehozás volt megadva
            if (ip != null && ip.MunkaPeldany == true)
            {
                funkcio = "IktatasElokeszites";
            }

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, result.Uid
                , "EREC_IraIratok", funkcio).Record;

            eventLogService.Insert(_ExecParam, eventLogRecord);
            #endregion

            #region infoszab dokumentum feltöltése

            if (ip != null && ip.Dokumentum != null)
            {
                ExecParam xpmUpload = _ExecParam.Clone();
                string iratId = (result.Record as ErkeztetesIktatasResult).IratId;
                Result resUpload = this.UploadIktatottDokumentum(xpmUpload, iratId, ip.Dokumentum);

                if (resUpload.IsError)
                {
                    throw new ResultException(resUpload);
                }
            }

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }

    private Result UploadIktatottDokumentum(ExecParam execParam, string iratId, Csatolmany csatolmany)
    {
        Result res = new Result();
        try
        {
            Logger.Debug("Csatolmány feltöltése");
            EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();

            Logger.Debug(String.Format("IratId: {0}", iratId));

            erec_Csatolmanyok.IraIrat_Id = iratId;
            erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;

            ExecParam xpmUpload = execParam;
            xpmUpload.Record_Id = iratId;

            Result resUpload = this.CsatolmanyUpload(xpmUpload, erec_Csatolmanyok, csatolmany, true);

            if (resUpload.IsError)
            {
                Logger.Error("Csatolmány feltöltése hiba!", xpmUpload, resUpload);
                throw new ResultException(resUpload);
            }
        }
        catch (Exception ex)
        {
            res = ResultException.GetResultFromException(ex);
        }

        return res;
    }

    /// <summary>
    /// Infoszab irat iktatása alszámra
    /// </summary>
    /// <param name="_ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_IraIktatoKonyvek_Id">Az iktatókönyv Id-ja, ahová az irat iktatásra kerül abban az esetben,
    /// ha a megadott ügyiratnak nincs nyitott ügyiratdarabja.</param>
    /// <param name="_EREC_UgyUgyiratok_Id">Az ügyirat Id-ja, ahová az iratot iktatni szeretnénk</param>
    /// <param name="_EREC_UgyUgyiratdarabok">Az esetlegesen létrehozandó ügyiratdarab adatait tartalmazó objektum.
    /// Új ügyiratdarab csak akkor fog létrejönni, ha a megadott ügyiratnak nincs nyitott ügyiratdarabja.</param>
    /// <param name="_EREC_IraIratok">A létrehozandó irat adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Irat tárgya, Irat kategória, stb...</param>
    /// <param name="_EREC_HataridosFeladatok">Az új irathoz megadható kezelési feljegyzést tartalmazza.
    /// Megadása nem kötelező.</param>
    /// <param name="_EREC_PldIratPeldanyok">Az iktatás során létrehozandó iratpéldány adatait tartalmazza.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Címzett, Címzett címe, stb...</param>
    /// <param name="UgyiratpeldanySzukseges">Ha értéke true, plusz egy iratpéldány létrejön az ügyirat felelősének címezve</param>
    /// <param name="KeszitoPeldanyaSzukseges">Ha értéke true, plusz egy iratpéldány létrejön az iktatónak címezve</param>
    /// <param name="UgyiratUjranyitasaHaLezart">Ha értéke 'true': ha lezárt az ügyirat, ügyirat újranyitása, irat új ügyiratdarabba iktatása;
    /// Ha értéke 'false': ha lezárt az ügyirat, nem kell újranyitni, hanem az utolsó (lezárt) ügyiratdarabba iktatunk</param>
    /// <returns>A művelet sikerességét jelző Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result InfoszabIratIktatasa_Alszamra(ExecParam _ExecParam, String _EREC_IraIktatoKonyvek_Id, String _EREC_UgyUgyiratok_Id,
                                              EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                              EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok,
                                              IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Belső irat iktatása alszámra Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Iktathat-e a luzer ebbe az ügyiratba

            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);

            ExecParam ugyiratGetExecParam = _ExecParam.Clone();
            ugyiratGetExecParam.Record_Id = _EREC_UgyUgyiratok_Id;

            Result ugyiratGetResult = ugyiratService.GetWithRightCheck(ugyiratGetExecParam, 'O');
            if (!string.IsNullOrEmpty(ugyiratGetResult.ErrorCode))
            {
                Result rsResult = new Result();
                rsResult.ErrorCode = "1";
                rsResult.ErrorMessage = "Nem iktathat ebbe az ügyiratba!";
                return rsResult;
            }

            #endregion Iktathat-e a luzer ebbe az ügyiratba

            IktatottVagyMunkaanyag iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Iktatottanyag;
            // Ha az iktatási paraméterekben megadták, akkor munkaanyagként kell létrehozni:
            if (ip != null && ip.MunkaPeldany == true)
            {
                iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Munkaanyag;
            }

            result = IratIktatasa(IktatasTipus.BelsoIratIktatas, NormalVagyAlszamraIktatas.AlszamraIktatas, iktatottVagyMunkaanyag,
                _ExecParam, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok_Id, null, _EREC_UgyUgyiratdarabok,
                _EREC_IraIratok, _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            string funkcio = "BelsoIratIktatasCsakAlszamra";
            // ha munkapéldány létrehozás volt megadva
            if (ip != null && ip.MunkaPeldany == true)
            {
                funkcio = "IktatasElokeszites";
            }

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, result.Uid
                , "EREC_IraIratok", funkcio).Record;

            eventLogService.Insert(_ExecParam, eventLogRecord);
            #endregion

            #region infoszab dokumentum feltöltése

            if (ip != null && ip.Dokumentum != null)
            {
                ExecParam xpmUpload = _ExecParam.Clone();
                string iratId = (result.Record as ErkeztetesIktatasResult).IratId;
                Result resUpload = this.UploadIktatottDokumentum(xpmUpload, iratId, ip.Dokumentum);

                if (resUpload.IsError)
                {
                    throw new ResultException(resUpload);
                }
            }

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }

    #endregion


    /// <summary>
    /// Bejövő irat iktatása (Nem alszámra iktatás)
    /// </summary>
    /// <param name="_ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_IraIktatoKonyvek_Id">Az iktatókönyv Id-ja, ahová az ügyirat iktatásra kerül</param>
    /// <param name="_EREC_UgyUgyiratok">A létrehozandó ügyirat adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Ügyirat tárgya, Ügy típusa,stb...
    /// </param>
    /// <param name="_EREC_UgyUgyiratdarabok">A létrehozandó ügyiratdarab adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Leírás, Eljárási szakasz, stb...</param>
    /// <param name="_EREC_IraIratok">A létrehozandó irat adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Irat tárgya, Irat kategória, stb...</param>
    /// <param name="_EREC_HataridosFeladatok">Az új irathoz megadható kezelési feljegyzést tartalmazza.
    /// Megadása nem kötelező.</param>
    /// <param name="_EREC_KuldKuldemenyek_Id">Az iktatandó küldemény Id-ja</param>
    /// <param name="Ugykor_Id">A megadott ügykör id (Irattári tételszám id) (Megadása nem kötelező)</param>
    /// <param name="Ugytipus">Az ügykörön belül megadott ügytípus kód (Megadása nem kötelező)</param>
    /// <returns>A művelet sikerességét jelző Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result BejovoIratIktatasa(ExecParam _ExecParam, String _EREC_IraIktatoKonyvek_Id,
                                     EREC_UgyUgyiratok _EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                     EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok, IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Bejövő irat iktatása Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (ip == null) ip = new IktatasiParameterek();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(_ExecParam, _EREC_IraIratok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            #region Küldemény vizsgálata  (iktatható-e, illetve szignálástípus meghatározása)

            bool kuldemenyBeiktathato = false;
            string szignalasTipusa = "";
            string javasoltFelelos = "";

            // TODO: Bejövő irat iktatása, NEM Alszámra  
            // Küldemény vizsgálata, beiktatható-e: ha NEM Iktathato:  - új irat létrehozása és hozzákapcsolása a küldeményhez, de ez nem lesz beiktatva
            Arguments args = new Arguments();
            args.Add(new Argument("Kuldemény Id", ip.KuldemenyId, ArgumentTypes.String));
            args.ValidateArguments();

            EREC_KuldKuldemenyekService service_kuld = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam execParam_kuldGet = _ExecParam.Clone();
            execParam_kuldGet.Record_Id = ip.KuldemenyId;

            Result result_kuldGet = service_kuld.Get(execParam_kuldGet);
            if (!String.IsNullOrEmpty(result_kuldGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_kuldGet);
            }

            EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)result_kuldGet.Record;

            // Küldemény iktatható
            // elvileg nem állítjuk sehol 1-esbe, csak 0, S, vagy U lehet
            if (kuldemeny.Iktathato == "1")
            {
                kuldemenyBeiktathato = true;
            }

            EREC_IraIratok erec_IraIrat_elokeszitett = null;


            if (!String.IsNullOrEmpty(kuldemeny.IraIratok_Id))
            {
                // Irat lekérése:
                ExecParam execParam_iratGet = _ExecParam.Clone();
                execParam_iratGet.Record_Id = kuldemeny.IraIratok_Id;

                Result result_iratGet = this.Get(execParam_iratGet);
                if (!String.IsNullOrEmpty(result_iratGet.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_iratGet);
                }

                erec_IraIrat_elokeszitett = (EREC_IraIratok)result_iratGet.Record;

                Logger.Debug("Van már előkészített irat a küldeményhez", _ExecParam);
            }


            if (kuldemenyBeiktathato == false)
            {
                #region Szignálás típusának lekérése:

                // ha már iktatásra előkészített küldeményről van szó,
                // az Iktathato mezőjébe bejegyeztük a szignálás típusát 
                // TODO: ez csak egy gyors megoldás, az iratmetadefinicio alapján kellene lekérni a szignálástípust, de az egy sz*r
                if (kuldemeny.Iktathato == "U")
                {
                    // "U" --> 1-es típus (szervezetre, ügyintézőre szignálás)
                    szignalasTipusa = KodTarak.SZIGNALAS_TIPUSA._1_Szervezetre_Ugyintezore_Iktatas;
                }
                else if (kuldemeny.Iktathato == "S")
                {
                    // "S" --> 2-es típus (szervezetre szignálás)
                    szignalasTipusa = KodTarak.SZIGNALAS_TIPUSA._2_Szervezetre_Iktatas_Ugyintezore;
                }
                else
                {
                    // Szignálástípus lekérése az ügykör, ügytípus alapján:

                    if (String.IsNullOrEmpty(ip.UgykorId))
                    {
                        //szignalasTipusa = KodTarak.SZIGNALAS_TIPUSA._1_Szervezetre_Ugyintezore_Iktatas;
                        // default a 6-os:
                        szignalasTipusa = KodTarak.SZIGNALAS_TIPUSA._6_IktatoSzabadonValaszthat;
                    }
                    else
                    {
                        // SzignalasTipus meghatározása az Ugykor_Id, Ugytipus alapján:
                        // 1 és 2 kivételével beiktatható:

                        EREC_SzignalasiJegyzekekService service_szignJegyzek = new EREC_SzignalasiJegyzekekService(this.dataContext);

                        ExecParam execParam_szignTipus = _ExecParam.Clone();

                        Result result_szignalasTipus = service_szignJegyzek.GetSzignalasTipusaByUgykorUgytipus(execParam_szignTipus, ip.UgykorId, ip.Ugytipus);
                        if (!String.IsNullOrEmpty(result_szignalasTipus.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_szignalasTipus);
                        }

                        if (result_szignalasTipus.Record != null && (!String.IsNullOrEmpty(result_szignalasTipus.Record.ToString())))
                        {
                            szignalasTipusa = result_szignalasTipus.Record.ToString();
                            javasoltFelelos = result_szignalasTipus.Uid;

                            Logger.Info("Szignálás típusa: " + szignalasTipusa, _ExecParam);
                            Logger.Info("Javasolt felelős mező: " + javasoltFelelos, _ExecParam);
                        }
                        else
                        {
                            //szignalasTipusa = KodTarak.SZIGNALAS_TIPUSA._1_Szervezetre_Ugyintezore_Iktatas;
                            // default a 6-os:
                            szignalasTipusa = KodTarak.SZIGNALAS_TIPUSA._6_IktatoSzabadonValaszthat;
                        }

                    }
                }

                #endregion


                // van-e már irat a küldeményhez:
                if (erec_IraIrat_elokeszitett != null && kuldemenyBeiktathato == false)
                {
                    // Beiktatható-e:
                    if (
                        //(!String.IsNullOrEmpty(erec_IraIrat_elokeszitett.Csoport_Id_Ugyfelelos) && !String.IsNullOrEmpty(erec_IraIrat_elokeszitett.FelhasznaloCsoport_Id_Ugyintez))
                        //|| (szignalasTipusa == KodTarak.SZIGNALAS_TIPUSA._2_Szervezetre_Iktatas_Ugyintezore && !String.IsNullOrEmpty(erec_IraIrat_elokeszitett.Csoport_Id_Ugyfelelos))
                        //)
                        (!String.IsNullOrEmpty(erec_IraIrat_elokeszitett.Csoport_Id_Ugyfelelos) && Contentum.eRecord.BaseUtility.Iratok.IsElokeszitettIratSzignalt(erec_IraIrat_elokeszitett))
                        || (szignalasTipusa == KodTarak.SZIGNALAS_TIPUSA._2_Szervezetre_Iktatas_Ugyintezore && !String.IsNullOrEmpty(erec_IraIrat_elokeszitett.Csoport_Id_Ugyfelelos))
                        )
                    {
                        kuldemenyBeiktathato = true;
                    }
                    else
                    {
                        // hiba:
                        Logger.Warn("Küldemény iktatás: a küldeményt még nem szignálták szervezetre vagy ügyintézőre", _ExecParam);
                        throw new ResultException(52592);
                    }
                }

            }

            #endregion


            //if (kuldemenyBeiktathato == true
            //    || szignalasTipusa == KodTarak.SZIGNALAS_TIPUSA._3_Iktatas_Szervezetre_Ugyintezore
            //    || szignalasTipusa == KodTarak.SZIGNALAS_TIPUSA._4_NincsSzervezetreSzignalas)

            if (kuldemenyBeiktathato == true
                ||
                    (szignalasTipusa != KodTarak.SZIGNALAS_TIPUSA._1_Szervezetre_Ugyintezore_Iktatas
                    && szignalasTipusa != KodTarak.SZIGNALAS_TIPUSA._2_Szervezetre_Iktatas_Ugyintezore)
                )
            {
                #region Küldemény iktatásra előkészített adatainak kezelése

                if (erec_IraIrat_elokeszitett != null)
                {
                    // az előkészített iratból adatok átmásolása az iktatáshoz megadott ügyirat, irat objektumokba

                    if (_EREC_UgyUgyiratok == null) { _EREC_UgyUgyiratok = new EREC_UgyUgyiratok(); }
                    if (_EREC_IraIratok == null) { _EREC_IraIratok = new EREC_IraIratok(); }

                    if (String.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos))
                    {
                        _EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos = erec_IraIrat_elokeszitett.Csoport_Id_Ugyfelelos;
                        _EREC_UgyUgyiratok.Updated.Csoport_Id_Ugyfelelos = true;
                    }

                    if (String.IsNullOrEmpty(_EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez))
                    {
                        //_EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = erec_IraIrat_elokeszitett.FelhasznaloCsoport_Id_Ugyintez;
                        _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = Contentum.eRecord.BaseUtility.Iratok.GetBusinessObjectFieldFromXML(erec_IraIrat_elokeszitett.Base.Note, Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok, "FelhasznaloCsoport_Id_Ugyintez");
                        _EREC_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                    }

                    if (String.IsNullOrEmpty(_EREC_UgyUgyiratok.Targy))
                    {
                        //_EREC_UgyUgyiratok.Targy = erec_IraIrat_elokeszitett.Base.Note;
                        _EREC_UgyUgyiratok.Targy = Contentum.eRecord.BaseUtility.Iratok.GetBusinessObjectFieldFromXML(erec_IraIrat_elokeszitett.Base.Note, Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok, "Targy");
                        _EREC_UgyUgyiratok.Updated.Targy = true;
                    }

                    if (!String.IsNullOrEmpty(erec_IraIrat_elokeszitett.IratMetaDef_Id))
                    {
                        _EREC_UgyUgyiratok.IratMetadefinicio_Id = erec_IraIrat_elokeszitett.IratMetaDef_Id;
                        _EREC_UgyUgyiratok.Updated.IratMetadefinicio_Id = true;

                        #region IratMetaDef lekérése az irattári tételszámhoz:

                        Logger.Debug("IratMetaDef. lekérése az irattári tételszám ügyiratba másolásához", _ExecParam);

                        EREC_IratMetaDefinicioService service_IratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);
                        ExecParam execParam_iratMetaDefGet = _ExecParam.Clone();
                        execParam_iratMetaDefGet.Record_Id = erec_IraIrat_elokeszitett.IratMetaDef_Id;

                        Result result_iratMetaDefGet = service_IratMetaDef.Get(execParam_iratMetaDefGet);
                        if (!String.IsNullOrEmpty(result_iratMetaDefGet.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_iratMetaDefGet);
                        }

                        EREC_IratMetaDefinicio iratMetaDef = (EREC_IratMetaDefinicio)result_iratMetaDefGet.Record;

                        // Irattári tételszám:
                        if (!String.IsNullOrEmpty(iratMetaDef.Ugykor_Id))
                        {
                            _EREC_UgyUgyiratok.IraIrattariTetel_Id = iratMetaDef.Ugykor_Id;
                            _EREC_UgyUgyiratok.Updated.IraIrattariTetel_Id = true;
                        }

                        // Ügytipus:
                        if (!String.IsNullOrEmpty(iratMetaDef.Ugytipus))
                        {
                            _EREC_UgyUgyiratok.UgyTipus = iratMetaDef.Ugytipus;
                            _EREC_UgyUgyiratok.Updated.UgyTipus = true;
                        }
                        #endregion

                    }

                    // Id, Ver megadása, hogy update-elni lehessen:
                    _EREC_IraIratok.Id = erec_IraIrat_elokeszitett.Id;

                    _EREC_IraIratok.Base.Ver = erec_IraIrat_elokeszitett.Base.Ver;
                    _EREC_IraIratok.Base.Updated.Ver = true;
                }

                //// Nem kell, mert az iktató lehet hogy felüldefiniálta a javasolt ügyfelelőst
                //#region 4-es típusnál, ha volt javasolt felelős, azt állítjuk be felelősnek és ügyfelelősnek:

                //if (szignalasTipusa == KodTarak.SZIGNALAS_TIPUSA._4_NincsSzervezetreSzignalas
                //    && !String.IsNullOrEmpty(javasoltFelelos))
                //{
                //    Logger.Info("Szignálás típus: 4; van megadva javasolt felelős", _ExecParam);

                //    // elvileg a javasolt felelős egy szervezet; ha nem sikerül lekérni a vezetőjét,
                //    // akkor úgy tekintjük, mintha személy lenne
                //    #region Szervezet vezetőjének lekérése

                //    // szervezet vezetőjének lekérdezése:
                //    Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok =
                //        Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

                //    ExecParam execParam_getLeader = _ExecParam.Clone();

                //    string ujFelelos = "";

                //    Result result_GetLeader = service_csoportok.GetLeader(execParam_getLeader, javasoltFelelos);
                //    #endregion
                //    if (!String.IsNullOrEmpty(result_GetLeader.ErrorCode)
                //        || result_GetLeader.Record == null)
                //    {
                //        Logger.Warn("Szervezet vezetőjének megállapítása sikertelen. Szervezet: " + javasoltFelelos, _ExecParam);

                //        ujFelelos = javasoltFelelos;

                //    }
                //    else
                //    {
                //        KRT_Csoportok csoportVezeto = (KRT_Csoportok)result_GetLeader.Record;

                //        if (!String.IsNullOrEmpty(csoportVezeto.Id))
                //        {
                //            ujFelelos = csoportVezeto.Id;
                //        }
                //        else
                //        {
                //            ujFelelos = javasoltFelelos;
                //        }
                //    }

                //    _EREC_UgyUgyiratok.Csoport_Id_Felelos = ujFelelos;
                //    _EREC_UgyUgyiratok.Updated.Csoport_Id_Felelos = true;

                //    _EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos = ujFelelos;
                //    _EREC_UgyUgyiratok.Updated.Csoport_Id_Ugyfelelos = true;


                //}
                //#endregion

                #endregion

                // Ha nincs megadva a felelős (a szignálási jegyzékből nem lehetett megállapítani),
                // beállítjuk az iktató szervezet vezetőjét:
                if (String.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Felelos))
                {
                    #region Szervezet vezetőjének meghatározása

                    Logger.Info("Nincs megadva felelős (kezelő) --> Iktatő szervezet vezetőjének lekérése", _ExecParam);

                    string szervezetId = _ExecParam.FelhasznaloSzervezet_Id;

                    // Megadott szervezet vezetőjének lekérdezése:
                    Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok =
                        Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

                    ExecParam execParam_getLeader = _ExecParam.Clone();

                    Result result_GetLeader = service_csoportok.GetLeader(execParam_getLeader, szervezetId);
                    if (String.IsNullOrEmpty(result_GetLeader.ErrorCode) && result_GetLeader.Record != null)
                    {
                        KRT_Csoportok csoportVezeto = (KRT_Csoportok)result_GetLeader.Record;

                        _EREC_UgyUgyiratok.Csoport_Id_Felelos = csoportVezeto.Id;
                    }
                    else
                    {
                        // hiba:
                        Logger.Error("Nem határozható meg az iktató szervezet vezetője", _ExecParam);
                        throw new ResultException(52120);
                    }

                    #endregion
                }

                EREC_PldIratPeldanyok _EREC_PldIratPeldanyok = new EREC_PldIratPeldanyok();
                if (ip != null && !String.IsNullOrEmpty(ip.Iratpeldany_Vonalkod))
                {
                    _EREC_PldIratPeldanyok.BarCode = ip.Iratpeldany_Vonalkod;
                }

                // BUG_2051
                _EREC_PldIratPeldanyok.UgyintezesModja = kuldemeny.UgyintezesModja;

                IktatottVagyMunkaanyag iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Iktatottanyag;
                // Ha az iktatási paraméterekben megadták, akkor munkaanyagként kell létrehozni:
                if (ip != null && ip.MunkaPeldany == true)
                {
                    iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Munkaanyag;
                }

                result = IratIktatasa(IktatasTipus.BejovoIratIktatas, NormalVagyAlszamraIktatas.NormalIktatas, iktatottVagyMunkaanyag,
                                _ExecParam, _EREC_IraIktatoKonyvek_Id, null, _EREC_UgyUgyiratok,
                                _EREC_UgyUgyiratdarabok, _EREC_IraIratok, _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip);

                #region Visszatérési információk
                ErkeztetesIktatasResult iktatasResult = null;
                if (result.Record != null && result.Record is ErkeztetesIktatasResult)
                {
                    iktatasResult = (ErkeztetesIktatasResult)result.Record;
                }
                #endregion

                #region BLG_1354
                SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
                sakkoraSvc.SetSakkora(_ExecParam.Clone(), iktatasResult, Sakkora.NormalVagyAlszamraIktatas.NormalIktatas);
                #endregion BLG_1354
            }
            else
            {
                // a sorosítás eredménye sajnos nem fér be a Note mezőbe...
                //_EREC_IraIratok.Base.Note = Contentum.eUtility.XmlFunction.ObjectToXml(_EREC_UgyUgyiratok, false);

                EREC_PldIratPeldanyok _EREC_PldIratPeldanyok = new EREC_PldIratPeldanyok();
                if (ip != null && !String.IsNullOrEmpty(ip.Iratpeldany_Vonalkod))
                {
                    _EREC_PldIratPeldanyok.BarCode = ip.Iratpeldany_Vonalkod;
                }

                if (String.IsNullOrEmpty(_EREC_UgyUgyiratok.IraIktatokonyv_Id))
                {
                    _EREC_UgyUgyiratok.IraIktatokonyv_Id = _EREC_IraIktatoKonyvek_Id;
                }


                // irat további jellemzőinek átvétele:
                _EREC_IraIratok.Base.Note = Contentum.eRecord.BaseUtility.Iratok.GetBusinessObjectsXMLForKuldemenyElokeszites(_EREC_IraIratok.Base.Note, _EREC_UgyUgyiratok, _EREC_PldIratPeldanyok);
                _EREC_IraIratok.Base.Updated.Note = true;

                _EREC_IraIratok.Base.Note = Contentum.eRecord.BaseUtility.Iratok.SetBusinessObjectFieldXMLForKuldemenyElokeszites(_EREC_IraIratok.Base.Note, Contentum.eUtility.Constants.TableNames.EREC_IraIratok, "FelhasznaloCsoport_Id_Ugyintez", _EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez);

                result = KuldemenyElokeszitesIktatasra(_ExecParam, kuldemeny, _EREC_IraIratok, szignalasTipusa, ip);
            }


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            string funkcio = "BejovoIratIktatas";
            // ha munkapéldány létrehozás volt megadva
            if (ip != null && ip.MunkaPeldany == true)
            {
                funkcio = "IktatasElokeszites";
            }

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_IraIratok", funkcio).Record;

            eventLogService.Insert(_ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }

    /// <summary>
    /// Segédeljárás
    /// A küldeményhez készít egy új még be nem iktatott iratot
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="kuldemeny"></param>
    /// <param name="Ugykor_Id"></param>
    /// <param name="Ugytipus"></param>
    /// <returns></returns>
    private Result KuldemenyElokeszitesIktatasra(ExecParam _ExecParam, EREC_KuldKuldemenyek kuldemeny
            , EREC_IraIratok iratAdatok_form, string SzignalasTipusa, IktatasiParameterek ip)
    {
        // Iktató szervezetének vezetőjét le kell kérdezni:
        if (String.IsNullOrEmpty(_ExecParam.FelhasznaloSzervezet_Id))
        {
            // hiba:
            Logger.Error("Nincs megadva a felhasználó szervezete", _ExecParam);
            throw new ResultException(52593);
        }

        if (ip == null) ip = new IktatasiParameterek();

        // Megadott szervezet vezetőjének lekérdezése:
        Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok =
            Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

        ExecParam execParam_getLeader = _ExecParam.Clone();

        Result result_GetLeader = service_csoportok.GetLeader(execParam_getLeader, _ExecParam.FelhasznaloSzervezet_Id);
        if (!String.IsNullOrEmpty(result_GetLeader.ErrorCode)
            || result_GetLeader.Record == null)
        {
            // hiba: Nincs meg a szervezet vezetője

            Logger.Error("Előkészített irat létrehozása: Nem határozható meg a szervezet vezetője: " + _ExecParam.FelhasznaloSzervezet_Id, _ExecParam);
            throw new ResultException(52594);
        }

        KRT_Csoportok csoportVezeto = (KRT_Csoportok)result_GetLeader.Record;


        #region Irat INSERT


        #region IratMetadefinicio id lekérése

        string iratMetaDef_Id = "";
        string generaltTargy = "";

        EREC_IratMetaDefinicioService service_iratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);

        Result result_iratMetaDef_Get = service_iratMetaDef.GetIratMetaDefinicioByUgykorUgytipus(_ExecParam, ip.UgykorId, ip.Ugytipus);
        if (!String.IsNullOrEmpty(result_iratMetaDef_Get.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_iratMetaDef_Get);
        }

        EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_iratMetaDef_Get.Record;
        if (erec_IratMetaDefinicio != null)
        {
            iratMetaDef_Id = erec_IratMetaDefinicio.Id;
            generaltTargy = erec_IratMetaDefinicio.GeneraltTargy;
        }

        #endregion

        EREC_IraIratok erec_IraIrat = iratAdatok_form;

        // küldemény hozzákapcsolása:
        erec_IraIrat.KuldKuldemenyek_Id = kuldemeny.Id;
        erec_IraIrat.Updated.KuldKuldemenyek_Id = true;

        // iktató:
        erec_IraIrat.FelhasznaloCsoport_Id_Iktato = Csoportok.GetFelhasznaloSajatCsoportId(_ExecParam);
        erec_IraIrat.Updated.FelhasznaloCsoport_Id_Iktato = true;

        // iratmetaDef_Id:
        erec_IraIrat.IratMetaDef_Id = iratMetaDef_Id;
        erec_IraIrat.Updated.IratMetaDef_Id = true;

        // GeneraltTargy:
        erec_IraIrat.GeneraltTargy = generaltTargy;
        erec_IraIrat.Updated.GeneraltTargy = true;

        ExecParam execParam_IratInsert = _ExecParam.Clone();

        Result result_iratInsert = this.Insert(execParam_IratInsert, erec_IraIrat);
        if (!String.IsNullOrEmpty(result_iratInsert.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_iratInsert);
        }

        string newIratId = result_iratInsert.Uid;
        erec_IraIrat.Id = newIratId;

        #endregion

        #region Kuldemeny UPDATE

        EREC_KuldKuldemenyekService service_kuld = new EREC_KuldKuldemenyekService(this.dataContext);

        kuldemeny.Updated.SetValueAll(false);
        kuldemeny.Base.Updated.SetValueAll(false);
        kuldemeny.Base.Updated.Ver = true;

        // Irat hozzákapcsolása:

        kuldemeny.IraIratok_Id = newIratId;
        kuldemeny.Updated.IraIratok_Id = true;

        if (iratAdatok_form.Updated.Minosites && iratAdatok_form.Minosites != kuldemeny.Minosites)
        {
            // minősítés átvétele az iratból
            kuldemeny.Minosites = iratAdatok_form.Minosites;
            kuldemeny.Updated.Minosites = true;
        }

        // Küldemény továbbítása az iktató szervezeti egység vezetőjének:
        if (!string.IsNullOrEmpty(csoportVezeto.Id))
        {
            kuldemeny.Csoport_Id_Felelos_Elozo = kuldemeny.Csoport_Id_Felelos;
            kuldemeny.Updated.Csoport_Id_Felelos_Elozo = true;

            kuldemeny.Csoport_Id_Felelos = csoportVezeto.Id;
            kuldemeny.Updated.Csoport_Id_Felelos = true;
        }

        // Iktathato mező beállítása a szignálástípus alapján
        // 'S': Szervezetre szignálás szükséges,
        // 'U': Szervezetre és Ügyintézőre szignálás is szükséges
        // ('1': Iktatható)
        if (SzignalasTipusa == KodTarak.SZIGNALAS_TIPUSA._1_Szervezetre_Ugyintezore_Iktatas)
        {
            kuldemeny.Iktathato = "U";
            kuldemeny.Updated.Iktathato = true;
        }
        else if (SzignalasTipusa == KodTarak.SZIGNALAS_TIPUSA._2_Szervezetre_Iktatas_Ugyintezore)
        {
            kuldemeny.Iktathato = "S";
            kuldemeny.Updated.Iktathato = true;
        }



        ExecParam execParam_kuldUpdate = _ExecParam.Clone();
        execParam_kuldUpdate.Record_Id = kuldemeny.Id;

        Result result_kuldUpdate = service_kuld.Update(execParam_kuldUpdate, kuldemeny);
        if (!String.IsNullOrEmpty(result_kuldUpdate.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_kuldUpdate);
        }
        #endregion

        // TODO: valahogy vissza kell jelezni a website felé, hogy 
        // nem történt iktatás, az iktatási adatok rögzültek, a küldemény továbbítva a szervezet vezetőjéhez:

        Result result = new Result();

        result.ErrorType = "NincsIktatva";
        //result.Record = kuldemeny;
        // A köv. felelőst adjuk vissza:
        result.Record = kuldemeny.Csoport_Id_Felelos;


        return result;


    }


    /// <summary>
    /// Belső irat főszámra iktatása
    /// </summary>
    /// <param name="_ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_IraIktatoKonyvek_Id">Az iktatókönyv Id-ja, ahová az ügyirat iktatásra kerül</param>
    /// <param name="_EREC_UgyUgyiratok">A létrehozandó ügyirat adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Ügyirat tárgya, Ügy típusa,stb...
    /// </param>
    /// <param name="_EREC_UgyUgyiratdarabok">A létrehozandó ügyiratdarab adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Leírás, Eljárási szakasz, stb...</param>
    /// <param name="_EREC_IraIratok">A létrehozandó irat adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Irat tárgya, Irat kategória, stb...</param>
    /// <param name="_EREC_HataridosFeladatok">Az új irathoz megadható kezelési feljegyzést tartalmazza.
    /// Megadása nem kötelező.</param>
    /// <param name="_EREC_PldIratPeldanyok">Az iktatás során létrehozandó iratpéldány adatait tartalmazza.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Címzett, Címzett címe, stb...</param>
    /// <param name="UgyiratpeldanySzukseges">Ha értéke true, plusz egy iratpéldány létrejön az ügyirat felelősének címezve</param>
    /// <param name="KeszitoPeldanyaSzukseges">Ha értéke true, plusz egy iratpéldány létrejön az iktatónak címezve</param>
    /// <returns>A művelet sikerességét jelző Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result BelsoIratIktatasa(ExecParam _ExecParam, String _EREC_IraIktatoKonyvek_Id,
                                     EREC_UgyUgyiratok _EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                     EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok,
                                     IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Belső irat iktatása Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            IktatottVagyMunkaanyag iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Iktatottanyag;
            // Ha az iktatási paraméterekben megadták, akkor munkaanyagként kell létrehozni:
            if (ip != null && ip.MunkaPeldany == true)
            {
                iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Munkaanyag;
            }

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(_ExecParam, _EREC_IraIratok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            result = IratIktatasa(IktatasTipus.BelsoIratIktatas, NormalVagyAlszamraIktatas.NormalIktatas, iktatottVagyMunkaanyag,
                               _ExecParam, _EREC_IraIktatoKonyvek_Id, null, _EREC_UgyUgyiratok,
                               _EREC_UgyUgyiratdarabok, _EREC_IraIratok, _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = null;
            if (result.Record != null && result.Record is ErkeztetesIktatasResult)
            {
                iktatasResult = (ErkeztetesIktatasResult)result.Record;
            }
            #endregion

            #region BLG_1354
            SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
            sakkoraSvc.SetSakkora(_ExecParam.Clone(), iktatasResult, Sakkora.NormalVagyAlszamraIktatas.NormalIktatas);
            #endregion BLG_1354

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            string funkcio = "BelsoIratIktatas";
            // ha munkapéldány létrehozás volt megadva
            if (ip != null && ip.MunkaPeldany == true)
            {
                funkcio = "IktatasElokeszites";
            }

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, result.Uid
                , "EREC_IraIratok", funkcio).Record;

            eventLogService.Insert(_ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }

    /// <summary>
    /// Belső irat iktatása + ügyintézésre átvétel
    /// (SZÜR WS (SZURGetFoszamWS) miatt kell, egy tranzakcióban kell futniuk...)
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_IraIktatoKonyvek_Id"></param>
    /// <param name="_EREC_UgyUgyiratok"></param>
    /// <param name="_EREC_UgyUgyiratdarabok"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_HataridosFeladatok"></param>
    /// <param name="_EREC_PldIratPeldanyok"></param>
    /// <param name="ip"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result BelsoIratIktatasaUgyintezesreAtvetellel(ExecParam _ExecParam, String _EREC_IraIktatoKonyvek_Id,
                                    EREC_UgyUgyiratok _EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                    EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok,
                                    IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Belső irat iktatása Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Belső irat iktatása:
            result = this.BelsoIratIktatasa(_ExecParam, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok, _EREC_UgyUgyiratdarabok, _EREC_IraIratok, _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip);
            if (result.IsError)
            {
                throw new ResultException(result);
            }

            // Ügyintézésre átvétel:
            ErkeztetesIktatasResult iktatasResult = (ErkeztetesIktatasResult)result.Record;
            string ujUgyiratId = iktatasResult.UgyiratId;

            ExecParam execParamFelelos = _ExecParam.Clone();
            execParamFelelos.Felhasznalo_Id = _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
            execParamFelelos.LoginUser_Id = execParamFelelos.Felhasznalo_Id;
            var resultAtvetelUgyintezesre = new EREC_UgyUgyiratokService(this.dataContext).AtvetelUgyintezesre(execParamFelelos, ujUgyiratId);
            if (resultAtvetelUgyintezesre.IsError)
            {
                throw new ResultException(resultAtvetelUgyintezesre);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }



    /// <summary>
    /// Bejövő irat iktatása alszámra
    /// </summary>
    /// <param name="_ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_IraIktatoKonyvek_Id">Az iktatókönyv Id-ja, ahová az irat iktatásra kerül abban az esetben,
    /// ha a megadott ügyiratnak nincs nyitott ügyiratdarabja.</param>
    /// <param name="_EREC_UgyUgyiratok_Id">Az ügyirat Id-ja, ahová az iratot iktatni szeretnénk</param>
    /// <param name="_EREC_UgyUgyiratdarabok">Az esetlegesen létrehozandó ügyiratdarab adatait tartalmazó objektum.
    /// Új ügyiratdarab csak akkor fog létrejönni, ha a megadott ügyiratnak nincs nyitott ügyiratdarabja.</param>
    /// <param name="_EREC_IraIratok">A létrehozandó irat adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Irat tárgya, Irat kategória, stb...</param>
    /// <param name="_EREC_HataridosFeladatok">Az új irathoz megadható kezelési feljegyzést tartalmazza.
    /// Megadása nem kötelező.</param>
    /// <param name="_EREC_KuldKuldemenyek_Id">Az iktatandó küldemény Id-ja</param>
    /// <param name="UgyiratUjranyitasaHaLezart">Ha értéke 'true': ha lezárt az ügyirat, ügyirat újranyitása, irat új ügyiratdarabba iktatása;
    /// Ha értéke 'false': ha lezárt az ügyirat, nem kell újranyitni, hanem az utolsó (lezárt) ügyiratdarabba iktatunk</param>
    /// <returns>A művelet sikerességét jelző Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result BejovoIratIktatasa_Alszamra(ExecParam _ExecParam, String _EREC_IraIktatoKonyvek_Id, String _EREC_UgyUgyiratok_Id,
                                              EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                              EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok, IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Bejövő irat iktatása alszámra Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (ip == null) ip = new IktatasiParameterek();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(_ExecParam, _EREC_IraIratok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            #region Iktathat-e a luzer ebbe az ügyiratba

            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);

            ExecParam ugyiratGetExecParam = _ExecParam.Clone();
            ugyiratGetExecParam.Record_Id = _EREC_UgyUgyiratok_Id;

            Result ugyiratGetResult = ugyiratService.GetWithRightCheck(ugyiratGetExecParam, 'O');
            if (!string.IsNullOrEmpty(ugyiratGetResult.ErrorCode))
            {
                Result rsResult = new Result();
                rsResult.ErrorCode = "1";
                rsResult.ErrorMessage = "Nem iktathat ebbe az ügyiratba!";
                return rsResult;
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)ugyiratGetResult.Record;

            #endregion Iktathat-e a luzer ebbe az ügyiratba

            EREC_PldIratPeldanyok _EREC_PldIratPeldanyok = new EREC_PldIratPeldanyok(); ;
            if (ip != null && !String.IsNullOrEmpty(ip.Iratpeldany_Vonalkod))
            {
                _EREC_PldIratPeldanyok.BarCode = ip.Iratpeldany_Vonalkod;
            }

            IktatottVagyMunkaanyag iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Iktatottanyag;
            // Ha az iktatási paraméterekben megadták, akkor munkaanyagként kell létrehozni:
            if (ip != null && ip.MunkaPeldany == true)
            {
                iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Munkaanyag;
            }

            result = IratIktatasa(IktatasTipus.BejovoIratIktatas, NormalVagyAlszamraIktatas.AlszamraIktatas, iktatottVagyMunkaanyag,
                                _ExecParam, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok_Id, null, _EREC_UgyUgyiratdarabok,
                                _EREC_IraIratok, _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = null;
            if (result.Record != null && result.Record is ErkeztetesIktatasResult)
            {
                iktatasResult = (ErkeztetesIktatasResult)result.Record;
            }
            #endregion

            #region BLG_1354
            SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
            sakkoraSvc.SetSakkora(_ExecParam.Clone(), iktatasResult, Sakkora.NormalVagyAlszamraIktatas.AlszamraIktatas);
            #endregion BLG_1354

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            string funkcio = "BejovoIratIktatasCsakAlszamra";
            // ha munkapéldány létrehozás volt megadva
            if (ip != null && ip.MunkaPeldany == true)
            {
                funkcio = "IktatasElokeszites";
            }

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_IraIratok", funkcio).Record;

            eventLogService.Insert(_ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }

    /// <summary>
    /// Belső irat iktatása alszámra
    /// </summary>
    /// <param name="_ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_IraIktatoKonyvek_Id">Az iktatókönyv Id-ja, ahová az irat iktatásra kerül abban az esetben,
    /// ha a megadott ügyiratnak nincs nyitott ügyiratdarabja.</param>
    /// <param name="_EREC_UgyUgyiratok_Id">Az ügyirat Id-ja, ahová az iratot iktatni szeretnénk</param>
    /// <param name="_EREC_UgyUgyiratdarabok">Az esetlegesen létrehozandó ügyiratdarab adatait tartalmazó objektum.
    /// Új ügyiratdarab csak akkor fog létrejönni, ha a megadott ügyiratnak nincs nyitott ügyiratdarabja.</param>
    /// <param name="_EREC_IraIratok">A létrehozandó irat adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Irat tárgya, Irat kategória, stb...</param>
    /// <param name="_EREC_HataridosFeladatok">Az új irathoz megadható kezelési feljegyzést tartalmazza.
    /// Megadása nem kötelező.</param>
    /// <param name="_EREC_PldIratPeldanyok">Az iktatás során létrehozandó iratpéldány adatait tartalmazza.
    /// Csak a felhasználó által a felületen is kitölthető mezőket veszi figyelembe, mint pl. Címzett, Címzett címe, stb...</param>
    /// <param name="UgyiratpeldanySzukseges">Ha értéke true, plusz egy iratpéldány létrejön az ügyirat felelősének címezve</param>
    /// <param name="KeszitoPeldanyaSzukseges">Ha értéke true, plusz egy iratpéldány létrejön az iktatónak címezve</param>
    /// <param name="UgyiratUjranyitasaHaLezart">Ha értéke 'true': ha lezárt az ügyirat, ügyirat újranyitása, irat új ügyiratdarabba iktatása;
    /// Ha értéke 'false': ha lezárt az ügyirat, nem kell újranyitni, hanem az utolsó (lezárt) ügyiratdarabba iktatunk</param>
    /// <returns>A művelet sikerességét jelző Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result BelsoIratIktatasa_Alszamra(ExecParam _ExecParam, String _EREC_IraIktatoKonyvek_Id, String _EREC_UgyUgyiratok_Id,
                                              EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                              EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok,
                                              IktatasiParameterek ip) //BLG_237, String eMailBoritekok_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Belső irat iktatása alszámra Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Iktathat-e a luzer ebbe az ügyiratba

            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);

            ExecParam ugyiratGetExecParam = _ExecParam.Clone();
            ugyiratGetExecParam.Record_Id = _EREC_UgyUgyiratok_Id;

            Result ugyiratGetResult = ugyiratService.GetWithRightCheck(ugyiratGetExecParam, 'O');
            if (!string.IsNullOrEmpty(ugyiratGetResult.ErrorCode))
            {
                Result rsResult = new Result();
                rsResult.ErrorCode = "1";
                rsResult.ErrorMessage = "Nem iktathat ebbe az ügyiratba!";
                return rsResult;
            }

            #endregion Iktathat-e a luzer ebbe az ügyiratba

            IktatottVagyMunkaanyag iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Iktatottanyag;
            // Ha az iktatási paraméterekben megadták, akkor munkaanyagként kell létrehozni:
            if (ip != null && ip.MunkaPeldany == true)
            {
                iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Munkaanyag;
            }

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(_ExecParam, _EREC_IraIratok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            result = IratIktatasa(IktatasTipus.BelsoIratIktatas, NormalVagyAlszamraIktatas.AlszamraIktatas, iktatottVagyMunkaanyag,
                _ExecParam, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok_Id, null, _EREC_UgyUgyiratdarabok,
                _EREC_IraIratok, _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip); // BLG_237, eMailBoritekok_Id);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = null;
            if (result.Record != null && result.Record is ErkeztetesIktatasResult)
            {
                iktatasResult = (ErkeztetesIktatasResult)result.Record;
            }
            #endregion

            #region BLG_1354
            SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
            sakkoraSvc.SetSakkora(_ExecParam.Clone(), iktatasResult, Sakkora.NormalVagyAlszamraIktatas.AlszamraIktatas);
            #endregion BLG_1354

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            string funkcio = "BelsoIratIktatasCsakAlszamra";
            // ha munkapéldány létrehozás volt megadva
            if (ip != null && ip.MunkaPeldany == true)
            {
                funkcio = "IktatasElokeszites";
            }

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, result.Uid
                , "EREC_IraIratok", funkcio).Record;

            eventLogService.Insert(_ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }




    /// <summary>
    /// Irat (küldemény) átiktatása
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="Irat_Id_AtIktatando"></param>
    /// <param name="_EREC_IraIktatoKonyvek_Id"></param>
    /// <param name="_EREC_UgyUgyiratok"></param>
    /// <param name="_EREC_UgyUgyiratdarabok"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_HataridosFeladatok"></param>
    /// <param name="_EREC_KuldKuldemenyek_Id"></param>
    /// <param name="Ugykor_Id"></param>
    /// <param name="Ugytipus"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result IratAtIktatasa(ExecParam _ExecParam, String Irat_Id_AtIktatando, String _EREC_IraIktatoKonyvek_Id,
                                     EREC_UgyUgyiratok _EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                     EREC_IraIratok _EREC_IraIratok, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok, EREC_HataridosFeladatok _EREC_HataridosFeladatok,
                                     IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Irat átiktatása Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(_ExecParam, _EREC_IraIratok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            result = IratAtIktatasa_segedFv(NormalVagyAlszamraIktatas.NormalIktatas, _ExecParam, Irat_Id_AtIktatando, _EREC_IraIktatoKonyvek_Id
                , null, _EREC_UgyUgyiratok, _EREC_UgyUgyiratdarabok, _EREC_IraIratok, _EREC_PldIratPeldanyok
                , _EREC_HataridosFeladatok, ip);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            // BLG_248
            // CR3191 : EDOK-ban rollback-elt tranzakció visszagörgetése migrált állományban is
            // ha hiba volt és az EDOK-ban vissza lesz görgetve a tranzakció, akkor a migráltban is szét kell szerelni 
            //if (String.IsNullOrEmpty(ip.SzerelendoUgyiratId) && _EREC_UgyUgyiratok.Updated.RegirendszerIktatoszam && !String.IsNullOrEmpty(_EREC_UgyUgyiratok.RegirendszerIktatoszam))
            if (String.IsNullOrEmpty(ip.SzerelendoUgyiratId) && !String.IsNullOrEmpty(ip.RegiAdatAzonosito))
            {
                Logger.Debug("EREC_UgyUgyiratokServiceService Migralt SzétSzerelés (FoszamSzerelesVisszavonasa) start. RégirendszerIktatoszam: " + ip.RegiAdatAzonosito);
                EREC_UgyUgyiratokService.FoszamSzerelesVisszavonasa(_ExecParam, ip.RegiAdatAzonosito);
            }
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }




    /// <summary>
    ///  Irat (küldemény) átiktatása (Alszámos iktatás)
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="Irat_Id_AtIktatando"></param>
    /// <param name="_EREC_IraIktatoKonyvek_Id"></param>
    /// <param name="_EREC_UgyUgyiratok_Id"></param>
    /// <param name="_EREC_UgyUgyiratdarabok"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_HataridosFeladatok"></param>
    /// <param name="UgyiratUjranyitasaHaLezart"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result IratAtIktatasa_Alszamra(ExecParam _ExecParam, String Irat_Id_AtIktatando, String _EREC_IraIktatoKonyvek_Id, String _EREC_UgyUgyiratok_Id,
                                              EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                              EREC_IraIratok _EREC_IraIratok, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok, EREC_HataridosFeladatok _EREC_HataridosFeladatok,
                                              IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Bejövő irat iktatása alszámra Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region E-IRAT-BARCODE-GEN
            VonalkodGeneralasElektronikusIratEsParameterEseten(_ExecParam, _EREC_IraIratok, ref ip);
            #endregion E-IRAT-BARCODE-GEN

            result = IratAtIktatasa_segedFv(NormalVagyAlszamraIktatas.AlszamraIktatas, _ExecParam, Irat_Id_AtIktatando, _EREC_IraIktatoKonyvek_Id
                        , _EREC_UgyUgyiratok_Id, null, _EREC_UgyUgyiratdarabok, _EREC_IraIratok, _EREC_PldIratPeldanyok
                        , _EREC_HataridosFeladatok, ip);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }





    private Result IratAtIktatasa_segedFv(NormalVagyAlszamraIktatas Normal_vagy_AlszamraIktatas,
                               ExecParam _ExecParam, String Irat_Id_AtIktatando, String _EREC_IraIktatoKonyvek_Id, String _EREC_UgyUgyiratok_Id,
                               EREC_UgyUgyiratok _EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                               EREC_IraIratok _EREC_IraIratok, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok, EREC_HataridosFeladatok _EREC_HataridosFeladatok,
                               IktatasiParameterek ip)
    {
        Logger.Info("Irat átiktatása - Start", _ExecParam);
        if (ip == null) ip = new IktatasiParameterek();

        #region Irat ellenőrzés

        EREC_IraIratok irat_Atiktatando = null;
        EREC_UgyUgyiratdarabok obj_UgyiratDarab = null;
        EREC_UgyUgyiratok obj_Ugyiratok = null;

        Result result_iratGet = this.GetObjectsHierarchyByIrat(_ExecParam, Irat_Id_AtIktatando, out irat_Atiktatando, out obj_UgyiratDarab
            , out obj_Ugyiratok);

        if (!String.IsNullOrEmpty(result_iratGet.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_iratGet);
        }

        // Elsődleges iratpéldány lekérése (kell az átiktathatóság ellenőrzéséhez)
        EREC_PldIratPeldanyokService service_pld = new EREC_PldIratPeldanyokService(this.dataContext);
        Result result_elsoPldGet = service_pld.GetElsoIratPeldanyByIraIrat(_ExecParam.Clone(), Irat_Id_AtIktatando);
        if (result_elsoPldGet.IsError)
        {
            // hiba:
            throw new ResultException(result_elsoPldGet);
        }
        EREC_PldIratPeldanyok obj_elsoPld = (EREC_PldIratPeldanyok)result_elsoPldGet.Record;

        Contentum.eRecord.BaseUtility.Iratok.Statusz iratStatusz = null;
        if (obj_elsoPld == null)
        {
            iratStatusz = Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(irat_Atiktatando);
        }
        else
        {
            iratStatusz = Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(irat_Atiktatando, obj_elsoPld);
        }
        Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
            Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(obj_Ugyiratok);

        ErrorDetails errorDetail = null;
        if (Contentum.eRecord.BaseUtility.Iratok.AtIktathato(iratStatusz, ugyiratStatusz, _ExecParam, out errorDetail) == false
            //|| irat_Atiktatando.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Bejovo
            //|| String.IsNullOrEmpty(irat_Atiktatando.KuldKuldemenyek_Id))                        
            )
        {
            // Nem lehet átiktatni:
            Logger.Error("Az iratot nem lehet átiktatni", _ExecParam);

            throw new ResultException(52621, errorDetail);
        }

        #endregion

        // BUG_12060
        if (iratStatusz.UgyiratId == _EREC_UgyUgyiratok_Id)
        {
            Logger.Error("Az önmagába átiktatás nem lehetséges!", _ExecParam);

            throw new ResultException(52875, errorDetail);

        }
        EREC_KuldKuldemenyek kuldemeny_Atiktatando = null;

        if (irat_Atiktatando.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
        {
            #region Küldemény Get

            EREC_KuldKuldemenyekService service_Kuld = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam execParam_kuldGet = _ExecParam.Clone();
            execParam_kuldGet.Record_Id = irat_Atiktatando.KuldKuldemenyek_Id;
            ip.KuldemenyId = irat_Atiktatando.KuldKuldemenyek_Id;

            Result result_KuldGet = service_Kuld.Get(execParam_kuldGet);
            if (!String.IsNullOrEmpty(result_KuldGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_KuldGet);
            }

            kuldemeny_Atiktatando = (EREC_KuldKuldemenyek)result_KuldGet.Record;

            #endregion

            #region Küldemény Update

            kuldemeny_Atiktatando.Updated.SetValueAll(false);
            kuldemeny_Atiktatando.Base.Updated.SetValueAll(false);
            kuldemeny_Atiktatando.Base.Updated.Ver = true;

            // Amit update-elni kell:
            // Allapot, TovabbitasAlatt, Felelős, Őrző, Iktathato, IraIratId = null,

            kuldemeny_Atiktatando.Allapot = KodTarak.KULDEMENY_ALLAPOT.Erkeztetve;
            kuldemeny_Atiktatando.Updated.Allapot = true;

            kuldemeny_Atiktatando.TovabbitasAlattAllapot = "";
            kuldemeny_Atiktatando.Updated.TovabbitasAlattAllapot = true;

            kuldemeny_Atiktatando.Csoport_Id_Felelos_Elozo = kuldemeny_Atiktatando.Csoport_Id_Felelos;
            kuldemeny_Atiktatando.Updated.Csoport_Id_Felelos_Elozo = true;

            kuldemeny_Atiktatando.Csoport_Id_Felelos = Csoportok.GetFelhasznaloSajatCsoportId(_ExecParam);
            kuldemeny_Atiktatando.Updated.Csoport_Id_Felelos = true;

            kuldemeny_Atiktatando.FelhasznaloCsoport_Id_Orzo = Csoportok.GetFelhasznaloSajatCsoportId(_ExecParam);
            kuldemeny_Atiktatando.Updated.FelhasznaloCsoport_Id_Orzo = true;

            kuldemeny_Atiktatando.Iktathato = "0";
            kuldemeny_Atiktatando.Updated.Iktathato = true;

            kuldemeny_Atiktatando.IraIratok_Id = "";
            kuldemeny_Atiktatando.Updated.IraIratok_Id = true;

            ExecParam execParam_KuldUpdate = _ExecParam.Clone();
            execParam_KuldUpdate.Record_Id = kuldemeny_Atiktatando.Id;

            Result result_kuldUpdate = service_Kuld.Update(execParam_KuldUpdate, kuldemeny_Atiktatando);
            if (!String.IsNullOrEmpty(result_kuldUpdate.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_kuldUpdate);
            }
            #endregion
        }

        #region Irat Update

        irat_Atiktatando.Updated.SetValueAll(false);
        irat_Atiktatando.Base.Updated.SetValueAll(false);
        irat_Atiktatando.Base.Updated.Ver = true;

        // Állapot: Átiktatott

        irat_Atiktatando.Allapot = KodTarak.IRAT_ALLAPOT.Atiktatott;
        irat_Atiktatando.Updated.Allapot = true;

        ExecParam execParam_iratUpdate = _ExecParam.Clone();
        execParam_iratUpdate.Record_Id = irat_Atiktatando.Id;

        Result result_iratUpdate = this.Update(execParam_iratUpdate, irat_Atiktatando);
        if (!String.IsNullOrEmpty(result_iratUpdate.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_iratUpdate);
        }

        #endregion

        #region DecrementIratszam
        Logger.Debug("DecrementIratszam start");
        //ügyirat újra lekérése, hogy stimmeljen a verzió
        obj_Ugyiratok = GetUgyiratByIrat(_ExecParam.Clone(), irat_Atiktatando);
        obj_Ugyiratok.Updated.SetValueAll(false);
        obj_Ugyiratok.Base.Updated.SetValueAll(false);
        obj_Ugyiratok.Base.Updated.Ver = true;
        ExecParam xpmDecrmentIratszam = _ExecParam.Clone();
        EREC_UgyUgyiratokService svcUgyiratok = new EREC_UgyUgyiratokService(this.dataContext);

        if (!IsMunkairat(irat_Atiktatando))
        {
            int ujIratszam = svcUgyiratok.DecrementIratszam(xpmDecrmentIratszam, obj_Ugyiratok);
            Logger.Debug("DecrementIratszam end, UjIratszam: " + ujIratszam);
        }

        Logger.Debug("Ugyirat Update start");
        ExecParam execParam_ugyiratUpdate = _ExecParam.Clone();
        execParam_ugyiratUpdate.Record_Id = obj_Ugyiratok.Id;

        #region ügyirat jellegének állítása, ha szükséges

        obj_Ugyiratok = svcUgyiratok.SetUgyiratJellegEsUgyintezesModjaByIratokAndSzereltek(_ExecParam, obj_Ugyiratok, Irat_Id_AtIktatando);

        #endregion ügyirat jellegének állítása, ha szükséges

        Result result_ugyiratUpdate = svcUgyiratok.Update(execParam_ugyiratUpdate, obj_Ugyiratok);
        if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
        {
            // hiba:
            Logger.Error("Ugyirat Update hiba", execParam_ugyiratUpdate, result_ugyiratUpdate);
            throw new ResultException(result_ugyiratUpdate);
        }
        Logger.Debug("Ugyiratok Update end");
        #endregion

        Result result = new Result();
        ip.Atiktatas = true;
        ip.IratId = Irat_Id_AtIktatando;

        // Küldemény felszabadítva, meghívjuk rá a normál iktatási folyamatot:
        if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
        {
            if (irat_Atiktatando.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
            {
                result = this.BejovoIratIktatasa(_ExecParam, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok, _EREC_UgyUgyiratdarabok, _EREC_IraIratok
                        , _EREC_HataridosFeladatok, ip);
            }
            else
            {
                // belső irat:
                result = this.BelsoIratIktatasa(_ExecParam, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok, _EREC_UgyUgyiratdarabok, _EREC_IraIratok
                        , _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip);
            }
        }
        else
        {
            if (irat_Atiktatando.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
            {
                result = this.BejovoIratIktatasa_Alszamra(_ExecParam, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok_Id, _EREC_UgyUgyiratdarabok
                        , _EREC_IraIratok, _EREC_HataridosFeladatok, ip);
            }
            else
            {
                // belső irat:
                result = this.BelsoIratIktatasa_Alszamra(_ExecParam, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok_Id, _EREC_UgyUgyiratdarabok
                        , _EREC_IraIratok, _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip);
            }
        }

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            throw new ResultException(result);
        }

        #region Csatolmányok, mellékletek átmásolása az új iratba:

        if (String.IsNullOrEmpty(result.ErrorCode) && !String.IsNullOrEmpty(result.Uid))
        {
            CsatolmanyAtmozgatas(_ExecParam, irat_Atiktatando.Id, result.Uid);
            MellekletAtmozgatas(_ExecParam, irat_Atiktatando.Id, result.Uid);
            if (irat_Atiktatando.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso &&
                irat_Atiktatando.Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet)
            {
                EmailBoritekUpdate(_ExecParam, irat_Atiktatando.Id, result.Uid);
            }
        }

        #endregion

        #region Régi és új irat között kapcsolat létrehozása:

        string ujIratId = result.Uid;

        EREC_UgyiratObjKapcsolatokService service_objKapcsolatok = new EREC_UgyiratObjKapcsolatokService(this.dataContext);
        ExecParam execParam_objKapcs = _ExecParam.Clone();

        EREC_UgyiratObjKapcsolatok objKapcsolat = new EREC_UgyiratObjKapcsolatok();

        objKapcsolat.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Atiktatas;
        objKapcsolat.Updated.KapcsolatTipus = true;

        // Előzmény a régi irat
        objKapcsolat.Obj_Id_Elozmeny = Irat_Id_AtIktatando;
        objKapcsolat.Updated.Obj_Id_Elozmeny = true;

        // Típus:
        objKapcsolat.Obj_Type_Elozmeny = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;
        objKapcsolat.Updated.Obj_Type_Elozmeny = true;

        objKapcsolat.Obj_Id_Kapcsolt = ujIratId;
        objKapcsolat.Updated.Obj_Id_Kapcsolt = true;

        objKapcsolat.Obj_Type_Kapcsolt = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;
        objKapcsolat.Updated.Obj_Type_Kapcsolt = true;

        Result result_objKapcs = service_objKapcsolatok.Insert(execParam_objKapcs, objKapcsolat);
        if (!String.IsNullOrEmpty(result_objKapcs.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_objKapcs);
        }

        #endregion

        #region régi ügyirat lezárás/sztornózás, ha csak egy irat volt az ügyiratban, vagy az összes felszabadított/sztornózott

        if (ip.EmptyUgyiratSztorno && obj_Ugyiratok.IratSzam == "0")
        {
            // ÜGYIRAT UPDATE:
            EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);

            Result result_ugyiratSztorno = service_ugyiratok.Sztornozas(_ExecParam.Clone(), obj_Ugyiratok.Id, String.Empty);
            if (!String.IsNullOrEmpty(result_ugyiratSztorno.ErrorCode))
            {
                // hiba:
                Logger.Error("Hiba az ügyirat sztornózása során", _ExecParam.Clone(), result_ugyiratSztorno);
                throw new ResultException(result_ugyiratSztorno);
            }

            obj_Ugyiratok.Base.Ver = (int.Parse(obj_Ugyiratok.Base.Ver) + 1).ToString();
        }

        #endregion régi ügyirat lezárás/sztornózás, ha csak egy irat volt az ügyiratban, vagy az összes felszabadított/sztornózott


        #region Eseménynaplózás

        KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

        KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, Irat_Id_AtIktatando, "EREC_IraIratok", "AtIktatas").Record;

        Result res = eventLogService.Insert(_ExecParam, eventLogRecord);

        #endregion

        return result;

    }

    private void CsatolmanyAtmozgatas(ExecParam execParam, string IratId_honnan, string IratId_hova)
    {
        if (String.IsNullOrEmpty(IratId_honnan) || String.IsNullOrEmpty(IratId_hova))
        {
            return;
        }

        Logger.Info("Irat csatolmányainak átnozgatása az új iratba", execParam);

        EREC_CsatolmanyokService service_csat = new EREC_CsatolmanyokService(this.dataContext);

        EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();

        // lekérjük az átiktatandó irat összes csatolmányát, és az új irathoz kötjük:

        search.IraIrat_Id.Value = IratId_honnan;
        search.IraIrat_Id.Operator = Query.Operators.equals;

        Result result_getAll = service_csat.GetAll(execParam, search);
        if (!String.IsNullOrEmpty(result_getAll.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_getAll);
        }

        foreach (DataRow row in result_getAll.Ds.Tables[0].Rows)
        {
            // ellenőrzés:
            string regiIratId = row["IraIrat_Id"].ToString();
            if (regiIratId != IratId_honnan)
            {
                throw new ResultException(52622);
            }

            string Id = row["Id"].ToString();
            string Ver = row["Ver"].ToString();

            #region Update

            EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
            erec_Csatolmanyok.Updated.SetValueAll(false);
            erec_Csatolmanyok.Base.Updated.SetValueAll(false);

            erec_Csatolmanyok.Base.Ver = Ver;
            erec_Csatolmanyok.Base.Updated.Ver = true;

            erec_Csatolmanyok.IraIrat_Id = IratId_hova;
            erec_Csatolmanyok.Updated.IraIrat_Id = true;

            ExecParam execParam_update = execParam.Clone();
            execParam_update.Record_Id = Id;

            Result result_update = service_csat.Update(execParam_update, erec_Csatolmanyok);
            if (!String.IsNullOrEmpty(result_update.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_update);
            }

            #endregion
        }
    }
    private void MellekletAtmozgatas(ExecParam execParam, string IratId_honnan, string IratId_hova)
    {
        if (String.IsNullOrEmpty(IratId_honnan) || String.IsNullOrEmpty(IratId_hova))
        {
            return;
        }

        Logger.Info("Irat mellékleteinek átmozgatása az új iratba", execParam);

        // ha az új irathoz másolódtak át mellékletek, akkor azokat kitöröljük, mert azokat átmozgatnánk mégegyszer a régi iratból:
        EREC_MellekletekService service_mellekletek = new EREC_MellekletekService(this.dataContext);

        #region Melleklet invalidate

        EREC_MellekletekSearch search_ujIratMellekletei = new EREC_MellekletekSearch();

        search_ujIratMellekletei.IraIrat_Id.Value = IratId_hova;
        search_ujIratMellekletei.IraIrat_Id.Operator = Query.Operators.equals;

        Result result_getAll_UjIratM = service_mellekletek.GetAll(execParam, search_ujIratMellekletei);
        if (!String.IsNullOrEmpty(result_getAll_UjIratM.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_getAll_UjIratM);
        }

        foreach (DataRow row in result_getAll_UjIratM.Ds.Tables[0].Rows)
        {
            // ellenőrzés:
            string ujIratId = row["IraIrat_Id"].ToString();
            if (ujIratId != IratId_hova)
            {
                // hiba:
                throw new ResultException(52623);
            }
            string Id = row["Id"].ToString();

            #region Invalidate

            ExecParam execParam_Invalidate = execParam.Clone();
            execParam_Invalidate.Record_Id = Id;

            Result result_invalidate = service_mellekletek.Invalidate(execParam_Invalidate);
            if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_invalidate);
            }
            #endregion
        }

        #endregion


        EREC_MellekletekSearch search_regiIrat = new EREC_MellekletekSearch();

        // lekérjük az átiktatandó irat összes mellékletét, és az új irathoz kötjük:

        search_regiIrat.IraIrat_Id.Value = IratId_honnan;
        search_regiIrat.IraIrat_Id.Operator = Query.Operators.equals;

        Result result_getAll = service_mellekletek.GetAll(execParam, search_regiIrat);
        if (!String.IsNullOrEmpty(result_getAll.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_getAll);
        }

        foreach (DataRow row in result_getAll.Ds.Tables[0].Rows)
        {
            // ellenőrzés:
            string regiIratId = row["IraIrat_Id"].ToString();
            if (regiIratId != IratId_honnan)
            {
                throw new ResultException(52623);
            }

            string Id = row["Id"].ToString();
            string Ver = row["Ver"].ToString();

            #region Update

            EREC_Mellekletek iratMelleklet = new EREC_Mellekletek();
            iratMelleklet.Updated.SetValueAll(false);
            iratMelleklet.Base.Updated.SetValueAll(false);

            iratMelleklet.Base.Ver = Ver;
            iratMelleklet.Base.Updated.Ver = true;

            iratMelleklet.IraIrat_Id = IratId_hova;
            iratMelleklet.Updated.IraIrat_Id = true;

            ExecParam execParam_update = execParam.Clone();
            execParam_update.Record_Id = Id;

            Result result_update = service_mellekletek.Update(execParam_update, iratMelleklet);
            if (!String.IsNullOrEmpty(result_update.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_update);
            }

            #endregion
        }
    }
    private void EmailBoritekUpdate(ExecParam execParam, string IratId_honnan, string IratId_hova)
    {
        if (String.IsNullOrEmpty(IratId_honnan) || String.IsNullOrEmpty(IratId_hova))
        {
            return;
        }


        EREC_eMailBoritekokService svc = new EREC_eMailBoritekokService(this.dataContext);
        EREC_eMailBoritekokSearch search = new EREC_eMailBoritekokSearch();
        search.IraIrat_Id.Value = IratId_honnan;
        search.IraIrat_Id.Operator = Query.Operators.equals;
        Result result = svc.GetAll(execParam, search);
        if (result.IsError)
        {
            // hiba:
            throw new ResultException(result);
        }

        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            string Id = row["Id"].ToString();
            string Ver = row["Ver"].ToString();

            #region Email boríték update

            EREC_eMailBoritekok erec_eMailBoritek = new EREC_eMailBoritekok();
            erec_eMailBoritek.Updated.SetValueAll(false);
            erec_eMailBoritek.Base.Updated.SetValueAll(false);

            erec_eMailBoritek.Base.Ver = Ver;
            erec_eMailBoritek.Base.Updated.Ver = true;

            // IraIratId-t kell beállítani:
            erec_eMailBoritek.IraIrat_Id = IratId_hova;
            erec_eMailBoritek.Updated.IraIrat_Id = true;

            ExecParam execParam_emailborUpdate = execParam.Clone();
            execParam_emailborUpdate.Record_Id = Id;

            Result result_emailborUpdate = svc.Update(execParam_emailborUpdate, erec_eMailBoritek);
            if (result_emailborUpdate.IsError)
            {
                // hiba:
                throw new ResultException(result_emailborUpdate);
            }

            #endregion
        }

    }

    private enum IktatasTipus { BejovoIratIktatas, BelsoIratIktatas }
    private enum NormalVagyAlszamraIktatas { NormalIktatas, AlszamraIktatas }
    private enum IktatottVagyMunkaanyag { Iktatottanyag, Munkaanyag }
    private enum SzerelesMod { None, Elokeszites, Szereles } // nem kell szerelni, csak kapcsolat bejegyzés, szerelés

    //[WebMethod()]
    //public string BarkodGeneraloTeszt()
    //{
    //    KRT_BarkodokService service = new KRT_BarkodokService();
    //    ExecParam execParam = new ExecParam();
    //    execParam.Felhasznalo_Id = "F088E89E-7917-4D0B-B94F-F4DAC84F0F95";

    //    Result result = service.BarkodSav_Igenyles(execParam, "G", 1);
    //    if (!String.IsNullOrEmpty(result.ErrorCode))
    //    {
    //        return result.ErrorCode + "; " + result.ErrorMessage;
    //    }

    //    return result.Uid;

    //}


    /// <summary> 
    /// Bejövő irat iktatása és Belső irat iktatása egy fv.-en belül; paraméterben adható meg, melyik legyen
    /// Normál, vagy Alszámra iktatás is választható  ( ==> 4 funkció 1 fv.-ben)
    /// </summary>
    /// <param name="Bejovo_vagy_BelsoIratIktatas"></param>
    /// <param name="Normal_vagy_AlszamraIktatas"></param>
    /// <param name="Iktatott_vagy_Munkaanyag">Ha MUNKAANYAG, az irat NEM kap alszámot, csak előkészített irat lesz. CSAK BELSŐ ÉS ALSZÁMOS IKTATÁSNÁL LEHET (egyelőre)</param>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_IraIktatoKonyvek_Id">Normál (NEM Alszámra) iktatásnál kell</param>
    /// <param name="_EREC_UgyUgyiratok_Id">Alszámra iktatásnál kell</param>
    /// <param name="_EREC_UgyUgyiratok"></param>
    /// <param name="_EREC_UgyUgyiratdarabok">NEM KELL MEGADNI (2007.11.26)</param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_HataridosFeladatok"></param>
    /// <param name="_EREC_PldIratPeldanyok"></param>
    /// <param name="_EREC_KuldKuldemenyek_Id"></param>
    /// <param name="UgyiratpeldanySzukseges"></param>
    /// <param name="KeszitoPeldanyaSzukseges"></param>
    /// <param name="UgyiratUjranyitasaHaLezart">csak alszámra iktatásnál kell</param>  
    /// <returns></returns>
    private Result IratIktatasa(IktatasTipus Bejovo_vagy_BelsoIratIktatas, NormalVagyAlszamraIktatas Normal_vagy_AlszamraIktatas, IktatottVagyMunkaanyag Iktatott_vagy_Munkaanyag,
                               ExecParam _ExecParam, String _EREC_IraIktatoKonyvek_Id, String _EREC_UgyUgyiratok_Id,
                               EREC_UgyUgyiratok _EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                               EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok,
                               IktatasiParameterek ip) //BLG_237, String eMailBoritekok_Id)
    {
        #region Paraméterek ellenőrzése

        if (ip == null) ip = new IktatasiParameterek();

        // Paraméterek meglétének ellenőrzése:
        if (_ExecParam == null
            || (String.IsNullOrEmpty(_EREC_IraIktatoKonyvek_Id) && Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            || (String.IsNullOrEmpty(_EREC_IraIktatoKonyvek_Id) && Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag && Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
            || (String.IsNullOrEmpty(_EREC_UgyUgyiratok_Id) && Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas)
            || (_EREC_UgyUgyiratok == null && Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
            //|| _EREC_UgyUgyiratdarabok == null
            || _EREC_IraIratok == null
            || (_EREC_PldIratPeldanyok == null && Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas)
            || (String.IsNullOrEmpty(ip.KuldemenyId) && Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas))
        {

            Logger.Error("Iktatás: Paraméter megadás hiányos", _ExecParam);
            if (String.IsNullOrEmpty(_EREC_IraIktatoKonyvek_Id))
            {
                Logger.Error("Nincs megadva iktatókönyv", _ExecParam);

                throw new ResultException(52114);
            }
            if (_ExecParam == null || String.IsNullOrEmpty(_ExecParam.Felhasznalo_Id))
            {
                Logger.Error("ExecParam megadása helytelen", _ExecParam);
            }

            throw new ResultException(52100);
        }

        // Lehet bejövőnél is (2009.01.19)
        //// Munkaanyag csak belső irat iktatásnál lehet:
        //if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag
        //    && Bejovo_vagy_BelsoIratIktatas != IktatasTipus.BelsoIratIktatas)
        //{
        //    Logger.Error("Munkaanyag csak belső irat iktatásnál lehet", _ExecParam);
        //    // TODO: szöveg helyett hibakód
        //    throw new ResultException("Munkaanyag csak belső irat iktatásnál lehet");
        //}

        #endregion

        // van-e megadva felelős (Nem alszámra iktatásnál)
        if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas && String.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Felelos))
        {
            Logger.Error("Iktatás: Nincs megadva felelős", _ExecParam);

            throw new ResultException(52108);
        }


        bool iktatoKonyvLezart = false;
        bool iktatokonyvFolyosorszamos = false;
        bool ugyiratLezart = false;
        bool ugyiratUjranyitasaSzukseges = false;
        bool utolagosPotlasSzukseges = false;
        bool kelleRegiAdtaSzereles = false;

        // lezárt iktatókönyves előzmény és
        // alszámra indított iktatás esetén át kell venni a B1 metaadatokat a létrehozott új ügyiratba,
        // ha volt főszámra iktatás joga, akkor ez a felületről történik
        bool kellMetaadatAtvetel = false;

        // TÜK beállítás van-e:
        bool isTUK = Rendszerparameterek.GetBoolean(_ExecParam, Rendszerparameterek.TUK, false);

        SzerelesMod szerelesMod = SzerelesMod.None;

        EREC_IraIktatoKonyvekService erec_IraIktatoKonyvekService = new EREC_IraIktatoKonyvekService(this.dataContext);
        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = null;


        #region Ügyirat - Iktatókönyv állapot vizsgálata:

        // Alszámra Iktatás - ügyirat lekérés
        // Főszámra iktatás szereléssel - ügyirat lekérés

        EREC_UgyUgyiratok elozmenyUgyirat = null;

        if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas || !String.IsNullOrEmpty(ip.SzerelendoUgyiratId))
        {
            // Ugyirat lekérése:
            Logger.Debug("Ügyirat lekérés - Start", _ExecParam);

            string elozmenyUgyiratId = "";
            if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas)
            {
                elozmenyUgyiratId = _EREC_UgyUgyiratok_Id;
            }
            else
            {
                elozmenyUgyiratId = ip.SzerelendoUgyiratId;
            }

            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_ugyiratGet = _ExecParam.Clone();
            execParam_ugyiratGet.Record_Id = elozmenyUgyiratId;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_ugyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                #region Előzmény Ügyirat vizsgálat

                elozmenyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                if (elozmenyUgyirat == null)
                {
                    // hiba
                    throw new ResultException(52106);
                }

                // Állapot ellenőrzés: lehet-e az ügyirathoz iktatni?
                // (Munkapéldánynál más ellenőrzés kell)
                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(elozmenyUgyirat);
                ErrorDetails errorDetail = null;
                ExecParam xpm = _ExecParam.Clone();

                if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                {
                    if (Contentum.eRecord.BaseUtility.Ugyiratok.LehetAlszamraIktatni(xpm, ugyiratStatusz, out errorDetail) == false)
                    {
                        Logger.Warn("Állapotellenőrzés: alszámos iktatás nem lehetséges az ügyiratra", _ExecParam);

                        throw new ResultException(52109, errorDetail);
                    }
                }
                else if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                {
                    if (Contentum.eRecord.BaseUtility.Ugyiratok.LehetMunkapeldanytLetrehozni(ugyiratStatusz, out errorDetail) == false)
                    {
                        Logger.Warn("Állapotellenőrzés: munkapéldányt nem lehet létrehozni az ügyiratban", _ExecParam);

                        //Az ügyiratban nem hozható létre új munkapéldány!
                        throw new ResultException(52135, errorDetail);
                    }
                }

                // Ha LEZÁRT AZ ÜGYIRAT:
                if (Contentum.eRecord.BaseUtility.Ugyiratok.Lezart(ugyiratStatusz))
                {
                    ugyiratLezart = true;
                }

                #endregion

                #region Iktatókönyv vizsgálat (Előzmény ügyiraté)

                // előzmény ügyirat iktatókönyvének lekérése:
                erec_IraIktatoKonyvek = GetIktatokonyvById(elozmenyUgyirat.IraIktatokonyv_Id, _ExecParam);
                // Lezárt-e az iktatókönyv:                
                iktatoKonyvLezart = Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatoKonyvek, out errorDetail);
                if (errorDetail != null)
                {
                    throw new ResultException(52104, errorDetail); // iktatókönyv lekérése sikertelen
                }


                if (erec_IraIktatoKonyvek.IktSzamOsztas == KodTarak.IKT_SZAM_OSZTAS.Folyosorszamos)
                {
                    iktatokonyvFolyosorszamos = true;
                }

                //CERTOP
                //Éven túli iktatás irattári tételszám alapján

                EREC_IraIrattariTetelekService svcIrattariTetelek = new EREC_IraIrattariTetelekService(this.dataContext);
                ExecParam xpmIrattariTetelek = _ExecParam.Clone();
                xpmIrattariTetelek.Record_Id = elozmenyUgyirat.IraIrattariTetel_Id;

                Result resirattariTetelek = svcIrattariTetelek.Get(xpmIrattariTetelek);

                if (!resirattariTetelek.IsError)
                {
                    EREC_IraIrattariTetelek irattariTetel = resirattariTetelek.Record as EREC_IraIrattariTetelek;

                    if (irattariTetel != null && "1".Equals(irattariTetel.EvenTuliIktatas))
                    {
                        iktatokonyvFolyosorszamos = true;
                    }
                }

                //if (iktatoKonyvLezart && _EREC_UgyUgyiratok == null)
                //{

                //    Logger.Warn("Iktatókönyv ellenőrzés: lezárt iktatókönyvben lévő előzmény ügyirat esetén az átadott paramétereknek tartalmazniuk kell a létrehozandó új ügyirat adatait!", _ExecParam);

                //    throw new ResultException(52136);
                //}

                if (iktatoKonyvLezart)
                {
                    if (!iktatokonyvFolyosorszamos)
                    {
                        // szerelt vagy elő van készítve szerelésre
                        if (!String.IsNullOrEmpty(elozmenyUgyirat.UgyUgyirat_Id_Szulo))
                        {
                            Logger.Warn("Iktatókönyv/ügyirat ellenőrzés: Az ügyirat már szerelt vagy szerelésre előkészített, nem választható előzménynek!", _ExecParam);
                            throw new ResultException(52137);
                        }

                        // munkaanyag létrehozásánál csak előkészített szerelés lehet
                        if (Contentum.eRecord.BaseUtility.Ugyiratok.Szerelheto(ugyiratStatusz, _ExecParam.Clone(), out errorDetail)
                            && Iktatott_vagy_Munkaanyag != IktatottVagyMunkaanyag.Munkaanyag)
                        {
                            szerelesMod = SzerelesMod.Szereles;
                        }
                        else
                        {
                            szerelesMod = SzerelesMod.Elokeszites;
                        }
                    }
                }
                else if (!String.IsNullOrEmpty(ip.SzerelendoUgyiratId) && Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
                {
                    // hiba, nem lezárt iktatókönyvben lévő ügyiratot próbálunk szerelni iktatáskor
                    Logger.Warn("Iktatókönyv/ügyirat ellenőrzés: A szerelendő előzmény ügyirat nincs lezárt iktatókönyvben!", _ExecParam);
                    throw new ResultException("A szerelendő előzmény ügyirat nincs lezárt iktatókönyvben!");
                }

                //int iktatokonyv_ev = 0;
                //try
                //{
                //    iktatokonyv_ev = Int32.Parse(erec_IraIktatoKonyvek.Ev);
                //}
                //catch
                //{ 
                //    // hiba:
                //    Logger.Error("Hiba az iktatókönyv.Ev konvertálásakor: " + erec_IraIktatoKonyvek.Ev, _ExecParam);
                //    throw new ResultException(52104);
                //}

                //// Lezárt-e az iktatókönyv:
                //if (iktatokonyv_ev < DateTime.Today.Year)
                //{
                //    iktatoKonyvLezart = true;
                //}

                #endregion

                if (iktatoKonyvLezart == false)
                {
                    // ha az ügyirat lezárt:
                    if (ugyiratLezart == true)
                    {
                        if (ip.UgyiratUjranyitasaHaLezart == true)
                        {
                            ugyiratUjranyitasaSzukseges = true;
                        }
                        else
                        {
                            utolagosPotlasSzukseges = true;
                        }
                    }
                }
                else if (iktatoKonyvLezart == true && !iktatokonyvFolyosorszamos && Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas)
                {
                    kellMetaadatAtvetel = true;
                }
            }
        }

        // Létrehozható munkaanyag ilyenkor is, előkészített szereléssel (2009.01.21.)
        //// Munkaanyag nem hozható létre lezárt ügyiratba, vagy lezárt iktatókönyvű ügyiratba
        //if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag
        //    && (iktatoKonyvLezart || ugyiratLezart))
        //{
        //    Logger.Error("Munkaanyag nem hozható létre lezárt ügyiratba, vagy lezárt iktatókönyvű ügyiratba", _ExecParam);           
        //    throw new ResultException(52134);
        //}


        #endregion

        #region Barkod generálása az ügyirat számára (Nem alszámos iktatás esetén), illetve az iratpéldány(ok) számára, ha a vonalkódkezelés nem azonosító
        // áthelyezve az ügyirat - iktatókönyv vizsgálatok utánra, mert az iktatókönyv lezártsága esetén is szükséges a barkód generálás


        //Vonalkód update és kötés if a vonalkódkezelés azonosítós
        //Contentum.eAdmin.Service.KRT_ParameterekService service_parameterek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
        ExecParam execParam_KRT_Param = _ExecParam.Clone();
        //CR 3058
        string vonalkodkezeles = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim();

        string barkod_ugyirat_Id = "";
        string barkod_pld1_Id = "";
        string barkod_pld1_Ver = "";
        string barkod_pld2_Id = "";
        string barkod_pld3_Id = "";
        string barkod_ugyirat = "";
        string barkod_pld1 = "";
        string barkod_pld2 = "";
        string barkod_pld3 = "";
        //BLG 1974
        //Amennyiben a VONALKOD_KEZELES = SAVOS és a VONALKOD_KIMENO_GENERALT értéke is 1-es, akkor belső keletkezésű/kimenő irat iktatása esetén generálunk vonalkódot, ha nincs megadva
        string vonalkodKiGeneralt = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KIMENO_GENERALT).Trim();
        if (vonalkodkezeles.ToUpper().Trim().Equals("SAVOS") && vonalkodKiGeneralt.Trim().Equals("1") && (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas || _EREC_IraIratok.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno))
        {
            ip.IratpeldanyVonalkodGeneralasHaNincs = true;
        }
        if (!vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))//CR 3058
        {
            //try
            {
                KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                ExecParam execParam_barkod = _ExecParam.Clone();

                // Barkod az ügyiratnak:
                if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas
                    || (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas && !iktatokonyvFolyosorszamos && iktatoKonyvLezart == true))
                {
                    Result result_barkod_ugyirat = service_barkodok.BarkodGeneralas(execParam_barkod);
                    if (!String.IsNullOrEmpty(result_barkod_ugyirat.ErrorCode))
                    {
                        // hiba:
                        Logger.Error("Vonalkód generálás az ügyiratnak sikertelen", _ExecParam);
                        throw new ResultException(result_barkod_ugyirat);
                    }
                    else
                    {
                        barkod_ugyirat_Id = result_barkod_ugyirat.Uid;
                        barkod_ugyirat = (String)result_barkod_ugyirat.Record;
                    }
                }

                /// Barkod az első iratpéldánynak (ha nem adtunk meg az objektumban, és az ügyintézés módja elektronikus,
                /// vagy előírtuk a vonalkódgenerálást az iratpéldánynak)
                if (String.IsNullOrEmpty(_EREC_PldIratPeldanyok.BarCode)
                    && (_EREC_PldIratPeldanyok.UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir
                        || ip.IratpeldanyVonalkodGeneralasHaNincs == true)
                   )
                {
                    Result result_barkod_pld1 = service_barkodok.BarkodGeneralas(execParam_barkod);
                    if (!String.IsNullOrEmpty(result_barkod_pld1.ErrorCode))
                    {
                        // hiba:
                        Logger.Error("Vonalkód generálás az iratpéldánynak: Sikertelen", _ExecParam);
                        throw new ResultException(result_barkod_pld1);
                    }
                    else
                    {
                        barkod_pld1_Id = result_barkod_pld1.Uid;
                        barkod_pld1 = (String)result_barkod_pld1.Record;
                    }
                }
                else
                {
                    barkod_pld1 = _EREC_PldIratPeldanyok.BarCode;

                    if (!String.IsNullOrEmpty(barkod_pld1))
                    {
                        Logger.Info("Volt megadva vonalkód az iratpéldánynak: " + barkod_pld1, _ExecParam);

                        #region BarCode Id lekérése:

                        Result result_BarCodeGetPld1 = service_barkodok.GetBarcodeByValue(execParam_barkod, barkod_pld1);
                        if (!String.IsNullOrEmpty(result_BarCodeGetPld1.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_BarCodeGetPld1);
                        }

                        KRT_Barkodok barkodObj_pld1 = (KRT_Barkodok)result_BarCodeGetPld1.Record;

                        barkod_pld1_Id = barkodObj_pld1.Id;
                        barkod_pld1_Ver = barkodObj_pld1.Base.Ver;

                        #endregion
                    }
                }

                // Barkod az esetleges további példányoknak: (csak akkor generálunk, ha az ügyintézés módja elektronikus)
                if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas
                    && _EREC_PldIratPeldanyok.UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
                {
                    if (ip.UgyiratPeldanySzukseges == true)
                    {
                        Result result_barkod_pld2 = service_barkodok.BarkodGeneralas(execParam_barkod);
                        if (!String.IsNullOrEmpty(result_barkod_pld2.ErrorCode))
                        {
                            // hiba:
                            Logger.Error("Vonalkód generálás az iratpéldánynak(2): Sikertelen", _ExecParam);
                            throw new ResultException(result_barkod_pld2);
                        }
                        else
                        {
                            barkod_pld2_Id = result_barkod_pld2.Uid;
                            barkod_pld2 = (String)result_barkod_pld2.Record;
                        }
                    }

                    if (ip.KeszitoPeldanyaSzukseges == true)
                    {
                        Result result_barkod_pld3 = service_barkodok.BarkodGeneralas(execParam_barkod);
                        if (!String.IsNullOrEmpty(result_barkod_pld3.ErrorCode))
                        {
                            // hiba:
                            Logger.Error("Vonalkód generálás az iratpéldánynak(3): Sikertelen", _ExecParam);
                            throw new ResultException(result_barkod_pld3);
                        }
                        else
                        {
                            barkod_pld3_Id = result_barkod_pld3.Uid;
                            barkod_pld3 = (String)result_barkod_pld3.Record;
                        }
                    }
                }

            }
        }        //catch (Exception e)
        //{
        //    // Előfordulhatnak még hibák a vonalkód generálásnál; lenyeljük a hibát egyelőre
        //    Logger.Error("Hiba a vonalkód generálás során",_ExecParam);
        //    Logger.Error("",e);
        //}
        #endregion


        #region Küldemény ellenőrzés, UPDATE (Csak Bejövő irat iktatásnál)
        EREC_KuldKuldemenyek erec_KuldKuldemenyek = null;

        // Bejövő irat iktatásánál kell küldeményt megadni, Belső irat iktatásnál nem
        if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
        {
            // Küldemény ellenőrzése, és állapotának, minősítésének beállítása
            Result result_CheckAndSetKuldemeny = CheckAndSet_Kuldemeny(_ExecParam, ip.KuldemenyId, _EREC_IraIratok.Minosites, out erec_KuldKuldemenyek);
            if (!String.IsNullOrEmpty(result_CheckAndSetKuldemeny.ErrorCode))
            {
                Logger.Warn("A küldemény nem iktatható", _ExecParam);

                // hiba, vagy nem iktatható a küldemény
                throw new ResultException(result_CheckAndSetKuldemeny);
            }

            #region Vonalkód ellenőrzés (Megadott iratpéldány vonalkódja nem lehet ugyanaz, mint a küldemény boríték vonalkódja)

            if (_EREC_PldIratPeldanyok != null && !String.IsNullOrEmpty(_EREC_PldIratPeldanyok.BarCode)
                && erec_KuldKuldemenyek != null && !String.IsNullOrEmpty(erec_KuldKuldemenyek.BarCode)
                && erec_KuldKuldemenyek.BarCode == _EREC_PldIratPeldanyok.BarCode)
            {
                // Ellenőrzés, van-e olyan BORÍTÉK melléklete a küldeménynek, aminek vonalkódja megegyezik a megadott iratpéldány vonalkóddal?

                EREC_MellekletekService service_kuldMellekletek = new EREC_MellekletekService(this.dataContext);

                EREC_MellekletekSearch search_kuldMellekletek = new EREC_MellekletekSearch();

                search_kuldMellekletek.KuldKuldemeny_Id.Value = erec_KuldKuldemenyek.Id;
                search_kuldMellekletek.KuldKuldemeny_Id.Operator = Query.Operators.equals;

                search_kuldMellekletek.AdathordozoTipus.Value = KodTarak.AdathordozoTipus.Boritek;
                search_kuldMellekletek.AdathordozoTipus.Operator = Query.Operators.equals;

                search_kuldMellekletek.BarCode.Value = _EREC_PldIratPeldanyok.BarCode;
                search_kuldMellekletek.BarCode.Operator = Query.Operators.equals;

                ExecParam execParam_kuldMellekletek = _ExecParam.Clone();

                Result result_kuldMellekletek = service_kuldMellekletek.GetAll(execParam_kuldMellekletek, search_kuldMellekletek);
                if (!String.IsNullOrEmpty(result_kuldMellekletek.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_kuldMellekletek);
                }

                // Ha talált ilyen borítékot:
                if (result_kuldMellekletek.Ds != null && result_kuldMellekletek.Ds.Tables[0].Rows.Count > 0)
                {
                    // hiba:
                    Logger.Warn("A küldemény borítékának vonalkódja és az irat vonalkódja nem egyezhet meg!", _ExecParam);

                    throw new ResultException(52121);
                }

            }

            #endregion
        }
        #endregion

        Sakkora.SetUgySakkoraAllapotFromIrat(_ExecParam, ref _EREC_UgyUgyiratok, _EREC_IraIratok);

        /// ha Normál (Nem Alszámra) iktatás van, VAGY ha az előzmény ügyirat iktatókönyve lezárt:
        /// (ÜGYIRAT INSERT rész)
        if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas
            || (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas && !iktatokonyvFolyosorszamos && iktatoKonyvLezart == true)
            )
        {

            //Iktatókönyv lekérése a főszám megállapításához:

            erec_IraIktatoKonyvek = GetIktatokonyvById(_EREC_IraIktatoKonyvek_Id, _ExecParam);

            if (erec_IraIktatoKonyvek == null)
            {
                // hiba
                throw new ResultException(52104);
            }

            int UjFoszam = 0;
            int UjMunkaUgyiratSorszam = 0;
            //tömeges iktatás befejezésekor nem kell iktatókönyv update
            if (!ip.TomegesIktatasVegrehajtasa)
            {
                #region TODO: Ellenőrzés, iktatókönyvbe iktathat-e? (nem teljes)

                if (Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatoKonyvek) == true && System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"] != null && System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"].ToUpper() != "MIGRATION")
                {
                    if (iktatokonyvFolyosorszamos)
                    {
                        if (Normal_vagy_AlszamraIktatas != NormalVagyAlszamraIktatas.AlszamraIktatas)
                        {
                            // hiba:
                            Logger.Error("Lezárt a megadott folyósorszámos iktatókönyv, ezért csak alszámra lehet bele iktatni", _ExecParam);
                            throw new ResultException("Lezárt a megadott folyósorszámos iktatókönyv, ezért csak alszámra lehet bele iktatni!");
                        }
                    }
                    else
                    {
                        // hiba:
                        Logger.Error("Lezárt a megadott iktatókönyv", _ExecParam);
                        throw new ResultException(52119);
                    }
                }

                //// Iktatokonyv_Id beállítása alszámra iktatásnál:
                //if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas)
                //{
                //    //_EREC_IraIktatoKonyvek_Id = _EREC_UgyUgyiratok.IraIktatokonyv_Id;
                //}

                //// Iktatókönyvbe iktathat-e?
                //// TODO: Figyelem, ez a metódus még nincs kész
                //Result result_iktatokonyvbeIktathat = Check_IktatoKonyvbeIktathat(_ExecParam, _EREC_IraIktatoKonyvek_Id);
                //if (!String.IsNullOrEmpty(result_iktatokonyvbeIktathat.ErrorCode))
                //{
                //    // nem iktathat, vagy hiba van:
                //    throw new ResultException(result_iktatokonyvbeIktathat);
                //}
                #endregion


                try
                {
                    UjFoszam = Int32.Parse(erec_IraIktatoKonyvek.UtolsoFoszam) + 1;
                }
                catch (Exception e)
                {
                    Logger.Error("Hiba az iktatókönyv utolsó főszámának lekérésekor: "
                        + e.StackTrace, _ExecParam);
                    Logger.Error("", e);

                    throw new ResultException(52100);
                }

                // Munkaügyirat esetén új sorszám megállapítása:
                if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                {
                    try
                    {
                        if (String.IsNullOrEmpty(erec_IraIktatoKonyvek.UtolsoSorszam))
                        {
                            UjMunkaUgyiratSorszam = 1;
                        }
                        else
                        {
                            UjMunkaUgyiratSorszam = Int32.Parse(erec_IraIktatoKonyvek.UtolsoSorszam) + 1;
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Hiba az iktatókönyv utolsó munkaügyirat-sorszámának lekérésekor: "
                       + e.StackTrace, _ExecParam);
                        Logger.Error("", e);

                        throw new ResultException(52100);
                    }
                }


                Logger.Debug("Iktatókönyv Update - Start", _ExecParam);


                #region Iktatokonyv UPDATE
                // Iktatokonyv UPDATE
                // (Utolsó főszám növelése)
                erec_IraIktatoKonyvek.Updated.SetValueAll(false);
                erec_IraIktatoKonyvek.Base.Updated.SetValueAll(false);
                erec_IraIktatoKonyvek.Base.Updated.Ver = true;

                // Munkaanyagnál még nem kap új főszámot, csak iktatott anyagnál:
                // BLG_1713
                // SZUR rendszerből készülhet Belső iktatás Normál főszámmal és Munkairattal
                // BUG_6117
                //if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                //    || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)))
                if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                     && ip.IsSZUR))
                {
                    // Iktatokonyv.UtolsoFoszam beállítása a megnövelt főszámra, és visszaírása az adatbázisba:
                    erec_IraIktatoKonyvek.UtolsoFoszam = UjFoszam.ToString();
                    erec_IraIktatoKonyvek.Updated.UtolsoFoszam = true;
                }
                else
                // Munkaanyagnál utolsó sorszám állítása:
                if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                {
                    // Új sorszám beállítása:
                    erec_IraIktatoKonyvek.UtolsoSorszam = UjMunkaUgyiratSorszam.ToString();
                    erec_IraIktatoKonyvek.Updated.UtolsoSorszam = true;
                }


                ExecParam execParam_IktatokonyvUpdate = _ExecParam.Clone();
                execParam_IktatokonyvUpdate.Record_Id = erec_IraIktatoKonyvek.Id;

                Result result_IktatokonyvUpdate = erec_IraIktatoKonyvekService.Update(execParam_IktatokonyvUpdate, erec_IraIktatoKonyvek);
                if (!String.IsNullOrEmpty(result_IktatokonyvUpdate.ErrorCode))
                {
                    Logger.Error("Iktatókönyv utolsó főszámának növelése sikertelen", _ExecParam, result_IktatokonyvUpdate);

                    //ResultError resultError = new ResultError(result_IktatokonyvUpdate);
                    // Ha verzióütközés van:
                    int errorNumber = ResultError.GetErrorNumberFromResult(result_IktatokonyvUpdate);
                    if (errorNumber == 50402 || errorNumber == 50401)
                    {
                        // Nem egyezik a verzió --> valami normális hibaüzenet kell a felhasználónak
                        throw new ResultException(52115);
                    }

                    // hiba
                    throw new ResultException(result_IktatokonyvUpdate);
                }
            }

            #endregion

            Logger.Debug("Ügyirat Insert Start", _ExecParam);

            #region Ügyirat INSERT

            // Ügyirat INSERT                
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_ugyiratokInsert = _ExecParam.Clone();

            if (iktatoKonyvLezart == true && !iktatokonyvFolyosorszamos)
            {
                #region Előzmény kikérés/kölcsönzés, ha szükséges, hogy később szerelni lehessen

                #region Irattárban van-e/Kikölcsönzött?
                bool kolcsonzesInditasKell = false;
                bool atmenetiIrattarbolKikeresKell = false;// lehet átmeneti vagy skontró irattár is
                bool kikolcsonzott = false;

                Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.None;

                if (elozmenyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott
                    || elozmenyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott)
                {
                    string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam_ugyiratokInsert).Obj_Id;

                    // Ha központi irattárban van:
                    if (!String.IsNullOrEmpty(elozmenyUgyirat.Csoport_Id_Felelos) && !String.IsNullOrEmpty(kozpontiIrattarId)
                        && elozmenyUgyirat.Csoport_Id_Felelos.ToLower() == kozpontiIrattarId.ToLower())
                    {
                        kolcsonzesInditasKell = true;
                    }
                    else
                    {
                        // Átmeneti irattárban van:
                        atmenetiIrattarbolKikeresKell = true;
                        kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.AtmenetiIrattar;
                    }


                    /// Irattárba küldött állapotnál előfordulhat, hogy valaki már beleiktatott, és keletkezett kikérő
                    /// Ilyenkor már nem indítunk új kikérést
                    if (elozmenyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
                    {
                        // ha van találat --> már kikérték, nem kell újra kikérni:
                        if (erec_UgyUgyiratokService.ExistsIrattariKikero(elozmenyUgyirat.Id, _ExecParam.Clone()))
                        {
                            kolcsonzesInditasKell = false;
                            atmenetiIrattarbolKikeresKell = false;
                        }
                    }

                }
                else if (elozmenyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban)
                {
                    // Skontró irattárban van-e:
                    string skontroIrattarId = KodTarak.SPEC_SZERVEK.GetSkontroIrattaros(execParam_ugyiratokInsert).Obj_Id;
                    if (!String.IsNullOrEmpty(elozmenyUgyirat.Csoport_Id_Felelos) && !String.IsNullOrEmpty(skontroIrattarId)
                    && elozmenyUgyirat.Csoport_Id_Felelos.ToLower() == skontroIrattarId.ToLower())
                    {
                        atmenetiIrattarbolKikeresKell = true;
                        kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.SkontroIrattar;
                    }
                }
                else if (elozmenyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott)
                {
                    kikolcsonzott = true;
                }

                #endregion

                #region Irattárban lévő ügyiratnál további műveletek

                if (atmenetiIrattarbolKikeresKell == true)
                {
                    #region Kikérés indítása:
                    Result result_kikeres = erec_UgyUgyiratokService.Kikeres(elozmenyUgyirat.Id, kikeresIrattarTipus, _ExecParam.Clone(), "Szerelés");
                    #endregion
                }
                else if (kolcsonzesInditasKell == true)
                {
                    #region Kölcsönzés indítása, automatikus jóváhagyással:
                    Result result_kikeres = erec_UgyUgyiratokService.Kolcsonzes(elozmenyUgyirat.Id, _ExecParam.Clone(), "Szerelés");
                    #endregion
                }
                else if (kikolcsonzott == true)
                {
                    // csak szereljük, itt nem kell ügyintézésre kikérni
                }
                #endregion Irattárban lévő ügyiratnál további műveletek

                #endregion Előzmény kikérés/kölcsönzés, ha szükséges
            }

            // ha iktatoKonyvLezart == true, _EREC_UgyUgyiratok a lekért előzmény ügyirat adatait fogja tartalmazni, de nekünk új kell
            if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas
                && !iktatokonyvFolyosorszamos && iktatoKonyvLezart == true)
            {
                // paraméterek ellenőrzése
                if (String.IsNullOrEmpty(ip.UgykorId) || String.IsNullOrEmpty(ip.Ugytipus))
                {
                    Logger.Warn("Iktatókönyv ellenőrzés: lezárt iktatókönyvben lévő előzmény ügyirat esetén az átadott paramétereknek tartalmazniuk kell a létrehozandó új ügyirat adatait!", _ExecParam);
                    throw new ResultException(52136);
                }
                //EREC_UgyUgyiratok elozmenyUgyirat = _EREC_UgyUgyiratok;
                _EREC_UgyUgyiratok = new EREC_UgyUgyiratok();
                _EREC_UgyUgyiratok.Updated.SetValueAll(false);
                _EREC_UgyUgyiratok.Base.Updated.SetValueAll(false);

                // TODO: előzmény ügyiratból néhány mezőt átmásolni: (??)
                #region előzmény adatok másolása
                _EREC_UgyUgyiratok.UgyintezesModja = elozmenyUgyirat.UgyintezesModja;
                _EREC_UgyUgyiratok.Updated.UgyintezesModja = true;

                _EREC_UgyUgyiratok.Hatarido = elozmenyUgyirat.Hatarido; // ezt lehet, hogy alább még felülírjuk az iktatási paraméterekből
                _EREC_UgyUgyiratok.Updated.Hatarido = true;

                _EREC_UgyUgyiratok.Targy = elozmenyUgyirat.Targy;
                _EREC_UgyUgyiratok.Updated.Targy = true;

                // módosítható
                _EREC_UgyUgyiratok.UgyTipus = ip.Ugytipus;
                _EREC_UgyUgyiratok.Updated.UgyTipus = true;

                // felelős és őrző az iktató lesz
                string Iktato_Csoport_Id = Csoportok.GetFelhasznaloSajatCsoportId(_ExecParam);

                _EREC_UgyUgyiratok.Csoport_Id_Felelos = Iktato_Csoport_Id;
                _EREC_UgyUgyiratok.Updated.Csoport_Id_Felelos = true;

                _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo = Iktato_Csoport_Id;
                _EREC_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Orzo = true;

                // TODO: módosítható(?) - nincs kint az iktatási képernyőn
                _EREC_UgyUgyiratok.Jelleg = elozmenyUgyirat.Jelleg;
                _EREC_UgyUgyiratok.Updated.Jelleg = true;

                // módosítható
                _EREC_UgyUgyiratok.IraIrattariTetel_Id = ip.UgykorId;
                _EREC_UgyUgyiratok.Updated.IraIrattariTetel_Id = true;

                _EREC_UgyUgyiratok.Surgosseg = elozmenyUgyirat.Surgosseg;
                _EREC_UgyUgyiratok.Updated.Surgosseg = true;

                _EREC_UgyUgyiratok.Partner_Id_Ugyindito = elozmenyUgyirat.Partner_Id_Ugyindito;
                _EREC_UgyUgyiratok.Updated.Partner_Id_Ugyindito = true;

                _EREC_UgyUgyiratok.NevSTR_Ugyindito = elozmenyUgyirat.NevSTR_Ugyindito;
                _EREC_UgyUgyiratok.Updated.NevSTR_Ugyindito = true;

                _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = elozmenyUgyirat.FelhasznaloCsoport_Id_Ugyintez;
                _EREC_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

                _EREC_UgyUgyiratok.Megjegyzes = elozmenyUgyirat.Megjegyzes;
                _EREC_UgyUgyiratok.Updated.Megjegyzes = true;

                _EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos = elozmenyUgyirat.Csoport_Id_Ugyfelelos;
                _EREC_UgyUgyiratok.Updated.Csoport_Id_Ugyfelelos = true;

                _EREC_UgyUgyiratok.Cim_Id_Ugyindito = elozmenyUgyirat.Cim_Id_Ugyindito;
                _EREC_UgyUgyiratok.Updated.Cim_Id_Ugyindito = true;

                _EREC_UgyUgyiratok.CimSTR_Ugyindito = elozmenyUgyirat.CimSTR_Ugyindito;
                _EREC_UgyUgyiratok.Updated.CimSTR_Ugyindito = true;

                // az új ügyiratot az iktató hozza létre
                _EREC_UgyUgyiratok.Base.Letrehozo_id = _ExecParam.Felhasznalo_Id;
                _EREC_UgyUgyiratok.Base.Updated.Letrehozo_id = true;
                #endregion előzmény adatok másolása

                #region ügyirat határidő módosítás, ha szükséges
                if (!String.IsNullOrEmpty(ip.UgyiratUjHatarido))
                {
                    _EREC_UgyUgyiratok.Hatarido = ip.UgyiratUjHatarido;
                    _EREC_UgyUgyiratok.Updated.Hatarido = true;
                }
                #endregion ügyirat határidő módosítás, ha szükséges

            }

            // iktatókönyvhöz kapcsolás
            _EREC_UgyUgyiratok.IraIktatokonyv_Id = erec_IraIktatoKonyvek.Id;
            _EREC_UgyUgyiratok.Updated.IraIktatokonyv_Id = true;

            if (!ip.TomegesIktatasVegrehajtasa)
            {
                // Főszám munkaanyagnál nincs, csak iktatott iratnál:
                if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                {
                    // főszám beállítása
                    _EREC_UgyUgyiratok.Foszam = UjFoszam.ToString();
                    _EREC_UgyUgyiratok.Updated.Foszam = true;

                    // UTOLSÓ ALSZÁM beállítása 1-re (mert egy iratot bele fogunk tenni)
                    _EREC_UgyUgyiratok.UtolsoAlszam = "1";
                    _EREC_UgyUgyiratok.Updated.UtolsoAlszam = true;
                }
                else if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                {
                    // BLG_1713
                    // SZUR rendszerből készülhet Belső iktatás Normál főszámmal és Munkairattal
                    // BUG_6117
                    //if ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas))
                    if ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (ip.IsSZUR))
                    {
                        // főszám beállítása
                        _EREC_UgyUgyiratok.Foszam = UjFoszam.ToString();
                        _EREC_UgyUgyiratok.Updated.Foszam = true;

                        // BUG_8369
                        if (String.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos))
                        {
                            var svc = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                            KRT_CsoportTagokSearch sch = new KRT_CsoportTagokSearch();
                            sch.Csoport_Id_Jogalany.Filter(_EREC_UgyUgyiratok.Csoport_Id_Felelos);
                            sch.TopRow = 1;

                            var xpm = _ExecParam.Clone();
                            Result res = svc.GetAll(xpm, sch);

                            if (!res.IsError && res.GetCount > 0)
                            {
                                var szervezetiEgyseg = res.Ds.Tables[0].Rows[0]["Csoport_Id"].ToString();
                                if (!String.IsNullOrEmpty(szervezetiEgyseg))
                                {
                                    _EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos = szervezetiEgyseg;
                                    _EREC_UgyUgyiratok.Updated.Csoport_Id_Ugyfelelos = true;
                                }
                            }
                        }
                    }
                    // Munkaanyagnál az utolsó alszám 0 lesz: (mert nem lesz benne iktatott irat, csak munkaanyag)
                    _EREC_UgyUgyiratok.UtolsoAlszam = "0";
                    _EREC_UgyUgyiratok.Updated.UtolsoAlszam = true;

                    // Munkaügyirat sorszám állítása:
                    _EREC_UgyUgyiratok.Sorszam = UjMunkaUgyiratSorszam.ToString();
                    _EREC_UgyUgyiratok.Updated.Sorszam = true;

                    // UTOLSÓ SORSZÁM állítása 1-re, mert egy munkairatot létre fogunk hozni benne
                    _EREC_UgyUgyiratok.UtolsoSorszam = "1";
                    _EREC_UgyUgyiratok.Updated.UtolsoSorszam = true;
                }
            }
            else
            {
                _EREC_UgyUgyiratok.Updated.Foszam = false;
                _EREC_UgyUgyiratok.Updated.UtolsoAlszam = false;
                _EREC_UgyUgyiratok.Updated.Sorszam = false;
                _EREC_UgyUgyiratok.Updated.UtolsoSorszam = false;
            }

            // Őrző beállítása a Felhasznalo csoportjára:
            _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo = Csoportok.GetFelhasznaloSajatCsoportId(_ExecParam);
            _EREC_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Orzo = true;

            #region CR3184 - FPH - Ügyiratnak nem generálódik vonalkód
            // Barkod beállítása:
            if (!String.IsNullOrEmpty(barkod_ugyirat))
            {
                _EREC_UgyUgyiratok.BARCODE = barkod_ugyirat;
                _EREC_UgyUgyiratok.Updated.BARCODE = true;
            }
            #endregion CR3184 - FPH - Ügyiratnak nem generálódik vonalkód

            // Állapot
            // BLG_1713 
            // SZUR rendszerből készülhet Belső iktatás Normál főszámmal és Munkairattal
            // BUG_6117
            //if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            //    || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)))
            if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                    && (ip.IsSZUR)))

            {
                _EREC_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Iktatott;
                _EREC_UgyUgyiratok.Updated.Allapot = true;

                // BLG_1348
                // Kiszedve az Ügyintezes_alatt-i státuszba állítás (más fogja majd vezérelni)
                //ügyintézés alatti állapot bejövő papír alaú irat esetén
                //if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
                //{
                //    if (Contentum.eRecord.BaseUtility.Ugyiratok.Uj_Ugyirat_Hatarido_Kezeles(_ExecParam))
                //    {
                //        if (Contentum.eRecord.BaseUtility.Kuldemenyek.IsPapirAlapu(erec_KuldKuldemenyek))
                //        {
                //            _EREC_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
                //        }
                //    }
                //}
            }
            else if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
            {
                _EREC_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag;
                _EREC_UgyUgyiratok.Updated.Allapot = true;
            }

            // Ügyindító beállítása:
            if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
            {
                //BUG_6309-ben kivettük
                //if (String.IsNullOrEmpty(_EREC_UgyUgyiratok.Partner_Id_Ugyindito) && String.IsNullOrEmpty(_EREC_UgyUgyiratok.NevSTR_Ugyindito))
                //{
                //    _EREC_UgyUgyiratok.Partner_Id_Ugyindito = erec_KuldKuldemenyek.Partner_Id_Bekuldo;
                //    _EREC_UgyUgyiratok.Updated.Partner_Id_Ugyindito = true;
                //    _EREC_UgyUgyiratok.NevSTR_Ugyindito = erec_KuldKuldemenyek.NevSTR_Bekuldo;
                //    _EREC_UgyUgyiratok.Updated.NevSTR_Ugyindito = true;
                //    _EREC_UgyUgyiratok.Cim_Id_Ugyindito = erec_KuldKuldemenyek.Cim_Id;
                //    _EREC_UgyUgyiratok.Updated.Cim_Id_Ugyindito = true;
                //    _EREC_UgyUgyiratok.CimSTR_Ugyindito = erec_KuldKuldemenyek.CimSTR_Bekuldo;
                //    _EREC_UgyUgyiratok.Updated.CimSTR_Ugyindito = true;
                //}
            }
            else if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas)
            {
                // elvileg ilyenkor ügyindító mezőt nem kell tölteni...
            }

            // Sürgősség kódtáras érték beállítása fix értékkel
            _EREC_UgyUgyiratok.Surgosseg = KodTarak.SURGOSSEG.Normal;
            _EREC_UgyUgyiratok.Updated.Surgosseg = true;

            if (String.IsNullOrEmpty(_EREC_UgyUgyiratok.IratMetadefinicio_Id))
            {
                #region Iratmetadef. Id meghatározása

                EREC_IratMetaDefinicioService service_iratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);

                Result result_iratMetaDef = service_iratMetaDef.GetIratMetaDefinicioByUgykorUgytipus(_ExecParam.Clone()
                    , _EREC_UgyUgyiratok.IraIrattariTetel_Id, _EREC_UgyUgyiratok.UgyTipus);
                if (!String.IsNullOrEmpty(result_iratMetaDef.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_iratMetaDef);
                }


                EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_iratMetaDef.Record;
                if (erec_IratMetaDefinicio != null)
                {
                    _EREC_UgyUgyiratok.IratMetadefinicio_Id = erec_IratMetaDefinicio.Id;
                    _EREC_UgyUgyiratok.Updated.IratMetadefinicio_Id = true;

                    _EREC_UgyUgyiratok.GeneraltTargy = erec_IratMetaDefinicio.GeneraltTargy;
                    _EREC_UgyUgyiratok.Updated.GeneraltTargy = true;
                }

                #endregion
            }

            /// Azonosító (Főszám) töltése:
            if (!ip.TomegesIktatasVegrehajtasa)
            {
                // BLG_292
                //_EREC_UgyUgyiratok.Azonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_EREC_UgyUgyiratok, erec_IraIktatoKonyvek);
                _EREC_UgyUgyiratok.Azonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_ExecParam, _EREC_UgyUgyiratok, erec_IraIktatoKonyvek);

                _EREC_UgyUgyiratok.Updated.Azonosito = true;
            }

            // ügyirat fajtáját (Jelleg) és az ügyintézés módját (UgyintezesModja)
            // meghatározzuk az iratpéldányból/küldeményből:
            string ugyintezesMod = KodTarak.UGYIRAT_JELLEG.Papir;
            string jelleg = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;

            if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas)
            {
                ugyintezesMod = _EREC_IraIratok.UgyintezesAlapja; // Elsődleges adathordozó
                jelleg = _EREC_IraIratok.AdathordozoTipusa;       // Ügyintézés alapja
            }
            else
            {
                // BUG_2051
                //ugyintezesMod = erec_KuldKuldemenyek.UgyintezesModja;
                //jelleg = erec_KuldKuldemenyek.AdathordozoTipusa;
                ugyintezesMod = erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa;
                jelleg = erec_KuldKuldemenyek.UgyintezesModja;
            }

            switch (jelleg)
            {
                case KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir:
                    _EREC_UgyUgyiratok.Jelleg = KodTarak.UGYIRAT_JELLEG.Elektronikus;
                    break;
                case KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir:
                default:
                    _EREC_UgyUgyiratok.Jelleg = KodTarak.UGYIRAT_JELLEG.Papir;
                    break;
            }

            _EREC_UgyUgyiratok.Updated.Jelleg = true;

            _EREC_UgyUgyiratok.UgyintezesModja = ugyintezesMod;
            _EREC_UgyUgyiratok.Updated.UgyintezesModja = true;

            #region SAKKORA ELTELT IDO DEFAULT
            try
            {
                SakkoraService sakkSvc = new SakkoraService(this.dataContext);
                sakkSvc.SetUgyiratElteltIdoDefault(_ExecParam, ref _EREC_UgyUgyiratok);
            }
            catch (Exception x)
            {
                Logger.Error("SetUgyiratElteltIdoDefault", x);
            }
            #endregion

            if (ip.TomegesIktatasVegrehajtasa)
            {
                ExecParam exec_tomegesIktatasUpdate = _ExecParam.Clone();
                exec_tomegesIktatasUpdate.Record_Id = _EREC_UgyUgyiratok.Id;
                Result result_tomegesIktatasUpdate = erec_UgyUgyiratokService.Update(exec_tomegesIktatasUpdate, _EREC_UgyUgyiratok);
                if (!String.IsNullOrEmpty(result_tomegesIktatasUpdate.ErrorCode))
                {
                    Logger.Debug("Ügyirat Update sikertelen: ", exec_tomegesIktatasUpdate, result_tomegesIktatasUpdate);

                    // hiba 
                    throw new ResultException(result_tomegesIktatasUpdate);
                }
            }
            else
            {
                Result result_ugyiratokInsert = erec_UgyUgyiratokService.Insert(execParam_ugyiratokInsert, _EREC_UgyUgyiratok);
                if (!String.IsNullOrEmpty(result_ugyiratokInsert.ErrorCode))
                {
                    Logger.Debug("Ügyirat Insert sikertelen: ", _ExecParam, result_ugyiratokInsert);

                    // hiba 
                    throw new ResultException(result_ugyiratokInsert);
                }

                _EREC_UgyUgyiratok.Id = result_ugyiratokInsert.Uid;
            }
            //CR 3058
            if (vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
            {
                KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                var execParam_Ugyirat = _ExecParam.Clone();
                execParam_Ugyirat.Record_Id = _EREC_UgyUgyiratok.Id;
                Result Erec_Ugyirat = erec_UgyUgyiratokService.Get(execParam_Ugyirat);

                //var iktatokonyv = GetIktatokonyvById(_EREC_IraIktatoKonyvek_Id, execParam_Ugyirat);

                #region CR 3111 Budaörsön a vonalkód generálás Munkapéldány létrehozásakor mindig 0-s sorszámot ad.
                var iktatokonyv = new EREC_IraIktatoKonyvek();
                if (!String.IsNullOrEmpty(_EREC_IraIktatoKonyvek_Id))
                    iktatokonyv = GetIktatokonyvById(_EREC_IraIktatoKonyvek_Id, execParam_Ugyirat);
                else
                    iktatokonyv = GetIktatokonyvById(((EREC_UgyUgyiratok)Erec_Ugyirat.Record).IraIktatokonyv_Id, execParam_Ugyirat);
                string barcode =
                    iktatokonyv.Iktatohely + "/" +
                    (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag ?
                        ("MU" +
                            (String.IsNullOrEmpty(((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam) ?
                                ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Sorszam.PadLeft(6, '0') :
                                ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam.PadLeft(6, '0'))
                        ) :
                        ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam.PadLeft(6, '0'))

                    + "/" + iktatokonyv.Ev;
                #endregion
                barcode = barcode.Replace(" ", "");

                KRT_Barkodok barcodeRecord = new KRT_Barkodok();
                //barcodeRecord.ErvKezd = beerkezes.ToString();
                barcodeRecord.Obj_type =
                barcodeRecord.Kod = barcode.Replace(" ", "");

                var execParam_BarCode = _ExecParam.Clone();

                var bid = srvBarcode.Insert(execParam_BarCode, barcodeRecord);
                if (!String.IsNullOrEmpty(bid.ErrorCode))
                {
                    //    hiba
                    Logger.Error("Hiba: Barkod rekordhoz ügyirat Insert", execParam_BarCode);
                    throw new ResultException(bid);
                }
                //var result_barcodebind = srvBarcode.BarkodBindToKuldemeny(execParam_BarCode, bid.Uid, result_ugyiratokInsert.Uid);
                // if (!String.IsNullOrEmpty(result_barcodebind.ErrorCode))
                // {
                //     // hiba
                //     Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", execParam_BarCode);
                //     throw new ResultException(result_barcodebind);
                // }
                ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).BARCODE = barcode;

                var ugyirat_Update_result = erec_UgyUgyiratokService.Update(execParam_Ugyirat, ((EREC_UgyUgyiratok)Erec_Ugyirat.Record));
                if (!String.IsNullOrEmpty(ugyirat_Update_result.ErrorCode))
                {
                    // hiba
                    Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", execParam_Ugyirat);
                    throw new ResultException(ugyirat_Update_result);
                }
                _EREC_UgyUgyiratok = ((EREC_UgyUgyiratok)Erec_Ugyirat.Record);
                barkod_ugyirat_Id = bid.Uid;
                barkod_ugyirat = barcodeRecord.Kod;
                barkod_pld1 = barcodeRecord.Kod;

            }


            //iktatasResult.UjUgyirat_Id = result_ugyiratokInsert.Uid;

            #endregion

            #region Előzményezés esetén eMigration adatok visszairasa kell

            if (String.IsNullOrEmpty(ip.SzerelendoUgyiratId) && _EREC_UgyUgyiratok.Updated.RegirendszerIktatoszam && !String.IsNullOrEmpty(_EREC_UgyUgyiratok.RegirendszerIktatoszam))
            {
                kelleRegiAdtaSzereles = true;

                // régi ügyirat ellenőrzése selejtezésre
                if (!String.IsNullOrEmpty(ip.RegiAdatId))
                {
                    Contentum.eMigration.Service.MIG_FoszamService foszamService = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
                    ExecParam xpmFoszam = _ExecParam.Clone();
                    xpmFoszam.Record_Id = ip.RegiAdatId;

                    Result result_foszam = foszamService.Get(xpmFoszam);

                    if (!result_foszam.IsError)
                    {
                        Contentum.eMigration.BusinessDocuments.MIG_Foszam mig_foszam = (Contentum.eMigration.BusinessDocuments.MIG_Foszam)result_foszam.Record;
                        if (mig_foszam != null && mig_foszam.UGYHOL == Contentum.eUtility.Constants.MIG_IratHelye.Selejtezett)
                        {
                            // Az ügyiratba nem lehet iktatni! Az ügyirat selejtezett.
                            throw new ResultException(52139);
                        }
                    }
                }
            }

            #endregion

            #region Alszámra indított iktatás és lezárt iktatókönyvben lévő előzmény esetén át kell venni a B1 metaadatokat
            if (kellMetaadatAtvetel)
            {
                EREC_ObjektumTargyszavaiService service_ObjektumTargyszavai = new EREC_ObjektumTargyszavaiService(this.dataContext);
                ExecParam execParam_ObjektumTargyszavai = _ExecParam.Clone();

                Logger.Debug("Objektumot kiegészítő (B1) metadatok átvétele lezárt iktatókönyvben lévő ügyiratból új ügyiratba", execParam_ObjektumTargyszavai);

                EREC_ObjektumTargyszavaiSearch search_ObjektumTargyszavai = new EREC_ObjektumTargyszavaiSearch();
                string Obj_Id = elozmenyUgyirat.Id; // = _EREC_UgyUgyiratok_Id
                string ObjTip_Kod_Tabla = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;

                Result result_OT_byDefinicioTipus = service_ObjektumTargyszavai.GetAllMetaByDefinicioTipus(execParam_ObjektumTargyszavai
                    , search_ObjektumTargyszavai
                    , Obj_Id
                    , ""
                    , ObjTip_Kod_Tabla
                    , KodTarak.OBJMETADEFINICIO_TIPUS.B1
                    , true
                    , true);

                if (!String.IsNullOrEmpty(result_OT_byDefinicioTipus.ErrorCode))
                {
                    Logger.Debug("Hiba az objektumot kiegészítő (B1) metadatok lekérésekor!", execParam_ObjektumTargyszavai, result_OT_byDefinicioTipus);

                    // hiba 
                    throw new ResultException(result_OT_byDefinicioTipus);
                }

                DataRow[] rows_OT_byDefinicioTipus = result_OT_byDefinicioTipus.Ds.Tables[0].Select("IsNull(Targyszo_Id,'')<>''");
                List<string> list_Ids_OT = new List<string>();
                if (rows_OT_byDefinicioTipus != null)
                {
                    foreach (DataRow row in rows_OT_byDefinicioTipus)
                    {
                        list_Ids_OT.Add(row["Targyszo_Id"].ToString());
                    }
                }

                search_ObjektumTargyszavai.Obj_Id.Value = Obj_Id;
                search_ObjektumTargyszavai.Obj_Id.Operator = Query.Operators.equals;

                if (list_Ids_OT.Count > 0)
                {
                    search_ObjektumTargyszavai.Targyszo_Id.Value = "'" + String.Join("','", list_Ids_OT.ToArray()) + "'";
                    search_ObjektumTargyszavai.Targyszo_Id.Operator = Query.Operators.inner;
                }

                Result result_OT_elozmeny = service_ObjektumTargyszavai.GetAll(execParam_ObjektumTargyszavai, search_ObjektumTargyszavai);

                // lista készítése
                foreach (DataRow row in result_OT_elozmeny.Ds.Tables[0].Rows)
                {
                    EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                    Utility.LoadBusinessDocumentFromDataRow(erec_ObjektumTargyszavai, row);

                    erec_ObjektumTargyszavai.Updated.SetValueAll(true);
                    erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);
                    erec_ObjektumTargyszavai.Id = "";

                    erec_ObjektumTargyszavai.Obj_Id = _EREC_UgyUgyiratok.Id;    // az újonnan létrehozott ügyirat azonosítója

                    Result result_ObjektumTargyszavai = service_ObjektumTargyszavai.Insert(execParam_ObjektumTargyszavai, erec_ObjektumTargyszavai);

                    if (!String.IsNullOrEmpty(result_ObjektumTargyszavai.ErrorCode))
                    {
                        Logger.Debug("Objektumot kiegészítő (B1) metadatok átvétele lezárt iktatókönyvben lévő ügyiratból új ügyiratba sikertelen: ", execParam_ObjektumTargyszavai, result_ObjektumTargyszavai);

                        // hiba 
                        throw new ResultException(result_ObjektumTargyszavai);
                    }

                }
                Logger.Debug("Objektumot kiegészítő (B1) metadatok átvétele lezárt iktatókönyvben lévő ügyiratból új ügyiratba sikeres.");

            }
            #endregion Alszámra indított iktatás és lezárt iktatókönyvben lévő előzmény esetén át kell venni a B1 metaadatokat

            // #region IraOnkormAdatokInsert átkerült az Irat insert után, mert szükség van az Irat azonosítójára

            // Érték nélküli tárgyszavakat majd kézzel felviszi, ha akarja, feleslegesen ne rendeljük itt hozzá (2008.07.04)
            //#region Metaadatok hozzárendelése ügyirathoz
            //// csak az érték nélküli tárgyszavak rendelődnek hozzá, ha van ilyen...

            //EREC_ObjektumTargyszavaiService service_ObjektumTargyszavai = new EREC_ObjektumTargyszavaiService(this.dataContext);
            //ExecParam execParam_ObjektumTargyszavai = _ExecParam.Clone();

            //Logger.Debug("Metadatok automatikus hozzárendelése az ügyirathoz az objektum metadefiníciói alapján", execParam_ObjektumTargyszavai);

            //Result result_ObjektumTargyszavai = service_ObjektumTargyszavai.AssignByDefinicioTipus(execParam_ObjektumTargyszavai
            //    , _EREC_UgyUgyiratok.Id
            //    , null
            //    , "EREC_UgyUgyiratok"
            //    , ""
            //    , false);

            //if (!String.IsNullOrEmpty(result_ObjektumTargyszavai.ErrorCode))
            //{
            //    Logger.Debug("Metaadatok automatikus hozzárendelése az ügyirathoz sikertelen: ", execParam_ObjektumTargyszavai, result_ObjektumTargyszavai);

            //    // hiba 
            //    throw new ResultException(result_ObjektumTargyszavai);
            //}

            //Logger.Debug("Metadatok automatikus hozzárendelése az ügyirathoz az objektum metaadatai alapján sikeres.");

            //#endregion Metaadatok hozzárendelése ügyirathoz

            #region BARCODE UPDATE (ügyirat)

            KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
            ExecParam execParam_barcodeUpdate = _ExecParam.Clone();

            Result result_barcodeUpdate = service_barkodok.BarkodBindToUgyirat(
                    execParam_barcodeUpdate, barkod_ugyirat_Id, _EREC_UgyUgyiratok.Id);

            if (!String.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
            {
                // hiba
                Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", _ExecParam);
                throw new ResultException(result_barcodeUpdate);
            }

            #endregion

            Logger.Debug("ÜgyiratDarab Insert - Start", _ExecParam);

            #region ÜgyiratDarab INSERT
            // Új ügyirathoz kell egy általános ügyiratdarab                

            EREC_UgyUgyiratdarabokService erec_UgyUgyiratdarabokService = new EREC_UgyUgyiratdarabokService(this.dataContext);
            ExecParam execParam_ugyiratdarabInsert = _ExecParam.Clone();

            if (_EREC_UgyUgyiratdarabok == null)
            {
                _EREC_UgyUgyiratdarabok = new EREC_UgyUgyiratdarabok();
            }

            // Ügyirathoz kapcsolás
            _EREC_UgyUgyiratdarabok.UgyUgyirat_Id = _EREC_UgyUgyiratok.Id;
            _EREC_UgyUgyiratdarabok.Updated.UgyUgyirat_Id = true;

            // BLG_1713
            // SZUR rendszerből készülhet Belső iktatás Normál főszámmal és Munkairattal
            // BUG_6117
            //if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            //    || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)))
            if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                    && (ip.IsSZUR)))
            {
                // NEM LENNE szükséges, csak a régi módszer miatt a felületen még sok helyen ezt jelenítjük meg                
                // ügyiratdarab-hoz főszám és iktatókönyv_id beállítása 
                // (UGYANAZ, mint ami az ügyiratban van)
                _EREC_UgyUgyiratdarabok.Foszam = _EREC_UgyUgyiratok.Foszam;
                _EREC_UgyUgyiratdarabok.Updated.Foszam = true;
            }

            _EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = _EREC_UgyUgyiratok.IraIktatokonyv_Id;
            _EREC_UgyUgyiratdarabok.Updated.IraIktatokonyv_Id = true;

            // utolsó alszám NEM KELL
            //_EREC_UgyUgyiratdarabok.UtolsoAlszam = "1";
            //_EREC_UgyUgyiratdarabok.Updated.UtolsoAlszam = true;

            // felelős beállítása NEM KELL
            //_EREC_UgyUgyiratdarabok.Csoport_Id_Felelos = _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo;
            //_EREC_UgyUgyiratdarabok.Updated.Csoport_Id_Felelos = true;

            /// Ügyintéző átvétele az ügyiratból, ha nincs külön megadva az ügyiratdarabnál // KELL??
            _EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez = _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
            _EREC_UgyUgyiratdarabok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

            /// Határidő átvétele az ügyiratból ha az ügyiratdarabnál nincs külön megadva NEM KELL
            //if (String.IsNullOrEmpty(_EREC_UgyUgyiratdarabok.Hatarido))
            //{
            //    _EREC_UgyUgyiratdarabok.Hatarido = _EREC_UgyUgyiratok.Hatarido;
            //}
            //_EREC_UgyUgyiratdarabok.Updated.Hatarido = true;


            // Állapot beállítása:
            _EREC_UgyUgyiratdarabok.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott;
            _EREC_UgyUgyiratdarabok.Updated.Allapot = true;

            // Eljárási szakasz: Osztatlan
            _EREC_UgyUgyiratdarabok.EljarasiSzakasz = KodTarak.ELJARASI_SZAKASZ.Osztatlan;
            _EREC_UgyUgyiratdarabok.Updated.EljarasiSzakasz = true;

            ///// Azonosító (Főszám) töltése
            ///// 
            //_EREC_UgyUgyiratdarabok.Azonosito = Contentum.eRecord.BaseUtility.UgyiratDarabok.GetFullFoszam(_EREC_UgyUgyiratdarabok, erec_IraIktatoKonyvek);
            //_EREC_UgyUgyiratdarabok.Updated.Azonosito = true;

            if (ip.TomegesIktatasVegrehajtasa)
            {
                ExecParam exec_tomegesIktatasUpdate = _ExecParam.Clone();
                exec_tomegesIktatasUpdate.Record_Id = _EREC_UgyUgyiratdarabok.Id;
                Result result_tomegesIktatasUpdate = erec_UgyUgyiratdarabokService.Update(exec_tomegesIktatasUpdate, _EREC_UgyUgyiratdarabok);
                if (!String.IsNullOrEmpty(result_tomegesIktatasUpdate.ErrorCode))
                {
                    Logger.Debug("Ügyiratdarab Update sikertelen: ", exec_tomegesIktatasUpdate, result_tomegesIktatasUpdate);

                    // hiba 
                    throw new ResultException(result_tomegesIktatasUpdate);
                }
            }
            else
            {
                Result result_ugyiratdarabInsert =
                erec_UgyUgyiratdarabokService.Insert(execParam_ugyiratdarabInsert, _EREC_UgyUgyiratdarabok);
                if (!String.IsNullOrEmpty(result_ugyiratdarabInsert.ErrorCode))
                {
                    // hiba
                    throw new ResultException(result_ugyiratdarabInsert);
                }

                _EREC_UgyUgyiratdarabok.Id = result_ugyiratdarabInsert.Uid;
            }

            #endregion


            // ha alszámra iktatásnál az előzmény ügyirat iktatókönyve lezárt volt,
            // előzmény ügyirat szerelése az újba, ill. a szerelés előkészítése:
            if (!iktatokonyvFolyosorszamos && iktatoKonyvLezart == true)
            {
                string elozmenyUgyiratId = "";
                if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas)
                {
                    elozmenyUgyiratId = _EREC_UgyUgyiratok_Id;
                }
                else
                {
                    elozmenyUgyiratId = ip.SzerelendoUgyiratId;
                }

                // Előzmény ügyirat szerelése az újba, vagy szerelés előkészítése
                if (szerelesMod == SzerelesMod.Szereles)
                {
                    #region Szerelés

                    Logger.Info(@"Előzmény ügyirat iktatókönyve lezárt --> előzmény ügyirat szerelése az újba
                    Előzmény ügyirat: " + elozmenyUgyiratId + " Az új ügyirat: " + _EREC_UgyUgyiratok.Id, _ExecParam);

                    ExecParam execParam_szereles = _ExecParam.Clone();
                    Result result_szereles =
                        erec_UgyUgyiratokService.Szereles(execParam_szereles, elozmenyUgyiratId, _EREC_UgyUgyiratok.Id, null);
                    if (!String.IsNullOrEmpty(result_szereles.ErrorCode))
                    {
                        // hiba:
                        Logger.Error("Hiba a szerelés során", _ExecParam, result_szereles);

                        throw new ResultException(result_szereles);
                    }

                    #endregion
                }
                else if (szerelesMod == SzerelesMod.Elokeszites)
                {
                    #region Szerelés előkészítés
                    // Ugyirat lekérése:
                    Logger.Debug("Ügyirat ismételt lekérés szerelés előkészítéshez - Start", _ExecParam);

                    ExecParam execParam_ugyiratGet = _ExecParam.Clone();
                    execParam_ugyiratGet.Record_Id = elozmenyUgyiratId;

                    Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_ugyiratGet);
                    if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                    {
                        // hiba
                        throw new ResultException(result_ugyiratGet);
                    }
                    else
                    {
                        elozmenyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
                    }

                    Logger.Info(@"Előzmény ügyirat iktatókönyve lezárt --> előzmény ügyirat szerelés előkészítése (szülő bejegyzése, ügyirat UPDATE)
                    Előzmény ügyirat: " + elozmenyUgyiratId + " Az új ügyirat: " + _EREC_UgyUgyiratok.Id, _ExecParam);

                    elozmenyUgyirat.Updated.SetValueAll(false);
                    elozmenyUgyirat.Base.Updated.SetValueAll(false);
                    elozmenyUgyirat.Base.Updated.Ver = true;

                    elozmenyUgyirat.UgyUgyirat_Id_Szulo = _EREC_UgyUgyiratok.Id;
                    elozmenyUgyirat.Updated.UgyUgyirat_Id_Szulo = true;

                    ExecParam execParam_elokeszites = _ExecParam.Clone();
                    execParam_elokeszites.Record_Id = elozmenyUgyiratId;
                    Result result_elokeszites =
                        erec_UgyUgyiratokService.Update(execParam_elokeszites, elozmenyUgyirat);
                    if (!String.IsNullOrEmpty(result_elokeszites.ErrorCode))
                    {
                        // hiba:
                        Logger.Error("Hiba a szerelés előkészítés során", _ExecParam, result_elokeszites);

                        throw new ResultException(result_elokeszites);
                    }

                    #endregion
                }
            }

        }
        else if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas
                && (iktatokonyvFolyosorszamos || iktatoKonyvLezart == false))
        {
            // a korábbiakban az elozmenyUgyiratba mentettük a lekért rekordot
            _EREC_UgyUgyiratok = elozmenyUgyirat;


            #region Ügyirat UPDATE
            EREC_UgyUgyiratokService erec_ugyugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);

            _EREC_UgyUgyiratok.Updated.SetValueAll(false);
            _EREC_UgyUgyiratok.Base.Updated.SetValueAll(false);
            _EREC_UgyUgyiratok.Base.Updated.Ver = true;


            ExecParam execParam_ugyiratUpdate = _ExecParam.Clone();
            execParam_ugyiratUpdate.Record_Id = _EREC_UgyUgyiratok.Id;

            if (!ip.TomegesIktatasVegrehajtasa)
            {
                // Munkaanyagnál nem kell az alszámot állítani:
                if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                {
                    #region Új alszám beállítása

                    erec_ugyugyiratokService.IncrementUtolsoAlszam(_ExecParam, _EREC_UgyUgyiratok);

                    #endregion
                }

                // Munkaanyagnál az utolsó munkairat-sorszámot kell állítani:
                if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                {
                    Logger.Debug("Utolsó munkairat sorszám állítása", _ExecParam);

                    int UjMunkairatSorszam = 0;

                    if (String.IsNullOrEmpty(_EREC_UgyUgyiratok.UtolsoSorszam))
                    {
                        UjMunkairatSorszam = 1;
                    }
                    else
                    {
                        UjMunkairatSorszam = Int32.Parse(_EREC_UgyUgyiratok.UtolsoSorszam) + 1;
                    }

                    Logger.Debug(String.Format("Munkairat sorszám:{0}", UjMunkairatSorszam));

                    // Utolsó sorszám mező állítása:
                    _EREC_UgyUgyiratok.UtolsoSorszam = UjMunkairatSorszam.ToString();
                    _EREC_UgyUgyiratok.Updated.UtolsoSorszam = true;
                }
            }

            #region ügyirat határidő módosítás, ha szükséges
            if (!String.IsNullOrEmpty(ip.UgyiratUjHatarido))
            {
                _EREC_UgyUgyiratok.Hatarido = ip.UgyiratUjHatarido;
                _EREC_UgyUgyiratok.Updated.Hatarido = true;
            }
            #endregion ügyirat határidő módosítás, ha szükséges

            // Ügyirat újranyitása:
            if (ugyiratUjranyitasaSzukseges == true)
            {
                Logger.Warn("Ügyirat lezárt, ügyirat újranyitása", _ExecParam);

                _EREC_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
                _EREC_UgyUgyiratok.Updated.Allapot = true;

                // Felelőst nem kell változtatni: (2008.07.03)
                //// felelős csoport az iktató lesz:
                //_EREC_UgyUgyiratok.Csoport_Id_Felelos_Elozo = _EREC_UgyUgyiratok.Csoport_Id_Felelos;
                //_EREC_UgyUgyiratok.Updated.Csoport_Id_Felelos_Elozo = true;

                //_EREC_UgyUgyiratok.Csoport_Id_Felelos = Csoportok.GetFelhasznaloSajatCsoportId(_ExecParam);
                //_EREC_UgyUgyiratok.Updated.Csoport_Id_Felelos = true;
            }

            if (utolagosPotlasSzukseges == true)
            {
                Logger.Warn("Ügyirat lezárt, és nem kell újranyitni --> utólagos pótlás", _ExecParam);

                // do nothing 
                //(marad lezárt az ügyirat)
            }


            #region Irattárban van-e/Kikölcsönzött?
            bool kolcsonzesInditasKell = false;
            bool atmenetiIrattarbolKikeresKell = false;// lehet átmeneti vagy skontró irattár is
            bool kikolcsonzott = false;

            Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.None;

            if (_EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott
                || _EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott)
            {
                string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam_ugyiratUpdate).Obj_Id;

                // Ha központi irattárban van:
                if (!String.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Felelos) && !String.IsNullOrEmpty(kozpontiIrattarId)
                    && _EREC_UgyUgyiratok.Csoport_Id_Felelos.ToLower() == kozpontiIrattarId.ToLower())
                {
                    kolcsonzesInditasKell = true;
                }
                else
                {
                    // Átmeneti irattárban van:
                    atmenetiIrattarbolKikeresKell = true;
                    kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.AtmenetiIrattar;
                }


                /// Irattárba küldött állapotnál előfordulhat, hogy valaki már beleiktatott, és keletkezett kikérő
                /// Ilyenkor már nem indítunk új kikérést
                if (_EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
                {
                    // ha van találat --> már kikérték, nem kell újra kikérni:
                    if (erec_ugyugyiratokService.ExistsIrattariKikero(_EREC_UgyUgyiratok.Id, _ExecParam.Clone()))
                    {
                        kolcsonzesInditasKell = false;
                        atmenetiIrattarbolKikeresKell = false;
                    }
                }

            }
            else if (_EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban)
            {
                // Skontró irattárban van-e:
                string skontroIrattarId = KodTarak.SPEC_SZERVEK.GetSkontroIrattaros(execParam_ugyiratUpdate).Obj_Id;
                if (!String.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Felelos) && !String.IsNullOrEmpty(skontroIrattarId)
                && _EREC_UgyUgyiratok.Csoport_Id_Felelos.ToLower() == skontroIrattarId.ToLower())
                {
                    atmenetiIrattarbolKikeresKell = true;
                    kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.SkontroIrattar;
                }
            }
            else if (_EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott)
            {
                kikolcsonzott = true;
            }

            #endregion

            // Ha kikölcsönzött ügyiratba iktatunk, állapotot át kell állítani lezártra (az irattári kikérőt pedig Ügyintézésre kikértre)
            if (kikolcsonzott == true)
            {
                // Állapot állítása:
                _EREC_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                _EREC_UgyUgyiratok.Updated.Allapot = true;
            }

            // BUG_8122
            //Ha elintézett visszakerül az iktatóhoz, és ügyintézés alatti állapotba lesz
            erec_ugyugyiratokService.ElintezetteNyilvanitasVisszavonasaInternal(_ExecParam.Clone(), _EREC_UgyUgyiratok);

            // UgyintezesModját átállítani vegyesre, ha szükséges
            string jelleg = KodTarak.UGYIRAT_JELLEG.Papir;
            if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas)
            {
                jelleg = _EREC_IraIratok.AdathordozoTipusa;
            }
            else
            {
                jelleg = erec_KuldKuldemenyek.AdathordozoTipusa;
            }

            //ha üres, akkor papírnak számít
            if (String.IsNullOrEmpty(jelleg))
            {
                jelleg = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
            }

            if (jelleg == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir && _EREC_UgyUgyiratok.Jelleg != KodTarak.UGYIRAT_JELLEG.Elektronikus
                || jelleg == KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir && _EREC_UgyUgyiratok.Jelleg != KodTarak.UGYIRAT_JELLEG.Papir)
            {
                _EREC_UgyUgyiratok.Jelleg = KodTarak.UGYIRAT_JELLEG.Vegyes;
                _EREC_UgyUgyiratok.Updated.Jelleg = true;
            }

            // BUG_8122
            ////Ha elintézett visszakerül az iktatóhoz, és ügyintézés alatti állapotba lesz
            //erec_ugyugyiratokService.ElintezetteNyilvanitasVisszavonasaInternal(_ExecParam.Clone(), _EREC_UgyUgyiratok);

            Result result_ugyiratUpdate = erec_ugyugyiratokService.Update(execParam_ugyiratUpdate, _EREC_UgyUgyiratok);
            if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
            {
                // hiba
                throw new ResultException(result_ugyiratUpdate);
            }
            #endregion


            #region Irattárban lévő ügyiratnál további műveletek

            if (atmenetiIrattarbolKikeresKell == true)
            {
                #region Kikérés indítása:
                Result result_kikeres = erec_ugyugyiratokService.Kikeres(_EREC_UgyUgyiratok.Id, kikeresIrattarTipus, _ExecParam.Clone(), "Iktatás");

                #endregion
            }
            else if (kolcsonzesInditasKell == true)
            {
                #region Kölcsönzés indítása, automatikus jóváhagyással:
                Result result_kikeres = erec_ugyugyiratokService.Kolcsonzes(_EREC_UgyUgyiratok.Id, _ExecParam.Clone(), "Iktatás");

                #endregion
            }
            else if (kikolcsonzott == true)
            {
                // Irattári kikérőt Ügyintézésre kikértre állítjuk:
                EREC_IrattariKikeroService service_irattariKikero = new EREC_IrattariKikeroService(this.dataContext);
                ExecParam execParam_kikeroGet = _ExecParam.Clone();
                execParam_kikeroGet.Record_Id = _EREC_UgyUgyiratok.Id;

                Result result_kikeroGet = service_irattariKikero.GetByUgyiratId(execParam_kikeroGet);
                if (!String.IsNullOrEmpty(result_kikeroGet.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_kikeroGet);
                }
                if (result_kikeroGet.Record != null)
                {
                    EREC_IrattariKikero irattariKikero = (EREC_IrattariKikero)result_kikeroGet.Record;
                    irattariKikero.Updated.SetValueAll(false);
                    irattariKikero.Base.Updated.SetValueAll(false);
                    irattariKikero.Base.Updated.Ver = true;

                    // Ügyintézésre kikértre kell állítani:
                    irattariKikero.FelhasznalasiCel = "U";
                    irattariKikero.Updated.FelhasznalasiCel = true;

                    ExecParam execParam_irattariKikeroUpdate = _ExecParam.Clone();
                    execParam_irattariKikeroUpdate.Record_Id = irattariKikero.Id;

                    Result result_irattariKikeroUpdate = service_irattariKikero.Update(execParam_irattariKikeroUpdate, irattariKikero);
                    if (!string.IsNullOrEmpty(result_irattariKikeroUpdate.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_irattariKikeroUpdate);
                    }
                }
            }

            #endregion

            EREC_UgyUgyiratdarabokService service_ugyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);

            if (ugyiratUjranyitasaSzukseges == true)
            {
                // az ügyirat újra lett nyitva:
                // lezárt ügyiratdarabok állapotát vissza kell állítani
                // kell??

                #region ÜgyiratDarabok állapotának visszaállítása

                ExecParam execParam_ugyiratDarabGetAll = _ExecParam.Clone();

                EREC_UgyUgyiratdarabokSearch search = new EREC_UgyUgyiratdarabokSearch();

                search.UgyUgyirat_Id.Value = _EREC_UgyUgyiratok.Id;
                search.UgyUgyirat_Id.Operator = Query.Operators.equals;

                Result result_ugyiratDarabGetAll = service_ugyiratDarabok.GetAll(execParam_ugyiratDarabGetAll, search);
                if (!String.IsNullOrEmpty(result_ugyiratDarabGetAll.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_ugyiratDarabGetAll);
                }

                foreach (DataRow row in result_ugyiratDarabGetAll.Ds.Tables[0].Rows)
                {
                    string UgyUgyirat_Id = row["UgyUgyirat_Id"].ToString();
                    // ellenőrzés:
                    if (UgyUgyirat_Id != _EREC_UgyUgyiratok.Id)
                    {
                        // hiba:
                        Logger.Error("Hiba az ügyiratdarab lekérdezésekor", _ExecParam);
                        throw new ResultException(52110);
                    }

                    string Id = row["Id"].ToString();
                    string Ver = row["Ver"].ToString();

                    // Ügyiratdarab UPDATE:
                    EREC_UgyUgyiratdarabok erec_ugyugyiratDarab = new EREC_UgyUgyiratdarabok();
                    erec_ugyugyiratDarab.Updated.SetValueAll(false);
                    erec_ugyugyiratDarab.Base.Updated.SetValueAll(false);

                    erec_ugyugyiratDarab.Base.Ver = Ver;
                    erec_ugyugyiratDarab.Base.Updated.Ver = true;

                    erec_ugyugyiratDarab.LezarasDat = "";
                    erec_ugyugyiratDarab.Updated.LezarasDat = true;

                    erec_ugyugyiratDarab.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott;
                    erec_ugyugyiratDarab.Updated.Allapot = true;

                    ExecParam execParam_ugyiratDarabUpdate = _ExecParam.Clone();
                    execParam_ugyiratDarabUpdate.Record_Id = Id;

                    Result result_ugyiratDarabUpdate =
                        service_ugyiratDarabok.Update(execParam_ugyiratDarabUpdate, erec_ugyugyiratDarab);

                    if (!String.IsNullOrEmpty(result_ugyiratDarabUpdate.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_ugyiratDarabUpdate);
                    }

                }



                #endregion

            }

            #region ÜgyiratDarab GET

            // Az ügyirat 'default' ügyiratdarabjának lekérdezése:

            ExecParam execParam_ugyiratDarabGet = _ExecParam.Clone();
            Result result_ugyiratDarabGet =
                service_ugyiratDarabok.GetDefaultUgyiratDarab(execParam_ugyiratDarabGet, _EREC_UgyUgyiratok.Id);
            if (!String.IsNullOrEmpty(result_ugyiratDarabGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratDarabGet);
            }

            if (result_ugyiratDarabGet.Record != null)
            {
                _EREC_UgyUgyiratdarabok = (EREC_UgyUgyiratdarabok)result_ugyiratDarabGet.Record;
            }
            else
            {
                // nem található default ügyiratdarab --> létrehozunk egyet
                // TODO: dobjunk inkább hibát??

                #region ÜgyiratDarab INSERT

                ExecParam execParam_ugyiratdarabInsert = _ExecParam.Clone();

                if (_EREC_UgyUgyiratdarabok == null)
                {
                    _EREC_UgyUgyiratdarabok = new EREC_UgyUgyiratdarabok();
                }

                // Ügyirathoz kapcsolás
                _EREC_UgyUgyiratdarabok.UgyUgyirat_Id = _EREC_UgyUgyiratok.Id;
                _EREC_UgyUgyiratdarabok.Updated.UgyUgyirat_Id = true;


                // NEM LENNE szükséges, csak a régi módszer miatt a felületen még sok helyen ezt jelenítjük meg                
                // ügyiratdarab-hoz főszám és iktatókönyv_id beállítása 
                // (UGYANAZ, mint ami az ügyiratban van)
                _EREC_UgyUgyiratdarabok.Foszam = _EREC_UgyUgyiratok.Foszam;
                _EREC_UgyUgyiratdarabok.Updated.Foszam = true;

                _EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = _EREC_UgyUgyiratok.IraIktatokonyv_Id;
                _EREC_UgyUgyiratdarabok.Updated.IraIktatokonyv_Id = true;

                //Ügyintéző átvétele az ügyiratból // KELL??
                _EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez = _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
                _EREC_UgyUgyiratdarabok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

                // Állapot beállítása:
                if (_EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart)
                {
                    _EREC_UgyUgyiratdarabok.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Lezart;
                }
                else
                {
                    _EREC_UgyUgyiratdarabok.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott;
                }
                _EREC_UgyUgyiratdarabok.Updated.Allapot = true;

                // Eljárási szakasz: Osztatlan
                _EREC_UgyUgyiratdarabok.EljarasiSzakasz = KodTarak.ELJARASI_SZAKASZ.Osztatlan;
                _EREC_UgyUgyiratdarabok.Updated.EljarasiSzakasz = true;

                Result result_ugyiratdarabInsert =
                   service_ugyiratDarabok.Insert(execParam_ugyiratdarabInsert, _EREC_UgyUgyiratdarabok);
                if (!String.IsNullOrEmpty(result_ugyiratdarabInsert.ErrorCode))
                {
                    // hiba
                    throw new ResultException(result_ugyiratdarabInsert);
                }

                _EREC_UgyUgyiratdarabok.Id = result_ugyiratdarabInsert.Uid;

                #endregion

            }


            #endregion


        }
        else
        {
            // Ilyen lehetőség elvileg nincs:
            Logger.Error("VALAMI EL VAN RONTVA AZ IKTATÁSNÁL", _ExecParam);
            throw new ResultException(52100);
        }





        /******************
         Ügyirat, ügyiratdarab létrehozásának/módosításának vége;
              
         Innentől Irat, IratPéldányok létrehozása
        ***********/

        #region UgyintezesKezdoDatuma
        try
        {
            SakkoraService sakkoraSvc = new SakkoraService(this.dataContext);
            sakkoraSvc.SetUgyUgyintezesKezdoDatumaKuldemenyBeerkezesere(ref _EREC_IraIratok, erec_KuldKuldemenyek);
        }
        catch (Exception x)
        {
            Logger.Error("SetUgyUgyintezesKezdoDatumaKuldemenyBeerkezesere", x);
        }
        #endregion

        Logger.Debug("Irat Insert Start", _ExecParam);

        #region Irat INSERT
        // Irat INSERT
        EREC_IraIratokService erec_IraIratokService = new EREC_IraIratokService(this.dataContext);
        ExecParam execParam_iratInsert = _ExecParam.Clone();

        // Irat Note mezőjében van letárolva az iratpéldánynál használatos elosztóívId, azt innen ki kell szedni
        EREC_IraElosztoivek elosztoiv = null;
        //if (!string.IsNullOrEmpty(_EREC_IraIratok.Base.Note))
        string elosztoIv_Id = Contentum.eRecord.BaseUtility.Iratok.GetElosztoivIdFromBusinessObjectsXML(_EREC_IraIratok.Base.Note);
        if (!string.IsNullOrEmpty(elosztoIv_Id))
        {
            EREC_IraElosztoivekService elosztoivService = new EREC_IraElosztoivekService(this.dataContext);
            ExecParam elosztoivExecParam = _ExecParam.Clone();
            //elosztoivExecParam.Record_Id = _EREC_IraIratok.Base.Note;
            elosztoivExecParam.Record_Id = elosztoIv_Id;

            Result elosztoivGetResult = elosztoivService.Get(elosztoivExecParam);
            if (!string.IsNullOrEmpty(elosztoivGetResult.ErrorCode))
                throw new ResultException(53401);

            elosztoiv = (EREC_IraElosztoivek)elosztoivGetResult.Record;
            _EREC_IraIratok.Base.Note = string.Empty;
            _EREC_IraIratok.Base.Updated.Note = false;
        }

        // ügyiratdarabhoz kapcsolás:
        _EREC_IraIratok.UgyUgyIratDarab_Id = _EREC_UgyUgyiratdarabok.Id;
        _EREC_IraIratok.Updated.UgyUgyIratDarab_Id = true;

        // Ügyirathoz kapcsolás, hogy ki lehessen hagyni az ügyiratdarab szintet:
        _EREC_IraIratok.Ugyirat_Id = _EREC_UgyUgyiratok.Id;
        _EREC_IraIratok.Updated.Ugyirat_Id = true;

        if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
        {
            #region TÜK-ös Bejövő iktatásnál küldemény néhány mezőjének átmásolása (BLG#1402)

            if (isTUK)
            {
                /// Ezeket a mezőket át kell másolni a küldeményből az iratba:
                /// MinositesErvenyessegiIdeje, Minosito, TerjedelemMegjegyzes, TerjedelemMennyiseg, TerjedelemMennyisegiEgyseg
                /// 
                if (erec_KuldKuldemenyek != null)
                {
                    _EREC_IraIratok.MinositesErvenyessegiIdeje = erec_KuldKuldemenyek.MinositesErvenyessegiIdeje;
                    _EREC_IraIratok.Updated.MinositesErvenyessegiIdeje = true;

                    // BUG_8204
                    //_EREC_IraIratok.Minosito = erec_KuldKuldemenyek.Minosito;
                    //_EREC_IraIratok.Updated.Minosito = true;

                    _EREC_IraIratok.TerjedelemMegjegyzes = erec_KuldKuldemenyek.TerjedelemMegjegyzes;
                    _EREC_IraIratok.Updated.TerjedelemMegjegyzes = true;

                    _EREC_IraIratok.TerjedelemMennyiseg = erec_KuldKuldemenyek.TerjedelemMennyiseg;
                    _EREC_IraIratok.Updated.TerjedelemMennyiseg = true;

                    _EREC_IraIratok.TerjedelemMennyisegiEgyseg = erec_KuldKuldemenyek.TerjedelemMennyisegiEgyseg;
                    _EREC_IraIratok.Updated.TerjedelemMennyisegiEgyseg = true;
                }
            }

            #endregion

            // küldemény id beállítása
            _EREC_IraIratok.KuldKuldemenyek_Id = ip.KuldemenyId;
            _EREC_IraIratok.Updated.KuldKuldemenyek_Id = true;

            // hivatkozási szám átvétele a küldeményből
            _EREC_IraIratok.HivatkozasiSzam = erec_KuldKuldemenyek.HivatkozasiSzam;
            _EREC_IraIratok.Updated.HivatkozasiSzam = true;

            // UgyintezesAlapja (Modja) átvétele a küldeményből
            // BLG_361
            if (String.IsNullOrEmpty(_EREC_IraIratok.UgyintezesAlapja))
            {
                // BUG#7141:
                // Ez az elsődleges adathordozó típusa:
                _EREC_IraIratok.UgyintezesAlapja = erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa; //erec_KuldKuldemenyek.UgyintezesModja;
                _EREC_IraIratok.Updated.UgyintezesAlapja = true;
            }
        }
        else if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas)
        {
            // Belső irat iktatásnál az iratpéldányból vesszük az UgyintezesAlapja-t 
            if (_EREC_PldIratPeldanyok != null)
            {
                // BLG_1876 
                // BLG Kapcsán javítva (BLG_361-ben kezeltük ezt)
                //_EREC_IraIratok.UgyintezesAlapja = _EREC_PldIratPeldanyok.UgyintezesModja;
                _EREC_IraIratok.AdathordozoTipusa = _EREC_PldIratPeldanyok.UgyintezesModja;
                _EREC_IraIratok.Updated.AdathordozoTipusa = true;
            }
        }


        // Postázás iránya: Bejövő vagy Belső
        if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
        {
            _EREC_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
        }
        else
        {
            // Lehet kimenő és belső is, felületen választható (BLG_635):
            if (_EREC_IraIratok.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Belso
                && _EREC_IraIratok.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Kimeno)
            {
                _EREC_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Belso;
            }
        }

        _EREC_IraIratok.Updated.PostazasIranya = true;

        if (!ip.TomegesIktatasVegrehajtasa)
        {
            // Alszám, iktatás dátuma csak Iktatott anyagnál, munkaanyagnál nem kell:
            if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            {
                // IKTATOTT ANYAG

                // alszám beállítása
                //_EREC_IraIratok.Alszam = ujAlszam.ToString();
                // az ügyirat utolsó alszáma már a megnövelt alszámot tartalmazza:
                _EREC_IraIratok.Alszam = _EREC_UgyUgyiratok.UtolsoAlszam;
                _EREC_IraIratok.Updated.Alszam = true;

                // iktatás dátuma
                if ("MIGRATION".Equals(System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"], StringComparison.InvariantCultureIgnoreCase) || String.IsNullOrEmpty(_EREC_IraIratok.IktatasDatuma))
                {
                    _EREC_IraIratok.IktatasDatuma = DateTime.Now.ToString();
                }
                _EREC_IraIratok.Updated.IktatasDatuma = true;
            }
            else
            {
                // MUNKAANYAG

                // Munkaanyagnál Sorszám beállítása (az ügyiratban már beállított utolsóSorszám mező értékére)
                _EREC_IraIratok.Sorszam = _EREC_UgyUgyiratok.UtolsoSorszam;
                _EREC_IraIratok.Updated.Sorszam = true;
            }
        }
        else
        {
            _EREC_IraIratok.Updated.Alszam = false;
            _EREC_IraIratok.Updated.IktatasDatuma = false;
            _EREC_IraIratok.Updated.Sorszam = false;
        }

        // iktató beállítása
        _EREC_IraIratok.FelhasznaloCsoport_Id_Iktato = FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
        _EREC_IraIratok.Updated.FelhasznaloCsoport_Id_Iktato = true;

        // őrző állítása (iktató)
        _EREC_IraIratok.FelhasznaloCsoport_Id_Orzo = _EREC_IraIratok.FelhasznaloCsoport_Id_Iktato;//FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
        _EREC_IraIratok.Updated.FelhasznaloCsoport_Id_Orzo = true;

        // felelős állítása
        if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
        {
            // felelős állítása (az ügyiratéval azonos)
            _EREC_IraIratok.Csoport_Id_Felelos = _EREC_UgyUgyiratok.Csoport_Id_Felelos;
            _EREC_IraIratok.Updated.Csoport_Id_Felelos = true;
        }
        else
        {
            // felelős állítása ( = iktató)
            _EREC_IraIratok.Csoport_Id_Felelos = FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
            _EREC_IraIratok.Updated.Csoport_Id_Felelos = true;
        }



        // ügyintéző átvétele az ügyiratból
        //_EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez = _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
        //_EREC_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;


        // Állapot
        if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
        {
            _EREC_IraIratok.Allapot = KodTarak.IRAT_ALLAPOT.Iktatott;
            _EREC_IraIratok.Updated.Allapot = true;
        }
        else
        {
            _EREC_IraIratok.Allapot = KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat;
            _EREC_IraIratok.Updated.Allapot = true;
        }

        // utolsó sorszám beállítása 1-re, mivel az első iratpéldányt mindenképpen létrehozzuk
        _EREC_IraIratok.UtolsoSorszam = "1";
        _EREC_IraIratok.Updated.UtolsoSorszam = true;

        if (!String.IsNullOrEmpty(_EREC_IraIratok.Irattipus))
        {
            #region IratMetaDefId beállítása:

            EREC_IratMetaDefinicioService service_iratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);

            string eljarasiSzakasz = "";
            if (_EREC_UgyUgyiratdarabok != null && !String.IsNullOrEmpty(_EREC_UgyUgyiratdarabok.EljarasiSzakasz))
            {
                eljarasiSzakasz = _EREC_UgyUgyiratdarabok.EljarasiSzakasz;
            }
            else
            {
                eljarasiSzakasz = KodTarak.ELJARASI_SZAKASZ.Osztatlan;
            }

            Result result_iratMetaDef = service_iratMetaDef.GetIratMetaDefinicioByIrattipus(_ExecParam.Clone()
                , _EREC_UgyUgyiratok.IraIrattariTetel_Id, _EREC_UgyUgyiratok.UgyTipus, eljarasiSzakasz, _EREC_IraIratok.Irattipus);
            if (!String.IsNullOrEmpty(result_iratMetaDef.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_iratMetaDef);
            }

            EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_iratMetaDef.Record;
            if (erec_IratMetaDefinicio != null)
            {
                _EREC_IraIratok.IratMetaDef_Id = erec_IratMetaDefinicio.Id;
                _EREC_IraIratok.Updated.IratMetaDef_Id = true;

                _EREC_IraIratok.GeneraltTargy = erec_IratMetaDefinicio.GeneraltTargy;
                _EREC_IraIratok.Updated.GeneraltTargy = true;
            }

            #endregion
        }

        /// Azonosító (Iktatószám) beállítása
        if (!ip.TomegesIktatasVegrehajtasa)
        {
            // BLG_292
            //_EREC_IraIratok.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok);
            _EREC_IraIratok.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok);
            _EREC_IraIratok.Updated.Azonosito = true;
        }
        else
        {
            _EREC_IraIratok.Updated.Azonosito = false;
        }

        #endregion


        //// Volt-e előkészített irat a küldeményhez? 
        //if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas
        //    && erec_KuldKuldemenyek != null && !String.IsNullOrEmpty(erec_KuldKuldemenyek.IraIratok_Id))
        //{
        //    Logger.Info("Volt előkészített irat a küldeményhez", _ExecParam);


        //    #region Előkészített irat GET

        //    Logger.Debug("Előkészített irat GET", _ExecParam);
        //    ExecParam execParam_elokeszitettIratGet = _ExecParam.Clone();
        //    execParam_elokeszitettIratGet.Record_Id = erec_KuldKuldemenyek.IraIratok_Id;

        //    Result result_elokeszitettIratGet = erec_IraIratokService.Get(execParam_elokeszitettIratGet);
        //    if (!String.IsNullOrEmpty(result_elokeszitettIratGet.ErrorCode))
        //    {
        //        // hiba:
        //        Logger.Error("Hiba az előkészített irat lekérése során", _ExecParam, result_elokeszitettIratGet);

        //        throw new ResultException(result_elokeszitettIratGet);
        //    }

        //    EREC_IraIratok eloKeszitettIrat = (EREC_IraIratok)result_elokeszitettIratGet.Record;

        //    if (eloKeszitettIrat != null
        //        && (String.IsNullOrEmpty(eloKeszitettIrat.Allapot) || eloKeszitettIrat.Allapot == KodTarak.IRAT_ALLAPOT.Iktatasraelokeszitett))
        //    {
        //        // Id, Ver átmásolása az updatehez
        //        _EREC_IraIratok.Id = eloKeszitettIrat.Id;

        //        _EREC_IraIratok.Base.Ver = eloKeszitettIrat.Base.Ver;
        //        _EREC_IraIratok.Base.Updated.Ver = true;

        //        iratUpdateKell = true;
        //    }
        //    else
        //    {
        //        Logger.Warn("Valami nem ok az előkészített irattal --> Update helyett új Irat Insert", _ExecParam);
        //    }

        //    #endregion
        //}


        String UjIrat_Id = String.Empty;
        if (!ip.TomegesIktatasVegrehajtasa)
        {
            Result result_iratInsert = erec_IraIratokService.Insert(execParam_iratInsert, _EREC_IraIratok);

            if (!String.IsNullOrEmpty(result_iratInsert.ErrorCode))
            {
                Logger.Error("Irat Insert-nél hiba", _ExecParam, result_iratInsert);
                // hiba
                throw new ResultException(result_iratInsert);
            }
            else
            {
                UjIrat_Id = result_iratInsert.Uid;
            }
        }
        else
        {
            ExecParam exec_tomegesIktatasUpdate = _ExecParam.Clone();
            exec_tomegesIktatasUpdate.Record_Id = _EREC_IraIratok.Id;

            Result iratokGetVer = erec_IraIratokService.Get(exec_tomegesIktatasUpdate);
            if (iratokGetVer.IsError)
            {
                Logger.Error("Irat lekerese sikertelen", exec_tomegesIktatasUpdate, iratokGetVer);
                throw new ResultException(iratokGetVer);
            }

            _EREC_IraIratok.Base.Ver = (iratokGetVer.Record as EREC_IraIratok).Base.Ver;

            Result result_tomegesIktatasUpdate = erec_IraIratokService.Update(exec_tomegesIktatasUpdate, _EREC_IraIratok);
            if (!String.IsNullOrEmpty(result_tomegesIktatasUpdate.ErrorCode))
            {
                Logger.Debug("Irat Update sikertelen: ", exec_tomegesIktatasUpdate, result_tomegesIktatasUpdate);
                throw new ResultException(result_tomegesIktatasUpdate);
            }
            else
            {
                UjIrat_Id = _EREC_IraIratok.Id;
            }
        }

        _EREC_IraIratok.Id = UjIrat_Id;

        //iktatasResult.UjIrat_Id = result_iratInsert.Uid;


        // Kezelési feljegyzések felvétel, ha meg van adva
        if (_EREC_HataridosFeladatok != null)
        {
            // Irat kezelési feljegyzés INSERT
            EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
            ExecParam execParam_kezfeljegyzesek = _ExecParam.Clone();

            _EREC_HataridosFeladatok.Obj_Id = UjIrat_Id;
            _EREC_HataridosFeladatok.Updated.Obj_Id = true;

            _EREC_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;
            _EREC_HataridosFeladatok.Updated.Obj_type = true;

            if (!String.IsNullOrEmpty(_EREC_IraIratok.Azonosito))
            {
                _EREC_HataridosFeladatok.Azonosito = _EREC_IraIratok.Azonosito;
                _EREC_HataridosFeladatok.Updated.Azonosito = true;
            }

            Result result_kezfeljegyzes = erec_HataridosFeladatokService.Insert(execParam_kezfeljegyzesek, _EREC_HataridosFeladatok);
            if (!String.IsNullOrEmpty(result_kezfeljegyzes.ErrorCode))
            {
                Logger.Error("Kezelési feljegyzés létrehozásánál hiba", _ExecParam, result_kezfeljegyzes);

                // hiba
                throw new ResultException(result_kezfeljegyzes);
            }
        }

        #region Küldemény mellékletek és csatolmányok átmásolása az irathoz
        if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
        {
            ExecParam execParam_MellekletCopy = _ExecParam.Clone();

            Copy_KuldMellekletek_Csatolmanyok(execParam_MellekletCopy, erec_KuldKuldemenyek.Id, _EREC_IraIratok.Id, erec_IraIktatoKonyvek.Iktatohely, _EREC_UgyUgyiratok.Foszam, erec_IraIktatoKonyvek.Ev, _EREC_IraIratok.Alszam);
        }
        #endregion

        #region Mellékletek (TÜK-ös belső irat iktatásnál, BLG#1402)

        if (isTUK && ip != null && ip.Mellekletek != null && ip.Mellekletek.Count > 0)
        {
            Logger.Debug(String.Format("Mellékletek száma: {0}", ip.Mellekletek.Count));

            EREC_MellekletekService svcMellekletek = new EREC_MellekletekService(this.dataContext);
            ExecParam xpmMellekletek = execParam_iratInsert.Clone();
            Result resMellekletek;

            foreach (EREC_Mellekletek erec_Mellekletek in ip.Mellekletek)
            {
                erec_Mellekletek.IraIrat_Id = _EREC_IraIratok.Id;
                resMellekletek = svcMellekletek.Insert(xpmMellekletek, erec_Mellekletek);

                if (resMellekletek.IsError)
                {
                    throw new ResultException(resMellekletek);
                }
            }
        }

        #endregion

        #region EREC_IraOnkormAdatok INSERT

        ExecParam execparam_IraOnkormAdatok = _ExecParam.Clone();
        execparam_IraOnkormAdatok.Record_Id = _EREC_IraIratok.Id;

        if (!ip.TomegesIktatasVegrehajtasa)
        {
            // mind a rekord Id-je, mind az IraIratok_Id megegyezik az irat azonosítójával
            EREC_IraOnkormAdatokService service_IraOnkormAdatok = new EREC_IraOnkormAdatokService(this.dataContext);

            Logger.Debug("Hatósági adatok INSERT start", execparam_IraOnkormAdatok);

            EREC_IraOnkormAdatok erec_IraOnkormAdatok = new EREC_IraOnkormAdatok();
            erec_IraOnkormAdatok.Id = _EREC_IraIratok.Id;
            erec_IraOnkormAdatok.Updated.Id = true;
            erec_IraOnkormAdatok.IraIratok_Id = _EREC_IraIratok.Id;
            erec_IraOnkormAdatok.Updated.IraIratok_Id = true;

            bool IKTATAS_UGYTIPUS_IRATMETADEFBOL_VALUE = (Rendszerparameterek.GetBoolean(execparam_IraOnkormAdatok, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true));

            // BLG_2020
            if (IKTATAS_UGYTIPUS_IRATMETADEFBOL_VALUE)
            {
                EREC_IratMetaDefinicioService service_IratMetaDefinicio = new EREC_IratMetaDefinicioService(this.dataContext);
                ExecParam execparam_IratMetaDefinicio = _ExecParam.Clone();
                Result result_IraMetaDefinicio;

                // UgyFajta kiolvasasa az EREC_IratMetaDefinicio alapján
                // (nem hatósági v. önkormányzati v. államigazgatási)
                if (!String.IsNullOrEmpty(_EREC_UgyUgyiratok.IratMetadefinicio_Id))
                {
                    execparam_IratMetaDefinicio.Record_Id = _EREC_UgyUgyiratok.IratMetadefinicio_Id;
                    result_IraMetaDefinicio = service_IratMetaDefinicio.Get(execparam_IratMetaDefinicio);
                    if (!String.IsNullOrEmpty(result_IraMetaDefinicio.ErrorCode))
                    {
                        Logger.Debug("Irat metadefiníció ill. ügyfajta meghatározása sikertelen: ", execparam_IratMetaDefinicio, result_IraMetaDefinicio);

                        // hiba 
                        throw new ResultException(result_IraMetaDefinicio);
                    }

                }
                if (!String.IsNullOrEmpty(_EREC_UgyUgyiratok.UgyTipus) && !String.IsNullOrEmpty(_EREC_UgyUgyiratok.IraIrattariTetel_Id))
                {
                    EREC_IratMetaDefinicioSearch search_IratMetaDefinicio = new EREC_IratMetaDefinicioSearch();
                    search_IratMetaDefinicio.Ugytipus.Value = _EREC_UgyUgyiratok.UgyTipus;
                    search_IratMetaDefinicio.Ugytipus.Operator = Query.Operators.equals;

                    search_IratMetaDefinicio.Ugykor_Id.Value = _EREC_UgyUgyiratok.IraIrattariTetel_Id;
                    search_IratMetaDefinicio.Ugykor_Id.Operator = Query.Operators.equals;

                    search_IratMetaDefinicio.EljarasiSzakasz.Operator = Query.Operators.isnull;
                    search_IratMetaDefinicio.Irattipus.Operator = Query.Operators.isnull;

                    search_IratMetaDefinicio.TopRow = 1;

                    search_IratMetaDefinicio.ErvKezd.Clear();
                    search_IratMetaDefinicio.ErvVege.Clear();

                    search_IratMetaDefinicio.OrderBy = "EREC_IratMetaDefinicio.LetrehozasIdo desc";

                    result_IraMetaDefinicio = service_IratMetaDefinicio.GetAll(execparam_IratMetaDefinicio, search_IratMetaDefinicio);

                    if (!String.IsNullOrEmpty(result_IraMetaDefinicio.ErrorCode))
                    {
                        Logger.Debug("Irat metadefiníció ill. ügyfajta meghatározása sikertelen: ", execparam_IratMetaDefinicio, result_IraMetaDefinicio);

                        // hiba 
                        throw new ResultException(result_IraMetaDefinicio);
                    }
                    else
                    {
                        if (result_IraMetaDefinicio.Ds.Tables[0].Rows.Count > 0)
                        {
                            erec_IraOnkormAdatok.UgyFajtaja = result_IraMetaDefinicio.Ds.Tables[0].Rows[0]["UgyFajta"].ToString();
                            erec_IraOnkormAdatok.Updated.UgyFajtaja = true;
                        }
                        else
                        {
                            // nincs definíció az UgyFajtaja-ra
                            erec_IraOnkormAdatok.UgyFajtaja = "<null>";
                            erec_IraOnkormAdatok.Updated.UgyFajtaja = true;
                        }
                    }
                }
                else
                {
                    // UgyTipus üres
                    erec_IraOnkormAdatok.UgyFajtaja = "<null>";
                    erec_IraOnkormAdatok.Updated.UgyFajtaja = true;
                }
            }
            else
            {
                erec_IraOnkormAdatok.UgyFajtaja = _EREC_UgyUgyiratok.Ugy_Fajtaja;
                erec_IraOnkormAdatok.Updated.UgyFajtaja = true;
            }

            Result result_IraOnkormAdatokInsert = service_IraOnkormAdatok.Insert(execparam_IraOnkormAdatok, erec_IraOnkormAdatok);
            if (!String.IsNullOrEmpty(result_IraOnkormAdatokInsert.ErrorCode))
            {
                Logger.Debug("Önkormányzati adatok Insert sikertelen: ", execparam_IraOnkormAdatok, result_IraOnkormAdatokInsert);

                // hiba 
                throw new ResultException(result_IraOnkormAdatokInsert);
            }
        }

        #endregion

        // Érték nélküli tárgyszavakat majd kézzel felviszi, ha akarja, feleslegesen ne rendeljük itt hozzá (2008.07.04)
        //#region Metaadatok hozzárendelése irathoz
        //// csak az érték nélküli tárgyszavak rendelődnek hozzá, ha van ilyen...

        //EREC_ObjektumTargyszavaiService service_ObjektumTargyszavai = new EREC_ObjektumTargyszavaiService(this.dataContext);
        //ExecParam execParam_ObjektumTargyszavai = _ExecParam.Clone();

        //Logger.Debug("Metadatok automatikus hozzárendelése az irathoz az objektum metadefinició alapján", execParam_ObjektumTargyszavai);

        //Result result_ObjektumTargyszavai = service_ObjektumTargyszavai.AssignByDefinicioTipus(execParam_ObjektumTargyszavai
        //    , _EREC_IraIratok.Id
        //    , null
        //    , "EREC_IraIratok"
        //    , ""
        //    , false);

        //if (!String.IsNullOrEmpty(result_ObjektumTargyszavai.ErrorCode))
        //{
        //    Logger.Debug("Metaadatok automatikus hozzárendelése irathoz sikertelen: ", execParam_ObjektumTargyszavai, result_ObjektumTargyszavai);

        //    // hiba 
        //    throw new ResultException(result_ObjektumTargyszavai);
        //}

        //Logger.Debug("Metadatok automatikus hozzárendelése az irathoz az objektum metadefinició alapján sikeres.");

        //#endregion Metaadatok hozzárendelése irathoz


        Logger.Debug("Iratpéldány Insert Start", _ExecParam);

        #region IratPéldány INSERT
        // IratPéldány INSERT

        if (ip.Atiktatas && !string.IsNullOrEmpty(ip.IratId))
        {
            EREC_PldIratPeldanyokService iratpeldanyService = new EREC_PldIratPeldanyokService(this.dataContext);
            EREC_PldIratPeldanyokSearch iratpeldanySearch = new EREC_PldIratPeldanyokSearch();

            iratpeldanySearch.IraIrat_Id.Value = ip.IratId;
            iratpeldanySearch.IraIrat_Id.Operator = Query.Operators.equals;

            Result iratpeldanyGetAllResult = iratpeldanyService.GetAll(execparam_IraOnkormAdatok.Clone(), iratpeldanySearch);
            if (!string.IsNullOrEmpty(iratpeldanyGetAllResult.ErrorCode) || iratpeldanyGetAllResult.Ds.Tables.Count < 1)
                throw new ResultException(iratpeldanyGetAllResult);

            foreach (DataRow r in iratpeldanyGetAllResult.Ds.Tables[0].Rows)
            {
                ExecParam iratpeldanyGetExecParam = execparam_IraOnkormAdatok.Clone();
                iratpeldanyGetExecParam.Record_Id = r["Id"].ToString();
                EREC_PldIratPeldanyok iratpeldany = (EREC_PldIratPeldanyok)iratpeldanyService.Get(iratpeldanyGetExecParam.Clone()).Record;

                iratpeldany.Updated.SetValueAll(false);
                iratpeldany.Base.Updated.SetValueAll(false);
                iratpeldany.Base.Updated.Ver = true;

                iratpeldany.IraIrat_Id = _EREC_IraIratok.Id;
                iratpeldany.Updated.IraIrat_Id = true;

                /// Azonosító (Iktatószám) felülírása:
                /// 
                // BLG_292
                //iratpeldany.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, iratpeldany);
                iratpeldany.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, iratpeldany);
                iratpeldany.Updated.Azonosito = true;

                Result iratpeldanyUpdateResult = iratpeldanyService.Update(iratpeldanyGetExecParam.Clone(), iratpeldany);
                if (!string.IsNullOrEmpty(iratpeldanyUpdateResult.ErrorCode))
                    throw new ResultException(iratpeldanyUpdateResult);

            }
        }
        else
        {
            EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
            ExecParam execParam_IratPeldanyInsert = _ExecParam.Clone();

            if (_EREC_PldIratPeldanyok == null) { _EREC_PldIratPeldanyok = new EREC_PldIratPeldanyok(); }

            // hivatkozás az új iratra:
            _EREC_PldIratPeldanyok.IraIrat_Id = UjIrat_Id;
            _EREC_PldIratPeldanyok.Updated.IraIrat_Id = true;

            _EREC_PldIratPeldanyok.Eredet = KodTarak.IRAT_FAJTA.Eredeti;
            _EREC_PldIratPeldanyok.Updated.Eredet = true;

            // BejovoIratIktatas -nal Kuldemeny-ből veszünk át néhány mezőt:
            // (címzett, cím, küldés módja, továbbító)
            if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
            {
                // Címzett:

                // Partner kiderítése a Kuldemeny.Csoport_Id_Cimzett alapján
                // Ha egyszemélyes a csoport, akkor van esély megtalálni a partnert:
                // Egyszemélyes csoport esetén a csoportId = felhasznaloId, 
                // és a Felhasznalóhoz lehet hozzárendelve Partner (KRT_Felhasznalok.Partner_id)

                try
                {
                    KRT_Partnerek krt_partner_cimzett = GetKRT_PartnerekByCsoportId(erec_KuldKuldemenyek.Csoport_Id_Cimzett, _ExecParam);

                    if (krt_partner_cimzett != null && !string.IsNullOrEmpty(krt_partner_cimzett.Id))
                    {
                        _EREC_PldIratPeldanyok.Partner_Id_Cimzett = krt_partner_cimzett.Id;
                        _EREC_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;

                        _EREC_PldIratPeldanyok.NevSTR_Cimzett = krt_partner_cimzett.Nev;
                        _EREC_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;
                    }
                    else if (!string.IsNullOrEmpty(erec_KuldKuldemenyek.Csoport_Id_Cimzett))
                    {
                        Logger.Debug("Küldemény címzettjéhez tartozó partner megállapítása sikertelen", _ExecParam);
                        Logger.Debug("Ellenőrzés elosztóív van-e az iratpéldány címzettjében");

                        EREC_IraElosztoivekService elosztoivService = new EREC_IraElosztoivekService(this.dataContext);

                        EREC_IraElosztoivekSearch elosztoivSearch = new EREC_IraElosztoivekSearch();
                        elosztoivSearch.Id.Value = erec_KuldKuldemenyek.Csoport_Id_Cimzett;//_ExecParam.Record_Id;
                        elosztoivSearch.Id.Operator = Query.Operators.equals;

                        //Result elosztoivGetResult = elosztoivService.Get(_ExecParam);
                        Result elosztoivGetResult = elosztoivService.GetAll(_ExecParam, elosztoivSearch);

                        if (!string.IsNullOrEmpty(elosztoivGetResult.ErrorCode) || elosztoivGetResult.Ds.Tables.Count == 0 || elosztoivGetResult.Ds.Tables[0].Rows.Count != 1)
                        {
                            Logger.Debug("A címzett nem elosztóív");
                            throw new Exception("A címzett nem partner és nem is elosztóív");
                        }

                        elosztoiv = new EREC_IraElosztoivek();
                        Utility.LoadBusinessDocumentFromDataRow(elosztoiv, elosztoivGetResult.Ds.Tables[0].Rows[0]);

                        //elosztoiv = (EREC_IraElosztoivek)elosztoivGetResult.Record;
                    }
                }
                catch (Exception e)
                {
                    Logger.Debug("EXCEPTION ELKAPVA Küldemény címzettjéhez tartozó partner megállapításakor", e);
                    // Mehet tovább, ez nem kritikus hiba
                }

                //// Cím állítása:
                //_EREC_PldIratPeldanyok.Cim_id_Cimzett = erec_KuldKuldemenyek.Cim_Id;
                //_EREC_PldIratPeldanyok.Updated.Cim_id_Cimzett = true;

                // KüldésMód:
                _EREC_PldIratPeldanyok.KuldesMod = erec_KuldKuldemenyek.KuldesMod;
                _EREC_PldIratPeldanyok.Updated.KuldesMod = true;

                // Továbbító:
                _EREC_PldIratPeldanyok.Tovabbito = erec_KuldKuldemenyek.Tovabbito;
                _EREC_PldIratPeldanyok.Updated.Tovabbito = false;

                // Iratpéldány fajtája a küldemény adathordozó típusa lesz:
                _EREC_PldIratPeldanyok.UgyintezesModja = erec_KuldKuldemenyek.AdathordozoTipusa;
                _EREC_PldIratPeldanyok.Updated.UgyintezesModja = true;
            }
            else
            {
                // elosztóív van-e a címzett mezőben
                if (!String.IsNullOrEmpty(_EREC_PldIratPeldanyok.Partner_Id_Cimzett))
                {
                    EREC_IraElosztoivekService elosztoivService = new EREC_IraElosztoivekService(this.dataContext);
                    //ExecParam elosztoivGetExecParam = execParam_IratPeldanyInsert.Clone();
                    //elosztoivGetExecParam.Record_Id = _EREC_PldIratPeldanyok.Partner_Id_Cimzett;

                    //Result elosztoivGetResult = elosztoivService.Get(elosztoivGetExecParam);
                    //if (string.IsNullOrEmpty(elosztoivGetResult.ErrorCode) && elosztoivGetResult.Record != null)
                    //    elosztoiv = (EREC_IraElosztoivek)elosztoivGetResult.Record;

                    ExecParam elosztoivGetAllExecParam = execParam_IratPeldanyInsert.Clone();
                    EREC_IraElosztoivekSearch elosztoivSearch = new EREC_IraElosztoivekSearch();
                    elosztoivSearch.Id.Value = _EREC_PldIratPeldanyok.Partner_Id_Cimzett;
                    elosztoivSearch.Id.Operator = Query.Operators.equals;

                    Result elosztoivGetAllResult = elosztoivService.GetAll(elosztoivGetAllExecParam, elosztoivSearch);

                    if (elosztoivGetAllResult.IsError || elosztoivGetAllResult.Ds.Tables[0].Rows.Count == 0)
                    {
                        Logger.Info("Iktatás (belső): A címzett nem elosztóív");
                    }
                    else
                    {
                        elosztoiv = new EREC_IraElosztoivek();
                        Utility.LoadBusinessDocumentFromDataRow(elosztoiv, elosztoivGetAllResult.Ds.Tables[0].Rows[0]);
                    }
                }
            }

            if (isTUK && erec_KuldKuldemenyek != null && !string.IsNullOrEmpty(erec_KuldKuldemenyek.IrattarId) && string.IsNullOrEmpty(_EREC_PldIratPeldanyok.IrattarId))
            {
                _EREC_PldIratPeldanyok.IrattarId = erec_KuldKuldemenyek.IrattarId;
                _EREC_PldIratPeldanyok.Updated.IrattarId = true;
                _EREC_PldIratPeldanyok.IrattariHely = erec_KuldKuldemenyek.IrattariHely;
                _EREC_PldIratPeldanyok.Updated.IrattariHely = true;
            }

            // Felelős ( = Irat.Felelős)
            _EREC_PldIratPeldanyok.Csoport_Id_Felelos = _EREC_IraIratok.Csoport_Id_Felelos;
            _EREC_PldIratPeldanyok.Updated.Csoport_Id_Felelos = true;

            // Őrző ( = Irat.Őrző)
            _EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = _EREC_IraIratok.FelhasznaloCsoport_Id_Orzo;
            _EREC_PldIratPeldanyok.Updated.FelhasznaloCsoport_Id_Orzo = true;

            if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            {
                // Állapot állítása
                _EREC_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                _EREC_PldIratPeldanyok.Updated.Allapot = true;

                // Továbbítás alatt állapot állítása
                _EREC_PldIratPeldanyok.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                _EREC_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;
            }
            else
            {
                // Munkaanyagnál:
                // Állapot állítása
                _EREC_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban;
                _EREC_PldIratPeldanyok.Updated.Allapot = true;

                // Továbbítás alatt állapot állítása
                _EREC_PldIratPeldanyok.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                _EREC_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;
            }

            // mindig insert előtt növeljük
            int sorszam_iratPeldany = 1;

            // Sorszám 1 lesz, mivel ez az új irat első példánya
            _EREC_PldIratPeldanyok.Sorszam = sorszam_iratPeldany.ToString();
            _EREC_PldIratPeldanyok.Updated.Sorszam = true;
            var bid = new Result();
            //BARCODE

            //CR 3058
            if (vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
            {
                EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                var execParam_Irat = _ExecParam.Clone();
                execParam_Irat.Record_Id = UjIrat_Id;
                Result Erec_Irat = erec_IraIratokService.Get(execParam_Irat);
                var execParam_Ugyirat = _ExecParam.Clone();
                execParam_Ugyirat.Record_Id = _EREC_UgyUgyiratok.Id;
                Result Erec_Ugyirat = erec_UgyUgyiratokService.Get(execParam_Ugyirat);


                #region CR 3111 Budaörsön a vonalkód generálás Munkapéldány létrehozásakor mindig 0-s sorszámot ad.
                var iktatokonyv = new EREC_IraIktatoKonyvek();
                iktatokonyv = GetIktatokonyvById(_EREC_UgyUgyiratok.IraIktatokonyv_Id, execParam_Irat);
                string barcode =
                    iktatokonyv.Iktatohely + " /" +
                    (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag ?
                        ("MU" +
                            (String.IsNullOrEmpty(((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam) ?
                                ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Sorszam.PadLeft(6, '0') :
                                ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam.PadLeft(6, '0'))
                            ) :
                            ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam.PadLeft(6, '0'))
                    + " - " +
                    (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag ?
                        ("MI" + ((EREC_IraIratok)Erec_Irat.Record).Sorszam.PadLeft(6, '0')) :
                        ((EREC_IraIratok)Erec_Irat.Record).Alszam.PadLeft(6, '0'))
                    + " /" + iktatokonyv.Ev;
                #endregion
                barcode = barcode.Replace(" ", "");
                KRT_Barkodok barcodeRecord = new KRT_Barkodok();
                barcodeRecord.Kod = barcode;

                bid = srvBarcode.Insert(execParam_Irat, barcodeRecord);
                if (!String.IsNullOrEmpty(bid.ErrorCode))
                {
                    // hiba
                    Logger.Error("Hiba: Barkod rekordhoz ügyirat Insert", execParam_Irat);
                    throw new ResultException(bid);
                }
                //var result_barcodebind = srvBarcode.BarkodBindToIratPeldany(execParam_IratPeldanyInsert, bid.Uid, result_iratInsert.Uid);
                //if (!String.IsNullOrEmpty(result_barcodebind.ErrorCode))
                //{
                //    // hiba
                //    Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", execParam_Irat);
                //    throw new ResultException(result_barcodebind);
                //}
                barkod_pld1 = barcode;

            }
            _EREC_PldIratPeldanyok.BarCode = barkod_pld1;
            _EREC_PldIratPeldanyok.Updated.BarCode = true;

            /// Azonosító (Iktatószám) beállítása:
            /// 
            // BLG_292
            //_EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok);
            _EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok);

            _EREC_PldIratPeldanyok.Updated.Azonosito = true;


            Result result_IratPeldanyInsert = new Result();
            ///BLG_612
            if (elosztoiv != null && elosztoiv.Fajta != KodTarak.ELOSZTOIV_FAJTA.Cimzettlista)
            {
                // BUG_7128
                // Elosztóívnél nem készül bontatlan példány, viszont a sorszám már növelésre került
                sorszam_iratPeldany -= 1;
                List<string> pldIratPeldanyokIdList = null;
                IratPeldanyokElosztoIvTetelekAlapjan(
                  Iktatott_vagy_Munkaanyag,
                  _ExecParam,
                  _EREC_UgyUgyiratok,
                  _EREC_IraIratok,
                  _EREC_PldIratPeldanyok,
                  erec_IraIktatoKonyvek,
                  vonalkodkezeles,
                  elosztoiv,
                  bid,
                  ref sorszam_iratPeldany,
                  ref result_IratPeldanyInsert,
                  out pldIratPeldanyokIdList);
            }
            else
            {
                bool vanElsoPeldany = true;
                PeldanyCreator peldanyCreator = null;

                if (ip.Peldanyok != null && ip.Peldanyok.Count > 0)
                {
                    peldanyCreator = new PeldanyCreator(this.dataContext,
                        _ExecParam,
                        erec_IraIktatoKonyvek,
                        _EREC_UgyUgyiratok,
                        _EREC_IraIratok,
                        _EREC_PldIratPeldanyok,
                        ip.Peldanyok);

                    Result creatorResult = peldanyCreator.InsertPeldanyok(out sorszam_iratPeldany, out vanElsoPeldany);

                    if (creatorResult.IsError)
                    {
                        Logger.Error("peldanyCreator.InsertPeldanyok hiba", _ExecParam, creatorResult);
                        throw new ResultException(creatorResult);
                    }
                }

                if (vanElsoPeldany)
                {
                    result_IratPeldanyInsert = erec_PldIratPeldanyokService.Insert(
                        execParam_IratPeldanyInsert, _EREC_PldIratPeldanyok);

                    if (!String.IsNullOrEmpty(result_IratPeldanyInsert.ErrorCode))
                    {
                        Logger.Error("Iratpéldány Insert-nél hiba", _ExecParam, result_IratPeldanyInsert);

                        // hiba
                        throw new ResultException(result_IratPeldanyInsert);
                    }
                    else
                    {
                        string newpldId = result_IratPeldanyInsert.Uid;

                        #region BARCODE UPDATE
                        KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                        ExecParam execParam_barcodeUpdate = _ExecParam.Clone();

                        Result result_barcodeUpdate = service_barkodok.CheckBarcode(execParam_barcodeUpdate.Clone(), _EREC_PldIratPeldanyok.BarCode);
                        //CR 3058
                        if (!String.IsNullOrEmpty(_EREC_PldIratPeldanyok.BarCode) && !vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
                        {


                            if (result_barcodeUpdate.ErrorCode == "52482")
                            {
                                result_barcodeUpdate = service_barkodok.GetBarcodeByValue(execParam_barcodeUpdate.Clone(), _EREC_PldIratPeldanyok.BarCode);

                                if (!string.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
                                    throw new ResultException(result_barcodeUpdate);

                                if ((result_barcodeUpdate.Record as KRT_Barkodok).Obj_Id != ip.KuldemenyId)
                                    throw new ResultException(52482);

                                result_barcodeUpdate = service_barkodok.FreeBarcode(execParam_barcodeUpdate.Clone(), _EREC_PldIratPeldanyok.BarCode);

                                result_barcodeUpdate = service_barkodok.GetBarcodeByValue(execParam_barcodeUpdate.Clone(), _EREC_PldIratPeldanyok.BarCode);

                                string ver = (result_barcodeUpdate.Record as KRT_Barkodok).Base.Ver;

                                result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                                    execParam_barcodeUpdate, barkod_pld1_Id, newpldId, ver);
                            }
                            else
                            {
                                result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                                    execParam_barcodeUpdate, barkod_pld1_Id, newpldId, barkod_pld1_Ver);
                            }

                            if (!String.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
                            {
                                Logger.Error("Hiba: Barkod rekordhoz iratpéldány hozzákötése", _ExecParam);
                                throw new ResultException(result_barcodeUpdate);
                            }
                        }
                        else if (vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))//CR 3058
                        {
                            result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                                    execParam_barcodeUpdate, bid.Uid, newpldId);
                        }
                        #endregion
                    }
                }
            }

            #endregion


            if (!String.IsNullOrEmpty(result_IratPeldanyInsert.Uid))
            {
                String UjIratPeldany_Id = result_IratPeldanyInsert.Uid;
                _EREC_PldIratPeldanyok.Id = UjIratPeldany_Id;
            }

            // CR3306 Kimenő email esetén automatikus postazas javítás
            EREC_PldIratPeldanyok IratPldForExpedialas = _EREC_PldIratPeldanyok;
            //#region Kimenő email esetén Expediáljuk is  az Irat példányt.
            //if (_EREC_IraIratok.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso && Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas && _EREC_IraIratok.Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet)
            //{

            //    ExecParam execParam_IratPeldanyExpedialas = _ExecParam.Clone();
            //    string[] exp_Id_Array = { _EREC_PldIratPeldanyok.Id };
            //    Result result_Expedialas = erec_PldIratPeldanyokService.Expedialas(execParam_IratPeldanyExpedialas, exp_Id_Array, string.Empty, _EREC_PldIratPeldanyok);
            //    if (result_Expedialas.IsError)
            //        throw new ResultException(result_Expedialas);

            //}
            //#endregion

            //iktatasResult.UjIratPeldany_Id = UjIratPeldany_Id;

            /// Plusz Iratpéldányok készítése, ha kérték:                
            #region BLG_612
            if (elosztoiv != null && elosztoiv.Fajta == KodTarak.ELOSZTOIV_FAJTA.Cimzettlista)
            {
                UpdateElosztoIvNoteField(_ExecParam.Clone(), elosztoiv.Id, UjIrat_Id);
            }
            #endregion

            #region Plusz iratpéldány az ügyirat felelősének:

            if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas &&
                ip.UgyiratPeldanySzukseges == true && !String.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Felelos))
            {
                KRT_Partnerek UgyiratFelelos_Partner = null;

                try
                {
                    UgyiratFelelos_Partner =
                        GetKRT_PartnerekByCsoportId(_EREC_UgyUgyiratok.Csoport_Id_Felelos, _ExecParam);
                }
                catch (Exception e)
                {
                    Logger.Error("Hiba új iratpéldány létrehozásakor (ügyirat felelősének szóló példánynál)", e);
                    // nem kritikus a hiba, nem kell emiatt rollback-elni
                }

                if (UgyiratFelelos_Partner != null && !String.IsNullOrEmpty(UgyiratFelelos_Partner.Id))
                {
                    // BUG_7128
                    //_EREC_PldIratPeldanyok.Id = null;
                    _EREC_PldIratPeldanyok.Updated.Id = false;
                    sorszam_iratPeldany++;

                    _EREC_PldIratPeldanyok.Sorszam = sorszam_iratPeldany.ToString();
                    _EREC_PldIratPeldanyok.Updated.Sorszam = true;

                    // Felelős beállítása
                    _EREC_PldIratPeldanyok.Csoport_Id_Felelos = _EREC_UgyUgyiratok.Csoport_Id_Felelos;
                    _EREC_PldIratPeldanyok.Updated.Csoport_Id_Felelos = true;

                    if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                    {
                        // Állapot állítása
                        _EREC_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                        _EREC_PldIratPeldanyok.Updated.Allapot = true;

                        // Továbbítás alatt állapot állítása
                        _EREC_PldIratPeldanyok.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                        _EREC_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;
                    }
                    else
                    {
                        // Munkaanyagnál:
                        // Állapot állítása
                        _EREC_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban;
                        _EREC_PldIratPeldanyok.Updated.Allapot = true;

                        // Továbbítás alatt állapot állítása
                        _EREC_PldIratPeldanyok.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                        _EREC_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;

                    }

                    _EREC_PldIratPeldanyok.Eredet = KodTarak.IRAT_FAJTA.Masolat;
                    _EREC_PldIratPeldanyok.Updated.Eredet = true;
                    #region BUG 4780 - Ügyiratban maradó példány címzettje rendszerparaméter alapján
                    ExecParam execParam = _ExecParam.Clone();
                    string peldanyCimzettje = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.UGYIRATBAN_MARADO_PELDANY_CIMZETTJE);
                    //ha nincs megadva a rendszerparaméter, akkor korábbi módon működik
                    if (string.IsNullOrEmpty(peldanyCimzettje))
                    {
                        _EREC_PldIratPeldanyok.Partner_Id_Cimzett = UgyiratFelelos_Partner.Id;
                        _EREC_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;

                        _EREC_PldIratPeldanyok.NevSTR_Cimzett = UgyiratFelelos_Partner.Nev;
                        _EREC_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;
                    }
                    else
                    {
                        _EREC_PldIratPeldanyok.Partner_Id_Cimzett = String.Empty;
                        _EREC_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;

                        _EREC_PldIratPeldanyok.NevSTR_Cimzett = peldanyCimzettje;
                        _EREC_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;
                    }

                    // BUG_13444
                    _EREC_PldIratPeldanyok.KuldesMod = Rendszerparameterek.Get(execParam, Rendszerparameterek.KULDEMENY_KULDES_MODJA_HELYBEN);
                    _EREC_PldIratPeldanyok.Updated.KuldesMod = true;
                    #endregion

                    // TODO: Mi legyen a címmel?
                    _EREC_PldIratPeldanyok.Cim_id_Cimzett = String.Empty;
                    _EREC_PldIratPeldanyok.Updated.Cim_id_Cimzett = true;

                    _EREC_PldIratPeldanyok.CimSTR_Cimzett = String.Empty;
                    _EREC_PldIratPeldanyok.Updated.CimSTR_Cimzett = true;

                    // BARCODE
                    _EREC_PldIratPeldanyok.BarCode = barkod_pld2;
                    _EREC_PldIratPeldanyok.Updated.BarCode = true;

                    /// Azonosító (Iktatószám) beállítása
                    /// 
                    // BLG_292
                    //_EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok);
                    _EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok);

                    _EREC_PldIratPeldanyok.Updated.Azonosito = true;

                    ExecParam execParam_PluszPeldany1 = _ExecParam.Clone();

                    Result result_PluszPeldany1 =
                        erec_PldIratPeldanyokService.Insert(execParam_PluszPeldany1, _EREC_PldIratPeldanyok);
                    if (!String.IsNullOrEmpty(result_PluszPeldany1.ErrorCode))
                    {
                        Logger.Error("Ügyirat felelősének szóló iratpéldány létrehozásakor hiba", _ExecParam, result_PluszPeldany1);

                        // hiba
                        sorszam_iratPeldany--;

                        throw new ResultException(result_PluszPeldany1);
                    }
                    else
                    {
                        #region BARCODE UPDATE

                        string newpldId = result_PluszPeldany1.Uid;

                        if (!String.IsNullOrEmpty(_EREC_PldIratPeldanyok.BarCode))
                        {
                            KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                            ExecParam execParam_barcodeUpdate = _ExecParam.Clone();

                            Result result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                                    execParam_barcodeUpdate, barkod_pld2_Id, newpldId);

                            if (!String.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
                            {
                                Logger.Error("Hiba: Barkod rekordhoz iratpéldány hozzákötése", _ExecParam);
                                throw new ResultException(result_barcodeUpdate);
                            }
                        }
                        #endregion
                    }

                }
                else
                {
                    // ha nem találtunk a csoporthoz partnert, akkor nem csinálunk új példányt
                    // TODO: Jó ez így, vagy dobjon esetleg hibát??
                }
            }
            #endregion

            #region Plusz iratpéldány az iktatónak:

            if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas &&
                ip.KeszitoPeldanyaSzukseges == true)
            {
                KRT_Partnerek Iktato_Partner = null;
                try
                {
                    Iktato_Partner = GetKRT_PartnerekByCsoportId(FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam), _ExecParam);
                }
                catch (Exception e)
                {
                    Logger.Error("Hiba új iratpéldány létrehozásakor (iktatónak szóló példánynál)", e);
                    // nem kritikus a hiba, nem kell emiatt rollback-elni
                }

                if (Iktato_Partner != null && !String.IsNullOrEmpty(Iktato_Partner.Id))
                {
                    sorszam_iratPeldany++;

                    _EREC_PldIratPeldanyok.Sorszam = sorszam_iratPeldany.ToString();
                    _EREC_PldIratPeldanyok.Updated.Sorszam = true;

                    // kezeloje az iktato
                    _EREC_PldIratPeldanyok.Csoport_Id_Felelos = FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
                    _EREC_PldIratPeldanyok.Updated.Csoport_Id_Felelos = true;

                    if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                    {
                        // Állapot állítása
                        _EREC_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                        _EREC_PldIratPeldanyok.Updated.Allapot = true;

                        _EREC_PldIratPeldanyok.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString; // "";
                        _EREC_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;
                    }
                    else
                    {
                        // Munkaanyagnál:
                        _EREC_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban;
                        _EREC_PldIratPeldanyok.Updated.Allapot = true;

                        _EREC_PldIratPeldanyok.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString; // "";
                        _EREC_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;
                    }

                    _EREC_PldIratPeldanyok.Eredet = KodTarak.IRAT_FAJTA.Masolat;
                    _EREC_PldIratPeldanyok.Updated.Eredet = true;

                    _EREC_PldIratPeldanyok.Partner_Id_Cimzett = Iktato_Partner.Id;
                    _EREC_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true; ;

                    _EREC_PldIratPeldanyok.NevSTR_Cimzett = Iktato_Partner.Nev;
                    _EREC_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;

                    // TODO: Mi legyen a címmel?
                    _EREC_PldIratPeldanyok.Cim_id_Cimzett = String.Empty;
                    _EREC_PldIratPeldanyok.Updated.Cim_id_Cimzett = true;

                    _EREC_PldIratPeldanyok.CimSTR_Cimzett = String.Empty;
                    _EREC_PldIratPeldanyok.Updated.CimSTR_Cimzett = true;

                    //BARCODE
                    _EREC_PldIratPeldanyok.BarCode = barkod_pld3;
                    _EREC_PldIratPeldanyok.Updated.BarCode = true;

                    /// Azonosító (Iktatószám) beállítása
                    /// 
                    // BLG_292
                    //_EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok);
                    _EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok);

                    _EREC_PldIratPeldanyok.Updated.Azonosito = true;

                    ExecParam execParam_PluszPeldany2 = _ExecParam.Clone();

                    Result result_PluszPeldany2 =
                        erec_PldIratPeldanyokService.Insert(execParam_PluszPeldany2, _EREC_PldIratPeldanyok);
                    if (!String.IsNullOrEmpty(result_PluszPeldany2.ErrorCode))
                    {
                        Logger.Error("Iktatónak szóló iratpéldány létrehozásakor hiba", _ExecParam, result_PluszPeldany2);

                        // hiba
                        sorszam_iratPeldany--;

                        throw new ResultException(result_PluszPeldany2);
                    }
                    else
                    {
                        #region BARCODE UPDATE

                        string newpldId = result_PluszPeldany2.Uid;

                        if (!String.IsNullOrEmpty(_EREC_PldIratPeldanyok.BarCode))
                        {
                            KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                            ExecParam execParam_barcodeUpdate = _ExecParam.Clone();

                            Result result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                                    execParam_barcodeUpdate, barkod_pld3_Id, newpldId);

                            if (!String.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
                            {
                                Logger.Error("Hiba: Barkod rekordhoz iratpéldány hozzákötése", _ExecParam);
                                throw new ResultException(result_barcodeUpdate);
                            }
                        }
                        #endregion
                    }

                }
                else
                {
                    // ha nem találtunk a csoporthoz partnert, akkor nem csinálunk új példányt
                    // TODO: Jó ez így, vagy dobjon esetleg hibát??
                }
            }
            #endregion

            // Irat UPDATE (UtolsoSorszam módosítása, ha volt plusz iratpéldány)
            if (sorszam_iratPeldany > 1)
            {
                // Verziót le kell kérni, mert előfordulhat, hogy már nem 1-es...
                var execParamIratGet = _ExecParam.Clone();
                execParamIratGet.Record_Id = _EREC_IraIratok.Id;
                var resultIratGet = this.Get(execParamIratGet);
                resultIratGet.CheckError();

                EREC_IraIratok iratForVer = (EREC_IraIratok)resultIratGet.Record;

                _EREC_IraIratok.Updated.SetValueAll(false);
                _EREC_IraIratok.Base.Updated.SetValueAll(false);

                _EREC_IraIratok.Base.Ver = iratForVer.Base.Ver;
                _EREC_IraIratok.Base.Updated.Ver = true;

                // Utolsó sorszám állítása:
                _EREC_IraIratok.UtolsoSorszam = sorszam_iratPeldany.ToString();
                _EREC_IraIratok.Updated.UtolsoSorszam = true;

                ExecParam execParam_iratUpdate = _ExecParam.Clone();
                execParam_iratUpdate.Record_Id = _EREC_IraIratok.Id;

                Result result_IratUpdate = erec_IraIratokService.Update(execParam_iratUpdate, _EREC_IraIratok);
                if (!String.IsNullOrEmpty(result_IratUpdate.ErrorCode))
                {
                    Logger.Error("Irat utolsó sorszámának módosításakor hiba", _ExecParam, result_IratUpdate);

                    // hiba
                    throw new ResultException(result_IratUpdate);
                }
            }

            // CR3306 Kimenő email esetén automatikus postazas javítás
            #region Kimenő email esetén Expediáljuk is  az Irat példányt.
            // TODO MUNKAPELDANY ellenörzés
            if (_EREC_IraIratok.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso
                && Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas
                && Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag
                && _EREC_IraIratok.Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet)
            {
                // BLG_237 
                //#region Email boríték update
                //if (string.IsNullOrEmpty(eMailBoritekok_Id))
                //{
                //    throw new ResultException("Nincs megadva az emailboritek Id!");
                //}

                //EREC_eMailBoritekokService service_emailboritekok = new EREC_eMailBoritekokService(this.dataContext);
                //ExecParam execParam_emailboritekGet = _ExecParam.Clone();
                //execParam_emailboritekGet.Record_Id = eMailBoritekok_Id;

                //Result result_emailborGet = service_emailboritekok.Get(execParam_emailboritekGet);
                //if (result_emailborGet.IsError)
                //{
                //    // hiba:
                //    throw new ResultException(result_emailborGet);
                //}

                //EREC_eMailBoritekok erec_eMailBoritek = (EREC_eMailBoritekok)result_emailborGet.Record;

                //// Iktatták-e már?
                //if (!string.IsNullOrEmpty(erec_eMailBoritek.IraIrat_Id))
                //{
                //    // Hiba: Az email már iktatva van!
                //    throw new ResultException(52122);
                //}

                //erec_eMailBoritek.Updated.SetValueAll(false);
                //erec_eMailBoritek.Base.Updated.SetValueAll(false);
                //erec_eMailBoritek.Base.Updated.Ver = true;

                //// IraIratId-t kell beállítani:
                //erec_eMailBoritek.IraIrat_Id = _EREC_IraIratok.Id;
                //erec_eMailBoritek.Updated.IraIrat_Id = true;

                ////

                //// Állapot?

                //ExecParam execParam_emailborUpdate = _ExecParam.Clone();
                //execParam_emailborUpdate.Record_Id = erec_eMailBoritek.Id;

                //Result result_emailborUpdate = service_emailboritekok.Update(execParam_emailborUpdate, erec_eMailBoritek);
                //if (result_emailborUpdate.IsError)
                //{
                //    // hiba:
                //    throw new ResultException(result_emailborUpdate);
                //}

                //#endregion  Email boríték update

                #region LZS - BLG_298 - kimenő e-mail kiadmányozása
                if (_EREC_IraIratok.KiadmanyozniKell.Equals("1"))
                {
                    EREC_IratAlairok iratAlairok = new EREC_IratAlairok();

                    iratAlairok.FelhasznaloCsoport_Id_Alairo = FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
                    iratAlairok.Updated.FelhasznaloCsoport_Id_Alairo = true;

                    iratAlairok.AlairasDatuma = DateTime.Now.ToString();
                    iratAlairok.Updated.AlairasDatuma = true;

                    iratAlairok.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                    iratAlairok.Updated.Allapot = true;

                    iratAlairok.AlairoSzerep = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
                    iratAlairok.Updated.AlairoSzerep = true;

                    iratAlairok.AlairasDatuma = DateTime.Now.ToString();
                    iratAlairok.Updated.AlairasDatuma = true;

                    iratAlairok.AlairasMod = "M_UTO";
                    iratAlairok.Updated.AlairasMod = true;

                    #region Aláíró szabályok lekérdezése adatbázisból:
                    ExecParam execParam_alairoSzabalyok = _ExecParam.Clone();

                    AlairasokService alairoSzabalyok = new AlairasokService(this.dataContext);
                    Result alairoSzabalyokResult = alairoSzabalyok.GetAll_PKI_IratAlairoSzabalyok(execParam_alairoSzabalyok, FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam), null, "Irat", UjIrat_Id, "IratJovahagyas", "BelsoIratJovahagyasUtoAdmin", KodTarak.ALAIRO_SZEREP.Kiadmanyozo);

                    foreach (DataRow row in alairoSzabalyokResult.Ds.Tables[0].Rows)
                    {
                        if (row["AlairasMod"].ToString() == "M_UTO")
                        {
                            iratAlairok.AlairasSzabaly_Id = row["AlairasSzabaly_Id"].ToString();
                            iratAlairok.Updated.AlairasSzabaly_Id = true;
                        }
                    }
                    #endregion

                    iratAlairok.Obj_Id = UjIrat_Id;
                    iratAlairok.Updated.Obj_Id = true;

                    iratAlairok.Leiras = "Kimenő e-mail automatikus kiadmányozása";
                    iratAlairok.Updated.Leiras = true;

                    ExecParam execParam_iratAlairas = _ExecParam.Clone();

                    EREC_IratAlairokService service_iratAlairok = new EREC_IratAlairokService(this.dataContext);

                    Result result_iratAlairas = service_iratAlairok.Insert(execParam_iratAlairas, iratAlairok);
                    if (result_iratAlairas.IsError)
                    {
                        // hiba:
                        Logger.Error(String.Format("result_iratAlairas: irat alairok service hibaval tert vissza: {0} - {1}", result_iratAlairas.ErrorCode, result_iratAlairas.ErrorMessage));
                        //throw new ResultException(result_iratAlairas);
                    }
                }
                #endregion

                ExecParam execParam_IratPeldanyExpedialas = _ExecParam.Clone();
                //string[] exp_Id_Array = { _EREC_PldIratPeldanyok.Id };
                string[] exp_Id_Array = { IratPldForExpedialas.Id };
                // Result result_Expedialas = erec_PldIratPeldanyokService.Expedialas(execParam_IratPeldanyExpedialas, exp_Id_Array, string.Empty, _EREC_PldIratPeldanyok);
                Result result_Expedialas = erec_PldIratPeldanyokService.Expedialas(execParam_IratPeldanyExpedialas, exp_Id_Array, string.Empty, IratPldForExpedialas, null);
                if (result_Expedialas.IsError)
                    throw new ResultException(result_Expedialas);

            }
            #endregion
        }

        #region irat szignálás
        /// BUG#7682: 
        /// Ha NMHH szakrendszeri hívás jön (SZUReGetalszamWS, SZURGetalszamWS) (ez egy belső irat iktatás alszámra),
        /// akkor az irat állapotot szignáltra kell állítani
        /// 
        if (((ip != null && ip.IsSZUR) || Rendszerparameterek.GetBoolean(_ExecParam, Rendszerparameterek.ALSZAM_SZIGNALT_ENABLED, false))
            && Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas
            && !String.IsNullOrEmpty(_EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez))
        {
            if (_EREC_IraIratok.Allapot == KodTarak.IRAT_ALLAPOT.Iktatott)
            {
                Result resultIratSzignalas = erec_IraIratokService.AutomatikusSzignalas(_ExecParam.Clone(), _EREC_IraIratok.Id, _EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez);

                if (resultIratSzignalas.IsError)
                {
                    throw new ResultException(resultIratSzignalas);
                }
            }
        }
        #endregion
        #region Regi Adat Szereles a vegere kell, mert kulon tranzakcioban fut
        if (kelleRegiAdtaSzereles)
        {
            #region EREC_UgyiratObjKapcsolatok insert
            Logger.Debug("EREC_UgyiratObjKapcsolatokService InsertMigraltSzerelt start");

            ExecParam xmpObjKapcsInsert = _ExecParam.Clone();
            EREC_UgyiratObjKapcsolatokService svcObjKapcs = new EREC_UgyiratObjKapcsolatokService(this.dataContext);

            svcObjKapcs.InsertMigraltSzerelt(xmpObjKapcsInsert, _EREC_UgyUgyiratok.Id, ip.RegiAdatId, ip.RegiAdatAzonosito);

            Logger.Debug("EREC_UgyiratObjKapcsolatokService InsertMigraltSzerelt end");
            #endregion

            // BLG_292
            //string ugyiratAzonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_EREC_UgyUgyiratok, erec_IraIktatoKonyvek);
            string ugyiratAzonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_ExecParam, _EREC_UgyUgyiratok, erec_IraIktatoKonyvek);

            EREC_UgyUgyiratokService.FoszamSzereles(_ExecParam, _EREC_UgyUgyiratok.RegirendszerIktatoszam, _EREC_UgyUgyiratok.Id, ugyiratAzonosito);

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, _EREC_UgyUgyiratok.Id, "EREC_UgyUgyiratok", "UgyiratSzereles").Record;

            eventLogRecord.Azonositoja = ip.RegiAdatAzonosito;

            eventLogService.Insert(_ExecParam, eventLogRecord);
            #endregion
        }
        #endregion


        /// ha minden OK volt, akkor egy hibakód nélküli Result megy vissza, amibe beletesszük 
        /// a létrehozott rekordok id-jait //egyelőre csak az iratét és az ügyiratét
        Result returnResult = new Result();
        returnResult.Uid = _EREC_IraIratok.Id;
        #region Visszatérési információk
        ErkeztetesIktatasResult iktatasResult = new ErkeztetesIktatasResult();
        iktatasResult.UgyiratId = _EREC_UgyUgyiratok.Id;
        // BLG_292
        //string FullFoszam = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_EREC_UgyUgyiratok, erec_IraIktatoKonyvek);
        string FullFoszam = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_ExecParam, _EREC_UgyUgyiratok, erec_IraIktatoKonyvek);

        iktatasResult.UgyiratAzonosito = FullFoszam;
        iktatasResult.UgyiratCsoportIdFelelos = _EREC_UgyUgyiratok.Csoport_Id_Felelos;
        iktatasResult.IratId = _EREC_IraIratok.Id;
        // BLG_292
        //string FullIktatoszam = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok);
        string FullIktatoszam = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok);

        iktatasResult.IratAzonosito = FullIktatoszam;
        //munkapéldány létrehozás volt-e?
        iktatasResult.MunkaPeldany = (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag);

        // Alszámra iktatás esetén, ha módosult az ügyirat határidő
        if (!String.IsNullOrEmpty(ip.UgyiratUjHatarido))
        {
            DateTime elozmenyDate;
            DateTime ugyiratDate;
            bool isElozmenyDateParsed = DateTime.TryParse(ip.UgyiratUjHatarido, out elozmenyDate);
            bool isUgyiratDateParsed = DateTime.TryParse(_EREC_UgyUgyiratok.Hatarido, out ugyiratDate);
            if (isElozmenyDateParsed && isUgyiratDateParsed && elozmenyDate == ugyiratDate)
            {
                iktatasResult.UgyiratUjHatarido = ip.UgyiratUjHatarido;
            }
        }
        returnResult.Record = iktatasResult;
        #endregion

        //Visszajelzés adatkapunak
        if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
        {
            EREC_eBeadvanyokService eBeadvanyokService = new EREC_eBeadvanyokService(this.dataContext);
            Result eBeadvanyokResult = eBeadvanyokService.SetRegistrationIdentifier(_ExecParam, iktatasResult.IratId);

            if (eBeadvanyokResult.IsError)
            {
                Logger.Error("SetRegistrationIdentifier hiba", _ExecParam, eBeadvanyokResult);
            }
        }


        Logger.Debug("Iktatás lefutása sikeres", _ExecParam);


        return returnResult;


        //}
        //catch (Exception e)
        //{
        //    Logger.Error("Általános hiba az iktatás során: " + e.StackTrace, _ExecParam);
        //    Logger.Error("", e);

        //    //ContextUtil.SetAbort();
        //    //return ResultError.CreateNewResultWithErrorCode(52100);

        //    throw e;
        //}
    }

    /// <summary>
    /// Küldemény iktathatóságának ellenőrzése, állapot átállítása "Iktatva"-ra
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <returns></returns>    
    private Result CheckAndSet_Kuldemeny(ExecParam _ExecParam, String _EREC_KuldKuldemenyek_Id, string Minosites, out EREC_KuldKuldemenyek _EREC_KuldKuldemenyek)
    {
        EREC_KuldKuldemenyek erec_KuldKuldemenyek = null;
        _EREC_KuldKuldemenyek = erec_KuldKuldemenyek;

        // Küldemény ellenőrzése        
        if (String.IsNullOrEmpty(_EREC_KuldKuldemenyek_Id))
        {
            // hiba
            //ContextUtil.SetAbort();
            //return ResultError.CreateNewResultWithErrorCode(52101);

            throw new ResultException(52101);
        }
        else
        {
            // küldemény lekérése:            
            EREC_KuldKuldemenyekService erec_KuldKuldemenyekService = new EREC_KuldKuldemenyekService(this.dataContext);

            ExecParam execParam_kuldemenyGet = _ExecParam.Clone();
            execParam_kuldemenyGet.Record_Id = _EREC_KuldKuldemenyek_Id;
            Result result_Kuldemeny = erec_KuldKuldemenyekService.Get(execParam_kuldemenyGet);
            if (!String.IsNullOrEmpty(result_Kuldemeny.ErrorCode))
            {
                Logger.Error("Hiba a küldemény lekérésekor", _ExecParam, result_Kuldemeny);

                // hiba
                //ContextUtil.SetAbort();
                //return result_Kuldemeny;

                throw new ResultException(result_Kuldemeny);
            }
            else
            {

                erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result_Kuldemeny.Record;
                if (erec_KuldKuldemenyek == null)
                {
                    // hiba
                    //ContextUtil.SetAbort();
                    //return ResultError.CreateNewResultWithErrorCode(52102);

                    throw new ResultException(52102);
                }
                else
                {
                    // Küldemény iktatható-e?
                    if (Check_KuldemenyIktathato(_ExecParam, erec_KuldKuldemenyek) == false)
                    {
                        Logger.Warn("A küldemény nem iktatható", _ExecParam);

                        // nem iktatható
                        //ContextUtil.SetAbort();
                        //return ResultError.CreateNewResultWithErrorCode(52103);

                        throw new ResultException(52103);
                    }
                    else
                    {
                        // update-elhetőség állítása:
                        erec_KuldKuldemenyek.Updated.SetValueAll(false);
                        erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);
                        erec_KuldKuldemenyek.Base.Updated.Ver = true;

                        // küldemény állapot beállítása: iktatott
                        erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Iktatva;
                        erec_KuldKuldemenyek.Updated.Allapot = true;

                        // küldemény minősítés beállítása: iratból visszaírva
                        erec_KuldKuldemenyek.Minosites = Minosites ?? "";
                        erec_KuldKuldemenyek.Updated.Minosites = true;

                        // Kuldemeny UPDATE:
                        ExecParam execParam_kuldemenyUpdate = _ExecParam.Clone();
                        execParam_kuldemenyUpdate.Record_Id = erec_KuldKuldemenyek.Id;

                        Result result_kuldemenyUpdate = erec_KuldKuldemenyekService.Update(execParam_kuldemenyUpdate, erec_KuldKuldemenyek);

                        if (!String.IsNullOrEmpty(result_kuldemenyUpdate.ErrorCode))
                        {
                            Logger.Error("Küldemény állapotának módosításakor hiba", _ExecParam, result_kuldemenyUpdate);

                            // hiba
                            //ContextUtil.SetAbort();
                            //return result_kuldemenyUpdate;

                            throw new ResultException(result_kuldemenyUpdate);
                        }
                        else
                        {
                            _EREC_KuldKuldemenyek = erec_KuldKuldemenyek;

                            Logger.Debug("Küldemény állapotának módosítása megtörtént", _ExecParam);



                            // Kézbesítési tétel INVALIDATE
                            // Ha volt kézbesítési tétel a küldeményre (még át nem vett), akkor annak törlése:
                            Logger.Debug("Küldemény kézbesítési tételeinek törlése, ha volt", _ExecParam);

                            EREC_IraKezbesitesiTetelekService service_kezbTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                            ExecParam execParam_kezbTetelInv = _ExecParam.Clone();

                            Result result_kezbTetelInv =
                                service_kezbTetelek.NemAtadottKezbesitesiTetelInvalidate(execParam_kezbTetelInv, erec_KuldKuldemenyek.Id);

                            if (!String.IsNullOrEmpty(result_kezbTetelInv.ErrorCode))
                            {
                                Logger.Error("Kézbesítési tétel érvénytelenítésekor hiba", _ExecParam, result_kezbTetelInv);

                                // hiba:
                                //ContextUtil.SetAbort();
                                //return result_kezbTetelInv;

                                throw new ResultException(result_kezbTetelInv);
                            }



                            // ha eljut eddig, nincs hiba                            
                            return new Result();
                        }
                    }
                }
            }
        }
    }


    /// <summary>
    /// Ellenőrzés, hogy a felhasználó iktathat-e a megadott iktatókönyvbe
    /// </summary>
    /// <param name="Felhasznalo_Id"></param>
    /// <param name="IktatoKonyv_Id"></param>
    /// <returns></returns>
    private Result Check_IktatoKonyvbeIktathat(ExecParam execParam, String IktatoKonyv_Id)
    {
        if (String.IsNullOrEmpty(IktatoKonyv_Id))
        {
            //return ResultError.CreateNewResultWithErrorCode(52105);

            throw new ResultException(52105);
        }
        // TODO: megcsinálni az ellenőrzést valahogy
        return new Result();
    }

    /// <summary>
    /// Iktatható: (Kuldemeny.Allapot == Érkeztetve ÉS Kuldemeny.Iktatnikell != Nem_Iktatando)
    /// </summary>
    /// <param name="erec_Kuldkuldemenyek"></param>
    /// <returns></returns>
    private bool Check_KuldemenyIktathato(ExecParam ExecParam, EREC_KuldKuldemenyek erec_Kuldkuldemenyek)
    {
        // BLG_1816
        if (System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"] != null && System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"].ToUpper() == "MIGRATION")
        {
            return true;
        }
        if (erec_Kuldkuldemenyek == null) { return false; }
        else
        {

            Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz kuldemenyStatusz
                = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(erec_Kuldkuldemenyek);

            ErrorDetails errorDetail = null;
            return Contentum.eRecord.BaseUtility.Kuldemenyek.Iktathato(ExecParam, kuldemenyStatusz, out errorDetail);


            //if (erec_Kuldkuldemenyek.Allapot == KodTarak.KULDEMENY_ALLAPOT.Erkeztetve
            //    && erec_Kuldkuldemenyek.IktatniKell == KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}

        }
    }

    /// <summary>
    /// Partner kiderítése a Csoport_Id alapján
    /// (Hiba esetén nem dobunk exceptiont, csak null-t adunk vissza)
    /// </summary>    
    private KRT_Partnerek GetKRT_PartnerekByCsoportId(string csoport_Id, ExecParam execParam)
    {
        Contentum.eAdmin.Service.KRT_PartnerekService krt_partnerekService =
            eAdminService.ServiceFactory.GetKRT_PartnerekService();

        Result result = krt_partnerekService.GetByCsoportId(execParam, csoport_Id);
        if (String.IsNullOrEmpty(result.ErrorCode)
            && (result.Record != null))
        {
            return (KRT_Partnerek)result.Record;
        }
        else
        {
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error("Hiba: EREC_IraIratokService.GetKRT_PartnerekByCsoportId", execParam, result);
            }

            return null;
        }
    }

    // Iktatókönyv lekérése
    private EREC_IraIktatoKonyvek GetIktatokonyvById(String iktatoKonyv_Id, ExecParam _ExecParam)
    {
        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = null;

        ExecParam execParam_iktatokonyvGet = _ExecParam.Clone();
        execParam_iktatokonyvGet.Record_Id = iktatoKonyv_Id;

        EREC_IraIktatoKonyvekService erec_IraIktatoKonyvekService = new EREC_IraIktatoKonyvekService(this.dataContext);

        Result result_iktatokonyvGet = erec_IraIktatoKonyvekService.Get(execParam_iktatokonyvGet);
        if (!String.IsNullOrEmpty(result_iktatokonyvGet.ErrorCode))
        {
            Logger.Error("Iktatókönyv lekérése sikertelen", _ExecParam, result_iktatokonyvGet);

            // hiba
            throw new ResultException(52104);
        }
        else
        {
            erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)result_iktatokonyvGet.Record;
            if (erec_IraIktatoKonyvek == null)
            {
                // hiba
                throw new ResultException(52104);
            }
            else if (erec_IraIktatoKonyvek.IktatoErkezteto != Contentum.eUtility.Constants.IktatoErkezteto.Iktato)
            {
                //hiba
                throw new ResultException(52104);
            }
        }

        return erec_IraIktatoKonyvek;
    }


    /// <summary>
    /// Küldemény mellékleteinek és csatolmányainak átmásolása az irathoz
    /// (UPDATE-elni kell az iratId-val)
    /// </summary>    
    private void Copy_KuldMellekletek_Csatolmanyok(ExecParam execParam, string kuldemeny_Id, string irat_Id, string iktatohely, string foszam, string ev, string alszam)
    {
        Logger.Debug("Küldemény mellékleteinek, csatolmányainak másolása az irathoz", execParam);

        if (String.IsNullOrEmpty(kuldemeny_Id)
            || String.IsNullOrEmpty(irat_Id))
        {
            return;
        }

        #region Mellékletek Update-elése az IratId-val:

        // küldemény mellékleteinek lekérése:
        EREC_MellekletekService service_kuldMellekletek = new EREC_MellekletekService(this.dataContext);
        //EREC_IratMellekletekService service_iratMellekletek = new EREC_IratMellekletekService(this.dataContext);

        EREC_MellekletekSearch search_mellekletek = new EREC_MellekletekSearch();

        search_mellekletek.KuldKuldemeny_Id.Value = kuldemeny_Id;
        search_mellekletek.KuldKuldemeny_Id.Operator = Query.Operators.equals;

        Result result_kuldmellekletekGetAll = service_kuldMellekletek.GetAll(execParam, search_mellekletek);
        if (!String.IsNullOrEmpty(result_kuldmellekletekGetAll.ErrorCode))
        {
            // hiba            
            throw new ResultException(result_kuldmellekletekGetAll);
        }

        if (result_kuldmellekletekGetAll.Ds != null
            && result_kuldmellekletekGetAll.Ds.Tables[0] != null)
        {
            foreach (DataRow row in result_kuldmellekletekGetAll.Ds.Tables[0].Rows)
            {
                // ellenőrzés:
                if (row["KuldKuldemeny_Id"].ToString() != kuldemeny_Id)
                {
                    Logger.Error("Hiba: service_kuldMellekletek.GetAll", execParam);

                    throw new ResultException(52116);
                }

                string mellekletId = row["Id"].ToString();

                string row_AdathordozoTipus = row["AdathordozoTipus"].ToString();
                string row_BarCode = row["BarCode"].ToString();

                // Melléklet UPDATE

                EREC_Mellekletek melleklet_update = new EREC_Mellekletek();
                melleklet_update.Updated.SetValueAll(false);
                melleklet_update.Base.Updated.SetValueAll(false);

                melleklet_update.Base.Ver = row["Ver"].ToString();
                melleklet_update.Base.Updated.Ver = true;

                // IratId megadása:
                melleklet_update.IraIrat_Id = irat_Id;
                melleklet_update.Updated.IraIrat_Id = true;

                ExecParam execParam_mellekletUpdate = execParam.Clone();
                execParam_mellekletUpdate.Record_Id = mellekletId;

                Result result_mellekletUpdate = service_kuldMellekletek.Update(execParam_mellekletUpdate, melleklet_update);
                if (!String.IsNullOrEmpty(result_mellekletUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_mellekletUpdate);
                }

                #region Vonalkódhoz kötés átállítása iratmellékletekre:
                // (csak akkor, ha a melléklet NEM papír irat, mert akkor azt az irathoz adminisztráljuk be)
                if (row_AdathordozoTipus != KodTarak.AdathordozoTipus.PapirAlapu)
                {
                    if (!String.IsNullOrEmpty(row_BarCode))
                    {
                        KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);

                        // Barkod rekord 
                        Result result_barkodGet = service_barkodok.GetBarcodeByValue(execParam.Clone(), row_BarCode);
                        if (!String.IsNullOrEmpty(result_barkodGet.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_barkodGet);
                        }

                        KRT_Barkodok barkodObj = (KRT_Barkodok)result_barkodGet.Record;

                        // IratMelléklethez kötés:
                        Result result_barkodBind = service_barkodok.BarkodBindToObject(execParam.Clone(), barkodObj.Id
                            , mellekletId, Contentum.eUtility.Constants.TableNames.EREC_IratMellekletek, barkodObj.Base.Ver);
                        if (!String.IsNullOrEmpty(result_barkodBind.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_barkodBind);
                        }
                    }
                }
                #endregion
            }
        }
        #endregion

        #region Csatolmányok Update-elése az IratId-val:

        EREC_CsatolmanyokService service_csatolmanyok = new EREC_CsatolmanyokService(this.dataContext);

        EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

        search_csatolmanyok.KuldKuldemeny_Id.Value = kuldemeny_Id;
        search_csatolmanyok.KuldKuldemeny_Id.Operator = Query.Operators.equals;

        Result result_csatGetAll = service_csatolmanyok.GetAll(execParam, search_csatolmanyok);
        if (!String.IsNullOrEmpty(result_csatGetAll.ErrorCode))
        {
            // hiba
            throw new ResultException(result_csatGetAll);
        }


        if (result_csatGetAll.Ds != null
            && result_csatGetAll.Ds.Tables[0] != null)
        {
            string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);

            foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
            {
                // ellenőrzés:
                if (row["KuldKuldemeny_Id"].ToString() != kuldemeny_Id)
                {
                    Logger.Error("Hiba: service_csatolmanyok.GetAll", execParam);

                    throw new ResultException(52117);
                }

                // Csatolmány UPDATE (csak az IratId-t kell módosítani)
                EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();

                erec_Csatolmanyok.Updated.SetValueAll(false);
                erec_Csatolmanyok.Base.Updated.SetValueAll(false);
                erec_Csatolmanyok.Base.Ver = row["Ver"].ToString();
                erec_Csatolmanyok.Base.Updated.Ver = true;

                erec_Csatolmanyok.IraIrat_Id = irat_Id;
                erec_Csatolmanyok.Updated.IraIrat_Id = true;

                Logger.Debug("Irat csatolmány update", execParam);

                ExecParam execParam_csatUpdate = execParam.Clone();
                execParam_csatUpdate.Record_Id = row["Id"].ToString();

                Result result_csatUpdate = service_csatolmanyok.Update(execParam_csatUpdate, erec_Csatolmanyok);
                if (!String.IsNullOrEmpty(result_csatUpdate.ErrorCode))
                {
                    // hiba
                    throw new ResultException(result_csatUpdate);
                }

                //áthelyezés ucm-ben
                if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
                {
                    string dokumentumId = row["Dokumentum_Id"].ToString();
                    UCMDocumentService ucmService = eDocumentService.ServiceFactory.GetUCMDocumentService();
                    Result result_Move = ucmService.MoveKuldemenyDocuments(execParam, dokumentumId, iktatohely, foszam, ev, alszam);

                    if (!String.IsNullOrEmpty(result_Move.ErrorCode))
                    {
                        // hiba
                        throw new ResultException(result_Move);
                    }
                }
            }
        }

        #endregion
    }


    #endregion

    #region Word-ös iktatáshoz

    /// <summary>
    /// Word-ös iktatáshoz adatok lekérése, hogy azt be lehessen illeszteni a dokumentumba:
    /// (Vonalkód generálása, várható iktatószám, alszám lekérése)
    /// </summary>
    /// <param name="felhasznaloId"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetIktatasiAdatok(ExecParam execParam, string ugyiratId)
    {
        return GetIktatasiAdatok(execParam, ugyiratId, true, true);
    }

    /// <summary>
    /// Word-ös iktatáshoz adatok lekérése, hogy azt be lehessen illeszteni a dokumentumba:
    /// (Várható iktatószám, alszám lekérése. Vonalkód generálás itt nincs.)
    /// </summary>
    /// <param name="felhasznaloId"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetIktatasiAdatok_MunkapeldanyBeiktatashoz(ExecParam execParam, string ugyiratId)
    {
        return GetIktatasiAdatok(execParam, ugyiratId, false, false);
    }

    // BUG_2600
    /// <summary>
    /// Word-ös csatoláshoz adatok lekérése, hogy azt be lehessen illeszteni a dokumentumba:
    /// (Várható iktatószám, alszám lekérése. Vonalkód generálás itt nincs.)
    /// </summary>
    /// <param name="felhasznaloId"></param>
    /// <param name="iratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetIktatasiAdatok_Csatolashoz(ExecParam execParam, string iratId)
    {
        Logger.Info("GetIktatasiAdatok_Csatolashoz - Start");

        // Paraméterek ellenőrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            Result errorResult = new Result();
            errorResult.ErrorCode = "asdf";
            errorResult.ErrorMessage = "Nincs megadva a felhasznaloId!";
            Logger.Error("Nincs megadva a felhasznaloId!");
            return errorResult;
        }
        if (String.IsNullOrEmpty(iratId))
        {
            Result errorResult = new Result();
            errorResult.ErrorCode = "asdfghjk";
            errorResult.ErrorMessage = "Nincs megadva az irat Id!";
            Logger.Error("Nincs megadva az irat Id!");
            return errorResult;
        }

        Logger.Debug("FelhasználóId: " + execParam.Felhasznalo_Id);

        //ExecParam execParam = new ExecParam();
        //execParam.Felhasznalo_Id = felhasznaloId;
        //execParam.LoginUser_Id = felhasznaloId;

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            #region Irat GET

            Logger.Debug("Irat GET - Start");

            EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);

            ExecParam execParam_iratGet = execParam.Clone();
            execParam_iratGet.Record_Id = iratId;

            Result result_iratGet = service_iratok.Get(execParam_iratGet);
            if (!String.IsNullOrEmpty(result_iratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_iratGet);
            }

            EREC_IraIratok irat = (EREC_IraIratok)result_iratGet.Record;

            #endregion

            #region Első Iratpéldány GET

            Logger.Debug("Első IratPld GET - Start");

            EREC_PldIratPeldanyokService service_iratpld = new EREC_PldIratPeldanyokService(this.dataContext);

            ExecParam execParam_iratpldGet = execParam.Clone();

            Result result_iratpldGet = service_iratpld.GetElsoIratPeldanyByIraIrat(execParam_iratpldGet, iratId);
            if (!String.IsNullOrEmpty(result_iratpldGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_iratpldGet);
            }

            EREC_PldIratPeldanyok iratpld = (EREC_PldIratPeldanyok)result_iratpldGet.Record;

            #endregion

            #region ügyintéző adatai
            KRT_FelhasznalokService felhService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            execParam.Record_Id = irat.FelhasznaloCsoport_Id_Ugyintez;
            Result ugyintezoResult = felhService.Get(execParam);
            KRT_Felhasznalok ugyintezoRecord = new KRT_Felhasznalok();
            if (!ugyintezoResult.IsError)
            {
                ugyintezoRecord = (KRT_Felhasznalok)ugyintezoResult.Record;
            }
            //TODO  - szűrési paramok
            #endregion
            #region Aláírók

            EREC_IratAlairokService service_iratAlairok = new EREC_IratAlairokService(this.dataContext);
            ExecParam execParam_iratAlairok = execParam.Clone();

            EREC_IratAlairokSearch search_iratAlairok = new EREC_IratAlairokSearch();
            search_iratAlairok.Obj_Id.Value = iratId;
            search_iratAlairok.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            search_iratAlairok.OrderBy = "AlairasSorrend";
            Result iratAlairok = service_iratAlairok.GetAllWithExtension(execParam_iratAlairok, search_iratAlairok);

            if (iratAlairok.IsError)
            {
                // hiba:
                throw new ResultException(iratAlairok);
            }

            XmlDocument xmlDoc = new System.Xml.XmlDocument();

            XmlNode docAlairok = xmlDoc.CreateNode(XmlNodeType.Element, "alairok", "");


            foreach (DataRow row in iratAlairok.Ds.Tables[0].Rows)
            {
                XmlNode docaRow = xmlDoc.CreateNode(XmlNodeType.Element, "alairo", "");
                XmlNode xmlNodeaNev = xmlDoc.CreateNode(XmlNodeType.Element, "nev", "");
                xmlNodeaNev.InnerXml = row["Alairo_Nev"].ToString();
                docaRow.AppendChild(xmlNodeaNev);

                XmlNode xmlNodeaSzerep = xmlDoc.CreateNode(XmlNodeType.Element, "szerepkor", "");
                xmlNodeaSzerep.InnerXml = row["AlairoSzerep_nev"].ToString();
                docaRow.AppendChild(xmlNodeaSzerep);

                docAlairok.AppendChild(docaRow);
            }
            #endregion

            // eredmény összeállítása (XML-ben)
            string xmlResult = @"
                <result>
                    <targy>" + irat.Targy + @"</targy>
                    <iktatoszam>" + irat.Azonosito + @"</iktatoszam>
                    <cimzett_nev>" + iratpld.NevSTR_Cimzett + @"</cimzett_nev>
                    <cimzett_cim>" + iratpld.CimSTR_Cimzett + @" </cimzett_cim>
                    <vonalkod>" + iratpld.BarCode + @" </vonalkod>              
                    <eloadonev>" + ugyintezoRecord.Nev + @"</eloadonev>";
            xmlResult += docAlairok.OuterXml + @"
                </result>";

            result.Record = xmlResult;

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(execParam, result);
        return result;
    }

    private Result GetIktatasiAdatok(ExecParam execParam, string ugyiratId, bool kellVonalkod, bool kellAlszam)
    {
        Logger.Info("GetIktatasiAdatok - Start");

        // Paraméterek ellenőrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            Result errorResult = new Result();
            errorResult.ErrorCode = "asdf";
            errorResult.ErrorMessage = "Nincs megadva a felhasznaloId!";
            Logger.Error("Nincs megadva a felhasznaloId!");
            return errorResult;
        }
        if (String.IsNullOrEmpty(ugyiratId))
        {
            Result errorResult = new Result();
            errorResult.ErrorCode = "asdfghjk";
            errorResult.ErrorMessage = "Nincs megadva az ügyirat Id!";
            Logger.Error("Nincs megadva az ügyirat Id!");
            return errorResult;
        }

        Logger.Debug("FelhasználóId: " + execParam.Felhasznalo_Id);

        //ExecParam execParam = new ExecParam();
        //execParam.Felhasznalo_Id = felhasznaloId;
        //execParam.LoginUser_Id = felhasznaloId;

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            #region Ügyirat GET

            Logger.Debug("ügyirat GET - Start");

            EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);

            ExecParam execParam_ugyiratGet = execParam.Clone();
            execParam_ugyiratGet.Record_Id = ugyiratId;

            Result result_ugyiratGet = service_ugyiratok.Get(execParam_ugyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }

            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

            #endregion

            #region Iktatókönyv GET

            Logger.Debug("Iktatókönyv GET - Start");

            EREC_IraIktatoKonyvekService service_IktKonyvGet = new EREC_IraIktatoKonyvekService(this.dataContext);

            ExecParam execParam_iktKonyvGet = execParam.Clone();
            execParam_iktKonyvGet.Record_Id = ugyirat.IraIktatokonyv_Id;

            Result result_iktKonyvGet = service_IktKonyvGet.Get(execParam_iktKonyvGet);
            if (!String.IsNullOrEmpty(result_iktKonyvGet.ErrorCode))
            {
                throw new ResultException(result_iktKonyvGet);
            }

            EREC_IraIktatoKonyvek iktatoKonyv = (EREC_IraIktatoKonyvek)result_iktKonyvGet.Record;

            #endregion

            Logger.Debug("Ügyirat utolsó alszáma: " + ugyirat.UtolsoAlszam);
            int utolsoAlszam = Int32.Parse(ugyirat.UtolsoAlszam);
            int utolsoSorszam = Int32.Parse(ugyirat.UtolsoSorszam);

            string alszam_result = (kellAlszam) ? (utolsoAlszam + 1).ToString() : "0";

            //Iktatószám összeállítása:
            EREC_IraIratok tempIrat = new EREC_IraIratok();
            tempIrat.Alszam = alszam_result;
            tempIrat.Sorszam = (utolsoSorszam + 1).ToString();
            // BLG_292
            //string iktatoszam_result = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(iktatoKonyv, ugyirat, tempIrat);
            string iktatoszam_result = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(execParam, iktatoKonyv, ugyirat, tempIrat);


            string vonalkod_result = "";
            if (kellVonalkod)
            {
                #region BARCODE generálás

                Logger.Debug("Vonalkód generálás");

                string vonalkodkezeles = Rendszerparameterek.Get(execParam.Clone(), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim();

                if (vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
                {
                    string barcode =
                        iktatoKonyv.Iktatohely + " /" +
                        (!kellAlszam ?
                            ("MU" +
                                (String.IsNullOrEmpty(ugyirat.Foszam) ?
                                    ugyirat.Sorszam.PadLeft(6, '0') :
                                    ugyirat.Foszam.PadLeft(6, '0'))
                                ) :
                                ugyirat.Foszam.PadLeft(6, '0'))
                        + " - " +
                        (!kellAlszam ?
                            ("MI" + (utolsoSorszam + 1).ToString().PadLeft(6, '0')) :
                            alszam_result.PadLeft(6, '0'))
                        + " /" + iktatoKonyv.Ev;
                    vonalkod_result = barcode.Replace(" ", "");
                }
                else
                {

                    KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                    ExecParam execParam_barkod = execParam.Clone();

                    Result result_barkod = service_barkodok.BarkodGeneralas(execParam_barkod);
                    if (!String.IsNullOrEmpty(result_barkod.ErrorCode))
                    {
                        throw new ResultException(result_barkod);
                    }
                    else
                    {
                        //barkod_ugyirat_Id = result_barkod_ugyirat.Uid;
                        vonalkod_result = (String)result_barkod.Record;
                    }
                }

                #endregion
            }

            // eredmény összeállítása (XML-ben)
            string xmlResult = @"
                <result>
                    <vonalkod>" + vonalkod_result + @"</vonalkod>
                    <iktatoszam>" + iktatoszam_result + @"</iktatoszam>
                    <alszam>" + alszam_result + @"</alszam>
                </result>";

            result.Record = xmlResult;

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public string IktatasWordbol_TesztWebMethod(int alszam, string vonalkod)
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";
        execParam.LoginUser_Id = execParam.Felhasznalo_Id;

        //        string iktatasiAdatok = @"
        //                <iktatasiadatok>
        //                    <kivalasztottUgyiratId>61CA6627-FA8D-DD11-9FD4-0019DB4CF22D</kivalasztottUgyiratId>
        //                    <alszam>" + alszam.ToString() + @"</alszam>
        //                    <vonalkod>" + vonalkod + @"</vonalkod>
        //                    <targy>teszt</targy>
        //                    <cimzett_nev>Gipsz Jakab</cimzett_nev>
        //                    <cimzett_id/>
        //                    <cimzett_cim/>
        //                    <cimzett_cim_id/>
        //                    <alairo_id>d97f54fb-fcc8-4905-96b3-092a64dcfbaa</alairo_id>
        //                </iktatasiadatok>
        //                ";

        string iktatasiAdatok = @"
            <iktatasiadatok>               
             <kivalasztottUgyiratId>bd7a171e-8f54-ea11-80e9-00155d017b45</kivalasztottUgyiratId>
                <alszam>" + alszam.ToString() + @"</alszam>
                <vonalkod>" + vonalkod.ToString() + @" </vonalkod>
                <targy>sdrfsdfsdf</targy>
                <cimzett_nev>Albert Ferenc Autószerelő (100949247)</cimzett_nev>
                <cimzett_id>a8a903b3-2a64-41c9-bf50-00f93c28fff3</cimzett_id>
                <cimzett_cim>1182 Budapest Üllöi  út 605. </cimzett_cim>
                <cimzett_cim_id>eb9598f9-4230-4b9f-bedd-14c86dd1f880</cimzett_cim_id>
                <megjegyzes>sdf</megjegyzes>
                <munkaanyag>NEM</munkaanyag>
                <ujirat>IGEN</ujirat>
                <kivalasztottIratId/>
                <docmetaIratId/>
                <docmetaDokumentumId/>
                <ujIratCttjeLegyen>NEM</ujIratCttjeLegyen>
                <tartalomSHA1>50A358D4693393A93279095F62FB2E4EA1C75867</tartalomSHA1>
                <alairok>
                    <alairo>
                        <id>bee37600-ab7e-436f-a95a-a8d257a72139</id>
                        <nev>Gábor Miklós</nev>
                        <szerepkor>3</szerepkor>
                        <szervezet>Külsősök</szervezet>
                        <szervezetid>60a275ea-c849-442d-a39c-bcfdfed6c42f</szervezetid>
                    </alairo>
                </alairok>
                <ujiratkiadmanyozhato>NEM</ujiratkiadmanyozhato>
            </iktatasiadatok>
            ";


        Csatolmany csatolmany = new Csatolmany();

        System.Text.UTF8Encoding utf8encoding = new System.Text.UTF8Encoding();
        csatolmany.Tartalom = utf8encoding.GetBytes("Egy meggymag nem meggymag");

        csatolmany.Nev = "proba_6_" + alszam.ToString() + ".txt";

        Result result = IktatasWordbol(execParam, iktatasiAdatok, csatolmany);
        return result.ErrorMessage;
    }

    //    [WebMethod()]
    //    public string MunkaanyagBeiktatas_TesztWebMethod()
    //    {
    //        ExecParam execParam = new ExecParam();
    //        execParam.Felhasznalo_Id = "f088e89e-7917-4d0b-b94f-f4dac84f0f95";
    //        execParam.LoginUser_Id = execParam.Felhasznalo_Id;

    //        string iktatasiAdatok = @"
    //                <iktatasiadatok>
    //                    <ugyirat_id>b6164a4b-80b0-4804-804c-588d87fea927</ugyirat_id>
    //                    <irat_id>d4d6e9bb-f859-4eba-838f-4861dd57361c</irat_id>
    //                    <alszam>4</alszam>
    //                    <vonalkod></vonalkod>
    //                    <targy>teszt</targy>
    //                    <cimzett_nev>Gipsz Jakab</cimzett_nev>
    //                    <cimzett_id/>
    //                    <cimzett_cim/>
    //                    <cimzett_cim_id/>
    //                    <alairo_id></alairo_id>
    //                </iktatasiadatok>
    //                ";

    //        Csatolmany csatolmany = new Csatolmany();
    //        csatolmany.Nev = "proba.txt";
    //        System.Text.UTF8Encoding utf8encoding = new System.Text.UTF8Encoding();

    //        csatolmany.Tartalom = utf8encoding.GetBytes("Egy meggymag nem meggymag");
    //        csatolmany.Nev = "proba.txt";

    //        Result result = MunkapeldanyBeiktatasa_Wordbol(execParam, iktatasiAdatok, csatolmany);
    //        return result.ErrorMessage;
    //    }


    //[WebMethod()]
    //public string MunkaanyagLetrehozas_TesztWebMethod()
    //{
    //    ExecParam execParam = new ExecParam();
    //    execParam.Felhasznalo_Id = "f088e89e-7917-4d0b-b94f-f4dac84f0f95";
    //    execParam.LoginUser_Id = execParam.Felhasznalo_Id;

    //    string ugyiratId = "61d87a66-bec6-416b-8b8c-bdb50027db63";

    //    EREC_IraIratok munkaanyag = new EREC_IraIratok();

    //    munkaanyag.Targy = "munkaanyag teszt";
    //    munkaanyag.Updated.Targy = true;

    //    EREC_PldIratPeldanyok pld = new EREC_PldIratPeldanyok();
    //    pld.NevSTR_Cimzett = "valami cimzett";
    //    pld.Updated.NevSTR_Cimzett = true;

    //    Result result = this.IktatasElokeszites(execParam, ugyiratId, munkaanyag, pld);

    //    return result.ErrorMessage;
    //}

    /// <summary>
    /// Wordbol addinból való iktatás/csatolás.
    /// Tranzakciókezlet. 
    ///
    /// Xml feldolgozása (iktatási adatok)
    /// Ha van irat, leszedjuk - kesobbi ellenorzesekhez
    /// Ellenőrzés: Meglévő iratba akarunk feltölteni? - nem kiadmányozott
    /// Ellenőrzés: Ha nem az eredeti iratba (worddoc iratId != kiválasztott iratId)...
    /// Ellenőrzés, a megadott alszám jó-e még 
    /// Csatolmány feltöltése Sharepointba 1. --> visszajön a KRT_Dokumentunok rekordId + XML adatok stringkent
    /// Iktatás ws meghívása (munkaanyag vagy nem munkaanyag)
    /// Csatolmány feltöltése Sharepointba 2. --> check in és a hozzá kapcsolódó feladatok.
    /// Alszám újbóli ellenőrzése -> ha nem jó, tranzakció visszagörgetés, fájl kiszedése MOSSból.
    /// Csatolmány hozzákötése az irathoz.
    /// Aláírás berögzítése, ha volt megadva aláíró.
    /// 
    /// </summary>
    /// <param name="execParam">execPar - benne a felhasználó adataival</param>
    /// <param name="iktatasiAdatokXml">xml string paraméterek</param>
    /// <param name="csatolmany">csatolmány objektum - aláírásokkal, mindennel együtt</param>
    /// <returns>Result - hiba vagy recordban xml string.</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(Csatolmany))]
    public Result IktatasWordbol(ExecParam execParam, string iktatasiAdatokXml, Csatolmany csatolmany)
    {
        Logger.Info(String.Format("Iktatás Wordből indul. iktatasiAdatokXml: {0}", iktatasiAdatokXml));

        //
        // (TODO: Feltöltés visszatörlése nem megoldott.)
        //

        #region Paraméterek ellenőrzése

        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(iktatasiAdatokXml) || csatolmany == null || csatolmany.Tartalom == null)
        {
            Result errorResult = new Result();
            errorResult.ErrorCode = "asdfghjk";
            errorResult.ErrorMessage = "Nincs megadva minden paraméter!";
            Logger.Error("Nincs megadva minden paraméter!");
            return errorResult;
        }
        #endregion

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        bool csatolmanyFeltoltve = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            //Később indítjuk a tranzakciót
            //isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            /* Lépések (a sokadik elkepzeles utan talan....):
             * 
             * 1. Xml feldolgozása (iktatási adatok)
             * 2. Ha van irat, leszedjuk - kesobbi ellenorzesekhez
             * 3. Ellenorzes: Meglevo iratba akarunk feltolteni? - nem kiadmanyozott
             * 4. Ellenorzes: Ha nem az eredeti iratba (worddoc iratId != kivalasztott iratId)...
             * 5. Ellenőrzés, a megadott alszám jó-e még 
             * 6. Csatolmány feltöltése Sharepointba 1. --> visszajön a KRT_Dokumentunok rekordId + XML adatok stringkent
             * 6.5 Tranzakció indítás
             * 7. Iktatás ws meghívása ha uj es nem munkaanyag
             * 7.5 Iktatás ws meghívása ha uj es munkaanyag
             * 8. Csatolmány feltöltése Sharepointba 2. --> check in es egyeb nyalanksagok
             * 9. Alszám újbóli ellenőrzése -> ha nem jó, tranzakció visszagörgetés, fájl kiszedése Sharepointból
             * 10. Csatolmány hozzákötése az irathoz
             * 11. Ha nem uj (tehat csak csatolas), az alapveto mezok valtozasanak ellenorzese
             * 12. Aláírás berögzítése, ha volt megadva aláíró
             * 
             */

            #region Xml feldolgozása (iktatási adatok)

            Logger.Debug("Xml feldolgozása (Iktatási adatok)", execParam);

            const string rootNodeName = "iktatasiadatok";
            const string ugyiratIdNodeName = "kivalasztottUgyiratId";

            const string alszamNodeName = "alszam";
            const string vonalkodNodeName = "vonalkod";
            const string targyNodeName = "targy";
            const string cimzettNevNodeName = "cimzett_nev";
            const string cimzettIdNodeName = "cimzett_id";
            const string cimzettCimNodeName = "cimzett_cim";
            const string cimzettCimIdNodeName = "cimzett_cim_id";
            const string alairokNodeName = "alairok";
            const string megjegyzesNodeName = "megjegyzes";
            const string munkaanyagNodeName = "munkaanyag";
            const string ujiratNodeName = "ujirat";
            const string iratIdKivalasztottNodeName = "kivalasztottIratId";
            const string iratIdDocMetabolNodeName = "docmetaIratId";
            const string dokumentumIdNodeName = "docmetaDokumentumId";
            const string wordTartalomSHA1NodeName = "tartalomSHA1";
            const string metaadatokNodeName = "metaadatok";
            const string ujiratkiadmanyozhatoNodeName = "ujiratkiadmanyozhato";
            const string ujiratTipusaEzLegyenNodeName = "ujIratTipusaEzLegyen";
            const string cttNeveNodeName = "cttneve";
            const string fodokumentumNodeName = "fodokumentum";
            const string hivatkozasiSzamNodeName = "hivatkozasiszam";
            const string eloadoIdNodeName = "eloadoid";
            const string eloadoNevNodeName = "eloadonev";
            // BLG_579
            const string AdathordozoTipusaNodeName = "adathordozotipus";
            const string ElsodlAdathordozoNodeName = "elsodladathordozo";

            String ugyiratId = String.Empty;
            String alszam = String.Empty;
            String vonalkod = String.Empty;
            String targy = String.Empty;
            String cimzettNev = String.Empty;
            String cimzettId = String.Empty;
            String cimzettCim = String.Empty;
            String cimzettCimId = String.Empty;
            String megjegyzes = String.Empty;
            String munkaanyag = String.Empty;
            String ujirat = String.Empty;
            bool isUjirat = false;
            String kivalasztottIratId = String.Empty;
            String docmetaIratId = String.Empty;
            String docmetaDokumentumId = String.Empty;
            String wordTartalomSHA1 = String.Empty;
            String ujiratkiadmanyozhato = String.Empty;
            String ujiratTipusaEzLegyen = String.Empty;
            String cttNeve = String.Empty;
            String fodokumentum = String.Empty;
            String hivatkozasiSzam = String.Empty;
            String eloadoId = String.Empty;
            String eloadoNev = String.Empty;
            // BLG_579
            String adathordozoTipus = String.Empty;
            String elsodlAdathordozoTipus = String.Empty;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(iktatasiAdatokXml);

            XmlNode munkaanyagNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + munkaanyagNodeName);
            if (munkaanyagNode != null)
            {
                munkaanyag = munkaanyagNode.InnerText;
                Logger.Debug("munkaanyag: " + munkaanyag, execParam);
            }

            XmlNode ujiratNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + ujiratNodeName);
            if (ujiratNode != null)
            {
                ujirat = ujiratNode.InnerText;
                isUjirat = ujirat.Equals("IGEN");
                Logger.Debug("ujirat: " + ujirat, execParam);
            }


            XmlNode iratIdNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + iratIdKivalasztottNodeName);
            if (!isUjirat)
            {
                if (iratIdNode != null)
                {
                    kivalasztottIratId = iratIdNode.InnerText;
                    Logger.Debug("IratId: " + kivalasztottIratId);
                }
            }

            // munkapéldánybeiktatásnál kell az irat id:
            if ("NEM".Equals(ujirat) && "IGEN".Equals(munkaanyag) && String.IsNullOrEmpty(kivalasztottIratId))
            {
                Logger.Error("Nincs megadva az irat Id!", execParam);
                throw new ResultException("Nincs megadva az irat Id!");
            }

            XmlNode ugyiratIdNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + ugyiratIdNodeName);
            if (ugyiratIdNode != null)
            {
                ugyiratId = ugyiratIdNode.InnerText;
                Logger.Debug("ÜgyiratId: " + ugyiratId);
            }
            else
            {
                Logger.Error("Nincs megadva az ügyirat Id!", execParam);
                throw new ResultException("Nincs megadva az ügyirat Id!");
            }

            XmlNode alszamNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + alszamNodeName);
            if (alszamNode != null)
            {
                alszam = alszamNode.InnerText;
                Logger.Debug("Alszám: " + alszam, execParam);
            }
            else
            {
                Logger.Error("Nincs megadva az irat alszáma", execParam);
                throw new ResultException("Nincs megadva az irat alszáma!");
            }

            XmlNode vonalkodNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + vonalkodNodeName);
            if (vonalkodNode != null)
            {
                vonalkod = vonalkodNode.InnerText;
                Logger.Debug("Vonalkód: " + vonalkod, execParam);
            }

            XmlNode targyNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + targyNodeName);
            if (targyNode != null)
            {
                targy = targyNode.InnerText;
                Logger.Debug("Tárgy: " + targy, execParam);
            }

            XmlNode cimzettNevNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + cimzettNevNodeName);
            if (cimzettNevNode != null)
            {
                cimzettNev = cimzettNevNode.InnerText;
                Logger.Debug("Címzett: " + cimzettNev, execParam);
            }

            XmlNode cimzettIdNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + cimzettIdNodeName);
            if (cimzettIdNode != null)
            {
                cimzettId = cimzettIdNode.InnerText;
                Logger.Debug("Címzett Id: " + cimzettId, execParam);
            }

            XmlNode cimzettCimNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + cimzettCimNodeName);
            if (cimzettCimNode != null)
            {
                cimzettCim = cimzettCimNode.InnerText;
                Logger.Debug("Címzett címe: " + cimzettCim, execParam);
            }

            XmlNode cimzettCimIdNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + cimzettCimIdNodeName);
            if (cimzettCimIdNode != null)
            {
                cimzettCimId = cimzettCimIdNode.InnerText;
                Logger.Debug("Címzett cím Id: " + cimzettCimId, execParam);
            }

            XmlNode alairokNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + alairokNodeName);
            if (alairokNode != null)
            {
                Logger.Debug("Aláírók megvannak. ", execParam);
            }

            XmlNode megjegyzesNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + megjegyzesNodeName);
            if (megjegyzesNode != null)
            {
                megjegyzes = megjegyzesNode.InnerXml; // XML-ben adjuk tovább, ezért maszkolva kell
                Logger.Debug("megjegyzes: " + megjegyzes, execParam);
            }

            XmlNode iratIdNode2 = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + iratIdDocMetabolNodeName);
            if (iratIdNode2 != null)
            {
                docmetaIratId = iratIdNode2.InnerText;
                Logger.Debug("docmetaIratId: " + docmetaIratId, execParam);
            }

            XmlNode dokumentumIdNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + dokumentumIdNodeName);
            if (dokumentumIdNode != null)
            {
                docmetaDokumentumId = dokumentumIdNode.InnerText;
                Logger.Debug("docmetaDokumentumId: " + docmetaDokumentumId, execParam);
            }

            XmlNode wordTartalomSHA1Node = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + wordTartalomSHA1NodeName);
            if (wordTartalomSHA1Node != null)
            {
                wordTartalomSHA1 = wordTartalomSHA1Node.InnerText;
                Logger.Debug("wordTartalomSHA1: " + wordTartalomSHA1, execParam);
            }

            XmlNode metaadatokNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + metaadatokNodeName);
            if (metaadatokNode != null)
            {
                Logger.Debug("Metaadatok megvannak. ", execParam);
            }


            XmlNode ujiratkiadmanyozhatoNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + ujiratkiadmanyozhatoNodeName);
            if (ujiratkiadmanyozhatoNode != null)
            {
                ujiratkiadmanyozhato = ujiratkiadmanyozhatoNode.InnerText;
                Logger.Debug("ujiratkiadmanyozhato: " + ujiratkiadmanyozhato, execParam);
            }

            XmlNode ujiratTipusaEzLegyenNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + ujiratTipusaEzLegyenNodeName);
            if (ujiratTipusaEzLegyenNode != null)
            {
                ujiratTipusaEzLegyen = ujiratTipusaEzLegyenNode.InnerText;
                Logger.Debug("ujiratTipusaEzLegyen: " + ujiratTipusaEzLegyen, execParam);
            }

            XmlNode cttNeveNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + cttNeveNodeName);
            if (cttNeveNode != null)
            {
                cttNeve = cttNeveNode.InnerText;
                Logger.Debug("cttNeve: " + cttNeve, execParam);
            }

            XmlNode fodokumentumNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + fodokumentumNodeName);
            if (fodokumentumNode != null)
            {
                fodokumentum = fodokumentumNode.InnerText;
                Logger.Debug("fodokumentum: " + fodokumentum, execParam);
            }

            XmlNode hivatkozasiSzamNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + hivatkozasiSzamNodeName);
            if (hivatkozasiSzamNode != null)
            {
                hivatkozasiSzam = hivatkozasiSzamNode.InnerText;
                Logger.Debug("hivatkozasiSzam: " + hivatkozasiSzam, execParam);
            }

            XmlNode eloadoIdNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + eloadoIdNodeName);
            if (eloadoIdNode != null)
            {
                eloadoId = eloadoIdNode.InnerText;
                Logger.Debug("eloadoId: " + eloadoId, execParam);
            }

            XmlNode eloadoNevNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + eloadoNevNodeName);
            if (eloadoNevNode != null)
            {
                eloadoNev = eloadoNevNode.InnerText;
                Logger.Debug("eloadoNev: " + eloadoNev, execParam);
            }

            // BLG_579
            XmlNode adathordozoTipusaNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + AdathordozoTipusaNodeName);
            if (adathordozoTipusaNode != null)
            {
                adathordozoTipus = adathordozoTipusaNode.InnerText;
                Logger.Debug("adathordozoTipus: " + adathordozoTipus, execParam);
            }

            // BLG_579
            XmlNode elsodlAdathordozoNode = xmlDoc.SelectSingleNode("./" + rootNodeName + "/" + ElsodlAdathordozoNodeName);
            if (elsodlAdathordozoNode != null)
            {
                elsodlAdathordozoTipus = elsodlAdathordozoNode.InnerText;
                Logger.Debug("elsodlAdathordozoTipus: " + elsodlAdathordozoTipus, execParam);
            }
            #endregion

            #region  Az ugyirathoz van-e mar ilyen SHA1 tartalmu feltoltve, mert ha igen akkor visszadobjuk hibaval

            if (!String.IsNullOrEmpty(wordTartalomSHA1))
            {
                // TODO:.... majd ide lesz.....
            }

            #endregion

            #region Ha van irat id, leszedjuk az iratot kesobbi ellenorzesek celjabol

            EREC_IraIratok erec_IraIrat = null;

            if (!String.IsNullOrEmpty(kivalasztottIratId))
            {
                Logger.Debug("Van kivalasztottIratId, ezert irat leszedese indul.");
                EREC_IraIratokService service_Iratok = new EREC_IraIratokService(this.dataContext);
                ExecParam execParam_iratGet = execParam.Clone();
                execParam_iratGet.Record_Id = kivalasztottIratId;

                Result result_IratGet = service_Iratok.Get(execParam_iratGet);
                if (!String.IsNullOrEmpty(result_IratGet.ErrorCode))
                {
                    // hiba:
                    Logger.Error(String.Format("Irat leszedese hiba! Code: {0} Message: {1}", result_IratGet.ErrorCode, result_IratGet.ErrorMessage));
                    //throw new ResultException(result_IratGet);
                    return result_IratGet;
                }
                else
                {
                    if (result_IratGet.Record == null)
                    {
                        // hiba:
                        Logger.Error(String.Format("Irat leszedese hiba! Null a record mezo."));
                        throw new ResultException(52321);
                    }

                    Logger.Debug(String.Format("Irat leszedese ok."));
                    erec_IraIrat = (EREC_IraIratok)result_IratGet.Record;
                }
            }

            #endregion

            #region Meglevo iratba akarunk feltolteni: ellenorizni kell, h nem kiadmanyozott-e

            if (ujirat.Equals("NEM") && munkaanyag.Equals("NEM") && !String.IsNullOrEmpty(kivalasztottIratId))
            {
                Logger.Debug(String.Format("Meglevo irat ellenorzese."));
                // kiadmanyozott
                if (erec_IraIrat != null)
                {

                    if (erec_IraIrat.Allapot == Contentum.eUtility.KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
                    //if (Contentum.eRecord.BaseUtility.Iratok.Modosithato(Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(erec_IraIrat), execParam))
                    {
                        Logger.Error(String.Format("A megadott id-ju irat mar kiadmanyozott! id: {0}", kivalasztottIratId));
                        throw new ResultException("Kiadmányozott iratba nem lehet feltölteni!");
                    }
                }
                else
                {
                    Logger.Error(String.Format("A wordbol megadott id-ju irat nem talalhato! id: {0}", kivalasztottIratId));
                    throw new ResultException("A wordbol megadott irat nem található!");
                }
            }

            #endregion

            #region Ha kulonbozik az ket irat id es !ujirat, vizsgalodni kell. (Ez meg nem tudom igy kell-e ide....!?!?!)

            bool ujKrtDokBejegyzesKell = false;

            Logger.Debug(String.Format("Kapott ket id -> docmetaIratId: {0} ; irat_id: {1} es az ujirat flag: {2}", docmetaIratId, kivalasztottIratId, ujirat));

            if (!String.IsNullOrEmpty(docmetaIratId) && !docmetaIratId.Equals(kivalasztottIratId) && ujirat.Equals("NEM"))
            {
                Logger.Debug(String.Format("Kulonbozik az ket irat id (docmetaIratId: {0} ; irat_id: {1}), meg kell nezni letezik-e ilyen nevu filehoz bejegyzes a krt_dokumentumokban.", docmetaIratId, kivalasztottIratId));

                // megnezzuk, h ehhez az irathoz van-e ilyen nevu file bejegyzes, mert ha nincs, akkor insert mindenkeppen kell a krt_dokumentumok tablaba

                EREC_CsatolmanyokService csatService = new EREC_CsatolmanyokService(this.dataContext);
                ExecParam execp2 = execParam.Clone();

                EREC_CsatolmanyokSearch csatSearch = new EREC_CsatolmanyokSearch();

                csatSearch.IraIrat_Id.Value = kivalasztottIratId;
                csatSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                csatSearch.Manual_Dokumentum_Nev.Value = csatolmany.Nev;
                csatSearch.Manual_Dokumentum_Nev.Operator = Contentum.eQuery.Query.Operators.equals;

                Result resultCsatt = csatService.GetAllWithExtension(execp2, csatSearch);

                if (!String.IsNullOrEmpty(resultCsatt.ErrorCode))
                {
                    Logger.Error(String.Format("Hibával tért vissza a csatolmány lekérdezés! ErrCode: {0}\nErrM: {1}", resultCsatt.ErrorCode, resultCsatt.ErrorMessage));
                    return resultCsatt;
                }

                if (resultCsatt.Ds.Tables[0].Rows.Count == 0)
                {
                    ujKrtDokBejegyzesKell = true;
                    Logger.Debug("Nulla a talált csatolmányok száma a megadott irat idhez, ezért lesz insert.");
                }
            }

            #endregion

            #region Ellenőrzés, a megadott alszám jó-e még

            Logger.Debug(String.Format("Ellenőrzés, a megadott alszám jó-e még."));

            // ügyirat lekérés:
            EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_ugyiratGet = execParam.Clone();
            execParam_ugyiratGet.Record_Id = ugyiratId;

            Result result_ugyiratGet = service_ugyiratok.Get(execParam_ugyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                Logger.Error(String.Format("Hiba az ugyirat lekerdezese soran! ErrCode: {0}  ErrMsg: {1}", result_ugyiratGet.ErrorCode, result_ugyiratGet.ErrorMessage));
                throw new ResultException(result_ugyiratGet);
            }

            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

            int utolsoAlszam = Int32.Parse(ugyirat.UtolsoAlszam);
            int megadottAlszam = Int32.Parse(alszam);

            if (ujirat.Equals("IGEN") && munkaanyag.Equals("NEM"))
            {
                if (megadottAlszam != utolsoAlszam + 1)
                {
                    // hiba:
                    Logger.Error("Alszám nem stimmel: ügyirat utolsó alszáma: " + utolsoAlszam.ToString() + " Megadott alszám: " + alszam, execParam);
                    throw new ResultException("A megadott alszám nem megfelelő, lehet hogy már foglalt.");
                }
            }

            #endregion

            // Tranzakció indítása:
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Objektumok feltöltése

            EREC_IraIratok iratObj = new EREC_IraIratok();
            iratObj.Updated.SetValueAll(false);
            iratObj.Base.Updated.SetValueAll(false);

            iratObj.Targy = targy;
            iratObj.Updated.Targy = true;

            if (!String.IsNullOrEmpty(hivatkozasiSzam))
            {
                iratObj.HivatkozasiSzam = hivatkozasiSzam;
                iratObj.Updated.HivatkozasiSzam = true;
            }

            if (!String.IsNullOrEmpty(eloadoId))
            {
                iratObj.FelhasznaloCsoport_Id_Ugyintez = eloadoId;
                iratObj.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
            }

            if (ujiratkiadmanyozhato.Equals("IGEN"))
            {
                Logger.Debug("--- kiadmanyozni kell igen");
                iratObj.KiadmanyozniKell = "1";
                iratObj.Updated.KiadmanyozniKell = true;
            }

            //iratObj.AdathordozoTipusa 
            //iratObj.AdathordozoTipusa = KodTarak.AdathordozoTipus.Egyeb???!!!;
            //iratObj.Updated.AdathordozoTipusa = true;

            //iratObj.kezeles modja - elektromos
            // BLG_579
            //iratObj.UgyintezesAlapja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            iratObj.UgyintezesAlapja = elsodlAdathordozoTipus;
            iratObj.Updated.UgyintezesAlapja = true;
            // BLG_579
            //iratObj.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir; //"1"; -> "0"
            iratObj.AdathordozoTipusa = adathordozoTipus;
            iratObj.Updated.AdathordozoTipusa = true;

            iratObj.Jelleg = KodTarak.IRAT_JELLEG.Word_dokumentum;
            iratObj.Updated.Jelleg = true;

            #region irat tipusa ez legyen pipa eseten + fodokumentum pipa 

            if (!String.IsNullOrEmpty(ujiratTipusaEzLegyen))
            {
                Logger.Debug(String.Format("kapott ujiratTipusaEzLegyen: {0}", ujiratTipusaEzLegyen));
                iratObj.Irattipus = ujiratTipusaEzLegyen;
                iratObj.Updated.Irattipus = true;
            }


            if (!String.IsNullOrEmpty(fodokumentum))
            {
                Logger.Debug(String.Format("service_iratMetaDef.GetIdIratMetaDefinicioByContentType resz indul. fodokumentum: {0}", fodokumentum));

                // IratMetaDefId meghatározása:
                EREC_IratMetaDefinicioService service_iratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);

                // a ctt neve kell ide!
                Result resIratMetaDef = service_iratMetaDef.GetIdIratMetaDefinicioByContentType(execParam.Clone(), fodokumentum);

                if (!String.IsNullOrEmpty(resIratMetaDef.ErrorCode))
                {
                    Logger.Error(String.Format("service_iratMetaDef.GetIdIratMetaDefinicioByContentType hibaval tert vissza!\nErrorCode: {0}\nErrorMessage: {1}", resIratMetaDef.ErrorCode, resIratMetaDef.ErrorMessage));
                    return resIratMetaDef;
                }

                Logger.Debug(String.Format("kapott iratmetadef id: {0}", resIratMetaDef.Uid));

                if (resIratMetaDef.Uid != null)
                {
                    iratObj.IratMetaDef_Id = resIratMetaDef.Uid;
                    iratObj.Updated.IratMetaDef_Id = true;
                }

            }

            #endregion

            EREC_PldIratPeldanyok iratPeldanyObj = new EREC_PldIratPeldanyok();
            iratPeldanyObj.Updated.SetValueAll(false);
            iratPeldanyObj.Base.Updated.SetValueAll(false);

            iratPeldanyObj.NevSTR_Cimzett = cimzettNev;
            iratPeldanyObj.Updated.NevSTR_Cimzett = true;

            iratPeldanyObj.Partner_Id_Cimzett = cimzettId;
            iratPeldanyObj.Updated.Partner_Id_Cimzett = true;

            iratPeldanyObj.CimSTR_Cimzett = cimzettCim;
            iratPeldanyObj.Updated.CimSTR_Cimzett = true;

            iratPeldanyObj.Cim_id_Cimzett = cimzettCimId;
            iratPeldanyObj.Updated.Cim_id_Cimzett = true;

            //munkaanyagnál már nem vágjuk fejbe a vonalkódot:
            if ("NEM".Equals(munkaanyag))
            {
                iratPeldanyObj.BarCode = vonalkod;
                iratPeldanyObj.Updated.BarCode = true;
            }
            // BLG_579
            //iratPeldanyObj.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            iratPeldanyObj.UgyintezesModja = adathordozoTipus;

            iratPeldanyObj.Updated.UgyintezesModja = true;

            string defaultKuldesMod;
            if (!Rendszerparameterek.IsParamExit(execParam, "EXPEDIALAS_MODJA_DEFAULT"))
            {
                defaultKuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Helyben;
            }
            else
            {
                defaultKuldesMod = Rendszerparameterek.Get(execParam, "EXPEDIALAS_MODJA_DEFAULT");
            }

            if (!String.IsNullOrEmpty(defaultKuldesMod))
            {
                iratPeldanyObj.KuldesMod = defaultKuldesMod;
                iratPeldanyObj.Updated.KuldesMod = true;
            }

            #endregion

            Result result_iktatas = null;

            //  csak ujirat eseten van iktatas, kulonben csatolas
            if ("IGEN".Equals(ujirat))
            {
                if ("IGEN".Equals(munkaanyag))
                {
                    #region Iktatás WS meghívása - uj es munkaanyag

                    ExecParam execParam_Iktatas = execParam.Clone();

                    Logger.Debug("IktatasWordbol IktatasElokeszites hivas elott.");
                    result_iktatas = this.IktatasElokeszites(execParam_Iktatas, ugyiratId
                            , iratObj, iratPeldanyObj);

                    if (!String.IsNullOrEmpty(result_iktatas.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_iktatas);
                    }
                    Logger.Debug("IktatasWordbol BelsoIratIktatasa_Alszamra hivas utan. Ok.");

                    kivalasztottIratId = result_iktatas.Uid;

                    #endregion
                }
                else
                {
                    #region Iktatás WS meghívása - uj es nem munkaanyag

                    ExecParam execParam_Iktatas = execParam.Clone();
                    IktatasiParameterek ip = new IktatasiParameterek();

                    result_iktatas = this.BelsoIratIktatasa_Alszamra(execParam_Iktatas, ugyirat.IraIktatokonyv_Id, ugyiratId
                            , new EREC_UgyUgyiratdarabok(), iratObj, null, iratPeldanyObj, ip);

                    if (!String.IsNullOrEmpty(result_iktatas.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_iktatas);
                    }

                    #endregion

                    kivalasztottIratId = result_iktatas.Uid;
                }
            }


            #region iktatoszam visszaselectalasa, mert vissza kell adni - de lehetne kicsit redundansabb a DB s nem kene ez a resz! vagy van fuggveny?

            String visszaIktatoszam = String.Empty;
            DateTime iktatasIdeje = DateTime.Now;

            Logger.Debug(String.Format("iktatoszam visszaselectalasa, mert vissza kell adni resz indul - kivalasztottIratId: {0}", kivalasztottIratId));

            EREC_IraIratokService service_Iratok2 = new EREC_IraIratokService(this.dataContext);
            ExecParam execParam_iratGet2 = execParam.Clone();
            execParam_iratGet2.Record_Id = kivalasztottIratId;

            EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();

            iratokSearch.Id.Value = kivalasztottIratId;
            iratokSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result result_IratGet2 = service_Iratok2.GetAllWithExtension(execParam_iratGet2, iratokSearch);
            if (!String.IsNullOrEmpty(result_IratGet2.ErrorCode))
            {
                // hiba:
                Logger.Error(String.Format("Irat leszedese hiba az iktatoszam miatti visszaselectkor! Code: {0} Message: {1}", result_IratGet2.ErrorCode, result_IratGet2.ErrorMessage));
                //throw new ResultException(result_IratGet);
                return result_IratGet2;
            }
            else
            {
                if (result_IratGet2.Ds.Tables[0].Rows.Count == 0)
                {
                    // hiba:
                    Logger.Error(String.Format("Irat leszedese hiba miatti visszaselectkor! Nincsenek eredmenysorok!"));
                    throw new ResultException(50101); //  Rekord lekérése az adatbázisból sikertelen
                }

                if (result_IratGet2.Ds.Tables[0].Rows.Count > 1)
                {
                    // hiba:
                    Logger.Error(String.Format("Irat leszedese hiba miatti visszaselectkor! Tul sok eredmenysor!"));
                    throw new ResultException(52100);  //  Hiba az iktatás során
                }

                Logger.Debug(String.Format("Irat leszedese ok."));
                visszaIktatoszam = Convert.ToString(result_IratGet2.Ds.Tables[0].Rows[0]["Iktatoszam_Merge"]);
                if (result_IratGet2.Ds.Tables[0].Rows[0]["IktatasDatuma"] != DBNull.Value)
                {
                    iktatasIdeje = Convert.ToDateTime(result_IratGet2.Ds.Tables[0].Rows[0]["IktatasDatuma"]);
                }
                else
                {
                    iktatasIdeje = DateTime.MinValue;
                }
            }


            #endregion


            #region Csatolmány feltöltés - Lépés 1: feltöltés

            Logger.Info("Csatolmány feltöltés 1.", execParam);

            DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();

            // KRT_Dokumentumok objektum létrehozása
            KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();
            if (csatolmany.Megnyithato != null)
            {
                krt_Dokumentumok.Megnyithato = csatolmany.Megnyithato;
            }
            if (csatolmany.Olvashato != null)
            {
                krt_Dokumentumok.Olvashato = csatolmany.Olvashato;
            }
            if (csatolmany.ElektronikusAlairas != null)
            {
                krt_Dokumentumok.ElektronikusAlairas = csatolmany.ElektronikusAlairas;
            }
            ExecParam execParam_upload = execParam.Clone();

            //  Iktatokonyv adatok:

            ExecParam execParamIktk = execParam.Clone();
            execParamIktk.Record_Id = ugyirat.IraIktatokonyv_Id;

            EREC_IraIktatoKonyvekService iktkServ = new EREC_IraIktatoKonyvekService();

            Logger.Debug(String.Format("iktkServ.Get elott."));
            Result resultIktatokonyvSelect = iktkServ.Get(execParamIktk);

            if (!String.IsNullOrEmpty(resultIktatokonyvSelect.ErrorCode))
            {
                Logger.Error(String.Format("EREC_IraIratokService.IktatasWordbol iktatokonyv select hiba.\nErrCode: {0}\nErrMsg: {1}\nKeresett id: {2}", resultIktatokonyvSelect.ErrorCode, resultIktatokonyvSelect.ErrorMessage, ugyirat.IraIktatokonyv_Id));
                //hiba:
                throw new ResultException(resultIktatokonyvSelect);
            }

            if (resultIktatokonyvSelect.Record == null)
            {
                Logger.Error(String.Format("EREC_IraIratokService.IktatasWordbol iktatokonyv select: ures a kapott record property. Keresett id: {0}", ugyirat.IraIktatokonyv_Id));
                //hiba:
                resultIktatokonyvSelect.ErrorCode = "IKTKS0001";
                resultIktatokonyvSelect.ErrorMessage = "Hiba az iktatókönyv keresésénél! Üres a visszakapott eredmény record property.";
                throw new ResultException(resultIktatokonyvSelect);
            }

            EREC_IraIktatoKonyvek iktatokonyv = (EREC_IraIktatoKonyvek)resultIktatokonyvSelect.Record;

            // Upload webservicenek átadandó adatok aláírás esetén xml-ben:
            string uploadXmlStrParams_alairasResz = "";
            if (csatolmany.AlairasAdatok.CsatolmanyAlairando)
            {
                uploadXmlStrParams_alairasResz = csatolmany.AlairasAdatok.GetInnerXml();
            }

            //  Az uploadnak xml strben atadott parameterek:
            string uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                        "<iktatokonyv>{1}</iktatokonyv>" +
                                                        "<sourceSharePath></sourceSharePath>" +
                                                        "<foszam>{0}</foszam>" +
                                                        "<alszam>{11}</alszam>" +
                                                        "<kivalasztottIratId>{2}</kivalasztottIratId>" +
                                                        "<docmetaDokumentumId>{3}</docmetaDokumentumId>" +
                                                        "<megjegyzes>{4}</megjegyzes>" +
                                                        "<munkaanyag>{5}</munkaanyag>" +
                                                        "<ujirat>{6}</ujirat>" +
                                                        "<vonalkod>{7}</vonalkod>" +
                                                        "<docmetaIratId>{8}</docmetaIratId>" +
                                                        "<ujKrtDokBejegyzesKell>{9}</ujKrtDokBejegyzesKell>" +
                                                        "<tartalomSHA1>{10}</tartalomSHA1>" +
                                                        "<ev>{12}</ev>" +
                                                        uploadXmlStrParams_alairasResz +
                                                    "</uploadparameterek>"
                                                    // BLG_292
                                                    //, Contentum.eRecord.BaseUtility.Ugyiratok.GetFoszamString(ugyirat)
                                                    , Contentum.eRecord.BaseUtility.Ugyiratok.GetFoszamString(ugyirat)
                                                    , iktatokonyv.Iktatohely
                                                    , isUjirat ? String.Empty : kivalasztottIratId
                                                    , docmetaDokumentumId
                                                    , megjegyzes
                                                    , munkaanyag
                                                    , ujirat
                                                    , vonalkod
                                                    , docmetaIratId
                                                    , ((ujKrtDokBejegyzesKell) ? "IGEN" : "NEM")
                                                    , wordTartalomSHA1
                                                    , isUjirat ? Contentum.eRecord.BaseUtility.Iratok.GetAlszamString(iratObj) : Contentum.eRecord.BaseUtility.Iratok.GetAlszamString(erec_IraIrat)
                                                    , iktatokonyv.Ev
                                                    );


            Result result_upload = documentService.UploadFromeRecordWithCTT(
                   execParam_upload
                   , Contentum.eUtility.Rendszerparameterek.Get(execParam_upload, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                   , csatolmany.Nev
                   , csatolmany.Tartalom
                   , krt_Dokumentumok
                   , uploadXmlStrParams
                );
            if (!String.IsNullOrEmpty(result_upload.ErrorCode))
            {
                //hiba:
                throw new ResultException(result_upload);
            }

            csatolmanyFeltoltve = true;

            // .Uid-ban jön vissza a KRT_Dokumentumok.Id

            string krt_dokumentumId = result_upload.Uid;

            //  Visszakapott egyeb adatok XMLbe toltese

            XmlDocument feltoltesbolKapottXmlDoc = new XmlDocument();
            try
            {
                feltoltesbolKapottXmlDoc.LoadXml((string)result_upload.Record);
            }
            catch (Exception exc1)
            {
                Logger.Error(String.Format("Feltoltesbol kapott xml string konvertalasa xml docca hiba.\nMessage: {0}\nStackTrace: {1}\nXmlString: {2}", exc1.Message, exc1.StackTrace, (string)result_upload.Record));
                result_upload = new Result();
                result_upload.ErrorCode = "UPLDXMLC0001";
                result_upload.ErrorMessage = String.Format("Feltoltesbol kapott xml string konvertalasa xml docca hiba!\nMessage: {0}\nStackTrace: {1}", exc1.Message, exc1.StackTrace);
                throw new ResultException(result_upload);
            }

            // az ujKrtDokBejegyzesKell megvizslatasa

            try
            {
                Logger.Debug(String.Format("Az upload elotti ujKrtDokBejegyzesKell: {0}", ujKrtDokBejegyzesKell));
                XmlNode nd = feltoltesbolKapottXmlDoc.SelectSingleNode("//ujKrtDokBejegyzesKell");
                ujKrtDokBejegyzesKell = Convert.ToString(nd.InnerText).Equals("IGEN") ? true : false;
                Logger.Debug(String.Format("Az upload utan visszakapott ujKrtDokBejegyzesKell: {0}", ujKrtDokBejegyzesKell));
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba a csatolas utan visszakapott ujKrtDokBejegyzesKell parameter vizsgalatakor! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Result ret = new Result();
                ret.ErrorCode = "AFUPL00001";
                ret.ErrorMessage = String.Format("Hiba a csatolas utan visszakapott ujKrtDokBejegyzesKell parameter vizsgalatakor! Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
                throw new ResultException(ret);
            }

            #endregion

            #region Csatolmány feltöltés. Lépés 2: a feltöltött file checkin.

            // itt mar bele kell rakni az irat_id-t a parameterekbe
            // ezt rakjuk be metaba az sps-be

            Logger.Debug(String.Format("result_iktatas.Uid: {0} ;  kivalasztottIratId: {1}", (result_iktatas == null) ? "null" : result_iktatas.Uid, kivalasztottIratId));
            string iratId = (result_iktatas != null) ? result_iktatas.Uid : kivalasztottIratId;

            try
            {
                Logger.Debug(String.Format("feltoltesbolKapottXmlDoc irat_id ({0}) betoltese.", iratId));
                XmlNode p = feltoltesbolKapottXmlDoc.SelectSingleNode("uploadparameterek");
                iratIdNode = feltoltesbolKapottXmlDoc.CreateNode(XmlNodeType.Element, "irat_id", "");
                iratIdNode.InnerText = iratId;
                p.AppendChild(iratIdNode);
            }
            catch (Exception exc)
            {
                Logger.Error(String.Format("irat id hozzaadasa az uploadbol kapott xml adatokhoz\nMessage: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
                throw new ResultException("Xml hiba.");
            }

            try
            {
                if (metaadatokNode != null)
                {
                    if (!String.IsNullOrEmpty(metaadatokNode.OuterXml))
                    {
                        Logger.Debug(String.Format("feltoltesbolKapottXmlDoc metaadatokNode ({0}) betoltese.", metaadatokNode.OuterXml.ToString()));
                        XmlNode p = feltoltesbolKapottXmlDoc.SelectSingleNode("uploadparameterek");
                        XmlNode metak = feltoltesbolKapottXmlDoc.CreateNode(XmlNodeType.Element, "metaadatok", "");
                        metak.InnerXml = metaadatokNode.InnerXml;
                        //Logger.Debug(String.Format("metak.InnerText: {0}\n metaadatokNode.InnerText: {1}", metak.InnerText, metaadatokNode.InnerText));
                        p.AppendChild(metak);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("metaadatokNode xml hozzaadasa az uploadbol kapott xml adatokhoz\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                throw new ResultException("Xml hiba. (2)");
            }

            Logger.Debug(String.Format("CheckInUploadedFileWithCTT elott."));

            result_upload = documentService.CheckInUploadedFileWithCTT(
                   execParam_upload
                   , feltoltesbolKapottXmlDoc.OuterXml.ToString()
                );

            if (!String.IsNullOrEmpty(result_upload.ErrorCode))
            {
                //hiba:
                Logger.Error(String.Format("CheckInUploadedFileWithCTT error!!!"));
                throw new ResultException(result_upload);
            }

            Logger.Debug(String.Format("CheckInUploadedFileWithCTT ok."));

            #endregion

            #region Kapott alszám ellenőrzése

            if ("IGEN".Equals(ujirat) && "NEM".Equals(munkaanyag))
            {

                ExecParam execParam_IratGet = execParam.Clone();
                execParam_IratGet.Record_Id = iratId;

                Result result_iratGet = this.Get(execParam_IratGet);
                if (!String.IsNullOrEmpty(result_iratGet.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_iratGet);
                }

                if (megadottAlszam != Int32.Parse(((EREC_IraIratok)result_iratGet.Record).Alszam))
                {
                    // hiba:
                    Logger.Error("Az iktatás során kapott alszám nem egyezik az előre megadottal.", execParam);
                    throw new ResultException("Az iktatás során kapott alszám nem egyezik az előre megadottal.");
                }
            }

            #endregion

            if ("IGEN".Equals(ujirat) || ujKrtDokBejegyzesKell)
            {
                #region Csatolmány hozzákötése az irathoz

                Logger.Debug("Csatolmány hozzákötése az irathoz", execParam);

                EREC_CsatolmanyokService service_csatolmanyok = new EREC_CsatolmanyokService(this.dataContext);

                EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
                erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.DokumentumMelleklet;
                erec_Csatolmanyok.Updated.DokumentumSzerep = true;

                //Ha bevan állitva a rendszerparaméter akkor ha az uj dokumentummal azonos nevü fődokumentum
                //van már ami doc akkor abból tervezett lesz az aktuálisbol meg fődokumentum
                Result handleAzonosNevuWorldFodokumentumResult = Contentum.eRecord.BaseUtility.CsatolmanyHelper.HandleAzonosNevuWordFodokumentum(execParam, erec_Csatolmanyok, csatolmany.Nev, iratId);
                if (handleAzonosNevuWorldFodokumentumResult != null)
                {
                    Logger.Error("Nem sikerült az azonos nevű csatolmány szerepének megváltoztatása", execParam, handleAzonosNevuWorldFodokumentumResult);
                    throw new ResultException(handleAzonosNevuWorldFodokumentumResult);
                }
                erec_Csatolmanyok.IraIrat_Id = iratId;
                erec_Csatolmanyok.Updated.IraIrat_Id = true;

                erec_Csatolmanyok.Dokumentum_Id = krt_dokumentumId;
                erec_Csatolmanyok.Updated.Dokumentum_Id = true;

                if ("IGEN".Equals(ujirat))
                {
                    erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
                }
                
                else
                {
                    //erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.DokumentumMelleklet;
                    //// BUG_2600
                    EREC_CsatolmanyokSearch erec_csatolmanyokSearch = new EREC_CsatolmanyokSearch();
                    erec_csatolmanyokSearch.IraIrat_Id.Value = iratId;
                    erec_csatolmanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                    Result result_Csatolmanyok = service_csatolmanyok.GetAll(execParam.Clone(), erec_csatolmanyokSearch);

                    if (String.IsNullOrEmpty(result_Csatolmanyok.ErrorCode))
                    {
                        if (result_Csatolmanyok.Ds.Tables[0].Rows.Count == 0)
                        {
                            erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
                        }
                    }
                }
                erec_Csatolmanyok.Updated.DokumentumSzerep = true;
                
                ExecParam execParam_iratDok = execParam.Clone();
                Result result_iratDokInsert = service_csatolmanyok.Insert(execParam_iratDok, erec_Csatolmanyok);
                if (!String.IsNullOrEmpty(result_iratDokInsert.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_iratDokInsert);
                }

                #endregion
            }


            #region Ha nem uj (tehat csak csatolas), az alapveto mezok valtozasanak ellenorzese (KOMMENTBEN VAN!)

            bool alairotFelKellVenni = false;

            //if (ujirat.Equals("NEM"))
            //{
            //    #region  ha valtozott a targy
            //    Logger.Debug("Ha nem uj (tehat csak csatolas), az alapveto mezok valtozasanak ellenorzese - ha valtozott a targy");

            //    if (!erec_IraIrat.Targy.Trim().Equals(targy.Trim()))
            //    {
            //        erec_IraIrat.Updated.SetValueAll(false);
            //        erec_IraIrat.Base.Updated.SetValueAll(false);

            //        erec_IraIrat.Targy = targy;
            //        erec_IraIrat.Updated.Targy = true;

            //        erec_IraIrat.Base.Updated.Ver = true;

            //        ExecParam execp = new ExecParam();
            //        execp.Alkalmazas_Id = execParam.Alkalmazas_Id;
            //        execp.Felhasznalo_Id = execParam.Felhasznalo_Id;
            //        execp.FelhasznaloSzervezet_Id = execParam.FelhasznaloSzervezet_Id;
            //        execp.Record_Id = erec_IraIrat.Id;

            //        EREC_IraIratokService service_Iratok = new EREC_IraIratokService(this.dataContext);
            //        Result resultIratUpd = service_Iratok.Update(execp, erec_IraIrat);

            //        if (!String.IsNullOrEmpty(resultIratUpd.ErrorCode))
            //        {
            //            Logger.Error(String.Format("Irat targy update hiba!\nErrocode: {0}\nErrorMessage: {1}", resultIratUpd.ErrorCode, resultIratUpd.ErrorMessage));
            //            throw new ResultException(resultIratUpd);
            //        }
            //    }

            //    #endregion

            //    #region az 1-es peldany megszerzese. Ehhez nezzuk a cimzett valtozast es ide vezetjuk at ha kell.
            //    Logger.Debug("Az 1-es peldany megszerzese resz kezdodik.");

            //    EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
            //    EREC_PldIratPeldanyokSearch pldSearch = new EREC_PldIratPeldanyokSearch();

            //    pldSearch.IraIrat_Id.Value = erec_IraIrat.Id;
            //    pldSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            //    pldSearch.Sorszam.Value = "1";
            //    pldSearch.Sorszam.Operator = Contentum.eQuery.Query.Operators.equals;

            //    ExecParam execParam_iratAlairas = execParam.Clone();
            //    Result resultPldIratPld1 = erec_PldIratPeldanyokService.GetAll(execParam_iratAlairas, pldSearch);

            //    if (!String.IsNullOrEmpty(resultPldIratPld1.ErrorCode))
            //    {
            //        Logger.Error(String.Format("Hiba az egyes iratpeldany lekerdezesekor. ErrCode: {0}\nErrMsg: {1}", resultPldIratPld1.ErrorCode, resultPldIratPld1.ErrorMessage));
            //        throw new ResultException(resultPldIratPld1);
            //    }

            //    if (resultPldIratPld1.Ds.Tables[0].Rows.Count == 0)
            //    {
            //        Logger.Error(String.Format("Hiba az egyes iratpeldany lekerdezesekor. Nincsen eredmenysor!"));
            //        throw new ResultException("Hiba az egyes iratpeldany lekerdezesekor. Nincsen eredmenysor!");
            //    }

            //    if (resultPldIratPld1.Ds.Tables[0].Rows.Count > 1)
            //    {
            //        Logger.Error(String.Format("Hiba az egyes iratpeldany lekerdezesekor.Tul sok eredmenysor!"));
            //        throw new ResultException("Hiba az egyes iratpeldany lekerdezesekor. Túl sok eredmenysor!");
            //    }

            //    #endregion

            //    #region ha valtozott a cimzett
            //    Logger.Debug("Ha nem uj (tehat csak csatolas), az alapveto mezok valtozasanak ellenorzese - ha valtozott a cimzett");

            //    /// TODO: peldanyba

            //    #endregion

            //    #region ha valtozott a cimzett cime 
            //    Logger.Debug("Ha nem uj (tehat csak csatolas), az alapveto mezok valtozasanak ellenorzese - ha valtozott a cimzett cime");

            //    /// TODO: peldanyba

            //    #endregion

            #endregion

            if (ujirat.Equals("IGEN") || alairotFelKellVenni)
            {
                #region Aláírás berögzítése, ha volt megadva aláíró

                if (alairokNode.SelectNodes("//alairo").Count != 0)
                {
                    Logger.Info("Aláírás berögzítése az irathoz...", execParam);

                    EREC_IratAlairokService service_iratAlairok = new EREC_IratAlairokService(this.dataContext);

                    XmlNodeList ndListAlairo = alairokNode.SelectNodes("//alairo");

                    Logger.Debug(String.Format("Alairok szama: {0}", ndListAlairo.Count));

                    int i = 0;

                    while (i < ndListAlairo.Count)
                    {

                        Logger.Debug(String.Format("Most jon: {0}/{1}", ndListAlairo.Count, (i + 1)));

                        XmlNode ndAlairo = ndListAlairo[i];

                        Logger.Debug(String.Format("itt jar 1"));

                        String alairoId = String.Empty;
                        String alairoSzerep = String.Empty;

                        try
                        {
                            alairoId = Convert.ToString(ndAlairo.SelectSingleNode("id").InnerText);
                        }
                        catch (Exception excc)
                        {
                            Logger.Error(String.Format("Hiba az alairok kezelesenel: az id node selectnel! Message: {0}\nStackTrace: {1}", excc.Message, excc.StackTrace));
                            Result error_result = new Result();
                            error_result.ErrorCode = "ALAH00001";
                            error_result.ErrorMessage = String.Format("Hiba az alairok kezelesenel: az id node selectnel! Message: {0}\nStackTrace: {1}", excc.Message, excc.StackTrace);
                            throw new ResultException(error_result);
                        }

                        Logger.Debug(String.Format("alairoId: {0}", alairoId));

                        try
                        {
                            alairoSzerep = Convert.ToString(ndAlairo.SelectSingleNode("szerepkor").InnerText);
                        }
                        catch (Exception excc)
                        {
                            Logger.Error(String.Format("Hiba az alairok kezelesenel: az szerepkor node selectnel! Message: {0}\nStackTrace: {1}", excc.Message, excc.StackTrace));
                            Result error_result = new Result();
                            error_result.ErrorCode = "ALAH00002";
                            error_result.ErrorMessage = String.Format("Hiba az alairok kezelesenel: az szerepkor node selectnel! Message: {0}\nStackTrace: {1}", excc.Message, excc.StackTrace);
                            throw new ResultException(error_result);
                        }

                        Logger.Debug(String.Format("alairoSzerep: {0}", alairoSzerep));

                        EREC_IratAlairok iratAlairok = new EREC_IratAlairok();

                        //iratAlairok.AlairasDatuma = DateTime.Now.ToString();
                        //iratAlairok.Updated.AlairasDatuma = true;

                        //  Dani szerint a Lattamozo legyen az alapertelmezett (2008.06.16)
                        //  Jottem, lattamoztam, gyoztem! :)
                        Logger.Debug(String.Format("AlairoSzerep beallitasa elott."));
                        iratAlairok.AlairoSzerep = (String.IsNullOrEmpty(alairoSzerep)) ? KodTarak.ALAIRO_SZEREP.Lattamozo : alairoSzerep;
                        iratAlairok.Updated.AlairoSzerep = true;

                        Logger.Debug(String.Format("FelhasznaloCsoport_Id_Alairo beallitasa elott."));
                        iratAlairok.FelhasznaloCsoport_Id_Alairo = alairoId;
                        iratAlairok.Updated.FelhasznaloCsoport_Id_Alairo = true;

                        Logger.Debug(String.Format("Obj_Id beallitasa elott."));
                        iratAlairok.Obj_Id = iratId;
                        iratAlairok.Updated.Obj_Id = true;

                        iratAlairok.AlairasSorrend = (i + 1).ToString();
                        iratAlairok.Updated.AlairasSorrend = true;

                        #region AlairasSzabaly_Id meghatározása (sp_PKI_IratAlairas futtatásával)

                        AlairasokService alairasokService = new AlairasokService(this.dataContext);

                        string folyamatKod = Contentum.eUtility.Constants.FolyamatKodok.IratJovahagyas;
                        string muveletKod = Contentum.eUtility.Constants.DokumentumMuveletKodok.BelsoIratJovahagyas;
                        string objektumTip = "Irat";

                        Logger.Debug("alairasokService.GetAll_PKI_IratAlairas Start...", execParam);

                        Result result_GetAll_PKI_IratAlairas =
                            alairasokService.GetAll_PKI_IratAlairas(execParam.Clone(), String.Empty, objektumTip, iratId
                            , folyamatKod, muveletKod, iratAlairok.AlairoSzerep);
                        if (result_GetAll_PKI_IratAlairas.IsError)
                        {
                            // hiba:
                            // Ideiglenesen egyelőre: nem dobjuk tovább a hibát
                            Logger.Error("Hiba az alairasokService.GetAll_PKI_IratAlairas -ban: ", execParam, result_GetAll_PKI_IratAlairas);
                            // CR3441 
                            // visszarakva
                            throw new ResultException(result_GetAll_PKI_IratAlairas);
                        }
                        else
                        {
                            try
                            {
                                if (result_GetAll_PKI_IratAlairas.Ds != null
                                    && result_GetAll_PKI_IratAlairas.Ds.Tables[0].Rows.Count > 0)
                                {
                                    string alairasSzabaly_Id = result_GetAll_PKI_IratAlairas.Ds.Tables[0].Rows[0]["AlairasSzabaly_Id"].ToString();

                                    iratAlairok.AlairasSzabaly_Id = alairasSzabaly_Id;
                                    iratAlairok.Updated.AlairasSzabaly_Id = true;
                                }
                            }
                            catch (Exception e)
                            {
                                Logger.Error("Hiba az alairasokService.GetAll_PKI_IratAlairas eredményének feldolgozása során...", e);
                            }
                        }

                        #endregion

                        Logger.Debug(String.Format("ExecParam elott."));

                        ExecParam execParam_iratAlairas = execParam.Clone();

                        Logger.Debug(String.Format("service_iratAlairok.Insert elott."));

                        Result result_iratAlairas = service_iratAlairok.Insert(execParam_iratAlairas, iratAlairok);
                        if (!String.IsNullOrEmpty(result_iratAlairas.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_iratAlairas);
                        }

                        i++;
                    } //while i
                }

                #endregion
            }

            // A feltöltött dokumentum URL-jével térünk vissza (amit elvileg visszakapunk a fájlfeltöltés eredményeként)      
            result.Record = result_upload.Record;

            //
            //  record kiegeszitese a visszaadando iktatoszammal

            Logger.Debug("record kiegeszitese a visszaadando iktatoszammal és a kapot irat id-val");

            try
            {
                XmlDocument xmldd = new XmlDocument();
                xmldd.LoadXml((String)result.Record);

                XmlNode ndIktszam = xmldd.CreateNode(XmlNodeType.Element, "iktatoszam", null);
                ndIktszam.InnerText = visszaIktatoszam;

                xmldd.FirstChild.AppendChild(ndIktszam);

                XmlNode ndIratId = xmldd.CreateNode(XmlNodeType.Element, "iratId", null);
                ndIratId.InnerText = iratId;

                xmldd.FirstChild.AppendChild(ndIratId);
                Logger.Debug(String.Format("visszaadando xml: {0}", xmldd.OuterXml));

                result.Record = Convert.ToString(xmldd.OuterXml);
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Hiba a visszaadando xml iktatoszammal valo bovitesenel!\nMessage: {0}\nStackTrace: {1}\nNem fog szerepelni az iktatoszam a visszaadott ertekek kozott!", ex.Message, ex.StackTrace));
            }

            //result = result_iktatas;

            #region Meta adatok feltoltese eRecordba

            Logger.Debug(String.Format("--- Meta adatok feltoltese eRecordba"));

            //if (metaadatokNode != null)
            //{
            //    int ndMetaadatokCount = metaadatokNode.SelectNodes("//metaadat").Count;

            //    Logger.Debug(String.Format("cttNeve: {0} - ujirat: {1} - ndMetaadatokCount: {2}", cttNeve, ujirat, ndMetaadatokCount));

            //    if (ndMetaadatokCount != 0 && !String.IsNullOrEmpty(cttNeve))
            //    {
            //        Logger.Debug(String.Format("meta adat szinkronizacio indul"));

            //        char[] sepa = { '_' };
            //        String[] sablonAzonositoParts = cttNeve.Trim().Split(sepa); ;

            //        Logger.Debug(String.Format("MergeWordMetaByContentType meghivasa elott."));
            //        Logger.Debug(String.Format("atadott metaadatok: {0}", metaadatokNode.OuterXml));
            //        Logger.Debug(String.Format("irat id: {0}", kivalasztottIratId));
            //        if (sablonAzonositoParts.Length > 1)
            //            Logger.Debug(String.Format("parameterkent kapott sablonAzonosito: {0} - MergeWordMetaByContentType-nek paramkent atadott sablonazonosito: {1}", cttNeve, sablonAzonositoParts[0]));
            //        else
            //            Logger.Debug(String.Format("parameterkent kapott sablonAzonosito: {0}", cttNeve));

            //        EREC_ObjektumTargyszavaiService ots = new EREC_ObjektumTargyszavaiService(this.dataContext);

            //        //ots.Timeout = 3 * 60 * 100;

            //        XmlDocument xdd = new XmlDocument();
            //        try
            //        {
            //            xdd.LoadXml(metaadatokNode.OuterXml);
            //        }
            //        catch (Exception ex22)
            //        {
            //            Logger.Error(String.Format("Hiba az xml string konvertalasakor!\nMessage: {0}\nStackTrace: {1}", ex22.Message, ex22.StackTrace));
            //            Result r2d2 = new Result();
            //            r2d2.ErrorCode = "METAXMLCONV0001";
            //            r2d2.ErrorMessage = String.Format("Hiba a kapott meta adatokat tartalmazó xml string konvertálásakor!\nMessage: {0}\nStackTrace: {1}", ex22.Message, ex22.StackTrace);
            //            return r2d2;
            //        }

            //        Result resultMeta = ots.MergeWordMetaByContentType(execParam.Clone()
            //            , xdd
            //            , kivalasztottIratId
            //            , "EREC_IraIratok"
            //            , sablonAzonositoParts[0]
            //            , false
            //            );

            //        if (!String.IsNullOrEmpty(resultMeta.ErrorCode))
            //        {
            //            Logger.Error(String.Format("Hibat dobott a MergeWordMetaByContentType method!\nErrCode: {0}\nErrMsg: {1}", resultMeta.ErrorCode, resultMeta.ErrorMessage));
            //            throw new ResultException(resultMeta);
            //        }

            //    }
            //    else
            //    {
            //        Logger.Debug(String.Format("nincs meta adat szinkronizacio"));
            //    }

            //}

            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, krt_dokumentumId, "KRT_Dokumentumok", "DokumentumokNewWord").Record;
                eventLogRecord.Munkaallomas = HttpContext.Current.Request.UserHostAddress;
                //
                //  nem egeszen vilagos ezt miert is nem adja at nehany helyen....
                //  az emailnal pl ha ezt hozzaadtuk, akkor mukodott rendesen
                //  itt is hozzaadjuk a biztosag kedveert

                if (String.IsNullOrEmpty(execParam.Felhasznalo_Id))
                {
                    Logger.Error("Ures a ExecParam.Felhasznalo_Id valtozo, ezert nem lesz bejegyezve a DocumentumokNewWord esemeny!");
                }
                else
                {
                    eventLogRecord.Felhasznalo_Id_Login = (execParam.Helyettesites_Id != null) ? execParam.LoginUser_Id : execParam.Felhasznalo_Id;
                }

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            #region KOZTER dokumentum kiegészítés

            Logger.Info("--- KOZTER dokumentum kiegeszites 1");

            if (IsKozterDocument(csatolmany))
            {
                Logger.Info(String.Format("--- KOZTER dokumentum kiegeszites 2: krt_dokumentumId : {0}", krt_dokumentumId));
                Logger.Info(String.Format("GetValueFromResultXml(result.Record.ToString(), 'tortentUjKrtDokBejegyzes'): {0} ", GetValueFromResultXml(result.Record.ToString(), "tortentUjKrtDokBejegyzes")));

                KozterInterfaceHandler ktiHandler = new KozterInterfaceHandler();
                Result resKtiInsert = ktiHandler.Insert(execParam.Clone()
                    , csatolmany
                    , visszaIktatoszam
                    , vonalkod
                    , krt_dokumentumId
                    , iktatasIdeje
                    , GetValueFromResultXml(result.Record.ToString(), "dokumentumFullUrl"));

                if (!String.IsNullOrEmpty(resKtiInsert.ErrorCode))
                {
                    Logger.Error(String.Format("KOZTER insert error: {0}\nMessage: {1}"
                        , resKtiInsert.ErrorCode
                        , resKtiInsert.ErrorMessage));
                    // TODO: ezt a hibát nem lenyelni!!!! üzenni vissza a felhasználónak!!!
                }
            }

            Logger.Info("--- KOZTER dokumentum kiegeszites 3");

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);

            if (csatolmanyFeltoltve == true)
            {
                // TODO: Fájl kiszedése Sharepointból valahogy, mivel azt nem tudjuk visszagörgetni
            }
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    #region kozter document methods - MAJD INNEN KI KELL EMELNI

    //
    // TODO: Ezeket a methodokat ki kell tenni majd az eUtility-be vagy valahova! min 3.0 framework kell hozza!
    //

    #region adatbazis - tabla elemek

    //
    //  web.configban hasznalja a KozterInterfaceConnectionString-et

    private class KozterInterfaceHandler
    {
        private KozterInterfaceStoredProcedure sp = null;

        private DataContext dataContext;

        public KozterInterfaceHandler()
        {
            dataContext = new DataContext(this.KozterInterfaceConnectionString);

            sp = new KozterInterfaceStoredProcedure(dataContext);
        }

        public KozterInterfaceHandler(DataContext _dataContext)
        {
            this.dataContext = _dataContext;
            sp = new KozterInterfaceStoredProcedure(dataContext);
        }

        private string KozterInterfaceConnectionString
        {
            get
            {
                System.Configuration.ConnectionStringSettings connStringSettings = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.KozterInterfaceConnectionString"];

                if (connStringSettings != null)
                {
                    string connectionString = connStringSettings.ConnectionString;

                    return connectionString;
                }

                return String.Empty;
            }
        }

        public Result Insert(ExecParam execParam
            , Csatolmany csatolmany
            , String iktatoszam
            , String vonalkod
            , String krtDokumentumId
            , DateTime iktatasDatuma
            , String documentFullUrl)
        {
            Result retVal = new Result();

            try
            {
                // csak összeszedve milyen adatok kellenek az interface-be
                String ktdok_kod = GetSPSCTTPropertyValue(csatolmany.Tartalom, "edok_w_dokkod");

                Logger.Info(String.Format("--- INSERT DATA TO KOZTER INTERFACE:\nktdok_id: {0}\nktdok_kod: {1}\nktdok_iktatoszam: {2}\nktdok_vonalkod: {3}\nktdok_dokid: {4}\nktdok_date: {5}\ndocumentFullUrl: {6}"
                    , "az insert adja meg"
                    , ktdok_kod
                    , iktatoszam
                    , vonalkod
                    , krtDokumentumId
                    , iktatasDatuma.ToString("yyyy.MM.dd")
                    , documentFullUrl));

                KozterInterface ktiData = new KozterInterface();
                ktiData.ktdok_id = "0";
                ktiData.ktdok_kod = ktdok_kod;
                ktiData.ktdok_iktszam = iktatoszam;
                ktiData.ktdok_vonalkod = vonalkod;
                ktiData.ktdok_dokid = krtDokumentumId;
                ktiData.ktdok_url = documentFullUrl;
                ktiData.ktdok_date = iktatasDatuma.ToString("yyyy.MM.dd");

                Logger.Info("ktiData <- ok");

                retVal = sp.Insert(execParam, ktiData);

                if (!String.IsNullOrEmpty(retVal.ErrorCode))
                {
                    Logger.Error(String.Format("Hiba KOZTER interface insertkor: res.ErrorCode: {0} - res.ErrorMessage: {1}"
                        , retVal.ErrorCode
                        , retVal.ErrorMessage));
                    Logger.Error(String.Format("INSERT DATA TO KOZTER INTERFACE error debug info :\nktdok_id: {0}\nktdok_kod: {1}\nktdok_iktatoszam: {2}\nktdok_vonalkod: {3}\nktdok_dokid: {4}\nktdok_date: {5}\ndocumentFullUrl: {6}"
                        , "az insert adja meg"
                        , ktdok_kod
                        , iktatoszam
                        , vonalkod
                        , krtDokumentumId
                        , iktatasDatuma.ToString("yyyy.MM.dd")
                        , documentFullUrl));
                }

                //ExecParam getExecparam = execParam.Clone();

                //Result gresult = sp.Get(getExecparam, 2);
                //if (!String.IsNullOrEmpty(gresult.ErrorCode))
                //{
                //    Logger.Error(String.Format("GET Error: {0}\nMessage: {1}"
                //        , gresult.ErrorCode
                //        , gresult.ErrorMessage));
                //}

                //if (gresult.Record != null)
                //{
                //    KozterInterface resultData = gresult.Record as KozterInterface;
                //    Logger.Info(String.Format("resultData.ktdok_iktszam: {0}", resultData.ktdok_iktszam));

                //    resultData.ktdok_siker = "I";
                //    Result uresult = sp.Update(execParam.Clone(), resultData, 2);

                //    if (!String.IsNullOrEmpty(uresult.ErrorCode))
                //    {
                //        Logger.Error(String.Format("UPDATE Error: {0}\nMessage: {1}"
                //            , uresult.ErrorCode
                //            , uresult.ErrorMessage));
                //    }
                //}
                //else
                //{
                //    Logger.Error("ures a gresult Rescord property-je!!!");
                //}


            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("Kozter interface insert error: {0}\nSource: {1}\nStackTrace: {2}"
                    , ex.Message
                    , ex.Source
                    , ex.StackTrace));
                retVal.ErrorCode = "KTIINSERT001";
                retVal.ErrorMessage = ex.Message;
            }

            return retVal;
        }

        public Result Get(ExecParam execParam, int id)
        {
            return sp.Get(execParam, id);
        }

        public Result GetAll(ExecParam execParam, String whereByManual)
        {
            return sp.GetAll(execParam, whereByManual);
        }

        private String GetSPSCTTPropertyValue(byte[] fileContent, String propertyName)
        {
            String retVal = String.Empty;
            MemoryStream ms = new MemoryStream();

            try
            {
                ms.Write(fileContent, 0, fileContent.Length);
                ms.Position = 0;

                using (Package wdPackage = Package.Open(ms))
                {
                    IEnumerator parts = wdPackage.GetParts().GetEnumerator();
                    while (parts.MoveNext() && String.IsNullOrEmpty(retVal))
                        if (((PackagePart)parts.Current).Uri.ToString().StartsWith("/customXml/item"))
                            retVal = GetPropertyValueFromPart(((PackagePart)parts.Current), propertyName);
                }

            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }

            ms.Close();
            return retVal;
        }

        private string GetPropertyValueFromPart(PackagePart packagePart, string propertyName)
        {
            String retVal = String.Empty;

            XmlDocument xmlD = new XmlDocument();
            xmlD.Load(packagePart.GetStream());

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(new NameTable());
            nsmgr.AddNamespace("p", "http://schemas.microsoft.com/office/2006/metadata/properties");
            nsmgr.AddNamespace("a", "http://schemas.microsoft.com/sharepoint/v3");

            String xpath = String.Format("//p:properties/documentManagement/a:{0}", propertyName);

            if (xmlD.SelectNodes(xpath, nsmgr).Count == 1)
                retVal = xmlD.SelectNodes(xpath, nsmgr)[0].InnerText;

            return retVal;
        }

    }

    #endregion adatbazis - tabla elemek

    #region iktatashoz hasznalt methodok

    private string GetValueFromResultXml(String xmlStr, string paramNodeName)
    {
        String retVal = String.Empty;

        XmlDocument xmlD = new XmlDocument();
        xmlD.LoadXml(xmlStr);

        if (xmlD.SelectNodes(String.Format("//{0}", paramNodeName)).Count == 1)
            retVal = xmlD.SelectNodes(String.Format("//{0}", paramNodeName))[0].InnerText;

        return retVal;
    }

    #region GetSPSCTTId

    private string GetSPSCTTId(byte[] fileContent)
    {
        String retVal = String.Empty;
        MemoryStream ms = new MemoryStream();

        try
        {
            ms.Write(fileContent, 0, fileContent.Length);
            ms.Position = 0;

            using (Package wdPackage = Package.Open(ms))
            {
                IEnumerator parts = wdPackage.GetParts().GetEnumerator();
                while (parts.MoveNext() && String.IsNullOrEmpty(retVal))
                    if (((PackagePart)parts.Current).Uri.ToString().StartsWith("/customXml/item"))
                        retVal = GetCTTValueFromPart(((PackagePart)parts.Current));
            }

        }
        catch (Exception ex)
        {
            Logger.Error(ex.Message);
        }

        ms.Close();
        return retVal;
    }

    private string GetCTTValueFromPart(PackagePart packagePart)
    {
        String retVal = String.Empty;

        XmlDocument xmlD = new XmlDocument();
        xmlD.Load(packagePart.GetStream());

        XmlNodeList ndListContentTypeSchema = xmlD.GetElementsByTagName("ct:contentTypeSchema");
        if (ndListContentTypeSchema.Count == 1)
            retVal = GetNodeAttribValue(ndListContentTypeSchema[0], "ma:contentTypeID");

        return retVal;
    }

    private string GetNodeAttribValue(XmlNode xmlNode, String attribName)
    {
        String retVal = String.Empty;

        for (int i = 0; i < xmlNode.Attributes.Count && String.IsNullOrEmpty(retVal); i++)
            if (attribName.Equals(xmlNode.Attributes[i].Name))
                retVal = xmlNode.Attributes[i].Value;

        return retVal;
    }

    #endregion GetSPSCTTId

    private bool IsKozterDocument(Csatolmany csatolmany)
    {
        bool retVal = false;
        //if ("0x0101003685BE14D0DA486792C1E617240A97220103".Equals(GetSPSCTTId(csatolmany.Tartalom)))
        if (GetSPSCTTId(csatolmany.Tartalom).StartsWith("0x0101003685BE14D0DA486792C1E617240A97220103"))
            retVal = true;
        return retVal;
    }

    #endregion iktatashoz hasznalt methodok

    #endregion kozter document methods - MAJD INNEN KI KELL EMELNI

    #endregion

    #region Munkaanyag létrehozás/beiktatás (Iktatás előkészítés)


    /// <summary>
    /// Munkaanyag (még nem beiktatott irat) létrehozása a megadott ügyiratban
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <param name="erec_IraIratok"></param>
    /// <param name="erec_PldIratPeldanyok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result IktatasElokeszites(ExecParam execParam, string ugyiratId, EREC_IraIratok erec_IraIratok, EREC_PldIratPeldanyok erec_PldIratPeldanyok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            IktatasiParameterek ip = new IktatasiParameterek();

            result = IratIktatasa(IktatasTipus.BelsoIratIktatas, NormalVagyAlszamraIktatas.AlszamraIktatas, IktatottVagyMunkaanyag.Munkaanyag,
                            execParam, null, ugyiratId, null, null, erec_IraIratok, null, erec_PldIratPeldanyok, ip);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyiratindító munkaanyag létrehozása a megadott iktatókönyvben
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iktatokonyvId"></param>
    /// <param name="erec_Ugyiratok"></param>
    /// <param name="erec_IraIratok"></param>
    /// <param name="erec_PldIratPeldanyok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result IktatasElokeszites_UgyiratInditoMunkaanyag(ExecParam execParam, string iktatokonyvId,
        EREC_UgyUgyiratok erec_Ugyiratok, EREC_IraIratok erec_IraIratok, EREC_PldIratPeldanyok erec_PldIratPeldanyok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            IktatasiParameterek ip = new IktatasiParameterek();

            // Kezelő kötelező, ha nincs kitöltve, beírjuk a felhasználót:
            if (erec_Ugyiratok != null && String.IsNullOrEmpty(erec_Ugyiratok.Csoport_Id_Felelos))
            {
                erec_Ugyiratok.Csoport_Id_Felelos = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
                erec_Ugyiratok.Updated.Csoport_Id_Felelos = true;
            }

            result = IratIktatasa(IktatasTipus.BelsoIratIktatas, NormalVagyAlszamraIktatas.NormalIktatas, IktatottVagyMunkaanyag.Munkaanyag,
                            execParam, iktatokonyvId, null, erec_Ugyiratok, null, erec_IraIratok, null, erec_PldIratPeldanyok, ip);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }




    bool isKiadmanyozas = false;
    /// <summary>
    /// Iktatásra előkészített irat (munkapéldány) beiktatása --> Rendelünk hozzá alszámot
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <param name="iratAdatok"></param>
    /// <param name="iratPeldanyAdatok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result MunkapeldanyBeiktatasa(ExecParam execParam, string iratId, EREC_IraIratok iratAdatok, EREC_PldIratPeldanyok iratPeldanyAdatok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        #region Paraméterek ellenőrzése

        if (execParam == null
            || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(iratId)
            || iratAdatok == null
            || iratPeldanyAdatok == null)
        {
            // hiba:
            return ResultError.CreateNewResultWithErrorCode(52130);
        }

        #endregion

        Logger.Info("Munkapéldány beiktatása - Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            /**
             * - Ügyirat ellenőrzése
             * - köv. alszám meghatározása
             * - ügyirat Update
             * - irat Update
             * - iratpéldány update
             */

            #region Irat hier. lekérése

            ExecParam execParam_iratHier = execParam.Clone();
            execParam_iratHier.Record_Id = iratId;

            Result result_iratHier = this.GetIratHierarchia(execParam_iratHier);
            if (!String.IsNullOrEmpty(result_iratHier.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_iratHier);
            }

            IratHierarchia iratHier = (IratHierarchia)result_iratHier.Record;

            EREC_UgyUgyiratok ugyirat = iratHier.UgyiratObj;

            #endregion

            bool ugyiratIsMunkapeldany = false;
            // az ügyirat munkaanyag-e?
            if (Contentum.eRecord.BaseUtility.Ugyiratok.Munkaanyag(ugyirat))
            {
                ugyiratIsMunkapeldany = true;
            }

            #region Iktathatóság ellenőrzése

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(ugyirat);
            Contentum.eRecord.BaseUtility.Iratok.Statusz iratStatusz = Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(iratHier.IratObj);
            ErrorDetails errorDetail = null;
            ExecParam xpm = execParam.Clone();
            // Alszámra iktathatóságot nem kell ellenőrizni, ha az ügyirat is munkapéldány:
            if (ugyiratIsMunkapeldany == false)
            {
                if (Contentum.eRecord.BaseUtility.Ugyiratok.LehetAlszamraIktatni(xpm, ugyiratStatusz, out errorDetail) == false)
                {
                    // hiba:
                    Logger.Warn("Az ügyiratba nem lehet iktatni!", execParam);
                    throw new ResultException(52131, errorDetail);
                }
            }

            // Ha nem munkaanyag az irat, hiba:
            //if (Contentum.eRecord.BaseUtility.Iratok.Munkaanyag(iratHier.IratObj) == false)
            // Ha nem iktatható be a munkairat:
            if (Contentum.eRecord.BaseUtility.Iratok.BeiktathatoAMunkairat(iratStatusz, out errorDetail) == false)
            {
                // hiba: A munkairat nem iktatható be!
                throw new ResultException(52138, errorDetail);
            }

            #endregion

            ugyirat.Updated.SetValueAll(false);
            ugyirat.Base.Updated.SetValueAll(false);
            ugyirat.Base.Updated.Ver = true;

            #region Iktatókönyv GET

            EREC_IraIktatoKonyvekService service_iktKonyv = new EREC_IraIktatoKonyvekService(this.dataContext);
            ExecParam execParam_iktKonyvGet = execParam.Clone();
            execParam_iktKonyvGet.Record_Id = ugyirat.IraIktatokonyv_Id;

            Result result_iktKonyvGet = service_iktKonyv.Get(execParam_iktKonyvGet);
            if (!String.IsNullOrEmpty(result_iktKonyvGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_iktKonyvGet);
            }

            EREC_IraIktatoKonyvek iktatoKonyv = (EREC_IraIktatoKonyvek)result_iktKonyvGet.Record;

            #endregion

            if (ugyiratIsMunkapeldany)
            {
                // Főszám az ügyiratnak (+ügyiratdarabnak), iktatókönyv utolsó főszám módosítása

                int ujFoszam = Int32.Parse(iktatoKonyv.UtolsoFoszam) + 1;

                #region Iktatókönyv update

                iktatoKonyv.Updated.SetValueAll(false);
                iktatoKonyv.Base.Updated.SetValueAll(false);
                iktatoKonyv.Base.Updated.Ver = true;

                iktatoKonyv.UtolsoFoszam = ujFoszam.ToString();
                iktatoKonyv.Updated.UtolsoFoszam = true;

                ExecParam execParam_iktKonyvUpdate = execParam.Clone();
                execParam_iktKonyvUpdate.Record_Id = iktatoKonyv.Id;

                Result result_iktKonyvUpdate = service_iktKonyv.Update(execParam_iktKonyvUpdate, iktatoKonyv);
                if (!String.IsNullOrEmpty(result_iktKonyvUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_iktKonyvUpdate);
                }

                #endregion

                // Ügyirat főszám, állapot beállítása:
                ugyirat.Foszam = ujFoszam.ToString();
                ugyirat.Updated.Foszam = true;

                if (ugyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt)
                {
                    ugyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Iktatott;
                    ugyirat.Updated.TovabbitasAlattAllapot = true;
                }
                else
                {
                    ugyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Iktatott;
                    ugyirat.Updated.Allapot = true;

                    // továbbítás alatt állapot törlése biztonság kedvéért
                    ugyirat.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                    ugyirat.Updated.TovabbitasAlattAllapot = true;
                }

                #region CR 2105 fix; Ugyiratdarab update, főszám beállítása

                EREC_UgyUgyiratdarabok ugyiratDarab = iratHier.UgyiratDarabObj;

                if (ugyiratDarab != null)
                {
                    ugyiratDarab.Updated.SetValueAll(false);
                    ugyiratDarab.Base.Updated.SetValueAll(false);
                    ugyiratDarab.Base.Updated.Ver = true;

                    ugyiratDarab.Foszam = ugyirat.Foszam;
                    ugyiratDarab.Updated.Foszam = true;

                    EREC_UgyUgyiratdarabokService svcUgyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);
                    ExecParam xpmUgyiratDarabUpdate = execParam.Clone();
                    xpmUgyiratDarabUpdate.Record_Id = ugyiratDarab.Id;
                    Result resUgyiratDarabUpdate = svcUgyiratDarabok.Update(xpmUgyiratDarabUpdate, ugyiratDarab);

                    if (resUgyiratDarabUpdate.IsError)
                    {
                        Logger.Error("UgyiratDarab update hiba", xpmUgyiratDarabUpdate, resUgyiratDarabUpdate);
                        throw new ResultException(resUgyiratDarabUpdate);
                    }
                }

                #endregion
            }

            // Alszám meghatározása
            //int ujAlszam = Int32.Parse(ugyirat.UtolsoAlszam) + 1;

            #region Ügyirat UPDATE

            EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);

            // utolsó alszám módosítása:
            service_ugyiratok.IncrementUtolsoAlszam(execParam, ugyirat);

            /// Azonosító (Főszám) módosítása:
            /// 
            // BLG_292
            //ugyirat.Azonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(ugyirat, iktatoKonyv);
            ugyirat.Azonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(execParam, ugyirat, iktatoKonyv);

            ugyirat.Updated.Azonosito = true;

            ExecParam execParam_ugyiratUpdate = execParam.Clone();
            execParam_ugyiratUpdate.Record_Id = ugyirat.Id;

            Result result_ugyiratUpdate = service_ugyiratok.Update(execParam_ugyiratUpdate, ugyirat);
            if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratUpdate);
            }

            #endregion

            # region Régi szerelt ügyirat Azonosító UPDATE
            if (ugyiratIsMunkapeldany)
            {
                Contentum.eMigration.Service.MIG_FoszamService service_MIG = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
                ExecParam execParam_MIG = execParam.Clone();

                Contentum.eMigration.BusinessDocuments.MIG_Foszam mig_Foszam = new Contentum.eMigration.BusinessDocuments.MIG_Foszam();
                mig_Foszam.Updated.SetValueAll(false);
                mig_Foszam.Base.Updated.SetValueAll(false);

                mig_Foszam.Edok_Utoirat_Azon = ugyirat.Azonosito;
                mig_Foszam.Updated.Edok_Utoirat_Azon = true;

                Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch search_MIG = new Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch();
                search_MIG.Edok_Utoirat_Id.Value = ugyirat.Id;
                search_MIG.Edok_Utoirat_Id.Operator = Query.Operators.equals;


                Result result_MIG = service_MIG.UpdateTomeges(execParam_MIG, search_MIG, mig_Foszam, false);

                if (result_MIG.IsError)
                {
                    Logger.Debug(String.Format("MIG_FoszamService UpdateTomeges hiba:{0},{1}", result_MIG.ErrorCode, result_MIG.ErrorMessage));
                    throw new ResultException(result_MIG);
                }
            }
            #endregion Régi szerelt ügyirat Azonosító UPDATE

            #region Irat UPDATE

            //irat újboli lekérése a verzió miatt
            ExecParam execParamIratGet = execParam.Clone();
            execParamIratGet.Record_Id = iratId;
            Result resultGet = this.Get(execParamIratGet);

            resultGet.CheckError();
            iratHier.IratObj = resultGet.Record as EREC_IraIratok;

            // Alszám beállítása:
            iratAdatok.Alszam = ugyirat.UtolsoAlszam;
            iratAdatok.Updated.Alszam = true;

            // Állapot: Iktatott
            iratAdatok.Allapot = KodTarak.IRAT_ALLAPOT.Iktatott;
            iratAdatok.Updated.Allapot = true;

            // Iktató:
            iratAdatok.FelhasznaloCsoport_Id_Iktato = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            iratAdatok.Updated.FelhasznaloCsoport_Id_Iktato = true;

            // Iktatás dátuma:
            if ("MIGRATION".Equals(System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"], StringComparison.InvariantCultureIgnoreCase) || String.IsNullOrEmpty(iratAdatok.IktatasDatuma))
            {
                iratAdatok.IktatasDatuma = DateTime.Now.ToString();
            }
            iratAdatok.Updated.IktatasDatuma = true;

            iratAdatok.Base.Ver = iratHier.IratObj.Base.Ver;
            iratAdatok.Base.Updated.Ver = true;

            /// Azonosító (Iktatószám) Update
            /// 
            // BLG_292
            //iratAdatok.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(iktatoKonyv, ugyirat, iratAdatok);
            iratAdatok.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(execParam, iktatoKonyv, ugyirat, iratAdatok);

            iratAdatok.Updated.Azonosito = true;

            ExecParam execParam_iratUpdate = execParam.Clone();
            execParam_iratUpdate.Record_Id = iratId;

            Result result_iratUpdate = this.Update(execParam_iratUpdate, iratAdatok);
            if (!String.IsNullOrEmpty(result_iratUpdate.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_iratUpdate);
            }

            #endregion

            EREC_PldIratPeldanyokService service_pld = new EREC_PldIratPeldanyokService(this.dataContext);

            if (ugyiratIsMunkapeldany)
            {
                #region Ha az ügyirat is most lett beiktatva, összes iratra és azok iratpéldányira Azonosito (Iktatószám) UPDATE

                // Ügyirat összes iratának lekérése, kivéve a most iktatott iratot, mert azt már Update-eltük
                EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();

                iratokSearch.Ugyirat_Id.Value = ugyirat.Id;
                iratokSearch.Ugyirat_Id.Operator = Query.Operators.equals;

                // az eredeti irat NEM kell:
                iratokSearch.Id.Value = iratId;
                iratokSearch.Id.Operator = Query.Operators.notequals;

                Result result_iratokGetAll = this.GetAll(execParam.Clone(), iratokSearch);
                if (result_iratokGetAll.IsError)
                {
                    // hiba:
                    throw new ResultException(result_iratokGetAll);
                }

                foreach (DataRow row in result_iratokGetAll.Ds.Tables[0].Rows)
                {
                    EREC_IraIratok iratObj_row = new EREC_IraIratok();
                    // irat objektum feltöltése:
                    Utility.LoadBusinessDocumentFromDataRow(iratObj_row, row);
                    // Ellenőrzés, jó volt-e az adatlekérés:
                    if (iratObj_row.Ugyirat_Id != ugyirat.Id)
                    {
                        // hiba:
                        throw new ResultException("Hiba az iratok lekérése során!");
                    }

                    #region Irat UPDATE

                    // Azonosito mezőt kell update-elni:

                    iratObj_row.Updated.SetValueAll(false);
                    iratObj_row.Base.Updated.SetValueAll(false);
                    iratObj_row.Base.Updated.Ver = true;

                    // Azonosito:
                    // BLG_292
                    //iratObj_row.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(iktatoKonyv, ugyirat, iratObj_row);
                    iratObj_row.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(execParam, iktatoKonyv, ugyirat, iratObj_row);

                    iratObj_row.Updated.Azonosito = true;

                    ExecParam execParam_iratAzonositoUpdate = execParam.Clone();
                    execParam_iratAzonositoUpdate.Record_Id = iratObj_row.Id;

                    Result result_iratAzonositoUpdate = this.Update(execParam_iratAzonositoUpdate, iratObj_row);
                    if (result_iratAzonositoUpdate.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_iratAzonositoUpdate);
                    }

                    #endregion

                    #region Iratpéldányok Azonosítójának módosítása

                    // Módosított irat Iratpéldányainak lekérése:

                    EREC_PldIratPeldanyokSearch search_iratpeldanyok = new EREC_PldIratPeldanyokSearch();

                    search_iratpeldanyok.IraIrat_Id.Value = iratObj_row.Id;
                    search_iratpeldanyok.IraIrat_Id.Operator = Query.Operators.equals;

                    Result result_iratpeldanyokGetAll = service_pld.GetAll(execParam.Clone(), search_iratpeldanyok);
                    if (result_iratpeldanyokGetAll.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_iratpeldanyokGetAll);
                    }

                    foreach (DataRow rowPld in result_iratpeldanyokGetAll.Ds.Tables[0].Rows)
                    {
                        EREC_PldIratPeldanyok pldObj_row = new EREC_PldIratPeldanyok();
                        // iratpéldány objektum feltöltése a DataRow-ból:
                        Utility.LoadBusinessDocumentFromDataRow(pldObj_row, rowPld);
                        // Ellenőrzés, jó volt-e az adatlekérés:
                        if (pldObj_row.IraIrat_Id != iratObj_row.Id)
                        {
                            // hiba:
                            throw new ResultException("Hiba az iratpéldányok lekérése során!");
                        }

                        #region Iratpéldány UPDATE

                        pldObj_row.Updated.SetValueAll(false);
                        pldObj_row.Base.Updated.SetValueAll(false);
                        pldObj_row.Base.Updated.Ver = true;

                        // Azonosító módosítása:
                        // BLG_292
                        //pldObj_row.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(iktatoKonyv, ugyirat, iratObj_row, pldObj_row);
                        pldObj_row.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(execParam, iktatoKonyv, ugyirat, iratObj_row, pldObj_row);

                        pldObj_row.Updated.Azonosito = true;

                        ExecParam execParam_pldUpdate = execParam.Clone();
                        execParam_pldUpdate.Record_Id = pldObj_row.Id;

                        Result result_pldUpdate = service_pld.Update(execParam_pldUpdate, pldObj_row);
                        if (result_pldUpdate.IsError)
                        {
                            // hiba:
                            throw new ResultException(result_pldUpdate);
                        }

                        #endregion
                    }

                    #endregion

                }

                #endregion
            }

            #region Iratpéldányok UPDATE


            // Az iktatandó irat iratpéldányinak lekérése:
            EREC_PldIratPeldanyokSearch search_pld = new EREC_PldIratPeldanyokSearch();

            search_pld.IraIrat_Id.Value = iratId;
            search_pld.IraIrat_Id.Operator = Query.Operators.equals;

            Result result_pldGetAll = service_pld.GetAll(execParam.Clone(), search_pld);
            if (!String.IsNullOrEmpty(result_pldGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_pldGetAll);
            }

            foreach (DataRow row in result_pldGetAll.Ds.Tables[0].Rows)
            {
                string row_Id = row["Id"].ToString();
                string row_Ver = row["Ver"].ToString();
                string row_IraIrat_Id = row["IraIrat_Id"].ToString();
                string row_Sorszam = row["Sorszam"].ToString();
                string row_Allapot = row["Allapot"].ToString();
                string row_TovabbitasAlattAllapot = row["TovabbitasAlattAllapot"].ToString();

                // Ellenőrzés:
                if (row_IraIrat_Id != iratId)
                {
                    // valami hiba van:
                    throw new ResultException(52132);
                }

                // A sztornózottakat nem Update-eljük:
                if (row_Allapot == KodTarak.IRATPELDANY_ALLAPOT.Sztornozott)
                {
                    continue;
                }

                #region Iratpéldány UPDATE

                // Az 1-es iratpéldányt a megadott iratpéldány adatokkal update-eljük, a többinél csak az Azonosítót és az állapotot
                EREC_PldIratPeldanyok iratpeldanyObj = null;
                if (row_Sorszam == "1")
                {
                    iratpeldanyObj = iratPeldanyAdatok;
                }
                else
                {
                    iratpeldanyObj = new EREC_PldIratPeldanyok();
                    iratpeldanyObj.Updated.SetValueAll(false);
                    iratpeldanyObj.Base.Updated.SetValueAll(false);
                }


                // Állapot:
                if (row_Allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt)
                {
                    // ha továbbítás alatt állapotban volt, akkor kicsit máshogy állítjuk be az állapotokat:
                    iratpeldanyObj.TovabbitasAlattAllapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                    iratpeldanyObj.Updated.TovabbitasAlattAllapot = true;

                    iratpeldanyObj.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt;
                    iratpeldanyObj.Updated.Allapot = true;
                }
                else
                {
                    iratpeldanyObj.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                    iratpeldanyObj.Updated.Allapot = true;

                    iratpeldanyObj.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString; ; // String.Empty;
                    iratpeldanyObj.Updated.TovabbitasAlattAllapot = true;
                }

                // Sorszámot be kell állítani az Azonosító Update-hez:
                iratpeldanyObj.Sorszam = row_Sorszam;

                /// Azonosito (Iktatószám) Update:
                /// 
                // BLG_292
                //iratpeldanyObj.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(iktatoKonyv, ugyirat, iratAdatok, iratpeldanyObj);
                iratpeldanyObj.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(execParam, iktatoKonyv, ugyirat, iratAdatok, iratpeldanyObj);

                iratpeldanyObj.Updated.Azonosito = true;

                iratpeldanyObj.Base.Ver = row_Ver;
                iratpeldanyObj.Base.Updated.Ver = true;

                ExecParam execParam_pldUpdate = execParam.Clone();
                execParam_pldUpdate.Record_Id = row_Id;

                Result result_pldUpdate = service_pld.Update(execParam_pldUpdate, iratpeldanyObj);
                if (!String.IsNullOrEmpty(result_pldUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_pldUpdate);
                }

                #endregion
            }

            #endregion

            #region irat szignálás
            /// BUG#8333: 
            /// Ha NMHH szakrendszeri hívás jön (SZUReGetalszamWS, SZURGetalszamWS) (ez egy belső irat iktatás alszámra),
            /// akkor az irat állapotot szignáltra kell állítani
            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.ALSZAM_SZIGNALT_ENABLED, false)
                && iratAdatok.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Bejovo
                && !String.IsNullOrEmpty(iratAdatok.FelhasznaloCsoport_Id_Ugyintez))
            {
                if (iratAdatok.Allapot == KodTarak.IRAT_ALLAPOT.Iktatott)
                {
                    if (!isKiadmanyozas && String.IsNullOrEmpty(iratAdatok.Id))
                    {
                        iratAdatok.Id = iratId;
                    }
                    Result resultIratSzignalas = this.AutomatikusSzignalas(execParam.Clone(), iratAdatok.Id, iratAdatok.FelhasznaloCsoport_Id_Ugyintez);

                    if (resultIratSzignalas.IsError)
                    {
                        throw new ResultException(resultIratSzignalas);
                    }
                }
            }
            #endregion

            result = result_iratUpdate;
            // Uid-ban visszaadjuk az ügyiratId-t:
            result.Uid = iratHier.UgyiratObj.Id;

            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = new ErkeztetesIktatasResult();
            iktatasResult.UgyiratId = iratHier.UgyiratObj.Id;
            // BLG_292
            //string FullFoszam = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(ugyirat, iktatoKonyv);
            string FullFoszam = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(execParam, ugyirat, iktatoKonyv);

            iktatasResult.UgyiratAzonosito = FullFoszam;
            iktatasResult.UgyiratCsoportIdFelelos = ugyirat.Csoport_Id_Felelos;
            iktatasResult.IratId = iratId;
            // BLG_292
            //string FullIktatoszam = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(iktatoKonyv, ugyirat, iratAdatok);
            string FullIktatoszam = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(execParam, iktatoKonyv, ugyirat, iratAdatok);
            iktatasResult.IratAzonosito = FullIktatoszam;
            result.Record = iktatasResult;
            #endregion

            if (!isKiadmanyozas)
            {
                #region sharepoint struktúra update-elése

                Logger.Debug("UpdateIktatottIratDocumentsLocation start");

                DocumentService erec_documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
                iratAdatok.Id = iratId;
                Result resUpdateLocations = erec_documentService.UpdateIktatottIratDocumentsLocation(execParam.Clone(), iratHier.UgyiratObj, iratAdatok);

                if (resUpdateLocations.IsError)
                {
                    Logger.Error("UpdateIktatottIratDocumentsLocation hiba", execParam, resUpdateLocations);
                    //throw new ResultException(resUpdateLocations);
                }

                Logger.Debug("UpdateIktatottIratDocumentsLocation end");

                #endregion

                #region iktatott dokumentumba meta adatok bejegyzése - CR#2179

                Logger.Info("====== CR#2179 ======== innnen ");

                try
                {

                    EREC_CsatolmanyokService erec_CsatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);
                    EREC_CsatolmanyokSearch erec_csatolmanyokSearch = new EREC_CsatolmanyokSearch();

                    EREC_PldIratPeldanyokSearch erec_peldanySearch = new EREC_PldIratPeldanyokSearch();


                    #region csatolmanyok olvasasa

                    erec_csatolmanyokSearch.IraIrat_Id.Value = iratId;
                    erec_csatolmanyokSearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                    Result result_Csatolmanyok = erec_CsatolmanyokService.GetAll(execParam.Clone(), erec_csatolmanyokSearch);

                    if (String.IsNullOrEmpty(result_Csatolmanyok.ErrorCode))
                    {

                        #region  vonalkod lekerdezese az elso (!) iratbeldanybol

                        String vonalkod = String.Empty;

                        erec_peldanySearch.IraIrat_Id.Value = iratId;
                        erec_peldanySearch.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                        erec_peldanySearch.Sorszam.Value = "1";
                        erec_peldanySearch.Sorszam.Operator = Contentum.eQuery.Query.Operators.equals;

                        Result result_Peldany = service_pld.GetAll(execParam.Clone(), erec_peldanySearch);

                        if (String.IsNullOrEmpty(result_Peldany.ErrorCode))
                        {
                            if (result_Peldany.Ds.Tables[0].Rows.Count == 1)
                            {
                                int i_row = 0;
                                while (i_row < result_Csatolmanyok.Ds.Tables[0].Rows.Count)
                                {

                                    #region  property-k beallitasa

                                    String propsXmlParameter = String.Format("<params>"
                                            + "<krtdocument_id>{0}</krtdocument_id>"
                                            + "<metaadatok>"
                                            + "<metaadat internalName=\"edok_w_vonalkod\" value=\"{1}\" />"
                                            + "<metaadat internalName=\"edok_w_iktatoszam\" value=\"{2}\" />"
                                            + "</metaadatok>"
                                            + "</params>"
                                            , Convert.ToString(result_Csatolmanyok.Ds.Tables[0].Rows[i_row]["Dokumentum_Id"])
                                            , Convert.ToString(result_Peldany.Ds.Tables[0].Rows[0]["BarCode"])
                                            , FullIktatoszam
                                            );

                                    Result result_SetDocumentProperties = erec_documentService.SetDocumentPropertiesAndRevalidate(execParam.Clone(), propsXmlParameter);

                                    if (String.IsNullOrEmpty(result_SetDocumentProperties.ErrorCode))
                                    {
                                        Logger.Error(String.Format("nem szakijuk meg a munkapeldany beiktatasa folyamatot !!!\n" +
                                            "Hibaval tert vissza az erec_documentService.SetDocumentPropertiesAndRevalidate: {0} - {1}"
                                            , result_SetDocumentProperties.ErrorCode
                                            , result_SetDocumentProperties.ErrorMessage));
                                    }

                                    #endregion

                                    i_row++;
                                }
                            }
                            else
                            {
                                Logger.Error(String.Format("nem szakijuk meg a munkapeldany beiktatasa folyamatot !!!\n" +
                                    "Nem egy sorral tert vissza az elso peldany leszedese (service_pld.GetAll), hanem: {0}"
                                    , result_Peldany.Ds.Tables[0].Rows.Count));
                            }
                        }
                        else
                        {
                            Logger.Error(String.Format("nem szakijuk meg a munkapeldany beiktatasa folyamatot !!!\n" +
                                "Hibaval tert vissza az elso peldany leszedese service_pld.GetAll: {0} - {1}"
                                , result_Peldany.ErrorCode
                                , result_Peldany.ErrorMessage));
                        }

                        #endregion

                    }
                    else
                    {
                        Logger.Error(String.Format("nem szakijuk meg a munkapeldany beiktatasa folyamatot !!!\n" +
                            "Hibaval tert vissza a erec_CsatolmanyokService.GetAllWithExtension: {0} - {1}"
                            , result_Csatolmanyok.ErrorCode
                            , result_Csatolmanyok.ErrorMessage));
                    }

                    #endregion

                }
                catch (Exception dsEx)
                {
                    // !!! nem szakijuk meg a munkapeldany beiktatasa folyamatot !!!
                    // !!! csak a hibat loggoljuk !!!
                    Logger.Error(String.Format("nem szakijuk meg a munkapeldany beiktatasa folyamatot !!!\nKivetel hiba az sps-es meta adatok bejegyzesekor: {0}\nSource: {1}\nStackTrace: {2}"
                        , dsEx.Message
                        , dsEx.Source
                        , dsEx.StackTrace));
                }

                Logger.Info("====== CR#2179 ======== idaig ");

                #endregion
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            // kétfajta esemény naplózódhat le: MunkapeldanyBeiktatas_Belso és MunkapeldanyBeiktatas_Bejovo
            string funkcioKod = "MunkapeldanyBeiktatas_Belso";
            if (iratHier.IratObj.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
            {
                funkcioKod = "MunkapeldanyBeiktatas_Bejovo";
            }

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id
                , iratId, "EREC_IraIratok", funkcioKod).Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }



    #endregion

    [WebMethod()]
    public Result Felszabaditas(ExecParam execParam, string iratId, bool ugyiratSztornoHaUres)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Debug("Irat felszabadítása Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            #region IratHierarchia lekérése

            ExecParam execParam_iratGet = execParam.Clone();
            execParam_iratGet.Record_Id = iratId;

            Result result_irathierarchiaGet = this.GetIratHierarchia(execParam_iratGet);
            if (result_irathierarchiaGet.IsError)
            {
                // hiba:
                throw new ResultException(result_irathierarchiaGet);
            }

            IratHierarchia iratHierarchia = (IratHierarchia)result_irathierarchiaGet.Record;
            EREC_IraIratok iratObj = iratHierarchia.IratObj;

            #endregion

            #region Ellenorzések, felszabadítható-e az irat

            // Van-e küldeményo (Bejovo-e az irat)
            if (string.IsNullOrEmpty(iratObj.KuldKuldemenyek_Id))
            {
                // Hiba: Nem található küldemény az irathoz. (Nem bejövo az irat)
                throw new ResultException(52662);
            }

            // Ellenorzés, felszabadítható-e a (bejövo) irat
            Contentum.eRecord.BaseUtility.Iratok.Statusz iratStatusz = Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(iratObj);
            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(iratHierarchia.UgyiratObj);
            ErrorDetails errorDetail = null;

            if (Contentum.eRecord.BaseUtility.Iratok.Felszabadithato(iratStatusz, ugyiratStatusz, execParam, out errorDetail) == false)
            {
                // Hiba: Az irat nem szabadítható fel!
                throw new ResultException(52661, errorDetail);
            }

            #region Küldemény GET

            ExecParam execParam_KuldGet = execParam.Clone();
            execParam_KuldGet.Record_Id = iratObj.KuldKuldemenyek_Id;
            EREC_KuldKuldemenyekService service_kuldemenyek = new EREC_KuldKuldemenyekService(this.dataContext);

            Result result_kuldGet = service_kuldemenyek.Get(execParam_KuldGet);
            if (result_kuldGet.IsError)
            {
                // hiba:
                throw new ResultException(result_kuldGet);
            }

            EREC_KuldKuldemenyek kuldemenyObj = (EREC_KuldKuldemenyek)result_kuldGet.Record;

            #endregion

            Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz kuldemenyStatusz = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(kuldemenyObj);

            // A küldemény felszabadítható-e?
            if (Contentum.eRecord.BaseUtility.Kuldemenyek.Felszabadithato(kuldemenyStatusz, out errorDetail) == false)
            {
                // hiba: A küldemény nem szabadítható fel!
                throw new ResultException(52663, errorDetail);
            }

            #endregion

            if (iratStatusz.Allapot != KodTarak.IRAT_ALLAPOT.Sztornozott)
            {
                #region Irat Update

                iratObj.Updated.SetValueAll(false);
                iratObj.Base.Updated.SetValueAll(false);
                iratObj.Base.Updated.Ver = true;

                // Állapot: Felszabadított
                iratObj.Allapot = KodTarak.IRAT_ALLAPOT.Felszabaditva;
                iratObj.Updated.Allapot = true;

                ExecParam execParam_iratUpdate = execParam.Clone();
                execParam_iratUpdate.Record_Id = iratObj.Id;

                Result result_iratUpdate = this.Update(execParam_iratUpdate, iratObj);
                if (result_iratUpdate.IsError)
                {
                    // hiba:
                    throw new ResultException(result_iratUpdate);
                }

                #endregion
            }

            #region Küldemény UPDATE

            kuldemenyObj.Updated.SetValueAll(false);
            kuldemenyObj.Base.Updated.SetValueAll(false);
            kuldemenyObj.Base.Updated.Ver = true;

            // Állapot: Érkeztetett
            kuldemenyObj.Allapot = KodTarak.KULDEMENY_ALLAPOT.Erkeztetve;
            kuldemenyObj.Updated.Allapot = true;

            // orzot és kezelot át kell állítani a felhasználóra:
            kuldemenyObj.Csoport_Id_Felelos = execParam.Felhasznalo_Id;
            kuldemenyObj.Updated.Csoport_Id_Felelos = true;

            kuldemenyObj.FelhasznaloCsoport_Id_Orzo = execParam.Felhasznalo_Id;
            kuldemenyObj.Updated.FelhasznaloCsoport_Id_Orzo = true;

            ExecParam execParam_kuldUpdate = execParam.Clone();
            execParam_kuldUpdate.Record_Id = kuldemenyObj.Id;

            Result result_kuldUpdate = service_kuldemenyek.Update(execParam_kuldUpdate, kuldemenyObj);
            if (result_kuldUpdate.IsError)
            {
                // hiba:
                throw new ResultException(result_kuldUpdate);
            }

            #endregion

            #region UGYIRAT GET
            EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);
            // BUG_4727
            //EREC_UgyUgyiratok ugyiratObj = iratHierarchia.UgyiratObj;
            ExecParam execParam_ugyirat = execParam.Clone();
            execParam_ugyirat.Record_Id = iratHierarchia.UgyiratObj.Id;
            Result result_ugyiratGet = service_ugyiratok.Get(execParam_ugyirat);
            if (result_ugyiratGet.IsError)
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }
            EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
            #endregion

            if (iratStatusz.Allapot != KodTarak.IRAT_ALLAPOT.Sztornozott)
            {
                #region Ügyirat UPDATE - Iratszám csökkentése (ha nem munkairat)
                if (!iratStatusz.Munkairat)
                {
                    ugyiratObj.Updated.SetValueAll(false);
                    ugyiratObj.Base.Updated.SetValueAll(false);
                    ugyiratObj.Base.Updated.Ver = true;

                    int ujIratszam = service_ugyiratok.DecrementIratszam(execParam.Clone(), ugyiratObj);

                    ExecParam execParam_ugyiratUpdate = execParam.Clone();
                    execParam_ugyiratUpdate.Record_Id = ugyiratObj.Id;

                    Result result_ugyiratUpdate = service_ugyiratok.Update(execParam_ugyiratUpdate, ugyiratObj);
                    if (result_ugyiratUpdate.IsError)
                    {
                        // hiba:                    
                        throw new ResultException(result_ugyiratUpdate);
                    }
                }
                #endregion
            }

            if (iratStatusz.Allapot != KodTarak.IRAT_ALLAPOT.Sztornozott)
            {
                #region régi ügyirat sztornózás, ha csak egy irat volt az ügyiratban, vagy az összes felszabadított/sztornózott

                if (ugyiratSztornoHaUres && ugyiratObj.IratSzam == "0")
                {
                    // Ügyirat Sztornó:                

                    Result result_ugyiratSztorno = service_ugyiratok.Sztornozas(execParam.Clone(), ugyiratObj.Id, String.Empty);
                    if (result_ugyiratSztorno.IsError)
                    {
                        // hiba:
                        Logger.Error("Hiba az ügyirat sztornózása során", execParam, result_ugyiratSztorno);
                        throw new ResultException(result_ugyiratSztorno);
                    }
                }

                #endregion régi ügyirat lezárás/sztornózás, ha csak egy irat volt az ügyiratban, vagy az összes felszabadított/sztornózott
            }

            if (iratStatusz.Allapot != KodTarak.IRAT_ALLAPOT.Sztornozott)
            {
                #region Iratpéldányok felszabadítása (sztornózása)

                // Iratpéldányok GETALL
                EREC_PldIratPeldanyokService service_pld = new EREC_PldIratPeldanyokService(this.dataContext);
                EREC_IraKezbesitesiTetelekService service_kezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);

                EREC_PldIratPeldanyokSearch search_pldGetAll = new EREC_PldIratPeldanyokSearch();
                search_pldGetAll.IraIrat_Id.Value = iratId;
                search_pldGetAll.IraIrat_Id.Operator = Query.Operators.equals;

                Result result_pldGetAll = service_pld.GetAll(execParam.Clone(), search_pldGetAll);
                if (result_pldGetAll.IsError)
                {
                    // hiba:
                    throw new ResultException(result_pldGetAll);
                }

                foreach (DataRow row in result_pldGetAll.Ds.Tables[0].Rows)
                {
                    string row_Id = row["Id"].ToString();
                    string row_IraIrat_Id = row["IraIrat_Id"].ToString();
                    string row_Ver = row["Ver"].ToString();

                    // ellenorzés:
                    if (row_IraIrat_Id != iratId)
                    {
                        // hiba az iratpéldányok lekérése során:
                        throw new ResultException(52664);
                    }

                    EREC_PldIratPeldanyok pldUpdateObj = new EREC_PldIratPeldanyok();
                    pldUpdateObj.Updated.SetValueAll(false);
                    pldUpdateObj.Base.Updated.SetValueAll(false);

                    pldUpdateObj.Base.Ver = row_Ver;
                    pldUpdateObj.Base.Updated.Ver = true;

                    // Állapot: Sztornózott
                    pldUpdateObj.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Sztornozott;
                    pldUpdateObj.Updated.Allapot = true;

                    // TovabbitasAlattAllapot:
                    pldUpdateObj.TovabbitasAlattAllapot = String.Empty;
                    pldUpdateObj.Updated.TovabbitasAlattAllapot = true;

                    // Sztornózás dátuma:
                    if ((System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"] != null && System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"].ToUpper() != "MIGRATION") || String.IsNullOrEmpty(pldUpdateObj.SztornirozasDat))
                    {
                        pldUpdateObj.SztornirozasDat = DateTime.Now.ToString();
                    }
                    pldUpdateObj.Updated.SztornirozasDat = true;

                    ExecParam execParam_PldUpdate = execParam.Clone();
                    execParam_PldUpdate.Record_Id = row_Id;

                    Result result_pldUpdate = service_pld.Update(execParam_PldUpdate, pldUpdateObj);
                    if (result_pldUpdate.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_pldUpdate);
                    }

                    // TODO: 

                    #region Az esetleges kézbesítési tételek törlése:

                    Result result_kezbTetelInvalidate =
                        service_kezbesitesiTetelek.NemAtadottKezbesitesiTetelInvalidate(execParam.Clone(), row_Id);

                    if (result_kezbTetelInvalidate.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_kezbTetelInvalidate);
                    }

                    #endregion
                }

                #endregion
            }

            if (iratStatusz.Allapot != KodTarak.IRAT_ALLAPOT.Sztornozott)
            {
                #region Vonalkód UPDATE, ha szükséges

                /// Ha a Barkodok táblában esetleg már máshova lenne kötve az a vonalkód (pl. egy iratpéldányhoz), akkor visszakötjük erre a küldeményre
                if (!String.IsNullOrEmpty(kuldemenyObj.BarCode))
                {
                    // Barkod objektum lekérése:
                    KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);

                    Result result_barkodokGet = service_barkodok.GetBarcodeByValue(execParam.Clone(), kuldemenyObj.BarCode);
                    if (result_barkodokGet.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_barkodokGet);
                    }

                    KRT_Barkodok barkodObj = (KRT_Barkodok)result_barkodokGet.Record;

                    if (barkodObj.Obj_Id != kuldemenyObj.Id && barkodObj.Kod == kuldemenyObj.BarCode)
                    {
                        #region Barkod UPDATE

                        barkodObj.Updated.SetValueAll(false);
                        barkodObj.Base.Updated.SetValueAll(false);
                        barkodObj.Base.Updated.Ver = true;

                        // Obj_Id
                        barkodObj.Obj_Id = kuldemenyObj.Id;
                        barkodObj.Updated.Obj_Id = true;

                        // Obj_type
                        barkodObj.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;
                        barkodObj.Updated.Obj_type = true;

                        ExecParam execParam_barkodUpdate = execParam.Clone();
                        execParam_barkodUpdate.Record_Id = barkodObj.Id;

                        Result result_barkodUpdate = service_barkodok.Update(execParam_barkodUpdate, barkodObj);
                        if (result_barkodUpdate.IsError)
                        {
                            // hiba:
                            throw new ResultException(result_barkodUpdate);
                        }

                        #endregion
                    }
                }

                #endregion
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, iratId, "EREC_IraIratok", "IratFelszabaditas").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("Irat felszabadítása Stop", execParam);
        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result Szignalas(ExecParam execParam, string erec_UgyUgyiratok_Id, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Debug("Szignálás - Start", execParam);

        Result result;
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = SzignalasInternal(execParam, erec_UgyUgyiratok_Id, csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok, isTransactionBeginHere, false, false);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        // Ha minden OK:
        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result TomegesSzignalas(ExecParam execParam, string[] erec_UgyUgyiratok_Ids, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Debug("Tömeges szignálás - Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            foreach (string erec_UgyUgyiratok_Id in erec_UgyUgyiratok_Ids)
            {
                result = SzignalasInternal(execParam, erec_UgyUgyiratok_Id, csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok, isTransactionBeginHere, false, false);
                if (result.IsError)
                    throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        // Ha minden OK:
        log.WsEnd(execParam, result);
        return result;
    }

    private Result SzignalasInternal(ExecParam execParam, string iratId, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok, bool isTransactionBeginHere, bool automatikus, bool csakIrat)
    {
        Result result = new Result() { Uid = iratId };

        // Paraméterek ellenőrzése:
        if (execParam == null || String.IsNullOrEmpty(iratId) || String.IsNullOrEmpty(csoport_Id_Felelos_Kovetkezo))
        {
            // hiba
            result = ResultError.CreateNewResultWithErrorCode(52150);
            result.Uid = iratId;
            Logger.Error("Szignálás: Hibás paraméterlista", execParam);
            throw new ResultException(result);
        }

        // Irat lekérése:
        var iratokService = new EREC_IraIratokService(this.dataContext);
        ExecParam execParam_UgyiratGet = execParam.Clone();
        execParam_UgyiratGet.Record_Id = iratId;

        Result result_ugyiratGet = iratokService.Get(execParam_UgyiratGet);
        if (result_ugyiratGet.IsError)
        {
            // hiba:
            throw new ResultException(result_ugyiratGet);
        }

        if (result_ugyiratGet.Record == null)
        {
            // hiba
            throw new ResultException(52151);
        }

        EREC_IraIratok erec_Irat = (EREC_IraIratok)result_ugyiratGet.Record;

        Contentum.eRecord.BaseUtility.Iratok.Statusz ugyiratStatusz =
            Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(erec_Irat);
        ExecParam execParam_szignalhato = execParam.Clone();
        ErrorDetails errorDetail = null;

        // Ellenőrzés, hogy Szignálható-e az ügyirat:
        if (Contentum.eRecord.BaseUtility.Iratok.Szignalhato(ugyiratStatusz, execParam_szignalhato, out errorDetail) == false)
        {
            // Nem Szignálható az ügyirat:
            throw new ResultException(52152, errorDetail);
        }
        // IRAT UPDATE:
        // mezők módosíthatóságának letiltása:
        erec_Irat.Updated.SetValueAll(false);
        erec_Irat.Base.Updated.SetValueAll(false);
        erec_Irat.Base.Updated.Ver = true;

        // Állapot átállítása:
        erec_Irat.FelhasznaloCsoport_Id_Ugyintez = csoport_Id_Felelos_Kovetkezo;
        erec_Irat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

        erec_Irat.Allapot = KodTarak.IRAT_ALLAPOT.Szignalt;
        erec_Irat.Updated.Allapot = true;

        // BUG_11120 Elintézett flag leszedése
        erec_Irat.Elintezett = "0";
        erec_Irat.Updated.Elintezett = true;

        //Szervezetre szignálás
        if (!String.IsNullOrEmpty(csoport_Id_Felelos_Kovetkezo))
        {
            KRT_CsoportokService csopService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
            ExecParam csopExecParam = execParam.Clone();
            csopExecParam.Record_Id = csoport_Id_Felelos_Kovetkezo;

            Result csopResult = csopService.Get(csopExecParam);

            if (csopResult.IsError)
                throw new ResultException(csopResult);

            KRT_Csoportok csoport = csopResult.Record as KRT_Csoportok;

            if (csoport.Tipus == KodTarak.CSOPORTTIPUS.Szervezet)
            {
                erec_Irat.Csoport_Id_Ugyfelelos = csoport_Id_Felelos_Kovetkezo;
                erec_Irat.Updated.Csoport_Id_Ugyfelelos = true;
            }
            else if (string.IsNullOrEmpty(erec_Irat.Csoport_Id_Ugyfelelos))
            {
                erec_Irat.Csoport_Id_Ugyfelelos = execParam.FelhasznaloSzervezet_Id ?? csoport_Id_Felelos_Kovetkezo;
                erec_Irat.Updated.Csoport_Id_Ugyfelelos = true;
            }
        }

        ExecParam execParam_IratUpdate = execParam.Clone();
        execParam_IratUpdate.Record_Id = erec_Irat.Id;

        Result result_IratUpdate = iratokService.Update(execParam_IratUpdate, erec_Irat);
        if (result_IratUpdate.IsError)
        {
            // hiba volt:
            throw new ResultException(result_IratUpdate);
        }

        // Kezelési feljegyzés INSERT, ha megadták
        if (erec_HataridosFeladatok != null)
        {
            EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
            ExecParam execParam_kezFeljInsert = execParam.Clone();

            // Ügyirathoz kötés
            erec_HataridosFeladatok.Obj_Id = erec_Irat.Id;
            erec_HataridosFeladatok.Updated.Obj_Id = true;

            erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;
            erec_HataridosFeladatok.Updated.Obj_type = true;

            if (!String.IsNullOrEmpty(erec_Irat.Azonosito))
            {
                erec_HataridosFeladatok.Azonosito = erec_Irat.Azonosito;
                erec_HataridosFeladatok.Updated.Azonosito = true;
            }

            Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
                execParam_kezFeljInsert, erec_HataridosFeladatok);
            if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_kezFeljInsert);
            }
        }

        result = result_IratUpdate;

        #region PLD IRAT SZIGNAL
        if (!csakIrat)
        {
            EREC_PldIratPeldanyokService svcIratPld = new EREC_PldIratPeldanyokService(this.dataContext);
            svcIratPld.IratPeldanySzignalasInternal(execParam.Clone(), erec_Irat.Id, csoport_Id_Felelos_Kovetkezo);
        }
        #endregion

        #region ucm
        SetUCMJogosultsag(execParam, iratId);
        #endregion

        #region Eseménynaplózás

        if (isTransactionBeginHere)
        {
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, iratId, "EREC_IraIratok", "IratSzignalas").Record;
            eventLogService.Insert(execParam, eventLogRecord);
        }

        #endregion

        return result;
    }

    private Result AutomatikusSzignalas(ExecParam execParam, string iratId, string csoport_Id_Felelos_Kovetkezo)
    {
        Result result = new Result();

        result = SzignalasInternal(execParam, iratId, csoport_Id_Felelos_Kovetkezo, null, false, true, false);

        KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
        KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, iratId, "EREC_IraIratok", "AutomatikusIratSzignalas").Record;
        eventLogService.Insert(execParam, eventLogRecord);

        return result;
    }

    public Result SzignalasByUgyirat(ExecParam execParam, string ugyiratId, string csoport_Id_Felelos_Kovetkezo)
    {
        Result result = new Result();

        EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
        iratokSearch.Ugyirat_Id.Value = ugyiratId;
        iratokSearch.Ugyirat_Id.Operator = Query.Operators.equals;

        iratokSearch.Allapot.Value = KodTarak.IRAT_ALLAPOT.Iktatott;
        iratokSearch.Allapot.Operator = Query.Operators.equals;

        Result getAllResult = this.GetAll(execParam.Clone(), iratokSearch);

        getAllResult.CheckError();

        foreach (DataRow row in getAllResult.Ds.Tables[0].Rows)
        {
            string iratId = row["Id"].ToString();
            string felhasznaloCsoport_Id_Ugyintez = row["FelhasznaloCsoport_Id_Ugyintez"].ToString();
            result = SzignalasInternal(execParam, iratId, csoport_Id_Felelos_Kovetkezo, null, false, true, true);
            result.CheckError();
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, iratId, "EREC_IraIratok", "AutomatikusIratSzignalas").Record;
            eventLogService.Insert(execParam, eventLogRecord);
        }



        return result;
    }

    // Segédeljárás, nem WebMethod
    public static bool UgyiratInditoIrat(EREC_IraIratok erec_IraIrat)
    {
        if (erec_IraIrat == null)
        {
            return false;
        }

        /// TODO:
        /// Jelenleg ügyiratindító az irat, ha alszáma 1, de lehet hogy később nem 1-ről fognak indulni???
        /// 

        try
        {
            int alszam = Int32.Parse(erec_IraIrat.Alszam);
            if (alszam == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch
        {
            return false;
        }
    }


    /// <summary>
    /// Irat kiadmányozása:
    /// Irat és iratpéldányainak kiadmányozott állapotba állítása
    /// Nem WebMethod, (EREC_IratAlairok felől hívva, ha kiadmányozó aláírja)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_IraIratok_Id"></param>
    /// <param name="felhCsopId_Kiadmanyozo"></param>
    /// <returns></returns>
    public Result Kiadmanyozas(ExecParam execParam, string erec_IraIratok_Id, string felhCsopId_Kiadmanyozo, bool isTomeges)
    {
        // Paraméterek ellenőrzése:
        if (execParam == null
            || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_IraIratok_Id)
            || String.IsNullOrEmpty(felhCsopId_Kiadmanyozo))
        {
            // hiba:
            return ResultError.CreateNewResultWithErrorCode(52320);
        }

        // Irat hierarchia lekérése (Kiadmányozás ellenőrzéséhez kell az ügyirat is)

        EREC_IraIratokService service_Iratok = new EREC_IraIratokService(this.dataContext);
        ExecParam execParam_iratGet = execParam.Clone();
        execParam_iratGet.Record_Id = erec_IraIratok_Id;

        Result result_IratHier = service_Iratok.GetIratHierarchia(execParam_iratGet);
        if (result_IratHier.IsError)
        {
            // hiba:
            throw new ResultException(result_IratHier);
        }

        if (result_IratHier.Record == null)
        {
            // hiba:
            throw new ResultException(52321);
        }

        IratHierarchia iratHierarchia = (IratHierarchia)result_IratHier.Record;

        EREC_IraIratok erec_IraIrat = iratHierarchia.IratObj;

        // ha nem is kell kiadmányozni:
        if (EREC_IraIratokService.Kiadmanyozando(erec_IraIrat) == false)
        {
            // Nem kiadmányozandó:
            throw new ResultException(52323);
        }


        //munkapéldány beiktatása
        if (Contentum.eRecord.BaseUtility.Iratok.Munkaanyag(erec_IraIrat))
        {
            if (Rendszerparameterek.GetBoolean(execParam, "MUNKAIRAT_AUTO_IKTATAS_KIADMANYOZASKOR", false))
            {
                EREC_PldIratPeldanyok erec_PldIratPeldany = new EREC_PldIratPeldanyok();
                erec_PldIratPeldany.Updated.SetValueAll(false);
                erec_PldIratPeldany.Base.Updated.SetValueAll(false);

                isKiadmanyozas = true;
                Result result_munkapeldanyBeiktatas = this.MunkapeldanyBeiktatasa(execParam, erec_IraIratok_Id, erec_IraIrat, erec_PldIratPeldany);
                isKiadmanyozas = false;

                if (result_munkapeldanyBeiktatas.IsError)
                {
                    throw new ResultException(result_munkapeldanyBeiktatas);
                }

                //irat újra lekérése
                Result resultIratGet = this.Get(execParam_iratGet);

                if (resultIratGet.IsError)
                {
                    throw new ResultException(resultIratGet);
                }

                erec_IraIrat = resultIratGet.Record as EREC_IraIratok;
            }
        }

        // Állapot ellenőrzése:
        Contentum.eRecord.BaseUtility.Iratok.Statusz iratStatusz =
            Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(erec_IraIrat);
        Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
            Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(iratHierarchia.UgyiratObj);

        ErrorDetails errorDetail;
        if (Contentum.eRecord.BaseUtility.Iratok.Kiadmanyozhato(iratStatusz, ugyiratStatusz, execParam, out errorDetail) == false)
        {
            // Nem kiadmányozható:
            throw new ResultException(52322, errorDetail);
        }

        // Irat UPDATE
        // módosíthatóság beállítása:
        erec_IraIrat.Updated.SetValueAll(false);
        erec_IraIrat.Base.Updated.SetValueAll(false);
        erec_IraIrat.Base.Updated.Ver = true;

        // Kiadmányozó állítása:
        erec_IraIrat.FelhasznaloCsoport_Id_Kiadmany = felhCsopId_Kiadmanyozo;
        erec_IraIrat.Updated.FelhasznaloCsoport_Id_Kiadmany = true;

        // Állapot 
        erec_IraIrat.Allapot = KodTarak.IRAT_ALLAPOT.Kiadmanyozott;
        erec_IraIrat.Updated.Allapot = true;

        ExecParam execParam_IratUpdate = execParam.Clone();
        execParam_IratUpdate.Record_Id = erec_IraIrat.Id;

        Result result_IratUpdate = service_Iratok.Update(execParam_IratUpdate, erec_IraIrat);
        if (!String.IsNullOrEmpty(result_IratUpdate.ErrorCode))
        {
            // hiba:
            //ContextUtil.SetAbort();
            //return result_IratUpdate;

            throw new ResultException(result_IratUpdate);
        }
        else
        {
            // Iratpéldányok állapotának állítása Kiadmányozott-ra, ahol lehet
            // TODO: Nem teljesen biztos, hogy ezt kell csinálni

            // Iratpéldányok lekérése:
            EREC_PldIratPeldanyokService service_Pld = new EREC_PldIratPeldanyokService(this.dataContext);

            EREC_PldIratPeldanyokSearch search_Pld = new EREC_PldIratPeldanyokSearch();
            search_Pld.IraIrat_Id.Value = erec_IraIrat.Id;
            search_Pld.IraIrat_Id.Operator = Query.Operators.equals;

            ExecParam execParam_PldGetAll = execParam.Clone();

            Result result_PldGetAll = service_Pld.GetAll(execParam_PldGetAll, search_Pld);
            if (!String.IsNullOrEmpty(result_PldGetAll.ErrorCode))
            {
                // hiba:
                //ContextUtil.SetAbort();
                //return result_PldGetAll;

                throw new ResultException(result_PldGetAll);
            }
            else
            {
                try
                {
                    foreach (DataRow row in result_PldGetAll.Ds.Tables[0].Rows)
                    {
                        // IratId ellenőrzése, hátha valami nem volt ok
                        string iraIratId = row["IraIrat_Id"].ToString();
                        if (iraIratId != erec_IraIrat.Id)
                        {
                            // hiba
                            //ContextUtil.SetAbort();
                            //return ResultError.CreateNewResultWithErrorCode(52324);

                            throw new ResultException(52324);
                        }
                        string allapot = row["Allapot"].ToString();
                        string tovabbitasAlattAllapot = row["TovabbitasAlattAllapot"].ToString();

                        // Csak akkor állítjuk kiadmányozottra, ha Iktatott, vagy Jóváhagyás alatt, 
                        // vagy továbbítás alatt ugyanezek az állapotok
                        if (allapot == KodTarak.IRATPELDANY_ALLAPOT.Iktatott
                            || allapot == KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt
                            || (allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt
                                && (tovabbitasAlattAllapot == KodTarak.IRATPELDANY_ALLAPOT.Iktatott
                                    || tovabbitasAlattAllapot == KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt)
                                    )
                                )
                        {
                            string iratPld_Id = row["Id"].ToString();

                            // Iratpéldány GET
                            ExecParam execParam_PldGet = execParam.Clone();
                            execParam_PldGet.Record_Id = iratPld_Id;

                            Result result_PldGet = service_Pld.Get(execParam_PldGet);
                            if (!string.IsNullOrEmpty(result_PldGet.ErrorCode))
                            {
                                // hiba:
                                //ContextUtil.SetAbort();
                                //return result_PldGet;

                                throw new ResultException(result_PldGet);
                            }
                            else
                            {
                                if (result_PldGet.Record == null)
                                {
                                    // hiba:
                                    //ContextUtil.SetAbort();
                                    //return ResultError.CreateNewResultWithErrorCode(52324);

                                    throw new ResultException(52324);
                                }

                                EREC_PldIratPeldanyok erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result_PldGet.Record;

                                // Iratpéldány UPDATE
                                erec_PldIratPeldanyok.Updated.SetValueAll(false);
                                erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);
                                erec_PldIratPeldanyok.Base.Updated.Ver = true;

                                #region  Allapot módosítása:
                                if (allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt)
                                {
                                    erec_PldIratPeldanyok.TovabbitasAlattAllapot = KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott;
                                    erec_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;
                                }
                                else
                                {
                                    erec_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott;
                                    erec_PldIratPeldanyok.Updated.Allapot = true;

                                    erec_PldIratPeldanyok.TovabbitasAlattAllapot = "";
                                    erec_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;
                                }
                                #endregion

                                ExecParam execParam_PldUpdate = execParam.Clone();
                                execParam_PldUpdate.Record_Id = erec_PldIratPeldanyok.Id;

                                Result result_PldUpdate = service_Pld.Update(execParam_PldUpdate, erec_PldIratPeldanyok);
                                if (!string.IsNullOrEmpty(result_PldUpdate.ErrorCode))
                                {
                                    // hiba:
                                    //ContextUtil.SetAbort();
                                    //return result_PldUpdate;

                                    throw new ResultException(result_PldUpdate);
                                }
                            }
                        }
                    }
                }
                catch (ResultException resultException)
                {
                    throw resultException;
                }
                catch
                {
                    // hiba:
                    //ContextUtil.SetAbort();
                    //return ResultError.CreateNewResultWithErrorCode(52324);

                    throw new ResultException(52324);
                }

                // ha minden OK:

                //automatikus kiküldés
                Result automatikusKikuldesResult = AutomatikusElektronikusUzenetKikuldes(execParam, erec_IraIratok_Id);

                if (automatikusKikuldesResult.IsError)
                {
                    Logger.Error("AutomatikusElektronikusUzenetKikuldes hiba", execParam, automatikusKikuldesResult);
                }

                return result_IratUpdate;
            }
        }
    }

    /// <summary>
    /// Az irat KiadmanyozniKell mezőjének alapján kell-e kiadmányozni
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_IraIratok_Id"></param>
    /// <returns></returns>
    public static bool Kiadmanyozando(DataContext _dataContext, ExecParam execParam, String erec_IraIratok_Id)
    {
        if (String.IsNullOrEmpty(erec_IraIratok_Id))
        {
            return false;
        }

        EREC_IraIratokService service = new EREC_IraIratokService(_dataContext);
        execParam.Record_Id = erec_IraIratok_Id;

        Result result_IratGet = service.Get(execParam);
        if (!String.IsNullOrEmpty(result_IratGet.ErrorCode))
        {
            return false;
        }
        else
        {
            EREC_IraIratok erec_IraIrat = (EREC_IraIratok)result_IratGet.Record;

            return Kiadmanyozando(erec_IraIrat);
        }
    }

    /// <summary>
    /// Az irat KiadmanyozniKell mezőjének alapján kell-e kiadmányozni
    /// </summary>
    public static bool Kiadmanyozando(EREC_IraIratok erec_IraIrat)
    {
        if (erec_IraIrat == null)
        {
            return false;
        }

        if (erec_IraIrat.KiadmanyozniKell == "1")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// A megadott csatolmány feltöltése az irathoz. (Az irat id-t a Record.IraIrat_Id-ban kell megadni.) ScanService-bol ezt hivjuk.
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <param name="csatolmany"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result CsatolmanyUpload(ExecParam ExecParam, EREC_Csatolmanyok Record, Csatolmany csatolmany)
    {
        return CsatolmanyUpload(ExecParam, Record, csatolmany, false);
    }

    public Result CsatolmanyUpload(ExecParam ExecParam, EREC_Csatolmanyok Record, Csatolmany csatolmany, bool ujIrat)
    {
        Logger.Debug(String.Format("Erec_irairatokService.CsatolmanyUpload indul. Record.Id: {0} - csatolmany.Nev: {1}", Record.Id, csatolmany.Nev));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();

        Result result = new Result();

        #region irat id alapjan mindenfele uploadhoz kello dolog lekerdezese

        String ugyiratFoszam = String.Empty;
        String iktatokonyvIktatohely = String.Empty;
        string ev = String.Empty;

        EREC_IraIratokSearch iratSearch = new EREC_IraIratokSearch();
        iratSearch.Id.Value = Record.IraIrat_Id;
        iratSearch.Id.Operator = Contentum.eQuery.Query.Operators.equals;

        ExecParam ep = ExecParam.Clone();
        Result resultIratExt = GetAllWithExtension(ep, iratSearch);

        if (!String.IsNullOrEmpty(resultIratExt.ErrorCode))
        {
            log.WsEnd(ExecParam, resultIratExt);
            throw new ResultException(resultIratExt);
        }

        if (resultIratExt.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Error(String.Format("EREC_IraIratokService.CsatolmanyUpload hiba: irat GetAllWithExtension nem adott vissza sorokat! Id: {0}", Record.IraIrat_Id));
            resultIratExt = new Result();
            resultIratExt.ErrorCode = "GETIEXT0001";
            resultIratExt.ErrorMessage = "EREC_IraIratokService.CsatolmanyUpload hiba: irat GetAllWithExtension nem adott vissza sorokat!";
            throw new ResultException(resultIratExt);
        }

        if (resultIratExt.Ds.Tables[0].Rows.Count > 1)
        {
            Logger.Error(String.Format("EREC_IraIratokService.CsatolmanyUpload hiba: irat GetAllWithExtension tul sok sort adott vissza! Id: {0}", Record.IraIrat_Id));
            resultIratExt = new Result();
            resultIratExt.ErrorCode = "GETIEXT0002";
            resultIratExt.ErrorMessage = "EREC_IraIratokService.CsatolmanyUpload hiba: irat GetAllWithExtension tul sok sort adott vissza!";
            throw new ResultException(resultIratExt);
        }

        try
        {
            ugyiratFoszam = Convert.ToString(resultIratExt.Ds.Tables[0].Rows[0]["Foszam"]);

            iktatokonyvIktatohely = Convert.ToString(resultIratExt.Ds.Tables[0].Rows[0]["Iktatoszam_Merge"]);
            iktatokonyvIktatohely = iktatokonyvIktatohely.Substring(0, iktatokonyvIktatohely.IndexOf("/", 0)).Trim();

            ev = Convert.ToString(resultIratExt.Ds.Tables[0].Rows[0]["Ev"]);
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("EREC_IraIratokService.CsatolmanyUpload hiba konvertalaskor: Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
            resultIratExt = new Result();
            resultIratExt.ErrorCode = "GETIEXT0003";
            resultIratExt.ErrorMessage = String.Format("EREC_IraIratokService.CsatolmanyUpload hiba konvertalaskor: Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            throw new ResultException(resultIratExt);
        }

        EREC_IraIratok irat = new EREC_IraIratok();
        Utility.LoadBusinessDocumentFromDataRow(irat, resultIratExt.Ds.Tables[0].Rows[0]);

        #region ugyirat lekerese
        EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();

        if (String.IsNullOrEmpty(ugyiratFoszam))
        {
            EREC_UgyUgyiratokService svcUgyirat = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam xpmUgyirat = ExecParam.Clone();
            xpmUgyirat.Record_Id = irat.Ugyirat_Id;
            Result resUgyirat = svcUgyirat.Get(xpmUgyirat);

            if (resUgyirat.IsError)
            {
                throw new ResultException(resUgyirat);
            }

            ugyirat = resUgyirat.Record as EREC_UgyUgyiratok;
        }
        else
        {
            ugyirat.Foszam = ugyiratFoszam;
        }
        #endregion

        #endregion

        try
        {
            string uploadXmlStrParams_alairasResz = "";
            if (csatolmany.AlairasAdatok.CsatolmanyAlairando)
            {
                uploadXmlStrParams_alairasResz = csatolmany.AlairasAdatok.GetInnerXml();
            }


            if (!String.IsNullOrEmpty(csatolmany.SourceSharePath))
            {
                #region Ha SourceSharePath meg van adva...

                string kivalasztottIratId = Record.IraIrat_Id;
                string uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                        "<iktatokonyv>{5}</iktatokonyv>" +
                                                        "<sourceSharePath>{6}</sourceSharePath>" +
                                                        "<foszam>{0}</foszam>" +
                                                        "<alszam>{8}</alszam>" +
                                                        "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                        "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                        "<megjegyzes></megjegyzes>" +
                                                        "<munkaanyag>{2}</munkaanyag>" +
                                                        "<ujirat>{3}</ujirat>" +
                                                        "<vonalkod></vonalkod>" +
                                                        "<docmetaIratId></docmetaIratId>" +
                                                        "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                        "<tartalomSHA1>{7}</tartalomSHA1>" +
                                                        "<ev>{9}</ev>" +
                                                        // Aláíráshoz szükséges rész:
                                                        uploadXmlStrParams_alairasResz +
                                                    "</uploadparameterek>"
                                                    , Contentum.eRecord.BaseUtility.Ugyiratok.GetFoszamString(ugyirat)
                                                    , ujIrat ? String.Empty : kivalasztottIratId
                                                    , Contentum.eRecord.BaseUtility.Iratok.Munkaanyag(irat) ? "IGEN" : "NEM"
                                                    , ujIrat ? "IGEN" : "NEM"
                                                    , "NEM"
                                                    , iktatokonyvIktatohely
                                                    , String.Empty           //csatolmany.SourceSharePath
                                                    , csatolmany.TartalomHash
                                                    , Contentum.eRecord.BaseUtility.Iratok.GetAlszamString(irat)
                                                    , ev
                                                    );

                result = documentService.UploadFromeRecordWithCTTCheckin
                    (
                       ExecParam
                       , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                       , csatolmany.Nev
                       , csatolmany.Tartalom
                       , null
                       , uploadXmlStrParams
                    );
                #endregion
            }
            else
            {
                // KRT_Dokumentumok objektum létrehozása
                KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();

                #region KRT_Dokumentumok mezőinek állítása

                if (csatolmany.Megnyithato != null)
                {
                    krt_Dokumentumok.Megnyithato = csatolmany.Megnyithato;
                }
                if (csatolmany.Olvashato != null)
                {
                    krt_Dokumentumok.Olvashato = csatolmany.Olvashato;
                }
                if (csatolmany.ElektronikusAlairas != null)
                {
                    krt_Dokumentumok.ElektronikusAlairas = csatolmany.ElektronikusAlairas;
                }
                if (!String.IsNullOrEmpty(csatolmany.Titkositas))
                {
                    krt_Dokumentumok.Titkositas = csatolmany.Titkositas;
                }
                if (csatolmany.OCRAdatok.OCRAllapot == "1")
                {
                    krt_Dokumentumok.OCRAllapot = csatolmany.OCRAdatok.OCRAllapot;
                    krt_Dokumentumok.OCRPrioritas = csatolmany.OCRAdatok.OCRPrioritas;
                }

                #endregion

                string kivalasztottIratId = Record.IraIrat_Id;
                string uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                        "<iktatokonyv>{5}</iktatokonyv>" +
                                                        "<sourceSharePath></sourceSharePath>" +
                                                        "<foszam>{0}</foszam>" +
                                                        "<alszam>{7}</alszam>" +
                                                        "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                        "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                        "<megjegyzes></megjegyzes>" +
                                                        "<munkaanyag>{2}</munkaanyag>" +
                                                        "<ujirat>{3}</ujirat>" +
                                                        "<vonalkod></vonalkod>" +
                                                        "<docmetaIratId></docmetaIratId>" +
                                                        "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                        "<tartalomSHA1>{6}</tartalomSHA1>" +
                                                        "<ev>{8}</ev>" +
                                                        // Aláíráshoz szükséges rész:
                                                        uploadXmlStrParams_alairasResz +
                                                    "</uploadparameterek>"
                                                    , Contentum.eRecord.BaseUtility.Ugyiratok.GetFoszamString(ugyirat)
                                                    , ujIrat ? String.Empty : kivalasztottIratId
                                                    , Contentum.eRecord.BaseUtility.Iratok.Munkaanyag(irat) ? "IGEN" : "NEM"
                                                    , ujIrat ? "IGEN" : "NEM"
                                                    , "NEM"
                                                    , iktatokonyvIktatohely
                                                    , csatolmany.TartalomHash
                                                    , Contentum.eRecord.BaseUtility.Iratok.GetAlszamString(irat)
                                                    , ev);

                Logger.Debug(String.Format("EREC_IraIratokService.CsatolmanyUpload documentService.UploadFromeRecordWithCTTCheckin elott. - uploadXmlStrParams: {0}", uploadXmlStrParams));
                result = documentService.UploadFromeRecordWithCTTCheckin
                    (
                       ExecParam
                       , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                       , csatolmany.Nev
                       , csatolmany.Tartalom
                       , krt_Dokumentumok
                       , uploadXmlStrParams
                    );
                Logger.Debug("EREC_IraIratokService.CsatolmanyUpload documentService.UploadFromeRecordWithCTTCheckin utan.");

            }


            if (result.IsError)
            {
                // hiba:
                log.WsEnd(ExecParam, result);
                throw new ResultException(result);
            }

            #region Eseménynaplózás

            Logger.Debug("Eseménynaplózás 1 indul.");


            // CR 3041  Helyettesítőt elrontotta  --nekrisz
            //if (String.IsNullOrEmpty(ExecParam.Felhasznalo_Id))
            //{
            //    Logger.Error("Ures a ExecParam.Felhasznalo_Id valtozo...");
            //}
            //else
            //{
            //    ExecParam.LoginUser_Id = ExecParam.Felhasznalo_Id;
            //}

            Result eventLogResult = null;
            KRT_EsemenyekService eventLogService = null;
            KRT_Esemenyek eventLogRecord = null;

            try
            {
                eventLogService = new KRT_EsemenyekService(this.dataContext);

                eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Dokumentumok", "DokumentumokNew").Record;

                //
                //  nem egeszen vilagos ezt miert is nem adja at nehany helyen....
                //  az emailnal pl ha ezt hozzaadtuk, akkor mukodott rendesen
                //  itt is hozzaadjuk a biztosag kedveert

                if (String.IsNullOrEmpty(ExecParam.Felhasznalo_Id))
                {
                    Logger.Error("Ures a ExecParam.Felhasznalo_Id valtozo, ezert nem lesz bejegyezve a DokumentumokNew esemeny!");
                }
                else
                {
                    eventLogRecord.Felhasznalo_Id_Login = (ExecParam.Helyettesites_Id != null) ? ExecParam.LoginUser_Id : ExecParam.Felhasznalo_Id;
                }

                eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);

                if (!String.IsNullOrEmpty(eventLogResult.ErrorCode))
                {
                    Logger.Error(String.Format("Hibaval tert vissza az esemeny insert! - {0}\n{1}", eventLogResult.ErrorCode, eventLogResult.ErrorMessage));
                }
            }
            catch (Exception ex1)
            {
                Logger.Error(String.Format("Hiba az eseménynaplózásban: {0}\nStack: {1}\nSource: {2}", ex1.Message, ex1.StackTrace, ex1.Source));
            }

            #endregion


            #region Eredmény feldolgozása

            // result.Record -ban jön vissza néhány dolog, pl. hogy kell-e új csatolmány bejegyzés, vagy csak update kell
            // (ha volt már egy ugyanilyen nevű csatolmány, akkor nem jött létre új KRT_Dokumentumok bejegyzés, így nem kell
            // új EREC_Csatolmanyok bejegyzés sem)

            /// A visszaadott xml string: 
            /// <result>
            /// <dokumentumFullUrl>XXXXXXXXXXXX</dokumentumFullUrl>
            /// <tortentUjKrtDokBejegyzes>XXXXXXXXXXXXX</tortentUjKrtDokBejegyzes>
            /// <csatolmanyId>XXXXXXXXXXXX</csatolmanyId>
            /// <csatolmanyVerzio>XXXXXXXXXXXX</csatolmanyVerzio>
            /// </result>


            Logger.Debug("EREC_IraIratokService.CsatolmanyUpload feltoltes ok.");

            bool csatolmanyUpdateKell = false;
            string csatolmanyIdToUpdate = String.Empty;
            string csatolmanyVerToUpdate = String.Empty;

            if (result.Record != null)
            {
                string resultXml = result.Record.ToString();
                if (!string.IsNullOrEmpty(resultXml))
                {
                    Logger.Debug(String.Format("feltoltesbol kapott resultXml: {0}", resultXml));

                    XmlDocument resultXmlDoc = new XmlDocument();
                    resultXmlDoc.LoadXml(resultXml);

                    string tortentUjKrtDokBejegyzes = GetParam("tortentUjKrtDokBejegyzes", resultXmlDoc);
                    csatolmanyIdToUpdate = GetParam("csatolmanyId", resultXmlDoc);
                    csatolmanyVerToUpdate = GetParam("csatolmanyVerzio", resultXmlDoc);

                    if (string.IsNullOrEmpty(tortentUjKrtDokBejegyzes)
                        && !string.IsNullOrEmpty(csatolmanyIdToUpdate))
                    {
                        csatolmanyUpdateKell = true;
                    }
                }
            }

            #endregion

            EREC_CsatolmanyokService erec_CsatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);

            Logger.Debug(String.Format("csatolmanyUpdateKell: {0}", csatolmanyUpdateKell));

            if (csatolmanyUpdateKell)
            {
                #region update elotti csatolmany objektum lekeres, ha document_id ures

                if (String.IsNullOrEmpty(Record.Dokumentum_Id))
                {
                    Logger.Debug("update elotti csatolmany objektum lekeres, ha document_id ures");

                    ExecParam exPar = ExecParam.Clone();
                    exPar.Record_Id = csatolmanyIdToUpdate;
                    Result resultCsatSelect = erec_CsatolmanyokService.Get(exPar);

                    if (!String.IsNullOrEmpty(resultCsatSelect.ErrorCode))
                    {
                        Logger.Error(String.Format("csatolmany lekeres soran hiba: {0}\nErrorMessage: {1}\nErrorDetail: {2}", resultCsatSelect.ErrorCode, resultCsatSelect.ErrorMessage, resultCsatSelect.ErrorDetail));
                        throw new ResultException(resultCsatSelect);
                    }

                    if (resultCsatSelect.Record == null)
                    {
                        Logger.Error(String.Format("csatolmany lekeres soran hiba: null a csatolmany select resultjaban visszakapott Record mezo!"));
                        resultCsatSelect = new Result();
                        resultCsatSelect.ErrorCode = "CSAUPCSASZE001";
                        resultCsatSelect.ErrorMessage = "Hiba a feltöltött csatolmány bejegyzés visszaellenőrzésekor! Az eredményobjektumban visszakapott Record mező értéke null.";
                        throw new ResultException(resultCsatSelect);
                    }

                    Record = (EREC_Csatolmanyok)resultCsatSelect.Record;
                }

                #endregion

                #region EREC_Csatolmanyok UPDATE

                Logger.Debug(String.Format("erec csat update"));

                Record.Base.Ver = csatolmanyVerToUpdate;
                Record.Base.Updated.Ver = true;

                ExecParam execParam_update = ExecParam.Clone();
                execParam_update.Record_Id = csatolmanyIdToUpdate;

                result = erec_CsatolmanyokService.Update(execParam_update, Record);
                result.CheckError();

                #endregion
            }
            else
            {
                #region EREC_Csatolmanyok INSERT

                Logger.Debug(String.Format("erec csat insert"));

                Record.Dokumentum_Id = result.Uid;
                Record.Updated.Dokumentum_Id = true;

                #region Fődokumentum meglétének ellenőrzése
                if (String.IsNullOrEmpty(Record.DokumentumSzerep))
                {
                    Logger.Debug(String.Format("Fődokumentum keresés indítás"));
                    Result result_fodoku_search = null;
                    EREC_CsatolmanyokSearch foduko_search = new EREC_CsatolmanyokSearch();
                    foduko_search.DokumentumSzerep.Value = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
                    foduko_search.DokumentumSzerep.Operator = Query.Operators.equals;
                    foduko_search.TopRow = 1;
                    foduko_search.IraIrat_Id.Value = Record.IraIrat_Id;
                    foduko_search.IraIrat_Id.Operator = Query.Operators.equals;
                    ExecParam execParam_fodoku_search = ExecParam.Clone();
                    result_fodoku_search = erec_CsatolmanyokService.GetAll(execParam_fodoku_search, foduko_search);

                    if (result_fodoku_search.IsError)
                    {
                        Logger.Error(String.Format("Fődokumentum keresés hibára futott"));
                    }
                    else if (result_fodoku_search.Ds.Tables[0].Rows.Count > 0)
                    {
                        Logger.Debug(String.Format("Van már fődokumentum"));
                    }
                    else
                    {
                        Logger.Debug(String.Format("Nincs még fődokumentum, az új fájlt beállítjuk Fődokumentumnak"));
                        Record.DokumentumSzerep = "01";
                        Record.Updated.DokumentumSzerep = true;
                    }

                }
                #endregion


                result = erec_CsatolmanyokService.Insert(ExecParam, Record);
                result.CheckError();

                #endregion

                //#region Eseménynaplózás
                //    eventLogService = new KRT_EsemenyekService(this.dataContext);

                //    eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_Csatolmanyok", "New").Record;

                //    eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
                //#endregion

            }

            #region ucm
            // BUG_12019
            try
            {
                SetUCMJogosultsag(ExecParam, Record.IraIrat_Id);
            }
            catch (Exception x)
            {
                Logger.Error("SetUCMJogosultsag hiba: " + x.Message);
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        //finally
        //{
        //    dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        //}

        Logger.Debug("Irat.CsatolmanyUpload vege ok.");

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Az xmlStrParambol visszaadja a megadott nevu parameter erteket.
    /// </summary>
    /// <param name="pname">A parameter neve.</param>
    /// <param name="xdoc">Az xmlStrParams XmlDockent.</param>
    /// <returns></returns>
    private string GetParam(string pname, XmlDocument xdoc)
    {
        string _retval = String.Empty;

        if (xdoc == null) return _retval;

        string xpath = String.Format("//{0}", pname);

        if (xdoc.SelectNodes(xpath).Count == 1)
        {
            try
            {
                XmlNode xn = xdoc.SelectSingleNode(xpath);
                if (xn != null)
                {
                    _retval = Convert.ToString(xn.InnerText);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("DocumentService.GetParam hiba!");
                Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error(String.Format("panem: {0}\nxmlDoc: {1}", pname, xdoc.OuterXml.ToString()));
            }
        }

        return _retval;
    }


    #region GetIratHierarchia

    /// <summary>
    /// Visszaadja a Result.Record-ban az iratot, a felette lévő ügyiratdarabot illetve ügyiratot egy IratHierarchia típusú objektum formájában    
    /// </summary>
    /// <param name="execParam">execParam.Record_Id -ban kell megadni az irat Id-ját</param>
    /// <returns></returns>
    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(IratHierarchia))]
    public Result GetIratHierarchia(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            IratHierarchia iratHierarchia = new IratHierarchia();

            EREC_IraIratok iratObj = null;
            EREC_UgyUgyiratdarabok ugyiratDarabObj = null;
            EREC_UgyUgyiratok ugyiratObj = null;

            result = this.GetObjectsHierarchyByIrat(execParam, execParam.Record_Id
                , out iratObj, out ugyiratDarabObj, out ugyiratObj);

            iratHierarchia.IratObj = iratObj;
            iratHierarchia.UgyiratDarabObj = ugyiratDarabObj;
            iratHierarchia.UgyiratObj = ugyiratObj;

            result.Record = iratHierarchia;

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    /// <summary>
    /// Visszaadja a Result.Record-ban az iratot, a felette lévő ügyiratdarabot illetve ügyiratot egy IratHierarchia típusú objektum formájában    
    /// Jogosultságellenőrzést végez
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="jogszint"></param>
    /// <returns></returns>
    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(IratHierarchia))]
    public Result GetIratHierarchiaWithRightCheck(ExecParam execParam, char jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            IratHierarchia iratHierarchia = new IratHierarchia();

            EREC_IraIratok iratObj = null;
            EREC_UgyUgyiratdarabok ugyiratDarabObj = null;
            EREC_UgyUgyiratok ugyiratObj = null;

            result = this.GetObjectsHierarchyByIrat(execParam, execParam.Record_Id, true, jogszint
                , out iratObj, out ugyiratDarabObj, out ugyiratObj);

            iratHierarchia.IratObj = iratObj;
            iratHierarchia.UgyiratDarabObj = ugyiratDarabObj;
            iratHierarchia.UgyiratObj = ugyiratObj;

            result.Record = iratHierarchia;

            #region Eseménynaplózás
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_IraIratok", "IraIratView").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }



    /// <summary>
    /// Segédeljárás: irat id-ja alapján visszaadja a hierarchiában szereplő
    /// négy rekord (irat, ügyiratdarab, ügyirat) üzleti objektumát
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratPeldany_Id"></param>
    /// <param name="erec_PldIratPeldanyok"></param>
    /// <param name="erec_IraIratok"></param>
    /// <param name="erec_UgyUgyiratdarabok"></param>
    /// <param name="erec_UgyUgyiratok"></param>
    public Result GetObjectsHierarchyByIrat(ExecParam execParam, string irat_Id
        , out EREC_IraIratok erec_IraIratok, out EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok, out EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        return GetObjectsHierarchyByIrat(execParam, irat_Id, false, ' ', out erec_IraIratok, out erec_UgyUgyiratdarabok, out erec_UgyUgyiratok);
    }



    public Result GetObjectsHierarchyByIrat(ExecParam execParam, string irat_Id, bool withRightCheck, char jogszint
        , out EREC_IraIratok erec_IraIratok, out EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok, out EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        erec_IraIratok = null;
        erec_UgyUgyiratdarabok = null;
        erec_UgyUgyiratok = null;

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            #region Irat Get
            EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);
            ExecParam execParam_irat = execParam.Clone();
            execParam_irat.Record_Id = irat_Id;

            Result result_irat = (withRightCheck) ?
                service_iratok.GetWithRightCheck(execParam_irat, jogszint)
                : service_iratok.Get(execParam_irat);

            if (!String.IsNullOrEmpty(result_irat.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_irat);
            }

            erec_IraIratok = (EREC_IraIratok)result_irat.Record;
            #endregion

            #region Ügyiratdarab Get
            EREC_UgyUgyiratdarabokService service_ugyiratdarab = new EREC_UgyUgyiratdarabokService(this.dataContext);
            ExecParam execParam_ugyiratDarab = execParam.Clone();
            execParam_ugyiratDarab.Record_Id = erec_IraIratok.UgyUgyIratDarab_Id;

            Result result_ugyiratDarab = service_ugyiratdarab.Get(execParam_ugyiratDarab);
            if (!String.IsNullOrEmpty(result_ugyiratDarab.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratDarab);
            }

            erec_UgyUgyiratdarabok = (EREC_UgyUgyiratdarabok)result_ugyiratDarab.Record;
            #endregion

            #region Ügyirat Get
            EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_ugyirat = execParam.Clone();
            execParam_ugyirat.Record_Id = erec_UgyUgyiratdarabok.UgyUgyirat_Id;

            Result result_ugyirat = service_ugyiratok.Get(execParam_ugyirat);
            if (!String.IsNullOrEmpty(result_ugyirat.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyirat);
            }

            erec_UgyUgyiratok = (EREC_UgyUgyiratok)result_ugyirat.Record;
            #endregion

            result = result_irat;

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            // továbbdobjuk a hibát:
            throw new ResultException(result);
        }

        return result;
    }

    #endregion

    #region Ügyirat iratai
    /// <summary>
    /// Visszaadja egy adott ügyirat minden iratának azonosítóját.
    /// </summary>
    /// <param name="execParam">Naplózáshoz szükséges adatok.</param>
    /// <param name="Ugyirat_Id">Az ügyirat azonosítója, melynek iratait keressük.</param>
    /// <returns>Az iratok azonosítóinak, mint stringeknek tömbje a result Record-ban, vagy hiba.</returns>
    public Result GetAllIratIdsByUgyirat(ExecParam execParam, String Ugyirat_Id)
    {
        string[] IraIratok_Ids = null;

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_IraIratokSearch search = new EREC_IraIratokSearch(true);
            search.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = Ugyirat_Id;
            search.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;

            Result result_IraIratokGetAll = GetAllWithExtension(execParam, search);

            if (!String.IsNullOrEmpty(result_IraIratokGetAll.ErrorCode))
            {
                throw new ResultException(result_IraIratokGetAll);
            }

            if (result_IraIratokGetAll.Ds.Tables[0].Rows.Count > 0)
            {
                IraIratok_Ids = new string[result_IraIratokGetAll.Ds.Tables[0].Rows.Count];
                int i = 0;

                foreach (DataRow row in result_IraIratokGetAll.Ds.Tables[0].Rows)
                {
                    string currentId = row["Id"].ToString();
                    if (!String.IsNullOrEmpty(currentId))
                    {
                        IraIratok_Ids[i] = currentId;
                        i++;
                    }
                }

            }

            result.Record = IraIratok_Ids;
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            // továbbdobjuk a hibát:
            throw new ResultException(result);
        }

        return result;

    }

    #endregion

    /// <summary>
    /// Irat sztornózása
    /// </summary>
    /// <param name="execParam">ExecParam.RecordId-ban az irat Id-ja, ha erec_irairatok null</param>
    /// <param name="erec_irairatok" remarks="nullable">A sztornózandó iratobjektum</param>
    /// <param name="ugyiratId" remarks="nullable">A sztornozandó irat ügyiratának id-ja</param>
    /// <returns>Irat Update Result</returns>
    public Result Sztornozas(ExecParam execParam, EREC_IraIratok erec_irairatok, string ugyiratId)
    {
        Logger.Debug("Erec_IraIratok Sztornozas start", execParam);

        Result result = new Result();

        #region erec_iratok lekerese
        if (erec_irairatok == null)
        {
            Logger.Debug("erec_irairatok == null");

            Arguments args = new Arguments();
            args.Add(new Argument("RecordId", execParam.Record_Id, ArgumentTypes.Guid));
            args.ValidateArguments();

            Logger.Debug("Irat lekerese start");
            ExecParam xpmGet = execParam.Clone();
            Result resGet = this.Get(execParam);
            if (resGet.IsError)
            {
                Logger.Error("Irat lekerese hiba", xpmGet, resGet);
                throw new ResultException(resGet);
            }
            Logger.Debug("Irat lekerese end");
            erec_irairatok = (EREC_IraIratok)resGet.Record;
        }
        #endregion

        Logger.Debug("Irat update start");
        // IRAT UPDATE:
        erec_irairatok.Updated.SetValueAll(false);
        erec_irairatok.Base.Updated.SetValueAll(false);
        erec_irairatok.Base.Updated.Ver = true;

        // módosítani kell: SztornirozasDat, Allapot
        if ((System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"] ?? "").ToUpper() != "MIGRATION" || String.IsNullOrEmpty(erec_irairatok.SztornirozasDat))
        {
            erec_irairatok.SztornirozasDat = DateTime.Now.ToString();
        }
        erec_irairatok.Updated.SztornirozasDat = true;

        erec_irairatok.Allapot = KodTarak.IRAT_ALLAPOT.Sztornozott;
        erec_irairatok.Updated.Allapot = true;

        ExecParam execParam_iratUpdate = execParam.Clone();
        execParam_iratUpdate.Record_Id = erec_irairatok.Id;

        Result result_iratUpdate = this.Update(execParam_iratUpdate, erec_irairatok);
        if (!String.IsNullOrEmpty(result_iratUpdate.ErrorCode))
        {
            // hiba:
            Logger.Error("Irat update hiba", execParam_iratUpdate, result_iratUpdate);
            throw new ResultException(result_iratUpdate);
        }

        Logger.Debug("Irat update end");

        result = result_iratUpdate;

        #region UgyiratId meghatarozasa
        if (String.IsNullOrEmpty(ugyiratId))
        {
            if (ugyiratId == null)
                Logger.Debug("UgyiratId NULL");
            else
                Logger.Debug("UgyiratId EMPTY");

            Logger.Debug("UgyiratDarab lekerese start");
            ExecParam xpmUgyiratDarab = execParam.Clone();
            xpmUgyiratDarab.Record_Id = erec_irairatok.UgyUgyIratDarab_Id;
            EREC_UgyUgyiratdarabokService svcUgyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);
            Result resUgyiratDarab = svcUgyiratDarabok.Get(xpmUgyiratDarab);
            if (resUgyiratDarab.IsError)
            {
                Logger.Error("UgyiratDarab lekerese hiba", xpmUgyiratDarab, resUgyiratDarab);
                throw new ResultException(resUgyiratDarab);
            }
            EREC_UgyUgyiratdarabok ugyiratDarab = (EREC_UgyUgyiratdarabok)resUgyiratDarab.Record;
            Logger.Debug("UgyiratDarab lekerese end, ugyiratId: " + ugyiratDarab.UgyUgyirat_Id);
            ugyiratId = ugyiratDarab.UgyUgyirat_Id;
        }
        #endregion

        EREC_UgyUgyiratokService svcUgyiratok = new EREC_UgyUgyiratokService(this.dataContext);

        #region DecrementIratszam
        if (!IsMunkairat(erec_irairatok))
        {
            Logger.Debug("DecrementIratszam start");
            ExecParam xpmDecrementIratszam = execParam.Clone();
            int ujIratszam = svcUgyiratok.DecrementIratszam(xpmDecrementIratszam, ugyiratId);
            Logger.Debug(String.Format("DecrementIratszam end, ujIratszam: {0}", ujIratszam));
        }
        #endregion

        #region ügyirat jellegének állítása, ha szükséges

        ExecParam ugyiratGetExecParam = execParam.Clone();
        ugyiratGetExecParam.Record_Id = ugyiratId;

        Result ugyiratGetResult = svcUgyiratok.Get(ugyiratGetExecParam);

        if (string.IsNullOrEmpty(ugyiratGetResult.ErrorCode) && ugyiratGetResult.Record != null)
        {
            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)ugyiratGetResult.Record;

            ugyirat = svcUgyiratok.SetUgyiratJellegEsUgyintezesModjaByIratokAndSzereltek(execParam, ugyirat, erec_irairatok.Id);

            Result ugyiratUpdateResult = svcUgyiratok.Update(ugyiratGetExecParam, ugyirat);
            if (!string.IsNullOrEmpty(ugyiratUpdateResult.ErrorCode))
                throw new ResultException(ugyiratUpdateResult);
        }

        #endregion ügyirat jellegének állítása, ha szükséges

        Logger.Debug("Erec_IraIratok Sztornozas end", execParam);
        return result;
    }

    private static bool IsMunkairat(EREC_IraIratok erec_irairatok)
    {
        Logger.Debug("IsMunkairat");
        Logger.Debug(String.Format("Alszam: {0}", erec_irairatok.Alszam ?? "NULL"));

        Contentum.eRecord.BaseUtility.Iratok.Statusz statusz = Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(erec_irairatok);

        bool isMunkairat = statusz.Munkairat;

        Logger.Debug("IsMunkairat return: " + isMunkairat);

        return isMunkairat;
    }

    /// <summary>
    /// Megállapítja egy ügyirat Jellegét és UgyintezesAlapját a benne lévő iratokból
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="except_IraIrat_Id">Ezt az iratot figyelmen kívül hagyja</param>
    /// <param name="_EREC_UgyUgyiratok">A kérdéses ügyirat (szükség van az Id, Jelleg mezőire)</param>
    /// <returns>A paraméterben megadott _EREC_UgyUgyiratok modosított Jelleg és UgyintezesModja mezőkkel</returns>
    private EREC_UgyUgyiratok SetUgyiratAdathordozoTipusa(ExecParam execParam, string except_IraIrat_Id, EREC_UgyUgyiratok _EREC_UgyUgyiratok)
    {
        EREC_UgyUgyiratok erec_UgyUgyiratok = _EREC_UgyUgyiratok;

        if (erec_UgyUgyiratok.Jelleg == KodTarak.UGYIRAT_JELLEG.Vegyes)
        {
            EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();

            iratokSearch.Allapot.Value = KodTarak.IRAT_ALLAPOT.Atiktatott + "," + KodTarak.IRAT_ALLAPOT.Sztornozott;
            iratokSearch.Allapot.Operator = Query.Operators.notinner;

            if (!string.IsNullOrEmpty(except_IraIrat_Id))
            {
                iratokSearch.Id.Value = except_IraIrat_Id;
                iratokSearch.Id.Operator = Query.Operators.notequals;
            }

            iratokSearch.Ugyirat_Id.Value = erec_UgyUgyiratok.Id;
            iratokSearch.Ugyirat_Id.Operator = Query.Operators.equals;

            Result jellegGetAllResult = this.GetAll(execParam.Clone(), iratokSearch);

            if (string.IsNullOrEmpty(jellegGetAllResult.ErrorCode) && jellegGetAllResult.Ds != null
                && jellegGetAllResult.Ds.Tables[0].Rows.Count > 0)
            {
                string jelleg = jellegGetAllResult.Ds.Tables[0].Rows[0]["AdathordozoTipusa"].ToString();

                for (int i = 0; i < jellegGetAllResult.Ds.Tables[0].Rows.Count; i++)
                {
                    if (jellegGetAllResult.Ds.Tables[0].Rows[i]["AdathordozoTipusa"].ToString() != jelleg)
                    {
                        break;
                    }

                    if (i == jellegGetAllResult.Ds.Tables[0].Rows.Count - 1)
                    {
                        erec_UgyUgyiratok.Jelleg = jelleg == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir ? KodTarak.UGYIRAT_JELLEG.Elektronikus : KodTarak.UGYIRAT_JELLEG.Papir;
                        erec_UgyUgyiratok.Updated.Jelleg = true;

                        // Ha az ügyintézés módja != az új jelleggel, UPDATE
                        if (erec_UgyUgyiratok.UgyintezesModja != jelleg)
                        {
                            erec_UgyUgyiratok.UgyintezesModja = jelleg;
                            erec_UgyUgyiratok.Updated.UgyintezesModja = true;
                        }
                    }
                }
            }
        }

        return erec_UgyUgyiratok;
    }

    // BLG_619
    [WebMethod()]
    public Result GetIratFelelosSzervezetKod(ExecParam execParam, string iratId)
    {
        Logger.Debug("Erec_IraIratokService GetIratFelelosSzervezetKod start");
        Logger.Debug(String.Format("IratId: {0}", iratId));
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetIratFelelosSzervezetKod(execParam, iratId);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error("Erec_IraIratokService GetIratFelelosSzervezetKod hiba", execParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("Erec_IraIratokService GetIratFelelosSzervezetKod end");

        return result;
    }

    /// <summary>
    /// A kiválasztott irat elintezetté nyilvánítása
    /// </summary>
    /// <param name="execParam">execParam</param>
    /// <param name="IratId">Kiválasztott irat Id-ja</param>
    /// <param name="Visszavonas">true ha visszavonás</param>
    [WebMethod()]
    public Result Elintezes(ExecParam execParam, EREC_IraIratok erec_IraIratok, EREC_HataridosFeladatok erec_HataridosFeladat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Debug("Irat Elintezetté nyilvánítása", execParam);
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (erec_IraIratok == null)
            {
                throw new ResultException(52240);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            #region Ellenörzés, hogy elintézhető-e

            var resultCheck = CheckIratElintezheto(execParam, erec_IraIratok.Id);
            resultCheck.CheckError();

            #endregion

            #region Update

            // Elintézetté nyílvánítás
            erec_IraIratok.Elintezett = "1";
            erec_IraIratok.Updated.Elintezett = true;
            if ((System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"] ?? String.Empty).ToUpper() != "MIGRATION" || String.IsNullOrEmpty(erec_IraIratok.IntezesIdopontja))
            {
                erec_IraIratok.IntezesIdopontja = DateTime.Now.ToString();
            }
            erec_IraIratok.Updated.IntezesIdopontja = true;
            ExecParam execParam_Update = execParam.Clone();
            execParam_Update.Record_Id = erec_IraIratok.Id;

            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRAT_INTEZKEDESREATVETEL_ELERHETO, false))
            {
                erec_IraIratok.Allapot = KodTarak.IRAT_ALLAPOT.Elintezett;
                erec_IraIratok.Updated.Allapot = true;
            }

            result = this.Update(execParam_Update, erec_IraIratok);
            if (result.IsError)
            {
                // hiba:
                throw new ResultException(result);
            }

            this.AddEREC_HataridosFeladatok(execParam, erec_IraIratok, erec_HataridosFeladat);

            #endregion

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, result.Uid
                , "EREC_IraIratok", "IratElintezes").Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ellenőrzés, hogy a megadott irat elintézhető-e
    /// Ha nem, a Result objektumba beleírva a hiba.
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    [WebMethod]
    public Result CheckIratElintezheto(ExecParam execParam, string iratId)
    {
        Result result = new Result();

        try
        {
            execParam.Record_Id = iratId;
            var resultGet = this.Get(execParam);
            resultGet.CheckError();

            EREC_IraIratok irat = resultGet.Record as EREC_IraIratok;

            ExecParam ep_ugyirat = execParam.Clone();
            ep_ugyirat.Record_Id = irat.Ugyirat_Id.ToString();
            EREC_UgyUgyiratokService service = new EREC_UgyUgyiratokService(this.dataContext);
            Result result_ugyirat = service.Get(ep_ugyirat);
            result_ugyirat.CheckError();

            EREC_UgyUgyiratok erec_UgyUgyiratok = result_ugyirat.Record as EREC_UgyUgyiratok;
            string felhasznaloSajatCsoportId = Csoportok.GetFelhasznaloSajatCsoportId(ep_ugyirat.Clone());
            if (erec_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != felhasznaloSajatCsoportId)
            {
                #region BLG_7317
                if (string.IsNullOrEmpty(irat.FelhasznaloCsoport_Id_Ugyintez))
                    throw new ResultException(80012);
                else if (irat.FelhasznaloCsoport_Id_Ugyintez != felhasznaloSajatCsoportId)
                    throw new ResultException(800120);
                #endregion
            }

            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRAT_INTEZKEDESREATVETEL_ELERHETO, false))
            {
                if (irat.Allapot != KodTarak.IRAT_ALLAPOT.IntezkedesAlatt)
                {
                    throw new ResultException(Contentum.eRecord.BaseUtility.Iratok.ErrorDetailMessages.IratCsakIntezkedesAlattiElintezheto);
                }
            }

            if (Rendszerparameterek.GetBoolean(execParam, "UGYIRAT_ELINTEZES_FELTETEL", false))
            {
                int errorCode;
                if (!ElintezhetoExtended(execParam, irat, out errorCode))
                {
                    // Nem nyilvánítható elintézetté az ügyirat:
                    throw new ResultException(errorCode);
                }
            }

            EREC_PldIratPeldanyokService peldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
            peldanyokService.CeckIratPeldanyaiCimzes(execParam, irat);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }

        return result;
    }

    internal bool ElintezhetoExtended(ExecParam execParam, EREC_IraIratok irat, out int errorCode)
    {
        bool ret = true;
        errorCode = 0;
        // BUG_8028
        //if (irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso
        //    || irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
        //{
        if (irat.Allapot != KodTarak.IRAT_ALLAPOT.Atiktatott
            && irat.Allapot != KodTarak.IRAT_ALLAPOT.Sztornozott
            && irat.Allapot != KodTarak.IRAT_ALLAPOT.Felszabaditva)
        {
            EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            if (!ugyiratokService.IratStatisztikaKitoltve(execParam, irat.Id))
            {
                //Az irat nem nyilvánítható elintézetté! Az irat statisztika nincs kitöltve!
                errorCode = 80416;
                return false;
            }
        }
        //}

        return ret;
    }

    /// <summary>
    /// A kiválasztott irat elintezetté nyilvánítása
    /// </summary>
    /// <param name="execParam">execParam</param>
    /// <param name="IratId">Kiválasztott irat Id-ja</param>
    /// <param name="Visszavonas">true ha visszavonás</param>
    [WebMethod()]
    public Result ElintezesVisszavonasa(ExecParam execParam, string iratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Debug("Irat Elintezetté nyilvánítás visszavonása", execParam);
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
        try
        {
            if (String.IsNullOrEmpty(iratId))
            {
                throw new ResultException(52240);
            }
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            execParam.Record_Id = iratId;

            result = this.Get(execParam);

            if (result.IsError)
            {
                // hiba:
                throw new ResultException(result);
            }
            EREC_IraIratok erec_IraIratok = result.Record as EREC_IraIratok;

            #region Ellenörzés, hogy őrzője-e az iratnak
            ExecParam ep_ugyirat = execParam.Clone();
            ep_ugyirat.Record_Id = erec_IraIratok.Ugyirat_Id.ToString();
            EREC_UgyUgyiratokService service = new EREC_UgyUgyiratokService(this.dataContext);
            Result result_ugyirat = service.Get(ep_ugyirat);
            if (result_ugyirat.IsError)
            {
                // hiba:
                throw new ResultException(result);
            }
            EREC_UgyUgyiratok erec_UgyUgyiratok = result_ugyirat.Record as EREC_UgyUgyiratok;
            string felhasznaloSajatCsoportId = Csoportok.GetFelhasznaloSajatCsoportId(ep_ugyirat.Clone());
            if (erec_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != felhasznaloSajatCsoportId)
            {
                #region BLG_7317
                if (string.IsNullOrEmpty(erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez))
                    throw new ResultException(80012);
                else if (erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez != felhasznaloSajatCsoportId)
                    throw new ResultException(800120);
                #endregion
            }
            #endregion

            #region Update
            erec_IraIratok.Updated.SetValueAll(false);
            erec_IraIratok.Base.Updated.SetValueAll(false);

            erec_IraIratok.Base.Updated.Ver = true;

            // Elintézetté nyílvánítás visszavonása
            erec_IraIratok.Elintezett = "0";
            erec_IraIratok.Updated.Elintezett = true;

            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRAT_INTEZKEDESREATVETEL_ELERHETO, false))
            {
                erec_IraIratok.Allapot = KodTarak.IRAT_ALLAPOT.IntezkedesAlatt;
                erec_IraIratok.Updated.Allapot = true;
            }

            // Intézés időpontját kitöröljük:
            erec_IraIratok.IntezesIdopontja = "<null>";
            erec_IraIratok.Updated.IntezesIdopontja = true;

            ExecParam execParam_Update = execParam.Clone();
            execParam_Update.Record_Id = iratId;

            result = this.Update(execParam_Update, erec_IraIratok);
            if (result.IsError)
            {
                // hiba:
                throw new ResultException(result);
            }

            #endregion

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);


            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, result.Uid
                , "EREC_IraIratok", "IratElintezesVisszavonas").Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// A kiválasztott irat átvétele intézkedésre, sakkóra állítással
    /// </summary>
    /// <param name="execParam">execParam</param>
    /// <param name="IratId">Kiválasztott irat Id-ja</param>
    [WebMethod()]
    public Result AtvetelIntezkedesre(ExecParam execParam, string[] iratIds, string sakkoraStatus, string okKod)
    {
        var log = Log.WsStart(execParam, Context, GetType());
        Logger.Debug("Irat átvétel intézkedésre", execParam);

        Result result = new Result();

        if (!Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRAT_INTEZKEDESREATVETEL_ELERHETO, false))
        {
            result.ErrorCode = "IRAT_INTEZKEDESREATVETEL_ELERHETO_FALSE";
            result.ErrorMessage = "Az irat intézkedésre átvetel funkció a rendszerben nincs engedélyezve.";
            return result;
        }

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            foreach (var iratId in iratIds)
            {
                if (String.IsNullOrEmpty(iratId))
                {
                    continue; // throw new ResultException(52240);
                }

                execParam.Record_Id = iratId;

                result = this.Get(execParam);
                if (result.Ds == null)
                {
                    result.Ds = new DataSet();
                    result.Ds.Tables.Add();
                    result.Ds.Tables[0].Columns.Add("Id");
                }

                if (result.IsError)
                {
                    result.Ds.Tables[0].Rows.Add(iratId);
                    throw new ResultException(result);
                }
                EREC_IraIratok erec_IraIratok = result.Record as EREC_IraIratok;

                #region Ellenörzés, hogy őrzője-e az iratnak
                ExecParam ep_ugyirat = execParam.Clone();
                ep_ugyirat.Record_Id = erec_IraIratok.Ugyirat_Id.ToString();
                EREC_UgyUgyiratokService service = new EREC_UgyUgyiratokService(this.dataContext);
                Result result_ugyirat = service.Get(ep_ugyirat);
                if (result_ugyirat.IsError)
                {
                    result.Ds.Tables[0].Rows.Add(iratId);
                    throw new ResultException(result);
                }
                EREC_UgyUgyiratok erec_UgyUgyiratok = result_ugyirat.Record as EREC_UgyUgyiratok;
                string felhasznaloSajatCsoportId = Csoportok.GetFelhasznaloSajatCsoportId(ep_ugyirat.Clone());
                if (erec_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != felhasznaloSajatCsoportId)
                {
                    result.Ds.Tables[0].Rows.Add(iratId);

                    #region BLG_7317
                    if (string.IsNullOrEmpty(erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez))
                        throw new ResultException(80012);
                    else if (erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez != felhasznaloSajatCsoportId)
                        throw new ResultException(800120);
                    #endregion
                }
                #endregion

                // iratpéldányok átvétele sakkóra állítással
                result = IratAtvetelUgyintezesreSakkoraAllitassal(execParam, iratId, sakkoraStatus, okKod);
                if (result.IsError)
                {
                    result.Ds.Tables[0].Rows.Add(iratId);
                    throw new ResultException(result);
                }

                #region Update
                erec_IraIratok.Updated.SetValueAll(false);
                erec_IraIratok.Base.Updated.SetValueAll(false);

                erec_IraIratok.Base.Updated.Ver = true;

                // Intézkedés alatt állapot beállítása
                erec_IraIratok.Allapot = KodTarak.IRAT_ALLAPOT.IntezkedesAlatt;
                erec_IraIratok.Updated.Allapot = true;

                // BUG_11120 Elintézett flag leszedése
                erec_IraIratok.Elintezett = "0";
                erec_IraIratok.Updated.Elintezett = true;

                // BUG_11120 IratHatasaUgyintezesre mezőt ne állítsuk:
                /* Sakkóra default érték beállítása, ha szükséges
                var isSakkora = Rendszerparameterek.Get(execParam, Rendszerparameterek.UGYINTEZESI_IDO_FELFUGGESZTES) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY.ToString();
                if (isSakkora)
                {
                    erec_IraIratok.IratHatasaUgyintezesre = Rendszerparameterek.Get(execParam, Rendszerparameterek.IRAT_INTEZKEDESREATVETEL_SAKKORA_DEFAULT);
                    erec_IraIratok.Updated.IratHatasaUgyintezesre = true;
                }*/

                //
                ExecParam execParam_Update = execParam.Clone();
                execParam_Update.Record_Id = iratId;

                result = this.Update(execParam_Update, erec_IraIratok);
                if (result.IsError)
                {
                    result.Ds.Tables[0].Rows.Add(iratId);
                    throw new ResultException(result);
                }

                #endregion

                #region Eseménynaplózás
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_IraIratok.Id
                    , "EREC_IraIratok", "IratAtvetelIntezkedesre").Record;

                eventLogService.Insert(execParam, eventLogRecord);
                #endregion

                dataContext.CommitIfRequired(isTransactionBeginHere);
            }
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private void AddEREC_HataridosFeladatok(ExecParam execParam, EREC_IraIratok erec_IraIratok, EREC_HataridosFeladatok erec_HataridosFeladat)
    {
        if (erec_HataridosFeladat != null)
        {
            // Irat kezelési feljegyzés INSERT
            EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
            ExecParam execParam_kezfeljegyzesek = execParam.Clone();

            erec_HataridosFeladat.Obj_Id = erec_IraIratok.Id;
            erec_HataridosFeladat.Updated.Obj_Id = true;

            erec_HataridosFeladat.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;
            erec_HataridosFeladat.Updated.Obj_type = true;

            if (!String.IsNullOrEmpty(erec_IraIratok.Azonosito))
            {
                erec_HataridosFeladat.Azonosito = erec_IraIratok.Azonosito;
                erec_HataridosFeladat.Updated.Azonosito = true;
            }

            Result result_kezfeljegyzes = erec_HataridosFeladatokService.Insert(execParam_kezfeljegyzesek, erec_HataridosFeladat);

            if (!String.IsNullOrEmpty(result_kezfeljegyzes.ErrorCode))
            {
                Logger.Error("Kezelési feljegyzés létrehozásánál hiba", execParam, result_kezfeljegyzes);

                // hiba
                throw new ResultException(result_kezfeljegyzes);
            }
        }
    }

    [WebMethod]
    public Result GetCsatolmanyJogosultak(ExecParam execParam, string iratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Debug(String.Format("IratId: {0}", iratId));
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetCsatolmanyJogosultak(execParam, iratId, true);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error("Erec_IraIratokService GetCsatolmanyJogosultak hiba", execParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);

        return result;
    }

    [WebMethod]
    public Result GetCsatolmanyJogosultakOrokolt(ExecParam execParam, string iratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Debug(String.Format("IratId: {0}", iratId));
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetCsatolmanyJogosultak(execParam, iratId, false);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error("Erec_IraIratokService GetCsatolmanyJogosultak hiba", execParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);

        return result;
    }

    public void SetUCMJogosultsag(ExecParam execParam, string iratId)
    {
        //migráció módban ne fusson az ucm jogosultság állítás
        if ("MIGRATION".Equals(System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"], StringComparison.InvariantCultureIgnoreCase))
        {
            return;
        }

        Logger.Info(String.Format("EREC_IraIratokService.SetUCMJogosultsag: {0}", iratId));

        string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);

        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
        {
            ExecParam iratokExecParam = execParam.Clone();
            iratokExecParam.Record_Id = iratId;
            Result iratokResult = this.Get(iratokExecParam);

            if (iratokResult.IsError)
                throw new ResultException(iratokResult);

            EREC_IraIratok irat = iratokResult.Record as EREC_IraIratok;

            EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam ugyiratokExecParam = execParam.Clone();
            ugyiratokExecParam.Record_Id = irat.Ugyirat_Id;
            Result ugyiratokResult = ugyiratokService.Get(ugyiratokExecParam);

            if (ugyiratokResult.IsError)
                throw new ResultException(ugyiratokResult);

            EREC_UgyUgyiratok ugyirat = ugyiratokResult.Record as EREC_UgyUgyiratok;

            EREC_IraIktatoKonyvekService iktatokonyvekService = new EREC_IraIktatoKonyvekService(this.dataContext);
            ExecParam iktatokonyvekExecParam = execParam.Clone();
            iktatokonyvekExecParam.Record_Id = ugyirat.IraIktatokonyv_Id;
            Result iktatokonyvekResult = iktatokonyvekService.Get(iktatokonyvekExecParam);

            if (iktatokonyvekResult.IsError)
                throw new ResultException(iktatokonyvekResult);

            EREC_IraIktatoKonyvek iktatokonyv = iktatokonyvekResult.Record as EREC_IraIktatoKonyvek;

            string iktatohely = iktatokonyv.Iktatohely;
            string foszam = ugyirat.Foszam;
            string ev = iktatokonyv.Ev;
            string alszam = irat.Alszam;

            Result jogosultakResult = this.GetCsatolmanyJogosultak(execParam, iratId);

            if (jogosultakResult.IsError)
            {
                throw new ResultException(jogosultakResult);
            }

            List<string> userNevek = new List<string>();

            foreach (DataRow row in jogosultakResult.Ds.Tables[0].Rows)
            {
                userNevek.Add(row["UserNev"].ToString());
            }

            string jog_felh_nids = String.Join(",", userNevek.ToArray());

            UCMDocumentService service = eDocumentService.ServiceFactory.GetUCMDocumentService();
            Result res = service.SetIratJogosultak(execParam, jog_felh_nids, iktatohely, foszam, ev, alszam);

            if (res.IsError)
            {
                throw new ResultException(res);
            }
        }
    }

    #region TOBB IRAT PLD
    /// <summary>
    /// BelsoIratIktatasa_TobbIratPld
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_IraIktatoKonyvek_Id"></param>
    /// <param name="_EREC_UgyUgyiratok"></param>
    /// <param name="_EREC_UgyUgyiratdarabok"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_HataridosFeladatok"></param>
    /// <param name="_EREC_PldIratPeldanyok"></param>
    /// <param name="ip"></param>
    /// <returns></returns>
    [WebMethod(MessageName = "BelsoIratIktatasa_TobbIratPld", Description = "BelsoIratIktatasa Tobb Irat Pld")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result BelsoIratIktatasa_TobbIratPld(ExecParam _ExecParam,
                                    string _EREC_IraIktatoKonyvek_Id,
                                    EREC_UgyUgyiratok _EREC_UgyUgyiratok,
                                    EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                    EREC_IraIratok _EREC_IraIratok,
                                    EREC_HataridosFeladatok _EREC_HataridosFeladatok,
                                    EREC_PldIratPeldanyok[] _EREC_PldIratPeldanyok,
                                    IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Belső irat iktatása tobb irat pld Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            IktatottVagyMunkaanyag iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Iktatottanyag;
            // Ha az iktatási paraméterekben megadták, akkor munkaanyagként kell létrehozni:
            if (ip != null && ip.MunkaPeldany == true)
            {
                iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Munkaanyag;
            }

            result = IratIktatasaTobbIratPld(IktatasTipus.BelsoIratIktatas, NormalVagyAlszamraIktatas.NormalIktatas, iktatottVagyMunkaanyag,
                               _ExecParam, _EREC_IraIktatoKonyvek_Id, null, _EREC_UgyUgyiratok,
                               _EREC_UgyUgyiratdarabok, _EREC_IraIratok, _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip);

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            string funkcio = "BelsoIratIktatas";
            // ha munkapéldány létrehozás volt megadva
            if (ip != null && ip.MunkaPeldany == true)
            {
                funkcio = "IktatasElokeszites";
            }

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, result.Uid
                , "EREC_IraIratok", funkcio).Record;

            eventLogService.Insert(_ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }

    /// <summary>
    /// BelsoIratIktatasa_Alszamra_TobbIratPld
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_IraIktatoKonyvek_Id"></param>
    /// <param name="_EREC_UgyUgyiratok_Id"></param>
    /// <param name="_EREC_UgyUgyiratdarabok"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_HataridosFeladatok"></param>
    /// <param name="_EREC_PldIratPeldanyok"></param>
    /// <param name="ip"></param>
    /// <returns></returns>
    [WebMethod(MessageName = "BelsoIratIktatasa_Alszamra_TobbIratPld", Description = "BelsoIratIktatasa Alszamra Tobb Irat Pld")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result BelsoIratIktatasa_Alszamra_TobbIratPld(
                                          ExecParam _ExecParam,
                                          string _EREC_IraIktatoKonyvek_Id,
                                          string _EREC_UgyUgyiratok_Id,
                                          EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                                          EREC_IraIratok _EREC_IraIratok,
                                          EREC_HataridosFeladatok _EREC_HataridosFeladatok,
                                          EREC_PldIratPeldanyok[] _EREC_PldIratPeldanyok,
                                          IktatasiParameterek ip)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_ExecParam, Context, GetType());
        Logger.Debug("Belső irat iktatása alszámra tobb irat pld Start", _ExecParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Iktathat-e a luzer ebbe az ügyiratba

            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);

            ExecParam ugyiratGetExecParam = _ExecParam.Clone();
            ugyiratGetExecParam.Record_Id = _EREC_UgyUgyiratok_Id;

            Result ugyiratGetResult = ugyiratService.GetWithRightCheck(ugyiratGetExecParam, 'O');
            if (!string.IsNullOrEmpty(ugyiratGetResult.ErrorCode))
            {
                Result rsResult = new Result();
                rsResult.ErrorCode = "1";
                rsResult.ErrorMessage = "Nem iktathat ebbe az ügyiratba!";
                return rsResult;
            }

            #endregion Iktathat-e a luzer ebbe az ügyiratba

            IktatottVagyMunkaanyag iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Iktatottanyag;
            // Ha az iktatási paraméterekben megadták, akkor munkaanyagként kell létrehozni:
            if (ip != null && ip.MunkaPeldany == true)
            {
                iktatottVagyMunkaanyag = IktatottVagyMunkaanyag.Munkaanyag;
            }

            result = IratIktatasaTobbIratPld(IktatasTipus.BelsoIratIktatas, NormalVagyAlszamraIktatas.AlszamraIktatas, iktatottVagyMunkaanyag,
                _ExecParam, _EREC_IraIktatoKonyvek_Id, _EREC_UgyUgyiratok_Id, null, _EREC_UgyUgyiratdarabok,
                _EREC_IraIratok, _EREC_HataridosFeladatok, _EREC_PldIratPeldanyok, ip);

            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            string funkcio = "BelsoIratIktatasCsakAlszamra";
            // ha munkapéldány létrehozás volt megadva
            if (ip != null && ip.MunkaPeldany == true)
            {
                funkcio = "IktatasElokeszites";
            }

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, result.Uid
                , "EREC_IraIratok", funkcio).Record;

            eventLogService.Insert(_ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }

    /// <summary>
    /// IratIktatasa
    /// </summary>
    /// <param name="Bejovo_vagy_BelsoIratIktatas"></param>
    /// <param name="Normal_vagy_AlszamraIktatas"></param>
    /// <param name="Iktatott_vagy_Munkaanyag"></param>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_IraIktatoKonyvek_Id"></param>
    /// <param name="_EREC_UgyUgyiratok_Id"></param>
    /// <param name="_EREC_UgyUgyiratok"></param>
    /// <param name="_EREC_UgyUgyiratdarabok"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_HataridosFeladatok"></param>
    /// <param name="_EREC_PldIratPeldanyok"></param>
    /// <param name="ip"></param>
    /// <returns></returns>
    private Result IratIktatasaTobbIratPld(IktatasTipus Bejovo_vagy_BelsoIratIktatas, NormalVagyAlszamraIktatas Normal_vagy_AlszamraIktatas,
                    IktatottVagyMunkaanyag Iktatott_vagy_Munkaanyag,
                    ExecParam _ExecParam, string _EREC_IraIktatoKonyvek_Id, string _EREC_UgyUgyiratok_Id,
                    EREC_UgyUgyiratok _EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok _EREC_UgyUgyiratdarabok,
                    EREC_IraIratok _EREC_IraIratok, EREC_HataridosFeladatok _EREC_HataridosFeladatok,
                    EREC_PldIratPeldanyok[] _EREC_PldIratPeldanyok,
                    IktatasiParameterek ip)
    {
        #region Paraméterek ellenőrzése

        if (ip == null) ip = new IktatasiParameterek();

        // Paraméterek meglétének ellenőrzése:
        if (_ExecParam == null
            || (string.IsNullOrEmpty(_EREC_IraIktatoKonyvek_Id) && Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            || (string.IsNullOrEmpty(_EREC_IraIktatoKonyvek_Id) && Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag && Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
            || (string.IsNullOrEmpty(_EREC_UgyUgyiratok_Id) && Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas)
            || (_EREC_UgyUgyiratok == null && Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
            //|| _EREC_UgyUgyiratdarabok == null
            || _EREC_IraIratok == null
            || (_EREC_PldIratPeldanyok == null && Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas)
            || (string.IsNullOrEmpty(ip.KuldemenyId) && Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas))
        {

            Logger.Error("Iktatás: Paraméter megadás hiányos", _ExecParam);
            if (string.IsNullOrEmpty(_EREC_IraIktatoKonyvek_Id))
            {
                Logger.Error("Nincs megadva iktatókönyv", _ExecParam);

                throw new ResultException(52114);
            }
            if (_ExecParam == null || string.IsNullOrEmpty(_ExecParam.Felhasznalo_Id))
            {
                Logger.Error("ExecParam megadása helytelen", _ExecParam);
            }

            throw new ResultException(52100);
        }

        // Lehet bejövőnél is (2009.01.19)
        //// Munkaanyag csak belső irat iktatásnál lehet:
        //if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag
        //    && Bejovo_vagy_BelsoIratIktatas != IktatasTipus.BelsoIratIktatas)
        //{
        //    Logger.Error("Munkaanyag csak belső irat iktatásnál lehet", _ExecParam);
        //    // TODO: szöveg helyett hibakód
        //    throw new ResultException("Munkaanyag csak belső irat iktatásnál lehet");
        //}

        #endregion

        // van-e megadva felelős (Nem alszámra iktatásnál)
        if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas && string.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Felelos))
        {
            Logger.Error("Iktatás: Nincs megadva felelős", _ExecParam);

            throw new ResultException(52108);
        }


        bool iktatoKonyvLezart = false;
        bool iktatokonyvFolyosorszamos = false;
        bool ugyiratLezart = false;
        bool ugyiratUjranyitasaSzukseges = false;
        bool utolagosPotlasSzukseges = false;
        bool kelleRegiAdtaSzereles = false;

        // lezárt iktatókönyves előzmény és
        // alszámra indított iktatás esetén át kell venni a B1 metaadatokat a létrehozott új ügyiratba,
        // ha volt főszámra iktatás joga, akkor ez a felületről történik
        bool kellMetaadatAtvetel = false;

        SzerelesMod szerelesMod = SzerelesMod.None;

        EREC_IraIktatoKonyvekService erec_IraIktatoKonyvekService = new EREC_IraIktatoKonyvekService(this.dataContext);
        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = null;


        #region Ügyirat - Iktatókönyv állapot vizsgálata:

        // Alszámra Iktatás - ügyirat lekérés
        // Főszámra iktatás szereléssel - ügyirat lekérés

        EREC_UgyUgyiratok elozmenyUgyirat = null;

        if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas || !string.IsNullOrEmpty(ip.SzerelendoUgyiratId))
        {
            // Ugyirat lekérése:
            Logger.Debug("Ügyirat lekérés - Start", _ExecParam);

            string elozmenyUgyiratId = "";
            if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas)
            {
                elozmenyUgyiratId = _EREC_UgyUgyiratok_Id;
            }
            else
            {
                elozmenyUgyiratId = ip.SzerelendoUgyiratId;
            }

            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_ugyiratGet = _ExecParam.Clone();
            execParam_ugyiratGet.Record_Id = elozmenyUgyiratId;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_ugyiratGet);
            if (!string.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                #region Előzmény Ügyirat vizsgálat

                elozmenyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                if (elozmenyUgyirat == null)
                {
                    // hiba
                    throw new ResultException(52106);
                }

                // Állapot ellenőrzés: lehet-e az ügyirathoz iktatni?
                // (Munkapéldánynál más ellenőrzés kell)
                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(elozmenyUgyirat);
                ErrorDetails errorDetail = null;
                ExecParam xpm = _ExecParam.Clone();

                if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                {
                    if (Contentum.eRecord.BaseUtility.Ugyiratok.LehetAlszamraIktatni(xpm, ugyiratStatusz, out errorDetail) == false)
                    {
                        Logger.Warn("Állapotellenőrzés: alszámos iktatás nem lehetséges az ügyiratra", _ExecParam);

                        throw new ResultException(52109, errorDetail);
                    }
                }
                else if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                {
                    if (Contentum.eRecord.BaseUtility.Ugyiratok.LehetMunkapeldanytLetrehozni(ugyiratStatusz, out errorDetail) == false)
                    {
                        Logger.Warn("Állapotellenőrzés: munkapéldányt nem lehet létrehozni az ügyiratban", _ExecParam);

                        //Az ügyiratban nem hozható létre új munkapéldány!
                        throw new ResultException(52135, errorDetail);
                    }
                }

                // Ha LEZÁRT AZ ÜGYIRAT:
                if (Contentum.eRecord.BaseUtility.Ugyiratok.Lezart(ugyiratStatusz))
                {
                    ugyiratLezart = true;
                }

                #endregion

                #region Iktatókönyv vizsgálat (Előzmény ügyiraté)

                // előzmény ügyirat iktatókönyvének lekérése:
                erec_IraIktatoKonyvek = GetIktatokonyvById(elozmenyUgyirat.IraIktatokonyv_Id, _ExecParam);
                // Lezárt-e az iktatókönyv:                
                iktatoKonyvLezart = Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatoKonyvek, out errorDetail);
                if (errorDetail != null)
                {
                    throw new ResultException(52104, errorDetail); // iktatókönyv lekérése sikertelen
                }


                if (erec_IraIktatoKonyvek.IktSzamOsztas == KodTarak.IKT_SZAM_OSZTAS.Folyosorszamos)
                {
                    iktatokonyvFolyosorszamos = true;
                }

                //CERTOP
                //Éven túli iktatás irattári tételszám alapján

                EREC_IraIrattariTetelekService svcIrattariTetelek = new EREC_IraIrattariTetelekService(this.dataContext);
                ExecParam xpmIrattariTetelek = _ExecParam.Clone();
                xpmIrattariTetelek.Record_Id = elozmenyUgyirat.IraIrattariTetel_Id;

                Result resirattariTetelek = svcIrattariTetelek.Get(xpmIrattariTetelek);

                if (!resirattariTetelek.IsError)
                {
                    EREC_IraIrattariTetelek irattariTetel = resirattariTetelek.Record as EREC_IraIrattariTetelek;

                    if (irattariTetel != null && "1".Equals(irattariTetel.EvenTuliIktatas))
                    {
                        iktatokonyvFolyosorszamos = true;
                    }
                }

                //if (iktatoKonyvLezart && _EREC_UgyUgyiratok == null)
                //{

                //    Logger.Warn("Iktatókönyv ellenőrzés: lezárt iktatókönyvben lévő előzmény ügyirat esetén az átadott paramétereknek tartalmazniuk kell a létrehozandó új ügyirat adatait!", _ExecParam);

                //    throw new ResultException(52136);
                //}

                if (iktatoKonyvLezart)
                {
                    if (!iktatokonyvFolyosorszamos)
                    {
                        // szerelt vagy elő van készítve szerelésre
                        if (!string.IsNullOrEmpty(elozmenyUgyirat.UgyUgyirat_Id_Szulo))
                        {
                            Logger.Warn("Iktatókönyv/ügyirat ellenőrzés: Az ügyirat már szerelt vagy szerelésre előkészített, nem választható előzménynek!", _ExecParam);
                            throw new ResultException(52137);
                        }

                        // munkaanyag létrehozásánál csak előkészített szerelés lehet
                        if (Contentum.eRecord.BaseUtility.Ugyiratok.Szerelheto(ugyiratStatusz, _ExecParam.Clone(), out errorDetail)
                            && Iktatott_vagy_Munkaanyag != IktatottVagyMunkaanyag.Munkaanyag)
                        {
                            szerelesMod = SzerelesMod.Szereles;
                        }
                        else
                        {
                            szerelesMod = SzerelesMod.Elokeszites;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(ip.SzerelendoUgyiratId) && Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
                {
                    // hiba, nem lezárt iktatókönyvben lévő ügyiratot próbálunk szerelni iktatáskor
                    Logger.Warn("Iktatókönyv/ügyirat ellenőrzés: A szerelendő előzmény ügyirat nincs lezárt iktatókönyvben!", _ExecParam);
                    throw new ResultException("A szerelendő előzmény ügyirat nincs lezárt iktatókönyvben!");
                }

                //int iktatokonyv_ev = 0;
                //try
                //{
                //    iktatokonyv_ev = Int32.Parse(erec_IraIktatoKonyvek.Ev);
                //}
                //catch
                //{ 
                //    // hiba:
                //    Logger.Error("Hiba az iktatókönyv.Ev konvertálásakor: " + erec_IraIktatoKonyvek.Ev, _ExecParam);
                //    throw new ResultException(52104);
                //}

                //// Lezárt-e az iktatókönyv:
                //if (iktatokonyv_ev < DateTime.Today.Year)
                //{
                //    iktatoKonyvLezart = true;
                //}

                #endregion

                if (iktatoKonyvLezart == false)
                {
                    // ha az ügyirat lezárt:
                    if (ugyiratLezart == true)
                    {
                        if (ip.UgyiratUjranyitasaHaLezart == true)
                        {
                            ugyiratUjranyitasaSzukseges = true;
                        }
                        else
                        {
                            utolagosPotlasSzukseges = true;
                        }
                    }
                }
                else if (iktatoKonyvLezart == true && !iktatokonyvFolyosorszamos && Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas)
                {
                    kellMetaadatAtvetel = true;
                }
            }
        }

        // Létrehozható munkaanyag ilyenkor is, előkészített szereléssel (2009.01.21.)
        //// Munkaanyag nem hozható létre lezárt ügyiratba, vagy lezárt iktatókönyvű ügyiratba
        //if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag
        //    && (iktatoKonyvLezart || ugyiratLezart))
        //{
        //    Logger.Error("Munkaanyag nem hozható létre lezárt ügyiratba, vagy lezárt iktatókönyvű ügyiratba", _ExecParam);           
        //    throw new ResultException(52134);
        //}


        #endregion

        #region Barkod generálása az ügyirat számára (Nem alszámos iktatás esetén), illetve az iratpéldány(ok) számára, ha a vonalkódkezelés nem azonosító
        // áthelyezve az ügyirat - iktatókönyv vizsgálatok utánra, mert az iktatókönyv lezártsága esetén is szükséges a barkód generálás


        //Vonalkód update és kötés if a vonalkódkezelés azonosítós
        //Contentum.eAdmin.Service.KRT_ParameterekService service_parameterek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
        ExecParam execParam_KRT_Param = _ExecParam.Clone();
        //CR 3058
        string vonalkodkezeles = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim();

        string barkod_ugyirat_Id = "";
        string barkod_pld1_Id = "";
        string barkod_pld1_Ver = "";
        string barkod_pld2_Id = "";
        string barkod_pld3_Id = "";
        string barkod_ugyirat = "";
        string barkod_pld1 = "";
        string barkod_pld2 = "";
        string barkod_pld3 = "";
        if (!vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))//CR 3058
        {
            //try
            {
                KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                ExecParam execParam_barkod = _ExecParam.Clone();

                // Barkod az ügyiratnak:
                if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas
                    || (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas && !iktatokonyvFolyosorszamos && iktatoKonyvLezart == true))
                {
                    Result result_barkod_ugyirat = service_barkodok.BarkodGeneralas(execParam_barkod);
                    if (!string.IsNullOrEmpty(result_barkod_ugyirat.ErrorCode))
                    {
                        // hiba:
                        Logger.Error("Vonalkód generálás az ügyiratnak sikertelen", _ExecParam);
                        throw new ResultException(result_barkod_ugyirat);
                    }
                    else
                    {
                        barkod_ugyirat_Id = result_barkod_ugyirat.Uid;
                        barkod_ugyirat = (string)result_barkod_ugyirat.Record;
                    }
                }

                /// Barkod az első iratpéldánynak (ha nem adtunk meg az objektumban, és az ügyintézés módja elektronikus,
                /// vagy előírtuk a vonalkódgenerálást az iratpéldánynak)
                if (string.IsNullOrEmpty(_EREC_PldIratPeldanyok[0].BarCode)
                    && (_EREC_PldIratPeldanyok[0].UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir
                        || ip.IratpeldanyVonalkodGeneralasHaNincs == true)
                    )
                {
                    Result result_barkod_pld1 = service_barkodok.BarkodGeneralas(execParam_barkod);
                    if (!string.IsNullOrEmpty(result_barkod_pld1.ErrorCode))
                    {
                        // hiba:
                        Logger.Error("Vonalkód generálás az iratpéldánynak: Sikertelen", _ExecParam);
                        throw new ResultException(result_barkod_pld1);
                    }
                    else
                    {
                        barkod_pld1_Id = result_barkod_pld1.Uid;
                        barkod_pld1 = (string)result_barkod_pld1.Record;
                    }
                }
                else
                {
                    barkod_pld1 = _EREC_PldIratPeldanyok[0].BarCode;

                    if (!string.IsNullOrEmpty(barkod_pld1))
                    {
                        Logger.Info("Volt megadva vonalkód az iratpéldánynak: " + barkod_pld1, _ExecParam);

                        #region BarCode Id lekérése:

                        Result result_BarCodeGetPld1 = service_barkodok.GetBarcodeByValue(execParam_barkod, barkod_pld1);
                        if (!string.IsNullOrEmpty(result_BarCodeGetPld1.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_BarCodeGetPld1);
                        }

                        KRT_Barkodok barkodObj_pld1 = (KRT_Barkodok)result_BarCodeGetPld1.Record;

                        barkod_pld1_Id = barkodObj_pld1.Id;
                        barkod_pld1_Ver = barkodObj_pld1.Base.Ver;

                        #endregion
                    }
                }

                // Barkod az esetleges további példányoknak: (csak akkor generálunk, ha az ügyintézés módja elektronikus)
                if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas
                    && _EREC_PldIratPeldanyok[0].UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
                {
                    if (ip.UgyiratPeldanySzukseges == true)
                    {
                        Result result_barkod_pld2 = service_barkodok.BarkodGeneralas(execParam_barkod);
                        if (!string.IsNullOrEmpty(result_barkod_pld2.ErrorCode))
                        {
                            // hiba:
                            Logger.Error("Vonalkód generálás az iratpéldánynak(2): Sikertelen", _ExecParam);
                            throw new ResultException(result_barkod_pld2);
                        }
                        else
                        {
                            barkod_pld2_Id = result_barkod_pld2.Uid;
                            barkod_pld2 = (string)result_barkod_pld2.Record;
                        }
                    }

                    if (ip.KeszitoPeldanyaSzukseges == true)
                    {
                        Result result_barkod_pld3 = service_barkodok.BarkodGeneralas(execParam_barkod);
                        if (!string.IsNullOrEmpty(result_barkod_pld3.ErrorCode))
                        {
                            // hiba:
                            Logger.Error("Vonalkód generálás az iratpéldánynak(3): Sikertelen", _ExecParam);
                            throw new ResultException(result_barkod_pld3);
                        }
                        else
                        {
                            barkod_pld3_Id = result_barkod_pld3.Uid;
                            barkod_pld3 = (string)result_barkod_pld3.Record;
                        }
                    }
                }

            }
        }        //catch (Exception e)
                 //{
                 //    // Előfordulhatnak még hibák a vonalkód generálásnál; lenyeljük a hibát egyelőre
                 //    Logger.Error("Hiba a vonalkód generálás során",_ExecParam);
                 //    Logger.Error("",e);
                 //}
        #endregion


        #region Küldemény ellenőrzés, UPDATE (Csak Bejövő irat iktatásnál)
        EREC_KuldKuldemenyek erec_KuldKuldemenyek = null;

        // Bejövő irat iktatásánál kell küldeményt megadni, Belső irat iktatásnál nem
        if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
        {
            // Küldemény ellenőrzése, és állapotának, minősítésének beállítása
            Result result_CheckAndSetKuldemeny = CheckAndSet_Kuldemeny(_ExecParam, ip.KuldemenyId, _EREC_IraIratok.Minosites, out erec_KuldKuldemenyek);
            if (!string.IsNullOrEmpty(result_CheckAndSetKuldemeny.ErrorCode))
            {
                Logger.Warn("A küldemény nem iktatható", _ExecParam);

                // hiba, vagy nem iktatható a küldemény
                throw new ResultException(result_CheckAndSetKuldemeny);
            }

            #region Vonalkód ellenőrzés (Megadott iratpéldány vonalkódja nem lehet ugyanaz, mint a küldemény boríték vonalkódja)

            if (_EREC_PldIratPeldanyok != null && !string.IsNullOrEmpty(_EREC_PldIratPeldanyok[0].BarCode)
                && erec_KuldKuldemenyek != null && !string.IsNullOrEmpty(erec_KuldKuldemenyek.BarCode)
                && erec_KuldKuldemenyek.BarCode == _EREC_PldIratPeldanyok[0].BarCode)
            {
                // Ellenőrzés, van-e olyan BORÍTÉK melléklete a küldeménynek, aminek vonalkódja megegyezik a megadott iratpéldány vonalkóddal?

                EREC_MellekletekService service_kuldMellekletek = new EREC_MellekletekService(this.dataContext);

                EREC_MellekletekSearch search_kuldMellekletek = new EREC_MellekletekSearch();

                search_kuldMellekletek.KuldKuldemeny_Id.Value = erec_KuldKuldemenyek.Id;
                search_kuldMellekletek.KuldKuldemeny_Id.Operator = Query.Operators.equals;

                search_kuldMellekletek.AdathordozoTipus.Value = KodTarak.AdathordozoTipus.Boritek;
                search_kuldMellekletek.AdathordozoTipus.Operator = Query.Operators.equals;

                search_kuldMellekletek.BarCode.Value = _EREC_PldIratPeldanyok[0].BarCode;
                search_kuldMellekletek.BarCode.Operator = Query.Operators.equals;

                ExecParam execParam_kuldMellekletek = _ExecParam.Clone();

                Result result_kuldMellekletek = service_kuldMellekletek.GetAll(execParam_kuldMellekletek, search_kuldMellekletek);
                if (!string.IsNullOrEmpty(result_kuldMellekletek.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_kuldMellekletek);
                }

                // Ha talált ilyen borítékot:
                if (result_kuldMellekletek.Ds != null && result_kuldMellekletek.Ds.Tables[0].Rows.Count > 0)
                {
                    // hiba:
                    Logger.Warn("A küldemény borítékának vonalkódja és az irat vonalkódja nem egyezhet meg!", _ExecParam);

                    throw new ResultException(52121);
                }

            }

            #endregion
        }
        #endregion

        /// ha Normál (Nem Alszámra) iktatás van, VAGY ha az előzmény ügyirat iktatókönyve lezárt:
        /// (ÜGYIRAT INSERT rész)
        if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas
            || (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas && !iktatokonyvFolyosorszamos && iktatoKonyvLezart == true)
            )
        {

            //Iktatókönyv lekérése a főszám megállapításához:

            erec_IraIktatoKonyvek = GetIktatokonyvById(_EREC_IraIktatoKonyvek_Id, _ExecParam);

            if (erec_IraIktatoKonyvek == null)
            {
                // hiba
                throw new ResultException(52104);
            }


            #region TODO: Ellenőrzés, iktatókönyvbe iktathat-e? (nem teljes)
            // BLG_1816
            if (Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatoKonyvek) == true && System.Web.Configuration.WebConfigurationManager.AppSettings["Migration"] != "True")
            {
                if (iktatokonyvFolyosorszamos)
                {
                    if (Normal_vagy_AlszamraIktatas != NormalVagyAlszamraIktatas.AlszamraIktatas)
                    {
                        // hiba:
                        Logger.Error("Lezárt a megadott folyósorszámos iktatókönyv, ezért csak alszámra lehet bele iktatni", _ExecParam);
                        throw new ResultException("Lezárt a megadott folyósorszámos iktatókönyv, ezért csak alszámra lehet bele iktatni!");
                    }
                }
                else
                {
                    // hiba:
                    Logger.Error("Lezárt a megadott iktatókönyv", _ExecParam);
                    throw new ResultException(52119);
                }
            }
            #endregion


            int UjFoszam;
            try
            {
                UjFoszam = Int32.Parse(erec_IraIktatoKonyvek.UtolsoFoszam) + 1;
            }
            catch (Exception e)
            {
                Logger.Error("Hiba az iktatókönyv utolsó főszámának lekérésekor: "
                    + e.StackTrace, _ExecParam);
                Logger.Error("", e);

                throw new ResultException(52100);
            }

            // Munkaügyirat esetén új sorszám megállapítása:
            int UjMunkaUgyiratSorszam = 0;
            if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
            {
                try
                {
                    if (string.IsNullOrEmpty(erec_IraIktatoKonyvek.UtolsoSorszam))
                    {
                        UjMunkaUgyiratSorszam = 1;
                    }
                    else
                    {
                        UjMunkaUgyiratSorszam = Int32.Parse(erec_IraIktatoKonyvek.UtolsoSorszam) + 1;
                    }
                }
                catch (Exception e)
                {
                    Logger.Error("Hiba az iktatókönyv utolsó munkaügyirat-sorszámának lekérésekor: "
                    + e.StackTrace, _ExecParam);
                    Logger.Error("", e);

                    throw new ResultException(52100);
                }
            }


            Logger.Debug("Iktatókönyv Update - Start", _ExecParam);


            #region Iktatokonyv UPDATE
            // Iktatokonyv UPDATE
            // (Utolsó főszám növelése)
            erec_IraIktatoKonyvek.Updated.SetValueAll(false);
            erec_IraIktatoKonyvek.Base.Updated.SetValueAll(false);
            erec_IraIktatoKonyvek.Base.Updated.Ver = true;

            // Munkaanyagnál még nem kap új főszámot, csak iktatott anyagnál:
            // BLG_1713
            // SZUR rendszerből készülhet Belső iktatás Normál főszámmal és Munkairattal
            // BUG_6117
            //if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            //    || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)))
            if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                    && (ip.IsSZUR)))
            {
                // Iktatokonyv.UtolsoFoszam beállítása a megnövelt főszámra, és visszaírása az adatbázisba:
                erec_IraIktatoKonyvek.UtolsoFoszam = UjFoszam.ToString();
                erec_IraIktatoKonyvek.Updated.UtolsoFoszam = true;
            }
            else
            // Munkaanyagnál utolsó sorszám állítása:
            if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
            {
                // Új sorszám beállítása:
                erec_IraIktatoKonyvek.UtolsoSorszam = UjMunkaUgyiratSorszam.ToString();
                erec_IraIktatoKonyvek.Updated.UtolsoSorszam = true;
            }


            ExecParam execParam_IktatokonyvUpdate = _ExecParam.Clone();
            execParam_IktatokonyvUpdate.Record_Id = erec_IraIktatoKonyvek.Id;

            Result result_IktatokonyvUpdate = erec_IraIktatoKonyvekService.Update(execParam_IktatokonyvUpdate, erec_IraIktatoKonyvek);
            if (!string.IsNullOrEmpty(result_IktatokonyvUpdate.ErrorCode))
            {
                Logger.Error("Iktatókönyv utolsó főszámának növelése sikertelen", _ExecParam, result_IktatokonyvUpdate);

                //ResultError resultError = new ResultError(result_IktatokonyvUpdate);
                // Ha verzióütközés van:
                int errorNumber = ResultError.GetErrorNumberFromResult(result_IktatokonyvUpdate);
                if (errorNumber == 50402 || errorNumber == 50401)
                {
                    // Nem egyezik a verzió --> valami normális hibaüzenet kell a felhasználónak
                    throw new ResultException(52115);
                }

                // hiba
                throw new ResultException(result_IktatokonyvUpdate);
            }

            #endregion


            Logger.Debug("Ügyirat Insert Start", _ExecParam);

            #region Ügyirat INSERT

            // Ügyirat INSERT                
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_ugyiratokInsert = _ExecParam.Clone();

            if (iktatoKonyvLezart == true && !iktatokonyvFolyosorszamos)
            {
                #region Előzmény kikérés/kölcsönzés, ha szükséges, hogy később szerelni lehessen

                #region Irattárban van-e/Kikölcsönzött?
                bool kolcsonzesInditasKell = false;
                bool atmenetiIrattarbolKikeresKell = false;// lehet átmeneti vagy skontró irattár is
                bool kikolcsonzott = false;

                Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.None;

                if (elozmenyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott
                    || elozmenyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott)
                {
                    string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam_ugyiratokInsert).Obj_Id;

                    // Ha központi irattárban van:
                    if (!string.IsNullOrEmpty(elozmenyUgyirat.Csoport_Id_Felelos) && !string.IsNullOrEmpty(kozpontiIrattarId)
                        && elozmenyUgyirat.Csoport_Id_Felelos.ToLower() == kozpontiIrattarId.ToLower())
                    {
                        kolcsonzesInditasKell = true;
                    }
                    else
                    {
                        // Átmeneti irattárban van:
                        atmenetiIrattarbolKikeresKell = true;
                        kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.AtmenetiIrattar;
                    }


                    /// Irattárba küldött állapotnál előfordulhat, hogy valaki már beleiktatott, és keletkezett kikérő
                    /// Ilyenkor már nem indítunk új kikérést
                    if (elozmenyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
                    {
                        // ha van találat --> már kikérték, nem kell újra kikérni:
                        if (erec_UgyUgyiratokService.ExistsIrattariKikero(elozmenyUgyirat.Id, _ExecParam.Clone()))
                        {
                            kolcsonzesInditasKell = false;
                            atmenetiIrattarbolKikeresKell = false;
                        }
                    }

                }
                else if (elozmenyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban)
                {
                    // Skontró irattárban van-e:
                    string skontroIrattarId = KodTarak.SPEC_SZERVEK.GetSkontroIrattaros(execParam_ugyiratokInsert).Obj_Id;
                    if (!string.IsNullOrEmpty(elozmenyUgyirat.Csoport_Id_Felelos) && !string.IsNullOrEmpty(skontroIrattarId)
                    && elozmenyUgyirat.Csoport_Id_Felelos.ToLower() == skontroIrattarId.ToLower())
                    {
                        atmenetiIrattarbolKikeresKell = true;
                        kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.SkontroIrattar;
                    }
                }
                else if (elozmenyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott)
                {
                    kikolcsonzott = true;
                }

                #endregion

                #region Irattárban lévő ügyiratnál további műveletek

                if (atmenetiIrattarbolKikeresKell == true)
                {
                    #region Kikérés indítása:
                    Result result_kikeres = erec_UgyUgyiratokService.Kikeres(elozmenyUgyirat.Id, kikeresIrattarTipus, _ExecParam.Clone(), "Szerelés");
                    #endregion
                }
                else if (kolcsonzesInditasKell == true)
                {
                    #region Kölcsönzés indítása, automatikus jóváhagyással:
                    Result result_kikeres = erec_UgyUgyiratokService.Kolcsonzes(elozmenyUgyirat.Id, _ExecParam.Clone(), "Szerelés");
                    #endregion
                }
                else if (kikolcsonzott == true)
                {
                    // csak szereljük, itt nem kell ügyintézésre kikérni
                }
                #endregion Irattárban lévő ügyiratnál további műveletek

                #endregion Előzmény kikérés/kölcsönzés, ha szükséges
            }

            // ha iktatoKonyvLezart == true, _EREC_UgyUgyiratok a lekért előzmény ügyirat adatait fogja tartalmazni, de nekünk új kell
            if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas
                && !iktatokonyvFolyosorszamos && iktatoKonyvLezart == true)
            {
                // paraméterek ellenőrzése
                if (string.IsNullOrEmpty(ip.UgykorId) || string.IsNullOrEmpty(ip.Ugytipus))
                {
                    Logger.Warn("Iktatókönyv ellenőrzés: lezárt iktatókönyvben lévő előzmény ügyirat esetén az átadott paramétereknek tartalmazniuk kell a létrehozandó új ügyirat adatait!", _ExecParam);
                    throw new ResultException(52136);
                }
                //EREC_UgyUgyiratok elozmenyUgyirat = _EREC_UgyUgyiratok;
                _EREC_UgyUgyiratok = new EREC_UgyUgyiratok();
                _EREC_UgyUgyiratok.Updated.SetValueAll(false);
                _EREC_UgyUgyiratok.Base.Updated.SetValueAll(false);

                // TODO: előzmény ügyiratból néhány mezőt átmásolni: (??)
                #region előzmény adatok másolása
                _EREC_UgyUgyiratok.UgyintezesModja = elozmenyUgyirat.UgyintezesModja;
                _EREC_UgyUgyiratok.Updated.UgyintezesModja = true;

                _EREC_UgyUgyiratok.Hatarido = elozmenyUgyirat.Hatarido; // ezt lehet, hogy alább még felülírjuk az iktatási paraméterekből
                _EREC_UgyUgyiratok.Updated.Hatarido = true;

                _EREC_UgyUgyiratok.Targy = elozmenyUgyirat.Targy;
                _EREC_UgyUgyiratok.Updated.Targy = true;

                // módosítható
                _EREC_UgyUgyiratok.UgyTipus = ip.Ugytipus;
                _EREC_UgyUgyiratok.Updated.UgyTipus = true;

                // felelős és őrző az iktató lesz
                string Iktato_Csoport_Id = Csoportok.GetFelhasznaloSajatCsoportId(_ExecParam);

                _EREC_UgyUgyiratok.Csoport_Id_Felelos = Iktato_Csoport_Id;
                _EREC_UgyUgyiratok.Updated.Csoport_Id_Felelos = true;

                _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo = Iktato_Csoport_Id;
                _EREC_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Orzo = true;

                // TODO: módosítható(?) - nincs kint az iktatási képernyőn
                _EREC_UgyUgyiratok.Jelleg = elozmenyUgyirat.Jelleg;
                _EREC_UgyUgyiratok.Updated.Jelleg = true;

                // módosítható
                _EREC_UgyUgyiratok.IraIrattariTetel_Id = ip.UgykorId;
                _EREC_UgyUgyiratok.Updated.IraIrattariTetel_Id = true;

                _EREC_UgyUgyiratok.Surgosseg = elozmenyUgyirat.Surgosseg;
                _EREC_UgyUgyiratok.Updated.Surgosseg = true;

                _EREC_UgyUgyiratok.Partner_Id_Ugyindito = elozmenyUgyirat.Partner_Id_Ugyindito;
                _EREC_UgyUgyiratok.Updated.Partner_Id_Ugyindito = true;

                _EREC_UgyUgyiratok.NevSTR_Ugyindito = elozmenyUgyirat.NevSTR_Ugyindito;
                _EREC_UgyUgyiratok.Updated.NevSTR_Ugyindito = true;

                _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = elozmenyUgyirat.FelhasznaloCsoport_Id_Ugyintez;
                _EREC_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

                _EREC_UgyUgyiratok.Megjegyzes = elozmenyUgyirat.Megjegyzes;
                _EREC_UgyUgyiratok.Updated.Megjegyzes = true;

                _EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos = elozmenyUgyirat.Csoport_Id_Ugyfelelos;
                _EREC_UgyUgyiratok.Updated.Csoport_Id_Ugyfelelos = true;

                _EREC_UgyUgyiratok.Cim_Id_Ugyindito = elozmenyUgyirat.Cim_Id_Ugyindito;
                _EREC_UgyUgyiratok.Updated.Cim_Id_Ugyindito = true;

                _EREC_UgyUgyiratok.CimSTR_Ugyindito = elozmenyUgyirat.CimSTR_Ugyindito;
                _EREC_UgyUgyiratok.Updated.CimSTR_Ugyindito = true;

                // az új ügyiratot az iktató hozza létre
                _EREC_UgyUgyiratok.Base.Letrehozo_id = _ExecParam.Felhasznalo_Id;
                _EREC_UgyUgyiratok.Base.Updated.Letrehozo_id = true;
                #endregion előzmény adatok másolása

                #region ügyirat határidő módosítás, ha szükséges
                if (!string.IsNullOrEmpty(ip.UgyiratUjHatarido))
                {
                    _EREC_UgyUgyiratok.Hatarido = ip.UgyiratUjHatarido;
                    _EREC_UgyUgyiratok.Updated.Hatarido = true;
                }
                #endregion ügyirat határidő módosítás, ha szükséges

            }

            // iktatókönyvhöz kapcsolás
            _EREC_UgyUgyiratok.IraIktatokonyv_Id = erec_IraIktatoKonyvek.Id;
            _EREC_UgyUgyiratok.Updated.IraIktatokonyv_Id = true;


            // Főszám munkaanyagnál nincs, csak iktatott iratnál:
            if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            {
                // főszám beállítása
                _EREC_UgyUgyiratok.Foszam = UjFoszam.ToString();
                _EREC_UgyUgyiratok.Updated.Foszam = true;

                // UTOLSÓ ALSZÁM beállítása 1-re (mert egy iratot bele fogunk tenni)
                _EREC_UgyUgyiratok.UtolsoAlszam = "1";
                _EREC_UgyUgyiratok.Updated.UtolsoAlszam = true;
            }
            else if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
            {
                // BLG_1713
                // SZUR rendszerből készülhet Belső iktatás Normál főszámmal és Munkairattal
                // BUG_6117
                //if ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas))
                if ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
                     && (ip.IsSZUR))
                {
                    // főszám beállítása
                    _EREC_UgyUgyiratok.Foszam = UjFoszam.ToString();
                    _EREC_UgyUgyiratok.Updated.Foszam = true;
                }
                // Munkaanyagnál az utolsó alszám 0 lesz: (mert nem lesz benne iktatott irat, csak munkaanyag)
                _EREC_UgyUgyiratok.UtolsoAlszam = "0";
                _EREC_UgyUgyiratok.Updated.UtolsoAlszam = true;

                // Munkaügyirat sorszám állítása:
                _EREC_UgyUgyiratok.Sorszam = UjMunkaUgyiratSorszam.ToString();
                _EREC_UgyUgyiratok.Updated.Sorszam = true;

                // UTOLSÓ SORSZÁM állítása 1-re, mert egy munkairatot létre fogunk hozni benne
                _EREC_UgyUgyiratok.UtolsoSorszam = "1";
                _EREC_UgyUgyiratok.Updated.UtolsoSorszam = true;
            }

            // Őrző beállítása a Felhasznalo csoportjára:
            _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo = Csoportok.GetFelhasznaloSajatCsoportId(_ExecParam);
            _EREC_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Orzo = true;

            #region CR3184 - FPH - Ügyiratnak nem generálódik vonalkód
            // Barkod beállítása:
            if (!string.IsNullOrEmpty(barkod_ugyirat))
            {
                _EREC_UgyUgyiratok.BARCODE = barkod_ugyirat;
                _EREC_UgyUgyiratok.Updated.BARCODE = true;
            }
            #endregion CR3184 - FPH - Ügyiratnak nem generálódik vonalkód

            // Állapot
            // BLG_1713 
            // SZUR rendszerből készülhet Belső iktatás Normál főszámmal és Munkairattal
            // BUG_6117
            //if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            //    || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)))
            if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                && (ip.IsSZUR)))

            {
                _EREC_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Iktatott;
                _EREC_UgyUgyiratok.Updated.Allapot = true;

                // BLG_1348
                // Kiszedve az Ügyintezes_alatt-i státuszba állítás (más fogja majd vezérelni)
                //ügyintézés alatti állapot bejövő papír alaú irat esetén
                //if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
                //{
                //    if (Contentum.eRecord.BaseUtility.Ugyiratok.Uj_Ugyirat_Hatarido_Kezeles(_ExecParam))
                //    {
                //        if (Contentum.eRecord.BaseUtility.Kuldemenyek.IsPapirAlapu(erec_KuldKuldemenyek))
                //        {
                //            _EREC_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
                //        }
                //    }
                //}
            }
            else if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
            {
                _EREC_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag;
                _EREC_UgyUgyiratok.Updated.Allapot = true;
            }

            // Ügyindító beállítása:
            if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
            {
                //LZS - BUG_6309-ben kivettük
                //if (string.IsNullOrEmpty(_EREC_UgyUgyiratok.Partner_Id_Ugyindito) && string.IsNullOrEmpty(_EREC_UgyUgyiratok.NevSTR_Ugyindito))
                //{
                //    _EREC_UgyUgyiratok.Partner_Id_Ugyindito = erec_KuldKuldemenyek.Partner_Id_Bekuldo;
                //    _EREC_UgyUgyiratok.Updated.Partner_Id_Ugyindito = true;
                //    _EREC_UgyUgyiratok.NevSTR_Ugyindito = erec_KuldKuldemenyek.NevSTR_Bekuldo;
                //    _EREC_UgyUgyiratok.Updated.NevSTR_Ugyindito = true;
                //    _EREC_UgyUgyiratok.Cim_Id_Ugyindito = erec_KuldKuldemenyek.Cim_Id;
                //    _EREC_UgyUgyiratok.Updated.Cim_Id_Ugyindito = true;
                //    _EREC_UgyUgyiratok.CimSTR_Ugyindito = erec_KuldKuldemenyek.CimSTR_Bekuldo;
                //    _EREC_UgyUgyiratok.Updated.CimSTR_Ugyindito = true;
                //}
            }
            else if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas)
            {
                // elvileg ilyenkor ügyindító mezőt nem kell tölteni...
            }

            // Sürgősség kódtáras érték beállítása fix értékkel
            _EREC_UgyUgyiratok.Surgosseg = KodTarak.SURGOSSEG.Normal;
            _EREC_UgyUgyiratok.Updated.Surgosseg = true;

            if (string.IsNullOrEmpty(_EREC_UgyUgyiratok.IratMetadefinicio_Id))
            {
                #region Iratmetadef. Id meghatározása

                EREC_IratMetaDefinicioService service_iratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);

                Result result_iratMetaDef = service_iratMetaDef.GetIratMetaDefinicioByUgykorUgytipus(_ExecParam.Clone()
                    , _EREC_UgyUgyiratok.IraIrattariTetel_Id, _EREC_UgyUgyiratok.UgyTipus);
                if (!string.IsNullOrEmpty(result_iratMetaDef.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_iratMetaDef);
                }


                EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_iratMetaDef.Record;
                if (erec_IratMetaDefinicio != null)
                {
                    _EREC_UgyUgyiratok.IratMetadefinicio_Id = erec_IratMetaDefinicio.Id;
                    _EREC_UgyUgyiratok.Updated.IratMetadefinicio_Id = true;

                    _EREC_UgyUgyiratok.GeneraltTargy = erec_IratMetaDefinicio.GeneraltTargy;
                    _EREC_UgyUgyiratok.Updated.GeneraltTargy = true;
                }

                #endregion
            }

            /// Azonosító (Főszám) töltése:
            /// 
            // BLG_292
            //_EREC_UgyUgyiratok.Azonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_EREC_UgyUgyiratok, erec_IraIktatoKonyvek);
            _EREC_UgyUgyiratok.Azonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_ExecParam, _EREC_UgyUgyiratok, erec_IraIktatoKonyvek);

            _EREC_UgyUgyiratok.Updated.Azonosito = true;

            // ügyirat fajtáját (Jelleg) és az ügyintézés módját (UgyintezesModja)
            // meghatározzuk az iratpéldányból/küldeményből:
            string ugyintezesMod = KodTarak.UGYIRAT_JELLEG.Papir;
            string jelleg = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;

            if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas)
            {
                ugyintezesMod = _EREC_IraIratok.UgyintezesAlapja;
                jelleg = _EREC_IraIratok.AdathordozoTipusa;
            }
            else
            {
                ugyintezesMod = erec_KuldKuldemenyek.UgyintezesModja;
                jelleg = erec_KuldKuldemenyek.AdathordozoTipusa;
            }

            switch (jelleg)
            {
                case KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir:
                    _EREC_UgyUgyiratok.Jelleg = KodTarak.UGYIRAT_JELLEG.Elektronikus;
                    break;
                case KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir:
                default:
                    _EREC_UgyUgyiratok.Jelleg = KodTarak.UGYIRAT_JELLEG.Papir;
                    break;
            }

            _EREC_UgyUgyiratok.Updated.Jelleg = true;

            _EREC_UgyUgyiratok.UgyintezesModja = ugyintezesMod;
            _EREC_UgyUgyiratok.Updated.UgyintezesModja = true;


            Result result_ugyiratokInsert = erec_UgyUgyiratokService.Insert(execParam_ugyiratokInsert, _EREC_UgyUgyiratok);
            if (!string.IsNullOrEmpty(result_ugyiratokInsert.ErrorCode))
            {
                Logger.Debug("Ügyirat Insert sikertelen: ", _ExecParam, result_ugyiratokInsert);

                // hiba 
                throw new ResultException(result_ugyiratokInsert);
            }

            _EREC_UgyUgyiratok.Id = result_ugyiratokInsert.Uid;
            //CR 3058
            if (vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
            {
                KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                var execParam_Ugyirat = _ExecParam.Clone();
                execParam_Ugyirat.Record_Id = result_ugyiratokInsert.Uid;
                Result Erec_Ugyirat = erec_UgyUgyiratokService.Get(execParam_Ugyirat);

                //var iktatokonyv = GetIktatokonyvById(_EREC_IraIktatoKonyvek_Id, execParam_Ugyirat);

                #region CR 3111 Budaörsön a vonalkód generálás Munkapéldány létrehozásakor mindig 0-s sorszámot ad.
                var iktatokonyv = new EREC_IraIktatoKonyvek();
                if (!string.IsNullOrEmpty(_EREC_IraIktatoKonyvek_Id))
                    iktatokonyv = GetIktatokonyvById(_EREC_IraIktatoKonyvek_Id, execParam_Ugyirat);
                else
                    iktatokonyv = GetIktatokonyvById(((EREC_UgyUgyiratok)Erec_Ugyirat.Record).IraIktatokonyv_Id, execParam_Ugyirat);
                string barcode =
                    iktatokonyv.Iktatohely + "/" +
                    (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag ?
                        ("MU" +
                            (string.IsNullOrEmpty(((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam) ?
                                ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Sorszam.PadLeft(6, '0') :
                                ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam.PadLeft(6, '0'))
                        ) :
                        ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam.PadLeft(6, '0'))

                    + "/" + iktatokonyv.Ev;
                #endregion
                barcode = barcode.Replace(" ", "");

                KRT_Barkodok barcodeRecord = new KRT_Barkodok();
                //barcodeRecord.ErvKezd = beerkezes.ToString();
                barcodeRecord.Obj_type =
                barcodeRecord.Kod = barcode.Replace(" ", "");

                var execParam_BarCode = _ExecParam.Clone();

                var bid = srvBarcode.Insert(execParam_BarCode, barcodeRecord);
                if (!string.IsNullOrEmpty(bid.ErrorCode))
                {
                    //    hiba
                    Logger.Error("Hiba: Barkod rekordhoz ügyirat Insert", execParam_BarCode);
                    throw new ResultException(bid);
                }
                //var result_barcodebind = srvBarcode.BarkodBindToKuldemeny(execParam_BarCode, bid.Uid, result_ugyiratokInsert.Uid);
                // if (!string.IsNullOrEmpty(result_barcodebind.ErrorCode))
                // {
                //     // hiba
                //     Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", execParam_BarCode);
                //     throw new ResultException(result_barcodebind);
                // }
                ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).BARCODE = barcode;

                var ugyirat_Update_result = erec_UgyUgyiratokService.Update(execParam_Ugyirat, ((EREC_UgyUgyiratok)Erec_Ugyirat.Record));
                if (!string.IsNullOrEmpty(ugyirat_Update_result.ErrorCode))
                {
                    // hiba
                    Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", execParam_Ugyirat);
                    throw new ResultException(ugyirat_Update_result);
                }
                _EREC_UgyUgyiratok = ((EREC_UgyUgyiratok)Erec_Ugyirat.Record);
                barkod_ugyirat_Id = bid.Uid;
                barkod_ugyirat = barcodeRecord.Kod;
                barkod_pld1 = barcodeRecord.Kod;

            }


            //iktatasResult.UjUgyirat_Id = result_ugyiratokInsert.Uid;

            #endregion

            #region Előzményezés esetén eMigration adatok visszairasa kell

            if (string.IsNullOrEmpty(ip.SzerelendoUgyiratId) && _EREC_UgyUgyiratok.Updated.RegirendszerIktatoszam && !string.IsNullOrEmpty(_EREC_UgyUgyiratok.RegirendszerIktatoszam))
            {
                kelleRegiAdtaSzereles = true;

                // régi ügyirat ellenőrzése selejtezésre
                if (!string.IsNullOrEmpty(ip.RegiAdatId))
                {
                    Contentum.eMigration.Service.MIG_FoszamService foszamService = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
                    ExecParam xpmFoszam = _ExecParam.Clone();
                    xpmFoszam.Record_Id = ip.RegiAdatId;

                    Result result_foszam = foszamService.Get(xpmFoszam);

                    if (!result_foszam.IsError)
                    {
                        Contentum.eMigration.BusinessDocuments.MIG_Foszam mig_foszam = (Contentum.eMigration.BusinessDocuments.MIG_Foszam)result_foszam.Record;
                        if (mig_foszam != null && mig_foszam.UGYHOL == Contentum.eUtility.Constants.MIG_IratHelye.Selejtezett)
                        {
                            // Az ügyiratba nem lehet iktatni! Az ügyirat selejtezett.
                            throw new ResultException(52139);
                        }
                    }
                }
            }

            #endregion

            #region Alszámra indított iktatás és lezárt iktatókönyvben lévő előzmény esetén át kell venni a B1 metaadatokat
            if (kellMetaadatAtvetel)
            {
                EREC_ObjektumTargyszavaiService service_ObjektumTargyszavai = new EREC_ObjektumTargyszavaiService(this.dataContext);
                ExecParam execParam_ObjektumTargyszavai = _ExecParam.Clone();

                Logger.Debug("Objektumot kiegészítő (B1) metadatok átvétele lezárt iktatókönyvben lévő ügyiratból új ügyiratba", execParam_ObjektumTargyszavai);

                EREC_ObjektumTargyszavaiSearch search_ObjektumTargyszavai = new EREC_ObjektumTargyszavaiSearch();
                string Obj_Id = elozmenyUgyirat.Id; // = _EREC_UgyUgyiratok_Id
                string ObjTip_Kod_Tabla = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;

                Result result_OT_byDefinicioTipus = service_ObjektumTargyszavai.GetAllMetaByDefinicioTipus(execParam_ObjektumTargyszavai
                    , search_ObjektumTargyszavai
                    , Obj_Id
                    , ""
                    , ObjTip_Kod_Tabla
                    , KodTarak.OBJMETADEFINICIO_TIPUS.B1
                    , true
                    , true);

                if (!string.IsNullOrEmpty(result_OT_byDefinicioTipus.ErrorCode))
                {
                    Logger.Debug("Hiba az objektumot kiegészítő (B1) metadatok lekérésekor!", execParam_ObjektumTargyszavai, result_OT_byDefinicioTipus);

                    // hiba 
                    throw new ResultException(result_OT_byDefinicioTipus);
                }

                DataRow[] rows_OT_byDefinicioTipus = result_OT_byDefinicioTipus.Ds.Tables[0].Select("IsNull(Targyszo_Id,'')<>''");
                List<string> list_Ids_OT = new List<string>();
                if (rows_OT_byDefinicioTipus != null)
                {
                    foreach (DataRow row in rows_OT_byDefinicioTipus)
                    {
                        list_Ids_OT.Add(row["Targyszo_Id"].ToString());
                    }
                }

                search_ObjektumTargyszavai.Obj_Id.Value = Obj_Id;
                search_ObjektumTargyszavai.Obj_Id.Operator = Query.Operators.equals;

                if (list_Ids_OT.Count > 0)
                {
                    search_ObjektumTargyszavai.Targyszo_Id.Value = "'" + string.Join("','", list_Ids_OT.ToArray()) + "'";
                    search_ObjektumTargyszavai.Targyszo_Id.Operator = Query.Operators.inner;
                }

                Result result_OT_elozmeny = service_ObjektumTargyszavai.GetAll(execParam_ObjektumTargyszavai, search_ObjektumTargyszavai);

                // lista készítése
                foreach (DataRow row in result_OT_elozmeny.Ds.Tables[0].Rows)
                {
                    EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                    Utility.LoadBusinessDocumentFromDataRow(erec_ObjektumTargyszavai, row);

                    erec_ObjektumTargyszavai.Updated.SetValueAll(true);
                    erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);
                    erec_ObjektumTargyszavai.Id = "";

                    erec_ObjektumTargyszavai.Obj_Id = _EREC_UgyUgyiratok.Id;    // az újonnan létrehozott ügyirat azonosítója

                    Result result_ObjektumTargyszavai = service_ObjektumTargyszavai.Insert(execParam_ObjektumTargyszavai, erec_ObjektumTargyszavai);

                    if (!string.IsNullOrEmpty(result_ObjektumTargyszavai.ErrorCode))
                    {
                        Logger.Debug("Objektumot kiegészítő (B1) metadatok átvétele lezárt iktatókönyvben lévő ügyiratból új ügyiratba sikertelen: ", execParam_ObjektumTargyszavai, result_ObjektumTargyszavai);

                        // hiba 
                        throw new ResultException(result_ObjektumTargyszavai);
                    }

                }
                Logger.Debug("Objektumot kiegészítő (B1) metadatok átvétele lezárt iktatókönyvben lévő ügyiratból új ügyiratba sikeres.");

            }
            #endregion Alszámra indított iktatás és lezárt iktatókönyvben lévő előzmény esetén át kell venni a B1 metaadatokat


            #region BARCODE UPDATE (ügyirat)

            KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
            ExecParam execParam_barcodeUpdate = _ExecParam.Clone();

            Result result_barcodeUpdate = service_barkodok.BarkodBindToUgyirat(
                    execParam_barcodeUpdate, barkod_ugyirat_Id, _EREC_UgyUgyiratok.Id);

            if (!string.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
            {
                // hiba
                Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", _ExecParam);
                throw new ResultException(result_barcodeUpdate);
            }

            #endregion

            Logger.Debug("ÜgyiratDarab Insert - Start", _ExecParam);

            #region ÜgyiratDarab INSERT
            // Új ügyirathoz kell egy általános ügyiratdarab                

            EREC_UgyUgyiratdarabokService erec_UgyUgyiratdarabokService = new EREC_UgyUgyiratdarabokService(this.dataContext);
            ExecParam execParam_ugyiratdarabInsert = _ExecParam.Clone();

            if (_EREC_UgyUgyiratdarabok == null)
            {
                _EREC_UgyUgyiratdarabok = new EREC_UgyUgyiratdarabok();
            }

            // Ügyirathoz kapcsolás
            _EREC_UgyUgyiratdarabok.UgyUgyirat_Id = _EREC_UgyUgyiratok.Id;
            _EREC_UgyUgyiratdarabok.Updated.UgyUgyirat_Id = true;

            // BLG_1713
            // SZUR rendszerből készülhet Belső iktatás Normál főszámmal és Munkairattal
            // BUG_6117
            //if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            //    || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)))
            if ((Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                || ((Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas) && (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas) && (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
                    && (ip.IsSZUR)))
            {
                // NEM LENNE szükséges, csak a régi módszer miatt a felületen még sok helyen ezt jelenítjük meg                
                // ügyiratdarab-hoz főszám és iktatókönyv_id beállítása 
                // (UGYANAZ, mint ami az ügyiratban van)
                _EREC_UgyUgyiratdarabok.Foszam = _EREC_UgyUgyiratok.Foszam;
                _EREC_UgyUgyiratdarabok.Updated.Foszam = true;
            }

            _EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = _EREC_UgyUgyiratok.IraIktatokonyv_Id;
            _EREC_UgyUgyiratdarabok.Updated.IraIktatokonyv_Id = true;

            /// Ügyintéző átvétele az ügyiratból, ha nincs külön megadva az ügyiratdarabnál // KELL??
            _EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez = _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
            _EREC_UgyUgyiratdarabok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;


            // Állapot beállítása:
            _EREC_UgyUgyiratdarabok.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott;
            _EREC_UgyUgyiratdarabok.Updated.Allapot = true;

            // Eljárási szakasz: Osztatlan
            _EREC_UgyUgyiratdarabok.EljarasiSzakasz = KodTarak.ELJARASI_SZAKASZ.Osztatlan;
            _EREC_UgyUgyiratdarabok.Updated.EljarasiSzakasz = true;

            Result result_ugyiratdarabInsert =
                erec_UgyUgyiratdarabokService.Insert(execParam_ugyiratdarabInsert, _EREC_UgyUgyiratdarabok);
            if (!string.IsNullOrEmpty(result_ugyiratdarabInsert.ErrorCode))
            {
                // hiba
                throw new ResultException(result_ugyiratdarabInsert);
            }

            _EREC_UgyUgyiratdarabok.Id = result_ugyiratdarabInsert.Uid;

            #endregion


            // ha alszámra iktatásnál az előzmény ügyirat iktatókönyve lezárt volt,
            // előzmény ügyirat szerelése az újba, ill. a szerelés előkészítése:
            if (!iktatokonyvFolyosorszamos && iktatoKonyvLezart == true)
            {
                string elozmenyUgyiratId = "";
                if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas)
                {
                    elozmenyUgyiratId = _EREC_UgyUgyiratok_Id;
                }
                else
                {
                    elozmenyUgyiratId = ip.SzerelendoUgyiratId;
                }

                // Előzmény ügyirat szerelése az újba, vagy szerelés előkészítése
                if (szerelesMod == SzerelesMod.Szereles)
                {
                    #region Szerelés

                    Logger.Info(@"Előzmény ügyirat iktatókönyve lezárt --> előzmény ügyirat szerelése az újba
            Előzmény ügyirat: " + elozmenyUgyiratId + " Az új ügyirat: " + _EREC_UgyUgyiratok.Id, _ExecParam);

                    ExecParam execParam_szereles = _ExecParam.Clone();
                    Result result_szereles =
                        erec_UgyUgyiratokService.Szereles(execParam_szereles, elozmenyUgyiratId, _EREC_UgyUgyiratok.Id, null);
                    if (!string.IsNullOrEmpty(result_szereles.ErrorCode))
                    {
                        // hiba:
                        Logger.Error("Hiba a szerelés során", _ExecParam, result_szereles);

                        throw new ResultException(result_szereles);
                    }

                    #endregion
                }
                else if (szerelesMod == SzerelesMod.Elokeszites)
                {
                    #region Szerelés előkészítés
                    // Ugyirat lekérése:
                    Logger.Debug("Ügyirat ismételt lekérés szerelés előkészítéshez - Start", _ExecParam);

                    ExecParam execParam_ugyiratGet = _ExecParam.Clone();
                    execParam_ugyiratGet.Record_Id = elozmenyUgyiratId;

                    Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_ugyiratGet);
                    if (!string.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                    {
                        // hiba
                        throw new ResultException(result_ugyiratGet);
                    }
                    else
                    {
                        elozmenyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
                    }

                    Logger.Info(@"Előzmény ügyirat iktatókönyve lezárt --> előzmény ügyirat szerelés előkészítése (szülő bejegyzése, ügyirat UPDATE)
            Előzmény ügyirat: " + elozmenyUgyiratId + " Az új ügyirat: " + _EREC_UgyUgyiratok.Id, _ExecParam);

                    elozmenyUgyirat.Updated.SetValueAll(false);
                    elozmenyUgyirat.Base.Updated.SetValueAll(false);
                    elozmenyUgyirat.Base.Updated.Ver = true;

                    elozmenyUgyirat.UgyUgyirat_Id_Szulo = _EREC_UgyUgyiratok.Id;
                    elozmenyUgyirat.Updated.UgyUgyirat_Id_Szulo = true;

                    ExecParam execParam_elokeszites = _ExecParam.Clone();
                    execParam_elokeszites.Record_Id = elozmenyUgyiratId;
                    Result result_elokeszites =
                        erec_UgyUgyiratokService.Update(execParam_elokeszites, elozmenyUgyirat);
                    if (!string.IsNullOrEmpty(result_elokeszites.ErrorCode))
                    {
                        // hiba:
                        Logger.Error("Hiba a szerelés előkészítés során", _ExecParam, result_elokeszites);

                        throw new ResultException(result_elokeszites);
                    }

                    #endregion
                }
            }

        }
        else if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.AlszamraIktatas
                && (iktatokonyvFolyosorszamos || iktatoKonyvLezart == false))
        {
            // a korábbiakban az elozmenyUgyiratba mentettük a lekért rekordot
            _EREC_UgyUgyiratok = elozmenyUgyirat;


            #region Ügyirat UPDATE
            EREC_UgyUgyiratokService erec_ugyugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);

            _EREC_UgyUgyiratok.Updated.SetValueAll(false);
            _EREC_UgyUgyiratok.Base.Updated.SetValueAll(false);
            _EREC_UgyUgyiratok.Base.Updated.Ver = true;


            ExecParam execParam_ugyiratUpdate = _ExecParam.Clone();
            execParam_ugyiratUpdate.Record_Id = _EREC_UgyUgyiratok.Id;

            // Munkaanyagnál nem kell az alszámot állítani:
            if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
            {
                #region Új alszám beállítása

                erec_ugyugyiratokService.IncrementUtolsoAlszam(_ExecParam, _EREC_UgyUgyiratok);

                #endregion
            }

            // Munkaanyagnál az utolsó munkairat-sorszámot kell állítani:
            if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag)
            {
                Logger.Debug("Utolsó munkairat sorszám állítása", _ExecParam);

                int UjMunkairatSorszam = 0;

                if (string.IsNullOrEmpty(_EREC_UgyUgyiratok.UtolsoSorszam))
                {
                    UjMunkairatSorszam = 1;
                }
                else
                {
                    UjMunkairatSorszam = Int32.Parse(_EREC_UgyUgyiratok.UtolsoSorszam) + 1;
                }

                Logger.Debug(string.Format("Munkairat sorszám:{0}", UjMunkairatSorszam));

                // Utolsó sorszám mező állítása:
                _EREC_UgyUgyiratok.UtolsoSorszam = UjMunkairatSorszam.ToString();
                _EREC_UgyUgyiratok.Updated.UtolsoSorszam = true;
            }

            #region ügyirat határidő módosítás, ha szükséges
            if (!string.IsNullOrEmpty(ip.UgyiratUjHatarido))
            {
                _EREC_UgyUgyiratok.Hatarido = ip.UgyiratUjHatarido;
                _EREC_UgyUgyiratok.Updated.Hatarido = true;
            }
            #endregion ügyirat határidő módosítás, ha szükséges

            // Ügyirat újranyitása:
            if (ugyiratUjranyitasaSzukseges == true)
            {
                Logger.Warn("Ügyirat lezárt, ügyirat újranyitása", _ExecParam);

                _EREC_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
                _EREC_UgyUgyiratok.Updated.Allapot = true;
            }

            if (utolagosPotlasSzukseges == true)
            {
                Logger.Warn("Ügyirat lezárt, és nem kell újranyitni --> utólagos pótlás", _ExecParam);

                // do nothing 
                //(marad lezárt az ügyirat)
            }


            #region Irattárban van-e/Kikölcsönzött?
            bool kolcsonzesInditasKell = false;
            bool atmenetiIrattarbolKikeresKell = false;// lehet átmeneti vagy skontró irattár is
            bool kikolcsonzott = false;

            Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.None;

            if (_EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott
                || _EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott)
            {
                string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam_ugyiratUpdate).Obj_Id;

                // Ha központi irattárban van:
                if (!string.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Felelos) && !string.IsNullOrEmpty(kozpontiIrattarId)
                    && _EREC_UgyUgyiratok.Csoport_Id_Felelos.ToLower() == kozpontiIrattarId.ToLower())
                {
                    kolcsonzesInditasKell = true;
                }
                else
                {
                    // Átmeneti irattárban van:
                    atmenetiIrattarbolKikeresKell = true;
                    kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.AtmenetiIrattar;
                }


                /// Irattárba küldött állapotnál előfordulhat, hogy valaki már beleiktatott, és keletkezett kikérő
                /// Ilyenkor már nem indítunk új kikérést
                if (_EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
                {
                    // ha van találat --> már kikérték, nem kell újra kikérni:
                    if (erec_ugyugyiratokService.ExistsIrattariKikero(_EREC_UgyUgyiratok.Id, _ExecParam.Clone()))
                    {
                        kolcsonzesInditasKell = false;
                        atmenetiIrattarbolKikeresKell = false;
                    }
                }

            }
            else if (_EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban)
            {
                // Skontró irattárban van-e:
                string skontroIrattarId = KodTarak.SPEC_SZERVEK.GetSkontroIrattaros(execParam_ugyiratUpdate).Obj_Id;
                if (!string.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Felelos) && !string.IsNullOrEmpty(skontroIrattarId)
                && _EREC_UgyUgyiratok.Csoport_Id_Felelos.ToLower() == skontroIrattarId.ToLower())
                {
                    atmenetiIrattarbolKikeresKell = true;
                    kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.SkontroIrattar;
                }
            }
            else if (_EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott)
            {
                kikolcsonzott = true;
            }

            #endregion

            // Ha kikölcsönzött ügyiratba iktatunk, állapotot át kell állítani lezártra (az irattári kikérőt pedig Ügyintézésre kikértre)
            if (kikolcsonzott == true)
            {
                // Állapot állítása:
                _EREC_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                _EREC_UgyUgyiratok.Updated.Allapot = true;
            }

            // UgyintezesModját átállítani vegyesre, ha szükséges
            string jelleg = KodTarak.UGYIRAT_JELLEG.Papir;
            if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas)
            {
                jelleg = _EREC_IraIratok.AdathordozoTipusa;
            }
            else
            {
                jelleg = erec_KuldKuldemenyek.AdathordozoTipusa;
            }

            //ha üres, akkor papírnak számít
            if (string.IsNullOrEmpty(jelleg))
            {
                jelleg = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
            }

            if (jelleg == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir && _EREC_UgyUgyiratok.Jelleg != KodTarak.UGYIRAT_JELLEG.Elektronikus
                || jelleg == KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir && _EREC_UgyUgyiratok.Jelleg != KodTarak.UGYIRAT_JELLEG.Papir)
            {
                _EREC_UgyUgyiratok.Jelleg = KodTarak.UGYIRAT_JELLEG.Vegyes;
                _EREC_UgyUgyiratok.Updated.Jelleg = true;
            }

            //Ha elintézett visszakerül az iktatóhoz, és ügyintézés alatti állapotba lesz
            erec_ugyugyiratokService.ElintezetteNyilvanitasVisszavonasaInternal(_ExecParam.Clone(), _EREC_UgyUgyiratok);

            Result result_ugyiratUpdate = erec_ugyugyiratokService.Update(execParam_ugyiratUpdate, _EREC_UgyUgyiratok);
            if (!string.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
            {
                // hiba
                throw new ResultException(result_ugyiratUpdate);
            }
            #endregion


            #region Irattárban lévő ügyiratnál további műveletek

            if (atmenetiIrattarbolKikeresKell == true)
            {
                #region Kikérés indítása:
                Result result_kikeres = erec_ugyugyiratokService.Kikeres(_EREC_UgyUgyiratok.Id, kikeresIrattarTipus, _ExecParam.Clone(), "Iktatás");

                #endregion
            }
            else if (kolcsonzesInditasKell == true)
            {
                #region Kölcsönzés indítása, automatikus jóváhagyással:
                Result result_kikeres = erec_ugyugyiratokService.Kolcsonzes(_EREC_UgyUgyiratok.Id, _ExecParam.Clone(), "Iktatás");

                #endregion
            }
            else if (kikolcsonzott == true)
            {
                // Irattári kikérőt Ügyintézésre kikértre állítjuk:
                EREC_IrattariKikeroService service_irattariKikero = new EREC_IrattariKikeroService(this.dataContext);
                ExecParam execParam_kikeroGet = _ExecParam.Clone();
                execParam_kikeroGet.Record_Id = _EREC_UgyUgyiratok.Id;

                Result result_kikeroGet = service_irattariKikero.GetByUgyiratId(execParam_kikeroGet);
                if (!string.IsNullOrEmpty(result_kikeroGet.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_kikeroGet);
                }
                if (result_kikeroGet.Record != null)
                {
                    EREC_IrattariKikero irattariKikero = (EREC_IrattariKikero)result_kikeroGet.Record;
                    irattariKikero.Updated.SetValueAll(false);
                    irattariKikero.Base.Updated.SetValueAll(false);
                    irattariKikero.Base.Updated.Ver = true;

                    // Ügyintézésre kikértre kell állítani:
                    irattariKikero.FelhasznalasiCel = "U";
                    irattariKikero.Updated.FelhasznalasiCel = true;

                    ExecParam execParam_irattariKikeroUpdate = _ExecParam.Clone();
                    execParam_irattariKikeroUpdate.Record_Id = irattariKikero.Id;

                    Result result_irattariKikeroUpdate = service_irattariKikero.Update(execParam_irattariKikeroUpdate, irattariKikero);
                    if (!string.IsNullOrEmpty(result_irattariKikeroUpdate.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_irattariKikeroUpdate);
                    }
                }
            }

            #endregion

            EREC_UgyUgyiratdarabokService service_ugyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);

            if (ugyiratUjranyitasaSzukseges == true)
            {
                // az ügyirat újra lett nyitva:
                // lezárt ügyiratdarabok állapotát vissza kell állítani
                // kell??

                #region ÜgyiratDarabok állapotának visszaállítása

                ExecParam execParam_ugyiratDarabGetAll = _ExecParam.Clone();

                EREC_UgyUgyiratdarabokSearch search = new EREC_UgyUgyiratdarabokSearch();

                search.UgyUgyirat_Id.Value = _EREC_UgyUgyiratok.Id;
                search.UgyUgyirat_Id.Operator = Query.Operators.equals;

                Result result_ugyiratDarabGetAll = service_ugyiratDarabok.GetAll(execParam_ugyiratDarabGetAll, search);
                if (!string.IsNullOrEmpty(result_ugyiratDarabGetAll.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_ugyiratDarabGetAll);
                }

                foreach (DataRow row in result_ugyiratDarabGetAll.Ds.Tables[0].Rows)
                {
                    string UgyUgyirat_Id = row["UgyUgyirat_Id"].ToString();
                    // ellenőrzés:
                    if (UgyUgyirat_Id != _EREC_UgyUgyiratok.Id)
                    {
                        // hiba:
                        Logger.Error("Hiba az ügyiratdarab lekérdezésekor", _ExecParam);
                        throw new ResultException(52110);
                    }

                    string Id = row["Id"].ToString();
                    string Ver = row["Ver"].ToString();

                    // Ügyiratdarab UPDATE:
                    EREC_UgyUgyiratdarabok erec_ugyugyiratDarab = new EREC_UgyUgyiratdarabok();
                    erec_ugyugyiratDarab.Updated.SetValueAll(false);
                    erec_ugyugyiratDarab.Base.Updated.SetValueAll(false);

                    erec_ugyugyiratDarab.Base.Ver = Ver;
                    erec_ugyugyiratDarab.Base.Updated.Ver = true;

                    erec_ugyugyiratDarab.LezarasDat = "";
                    erec_ugyugyiratDarab.Updated.LezarasDat = true;

                    erec_ugyugyiratDarab.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott;
                    erec_ugyugyiratDarab.Updated.Allapot = true;

                    ExecParam execParam_ugyiratDarabUpdate = _ExecParam.Clone();
                    execParam_ugyiratDarabUpdate.Record_Id = Id;

                    Result result_ugyiratDarabUpdate =
                        service_ugyiratDarabok.Update(execParam_ugyiratDarabUpdate, erec_ugyugyiratDarab);

                    if (!string.IsNullOrEmpty(result_ugyiratDarabUpdate.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_ugyiratDarabUpdate);
                    }

                }
                #endregion
            }

            #region ÜgyiratDarab GET

            // Az ügyirat 'default' ügyiratdarabjának lekérdezése:

            ExecParam execParam_ugyiratDarabGet = _ExecParam.Clone();
            Result result_ugyiratDarabGet =
                service_ugyiratDarabok.GetDefaultUgyiratDarab(execParam_ugyiratDarabGet, _EREC_UgyUgyiratok.Id);
            if (!string.IsNullOrEmpty(result_ugyiratDarabGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratDarabGet);
            }

            if (result_ugyiratDarabGet.Record != null)
            {
                _EREC_UgyUgyiratdarabok = (EREC_UgyUgyiratdarabok)result_ugyiratDarabGet.Record;
            }
            else
            {
                // nem található default ügyiratdarab --> létrehozunk egyet
                // TODO: dobjunk inkább hibát??

                #region ÜgyiratDarab INSERT

                ExecParam execParam_ugyiratdarabInsert = _ExecParam.Clone();

                if (_EREC_UgyUgyiratdarabok == null)
                {
                    _EREC_UgyUgyiratdarabok = new EREC_UgyUgyiratdarabok();
                }

                // Ügyirathoz kapcsolás
                _EREC_UgyUgyiratdarabok.UgyUgyirat_Id = _EREC_UgyUgyiratok.Id;
                _EREC_UgyUgyiratdarabok.Updated.UgyUgyirat_Id = true;


                // NEM LENNE szükséges, csak a régi módszer miatt a felületen még sok helyen ezt jelenítjük meg                
                // ügyiratdarab-hoz főszám és iktatókönyv_id beállítása 
                // (UGYANAZ, mint ami az ügyiratban van)
                _EREC_UgyUgyiratdarabok.Foszam = _EREC_UgyUgyiratok.Foszam;
                _EREC_UgyUgyiratdarabok.Updated.Foszam = true;

                _EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = _EREC_UgyUgyiratok.IraIktatokonyv_Id;
                _EREC_UgyUgyiratdarabok.Updated.IraIktatokonyv_Id = true;

                //Ügyintéző átvétele az ügyiratból // KELL??
                _EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez = _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
                _EREC_UgyUgyiratdarabok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

                // Állapot beállítása:
                if (_EREC_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart)
                {
                    _EREC_UgyUgyiratdarabok.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Lezart;
                }
                else
                {
                    _EREC_UgyUgyiratdarabok.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott;
                }
                _EREC_UgyUgyiratdarabok.Updated.Allapot = true;

                // Eljárási szakasz: Osztatlan
                _EREC_UgyUgyiratdarabok.EljarasiSzakasz = KodTarak.ELJARASI_SZAKASZ.Osztatlan;
                _EREC_UgyUgyiratdarabok.Updated.EljarasiSzakasz = true;

                Result result_ugyiratdarabInsert =
                    service_ugyiratDarabok.Insert(execParam_ugyiratdarabInsert, _EREC_UgyUgyiratdarabok);
                if (!string.IsNullOrEmpty(result_ugyiratdarabInsert.ErrorCode))
                {
                    // hiba
                    throw new ResultException(result_ugyiratdarabInsert);
                }

                _EREC_UgyUgyiratdarabok.Id = result_ugyiratdarabInsert.Uid;

                #endregion

            }
            #endregion
        }
        else
        {
            // Ilyen lehetőség elvileg nincs:
            Logger.Error("VALAMI EL VAN RONTVA AZ IKTATÁSNÁL", _ExecParam);
            throw new ResultException(52100);
        }

        /******************
            Ügyirat, ügyiratdarab létrehozásának/módosításának vége;

            Innentől Irat, IratPéldányok létrehozása
        ***********/


        Logger.Debug("Irat Insert Start", _ExecParam);

        #region Irat INSERT
        // Irat INSERT
        EREC_IraIratokService erec_IraIratokService = new EREC_IraIratokService(this.dataContext);
        ExecParam execParam_iratInsert = _ExecParam.Clone();

        // Irat Note mezőjében van letárolva az iratpéldánynál használatos elosztóívId, azt innen ki kell szedni
        EREC_IraElosztoivek elosztoiv = null;
        //if (!string.IsNullOrEmpty(_EREC_IraIratok.Base.Note))
        string elosztoIv_Id = Contentum.eRecord.BaseUtility.Iratok.GetElosztoivIdFromBusinessObjectsXML(_EREC_IraIratok.Base.Note);
        if (!string.IsNullOrEmpty(elosztoIv_Id))
        {
            EREC_IraElosztoivekService elosztoivService = new EREC_IraElosztoivekService(this.dataContext);
            ExecParam elosztoivExecParam = _ExecParam.Clone();
            //elosztoivExecParam.Record_Id = _EREC_IraIratok.Base.Note;
            elosztoivExecParam.Record_Id = elosztoIv_Id;

            Result elosztoivGetResult = elosztoivService.Get(elosztoivExecParam);
            if (!string.IsNullOrEmpty(elosztoivGetResult.ErrorCode))
                throw new ResultException(53401);

            elosztoiv = (EREC_IraElosztoivek)elosztoivGetResult.Record;
            _EREC_IraIratok.Base.Note = string.Empty;
            _EREC_IraIratok.Base.Updated.Note = false;
        }

        // ügyiratdarabhoz kapcsolás:
        _EREC_IraIratok.UgyUgyIratDarab_Id = _EREC_UgyUgyiratdarabok.Id;
        _EREC_IraIratok.Updated.UgyUgyIratDarab_Id = true;

        // Ügyirathoz kapcsolás, hogy ki lehessen hagyni az ügyiratdarab szintet:
        _EREC_IraIratok.Ugyirat_Id = _EREC_UgyUgyiratok.Id;
        _EREC_IraIratok.Updated.Ugyirat_Id = true;

        if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
        {
            // küldemény id beállítása
            _EREC_IraIratok.KuldKuldemenyek_Id = ip.KuldemenyId;
            _EREC_IraIratok.Updated.KuldKuldemenyek_Id = true;

            // hivatkozási szám átvétele a küldeményből
            _EREC_IraIratok.HivatkozasiSzam = erec_KuldKuldemenyek.HivatkozasiSzam;
            _EREC_IraIratok.Updated.HivatkozasiSzam = true;

            // UgyintezesAlapja (Modja) átvétele a küldeményből
            // BLG_361
            if (string.IsNullOrEmpty(_EREC_IraIratok.UgyintezesAlapja))
            {
                // BUG#7141:
                // Ez az elsődleges adathordozó típusa:
                _EREC_IraIratok.UgyintezesAlapja = erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa; //erec_KuldKuldemenyek.UgyintezesModja;
                _EREC_IraIratok.Updated.UgyintezesAlapja = true;
            }
        }
        else if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas)
        {
            // Belső irat iktatásnál az iratpéldányból vesszük az UgyintezesAlapja-t 
            if (_EREC_PldIratPeldanyok != null)
            {
                // BLG_1876 
                // BLG Kapcsán javítva (BLG_361-ben kezeltük ezt)
                //_EREC_IraIratok.UgyintezesAlapja = _EREC_PldIratPeldanyok.UgyintezesModja;
                _EREC_IraIratok.AdathordozoTipusa = _EREC_PldIratPeldanyok[0].UgyintezesModja;
                _EREC_IraIratok.Updated.UgyintezesAlapja = true;
            }
        }


        // Postázás iránya: Bejövő vagy Belső
        if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
        {
            _EREC_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
        }
        else
        {
            // Lehet kimenő és belső is, felületen választható (BLG_635):
            if (_EREC_IraIratok.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Belso
                && _EREC_IraIratok.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Kimeno)
            {
                _EREC_IraIratok.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Belso;
            }
        }

        _EREC_IraIratok.Updated.PostazasIranya = true;

        // Alszám, iktatás dátuma csak Iktatott anyagnál, munkaanyagnál nem kell:
        if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
        {
            // IKTATOTT ANYAG

            // alszám beállítása
            //_EREC_IraIratok.Alszam = ujAlszam.ToString();
            // az ügyirat utolsó alszáma már a megnövelt alszámot tartalmazza:
            _EREC_IraIratok.Alszam = _EREC_UgyUgyiratok.UtolsoAlszam;
            _EREC_IraIratok.Updated.Alszam = true;

            // iktatás dátuma
            // BLG_1816
            //_EREC_IraIratok.IktatasDatuma = DateTime.Now.ToString();
            if (System.Web.Configuration.WebConfigurationManager.AppSettings["Migration"] != "True")
            {
                _EREC_IraIratok.IktatasDatuma = DateTime.Now.ToString();
            }
            _EREC_IraIratok.Updated.IktatasDatuma = true;
        }
        else
        {
            // MUNKAANYAG

            // Munkaanyagnál Sorszám beállítása (az ügyiratban már beállított utolsóSorszám mező értékére)
            _EREC_IraIratok.Sorszam = _EREC_UgyUgyiratok.UtolsoSorszam;
            _EREC_IraIratok.Updated.Sorszam = true;
        }

        // iktató beállítása
        _EREC_IraIratok.FelhasznaloCsoport_Id_Iktato = FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
        _EREC_IraIratok.Updated.FelhasznaloCsoport_Id_Iktato = true;

        // őrző állítása (iktató)
        _EREC_IraIratok.FelhasznaloCsoport_Id_Orzo = _EREC_IraIratok.FelhasznaloCsoport_Id_Iktato;//FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
        _EREC_IraIratok.Updated.FelhasznaloCsoport_Id_Orzo = true;

        // felelős állítása
        if (Normal_vagy_AlszamraIktatas == NormalVagyAlszamraIktatas.NormalIktatas)
        {
            // felelős állítása (az ügyiratéval azonos)
            _EREC_IraIratok.Csoport_Id_Felelos = _EREC_UgyUgyiratok.Csoport_Id_Felelos;
            _EREC_IraIratok.Updated.Csoport_Id_Felelos = true;
        }
        else
        {
            // felelős állítása ( = iktató)
            _EREC_IraIratok.Csoport_Id_Felelos = FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
            _EREC_IraIratok.Updated.Csoport_Id_Felelos = true;
        }



        // ügyintéző átvétele az ügyiratból
        //_EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez = _EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
        //_EREC_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;


        // Állapot
        if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
        {
            _EREC_IraIratok.Allapot = KodTarak.IRAT_ALLAPOT.Iktatott;
            _EREC_IraIratok.Updated.Allapot = true;
        }
        else
        {
            _EREC_IraIratok.Allapot = KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat;
            _EREC_IraIratok.Updated.Allapot = true;
        }

        // utolsó sorszám beállítása 1-re, mivel az első iratpéldányt mindenképpen létrehozzuk
        _EREC_IraIratok.UtolsoSorszam = "1";
        _EREC_IraIratok.Updated.UtolsoSorszam = true;

        if (!string.IsNullOrEmpty(_EREC_IraIratok.Irattipus))
        {
            #region IratMetaDefId beállítása:

            EREC_IratMetaDefinicioService service_iratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);

            string eljarasiSzakasz = "";
            if (_EREC_UgyUgyiratdarabok != null && !string.IsNullOrEmpty(_EREC_UgyUgyiratdarabok.EljarasiSzakasz))
            {
                eljarasiSzakasz = _EREC_UgyUgyiratdarabok.EljarasiSzakasz;
            }
            else
            {
                eljarasiSzakasz = KodTarak.ELJARASI_SZAKASZ.Osztatlan;
            }

            Result result_iratMetaDef = service_iratMetaDef.GetIratMetaDefinicioByIrattipus(_ExecParam.Clone()
                , _EREC_UgyUgyiratok.IraIrattariTetel_Id, _EREC_UgyUgyiratok.UgyTipus, eljarasiSzakasz, _EREC_IraIratok.Irattipus);
            if (!string.IsNullOrEmpty(result_iratMetaDef.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_iratMetaDef);
            }

            EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_iratMetaDef.Record;
            if (erec_IratMetaDefinicio != null)
            {
                _EREC_IraIratok.IratMetaDef_Id = erec_IratMetaDefinicio.Id;
                _EREC_IraIratok.Updated.IratMetaDef_Id = true;

                _EREC_IraIratok.GeneraltTargy = erec_IratMetaDefinicio.GeneraltTargy;
                _EREC_IraIratok.Updated.GeneraltTargy = true;
            }

            #endregion
        }

        /// Azonosító (Iktatószám) beállítása
        ///
        // BLG_292
        //_EREC_IraIratok.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok);
        _EREC_IraIratok.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok);
        _EREC_IraIratok.Updated.Azonosito = true;

        #endregion

        Result result_iratInsert = erec_IraIratokService.Insert(execParam_iratInsert, _EREC_IraIratok);

        if (!string.IsNullOrEmpty(result_iratInsert.ErrorCode))
        {
            Logger.Error("Irat Insert-nél hiba", _ExecParam, result_iratInsert);

            // hiba
            throw new ResultException(result_iratInsert);
        }
        else
        {
            string UjIrat_Id = result_iratInsert.Uid;

            _EREC_IraIratok.Id = UjIrat_Id;

            //iktatasResult.UjIrat_Id = result_iratInsert.Uid;


            // Kezelési feljegyzések felvétel, ha meg van adva
            if (_EREC_HataridosFeladatok != null)
            {
                // Irat kezelési feljegyzés INSERT
                EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
                ExecParam execParam_kezfeljegyzesek = _ExecParam.Clone();

                _EREC_HataridosFeladatok.Obj_Id = UjIrat_Id;
                _EREC_HataridosFeladatok.Updated.Obj_Id = true;

                _EREC_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;
                _EREC_HataridosFeladatok.Updated.Obj_type = true;

                if (!string.IsNullOrEmpty(_EREC_IraIratok.Azonosito))
                {
                    _EREC_HataridosFeladatok.Azonosito = _EREC_IraIratok.Azonosito;
                    _EREC_HataridosFeladatok.Updated.Azonosito = true;
                }

                Result result_kezfeljegyzes = erec_HataridosFeladatokService.Insert(execParam_kezfeljegyzesek, _EREC_HataridosFeladatok);
                if (!string.IsNullOrEmpty(result_kezfeljegyzes.ErrorCode))
                {
                    Logger.Error("Kezelési feljegyzés létrehozásánál hiba", _ExecParam, result_kezfeljegyzes);

                    // hiba
                    throw new ResultException(result_kezfeljegyzes);
                }
            }


            #region Küldemény mellékletek és csatolmányok átmásolása az irathoz
            if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
            {
                ExecParam execParam_MellekletCopy = _ExecParam.Clone();

                Copy_KuldMellekletek_Csatolmanyok(execParam_MellekletCopy, erec_KuldKuldemenyek.Id, _EREC_IraIratok.Id, erec_IraIktatoKonyvek.Iktatohely, _EREC_UgyUgyiratok.Foszam, erec_IraIktatoKonyvek.Ev, _EREC_IraIratok.Alszam);
            }
            #endregion


            #region EREC_IraOnkormAdatok INSERT
            // mind a rekord Id-je, mind az IraIratok_Id megegyezik az irat azonosítójával
            EREC_IraOnkormAdatokService service_IraOnkormAdatok = new EREC_IraOnkormAdatokService(this.dataContext);
            ExecParam execparam_IraOnkormAdatok = _ExecParam.Clone();
            execparam_IraOnkormAdatok.Record_Id = _EREC_IraIratok.Id;

            Logger.Debug("Hatósági adatok INSERT start", execparam_IraOnkormAdatok);

            EREC_IraOnkormAdatok erec_IraOnkormAdatok = new EREC_IraOnkormAdatok();
            erec_IraOnkormAdatok.Id = _EREC_IraIratok.Id;
            erec_IraOnkormAdatok.Updated.Id = true;
            erec_IraOnkormAdatok.IraIratok_Id = _EREC_IraIratok.Id;
            erec_IraOnkormAdatok.Updated.IraIratok_Id = true;

            EREC_IratMetaDefinicioService service_IratMetaDefinicio = new EREC_IratMetaDefinicioService(this.dataContext);
            ExecParam execparam_IratMetaDefinicio = _ExecParam.Clone();
            Result result_IraMetaDefinicio;

            // UgyFajta kiolvasasa az EREC_IratMetaDefinicio alapján
            // (nem hatósági v. önkormányzati v. államigazgatási)
            if (!string.IsNullOrEmpty(_EREC_UgyUgyiratok.IratMetadefinicio_Id))
            {
                execparam_IratMetaDefinicio.Record_Id = _EREC_UgyUgyiratok.IratMetadefinicio_Id;
                result_IraMetaDefinicio = service_IratMetaDefinicio.Get(execparam_IratMetaDefinicio);
                if (!string.IsNullOrEmpty(result_IraMetaDefinicio.ErrorCode))
                {
                    Logger.Debug("Irat metadefiníció ill. ügyfajta meghatározása sikertelen: ", execparam_IratMetaDefinicio, result_IraMetaDefinicio);

                    // hiba 
                    throw new ResultException(result_IraMetaDefinicio);
                }

            }
            if (!string.IsNullOrEmpty(_EREC_UgyUgyiratok.UgyTipus))
            {
                EREC_IratMetaDefinicioSearch search_IratMetaDefinicio = new EREC_IratMetaDefinicioSearch();
                search_IratMetaDefinicio.Ugytipus.Value = _EREC_UgyUgyiratok.UgyTipus;
                search_IratMetaDefinicio.Ugytipus.Operator = Query.Operators.equals;

                search_IratMetaDefinicio.Ugykor_Id.Value = _EREC_UgyUgyiratok.IraIrattariTetel_Id;
                search_IratMetaDefinicio.Ugykor_Id.Operator = Query.Operators.equals;

                search_IratMetaDefinicio.EljarasiSzakasz.Operator = Query.Operators.isnull;
                search_IratMetaDefinicio.Irattipus.Operator = Query.Operators.isnull;

                search_IratMetaDefinicio.TopRow = 1;

                search_IratMetaDefinicio.ErvKezd.Clear();
                search_IratMetaDefinicio.ErvVege.Clear();

                search_IratMetaDefinicio.OrderBy = "EREC_IratMetaDefinicio.LetrehozasIdo desc";

                result_IraMetaDefinicio = service_IratMetaDefinicio.GetAll(execparam_IratMetaDefinicio, search_IratMetaDefinicio);

                if (!string.IsNullOrEmpty(result_IraMetaDefinicio.ErrorCode))
                {
                    Logger.Debug("Irat metadefiníció ill. ügyfajta meghatározása sikertelen: ", execparam_IratMetaDefinicio, result_IraMetaDefinicio);

                    // hiba 
                    throw new ResultException(result_IraMetaDefinicio);
                }
                else
                {
                    if (result_IraMetaDefinicio.Ds.Tables[0].Rows.Count > 0)
                    {
                        erec_IraOnkormAdatok.UgyFajtaja = result_IraMetaDefinicio.Ds.Tables[0].Rows[0]["UgyFajta"].ToString();
                        erec_IraOnkormAdatok.Updated.UgyFajtaja = true;
                    }
                    else
                    {
                        // nincs definíció az UgyFajtaja-ra
                        erec_IraOnkormAdatok.UgyFajtaja = "<null>";
                        erec_IraOnkormAdatok.Updated.UgyFajtaja = true;
                    }
                }
            }
            else
            {
                // UgyTipus üres
                erec_IraOnkormAdatok.UgyFajtaja = "<null>";
                erec_IraOnkormAdatok.Updated.UgyFajtaja = true;
            }

            Result result_IraOnkormAdatokInsert = service_IraOnkormAdatok.Insert(execparam_IraOnkormAdatok, erec_IraOnkormAdatok);
            if (!string.IsNullOrEmpty(result_IraOnkormAdatokInsert.ErrorCode))
            {
                Logger.Debug("Önkormányzati adatok Insert sikertelen: ", execparam_IraOnkormAdatok, result_IraOnkormAdatokInsert);

                // hiba 
                throw new ResultException(result_IraOnkormAdatokInsert);
            }

            #endregion



            Logger.Debug("Iratpéldány Insert Start", _ExecParam);

            #region IratPéldány INSERT
            // IratPéldány INSERT

            if (ip.Atiktatas && !string.IsNullOrEmpty(ip.IratId))
            {
                EREC_PldIratPeldanyokService iratpeldanyService = new EREC_PldIratPeldanyokService(this.dataContext);
                EREC_PldIratPeldanyokSearch iratpeldanySearch = new EREC_PldIratPeldanyokSearch();

                iratpeldanySearch.IraIrat_Id.Value = ip.IratId;
                iratpeldanySearch.IraIrat_Id.Operator = Query.Operators.equals;

                Result iratpeldanyGetAllResult = iratpeldanyService.GetAll(execparam_IraOnkormAdatok.Clone(), iratpeldanySearch);
                if (!string.IsNullOrEmpty(iratpeldanyGetAllResult.ErrorCode) || iratpeldanyGetAllResult.Ds.Tables.Count < 1)
                    throw new ResultException(iratpeldanyGetAllResult);

                foreach (DataRow r in iratpeldanyGetAllResult.Ds.Tables[0].Rows)
                {
                    ExecParam iratpeldanyGetExecParam = execparam_IraOnkormAdatok.Clone();
                    iratpeldanyGetExecParam.Record_Id = r["Id"].ToString();
                    EREC_PldIratPeldanyok iratpeldany = (EREC_PldIratPeldanyok)iratpeldanyService.Get(iratpeldanyGetExecParam.Clone()).Record;

                    iratpeldany.Updated.SetValueAll(false);
                    iratpeldany.Base.Updated.SetValueAll(false);
                    iratpeldany.Base.Updated.Ver = true;

                    iratpeldany.IraIrat_Id = _EREC_IraIratok.Id;
                    iratpeldany.Updated.IraIrat_Id = true;

                    /// Azonosító (Iktatószám) felülírása:
                    /// 
                    // BLG_292
                    //iratpeldany.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, iratpeldany);
                    iratpeldany.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, iratpeldany);
                    iratpeldany.Updated.Azonosito = true;

                    Result iratpeldanyUpdateResult = iratpeldanyService.Update(iratpeldanyGetExecParam.Clone(), iratpeldany);
                    if (!string.IsNullOrEmpty(iratpeldanyUpdateResult.ErrorCode))
                        throw new ResultException(iratpeldanyUpdateResult);

                }
            }
            else
            {
                EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
                ExecParam execParam_IratPeldanyInsert = _ExecParam.Clone();

                //if (_EREC_PldIratPeldanyok == null)
                //{
                //    _EREC_PldIratPeldanyok = new EREC_PldIratPeldanyok();
                //}
                for (int i = 0; i < _EREC_PldIratPeldanyok.Length; i++)
                {
                    // hivatkozás az új iratra:
                    _EREC_PldIratPeldanyok[i].IraIrat_Id = UjIrat_Id;
                    _EREC_PldIratPeldanyok[i].Updated.IraIrat_Id = true;

                    _EREC_PldIratPeldanyok[i].Eredet = KodTarak.IRAT_FAJTA.Eredeti;
                    _EREC_PldIratPeldanyok[i].Updated.Eredet = true;
                }


                // BejovoIratIktatas -nal Kuldemeny-ből veszünk át néhány mezőt:
                // (címzett, cím, küldés módja, továbbító)
                if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BejovoIratIktatas)
                {
                    // Címzett:

                    // Partner kiderítése a Kuldemeny.Csoport_Id_Cimzett alapján
                    // Ha egyszemélyes a csoport, akkor van esély megtalálni a partnert:
                    // Egyszemélyes csoport esetén a csoportId = felhasznaloId, 
                    // és a Felhasznalóhoz lehet hozzárendelve Partner (KRT_Felhasznalok.Partner_id)

                    for (int i = 0; i < _EREC_PldIratPeldanyok.Length; i++)
                    {
                        try
                        {
                            KRT_Partnerek krt_partner_cimzett = GetKRT_PartnerekByCsoportId(erec_KuldKuldemenyek.Csoport_Id_Cimzett, _ExecParam);

                            if (krt_partner_cimzett != null && !string.IsNullOrEmpty(krt_partner_cimzett.Id))
                            {
                                _EREC_PldIratPeldanyok[i].Partner_Id_Cimzett = krt_partner_cimzett.Id;
                                _EREC_PldIratPeldanyok[i].Updated.Partner_Id_Cimzett = true;

                                _EREC_PldIratPeldanyok[i].NevSTR_Cimzett = krt_partner_cimzett.Nev;
                                _EREC_PldIratPeldanyok[i].Updated.NevSTR_Cimzett = true;
                            }
                            else
                            {
                                Logger.Debug("Küldemény címzettjéhez tartozó partner megállapítása sikertelen", _ExecParam);
                                Logger.Debug("Ellenőrzés elosztóív van-e az iratpéldány címzettjében");

                                EREC_IraElosztoivekService elosztoivService = new EREC_IraElosztoivekService(this.dataContext);

                                EREC_IraElosztoivekSearch elosztoivSearch = new EREC_IraElosztoivekSearch();
                                elosztoivSearch.Id.Value = erec_KuldKuldemenyek.Csoport_Id_Cimzett;//_ExecParam.Record_Id;
                                elosztoivSearch.Id.Operator = Query.Operators.equals;

                                //Result elosztoivGetResult = elosztoivService.Get(_ExecParam);
                                Result elosztoivGetResult = elosztoivService.GetAll(_ExecParam, elosztoivSearch);

                                if (!string.IsNullOrEmpty(elosztoivGetResult.ErrorCode) || elosztoivGetResult.Ds.Tables.Count == 0 || elosztoivGetResult.Ds.Tables[0].Rows.Count != 1)
                                {
                                    Logger.Debug("A címzett nem elosztóív");
                                    throw new Exception("A címzett nem partner és nem is elosztóív");
                                }

                                elosztoiv = new EREC_IraElosztoivek();
                                Utility.LoadBusinessDocumentFromDataRow(elosztoiv, elosztoivGetResult.Ds.Tables[0].Rows[0]);

                                //elosztoiv = (EREC_IraElosztoivek)elosztoivGetResult.Record;
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Debug("EXCEPTION ELKAPVA Küldemény címzettjéhez tartozó partner megállapításakor", e);
                            // Mehet tovább, ez nem kritikus hiba
                        }

                        // KüldésMód:
                        _EREC_PldIratPeldanyok[i].KuldesMod = erec_KuldKuldemenyek.KuldesMod;
                        _EREC_PldIratPeldanyok[i].Updated.KuldesMod = true;

                        // Továbbító:
                        _EREC_PldIratPeldanyok[i].Tovabbito = erec_KuldKuldemenyek.Tovabbito;
                        _EREC_PldIratPeldanyok[i].Updated.Tovabbito = false;
                    }

                }
                else
                {
                    // elosztóív van-e a címzett mezőben
                    if (!string.IsNullOrEmpty(_EREC_PldIratPeldanyok[0].Partner_Id_Cimzett))
                    {
                        EREC_IraElosztoivekService elosztoivService = new EREC_IraElosztoivekService(this.dataContext);
                        //ExecParam elosztoivGetExecParam = execParam_IratPeldanyInsert.Clone();
                        //elosztoivGetExecParam.Record_Id = _EREC_PldIratPeldanyok.Partner_Id_Cimzett;

                        //Result elosztoivGetResult = elosztoivService.Get(elosztoivGetExecParam);
                        //if (string.IsNullOrEmpty(elosztoivGetResult.ErrorCode) && elosztoivGetResult.Record != null)
                        //    elosztoiv = (EREC_IraElosztoivek)elosztoivGetResult.Record;

                        ExecParam elosztoivGetAllExecParam = execParam_IratPeldanyInsert.Clone();
                        EREC_IraElosztoivekSearch elosztoivSearch = new EREC_IraElosztoivekSearch();
                        elosztoivSearch.Id.Value = _EREC_PldIratPeldanyok[0].Partner_Id_Cimzett;
                        elosztoivSearch.Id.Operator = Query.Operators.equals;

                        Result elosztoivGetAllResult = elosztoivService.GetAll(elosztoivGetAllExecParam, elosztoivSearch);

                        if (elosztoivGetAllResult.IsError || elosztoivGetAllResult.Ds.Tables[0].Rows.Count == 0)
                        {
                            Logger.Info("Iktatás (belső): A címzett nem elosztóív");
                        }
                        else
                        {
                            elosztoiv = new EREC_IraElosztoivek();
                            Utility.LoadBusinessDocumentFromDataRow(elosztoiv, elosztoivGetAllResult.Ds.Tables[0].Rows[0]);
                        }
                    }
                }
                // mindig insert előtt növeljük
                int sorszam_iratPeldany = 0;
                var bid = new Result();
                for (int i = 0; i < _EREC_PldIratPeldanyok.Length; i++)
                {
                    ++sorszam_iratPeldany;
                    // Felelős ( = Irat.Felelős)
                    _EREC_PldIratPeldanyok[i].Csoport_Id_Felelos = _EREC_IraIratok.Csoport_Id_Felelos;
                    _EREC_PldIratPeldanyok[i].Updated.Csoport_Id_Felelos = true;

                    // Őrző ( = Irat.Őrző)
                    _EREC_PldIratPeldanyok[i].FelhasznaloCsoport_Id_Orzo = _EREC_IraIratok.FelhasznaloCsoport_Id_Orzo;
                    _EREC_PldIratPeldanyok[i].Updated.FelhasznaloCsoport_Id_Orzo = true;

                    if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                    {
                        // Állapot állítása
                        _EREC_PldIratPeldanyok[i].Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                        _EREC_PldIratPeldanyok[i].Updated.Allapot = true;

                        // Továbbítás alatt állapot állítása
                        _EREC_PldIratPeldanyok[i].TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                        _EREC_PldIratPeldanyok[i].Updated.TovabbitasAlattAllapot = true;
                    }
                    else
                    {
                        // Munkaanyagnál:
                        // Állapot állítása
                        _EREC_PldIratPeldanyok[i].Allapot = KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban;
                        _EREC_PldIratPeldanyok[i].Updated.Allapot = true;

                        // Továbbítás alatt állapot állítása
                        _EREC_PldIratPeldanyok[i].TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                        _EREC_PldIratPeldanyok[i].Updated.TovabbitasAlattAllapot = true;
                    }



                    // Sorszám 1 lesz, mivel ez az új irat első példánya
                    _EREC_PldIratPeldanyok[i].Sorszam = sorszam_iratPeldany.ToString();
                    _EREC_PldIratPeldanyok[i].Updated.Sorszam = true;
                    bid = new Result();
                    //BARCODE

                    //CR 3058
                    if (vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
                    {
                        EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                        KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                        var execParam_Irat = _ExecParam.Clone();
                        execParam_Irat.Record_Id = result_iratInsert.Uid;
                        Result Erec_Irat = erec_IraIratokService.Get(execParam_Irat);
                        var execParam_Ugyirat = _ExecParam.Clone();
                        execParam_Ugyirat.Record_Id = _EREC_UgyUgyiratok.Id;
                        Result Erec_Ugyirat = erec_UgyUgyiratokService.Get(execParam_Ugyirat);


                        #region CR 3111 Budaörsön a vonalkód generálás Munkapéldány létrehozásakor mindig 0-s sorszámot ad.
                        var iktatokonyv = new EREC_IraIktatoKonyvek();
                        iktatokonyv = GetIktatokonyvById(_EREC_UgyUgyiratok.IraIktatokonyv_Id, execParam_Irat);
                        string barcode =
                            iktatokonyv.Iktatohely + " /" +
                            (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag ?
                                ("MU" +
                                    (string.IsNullOrEmpty(((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam) ?
                                        ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Sorszam.PadLeft(6, '0') :
                                        ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam.PadLeft(6, '0'))
                                    ) :
                                    ((EREC_UgyUgyiratok)Erec_Ugyirat.Record).Foszam.PadLeft(6, '0'))
                            + " - " +
                            (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag ?
                                ("MI" + ((EREC_IraIratok)Erec_Irat.Record).Sorszam.PadLeft(6, '0')) :
                                ((EREC_IraIratok)Erec_Irat.Record).Alszam.PadLeft(6, '0'))
                            + " /" + iktatokonyv.Ev;
                        #endregion
                        barcode = barcode.Replace(" ", "");
                        KRT_Barkodok barcodeRecord = new KRT_Barkodok();
                        barcodeRecord.Kod = barcode;

                        bid = srvBarcode.Insert(execParam_Irat, barcodeRecord);
                        if (!string.IsNullOrEmpty(bid.ErrorCode))
                        {
                            // hiba
                            Logger.Error("Hiba: Barkod rekordhoz ügyirat Insert", execParam_Irat);
                            throw new ResultException(bid);
                        }
                        //var result_barcodebind = srvBarcode.BarkodBindToIratPeldany(execParam_IratPeldanyInsert, bid.Uid, result_iratInsert.Uid);
                        //if (!string.IsNullOrEmpty(result_barcodebind.ErrorCode))
                        //{
                        //    // hiba
                        //    Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", execParam_Irat);
                        //    throw new ResultException(result_barcodebind);
                        //}
                        barkod_pld1 = barcode;

                    }
                    if (i == 0)  //Csak az elsőnek van megadott barcode-ja
                    {
                        _EREC_PldIratPeldanyok[i].BarCode = barkod_pld1;
                        _EREC_PldIratPeldanyok[i].Updated.BarCode = true;
                    }

                    /// Azonosító (Iktatószám) beállítása:
                    /// 
                    // BLG_292
                    //_EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok);
                    _EREC_PldIratPeldanyok[i].Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok[i]);

                    _EREC_PldIratPeldanyok[i].Updated.Azonosito = true;

                }
                Result result_IratPeldanyInsert = new Result();
                if (elosztoiv != null)
                {
                    #region Elosztóívek kezelése

                    sorszam_iratPeldany = 0;

                    // elosztóív listát adtak meg címzettként
                    EREC_IraElosztoivTetelekService elosztoivTetelekService = new EREC_IraElosztoivTetelekService(this.dataContext);
                    EREC_IraElosztoivTetelekSearch elosztoivTetelekSearch = new EREC_IraElosztoivTetelekSearch();

                    elosztoivTetelekSearch.ElosztoIv_Id.Value = elosztoiv.Id;
                    elosztoivTetelekSearch.ElosztoIv_Id.Operator = Query.Operators.equals;

                    Result elosztoivTetelekGetAllResult = elosztoivTetelekService.GetAll(execParam_IratPeldanyInsert, elosztoivTetelekSearch);

                    if (string.IsNullOrEmpty(elosztoivTetelekGetAllResult.ErrorCode))
                    {
                        KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
                        ExecParam barkodExecParam = _ExecParam.Clone();
                        Result barkodGenerateResult = new Result();
                        string kuldesMod = _EREC_PldIratPeldanyok[0].KuldesMod;

                        for (int i = 0; i < elosztoivTetelekGetAllResult.Ds.Tables[0].Rows.Count; i++)
                        {
                            EREC_PldIratPeldanyok iratpeldany = _EREC_PldIratPeldanyok[0];
                            // partnerId beállítás
                            iratpeldany.Partner_Id_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Partner_Id"].ToString();
                            iratpeldany.Updated.Partner_Id_Cimzett = true;
                            iratpeldany.NevSTR_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["NevSTR"].ToString();
                            iratpeldany.Updated.NevSTR_Cimzett = true;

                            // címzett beállítás
                            iratpeldany.Cim_id_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Cim_Id"].ToString();
                            iratpeldany.Updated.Cim_id_Cimzett = true;
                            iratpeldany.CimSTR_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["CimSTR"].ToString();
                            iratpeldany.Updated.CimSTR_Cimzett = true;

                            // küldés mód beállítása az elosztóívtételeknél beállítottra, ha iktatáskor nem adták meg

                            if (string.IsNullOrEmpty(kuldesMod))
                                iratpeldany.KuldesMod = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Kuldesmod"].ToString();

                            // sorszám beállítása

                            sorszam_iratPeldany++;

                            iratpeldany.Sorszam = sorszam_iratPeldany.ToString();
                            iratpeldany.Updated.Sorszam = true;

                            for (int j = 0; j < _EREC_PldIratPeldanyok.Length; j++)
                            {
                                // Állapotot újra be kell állítani, mert visszaíródik az előző insert után a továbbítás alatt állapot
                                if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                                {
                                    // Állapot állítása
                                    _EREC_PldIratPeldanyok[j].Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                                    _EREC_PldIratPeldanyok[j].Updated.Allapot = true;

                                    // Továbbítás alatt állapot állítása
                                    _EREC_PldIratPeldanyok[j].TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                                    _EREC_PldIratPeldanyok[j].Updated.TovabbitasAlattAllapot = true;

                                }
                                else
                                {
                                    // Munkaanyagnál:
                                    // Állapot állítása
                                    _EREC_PldIratPeldanyok[j].Allapot = KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban;
                                    _EREC_PldIratPeldanyok[j].Updated.Allapot = true;

                                    // Továbbítás alatt állapot állítása
                                    _EREC_PldIratPeldanyok[j].TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                                    _EREC_PldIratPeldanyok[j].Updated.TovabbitasAlattAllapot = true;

                                }

                                string UjIratPeldany_Id = result_IratPeldanyInsert.Uid;
                                _EREC_PldIratPeldanyok[i].Id = UjIratPeldany_Id;
                            }
                            // vonalkód generálás (csak ha ügyintézésmódja elektronikus:)
                            string generaltVonalkod = "";
                            //CR 3058
                            if (_EREC_PldIratPeldanyok[0].UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir && !vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
                            {
                                barkodGenerateResult = barkodService.BarkodGeneralas(barkodExecParam);
                                if (!string.IsNullOrEmpty(barkodGenerateResult.ErrorCode))
                                    throw new ResultException(barkodGenerateResult);

                                generaltVonalkod = (string)barkodGenerateResult.Record;
                            }
                            iratpeldany.BarCode = generaltVonalkod;
                            iratpeldany.Updated.BarCode = true;

                            /// Azonosító (Iktatószám) beállítása:
                            /// 
                            // BLG_292
                            //iratpeldany.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, iratpeldany);
                            iratpeldany.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, iratpeldany);
                            iratpeldany.Updated.Azonosito = true;

                            // iratpéldány insert;
                            result_IratPeldanyInsert = erec_PldIratPeldanyokService.Insert(execParam_IratPeldanyInsert, iratpeldany);
                            if (!string.IsNullOrEmpty(result_IratPeldanyInsert.ErrorCode))
                            {
                                Logger.Error("Iratpéldány Insert-nél hiba", _ExecParam, result_IratPeldanyInsert);
                                throw new ResultException(result_IratPeldanyInsert);
                            }

                            // vonalkód kötése, ha volt generálás:
                            //CR 3058
                            if ((!string.IsNullOrEmpty(iratpeldany.BarCode)
                                && !string.IsNullOrEmpty(barkodGenerateResult.Uid)) && !vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
                            {
                                barkodGenerateResult = barkodService.BarkodBindToIratPeldany(_ExecParam.Clone(), barkodGenerateResult.Uid, result_IratPeldanyInsert.Uid);
                                /*if (!string.IsNullOrEmpty(barkodGenerateResult.ErrorCode))
                                {
                                    throw new ResultException(barkodGenerateResult);
                                }*/
                            }
                            else if ((!string.IsNullOrEmpty(iratpeldany.BarCode)
                                && vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO")))
                            {
                                KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                                var result_barcodebind = srvBarcode.BarkodBindToIratPeldany(_ExecParam.Clone(), bid.Uid, result_IratPeldanyInsert.Uid);
                                if (!string.IsNullOrEmpty(result_barcodebind.ErrorCode))
                                {
                                    // hiba
                                    Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", _ExecParam.Clone());
                                    throw new ResultException(result_barcodebind);
                                }
                            }
                        }

                        if (elosztoivTetelekGetAllResult.Ds.Tables[0].Rows.Count == 0)
                            throw new ResultException(53400);
                    }
                    #endregion
                }
                else
                {
                    for (int k = 0; k < _EREC_PldIratPeldanyok.Length; k++)
                    {
                        result_IratPeldanyInsert = erec_PldIratPeldanyokService.Insert(
                        execParam_IratPeldanyInsert, _EREC_PldIratPeldanyok[k]);
                        if (!string.IsNullOrEmpty(result_IratPeldanyInsert.ErrorCode))
                        {
                            Logger.Error("Iratpéldány Insert-nél hiba", _ExecParam, result_IratPeldanyInsert);

                            // hiba
                            throw new ResultException(result_IratPeldanyInsert);
                        }
                        else
                        {
                            string newpldId = result_IratPeldanyInsert.Uid;
                            if (k == 0) //Csak az elsőnek van barcode-ja
                            {
                                #region BARCODE UPDATE
                                KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                                ExecParam execParam_barcodeUpdate = _ExecParam.Clone();

                                Result result_barcodeUpdate = service_barkodok.CheckBarcode(execParam_barcodeUpdate.Clone(), _EREC_PldIratPeldanyok[k].BarCode);
                                //CR 3058
                                if (!string.IsNullOrEmpty(_EREC_PldIratPeldanyok[k].BarCode) && !vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
                                {


                                    if (result_barcodeUpdate.ErrorCode == "52482")
                                    {
                                        result_barcodeUpdate = service_barkodok.GetBarcodeByValue(execParam_barcodeUpdate.Clone(), _EREC_PldIratPeldanyok[k].BarCode);

                                        if (!string.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
                                            throw new ResultException(result_barcodeUpdate);

                                        if ((result_barcodeUpdate.Record as KRT_Barkodok).Obj_Id != ip.KuldemenyId)
                                            throw new ResultException(52482);

                                        result_barcodeUpdate = service_barkodok.FreeBarcode(execParam_barcodeUpdate.Clone(), _EREC_PldIratPeldanyok[k].BarCode);

                                        result_barcodeUpdate = service_barkodok.GetBarcodeByValue(execParam_barcodeUpdate.Clone(), _EREC_PldIratPeldanyok[k].BarCode);

                                        string ver = (result_barcodeUpdate.Record as KRT_Barkodok).Base.Ver;

                                        result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                                            execParam_barcodeUpdate, barkod_pld1_Id, newpldId, ver);
                                    }
                                    else
                                    {
                                        result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                                            execParam_barcodeUpdate, barkod_pld1_Id, newpldId, barkod_pld1_Ver);
                                    }

                                    if (!string.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
                                    {
                                        Logger.Error("Hiba: Barkod rekordhoz iratpéldány hozzákötése", _ExecParam);
                                        throw new ResultException(result_barcodeUpdate);
                                    }
                                }
                                else if (vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))//CR 3058
                                {
                                    result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                                            execParam_barcodeUpdate, bid.Uid, newpldId);
                                }
                                #endregion
                            }
                        }

                        string UjIratPeldany_Id = result_IratPeldanyInsert.Uid;
                        _EREC_PldIratPeldanyok[k].Id = UjIratPeldany_Id;
                    }
                }
                #endregion

                EREC_PldIratPeldanyok IratPldForExpedialas = _EREC_PldIratPeldanyok[0];
                /// Plusz Iratpéldányok készítése, ha kérték:                

                #region Plusz iratpéldány az ügyirat felelősének:

                if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas &&
                    ip.UgyiratPeldanySzukseges == true && !string.IsNullOrEmpty(_EREC_UgyUgyiratok.Csoport_Id_Felelos))
                {
                    KRT_Partnerek UgyiratFelelos_Partner = null;

                    try
                    {
                        UgyiratFelelos_Partner =
                            GetKRT_PartnerekByCsoportId(_EREC_UgyUgyiratok.Csoport_Id_Felelos, _ExecParam);
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Hiba új iratpéldány létrehozásakor (ügyirat felelősének szóló példánynál)", e);
                        // nem kritikus a hiba, nem kell emiatt rollback-elni
                    }

                    if (UgyiratFelelos_Partner != null && !string.IsNullOrEmpty(UgyiratFelelos_Partner.Id))
                    {
                        sorszam_iratPeldany++;
                        EREC_PldIratPeldanyok pluszIratPeldanyFelelos = _EREC_PldIratPeldanyok[0];
                        pluszIratPeldanyFelelos.Sorszam = sorszam_iratPeldany.ToString();
                        pluszIratPeldanyFelelos.Updated.Sorszam = true;

                        // Felelős beállítása
                        pluszIratPeldanyFelelos.Csoport_Id_Felelos = _EREC_UgyUgyiratok.Csoport_Id_Felelos;
                        pluszIratPeldanyFelelos.Updated.Csoport_Id_Felelos = true;

                        if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                        {
                            // Állapot állítása
                            pluszIratPeldanyFelelos.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                            pluszIratPeldanyFelelos.Updated.Allapot = true;

                            // Továbbítás alatt állapot állítása
                            pluszIratPeldanyFelelos.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                            pluszIratPeldanyFelelos.Updated.TovabbitasAlattAllapot = true;
                        }
                        else
                        {
                            // Munkaanyagnál:
                            // Állapot állítása
                            pluszIratPeldanyFelelos.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban;
                            pluszIratPeldanyFelelos.Updated.Allapot = true;

                            // Továbbítás alatt állapot állítása
                            pluszIratPeldanyFelelos.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                            pluszIratPeldanyFelelos.Updated.TovabbitasAlattAllapot = true;

                        }

                        pluszIratPeldanyFelelos.Eredet = KodTarak.IRAT_FAJTA.Masolat;
                        pluszIratPeldanyFelelos.Updated.Eredet = true;

                        pluszIratPeldanyFelelos.Partner_Id_Cimzett = UgyiratFelelos_Partner.Id;
                        pluszIratPeldanyFelelos.Updated.Partner_Id_Cimzett = true;

                        pluszIratPeldanyFelelos.NevSTR_Cimzett = UgyiratFelelos_Partner.Nev;
                        pluszIratPeldanyFelelos.Updated.NevSTR_Cimzett = true;

                        // TODO: Mi legyen a címmel?
                        pluszIratPeldanyFelelos.Cim_id_Cimzett = string.Empty;
                        pluszIratPeldanyFelelos.Updated.Cim_id_Cimzett = true;

                        pluszIratPeldanyFelelos.CimSTR_Cimzett = string.Empty;
                        pluszIratPeldanyFelelos.Updated.CimSTR_Cimzett = true;

                        // BARCODE
                        pluszIratPeldanyFelelos.BarCode = barkod_pld2;
                        pluszIratPeldanyFelelos.Updated.BarCode = true;

                        /// Azonosító (Iktatószám) beállítása
                        /// 
                        // BLG_292
                        //_EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok);

                        pluszIratPeldanyFelelos.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, pluszIratPeldanyFelelos);
                        pluszIratPeldanyFelelos.Updated.Azonosito = true;

                        ExecParam execParam_PluszPeldany1 = _ExecParam.Clone();

                        Result result_PluszPeldany1 =
                            erec_PldIratPeldanyokService.Insert(execParam_PluszPeldany1, pluszIratPeldanyFelelos);
                        if (!string.IsNullOrEmpty(result_PluszPeldany1.ErrorCode))
                        {
                            Logger.Error("Ügyirat felelősének szóló iratpéldány létrehozásakor hiba", _ExecParam, result_PluszPeldany1);

                            // hiba
                            sorszam_iratPeldany--;

                            throw new ResultException(result_PluszPeldany1);
                        }
                        else
                        {
                            #region BARCODE UPDATE

                            string newpldId = result_PluszPeldany1.Uid;

                            if (!string.IsNullOrEmpty(pluszIratPeldanyFelelos.BarCode))
                            {
                                KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                                ExecParam execParam_barcodeUpdate = _ExecParam.Clone();

                                Result result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                                        execParam_barcodeUpdate, barkod_pld2_Id, newpldId);

                                if (!string.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
                                {
                                    Logger.Error("Hiba: Barkod rekordhoz iratpéldány hozzákötése", _ExecParam);
                                    throw new ResultException(result_barcodeUpdate);
                                }
                            }
                            #endregion
                        }

                    }
                    else
                    {
                        // ha nem találtunk a csoporthoz partnert, akkor nem csinálunk új példányt
                        // TODO: Jó ez így, vagy dobjon esetleg hibát??
                    }
                }
                #endregion

                #region Plusz iratpéldány az iktatónak:

                if (Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas &&
                    ip.KeszitoPeldanyaSzukseges == true)
                {
                    KRT_Partnerek Iktato_Partner = null;
                    try
                    {
                        Iktato_Partner = GetKRT_PartnerekByCsoportId(FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam), _ExecParam);
                    }
                    catch (Exception e)
                    {
                        Logger.Error("Hiba új iratpéldány létrehozásakor (iktatónak szóló példánynál)", e);
                        // nem kritikus a hiba, nem kell emiatt rollback-elni
                    }

                    if (Iktato_Partner != null && !string.IsNullOrEmpty(Iktato_Partner.Id))
                    {
                        sorszam_iratPeldany++;
                        EREC_PldIratPeldanyok pluszIratPeldanyIktato = _EREC_PldIratPeldanyok[0];
                        pluszIratPeldanyIktato.Sorszam = sorszam_iratPeldany.ToString();
                        pluszIratPeldanyIktato.Updated.Sorszam = true;

                        // kezeloje az iktato
                        pluszIratPeldanyIktato.Csoport_Id_Felelos = FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
                        pluszIratPeldanyIktato.Updated.Csoport_Id_Felelos = true;

                        if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                        {
                            // Állapot állítása
                            pluszIratPeldanyIktato.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                            pluszIratPeldanyIktato.Updated.Allapot = true;

                            pluszIratPeldanyIktato.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString; // "";
                            pluszIratPeldanyIktato.Updated.TovabbitasAlattAllapot = true;
                        }
                        else
                        {
                            // Munkaanyagnál:
                            pluszIratPeldanyIktato.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban;
                            pluszIratPeldanyIktato.Updated.Allapot = true;

                            pluszIratPeldanyIktato.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString; // "";
                            pluszIratPeldanyIktato.Updated.TovabbitasAlattAllapot = true;
                        }

                        pluszIratPeldanyIktato.Eredet = KodTarak.IRAT_FAJTA.Masolat;
                        pluszIratPeldanyIktato.Updated.Eredet = true;

                        pluszIratPeldanyIktato.Partner_Id_Cimzett = Iktato_Partner.Id;
                        pluszIratPeldanyIktato.Updated.Partner_Id_Cimzett = true; ;

                        pluszIratPeldanyIktato.NevSTR_Cimzett = Iktato_Partner.Nev;
                        pluszIratPeldanyIktato.Updated.NevSTR_Cimzett = true;

                        // TODO: Mi legyen a címmel?
                        pluszIratPeldanyIktato.Cim_id_Cimzett = string.Empty;
                        pluszIratPeldanyIktato.Updated.Cim_id_Cimzett = true;

                        pluszIratPeldanyIktato.CimSTR_Cimzett = string.Empty;
                        pluszIratPeldanyIktato.Updated.CimSTR_Cimzett = true;

                        //BARCODE
                        pluszIratPeldanyIktato.BarCode = barkod_pld3;
                        pluszIratPeldanyIktato.Updated.BarCode = true;

                        /// Azonosító (Iktatószám) beállítása
                        /// 
                        // BLG_292
                        //_EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok);
                        pluszIratPeldanyIktato.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, pluszIratPeldanyIktato);

                        pluszIratPeldanyIktato.Updated.Azonosito = true;

                        ExecParam execParam_PluszPeldany2 = _ExecParam.Clone();

                        Result result_PluszPeldany2 =
                            erec_PldIratPeldanyokService.Insert(execParam_PluszPeldany2, pluszIratPeldanyIktato);
                        if (!string.IsNullOrEmpty(result_PluszPeldany2.ErrorCode))
                        {
                            Logger.Error("Iktatónak szóló iratpéldány létrehozásakor hiba", _ExecParam, result_PluszPeldany2);

                            // hiba
                            sorszam_iratPeldany--;

                            throw new ResultException(result_PluszPeldany2);
                        }
                        else
                        {
                            #region BARCODE UPDATE

                            string newpldId = result_PluszPeldany2.Uid;

                            if (!string.IsNullOrEmpty(pluszIratPeldanyIktato.BarCode))
                            {
                                KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                                ExecParam execParam_barcodeUpdate = _ExecParam.Clone();

                                Result result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                                        execParam_barcodeUpdate, barkod_pld3_Id, newpldId);

                                if (!string.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
                                {
                                    Logger.Error("Hiba: Barkod rekordhoz iratpéldány hozzákötése", _ExecParam);
                                    throw new ResultException(result_barcodeUpdate);
                                }
                            }
                            #endregion
                        }

                    }
                    else
                    {
                        // ha nem találtunk a csoporthoz partnert, akkor nem csinálunk új példányt
                        // TODO: Jó ez így, vagy dobjon esetleg hibát??
                    }
                }
                #endregion

                // Irat UPDATE (UtolsoSorszam módosítása, ha volt plusz iratpéldány)
                if (sorszam_iratPeldany > 1)
                {
                    // Az előbb hoztuk létre az iratot, így a verziójának 1-nek kell lennie
                    // (be kell állítani a verziót, hogy menjen az update (le is kérhetnénk újra a rekordot))
                    _EREC_IraIratok.Updated.SetValueAll(false);
                    _EREC_IraIratok.Base.Updated.SetValueAll(false);

                    //_EREC_IraIratok.Base.Ver = "1";
                    //_EREC_IraIratok.Base.Updated.Ver = true;

                    ExecParam execParam_iratUpdate = _ExecParam.Clone();
                    execParam_iratUpdate.Record_Id = _EREC_IraIratok.Id;

                    Result iratokGetVer = erec_IraIratokService.Get(execParam_iratUpdate);
                    if (iratokGetVer.IsError)
                    {
                        Logger.Error("Irat lekerese sikertelen", execParam_iratUpdate, iratokGetVer);
                        throw new ResultException(iratokGetVer);
                    }

                    _EREC_IraIratok.Base.Ver = (iratokGetVer.Record as EREC_IraIratok).Base.Ver;
                    _EREC_IraIratok.Base.Updated.Ver = true;

                    // Utolsó sorszám állítása:
                    _EREC_IraIratok.UtolsoSorszam = sorszam_iratPeldany.ToString();
                    _EREC_IraIratok.Updated.UtolsoSorszam = true;

                    Result result_IratUpdate = erec_IraIratokService.Update(execParam_iratUpdate, _EREC_IraIratok);
                    if (!string.IsNullOrEmpty(result_IratUpdate.ErrorCode))
                    {
                        Logger.Error("Irat utolsó sorszámának módosításakor hiba", _ExecParam, result_IratUpdate);

                        // hiba
                        throw new ResultException(result_IratUpdate);
                    }
                }

                // CR3306 Kimenő email esetén automatikus postazas javítás
                #region Kimenő email esetén Expediáljuk is  az Irat példányt.
                // TODO MUNKAPELDANY ellenörzés
                if (_EREC_IraIratok.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso
                    && Bejovo_vagy_BelsoIratIktatas == IktatasTipus.BelsoIratIktatas
                    && Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag
                    && _EREC_IraIratok.Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet)
                {

                    ExecParam execParam_IratPeldanyExpedialas = _ExecParam.Clone();
                    //string[] exp_Id_Array = { _EREC_PldIratPeldanyok.Id };

                    List<string> listIds = new List<string>();
                    foreach (EREC_PldIratPeldanyok item in _EREC_PldIratPeldanyok)
                    {
                        listIds.Add(item.Id);
                    }

                    string[] exp_Id_Array = listIds.ToArray(); //{ IratPldForExpedialas.Id };
                    // Result result_Expedialas = erec_PldIratPeldanyokService.Expedialas(execParam_IratPeldanyExpedialas, exp_Id_Array, string.Empty, _EREC_PldIratPeldanyok);
                    Result result_Expedialas = erec_PldIratPeldanyokService.Expedialas(execParam_IratPeldanyExpedialas, exp_Id_Array, string.Empty, IratPldForExpedialas, null);
                    if (result_Expedialas.IsError)
                        throw new ResultException(result_Expedialas);

                }
                #endregion
            }
            #region Regi Adat Szereles a vegere kell, mert kulon tranzakcioban fut
            if (kelleRegiAdtaSzereles)
            {
                #region EREC_UgyiratObjKapcsolatok insert
                Logger.Debug("EREC_UgyiratObjKapcsolatokService InsertMigraltSzerelt start");

                ExecParam xmpObjKapcsInsert = _ExecParam.Clone();
                EREC_UgyiratObjKapcsolatokService svcObjKapcs = new EREC_UgyiratObjKapcsolatokService(this.dataContext);

                svcObjKapcs.InsertMigraltSzerelt(xmpObjKapcsInsert, _EREC_UgyUgyiratok.Id, ip.RegiAdatId, ip.RegiAdatAzonosito);

                Logger.Debug("EREC_UgyiratObjKapcsolatokService InsertMigraltSzerelt end");
                #endregion

                // BLG_292
                //string ugyiratAzonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_EREC_UgyUgyiratok, erec_IraIktatoKonyvek);
                string ugyiratAzonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_ExecParam, _EREC_UgyUgyiratok, erec_IraIktatoKonyvek);

                EREC_UgyUgyiratokService.FoszamSzereles(_ExecParam, _EREC_UgyUgyiratok.RegirendszerIktatoszam, _EREC_UgyUgyiratok.Id, ugyiratAzonosito);

                #region Eseménynaplózás
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(_ExecParam, dataContext.Tranz_Id, _EREC_UgyUgyiratok.Id, "EREC_UgyUgyiratok", "UgyiratSzereles").Record;

                eventLogRecord.Azonositoja = ip.RegiAdatAzonosito;

                eventLogService.Insert(_ExecParam, eventLogRecord);
                #endregion
            }
            #endregion

            /// ha minden OK volt, akkor egy hibakód nélküli Result megy vissza, amibe beletesszük 
            /// a létrehozott rekordok id-jait //egyelőre csak az iratét és az ügyiratét
            Result returnResult = new Result();
            returnResult.Uid = _EREC_IraIratok.Id;
            #region Visszatérési információk
            ErkeztetesIktatasResult iktatasResult = new ErkeztetesIktatasResult();
            iktatasResult.UgyiratId = _EREC_UgyUgyiratok.Id;
            // BLG_292
            //string FullFoszam = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_EREC_UgyUgyiratok, erec_IraIktatoKonyvek);
            string FullFoszam = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(_ExecParam, _EREC_UgyUgyiratok, erec_IraIktatoKonyvek);

            iktatasResult.UgyiratAzonosito = FullFoszam;
            iktatasResult.UgyiratCsoportIdFelelos = _EREC_UgyUgyiratok.Csoport_Id_Felelos;
            iktatasResult.IratId = _EREC_IraIratok.Id;
            // BLG_292
            //string FullIktatoszam = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok);
            string FullIktatoszam = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok);

            iktatasResult.IratAzonosito = FullIktatoszam;
            //munkapéldány létrehozás volt-e?
            iktatasResult.MunkaPeldany = (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Munkaanyag);

            // Alszámra iktatás esetén, ha módosult az ügyirat határidő
            if (!string.IsNullOrEmpty(ip.UgyiratUjHatarido))
            {
                DateTime elozmenyDate;
                DateTime ugyiratDate;
                bool isElozmenyDateParsed = DateTime.TryParse(ip.UgyiratUjHatarido, out elozmenyDate);
                bool isUgyiratDateParsed = DateTime.TryParse(_EREC_UgyUgyiratok.Hatarido, out ugyiratDate);
                if (isElozmenyDateParsed && isUgyiratDateParsed && elozmenyDate == ugyiratDate)
                {
                    iktatasResult.UgyiratUjHatarido = ip.UgyiratUjHatarido;
                }
            }
            returnResult.Record = iktatasResult;
            #endregion

            Logger.Debug("Iktatás lefutása sikeres", _ExecParam);


            return returnResult;
        }
    }
    #endregion TOBB IRAT PLD

    Result AutomatikusElektronikusUzenetKikuldes(ExecParam execParam, string iratId)
    {
        Logger.Info("AutomatikusElektronikusUzenetKikuldes kezdete");
        Logger.Debug(String.Format("iratId: {0}", iratId));

        Result result = new Result();

        try
        {
            bool automatikusEUzenetKikuldes = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.AUTO_EUZENET_KIKULDES_KIADMANYOZAS_UTAN, false);
            Logger.Debug(String.Format("automatikusEUzenetKikuldes={0}", automatikusEUzenetKikuldes));
            if (automatikusEUzenetKikuldes)
            {
                DataContext newContext = new DataContext(this.Application);
                EREC_CsatolmanyokService csatolmanyokService = new EREC_CsatolmanyokService(newContext);
                EREC_CsatolmanyokSearch csatolmanyokSearch = new EREC_CsatolmanyokSearch();
                csatolmanyokSearch.IraIrat_Id.Value = iratId;
                csatolmanyokSearch.IraIrat_Id.Operator = Query.Operators.equals;

                newContext.OpenConnectionIfRequired();
                newContext.BeginTransactionIfRequired(IsolationLevel.ReadUncommitted);
                Result csatolmanyokResult = csatolmanyokService.GetAllWithExtensionInternal(execParam, csatolmanyokSearch, false);

                if (csatolmanyokResult.IsError)
                {
                    Logger.Error("EREC_CsatolmanyokService.GetAllWithExtension hiba", execParam, csatolmanyokResult);
                    throw new ResultException(csatolmanyokResult);
                }

                newContext.CommitIfRequired(true);
                newContext.CloseConnectionIfRequired(true);

                int csatolmanyokSzama = csatolmanyokResult.Ds.Tables[0].Rows.Count;
                Logger.Debug(String.Format("csatolmanyokSzama={0}", csatolmanyokSzama));

                int hitelesCsatolmanyokSzama = 0;
                string dokumentumId = String.Empty;

                if (csatolmanyokSzama > 1)
                {
                    foreach (DataRow row in csatolmanyokResult.Ds.Tables[0].Rows)
                    {
                        string elektronikusAlairas = row["ElektronikusAlairas"].ToString();

                        if (!String.IsNullOrEmpty(elektronikusAlairas) && elektronikusAlairas != "0")
                        {
                            dokumentumId = row["Dokumentum_Id"].ToString();
                            hitelesCsatolmanyokSzama++;
                        }
                    }

                    Logger.Debug(String.Format("hitelesCsatolmanyokSzama={0}", csatolmanyokSzama));
                    Logger.Debug(String.Format("dokumentumId={0}", dokumentumId));
                }



                if (csatolmanyokSzama == 1 || hitelesCsatolmanyokSzama == 1)
                {
                    EREC_PldIratPeldanyokService peldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
                    EREC_PldIratPeldanyokSearch peldanyokSearch = new EREC_PldIratPeldanyokSearch();
                    peldanyokSearch.IraIrat_Id.Value = iratId;
                    peldanyokSearch.IraIrat_Id.Operator = Query.Operators.equals;

                    Result peldanyokResult = peldanyokService.GetAll(execParam, peldanyokSearch);

                    if (peldanyokResult.IsError)
                    {
                        Logger.Error("EREC_PldIratPeldanyokService.GetAll hiba", execParam, peldanyokResult);
                        throw new ResultException(peldanyokResult);
                    }

                    foreach (DataRow row in peldanyokResult.Ds.Tables[0].Rows)
                    {
                        EREC_PldIratPeldanyok peldany = new EREC_PldIratPeldanyok();
                        Utility.LoadBusinessDocumentFromDataRow(peldany, row);

                        Logger.Debug(String.Format("pledanyId={0}", peldany.Id));
                        Logger.Debug(String.Format("kuldesMod={0}", peldany.KuldesMod));
                        Logger.Debug(String.Format("cimSTR_Cimzett={0}", peldany.CimSTR_Cimzett));

                        if (String.IsNullOrEmpty(peldany.CimSTR_Cimzett))
                        {
                            Logger.Warn(String.Format("A példány ({0}) címzettje üres!", peldany.Id));
                        }
                        else
                        {

                            if (peldany.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.Elhisz)
                            {
                                ElektronikusUzenetKikuldesToElhisz(execParam, peldany, dokumentumId);
                            }

                            if (peldany.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu)
                            {
                                ElektronikusUzenetKikuldesToHivataliKapu(execParam, peldany, dokumentumId);
                            }
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error("AutomatikusElektronikusUzenetKikuldes hiba", execParam, result);
        }

        Logger.Info("AutomatikusElektronikusUzenetKikuldes vege");
        return result;
    }

    void ElektronikusUzenetKikuldesToElhisz(ExecParam execParam, EREC_PldIratPeldanyok peldany, string dokumentumId)
    {
        Logger.Info("ElektronikusUzenetKikuldesToElhisz kezdete");
        Logger.Debug(String.Format("peldanyId={0}", peldany.Id));
        Logger.Debug(String.Format("dokumentumId={0}", dokumentumId));

        ExecParam execParamOrzo = execParam.Clone();
        execParamOrzo.Felhasznalo_Id = peldany.FelhasznaloCsoport_Id_Orzo;

        string[] dokumentum_Id_Array = null;

        if (!String.IsNullOrEmpty(dokumentumId))
        {
            dokumentum_Id_Array = new string[] { dokumentumId };
        }

        EREC_PldIratPeldanyokService peldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
        Result peldanyokResult = peldanyokService.Expedialas(execParamOrzo, new string[] { peldany.Id }, String.Empty, peldany, dokumentum_Id_Array);

        if (peldanyokResult.IsError)
        {
            Logger.Error("EREC_PldIratPeldanyokService.Expedialas hiba", execParamOrzo, peldanyokResult);
            throw new ResultException(peldanyokResult);
        }


        string kuldemenyId = peldanyokResult.Uid;

        Logger.Debug(String.Format("kuldemenyId={0}", kuldemenyId));

        string elhiszUserId = Rendszerparameterek.Get(execParam, "ElhiszUserId");

        Logger.Debug(String.Format("elhiszUserId={0}", elhiszUserId));

        //átadás az elhisz technikai felhasználónak
        if (!String.IsNullOrEmpty(elhiszUserId))
        {
            EREC_KuldKuldemenyekService kuldemenyekService = new EREC_KuldKuldemenyekService(this.dataContext);

            // BUG_11881
            Result kuldemenyekResult;
            if ((peldanyokResult.Ds.Tables != null) && (peldanyokResult.Ds.Tables[0].Rows.Count > 0))
            {
                kuldemenyekResult = kuldemenyekService.Atadas_Tomeges(execParamOrzo, peldanyokResult.Ds.Tables[0].Rows.OfType<DataRow>().Select(k => k[0].ToString()).ToArray(), elhiszUserId, null);
            }
            else
                kuldemenyekResult = kuldemenyekService.Atadas_Tomeges(execParamOrzo, new string[] { kuldemenyId }, elhiszUserId, null);

            if (kuldemenyekResult.IsError)
            {
                Logger.Error("EREC_KuldKuldemenyekService.Atadas_Tomeges hiba", execParamOrzo, kuldemenyekResult);
                throw new ResultException(peldanyokResult);
            }
        }

        Logger.Info("ElektronikusUzenetKikuldesToElhisz vege");
    }

    void ElektronikusUzenetKikuldesToHivataliKapu(ExecParam execParam, EREC_PldIratPeldanyok peldany, string dokumentumId)
    {
        Logger.Info("ElektronikusUzenetKikuldesToHivataliKapu kezdete");
        Logger.Debug(String.Format("peldanyId={0}", peldany.Id));
        Logger.Debug(String.Format("dokumentumId={0}", dokumentumId));

        ExecParam execParamOrzo = execParam.Clone();
        execParamOrzo.Felhasznalo_Id = peldany.FelhasznaloCsoport_Id_Orzo;

        string[] dokumentum_Id_Array = null;

        if (!String.IsNullOrEmpty(dokumentumId))
        {
            dokumentum_Id_Array = new string[] { dokumentumId };
        }

        EREC_PldIratPeldanyokService peldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
        EREC_eBeadvanyok _EREC_eBeadvanyok = new EREC_eBeadvanyok();

        _EREC_eBeadvanyok.PartnerNev = peldany.NevSTR_Cimzett;
        if (peldany.CimSTR_Cimzett.Length == 32)
        {
            _EREC_eBeadvanyok.PartnerKapcsolatiKod = peldany.CimSTR_Cimzett;
        }
        else
        {
            _EREC_eBeadvanyok.PartnerKRID = peldany.CimSTR_Cimzett;
        }

        Result peldanyokResult = peldanyokService.Expedialas_HivataliKapus(execParamOrzo, new string[] { peldany.Id }, String.Empty, peldany, _EREC_eBeadvanyok, dokumentum_Id_Array, null);

        if (peldanyokResult.IsError)
        {
            Logger.Error("EREC_PldIratPeldanyokService.Expedialas_HivataliKapus hiba", execParamOrzo, peldanyokResult);
            throw new ResultException(peldanyokResult);
        }

        Logger.Info("ElektronikusUzenetKikuldesToHivataliKapu vege");
    }

    [WebMethod()]
    public Result FizetesiInformacio(ExecParam execParam, string iktatoSzam, string tranzakcioAzonosito, string targyszavakJSON)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
            iratokSearch.Azonosito.Value = iktatoSzam;
            iratokSearch.Azonosito.Operator = Query.Operators.equals;

            Result iratokResult = this.GetAll(execParam, iratokSearch);

            if (iratokResult.IsError)
            {
                throw new ResultException(iratokResult);
            }

            if (iratokResult.Ds.Tables[0].Rows.Count == 0)
            {
                throw new Exception("Az irat nem található!");
            }

            string iratId = iratokResult.Ds.Tables[0].Rows[0]["Id"].ToString();

            Dictionary<string, string> targyszavakDict = new Dictionary<string, string>();
            try
            {
                if (!String.IsNullOrEmpty(targyszavakJSON))
                {
                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    targyszavakDict = serializer.Deserialize<Dictionary<string, string>>(targyszavakJSON);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Hiba a JSON feldolgozása során: ", ex.Message));
            }

            if (!targyszavakDict.ContainsKey("fizetes_tr_azon"))
            {
                targyszavakDict.Add("fizetes_tr_azon", tranzakcioAzonosito);
            }

            if (targyszavakDict.Count > 0)
            {
                this.SetObjektumTargyszavak(execParam, iratId, Contentum.eUtility.Constants.TableNames.EREC_IraIratok, targyszavakDict);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result FizetesiInformacioKivonatFeldolgozas(ExecParam execParam, string kivonatAzonosito, DateTime kivonatDatum)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            string ws = UI.GetAppSetting("FizetesiInformacio_KivonatWS");

            if (string.IsNullOrEmpty(ws))
            {
                throw new Exception("Nem található webservice URL");
            }

            WebRequest wr = WebRequest.Create(ws + kivonatAzonosito);
            wr.Credentials = CredentialCache.DefaultCredentials;
            StreamReader reader = new StreamReader(wr.GetResponse().GetResponseStream());
            string azonositok = reader.ReadToEnd();
            reader.Close();

            if (string.IsNullOrEmpty(azonositok))
            {
                return result;
            }

            Dictionary<string, string> targyszavakDict = new Dictionary<string, string>();
            targyszavakDict.Add("fizetes_statusz", "Fizetett - beérkezett");
            targyszavakDict.Add("fizetes_beerkezes_datum", kivonatDatum.ToString());

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string targyszavakJSON = serializer.Serialize(targyszavakDict);


            string[] ids = azonositok.Split(',');

            foreach (string id in ids)
            {
                FizetesiInformacio(execParam, kivonatAzonosito, id, targyszavakJSON);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    private void SetObjektumTargyszavak(ExecParam execParam, string objId, string objTableName, Dictionary<string, string> targyszoErtekek)
    {
        #region Objektum tárgyszavak lekérése

        var serviceObjTargyszavai = new EREC_ObjektumTargyszavaiService(this.dataContext);

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

        Result resultGetAll = serviceObjTargyszavai.GetAllMetaByObjMetaDefinicio(execParam, search, objId, null
            , objTableName, null, null, false
            , null, false, true);


        if (resultGetAll.IsError)
        {
            throw new ResultException(resultGetAll);
        }

        Dictionary<string, DataRow> objTargyszavakDict = new Dictionary<string, DataRow>();

        foreach (DataRow row in resultGetAll.Ds.Tables[0].Rows)
        {
            string belsoAzonosito = row["BelsoAzonosito"].ToString();

            // Paraméterben jött-e ilyen tárgyszó:
            var keresettTargyszo = targyszoErtekek.FirstOrDefault(e => e.Key == belsoAzonosito);

            objTargyszavakDict[belsoAzonosito] = row;
        }

        #endregion

        #region EREC_ObjektumTargyszavai objektumok feltöltése

        List<EREC_ObjektumTargyszavai> modositandoObjTargyszavak = new List<EREC_ObjektumTargyszavai>();

        foreach (KeyValuePair<string, string> modositandoTargyszo in targyszoErtekek)
        {
            // Ha a lekérdezés nem adott vissza ilyen tárgyszót, akkor hiba:
            if (!objTargyszavakDict.ContainsKey(modositandoTargyszo.Key))
            {
                throw new ResultException("Hiányzó tárgyszó: " + modositandoTargyszo.Key);
            }

            DataRow rowObjTargyszo = objTargyszavakDict[modositandoTargyszo.Key];

            // Tárgyszavas objektum feltöltése:
            EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
            erec_ObjektumTargyszavai.Updated.SetValueAll(false);
            erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

            erec_ObjektumTargyszavai.Id = rowObjTargyszo["Id"].ToString();
            erec_ObjektumTargyszavai.Updated.Id = true;

            erec_ObjektumTargyszavai.Obj_Metaadatai_Id = rowObjTargyszo["Obj_Metaadatai_Id"].ToString();
            erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

            erec_ObjektumTargyszavai.Targyszo_Id = rowObjTargyszo["Targyszo_Id"].ToString();
            erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

            erec_ObjektumTargyszavai.Targyszo = rowObjTargyszo["Targyszo"].ToString();
            erec_ObjektumTargyszavai.Updated.Targyszo = true;

            erec_ObjektumTargyszavai.Ertek = modositandoTargyszo.Value;
            erec_ObjektumTargyszavai.Updated.Ertek = true;

            modositandoObjTargyszavak.Add(erec_ObjektumTargyszavai);
        }

        #endregion

        #region Objektum tárgyszó insert/update

        // Insert/update webservice hívása:
        var resultInsertUpdate = serviceObjTargyszavai.InsertOrUpdateValuesByObjMetaDefinicio(
                        execParam
                        , modositandoObjTargyszavak.ToArray()
                        , objId.ToString()
                        , null
                        , objTableName, "*", null, false, null, false, false);

        if (resultInsertUpdate.IsError)
        {
            throw new ResultException(resultInsertUpdate);
        }

        #endregion
    }

    /// <summary>
    /// Irat AdathordozoTipusa mezőjének beállítása az iratpéldányok UgyintezesModja alapján (BLG#3451)
    /// Ha az irat iratpéldányainak UgyintezesModja mezője nem homogén, akkor az irat AdathordozoTipusa mező értéke vegyes lesz (UGYINTEZES_ALAPJA.Vegyes),
    /// egyébként az lesz, ami az iratpéldányokban lévő érték.
    /// </summary>
    /// <param name="iratId"></param>
    public void RefreshAdathordozoTipusaByIratPld(ExecParam execParam, string iratId)
    {
        /// Feladat:
        /// - Irat lekérése
        /// - Iratpéldányok lekérése (sztornózottakat nem figyeljük)
        /// - Iratpéldányok UgyintezesModja alapján megállapítani az irat AdathordozoTipusa mező értékét
        /// - Irat Update
        /// 

        #region Irat lekérése

        var execParamIratGet = execParam.Clone();
        execParamIratGet.Record_Id = iratId;
        var resultIratGet = this.Get(execParamIratGet);
        if (resultIratGet.IsError)
        {
            throw new ResultException(resultIratGet);
        }

        EREC_IraIratok irat = (EREC_IraIratok)resultIratGet.Record;

        #endregion

        #region Iratpéldányok lekérése

        EREC_PldIratPeldanyokService pldService = new EREC_PldIratPeldanyokService(this.dataContext);
        EREC_PldIratPeldanyokSearch pldSearch = new EREC_PldIratPeldanyokSearch();
        pldSearch.IraIrat_Id.Value = iratId.ToString();
        pldSearch.IraIrat_Id.Operator = Query.Operators.equals;

        // Sztornózottakat nem figyeljük:
        pldSearch.Allapot.Value = KodTarak.IRATPELDANY_ALLAPOT.Sztornozott;
        pldSearch.Allapot.Operator = Query.Operators.notequals;

        var resultPldGetAll = pldService.GetAll(execParam.Clone(), pldSearch);
        if (resultPldGetAll.IsError)
        {
            throw new ResultException(resultPldGetAll);
        }

        #endregion

        #region Irat AdathordozoTipusa mező értékének megállapítása

        // (EREC_IraIratok.AdathordozoTipusa és az EREC_PldIratPeldanyok.UgyintezesModja mezők értékei ugyanúgy az UGYINTEZES_ALAPJA kódcsoport elemeit vehetik fel)
        string adathordozoTipusaUjErtek = String.Empty;
        foreach (DataRow pldRow in resultPldGetAll.Ds.Tables[0].Rows)
        {
            string ugyintezesModjaPld = pldRow["UgyintezesModja"].ToString();
            if (String.IsNullOrEmpty(adathordozoTipusaUjErtek))
            {
                adathordozoTipusaUjErtek = ugyintezesModjaPld;
            }
            else if (adathordozoTipusaUjErtek != ugyintezesModjaPld && !String.IsNullOrEmpty(ugyintezesModjaPld))
            {
                // Eltérő érték jött --> Vegyes lesz az érték
                adathordozoTipusaUjErtek = KodTarak.UGYINTEZES_ALAPJA.Vegyes;
                break;
            }
        }

        #endregion

        #region Irat Update (ha kell)

        // Ha az új érték eltér a régitől, akkor UPDATE:
        if (adathordozoTipusaUjErtek != irat.AdathordozoTipusa && !String.IsNullOrEmpty(adathordozoTipusaUjErtek))
        {
            // Irat UPDATE:
            irat.Updated.SetValueAll(false);
            irat.Base.Updated.SetValueAll(false);
            irat.Base.Updated.Ver = true;

            irat.AdathordozoTipusa = adathordozoTipusaUjErtek;
            irat.Updated.AdathordozoTipusa = true;

            ExecParam execParam_iratUpdate = execParam.Clone();
            execParam_iratUpdate.Record_Id = irat.Id;

            Result result_iratUpdate = this.Update(execParam_iratUpdate, irat);
            if (result_iratUpdate.IsError)
            {
                // hiba:
                throw new ResultException(result_iratUpdate);
            }
        }

        #endregion
    }

    [WebMethod()]
    public Result TomegesIktatas_INIT(ExecParam execParam, Guid iktatokonyv_Id, Guid? erkeztetokonyv_Id, int? foszam, int iratDarabszam, int iratIrany, bool kuldemenyAzonositott)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            TomegesIktatasManager manager = new TomegesIktatasManager(this.dataContext);
            result = manager.TomegesIktatas_INIT(execParam, iktatokonyv_Id, erkeztetokonyv_Id, foszam, iratDarabszam, iratIrany, kuldemenyAzonositott);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result TomegesIktatas_EXEC(ExecParam execParam, Guid folyamatId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            TomegesIktatasManager manager = new TomegesIktatasManager(this.dataContext);
            result = manager.TomegesIktatas_EXEC(execParam, folyamatId);

            if (result.IsError)
            {
                throw new ResultException(result);
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result TomegesIktatas_EXEC_Job()
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309"; //Admin

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            TomegesIktatasManager manager = new TomegesIktatasManager(this.dataContext);
            result = manager.TomegesIktatas_EXEC_Job(execParam);

            if (result.IsError)
            {
                throw new ResultException(result);
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetTomegesOlvasasiJog(ExecParam execParam, string[] iratIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetTomegesOlvasasiJog(execParam, iratIds);

            if (result.IsError)
            {
                throw new ResultException(result);
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #region SztornoKiadmanyozottIratIds

    [WebMethod()]
    public string SztornoKiadmanyozottIratIds(string iratAzonositokString)
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54e861a5-36ed-44ca-baa7-c287d125b309";
        execParam.LoginUser_Id = "54e861a5-36ed-44ca-baa7-c287d125b309";

        StringBuilder errLog = new StringBuilder();

        List<string> iratAzonositok = new List<string>();
        iratAzonositok.AddRange(iratAzonositokString.Split(','));

        Contentum.eRecord.Service.EREC_IraIratokService iratService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

        foreach (string azonosito in iratAzonositok)
        {
            errLog.AppendLine("SZORNÓZÁS START: IRAT AZONOSITO: " + azonosito + Environment.NewLine);

            ExecParam iratExecParam = execParam.Clone();
            EREC_IraIratokSearch iratSearch = new EREC_IraIratokSearch();
            iratSearch.Azonosito.Value = azonosito;
            iratSearch.Azonosito.Operator = Contentum.eQuery.Query.Operators.equals;
            Result iratResult = iratService.GetAll(iratExecParam, iratSearch);

            if (iratResult.IsError)
            {
                errLog.AppendLine("Hiba az irat lekérésekor. Hiba: " + iratResult.ErrorMessage + Environment.NewLine);
                errLog.AppendLine("SZORNÓZÁS END: IRAT AZONOSITO: " + azonosito + Environment.NewLine);
                continue;
            }

            if (iratResult.Ds.Tables[0].Rows.Count < 1)
            {
                errLog.AppendLine("Nem található azh irat. Azonosito: " + azonosito + Environment.NewLine);
                errLog.AppendLine("SZORNÓZÁS END: IRAT AZONOSITO: " + azonosito + Environment.NewLine);
                continue;
            }

            string irid = iratResult.Ds.Tables[0].Rows[0]["Id"].ToString();
            string Ugyirat_Id = iratResult.Ds.Tables[0].Rows[0]["Ugyirat_Id"].ToString();

            string irany = "";

            Contentum.eRecord.Service.EREC_PldIratPeldanyokService iratPeldanyService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            EREC_PldIratPeldanyokSearch ss = new EREC_PldIratPeldanyokSearch(true);
            ss.IraIrat_Id.Value = irid;
            ss.IraIrat_Id.Operator = Contentum.eQuery.Query.Operators.equals; ;

            //ss.Extended_EREC_IraIratokSearch.Azonosito.Value = azonosito;
            //ss.Extended_EREC_IraIratokSearch.Azonosito.Operator = Contentum.eQuery.Query.Operators.equals;


            Result iratPeldanyokResult = iratPeldanyService.GetAll(execParam.Clone(), ss);

            if (iratPeldanyokResult.IsError)
            {
                errLog.AppendLine("Hiba az iratpéldányok lekérésekor. Hiba: " + iratPeldanyokResult.ErrorMessage + Environment.NewLine);
                errLog.AppendLine("SZORNÓZÁS END: IRAT AZONOSITO: " + azonosito + Environment.NewLine);
                continue;
            }

            List<string> iratPeldanyok = new List<string>();
            List<string> kuldemenyek = new List<string>();

            Contentum.eRecord.Service.EREC_Kuldemeny_IratPeldanyaiService service_kuldIratPeldanyai = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService();

            if (iratPeldanyokResult.Ds.Tables[0].Rows.Count > 0)
            {
                iratPeldanyok.AddRange(iratPeldanyokResult.Ds.Tables[0].AsEnumerable().OrderByDescending(x => int.Parse(x["Sorszam"].ToString())).Select(x => x["Id"].ToString()).Distinct().ToArray());

                EREC_Kuldemeny_IratPeldanyaiSearch search = new EREC_Kuldemeny_IratPeldanyaiSearch(true);
                search.Peldany_Id.Value = Search.GetSqlInnerString(iratPeldanyok.ToArray());
                search.Peldany_Id.Operator = Contentum.eQuery.Query.Operators.inner;

                Result result_kuldIratPeldanyaiGetAll = service_kuldIratPeldanyai.GetAll(execParam, search);

                if (result_kuldIratPeldanyaiGetAll.IsError)
                {
                    errLog.AppendLine("Hiba a küldemény iratpéldányainak lekérésekor. Hiba: " + result_kuldIratPeldanyaiGetAll.ErrorMessage + Environment.NewLine);
                    errLog.AppendLine("SZORNÓZÁS END: IRAT AZONOSITO: " + azonosito + Environment.NewLine);
                    continue;
                }

                if (result_kuldIratPeldanyaiGetAll.Ds.Tables[0].Rows.Count > 0)
                {
                    string kuldId;
                    foreach (DataRow row in result_kuldIratPeldanyaiGetAll.Ds.Tables[0].Rows)
                    {
                        kuldId = row["KuldKuldemeny_Id"].ToString();

                        if (!kuldemenyek.Contains(kuldId))
                        {
                            kuldemenyek.Add(kuldId);
                        }
                    }
                }
                else
                {
                    errLog.AppendLine("Nem található küldemény iratpéldány" + Environment.NewLine);
                }
            }
            else
            {
                errLog.AppendLine("Nem található iratpéldány." + Environment.NewLine);
            }

            if (iratResult.Ds.Tables[0].Rows[0]["PostazasIranya"] != DBNull.Value)
            {
                irany = iratResult.Ds.Tables[0].Rows[0]["PostazasIranya"].ToString();
            }

            Contentum.eRecord.Service.EREC_KuldKuldemenyekService kuldService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            string sztornozasIndoka = "4532 SD ticket alapján sztornózás";

            if (irany == "1")
            {
                Contentum.eRecord.Service.EREC_UgyUgyiratokService ugyIratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam ugyiratExecparam = execParam.Clone();
                ugyiratExecparam.Record_Id = Ugyirat_Id;
                Result ugyiratResult = ugyIratService.Get(ugyiratExecparam);
                EREC_UgyUgyiratok ugyirat = null;
                if (ugyiratResult.IsError)
                {
                    errLog.AppendLine("Hiba az ügyirat lekérése során. Irat azonosító: " + azonosito + " Hibaüzenet: " + ugyiratResult.ErrorMessage + " Hibakód: " + ugyiratResult.ErrorCode + Environment.NewLine);
                    continue;
                }
                else
                {
                    ugyirat = (EREC_UgyUgyiratok)ugyiratResult.Record;
                }

                if (ugyirat != null)
                {
                    ExecParam felszabaditasExecParam = execParam.Clone();
                    felszabaditasExecParam.Felhasznalo_Id = ugyirat.FelhasznaloCsoport_Id_Orzo;
                    Result felszabaditasResult = iratService.Felszabaditas(felszabaditasExecParam, irid, true);

                    if (felszabaditasResult.IsError)
                    {
                        errLog.AppendLine("Nem sikerült felszabadítani az iratot. Irat azonosító: " + azonosito + " Hibaüzenet: " + felszabaditasResult.ErrorMessage + " Hibakód: " + felszabaditasResult.ErrorCode + Environment.NewLine);
                    }
                    else
                    {
                        errLog.AppendLine("Sikeres irat felszabasítás. Azonosító: " + azonosito + Environment.NewLine);
                    }
                }

                string munkaAllomas = iratResult.Ds.Tables[0].Rows[0]["Munkaallomas"] == DBNull.Value ? null : iratResult.Ds.Tables[0].Rows[0]["Munkaallomas"].ToString();

                if (munkaAllomas == "ASP.ADO")
                {

                    if (iratResult.Ds.Tables[0].Rows[0]["KuldKuldemenyek_Id"] != DBNull.Value)
                    {
                        kuldemenyek.Add(iratResult.Ds.Tables[0].Rows[0]["KuldKuldemenyek_Id"].ToString());
                    }

                    if (kuldemenyek.Count > 0)
                    {
                        foreach (string id in kuldemenyek.Distinct().ToList())
                        {
                            ExecParam kuldExecParam = execParam.Clone();
                            kuldExecParam.Record_Id = id;

                            EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)kuldService.Get(kuldExecParam).Record;
                            kuldExecParam.Felhasznalo_Id = kuldemeny.FelhasznaloCsoport_Id_Orzo;

                            errLog.AppendLine("Start küldemény sztornó. Küldemény Id: " + id + Environment.NewLine);

                            Result kuldRes = kuldService.Sztorno(kuldExecParam, sztornozasIndoka);

                            if (kuldRes.IsError)
                            {
                                errLog.AppendLine("Nem sikerült sztornózni a küldeményt. Irat azonosító: " + azonosito + " Küldemény Id: " + id + " Hibaüzenet: " + kuldRes.ErrorMessage + " Hibakód: " + kuldRes.ErrorCode + Environment.NewLine);
                            }
                            else
                            {
                                errLog.AppendLine("Sikeres küldemény sztornó. Küldemény Id: " + id + Environment.NewLine);
                            }
                        }
                    }
                    iratExecParam.Record_Id = iratResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                    Result iratGetResult = iratService.Get(iratExecParam);

                    if (!iratGetResult.IsError && iratGetResult.Record != null)
                    {
                        EREC_IraIratok irat = (EREC_IraIratok)iratGetResult.Record;
                        if (irat.Allapot == KodTarak.IRAT_ALLAPOT.Felszabaditva || irat.Allapot == KodTarak.IRAT_ALLAPOT.Foglalt)
                        {
                            Result result_iratUpdate = this.Sztornozas(iratExecParam, irat, Ugyirat_Id);
                        }
                    }

                }
            }
            else
            {
                if (kuldemenyek.Count > 0)
                {
                    foreach (string id in kuldemenyek)
                    {
                        ExecParam kuldExecParam = execParam.Clone();
                        kuldExecParam.Record_Id = id;

                        EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)kuldService.Get(kuldExecParam).Record;
                        //kuldExecParam.Felhasznalo_Id = kuldemeny.FelhasznaloCsoport_Id_Orzo;

                        errLog.AppendLine("Start küldemény sztornó. Küldemény Id: " + id + Environment.NewLine);

                        Result kuldRes = kuldService.KimenoKuldemenySztorno(kuldExecParam);

                        if (kuldRes.IsError)
                        {
                            errLog.AppendLine("Nem sikerült sztornózni a küldeményt. Irat azonosító: " + azonosito + " Küldemény Id: " + id + " Hibaüzenet: " + kuldRes.ErrorMessage + " Hibakód: " + kuldRes.ErrorCode + Environment.NewLine);
                        }
                        else
                        {
                            errLog.AppendLine("Sikeres küldemény sztornó. Küldemény Id: " + id + Environment.NewLine);
                        }
                    }
                }

                ExecParam alairasExecParam = execParam.Clone();
                Contentum.eRecord.Service.EREC_IratAlairokService svcAlairok = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                EREC_IratAlairokSearch alairokSearch = new EREC_IratAlairokSearch();
                alairokSearch.Obj_Id.Value = irid;
                alairokSearch.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                alairokSearch.AlairoSzerep.Value = "1";
                alairokSearch.AlairoSzerep.Operator = Contentum.eQuery.Query.Operators.equals;

                Result alairokResult = svcAlairok.GetAll(alairasExecParam, alairokSearch);

                if (alairokResult.IsError)
                {
                    errLog.AppendLine("Hiba az irat aláírók lekérésekor. Hiba: " + alairokResult.ErrorMessage + Environment.NewLine);
                    errLog.AppendLine("SZORNÓZÁS END: IRAT AZONOSITO: " + azonosito + Environment.NewLine);
                    continue;
                }

                if (alairokResult.Ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < alairokResult.Ds.Tables[0].Rows.Count; i++)
                    {
                        alairasExecParam.Record_Id = alairokResult.Ds.Tables[0].Rows[i]["Id"].ToString();
                        string f = null;
                        if (alairokResult.Ds.Tables[0].Rows[0]["FelhasznaloCsoport_Id_Alairo"] != DBNull.Value)
                        {
                            f = alairokResult.Ds.Tables[0].Rows[0]["FelhasznaloCsoport_Id_Alairo"].ToString();
                        }
                        else if (alairokResult.Ds.Tables[0].Rows[0]["FelhaszCsop_Id_HelyettesAlairo"] != DBNull.Value)
                        {
                            f = alairokResult.Ds.Tables[0].Rows[0]["FelhaszCsop_Id_HelyettesAlairo"].ToString();
                        }

                        if (!string.IsNullOrEmpty(f))
                        {
                            alairasExecParam.Felhasznalo_Id = f;
                        }
                        Result result = svcAlairok.AlairasVisszavonas(alairasExecParam, irid, "");
                        if (result.IsError)
                        {
                            errLog.AppendLine("Hiba az AlairasVisszavonas alatt. Hiba: " + result.ErrorMessage + Environment.NewLine);
                            errLog.AppendLine("SZORNÓZÁS END: IRAT AZONOSITO: " + azonosito + Environment.NewLine);
                            continue;
                        }
                    }
                }
                else
                {
                    errLog.AppendLine("Nem talÁlható irat aláíró." + Environment.NewLine);
                }



                foreach (string pId in iratPeldanyok)
                {
                    ExecParam iratPeldanyExecParam = execParam.Clone();

                    iratPeldanyExecParam.Record_Id = pId;
                    EREC_PldIratPeldanyok iratPeldany = (EREC_PldIratPeldanyok)iratPeldanyService.Get(iratPeldanyExecParam).Record;
                    iratPeldanyExecParam.Felhasznalo_Id = iratPeldany.FelhasznaloCsoport_Id_Orzo;

                    Result result = iratPeldanyService.Sztornozas(iratPeldanyExecParam, pId, String.Empty, false, String.Empty);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        errLog.AppendLine("Hiba az iratpéldány Sztornozas alatt. Példány Id: " + pId + " Hiba. Nem megfelő az állapota vagy nem ön az örzője: " + result.ErrorMessage + Environment.NewLine + "Hibakód: " + result.ErrorCode + Environment.NewLine);
                    }
                }

                iratExecParam.Record_Id = iratResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                Result iratGetResult = iratService.Get(iratExecParam);

                if (!iratGetResult.IsError && iratGetResult.Record != null)
                {
                    EREC_IraIratok irat = (EREC_IraIratok)iratGetResult.Record;
                    if (irat.Allapot == KodTarak.IRAT_ALLAPOT.Felszabaditva || irat.Allapot == KodTarak.IRAT_ALLAPOT.Foglalt)
                    {
                        Result result_iratUpdate = this.Sztornozas(iratExecParam, irat, Ugyirat_Id);
                    }
                }

                Contentum.eRecord.Service.EREC_UgyUgyiratokService ugyService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam ugyExecParam = execParam.Clone();
                ugyExecParam.Record_Id = Ugyirat_Id;
                EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)ugyService.Get(ugyExecParam).Record;

                if (ugyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Sztornozott)
                {
                    ugyExecParam.Felhasznalo_Id = ugyirat.FelhasznaloCsoport_Id_Orzo;
                    Result ugysztornoResult = ugyService.Sztornozas(ugyExecParam, Ugyirat_Id, "4532 SD ticket alapján sztornózás");
                    if (!String.IsNullOrEmpty(ugysztornoResult.ErrorCode))
                    {
                        errLog.AppendLine("Hiba az ügyirat sztornozas alatt. Ügyirat Id: " + Ugyirat_Id + " Hiba: " + ugysztornoResult.ErrorMessage + Environment.NewLine + "Hibakód: " + ugysztornoResult.ErrorCode + Environment.NewLine);
                    }
                }

                errLog.AppendLine("SZORNÓZÁS END: IRAT AZONOSITO: " + azonosito + Environment.NewLine);
            }
        }
        return errLog.ToString();
    }

    #endregion

    #region SetTemplateToIrat

    private HatosagiStatisztikaTargyszavakTemplate LoadHatosagiStatisztikaTargyszavakTemplate(ExecParam execParam, string templateId)
    {
        HatosagiStatisztikaTargyszavakTemplate template = null;
        execParam.Record_Id = templateId;
        Type searchObjectType = typeof(HatosagiStatisztikaTargyszavakTemplate);
        Contentum.eAdmin.Service.KRT_UIMezoObjektumErtekekService _KRT_UIMezoObjektumErtekekService = eAdminService.ServiceFactory.GetKRT_UIMezoObjektumErtekekService();
        Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek _KRT_UIMezoObjektumErtekek = null;
        Contentum.eBusinessDocuments.Result result = new Contentum.eBusinessDocuments.Result();
        result = _KRT_UIMezoObjektumErtekekService.Get(execParam);
        if (result.Record != null)
        {
            _KRT_UIMezoObjektumErtekek = (Contentum.eBusinessDocuments.KRT_UIMezoObjektumErtekek)result.Record;
        }
        else if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error("Template nem található");
            return template;
        }
        if (_KRT_UIMezoObjektumErtekek != null)
        {
            template = (HatosagiStatisztikaTargyszavakTemplate)XmlFunction.XmlToObject(_KRT_UIMezoObjektumErtekek.TemplateXML, searchObjectType);
        }
        return template;
    }

    private Dictionary<string, string> LoadTargySzavakFromTemplate(HatosagiStatisztikaTargyszavakTemplate targyszavakTemplate)
    {
        Dictionary<string, string> targyszok = new Dictionary<string, string>();

        if (targyszavakTemplate == null)
        {
            return null;
        }

        var props = targyszavakTemplate.GetProperties();
        foreach (var prop in props)
        {
            var attrs = prop.GetCustomAttributes(typeof(TargyszavakIdAttribute), false);
            if (attrs != null && attrs.Length == 1)
            {
                var id = (attrs[0] as TargyszavakIdAttribute).Id;
                if (!String.IsNullOrEmpty(id))
                {
                    object val = prop.GetValue(targyszavakTemplate, null);
                    targyszok.Add(prop.Name, val == null ? null : val as string);
                }
            }
        }

        return targyszok;
    }

    private bool HasObjektumTargyszavai(EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch
  , string Obj_Id, string ObjMetaDefinicio_Id
  , string TableName, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude
  , string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, ExecParam execParam)
    {

        Result result = GetAllMetaByObjMetaDefinicio(Obj_Id, ObjMetaDefinicio_Id, TableName, ColumnName, ColumnValues, bColumnValuesExclude, DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus, execParam);

        if (result.IsError)
            return false;

        if (result.Ds.Tables[0].Rows.Count < 1)
            return false;

        return true;
    }

    private DataSet GetObjektumTargyszavai(EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch
      , String Obj_Id, String ObjMetaDefinicio_Id
      , String TableName, String ColumnName, String[] ColumnValues, bool bColumnValuesExclude
      , String DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, ExecParam execParam)
    {
        Result result = GetAllMetaByObjMetaDefinicio(Obj_Id, ObjMetaDefinicio_Id, TableName, ColumnName, ColumnValues, bColumnValuesExclude, DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus, execParam);

        if (result.IsError)
        {
            return null;
        }

        return result.Ds;
    }

    private Result GetAllMetaByObjMetaDefinicio(string Obj_Id, string ObjMetaDefinicio_Id, string TableName, string ColumnName, string[] ColumnValues, bool bColumnValuesExclude, string DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus, ExecParam execParam)
    {
        Contentum.eRecord.Service.EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

        Result result = service_ot.GetAllMetaByObjMetaDefinicio(execParam, search, Obj_Id, ObjMetaDefinicio_Id
            , TableName, ColumnName, ColumnValues, bColumnValuesExclude
            , DefinicioTipus, bCsakSajatSzint, bCsakAutomatikus);
        return result;
    }

    [WebMethod()]
    public string SetTemplateToIrat(string iratId, string templateId)
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54e861a5-36ed-44ca-baa7-c287d125b309";
        execParam.LoginUser_Id = "54e861a5-36ed-44ca-baa7-c287d125b309";

        StringBuilder errLog = new StringBuilder();

        HatosagiStatisztikaTargyszavakTemplate template = LoadHatosagiStatisztikaTargyszavakTemplate(execParam.Clone(), templateId);

        if (template != null)
        {
            Dictionary<string, string> targyszavakDict = LoadTargySzavakFromTemplate(template);

            if (targyszavakDict != null)
            {
                //ASPADOHelper.SetObjektumTargyszavak(execParam, iratId, Contentum.eUtility.Constants.TableNames.EREC_IraIratok, targyszavakDict, response);
                EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

                if (HasObjektumTargyszavai(search, iratId, null
           , Contentum.eUtility.Constants.TableNames.EREC_IraIratok, Contentum.eUtility.Constants.ColumnNames.EREC_IraIratok.Ugy_Fajtaja,
           new[] { "1" },
           false, KodTarak.OBJMETADEFINICIO_TIPUS.HatosagiAdatok, false, true,
            execParam.Clone()))
                {
                    DataSet ds = GetObjektumTargyszavai(search, iratId, null
                          , Contentum.eUtility.Constants.TableNames.EREC_IraIratok, Contentum.eUtility.Constants.ColumnNames.EREC_IraIratok.Ugy_Fajtaja,
                          new[] { "1" },
                          false, KodTarak.OBJMETADEFINICIO_TIPUS.HatosagiAdatok, false, true,
                           execParam.Clone());

                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        List<EREC_ObjektumTargyszavai> EREC_ObjektumTargyszavaiList = new List<EREC_ObjektumTargyszavai>();

                        var rows = ds.Tables[0].Rows;
                        for (int i = 0; i < rows.Count; i++)
                        {
                            if (targyszavakDict.ContainsKey(rows[i]["BelsoAzonosito"].ToString()))
                            {
                                EREC_ObjektumTargyszavai obj = new EREC_ObjektumTargyszavai();
                                obj.Updated.SetValueAll(false);
                                obj.Base.Updated.SetValueAll(false);

                                if (rows[i]["Id"] != DBNull.Value && !String.IsNullOrEmpty(rows[i]["Id"].ToString()))
                                {
                                    obj.Id = rows[i]["Id"].ToString();
                                    obj.Updated.Id = true;
                                }

                                obj.Targyszo = rows[i]["Targyszo"].ToString();
                                obj.Updated.Targyszo = true;
                                obj.Targyszo_Id = rows[i]["Targyszo_Id"].ToString();
                                obj.Updated.Targyszo_Id = true;
                                obj.Obj_Metaadatai_Id = rows[i]["Obj_Metaadatai_Id"].ToString();
                                obj.Updated.Obj_Metaadatai_Id = true;
                                obj.Ertek = targyszavakDict[rows[i]["BelsoAzonosito"].ToString()];
                                obj.Updated.Ertek = true;

                                EREC_ObjektumTargyszavaiList.Add(obj);
                            }
                        }

                        if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Count > 0)
                        {
                            Contentum.eRecord.Service.EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();

                            Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execParam.Clone()
                                , EREC_ObjektumTargyszavaiList.ToArray()
                                , iratId
                                , null
                                , Contentum.eUtility.Constants.TableNames.EREC_IraIratok
                                , KodTarak.OBJMETADEFINICIO_TIPUS.HatosagiAdatok
                                , false
                                );

                            if (!string.IsNullOrEmpty(result_ot.ErrorCode))
                            {
                                errLog.Append("Nem sikerült beállítani a hatósági staisztikákat. " + result_ot.ErrorMessage);
                            }
                        }
                    }
                }
            }
            else
            {
                errLog.AppendLine("Hiba a tárgyszavak parsolása során");
            }
        }
        else
        {
            errLog.AppendLine("Nem tal'lható a megadott template.");
        }

        if (string.IsNullOrEmpty(errLog.ToString()))
        {
            errLog.AppendLine("Sikeres template hozzárendelés. IratId: " + iratId + "TemplateId: " + templateId);
        }

        return errLog.ToString();

    }

    #endregion

    [WebMethod()]
    public Result GetForSablon(ExecParam execParam, string iratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetForSablon(execParam, iratId);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result Get_ASPADO_Iratok(ExecParam execParam, int FoszamFrom, int FoszamTo)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Get_ASPADO_Iratok(execParam, FoszamFrom, FoszamTo);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result Get_ASPADO_Iratok_IratIds(ExecParam execParam, string iratIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Get_ASPADO_Iratok_IratIds(execParam, iratIds);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    [WebMethod()]
    public Result ASPADOSztorno_Get(ExecParam execParam, string iratIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ASPADOSztorno_Get(execParam,iratIds);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result ASPADOSztorno_Update(ExecParam execParam, string iratIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ASPADOSztorno_Update(execParam, iratIds);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
}