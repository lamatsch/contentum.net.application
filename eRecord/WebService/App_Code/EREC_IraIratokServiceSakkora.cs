﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;


/// <summary>
/// Summary description for EREC_IraIratokService - Sakkora
/// </summary>
public partial class EREC_IraIratokService
{
    #region SAKKORA

    /// <summary>
    /// GetIratVoltElsoInduloSakkora
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    public bool GetIratVoltElsoInduloSakkora(ExecParam ExecParam, EREC_IraIratok Record)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool resultValue = false;
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.GetIratVoltElsoInduloSakkora(ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            resultValue = Convert.ToBoolean(result.Record);
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return resultValue;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    public Result UpdateIratDirectWithoutFlow(ExecParam ExecParam, EREC_IraIratok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();

        try
        {
            result = sp.Insert(Constants.Update, ExecParam, Record);
            if (result.IsError)
            {
                throw new ResultException(result);
            }

        }
        catch (ResultException re)
        {
            result = re.GetResult();
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region SVC GET
    private EREC_UgyUgyiratok GetUgyiratByIrat(ExecParam execParam, EREC_IraIratok irat)
    {
        EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam ugyiratokExecParam = execParam.Clone();
        ugyiratokExecParam.Record_Id = irat.Ugyirat_Id;
        Result ugyiratokResult = ugyiratokService.Get(ugyiratokExecParam);

        if (ugyiratokResult.IsError)
            throw new ResultException(ugyiratokResult);

        EREC_UgyUgyiratok ugy = (EREC_UgyUgyiratok)ugyiratokResult.Record;
        return ugy;
    }

    #endregion SVC GET

    #endregion SAKKORA
}