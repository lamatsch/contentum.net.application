﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;
using System.Linq;

/// <summary>
/// Summary description for EREC_IraIratokServiceVol1
/// </summary>
public partial class EREC_IraIratokService
{
    #region ELOSZTOIV 
    public List<string> IratPeldanyokLetrehozasasCimzettListaAlapjan(ExecParam execPaam, EREC_PldIratPeldanyok existingPldIrat, EREC_PldIratPeldanyok iratPeldanyAdatok)
    {
        IktatottVagyMunkaanyag iktatott_vagy_Munkaanyag;
        EREC_UgyUgyiratok _EREC_UgyUgyiratok;
        EREC_IraIratok _EREC_IraIratok;
        EREC_PldIratPeldanyok _EREC_PldIratPeldanyok;
        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek;
        string vonalkodkezeles;
        EREC_IraElosztoivek elosztoiv;
        Result resultBarCode;
        int Sorszam;


        Sorszam = GetIratIratPeldanyokSzama(execPaam, existingPldIrat.IraIrat_Id);
        GetParametersFromPldIrat(
                execPaam,
                existingPldIrat,
                out iktatott_vagy_Munkaanyag,
                out _EREC_UgyUgyiratok,
                out _EREC_IraIratok,
                out _EREC_PldIratPeldanyok,
                out erec_IraIktatoKonyvek,
                out vonalkodkezeles,
                out elosztoiv,
                out resultBarCode);

        Result result_IratPeldanyInsert = new Result();
        List<string> pldIratPeldanyokIdList = null;

        IratPeldanyokElosztoIvTetelekAlapjan(
                 iktatott_vagy_Munkaanyag,
                 execPaam.Clone(),
                 _EREC_UgyUgyiratok,
                 _EREC_IraIratok,
                 _EREC_PldIratPeldanyok,
                 erec_IraIktatoKonyvek,
                 vonalkodkezeles,
                 elosztoiv,
                 resultBarCode,
                 existingPldIrat,
                ref Sorszam,
                ref result_IratPeldanyInsert,
                out pldIratPeldanyokIdList
            );

        return pldIratPeldanyokIdList;
    }

    /// <summary>
    /// Összegűjti a paramétereket a címzettlistás irat példány létrehozásához
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="iratPeldany"></param>
    private void GetParametersFromPldIrat(
        ExecParam _ExecParam,
        EREC_PldIratPeldanyok iratPeldany,
        out IktatottVagyMunkaanyag iktatott_vagy_Munkaanyag,
        out EREC_UgyUgyiratok _EREC_UgyUgyiratok,
        out EREC_IraIratok _EREC_IraIratok,
        out EREC_PldIratPeldanyok _EREC_PldIratPeldanyok,
        out EREC_IraIktatoKonyvek erec_IraIktatoKonyvek,
        out string vonalkodkezeles,
        out EREC_IraElosztoivek elosztoiv,
        out Result resultBarCode
        )
    {
        elosztoiv = null;
        resultBarCode = null;

        _EREC_PldIratPeldanyok = iratPeldany;
        vonalkodkezeles = "AZONOSITO";

        iktatott_vagy_Munkaanyag = IktatottVagyMunkaanyag.Iktatottanyag;
        if (iratPeldany.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban)
            iktatott_vagy_Munkaanyag = IktatottVagyMunkaanyag.Munkaanyag;

        #region BARCODE
        if (!string.IsNullOrEmpty(iratPeldany.BarCode))
        {
            KRT_BarkodokService serviceBarCode = new KRT_BarkodokService(this.dataContext);
            ExecParam execBarCode = _ExecParam.Clone();
            KRT_BarkodokSearch srcBarCode = new KRT_BarkodokSearch();
            srcBarCode.Kod.Value = iratPeldany.BarCode;
            srcBarCode.Kod.Operator = Query.Operators.equals;
            srcBarCode.TopRow = 1;
            resultBarCode = serviceBarCode.GetAll(execBarCode, srcBarCode);
            if (!string.IsNullOrEmpty(resultBarCode.ErrorCode))
            {
                Logger.Error("EREC_IraIratokService.GetParametersFromPldIrat ", execBarCode, resultBarCode);
            }
        }
        #endregion

        #region ELOSZTOIV
        EREC_IraElosztoivekService serviceEIV = new EREC_IraElosztoivekService(this.dataContext);
        ExecParam execEIv = _ExecParam.Clone();
        EREC_IraElosztoivekSearch src = new EREC_IraElosztoivekSearch();
        src.WhereByManual = " and EREC_IraElosztoivek.Note = '" + iratPeldany.IraIrat_Id + "'";
        Result resultEIVAll = serviceEIV.GetAll(execEIv, src);
        if (!string.IsNullOrEmpty(resultEIVAll.ErrorCode) || resultEIVAll.Ds.Tables[0].Rows.Count < 1)
        {
            Logger.Error("EREC_IraIratokService.GetParametersFromPldIrat ", execEIv, resultEIVAll);
        }
        else
        {
            Result resEiv = GetElosztoIv(_ExecParam.Clone(), resultEIVAll.Ds.Tables[0].Rows[0]["id"].ToString());
            elosztoiv = (EREC_IraElosztoivek)resEiv.Record;
        }
        #endregion

        #region IRAT
        EREC_IraIratokService serviceIrat = new EREC_IraIratokService(this.dataContext);
        ExecParam execIrat = _ExecParam.Clone();
        execIrat.Record_Id = iratPeldany.IraIrat_Id;
        Result resultIrat = serviceIrat.Get(execIrat);
        if (!string.IsNullOrEmpty(resultIrat.ErrorCode))
        {
            Logger.Error("EREC_IraIratokService.GetParametersFromPldIrat ", execIrat, resultIrat);
        }
        _EREC_IraIratok = (EREC_IraIratok)resultIrat.Record;
        #endregion

        #region UGYIRAT
        EREC_UgyUgyiratokService serviceUgy = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam execUgy = _ExecParam.Clone();
        execUgy.Record_Id = _EREC_IraIratok.Ugyirat_Id;
        Result resultUgy = serviceUgy.Get(execUgy);
        if (!string.IsNullOrEmpty(resultUgy.ErrorCode))
        {
            Logger.Error("EREC_IraIratokService.GetParametersFromPldIrat ", execUgy, resultUgy);
        }
        _EREC_UgyUgyiratok = (EREC_UgyUgyiratok)resultUgy.Record;
        #endregion UGYIRAT

        #region IKTATOKONYV
        EREC_IraIktatoKonyvekService serviceIktato = new EREC_IraIktatoKonyvekService(this.dataContext);
        ExecParam execIktato = _ExecParam.Clone();
        execIktato.Record_Id = _EREC_UgyUgyiratok.IraIktatokonyv_Id;
        Result resultIktatoKonyv = serviceIktato.Get(execIktato);
        if (!string.IsNullOrEmpty(resultIktatoKonyv.ErrorCode))
        {
            Logger.Error("EREC_IraIratokService.GetParametersFromPldIrat ", execIktato, resultIktatoKonyv);
        }
        erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)resultIktatoKonyv.Record;
        #endregion UGYIRAT

    }

    /// <summary>
    /// IratPeldanyokElosztoIvTetelekAlapjan
    /// </summary>
    /// <param name="Iktatott_vagy_Munkaanyag"></param>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_UgyUgyiratok"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_PldIratPeldanyok"></param>
    /// <param name="erec_IraIktatoKonyvek"></param>
    /// <param name="vonalkodkezeles"></param>
    /// <param name="elosztoiv"></param>
    /// <param name="bid"></param>
    /// <param name="sorszam_iratPeldany"></param>
    /// <param name="result_IratPeldanyInsert"></param>
    /// <param name="pldIratPeldanyokIdList"></param>
    private void IratPeldanyokElosztoIvTetelekAlapjan(
        IktatottVagyMunkaanyag Iktatott_vagy_Munkaanyag,
        ExecParam _ExecParam,
        EREC_UgyUgyiratok _EREC_UgyUgyiratok,
        EREC_IraIratok _EREC_IraIratok,
        EREC_PldIratPeldanyok _EREC_PldIratPeldanyok,
        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek,
        string vonalkodkezeles,
        EREC_IraElosztoivek elosztoiv,
        Result bid,
        ref int sorszam_iratPeldany,
        ref Result result_IratPeldanyInsert,
        out List<string> pldIratPeldanyokIdList)
    {
        IratPeldanyokElosztoIvTetelekAlapjan(Iktatott_vagy_Munkaanyag, _ExecParam, _EREC_UgyUgyiratok, _EREC_IraIratok, _EREC_PldIratPeldanyok,
            erec_IraIktatoKonyvek, vonalkodkezeles, elosztoiv, bid, null, ref sorszam_iratPeldany, ref result_IratPeldanyInsert, out pldIratPeldanyokIdList);
    }

    /// <summary>
    /// IratPeldanyokElosztoIvTetelekAlapjan
    /// </summary>
    /// <param name="Iktatott_vagy_Munkaanyag"></param>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_UgyUgyiratok"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_PldIratPeldanyok"></param>
    /// <param name="erec_IraIktatoKonyvek"></param>
    /// <param name="vonalkodkezeles"></param>
    /// <param name="elosztoiv"></param>
    /// <param name="sorszam_iratPeldany"></param>
    /// <param name="bid"></param>
    /// <param name="result_IratPeldanyInsert"></param>
    private void IratPeldanyokElosztoIvTetelekAlapjan(
        IktatottVagyMunkaanyag Iktatott_vagy_Munkaanyag,
        ExecParam _ExecParam,
        EREC_UgyUgyiratok _EREC_UgyUgyiratok,
        EREC_IraIratok _EREC_IraIratok,
        EREC_PldIratPeldanyok _EREC_PldIratPeldanyok,
        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek,
        string vonalkodkezeles,
        EREC_IraElosztoivek elosztoiv,
        Result bid,
        EREC_PldIratPeldanyok existingPldIrat,
        ref int sorszam_iratPeldany,
        ref Result result_IratPeldanyInsert,
        out List<string> pldIratPeldanyokIdList)
    {
        pldIratPeldanyokIdList = new List<string>();
        // elosztóív listát adtak meg címzettként
        EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
        Result elosztoivTetelekGetAllResult = GetElosztoIvById(elosztoiv.Id, _ExecParam.Clone());

        if (string.IsNullOrEmpty(elosztoivTetelekGetAllResult.ErrorCode))
        {
            string kuldesModFoIratPeldany = _EREC_PldIratPeldanyok.KuldesMod;

            for (int i = 0; i < elosztoivTetelekGetAllResult.Ds.Tables[0].Rows.Count; i++)
            {
                //Létező 1 példány adatait beállítjuk a cimlista első elemének adataival
                if (i == 0 && existingPldIrat != null)
                {
                    EREC_PldIratPeldanyok iratpeldanyElsoLetezo = GetIratPeldany(_ExecParam.Clone(), existingPldIrat.Id);

                    // partnerId beállítás
                    iratpeldanyElsoLetezo.Partner_Id_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Partner_Id"].ToString();
                    iratpeldanyElsoLetezo.Updated.Partner_Id_Cimzett = true;
                    iratpeldanyElsoLetezo.NevSTR_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["NevSTR"].ToString();
                    iratpeldanyElsoLetezo.Updated.NevSTR_Cimzett = true;

                    // címzett beállítás
                    iratpeldanyElsoLetezo.Cim_id_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Cim_Id"].ToString();
                    iratpeldanyElsoLetezo.Updated.Cim_id_Cimzett = true;
                    iratpeldanyElsoLetezo.CimSTR_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["CimSTR"].ToString();
                    iratpeldanyElsoLetezo.Updated.CimSTR_Cimzett = true;

                    // küldés mód beállítása az elosztóívtételeknél beállítottra, ha iktatáskor nem adták meg

                    if (string.IsNullOrEmpty(elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Kuldesmod"].ToString()))
                        iratpeldanyElsoLetezo.KuldesMod = kuldesModFoIratPeldany;
                    else
                        iratpeldanyElsoLetezo.KuldesMod = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Kuldesmod"].ToString();

                    ExecParam execParamUpdate = _ExecParam.Clone();
                    execParamUpdate.Record_Id = existingPldIrat.Id;
                    Result result_IratPeldanyUpdate = erec_PldIratPeldanyokService.Update(execParamUpdate, iratpeldanyElsoLetezo);
                    if (!string.IsNullOrEmpty(result_IratPeldanyUpdate.ErrorCode))
                    {
                        Logger.Error("Iratpéldány Update-nél hiba", _ExecParam, result_IratPeldanyUpdate);
                        throw new ResultException(result_IratPeldanyUpdate);
                    }
                }
                else
                {
                    EREC_PldIratPeldanyok iratpeldany = _EREC_PldIratPeldanyok;
                    iratpeldany.Id = Guid.NewGuid().ToString();
                    iratpeldany.Updated.Id = true;
                    // partnerId beállítás
                    iratpeldany.Partner_Id_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Partner_Id"].ToString();
                    iratpeldany.Updated.Partner_Id_Cimzett = true;
                    iratpeldany.NevSTR_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["NevSTR"].ToString();
                    iratpeldany.Updated.NevSTR_Cimzett = true;

                    // címzett beállítás
                    iratpeldany.Cim_id_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Cim_Id"].ToString();
                    iratpeldany.Updated.Cim_id_Cimzett = true;
                    iratpeldany.CimSTR_Cimzett = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["CimSTR"].ToString();
                    iratpeldany.Updated.CimSTR_Cimzett = true;

                    // küldés mód beállítása az elosztóívtételeknél beállítottra, ha iktatáskor nem adták meg

                    if (string.IsNullOrEmpty(elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Kuldesmod"].ToString()))
                        iratpeldany.KuldesMod = kuldesModFoIratPeldany;
                    else
                        iratpeldany.KuldesMod = elosztoivTetelekGetAllResult.Ds.Tables[0].Rows[i]["Kuldesmod"].ToString();

                    // sorszám beállítása

                    ++sorszam_iratPeldany;

                    iratpeldany.Sorszam = sorszam_iratPeldany.ToString();
                    iratpeldany.Updated.Sorszam = true;

                    // Állapotot újra be kell állítani, mert visszaíródik az előző insert után a továbbítás alatt állapot
                    if (Iktatott_vagy_Munkaanyag == IktatottVagyMunkaanyag.Iktatottanyag)
                    {
                        // Állapot állítása
                        _EREC_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                        _EREC_PldIratPeldanyok.Updated.Allapot = true;

                        // Továbbítás alatt állapot állítása
                        _EREC_PldIratPeldanyok.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                        _EREC_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;
                    }
                    else
                    {
                        // Munkaanyagnál:
                        // Állapot állítása
                        _EREC_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban;
                        _EREC_PldIratPeldanyok.Updated.Allapot = true;

                        // Továbbítás alatt állapot állítása
                        _EREC_PldIratPeldanyok.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                        _EREC_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;

                    }

                    // vonalkód generálás (csak ha ügyintézésmódja elektronikus:)
                    KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
                    ExecParam barkodExecParam = _ExecParam.Clone();
                    Result barkodGenerateResult = new Result();
                    string generaltVonalkod = "";
                    //CR 3058
                    // BUG_8100 
                    //if (_EREC_PldIratPeldanyok.UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir && !vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
                    if (!vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
                    {
                        barkodGenerateResult = barkodService.BarkodGeneralas(barkodExecParam);
                        if (!string.IsNullOrEmpty(barkodGenerateResult.ErrorCode))
                            throw new ResultException(barkodGenerateResult);

                        generaltVonalkod = (string)barkodGenerateResult.Record;
                    }
                    iratpeldany.BarCode = generaltVonalkod;
                    iratpeldany.Updated.BarCode = true;

                    /// Azonosító (Iktatószám) beállítása:
                    /// 
                    // BLG_292
                    //iratpeldany.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, iratpeldany);
                    iratpeldany.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatoKonyvek, _EREC_UgyUgyiratok, _EREC_IraIratok, iratpeldany);
                    iratpeldany.Updated.Azonosito = true;

                    // iratpéldány insert;
                    result_IratPeldanyInsert = erec_PldIratPeldanyokService.Insert(_ExecParam.Clone(), iratpeldany);
                    if (!string.IsNullOrEmpty(result_IratPeldanyInsert.ErrorCode))
                    {
                        Logger.Error("Iratpéldány Insert-nél hiba", _ExecParam, result_IratPeldanyInsert);
                        throw new ResultException(result_IratPeldanyInsert);
                    }

                    pldIratPeldanyokIdList.Add(result_IratPeldanyInsert.Uid);

                    // vonalkód kötése, ha volt generálás:
                    //CR 3058
                    if ((!String.IsNullOrEmpty(iratpeldany.BarCode)
                        && !String.IsNullOrEmpty(barkodGenerateResult.Uid)) && !vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))
                    {
                        barkodGenerateResult = barkodService.BarkodBindToIratPeldany(_ExecParam.Clone(), barkodGenerateResult.Uid, result_IratPeldanyInsert.Uid);
                    }
                    else if ((!String.IsNullOrEmpty(iratpeldany.BarCode)
                        && vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO")))
                    {
                        KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                        var result_barcodebind = srvBarcode.BarkodBindToIratPeldany(_ExecParam.Clone(), bid.Uid, result_IratPeldanyInsert.Uid);
                        if (!String.IsNullOrEmpty(result_barcodebind.ErrorCode))
                        {
                            // hiba
                            Logger.Error("Hiba: Barkod rekordhoz ügyirat hozzákötése", _ExecParam.Clone());
                            throw new ResultException(result_barcodebind);
                        }
                    }
                }

            }

            if (elosztoivTetelekGetAllResult.Ds.Tables[0].Rows.Count == 0)
                throw new ResultException(53400);
        }
    }

    /// <summary>
    /// GetElosztoIvById
    /// </summary>
    /// <param name="elosztoivId"></param>
    /// <param name="execParam_IratPeldanyInsert"></param>
    /// <returns></returns>
    private Result GetElosztoIvById(string elosztoivId, ExecParam execParam_IratPeldanyInsert)
    {
        EREC_IraElosztoivTetelekService elosztoivTetelekService = new EREC_IraElosztoivTetelekService(this.dataContext);

        EREC_IraElosztoivTetelekSearch elosztoivTetelekSearch = new EREC_IraElosztoivTetelekSearch();
        elosztoivTetelekSearch.ElosztoIv_Id.Value = elosztoivId;
        elosztoivTetelekSearch.ElosztoIv_Id.Operator = Query.Operators.equals;
        Result elosztoivTetelekGetAllResult = elosztoivTetelekService.GetAll(execParam_IratPeldanyInsert, elosztoivTetelekSearch);
        return elosztoivTetelekGetAllResult;
    }
    /// <summary>
    /// Elosztoiv lekérése irat alapján
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <returns></returns>
    private Result GetElosztoIvByIrat(ExecParam execParam, string iratId)
    {
        if (string.IsNullOrEmpty(iratId))
            return null;
        EREC_IraElosztoivekService service = new EREC_IraElosztoivekService(this.dataContext);
        EREC_IraElosztoivekSearch src = new EREC_IraElosztoivekSearch();
        src.WhereByManual = " and EREC_IraElosztoivek.Note = '" + iratId + "'";
        Result result = service.GetAll(execParam, src);

        return result;
    }
    /// <summary>
    /// Elosztoiv lekérése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    private Result GetElosztoIv(ExecParam execParam, string id)
    {
        EREC_IraElosztoivekService service = new EREC_IraElosztoivekService(this.dataContext);
        execParam.Record_Id = id;
        Result result = service.Get(execParam);
        return result;
    }
    /// <summary>
    /// Irat id mentése az elosztoivhez
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="elosztoIvId"></param>
    /// <param name="iratId"></param>
    /// <returns></returns>
    private Result UpdateElosztoIvNoteField(ExecParam execParam, string elosztoIvId, string iratId)
    {
        if (string.IsNullOrEmpty(elosztoIvId) || string.IsNullOrEmpty(iratId))
            return null;

        Result resultEIv = GetElosztoIv(execParam, elosztoIvId);
        if (string.IsNullOrEmpty(resultEIv.ErrorCode))
        {
            EREC_IraElosztoivek eiv = (EREC_IraElosztoivek)resultEIv.Record;
            eiv.Base.Note = iratId;
            eiv.Base.Updated.Note = true;

            UpdateElosztoIv(execParam.Clone(), eiv);
        }
        return null;
    }
    /// <summary>
    /// ElosztoIv módosítása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="elosztoiv"></param>
    /// <returns></returns>
    private Result UpdateElosztoIv(ExecParam execParam, EREC_IraElosztoivek elosztoiv)
    {
        EREC_IraElosztoivekService service = new EREC_IraElosztoivekService(this.dataContext);
        execParam.Record_Id = elosztoiv.Id;
        Result result = service.Update(execParam, elosztoiv);
        return result;
    }

    private int GetIratIratPeldanyokSzama(ExecParam execParam, string iratId)
    {
        EREC_PldIratPeldanyokService service = new EREC_PldIratPeldanyokService(this.dataContext);
        EREC_PldIratPeldanyokSearch src = new EREC_PldIratPeldanyokSearch();
        src.IraIrat_Id.Value = iratId;
        src.IraIrat_Id.Operator = Query.Operators.equals;
        Result result = service.GetAll(execParam, src);
        if (!string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count < 1)
            return 0;
        return result.Ds.Tables[0].Rows.Count;
    }

    private EREC_PldIratPeldanyok GetIratPeldany(ExecParam execParam, string id)
    {
        EREC_PldIratPeldanyokService service = new EREC_PldIratPeldanyokService(this.dataContext);
        execParam.Record_Id = id;
        Result result = service.Get(execParam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
            return null;
        return (EREC_PldIratPeldanyok)result.Record;
    }
    #endregion ELOSZTOIV

    #region 1560 VONALKODGENERALAS
    /// <summary>
    /// VonalkodGeneralasElektronikusIratEseten
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kuldemeny"></param>
    private void VonalkodGeneralasElektronikusIratEsParameterEseten(ExecParam execParam, ref EREC_KuldKuldemenyek kuldemeny, ref IktatasiParameterek iktatasiParameterek)
    {
        if (Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT) == "1"
               && kuldemeny.AdathordozoTipusa == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir
               && string.IsNullOrEmpty(kuldemeny.BarCode))
        {
            kuldemeny.BarCode = VonalkodGeneralas(execParam);
            kuldemeny.Updated.BarCode = true;

            if (string.IsNullOrEmpty(iktatasiParameterek.Iratpeldany_Vonalkod))
                iktatasiParameterek.Iratpeldany_Vonalkod = kuldemeny.BarCode;
        }
    }
    /// <summary>
    /// VonalkodGeneralasElektronikusIratEsParameterEseten
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kuldemeny"></param>
    private void VonalkodGeneralasElektronikusIratEsParameterEseten(ExecParam execParam, EREC_IraIratok irat, ref IktatasiParameterek iktatasiParameterek)
    {
        if (Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT) == "1"
               && irat.AdathordozoTipusa == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir
               && string.IsNullOrEmpty(iktatasiParameterek.Iratpeldany_Vonalkod))
        {
            iktatasiParameterek.Iratpeldany_Vonalkod = VonalkodGeneralas(execParam);
        }
    }
    /// <summary>
    /// VonalkodGeneralas
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    private string VonalkodGeneralas(ExecParam execParam)
    {
        KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
        ExecParam execParam_barkod = execParam.Clone();

        Result result_barkod = service_barkodok.BarkodGeneralas(execParam_barkod);
        if (!String.IsNullOrEmpty(result_barkod.ErrorCode))
        {
            throw new ResultException(result_barkod);
        }
        else
        {
            return (String)result_barkod.Record;
        }
    }
    #endregion 1560 VONALKODGENERALAS

    [WebMethod()]
    /// <summary>
    /// Irat átvétele ügyintézésre + sakkóra státusz állítás
    /// (NMHH-s webservice-hez)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <param name="SakkoraStatus"></param>
    /// <param name="OkKod"></param>
    /// <returns></returns>
    public Result IratAtvetelUgyintezesreSakkoraAllitassal(ExecParam execParam, string iratId, string sakkoraStatus, string okKod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        #region CHECKS
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            Result result1 = ResultError.CreateNewResultWithErrorCode(52160);
            log.WsEnd(execParam, result1);
            return result1;
        }

        if (string.IsNullOrEmpty(iratId))
        {
            Result res = new Result() { ErrorMessage = "Irat azonosító nincs megadva !" };
            log.WsEnd(execParam, res);
            return res;
        }
        #endregion

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region IRAT PELDANYOK ATVETELE
            EREC_PldIratPeldanyokService service = new EREC_PldIratPeldanyokService(this.dataContext);

            #region GET ALL IRAT PELDANY
            EREC_PldIratPeldanyokSearch src = new EREC_PldIratPeldanyokSearch();
            src.IraIrat_Id.Value = iratId;
            src.IraIrat_Id.Operator = Query.Operators.equals;
            src.Allapot.Value = KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt;
            src.Allapot.Operator = Query.Operators.equals;

            Result resultIratPeldanyokList = service.GetAll(execParam, src);
            resultIratPeldanyokList.CheckError();
            #endregion
            
            Result resultIratPeldanyAtvetel = null;

            if (resultIratPeldanyokList.Ds.Tables[0].Rows.Count > 0)
            {
                List<string> idsList = new List<string>();
                for (int i = 0; i < resultIratPeldanyokList.Ds.Tables[0].Rows.Count; i++)
                {
                    idsList.Add(resultIratPeldanyokList.Ds.Tables[0].Rows[i]["Id"].ToString());
                }

                if (idsList != null && idsList.Count > 0)
                {
                    // BUG#6297: Csak azokat próbáljuk meg átvenni, amiket neki (vagy a szervezetének) címeztek, különben hiba jön:
                    #region Kézbesítési tételek lekérése

                    // Kézbesítési tételek lekérése: szűrés az iratpéldányokra, és hogy a címzett a felhasználó vagy a felhasználó szervezete
                    EREC_IraKezbesitesiTetelekSearch searchKezbTetelek = new EREC_IraKezbesitesiTetelekSearch(true);
                    searchKezbTetelek.Csoport_Id_Cel.Value = "'" + Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam) + "'";
                    if (!String.IsNullOrEmpty(execParam.FelhasznaloSzervezet_Id))
                    {
                        searchKezbTetelek.Csoport_Id_Cel.Value += ",'" + execParam.FelhasznaloSzervezet_Id + "'";
                    }
                    searchKezbTetelek.Csoport_Id_Cel.Operator = Query.Operators.inner;

                    // Iratpéldány Id-k:
                    searchKezbTetelek.Obj_Id.Value = Search.GetSqlInnerString(idsList.ToArray());
                    searchKezbTetelek.Obj_Id.Operator = Query.Operators.inner;
                    
                    // Szűrés csak az átveendőkre:
                    searchKezbTetelek.Allapot.Value = Search.GetSqlInnerString(new string[] {
                     KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                     , KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott});
                    searchKezbTetelek.Allapot.Operator = Query.Operators.inner;

                    var resultKezbTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext).GetAll(execParam, searchKezbTetelek);
                    resultKezbTetelek.CheckError();

                    // Azok az iratpéldány Id-k, amik átvehetők:
                    List<string> pldIdsListAtvehetok = new List<string>();
                    foreach(DataRow rowKezbTetel in resultKezbTetelek.Ds.Tables[0].Rows)
                    {
                        pldIdsListAtvehetok.Add(rowKezbTetel["Obj_Id"].ToString());
                    }

                    // Az átvételt csak azokra az iratpéldányokra fogjuk meghívni, amik ebben a listában is benne vannak:
                    idsList = idsList.Where(e => pldIdsListAtvehetok.Contains(e)).ToList();

                    #endregion

                    if (idsList.Count > 0)
                    {
                        resultIratPeldanyAtvetel = service.Atvetel_Tomeges(execParam, idsList.ToArray(), execParam.Felhasznalo_Id);
                        resultIratPeldanyAtvetel.CheckError();
                    }
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
   
    /// <summary>
    /// Irat Ugy fajta beallitas
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="id"></param>
    /// <param name="ugyFajta"></param>
    /// <returns></returns>
    [WebMethod(Description = "Irat Ugy fajta beallitas")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIratok))]
    public Result UpdateUgyFajta(ExecParam ExecParam, string ugyFajta)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Régi irat lekérése DB-bõl
            Result result_iratGet = this.Get(ExecParam);
            result_iratGet.CheckError();
            EREC_IraIratok Record = result_iratGet.Record as EREC_IraIratok;
            #endregion

            if (Record.Ugy_Fajtaja != ugyFajta)
            {
                //LZS - BUG_8696
                Record.Updated.SetValueAll(false);
                Record.Ugy_Fajtaja = ugyFajta;
                Record.Updated.Ugy_Fajtaja = true;

                EREC_IraIratokStoredProcedure sp = new EREC_IraIratokStoredProcedure(dataContext);
                result = sp.Insert(Constants.Update, ExecParam, Record);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}