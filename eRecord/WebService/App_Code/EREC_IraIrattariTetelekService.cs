using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IraIrattariTetelekService : System.Web.Services.WebService
{
    #region Gener�ltb�l �tvettek
    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_IraIrattariTetelek Record)
    /// Egy rekord felv�tele a EREC_IraIrattariTetelek t�bl�ba,
    /// �s a neki megfelel� irat metadefin�ci� l�trehoz�sa (ha nem l�tezik m�g), az EREC_IratMetaDefinicio t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIrattariTetelek))]
    public Result Insert(ExecParam ExecParam, EREC_IraIrattariTetelek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_IraIrattariTetelek", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            #region Irat metadefin�ci� felv�tele
            // l�tezik-e m�r hozz�rendel�s
            EREC_IratMetaDefinicioService serviceIMD = new EREC_IratMetaDefinicioService(this.dataContext);
            ExecParam execParamIMD = ExecParam.Clone();

            // elvileg nem lehet m�g ilyen...
            EREC_IratMetaDefinicio erec_IratMetaDefinicio = new EREC_IratMetaDefinicio();
            erec_IratMetaDefinicio.Ugykor_Id = result.Uid;
            erec_IratMetaDefinicio.Updated.Ugykor_Id = true;
            erec_IratMetaDefinicio.UgykorKod = Record.IrattariTetelszam;
            erec_IratMetaDefinicio.Updated.UgykorKod = true;
            // BUG_3918
            erec_IratMetaDefinicio.ErvKezd = Record.ErvKezd;
            erec_IratMetaDefinicio.Updated.ErvKezd = true;
            erec_IratMetaDefinicio.ErvVege = Record.ErvVege;
            erec_IratMetaDefinicio.Updated.ErvVege = true;
            Result result_insertIMD = serviceIMD.Insert(execParamIMD, erec_IratMetaDefinicio);

            if (!String.IsNullOrEmpty(result_insertIMD.ErrorCode))
            {
                throw new ResultException(result_insertIMD);
            }

            #endregion Irat metadefin�ci� felv�tele

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Update(ExecParam ExecParam, EREC_IraIrattariTetelek Record)
    /// Az EREC_IraIrattariTetelek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa.
    /// Ha az IrattariTetelszam v�ltozott, a hozz� tartoz� irat metadefin�ci�t is Update-elni kell
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIrattariTetelek))]
    public Result Update(ExecParam ExecParam, EREC_IraIrattariTetelek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region eredeti rekord lek�rdez�se
            EREC_IraIrattariTetelek erec_IraIrattariTetelek = null;

            // az�rt sz�ks�ges, �ssze lehessen hasonl�tani az IrattariTetelszamot
            if (Record.Updated.IrattariTetelszam == true)
            {
                Result result_get = Get(ExecParam);

                if (!String.IsNullOrEmpty(result_get.ErrorCode))
                {
                    throw new ResultException(result_get);
                }

                erec_IraIrattariTetelek = (EREC_IraIrattariTetelek)result_get.Record;
            }

            #endregion  eredeti rekord lek�rdez�se

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_IraIrattariTetelek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            #region irat metadefin�ci� update, ha sz�ks�ges
            // BUG_3918 
            //if (Record.Updated.IrattariTetelszam == true
            //    && erec_IraIrattariTetelek != null
            //    && erec_IraIrattariTetelek.IrattariTetelszam != Record.IrattariTetelszam)
            if ((Record.Updated.IrattariTetelszam == true
               && erec_IraIrattariTetelek != null
               && erec_IraIrattariTetelek.IrattariTetelszam != Record.IrattariTetelszam)
               || (Record.Updated.ErvKezd == true && erec_IraIrattariTetelek != null
               && erec_IraIrattariTetelek.ErvKezd != Record.ErvKezd) 
               || (Record.Updated.ErvVege == true && erec_IraIrattariTetelek != null
               && erec_IraIrattariTetelek.ErvVege != Record.ErvVege) )               
            {
                // al�rendelt irat metadefin�ci� szintek UPDATE-je
                // el�g a legfels�t megtal�lni, az EREC_IratMEtaDefinicioService.Update elv�gzi
                // a teljes hierarchia kezel�s�t
                EREC_IratMetaDefinicioService serviceIMD = new EREC_IratMetaDefinicioService(this.dataContext);
                EREC_IratMetaDefinicioSearch searchIMD = new EREC_IratMetaDefinicioSearch();
                ExecParam execParamIMD = ExecParam.Clone();

                searchIMD.Ugykor_Id.Value = ExecParam.Record_Id;
                searchIMD.Ugykor_Id.Operator = Query.Operators.equals;

                searchIMD.Ugytipus.Value = "";
                searchIMD.Ugytipus.Operator = Query.Operators.isnull;

                searchIMD.EljarasiSzakasz.Value = "";
                searchIMD.EljarasiSzakasz.Operator = Query.Operators.isnull;

                searchIMD.Irattipus.Value = "";
                searchIMD.Irattipus.Operator = Query.Operators.isnull;

                Result result_getallIMD = serviceIMD.GetAll(execParamIMD, searchIMD);
                if (!String.IsNullOrEmpty(result_getallIMD.ErrorCode))
                {
                    throw new ResultException(result_getallIMD);
                }

                // elvileg csak egyet szabad tal�lnia
                if (result_getallIMD.Ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataRow row = result_getallIMD.Ds.Tables[0].Rows[0];
                    string Id = row["Id"].ToString();
                    if (!String.IsNullOrEmpty(Id))
                    {
                        execParamIMD.Record_Id = Id;
                        EREC_IratMetaDefinicio erec_IratMetaDefinicio = new EREC_IratMetaDefinicio();
                        Utility.LoadBusinessDocumentFromDataRow(erec_IratMetaDefinicio, row);

                        if (erec_IratMetaDefinicio != null)
                        {
                            erec_IratMetaDefinicio.UgykorKod = Record.IrattariTetelszam;
                            erec_IratMetaDefinicio.Updated.UgykorKod = true;

                            // BUG_3918 
                            erec_IratMetaDefinicio.ErvKezd = Record.ErvKezd;
                            erec_IratMetaDefinicio.Updated.ErvKezd = Record.Updated.ErvKezd;
                            erec_IratMetaDefinicio.ErvVege = Record.ErvVege;
                            erec_IratMetaDefinicio.Updated.ErvVege = Record.Updated.ErvVege;


                            Result result_updateIMD = serviceIMD.Update(execParamIMD, erec_IratMetaDefinicio);

                            if (!String.IsNullOrEmpty(result_updateIMD.ErrorCode))
                            {
                                throw new ResultException(result_updateIMD);
                            }
                        }
                    }
                }

            }

            #endregion irat metadefin�ci� update, ha sz�ks�ges

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az EREC_IraIrattariTetelek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s).
    /// �rv�nytelen�ti az adott �gyk�rbe tartoz� �sszes irat metadefin�ci�t is.
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIrattariTetelek))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_IraIrattariTetelek", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion


            #region Irat metadefin�ci�k t�rl�se
            // al�rendelt szintek meghat�roz�sa, �rv�nytelen�t�se
            // el�g a legfels�t megtal�lni, az EREC_IratMEtaDefinicioService.Update elv�gzi
            // a teljes hierarchia kezel�s�t
            EREC_IratMetaDefinicioService serviceIMD = new EREC_IratMetaDefinicioService(this.dataContext);
            EREC_IratMetaDefinicioSearch searchIMD = new EREC_IratMetaDefinicioSearch();
            ExecParam execParamIMD = ExecParam.Clone();

            searchIMD.Ugykor_Id.Value = ExecParam.Record_Id;
            searchIMD.Ugykor_Id.Operator = Query.Operators.equals;

            searchIMD.Ugytipus.Value = "";
            searchIMD.Ugytipus.Operator = Query.Operators.isnull;

            searchIMD.EljarasiSzakasz.Value = "";
            searchIMD.EljarasiSzakasz.Operator = Query.Operators.isnull;

            searchIMD.Irattipus.Value = "";
            searchIMD.Irattipus.Operator = Query.Operators.isnull;

            Result result_getallIMD = serviceIMD.GetAll(execParamIMD, searchIMD);
            if (!String.IsNullOrEmpty(result_getallIMD.ErrorCode))
            {
                throw new ResultException(result_getallIMD);
            }

            // elvileg csak egyet szabad tal�lnia
            if (result_getallIMD.Ds.Tables[0].Rows.Count > 0)
            {
                System.Data.DataRow row = result_getallIMD.Ds.Tables[0].Rows[0];
                string Id = row["Id"].ToString();
                if (!String.IsNullOrEmpty(Id))
                {
                    execParamIMD.Record_Id = Id;
                    Result result_invalidateIMD = serviceIMD.Invalidate(execParamIMD);

                    if (!String.IsNullOrEmpty(result_invalidateIMD.ErrorCode))
                    {
                        throw new ResultException(result_invalidateIMD);
                    }
                }
            }
            #endregion Irat metadefin�ci�k t�rl�se


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion Gener�ltb�l �tvettek

    #region BLG_2968
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIrattariTetelekSearch))]
    public String CreateIrattariTetelUgyiratokDexXml(ExecParam ExecParam, EREC_IraIrattariTetelekSearch _EREC_IraIrattariTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        String result = String.Empty;
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.CreateIrattariTetelUgyiratokDexXml(ExecParam, _EREC_IraIrattariTetelekSearch);

        }
        catch (Exception e)
        {
            result = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam);
        return result;
    }
    #endregion

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIrattariTetelekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraIrattariTetelekSearch _EREC_IraIrattariTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_IraIrattariTetelekSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_EREC_IraIrattariTetelekSearch, new EREC_IraIrattariTetelekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "EREC_IraIrattariTetelek", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_IraIrattariTetelekSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_EREC_IraIrattariTetelekSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _EREC_IraIrattariTetelekSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIrattariTetelekSearch))]
    public Result GetAllWithExtensionWithIktatoKonyvId(ExecParam ExecParam, EREC_IraIrattariTetelekSearch _EREC_IraIrattariTetelekSearch, string iktatokonyvId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtensionWithIktatoKonyvId(ExecParam, _EREC_IraIrattariTetelekSearch, iktatokonyvId);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_EREC_IraIrattariTetelekSearch, new EREC_IraIrattariTetelekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "EREC_IraIrattariTetelek", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_IraIrattariTetelekSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_EREC_IraIrattariTetelekSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _EREC_IraIrattariTetelekSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraIrattariTetelekSearch))]
    public Result GetAllWithIktathat(ExecParam ExecParam, EREC_IraIrattariTetelekSearch _EREC_IraIrattariTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithIktathat(ExecParam, _EREC_IraIrattariTetelekSearch);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}