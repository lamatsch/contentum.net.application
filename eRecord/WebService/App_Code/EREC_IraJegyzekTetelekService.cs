using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;
using System.Collections.Generic;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IraJegyzekTetelekService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraJegyzekTetelekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraJegyzekTetelekSearch _EREC_IraJegyzekTetelekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_IraJegyzekTetelekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region fel�lvizsg�lat
    [WebMethod()]
    public Result JegyzekreHelyezes(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array
    , String jegyzekId, string megjegyzes)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_UgyUgyiratok_Id_Array == null || erec_UgyUgyiratok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52700);
            }

            // jegyz�k lek�r�se: Egy�b szervezetnek �tad�sn�l el kell menteni az el�z� �llapotot
            EREC_IraJegyzekekService service_jegyzekek = new EREC_IraJegyzekekService();
            ExecParam execParam_jegyzekekGet = execParam.Clone();
            execParam_jegyzekekGet.Record_Id = jegyzekId;

            Result result_jegyzekekGet = service_jegyzekek.Get(execParam_jegyzekekGet);

            if (result_jegyzekekGet.IsError)
            {
                throw new ResultException(result_jegyzekekGet);
            }

            EREC_IraJegyzekek erec_IraJegyzekek = (EREC_IraJegyzekek)result_jegyzekekGet.Record;

            List<string> jegyzektetelIds = new List<string>();

            if (erec_UgyUgyiratok_Id_Array.Length > 0)
            {
                if (!Rendszerparameterek.IsTUK(execParam))
                {
                    Result result_ugyiratokGetAll = SetJegyzekreHelyezesAllapot_Ugyiratok(execParam, erec_UgyUgyiratok_Id_Array, erec_IraJegyzekek);

                    if (result_ugyiratokGetAll.IsError)
                    {
                        throw new ResultException(result_ugyiratokGetAll);
                    }

                    foreach (DataRow row in result_ugyiratokGetAll.Ds.Tables[0].Rows)
                    {

                        //irajegyz�kt�telek insert�l�sa
                        EREC_IraJegyzekTetelek jegyzekTetel = new EREC_IraJegyzekTetelek();
                        jegyzekTetel.Jegyzek_Id = jegyzekId;
                        jegyzekTetel.Obj_Id = row["Id"].ToString();
                        jegyzekTetel.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                        jegyzekTetel.Megjegyzes = megjegyzes;

                        string azonosito = row["Foszam_Merge"].ToString();

                        jegyzekTetel.Azonosito = azonosito;

                        Result result_insert = this.Insert(execParam, jegyzekTetel);

                        if (result_insert.IsError)
                        {
                            throw new ResultException(result_insert);
                        }


                        // esem�nynapl�hoz
                        jegyzektetelIds.Add(result_insert.Uid);
                    }
                }
                else //T�K eset�n p�ld�nyok jegyz�kre helyez�se
                {
                    EREC_PldIratPeldanyokService peldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
                    EREC_PldIratPeldanyokSearch peldanyokSearch = new EREC_PldIratPeldanyokSearch();

                    peldanyokSearch.IraIrat_Id.Value = String.Format("Select Id from EREC_IraIratok where Ugyirat_Id in ({0})", Search.GetSqlInnerString(erec_UgyUgyiratok_Id_Array));
                    peldanyokSearch.IraIrat_Id.Operator = Query.Operators.inner;

                    string iAllapotok = "'" + KodTarak.IRATPELDANY_ALLAPOT.Iktatott
                    + "','" + KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt
                    + "','" + KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott
                    + "','" + KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt
                    + "','" + KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban + "'";

                    peldanyokSearch.Allapot.Value = iAllapotok;
                    peldanyokSearch.Allapot.Operator = Query.Operators.inner;

                    Result peldanyokResult = peldanyokService.GetAll(execParam, peldanyokSearch);

                    if (peldanyokResult.IsError)
                    {
                        throw new ResultException(peldanyokResult);
                    }

                    List<string> peldanyok = new List<string>();

                    foreach (DataRow row in peldanyokResult.Ds.Tables[0].Rows)
                    {

                        peldanyok.Add(row["Id"].ToString());
                    }

                    if (peldanyok.Count > 0)
                    {
                        Result peldanyJegyzekreHelyezesResult = this.JegyzekreHelyezes_Peldanyok(execParam, peldanyok.ToArray(), jegyzekId, megjegyzes);

                        if (peldanyJegyzekreHelyezesResult.IsError)
                        {
                            throw new ResultException(peldanyJegyzekreHelyezesResult);
                        }

                        jegyzektetelIds = peldanyJegyzekreHelyezesResult.Record as List<string>;
                    }
                }
            }


            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                string ids = Search.GetSqlInnerString(jegyzektetelIds.ToArray());

                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ids, "JegyzekTetelNew");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_IraJegyzekTetelek::JegyzekTetelNew: ", execParam, eventLogResult);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    private string[] AddSzereltUgyiratokToArray(ExecParam execParam, string[] erec_UgyUgyiratok_Id_Array)
    {
        Result res = new Result();
        EREC_UgyUgyiratokService svc = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam xpm = execParam.Clone();
        List<string> szereltIds = new List<string>();
        foreach (string id in erec_UgyUgyiratok_Id_Array)
        {
            xpm.Record_Id = id;
            res = svc.GetAllSzereltByParent(xpm);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                throw new ResultException(res);
            }

            foreach (DataRow rowSzereltUgyirat in res.Ds.Tables[0].Rows)
            {
                string ugyiratId = rowSzereltUgyirat["Id"].ToString();
                szereltIds.Add(ugyiratId);
            }
        }

        if (szereltIds.Count > 0)
        {
            string[] temp = new string[erec_UgyUgyiratok_Id_Array.Length + szereltIds.Count];
            for (int i = 0; i < erec_UgyUgyiratok_Id_Array.Length; i++)
            {
                temp[i] = erec_UgyUgyiratok_Id_Array[i];
            }

            for (int i = erec_UgyUgyiratok_Id_Array.Length; i < erec_UgyUgyiratok_Id_Array.Length + szereltIds.Count; i++)
            {
                temp[i] = szereltIds[i - erec_UgyUgyiratok_Id_Array.Length];
            }

            erec_UgyUgyiratok_Id_Array = temp;
        }

        return erec_UgyUgyiratok_Id_Array;
    }

    private string[] AddCsatoltUgyiratokToArray(ExecParam execParam, string[] erec_UgyUgyiratok_Id_Array)
    {
        //csatol�sok lek�r�se
        EREC_UgyiratKapcsolatokSearch schUgyiratKapcsolat = new EREC_UgyiratKapcsolatokSearch();
        EREC_UgyiratKapcsolatokService svcUgyiratKapcsolatok = new EREC_UgyiratKapcsolatokService(this.dataContext);

        //string ugyiratIds = String.Empty;
        //for (int i = 0; i < erec_UgyUgyiratok_Id_Array.Length; i++)
        //{
        //    if (i != 0)
        //    {
        //        ugyiratIds += ",";
        //    }
        //    ugyiratIds += "'" + erec_UgyUgyiratok_Id_Array[i] + "'";
        //}

        string ugyiratIds = Search.GetSqlInnerString(erec_UgyUgyiratok_Id_Array);
        schUgyiratKapcsolat.Ugyirat_Ugyirat_Beepul.Value = ugyiratIds;
        schUgyiratKapcsolat.Ugyirat_Ugyirat_Beepul.Operator = Query.Operators.inner;

        Result res = svcUgyiratKapcsolatok.GetAllWithExtension(execParam, schUgyiratKapcsolat);
        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            throw new ResultException(res);
        }

        List<string> csatoltIds = new List<string>();

        foreach (DataRow rowCsatoltugyirat in res.Ds.Tables[0].Rows)
        {
            string ugyiratId = rowCsatoltugyirat["Ugyirat_Ugyirat_Felepul"].ToString();
            if (!ugyiratIds.Contains(ugyiratId))
            {
                csatoltIds.Add(ugyiratId);
            }
        }

        if (csatoltIds.Count > 0)
        {
            string[] temp = new string[erec_UgyUgyiratok_Id_Array.Length + csatoltIds.Count];
            for (int i = 0; i < erec_UgyUgyiratok_Id_Array.Length; i++)
            {
                temp[i] = erec_UgyUgyiratok_Id_Array[i];
            }

            for (int i = erec_UgyUgyiratok_Id_Array.Length; i < erec_UgyUgyiratok_Id_Array.Length + csatoltIds.Count; i++)
            {
                temp[i] = csatoltIds[i - erec_UgyUgyiratok_Id_Array.Length];
            }

            erec_UgyUgyiratok_Id_Array = temp;
        }

        return erec_UgyUgyiratok_Id_Array;
    }
    

    [WebMethod()]
    public Result CheckCsatolasok(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_UgyUgyiratok_Id_Array == null || erec_UgyUgyiratok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52700);
            }
            else
            {
                EREC_UgyiratKapcsolatokSearch schUgyiratKapcsolat = new EREC_UgyiratKapcsolatokSearch();
                EREC_UgyiratKapcsolatokService svcUgyiratKapcsolatok = new EREC_UgyiratKapcsolatokService(this.dataContext);

                //string ugyiratIds = String.Empty;
                //for (int i = 0; i < erec_UgyUgyiratok_Id_Array.Length; i++)
                //{
                //    if (i != 0)
                //    {
                //        ugyiratIds += ",";
                //    }
                //    ugyiratIds +=  "'" + erec_UgyUgyiratok_Id_Array[i] + "'";
                //}

                schUgyiratKapcsolat.Ugyirat_Ugyirat_Beepul.Value = Search.GetSqlInnerString(erec_UgyUgyiratok_Id_Array); //ugyiratIds;
                schUgyiratKapcsolat.Ugyirat_Ugyirat_Beepul.Operator = Query.Operators.inner;

                Result res = svcUgyiratKapcsolatok.GetAllWithExtension(execParam, schUgyiratKapcsolat);
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    throw new ResultException(res);
                }

                EREC_UgyUgyiratokService svcUgyiratok = new EREC_UgyUgyiratokService(this.dataContext);
                ExecParam execParam_UgyiratGet = execParam.Clone();
                EREC_UgyUgyiratokSearch sch = new EREC_UgyUgyiratokSearch();
                sch.Id.Value = erec_UgyUgyiratok_Id_Array[0];
                sch.Id.Operator = Query.Operators.equals;

                Result result_ugyiratGet = svcUgyiratok.GetAllWithExtension(execParam_UgyiratGet,sch);
                if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_ugyiratGet);
                }

                string irattariJel = result_ugyiratGet.Ds.Tables[0].Rows[0]["IrattariJel"].ToString();

                List<DataRow> deletableRows = new List<DataRow>();

                foreach (DataRow rowUgyirat in res.Ds.Tables[0].Rows)
                {
                    if(IsCsatolasOk(execParam, rowUgyirat["Ugyirat_Ugyirat_Felepul"].ToString(),irattariJel))
                    {
                        deletableRows.Add(rowUgyirat);
                    }
                }

                foreach (DataRow rowUgyirat in deletableRows)
                {
                    res.Ds.Tables[0].Rows.Remove(rowUgyirat);
                }

                result = res;
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    private bool IsCsatolasOk(ExecParam execParam, string ugyiratFelepul, string irattariJel)
    {
        EREC_UgyUgyiratokService svcUgyiratok = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam execParam_UgyiratGet = execParam.Clone();
        EREC_UgyUgyiratokSearch sch = new EREC_UgyUgyiratokSearch();
        sch.Id.Value = ugyiratFelepul;
        sch.Id.Operator = Query.Operators.equals;

        Result result_ugyiratGet = svcUgyiratok.GetAllWithExtension(execParam_UgyiratGet,sch);
        if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_ugyiratGet);
        }

        DataRow row_ugyiratBeepul = result_ugyiratGet.Ds.Tables[0].Rows[0];

        if (DateTime.Parse(row_ugyiratBeepul["MegorzesiIdoVege"].ToString()) > DateTime.Now)
        {
            return false;
        }

        if (row_ugyiratBeepul["IrattariJel"].ToString() != irattariJel)
        {
            return false;
        }

        if (row_ugyiratBeepul["Allapot"].ToString() != KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott)
        {
            return false;
        }

        if (row_ugyiratBeepul["Csoport_Id_Felelos"].ToString() != KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id)
        {
            return false;
        }

        return true;
    }

    [WebMethod()]
    public Result CheckSzerelesek(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_UgyUgyiratok_Id_Array == null || erec_UgyUgyiratok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52700);
            }
            else
            {
                EREC_UgyUgyiratokService svcUgyiratok = new EREC_UgyUgyiratokService(this.dataContext);
                
                //iratt�ri jel meghat�roz�sa
                ExecParam execParam_UgyiratGet = execParam.Clone();
                EREC_UgyUgyiratokSearch sch = new EREC_UgyUgyiratokSearch();
                sch.Id.Value = erec_UgyUgyiratok_Id_Array[0];
                sch.Id.Operator = Query.Operators.equals;

                Result result_ugyiratGet = svcUgyiratok.GetAllWithExtension(execParam_UgyiratGet,sch);
                if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_ugyiratGet);
                }

                string irattariJel = result_ugyiratGet.Ds.Tables[0].Rows[0]["IrattariJel"].ToString();

                result.Ds = result_ugyiratGet.Ds;
                result.Ds.Tables[0].Rows.Clear();
                ExecParam xpm = execParam.Clone();
                string badSzerelt�gyiratok = String.Empty;

                foreach (string ParentId in erec_UgyUgyiratok_Id_Array)
                {
                    xpm.Record_Id = ParentId;
                    Result res = svcUgyiratok.GetAllSzereltByParent(xpm);
                    
                    if (!String.IsNullOrEmpty(res.ErrorCode))
                    {
                        throw new ResultException(res);
                    }

                    CheckSzereltUgyiratok(xpm, res.Ds.Tables[0], result.Ds.Tables[0], irattariJel);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    private void CheckSzereltUgyiratok(ExecParam execParam, DataTable dataTableSource, DataTable dataTableDest, string irattariJel)
    {
        if (dataTableSource.Rows.Count == 0)
        {
            return;
        }

        ExecParam xpm = execParam.Clone();
        xpm.Record_Id = String.Empty;
        //string ids = String.Empty;
        List<string> lstIds = new List<string>(dataTableSource.Rows.Count);
        foreach (DataRow rowSzerelt in dataTableSource.Rows)
        {
            //if (ids != String.Empty)
            //{
            //    ids += ",";
            //}

            //ids += "'" + rowSzerelt["Id"].ToString() + "'";
            lstIds.Add(rowSzerelt["Id"].ToString());
        }

        EREC_UgyUgyiratokService svc = new EREC_UgyUgyiratokService(this.dataContext);
        EREC_UgyUgyiratokSearch sch = new EREC_UgyUgyiratokSearch();
        sch.Id.Value = Search.GetSqlInnerString(lstIds.ToArray()); //ids;
        sch.Id.Operator = Query.Operators.inner;

        Result res = svc.GetAllWithExtension(xpm, sch);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            throw new ResultException(res);
        }

        List<DataRow> addRows = new List<DataRow>();
        foreach(DataRow rowSzerelt in res.Ds.Tables[0].Rows)
        {
            if (rowSzerelt["IrattariJel"].ToString() != irattariJel)
            {
                dataTableDest.ImportRow(rowSzerelt);
            }
        }
    }

    [WebMethod()]
    public Result CheckElokeszitettSzerelesek(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_UgyUgyiratok_Id_Array == null || erec_UgyUgyiratok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52700);
            }
            else
            {
                // hozz�vessz�k a szerelt �s csatolt �gyiratokat, mert ezek egy�tt mozognak majd
                erec_UgyUgyiratok_Id_Array = AddSzereltUgyiratokToArray(execParam, erec_UgyUgyiratok_Id_Array);
                erec_UgyUgyiratok_Id_Array = AddCsatoltUgyiratokToArray(execParam, erec_UgyUgyiratok_Id_Array);

                EREC_UgyUgyiratokService svcUgyiratok = new EREC_UgyUgyiratokService(this.dataContext);

                ExecParam xpm = execParam.Clone();

                DataTable dataTableDest = null;

                // t�meges ellen�rz�s
                Result resByNode = svcUgyiratok.GetAllElokeszitettSzereltByNode(xpm, erec_UgyUgyiratok_Id_Array, true);

                if (resByNode.IsError)
                {
                    throw new ResultException(resByNode);
                }

                dataTableDest = CheckElokeszitettSzereltUgyiratok(xpm, resByNode.Ds.Tables[0], dataTableDest, erec_UgyUgyiratok_Id_Array);


                result.Ds = new DataSet();
                if (dataTableDest != null)
                {
                    result.Ds.Tables.Add(dataTableDest);
                }
                else
                {
                    // csak egy �res t�bl�t adunk hozz�
                    result.Ds.Tables.Add(new DataTable());
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    private DataTable CheckElokeszitettSzereltUgyiratok(ExecParam execParam, DataTable dataTableSource, DataTable dataTableDest, String[] erec_UgyUgyiratok_Id_Array)
    {
        DataTable dataTableResult = dataTableDest;

        if (dataTableSource.Rows.Count == 0)
        {
            return dataTableDest;
        }

        ExecParam xpm = execParam.Clone();
        xpm.Record_Id = String.Empty;

        List<string> lstIds = new List<string>(dataTableSource.Rows.Count);
        foreach (DataRow rowElokeszitettSzerelt in dataTableSource.Rows)
        {
            lstIds.Add(rowElokeszitettSzerelt["Id"].ToString());
        }

        EREC_UgyUgyiratokService svc = new EREC_UgyUgyiratokService(this.dataContext);
        EREC_UgyUgyiratokSearch sch = new EREC_UgyUgyiratokSearch();
        sch.Id.Value = Search.GetSqlInnerString(lstIds.ToArray());
        sch.Id.Operator = Query.Operators.inner;

        Result res = svc.GetAllWithExtension(xpm, sch);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        // m�g nem volt szerkezete
        if (dataTableResult == null || dataTableResult.Columns.Count == 0)
        {
            dataTableResult = res.Ds.Tables[0].Clone();
        }

        foreach (DataRow rowElokeszitettSzerelt in res.Ds.Tables[0].Rows)
        {
            if ((Array.Exists(erec_UgyUgyiratok_Id_Array
                , delegate (string item)
                    {
                        return item.Equals(rowElokeszitettSzerelt["Id"].ToString(), StringComparison.InvariantCultureIgnoreCase);
                    }
                ) == false)
                || (Array.Exists(erec_UgyUgyiratok_Id_Array
                    , delegate(string item)
                    {
                        return item.Equals(rowElokeszitettSzerelt["UgyUgyirat_Id_Szulo"].ToString(), StringComparison.InvariantCultureIgnoreCase);
                    }
                ) == false)
                )
            {
                dataTableResult.ImportRow(rowElokeszitettSzerelt);
            }
        }

        return dataTableResult;
    }

    [WebMethod()]
    public Result BreakUpCsatolasok(ExecParam execParam, String[] erec_UgyUgyirartKapcsolatok_Id_Array)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_UgyUgyirartKapcsolatok_Id_Array == null || erec_UgyUgyirartKapcsolatok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52700);
            }
            else
            {

                for (int i = 0; i < erec_UgyUgyirartKapcsolatok_Id_Array.Length; i++)
                {
                    String ugyirat_Id = erec_UgyUgyirartKapcsolatok_Id_Array[i];

                    // �gyirat lek�r�se:
                    EREC_UgyiratKapcsolatokService svcUgyiratkapcsolatok = new EREC_UgyiratKapcsolatokService(this.dataContext);
                    ExecParam execParam_UgyiratGet = execParam.Clone();
                    execParam_UgyiratGet.Record_Id = ugyirat_Id;

                    Result result_invalidate = svcUgyiratkapcsolatok.Invalidate(execParam_UgyiratGet);
                    if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_invalidate);
                    }
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    [WebMethod()]
    public Result BreakUpSzerelesek(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_UgyUgyiratok_Id_Array == null || erec_UgyUgyiratok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52700);
            }
            else
            {

                //for (int i = 0; i < erec_UgyUgyiratok_Id_Array.Length; i++)
                foreach(String ugyirat_Id in erec_UgyUgyiratok_Id_Array)
                {
                    //String ugyirat_Id = erec_UgyUgyiratok_Id_Array[i];

                    // �gyirat lek�r�se:
                    EREC_UgyUgyiratokService svc = new EREC_UgyUgyiratokService(this.dataContext);
                    ExecParam execParam_UgyiratGet = execParam.Clone();
                    execParam_UgyiratGet.Record_Id = ugyirat_Id;

                    Result result_get = svc.Get(execParam_UgyiratGet);
                    if (!String.IsNullOrEmpty(result_get.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_get);
                    }

                    EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)result_get.Record;
                    ugyirat.Updated.SetValueAll(false);
                    ugyirat.Base.Updated.SetValueAll(false);
                    ugyirat.Base.Updated.Ver = true;

                    ugyirat.UgyUgyirat_Id_Szulo = String.Empty;
                    ugyirat.Updated.UgyUgyirat_Id_Szulo = true;

                    Result result_update = svc.Update(execParam_UgyiratGet, ugyirat);

                    if (!String.IsNullOrEmpty(result_update.ErrorCode))
                    {
                        throw new ResultException(result_update);
                    }
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    [WebMethod()]
    public Result BreakUpElokeszitettSzerelesek(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_UgyUgyiratok_Id_Array == null || erec_UgyUgyiratok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52700);
            }
            else
            {
                // t�meges update
                // �gyiratok lek�r�se:
                EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);
                ExecParam execParam_UgyiratGetAll = execParam.Clone();

                EREC_UgyUgyiratokSearch search_ugyiratok = new EREC_UgyUgyiratokSearch();
                search_ugyiratok.Id.Value = Search.GetSqlInnerString(erec_UgyUgyiratok_Id_Array);
                search_ugyiratok.Id.Operator = Query.Operators.inner;

                Result result_getAll = service_ugyiratok.GetAll(execParam_UgyiratGetAll, search_ugyiratok);
                if (result_getAll.IsError)
                {
                    // hiba:
                    throw new ResultException(result_getAll);
                }

                if (result_getAll.Ds.Tables[0].Rows.Count > 0)
                {
                    List<string> lstIds = new List<string>(result_getAll.Ds.Tables[0].Rows.Count);
                    List<string> lstVers = new List<string>(result_getAll.Ds.Tables[0].Rows.Count);

                    foreach (DataRow row in result_getAll.Ds.Tables[0].Rows)
                    {
                        lstIds.Add(row["Id"].ToString());
                        lstVers.Add(row["Ver"].ToString());
                    }

                    EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
                    ugyirat.Updated.SetValueAll(false);
                    ugyirat.Base.Updated.SetValueAll(false);
                    //ugyirat.Base.Updated.Ver = true;

                    ugyirat.UgyUgyirat_Id_Szulo = String.Empty;
                    ugyirat.Updated.UgyUgyirat_Id_Szulo = true;

                    Result result_updateTomeges = service_ugyiratok.Update_Tomeges(execParam.Clone()
                        , result_getAll.Ds.Tables[0], lstIds.ToArray(), lstVers.ToArray()
                        , ugyirat, DateTime.Now);

                    if (result_updateTomeges.IsError)
                    {
                        throw new ResultException(result_updateTomeges);
                    }
                }
                else
                {
                    // TODO: hiba?
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(List<string>))]
    public Result JegyzekreHelyezes_Peldanyok(ExecParam execParam, String[] erec_PldIratPeldanyok_Id_Array
    , String jegyzekId, string megjegyzes)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_PldIratPeldanyok_Id_Array == null || erec_PldIratPeldanyok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52700);
            }

            // jegyz�k lek�r�se: Egy�b szervezetnek �tad�sn�l el kell menteni az el�z� �llapotot
            EREC_IraJegyzekekService service_jegyzekek = new EREC_IraJegyzekekService();
            ExecParam execParam_jegyzekekGet = execParam.Clone();
            execParam_jegyzekekGet.Record_Id = jegyzekId;

            Result result_jegyzekekGet = service_jegyzekek.Get(execParam_jegyzekekGet);

            if (result_jegyzekekGet.IsError)
            {
                throw new ResultException(result_jegyzekekGet);
            }

            EREC_IraJegyzekek erec_IraJegyzekek = (EREC_IraJegyzekek)result_jegyzekekGet.Record;

            Result result_update = new Result();

            List<string> jegyzektetelIds = new List<string>(erec_PldIratPeldanyok_Id_Array.Length);

            if (erec_PldIratPeldanyok_Id_Array.Length > 0)
            {
                Result result_peldanyokGetAll = SetJegyzekreHelyezesAllapot_Peldanyok(execParam, erec_PldIratPeldanyok_Id_Array, erec_IraJegyzekek);

                if (result_peldanyokGetAll.IsError)
                {
                    throw new ResultException(result_peldanyokGetAll);
                }

                foreach (DataRow row in result_peldanyokGetAll.Ds.Tables[0].Rows)
                {
                    //irajegyz�kt�telek insert�l�sa
                    EREC_IraJegyzekTetelek jegyzekTetel = new EREC_IraJegyzekTetelek();
                    jegyzekTetel.Jegyzek_Id = jegyzekId;
                    jegyzekTetel.Obj_Id = row["Id"].ToString();
                    jegyzekTetel.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok;
                    jegyzekTetel.Megjegyzes = megjegyzes;

                    string azonosito = row["Azonosito"].ToString();

                    jegyzekTetel.Azonosito = azonosito;

                    Result result_insert = this.Insert(execParam, jegyzekTetel);

                    if (result_insert.IsError)
                    {
                        throw new ResultException(result_insert);
                    }


                    // esem�nynapl�hoz
                    jegyzektetelIds.Add(result_insert.Uid);

                }

                result.Record = jegyzektetelIds;

            }


            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                string ids = Search.GetSqlInnerString(jegyzektetelIds.ToArray());

                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ids, "JegyzekTetelNew");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_IraJegyzekTetelek::JegyzekTetelNew: ", execParam, eventLogResult);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    public Result SetJegyzekreHelyezesAllapot_Ugyiratok(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array
    , EREC_IraJegyzekek jegyzek)
    {
        erec_UgyUgyiratok_Id_Array = AddCsatoltUgyiratokToArray(execParam, erec_UgyUgyiratok_Id_Array);

        erec_UgyUgyiratok_Id_Array = AddSzereltUgyiratokToArray(execParam, erec_UgyUgyiratok_Id_Array);

        if (erec_UgyUgyiratok_Id_Array.Length > 0)
        {
            // �gyiratok lek�r�se:
            ExecParam execParam_ugyiratok = execParam.Clone();
            EREC_UgyUgyiratokSearch search_ugyiratok = new EREC_UgyUgyiratokSearch();
            search_ugyiratok.Id.Value = Search.GetSqlInnerString(erec_UgyUgyiratok_Id_Array);
            search_ugyiratok.Id.Operator = Query.Operators.inner;

            EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);

            Result result_ugyiratokGetAll = service_ugyiratok.GetAllWithExtension(execParam_ugyiratok, search_ugyiratok); // t�meges lek�r�s

            if (result_ugyiratokGetAll.IsError)
            {
                throw new ResultException(result_ugyiratokGetAll);
            }

            Result result_update = new Result();
            foreach (DataRow row in result_ugyiratokGetAll.Ds.Tables[0].Rows)
            {
                EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
                Utility.LoadBusinessDocumentFromDataRow(erec_UgyUgyirat, row);

                //�gyirat �llapot�nak
                if (erec_UgyUgyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt)
                {
                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    if (jegyzek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value)
                    {
                        // Egy�b szervezetnek �tad�sn�l az el�z� �llapotot kell tudni vissza�ll�tani: elmentj�k
                        erec_UgyUgyirat.TovabbitasAlattAllapot = erec_UgyUgyirat.Allapot;
                        erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                    }

                    erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett;
                    erec_UgyUgyirat.Updated.Allapot = true;

                    execParam_ugyiratok.Record_Id = erec_UgyUgyirat.Id;

                    result_update = service_ugyiratok.Update(execParam_ugyiratok, erec_UgyUgyirat);

                    // ha hiba volt az egyikn�l, nem megy�nk tov�bb:
                    if (result_update.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_update);
                    }
                }
            }

            return result_ugyiratokGetAll;
        }

        return new Result();
    }

    public void SetJegyzekreHelyezesAllapot_Iratok(ExecParam execParam, String[] erec_IraIratok_Id_Array
    , EREC_IraJegyzekek jegyzek)
    {

        if (erec_IraIratok_Id_Array.Length > 0)
        {
            // p�ld�nyok lek�r�se:
            ExecParam execParam_iratok = execParam.Clone();
            EREC_IraIratokSearch search_iratok = new EREC_IraIratokSearch();
            search_iratok.Id.Value = Search.GetSqlInnerString(erec_IraIratok_Id_Array);
            search_iratok.Id.Operator = Query.Operators.inner;

            EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);

            Result result_iratokGetAll = service_iratok.GetAll(execParam_iratok, search_iratok); // t�meges lek�r�s

            if (result_iratokGetAll.IsError)
            {
                throw new ResultException(result_iratokGetAll);
            }

            Result result_update = new Result();
            List<string> ugyiratIdList = new List<string>();
            foreach (DataRow row in result_iratokGetAll.Ds.Tables[0].Rows)
            {
                string ugyiratId = row["Ugyirat_Id"].ToString();

                if (!ugyiratIdList.Contains(ugyiratId))
                    ugyiratIdList.Add(ugyiratId);

                EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
                Utility.LoadBusinessDocumentFromDataRow(erec_IraIratok, row);


                erec_IraIratok.Updated.SetValueAll(false);
                erec_IraIratok.Base.Updated.SetValueAll(false);
                erec_IraIratok.Base.Updated.Ver = true;

                erec_IraIratok.Allapot = KodTarak.IRAT_ALLAPOT.JegyzekreHelyezett;
                erec_IraIratok.Updated.Allapot = true;

                execParam_iratok.Record_Id = erec_IraIratok.Id;

                result_update = service_iratok.Update(execParam_iratok, erec_IraIratok);

                // ha hiba volt az egyikn�l, nem megy�nk tov�bb:
                if (result_update.IsError)
                {
                    // hiba:
                    throw new ResultException(result_update);
                }
            }

            //�gyiratok lez�r�sa
            search_iratok = new EREC_IraIratokSearch();
            search_iratok.Ugyirat_Id.Value = Search.GetSqlInnerString(ugyiratIdList.ToArray());
            search_iratok.Ugyirat_Id.Operator = Query.Operators.inner;

            result_iratokGetAll = service_iratok.GetAll(execParam_iratok, search_iratok);

            if (result_iratokGetAll.IsError)
            {
                throw new ResultException(result_iratokGetAll);
            }


            DataTable tbIratok = result_iratokGetAll.Ds.Tables[0];
            List<string> lezarandoUgyiratIdList = new List<string>();

            foreach (string ugyIratId in ugyiratIdList)
            {
                bool ugyiratLezarando = true;

                foreach (DataRow row in tbIratok.Rows)
                {
                    string allapot = row["Allapot"].ToString();
                    string iratUgyiratId = row["Ugyirat_Id"].ToString();

                    if (ugyIratId == iratUgyiratId)
                    {
                        if (allapot != KodTarak.IRAT_ALLAPOT.JegyzekreHelyezett)
                        {
                            ugyiratLezarando = false;
                            break;
                        }
                    }
                }

                if (ugyiratLezarando)
                {
                    lezarandoUgyiratIdList.Add(ugyIratId);
                }
            }

            if (lezarandoUgyiratIdList.Count > 0)
            {
                SetJegyzekreHelyezesAllapot_Ugyiratok(execParam, lezarandoUgyiratIdList.ToArray(), jegyzek);
            }
        }

    }

    public Result SetJegyzekreHelyezesAllapot_Peldanyok(ExecParam execParam, String[] erec_PldIratPeldanyok_Id_Array
    , EREC_IraJegyzekek jegyzek)
    {
        Result result = new Result();
        if (erec_PldIratPeldanyok_Id_Array.Length > 0)
        {
            // p�ld�nyok lek�r�se:
            ExecParam execParam_peldanyok = execParam.Clone();
            EREC_PldIratPeldanyokSearch search_peldanyok = new EREC_PldIratPeldanyokSearch();
            search_peldanyok.Id.Value = Search.GetSqlInnerString(erec_PldIratPeldanyok_Id_Array);
            search_peldanyok.Id.Operator = Query.Operators.inner;

            EREC_PldIratPeldanyokService service_peldanyok = new EREC_PldIratPeldanyokService(this.dataContext);

            Result result_peldanyokGetAll = service_peldanyok.GetAll(execParam_peldanyok, search_peldanyok); // t�meges lek�r�s

            if (result_peldanyokGetAll.IsError)
            {
                throw new ResultException(result_peldanyokGetAll);
            }
            result = result_peldanyokGetAll;
            Result result_update = new Result();
            List<string> iratIdList = new List<string>();
            foreach (DataRow row in result_peldanyokGetAll.Ds.Tables[0].Rows)
            {
                string iratId = row["IraIrat_Id"].ToString();

                if (!iratIdList.Contains(iratId))
                    iratIdList.Add(iratId);

                EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
                Utility.LoadBusinessDocumentFromDataRow(erec_PldIratPeldanyok, row);


                erec_PldIratPeldanyok.Updated.SetValueAll(false);
                erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);
                erec_PldIratPeldanyok.Base.Updated.Ver = true;

                // Egy�b szervezetnek �tad�sn�l az el�z� �llapotot kell tudni vissza�ll�tani: elmentj�k
                erec_PldIratPeldanyok.TovabbitasAlattAllapot = erec_PldIratPeldanyok.Allapot;
                erec_PldIratPeldanyok.Updated.TovabbitasAlattAllapot = true;

                erec_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Jegyzekre_helyezett;
                erec_PldIratPeldanyok.Updated.Allapot = true;

                execParam_peldanyok.Record_Id = erec_PldIratPeldanyok.Id;

                result_update = service_peldanyok.Update(execParam_peldanyok, erec_PldIratPeldanyok);

                // ha hiba volt az egyikn�l, nem megy�nk tov�bb:
                if (result_update.IsError)
                {
                    // hiba:
                    throw new ResultException(result_update);
                }
            }

            //iratok jegyz�kre helyez�se
            search_peldanyok = new EREC_PldIratPeldanyokSearch();
            search_peldanyok.IraIrat_Id.Value = Search.GetSqlInnerString(iratIdList.ToArray());
            search_peldanyok.IraIrat_Id.Operator = Query.Operators.inner;

            result_peldanyokGetAll = service_peldanyok.GetAll(execParam_peldanyok, search_peldanyok);

            if (result_peldanyokGetAll.IsError)
            {
                throw new ResultException(result_peldanyokGetAll);
            }

            DataTable tbPeldanyok = result_peldanyokGetAll.Ds.Tables[0];
            List<string> lezarandoIratIdList = new List<string>();

            foreach (string iratId in iratIdList)
            {
                bool iratLezarando = true;

                foreach (DataRow row in tbPeldanyok.Rows)
                {
                    string allapot = row["Allapot"].ToString();
                    string peldanyIratId = row["IraIrat_Id"].ToString();

                    if (iratId == peldanyIratId)
                    {
                        if (allapot != KodTarak.IRATPELDANY_ALLAPOT.Jegyzekre_helyezett)
                        {
                            iratLezarando = false;
                            break;
                        }
                    }
                }

                if (iratLezarando)
                {
                    lezarandoIratIdList.Add(iratId);
                }
            }

            if (lezarandoIratIdList.Count > 0)
            {
                SetJegyzekreHelyezesAllapot_Iratok(execParam, lezarandoIratIdList.ToArray(), jegyzek);
            }
        }

        return result;
    }

    #endregion

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraJegyzekTetelek))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result result_get = this.Get(ExecParam);
            if (!String.IsNullOrEmpty(result_get.ErrorCode))
            {
                throw new ResultException(result_get);
            }

            EREC_IraJegyzekTetelek jegyzekTetel = (EREC_IraJegyzekTetelek)result_get.Record;

            #region Jegyz�k lek�r�se: t�pus vizsg�lata miatt
            EREC_IraJegyzekekService svcJegyzek = new EREC_IraJegyzekekService(this.dataContext);

            ExecParam execParam_JegyzekGet = ExecParam.Clone();
            execParam_JegyzekGet.Record_Id = jegyzekTetel.Jegyzek_Id;

            Result resJegyzek = svcJegyzek.Get(execParam_JegyzekGet);

            if (resJegyzek.IsError)
            {
                throw new ResultException(resJegyzek);
            }

            EREC_IraJegyzekek erec_IraJegyzekek = (EREC_IraJegyzekek)resJegyzek.Record;

            #endregion Jegyz�k lek�r�se: t�pus vizsg�lata miatt

            List<string> jegyzektetelIds = new List<string>();

            if (jegyzekTetel.Obj_type == Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok)
            {
                string peldanyId = jegyzekTetel.Obj_Id;

                EREC_PldIratPeldanyokService svcPeldany = new EREC_PldIratPeldanyokService(this.dataContext);
                ExecParam xpmPeldany = ExecParam.Clone();
                xpmPeldany.Record_Id = peldanyId;

                Result resPeldany = svcPeldany.Get(xpmPeldany);
                if (resPeldany.IsError)
                {
                    throw new ResultException(resPeldany);
                }

                EREC_PldIratPeldanyok peldany = resPeldany.Record as EREC_PldIratPeldanyok;
                peldany.Updated.SetValueAll(false);
                peldany.Base.Updated.SetValueAll(false);
                peldany.Base.Updated.Ver = true;

                if (erec_IraJegyzekek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value)
                {

                        peldany.Allapot = String.IsNullOrEmpty(peldany.TovabbitasAlattAllapot) ? KodTarak.IRATPELDANY_ALLAPOT.Iktatott : peldany.TovabbitasAlattAllapot;
                        peldany.Updated.Allapot = true;

                        peldany.TovabbitasAlattAllapot = peldany.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt ? KodTarak.IRATPELDANY_ALLAPOT.Iktatott : Contentum.eUtility.Constants.BusinessDocument.nullString;
                        peldany.Updated.TovabbitasAlattAllapot = true;
                }
                else
                {
                        peldany.Allapot = Contentum.eUtility.KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                        peldany.Updated.Allapot = true;
                }

                Result respeldanyUp = svcPeldany.Update(xpmPeldany, peldany);

                if (!String.IsNullOrEmpty(respeldanyUp.ErrorCode))
                {
                    throw new ResultException(respeldanyUp);
                }

                result = sp.Invalidate(ExecParam);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }
            else
            {

                #region �gyirat

                string ugyiratId = jegyzekTetel.Obj_Id;

                EREC_UgyUgyiratokService svcUgyirat = new EREC_UgyUgyiratokService(this.dataContext);
                ExecParam xpmUgyirat = ExecParam.Clone();
                xpmUgyirat.Record_Id = ugyiratId;

                Result resUgyirat = svcUgyirat.Get(xpmUgyirat);
                if (resUgyirat.IsError)
                {
                    throw new ResultException(resUgyirat);
                }

                #region szerelt �gyiratok lek�r�se, update-el�se
                Result result_szuloHierarchy = svcUgyirat.GetAllSzereltByParent(xpmUgyirat);

                if (result_szuloHierarchy.IsError)
                {
                    throw new ResultException(result_szuloHierarchy);
                }

                List<string> ugyiratList = new List<string>();
                ugyiratList.Add(xpmUgyirat.Record_Id);

                foreach (DataRow row in result_szuloHierarchy.Ds.Tables[0].Rows)
                {
                    ugyiratList.Add(row["Id"].ToString());
                }


                EREC_UgyUgyiratok ugyirat = resUgyirat.Record as EREC_UgyUgyiratok;
                ugyirat.Updated.SetValueAll(false);
                ugyirat.Base.Updated.SetValueAll(false);
                ugyirat.Base.Updated.Ver = true;

                if (erec_IraJegyzekek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value)
                {
                    //�gyirat mez�inek vissza�ll�t�sa
                    if (ugyirat.Allapot == Contentum.eUtility.KodTarak.UGYIRAT_ALLAPOT.Szerelt)
                    {

                        //if (result_szuloHierarchy.IsError)
                        //{
                        //    throw new ResultException(result_szuloHierarchy);
                        //}

                        //string allapot_szulo = Contentum.eUtility.KodTarak.UGYIRAT_ALLAPOT.Iktatott; // default;
                        //if (result_szuloHierarchy.Ds.Tables[0].Rows.Count > 0)
                        //{
                        //    allapot_szulo = result_szuloHierarchy.Ds.Tables[0].Rows[result_szuloHierarchy.Ds.Tables[0].Rows.Count - 1]["Allapot"].ToString();
                        //}

                        //// TODO: milyen �llapot?
                        //ugyirat.TovabbitasAlattAllapot = allapot_szulo;
                        //ugyirat.Updated.TovabbitasAlattAllapot = true;
                    }
                    else
                    {
                        // TODO: mi legyen az alap�rtelmezet �llapot?
                        ugyirat.Allapot = String.IsNullOrEmpty(ugyirat.TovabbitasAlattAllapot) ? KodTarak.UGYIRAT_ALLAPOT.Iktatott : ugyirat.TovabbitasAlattAllapot;
                        ugyirat.Updated.Allapot = true;

                        // TODO: mi legyen az alap�rtelmezet �llapot?
                        // ha tov�bb�t�s alatt volt, akkor iktatott
                        ugyirat.TovabbitasAlattAllapot = ugyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt ? KodTarak.UGYIRAT_ALLAPOT.Iktatott : Contentum.eUtility.Constants.BusinessDocument.nullString;
                        ugyirat.Updated.TovabbitasAlattAllapot = true;
                    }
                }
                else
                {
                    //�gyirat mez�inek vissza�ll�t�sa
                    // egyel�re a legfels� szintr�l �ll�tjuk be
                    // TODO: teljes �gyirat szerel�si hierarchia t�rl�se a jegyz�kr�l, ha valamelyik eleme t�rl�sre ker�l
                    if (ugyirat.Allapot == Contentum.eUtility.KodTarak.UGYIRAT_ALLAPOT.Szerelt)
                    {
                        //bool isParentStateUnknown = true;

                        //if (!String.IsNullOrEmpty(ugyirat.UgyUgyirat_Id_Szulo))
                        //{
                        //    ExecParam execParam_szuloGet = ExecParam.Clone();
                        //    execParam_szuloGet.Record_Id = ugyirat.UgyUgyirat_Id_Szulo;
                        //    Result result_szuloGet = svcUgyirat.Get(execParam_szuloGet);

                        //    if (result_szuloGet.IsError)
                        //    {
                        //        throw new ResultException(result_szuloGet);
                        //    }

                        //    EREC_UgyUgyiratok erec_UgyUgyiratSzulo = result_szuloGet.Record as EREC_UgyUgyiratok;

                        //    if (erec_UgyUgyiratSzulo != null && !String.IsNullOrEmpty(erec_UgyUgyiratSzulo.Allapot))
                        //    {
                        //        ugyirat.TovabbitasAlattAllapot = erec_UgyUgyiratSzulo.Allapot;
                        //        ugyirat.Updated.TovabbitasAlattAllapot = true;

                        //        isParentStateUnknown = false;
                        //    }
                        //}

                        //if (isParentStateUnknown)
                        //{
                        // default be�ll�t�sa
                        //ugyirat.TovabbitasAlattAllapot = Contentum.eUtility.KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott;
                        //ugyirat.Updated.TovabbitasAlattAllapot = true;
                        //}
                    }
                    else
                    {
                        ugyirat.Allapot = Contentum.eUtility.KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott;
                        ugyirat.Updated.Allapot = true;
                    }
                }

                ugyirat.FelhCsoport_Id_Selejtezo = String.Empty;
                ugyirat.Updated.FelhCsoport_Id_Selejtezo = true;

                ugyirat.SelejtezesDat = Contentum.eUtility.Constants.BusinessDocument.nullString;
                ugyirat.Updated.SelejtezesDat = true;

                ugyirat.LeveltariAtvevoNeve = String.Empty;
                ugyirat.Updated.LeveltariAtvevoNeve = true;

                ugyirat.FelhCsoport_Id_Felulvizsgalo = String.Empty;
                ugyirat.Updated.FelhCsoport_Id_Felulvizsgalo = true;

                ugyirat.FelulvizsgalatDat = Contentum.eUtility.Constants.BusinessDocument.nullString;
                ugyirat.Updated.FelulvizsgalatDat = true;

                xpmUgyirat.Record_Id = ugyirat.Id;
                Result resUgyiratUp = svcUgyirat.Update(xpmUgyirat, ugyirat);

                if (!String.IsNullOrEmpty(resUgyiratUp.ErrorCode))
                {
                    throw new ResultException(resUgyiratUp);
                }

                #endregion

                #region Szerelt �gyiratok jegyz�kt�teleinek lek�r�se
                EREC_IraJegyzekTetelekSearch search = new EREC_IraJegyzekTetelekSearch();
                search.Jegyzek_Id.Value = erec_IraJegyzekek.Id;
                search.Jegyzek_Id.Operator = Query.Operators.equals;

                search.Obj_Id.Value = Search.GetSqlInnerString(ugyiratList.ToArray());
                search.Obj_Id.Operator = Query.Operators.inner;

                Result result_jegyzektetelekGetAll = this.GetAll(ExecParam.Clone(), search);
                if (result_jegyzektetelekGetAll.IsError)
                {
                    throw new ResultException(result_jegyzektetelekGetAll);
                }

                #endregion Szerelt �gyiratok jegyz�kt�teleinek lek�r�se

                ExecParam xpmInvalidate = ExecParam.Clone();
                
                foreach (DataRow row in result_jegyzektetelekGetAll.Ds.Tables[0].Rows)
                {
                    xpmInvalidate.Record_Id = row["Id"].ToString();
                    //esem�nynapl�hoz
                    jegyzektetelIds.Add(xpmInvalidate.Record_Id);

                    jegyzekTetel.Base.Updated.SetValueAll(false);
                    jegyzekTetel.Updated.SetValueAll(false);
                    jegyzekTetel.Base.Updated.Ver = true;

                    Result resInvalidate = sp.Invalidate(xpmInvalidate); ;

                    if (resInvalidate.IsError)
                    {
                        throw new ResultException(resInvalidate);
                    }

                    if (jegyzekTetel.Id == xpmInvalidate.Record_Id)
                    {
                        result = resInvalidate;
                    }
                }


                //result = sp.Invalidate(ExecParam);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }

            #endregion

            #region Esem�nynapl�z�s
            if (!isTransactionBeginHere)
            {
                jegyzektetelIds.Remove(ExecParam.Record_Id);
            }
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            string ids = Search.GetSqlInnerString(jegyzektetelIds.ToArray());

            Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(ExecParam, ids, "JegyzekTetelInvalidate");
            if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                Logger.Debug("[ERROR]EREC_IraJegyzekTetelek::JegyzekTetelInvalidate: ", ExecParam, eventLogResult);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}