using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;
using System.Collections.Generic;

using Contentum.eDocument;
using System.Xml.Linq;
using System.IO;
using System.Linq;
using Contentum.eDocument.Service;
using System.Net;
using ICSharpCode.SharpZipLib.Zip;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IraJegyzekekService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraJegyzekekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraJegyzekekSearch _EREC_IraJegyzekekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_IraJegyzekekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region fel�lvizsg�lat
    /// <summary>
    /// Jegyz�k lez�r�sa v�grehajt�s (�tad�s) el�tt
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result JegyzekLezarasa(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            Result result_get = this.Get(execParam);

            EREC_IraJegyzekek jegyzek = (EREC_IraJegyzekek)result_get.Record;


            jegyzek.Updated.SetValueAll(false);
            jegyzek.Base.Updated.SetValueAll(false);
            jegyzek.Base.Updated.Ver = true;

            List<string> ugyiratIdList;
            List<string> peldanyIdList;
            GetJegyzekTetelek(execParam, jegyzek.Id, out ugyiratIdList, out peldanyIdList);

            if (ugyiratIdList.Count > 0)
            {
                JegyzekUgyiratokLezarasa(execParam, ugyiratIdList);
            }

            if (peldanyIdList.Count > 0)
            {
                JegyzekPeldanyokLezarasa(execParam, peldanyIdList);
            }

            jegyzek.LezarasDatuma = DateTime.Now.ToString();
            jegyzek.Updated.LezarasDatuma = true;

            jegyzek.Allapot = KodTarak.JEGYZEK_ALLAPOT.Lezart;
            jegyzek.Updated.Allapot = true;

            Result result_update = this.Update(execParam, jegyzek);
            result = result_update;


            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_IraJegyzekek", "JegyzekLezaras").Record;
                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// Jegyz�k v�grehajt�sa (�tad�s)
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result JegyzekVegrehajtasa(ExecParam execParam, string LeveltariAtvevo, string megsemmisitesiNaploId, bool csatolmanyokTorleseAszinkron)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            Result result_get = this.Get(execParam);

            EREC_IraJegyzekek jegyzek = (EREC_IraJegyzekek)result_get.Record;


            jegyzek.Updated.SetValueAll(false);
            jegyzek.Base.Updated.SetValueAll(false);
            jegyzek.Base.Updated.Ver = true;

            List<string> ugyiratIdList;
            List<string> peldanyIdList;
            GetJegyzekTetelek(execParam, jegyzek.Id, out ugyiratIdList, out peldanyIdList);

            if (ugyiratIdList.Count > 0)
            {
                JegyzekUgyiratokVegrehajtasa(execParam, ugyiratIdList, jegyzek, LeveltariAtvevo, csatolmanyokTorleseAszinkron);
            }

            if (peldanyIdList.Count > 0)
            {
                JegyzekPeldanyokVegrehajtasa(execParam, peldanyIdList, jegyzek, LeveltariAtvevo, csatolmanyokTorleseAszinkron);
            }

            if (!String.IsNullOrEmpty(LeveltariAtvevo))
            {
                jegyzek.Base.Note = LeveltariAtvevo;
                jegyzek.Base.Updated.Note = true;
            }
            jegyzek.FelhasznaloCsoport_Id_Vegrehaj = execParam.Felhasznalo_Id;
            jegyzek.Updated.FelhasznaloCsoport_Id_Vegrehaj = true;
            jegyzek.VegrehajtasDatuma = DateTime.Now.ToString();
            jegyzek.Updated.VegrehajtasDatuma = true;

            if (!String.IsNullOrEmpty(megsemmisitesiNaploId) && Rendszerparameterek.IsTUK(execParam))
            {
                SetJegyzekIktatoszam(execParam, jegyzek, megsemmisitesiNaploId);
            }

            jegyzek.Allapot = csatolmanyokTorleseAszinkron ? KodTarak.JEGYZEK_ALLAPOT.VegrehajtasFolyamatban : KodTarak.JEGYZEK_ALLAPOT.Vegrahajtott;
            jegyzek.Updated.Allapot = true;

            if (csatolmanyokTorleseAszinkron)
            {
                jegyzek.VegrehajtasKezdoDatuma = DateTime.Now.ToString();
                jegyzek.Updated.VegrehajtasKezdoDatuma = true;
            }


            Result result_update = this.Update(execParam, jegyzek);
            result = result_update;

            // Kiv�telesen kett�s napl�z�s: nem csak a jegyz�khez, hanem az �gyiratokhoz is k�tj�k az esem�nybejegyz�st!
            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_IraJegyzekek", "JegyzekVegrehajtas").Record;
                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);

                if (ugyiratIdList.Count > 0)
                {
                    //}

                    //if (isTransactionBeginHere)
                    //{
                    //KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                    string ids = Search.GetSqlInnerString(ugyiratIdList.ToArray());

                    //Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ids, "UgyiratJegyzekVegrehajtas");
                    eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ids, "UgyiratJegyzekVegrehajtas");
                    if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                        Logger.Debug("[ERROR]EREC_UgyUgyiratok::UgyiratJegyzekVegrehajtas: ", execParam, eventLogResult);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    private Result DeleteCsatolmanyok(ExecParam xpm)
    {
        string ugyiratId = xpm.Record_Id;
        EREC_IraIratokSearch sch = new EREC_IraIratokSearch(true);
        sch.Ugyirat_Id.Value = ugyiratId;
        sch.Ugyirat_Id.Operator = Query.Operators.equals;
        EREC_IraIratokService svc = new EREC_IraIratokService(this.dataContext);
        Result res = svc.GetAllWithExtension(xpm, sch);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            throw new ResultException(res);
        }

        foreach (DataRow row in res.Ds.Tables[0].Rows)
        {
            string iratId = row["Id"].ToString();
            ExecParam xpm1 = xpm.Clone();
            EREC_CsatolmanyokSearch sch1 = new EREC_CsatolmanyokSearch();
            sch1.IraIrat_Id.Value = iratId;
            sch1.IraIrat_Id.Operator = Query.Operators.equals;
            EREC_CsatolmanyokService svc1 = new EREC_CsatolmanyokService(this.dataContext);
            Result res1 = svc1.GetAll(xpm1, sch1);

            if (!String.IsNullOrEmpty(res1.ErrorCode))
            {
                throw new ResultException(res1);
            }

            foreach (DataRow row1 in res1.Ds.Tables[0].Rows)
            {
                ExecParam xpm2 = xpm.Clone();
                xpm2.Record_Id = row1["Id"].ToString();
                Result res2 = svc1.Invalidate(xpm2);

                if (!String.IsNullOrEmpty(res2.ErrorCode))
                {
                    throw new ResultException(res2);
                }
            }
        }

        return res;
    }

    private Result DeleteCsatolmanyokTomeges(ExecParam xpm, List<string> ugyiratIds, string jegyzekId)
    {
        // Iratok keres�se �gyiratokhoz
        EREC_IraIratokSearch sch = new EREC_IraIratokSearch(true);
        sch.Ugyirat_Id.Value = Search.GetSqlInnerString(ugyiratIds.ToArray());
        sch.Ugyirat_Id.Operator = Query.Operators.inner;
        EREC_IraIratokService svc = new EREC_IraIratokService(this.dataContext);
        Result res = svc.GetAll(xpm, sch);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            throw new ResultException(res);
        }

        List<String> iratIds = new List<string>(res.Ds.Tables[0].Rows.Count);
        foreach (DataRow row in res.Ds.Tables[0].Rows)
        {
            string iratId = row["Id"].ToString();
            iratIds.Add(iratId);
            #region Selejtez�s - Dokumentumok �thelyez�se SPS-en
            //bernat.laszlo added

            ExecParam iratExecParam = xpm.Clone();
            iratExecParam.Record_Id = row["Id"].ToString();
            Result iratResult = svc.Get(iratExecParam);

            if (String.IsNullOrEmpty(iratResult.ErrorCode))
            {
                ExecParam execP_Selejtezes = xpm.Clone();

                Contentum.eDocument.Service.DocumentService documentService = eDocumentService.ServiceFactory.GetDocumentService();
                Result result_Selejtezes = documentService.SelejtezesIratDocumentsLocation(execP_Selejtezes, (EREC_IraIratok)iratResult.Record);
                if (result_Selejtezes.IsError)
                {
                    throw new ResultException(result_Selejtezes);
                }
            }
            else
            {
                throw new ResultException(iratResult);
            }
            #endregion
        }

        ExecParam execParam_getAll = xpm.Clone();
        EREC_CsatolmanyokSearch sch1 = new EREC_CsatolmanyokSearch();
        sch1.IraIrat_Id.Value = String.Format("(SELECT Id FROM EREC_IraIratok WHERE Ugyirat_Id IN (select Obj_id from EREC_IraJegyzekTetelek where Jegyzek_Id = '{0}' and getdate() between ErvKezd and ErvVege))", jegyzekId);
        sch1.IraIrat_Id.Operator = Query.Operators.inner;
        EREC_CsatolmanyokService svc1 = new EREC_CsatolmanyokService(this.dataContext);
        Result res1 = svc1.GetAll(execParam_getAll, sch1);

        if (!String.IsNullOrEmpty(res1.ErrorCode))
        {
            throw new ResultException(res1);
        }

        ExecParam[] execParams = new ExecParam[res1.Ds.Tables[0].Rows.Count];
        for (int i = 0; i < res1.Ds.Tables[0].Rows.Count; i++)
        {
            DataRow row = res1.Ds.Tables[0].Rows[i];
            ExecParam xpm1 = xpm.Clone();
            xpm1.Record_Id = row["Id"].ToString();

            execParams[i] = xpm1;

            /* #region Selejtez�s - Dokumentumok �thelyez�se SPS-en
             //bernat.laszlo
             ExecParam execParam_uploadFromShare = xpm.Clone();
            

             Contentum.eAdmin.Service.KRT_DokumentumokService dokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_DokumentumokService();
             ExecParam dokExecParam = xpm.Clone();
             dokExecParam.Record_Id = row["Dokumentum_Id"].ToString();
             Result dokResult = dokService.Get(dokExecParam);
             if (String.IsNullOrEmpty(dokResult.ErrorCode))
             {
                 KRT_Dokumentumok krt_Dokumentumok = (KRT_Dokumentumok)dokResult.Record;

                 string sourcePath = krt_Dokumentumok.External_Link;
                 string fileName = krt_Dokumentumok.FajlNev;
                 string documentStoreType = krt_Dokumentumok.External_Source;
                 string doktarFolderPath = DateTime.Now.Year.ToString() + "\\SELEJTEZES\\" + krt_Dokumentumok.Id + "\\";
                 Result result_uploadFromShare = documentService.UploadFromShare(
                     execParam_uploadFromShare
                     , documentStoreType
                     , doktarSitePath
                     , doktarDocLibPath
                     , doktarFolderPath
                     , sourcePath
                     , fileName
                 );
                 if (result_uploadFromShare.IsError)
                 {
                     throw new ResultException(result_uploadFromShare);
                 }
             }
             #endregion*/


        }

        Result res_invalidate = svc1.TomegesInvalidate(xpm.Clone(), sch1);

        if (res_invalidate.IsError)
        {
            throw new ResultException(res_invalidate);
        }

        return res;
    }

    /// <summary>
    /// Jegyz�k sztorn�z�sa
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result JegyzekSztornozas(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result resGet = this.Get(execParam);

            if (resGet.IsError)
            {
                throw new ResultException(resGet);
            }

            #region Ellen�rz�s - nincs rajta t�tel
            EREC_IraJegyzekTetelekService service_jegyzektetelek = new EREC_IraJegyzekTetelekService(this.dataContext);
            EREC_IraJegyzekTetelekSearch search_jegyzektetelek = new EREC_IraJegyzekTetelekSearch();

            ExecParam execParam_jegyzektetelek = execParam.Clone();

            search_jegyzektetelek.Jegyzek_Id.Value = execParam.Record_Id;
            search_jegyzektetelek.Jegyzek_Id.Operator = Query.Operators.equals;

            search_jegyzektetelek.TopRow = 1; // csak l�tez�st vizsg�lunk

            Result result_jegyzektetelekGetAll = service_jegyzektetelek.GetAll(execParam_jegyzektetelek, search_jegyzektetelek);
            if (result_jegyzektetelekGetAll.IsError)
            {
                throw new ResultException(result_jegyzektetelekGetAll);
            }

            if (result_jegyzektetelekGetAll.Ds.Tables[0].Rows.Count > 0)
            {
                //"Hiba a jegyz�k sztorn�z�sa sor�n: A jegyz�k nem sztorn�zhat�, mert a jegyz�khez tartoznak t�telek!"
                throw new ResultException(55102);
            }
            #endregion Ellen�rz�s - nincs rajta t�tel

            EREC_IraJegyzekek jegyzek = (EREC_IraJegyzekek)resGet.Record;
            jegyzek.Base.Updated.SetValueAll(false);
            jegyzek.Updated.SetValueAll(false);
            jegyzek.Base.Updated.Ver = true;

            jegyzek.SztornirozasDat = DateTime.Now.ToString();
            jegyzek.Updated.SztornirozasDat = true;

            jegyzek.Allapot = KodTarak.JEGYZEK_ALLAPOT.Sztornozott;
            jegyzek.Updated.Allapot = true;

            result = this.Update(execParam, jegyzek);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_IraJegyzekek", "JegyzekSztornozas").Record;
                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    #endregion

    #region lev�lt�ri �tad�s

    public Result Create_MetadataXML(ExecParam ExecParam, string jegyzekId, string atadasStatusz)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.Create_MetadataXML(ExecParam, new Guid(jegyzekId), atadasStatusz);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result CreateLeveltariAdatCsomag(ExecParam ExecParam, string jegyzekId, string atadasStatusz, out string[] csomagFajlok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        csomagFajlok = null;

        try
        {
            Result resultXML = Create_MetadataXML(ExecParam, jegyzekId, atadasStatusz);

            if (resultXML.IsError)
            {
                throw new ResultException(resultXML);
            }

            string xml = resultXML.Record as string;

            XDocument doc = XDocument.Parse(xml);

            string objId = doc.Root.Attribute("OBJID").Value;

            Logger.Debug(String.Format("objId={0}", objId));

            #region  k�nyvt�rszerkezet l�trehoz�sa

            string tempPath = Server.MapPath("~/MetadataXML/Work");

            DirectoryInfo tempDirectory = Directory.CreateDirectory(Path.Combine(tempPath, objId));

            Logger.Debug(String.Format("tempDirectory={0}", tempDirectory));

            DirectoryInfo headerDirectory = tempDirectory.CreateSubdirectory("header");
            DirectoryInfo contentDirectory = tempDirectory.CreateSubdirectory("content");
            DirectoryInfo documentationDirectory = contentDirectory.CreateSubdirectory("documentation");
            DirectoryInfo dataDirectory = contentDirectory.CreateSubdirectory("data");

            #endregion

            //f�jlok let�lt�se �s elment�se
            var files = from file in doc.Descendants()
                        where file.Name.LocalName == "file"
                        select file;

            string idPrefix = "uuid-";

            foreach (var file in files)
            {
                var flocat = file.Elements().Where(e => e.Name.LocalName == "FLocat").FirstOrDefault();
                string documentId = flocat.Attribute("ID").Value;
                if(!String.IsNullOrEmpty(documentId))
                {
                    if (documentId.StartsWith(idPrefix))
                        documentId = documentId.Substring(idPrefix.Length);
                }
                Logger.Debug(String.Format("documentId={0}", documentId));
                string filePath = flocat.Attributes().Where(a => a.Name.LocalName == "href").Single().Value;
                Logger.Debug(String.Format("filePath={0}", filePath));

                byte[] content = GetDocContent(ExecParam, documentId);

                string fileFullPath = Path.Combine(contentDirectory.FullName, filePath);
                Logger.Debug(String.Format("fileFullPath={0}", fileFullPath));

                //k�nyvt�r l�trehoz�sa
                Directory.CreateDirectory(Path.GetDirectoryName(fileFullPath));

                //file elment�se
                File.WriteAllBytes(fileFullPath, content);

                //md5 kisz�mol�sa
                if (file.Attribute("CHECKSUM") != null)
                {
                    Logger.Debug("MD5");
                    file.Attribute("CHECKSUM").Value = CalculateMD5(content);
                }
            }

            //xml elment�se
            string xmlFilePath = Path.Combine(headerDirectory.FullName, "METADATA.XML");
            XmlWriterSettings ws = new XmlWriterSettings();
            ws.Encoding = new UTF8Encoding(false);
            using (XmlWriter writer = XmlWriter.Create(xmlFilePath, ws))
            {
                doc.Save(writer);
            }

            Logger.Debug(String.Format("xmlFilePath={0}", xmlFilePath));

            //zip f�jl
            string outputDirectory = Server.MapPath("~/MetadataXML");
            string zipFilePath = Path.Combine(outputDirectory, String.Format("{0}.zip", objId));
            Logger.Debug(String.Format("zipFilePath={0}", zipFilePath));
            FastZip fastZip = new FastZip();
            fastZip.UseZip64 = UseZip64.On;
            fastZip.CreateZip(zipFilePath, tempDirectory.FullName, true, null);

            //checksum file
            string checksumFilePath = Path.Combine(outputDirectory, String.Format("{0}.txt", objId));
            Logger.Debug(String.Format("checksumFilePath={0}", checksumFilePath));
            StringBuilder sb = new StringBuilder();
            //xml file
            sb.Append("metadata.xml: ");
            byte[] xmlFileContent = File.ReadAllBytes(xmlFilePath);
            sb.AppendLine(xmlFileContent.Length.ToString());
            sb.Append("SHA1: ");
            sb.AppendLine(Hash(xmlFileContent));
            //zip file
            sb.Append(String.Format("{0}: ", Path.GetFileName(zipFilePath)));
            byte[] zipFileContent = File.ReadAllBytes(zipFilePath);
            sb.AppendLine(zipFileContent.Length.ToString());
            sb.Append("SHA1: ");
            sb.Append(Hash(zipFileContent));

            File.WriteAllText(checksumFilePath, sb.ToString(), Encoding.UTF8);

            //temp directory t�rl�se
            tempDirectory.Delete(true);

            List<string> csomagFajlList = new List<string>();
            csomagFajlList.Add(GetFileUrl(zipFilePath));
            csomagFajlList.Add(GetFileUrl(checksumFilePath));

            csomagFajlok = csomagFajlList.ToArray();
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }

        log.WsEnd(ExecParam, result);
        return result;

    }

    byte[] GetDocContent(ExecParam execParam, string documentId)
    {
        Logger.Debug(String.Format("GetDocContent: {0}", documentId));

        KRT_DokumentumokService service = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
        ExecParam getExecParam = execParam.Clone();
        getExecParam.Record_Id = documentId;

        Result res = service.Get(getExecParam);

        if (res.IsError)
            throw new ResultException(res);

        KRT_Dokumentumok dok = res.Record as KRT_Dokumentumok;

        string externalLink = dok.External_Link;

        using (WebClient wc = new WebClient())
        {
            wc.Credentials = new NetworkCredential(UI.GetAppSetting("eSharePointBusinessServiceUserName"), UI.GetAppSetting("eSharePointBusinessServicePassword"), UI.GetAppSetting("eSharePointBusinessServiceUserDomain"));
            return wc.DownloadData(externalLink);
        }
    }

    string Hash(byte[] content)
    {
        using (SHA1Managed sha1 = new SHA1Managed())
        {
            var hash = sha1.ComputeHash(content);
            var sb = new StringBuilder(hash.Length * 2);

            foreach (byte b in hash)
            {
                // can be "x2" if you want lowercase
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }
    }

    public static string GetFileUrl(string filePath)
    {
        string appPath = HttpContext.Current.Server.MapPath("~");
        filePath = filePath.Replace(appPath, "").Replace("\\", "/").TrimStart('/');

        HttpRequest request = HttpContext.Current.Request;
        string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + request.ApplicationPath.TrimEnd('/') + "/";
        return baseUrl + filePath;
    }

    static string CalculateMD5(byte[] content)
    {
        using (MD5 md5 = MD5.Create())
        {
            var hash = md5.ComputeHash(content);
            var sb = new StringBuilder(hash.Length * 2);

            foreach (byte b in hash)
            {
                // can be "x2" if you want lowercase
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }
    }

    #endregion

    #region objtipusok

    void JegyzekUgyiratokLezarasa(ExecParam execParam, List<string> ugyiratIdList)
    {
        ExecParam execParam_ugyiratok = execParam.Clone();
        EREC_UgyUgyiratokSearch search_ugyiratok = new EREC_UgyUgyiratokSearch();
        search_ugyiratok.Id.Value = Search.GetSqlInnerString(ugyiratIdList.ToArray());
        search_ugyiratok.Id.Operator = Query.Operators.inner;

        EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);
        Result result_ugyiratokGetAll = service_ugyiratok.GetAll(execParam_ugyiratok, search_ugyiratok);

        if (result_ugyiratokGetAll.IsError)
        {
            throw new ResultException(result_ugyiratokGetAll);
        }

        if (result_ugyiratokGetAll.Ds.Tables[0].Rows.Count < ugyiratIdList.Count)
        {
            // "Hiba a jegyz�k lez�r�sa sor�n: Nem tal�lhat� meg minden jegyz�kre vett �gyirat!"
            throw new ResultException(55101);
        }

        // Sz�tv�laszt�s: szereltek �llapot�t nem kell �ll�tani
        DataTable tbSzerelt = null;
        DataTable tbNemSzerelt = null;
        DataRow[] rowsSzerelt = result_ugyiratokGetAll.Ds.Tables[0].Select(String.Format("Allapot='{0}'", KodTarak.UGYIRAT_ALLAPOT.Szerelt));
        if (rowsSzerelt.Length > 0)
        {
            tbSzerelt = rowsSzerelt.CopyToDataTable();
        }

        DataRow[] rowsNemSzerelt = result_ugyiratokGetAll.Ds.Tables[0].Select(String.Format("Allapot<>'{0}'", KodTarak.UGYIRAT_ALLAPOT.Szerelt));
        if (rowsNemSzerelt.Length > 0)
        {
            tbNemSzerelt = rowsNemSzerelt.CopyToDataTable();

            List<string> lsIdsNemSzerelt = new List<string>(tbNemSzerelt.Rows.Count);
            List<string> lsVersNemSzerelt = new List<string>(tbNemSzerelt.Rows.Count);
            foreach (DataRow row in tbNemSzerelt.Rows)
            {
                string id = row["Id"].ToString();
                string ver = row["Ver"].ToString();

                lsIdsNemSzerelt.Add(id);
                lsVersNemSzerelt.Add(ver);
            }

            #region �gyirat objektum �ssze�ll�t�sa t�meges UPDATE-hez
            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;

            //�llapot be�ll�t�s
            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo;
            erec_UgyUgyirat.Updated.Allapot = true;

            #endregion �gyirat objektum �ssze�ll�t�sa t�meges UPDATE-hez

            Result result_updateTomeges = service_ugyiratok.Update_Tomeges(execParam.Clone()
                , tbNemSzerelt, lsIdsNemSzerelt.ToArray()
                , lsVersNemSzerelt.ToArray()
                , erec_UgyUgyirat, DateTime.Now);

            if (result_updateTomeges.IsError)
            {
                throw new ResultException(result_updateTomeges);
            }
        }

        // Szereltek �llapot�t nem kell �ll�tani
        if (rowsSzerelt.Length > 0)
        {
            tbSzerelt = rowsSzerelt.CopyToDataTable();

            List<string> lsIdsSzerelt = new List<string>(tbSzerelt.Rows.Count);
            List<string> lsVersSzerelt = new List<string>(tbSzerelt.Rows.Count);
            foreach (DataRow row in tbSzerelt.Rows)
            {
                string id = row["Id"].ToString();
                string ver = row["Ver"].ToString();

                lsIdsSzerelt.Add(id);
                lsVersSzerelt.Add(ver);
            }

            #region �gyirat objektum �ssze�ll�t�sa t�meges UPDATE-hez
            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;

            //tov�b�t�s alatt �llapot be�ll�t�s
            erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo;
            erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

            #endregion �gyirat objektum �ssze�ll�t�sa t�meges UPDATE-hez

            Result result_updateTomeges = service_ugyiratok.Update_Tomeges(execParam.Clone()
                , tbSzerelt, lsIdsSzerelt.ToArray()
                , lsVersSzerelt.ToArray()
                , erec_UgyUgyirat, DateTime.Now);

            if (result_updateTomeges.IsError)
            {
                throw new ResultException(result_updateTomeges);
            }

        }
    }

    void JegyzekPeldanyokLezarasa(ExecParam execParam, List<string> peldanyIdList)
    {
        ExecParam execParam_peldanyok = execParam.Clone();
        EREC_PldIratPeldanyokSearch search_peldanyok = new EREC_PldIratPeldanyokSearch();
        search_peldanyok.Id.Value = Search.GetSqlInnerString(peldanyIdList.ToArray());
        search_peldanyok.Id.Operator = Query.Operators.inner;

        EREC_PldIratPeldanyokService service_peldanyok = new EREC_PldIratPeldanyokService(this.dataContext);
        Result result_peldanyokGetAll = service_peldanyok.GetAll(execParam_peldanyok, search_peldanyok);

        if (result_peldanyokGetAll.IsError)
        {
            throw new ResultException(result_peldanyokGetAll);
        }

        if (result_peldanyokGetAll.Ds.Tables[0].Rows.Count < peldanyIdList.Count)
        {
            // "Hiba a jegyz�k lez�r�sa sor�n: Nem tal�lhat� meg minden jegyz�kre vett �gyirat!"
            throw new ResultException(55101);
        }

        DataTable tbPeldanyok = result_peldanyokGetAll.Ds.Tables[0];
        if (tbPeldanyok.Rows.Count > 0)
        {
            List<string> lsIds = new List<string>();
            List<string> lsVers = new List<string>();

            List<string> iratIdList = new List<string>();

            foreach (DataRow row in tbPeldanyok.Rows)
            {
                string id = row["Id"].ToString();
                string ver = row["Ver"].ToString();
                string iratId = row["IraIrat_Id"].ToString();

                lsIds.Add(id);
                lsVers.Add(ver);

                if (!iratIdList.Contains(iratId))
                    iratIdList.Add(iratId);
            }

            #region p�ld�ny objektum �ssze�ll�t�sa t�meges UPDATE-hez

            EREC_PldIratPeldanyok erec_Peldany = new EREC_PldIratPeldanyok();
            erec_Peldany.Updated.SetValueAll(false);
            erec_Peldany.Base.Updated.SetValueAll(false);
            erec_Peldany.Base.Updated.Ver = true;

            //�llapot be�ll�t�s
            erec_Peldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Lezart_jegyzeken;
            erec_Peldany.Updated.Allapot = true;

            #endregion

            Result result_updateTomeges = service_peldanyok.UpdateTomeges(execParam.Clone(), lsIds, lsVers, erec_Peldany, DateTime.Now);

            if (result_updateTomeges.IsError)
            {
                throw new ResultException(result_updateTomeges);
            }

            //iratok lez�r�sa
            search_peldanyok = new EREC_PldIratPeldanyokSearch();
            search_peldanyok.IraIrat_Id.Value = Search.GetSqlInnerString(iratIdList.ToArray());
            search_peldanyok.IraIrat_Id.Operator = Query.Operators.inner;

            result_peldanyokGetAll = service_peldanyok.GetAll(execParam_peldanyok, search_peldanyok);

            if (result_peldanyokGetAll.IsError)
            {
                throw new ResultException(result_peldanyokGetAll);
            }

            tbPeldanyok = result_peldanyokGetAll.Ds.Tables[0];
            List<string> lezarandoIratIdList = new List<string>();

            foreach (string iratId in iratIdList)
            {
                bool iratLezarando = true;

                foreach (DataRow row in tbPeldanyok.Rows)
                {
                    string allapot = row["Allapot"].ToString();
                    string peldanyIratId = row["IraIrat_Id"].ToString();

                    if (iratId == peldanyIratId)
                    {
                        if (allapot != KodTarak.IRATPELDANY_ALLAPOT.Lezart_jegyzeken)
                        {
                            iratLezarando = false;
                            break;
                        }
                    }
                }

                if (iratLezarando)
                {
                    lezarandoIratIdList.Add(iratId);
                }
            }

            if (lezarandoIratIdList.Count > 0)
            {
                JegyzekIratokLezarasa(execParam, lezarandoIratIdList);
            }
        }
    }

    void JegyzekIratokLezarasa(ExecParam execParam, List<string> iratIdList)
    {
        ExecParam execParam_iratok = execParam.Clone();
        EREC_IraIratokSearch search_iratok = new EREC_IraIratokSearch();
        search_iratok.Id.Value = Search.GetSqlInnerString(iratIdList.ToArray());
        search_iratok.Id.Operator = Query.Operators.inner;

        EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);
        Result result_iratokGetAll = service_iratok.GetAll(execParam_iratok, search_iratok);

        if (result_iratokGetAll.IsError)
        {
            throw new ResultException(result_iratokGetAll);
        }

        if (result_iratokGetAll.Ds.Tables[0].Rows.Count < iratIdList.Count)
        {
            // "Hiba a jegyz�k lez�r�sa sor�n: Nem tal�lhat� meg minden jegyz�kre vett �gyirat!"
            throw new ResultException(55101);
        }

        DataTable tbIratok = result_iratokGetAll.Ds.Tables[0];
        if (tbIratok.Rows.Count > 0)
        {
            List<string> ugyiratIdList = new List<string>();

            foreach (DataRow row in tbIratok.Rows)
            {
                string id = row["Id"].ToString();
                string ver = row["Ver"].ToString();
                string ugyiratId = row["Ugyirat_Id"].ToString();

                if (!ugyiratIdList.Contains(ugyiratId))
                    ugyiratIdList.Add(ugyiratId);

                EREC_IraIratok erec_Irat = new EREC_IraIratok();
                erec_Irat.Updated.SetValueAll(false);
                erec_Irat.Base.Updated.SetValueAll(false);

                erec_Irat.Base.Ver = ver;
                erec_Irat.Base.Updated.Ver = true;

                //�llapot be�ll�t�s
                erec_Irat.Allapot = KodTarak.IRAT_ALLAPOT.LezartJegyzekben;
                erec_Irat.Updated.Allapot = true;

                ExecParam updateExecParam = execParam.Clone();
                updateExecParam.Record_Id = id;

                Result result_update = service_iratok.Update(updateExecParam, erec_Irat);

                if (result_update.IsError)
                {
                    throw new ResultException(result_update);
                }
            }

            //�gyiratok lez�r�sa
            search_iratok = new EREC_IraIratokSearch();
            search_iratok.Ugyirat_Id.Value = Search.GetSqlInnerString(ugyiratIdList.ToArray());
            search_iratok.Ugyirat_Id.Operator = Query.Operators.inner;

            result_iratokGetAll = service_iratok.GetAll(execParam_iratok, search_iratok);

            if (result_iratokGetAll.IsError)
            {
                throw new ResultException(result_iratokGetAll);
            }


            tbIratok = result_iratokGetAll.Ds.Tables[0];
            List<string> lezarandoUgyiratIdList = new List<string>();

            foreach (string ugyIratId in ugyiratIdList)
            {
                bool ugyiratLezarando = true;

                foreach (DataRow row in tbIratok.Rows)
                {
                    string allapot = row["Allapot"].ToString();
                    string iratUgyiratId = row["Ugyirat_Id"].ToString();

                    if (ugyIratId == iratUgyiratId)
                    {
                        if (allapot != KodTarak.IRAT_ALLAPOT.LezartJegyzekben)
                        {
                            ugyiratLezarando = false;
                            break;
                        }
                    }
                }

                if (ugyiratLezarando)
                {
                    lezarandoUgyiratIdList.Add(ugyIratId);
                }
            }

            if (lezarandoUgyiratIdList.Count > 0)
            {
                JegyzekUgyiratokLezarasa(execParam, lezarandoUgyiratIdList);
            }
        }
    }

    void JegyzekUgyiratokVegrehajtasa(ExecParam execParam, List<string> ugyiratIdList, EREC_IraJegyzekek jegyzek, string LeveltariAtvevo, bool csatolmanyokTorleseAszinkron)
    {
        ExecParam execParam_ugyiratok = execParam.Clone();
        EREC_UgyUgyiratokSearch search_ugyiratok = new EREC_UgyUgyiratokSearch();
        search_ugyiratok.Id.Value = Search.GetSqlInnerString(ugyiratIdList.ToArray());
        search_ugyiratok.Id.Operator = Query.Operators.inner;

        EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);

        Result result_ugyiratokGetAll = service_ugyiratok.GetAll(execParam_ugyiratok, search_ugyiratok); // t�meges lek�r�s

        if (result_ugyiratokGetAll.IsError)
        {
            throw new ResultException(result_ugyiratokGetAll);
        }

        if (result_ugyiratokGetAll.Ds.Tables[0].Rows.Count < ugyiratIdList.Count)
        {
            // "Hiba a jegyz�k v�grehajt�sa sor�n: Nem tal�lhat� meg minden jegyz�kre vett �gyirat!"
            throw new ResultException(55103);
        }


        // Sz�tv�laszt�s: szereltek �llapot�t nem kell �ll�tani
        DataTable tbSzerelt = null;
        DataTable tbNemSzerelt = null;
        DataRow[] rowsSzerelt = result_ugyiratokGetAll.Ds.Tables[0].Select(String.Format("Allapot='{0}'", KodTarak.UGYIRAT_ALLAPOT.Szerelt));
        if (rowsSzerelt.Length > 0)
        {
            tbSzerelt = rowsSzerelt.CopyToDataTable();
        }

        DataRow[] rowsNemSzerelt = result_ugyiratokGetAll.Ds.Tables[0].Select(String.Format("Allapot<>'{0}'", KodTarak.UGYIRAT_ALLAPOT.Szerelt));

        string egyebSzervezetIrattarId = null;
        if (jegyzek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value) // "E"
        {
            egyebSzervezetIrattarId = KodTarak.SPEC_SZERVEK.GetEgyebSzervezetIrattar(execParam.Clone());
            if (String.IsNullOrEmpty(egyebSzervezetIrattarId))
            {
                // "Hiba a jegyz�k v�grehajt�sa sor�n: Nincs megadva az egy�b szervezet iratt�r azonos�t�ja!"
                throw new ResultException(55104);
            }
        }

        if (rowsNemSzerelt.Length > 0)
        {
            tbNemSzerelt = rowsNemSzerelt.CopyToDataTable();

            List<string> lsIdsNemSzerelt = new List<string>(tbNemSzerelt.Rows.Count);
            List<string> lsVersNemSzerelt = new List<string>(tbNemSzerelt.Rows.Count);
            foreach (DataRow row in tbNemSzerelt.Rows)
            {
                string id = row["Id"].ToString();
                string ver = row["Ver"].ToString();

                lsIdsNemSzerelt.Add(id);
                lsVersNemSzerelt.Add(ver);
            }

            #region �gyirat objektum �ssze�ll�t�sa t�meges UPDATE-hez
            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;

            //�llapot be�ll�t�s
            if (jegyzek.Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value) //"S"
            {
                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Selejtezett;
                erec_UgyUgyirat.Updated.Allapot = true;
            }
            else if (jegyzek.Tipus == UI.JegyzekTipus.LeveltariAtadasJegyzek.Value) //"L"
            {
                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott;
                erec_UgyUgyirat.Updated.Allapot = true;
            }
            else if (jegyzek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value) //"E"
            {
                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.EgyebSzervezetnekAtadott;
                erec_UgyUgyirat.Updated.Allapot = true;

                #region Egy�b szervezet iratt�r bejegyz�se �rz�nek (irat helye) �s felel�snek (kezel�)
                //// megjegyezz�k az el�z� felel�st
                //erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
                //erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;

                // be�rjuk a szimbolikus iratt�rat
                erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo = egyebSzervezetIrattarId;
                erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;

                erec_UgyUgyirat.Csoport_Id_Felelos = egyebSzervezetIrattarId;
                erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                #endregion Egy�b szervezet iratt�r bejegyz�se �rz�nek (irat helye) �s felel�snek (kezel�)
            }

            // tov�bb�t�s alatt �llapot t�rl�se
            erec_UgyUgyirat.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

            if (!String.IsNullOrEmpty(LeveltariAtvevo))
            {
                erec_UgyUgyirat.LeveltariAtvevoNeve = LeveltariAtvevo;
                erec_UgyUgyirat.Updated.LeveltariAtvevoNeve = true;
            }

            erec_UgyUgyirat.FelhCsoport_Id_Selejtezo = execParam.Felhasznalo_Id;
            erec_UgyUgyirat.Updated.FelhCsoport_Id_Selejtezo = true;

            erec_UgyUgyirat.SelejtezesDat = DateTime.Now.ToString();
            erec_UgyUgyirat.Updated.SelejtezesDat = true;
            #endregion �gyirat objektum �ssze�ll�t�sa t�meges UPDATE-hez

            //Egy�b szervezetnek �tad�sn�l ne keletkezzen k�zbes�t�si t�tel
            service_ugyiratok.TomegesUpdateKellKezbesitesiTetel = false;

            Result result_updateTomeges = service_ugyiratok.Update_Tomeges(execParam.Clone()
                , tbNemSzerelt, lsIdsNemSzerelt.ToArray()
                , lsVersNemSzerelt.ToArray()
                , erec_UgyUgyirat, DateTime.Now);


            if (result_updateTomeges.IsError)
            {
                throw new ResultException(result_updateTomeges);
            }

        }

        // Szereltek �llapot�t vissza kell �ll�tani, a tov�bb�t�s alatt �llapot legyen a sz�l�nek megfelel�
        if (rowsSzerelt.Length > 0)
        {
            tbSzerelt = rowsSzerelt.CopyToDataTable();

            List<string> lsIdsSzerelt = new List<string>(tbSzerelt.Rows.Count);
            List<string> lsVersSzerelt = new List<string>(tbSzerelt.Rows.Count);
            foreach (DataRow row in tbSzerelt.Rows)
            {
                string id = row["Id"].ToString();
                string ver = row["Ver"].ToString();

                lsIdsSzerelt.Add(id);
                lsVersSzerelt.Add(ver);
            }

            #region �gyirat objektum �ssze�ll�t�sa t�meges UPDATE-hez
            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;

            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Szerelt;
            erec_UgyUgyirat.Updated.Allapot = true;

            if (jegyzek.Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value) //"S"
            {
                erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Selejtezett;
                erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
            }
            else if (jegyzek.Tipus == UI.JegyzekTipus.LeveltariAtadasJegyzek.Value) //"L"
            {
                erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott;
                erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
            }
            else if (jegyzek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value) //"E"
            {
                erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.EgyebSzervezetnekAtadott;
                erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

                #region Egy�b szervezet iratt�r bejegyz�se �rz�nek (irat helye) �s felel�snek (kezel�)
                //// megjegyezz�k az el�z� felel�st
                //erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
                //erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;

                // be�rjuk a szimbolikus iratt�rat
                erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo = egyebSzervezetIrattarId;
                erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;

                erec_UgyUgyirat.Csoport_Id_Felelos = egyebSzervezetIrattarId;
                erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                #endregion Egy�b szervezet iratt�r bejegyz�se �rz�nek (irat helye) �s felel�snek (kezel�)
            }

            if (!String.IsNullOrEmpty(LeveltariAtvevo))
            {
                erec_UgyUgyirat.LeveltariAtvevoNeve = LeveltariAtvevo;
                erec_UgyUgyirat.Updated.LeveltariAtvevoNeve = true;
            }

            erec_UgyUgyirat.FelhCsoport_Id_Selejtezo = execParam.Felhasznalo_Id;
            erec_UgyUgyirat.Updated.FelhCsoport_Id_Selejtezo = true;

            erec_UgyUgyirat.SelejtezesDat = DateTime.Now.ToString();
            erec_UgyUgyirat.Updated.SelejtezesDat = true;
            #endregion �gyirat objektum �ssze�ll�t�sa t�meges UPDATE-hez

            //Egy�b szervezetnek �tad�sn�l ne keletkezzen k�zbes�t�si t�tel
            service_ugyiratok.TomegesUpdateKellKezbesitesiTetel = false;

            Result result_updateTomeges = service_ugyiratok.Update_Tomeges(execParam.Clone()
                , tbSzerelt, lsIdsSzerelt.ToArray()
                , lsVersSzerelt.ToArray()
                , erec_UgyUgyirat, DateTime.Now);


            if (result_updateTomeges.IsError)
            {
                throw new ResultException(result_updateTomeges);
            }

        }

        if (!csatolmanyokTorleseAszinkron)
        {
            Result result_deleteCsatolmanyok = DeleteCsatolmanyokTomeges(execParam_ugyiratok, ugyiratIdList, jegyzek.Id);

            if (result_deleteCsatolmanyok.IsError)
            {
                throw new ResultException(result_deleteCsatolmanyok);
            }
        }

    }


    void JegyzekPeldanyokVegrehajtasa(ExecParam execParam, List<string> peldanyIdList, EREC_IraJegyzekek jegyzek, string LeveltariAtvevo, bool csatolmanyokTorleseAszinkron)
    {
        ExecParam execParam_peldanyok = execParam.Clone();
        EREC_PldIratPeldanyokSearch search_peldanyok = new EREC_PldIratPeldanyokSearch();
        search_peldanyok.Id.Value = Search.GetSqlInnerString(peldanyIdList.ToArray());
        search_peldanyok.Id.Operator = Query.Operators.inner;

        EREC_PldIratPeldanyokService service_peldanyok = new EREC_PldIratPeldanyokService(this.dataContext);

        Result result_peldanyokGetAll = service_peldanyok.GetAll(execParam_peldanyok, search_peldanyok); // t�meges lek�r�s

        if (result_peldanyokGetAll.IsError)
        {
            throw new ResultException(result_peldanyokGetAll);
        }

        if (result_peldanyokGetAll.Ds.Tables[0].Rows.Count < peldanyIdList.Count)
        {
            // "Hiba a jegyz�k v�grehajt�sa sor�n: Nem tal�lhat� meg minden jegyz�kre vett �gyirat!"
            throw new ResultException(55103);
        }


        DataTable tbPeldanyok = result_peldanyokGetAll.Ds.Tables[0];

        string egyebSzervezetIrattarId = null;
        if (jegyzek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value) // "E"
        {
            egyebSzervezetIrattarId = KodTarak.SPEC_SZERVEK.GetEgyebSzervezetIrattar(execParam.Clone());
            if (String.IsNullOrEmpty(egyebSzervezetIrattarId))
            {
                // "Hiba a jegyz�k v�grehajt�sa sor�n: Nincs megadva az egy�b szervezet iratt�r azonos�t�ja!"
                throw new ResultException(55104);
            }
        }

        if (tbPeldanyok.Rows.Count > 0)
        {
            List<string> lsIds = new List<string>(tbPeldanyok.Rows.Count);
            List<string> lsVers = new List<string>(tbPeldanyok.Rows.Count);
            List<string> iratIdList = new List<string>();

            foreach (DataRow row in tbPeldanyok.Rows)
            {
                string id = row["Id"].ToString();
                string ver = row["Ver"].ToString();
                string iratId = row["IraIrat_Id"].ToString();

                lsIds.Add(id);
                lsVers.Add(ver);

                if (!iratIdList.Contains(iratId))
                    iratIdList.Add(iratId);
            }

            #region p�ld�ny objektum �ssze�ll�t�sa t�meges UPDATE-hez
            EREC_PldIratPeldanyok erec_Peldany = new EREC_PldIratPeldanyok();
            erec_Peldany.Updated.SetValueAll(false);
            erec_Peldany.Base.Updated.SetValueAll(false);
            erec_Peldany.Base.Updated.Ver = true;

            //�llapot be�ll�t�s
            if (jegyzek.Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value) //"S"
            {
                erec_Peldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Selejtezett;
                erec_Peldany.Updated.Allapot = true;
            }
            else if (jegyzek.Tipus == UI.JegyzekTipus.LeveltariAtadasJegyzek.Value) //"L"
            {
                erec_Peldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.LeveltarbaAdott;
                erec_Peldany.Updated.Allapot = true;
            }
            else if (jegyzek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value) //"E"
            {
                erec_Peldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.EgyebSzervezetnekAtadott;
                erec_Peldany.Updated.Allapot = true;

                #region Egy�b szervezet iratt�r bejegyz�se �rz�nek (irat helye) �s felel�snek (kezel�)

                // be�rjuk a szimbolikus iratt�rat
                erec_Peldany.FelhasznaloCsoport_Id_Orzo = egyebSzervezetIrattarId;
                erec_Peldany.Updated.FelhasznaloCsoport_Id_Orzo = true;

                erec_Peldany.Csoport_Id_Felelos = egyebSzervezetIrattarId;
                erec_Peldany.Updated.Csoport_Id_Felelos = true;

                #endregion Egy�b szervezet iratt�r bejegyz�se �rz�nek (irat helye) �s felel�snek (kezel�)
            }

            // tov�bb�t�s alatt �llapot t�rl�se
            erec_Peldany.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_Peldany.Updated.TovabbitasAlattAllapot = true;

            if (!String.IsNullOrEmpty(LeveltariAtvevo))
            {
                erec_Peldany.LeveltariAtvevoNeve = LeveltariAtvevo;
                erec_Peldany.Updated.LeveltariAtvevoNeve = true;
            }

            erec_Peldany.FelhCsoport_Id_Selejtezo = execParam.Felhasznalo_Id;
            erec_Peldany.Updated.FelhCsoport_Id_Selejtezo = true;

            erec_Peldany.SelejtezesDat = DateTime.Now.ToString();
            erec_Peldany.Updated.SelejtezesDat = true;

            #endregion

            Result result_updateTomeges = service_peldanyok.UpdateTomeges(execParam.Clone(), lsIds, lsVers, erec_Peldany, DateTime.Now);


            if (result_updateTomeges.IsError)
            {
                throw new ResultException(result_updateTomeges);
            }

            //iratok v�grehajt�sa
            search_peldanyok = new EREC_PldIratPeldanyokSearch();
            search_peldanyok.IraIrat_Id.Value = Search.GetSqlInnerString(iratIdList.ToArray());
            search_peldanyok.IraIrat_Id.Operator = Query.Operators.inner;

            result_peldanyokGetAll = service_peldanyok.GetAll(execParam_peldanyok, search_peldanyok);

            if (result_peldanyokGetAll.IsError)
            {
                throw new ResultException(result_peldanyokGetAll);
            }

            tbPeldanyok = result_peldanyokGetAll.Ds.Tables[0];
            List<string> vegrehajtandoIratIdList = new List<string>();

            foreach (string iratId in iratIdList)
            {
                bool iratVegrehajtano = true;

                foreach (DataRow row in tbPeldanyok.Rows)
                {
                    string allapot = row["Allapot"].ToString();
                    string peldanyIratId = row["IraIrat_Id"].ToString();

                    if (iratId == peldanyIratId)
                    {
                        if (allapot != KodTarak.IRAT_ALLAPOT.Selejtezett
                            && allapot != KodTarak.IRAT_ALLAPOT.LeveltarbaAdott
                            && allapot != KodTarak.IRAT_ALLAPOT.EgyebSzervezetnekAtadott)
                        {
                            iratVegrehajtano = false;
                            break;
                        }
                    }
                }

                if (iratVegrehajtano)
                {
                    vegrehajtandoIratIdList.Add(iratId);
                }
            }

            if (vegrehajtandoIratIdList.Count > 0)
            {
                JegyzekIratokVegrehajtasa(execParam, iratIdList, jegyzek, LeveltariAtvevo, csatolmanyokTorleseAszinkron);
            }

        }
    }

    void JegyzekIratokVegrehajtasa(ExecParam execParam, List<string> iratIdList, EREC_IraJegyzekek jegyzek, string LeveltariAtvevo, bool csatolmanyokTorleseAszinkron)
    {
        ExecParam execParam_iratok = execParam.Clone();
        EREC_IraIratokSearch search_iratok = new EREC_IraIratokSearch();
        search_iratok.Id.Value = Search.GetSqlInnerString(iratIdList.ToArray());
        search_iratok.Id.Operator = Query.Operators.inner;

        EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);

        Result result_iratokGetAll = service_iratok.GetAll(execParam_iratok, search_iratok); // t�meges lek�r�s

        if (result_iratokGetAll.IsError)
        {
            throw new ResultException(result_iratokGetAll);
        }

        if (result_iratokGetAll.Ds.Tables[0].Rows.Count < iratIdList.Count)
        {
            // "Hiba a jegyz�k v�grehajt�sa sor�n: Nem tal�lhat� meg minden jegyz�kre vett �gyirat!"
            throw new ResultException(55103);
        }


        DataTable tbIratok = result_iratokGetAll.Ds.Tables[0];

        string egyebSzervezetIrattarId = null;
        if (jegyzek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value) // "E"
        {
            egyebSzervezetIrattarId = KodTarak.SPEC_SZERVEK.GetEgyebSzervezetIrattar(execParam.Clone());
            if (String.IsNullOrEmpty(egyebSzervezetIrattarId))
            {
                // "Hiba a jegyz�k v�grehajt�sa sor�n: Nincs megadva az egy�b szervezet iratt�r azonos�t�ja!"
                throw new ResultException(55104);
            }
        }

        if (tbIratok.Rows.Count > 0)
        {
            List<string> ugyiratIdList = new List<string>();

            foreach (DataRow row in tbIratok.Rows)
            {
                string id = row["Id"].ToString();
                string ver = row["Ver"].ToString();
                string ugyiratId = row["Ugyirat_Id"].ToString();

                if (!ugyiratIdList.Contains(ugyiratId))
                    ugyiratIdList.Add(ugyiratId);

                EREC_IraIratok erec_Irat = new EREC_IraIratok();
                erec_Irat.Updated.SetValueAll(false);
                erec_Irat.Base.Updated.SetValueAll(false);
                erec_Irat.Base.Ver = ver;
                erec_Irat.Base.Updated.Ver = true;

                //�llapot be�ll�t�s
                if (jegyzek.Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value) //"S"
                {
                    erec_Irat.Allapot = KodTarak.IRAT_ALLAPOT.Selejtezett;
                    erec_Irat.Updated.Allapot = true;
                }
                else if (jegyzek.Tipus == UI.JegyzekTipus.LeveltariAtadasJegyzek.Value) //"L"
                {
                    erec_Irat.Allapot = KodTarak.IRAT_ALLAPOT.LeveltarbaAdott;
                    erec_Irat.Updated.Allapot = true;
                }
                else if (jegyzek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value) //"E"
                {
                    erec_Irat.Allapot = KodTarak.IRAT_ALLAPOT.EgyebSzervezetnekAtadott;
                    erec_Irat.Updated.Allapot = true;

                    #region Egy�b szervezet iratt�r bejegyz�se �rz�nek (irat helye) �s felel�snek (kezel�)

                    // be�rjuk a szimbolikus iratt�rat
                    erec_Irat.FelhasznaloCsoport_Id_Orzo = egyebSzervezetIrattarId;
                    erec_Irat.Updated.FelhasznaloCsoport_Id_Orzo = true;

                    erec_Irat.Csoport_Id_Felelos = egyebSzervezetIrattarId;
                    erec_Irat.Updated.Csoport_Id_Felelos = true;

                    #endregion Egy�b szervezet iratt�r bejegyz�se �rz�nek (irat helye) �s felel�snek (kezel�)
                }

                if (!String.IsNullOrEmpty(LeveltariAtvevo))
                {
                    erec_Irat.LeveltariAtvevoNeve = LeveltariAtvevo;
                    erec_Irat.Updated.LeveltariAtvevoNeve = true;
                }

                erec_Irat.FelhCsoport_Id_Selejtezo = execParam.Felhasznalo_Id;
                erec_Irat.Updated.FelhCsoport_Id_Selejtezo = true;

                erec_Irat.SelejtezesDat = DateTime.Now.ToString();
                erec_Irat.Updated.SelejtezesDat = true;

                ExecParam updateExecParam = execParam.Clone();
                updateExecParam.Record_Id = id;

                Result result_update = service_iratok.Update(updateExecParam, erec_Irat);

                if (result_update.IsError)
                {
                    throw new ResultException(result_update);
                }
            }

            //�gyiratok v�grehajtasa
            search_iratok = new EREC_IraIratokSearch();
            search_iratok.Ugyirat_Id.Value = Search.GetSqlInnerString(ugyiratIdList.ToArray());
            search_iratok.Ugyirat_Id.Operator = Query.Operators.inner;

            result_iratokGetAll = service_iratok.GetAll(execParam_iratok, search_iratok);

            if (result_iratokGetAll.IsError)
            {
                throw new ResultException(result_iratokGetAll);
            }


            tbIratok = result_iratokGetAll.Ds.Tables[0];
            List<string> vegrehajtandoUgyiratIdList = new List<string>();

            foreach (string ugyIratId in ugyiratIdList)
            {
                bool ugyiratVegrehajtando = true;

                foreach (DataRow row in tbIratok.Rows)
                {
                    string allapot = row["Allapot"].ToString();
                    string iratUgyiratId = row["Ugyirat_Id"].ToString();

                    if (ugyIratId == iratUgyiratId)
                    {
                        if (allapot != KodTarak.IRAT_ALLAPOT.Selejtezett
                            && allapot != KodTarak.IRAT_ALLAPOT.LeveltarbaAdott
                            && allapot != KodTarak.IRAT_ALLAPOT.EgyebSzervezetnekAtadott)
                        {
                            ugyiratVegrehajtando = false;
                            break;
                        }
                    }
                }

                if (ugyiratVegrehajtando)
                {
                    vegrehajtandoUgyiratIdList.Add(ugyIratId);
                }
            }

            if (vegrehajtandoUgyiratIdList.Count > 0)
            {
                JegyzekUgyiratokVegrehajtasa(execParam, vegrehajtandoUgyiratIdList, jegyzek, LeveltariAtvevo, csatolmanyokTorleseAszinkron);
            }
        }
    }

    void GetJegyzekTetelek(ExecParam execParam, string jegyzekId, out List<string> ugyiratIdList, out List<string> peldanyIdList)
    {
        ugyiratIdList = new List<string>();
        peldanyIdList = new List<string>();

        EREC_IraJegyzekTetelekService service_jegyzektetelek = new EREC_IraJegyzekTetelekService(this.dataContext);
        EREC_IraJegyzekTetelekSearch search_jegyzektetelek = new EREC_IraJegyzekTetelekSearch();
        ExecParam execParam_jegyzektetelek = execParam.Clone();
        search_jegyzektetelek.Jegyzek_Id.Value = jegyzekId;
        search_jegyzektetelek.Jegyzek_Id.Operator = Query.Operators.equals;

        Result result_jegyzektetelekGetAll = service_jegyzektetelek.GetAll(execParam_jegyzektetelek, search_jegyzektetelek);

        if (result_jegyzektetelekGetAll.IsError)
        {
            throw new ResultException(result_jegyzektetelekGetAll);
        }

        
        foreach (DataRow row in result_jegyzektetelekGetAll.Ds.Tables[0].Rows)
        {
            string objType = row["Obj_Type"].ToString();
            string objId = row["Obj_Id"].ToString();

            if (objType == Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok)
            {
                if (!peldanyIdList.Contains(objId))
                {
                    peldanyIdList.Add(objId);
                }
            }
            else
            {
                if (!ugyiratIdList.Contains(objId))
                {
                    ugyiratIdList.Add(objId);
                }
            }
        }
    }

    #endregion

    #region megsemmisit�si naplo
    void SetJegyzekIktatoszam(ExecParam execParam, EREC_IraJegyzekek jegyzek, string megsemmisitesiNaploId)
    {
        string iktatokonyvId = megsemmisitesiNaploId;

        //iktat�k�nyv lek�r�se
        ExecParam execParam_iktatokonyv = execParam.Clone();
        execParam_iktatokonyv.Record_Id = iktatokonyvId;

        EREC_IraIktatoKonyvekService erec_IraIktatoKonyvekService = new EREC_IraIktatoKonyvekService(this.dataContext);

        Result result_iktatokonyvGet = erec_IraIktatoKonyvekService.Get(execParam_iktatokonyv);

        if (result_iktatokonyvGet.IsError)
            throw new ResultException(result_iktatokonyvGet);

        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = result_iktatokonyvGet.Record as EREC_IraIktatoKonyvek;

        int ujFoszam = Int32.Parse(erec_IraIktatoKonyvek.UtolsoFoszam) + 1;

        //utols� f�sz�m update
        erec_IraIktatoKonyvek.Updated.SetValueAll(false);
        erec_IraIktatoKonyvek.Base.Updated.SetValueAll(false);
        erec_IraIktatoKonyvek.Base.Updated.Ver = true;

        erec_IraIktatoKonyvek.UtolsoFoszam = ujFoszam.ToString();
        erec_IraIktatoKonyvek.Updated.UtolsoFoszam = true;

        Result result_IktatokonyvUpdate = erec_IraIktatoKonyvekService.Update(execParam_iktatokonyv, erec_IraIktatoKonyvek);

        if (result_IktatokonyvUpdate.IsError)
            throw new ResultException(result_IktatokonyvUpdate);

        jegyzek.Foszam = ujFoszam.ToString();
        jegyzek.Updated.Foszam = true;

        //iktat�sz�m meghat�roz�sa
        EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
        ugyirat.Foszam = jegyzek.Foszam;
        jegyzek.Azonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(execParam, ugyirat, erec_IraIktatoKonyvek);
        jegyzek.Updated.Azonosito = true;

        jegyzek.IraIktatokonyv_Id = megsemmisitesiNaploId;
        jegyzek.Updated.IraIktatokonyv_Id = true;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraJegyzekek))]
    public Result JegyzekMegsemmisitese(ExecParam execParam, EREC_IraJegyzekek jegyzek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = this.Update(execParam, jegyzek);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_IraJegyzekek", "JegyzekMegsemmisites").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #endregion

    #region Aszinkron jegyz�k v�grahajt�s

    [WebMethod()]
    public Result FolyamatbanLevoJegyzekekVegrehajtasa()
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309"; //Admin
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            EREC_IraJegyzekekSearch jegyzekekSearch = new EREC_IraJegyzekekSearch();
            jegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.VegrehajtasFolyamatban;
            jegyzekekSearch.Allapot.Operator = Query.Operators.equals;

            Logger.Debug("EREC_IraJegyzekekService.GetAll kezdete");
            Result jegyzekekResult = this.GetAll(execParam, jegyzekekSearch);
            Logger.Debug("EREC_IraJegyzekekService.GetAll vege");

            if (jegyzekekResult.IsError)
            {
                Logger.Error("EREC_IraJegyzekekService.GetAll hiba", execParam, jegyzekekResult);
                throw new ResultException(jegyzekekResult);
            }

            DataTable jegyzekekTable = jegyzekekResult.Ds.Tables[0];
            Logger.Debug(String.Format("Jegyz�kek sz�ma: {0}", jegyzekekTable.Rows.Count));

            foreach (DataRow row in jegyzekekTable.Rows)
            {
                EREC_IraJegyzekek jegyzek = new EREC_IraJegyzekek();
                Utility.LoadBusinessDocumentFromDataRow(jegyzek, row);
                try
                {
                    FolyamatbanLevoJegyzekVegrehajtasa(jegyzek);
                }
                catch (Exception ex)
                {
                    Result error = ResultException.GetResultFromException(ex);
                    Logger.Error(String.Format("FolyamatbanLevoJegyzekVegrehajtasa hiba: {0}", jegyzek.Id), execParam, error);
                }
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    void FolyamatbanLevoJegyzekVegrehajtasa(EREC_IraJegyzekek jegyzek)
    {
        Logger.Info(String.Format("FolyamatbanLevoJegyzekVegrehajtasa kezdete: {0}", jegyzek.Id));
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = jegyzek.FelhasznaloCsoport_Id_Vegrehaj;
        execParam.Record_Id = jegyzek.Id;

        List<string> ugyiratIdList;
        List<string> peldanyIdList;
        GetJegyzekTetelek(execParam, jegyzek.Id, out ugyiratIdList, out peldanyIdList);

        Logger.Debug(String.Format("�gyiratok sz�ma: {0}", ugyiratIdList.Count));
        Logger.Debug(String.Format("p�ld�nyok sz�ma: {0}", peldanyIdList.Count));

        List<string> iratIdList = new List<string>();

        EREC_CsatolmanyokService csatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);
        EREC_CsatolmanyokSearch csatolmanyokSearch = new EREC_CsatolmanyokSearch();

        if (ugyiratIdList.Count > 0)
        {
            csatolmanyokSearch.IraIrat_Id.Value = String.Format("select id from EREC_IraIratok where Ugyirat_Id in ({0})", Search.GetSqlInnerString(ugyiratIdList.ToArray()));
            csatolmanyokSearch.IraIrat_Id.Operator = Query.Operators.inner;

            Logger.Debug("EREC_CsatolmanyokService.GetAll kezdete");
            Result csatolmanyokResult = csatolmanyokService.GetAll(execParam, csatolmanyokSearch);
            Logger.Debug("EREC_CsatolmanyokService.GetAll vege");

            if (csatolmanyokResult.IsError)
            {
                Logger.Error("EREC_CsatolmanyokService.GetAll hiba", execParam, csatolmanyokResult);
                throw new ResultException(csatolmanyokResult);
            }

            DataTable csatolmanyokResultTable = csatolmanyokResult.Ds.Tables[0];
            Logger.Debug(String.Format("Csatolm�nyok sz�ma: {0}", csatolmanyokResultTable.Rows.Count));

            foreach (DataRow row in csatolmanyokResultTable.Rows)
            {
                string iratId = row["IraIrat_Id"].ToString();

                if (!iratIdList.Contains(iratId))
                {
                    iratIdList.Add(iratId);
                }
            }
        }

        if (peldanyIdList.Count > 0)
        {
            csatolmanyokSearch.IraIrat_Id.Value = String.Format("select EREC_IraIratok.Id from EREC_IraIratok join EREC_PldIratPeldanyok on EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id" +
                " where EREC_PldIratPeldanyok.Id in ({0}) and EREC_IraIratok.Allapot in ({1})", Search.GetSqlInnerString(peldanyIdList.ToArray()), Search.GetSqlInnerString(new string[] { KodTarak.IRAT_ALLAPOT.Selejtezett, KodTarak.IRAT_ALLAPOT.LeveltarbaAdott, KodTarak.IRAT_ALLAPOT.EgyebSzervezetnekAtadott }));
            csatolmanyokSearch.IraIrat_Id.Operator = Query.Operators.inner;

            Logger.Debug("EREC_CsatolmanyokService.GetAll kezdete");
            Result csatolmanyokResult = csatolmanyokService.GetAll(execParam, csatolmanyokSearch);
            Logger.Debug("EREC_CsatolmanyokService.GetAll vege");

            if (csatolmanyokResult.IsError)
            {
                Logger.Error("EREC_CsatolmanyokService.GetAll hiba", execParam, csatolmanyokResult);
                throw new ResultException(csatolmanyokResult);
            }

            DataTable csatolmanyokResultTable = csatolmanyokResult.Ds.Tables[0];
            Logger.Debug(String.Format("Csatolm�nyok sz�ma: {0}", csatolmanyokResultTable.Rows.Count));

            foreach (DataRow row in csatolmanyokResultTable.Rows)
            {
                string iratId = row["IraIrat_Id"].ToString();

                if (!iratIdList.Contains(iratId))
                {
                    iratIdList.Add(iratId);
                }
            }
        }

        Logger.Debug(String.Format("Iratok sz�ma: {0}", iratIdList.Count));

        foreach (string iratId in iratIdList)
        {
            try
            {
                DeleteIratCsatolmanyok(execParam, iratId);
            }
            catch (Exception ex)
            {
                Result error = ResultException.GetResultFromException(ex);
                Logger.Error(String.Format("DeleteIratCsatolmanyok hiba: {0}", iratId), execParam, error);
            }
        }

        jegyzek.Updated.SetValueAll(false);
        jegyzek.Base.Updated.SetValueAll(false);
        jegyzek.Base.Updated.Ver = true;

        jegyzek.Allapot = KodTarak.JEGYZEK_ALLAPOT.Vegrahajtott;
        jegyzek.Updated.Allapot = true;

        jegyzek.VegrehajtasDatuma = DateTime.Now.ToString();
        jegyzek.Updated.VegrehajtasDatuma = true;

        Logger.Debug("EREC_IraJegyzekekService.Update kezdete");
        Result jegyzekResult = this.Update(execParam, jegyzek);
        Logger.Debug("EREC_IraJegyzekekService.Update vege");

        if (jegyzekResult.IsError)
        {
            Logger.Error("EREC_IraJegyzekekService.Update hiba", execParam, jegyzekResult);
            throw new ResultException(jegyzekResult);
        }


        Logger.Info("FolyamatbanLevoJegyzekVegrehajtasa vege");
    }

    private void DeleteIratCsatolmanyok(ExecParam execParam, string iratId)
    {
        Logger.Info(String.Format("DeleteIratCsatolmanyok kezdete: {0}", iratId));

        EREC_IraIratok irat = new EREC_IraIratok();
        irat.Id = iratId;

        Contentum.eDocument.Service.DocumentService documentService = eDocumentService.ServiceFactory.GetDocumentService();
        Logger.Debug("DocumentService.SelejtezesIratDocumentsLocation kezdete");
        Result result_Selejtezes = documentService.SelejtezesIratDocumentsLocation(execParam, irat);
        Logger.Debug("DocumentService.SelejtezesIratDocumentsLocation vege");
        if (result_Selejtezes.IsError)
        {
            Logger.Error("DocumentService.SelejtezesIratDocumentsLocation hiba", execParam, result_Selejtezes);
            throw new ResultException(result_Selejtezes);
        }


        ExecParam execParamInvalidate = execParam.Clone();
        EREC_CsatolmanyokSearch csatolmanyokSearch = new EREC_CsatolmanyokSearch();
        csatolmanyokSearch.IraIrat_Id.Value = iratId;
        csatolmanyokSearch.IraIrat_Id.Operator = Query.Operators.equals;
        EREC_CsatolmanyokService csatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);

        Logger.Debug("EREC_CsatolmanyokService.TomegesInvalidate kezdete");
        Result res_invalidate = csatolmanyokService.TomegesInvalidate(execParamInvalidate, csatolmanyokSearch);
        Logger.Debug("EREC_CsatolmanyokService.TomegesInvalidate vege");

        if (res_invalidate.IsError)
        {
            Logger.Error("EREC_CsatolmanyokService.TomegesInvalidate hiba", execParamInvalidate, res_invalidate);
            throw new ResultException(res_invalidate);
        }

        Logger.Info("DeleteIratCsatolmanyok vege");
    }

    #endregion
}