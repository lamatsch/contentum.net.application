﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data;
using System.Collections.Generic;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IraKezbesitesiFejekService : System.Web.Services.WebService
{
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraKezbesitesiTetelekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraKezbesitesiFejekSearch _EREC_IraKezbesitesiFejekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_IraKezbesitesiFejekSearch);
            
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}
