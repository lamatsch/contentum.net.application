using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data;
using System.Collections.Generic;
using System.Collections;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IraKezbesitesiTetelekService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraKezbesitesiTetelekSearch))]
    public Result GetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_IraKezbesitesiTetelekSearch _EREC_IraKezbesitesiTetelekSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtension(ExecParam, _EREC_IraKezbesitesiTetelekSearch, Jogosultak);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_EREC_IraKezbesitesiTetelekSearch, new EREC_IraKezbesitesiTetelekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "EREC_IraKezbesitesiTetelek", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _EREC_IraKezbesitesiTetelekSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraKezbesitesiTetelekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraKezbesitesiTetelekSearch _EREC_IraKezbesitesiTetelekSearch)
    {
        return GetAllWithExtensionAndJogosultak(ExecParam, _EREC_IraKezbesitesiTetelekSearch, false);
    }

    /// <summary>
    /// T�bb egym�skoz kapcsolt t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// (A GetAll kiterjeszt�se join m�velettel �sszekapcsolt t�bl�kra). 
    /// Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalmaz�s, h�v� HTML oldal).
    /// Jogosults�gra sz�r�ssel
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_IraKezbesitesiTetelekSearch"></param>
    /// <param name="Jogosultak"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result AltalanosKeresesGetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_IraKezbesitesiTetelekSearch _EREC_IraKezbesitesiTetelekSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.AltalanosKeresesGetAllWithExtension(ExecParam, _EREC_IraKezbesitesiTetelekSearch, Jogosultak);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_EREC_IraKezbesitesiTetelekSearch, new EREC_IraKezbesitesiTetelekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "EREC_IraKezbesitesiTetelek", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_IraKezbesitesiTetelekSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_EREC_IraKezbesitesiTetelekSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _EREC_IraKezbesitesiTetelekSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraKezbesitesiTetelekSearch))]
    public Result ForXml(ExecParam ExecParam, EREC_IraKezbesitesiTetelekSearch _EREC_IraKezbesitesiTetelekSearch, string AtadasIrany, string fejId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ForXml(ExecParam, _EREC_IraKezbesitesiTetelekSearch, AtadasIrany, fejId);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result Atadas_Tomeges(ExecParam execParam, String[] erec_IraKezbesitesiTetelek_Id_Array)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_IraKezbesitesiTetelek_Id_Array == null || erec_IraKezbesitesiTetelek_Id_Array.Length == 0)
            {
                // hiba
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52210);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52210);
            }
            else
            {
                Result result_atadas = new Result();

                string selectedIds = "'" + erec_IraKezbesitesiTetelek_Id_Array[0] + "'";
                for (int i = 1; i < erec_IraKezbesitesiTetelek_Id_Array.Length; i++)
                {
                    selectedIds += ",'" + erec_IraKezbesitesiTetelek_Id_Array[i] + "'";
                }

                EREC_IraKezbesitesiTetelekSearch search = new EREC_IraKezbesitesiTetelekSearch();
                search.Id.Value = selectedIds;
                search.Id.Operator = Query.Operators.inner;

                result_atadas = GetAll(execParam.Clone(), search);
                if (!string.IsNullOrEmpty(result_atadas.ErrorCode))
                    throw new ResultException(result_atadas);

                string selectedUUIds = string.Empty;
                string selectedPIIds = string.Empty;
                string selectedKKIds = string.Empty;

                selectedIds = string.Empty;
                string selectedVers = string.Empty;
                result.Ds = new DataSet();
                result.Ds.Tables.Add();
                result.Ds.Tables[0].Columns.Add("Id");

                // kijel�lt t�telek id-jainak �ssze�ll�t�sa
                foreach (DataRow r in result_atadas.Ds.Tables[0].Rows)
                {
                    selectedIds += ",'" + r["Id"].ToString() + "'";
                    selectedVers += "," + r["Ver"].ToString();

                    if (r["Allapot"].ToString() != Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt)
                        result.Ds.Tables[0].Rows.Add(r["Obj_Id"]);

                    switch (r["Obj_type"].ToString())
                    {
                        case Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok:
                            selectedUUIds += ",'" + r["Obj_Id"].ToString() + "'";
                            break;
                        case Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok:
                            selectedPIIds += ",'" + r["Obj_Id"].ToString() + "'";
                            break;
                        case Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek:
                            selectedKKIds += ",'" + r["Obj_Id"].ToString() + "'";
                            break;
                    }
                }

                if (result.Ds.Tables[0].Rows.Count > 0)
                    throw new ResultException(52212);

                // vessz�k lev�g�sa a konkaten�ci�k elej�r�l
                selectedUUIds = selectedUUIds.TrimStart(',');
                selectedPIIds = selectedPIIds.TrimStart(',');
                selectedKKIds = selectedKKIds.TrimStart(',');
                selectedIds = selectedIds.TrimStart(',');
                selectedVers = selectedVers.TrimStart(',');

                #region �gyiratok t�meges �tad�sa
                if (!string.IsNullOrEmpty(selectedUUIds))
                {
                    EREC_UgyUgyiratokSearch ugyiratSearch = new EREC_UgyUgyiratokSearch();
                    ugyiratSearch.Id.Value = selectedUUIds;
                    ugyiratSearch.Id.Operator = Query.Operators.inner;

                    EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);
                    Result ugyiratokGetAllResult = ugyiratService.GetAll(execParam.Clone(), ugyiratSearch);
                    if (!string.IsNullOrEmpty(ugyiratokGetAllResult.ErrorCode))
                        throw new ResultException(ugyiratokGetAllResult);

                    foreach (System.Data.DataRow r in ugyiratokGetAllResult.Ds.Tables[0].Rows)
                    {
                        if (r["Allapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt
                            || r["Allapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Szignalt
                            || r["Allapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Iktatott
                            || r["Allapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo
                            )
                        {
                            // kik�r�n l�v� �gyirat eset�n �gyirat �llapot�t �ll�tani kell
                            if (r["Allapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo)
                            {
                                EREC_IrattariKikeroService kikeroService = new EREC_IrattariKikeroService(this.dataContext);
                                ExecParam kikeroExecParam = execParam.Clone();
                                kikeroExecParam.Record_Id = r["Id"].ToString();
                                Result kikeroResult = kikeroService.KolcsonzesKiadas(kikeroExecParam);

                                if (!string.IsNullOrEmpty(kikeroResult.ErrorCode))
                                {
                                    result.Ds.Tables[0].Rows.Add(r["Id"]);
                                    throw new ResultException(kikeroResult);
                                }
                            }
                        }
                        else
                        {
                            result.Ds.Tables[0].Rows.Add(r["Id"]);
                            //throw new ResultException(52212);
                        }
                    }

                    if (result.Ds.Tables[0].Rows.Count > 0)
                        throw new ResultException(52212);
                }
                #endregion

                #region iratp�ld�nyok t�meges �tad�sa
                if (!string.IsNullOrEmpty(selectedPIIds))
                {
                    EREC_PldIratPeldanyokSearch iratpeldanySearch = new EREC_PldIratPeldanyokSearch();
                    iratpeldanySearch.Id.Value = selectedPIIds;
                    iratpeldanySearch.Id.Operator = Query.Operators.inner;

                    EREC_PldIratPeldanyokService iratpeldanyService = new EREC_PldIratPeldanyokService(this.dataContext);
                    // GetAllWithExtension kell, hogy az eredm�nyb�l felt�lthet� legyen az iratp�ld�ny, irat �s �gyirat st�tusz is
                    Result iratpeldanyGetAllResult = iratpeldanyService.GetAllWithExtension(execParam.Clone(), iratpeldanySearch);
                    if (!string.IsNullOrEmpty(iratpeldanyGetAllResult.ErrorCode))
                    {
                        throw new ResultException(iratpeldanyGetAllResult);
                    }

                    //System.Collections.Hashtable iratpeldanyStatuszok = Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByDataSet(execParam.Clone(), iratpeldanyGetAllResult.Ds);
                    Hashtable iratPeldanyStatuszok;
                    Hashtable iratStatuszok;
                    Hashtable ugyiratStatuszok;
                    ErrorDetails errorDetail;

                    // statuszok felt�lt�se:
                    Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotHierarchiaByDataSet(execParam.Clone(), iratpeldanyGetAllResult.Ds
                        , out iratPeldanyStatuszok, out iratStatuszok, out ugyiratStatuszok);

                    if (iratPeldanyStatuszok == null || iratStatuszok == null || ugyiratStatuszok == null)
                    {
                        // Hiba az iratp�ld�nyok �llapot�nak ellen�rz�se sor�n!
                        throw new ResultException(52295);
                    }

                    foreach (DataRow rk in iratpeldanyGetAllResult.Ds.Tables[0].Rows)
                    {
                        string pld_Id = rk["Id"].ToString();

                        if (!Contentum.eRecord.BaseUtility.IratPeldanyok.Atadhato(
                                iratPeldanyStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz
                                , iratStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.Iratok.Statusz
                                , ugyiratStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz
                                , execParam.Clone(), out errorDetail)
                            )
                        {
                            result.Ds.Tables[0].Rows.Add(rk["Id"]);
                        }
                    }

                    if (result.Ds.Tables[0].Rows.Count > 0)
                        throw new ResultException(52212);
                }
                #endregion

                #region k�ldem�nyek t�meges �tad�sa
                if (!string.IsNullOrEmpty(selectedKKIds))
                {
                    EREC_KuldKuldemenyekSearch kuldemenySearch = new EREC_KuldKuldemenyekSearch();
                    kuldemenySearch.Id.Value = selectedKKIds;
                    kuldemenySearch.Id.Operator = Query.Operators.inner;

                    EREC_KuldKuldemenyekService kuldemenyService = new EREC_KuldKuldemenyekService(this.dataContext);
                    Result kuldemenyUpdateResult = kuldemenyService.GetAll(execParam.Clone(), kuldemenySearch);
                    if (!string.IsNullOrEmpty(kuldemenyUpdateResult.ErrorCode))
                        throw new ResultException(kuldemenyUpdateResult);

                    System.Collections.Hashtable kuldemenyStatuszok = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByDataSet(execParam.Clone(), kuldemenyUpdateResult.Ds);
                    foreach (DataRow rk in kuldemenyUpdateResult.Ds.Tables[0].Rows)
                    {
                        if (!Contentum.eRecord.BaseUtility.Kuldemenyek.Atadhato(execParam.Clone(), (kuldemenyStatuszok[rk["Id"].ToString()] as Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz)))
                            result.Ds.Tables[0].Rows.Add(rk["Id"]);
                        //throw new ResultException(52212);
                    }

                    if (result.Ds.Tables[0].Rows.Count > 0)
                        throw new ResultException(52212);
                }
                #endregion

                #region K�zbes�t�si t�tel UPDATE:
                Result kezbTetelUpdateResult = sp.AtadasTomeges(execParam.Clone(), selectedIds, selectedVers);
                if (!string.IsNullOrEmpty(kezbTetelUpdateResult.ErrorCode))
                {
                    result.Ds = kezbTetelUpdateResult.Ds;
                    throw new ResultException(kezbTetelUpdateResult);
                }
                #endregion


                #region Esem�nynapl�z�s
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomeges(execParam, selectedIds, "EREC_IraKezbesitesiTetelek", "Modify");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_IraKezbesitesiTetelek::Modositas(AtadasTomeges): ", execParam, eventLogResult);
                #endregion

                result = kezbTetelUpdateResult;
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            Result errorResult = ResultException.GetResultFromException(e);
            result.ErrorCode = errorResult.ErrorCode;
            result.ErrorMessage = errorResult.ErrorMessage;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result Atadas(ExecParam execParam, String erec_IraKezbesitesiTetelek_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        // Param�terellen�rz�s:
        if (execParam == null || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52210);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // K�zbes�t�si t�tel lek�r�se:
            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbtetelGet = execParam.Clone();
            execParam_kezbtetelGet.Record_Id = erec_IraKezbesitesiTetelek_Id;

            Result result_kezbTetelGet = service_KezbesitesiTetelek.Get(execParam_kezbtetelGet);
            if (!String.IsNullOrEmpty(result_kezbTetelGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_kezbtetelGet, result_kezbTetelGet);
                //return result_kezbTetelGet;

                throw new ResultException(result_kezbTetelGet);
            }

            if (result_kezbTetelGet.Record == null)
            {
                // hiba:
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52211);
                //log.WsEnd(execParam_kezbtetelGet, result1);
                //return result1;

                throw new ResultException(52211);
            }
            else
            {
                EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetel =
                    (EREC_IraKezbesitesiTetelek)result_kezbTetelGet.Record;

                if (erec_IraKezbesitesiTetel.Allapot != Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt)
                {
                    // Nem j� az �llapot:
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52212);
                    //log.WsEnd(execParam_kezbtetelGet, result1);
                    //return result1;

                    throw new ResultException(52212);
                }
                else
                {
                    bool atadhato = false;

                    // T�tel �tadhat�s�g�nak vizsg�lata:
                    // T�tel t�pus�t�l f�gg�en sz�t�gazunk:
                    switch (erec_IraKezbesitesiTetel.Obj_type)
                    {
                        case Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok:
                            {
                                // �gyirat �llapot�nak vizsg�lata:
                                ExecParam xpmUgyiratGet = execParam.Clone();
                                xpmUgyiratGet.Record_Id = erec_IraKezbesitesiTetel.Obj_Id;
                                EREC_UgyUgyiratokService svcUgyiratok = new EREC_UgyUgyiratokService(this.dataContext);
                                Result resUgyiratGet = svcUgyiratok.Get(xpmUgyiratGet);
                                if (!String.IsNullOrEmpty(resUgyiratGet.ErrorCode))
                                {
                                    throw new ResultException(resUgyiratGet);
                                }

                                EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)resUgyiratGet.Record;

                                ExecParam execParam_ugyiratStatusz = execParam.Clone();
                                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(ugyirat);
                                if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt
                                    || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Szignalt
                                    || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Iktatott
                                    || ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo
                                    )
                                {
                                    // kik�r�n l�v� �gyirat eset�n �gyirat �llapot�t �ll�tani kell
                                    if (ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo)
                                    {
                                        EREC_IrattariKikeroService kikeroService = new EREC_IrattariKikeroService(this.dataContext);
                                        Result kikeroResult = kikeroService.KolcsonzesKiadas(xpmUgyiratGet);

                                        if (!string.IsNullOrEmpty(kikeroResult.ErrorCode))
                                            throw new ResultException(kikeroResult);
                                    }
                                    atadhato = true;
                                }
                                else
                                {
                                    atadhato = false;
                                }
                                break;
                            }
                        case Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok:
                            {
                                EREC_PldIratPeldanyokService service_iratPeldanyok = new EREC_PldIratPeldanyokService(this.dataContext);
                                ExecParam execParam_IratPeldanyGet = execParam.Clone();
                                execParam_IratPeldanyGet.Record_Id = erec_IraKezbesitesiTetel.Obj_Id;

                                Result result_IratPeldanyGet = service_iratPeldanyok.Get(execParam_IratPeldanyGet);
                                if (!String.IsNullOrEmpty(result_IratPeldanyGet.ErrorCode))
                                {
                                    throw new ResultException(result_IratPeldanyGet);
                                }


                                EREC_PldIratPeldanyok peldany = (EREC_PldIratPeldanyok)result_IratPeldanyGet.Record;

                                Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz iratPeldanyStatusz =
                                    Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByBusinessDocument(peldany);

                                EREC_IraIratokService iratokService = new EREC_IraIratokService(this.dataContext);
                                ExecParam xpmIratok = execParam.Clone();
                                xpmIratok.Record_Id = peldany.IraIrat_Id;

                                Result resIratok = iratokService.Get(xpmIratok);

                                if (resIratok.IsError)
                                {
                                    throw new ResultException(resIratok);
                                }

                                EREC_IraIratok irat = resIratok.Record as EREC_IraIratok;

                                Contentum.eRecord.BaseUtility.Iratok.Statusz iratStatusz =
                                    Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(irat);

                                EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                                ExecParam xpmUgyiratok = execParam.Clone();
                                xpmUgyiratok.Record_Id = irat.Ugyirat_Id;

                                Result resUgyiratok = ugyiratokService.Get(xpmUgyiratok);

                                if (resUgyiratok.IsError)
                                {
                                    throw new ResultException(resUgyiratok);
                                }

                                EREC_UgyUgyiratok ugyirat = resUgyiratok.Record as EREC_UgyUgyiratok;

                                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(ugyirat);

                                ExecParam execParam_atadhato = execParam.Clone();
                                ErrorDetails errorDetail = null;
                                if (Contentum.eRecord.BaseUtility.IratPeldanyok.Atadhato(
                                    iratPeldanyStatusz, iratStatusz, ugyiratStatusz, execParam_atadhato, out errorDetail) == true)
                                {
                                    atadhato = true;
                                }
                                else
                                {
                                    atadhato = false;
                                }
                                break;
                            }
                        case Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek:
                            {
                                // K�ldem�ny:
                                ExecParam execParam_kuldStatusz = execParam.Clone();
                                Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz kuldemenyStatusz =
                                    Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotById(
                                    erec_IraKezbesitesiTetel.Obj_Id, execParam_kuldStatusz, null);
                                if (Contentum.eRecord.BaseUtility.Kuldemenyek.Atadhato(
                                    execParam_kuldStatusz, kuldemenyStatusz) == true)
                                {
                                    atadhato = true;
                                }
                                else
                                {
                                    atadhato = false;
                                }
                                break;
                            }
                        default:
                            // hiba (�rv�nytelen t�pus)
                            //Result result1 = ResultError.CreateNewResultWithErrorCode(52213);
                            //log.WsEnd(execParam_kezbtetelGet, result1);
                            //return result1;

                            throw new ResultException(52213);
                    }

                    if (atadhato == false)
                    {
                        // nem adhat� �t a t�tel:
                        //Result result1 = ResultError.CreateNewResultWithErrorCode(52212);
                        //log.WsEnd(execParam_kezbtetelGet, result1);
                        //return result1;

                        throw new ResultException(52212);
                    }
                    else
                    {
                        // K�zbes�t�si fej l�trehoz�sa
                        EREC_IraKezbesitesiFejek kezbesitesiFej = new EREC_IraKezbesitesiFejek();

                        kezbesitesiFej.Id = Guid.NewGuid().ToString();
                        kezbesitesiFej.Updated.Id = true;

                        kezbesitesiFej.CsomagAzonosito = "Atadas";
                        kezbesitesiFej.Updated.CsomagAzonosito = true;

                        kezbesitesiFej.Tipus = "A";
                        kezbesitesiFej.Updated.Tipus = true;

                        EREC_IraKezbesitesiFejekService kezbesitesiFejService = new EREC_IraKezbesitesiFejekService(this.dataContext);
                        Result kezbesitesiFejInsertResult = kezbesitesiFejService.Insert(execParam.Clone(), kezbesitesiFej);
                        if (!string.IsNullOrEmpty(kezbesitesiFejInsertResult.ErrorCode))
                            throw new ResultException(kezbesitesiFejInsertResult);


                        // K�zbes�t�si t�tel UPDATE:
                        erec_IraKezbesitesiTetel.Updated.SetValueAll(false);
                        erec_IraKezbesitesiTetel.Base.Updated.SetValueAll(false);
                        erec_IraKezbesitesiTetel.Base.Updated.Ver = true;

                        /// Amit m�dos�tani kell:
                        /// AtadasDat, Allapot, KezbesitesiFej
                        /// 

                        erec_IraKezbesitesiTetel.KezbesitesFej_Id = kezbesitesiFej.Id;
                        erec_IraKezbesitesiTetel.Updated.KezbesitesFej_Id = true;

                        erec_IraKezbesitesiTetel.AtadasDat = DateTime.Now.ToString();
                        erec_IraKezbesitesiTetel.Updated.AtadasDat = true;

                        erec_IraKezbesitesiTetel.Allapot = Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott;
                        erec_IraKezbesitesiTetel.Updated.Allapot = true;

                        ExecParam execParam_kezbTetelUpdate = execParam.Clone();
                        execParam_kezbTetelUpdate.Record_Id = erec_IraKezbesitesiTetel.Id;

                        Result result_kezbTetelUpdate = service_KezbesitesiTetelek.Update(execParam_kezbTetelUpdate, erec_IraKezbesitesiTetel);
                        if (!String.IsNullOrEmpty(result_kezbTetelUpdate.ErrorCode))
                        {
                            // hiba:
                            //ContextUtil.SetAbort();
                            //log.WsEnd(execParam_kezbTetelUpdate, result_kezbTetelUpdate);
                            //return result_kezbTetelUpdate;

                            throw new ResultException(result_kezbTetelUpdate);
                        }
                        else
                        {
                            // Ha minden ok:
                            result = result_kezbTetelUpdate;
                        }
                    }
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result Atvetel_Tomeges(ExecParam execParam, String[] erec_IraKezbesitesiTetelek_Id_Array)
    {
        //kor�bbi �gon m�k�dik, ahol a bejelentkezett felhaszn�l� veszi �t
        return Atvetel_Tomeges_Idegen(execParam, erec_IraKezbesitesiTetelek_Id_Array, execParam.Felhasznalo_Id);
    }

    //egy m�sik felhaszn�l� nev�ben �tv�tel (T�K)
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result Atvetel_Tomeges_Idegen(ExecParam execParam, String[] erec_IraKezbesitesiTetelek_Id_Array, String atvevoFelhasznaloId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (erec_IraKezbesitesiTetelek_Id_Array == null || erec_IraKezbesitesiTetelek_Id_Array.Length == 0)
            {
                // hiba
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52220);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52220);
            }
            else
            {
                Result result_atvetel = new Result();

                string selectedIds = Search.GetSqlInnerString(erec_IraKezbesitesiTetelek_Id_Array); //"'" + erec_IraKezbesitesiTetelek_Id_Array[0] + "'";
                //for (int i = 1; i < erec_IraKezbesitesiTetelek_Id_Array.Length; i++)
                //{
                //    selectedIds += ",'" + erec_IraKezbesitesiTetelek_Id_Array[i] + "'";
                //}

                EREC_IraKezbesitesiTetelekSearch search = new EREC_IraKezbesitesiTetelekSearch();
                search.Id.Value = selectedIds;
                search.Id.Operator = Query.Operators.inner;

                result_atvetel = GetAll(execParam.Clone(), search);
                if (!string.IsNullOrEmpty(result_atvetel.ErrorCode))
                    throw new ResultException(result_atvetel);

                string selectedUUIds = string.Empty;
                List<string> selectedUUIdsList = new List<string>();
                string selectedPIIds = string.Empty;
                string selectedKKIds = string.Empty;
                string selectedMIds = string.Empty;
                result.Ds = new DataSet();
                result.Ds.Tables.Add();
                result.Ds.Tables[0].Columns.Add("Id");

                // kijel�lt t�telek id-jainak �ssze�ll�t�sa
                foreach (DataRow r in result_atvetel.Ds.Tables[0].Rows)
                {
                    if (r["Allapot"].ToString() != KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                        && r["Allapot"].ToString() != KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott)
                        result.Ds.Tables[0].Rows.Add(r["Obj_Id"]);

                    string Obj_Id = r["Obj_Id"].ToString();
                    switch (r["Obj_type"].ToString())
                    {
                        case Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok:
                            selectedUUIds += ",'" + Obj_Id + "'";
                            selectedUUIdsList.Add(Obj_Id);
                            break;
                        case Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok:
                            selectedPIIds += ",'" + Obj_Id + "'";
                            break;
                        case Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek:
                            selectedKKIds += ",'" + Obj_Id + "'";
                            break;
                        case Contentum.eUtility.Constants.TableNames.KRT_Mappak:
                            selectedMIds += ",'" + Obj_Id + "'";
                            break;
                    }
                }

                if (result.Ds.Tables[0].Rows.Count > 0)
                    throw new ResultException(52222);

                // vessz�k lev�g�sa a konkaten�ci�k elej�r�l
                selectedUUIds = selectedUUIds.TrimStart(',');
                selectedPIIds = selectedPIIds.TrimStart(',');
                selectedKKIds = selectedKKIds.TrimStart(',');
                selectedMIds = selectedMIds.TrimStart(',');

                List<string> csoportIds = new List<string>();
                string obj_Ids = selectedPIIds;

                #region iratp�ld�nyok t�meges �tv�tele
                if (!string.IsNullOrEmpty(selectedPIIds))
                {
                    obj_Ids += ",";

                    EREC_PldIratPeldanyokSearch iratpeldanySearch = new EREC_PldIratPeldanyokSearch();
                    iratpeldanySearch.Id.Value = selectedPIIds;
                    iratpeldanySearch.Id.Operator = Query.Operators.inner;

                    EREC_PldIratPeldanyokService iratpeldanyService = new EREC_PldIratPeldanyokService(this.dataContext);
                    // GetAllWithExtension kell, hogy az eredm�nyb�l felt�lthet� legyen az iratp�ld�ny, irat �s �gyirat st�tusz is
                    Result iratpeldanyUpdateResult = iratpeldanyService.GetAllWithExtension(execParam.Clone(), iratpeldanySearch);
                    if (!string.IsNullOrEmpty(iratpeldanyUpdateResult.ErrorCode))
                        throw new ResultException(iratpeldanyUpdateResult);

                    //System.Collections.Hashtable iratpeldanyStatuszok = Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByDataSet(execParam.Clone(), iratpeldanyUpdateResult.Ds);
                    Hashtable iratPeldanyStatuszok;
                    Hashtable iratStatuszok;
                    Hashtable ugyiratStatuszok;

                    // statuszok felt�lt�se:
                    Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotHierarchiaByDataSet(execParam.Clone(), iratpeldanyUpdateResult.Ds
                        , out iratPeldanyStatuszok, out iratStatuszok, out ugyiratStatuszok);

                    if (iratPeldanyStatuszok == null || iratStatuszok == null || ugyiratStatuszok == null)
                    {
                        // Hiba az iratp�ld�nyok �llapot�nak ellen�rz�se sor�n!
                        throw new ResultException(52295);
                    }

                    EREC_PldIratPeldanyok erec_PldIratPeldany = new EREC_PldIratPeldanyok();
                    EREC_PldIratPeldanyokService service_iratPeldanyok = new EREC_PldIratPeldanyokService(this.dataContext);
                    ExecParam kezbesitesiTetelekUpdateExecParam = execParam.Clone();

                    selectedPIIds = string.Empty;
                    string selectedPIVers = string.Empty;
                    foreach (DataRow rk in iratpeldanyUpdateResult.Ds.Tables[0].Rows)
                    {
                        string pld_Id = rk["Id"].ToString();

                        if (!Contentum.eRecord.BaseUtility.IratPeldanyok.Atveheto(
                                iratPeldanyStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz
                                , ugyiratStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz
                                , execParam.Clone())
                            )
                        {
                            result.Ds.Tables[0].Rows.Add(rk["Id"]);
                        }
                        //throw new ResultException(52292);

                        // IsUserMember lista t�lt�s
                        if (!csoportIds.Contains(rk["Csoport_Id_Felelos"].ToString()))
                            csoportIds.Add(rk["Csoport_Id_Felelos"].ToString());

                        selectedPIIds += ",'" + rk["Id"].ToString() + "'";
                        selectedPIVers += "," + rk["Ver"].ToString();

                    }

                    if (result.Ds.Tables[0].Rows.Count > 0)
                        throw new ResultException(52292);

                    selectedPIIds = selectedPIIds.TrimStart(',');
                    selectedPIVers = selectedPIVers.TrimStart(',');

                    // iratp�ld�ny UPDATE:
                    iratpeldanyUpdateResult = service_iratPeldanyok.AtvetelTomeges(execParam.Clone(), selectedPIIds, selectedPIVers, atvevoFelhasznaloId);
                    if (!string.IsNullOrEmpty(iratpeldanyUpdateResult.ErrorCode))
                    {
                        result.Ds = iratpeldanyUpdateResult.Ds;
                        throw new ResultException(iratpeldanyUpdateResult);
                    }

                }
                #endregion

                #region �gyiratok t�meges �tv�tele
                if (!string.IsNullOrEmpty(selectedUUIds))
                {
                    obj_Ids += selectedUUIds + ",";

                    EREC_UgyUgyiratokSearch ugyiratSearch = new EREC_UgyUgyiratokSearch();
                    ugyiratSearch.Id.Value = selectedUUIds;
                    ugyiratSearch.Id.Operator = Query.Operators.inner;

                    EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                    Result ugyiratUpdateResult = erec_UgyUgyiratokService.GetAll(execParam.Clone(), ugyiratSearch);
                    if (!string.IsNullOrEmpty(ugyiratUpdateResult.ErrorCode))
                        throw new ResultException(ugyiratUpdateResult);

                    System.Collections.Hashtable ugyiratStatuszok = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), ugyiratUpdateResult.Ds);

                    selectedUUIds = string.Empty;
                    selectedUUIdsList.Clear();
                    string selectedUUVers = string.Empty;
                    List<string> selectedUUVersList = new List<string>();
                    foreach (DataRow rk in ugyiratUpdateResult.Ds.Tables[0].Rows)
                    {
                        if (!Contentum.eRecord.BaseUtility.Ugyiratok.Atveheto((ugyiratStatuszok[rk["Id"].ToString()] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz), execParam.Clone()))
                            result.Ds.Tables[0].Rows.Add(rk["Id"]);
                        //throw new ResultException(52232);

                        // IsUserMember lista t�lt�s
                        if (!csoportIds.Contains(rk["Csoport_Id_Felelos"].ToString()))
                            csoportIds.Add(rk["Csoport_Id_Felelos"].ToString());

                        #region �gyiratUpdate


                        // Iratt�rb�l k�lcs�nz�tt �llapotok figyel�se
                        if (rk["Allapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt
                            && (rk["TovabbitasAlattAllapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo
                                || rk["TovabbitasAlattAllapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott
                                || rk["TovabbitasAlattAllapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert)
                            )
                        {
                            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
                            ExecParam ugyiratExecParam = execParam.Clone();
                            ugyiratExecParam.Record_Id = rk["Id"].ToString();

                            erec_UgyUgyirat.Updated.SetValueAll(false);
                            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                            erec_UgyUgyirat.Base.Updated.Ver = true;
                            erec_UgyUgyirat.Base.Ver = rk["Ver"].ToString();
                            erec_UgyUgyirat.Id = rk["Id"].ToString();

                            if (rk["TovabbitasAlattAllapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo)
                            {
                                EREC_IrattariKikeroService kikeroService = new EREC_IrattariKikeroService(this.dataContext);
                                ExecParam kikeroExecParam = ugyiratExecParam.Clone();
                                kikeroExecParam.Record_Id = erec_UgyUgyirat.Id;

                                Result kikeroResult = kikeroService.KolcsonzesAtvetel(kikeroExecParam);
                                if (!string.IsNullOrEmpty(kikeroResult.ErrorCode))
                                {
                                    result.Ds.Tables[0].Rows.Add(rk["Id"]);
                                    throw new ResultException(kikeroResult);
                                }
                                continue;
                            }
                            else if (rk["TovabbitasAlattAllapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott)
                            {
                                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                                erec_UgyUgyirat.Updated.Allapot = true;

                                erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo = atvevoFelhasznaloId;
                                erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;

                                erec_UgyUgyirat.TovabbitasAlattAllapot = "";
                                erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                            }
                            else if (rk["TovabbitasAlattAllapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert)
                            {
                                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                                erec_UgyUgyirat.Updated.Allapot = true;

                                erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo = atvevoFelhasznaloId;
                                erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;

                                erec_UgyUgyirat.TovabbitasAlattAllapot = "";
                                erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                            }

                            //Felel�s �ll�t�sa az �tvev� felhaszn�l�ra, ha eddig szervezet volt
                            if (rk["Csoport_Id_Felelos"].ToString() != atvevoFelhasznaloId)
                            {
                                erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;
                                erec_UgyUgyirat.Csoport_Id_Felelos = atvevoFelhasznaloId;
                            }

                            ExecParam ugyiratExecParam_ugyiratUpdate = ugyiratExecParam.Clone();
                            ugyiratExecParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;


                            Result result_UgyiratUpdate = erec_UgyUgyiratokService.Update(ugyiratExecParam_ugyiratUpdate, erec_UgyUgyirat);
                            if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
                            {
                                // hiba:
                                //ContextUtil.SetAbort();
                                //log.WsEnd(ugyiratExecParam_ugyiratUpdate, result_UgyiratUpdate);
                                //return result_UgyiratUpdate;

                                result.Ds.Tables[0].Rows.Add(rk["Id"]);
                                throw new ResultException(result_UgyiratUpdate);
                            }
                            //result = result_UgyiratUpdate;

                        }
                        else
                        {
                            selectedUUIds += ",'" + rk["Id"].ToString() + "'";
                            selectedUUIdsList.Add(rk["Id"].ToString());
                            selectedUUVers += "," + rk["Ver"].ToString();
                            selectedUUVersList.Add(rk["Ver"].ToString());
                        }

                        #endregion
                    }

                    if (result.Ds.Tables[0].Rows.Count > 0)
                        throw new ResultException(52232);

                    selectedUUIds = selectedUUIds.TrimStart(',');
                    selectedUUVers = selectedUUVers.TrimStart(',');
                    // BUG_3786
                    if (selectedUUIdsList.Count > 0)
                    {
                        result_atvetel = erec_UgyUgyiratokService.AtvetelTomegesInternal(execParam.Clone(), selectedUUIdsList.ToArray(), selectedUUVersList.ToArray(), atvevoFelhasznaloId);
                        if (!string.IsNullOrEmpty(result_atvetel.ErrorCode))
                        {
                            result.Ds = result_atvetel.Ds;
                            throw new ResultException(result_atvetel);
                        }
                    }
                }
                #endregion

                #region k�ldem�nyek t�meges �tv�tele
                if (!string.IsNullOrEmpty(selectedKKIds))
                {
                    obj_Ids += selectedKKIds + ",";
                    EREC_KuldKuldemenyekSearch kuldemenySearch = new EREC_KuldKuldemenyekSearch();
                    kuldemenySearch.Id.Value = selectedKKIds;
                    kuldemenySearch.Id.Operator = Query.Operators.inner;

                    EREC_KuldKuldemenyekService kuldemenyService = new EREC_KuldKuldemenyekService(this.dataContext);
                    Result kuldemenyUpdateResult = kuldemenyService.GetAll(execParam.Clone(), kuldemenySearch);
                    if (!string.IsNullOrEmpty(kuldemenyUpdateResult.ErrorCode))
                        throw new ResultException(kuldemenyUpdateResult);

                    System.Collections.Hashtable kuldemenyStatuszok = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByDataSet(execParam.Clone(), kuldemenyUpdateResult.Ds);
                    EREC_KuldKuldemenyek erec_KuldKuldemenyek = new EREC_KuldKuldemenyek();
                    EREC_KuldKuldemenyekService service_kuldemenyek = new EREC_KuldKuldemenyekService(this.dataContext);
                    ExecParam kezbesitesiTetelekUpdateExecParam = execParam.Clone();

                    selectedKKIds = string.Empty;
                    string selectedKKVers = string.Empty;
                    foreach (DataRow rk in kuldemenyUpdateResult.Ds.Tables[0].Rows)
                    {
                        selectedKKIds += ",'" + rk["Id"].ToString() + "'";
                        selectedKKVers += "," + rk["Ver"].ToString();

                        if (!Contentum.eRecord.BaseUtility.Kuldemenyek.Atveheto(execParam.Clone(), (kuldemenyStatuszok[rk["Id"].ToString()] as Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz)))
                            result.Ds.Tables[0].Rows.Add(rk["Id"]);

                        // IsUserMember lista t�lt�s
                        if (!csoportIds.Contains(rk["Csoport_Id_Felelos"].ToString()))
                            csoportIds.Add(rk["Csoport_Id_Felelos"].ToString());
                    }

                    if (result.Ds.Tables[0].Rows.Count > 0)
                        throw new ResultException(52391);

                    selectedKKIds = selectedKKIds.TrimStart(',');
                    selectedKKVers = selectedKKVers.TrimStart(',');

                    // k�ldem�ny UPDATE:
                    Result kuldemenyekUpdateResult = service_kuldemenyek.Atvetel_Tomeges(execParam.Clone(), selectedKKIds, selectedKKVers, atvevoFelhasznaloId);
                    if (!string.IsNullOrEmpty(kuldemenyekUpdateResult.ErrorCode))
                    {
                        result.Ds = kuldemenyekUpdateResult.Ds;
                        throw new ResultException(kuldemenyekUpdateResult);
                    }
                }
                #endregion

                #region dosszi�k t�meges �tv�tele

                if (!string.IsNullOrEmpty(selectedMIds))
                {
                    obj_Ids += selectedMIds + ",";

                    KRT_MappakService mappakService = new KRT_MappakService(this.dataContext);

                    result_atvetel = mappakService.Atvetel_Tomeges(execParam.Clone(), selectedMIds, atvevoFelhasznaloId);

                    if (!string.IsNullOrEmpty(result_atvetel.ErrorCode))
                    {
                        result.Ds = result_atvetel.Ds;
                        throw new ResultException(result_atvetel);
                    }
                }

                #endregion dosszi�k t�meges �tv�tele

                obj_Ids = obj_Ids.TrimEnd(',');

                #region IsMember ellen�rz�s az �sszes csoportra
                RightsService service_isMember = new RightsService(this.dataContext);
                Result isMemberResult = new Result();
                result.Ds = new DataSet();
                result.Ds.Tables.Add();
                result.Ds.Tables[0].Columns.Add("Id");

                List<string> failedCsoportMembers = new List<string>();
                foreach (string s in csoportIds)
                {
                    if (!string.IsNullOrEmpty(service_isMember.IsUserMember(execParam.Clone(), s).ErrorCode))
                    {
                        failedCsoportMembers.Add(s);
                    }
                }

                if (failedCsoportMembers.Count > 0)
                {
                    string csoportok = "";
                    foreach (string s in failedCsoportMembers)
                        csoportok += "'" + s + "',";
                    csoportok = csoportok.TrimEnd(',');

                    #region �gyiratok
                    EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);
                    EREC_UgyUgyiratokSearch ugyiratSearch = new EREC_UgyUgyiratokSearch();
                    ugyiratSearch.Id.Value = selectedUUIds;
                    ugyiratSearch.Id.Operator = Query.Operators.inner;

                    ugyiratSearch.Csoport_Id_Felelos.Value = csoportok;
                    ugyiratSearch.Csoport_Id_Felelos.Operator = Query.Operators.inner;

                    Result ugyiratGetAllResult = ugyiratService.GetAll(execParam.Clone(), ugyiratSearch);
                    if (ugyiratGetAllResult.Ds != null && ugyiratGetAllResult.Ds.Tables.Count != 0)
                    {
                        foreach (DataRow r in ugyiratGetAllResult.Ds.Tables[0].Rows)
                            result.Ds.Tables[0].Rows.Add(r["Id"]);
                    }
                    #endregion

                    #region Iratp�ld�nyok
                    EREC_PldIratPeldanyokService iratpeldanyService = new EREC_PldIratPeldanyokService(this.dataContext);
                    EREC_PldIratPeldanyokSearch iratpeldanySearch = new EREC_PldIratPeldanyokSearch();
                    iratpeldanySearch.Id.Value = selectedPIIds;
                    iratpeldanySearch.Id.Operator = Query.Operators.inner;

                    iratpeldanySearch.Csoport_Id_Felelos.Value = csoportok;
                    iratpeldanySearch.Csoport_Id_Felelos.Operator = Query.Operators.inner;

                    Result iratpeldanyGetAllResult = iratpeldanyService.GetAll(execParam.Clone(), iratpeldanySearch);
                    if (iratpeldanyGetAllResult.Ds != null && iratpeldanyGetAllResult.Ds.Tables.Count != 0)
                    {
                        foreach (DataRow r in iratpeldanyGetAllResult.Ds.Tables[0].Rows)
                            result.Ds.Tables[0].Rows.Add(r["Id"]);
                    }
                    #endregion

                    #region K�ldem�nyek
                    EREC_KuldKuldemenyekService kuldemenyService = new EREC_KuldKuldemenyekService(this.dataContext);
                    EREC_KuldKuldemenyekSearch kuldemenySearch = new EREC_KuldKuldemenyekSearch();
                    kuldemenySearch.Id.Value = selectedKKIds;
                    kuldemenySearch.Id.Operator = Query.Operators.inner;

                    kuldemenySearch.Csoport_Id_Felelos.Value = csoportok;
                    kuldemenySearch.Csoport_Id_Felelos.Operator = Query.Operators.inner;

                    Result kuldemenyGetAllResult = kuldemenyService.GetAll(execParam.Clone(), kuldemenySearch);
                    if (kuldemenyGetAllResult.Ds != null && kuldemenyGetAllResult.Ds.Tables.Count != 0)
                    {
                        foreach (DataRow r in kuldemenyGetAllResult.Ds.Tables[0].Rows)
                            result.Ds.Tables[0].Rows.Add(r["Id"]);
                    }
                    #endregion

                    throw new ResultException(52393);
                }
                #endregion

                #region K�zbes�t�si t�telek update
                result_atvetel = SetKezbesitesiTetelekToAtvettTomeges(execParam.Clone(), obj_Ids);
                if (!string.IsNullOrEmpty(result_atvetel.ErrorCode))
                {
                    result.Ds = result_atvetel.Ds;
                    throw new ResultException(result_atvetel);
                }

                #endregion

                #region Esem�nynapl�z�s
                if (isTransactionBeginHere)
                {
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                    Result eventLogResult = eventLogService.InsertTomeges(execParam, selectedIds, "EREC_IraKezbesitesiTetelek", "Modify");
                    if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                        Logger.Debug("[ERROR]EREC_IraKezbesitesiTetelek::Modositas(AtvetelTomeges): ", execParam, eventLogResult);
                }
                #endregion

                result = result_atvetel;
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            Result errorResult = ResultException.GetResultFromException(e);
            result.ErrorCode = errorResult.ErrorCode;
            result.ErrorMessage = errorResult.ErrorMessage;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result Atvetel(ExecParam execParam, String erec_IraKezbesitesiTetelek_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        if (execParam == null || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52220);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // K�zbes�t�si t�tel lek�r�se:
            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbtetelGet = execParam.Clone();
            execParam_kezbtetelGet.Record_Id = erec_IraKezbesitesiTetelek_Id;

            Result result_kezbTetelGet = service_KezbesitesiTetelek.Get(execParam_kezbtetelGet);
            if (!String.IsNullOrEmpty(result_kezbTetelGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_kezbtetelGet, result_kezbTetelGet);
                //return result_kezbTetelGet;

                throw new ResultException(result_kezbTetelGet);
            }

            if (result_kezbTetelGet.Record == null)
            {
                // hiba:
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52211);
                //log.WsEnd(execParam_kezbtetelGet, result1);
                //return result1;

                throw new ResultException(52211);
            }
            else
            {
                EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetel =
                    (EREC_IraKezbesitesiTetelek)result_kezbTetelGet.Record;

                if (erec_IraKezbesitesiTetel.Allapot != KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                    && erec_IraKezbesitesiTetel.Allapot != KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott)
                {
                    // Nem j� az �llapot:
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52222);
                    //log.WsEnd(execParam_kezbtetelGet, result1);
                    //return result1;

                    throw new ResultException(52222);
                }
                else
                {
                    /// T�tel �llapot�t�l f�gg�en megh�vjuk az �gyirat/Iratp�ld�ny/K�ldem�ny �tv�tel met�dus�t
                    /// (azok fogj�k majd a k�zbes�t�si t�telt is m�dos�tani)
                    /// 

                    switch (erec_IraKezbesitesiTetel.Obj_type)
                    {
                        case Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok:
                            {
                                EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                                ExecParam execParam_ugyiratAtvetel = execParam.Clone();

                                Result result_UgyiratAtvetel =
                                    ugyiratokService.Atvetel(execParam_ugyiratAtvetel, erec_IraKezbesitesiTetel.Obj_Id);

                                result = result_UgyiratAtvetel;
                                break;
                            }
                        case Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek:
                            {
                                EREC_KuldKuldemenyekService kuldemenyekService = new EREC_KuldKuldemenyekService(this.dataContext);
                                ExecParam execParam_kuldemenyAtvetel = execParam.Clone();

                                Result result_kuldemenyAtvetel =
                                    kuldemenyekService.Atvetel(execParam_kuldemenyAtvetel, erec_IraKezbesitesiTetel.Obj_Id);

                                result = result_kuldemenyAtvetel;
                                break;
                            }
                        case Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok:
                            {
                                EREC_PldIratPeldanyokService iratPeldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
                                ExecParam execParam_iratPldAtvetel = execParam.Clone();

                                Result result_IratPldAtvetel =
                                    iratPeldanyokService.Atvetel(execParam, erec_IraKezbesitesiTetel.Obj_Id);

                                result = result_IratPldAtvetel;
                                break;
                            }
                        default:
                            // hiba (�rv�nytelen t�pus)
                            //Result result1 = ResultError.CreateNewResultWithErrorCode(52223);
                            //log.WsEnd(execParam_kezbtetelGet, result1);
                            //return result1;

                            throw new ResultException(52223);
                    }
                }
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    /// <summary>
    /// K�zbes�t�si t�tel (�t nem vett) lek�r�se az objektum adatai alapj�n (azonos�t�, t�pus)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="Obj_Id">K�zbes�tett objektum azonos�t�ja</param>
    /// <param name="Obj_type">K�zbes�tett objektum t�pusa (t�blan�v)</param>
    /// <returns></returns>
    public Result GetKezbesitesitetelByObject(ExecParam execParam, String Obj_Id, string Obj_type)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        if (execParam == null || String.IsNullOrEmpty(Obj_Id) || String.IsNullOrEmpty(Obj_type))
        {
            // hiba
            // Hiba a k�zbes�t�si t�tel lek�r�sekor objektum adatok alapj�n: Hi�nyz� param�ter(ek)!
            Result result1 = ResultError.CreateNewResultWithErrorCode(53770);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // K�zbes�t�si t�tel lek�r�se:
            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbtetelGetAll = execParam.Clone();

            EREC_IraKezbesitesiTetelekSearch search = new EREC_IraKezbesitesiTetelekSearch();
            search.Obj_Id.Value = Obj_Id;
            search.Obj_Id.Operator = Query.Operators.equals;

            search.Obj_type.Value = Obj_type;
            search.Obj_type.Operator = Query.Operators.equals;

            // a felhaszn�l�bar�tabb visszajelz�s �rdek�ben az �tadott mellett
            // a t�bbi "tov�bb�t�s alatti" �llapot�t is lek�rj�k, �s ellen�rizz�k
            //search.Allapot.Value = Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott;
            //search.Allapot.Operator = Query.Operators.equals;

            search.Allapot.Value = Search.GetSqlInnerString(new string[] { KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott,
                KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt,
                KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott });
            search.Allapot.Operator = Query.Operators.inner;

            Result result_kezbTetelGetAll = service_KezbesitesiTetelek.GetAll(execParam_kezbtetelGetAll, search);
            if (result_kezbTetelGetAll.IsError)
            {
                // hiba:
                throw new ResultException(result_kezbTetelGetAll);
            }

            if (result_kezbTetelGetAll.Ds.Tables[0].Rows.Count != 1)
            {
                Logger.Warn(String.Format("A kapott k�zbes�t�si t�telek sz�ma: {0}", result_kezbTetelGetAll.Ds.Tables[0].Rows.Count));
                // hiba:
                // Az objektumhoz nem tal�lhat� �t nem vett k�zbes�t�si t�tel (vagy t�bb is tal�lhat�)!
                throw new ResultException(53754);
            }
            else
            {
                EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek = new EREC_IraKezbesitesiTetelek();
                Utility.LoadBusinessDocumentFromDataRow(erec_IraKezbesitesiTetelek, result_kezbTetelGetAll.Ds.Tables[0].Rows[0]);

                result.Record = erec_IraKezbesitesiTetelek;

            }

            #region Esem�nynapl�z�s
            // napl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, Obj_Id, Obj_type, "KezbesitesiTetelView").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #region Visszak�ld�s
    public static Result CheckKezbesitesiTetelVisszakuldheto(ExecParam execParam, EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek)
    {
        Result result = null;
        if (execParam == null || erec_IraKezbesitesiTetelek == null
            || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_Id)
            || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_type))
        {
            // Param�ter hiba:
            // Hiba a visszak�ld�s sor�n: hi�nyz� param�ter(ek)!
            result = ResultError.CreateNewResultWithErrorCode(53700);
        }
        else if (erec_IraKezbesitesiTetelek.Allapot != Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott)
        {
            // Nem j� az �llapot:
            if (erec_IraKezbesitesiTetelek.Allapot == Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott
                && execParam.Felhasznalo_Id == erec_IraKezbesitesiTetelek.Csoport_Id_Cel)
            {
                // A t�tel nem k�ldhet� vissza, mert azt visszak�ldt�k �nnek, mint eredeti felad�nak!
                ErrorDetails errorDetail = new ErrorDetails(53705
                    , Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.EREC_IraKezbesitesiTetelek
                    , erec_IraKezbesitesiTetelek.Obj_Id
                    , Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.Felhasznalo_Id_Atado_USER
                    , erec_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER);
                // A k�zbes�t�si t�tel nem k�ldhet� vissza!
                result = ResultError.CreateNewResultWithErrorCode(53709, errorDetail);
            }
            else
            {
                // A t�tel nem k�ldhet� vissza, mert a k�zbes�t�s �llapota nem megfelel�.
                ErrorDetails errorDetail = new ErrorDetails(53701
                    , Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.EREC_IraKezbesitesiTetelek
                    , erec_IraKezbesitesiTetelek.Obj_Id
                    , Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.Allapot
                    , erec_IraKezbesitesiTetelek.Allapot);
                // A k�zbes�t�si t�tel nem k�ldhet� vissza!
                result = ResultError.CreateNewResultWithErrorCode(53709, errorDetail);
            }
        }
        else if (erec_IraKezbesitesiTetelek.Obj_type != Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok
                && erec_IraKezbesitesiTetelek.Obj_type != Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek
                && erec_IraKezbesitesiTetelek.Obj_type != Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok
                && erec_IraKezbesitesiTetelek.Obj_type != Contentum.eUtility.Constants.TableNames.KRT_Mappak)
        {
            Logger.Error(String.Format("Kapott objektum t�pus: {0}", erec_IraKezbesitesiTetelek.Obj_type));
            // �rv�nytelen t�pus:
            // Hiba a visszak�ld�s sor�n: ismeretlen t�pus� iratkezel�si objektum!
            result = ResultError.CreateNewResultWithErrorCode(53706);
        }
        else if (execParam.Felhasznalo_Id.Equals(erec_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER, StringComparison.InvariantCultureIgnoreCase))
        {
            // A t�tel nem k�ldhet� vissza, mert azt �n adta �t �s csak a c�mzett k�ldheti vissza �nnek!
            ErrorDetails errorDetail = new ErrorDetails(53702
                , Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.EREC_IraKezbesitesiTetelek
                , erec_IraKezbesitesiTetelek.Obj_Id
                , Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.Csoport_Id_Cel
                , erec_IraKezbesitesiTetelek.Csoport_Id_Cel);
            // A k�zbes�t�si t�tel nem k�ldhet� vissza!
            result = ResultError.CreateNewResultWithErrorCode(53709, errorDetail);
        }
        else
        {
            // ha minden OK:
            result = new Result();
        }

        return result;
    }

    /// <summary>
    /// �tvenni nem k�v�nt t�tel visszak�ld�se az �tad�nak, a vsszak�ldend� objektum adatai alapj�n (azonos�t�, t�pus)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="Obj_Id">Visszak�ldend� objektum azonos�t�ja</param>
    /// <param name="Obj_type">Visszak�ldend� objektum t�pusa (t�blan�v)</param>
    /// <param name="visszakuldesIndoka"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result VisszakuldesByObject(ExecParam execParam, String Obj_Id, string Obj_type, string visszakuldesIndoka)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        if (execParam == null || String.IsNullOrEmpty(Obj_Id) || String.IsNullOrEmpty(Obj_type))
        {
            // hiba
            // Hiba a visszak�ld�s sor�n: hi�nyz� param�ter(ek)!
            Result result1 = ResultError.CreateNewResultWithErrorCode(53700);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // K�zbes�t�si t�tel lek�r�se:
            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbtetelGet = execParam.Clone();

            Result result_kezbTetelGet = service_KezbesitesiTetelek.GetKezbesitesitetelByObject(execParam_kezbtetelGet, Obj_Id, Obj_type);
            if (result_kezbTetelGet.IsError)
            {
                // hiba:
                throw new ResultException(result_kezbTetelGet);
            }

            EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek = (EREC_IraKezbesitesiTetelek)result_kezbTetelGet.Record;

            result = this.VisszakuldesInternal(execParam, erec_IraKezbesitesiTetelek, visszakuldesIndoka);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            // napl�z�s a VisszakuldesInternal private met�dusban

            //// napl�z�s (objektum t�pust�l f�gg� bejegyz�s)
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, Obj_Id, Obj_type, functionRight).Record;
            //    if (eventLogRecord != null && !String.IsNullOrEmpty(visszakuldesIndoka))
            //    {
            //        eventLogRecord.Base.Note = visszakuldesIndoka;
            //    }

            //    eventLogService.Insert(execParam, eventLogRecord);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// �tvenni nem k�v�nt t�tel visszak�ld�se az �tad�nak, az objektumt�pust�l f�gg� met�dusok megh�v�sa
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_IraKezbesitesiTetelek">Az objektum adatait tartalmaz�, �tadott �llapot� k�zbes�t�si t�tel</param>
    /// <returns></returns>
    private Result VisszakuldesByObjectType(ExecParam execParam, EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek)
    {
        Logger.InfoStart("VisszakuldesByObjectType START");

        #region K�zbes�t�si t�tel ellen�rz�s
        Result result_kezbesitesiTetelCheck = EREC_IraKezbesitesiTetelekService.CheckKezbesitesiTetelVisszakuldheto(execParam, erec_IraKezbesitesiTetelek);
        if (result_kezbesitesiTetelCheck.IsError)
        {
            throw new ResultException(result_kezbesitesiTetelCheck);
        }
        #endregion K�zbes�t�si t�tel ellen�rz�s

        Result result = new Result();

        ExecParam execParam_visszakuldes = execParam.Clone();

        switch (erec_IraKezbesitesiTetelek.Obj_type)
        {
            case Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok:
                {
                    EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);

                    result = ugyiratokService.Visszakuldes(execParam_visszakuldes, erec_IraKezbesitesiTetelek);

                    break;
                }
            case Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek:
                {
                    EREC_KuldKuldemenyekService kuldemenyekService = new EREC_KuldKuldemenyekService(this.dataContext);

                    result = kuldemenyekService.Visszakuldes(execParam_visszakuldes, erec_IraKezbesitesiTetelek);

                    break;
                }
            case Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok:
                {
                    EREC_PldIratPeldanyokService iratPeldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);

                    result = iratPeldanyokService.Visszakuldes(execParam_visszakuldes, erec_IraKezbesitesiTetelek);

                    break;
                }
            case Contentum.eUtility.Constants.TableNames.KRT_Mappak:
                {
                    KRT_MappakService mappakService = new KRT_MappakService(this.dataContext);

                    result = mappakService.Visszakuldes(execParam_visszakuldes, erec_IraKezbesitesiTetelek);

                    break;
                }
            default:
                // hiba (�rv�nytelen t�pus)
                // Hiba a visszak�ld�s sor�n: ismeretlen t�pus� iratkezel�si objektum!
                throw new ResultException(53706);
        }
        //}

        if (result.IsError)
        {
            throw new ResultException(result);
        }

        Logger.InfoEnd("VisszakuldesByObjectType END");
        return result;
    }

    /// <summary>
    /// �tvenni nem k�v�nt t�tel visszak�ld�se az �tad�nak
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kezbesitesiTetel_Id"></param>
    /// <param name="visszakuldesIndoka"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result Visszakuldes(ExecParam execParam, String kezbesitesiTetel_Id, string visszakuldesIndoka)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        if (execParam == null || String.IsNullOrEmpty(kezbesitesiTetel_Id))
        {
            // hiba
            // Hiba a visszak�ld�s sor�n: hi�nyz� param�ter(ek)!
            Result result1 = ResultError.CreateNewResultWithErrorCode(53700);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // K�zbes�t�si t�tel lek�r�se:
            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbtetelGet = execParam.Clone();
            execParam_kezbtetelGet.Record_Id = kezbesitesiTetel_Id;

            Result result_kezbTetelGet = service_KezbesitesiTetelek.Get(execParam_kezbtetelGet);
            if (result_kezbTetelGet.IsError)
            {
                // hiba:
                throw new ResultException(result_kezbTetelGet);
            }

            if (result_kezbTetelGet.Record == null)
            {
                // hiba:
                // Hiba a visszak�ld�s sor�n: nem tal�lhat� a megfelel� k�zbes�t�si t�tel!
                throw new ResultException(53704);
            }
            else
            {
                EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek =
                    (EREC_IraKezbesitesiTetelek)result_kezbTetelGet.Record;

                result = this.VisszakuldesInternal(execParam, erec_IraKezbesitesiTetelek, visszakuldesIndoka);
            }

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            // napl�z�s az objektum t�pus szerint h�vott met�dusokban
            //// napl�z�s
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_IraKezbesitesiTetelek_Id, "EREC_IraKezbesitesitetelek", "KezbesitesiTetelVisszakuldes").Record;
            //    if (eventLogRecord != null && !String.IsNullOrEmpty(visszakuldesIndoka))
            //    {
            //        eventLogRecord.Base.Note = visszakuldesIndoka;
            //    }

            //    eventLogService.Insert(execParam, eventLogRecord);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private Result VisszakuldesInternal(ExecParam execParam, EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek, string visszakuldesIndoka)
    {
        Logger.InfoStart("VisszakuldesInternal START");

        #region K�zbes�t�si t�tel ellen�rz�s
        Result result_kezbesitesiTetelCheck = EREC_IraKezbesitesiTetelekService.CheckKezbesitesiTetelVisszakuldheto(execParam, erec_IraKezbesitesiTetelek);
        if (result_kezbesitesiTetelCheck.IsError)
        {
            throw new ResultException(result_kezbesitesiTetelCheck);
        }
        #endregion K�zbes�t�si t�tel ellen�rz�s

        Result result = new Result();

        string functionRight = null;

        erec_IraKezbesitesiTetelek.Base.Note = visszakuldesIndoka;
        erec_IraKezbesitesiTetelek.Base.Updated.Note = true;

        #region Objektum t�pus szerinti m�veletek
        Result result_ByObjectType = this.VisszakuldesByObjectType(execParam, erec_IraKezbesitesiTetelek);

        if (result_ByObjectType.IsError)
        {
            throw new ResultException(result_ByObjectType);
        }

        switch (erec_IraKezbesitesiTetelek.Obj_type)
        {
            case Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok:
                functionRight = "UgyiratVisszakuldes";
                break;
            case Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek:
                functionRight = "KuldemenyVisszakuldes";
                break;
            case Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok:
                functionRight = "IratPeldanyVisszakuldes";
                break;
            default:
                functionRight = "KezbesitesiTetelVisszakuldes";
                break;
        }
        #endregion Objektum t�pus szerinti m�veletek

        #region K�zbes�t�si fej l�trehoz�sa
        // K�zbes�t�si fej l�trehoz�sa
        EREC_IraKezbesitesiFejek erec_IraKezbesitesiFejek = new EREC_IraKezbesitesiFejek();

        erec_IraKezbesitesiFejek.CsomagAzonosito = "Visszakuldes";
        erec_IraKezbesitesiFejek.Updated.CsomagAzonosito = true;

        erec_IraKezbesitesiFejek.Tipus = "A";   // �tad�s
        erec_IraKezbesitesiFejek.Updated.Tipus = true;

        EREC_IraKezbesitesiFejekService service_kezbesitesiFejek = new EREC_IraKezbesitesiFejekService(this.dataContext);
        Result result_kezbesitesiFejekInsert = service_kezbesitesiFejek.Insert(execParam.Clone(), erec_IraKezbesitesiFejek);
        if (result_kezbesitesiFejekInsert.IsError)
        {
            throw new ResultException(result_kezbesitesiFejekInsert);
        }

        string kezbesitesiFej_Id = result_kezbesitesiFejekInsert.Uid;
        #endregion K�zbes�t�si fej l�trehoz�sa

        /// K�zbes�t�si t�tel visszaford�t�sa, �llapot visszak�ld�ttre �ll�t�sa, visszak�ld�s (�tad�s) d�tuma
        /// �j k�zbes�t�si fej l�trehoz�sa (Visszak�ld�tt)
        erec_IraKezbesitesiTetelek.Updated.SetValueAll(false);
        erec_IraKezbesitesiTetelek.Base.Updated.SetValueAll(false);
        erec_IraKezbesitesiTetelek.Base.Updated.Ver = true;

        string eredeti_cimzett = erec_IraKezbesitesiTetelek.Csoport_Id_Cel;
        erec_IraKezbesitesiTetelek.Csoport_Id_Cel = erec_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER;
        erec_IraKezbesitesiTetelek.Updated.Csoport_Id_Cel = true;

        erec_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER = execParam.Felhasznalo_Id;
        erec_IraKezbesitesiTetelek.Updated.Felhasznalo_Id_Atado_USER = true;

        erec_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_LOGIN = execParam.LoginUser_Id;
        erec_IraKezbesitesiTetelek.Updated.Felhasznalo_Id_Atado_LOGIN = true;

        erec_IraKezbesitesiTetelek.Allapot = KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott;
        erec_IraKezbesitesiTetelek.Updated.Allapot = true;

        erec_IraKezbesitesiTetelek.AtadasDat = DateTime.Now.ToString();
        erec_IraKezbesitesiTetelek.Updated.AtadasDat = true;

        erec_IraKezbesitesiTetelek.KezbesitesFej_Id = kezbesitesiFej_Id;
        erec_IraKezbesitesiTetelek.Updated.KezbesitesFej_Id = true;

        // visszak�ld�s indoka
        erec_IraKezbesitesiTetelek.Base.Updated.Note = true;


        ExecParam execParam_update = execParam.Clone();
        execParam_update.Record_Id = erec_IraKezbesitesiTetelek.Id;
        Result result_update = Update(execParam_update, erec_IraKezbesitesiTetelek);

        if (result_update.IsError)
        {
            throw new ResultException(result_update);
        }

        result = result_update;

        #region Esem�nynapl�z�s
        // napl�z�s (objektum t�pust�l f�gg� bejegyz�s)
        KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

        KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_IraKezbesitesiTetelek.Obj_Id, erec_IraKezbesitesiTetelek.Obj_type, functionRight).Record;
        if (eventLogRecord != null && !String.IsNullOrEmpty(visszakuldesIndoka))
        {
            eventLogRecord.Base.Note = visszakuldesIndoka;
        }

        eventLogService.Insert(execParam, eventLogRecord);
        #endregion Esem�nynapl�z�s

        Logger.InfoEnd("VisszakuldesInternal END");
        return result;
    }
    #endregion Visszak�ld�s

    #region Visszak�ld�s iratt�rb�l

    public static Result CheckKezbesitesiTetelVisszakuldhetoIrattarbol(ExecParam execParam, EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek)
    {
        Result result = null;
        if (execParam == null || erec_IraKezbesitesiTetelek == null
            || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_Id)
            || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_type))
        {
            // Param�ter hiba:
            // Hiba az iratt�rb�l visszak�ld�s sor�n: hi�nyz� param�ter(ek)!
            result = ResultError.CreateNewResultWithErrorCode(53750);
        }
        else if (erec_IraKezbesitesiTetelek.Allapot != Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott)
        {
            // Nem j� az �llapot:
            //if (erec_IraKezbesitesiTetelek.Allapot == Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott
            //    && execParam.Felhasznalo_Id == erec_IraKezbesitesiTetelek.Csoport_Id_Cel)
            //{
            //    // A t�tel nem k�ldhet� vissza, mert azt visszak�ldt�k �nnek, mint eredeti felad�nak!
            //    ErrorDetails errorDetail = new ErrorDetails(53705
            //        , Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.EREC_IraKezbesitesiTetelek
            //        , erec_IraKezbesitesiTetelek.Obj_Id
            //        , Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.Felhasznalo_Id_Atado_USER
            //        , erec_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER);
            //    // A k�zbes�t�si t�tel nem k�ldhet� vissza!
            //    result = ResultError.CreateNewResultWithErrorCode(53709, errorDetail);
            //}
            //else
            //{
                // A t�tel nem k�ldhet� vissza, mert a k�zbes�t�s �llapota nem megfelel�.
                ErrorDetails errorDetail = new ErrorDetails(53701
                    , Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.EREC_IraKezbesitesiTetelek
                    , erec_IraKezbesitesiTetelek.Obj_Id
                    , Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.Allapot
                    , erec_IraKezbesitesiTetelek.Allapot);
                // A k�zbes�t�si t�tel nem k�ldhet� vissza!
                result = ResultError.CreateNewResultWithErrorCode(53709, errorDetail);
            //}
        }
        else if (erec_IraKezbesitesiTetelek.Obj_type != Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok)
        {
            Logger.Error(String.Format("Kapott objektum t�pus: {0}", erec_IraKezbesitesiTetelek.Obj_type));
            // �rv�nytelen t�pus:
            // Hiba az iratt�rb�l visszak�ld�s sor�n: nem megfelel� t�pus� iratkezel�si objektum (nem �gyirat)!
            result = ResultError.CreateNewResultWithErrorCode(53757);
        }
        else
        {
            // ha minden OK:
            result = new Result();
        }

        return result;
    }


    /// <summary>
    /// Iratt�rba �tvenni nem k�v�nt t�tel visszak�ld�se az �tad�nak
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kezbesitesiTetel_Id"></param>
    /// <param name="visszakuldesIndoka"></param>
    /// <param name="atmenetiIrattar_Id">Ha k�zponti iratt�rb�l �tmeneti iratt�rba k�ldik vissza, itt meg kell adni a c�mzett iratt�rat</param>
    /// <returns></returns>
    public Result VisszakuldesIrattarbolInternal(ExecParam execParam, EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek, string visszakuldesIndoka, string atmenetiIrattar_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region K�zbes�t�si t�tel ellen�rz�s
            Result result_kezbesitesiTetelCheck = EREC_IraKezbesitesiTetelekService.CheckKezbesitesiTetelVisszakuldhetoIrattarbol(execParam, erec_IraKezbesitesiTetelek);
            if (result_kezbesitesiTetelCheck.IsError)
            {
                throw new ResultException(result_kezbesitesiTetelCheck);
            }
            #endregion K�zbes�t�si t�tel ellen�rz�s

            #region K�zbes�t�si fej l�trehoz�sa
            // K�zbes�t�si fej l�trehoz�sa
            EREC_IraKezbesitesiFejek erec_IraKezbesitesiFejek = new EREC_IraKezbesitesiFejek();

            erec_IraKezbesitesiFejek.CsomagAzonosito = "VisszakuldesIrattarbol";
            erec_IraKezbesitesiFejek.Updated.CsomagAzonosito = true;

            erec_IraKezbesitesiFejek.Tipus = "A";   // �tad�s
            erec_IraKezbesitesiFejek.Updated.Tipus = true;

            EREC_IraKezbesitesiFejekService service_kezbesitesiFejek = new EREC_IraKezbesitesiFejekService(this.dataContext);
            Result result_kezbesitesiFejekInsert = service_kezbesitesiFejek.Insert(execParam.Clone(), erec_IraKezbesitesiFejek);
            if (result_kezbesitesiFejekInsert.IsError)
            {
                throw new ResultException(result_kezbesitesiFejekInsert);
            }

            string kezbesitesiFej_Id = result_kezbesitesiFejekInsert.Uid;
            #endregion K�zbes�t�si fej l�trehoz�sa

            /// K�zbes�t�si t�tel visszaford�t�sa, �llapot visszak�ld�ttre �ll�t�sa, visszak�ld�s (�tad�s) d�tuma
            /// �j k�zbes�t�si fej l�trehoz�sa (Visszak�ld�tt)
            erec_IraKezbesitesiTetelek.Updated.SetValueAll(false);
            erec_IraKezbesitesiTetelek.Base.Updated.SetValueAll(false);
            erec_IraKezbesitesiTetelek.Base.Updated.Ver = true;

            //string eredeti_cimzett = erec_IraKezbesitesiTetelek.Csoport_Id_Cel;
            if (String.IsNullOrEmpty(atmenetiIrattar_Id))
            {
                erec_IraKezbesitesiTetelek.Csoport_Id_Cel = erec_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER;
                erec_IraKezbesitesiTetelek.Updated.Csoport_Id_Cel = true;
            }
            else
            {
                erec_IraKezbesitesiTetelek.Csoport_Id_Cel = atmenetiIrattar_Id;
                erec_IraKezbesitesiTetelek.Updated.Csoport_Id_Cel = true;
            }

            erec_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER = execParam.Felhasznalo_Id;
            erec_IraKezbesitesiTetelek.Updated.Felhasznalo_Id_Atado_USER = true;

            erec_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_LOGIN = execParam.LoginUser_Id;
            erec_IraKezbesitesiTetelek.Updated.Felhasznalo_Id_Atado_LOGIN = true;

            erec_IraKezbesitesiTetelek.Allapot = KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott;
            erec_IraKezbesitesiTetelek.Updated.Allapot = true;

            erec_IraKezbesitesiTetelek.AtadasDat = DateTime.Now.ToString();
            erec_IraKezbesitesiTetelek.Updated.AtadasDat = true;

            erec_IraKezbesitesiTetelek.KezbesitesFej_Id = kezbesitesiFej_Id;
            erec_IraKezbesitesiTetelek.Updated.KezbesitesFej_Id = true;

            // visszak�ld�s indoka
            erec_IraKezbesitesiTetelek.Base.Note = visszakuldesIndoka;
            erec_IraKezbesitesiTetelek.Base.Updated.Note = true;


            ExecParam execParam_update = execParam.Clone();
            execParam_update.Record_Id = erec_IraKezbesitesiTetelek.Id;
            Result result_update = Update(execParam_update, erec_IraKezbesitesiTetelek);

            if (result_update.IsError)
            {
                throw new ResultException(result_update);
            }

            result = result_update;

            #region Esem�nynapl�z�s
            // napl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_IraKezbesitesiTetelek.Obj_Id, erec_IraKezbesitesiTetelek.Obj_type, "UgyiratVisszakuldesIrattarbol").Record;
            if (eventLogRecord != null && !String.IsNullOrEmpty(visszakuldesIndoka))
            {
                eventLogRecord.Base.Note = visszakuldesIndoka;
            }

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion Esem�nynapl�z�s

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    #endregion Visszak�ld�s iratt�rb�l

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result Sztorno(ExecParam execParam, String erec_IraKezbesitesiTetelek_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        if (execParam == null || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52340);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // K�zbes�t�si t�tel lek�r�se:
            //EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService();
            ExecParam execParam_kezbtetelGet = execParam.Clone();
            execParam_kezbtetelGet.Record_Id = erec_IraKezbesitesiTetelek_Id;

            Result result_kezbTetelGet = this.Get(execParam_kezbtetelGet);
            if (!String.IsNullOrEmpty(result_kezbTetelGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_kezbtetelGet, result_kezbTetelGet);
                //return result_kezbTetelGet;

                throw new ResultException(result_kezbTetelGet);
            }

            if (result_kezbTetelGet.Record == null)
            {
                // hiba:
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52341);
                //log.WsEnd(execParam_kezbtetelGet, result1);
                //return result1;

                throw new ResultException(52341);
            }
            else
            {
                EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetel =
                    (EREC_IraKezbesitesiTetelek)result_kezbTetelGet.Record;

                // Csak �tad�sra kijel�lt k�zbes�t�si t�telt lehet sztorn�zni
                if (erec_IraKezbesitesiTetel.Allapot != Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt &&
                    erec_IraKezbesitesiTetel.Allapot != Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott)
                {
                    // nem sztorn�zhat�:
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52342);
                    //log.WsEnd(execParam_kezbtetelGet, result1);
                    //return result1;

                    throw new ResultException(52342);
                }
                else
                {
                    /// T�tel �llapot�t�l f�gg�en megh�vjuk az �gyirat/Iratp�ld�ny/K�ldem�ny �tad�srakijelol�sSztorn� met�dus�t
                    /// (azok fogj�k majd a k�zbes�t�si t�telt is m�dos�tani)
                    /// 


                    switch (erec_IraKezbesitesiTetel.Obj_type)
                    {
                        case Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok:
                            {
                                EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                                ExecParam execParam_ugyiratAtadasraKijelolSztorno = execParam.Clone();

                                Result result_ugyiratAtadasraKijelolSztorno = ugyiratokService.AtadasraKijelolesSztorno(execParam_ugyiratAtadasraKijelolSztorno
                                        , erec_IraKezbesitesiTetel.Obj_Id);

                                result = result_ugyiratAtadasraKijelolSztorno;
                                break;
                            }
                        case Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek:
                            {
                                EREC_KuldKuldemenyekService kuldemenyekService = new EREC_KuldKuldemenyekService(this.dataContext);
                                ExecParam execParam_kuldAtadasraKijelolSztorno = execParam.Clone();

                                Result result_kuldAtadasraKijelolSztorno = kuldemenyekService.AtadasraKijelolesSztorno(execParam_kuldAtadasraKijelolSztorno
                                        , erec_IraKezbesitesiTetel.Obj_Id);

                                result = result_kuldAtadasraKijelolSztorno;
                                break;
                            }
                        case Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok:
                            {
                                EREC_PldIratPeldanyokService iratPeldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);
                                ExecParam execParam_iratPldAtadasraKijelolSztorno = execParam.Clone();

                                Result result_iratPldAtadasraKijelolSztorno = iratPeldanyokService.AtadasraKijelolesSztorno(execParam_iratPldAtadasraKijelolSztorno
                                        , erec_IraKezbesitesiTetel.Obj_Id);

                                result = result_iratPldAtadasraKijelolSztorno;
                                break;
                            }
                        case Contentum.eUtility.Constants.TableNames.KRT_Mappak:
                            {
                                KRT_MappakService mappakService = new KRT_MappakService(this.dataContext);

                                Result result_dosszieAtadasraKijelolSztorno = mappakService.AtadasraKijelolesSztorno(execParam.Clone(), erec_IraKezbesitesiTetel.Obj_Id);

                                result = result_dosszieAtadasraKijelolSztorno;
                                break;
                            }
                        default:
                            // hiba (�rv�nytelen t�pus)
                            //Result result1 = ResultError.CreateNewResultWithErrorCode(52343);
                            //log.WsEnd(execParam_kezbtetelGet, result1);
                            //return result1;

                            throw new ResultException(52343);
                    }

                }
            }

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }



    /// <summary>
    /// NEM WebMethod, csak egy seg�delj�r�s
    /// (Az �gyiratok (K�ldem�nyek, Iratp�ld�nyok) �tv�tele met�dusokb�l h�vva)
    /// Megkeresi a megadott rekordhoz tartoz� �tad�sra kijel�lt, vagy �tadott k�zbes�t�si t�telt(t�teleket),
    /// �s �tvett �llapot�ra �ll�tja be azokat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="obj_Id"></param>
    /// <param name="Obj_type"></param>
    /// <returns></returns>
    public Result SetKezbesitesiTetelToAtvett(ExecParam execParam, String obj_Id)
    {
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(obj_Id))
        {
            // hiba
            return ResultError.CreateNewResultWithErrorCode(52220);
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // K�zbes�t�si t�telek lek�r�se:
            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_GetAll = execParam.Clone();

            EREC_IraKezbesitesiTetelekSearch searchObject = new EREC_IraKezbesitesiTetelekSearch();
            searchObject.Obj_Id.Value = obj_Id;
            searchObject.Obj_Id.Operator = Query.Operators.equals;

            // �llapot: �tad�sra kijel�lt vagy �tadott vagy Visszak�ld�tt
            searchObject.Allapot.Value = Search.GetSqlInnerString(new string[] {KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt
                , KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                , KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott});
            searchObject.Allapot.Operator = Query.Operators.inner;

            Result result_GetAll = service_KezbesitesiTetelek.GetAll(execParam_GetAll, searchObject);
            if (!String.IsNullOrEmpty(result_GetAll.ErrorCode))
            {
                // hiba:
                //return result_GetAll;

                throw new ResultException(result_GetAll);
            }
            else
            {
                DataSet ds = result_GetAll.Ds;

                try
                {
                    #region K�zbes�t�si fej l�trehoz�sa (csak akkor, ha volt k�zbes�t�si t�tel)

                    EREC_IraKezbesitesiFejek kezbesitesiFej = new EREC_IraKezbesitesiFejek();

                    kezbesitesiFej.Id = Guid.NewGuid().ToString();
                    kezbesitesiFej.Updated.Id = true;

                    kezbesitesiFej.CsomagAzonosito = "Atvetel";
                    kezbesitesiFej.Updated.CsomagAzonosito = true;

                    kezbesitesiFej.Tipus = "V";
                    kezbesitesiFej.Updated.Tipus = true;

                    // csak akkor hozzuk l�tre, ha volt k�zbes�t�si t�tel:
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        EREC_IraKezbesitesiFejekService kezbesitesiFejService = new EREC_IraKezbesitesiFejekService(this.dataContext);
                        Result kezbesitesiFejInsertResult = kezbesitesiFejService.Insert(execParam.Clone(), kezbesitesiFej);
                        if (!string.IsNullOrEmpty(kezbesitesiFejInsertResult.ErrorCode))
                        {
                            throw new ResultException(kezbesitesiFejInsertResult);
                        }
                    }
                    #endregion

                    // k�zbes�t�si t�telek UPDATE-el�se:

                    // csak egyszer k�rj�k le
                    string strDate = DateTime.Now.ToString();
                    string felhasznaloSajatCsoportId = Csoportok.GetFelhasznaloSajatCsoportId(execParam);

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        //String tetel_Id = row["Id"].ToString();

                        //// K�zbes�t�si t�tel lek�r�se:
                        //ExecParam execParam_kezbTetelGet = execParam.Clone();
                        //execParam_kezbTetelGet.Record_Id = tetel_Id;

                        //Result result_kezbTetelGet = service_KezbesitesiTetelek.Get(execParam_kezbTetelGet);
                        //if (result_kezbTetelGet.IsError)
                        //{
                        //    // hiba:
                        //    //return ResultError.CreateNewResultWithErrorCode(52221);

                        //    throw new ResultException(52221);
                        //}
                        //else
                        //{
                        //EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetel =
                        //    (EREC_IraKezbesitesiTetelek)result_kezbTetelGet.Record;

                        EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetel = new EREC_IraKezbesitesiTetelek();
                        Utility.LoadBusinessDocumentFromDataRow(erec_IraKezbesitesiTetel, row);

                        // Ellen�rz�s, t�nyleg a k�rt obj_Id -hoz tartozik-e:
                        if (erec_IraKezbesitesiTetel.Obj_Id != obj_Id)
                        {
                            // hiba:
                            //return ResultError.CreateNewResultWithErrorCode(52221);

                            throw new ResultException(52221);
                        }

                        // K�zbes�t�si t�tel UPDATE:
                        erec_IraKezbesitesiTetel.Updated.SetValueAll(false);
                        erec_IraKezbesitesiTetel.Base.Updated.SetValueAll(false);
                        erec_IraKezbesitesiTetel.Base.Updated.Ver = true;

                        /// Amit m�dos�tani kell:
                        /// AtvetelDat,Felhasznalo_Id_AtvevoUser (Felhasznalo_Id_AtvevoLogin), Allapot
                        /// 

                        // �tad�s d�tum�t kit�ltj�k, ha eddig nem volt megadva (mert csak �tad�sra kijel�lt):
                        if (String.IsNullOrEmpty(erec_IraKezbesitesiTetel.AtadasDat))
                        {
                            erec_IraKezbesitesiTetel.AtadasDat = strDate;
                            erec_IraKezbesitesiTetel.Updated.AtadasDat = true;

                            erec_IraKezbesitesiTetel.Base.Note = "SetKezbesitesiTetelToAtvett";
                            erec_IraKezbesitesiTetel.Base.Updated.Note = true;

                            if (erec_IraKezbesitesiTetel.Allapot == Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott)
                            {
                                Logger.Warn(String.Format("�tadott k�zbes�t�si t�tel hi�nyz� �tad�si d�tummal: '{0}'", erec_IraKezbesitesiTetel.Id));
                            }
                        }

                        // �tv�tel d�tuma:
                        erec_IraKezbesitesiTetel.AtvetelDat = strDate;
                        erec_IraKezbesitesiTetel.Updated.AtvetelDat = true;

                        // �tvev� felhaszn�l� be�ll�t�sa:
                        erec_IraKezbesitesiTetel.Felhasznalo_Id_AtvevoUser = felhasznaloSajatCsoportId;
                        erec_IraKezbesitesiTetel.Updated.Felhasznalo_Id_AtvevoUser = true;

                        erec_IraKezbesitesiTetel.Felhasznalo_Id_AtvevoLogin = felhasznaloSajatCsoportId;
                        erec_IraKezbesitesiTetel.Updated.Felhasznalo_Id_AtvevoLogin = true;

                        // �llapot be�ll�t�sa �tvettre:
                        erec_IraKezbesitesiTetel.Allapot = Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atvett;
                        erec_IraKezbesitesiTetel.Updated.Allapot = true;

                        // �tv�teli fej
                        erec_IraKezbesitesiTetel.AtveteliFej_Id = kezbesitesiFej.Id;
                        erec_IraKezbesitesiTetel.Updated.AtveteliFej_Id = true;

                        ExecParam execParam_kezbTetelUpdate = execParam.Clone();
                        execParam_kezbTetelUpdate.Record_Id = erec_IraKezbesitesiTetel.Id;

                        Result result_kezbTetelUpdate = service_KezbesitesiTetelek.Update(execParam_kezbTetelUpdate, erec_IraKezbesitesiTetel);
                        if (!String.IsNullOrEmpty(result_kezbTetelUpdate.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_kezbTetelUpdate);
                        }

                        result = result_kezbTetelUpdate;
                        result.Uid = kezbesitesiFej.Id;
                        //}
                    }
                }
                catch (ResultException resultException)
                {
                    throw resultException;
                }
                catch
                {
                    // hiba:
                    Result result_error = ResultError.CreateNewResultWithErrorCode(52220);
                    throw new ResultException(result_error);
                }
            }



            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;

    }

    public Result SetKezbesitesiTetelekToAtvettTomeges(ExecParam execParam, String obj_Ids)
    {
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(obj_Ids))
        {
            // hiba
            return ResultError.CreateNewResultWithErrorCode(52220);
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // K�zbes�t�si t�telek lek�r�se:
            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_GetAll = execParam.Clone();

            EREC_IraKezbesitesiTetelekSearch searchObject = new EREC_IraKezbesitesiTetelekSearch();
            searchObject.Obj_Id.Value = obj_Ids;
            searchObject.Obj_Id.Operator = Query.Operators.inner;

            // �llapot: �tad�sra kijel�lt vagy �tadott vagy Visszak�ld�tt
            searchObject.Allapot.Value = Search.GetSqlInnerString(new string[] {KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt
                , KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                , KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott});
            searchObject.Allapot.Operator = Query.Operators.inner;

            Result result_GetAll = service_KezbesitesiTetelek.GetAll(execParam_GetAll, searchObject);
            if (!String.IsNullOrEmpty(result_GetAll.ErrorCode))
            {
                // hiba:
                //return result_GetAll;

                throw new ResultException(result_GetAll);
            }
            else
            {
                DataSet ds = result_GetAll.Ds;

                try
                {
                    string selectedIds = string.Empty;
                    string selectedVers = string.Empty;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        selectedIds += ",'" + row["Id"].ToString() + "'";
                        selectedVers += "," + row["Ver"].ToString();
                    }

                    selectedIds = selectedIds.TrimStart(',');
                    selectedVers = selectedVers.TrimStart(',');

                    result = sp.AtvetelTomeges(execParam.Clone(), selectedIds, selectedVers);
                    if (!string.IsNullOrEmpty(result.ErrorCode))
                        throw new ResultException(result);

                }
                catch (ResultException resultException)
                {
                    throw resultException;
                }
                catch
                {
                    // hiba:
                    Result result_error = ResultError.CreateNewResultWithErrorCode(52220);
                    throw new ResultException(result_error);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;

    }



    /// <summary>
    /// NEM WebMethod, csak egy seg�delj�r�s
    /// Megkeresi a megadott rekordhoz tartoz� �tad�sra kijel�lt t�telt(t�teleket),
    /// �s �tadott �llapot�ra �ll�tja be azokat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="obj_Id"></param>
    /// <param name="Obj_type"></param>
    /// <returns></returns>
    public Result SetKezbesitesiTetelToAtadott_Tomeges(ExecParam execParam, String[] obj_Ids_Array)
    {
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || obj_Ids_Array == null || obj_Ids_Array.Length == 0)
        {
            // hiba
            return ResultError.CreateNewResultWithErrorCode(52490);
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // K�zbes�t�si t�telek lek�r�se:
            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_GetAll = execParam.Clone();

            EREC_IraKezbesitesiTetelekSearch searchObject = new EREC_IraKezbesitesiTetelekSearch();

            // obj_id-kb�l lista �ssze�ll�t�sa a sz�r�felt�telhez:           
            for (int i = 0; i < obj_Ids_Array.Length; i++)
            {
                if (i == 0)
                {
                    searchObject.Obj_Id.Value = "'" + obj_Ids_Array[i] + "'";
                }
                else
                {
                    searchObject.Obj_Id.Value += ",'" + obj_Ids_Array[i] + "'";
                }
            }

            searchObject.Obj_Id.Operator = Query.Operators.inner;

            // �llapot: �tad�sra kijel�lt
            searchObject.Allapot.Value = Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt;
            searchObject.Allapot.Operator = Query.Operators.equals;

            Result result_GetAll = service_KezbesitesiTetelek.GetAll(execParam_GetAll, searchObject);
            if (!String.IsNullOrEmpty(result_GetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_GetAll);
            }
            else
            {
                DataSet ds = result_GetAll.Ds;

                //if (ds.Tables[0].Rows.Count != obj_Ids_Array.Length)
                //{
                //    // hiba:
                //    throw new ResultException(52490);
                //}

                try
                {
                    // K�zbes�t�si fej l�trehoz�sa
                    EREC_IraKezbesitesiFejek kezbesitesiFej = new EREC_IraKezbesitesiFejek();

                    kezbesitesiFej.Id = Guid.NewGuid().ToString();
                    kezbesitesiFej.Updated.Id = true;

                    kezbesitesiFej.CsomagAzonosito = "Atadas";
                    kezbesitesiFej.Updated.CsomagAzonosito = true;

                    kezbesitesiFej.Tipus = "A";
                    kezbesitesiFej.Updated.Tipus = true;

                    EREC_IraKezbesitesiFejekService kezbesitesiFejService = new EREC_IraKezbesitesiFejekService(this.dataContext);
                    Result kezbesitesiFejInsertResult = kezbesitesiFejService.Insert(execParam.Clone(), kezbesitesiFej);
                    if (!string.IsNullOrEmpty(kezbesitesiFejInsertResult.ErrorCode))
                        throw new ResultException(kezbesitesiFejInsertResult);

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string tetel_Id = row["Id"].ToString();
                        string tetel_Ver = row["Ver"].ToString();

                        // K�zbes�t�si t�tel UPDATE:
                        EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetel = new EREC_IraKezbesitesiTetelek();

                        erec_IraKezbesitesiTetel.Updated.SetValueAll(false);
                        erec_IraKezbesitesiTetel.Base.Updated.SetValueAll(false);

                        // verzi� be�ll�t�sa:
                        erec_IraKezbesitesiTetel.Base.Ver = tetel_Ver;
                        erec_IraKezbesitesiTetel.Base.Updated.Ver = true;

                        /// Amit m�dos�tani kell:
                        /// AtadasDat, Allapot

                        // �tad�s d�tuma:
                        erec_IraKezbesitesiTetel.AtadasDat = DateTime.Now.ToString();
                        erec_IraKezbesitesiTetel.Updated.AtadasDat = true;

                        // �llapot be�ll�t�sa �tadottra:
                        erec_IraKezbesitesiTetel.Allapot = Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott;
                        erec_IraKezbesitesiTetel.Updated.Allapot = true;

                        // K�zbes�t�si fej t�lt�se
                        erec_IraKezbesitesiTetel.KezbesitesFej_Id = kezbesitesiFej.Id;
                        erec_IraKezbesitesiTetel.Updated.KezbesitesFej_Id = true;

                        ExecParam execParam_kezbTetelUpdate = execParam.Clone();
                        execParam_kezbTetelUpdate.Record_Id = tetel_Id;

                        Result result_kezbTetelUpdate = service_KezbesitesiTetelek.Update(execParam_kezbTetelUpdate, erec_IraKezbesitesiTetel);
                        if (!String.IsNullOrEmpty(result_kezbTetelUpdate.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_kezbTetelUpdate);
                        }

                        result = result_kezbTetelUpdate;
                        result.Uid = kezbesitesiFej.Id;
                    }

                }
                catch (ResultException resultException)
                {
                    throw resultException;
                }
                catch
                {
                    // hiba:
                    Result result_error = ResultError.CreateNewResultWithErrorCode(52220);
                    throw new ResultException(result_error);
                }
            }



            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;

    }





    /// <summary>
    /// NEM WebMethod, csak egy seg�delj�r�s
    /// (Az �gyiratok (K�ldem�nyek, Iratp�ld�nyok) �tad�srakijel�l�svisszavon�sa met�dusokb�l h�vva)
    /// Ha a megadott rekordra l�tezik �tad�sra kijel�lt �llapot� k�zbes�t�si t�tel, �rv�nytelen�teni kell azokat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="obj_Id"></param>
    /// <returns></returns>
    public Result AtadasraKijeloltekInvalidate(ExecParam execParam, String obj_Id)
    {
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(obj_Id))
        {
            // hiba
            return ResultError.CreateNewResultWithErrorCode(52344);
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // K�zbes�t�si t�telek lek�r�se:
            //EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService();
            ExecParam execParam_GetAll = execParam.Clone();

            EREC_IraKezbesitesiTetelekSearch searchObject = new EREC_IraKezbesitesiTetelekSearch();
            searchObject.Obj_Id.Value = obj_Id;
            searchObject.Obj_Id.Operator = Query.Operators.equals;

            // �llapot: �tad�sra kijel�lt
            //searchObject.Allapot.Value = Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt;
            //searchObject.Allapot.Operator = Query.Operators.equals;

            // �llapot: �tad�sra kijel�lt, vagy �tadott  (--> Nem �tvett)
            searchObject.Allapot.Value = "'" + Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt
                + "','" + Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                + "'";
            searchObject.Allapot.Operator = Query.Operators.inner;

            Result result_GetAll = this.GetAll(execParam_GetAll, searchObject);
            if (!String.IsNullOrEmpty(result_GetAll.ErrorCode))
            {
                // hiba:
                //return result_GetAll;

                throw new ResultException(result_GetAll);
            }
            else
            {
                DataSet ds = result_GetAll.Ds;

                try
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        String tetel_Id = row["Id"].ToString();

                        // Ellen�rz�s, t�nyleg a k�rt obj_Id -hoz tartozik-e:
                        String row_obj_Id = row["Obj_Id"].ToString();
                        if (row_obj_Id != obj_Id)
                        {
                            // hiba:
                            //return ResultError.CreateNewResultWithErrorCode(52345);

                            throw new ResultException(52345);
                        }


                        // K�zbes�t�si t�tel INVALIDATE:

                        ExecParam execParam_kezbTetelInvalidate = execParam.Clone();
                        execParam_kezbTetelInvalidate.Record_Id = tetel_Id;

                        Result result_kezbTetelInvalidate = this.Invalidate(execParam_kezbTetelInvalidate);
                        if (!String.IsNullOrEmpty(result_kezbTetelInvalidate.ErrorCode))
                        {
                            // hiba:
                            //return result_kezbTetelInvalidate;

                            throw new ResultException(result_kezbTetelInvalidate);
                        }
                    }
                }
                catch (ResultException resultException)
                {
                    throw resultException;
                }
                catch
                {
                    // hiba:
                    //return ResultError.CreateNewResultWithErrorCode(52346);

                    throw new ResultException(52346);
                }
            }

            // ha nincs hiba:


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }




    /// <summary>
    /// NEM WebMethod, csak egy seg�delj�r�s
    /// Ha a megadott rekordra l�tezik m�g �t nem adott k�zbes�t�si t�tel, �rv�nytelen�teni kell azokat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="obj_Id"></param>
    /// <returns></returns>
    public Result NemAtadottKezbesitesiTetelInvalidate(ExecParam execParam, String obj_Id)
    {
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(obj_Id))
        {
            // hiba
            return ResultError.CreateNewResultWithErrorCode(52470);
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // K�zbes�t�si t�telek lek�r�se:
            //EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService();
            ExecParam execParam_GetAll = execParam.Clone();

            EREC_IraKezbesitesiTetelekSearch searchObject = new EREC_IraKezbesitesiTetelekSearch();
            searchObject.Obj_Id.Value = obj_Id;
            searchObject.Obj_Id.Operator = Query.Operators.equals;

            // �llapot: �tad�sra kijel�lt, vagy �tadott  (--> Nem �tvett)
            searchObject.Allapot.Value = "'" + Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt
                + "','" + Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                + "','" + Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott
                + "'";
            searchObject.Allapot.Operator = Query.Operators.inner;

            Result result_GetAll = this.GetAll(execParam_GetAll, searchObject);
            if (!String.IsNullOrEmpty(result_GetAll.ErrorCode))
            {
                // hiba:
                //return result_GetAll;

                throw new ResultException(result_GetAll);
            }
            else
            {
                DataSet ds = result_GetAll.Ds;

                try
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        String tetel_Id = row["Id"].ToString();

                        // Ellen�rz�s, t�nyleg a k�rt obj_Id -hoz tartozik-e:
                        String row_obj_Id = row["Obj_Id"].ToString();
                        if (row_obj_Id != obj_Id)
                        {
                            // hiba:
                            //return ResultError.CreateNewResultWithErrorCode(52471);

                            throw new ResultException(52471);
                        }


                        // K�zbes�t�si t�tel INVALIDATE:

                        ExecParam execParam_kezbTetelInvalidate = execParam.Clone();
                        execParam_kezbTetelInvalidate.Record_Id = tetel_Id;

                        Result result_kezbTetelInvalidate = this.Invalidate(execParam_kezbTetelInvalidate);
                        if (!String.IsNullOrEmpty(result_kezbTetelInvalidate.ErrorCode))
                        {
                            // hiba:
                            //return result_kezbTetelInvalidate;

                            throw new ResultException(result_kezbTetelInvalidate);
                        }
                    }
                }
                catch (ResultException resultException)
                {
                    throw resultException;
                }
                catch
                {
                    // hiba:
                    //return ResultError.CreateNewResultWithErrorCode(52471);

                    throw new ResultException(52471);
                }
            }

            // ha nincs hiba:

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        return result;
    }

    private void InitializeComponent()
    {

    }



}