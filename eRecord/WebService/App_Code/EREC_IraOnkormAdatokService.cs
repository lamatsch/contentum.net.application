using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IraOnkormAdatokService : System.Web.Services.WebService
{
    [Obsolete]
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla Ids param�terben kijel�lt rekordjainak m�dos�t�sa. A m�dos�tand� adatokat az UgyFajta param�ter hat�rozza meg. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraOnkormAdatok))]
    public Result UpdateRecordsByUgyFajta(ExecParam ExecParam, string[] Ids, string UgyFajta)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        //bool isConnectionOpenHere = false;
        //bool isTransactionBeginHere = false;

        //try
        //{
        //    isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
        //    isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

        //    if (Ids == null || Ids.Length == 0)
        //    {
        //        throw new ResultException("Az Ids param�ter nem tartalmaz �rt�ket!");
        //    }

        //    #region rekordok lek�r�se

        //    string IdsJoined = "'" + String.Join("','", Ids) + "'";

        //    // a verzi� miatt el�bb le kell k�rni a rekordokat - tov�bb�, ha nem l�tezik, l�tre kell hozni
        //    EREC_IraOnkormAdatokSearch search = new EREC_IraOnkormAdatokSearch();
        //    search.Id.Value = IdsJoined;
        //    search.Id.Operator = Contentum.eQuery.Query.Operators.inner;

        //    Result result_getall = sp.GetAll(ExecParam, search);

        //    if (!String.IsNullOrEmpty(result_getall.ErrorCode))
        //    {
        //        throw new ResultException(result_getall);
        //    }
        //    #endregion rekordok lek�r�se

        //    #region nem tal�lt rekordok
        //    int cnt = result_getall.Ds.Tables[0].Rows.Count;
        //    int cntdiff = Ids.Length - cnt;
        //    if (cntdiff > 0)
        //    {
        //        string IdsFoundJoined = "";
        //        if (cnt > 0)
        //        {
        //            string[] IdsFound = new string[cnt];
        //            for (int i = 0; i < cnt; i++)
        //            {
        //                System.Data.DataRow row = result_getall.Ds.Tables[0].Rows[i];
        //                string Id = row["Id"].ToString();
        //                IdsFound[i] = Id;
        //            }
        //            IdsFoundJoined = "'" + String.Join("','", IdsFound) + "'";
        //        }
        //        foreach (string Id in Ids)
        //        {
        //            if (!IdsFoundJoined.Contains("'" + Id + "'"))
        //            {
        //                EREC_IraOnkormAdatok erec_IraOnkormAdatokInsert = new EREC_IraOnkormAdatok();
        //                erec_IraOnkormAdatokInsert.Id = Id;
        //                erec_IraOnkormAdatokInsert.Updated.Id = true;

        //                erec_IraOnkormAdatokInsert.IraIratok_Id = Id;
        //                erec_IraOnkormAdatokInsert.Updated.IraIratok_Id = true;

        //                Result result_IraOnkormAdatokInsert = Insert(ExecParam, erec_IraOnkormAdatokInsert);

        //                if (!String.IsNullOrEmpty(result_IraOnkormAdatokInsert.ErrorCode))
        //                {
        //                    Logger.Debug("�nkorm�nyzati adatok rekord l�trehoz�sa sikertelen: ", ExecParam, result_IraOnkormAdatokInsert);

        //                    throw new ResultException(result_IraOnkormAdatokInsert);
        //                }
        //            }
        //        }

        //        #region rekordok �jb�li lek�r�se
        //        result_getall = sp.GetAll(ExecParam, search);

        //        if (!String.IsNullOrEmpty(result_getall.ErrorCode))
        //        {
        //            throw new ResultException(result_getall);
        //        }
        //        #endregion rekordok �jb�li lek�r�se
        //    }

        //    cntdiff = Ids.Length - result_getall.Ds.Tables[0].Rows.Count;
        //    if (cntdiff > 0)
        //    {
        //        // ennek nem szabad bek�vetkeznie
        //        throw new ResultException("L�trehoz�si/lek�r�si hiba: A rekordok egy r�sze nem tal�lhat�!");
        //    }
        //    #endregion nem tal�lt rekordok

        //    foreach (System.Data.DataRow row in result_getall.Ds.Tables[0].Rows)
        //    {
        //        string Id = row["Id"].ToString();
        //        ExecParam execParam_Update = ExecParam.Clone();
        //        execParam_Update.Record_Id = Id;

        //        EREC_IraOnkormAdatok erec_IraOnkormAdatok = new EREC_IraOnkormAdatok();
        //        Utility.LoadBusinessDocumentFromDataRow(erec_IraOnkormAdatok, row);
        //        erec_IraOnkormAdatok.Updated.SetValueAll(false);
        //        erec_IraOnkormAdatok.Base.Updated.SetValueAll(false);

        //        erec_IraOnkormAdatok.UgyFajtaja = UgyFajta;
        //        erec_IraOnkormAdatok.Updated.UgyFajtaja = true;

        //        if (String.IsNullOrEmpty(UgyFajta) || UgyFajta == "0") // nem meghat�rozott vagy nem hat�s�gi �gyek
        //        {
        //            erec_IraOnkormAdatok.DontestHozta = "<null>";
        //            erec_IraOnkormAdatok.Updated.DontestHozta = true;
        //            erec_IraOnkormAdatok.DontesFormaja = "<null>";
        //            erec_IraOnkormAdatok.Updated.DontesFormaja = true;
        //            erec_IraOnkormAdatok.UgyintezesHataridore = "<null>";
        //            erec_IraOnkormAdatok.Updated.UgyintezesHataridore = true;
        //            erec_IraOnkormAdatok.HataridoTullepes = "<null>";
        //            erec_IraOnkormAdatok.Updated.HataridoTullepes = true;
        //            erec_IraOnkormAdatok.JogorvoslatiEljarasTipusa = "<null>";
        //            erec_IraOnkormAdatok.Updated.JogorvoslatiEljarasTipusa = true;
        //            erec_IraOnkormAdatok.JogorvoslatiDontesTipusa = "<null>";
        //            erec_IraOnkormAdatok.Updated.JogorvoslatiDontesTipusa = true;
        //            erec_IraOnkormAdatok.JogorvoslatiDontestHozta = "<null>";
        //            erec_IraOnkormAdatok.Updated.JogorvoslatiDontestHozta = true;
        //            erec_IraOnkormAdatok.JogorvoslatiDontesTartalma = "<null>";
        //            erec_IraOnkormAdatok.Updated.JogorvoslatiDontesTartalma = true;
        //            erec_IraOnkormAdatok.JogorvoslatiDontes = "<null>";
        //            erec_IraOnkormAdatok.Updated.JogorvoslatiDontes = true;
        //        }

        //        // verzi�
        //        erec_IraOnkormAdatok.Base.Updated.Ver = true;

        //        Result result_IraOnkormAdatokUpdate = Update(execParam_Update, erec_IraOnkormAdatok);
        //        if (!String.IsNullOrEmpty(result_IraOnkormAdatokUpdate.ErrorCode))
        //        {
        //            Logger.Debug("�nkorm�nyzati adatok Update sikertelen: ", execParam_Update, result_IraOnkormAdatokUpdate);

        //            // hiba 
        //            throw new ResultException(result_IraOnkormAdatokUpdate);
        //        }

        //    }

        //    #region Esem�nynapl�z�s
        //    // Az Update elj�r�sban t�rt�nik
        //    #endregion

        //    // COMMIT
        //    dataContext.CommitIfRequired(isTransactionBeginHere);
        //}
        //catch (Exception e)
        //{
        //    dataContext.RollbackIfRequired(isTransactionBeginHere);
        //    result = ResultException.GetResultFromException(e);
        //}
        //finally
        //{
        //    dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        //}

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IraOnkormAdatokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IraOnkormAdatokSearch _EREC_IraOnkormAdatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_IraOnkormAdatokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Get(ExecParam ExecParam)  
    /// A EREC_IraOnkormAdatok rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter).
    /// Kieg�sz�tve az 'A' adatlaphoz sz�ks�ges adatokkal
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[System.Xml.Serialization.XmlInclude(typeof(EREC_IraOnkormAdatok))]
    public Result GetWithExtensionForAdatlap(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithExtensionForAdatlap(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [Obsolete]
    /// <summary>
    /// Adatok t�rl�se
    /// </summary>
    /// <param name="execParam">Record_Id</param>
    /// <returns></returns>
    public Result DeleteAdatok(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        //bool isConnectionOpenHere = false;
        //bool isTransactionBeginHere = false;

        //try
        //{
        //    isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
        //    isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

        //    Result resGet = this.Get(execParam);

        //    //Ha nincs hat�s�gi adat nem hiba
        //    if (resGet.ErrorMessage != "[50101]")
        //    {
        //        if (resGet.IsError)
        //        {
        //            throw new ResultException(resGet);
        //        }

        //        EREC_IraOnkormAdatok erec_IraOnkormAdatok = (EREC_IraOnkormAdatok)resGet.Record;
        //        erec_IraOnkormAdatok.Base.Updated.SetValueAll(false);
        //        erec_IraOnkormAdatok.Updated.SetValueAll(false);
        //        erec_IraOnkormAdatok.Base.Updated.Ver = true;

        //        erec_IraOnkormAdatok.DontestHozta = "<null>";
        //        erec_IraOnkormAdatok.Updated.DontestHozta = true;
        //        erec_IraOnkormAdatok.DontesFormaja = "<null>";
        //        erec_IraOnkormAdatok.Updated.DontesFormaja = true;
        //        erec_IraOnkormAdatok.UgyintezesHataridore = "<null>";
        //        erec_IraOnkormAdatok.Updated.UgyintezesHataridore = true;
        //        erec_IraOnkormAdatok.HataridoTullepes = "<null>";
        //        erec_IraOnkormAdatok.Updated.HataridoTullepes = true;
        //        erec_IraOnkormAdatok.JogorvoslatiEljarasTipusa = "<null>";
        //        erec_IraOnkormAdatok.Updated.JogorvoslatiEljarasTipusa = true;
        //        erec_IraOnkormAdatok.JogorvoslatiDontesTipusa = "<null>";
        //        erec_IraOnkormAdatok.Updated.JogorvoslatiDontesTipusa = true;
        //        erec_IraOnkormAdatok.JogorvoslatiDontestHozta = "<null>";
        //        erec_IraOnkormAdatok.Updated.JogorvoslatiDontestHozta = true;
        //        erec_IraOnkormAdatok.JogorvoslatiDontesTartalma = "<null>";
        //        erec_IraOnkormAdatok.Updated.JogorvoslatiDontesTartalma = true;
        //        erec_IraOnkormAdatok.JogorvoslatiDontes = "<null>";
        //        erec_IraOnkormAdatok.Updated.JogorvoslatiDontes = true;

        //        result = this.Update(execParam, erec_IraOnkormAdatok);
        //    }

        //    // COMMIT
        //    dataContext.CommitIfRequired(isTransactionBeginHere);
        //}
        //catch (Exception e)
        //{
        //    dataContext.RollbackIfRequired(isTransactionBeginHere);
        //    result = ResultException.GetResultFromException(e);
        //}
        //finally
        //{
        //    dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        //}

        log.WsEnd(execParam, result);
        return result;
    }
}