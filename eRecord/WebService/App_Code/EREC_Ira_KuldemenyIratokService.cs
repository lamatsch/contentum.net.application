using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_Ira_KuldemenyIratokService : System.Web.Services.WebService
{

    /// TODO: M�g nem OK, nincs legener�lva a m�sik fele !!!

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Ira_KuldemenyIratokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_Ira_KuldemenyIratokSearch _EREC_Ira_KuldemenyIratokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
                
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            // TODO: majd visszatenni
            //isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

         result = sp.GetAllWithExtension(ExecParam, _EREC_Ira_KuldemenyIratokSearch);
            
     }
     catch (Exception e)
     {
         result = ResultException.GetResultFromException(e);
     }
     finally
     {
         // TODO: majd visszatenni
         //dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
     }

        log.WsEnd(ExecParam, result);
        return result;
    }

}