using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
//using System.EnterpriseServices;
using System.Data;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using AlairasokUtility = Contentum.eUtility.AlairasokUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IratAlairokService : System.Web.Services.WebService
{
    #region Gener�ltb�l �tvett

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratAlairok))]
    public Result Insert(ExecParam ExecParam, EREC_IratAlairok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
    
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (String.IsNullOrEmpty(Record.Allapot))
            {
                if (Record.AlairasMod == "M_UTO" || Record.AlairasMod == "Ut�lagosan adminisztr�lt al��r�s") // T�meges al��r�s eset�n Excel Al��r�s t�pusa cella �rt�k
                {
                    Record.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
                    Record.Updated.Allapot = true;
                    Record.AlairasDatuma = DateTime.Now.ToString();
                    Record.Updated.AlairasDatuma = true;
                }
                else
                {
                    Record.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
                    Record.Updated.Allapot = true;
                }
            }

            // Ellen�rz�s, hogy ha m�r volt az irathoz kiadm�nyoz�, akkor ne sz�rjunk be m�g egyet:

            // Kiadm�nyoz�t akarunk most besz�rni?
            bool kiadmanyozo = false;
            bool kiadmanyozniKell = false;
            bool alairt = false;

            if (Record.AlairoSzerep == KodTarak.ALAIRO_SZEREP.Kiadmanyozo)
            {
                kiadmanyozo = true;

                int kiadmanyozokSzama = KiadmanyozokSzama(ExecParam, Record.Obj_Id);
                if (kiadmanyozokSzama == -1)
                {
                    // hiba:
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52310);
                    //log.WsEnd(ExecParam, result1);
                    //return result1;

                    throw new ResultException(52310);
                }
                else if (kiadmanyozokSzama > 0)
                {
                    // Nem lehet t�bb kiadm�nyoz�t felvenni az irathoz! -->kil�p�s
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52311);
                    //log.WsEnd(ExecParam, result1);
                    //return result1;

                    throw new ResultException(52311);
                }


                // vizsg�lni, hogy kiadm�nyozand�-e, mert ha nem
                // nem vehet�nk fel kiadm�nyoz�t:

                // Kiadm�nyozni kell-e az iratot?
                kiadmanyozniKell = EREC_IraIratokService.Kiadmanyozando(this.dataContext, ExecParam, Record.Obj_Id);
                if (kiadmanyozniKell == false)
                {
                    // Nem vehet� fel kiadm�nyoz�:
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52312);
                    //log.WsEnd(ExecParam, result1);
                    //return result1;

                    throw new ResultException(52312);
                }
            }

            if (!String.IsNullOrEmpty(Record.AlairasDatuma))
            {
                alairt = true;
            }


            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (kiadmanyozo == true)
            {
                // ha kiadm�nyoz�t vett�nk fel, be kell sz�rni az irat FelhCsop_Id_Kiadmanyozo mez�j�be is:
                Result result_iratUpdate = SetKiadmanyozo(ExecParam, Record.Obj_Id, Record.FelhasznaloCsoport_Id_Alairo);
                if (!String.IsNullOrEmpty(result_iratUpdate.ErrorCode))
                {
                    // hiba:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParam, result_iratUpdate);
                    //return result_iratUpdate;

                    throw new ResultException(result_iratUpdate);
                }
            }

            // Ha kiadm�nyoz� az al��r�, �s al� is �rta, akkor Irat kiadm�nyoz�s is kell:
            // (Ha nem volt hiba a besz�r�sn�l)
            if (kiadmanyozo == true && alairt == true && String.IsNullOrEmpty(result.ErrorCode)
                && kiadmanyozniKell == true)
            {

                EREC_IraIratokService service_IraIratok = new EREC_IraIratokService(this.dataContext);
                Result result_Kiadmanyozas =
                    service_IraIratok.Kiadmanyozas(ExecParam, Record.Obj_Id, Record.FelhasznaloCsoport_Id_Alairo, AlairasokUtility.IsTomeges(Record));
                if (!String.IsNullOrEmpty(result_Kiadmanyozas.ErrorCode))
                {
                    // hiba:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParam, result_Kiadmanyozas);
                    //return result_Kiadmanyozas;

                    throw new ResultException(result_Kiadmanyozas);
                }
            }

            #region Jogosult�s ad�sa az al��r�nak

            RightsService aclService = new RightsService(this.dataContext);

            Result aclResult = aclService.AddCsoportToJogtargy(ExecParam.Clone(), Record.Obj_Id, Record.FelhasznaloCsoport_Id_Alairo, 'I');

            //helyettes al��r� is kap jogosults�got
            if (!String.IsNullOrEmpty(Record.FelhaszCsop_Id_HelyettesAlairo))
            {
                aclResult = aclService.AddCsoportToJogtargy(ExecParam.Clone(), Record.Obj_Id, Record.FelhaszCsop_Id_HelyettesAlairo, 'I');
            }

            #endregion


            #region Esem�nynapl�z�s

            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_IratAlairok", "New").Record;

            Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);

            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratAlairok))]
    public Result Update(ExecParam ExecParam, EREC_IratAlairok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Al��r�s vizsg�lata:
            // Ha most �ppen al��rta egy kiadm�nyoz�, akkor irat kiadm�nyoz�sa:
            bool alairt = false;

            if (!String.IsNullOrEmpty(Record.AlairasDatuma) && Record.Updated.AlairasDatuma == true && Record.Allapot != KodTarak.IRATALAIRAS_ALLAPOT.Visszautasitott)
            {
                alairt = true;
            }

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (alairt)
            {
                // Ha kiadm�nyoz� �rta al� --> Irat kiadm�nyoz�s:

                // EREC_IratAlairok GET
                EREC_IratAlairokService service_iratAlairok = new EREC_IratAlairokService(this.dataContext);

                Result result_alairoGet = service_iratAlairok.Get(ExecParam);
                if (!String.IsNullOrEmpty(result_alairoGet.ErrorCode))
                {
                    // hiba:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParam, result_alairoGet);
                    //return result_alairoGet;

                    throw new ResultException(result_alairoGet);
                }
                else
                {
                    EREC_IratAlairok erec_iratAlairok = (EREC_IratAlairok)result_alairoGet.Record;

                    // ha kiadm�nyoz� volt, akkor Irat kiadm�nyoz�s:
                    if (erec_iratAlairok.AlairoSzerep == KodTarak.ALAIRO_SZEREP.Kiadmanyozo)
                    {
                        // Kiadm�nyoz�s:
                        EREC_IraIratokService service_IraIratok = new EREC_IraIratokService(this.dataContext);
                        Result result_Kiadmanyozas =
                            service_IraIratok.Kiadmanyozas(ExecParam, erec_iratAlairok.Obj_Id, erec_iratAlairok.FelhasznaloCsoport_Id_Alairo, AlairasokUtility.IsTomeges(Record));
                        if (!String.IsNullOrEmpty(result_Kiadmanyozas.ErrorCode))
                        {
                            // hiba:
                            //ContextUtil.SetAbort();
                            //log.WsEnd(ExecParam, result_Kiadmanyozas);
                            //return result_Kiadmanyozas;

                            throw new ResultException(result_Kiadmanyozas);
                        }
                    }
                }
            }

            #region Jogosult�s ad�sa az al��r�nak

            if (Record.Updated.FelhaszCsop_Id_HelyettesAlairo)
            {
                //helyettes al��r� is kap jogosults�got
                if (!String.IsNullOrEmpty(Record.FelhaszCsop_Id_HelyettesAlairo))
                {
                    RightsService aclService = new RightsService(this.dataContext);
                    Result aclResult = aclService.AddCsoportToJogtargy(ExecParam.Clone(), Record.Obj_Id, Record.FelhaszCsop_Id_HelyettesAlairo, 'I');
                }
            }

            #endregion

            #region Esem�nynapl�z�s

            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_IratAlairok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratAlairok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);

            // ha nincs hiba:
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                // Ha kiadm�nyoz� bejegyz�st t�r�lt�nk, ki kell szedni az Irat felhCsop_Id_Kiadmanyozo mez�j�t is

                // EREC_IratAlairok GET
                EREC_IratAlairokService service_iratAlairok = new EREC_IratAlairokService(this.dataContext);

                Result result_alairoGet = service_iratAlairok.Get(ExecParam);
                if (!String.IsNullOrEmpty(result_alairoGet.ErrorCode))
                {
                    // hiba:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParam, result_alairoGet);
                    //return result_alairoGet;

                    throw new ResultException(result_alairoGet);
                }
                else
                {
                    EREC_IratAlairok erec_iratAlairok = (EREC_IratAlairok)result_alairoGet.Record;

                    // ha kiadm�nyoz� volt, akkor Irat felhCsop_Id_Kiadmanyozo mez�j�nek t�rl�se:
                    if (erec_iratAlairok.AlairoSzerep == KodTarak.ALAIRO_SZEREP.Kiadmanyozo)
                    {
                        ExecParam execParam_iratUpdate = ExecParam.Clone();
                        Result result_iratUpdate = SetKiadmanyozo(execParam_iratUpdate, erec_iratAlairok.Obj_Id, "");
                        if (!String.IsNullOrEmpty(result_iratUpdate.ErrorCode))
                        {
                            // hiba:
                            //ContextUtil.SetAbort();
                            //log.WsEnd(ExecParam, result_iratUpdate);
                            //return result_iratUpdate;

                            throw new ResultException(result_iratUpdate);
                        }
                    }
                }
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_IratAlairok", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    
    #endregion

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratAlairokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IratAlairokSearch _EREC_IratAlairokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            // Snapshot Isolation Level
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtension(ExecParam, _EREC_IratAlairokSearch);

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere && !Search.IsIdentical(_EREC_IratAlairokSearch, new EREC_IratAlairokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "EREC_IratAlairok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_IratAlairokSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (string.IsNullOrEmpty(query.Where))
                    {
                        query.Where = _EREC_IratAlairokSearch.WhereByManual;
                    }
                    else
                    {
                        query.Where += " and " + _EREC_IratAlairokSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]    
    public Result HistoryGetAllByObjId(ExecParam ExecParam, string ObjId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            // Snapshot Isolation Level
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.HistoryGetAllRecordByObjId(ExecParam, ObjId);
            

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    public Result AlairasElutasitasByCsatolmanyId(ExecParam execParam, string csatolmanyId, string megjegyzes)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Csatolm�nyId alapj�n IratId lek�r�se

            EREC_CsatolmanyokService service_csatolmanyok = new EREC_CsatolmanyokService(this.dataContext);

            ExecParam execParam_csatolmany = execParam.Clone();
            execParam_csatolmany.Record_Id = csatolmanyId;

            Result result_CsatolmanyGet = service_csatolmanyok.Get(execParam_csatolmany);
            if (result_CsatolmanyGet.IsError)
            {
                // hiba:
                throw new ResultException(result_CsatolmanyGet);
            }

            EREC_Csatolmanyok csatolmany = (EREC_Csatolmanyok)result_CsatolmanyGet.Record;

            string iratId = csatolmany.IraIrat_Id;

            if (string.IsNullOrEmpty(iratId))
            {
                // hiba, Irat beazonos�t�sa sikertelen!
                throw new ResultException(52761);
            }

            #endregion
            
            #region IratAlairok rekord lek�r�se:

            EREC_IratAlairokSearch search_iratAlairok = new EREC_IratAlairokSearch();

            // Keres�s: az adott irathoz a felhasznalo az al��r�, �s �llapot al��rand�

            search_iratAlairok.Obj_Id.Value = iratId;
            search_iratAlairok.Obj_Id.Operator = Query.Operators.equals;

            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Value = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;
            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.Group = "alairo";
            search_iratAlairok.FelhasznaloCsoport_Id_Alairo.GroupOperator = Query.Operators.or;

            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.Value = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.Operator = Query.Operators.equals;
            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.Group = "alairo";
            search_iratAlairok.FelhaszCsop_Id_HelyettesAlairo.GroupOperator = Query.Operators.or;

            search_iratAlairok.Allapot.Value = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
            search_iratAlairok.Allapot.Operator = Query.Operators.equals;

            Result result_IratAlairokGetAll = this.GetAll(execParam.Clone(), search_iratAlairok);
            if (result_IratAlairokGetAll.IsError)
            {
                // hiba:
                throw new ResultException(result_IratAlairokGetAll);
            }

            Logger.Debug("EREC_IratAlairok lek�r�se: tal�latok sz�ma: " + result_IratAlairokGetAll.Ds.Tables[0].Rows.Count);

            if (result_IratAlairokGetAll.Ds.Tables[0].Rows.Count == 0)
            {
                // hiba, Nincs felv�ve el�jegyzett al��r�s az irathoz!
                throw new ResultException(52762);
            }

            DataRow row_IratAlairok = result_IratAlairokGetAll.Ds.Tables[0].Rows[0];

            #endregion

            #region IratAlairok UPDATE

            EREC_IratAlairok iratAlairok_UpdateObj = new EREC_IratAlairok();
            iratAlairok_UpdateObj.Updated.SetValueAll(false);
            iratAlairok_UpdateObj.Base.Updated.SetValueAll(false);

            string iratAlairok_Id = row_IratAlairok["Id"].ToString();
            string iratAlairok_Ver = row_IratAlairok["Ver"].ToString();

            // Verzi�:
            iratAlairok_UpdateObj.Base.Ver = iratAlairok_Ver;
            iratAlairok_UpdateObj.Base.Updated.Ver = true;

            // �llapot: Visszautas�tott:
            iratAlairok_UpdateObj.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Visszautasitott;
            iratAlairok_UpdateObj.Updated.Allapot = true;

            // D�tum:
            iratAlairok_UpdateObj.AlairasDatuma = DateTime.Now.ToString();
            iratAlairok_UpdateObj.Updated.AlairasDatuma = true;

            // Megjegyz�s:
            iratAlairok_UpdateObj.Leiras = megjegyzes;
            iratAlairok_UpdateObj.Updated.Leiras = true;

            ExecParam execParam_iratAlairokUpdate = execParam.Clone();
            execParam_iratAlairokUpdate.Record_Id = iratAlairok_Id;

            Result result_iratAlairokUpdate = this.Update(execParam_iratAlairokUpdate, iratAlairok_UpdateObj);
            if (result_iratAlairokUpdate.IsError)
            {
                // hiba:
                throw new ResultException(result_iratAlairokUpdate);
            }

            result = result_iratAlairokUpdate;

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }
    /// <summary>
    /// Alap�llapotba teszi a param�terk�nt megadott irat �sszes al��r� rekordj�t
    /// </summary>
    /// <param name="execParam">ExecParam objektum</param>
    /// <param name="IratId">Irat Id-ja</param>
    /// <returns></returns>
    [WebMethod()]
    public Result AlairasAlapallapotbaHelyezes(ExecParam execParam, string IratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();            
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Al��r�sok megkeres�se
            EREC_IratAlairokSearch search = new EREC_IratAlairokSearch();
            search.Obj_Id.Value = IratId;
            search.Obj_Id.Operator = Query.Operators.equals;
            Result result_alairok = this.GetAll(execParam, search);
            if (result_alairok.IsError)
                throw new ResultException(result_alairok);
            if (result_alairok.Ds.Tables[0].Rows.Count == 0) {
                Logger.Info("Nincs bejegyzett al��r�!");
                return result;
            }            
            #endregion

            #region Ellen�rz�s, hogy az�ta nem lett-e m�r kiadm�nyozott az irat.
            EREC_IraIratokService service_iratok = new EREC_IraIratokService();
            ExecParam execparam_Iratok = execParam.Clone();
            execparam_Iratok.Record_Id = IratId;

            Result result_iratok = service_iratok.Get(execparam_Iratok);

            if (result_iratok.IsError || result_iratok.Record == null)
                throw new ResultException(result_iratok);

            EREC_IraIratok IratObj = result_iratok.Record as EREC_IraIratok;

            if (IratObj.Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
                throw new ResultException("Az Irat �llapota nem megfelel�!");
            #endregion

            #region Update - Nem t�meges :(
            foreach (DataRow dr in result_alairok.Ds.Tables[0].Rows)
            {
                string Id = dr["Id"].ToString();
                string Ver = dr["Ver"].ToString();

                ExecParam execParam_upd = execParam.Clone();
                execParam_upd.Record_Id = Id;
                
                EREC_IratAlairok obj = new EREC_IratAlairok();               

                obj.Updated.SetValueAll(false);
                obj.Base.Updated.SetValueAll(false);

                obj.Base.Ver = Ver;
                obj.Base.Updated.Ver = true;

                obj.AlairasDatuma = string.Empty;
                obj.Updated.AlairasDatuma = true;

                obj.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
                obj.Updated.Allapot = true;
                
                obj.Leiras = "Az al��r�si folyamat �jrakezd�s�t kezdem�nyezte az irat �gyint�z�je";
                obj.Updated.Leiras = true;

                Result result_update = this.Update(execParam_upd, obj);
                if (result_update.IsError)
                    throw new ResultException(result_update);

            }
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Visszavonja az execParam-ban megadott azonosit�j� param�terk�nt megadott irathoz tartoz� al��r� rekordot 
    /// </summary>
    /// <param name="execParam">ExecParam Objektum -> Al��r� rekorddal</param>
    /// <param name="IratId"></param>
    /// <param name="VisszavonasIndok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result AlairasVisszavonas(ExecParam execParam, string IratId, string VisszavonasIndok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (string.IsNullOrEmpty(execParam.Record_Id) || string.IsNullOrEmpty(IratId))
            {
                Logger.Error(string.Format("Hi�nyz� param�terek:{0}IratId = {1}{0}ExecParam.RecordId{2}",Environment.NewLine,execParam.Record_Id,IratId));                    
                return result;
            }

            if (string.IsNullOrEmpty(VisszavonasIndok))
            {
                Logger.Info("Visszavon�s indoka �res!");                
            }


            #region Ellen�rz�s, hogy az�ta v�ltozott-e a visszavon�shoz sz�ks�ges el�felt�telek
            Result result_elofeltetelek = this.CheckAlairasVisszavonhato(execParam.Clone(), IratId);
            if (result_elofeltetelek.IsError)
                // BUG_7238
                // throw new ResultException(result_elofeltetelek);          
                return result_elofeltetelek;

            #endregion

            #region Al��r�sok Get
            Result result_alairok = this.Get(execParam);
            if (result_alairok.IsError)
                throw new ResultException(result_alairok);
            if (result_alairok.Record == null)
            {
                Logger.Info("Nincs bejegyzett al��r�!");
                return result;
            }
            #endregion            
            EREC_IratAlairok Record = result_alairok.Record as EREC_IratAlairok;
            EREC_IratAlairok obj = new EREC_IratAlairok();
            obj.Id = Record.Id;

            obj.Updated.SetValueAll(false);
            obj.Base.Updated.SetValueAll(false);

            obj.Base.Ver = Record.Base.Ver;
            obj.Base.Updated.Ver = true;
            
            //D�tum
            obj.AlairasDatuma = string.Empty;
            obj.Updated.AlairasDatuma = true;
            
            //�llapot
            obj.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
            obj.Updated.Allapot = true;

            //Le�r�s
            obj.Leiras = VisszavonasIndok;
            obj.Updated.Leiras = true;

            Result result_update = this.Update(execParam.Clone(), obj);
            if (result_update.IsError)                
                throw new ResultException(result_update);

            #region Ha kiadm�nyoz� vonja vissza akkor legyen az �llapota iktatott (#3102)
            if (Record.AlairoSzerep == KodTarak.ALAIRO_SZEREP.Kiadmanyozo)
            {
                using (EREC_IraIratokService svcIratok = new EREC_IraIratokService(dataContext))
                {
                    ExecParam execParamIratok = execParam.Clone();
                    execParamIratok.Record_Id = IratId;
                    Result res_Iratok = svcIratok.Get(execParamIratok);
                    if (res_Iratok.IsError)                        
                        throw new ResultException(res_Iratok);
                    if (res_Iratok.Record == null)
                        throw new ResultException("");
                    EREC_IraIratok erecIratok = res_Iratok.Record as EREC_IraIratok;
                    if (erecIratok.Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
                    {
                        EREC_IraIratok newObj = new EREC_IraIratok();
                        

                        newObj.Updated.SetValueAll(false);
                        newObj.Base.Updated.SetValueAll(false);
                        
                        newObj.Id = IratId;

                        newObj.Base.Ver = erecIratok.Base.Ver;
                        newObj.Base.Updated.Ver = true;
                        
                        newObj.Allapot = KodTarak.IRAT_ALLAPOT.Iktatott;
                        newObj.Updated.Allapot = true;

                        res_Iratok = svcIratok.Update(execParamIratok, newObj);
                        if (res_Iratok.IsError)
                            throw new ResultException(res_Iratok);

                        #region IratPeldanyok allapot�nak vissza �ll�t�sa
                        EREC_PldIratPeldanyokService svcIratPeldanyok = new EREC_PldIratPeldanyokService(dataContext);
                        
                        EREC_PldIratPeldanyokSearch schIratPeldanyok = new EREC_PldIratPeldanyokSearch();
                        schIratPeldanyok.IraIrat_Id.Value = IratId;
                        schIratPeldanyok.IraIrat_Id.Operator = Query.Operators.equals;
                        schIratPeldanyok.IraIrat_Id.GroupOperator = Query.Operators.and;
                        schIratPeldanyok.Allapot.Value = KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott;

                        Result res_IratPeldanyok = svcIratPeldanyok.GetAll(execParam.Clone(), schIratPeldanyok);
                        if (res_IratPeldanyok.IsError)
                            throw new ResultException(res_IratPeldanyok);
                        foreach  (DataRow  dr in res_IratPeldanyok.Ds.Tables[0].Rows)
                        {
                            string PldId = dr["Id"].ToString();
                            string PldVer = dr["Ver"].ToString();
                            EREC_PldIratPeldanyok newPldObj = new EREC_PldIratPeldanyok();


                            newPldObj.Updated.SetValueAll(false);
                            newPldObj.Base.Updated.SetValueAll(false);

                            newPldObj.Id = PldId;

                            newPldObj.Base.Ver = PldVer;
                            newPldObj.Base.Updated.Ver = true;

                            newPldObj.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                            newPldObj.Updated.Allapot = true;
                            
                            ExecParam pldIratokUpd = execParam.Clone();
                            pldIratokUpd.Record_Id = PldId;

                            res_IratPeldanyok = svcIratPeldanyok.Update(pldIratokUpd, newPldObj);
                            if (res_IratPeldanyok.IsError)
                                throw new ResultException(res_IratPeldanyok);
                            
                        }
                        #endregion
                    }                    
                }
            }
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    // Be�ll�tja a kiadm�nyoz�t a megadott iratban
    // Seg�dfv.
    private Result SetKiadmanyozo(ExecParam execParam, String erec_IraIratok_Id, String felhCsopId_Kiadmanyozo)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            EREC_IraIratokService service_Iratok = new EREC_IraIratokService(this.dataContext);

            // Irat GET:
            ExecParam execParam_iratGet = execParam.Clone();
            execParam_iratGet.Record_Id = erec_IraIratok_Id;

            Result result_iratGet = service_Iratok.Get(execParam_iratGet);
            if (!String.IsNullOrEmpty(result_iratGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_iratGet, result_iratGet);
                //return result_iratGet;

                throw new ResultException(result_iratGet);
            }
            else
            {
                EREC_IraIratok erec_IraIrat = (EREC_IraIratok)result_iratGet.Record;

                // Irat UPDATE:
                erec_IraIrat.Updated.SetValueAll(false);
                erec_IraIrat.Base.Updated.SetValueAll(false);
                erec_IraIrat.Base.Updated.Ver = true;

                erec_IraIrat.FelhasznaloCsoport_Id_Kiadmany = felhCsopId_Kiadmanyozo;
                erec_IraIrat.Updated.FelhasznaloCsoport_Id_Kiadmany = true;

                ExecParam execParam_IratUpdate = execParam.Clone();
                execParam_IratUpdate.Record_Id = erec_IraIrat.Id;

                result = service_Iratok.Update(execParam_IratUpdate, erec_IraIrat);
                
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// Ellen�rzi a param�terk�nt megadott iratr�l, hogy visszavonahat�-e hozz� tartoz� al��r�s.
    /// </summary>
    /// <param name="execParam">ExecParam objektum</param>
    /// <param name="IratId">Ellen�rz�sre kijel�lt irat Id-ja</param>
    /// <returns></returns>
    [WebMethod()]
    public  Result CheckAlairasVisszavonhato(ExecParam execParam, string IratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result _result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
         
        try
        {
             isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
             isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string FelhasznaloId = execParam.Felhasznalo_Id;       
            EREC_IratAlairokService service = new EREC_IratAlairokService();

            #region A felhaszn�l� szerepel az �rv�nyes al��r�k k�z�tt �s � van al��r�son
            //Lek�rj�k az utols� al��r�it aki m�r al��rta
            EREC_IratAlairokSearch alairokSearch = new EREC_IratAlairokSearch();
            alairokSearch.Obj_Id.Value = IratId;
            alairokSearch.Obj_Id.Operator = Query.Operators.equals;
            alairokSearch.Obj_Id.GroupOperator = Query.Operators.and;
            alairokSearch.Allapot.Value = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
            alairokSearch.Allapot.Operator = Query.Operators.notequals;
            alairokSearch.OrderBy = "AlairasSorrend DESC";            
            alairokSearch.TopRow = 1;

            Result result = service.GetAll(execParam, alairokSearch);
            if (result.IsError)
                throw new ResultException(result);
            // Ha �res a t�bla hiba.
            if (result.Ds.Tables[0].Rows.Count == 0)
            {
                // BUG_7238
                //throw new ResultException("Nincs bejegyzett al��r�!");
                result.ErrorCode = "Nincs bejegyzett al��r�!";
                result.ErrorMessage = "Nincs bejegyzett al��r�!";
                Logger.Debug("IratId: " + IratId +", Nincs bejegyzett al��r�!");
                return result;
            }
            DataRow lastobj = result.Ds.Tables[0].Rows[0];
                
            // Ellen�rizz�k, hogy � k�vetkezik-e az al��r�k sor�ban.
            if (lastobj["FelhasznaloCsoport_Id_Alairo"].ToString() != FelhasznaloId
                && lastobj["FelhaszCsop_Id_HelyettesAlairo"].ToString() != FelhasznaloId)
            {
                // BUG_7238
                //throw new ResultException("Nem a felhaszn�l� k�vetkezik az al��r�ssal!");  
                result.ErrorCode = "Nem a felhaszn�l� k�vetkezik az al��r�ssal!";
                result.ErrorMessage = "Nem a felhaszn�l� k�vetkezik az al��r�ssal!";
                Logger.Debug("IratId: " + IratId + ", Nem a felhaszn�l� k�vetkezik az al��r�ssal!");
                return result;
            }

            //Az al��r� sora al��rt vagy visszautas�tott 
            if (lastobj["Allapot"].ToString() != KodTarak.IRATALAIRAS_ALLAPOT.Alairt && lastobj["Allapot"].ToString() != KodTarak.IRATALAIRAS_ALLAPOT.Visszautasitott)
            {
                // BUG_7238
                //throw new ResultException("Az al��r�s �llapota nem megfelel�!");
                result.ErrorCode = "Az al��r�s �llapota nem megfelel�!";
                result.ErrorMessage = "Az al��r�s �llapota nem megfelel�!";
                Logger.Debug("IratId: " + IratId + ", Az al��r�s �llapota nem megfelel�!");
                return result;
            }

            #endregion

            #region Van olyan iratp�ld�nya az iratnak, amely expedi�lt vagy post�zott => Nem vonhat� vissza
            EREC_PldIratPeldanyokService service_pld = new EREC_PldIratPeldanyokService();
            EREC_PldIratPeldanyokSearch search_pld = new EREC_PldIratPeldanyokSearch();
            search_pld.IraIrat_Id.Value = IratId;
            search_pld.IraIrat_Id.Operator = Query.Operators.equals;
            // BUG_7238
            string allapotok = Search.GetSqlInnerString(new string[] {
                  KodTarak.IRATPELDANY_ALLAPOT.Expedialt
                , KodTarak.IRATPELDANY_ALLAPOT.Postazott});
            search_pld.Allapot.Value = allapotok;
            search_pld.Allapot.Operator = Query.Operators.inner;
            search_pld.Allapot.Group = "531";
            search_pld.Allapot.GroupOperator = Query.Operators.or;

            search_pld.TovabbitasAlattAllapot.Value = allapotok;
            search_pld.TovabbitasAlattAllapot.Operator = Query.Operators.inner;
            search_pld.TovabbitasAlattAllapot.Group = "531";
            search_pld.TovabbitasAlattAllapot.GroupOperator = Query.Operators.or;

            //search_pld.IraIrat_Id.GroupOperator = Query.Operators.and;
            //search_pld.Allapot.Value = KodTarak.IRATPELDANY_ALLAPOT.Expedialt;
            //search_pld.Allapot.Operator = Query.Operators.equals;
            //search_pld.Allapot.GroupOperator = Query.Operators.or;
            //search_pld.Allapot.Value = KodTarak.IRATPELDANY_ALLAPOT.Postazott;
            //search_pld.Allapot.Operator = Query.Operators.equals;

            Result result_pld = service_pld.GetAll(execParam.Clone(), search_pld);
        
            if (result_pld.IsError)
                throw new ResultException(result_pld);
      
            if (result_pld.Ds.Tables[0].Rows.Count > 0 )
            {
                // BUG_7238
                //throw new ResultException("Van olyan iratp�ld�nya az iratnak amely expedi�lt vagy post�zott �llapot�");         
                result.ErrorCode = "Van olyan iratp�ld�nya az iratnak amely expedi�lt vagy post�zott �llapot�!";
                result.ErrorMessage = "Van olyan iratp�ld�nya az iratnak amely expedi�lt vagy post�zott �llapot�!";
                Logger.Debug("IratId: " + IratId + ", Van olyan iratp�ld�nya az iratnak amely expedi�lt vagy post�zott �llapot�!");
                return result;
            }
            #endregion
       
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            _result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, _result);
        return _result;
    }

    /// <summary>
    /// Az irathoz tartoz� kiadm�nyoz�k sz�ma (EREC_IratAlairok t�bl�ban)
    /// Seg�df�ggv�ny, nem webmethod
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_IraIratok_Id"></param>
    /// <returns>Az irathoz tartoz� kiadm�nyoz�k sz�ma; ha hiba volt: -1</returns>
    public int KiadmanyozokSzama(ExecParam execParam, String erec_IraIratok_Id)
    {
        if (execParam == null 
            || String.IsNullOrEmpty(erec_IraIratok_Id))
        {
            return -1;
        }

        EREC_IratAlairokService service_iratAlairok = new EREC_IratAlairokService(this.dataContext);

        EREC_IratAlairokSearch search_iratAlairok = new EREC_IratAlairokSearch();
        search_iratAlairok.Obj_Id.Value = erec_IraIratok_Id;
        search_iratAlairok.Obj_Id.Operator = Query.Operators.equals;

        Result result_alairokGetAll = service_iratAlairok.GetAll(execParam, search_iratAlairok);
        if (!String.IsNullOrEmpty(result_alairokGetAll.ErrorCode))
        {
            // hiba:
            return -1;
        }
        else
        {
            try
            {
                int kiadmanyozokSzama = 0;

                foreach (DataRow row in result_alairokGetAll.Ds.Tables[0].Rows)
                {
                    String alairoSzerep = row["AlairoSzerep"].ToString();
                    if (alairoSzerep == KodTarak.ALAIRO_SZEREP.Kiadmanyozo)
                    {
                        kiadmanyozokSzama++;
                    }
                }

                return kiadmanyozokSzama;
            }
            catch
            {
                return -1;
            }
        }
    }
    




}