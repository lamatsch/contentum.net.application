using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IratMellekletekService : System.Web.Services.WebService
{
    #region Generáltból átvettek
    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_IratMellekletek Record)
    /// Egy rekord felvétele a EREC_IratMellekletek táblába. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratMellekletek))]
    public Result Insert(ExecParam ExecParam, EREC_IratMellekletek Record)
    {
        return this.Insert(ExecParam, Record, true);
    }


    public Result Insert(ExecParam ExecParam, EREC_IratMellekletek Record, bool checkBarcode)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //Vonalkód ellenőrzése
            KRT_BarkodokService srvBarcode = new KRT_BarkodokService();
            Result resBarcode = new Result();
            if (checkBarcode)
            {
                srvBarcode = new KRT_BarkodokService(this.dataContext);
                resBarcode = srvBarcode.CheckBarcode(ExecParam, Record.BarCode);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }
            }

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //vonalkód kötése, ha meg volt adva
            if (checkBarcode)
            {
                if (!String.IsNullOrEmpty(resBarcode.Uid))
                {
                    string barkodId = resBarcode.Uid;
                    string barkodVer = resBarcode.Ds.Tables[0].Rows[0]["Ver"].ToString();
                    string mellekletId = result.Uid;
                    resBarcode = new Result();
                    resBarcode = srvBarcode.BarkodBindToIratMelleklet(ExecParam, barkodId, mellekletId, barkodVer);
                    if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                    {
                        throw new ResultException(resBarcode);
                    }
                }
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_IratMellekletek", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratMellekletekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IratMellekletekSearch _EREC_IratMellekletekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_IratMellekletekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}