using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IratMetaDefinicioService : System.Web.Services.WebService
{
    #region Generaltbol atvett
    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_IratMetaDefinicio Record)
    /// Egy rekord felv�tele a EREC_IratMetaDefinicio t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratMetaDefinicio))]
    public Result Insert(ExecParam ExecParam, EREC_IratMetaDefinicio Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // �gyt�pus szinten k�dgener�l�s, ha kell, �s k�dellen�rz�s
            if ((String.IsNullOrEmpty(Record.EljarasiSzakasz) || !Record.Updated.EljarasiSzakasz)
                && (String.IsNullOrEmpty(Record.Irattipus) || !Record.Updated.Irattipus)
                && !String.IsNullOrEmpty(Record.UgytipusNev) && Record.Updated.UgytipusNev
                )
            {
                if (String.IsNullOrEmpty(Record.Ugytipus) || !Record.Updated.Ugytipus)
                {
                    ExecParam execParam_nextUgytipusCode = ExecParam.Clone();
                    execParam_nextUgytipusCode.Record_Id = Record.Ugykor_Id;

                    Result result_nextUgytipusCode = GetNextUgytipusCodeByUgykor(execParam_nextUgytipusCode);

                    if (!String.IsNullOrEmpty(result_nextUgytipusCode.ErrorCode))
                    {
                        throw new ResultException(result_nextUgytipusCode);
                    }

                    String NextCode = result_nextUgytipusCode.Record.ToString();

                    Record.Ugytipus = NextCode;
                    Record.Updated.Ugytipus = true;
                }
                else
                {
                    EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();
                    search.Ugykor_Id.Value = Record.Ugykor_Id;
                    search.Ugykor_Id.Operator = Query.Operators.equals;

                    search.Ugytipus.Value = Record.Ugytipus;
                    search.Ugytipus.Operator = Query.Operators.equals;

                    Result result_ugytipusGetAll = GetAll(ExecParam, search);

                    if (!String.IsNullOrEmpty(result_ugytipusGetAll.ErrorCode))
                    {
                        throw new ResultException(result_ugytipusGetAll);
                    }
                    else if (result_ugytipusGetAll.Ds.Tables[0].Rows.Count > 0)
                    {
                        throw new ResultException(52800); // Az �gyt�pus k�d nem egyedi az �gyk�r�n bel�l!
                    }
                }
            }

            #region ellen�rz�s, volt-e m�r ilyen bejegyz�s
            // minden szinten: van-e m�r ilyen rekord:
            CheckIfRecordExists(ExecParam, Record);
            #endregion ellen�rz�s, volt-e m�r ilyen bejegyz�s

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_IratMetaDefinicio", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Update(ExecParam ExecParam, EREC_IratMetaDefinicio Record)
    /// Az EREC_IratMetaDefinicio t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratMetaDefinicio))]
    public Result Update(ExecParam ExecParam, EREC_IratMetaDefinicio Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region ellen�rz�s, volt-e m�r ilyen bejegyz�s
            // minden szinten: van-e m�r ilyen rekord:
            CheckIfRecordExists(ExecParam, Record);
            #endregion ellen�rz�s, volt-e m�r ilyen bejegyz�s

            // le kell k�rni az eredeti objektumot, hogy meghat�rozzuk majd a gyermekeit/feletteseit
            Result result_get = sp.Get(ExecParam);

            if (!String.IsNullOrEmpty(result_get.ErrorCode))
            {
                throw new ResultException(result_get);
            }

            EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_get.Record;

            // saj�t szint update
            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // al�- ill. f�l�rendelt szintek meghat�roz�sa, update-je, ha sz�ks�ges,
            // mert valamely redund�ns �rt�k megv�ltozott

            // TODO: a mostani megold�s nem engedi a mozgat�st, pl. elj�r�si szakasz �thelyez�se egy m�sik �gyt�pusba,
            // mert az �gyt�pus megv�ltoz�sa minden azonos �gyt�pus� elemet m�dos�t az �j �gyt�pusra
            bool redundantValueChanged = false;
            if (Record.UgykorKod != erec_IratMetaDefinicio.UgykorKod
                || Record.UgyFajta != erec_IratMetaDefinicio.UgyFajta
                || Record.Ugytipus != erec_IratMetaDefinicio.Ugytipus
                || Record.UgytipusNev != erec_IratMetaDefinicio.UgytipusNev
                || Record.EljarasiSzakasz != erec_IratMetaDefinicio.EljarasiSzakasz)
            {
                redundantValueChanged = true;
            }

            if (redundantValueChanged && !String.IsNullOrEmpty(erec_IratMetaDefinicio.Ugykor_Id))  // az �gyk�r k�telez� param�ter
            {
                EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();
                //1. UgykorKod v�ltoz�s: minden ilyen Ugykor_Id-vel rendelkez� t�tel
                //2. UgyFajta v�ltoz�s: minden ilyen �gyt�pus� t�tel
                //3. UgytipusNev v�ltoz�s: minden ilyen �gyt�pus� t�tel
                //4. EljarasiSzakasz v�ltoz�s: minden ilyen elj�r�si szakasz� t�tel

                // az eredeti rekordot nem k�rj�k le �jra
                search.Id.Value = ExecParam.Record_Id;
                search.Id.Operator = Query.Operators.notequals;

                // az �gyk�r azonos�t� nem v�ltozhat �s minden m�dos�tand� t�telben azonos
                search.Ugykor_Id.Value = erec_IratMetaDefinicio.Ugykor_Id;
                search.Ugykor_Id.Operator = Query.Operators.equals;

                #region �gyk�r szint
                if (Record.UgykorKod != erec_IratMetaDefinicio.UgykorKod)
                {
                    Result result_getall = GetAll(ExecParam, search);
                    if (!String.IsNullOrEmpty(result_getall.ErrorCode))
                    {
                        throw new ResultException(result_getall);
                    }

                    // tal�lt elemek UPDATE-je
                    int count = result_getall.Ds.Tables[0].Rows.Count;
                    if (count > 0)
                    {
                        ExecParam[] execParams = new ExecParam[count];

                        EREC_IratMetaDefinicio erec_IratMetaDefinicio_modify = new EREC_IratMetaDefinicio();
                        erec_IratMetaDefinicio_modify.Updated.SetValueAll(false);
                        erec_IratMetaDefinicio_modify.Base.Updated.SetValueAll(false);

                        erec_IratMetaDefinicio_modify.UgykorKod = Record.UgykorKod;
                        erec_IratMetaDefinicio_modify.Updated.UgykorKod = true;

                        for (int i = 0; i < count; i++)
                        {
                            execParams[i] = ExecParam.Clone();
                            execParams[i].Record_Id = result_getall.Ds.Tables[0].Rows[i]["Id"].ToString();

                            // Verzi�
                            erec_IratMetaDefinicio_modify.Base.Ver = result_getall.Ds.Tables[0].Rows[i]["Ver"].ToString();
                            erec_IratMetaDefinicio_modify.Base.Updated.Ver = true;

                            Result result_update = sp.Insert(Constants.Update, execParams[i], erec_IratMetaDefinicio_modify);

                            if (!String.IsNullOrEmpty(result_update.ErrorCode))
                            {
                                throw new ResultException(result_update);
                            }
                        }

                        #region Esem�nynapl�z�s (al�rendelt szintek)
                        if (isTransactionBeginHere)
                        {
                            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                            string Ids = "";

                            for (int i = 0; i < execParams.Length; i++)
                            {
                                Ids += "'" + execParams[i].Record_Id + "',";
                            }

                            Ids = Ids.TrimEnd(',');

                            Result eventLogResult = eventLogService.InsertTomeges(ExecParam, Ids, "EREC_IratMetaDefinicio", "Modify");
                        }
                        #endregion Esem�nynapl�z�s (al�rendelt szintek)

                    }

                }
                #endregion �gyk�r szint

                #region �gyt�pus szint
                if (Record.Ugytipus != erec_IratMetaDefinicio.Ugytipus
                    || Record.UgytipusNev != erec_IratMetaDefinicio.UgytipusNev
                    || Record.UgyFajta != erec_IratMetaDefinicio.UgyFajta)
                {
                    // az �gyt�pus k�dja azonos�t
                    if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.Ugytipus))
                    {
                        search.Ugytipus.Value = erec_IratMetaDefinicio.Ugytipus;
                        search.Ugytipus.Operator = Query.Operators.equals;
                    }
                    else
                    {
                    // elvben NULL �gyt�pus nem m�dosulhat valamire, de biztons�g kedv��rt lekezelj�k
                        search.Ugytipus.Value = "";
                        search.Ugytipus.Operator = Query.Operators.isnull;
                    }

                    Result result_getall = GetAll(ExecParam, search);
                    if (!String.IsNullOrEmpty(result_getall.ErrorCode))
                    {
                        throw new ResultException(result_getall);
                    }

                    // tal�lt elemek UPDATE-je
                    int count = result_getall.Ds.Tables[0].Rows.Count;
                    if (count > 0)
                    {
                        ExecParam[] execParams = new ExecParam[count];

                        EREC_IratMetaDefinicio erec_IratMetaDefinicio_modify = new EREC_IratMetaDefinicio();
                        erec_IratMetaDefinicio_modify.Updated.SetValueAll(false);
                        erec_IratMetaDefinicio_modify.Base.Updated.SetValueAll(false);

                        erec_IratMetaDefinicio_modify.Ugytipus = Record.Ugytipus;
                        erec_IratMetaDefinicio_modify.Updated.Ugytipus = true;

                        erec_IratMetaDefinicio_modify.UgytipusNev = Record.UgytipusNev;
                        erec_IratMetaDefinicio_modify.Updated.UgytipusNev = true;

                        erec_IratMetaDefinicio_modify.UgyFajta = Record.UgyFajta;
                        erec_IratMetaDefinicio_modify.Updated.UgyFajta = true;

                        for (int i = 0; i < count; i++)
                        {
                            execParams[i] = ExecParam.Clone();
                            execParams[i].Record_Id = result_getall.Ds.Tables[0].Rows[i]["Id"].ToString();

                            // Verzi�
                            erec_IratMetaDefinicio_modify.Base.Ver = result_getall.Ds.Tables[0].Rows[i]["Ver"].ToString();
                            erec_IratMetaDefinicio_modify.Base.Updated.Ver = true;

                            Result result_update = sp.Insert(Constants.Update, execParams[i], erec_IratMetaDefinicio_modify);

                            if (!String.IsNullOrEmpty(result_update.ErrorCode))
                            {
                                throw new ResultException(result_update);
                            }
                        }

                        #region Esem�nynapl�z�s (al�rendelt szintek)
                        if (isTransactionBeginHere)
                        {
                            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                            string Ids = "";

                            for (int i = 0; i < execParams.Length; i++)
                            {
                                Ids += "'" + execParams[i].Record_Id + "',";
                            }

                            Ids = Ids.TrimEnd(',');

                            Result eventLogResult = eventLogService.InsertTomeges(ExecParam, Ids, "EREC_IratMetaDefinicio", "Modify");
                        }
                        #endregion Esem�nynapl�z�s (al�rendelt szintek)

                    }

                }
                #endregion �gyt�pus szint

                #region elj�r�si szakasz szint
                if (Record.EljarasiSzakasz != erec_IratMetaDefinicio.EljarasiSzakasz && !String.IsNullOrEmpty(Record.Ugytipus))
                {
                    // az �gyt�pus k�dja azonos�t (itt m�r az �j �rt�k, mert ha kellett, m�r m�dos�tottuk az el�z� l�p�sben)
                    search.Ugytipus.Value = Record.Ugytipus;
                    search.Ugytipus.Operator = Query.Operators.equals;

                    if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.EljarasiSzakasz))
                    {
                        search.EljarasiSzakasz.Value = erec_IratMetaDefinicio.EljarasiSzakasz;
                        search.EljarasiSzakasz.Operator = Query.Operators.equals;
                    }
                    else
                    {
                        // elvben NULL �gyt�pus nem m�dosulhat valamire, de biztons�g kedv��rt lekezelj�k
                        search.EljarasiSzakasz.Value = "";
                        search.EljarasiSzakasz.Operator = Query.Operators.isnull;
                    }

                    Result result_getall = GetAll(ExecParam, search);
                    if (!String.IsNullOrEmpty(result_getall.ErrorCode))
                    {
                        throw new ResultException(result_getall);
                    }

                    // tal�lt elemek UPDATE-je
                    int count = result_getall.Ds.Tables[0].Rows.Count;
                    if (count > 0)
                    {
                        ExecParam[] execParams = new ExecParam[count];

                        EREC_IratMetaDefinicio erec_IratMetaDefinicio_modify = new EREC_IratMetaDefinicio();
                        erec_IratMetaDefinicio_modify.Updated.SetValueAll(false);
                        erec_IratMetaDefinicio_modify.Base.Updated.SetValueAll(false);

                        erec_IratMetaDefinicio_modify.EljarasiSzakasz = Record.EljarasiSzakasz;
                        erec_IratMetaDefinicio_modify.Updated.EljarasiSzakasz = true;

                        for (int i = 0; i < count; i++)
                        {
                            execParams[i] = ExecParam.Clone();
                            execParams[i].Record_Id = result_getall.Ds.Tables[0].Rows[i]["Id"].ToString();

                            // Verzi�
                            erec_IratMetaDefinicio_modify.Base.Ver = result_getall.Ds.Tables[0].Rows[i]["Ver"].ToString();
                            erec_IratMetaDefinicio_modify.Base.Updated.Ver = true;

                            Result result_update = sp.Insert(Constants.Update, execParams[i], erec_IratMetaDefinicio_modify);

                            if (!String.IsNullOrEmpty(result_update.ErrorCode))
                            {
                                throw new ResultException(result_update);
                            }
                        }

                        #region Esem�nynapl�z�s (al�rendelt szintek)
                        if (isTransactionBeginHere)
                        {
                            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                            string Ids = "";

                            for (int i = 0; i < execParams.Length; i++)
                            {
                                Ids += "'" + execParams[i].Record_Id + "',";
                            }

                            Ids = Ids.TrimEnd(',');

                            Result eventLogResult = eventLogService.InsertTomeges(ExecParam, Ids, "EREC_IratMetaDefinicio", "Modify");
                        }
                        #endregion Esem�nynapl�z�s (al�rendelt szintek)

                    }

                }
                #endregion elj�r�si szakasz szint
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_IratMetaDefinicio", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az EREC_IratMetaDefinicio t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratMetaDefinicio))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // le kell k�rni az eredeti objektumot, hogy meghat�rozzuk majd a gyermekeit
            Result result_get = sp.Get(ExecParam);

            if (!String.IsNullOrEmpty(result_get.ErrorCode))
            {
                throw new ResultException(result_get);
            }

            EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_get.Record;

            // saj�t szint �rv�nytelen�t�se
            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // al�rendelt szintek meghat�roz�sa, �rv�nytelen�t�se
            if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.Ugykor_Id)  // az �gyk�r k�telez� param�ter
                && String.IsNullOrEmpty(erec_IratMetaDefinicio.Irattipus)) // ha az iratt�pus ki van t�ltve, nincsenek al�rendelt szintek
            {
                EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

                // az eredeti rekordot nem k�rj�k le �jra
                search.Id.Value = ExecParam.Record_Id;
                search.Id.Operator = Query.Operators.notequals;

                search.Ugykor_Id.Value = erec_IratMetaDefinicio.Ugykor_Id;
                search.Ugykor_Id.Operator = Query.Operators.equals;

                if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.Ugytipus))
                {
                    search.Ugytipus.Value = erec_IratMetaDefinicio.Ugytipus;
                    search.Ugytipus.Operator = Query.Operators.equals;

                    if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.EljarasiSzakasz))
                    {
                        search.EljarasiSzakasz.Value = erec_IratMetaDefinicio.EljarasiSzakasz;
                        search.EljarasiSzakasz.Operator = Query.Operators.equals;
                    }
                }

                Result result_getall = GetAll(ExecParam, search);
                if (!String.IsNullOrEmpty(result_getall.ErrorCode))
                {
                    throw new ResultException(result_getall);
                }

                // al�rendelt elemek �rv�nytelen�t�se
                int count = result_getall.Ds.Tables[0].Rows.Count;
                if (count > 0)
                {
                    ExecParam[] execParams = new ExecParam[count];

                    for (int i = 0; i < count; i++)
                    {
                        execParams[i] = ExecParam.Clone();
                        execParams[i].Record_Id = result_getall.Ds.Tables[0].Rows[i]["Id"].ToString();
                        Result result_invalidate = sp.Invalidate(execParams[i]);

                        if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                        {
                            throw new ResultException(result_invalidate);
                        }
                    }


                    #region Esem�nynapl�z�s (al�rendelt szintek)
                    if (isTransactionBeginHere)
                    {
                        KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                        string Ids = "";

                        for (int i = 0; i < execParams.Length; i++)
                        {
                            Ids += "'" + execParams[i].Record_Id + "',";
                        }

                        Ids = Ids.TrimEnd(',');

                        Result eventLogResult = eventLogService.InsertTomeges(ExecParam, Ids, "EREC_IratMetaDefinicio", "Invalidate");
                    }
                    #endregion Esem�nynapl�z�s (al�rendelt szintek)

                }

            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_IratMetaDefinicio", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion Generaltbol atvett

    // seg�delj�r�s az egyedis�g ellen�rz�s�re
    private void CheckIfRecordExists(ExecParam ExecParam, EREC_IratMetaDefinicio Record)
    {
        #region ellen�rz�s, volt-e m�r ilyen bejegyz�s
 
        // minden szinten: van-e m�r ilyen rekord:
        EREC_IratMetaDefinicioSearch search_getAll = new EREC_IratMetaDefinicioSearch();
        search_getAll.Ugykor_Id.Value = Record.Ugykor_Id;
        search_getAll.Ugykor_Id.Operator = Query.Operators.equals;

        if (!String.IsNullOrEmpty(ExecParam.Record_Id))
        {
            search_getAll.Id.Value = ExecParam.Record_Id;
            search_getAll.Id.Operator = Query.Operators.notequals;
        }

        if (!String.IsNullOrEmpty(Record.Ugytipus) && Record.Updated.Ugytipus)
        {
            search_getAll.Ugytipus.Value = Record.Ugytipus;
            search_getAll.Ugytipus.Operator = Query.Operators.equals;
        }
        else
        {
            search_getAll.Ugytipus.Value = "";
            search_getAll.Ugytipus.Operator = Query.Operators.isnull;
        }

        if (!String.IsNullOrEmpty(Record.EljarasiSzakasz) && Record.Updated.EljarasiSzakasz)
        {
            search_getAll.EljarasiSzakasz.Value = Record.EljarasiSzakasz;
            search_getAll.EljarasiSzakasz.Operator = Query.Operators.equals;
        }
        else
        {
            search_getAll.EljarasiSzakasz.Value = "";
            search_getAll.EljarasiSzakasz.Operator = Query.Operators.isnull;
        }

        if (!String.IsNullOrEmpty(Record.Irattipus) && Record.Updated.Irattipus)
        {
            search_getAll.Irattipus.Value = Record.Irattipus;
            search_getAll.Irattipus.Operator = Query.Operators.equals;
        }
        else
        {
            search_getAll.Irattipus.Value = "";
            search_getAll.Irattipus.Operator = Query.Operators.isnull;
        }

        Result result_getAll = GetAll(ExecParam, search_getAll);

        if (!String.IsNullOrEmpty(result_getAll.ErrorCode))
        {
            throw new ResultException(result_getAll);
        }

        if (result_getAll.Ds.Tables[0].Rows.Count > 0)
        {
            throw new ResultException(547);// M�r van ilyen bejegyz�s az adatb�zisban!
        }

        #endregion ellen�rz�s, volt-e m�r ilyen bejegyz�s
    
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratMetaDefinicioSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IratMetaDefinicioSearch _EREC_IratMetaDefinicioSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Logger.Info("EREC_IratMetaDefinicioService.GetAllWithExtension start");
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.GetAllWithExtension(ExecParam, _EREC_IratMetaDefinicioSearch);

        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Info("EREC_IratMetaDefinicioService.GetAllWithExtension end");
        log.WsEnd(ExecParam, result);
        return result;


    }


    /// <summary>
    /// Result.Record -ba tessz�k a megtal�lt sor id-j�t
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="Ugykor_Id"></param>
    /// <param name="Ugytipus"></param>
    /// <returns></returns>
    public Result GetIratMetaDefinicioIdByUgykorUgytipus(ExecParam execParam, string Ugykor_Id, string Ugytipus)
    {
        Logger.Info("EREC_IratMetaDefinicioService.GetIratMetaDefinicioIdByUgykorUgytipus start");
        Result result = new Result();
        result.Record = "";
        if (String.IsNullOrEmpty(Ugykor_Id))
        {
            result.Record = "";
            return result;
        }
        bool isConnectionOpenHere = false;

        try
        {

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

            search.Ugykor_Id.Value = Ugykor_Id;
            search.Ugykor_Id.Operator = Query.Operators.equals;

            if (!String.IsNullOrEmpty(Ugytipus))
            {
                search.Ugytipus.Value = Ugytipus;
                search.Ugytipus.Operator = Query.Operators.equals;
            }
            else
            {
                search.Ugytipus.Value = "";
                search.Ugytipus.Operator = Query.Operators.isnull;
            }
            
            search.EljarasiSzakasz.Operator = Query.Operators.isnull;
            search.Irattipus.Operator = Query.Operators.isnull;

            Result result_getAll = this.GetAll(execParam, search);
            if (!String.IsNullOrEmpty(result_getAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_getAll);
            }


            if (result_getAll.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = result_getAll.Ds.Tables[0].Rows[0];

                if (row["Id"] != null)
                {
                    result.Record = row["Id"].ToString();
                }
                else
                {
                    result.Record = "";
                }
            }

        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Info("EREC_IratMetaDefinicioService.GetIratMetaDefinicioIdByUgykorUgytipus end");
        return result;
    }


    /// <summary>
    /// Result.Record -ba tessz�k a megtal�lt sor id-j�t
    /// </summary>
    public Result GetIratMetaDefinicioIdByIrattipus(ExecParam execParam, string ugykor_Id, string ugytipus, string eljarasiSzakasz, string iratTipus)
    {
        Result result = new Result();
        result.Record = "";
        if (String.IsNullOrEmpty(ugykor_Id) || String.IsNullOrEmpty(ugytipus) 
            || String.IsNullOrEmpty(eljarasiSzakasz) || String.IsNullOrEmpty(iratTipus))
        {
            result.Record = "";
            return result;
        }
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

            search.Ugykor_Id.Value = ugykor_Id;
            search.Ugykor_Id.Operator = Query.Operators.equals;

            search.Ugytipus.Value = ugytipus;
            search.Ugytipus.Operator = Query.Operators.equals;

            search.EljarasiSzakasz.Value = eljarasiSzakasz;
            search.EljarasiSzakasz.Operator = Query.Operators.equals;

            search.Irattipus.Value = iratTipus;
            search.Irattipus.Operator = Query.Operators.equals;
            
            Result result_getAll = this.GetAll(execParam, search);
            if (!String.IsNullOrEmpty(result_getAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_getAll);
            }


            if (result_getAll.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = result_getAll.Ds.Tables[0].Rows[0];

                if (row["Id"] != null)
                {
                    result.Record = row["Id"].ToString();
                }
                else
                {
                    result.Record = "";
                }
            }

        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Info("EREC_IratMetaDefinicioService.GetIratMetaDefinicioIdByUgykorUgytipus end");

        return result;
    }

    /// <summary>
    /// Result.Record a megtal�lt sornak megfelel� irat metadefin�ci�
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="Ugykor_Id"></param>
    /// <param name="Ugytipus"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "EREC_IratMetaDefinicio objektum lek�r�se �gyk�r �s �gyt�pus alapj�n. Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratMetaDefinicio))]
    public Result GetIratMetaDefinicioByUgykorUgytipus(ExecParam execParam, string Ugykor_Id, string Ugytipus)
    {
        Logger.Info("EREC_IratMetaDefinicioService.GetIratMetaDefinicioByUgykorUgytipus start");
        Result result = new Result();

        if (String.IsNullOrEmpty(Ugykor_Id))
        {
            return result;
        }
        bool isConnectionOpenHere = false;

        try
        {

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

            search.Ugykor_Id.Value = Ugykor_Id;
            search.Ugykor_Id.Operator = Query.Operators.equals;

            if (!String.IsNullOrEmpty(Ugytipus))
            {
                search.Ugytipus.Value = Ugytipus;
                search.Ugytipus.Operator = Query.Operators.equals;
            }
            else
            {
                search.Ugytipus.Value = "";
                search.Ugytipus.Operator = Query.Operators.isnull;
            }

            search.EljarasiSzakasz.Operator = Query.Operators.isnull;
            search.Irattipus.Operator = Query.Operators.isnull;

            search.ErvKezd.Clear();
            search.ErvVege.Clear();

            search.OrderBy = "EREC_IratMetaDefinicio.LetrehozasIdo desc";

            Result result_getAll = this.GetAll(execParam, search);
            if (!String.IsNullOrEmpty(result_getAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_getAll);
            }

            if (result_getAll.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = result_getAll.Ds.Tables[0].Rows[0];
             
                // DataRow-b�l m�r �ssze lehet �ll�tani a businessObject-et, nem kell k�l�n Get:
                EREC_IratMetaDefinicio iratMetaDefObj = new EREC_IratMetaDefinicio();
                Utility.LoadBusinessDocumentFromDataRow(iratMetaDefObj, row);

                result.Record = iratMetaDefObj;                
            }

        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Info("EREC_IratMetaDefinicioService.GetIratMetaDefinicioByUgykorUgytipus end");
        return result;
    }

    /// <summary>
    /// Result.Record a megtal�lt sornak megfelel� irat metadefin�ci�
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="Ugykor_Id"></param>
    /// <param name="Ugytipus"></param>
    /// <param name="EljarasiSzakasz"></param>
    /// <param name="Irattipus"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "EREC_IratMetaDefinicio objektum lek�r�se �gyk�r �s �gyt�pus alapj�n. Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratMetaDefinicio))]
    public Result GetIratMetaDefinicioByIrattipus(ExecParam execParam, string Ugykor_Id, string Ugytipus, string EljarasiSzakasz, string Irattipus)
    {
        Logger.Info("EREC_IratMetaDefinicioService.GetIratMetaDefinicioByIrattipus start");
        Result result = new Result();

        if (String.IsNullOrEmpty(Ugykor_Id) || String.IsNullOrEmpty(Ugytipus)
            || String.IsNullOrEmpty(EljarasiSzakasz) || String.IsNullOrEmpty(Irattipus))
        {
            return result;
        }
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

            search.Ugykor_Id.Value = Ugykor_Id;
            search.Ugykor_Id.Operator = Query.Operators.equals;

            search.Ugytipus.Value = Ugytipus;
            search.Ugytipus.Operator = Query.Operators.equals;

            search.EljarasiSzakasz.Value = EljarasiSzakasz;
            search.EljarasiSzakasz.Operator = Query.Operators.equals;

            search.Irattipus.Value = Irattipus;
            search.Irattipus.Operator = Query.Operators.equals;

            Result result_getAll = this.GetAll(execParam, search);
            if (!String.IsNullOrEmpty(result_getAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_getAll);
            }


            if (result_getAll.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = result_getAll.Ds.Tables[0].Rows[0];

                // DataRow-b�l m�r �ssze lehet �ll�tani a businessObject-et, nem kell k�l�n Get:
                EREC_IratMetaDefinicio iratMetaDefObj = new EREC_IratMetaDefinicio();
                Utility.LoadBusinessDocumentFromDataRow(iratMetaDefObj, row);

                result.Record = iratMetaDefObj;
            }
        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Info("EREC_IratMetaDefinicioService.GetIratMetaDefinicioByIrattipus end");

        return result;
    }

    /// <summary>
    /// Nomen est omen. :)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="contentType">Ctt ide.</param>
    /// <returns>Result - Uid-ben a talalt id.</returns>
    ///
    [WebMethod()]
    public Result GetIdIratMetaDefinicioByContentType(ExecParam execParam, String contentType)
    {
        Logger.Info(String.Format("EREC_IratMetaDefinicioService.GetIdIratMetaDefinicioByContentType indul. param: contentType: {0}", contentType));
        Result result = new Result();

        if (String.IsNullOrEmpty(contentType))
        {
            Logger.Debug("Ures param ctt.");
            return result;
        }
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetIdByContentType(execParam, contentType);

        }
        catch (Exception e)
        {
            Logger.Error(e.Message);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Info("EREC_IratMetaDefinicioService.GetIdIratMetaDefinicioByContentType vege.");

        return result;
    }

    /// <summary>
    /// IratMetaDefinicioTerkephez meghat�rozza alulr�l felfel� hierarchikusan
    /// azon objektum Id-ket, melyek megfelelenek az �tadott keres�si felt�teleknek
    /// Ha nincs megadva felt�tel, �res t�bl�t ad vissza az �sszes Id helyett,
    /// az als�bb szinteken megfogalmazott felt�telekb�l sz�rmaz� sz�r�si
    /// t�bl�kkal akkor is megsz�ri a magasabb szinteket, ha azokra nincs egy�b
    /// felt�tel
    /// Nyilv�ntartja, ha egy szinten a felt�tel mellett m�r nem volt tal�lat,
    /// �gy megk�l�nb�ztethet� a felt�tel n�lk�li �s a felt�tel miatt �res
    /// sz�r�t�bla, �s nincs tov�bbi felesleges lek�r�s
    /// Visszaadott �rt�kek:
    ///    Table[0]: IsFilteredWithoutHit: '1', ha valamely felt�tel miatt nem volt tal�lat, 1 egy�bk�nt
    ///    Sz�r�t�bl�k (�res vagy Id-k, rendezve)
    ///    Table[1]: EREC_AgazatiJelek
    ///    Table[2]: EREC_IrattariTetelek
    ///    Table[3]: EREC_IratMetaDefinicio (�sszes t�nylegesen illeszked� rekord - a sz�l�k nem: Szint = 0)
    ///    Table[4]: EREC_IratMetaDefinicio (Irattipus, EljarasiSzakasz: null; Ugytipus: not null)
    ///    Table[5]: EREC_IratMetaDefinicio (Irattipus: null; EljarasiSzakasz: not null)
    ///    Table[6]: EREC_IratMetaDefinicio (Irattipus: not null)
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_IrattariTervHierarchiaSearch">Keres�si objektum, mely az EREC_AgazatiJelek, EREC_IrattariTetelek,
    ///    EREC_IratMetaDefinicio t�bl�kra vonatkoz� keres�si
    ///    objektumokat fogja egybe
    /// </param>
    /// <returns>Hiba vagy a Record �rt�ke true, ha a sz�r�si felt�telek miatt nem volt tal�lat, egy�bk�nt false, a DS pedig
    /// a sz�r�s eredm�nyek�pp kapott Id-kb�l �ll� t�bl�kat tartalmazza (�resek, he nem volt felt�tel)</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(IrattariTervHierarchiaSearch))]
    public Result GetAllIrattariTervHierarchiaFilter(ExecParam ExecParam, IrattariTervHierarchiaSearch _IrattariTervHierarchiaSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllIrattariTervHierarchiaFilter(ExecParam, _IrattariTervHierarchiaSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// GetNextUgytipusCodeByUgykor(ExecParam ExecParam)  
    /// A EREC_IratMetaDefinicio t�bla adott �gyk�rbe tartoz� automatikusan gener�lt k�d� �gyt�pusai alapj�n meghat�rozza a k�vetkez� k�dot.
    /// Az Ugykor_Id-t az ExecParam.Record_Id param�ter tartalmazza. 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    //[WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "A EREC_IratMetaDefinicio t�bla adott �gyk�rbe tartoz� automatikusan gener�lt k�d� �gyt�pusai alapj�n meghat�rozza a k�vetkez� k�dot. Az Ugykor_Id-t az ExecParam.Record_Id param�ter tartalmazza. Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    //[System.Xml.Serialization.XmlInclude(typeof(EREC_IratMetaDefinicio))]
    public Result GetNextUgytipusCodeByUgykor(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetNextUgytipusCodeByUgykor(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


}