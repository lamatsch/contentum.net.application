using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Collections.Generic;
using System.Data;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_Irat_IktatokonyveiService : System.Web.Services.WebService
{
    /// <summary>
    /// T�bb egym�skoz kapcsolt t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// (A GetAll kiterjeszt�se join m�velettel �sszekapcsolt t�bl�kra). 
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza</param>
    /// <param name="_EREC_Irat_IktatokonyveiSearch">A keres�si felt�teleket tartalmaz� objektum</param>
    /// <returns>Result.Ds DataSet objektum ker�l felt�lt�sre a lek�rdez�s eredm�ny�vel</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Irat_IktatokonyveiSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_Irat_IktatokonyveiSearch _EREC_Irat_IktatokonyveiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_Irat_IktatokonyveiSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// T�bb egym�skoz kapcsolt t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se, sz�k�tve a megadott iktat�k�nyvre.
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza</param>
    /// <param name="_EREC_IraIktatoKonyvek">Az iktat�k�nyvet tartalmaz� objektum, amire a tal�lati lista sz�k�tve lesz.</param>
    /// <param name="_EREC_Irat_IktatokonyveiSearch">A t�bl�ra vonatkoz� plusz keres�si felt�teleket tartalmaz� objektum.</param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Irat_IktatokonyveiSearch))]
    public Result GetAllByIktatoKonyv(ExecParam ExecParam, EREC_IraIktatoKonyvek _EREC_IraIktatoKonyvek
        , EREC_Irat_IktatokonyveiSearch _EREC_Irat_IktatokonyveiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByIktatoKonyv(ExecParam, _EREC_IraIktatoKonyvek, _EREC_Irat_IktatokonyveiSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod]
    public Result AddCsoportokToIktatokonyv(ExecParam ExecParam, string IraIktatokonyv_Id, List<string> Csoport_Ids_Iktathat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Debug("jelenleg hozz�rendelt csoportok lek�r�se");

            EREC_IraIktatoKonyvek IraIktatoKonyv = new EREC_IraIktatoKonyvek();
            IraIktatoKonyv.Id = IraIktatokonyv_Id;

            Result resultGetAll = GetAllByIktatoKonyv(ExecParam, IraIktatoKonyv, new EREC_Irat_IktatokonyveiSearch());

            if (!String.IsNullOrEmpty(resultGetAll.ErrorCode))
            {
                throw new ResultException(resultGetAll);
            }

            List<string> CurrentCsoportIds = new List<string>();

            foreach (DataRow row in resultGetAll.Ds.Tables[0].Rows)
            {
                string csoportId = row["Csoport_Id_Iktathat"].ToString();
                CurrentCsoportIds.Add(csoportId);
            }

            List<string> AddCsoportIds = new List<string>();

            foreach (string csoportId in Csoport_Ids_Iktathat)
            {
                if (!CurrentCsoportIds.Contains(csoportId))
                {
                    AddCsoportIds.Add(csoportId);
                }
            }

            Logger.Debug("�j csoportok hozz�rendel�se");

            foreach (string csoportId in AddCsoportIds)
            {
                EREC_Irat_Iktatokonyvei iik = new EREC_Irat_Iktatokonyvei();
                iik.Csoport_Id_Iktathat = csoportId;
                iik.IraIktatokonyv_Id = IraIktatokonyv_Id;
                Result resultAdd = Insert(ExecParam, iik);

                if (!String.IsNullOrEmpty(resultAdd.ErrorCode))
                {
                    throw new ResultException(resultAdd);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}