﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IratelemKapcsolatokService : System.Web.Services.WebService
{

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IratelemKapcsolatokSearch))]
    public Result GetAllWithExtension(ExecParam execParam, EREC_IratelemKapcsolatokSearch erec_IratelemKapcsolatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(execParam, erec_IratelemKapcsolatokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    [WebMethod()]
    public Result CsatolmanyokKoteseMelleklethez(ExecParam execParam, string mellekletId, string[] csatolmanyokIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        // Paraméterek ellenőrzése:
        if (execParam == null
            || string.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || string.IsNullOrEmpty(mellekletId)
            || csatolmanyokIds == null || csatolmanyokIds.Length == 0)
        {
            // hiba:
            return ResultError.CreateNewResultWithErrorCode(52420);
        }
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            foreach (string csatolmanyId in csatolmanyokIds)
            {
                #region EREC_IratelemKapcsolatok INSERT

                EREC_IratelemKapcsolatok iratElemKapcs = new EREC_IratelemKapcsolatok();

                iratElemKapcs.Csatolmany_Id = csatolmanyId;
                iratElemKapcs.Updated.Csatolmany_Id = true;

                iratElemKapcs.Melleklet_Id = mellekletId;
                iratElemKapcs.Updated.Melleklet_Id = true;

                ExecParam execParam_insert = execParam.Clone();
                result = this.Insert(execParam_insert, iratElemKapcs);

                if (result.IsError)
                {
                    // hiba:
                    throw new ResultException(result);
                }

                #endregion
            }
            
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }



    [WebMethod()]
    public Result MellekletekKoteseCsatolmanyhoz(ExecParam execParam, string csatolmanyId, string[] mellekletekIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        // Paraméterek ellenőrzése:
        if (execParam == null
            || string.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || string.IsNullOrEmpty(csatolmanyId)
            || mellekletekIds == null || mellekletekIds.Length == 0)
        {
            // hiba:
            return ResultError.CreateNewResultWithErrorCode(52420);
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            foreach (string mellekletId in mellekletekIds)
            {
                #region EREC_IratelemKapcsolatok INSERT

                EREC_IratelemKapcsolatok iratElemKapcs = new EREC_IratelemKapcsolatok();

                iratElemKapcs.Csatolmany_Id = csatolmanyId;
                iratElemKapcs.Updated.Csatolmany_Id = true;

                iratElemKapcs.Melleklet_Id = mellekletId;
                iratElemKapcs.Updated.Melleklet_Id = true;

                ExecParam execParam_insert = execParam.Clone();
                result = this.Insert(execParam_insert, iratElemKapcs);

                if (result.IsError)
                {
                    // hiba:
                    throw new ResultException(result);
                }

                #endregion
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

}